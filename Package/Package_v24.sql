ALTER SESSION SET CURRENT_SCHEMA=c##SCF;
--------------------------------------------------------
-- DATA: 04/06/2019
-- MELHORIA: RELATORIO DE TRANSACAO QUANDO SELECIONA TODOS OS FILTROS
-- OCORRIA ERRO E NÃO LOGAVA TUDO 
--------------------------------------------------------
/
CREATE OR REPLACE PACKAGE "PKG_SCF"
AS
type TP_REFCURSOR
IS
  ref
  CURSOR;
    PROCEDURE PR_CONFIG_S(
        pTIPO_OBJETO VARCHAR2 := NULL,
        pCONTEUDO_OBJETO CLOB := NULL,
        retornarRegistro CHAR := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONFIG_L(
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 100 ) ;
    PROCEDURE PR_CONFIG_B(
        pTIPO_OBJETO VARCHAR2,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONFIG_R(
        pTIPO_OBJETO NUMBER );
    PROCEDURE PR_ARQUIVO_S(
        pCODIGO_ARQUIVO       NUMBER   := NULL,
        pTIPO_ARQUIVO         VARCHAR2 := NULL,
        pLAYOUT_ARQUIVO       VARCHAR2 := NULL,
        pNOME_ARQUIVO         VARCHAR2 := NULL,
        pSTATUS_ARQUIVO       CHAR     := NULL,
        pCODIGO_PROCESSO      NUMBER   := NULL,
        pCHAVE_ARQUIVO        NUMBER   := NULL,
        pCODIGO_EMPRESA_GRUPO NUMBER   := NULL,
        retornarRegistro      CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ARQUIVO_L(
        pFILTRO_NOME_ARQUIVO    VARCHAR2 := NULL,
        pFILTRO_STATUS_ARQUIVO  CHAR     := NULL,
        pFILTRO_DATA_INCLUSAO   DATE     := NULL,
        pFILTRO_CODIGO_PROCESSO NUMBER   := NULL,
        pFILTRO_CHAVE_ARQUIVO   VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_ARQUIVO_B(
        pCODIGO_ARQUIVO NUMBER,
        retornarDetalhe CHAR := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ARQUIVO_R(
        pCODIGO_ARQUIVO NUMBER := NULL );
    PROCEDURE PR_ARQUIVO_ITEM_STATUS_A(
        pCODIGO_ARQUIVO NUMBER );
    PROCEDURE PR_ARQUIVO_ITEM_STATUS_S(
        pFILTRO_CODIGO_ARQUIVO      NUMBER   := NULL,
        pFILTRO_CODIGO_ARQUIVO_ITEM NUMBER   := NULL,
        pFILTRO_TIPO_ARQUIVO_ITEM   VARCHAR2 := NULL,
        pFILTRO_TIPO_CRITICA        VARCHAR2 := NULL,
        pFILTRO_NOME_CAMPO          VARCHAR2 := NULL,
        pFILTRO_STATUS_ARQUIVO_ITEM CHAR     := NULL,
        pSTATUS_ARQUIVO_ITEM        CHAR,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ARQUIVO_ITEM_S(
        pCODIGO_ARQUIVO_ITEM NUMBER := NULL,
        pCODIGO_ARQUIVO      NUMBER := NULL,
        pCONTEUDO_ARQUIVO_ITEM CLOB := NULL,
        pTIPO_ARQUIVO_ITEM   VARCHAR2 := NULL,
        pSTATUS_ARQUIVO_ITEM CHAR     := NULL,
        pCHAVE               VARCHAR2 := NULL,
        pREFERENCIA          VARCHAR2 := NULL,
        retornarRegistro     CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ARQUIVO_ITEM_L(
        pCODIGO_ARQUIVO             NUMBER   := NULL,
        pCODIGO_ARQUIVO_ITEM        NUMBER   := NULL,
        pTIPO_ARQUIVO_ITEM          VARCHAR2 := NULL,
        pTIPO_CRITICA               VARCHAR2 := NULL,
        pNOME_CAMPO                 VARCHAR2 := NULL,
        pCODIGO_PROCESSO            NUMBER   := NULL,
        pFILTRARSEMCRITICA          VARCHAR2 := 'N',
        pFILTRARAPENASSOBANALISE    VARCHAR2 := 'N',
        pFILTRARCOMCRITICA          VARCHAR2 := 'N',
        pRECEBERPENDENTEDESBLOQUEIO VARCHAR2 := 'N',
        pFILTRARNAOBLOQUEADOS       VARCHAR2 := 'N',
        pFILTRARAPENASBLOQUEADOS    VARCHAR2 := 'N',
        pIGNORAR_CONTEUDO           VARCHAR2 := 'N',
        ctSTATUS_BLOQUEADO          CHAR     := NULL,
        ctSTATUS_EXCLUIDO           CHAR     := NULL,
        ctSTATUS_CANCELADO          VARCHAR2 := 'N',
        pSTATUS_ARQUIVO_ITEM        CHAR     := NULL,
        pSTATUS_ARQUIVO_ITEM2       CHAR     := NULL,
        pCHAVE                      VARCHAR2 := NULL,
        pCHAVE_INICIO               VARCHAR2 := NULL,
        MAXLINHAS                   INT      := 0,
        LIMITE_MINIMO               NUMBER   := 0,
        LIMITE_MAXIMO               NUMBER   := 0,
        pDTINCINICIAL               DATE     := NULL,
        pDTINCFINAL                 DATE     := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ARQUIVO_ITEM_B(
        pCODIGO_ARQUIVO_ITEM NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ARQUIVO_ITEM_R(
        pCODIGO_ARQUIVO NUMBER := NULL );
    PROCEDURE PR_CRITICA_S(
        pCODIGO_CRITICA      NUMBER   := NULL,
        pCODIGO_PROCESSO     NUMBER   := NULL,
        pCODIGO_ARQUIVO      NUMBER   := NULL,
        pCODIGO_ARQUIVO_ITEM NUMBER   := NULL,
        pDATA_CRITICA        DATE     := NULL,
        pTIPO_CRITICA        VARCHAR2 := NULL,
        pDESCRICAO_CRITICA   VARCHAR2 := NULL,
        pCONTEUDO_CRITICA CLOB        := NULL,
        pTIPO_LINHA      VARCHAR2          := NULL,
        pNOME_CAMPO      VARCHAR2          := NULL,
        retornarRegistro CHAR              := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CRITICA_L(
        pFILTRO_CODIGO_ARQUIVO    NUMBER   := NULL,
        pFILTRO_CODIGO_PROCESSO   NUMBER   := NULL,
        pFILTRO_TIPO_CRITICA      VARCHAR2 := NULL,
        pFILTRO_TIPO_LINHA        VARCHAR2 := NULL,
        pFILTRO_NOME_CAMPO        VARCHAR2 := NULL,
        pFILTRO_DESCRICAO_CRITICA VARCHAR2 := NULL,
        pFILTRO_AGRUPADO          INT      := 0,
        pCODIGO_ARQUIVO_ITEM      NUMBER   := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_CRITICA_B(
        pCODIGO_CRITICA NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CRITICA_R(
        pCODIGO_CRITICA NUMBER := NULL,
        pCODIGO_ARQUIVO NUMBER := NULL );
    PROCEDURE PR_CRITICA_AGRUPAMENTO_L(
        pCODIGO_PROCESSO NUMBER := NULL,
        pCODIGO_ARQUIVO  NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CRITICA_AGRUPAMENTO_DET_B(
        pCODIGO_ARQUIVO NUMBER   := NULL,
        pTIPO_LINHA     VARCHAR2 := NULL,
        pTIPO_CRITICA   VARCHAR2 := NULL,
        pNOME_CAMPO     VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_SUBSTITUI_CONTA(
        pCODIGO_CONTA_FAVORECIDO NUMBER   := NULL ,
        pCODIGO_PRODUTO          NUMBER   := NULL ,
        pREFERENCIA              VARCHAR2 := NULL ,
        pQTDE_ALTERADA OUT NUMBER );
    PROCEDURE PR_TRANSACAO_S(
        pCODIGO_TRANSACAO OUT NUMBER,
        pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
        pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
        pSTATUS_TRANSACAO             NUMBER   := NULL,
        pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
        pNSU_HOST                     VARCHAR2 := NULL,
        pDATA_TRANSACAO               DATE     := NULL,
        pTIPO_TRANSACAO               VARCHAR2 := NULL,
        pCODIGO_ARQUIVO               NUMBER   := NULL,
        pCODIGO_ARQUIVO2              NUMBER   := NULL,
        pCHAVE                        VARCHAR2 := NULL,
        pCHAVE2                       VARCHAR2 := NULL,
        pCHAVE3                       VARCHAR2 := NULL,
        pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
        pCODIGO_PROCESSO              NUMBER   := NULL,
        pSTATUS_RET_CESSAO            NUMBER   := NULL,
        pCODIGO_PROCESSO_RET_CESSAO   NUMBER   := NULL,
        pCODIGO_ARQUIVO_RET_CESSAO    NUMBER   := NULL,
        pDATA_REPASSE                 DATE     := NULL,
        pCODIGO_PRODUTO               NUMBER   := NULL,
        pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
        pCODIGO_FAVORECIDO            NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
        pDATA_REPASSE_CALCULADA       DATE     := NULL,
        pVALOR_REPASSE                NUMBER   := NULL,
        pDATA_MOVIMENTO               DATE     := NULL,
        pFL_ATUALIZA_TRANSACAO        CHAR     := 'N',
        pTIPO_FINANCEIRO_CONTABIL     CHAR     := NULL,
        pVALOR_BRUTO                  NUMBER   := NULL,
        pVALOR_DESCONTO               NUMBER   := NULL,
        pVALOR_LIQUIDO                NUMBER   := NULL,
        pREFERENCIA                   VARCHAR2 := NULL,
        retornarRegistro              CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_STATUS_S(
        pCODIGO_ARQUIVO_ITEM NUMBER,
        pSTATUS_TRANSACAO    NUMBER,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_TRANSACAO_L(
        pFILTRO_CODIGO_FAVORECIDO  NUMBER   := NULL,
        pCODIGO_ARQUIVO            NUMBER   := NULL,
        pTIPO_TRANSACAO            VARCHAR2 := NULL,
        pDATA_INCLUSAO             DATE     := NULL,
        pDATA_INCLUSAO_MAIOR       DATE     := NULL,
        pDATA_INCLUSAO_MENOR       DATE     := NULL,
        pCODIGO_ARQUIVO_RET_CESSAO NUMBER   := NULL,
        pSTATUS_RET_CESSAO         NUMBER   := NULL,
        pNSU_HOST                  VARCHAR2 := NULL,
        pCHAVE                     VARCHAR2 := NULL,
        pCHAVE_LIKE                VARCHAR2 := NULL,
        pCHAVE2                    VARCHAR2 := NULL,
        pFILTRO_RETORNO_CESSAO     CHAR     := 'N',
        pCODIGO_PROCESSO           NUMBER   := NULL,
        pDATA_MOVIMENTO_MAIOR      DATE     := NULL,
        pDATA_MOVIMENTO_MENOR      DATE     := NULL,
        pCODIGO_ESTABELECIMENTO    NUMBER   := NULL,
        pREFERENCIA                VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0);
    PROCEDURE PR_TRANSACAO_B(
        pCODIGO_TRANSACAO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_R(
        pCODIGO_TRANSACAO NUMBER := NULL );
    PROCEDURE PR_TRANSACAO_AJUSTE_S(
        pCODIGO_TRANSACAO OUT NUMBER,
        pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
        pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
        pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
        pNSU_HOST                     VARCHAR2 := NULL,
        pDATA_TRANSACAO               DATE     := NULL,
        pCODIGO_ARQUIVO               NUMBER   := NULL,
        pCODIGO_ARQUIVO2              NUMBER   := NULL,
        pCHAVE                        VARCHAR2 := NULL,
        pCHAVE2                       VARCHAR2 := NULL,
        pCHAVE3                       VARCHAR2 := NULL,
        pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
        pCODIGO_PROCESSO              NUMBER   := NULL,
        pDATA_TRANSACAO_ORIGINAL      DATE     := NULL,
        pTIPO_LANCAMENTO              VARCHAR2 := NULL,
        pDATA_REPASSE                 DATE     := NULL,
        pTIPO_AJUSTE                  VARCHAR2 := NULL,
        pCODIGO_AJUSTE                VARCHAR2 := NULL,
        pMOTIVO_AJUSTE                VARCHAR2 := NULL,
        pVALOR_AJUSTE                 NUMBER   := NULL,
        pVALOR_DESCONTO               NUMBER   := NULL,
        pVALOR_LIQUIDO                NUMBER   := NULL,
        pBANCO                        VARCHAR2 := NULL,
        pAGENCIA                      VARCHAR2 := NULL,
        pCONTA                        VARCHAR2 := NULL,
        pNSU_HOST_ORIGINAL            VARCHAR2 := NULL,
        pCODIGO_PRODUTO               NUMBER   := NULL,
        pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
        pCODIGO_FAVORECIDO            NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
        pDATA_REPASSE_CALCULADA       DATE     := NULL,
        pVALOR_REPASSE                NUMBER   := NULL,
        pSTATUS_RET_CESSAO            NUMBER   := NULL,
        pSTATUS_TRANSACAO             NUMBER   := NULL,
        pDATA_MOVIMENTO               DATE     := NULL,
        pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
        pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
        pNUM_CONTA_CLIENTE        VARCHAR2        := NULL,
        pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
        pNUMERO_CARTAO            VARCHAR2        := NULL,
        pREFERENCIA               VARCHAR2        := NULL,
        retornarRegistro          CHAR            := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_AJUSTE_B(
        pCODIGO_TRANSACAO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_AJUSTE_L(
        pCODIGO_ARQUIVO NUMBER := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_TRANSACAO_AJUSTE_R(
        pCODIGO_TRANSACAO NUMBER );
    PROCEDURE PR_TRANSACAO_ANULACAO_PAGTO_S(
        pCODIGO_TRANSACAO OUT NUMBER,
        pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
        pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
        pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
        pNSU_HOST                     VARCHAR2 := NULL,
        pDATA_TRANSACAO               DATE     := NULL,
        pCODIGO_ARQUIVO               NUMBER   := NULL,
        pCODIGO_ARQUIVO2              NUMBER   := NULL,
        pCHAVE                        VARCHAR2 := NULL,
        pCHAVE2                       VARCHAR2 := NULL,
        pCHAVE3                       VARCHAR2 := NULL,
        pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
        pCODIGO_PROCESSO              NUMBER   := NULL,
        pNSU_HOST_ORIGINAL            VARCHAR2 := NULL,
        pNSU_TEF_ORIGINAL             VARCHAR2 := NULL,
        pDATA_TRANSACAO_ORIGINAL      DATE     := NULL,
        pCODIGO_AUTORIZACAO_ORIGINAL  VARCHAR2 := NULL,
        pTIPO_LANCAMENTO              VARCHAR2 := NULL,
        pDATA_REEMBOLSO               DATE     := NULL,
        pFORMA_MEIO_PAGAMENTO         VARCHAR2 := NULL,
        pCODIGO_ANULACAO              VARCHAR2 := NULL,
        pMOTIVO_ANULACAO              VARCHAR2 := NULL,
        pVALOR_PAGAMENTO              NUMBER   := NULL,
        pVALOR_DESCONTO               NUMBER   := NULL,
        pVALOR_LIQUIDO                NUMBER   := NULL,
        pNUMERO_CARTAO                VARCHAR2 := NULL,
        pNUMERO_CONTA_CLIENTE         VARCHAR2 := NULL,
        pCODIGO_PRODUTO               NUMBER   := NULL,
        pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
        pCODIGO_FAVORECIDO            NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
        pDATA_REPASSE                 DATE     := NULL,
        pDATA_REPASSE_CALCULADA       DATE     := NULL,
        pVALOR_REPASSE                NUMBER   := NULL,
        pSTATUS_RET_CESSAO            NUMBER   := NULL,
        pSTATUS_TRANSACAO             NUMBER   := NULL,
        pDATA_MOVIMENTO               DATE     := NULL,
        pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
        pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
        pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
        pREFERENCIA               VARCHAR2        := NULL,
        retornarRegistro          CHAR            := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_ANULACAO_PAGTO_B(
        pCODIGO_TRANSACAO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_ANULACAO_PAGTO_L(
        pCODIGO_ARQUIVO NUMBER := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_TRANSACAO_ANULACAO_PAGTO_R(
        pCODIGO_TRANSACAO NUMBER );
    PROCEDURE PR_TRANSACAO_ANULACAO_VENDA_S(
        pCODIGO_TRANSACAO OUT NUMBER,
        pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
        pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
        pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
        pNSU_HOST                     VARCHAR2 := NULL,
        pDATA_TRANSACAO               DATE     := NULL,
        pCODIGO_ARQUIVO               NUMBER   := NULL,
        pCODIGO_ARQUIVO2              NUMBER   := NULL,
        pCHAVE                        VARCHAR2 := NULL,
        pCHAVE2                       VARCHAR2 := NULL,
        pCHAVE3                       VARCHAR2 := NULL,
        pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
        pCODIGO_PROCESSO              NUMBER   := NULL,
        pNSU_HOST_ORIGINAL            VARCHAR2 := NULL,
        pNSU_TEF_ORIGINAL             VARCHAR2 := NULL,
        pDATA_TRANSACAO_ORIGINAL      DATE     := NULL,
        pCODIGO_AUTORIZACAO_ORIGINAL  VARCHAR2 := NULL,
        pTIPO_LANCAMENTO              VARCHAR2 := NULL,
        pDATA_REEMBOLSO               DATE     := NULL,
        pCODIGO_ANULACAO              VARCHAR2 := NULL,
        pMOTIVO_ANULACAO              VARCHAR2 := NULL,
        pVALOR_ANULACAO               NUMBER   := NULL,
        pVALOR_DESCONTO               NUMBER   := NULL,
        pVALOR_LIQUIDO                NUMBER   := NULL,
        pNUMERO_CARTAO                VARCHAR2 := NULL,
        pNUMERO_PARCELA               INT      := NULL,
        pNUMERO_PARCELA_TOTAL         INT      := NULL,
        pVALOR_PARCELA                NUMBER   := NULL,
        pVALOR_PARCELA_DESCONTO       NUMBER   := NULL,
        pVALOR_PARCELA_LIQUIDO        NUMBER   := NULL,
        pBANCO                        VARCHAR2 := NULL,
        pAGENCIA                      VARCHAR2 := NULL,
        pCONTA                        VARCHAR2 := NULL,
        pNUMERO_CONTA_CLIENTE         VARCHAR2 := NULL,
        pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
        pCODIGO_FAVORECIDO            NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
        pCODIGO_PRODUTO               NUMBER   := NULL,
        pDATA_REPASSE                 DATE     := NULL,
        pDATA_REPASSE_CALCULADA       DATE     := NULL,
        pVALOR_REPASSE                NUMBER   := NULL,
        pSTATUS_RET_CESSAO            NUMBER   := NULL,
        pSTATUS_TRANSACAO             NUMBER   := NULL,
        pDATA_MOVIMENTO               DATE     := NULL,
        pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
        pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
        pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
        pREFERENCIA               VARCHAR2        := NULL,
        retornarRegistro          CHAR            := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_ANULACAO_VENDA_B(
        pCODIGO_TRANSACAO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_ANULACAO_VENDA_L(
        pCODIGO_ARQUIVO NUMBER := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_TRANSACAO_ANULACAO_VENDA_R(
        pCODIGO_TRANSACAO NUMBER );
    PROCEDURE PR_TRANSACAO_PAGAMENTO_S(
        pCODIGO_TRANSACAO OUT NUMBER,
        pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
        pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
        pSTATUS_TRANSACAO             NUMBER   := NULL,
        pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
        pNSU_HOST                     VARCHAR2 := NULL,
        pDATA_TRANSACAO               DATE     := NULL,
        pCODIGO_ARQUIVO               NUMBER   := NULL,
        pCODIGO_ARQUIVO2              NUMBER   := NULL,
        pCHAVE                        VARCHAR2 := NULL,
        pCHAVE2                       VARCHAR2 := NULL,
        pCHAVE3                       VARCHAR2 := NULL,
        pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
        pCODIGO_PROCESSO              NUMBER   := NULL,
        pNSU_TEF                      VARCHAR2 := NULL,
        pCODIGO_AUTORIZACAO           VARCHAR2 := NULL,
        pTIPO_LANCAMENTO              VARCHAR2 := NULL,
        pDATA_REPASSE                 DATE     := NULL,
        pMEIO_CAPTURA                 VARCHAR2 := NULL,
        pVALOR_PAGAMENTO              NUMBER   := NULL,
        pVALOR_DESCONTO               NUMBER   := NULL,
        pVALOR_LIQUIDO                NUMBER   := NULL,
        pNUMERO_CARTAO                VARCHAR2 := NULL,
        pQUANTIDADE_MEIO_PAGAMENTO    INT      := NULL,
        pMEIO_PAGAMENTO               VARCHAR2 := NULL,
        pMEIO_PAGAMENTO_SEQ           INT      := NULL,
        pMEIO_PAGAMENTO_VALOR         NUMBER   := NULL,
        pBANCO                        VARCHAR2 := NULL,
        pAGENCIA                      VARCHAR2 := NULL,
        pCONTA                        VARCHAR2 := NULL,
        pNUMERO_CONTA_CLIENTE         VARCHAR2 := NULL,
        pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
        pCODIGO_FAVORECIDO            NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
        pCODIGO_PRODUTO               NUMBER   := NULL,
        pDATA_REPASSE_CALCULADA       DATE     := NULL,
        pVALOR_REPASSE                NUMBER   := NULL,
        pSTATUS_RET_CESSAO            NUMBER   := NULL,
        pDATA_MOVIMENTO               DATE     := NULL,
        pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
        pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
        pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
        pREFERENCIA               VARCHAR2        := NULL,
        retornarRegistro          CHAR            := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_PAGAMENTO_B(
        pCODIGO_TRANSACAO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_PAGAMENTO_L(
        pCODIGO_ARQUIVO NUMBER := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_TRANSACAO_PAGAMENTO_R(
        pCODIGO_TRANSACAO NUMBER );
    PROCEDURE PR_TRANSACAO_VENDA_S(
        pCODIGO_TRANSACAO OUT NUMBER,
        pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
        pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
        pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
        pNSU_HOST                     VARCHAR2 := NULL,
        pDATA_TRANSACAO               DATE     := NULL,
        pCODIGO_ARQUIVO               NUMBER   := NULL,
        pCODIGO_ARQUIVO2              NUMBER   := NULL,
        pCHAVE                        VARCHAR2 := NULL,
        pCHAVE2                       VARCHAR2 := NULL,
        pCHAVE3                       VARCHAR2 := NULL,
        pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
        pCODIGO_PROCESSO              NUMBER   := NULL,
        pNSU_TEF                      VARCHAR2 := NULL,
        pCODIGO_AUTORIZACAO           VARCHAR2 := NULL,
        pDATA_REPASSE                 DATE     := NULL,
        pTIPO_PRODUTO                 VARCHAR2 := NULL,
        pMEIO_CAPTURA                 VARCHAR2 := NULL,
        pVALOR_VENDA                  NUMBER   := NULL,
        pVALOR_DESCONTO               NUMBER   := NULL,
        pVALOR_LIQUIDO                NUMBER   := NULL,
        pNUMERO_CARTAO                VARCHAR2 := NULL,
        pNUMERO_PARCELA               INT      := NULL,
        pNUMERO_PARCELA_TOTAL         INT      := NULL,
        pVALOR_PARCELA                NUMBER   := NULL,
        pVALOR_PARCELA_DESCONTO       NUMBER   := NULL,
        pVALOR_PARCELA_LIQUIDO        NUMBER   := NULL,
        pBANCO                        VARCHAR2 := NULL,
        pAGENCIA                      VARCHAR2 := NULL,
        pCONTA                        VARCHAR2 := NULL,
        pNUMERO_CONTA_CLIENTE         VARCHAR2 := NULL,
        pCODIGO_PRODUTO               INT      := NULL,
        pCODIGO_PLANO                 INT      := NULL,
        pCUPOM_FISCAL                 VARCHAR2 := NULL,
        pMODALIDADE                   VARCHAR2 := NULL,
        pTIPO_LANCAMENTO              VARCHAR2 := NULL,
        pVALOR_COMISSAO               NUMBER   := NULL,
        pSTATUS_RET_CESSAO            NUMBER   := NULL,
        pCODIGO_PROCESSO_RET_CESSAO   NUMBER   := NULL,
        pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
        pCODIGO_FAVORECIDO            NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
        pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
        retornarRegistro              CHAR     := 'N',
        pBANCO_ORIGINAL               VARCHAR2 := NULL,
        pAGENCIA_ORIGINAL             VARCHAR2 := NULL,
        pCONTA_ORIGINAL               VARCHAR2 := NULL,
        pCODIGO_ARQUIVO_RET_CESSAO    NUMBER   := NULL,
        pDATA_REPASSE_CALCULADA       DATE     := NULL,
        pSTATUS_TRANSACAO             NUMBER   := NULL,
        pVALOR_REPASSE                NUMBER   := NULL,
        pDATA_MOVIMENTO               DATE     := NULL,
        pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
        pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
        pDATA_PRE_AUTORIZACAO     DATE            := NULL,
        pNSU_HOST_ORIGINAL        VARCHAR2        := NULL,
        pNSU_HOST_REVERSAO        VARCHAR2        := NULL,
        pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
        pREFERENCIA               VARCHAR2        := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_VENDA_B(
        pCODIGO_TRANSACAO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TRANSACAO_VENDA_L(
        pCODIGO_ARQUIVO     NUMBER  := NULL,
        pDATA_INCLUSAO      DATE    := NULL,
        pCODIGO_PROCESSO    NUMBER  := NULL,
        pSTATUS_CONCILIACAO CHAR    := NULL,
        pCHAVE              VARCHAR := NULL,
        pINFORMAR_ORIGEM    VARCHAR := 'N',
        pCODIGO_PLANO       NUMBER  := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_TRANSACAO_VENDA_R(
        pCODIGO_TRANSACAO NUMBER );
    PROCEDURE PR_TRANSACAO_VENDA_AGENDAR(
        pCODIGO_PROCESSO      NUMBER,
        pDATA_PAGAMENTO       DATE,
        pGERAR_APENAS_ORIGEM2 CHAR := 'N' );
    PROCEDURE PR_TRANSACAO_NAO_CONCILIADA_L(
        pTIPO_PROCESSO VARCHAR := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_S(
        pCODIGO_USUARIO        VARCHAR2 := NULL,
        pNOME_USUARIO          VARCHAR2 := NULL,
        pSENHA                 VARCHAR2 := NULL,
        pASSINATURA_ELETRONICA VARCHAR2 := NULL,
        pSTATUS                NUMBER   := NULL,
        pEMAIL                 VARCHAR2 := NULL,
        pDATA_EXPIRACAO_SENHA  DATE     := NULL,
        pCPF                   VARCHAR2 := NULL,
        pMATRICULA             VARCHAR2 := NULL,
        pBLOQUEADO             CHAR     := NULL,
        pDATA_BLOQUEADO_INICIO DATE     := NULL,
        pDATA_BLOQUEADO_FIM    DATE     := NULL,
        pLOGIN                 VARCHAR  := NULL,
        retornarRegistro       CHAR     := 'N',
        retornarPermissoes     CHAR     := 'N',
        retornarPerfisAcesso   CHAR     := 'N',
        retornarGruposAcesso   CHAR     := 'N',
        retcur OUT TP_REFCURSOR,
        retcurPermissoes OUT TP_REFCURSOR,
        retcurPerfisAcesso OUT TP_REFCURSOR,
        retcurGruposAcesso OUT TP_REFCURSOR,
        retcurGrupoPerfilAcesso OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_B(
        pCODIGO_USUARIO           VARCHAR2,
        retornarPermissoes        CHAR := 'N',
        retornarPerfisAcesso      CHAR := 'N',
        retornarGruposAcesso      CHAR := 'N',
        retornarGrupoPerfilAcesso CHAR := 'N',
        retcur OUT TP_REFCURSOR,
        retcurPermissoes OUT TP_REFCURSOR,
        retcurPerfisAcesso OUT TP_REFCURSOR,
        retcurGruposAcesso OUT TP_REFCURSOR,
        retcurGrupoPerfilAcesso OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_L(
        pNOME_USUARIO VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 100 );
    PROCEDURE PR_USUARIO_R(
        pCODIGO_USUARIO VARCHAR2 := NULL) ;
    PROCEDURE PR_PERFIL_S(
        pCODIGO_PERFIL     NUMBER   := NULL,
        pNOME_PERFIL       VARCHAR2 := NULL,
        retornarRegistro   CHAR     := 'N',
        retornarPermissoes CHAR     := 'N',
        retcurPermissoes OUT TP_REFCURSOR,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PERFIL_B(
        pCODIGO_PERFIL     NUMBER,
        retornarPermissoes CHAR := 'N',
        retcur OUT TP_REFCURSOR,
        retcurPermissoes OUT TP_REFCURSOR );
    PROCEDURE PR_PERFIL_L(
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_PERFIL_R(
        pCODIGO_PERFIL NUMBER );
    PROCEDURE PR_GRUPO_ACESSO_S(
        pCODIGO_GRUPO_ACESSO NUMBER   := NULL,
        pNOME_GRUPO_ACESSO   VARCHAR2 := NULL,
        retornarRegistro     CHAR     := 'N',
        retornarPermissoes   CHAR     := 'N',
        retornarPerfisAcesso CHAR     := 'N',
        retcurPerfisAcesso OUT TP_REFCURSOR,
        retcurPermissoes OUT TP_REFCURSOR,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_GRUPO_ACESSO_B(
        pCODIGO_GRUPO_ACESSO NUMBER,
        retornarPermissoes   CHAR := 'N',
        retornarPerfisAcesso CHAR := 'N',
        retcur OUT TP_REFCURSOR,
        retcurPermissoes OUT TP_REFCURSOR,
        retcurPerfisAcesso OUT TP_REFCURSOR );
    PROCEDURE PR_GRUPO_ACESSO_L(
        pCODIGO_GRUPO_ACESSO NUMBER := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_GRUPO_ACESSO_R(
        pCODIGO_GRUPO_ACESSO NUMBER );
    PROCEDURE PR_USUARIO_PERMISSAO_S(
        pCODIGO_USUARIO   VARCHAR2 := NULL,
        pCODIGO_PERMISSAO NUMBER   := NULL,
        pSTATUS_PERMISSAO NUMBER   := NULL,
        retornarRegistro  CHAR     := 'N',
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_USUARIO_PERMISSAO_B(
        pCODIGO_USUARIO   VARCHAR2,
        pCODIGO_PERMISSAO NUMBER := NULL ,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_USUARIO_PERMISSAO_L(
        pCODIGO_USUARIO   VARCHAR2 := NULL,
        pCODIGO_PERMISSAO NUMBER   := NULL ,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_USUARIO_PERMISSAO_R(
        pCODIGO_USUARIO   VARCHAR2,
        pCODIGO_PERMISSAO NUMBER );
    PROCEDURE PR_USUARIO_GRUPO_ACESSO_S(
        pCODIGO_USUARIO      VARCHAR2 := NULL,
        pCODIGO_GRUPO_ACESSO NUMBER   := NULL,
        retornarRegistro     CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_GRUPO_ACESSO_B(
        pCODIGO_USUARIO      VARCHAR2,
        pCODIGO_GRUPO_ACESSO NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_GRUPO_ACESSO_L(
        pCODIGO_USUARIO      VARCHAR2 := NULL,
        pCODIGO_GRUPO_ACESSO NUMBER   := NULL ,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_GRUPO_ACESSO_R(
        pCODIGO_USUARIO      VARCHAR2,
        pCODIGO_GRUPO_ACESSO NUMBER );
    PROCEDURE PR_USUARIO_PERFIL_S(
        pCODIGO_USUARIO  VARCHAR2 := NULL,
        pCODIGO_PERFIL   NUMBER   := NULL,
        retornarRegistro CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_PERFIL_B(
        pCODIGO_USUARIO VARCHAR2,
        pCODIGO_PERFIL  NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_PERFIL_L(
        pCODIGO_USUARIO VARCHAR2 := NULL,
        pCODIGO_PERFIL  NUMBER   := NULL ,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_USUARIO_PERFIL_R(
        pCODIGO_USUARIO VARCHAR2,
        pCODIGO_PERFIL  NUMBER );
    PROCEDURE PR_PERFIL_PERMISSAO_S(
        pCODIGO_PERFIL    NUMBER := NULL,
        pCODIGO_PERMISSAO NUMBER := NULL,
        pSTATUS_PERMISSAO NUMBER := NULL,
        retornarRegistro  CHAR   := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PERFIL_PERMISSAO_B(
        pCODIGO_PERFIL    NUMBER,
        pCODIGO_PERMISSAO NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PERFIL_PERMISSAO_L(
        pCODIGO_PERFIL    NUMBER := NULL,
        pCODIGO_PERMISSAO NUMBER := NULL ,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PERFIL_PERMISSAO_R(
        pCODIGO_PERFIL    NUMBER,
        pCODIGO_PERMISSAO NUMBER );
    PROCEDURE PR_GRUPO_ACESSO_PERMISSAO_S(
        pCODIGO_GRUPO_ACESSO VARCHAR2 := NULL,
        pCODIGO_PERMISSAO    NUMBER   := NULL,
        pSTATUS_PERMISSAO    NUMBER   := NULL,
        retornarRegistro     CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_GRUPO_ACESSO_PERMISSAO_B(
        pCODIGO_GRUPO_ACESSO VARCHAR2,
        pCODIGO_PERMISSAO    NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_GRUPO_ACESSO_PERMISSAO_L(
        pCODIGO_GRUPO_ACESSO VARCHAR2 := NULL,
        pCODIGO_PERMISSAO    NUMBER   := NULL ,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_GRUPO_ACESSO_PERMISSAO_R(
        pCODIGO_GRUPO_ACESSO VARCHAR2,
        pCODIGO_PERMISSAO    NUMBER );
    PROCEDURE PR_GRUPO_ACESSO_PERFIL_S(
        pCODIGO_GRUPO_ACESSO NUMBER := NULL,
        pCODIGO_PERFIL       NUMBER := NULL,
        retornarRegistro     CHAR   := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_GRUPO_ACESSO_PERFIL_B(
        pCODIGO_GRUPO_ACESSO NUMBER,
        pCODIGO_PERFIL       NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_GRUPO_ACESSO_PERFIL_L(
        pCODIGO_GRUPO_ACESSO NUMBER := NULL,
        pCODIGO_PERFIL       NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_GRUPO_ACESSO_PERFIL_R(
        pCODIGO_GRUPO_ACESSO NUMBER,
        pCODIGO_PERFIL       NUMBER );
    PROCEDURE PR_LISTA_B(
        pCODIGO_LISTA    NUMBER   := NULL,
        pMNEMONICO_LISTA VARCHAR2 := NULL,
        retornarItens    CHAR     := 'N',
        retcur OUT TP_REFCURSOR,
        retcurItens OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_L(
        pNOME_LISTA VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_S(
        pCODIGO_LISTA    NUMBER,
        pNOME_LISTA      VARCHAR2 := NULL,
        pMNEMONICO_LISTA VARCHAR2 := NULL,
        retornarRegistro CHAR     := 'N',
        retornarItens    CHAR     := 'N',
        retcur OUT TP_REFCURSOR,
        retcurItens OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_R(
        pCODIGO_LISTA NUMBER := NULL );
    /******************************************************************
    *
    * TB_LISTA_ITEM
    *
    ******************************************************************/
    PROCEDURE PR_LISTA_ITEM_B(
        pCODIGO_LISTA_ITEM NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_ITEM_L(
        pFILTRO_CODIGO_LISTA NUMBER   := NULL,
        pNOME_LISTA          VARCHAR2 := NULL,
        pVALOR               VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR ) ;
    PROCEDURE PR_LISTA_ITEM_S(
        pCODIGO_LISTA_ITEM    NUMBER   := NULL,
        pCODIGO_LISTA         NUMBER   := NULL,
        pNOME_LISTA_ITEM      VARCHAR2 := NULL,
        pMNEMONICO_LISTA_ITEM VARCHAR2 := NULL,
        pVALOR                VARCHAR2 := NULL,
        pATIVO                CHAR     := 'S',
        retornarRegistro      CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_ITEM_R(
        pCODIGO_LISTA_ITEM NUMBER := NULL );
    /******************************************************************
    *
    * TB_PAGAMENTO
    *
    ******************************************************************/
    PROCEDURE PR_PAGAMENTO_B(
        pCODIGO_PAGAMENTO NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PAGAMENTO_L(
        pFILTRO_CODIGO_FAVORECIDO  NUMBER   := NULL,
        pFILTRO_DATA_PAGAMENTO     VARCHAR2 := NULL,
        pFILTRO_DATA_PAGAMENTO_FIM VARCHAR2 := NULL,
        pFILTRO_DATA_LIQUIDACAO    VARCHAR2 := NULL,
        pFILTRO_BANCO              VARCHAR2 := NULL,
        pFILTRO_AGENCIA            VARCHAR2 := NULL,
        pFILTRO_CONTA              VARCHAR2 := NULL,
        pFILTRO_STATUS_PAGAMENTO   CHAR     := NULL,
        pFILTRO_PAGAMENTO_NEGATIVO CHAR     := NULL,
        pFILTRO_DATA_ENVIO_DE      VARCHAR2 := NULL,
        pFILTRO_DATA_ENVIO_ATE     VARCHAR2 := NULL,
        pFILTRO_DATA_GERACAO_DE    VARCHAR2 := NULL,
        pFILTRO_DATA_GERACAO_ATE   VARCHAR2 := NULL,
        pRETORNAR_EXCLUIDOS        CHAR     := '0',
        pRETORNAR_QUANTIDADE       CHAR     := '0',
        pRETORNAR_REFERENCIAS      CHAR     := '0',
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_PAGAMENTO_S(
        pCODIGO_PAGAMENTO OUT NUMBER,
        pBANCO                   VARCHAR2 := NULL,
        pAGENCIA                 VARCHAR2 := NULL,
        pCONTA                   VARCHAR2 := NULL,
        pDATA_PAGAMENTO          DATE     := NULL,
        pVALOR_PAGAMENTO         NUMBER   := NULL,
        pSTATUS_PAGAMENTO        CHAR     := NULL,
        pCODIGO_FAVORECIDO       NUMBER   := NULL,
        pCODIGO_EMPRESA_GRUPO    NUMBER   := NULL,
        pDATA_ENVIO              DATE     := NULL,
        pDATA_GERACAO            DATE     := NULL,
        pDATA_EXCLUSAO           DATE     := NULL,
        pCODIGO_USUARIO_ENVIO    VARCHAR2 := NULL,
        pCODIGO_USUARIO_GERACAO  VARCHAR2 := NULL,
        pCODIGO_USUARIO_EXCLUSAO VARCHAR2 := NULL,
        retornarRegistro         CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PAGAMENTO_R(
        pCODIGO_PAGAMENTO NUMBER := NULL );
    PROCEDURE PR_PAGAMENTO_R2(
        pCODIGO_PAGAMENTO NUMBER);
    PROCEDURE PR_PAGAMENTO_A(
        pFILTRO_CODIGO_PAGAMENTO NUMBER := NULL,
        pFILTRO_CODIGO_PROCESSO  NUMBER := NULL );
    PROCEDURE PR_PAGAMENTO_A2(
        pDATA_PAGAMENTO_MAIOR DATE    := NULL,
        pCODIGO_PROCESSO      NUMBER  := NULL,
        pREGRA_ATACADAO       VARCHAR := NULL );
    /******************************************************************
    *
    * TB_ESTABELECIMENTO
    *
    ******************************************************************/
    PROCEDURE PR_ESTABELECIMENTO_B(
        pCODIGO_ESTABELECIMENTO NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ESTABELECIMENTO_L(
        pFILTRO_CODIGO_FAVORECIDO NUMBER   := NULL,
        pFILTRO_CNPJ              VARCHAR2 := NULL,
        pFILTRO_RAZAO_SOCIAL      VARCHAR2 := NULL,
        pFILTRO_CODIGO_GRUPO      NUMBER   := NULL,
        pFILTRO_CODIGO_SUBGRUPO   NUMBER   := NULL,
        pFILTRO_CODIGO_CSU        VARCHAR2 := NULL,
        pFILTRO_CODIGO_SITEF      VARCHAR2 := NULL,
        pFILTRO_CNPJ_FAVORECIDO   VARCHAR2 := NULL,
        maxLinhas                 INT      := 0,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ESTABELECIMENTO_S(
        pCODIGO_ESTABELECIMENTO  NUMBER   := NULL,
        pCNPJ                    VARCHAR2 := NULL,
        pRAZAO_SOCIAL            VARCHAR2 := NULL,
        pBANCO                   VARCHAR2 := NULL,
        pAGENCIA                 VARCHAR2 := NULL,
        pCONTA                   VARCHAR2 := NULL,
        pCODIGO_FAVORECIDO       NUMBER   := NULL,
        pESTABELECIMENTO_CSU     VARCHAR2 := NULL,
        pESTABELECIMENTO_SITEF   VARCHAR2 := NULL,
        pCODIGO_EMPRESA_SUBGRUPO VARCHAR2 := NULL,
        retornarRegistro         CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ESTABELECIMENTO_R(
        pCODIGO_ESTABELECIMENTO NUMBER := NULL );
    /******************************************************************
    *
    * TB_PROCESSO
    *
    ******************************************************************/
    PROCEDURE PR_PROCESSO_B(
        pCODIGO_PROCESSO NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PROCESSO_L(
        pFILTRO_TIPO_PROCESSO       VARCHAR2 := NULL,
        pFILTRO_DATA_INCLUSAO       DATE     := NULL,
        pFILTRO_DATA_INCLUSAO_MAIOR DATE     := NULL,
        pFILTRO_DATA_INCLUSAO_MENOR DATE     := NULL,
        pFILTRO_DATA_STATUS         DATE     := NULL,
        pFILTRO_DATA_STATUS_MAIOR   DATE     := NULL,
        pFILTRO_DATA_STATUS_MENOR   DATE     := NULL,
        pFILTRO_STATUS_PROCESSO     VARCHAR2 := NULL,
        pFILTRO_STATUS_PROCESSO_DIF VARCHAR2 := NULL,
        pFILTRO_STATUS_BLOQUEIO     CHAR     := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_PROCESSO_S(
        pCODIGO_PROCESSO       NUMBER   := NULL,
        pTIPO_PROCESSO         VARCHAR2 := NULL,
        pTIPO_ESTAGIO_ATUAL    VARCHAR2 := NULL,
        pSERIALIZACAO_PROCESSO VARCHAR2 := NULL,
        pSTATUS_PROCESSO       CHAR     := NULL,
        pDATA_STATUS           VARCHAR2 := NULL,
        pSTATUS_ESTAGIO        CHAR     := NULL,
        pDATA_ESTAGIO          DATE     := NULL,
        pSTATUS_BLOQUEIO       CHAR     := NULL,
        pDATA_ATUALIZACAO      DATE     := NULL,
        retornarRegistro       CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PROCESSO_R(
        pCODIGO_PROCESSO NUMBER := NULL );
    /******************************************************************
    *
    * TB_LOG
    *
    ******************************************************************/
    PROCEDURE PR_LOG_B(
        pCODIGO_LOG NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LOG_L(
        pFILTRO_TIPO_LOG       VARCHAR2 := NULL,
        pFILTRO_DATA_LOG       DATE     := NULL,
        pFILTRO_DATA_LOG_MAIOR DATE     := NULL,
        pFILTRO_DATA_LOG_MENOR DATE     := NULL,
        pFILTRO_CODIGO_USUARIO VARCHAR2 := NULL,
        pFILTRO_TIPO_ORIGEM    VARCHAR2 := NULL,
        pFILTRO_CODIGO_ORIGEM  NUMBER   := NULL,
        retcur OUT TP_REFCURSOR,
        maxLinhas INT := 0 );
    PROCEDURE PR_LOG_S(
        pCODIGO_LOG     NUMBER   := NULL,
        pDATA_LOG       DATE     := NULL,
        pTIPO_LOG       VARCHAR2 := NULL,
        pCODIGO_USUARIO VARCHAR2 := NULL,
        pDESCRICAO_LOG  VARCHAR2 := NULL,
        pSERIALIZACAO_LOG CLOB   := NULL,
        pTIPO_ORIGEM     VARCHAR2    := NULL,
        pCODIGO_ORIGEM   NUMBER      := NULL,
        retornarRegistro CHAR        := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LOG_R(
        pCODIGO_LOG NUMBER := NULL );
    PROCEDURE PR_PRODUTO_PLANO_S(
        pCODIGO_PRODUTO NUMBER := NULL,
        pCODIGO_PLANO   NUMBER := NULL );
    PROCEDURE PR_PRODUTO_PLANO_R(
        pCODIGO_PRODUTO NUMBER := NULL,
        pCODIGO_PLANO   NUMBER := NULL );
    PROCEDURE PR_PRODUTO_S(
        pCODIGO_PRODUTO      NUMBER   := NULL,
        pCODIGO_PRODUTO_TSYS VARCHAR2 := NULL,
        pNOME_PRODUTO        VARCHAR2 := NULL,
        pDESCRICAO_PRODUTO   VARCHAR2 := NULL,
        pCEDIVEL             CHAR     := NULL,
        pPRAZO_PGMTO         NUMBER   := NULL,
        pDIAS_VENCIDOS       NUMBER   := NULL,
        pDIAS_VENCIDOS_AJ    NUMBER   := NULL,
        retornarRegistro     CHAR     := 'N',
        retcur OUT TP_REFCURSOR,
        retcurPlanos OUT TP_REFCURSOR );
    PROCEDURE PR_PRODUTO_B(
        pCODIGO_PRODUTO NUMBER,
        retcur OUT TP_REFCURSOR,
        retcurPlanos OUT TP_REFCURSOR );
    PROCEDURE PR_PRODUTO_L(
        pNOME_PRODUTO VARCHAR2 := NULL,
        maxLinhas     INT      := 0,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PRODUTO_R(
        pCODIGO_PRODUTO       NUMBER,
        pREMOVER_ASSOC_PLANOS CHAR );
    PROCEDURE PR_PLANO_S(
        pCODIGO_PLANO        NUMBER   := NULL,
        pCODIGO_PLANO_TSYS   VARCHAR2 := NULL,
        pNOME_PLANO          VARCHAR2 := NULL,
        pDESCRICAO_PLANO     VARCHAR2 := NULL,
        pQUANTIDADE_PARCELAS NUMBER   := NULL,
        pCOMISSAO            NUMBER   := NULL,
        pATIVO               CHAR     := NULL,
        retornarRegistro     CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PLANO_B(
        pCODIGO_PLANO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PLANO_L(
        pATIVO               CHAR     := NULL,
        pNOME_PLANO          VARCHAR2 := NULL,
        pDESCRICAO_PLANO     VARCHAR2 := NULL,
        pQUANTIDADE_PARCELAS NUMBER   := NULL,
        pCOMISSAO            NUMBER   := NULL,
        pCODIGO_PLANO_TSYS   VARCHAR2 := NULL,
        pCODIGO_PRODUTO      NUMBER   := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PLANO_R(
        pCODIGO_PLANO         NUMBER,
        pREMOVER_ASSOC_PLANOS CHAR );
    PROCEDURE PR_DIA_NAO_UTIL_S(
        pCODIGO_DIA_NAO_UTIL        NUMBER   := NULL,
        pDATA_DIA_NAO_UTIL          DATE     := NULL,
        pCLASSIFICACAO_DIA_NAO_UTIL VARCHAR2 := NULL,
        pDESCRICAO_DIA_NAO_UTIL     VARCHAR2 := NULL,
        pATIVO                      CHAR     := NULL,
        retornarRegistro            CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_DIA_NAO_UTIL_B(
        pCODIGO_DIA_NAO_UTIL NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_DIA_NAO_UTIL_L(
        pFILTRO_ATIVO      CHAR := NULL,
        pFILTRO_DATA_MAIOR DATE := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_DIA_NAO_UTIL_R(
        pCODIGO_DIA_NAO_UTIL NUMBER);
    PROCEDURE PR_EMPRESA_GRUPO_B(
        pCODIGO_EMPRESA_GRUPO NUMBER,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_EMPRESA_GRUPO_S(
        pCODIGO_EMPRESA_GRUPO          NUMBER   := NULL,
        pNOME_EMPRESA_GRUPO            VARCHAR2 := NULL,
        pDESCRICAO_EMPRESA_GRUPO       VARCHAR2 := NULL,
        pENVIAR_NEGATIVO_EMPRESA_GRUPO CHAR     := 'N',
        retornarRegistro               CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_EMPRESA_GRUPO_L(
        pNOME_EMPRESA_GRUPO VARCHAR2 := NULL,
        maxLinhas           INT      :=0,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_EMPRESA_GRUPO_R(
        pCODIGO_EMPRESA_GRUPO NUMBER);
    PROCEDURE PR_NUMERO_SEQUENCIAL_L(
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_RELATORIO_VENCIMENTO_L2(
        pCODIGO_EMPRESA_GRUPO     NUMBER   := NULL,
        pCODIGO_EMPRESA_SUB_GRUPO NUMBER   := NULL,
        pCODIGO_FAVORECIDO        NUMBER   := NULL,
        pCODIGO_PRODUTO           VARCHAR2 := NULL,
        pTIPO_REGISTRO            VARCHAR2 := NULL,
        pCONSOLIDA_AG_VC          CHAR     := 'N',
        pCONSOLIDA_MAIOR          CHAR     := 'N',
        pSTATUS_RETORNO_CESSAO    CHAR     := NULL,
        pDATA_INICIO_TRANSACAO    DATE     := NULL,
        pDATA_FIM_TRANSACAO       DATE     := NULL,
        pDATA_INICIO_VENCIMENTO   DATE     := NULL,
        pDATA_FIM_VENCIMENTO      DATE     := NULL,
        pDATA_INICIO_MOVIMENTO    DATE     := NULL,
        pDATA_FIM_MOVIMENTO       DATE     := NULL,
        pDATA_INICIO_AGENDAMENTO  DATE     := NULL,
        pDATA_FIM_AGENDAMENTO     DATE     := NULL,
        pDATA_POSICAO_CARTEIRA    DATE     := NULL,
        pTIPO_FINANCEIRO_CONTABIL CHAR     := NULL,
        pDATA_INICIO_CCI          DATE     := NULL,
        pDATA_FIM_CCI             DATE     := NULL,
        pREFERENCIA               VARCHAR2 := NULL,
        pCONSOLIDA_REFERENCIA     CHAR     := 'N',
        pCODIGO_PROCESSO          NUMBER   := NULL,
        pGERAR_PAGAMENTOS         CHAR     := 'N',
        pRETORNAR_PAGOS           CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_GERAR_PAGAMENTOS(
        pSELECTREL       VARCHAR := NULL,
        pQUERY           VARCHAR := NULL,
        pJOIN            VARCHAR := NULL,
        pCONDICAO        VARCHAR := NULL,
        pGROUPBY         VARCHAR := NULL,
        pORDERBY         VARCHAR := NULL,
        pCODIGO_PROCESSO NUMBER  := NULL );
    PROCEDURE PR_EMPRESA_SUBGRUPO_S(
        pCODIGO_EMPRESA_SUBGRUPO    NUMBER   := NULL,
        pNOME_EMPRESA_SUBGRUPO      VARCHAR2 := NULL,
        pDESCRICAO_EMPRESA_SUBGRUPO VARCHAR2 := NULL,
        pCODIGO_EMPRESA_GRUPO       NUMBER   := NULL,
        retornarRegistro            CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_EMPRESA_SUBGRUPO_B(
        pCODIGO_EMPRESA_SUBGRUPO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_EMPRESA_SUBGRUPO_L(
        pNOME_EMPRESA_SUBGRUPO   VARCHAR2 := NULL,
        pCODIGO_EMPRESA_SUBGRUPO NUMBER   := NULL,
        pCODIGO_EMPRESA_GRUPO    NUMBER   := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_EMPRESA_SUBGRUPO_R(
        pCODIGO_EMPRESA_SUBGRUPO NUMBER);
    PROCEDURE PR_FAVORECIDO_S(
        pCODIGO_FAVORECIDO    NUMBER   := NULL,
        pNOME_FAVORECIDO      VARCHAR2 := NULL,
        pCNPJ_FAVORECIDO      VARCHAR2 := NULL,
        pFAVORECIDO_ORIGINAL  CHAR     := NULL,
        pFAVORECIDO_BLOQUEADO SMALLINT := 0,
        retornarRegistro      CHAR     := 'N',
        retcurContas OUT TP_REFCURSOR,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_FAVORECIDO_B(
        pCODIGO_FAVORECIDO NUMBER,
        retornarContas     CHAR := 'N',
        retcur OUT TP_REFCURSOR,
        retcurContas OUT TP_REFCURSOR);
    PROCEDURE PR_FAVORECIDO_L(
        pCODIGO_FAVORECIDO NUMBER   := NULL,
        pCNPJ_FAVORECIDO   VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_FAVORECIDO_R(
        pCODIGO_FAVORECIDO NUMBER);
    PROCEDURE PR_CONFIGURACAO_SISTEMA_B(
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_CONFIGURACAO_SISTEMA_S(
        pCODIGO_CONFIGURACAO_SISTEMA   NUMBER   := NULL,
        pPRAZO_PAGAMENTO               NUMBER   := NULL,
        pDIAS_VENCIDO                  NUMBER   := NULL,
        pDIR_IMP_EXTRATO               VARCHAR2 := NULL,
        pDIR_IMP_EXTRATO_ATACADAO      VARCHAR2 := NULL,
        pDIR_IMP_EXTRATO_GALERIA       VARCHAR2 := NULL,
        pDIR_EXP_CESSAO                VARCHAR2 := NULL,
        pDIR_IMP_RET_CESSAO            VARCHAR2 := NULL,
        pDIR_EXP_MATERA_CONTABIL       VARCHAR2 := NULL,
        pDIR_EXP_MATERA_FINANCEIRO     VARCHAR2 := NULL,
        pDIR_EXP_MATERA_EXTRATO        VARCHAR2 :=NULL,
        pFL_ENVIO_MATERA_AUT           CHAR     := NULL,
        pFL_ENVIO_CESSAO_AUT           CHAR     := NULL,
        pHORA_DE_LEITURA_EXTRATO       VARCHAR2 := NULL,
        pHORA_ATE_LEITURA_RET_CESSAO   VARCHAR2 := NULL,
        pDIR_IMP_ATACADAO_TSYS         VARCHAR2 := NULL,
        pDIR_IMP_ATACADAO_SITEF        VARCHAR2 := NULL,
        pDIR_IMP_ATACADAO_MA           VARCHAR2 := NULL,
        pDIR_EXP_ATACADAO_CSUSEM       VARCHAR2 := NULL,
        pDIR_EXP_ATACADAO_CONC         VARCHAR2 := NULL,
        pHORA_DE_LEITURA_ATACADAO      VARCHAR2 := NULL,
        pHORA_ATE_LEITURA_ATACADAO     VARCHAR2 := NULL,
        pDIR_EXP_REL_TRANSACAO         VARCHAR2 := NULL,
        pDIR_EXP_REL_VENDA             VARCHAR2 := NULL,
        pDIR_EXP_REL_VENCIMENTO        VARCHAR2 := NULL,
        pDIR_EXP_REL_CONSOLIDADO       VARCHAR2 := NULL,
        pDIR_EXP_REL_SALDO_CONSILIACAO VARCHAR2 := NULL,
        pDIR_EXP_REL_RECEBIMENTO       VARCHAR2 := NULL,
        pDIR_EXP_REL_CHEQUES           VARCHAR2 := NULL,
        pFL_HOMOLOGACAO_TSYS           CHAR     := NULL,
        pFL_REPROCESSAR_NSU_PROCESSADO CHAR     := NULL,
        pFL_REPROCESSAR_NSU_CESSIONADO CHAR     := NULL,
        pSTATUS_AGENDADOR              CHAR     := NULL,
        pSTATUS_EXPURGO                CHAR     := NULL,
        pDIAS_AGUARDAR_EXPURGO         NUMBER   := NULL,
        pDIAS_IGNORAR_EXPURGO          NUMBER   := NULL,
        pDAT_ULTM_EXPURGO              DATE     := NULL,
        pDIR_EXP_CESSAO_ATACADAO       VARCHAR2 := NULL,
        pDIR_EXP_CESSAO_GALERIA        VARCHAR2 := NULL,
        pFL_ENVIO_CESSAO_AUT_ATACADAO  CHAR     := NULL,
        pFL_ENVIO_CESSAO_AUT_GALERIA   CHAR     := NULL,
        pLAYOUT_CCI                    VARCHAR2 := NULL,
        pLAYOUT_ATACADAO               VARCHAR2 := NULL,
        pLAYOUT_GALERIA                VARCHAR2 := NULL,
        retornarRegistro               CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONFIGURACAO_SISTEMA_R(
        pCODIGO_CONFIGURACAO_SISTEMA NUMBER) ;
    PROCEDURE PR_CONFIG_TIPO_REGISTRO_S(
        pCODIGO_CONFIG_TIPO_REGISTRO NUMBER   := NULL,
        pTIPO_ARQUIVO                VARCHAR2 := NULL,
        pTIPO_REGISTRO               VARCHAR2 := NULL,
        pFL_ENVIAR_CCI               CHAR     := NULL,
        pFL_GERAR_AGENDA             CHAR     := NULL,
        pDIAS_REPASSE                INT      := NULL,
        pATIVO                       CHAR     := NULL,
        retornarRegistro             CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONFIG_TIPO_REGISTRO_B(
        pCODIGO_CONFIG_TIPO_REGISTRO NUMBER,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_CONFIG_TIPO_REGISTRO_L(
        pTIPO_ARQUIVO  VARCHAR2 := NULL,
        pFL_ENVIAR_CCI VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONFIG_TIPO_REGISTRO_R(
        pCODIGO_CONFIG_TIPO_REGISTRO NUMBER);
    PROCEDURE PR_RELATORIO_TRANSACAO_L(
        pFILTRO_ORIGEM                 VARCHAR2 := NULL,
        pFILTRO_EMPRESA_GRUPO          NUMBER   := NULL,
        pFILTRO_EMPRESA_SUBGRUPO       NUMBER   := NULL,
        pFILTRO_ESTABELECIMENTO        NUMBER   := NULL,
        pFILTRO_FAVORECIDO             NUMBER   := NULL,
        pFILTRO_PRODUTO                NUMBER   := NULL,
        pFILTRO_PLANO                  NUMBER   := NULL,
        pFILTRO_MEIO_CAPTURA           NUMBER   := NULL,
        pFILTRO_TIPO_REGISTRO          VARCHAR2 := NULL,
        pFILTRO_TIPO_TRANSACAO         VARCHAR2 := NULL,
        pFILTRO_NUMERO_PAGAMENTO       NUMBER   := NULL,
        pFILTRO_CONTA_CLIENTE          VARCHAR2 := NULL,
        pFILTRO_DATA_PROCESSAMENTO_DE  DATE     := NULL,
        pFILTRO_DATA_PROCESSAMENTO_ATE DATE     := NULL,
        pFILTRO_DATA_TRANSACAO_DE      DATE     := NULL,
        pFILTRO_DATA_TRANSACAO_ATE     DATE     := NULL,
        pFILTRO_DATA_VENCIMENTO_DE     DATE     := NULL,
        pFILTRO_DATA_VENCIMENTO_ATE    DATE     := NULL,
        pFILTRO_DATA_AGENDAMENTO_DE    DATE     := NULL,
        pFILTRO_DATA_AGENDAMENTO_ATE   DATE     := NULL,
        pFILTRO_DATA_ENVIO_MATERA_DE   DATE     := NULL,
        pFILTRO_DATA_ENVIO_MATERA_ATE  DATE     := NULL,
        pFILTRO_DATA_LIQUIDACAO_DE     DATE     := NULL,
        pFILTRO_DATA_LIQUIDACAO_ATE    DATE     := NULL,
        pFILTRO_STATUS_RETORNO         NUMBER   := NULL,
        pFILTRO_STATUS_PAGAMENTO       NUMBER   := NULL,
        pFILTRO_STATUS_CONCILIACAO     NUMBER   := NULL,
        pFILTRO_STATUS_VALIDACAO       NUMBER   := NULL,
        pFILTRO_DATA_MOVIMENTO_DE      DATE     := NULL,
        pFILTRO_DATA_MOVIMENTO_ATE     DATE     := NULL,
        pFILTRO_STATUS_CANC_ON_LINE    CHAR     := NULL,
        pFILTRO_DATA_RET_CESSAO_DE     DATE     := NULL,
        pFILTRO_DATA_RET_CESSAO_ATE    DATE     := NULL,
        pFILTRO_TIPO_FINANC_CTBIL      CHAR     := NULL,
        pFILTRO_NSU_HOST               VARCHAR2 := NULL,
        pFILTRO_REFERENCIA             VARCHAR2 := NULL,
        maxLinhas                      INT      := 0,
        retcur OUT TP_REFCURSOR,
        retcur_query OUT TP_REFCURSOR);
    PROCEDURE PR_PAGAMENTO_LIQUIDAR_MANUAL(
        pCODIGO_PAGAMENTO NUMBER,
        pDATA_LIQUIDACAO  DATE );
    PROCEDURE PR_EXTRATO_PREPARAR_AGENDA(
        pCODIGO_PROCESSO NUMBER,
        pREGRA_ATACADAO  VARCHAR := 'N' );
    PROCEDURE PR_EXTRATO_AGENDAR(
        pCODIGO_PROCESSO NUMBER,
        pREGRA_ATACADAO  VARCHAR := 'N' );
    PROCEDURE PR_RELATORIO_VENDAS_L1(
        pCODIGO_EMPRESA_GRUPO         NUMBER   := NULL,
        pCODIGO_EMPRESA_SUBGRUPO      NUMBER   := NULL,
        pCODIGO_BANCO_FAVORECIDO      NUMBER   := NULL,
        pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
        pTIPO_REGISTRO                VARCHAR2 := NULL,
        pCONSOLIDA_GRUPO              CHAR     := 'N',
        pCONSOLIDA_DATA_VENDA         CHAR     := 'N',
        pCONSOLIDA_DATA_PROCESSAMENTO CHAR     := 'N',
        pCONSOLIDA_BANCO              CHAR     := 'N',
        pCONSOLIDA_ESTABELECIMENTO    CHAR     := 'N',
        pDATA_INICIO_VENDA            DATE     := NULL,
        pDATA_FIM_VENDA               DATE     := NULL,
        pDATA_INICIO_MOVIMENTO        DATE     := NULL,
        pDATA_FIM_MOVIMENTO           DATE     := NULL,
        pDATA_INICIO_CCI              DATE     := NULL,
        pDATA_FIM_CCI                 DATE     := NULL,
        pTIPO_FINANCEIRO_CONTABIL     CHAR     := NULL,
        pREFERENCIA                   VARCHAR2 := NULL,
        pCONSOLIDA_REFERENCIA         CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONTA_FAVORECIDO_S(
        pCODIGO_CONTA_FAVORECIDO NUMBER   := NULL,
        pCODIGO_FAVORECIDO       NUMBER   := NULL,
        pAGENCIA                 VARCHAR2 := NULL,
        pBANCO                   VARCHAR2 := NULL,
        pCONTA                   VARCHAR2 := NULL,
        pAGENCIA_CCI             VARCHAR2 := NULL,
        pCONTA_CCI               VARCHAR2 := NULL,
        retornarRegistro         CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONTA_FAVORECIDO_U(
        pCODIGO_CONTA_FAVORECIDO      NUMBER := NULL,
        pCODIGO_CONTA_FAVORECIDO_NOVO NUMBER := NULL,
        pCODIGO_PROCESSO              NUMBER := NULL );
    PROCEDURE PR_CONTA_FAVORECIDO_B(
        pCODIGO_CONTA_FAVORECIDO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONTA_FAVORECIDO_L(
        pCODIGO_FAVORECIDO NUMBER   := NULL,
        pBANCO             VARCHAR2 := NULL,
        pAGENCIA           VARCHAR2 := NULL,
        pCONTA             VARCHAR2 := NULL,
        pAGENCIA_CCI       VARCHAR2 := NULL,
        pCONTA_CCI         VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONTA_FAVORECIDO_R(
        pCODIGO_CONTA_FAVORECIDO NUMBER);
    PROCEDURE PR_CONTA_PRODUTO_S(
        pCODIGO_CONTA_FAVORECIDO NUMBER   := NULL ,
        pCODIGO_PRODUTO          NUMBER   := NULL ,
        pREFERENCIA              VARCHAR2 := NULL ,
        retornarRegistro         CHAR     := 'N' ,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONTA_PRODUTO_L(
        pCODIGO_PRODUTO          NUMBER  := NULL,
        pCODIGO_CONTA_FAVORECIDO NUMBER  := NULL,
        pCODIGO_FAVORECIDO       NUMBER  := NULL,
        pREFERENCIA              VARCHAR2:= NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CONTA_PRODUTO_R(
        pCODIGO_CONTA_FAVORECIDO NUMBER   := NULL ,
        pCODIGO_PRODUTO          NUMBER   := NULL ,
        pREFERENCIA              VARCHAR2 := NULL );
    PROCEDURE PR_CONTA_PRODUTO_B(
        pCODIGO_CONTA_FAVORECIDO NUMBER ,
        pCODIGO_PRODUTO          NUMBER ,
        pREFERENCIA              VARCHAR2 ,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ARQUIVO_EXPORTACAO_GERAR(
        pSIGLA_REDE     VARCHAR2 := NULL,
        pDATA_MOVIMENTO DATE     := NULL,
        retcur OUT TP_REFCURSOR );
    /******************************************************************
    *
    * TB_PERMISSAO
    *
    ******************************************************************/
    PROCEDURE PR_PERMISSAO_S(
        pCODIGO_PERMISSAO    NUMBER   := NULL,
        pNOME_PERMISSAO      VARCHAR2 := NULL,
        pDESCRICAO_PERMISSAO VARCHAR2 := NULL,
        retornarRegistro     CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PERMISSAO_B(
        pCODIGO_PERMISSAO NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PERMISSAO_L(
        pCODIGO_PERMISSAO NUMBER := NULL ,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PERMISSAO_R(
        pCODIGO_PERMISSAO NUMBER );
    PROCEDURE PR_PRODUTO_PLANO_L(
        pCODIGO_PLANO NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_REL_TRANS_INCONSISTENTE_L(
        pCODIGO_ARQUIVO NUMBER,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_REL_TRANS_INCONSISTENTE_B(
        pCNPJ VARCHAR2 :=NULL,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_ESTORNO_EXTRATO_S(
        pCodigoProcesso NUMBER );
    PROCEDURE PR_ESTORNO_RETORNO_CESSAO_S(
        pCodigoProcessoCessao NUMBER );
    PROCEDURE PR_ITEM_DESBLOQUEIO_S(
        pCODIGO_ITEM_DESBLOQUEIO NUMBER := NULL,
        pCODIGO_ARQUIVO_ORIGEM   NUMBER := NULL,
        pCODIGO_ARQUIVO_DESTINO  NUMBER := NULL,
        pCODIGO_ARQUIVO_ITEM     NUMBER := NULL,
        retornarRegistro         CHAR   := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_ITEM_DESBLOQUEIO_B(
        pCODIGO_ITEM_DESBLOQUEIO NUMBER,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_ITEM_DESBLOQUEIO_L(
        pCODIGO_ITEM_DESBLOQUEIO NUMBER := NULL,
        pCODIGO_ARQUIVO_ORIGEM   NUMBER := NULL,
        pCODIGO_ARQUIVO_DESTINO  NUMBER := NULL,
        pCODIGO_ARQUIVO_ITEM     NUMBER := NULL,
        pSTATUS_DESBLOQUEIO      NUMBER := NULL,
        retcur OUT TP_REFCURSOR);
    /*
    PROCEDURE PR_ARQUIVO_ITEM_DESBLOQUEIO_S (
    pCODIGO_ARQUIVO number := null,
    retcur out TP_REFCURSOR
    );
    PROCEDURE PR_ITEM_ACERTAR_BLOQUEIO_S (
    pCODIGO_ARQUIVO_ITEM number := null,
    retcur out TP_REFCURSOR
    );
    */
    PROCEDURE PR_PROCESSAR_DESBLOQUEADOS(
        pCODIGO_ARQUIVO_DESTINO NUMBER := NULL,
        pCODIGO_PROCESSO        NUMBER := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_SESSAO_S(
        pCODIGO_SESSAO VARCHAR2,
        pCONTEUDO_SESSAO BLOB,
        retornarRegistro CHAR := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_SESSAO_B(
        pCODIGO_SESSAO VARCHAR2,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_SESSAO_R(
        pCODIGO_SESSAO VARCHAR2);
    PROCEDURE PR_SESSAO_L(
        pDATA_INCLUSAO VARCHAR2,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_PAGAMENTO_ATUALIZAR_STATUS(
        pCODIGO_PAGAMENTO        NUMBER,
        pSTATUS_PAGAMENTO        CHAR    := NULL,
        pCODIGO_USUARIO_ENVIO    VARCHAR := NULL,
        pCODIGO_USUARIO_EXCLUSAO VARCHAR := NULL );
    PROCEDURE PR_PROCESSO_ENVIADO_MATERA(
        pcodigo_processo NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_ITEM_REFERENCIA_L(
        pCODIGO_LISTA_ITEM_REFERENCIA NUMBER   := NULL,
        pCODIGO_LISTA_ITEM_SITUACAO   VARCHAR2 := NULL,
        pCODIGO_LISTA_ITEM            NUMBER   := NULL,
        pNOME_REFERENCIA              VARCHAR2 := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_ITEM_REFERENCIA_B(
        pCODIGO_LISTA_ITEM_REFERENCIA NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_ITEM_REFERENCIA_S(
        pCODIGO_LISTA_ITEM_REFERENCIA NUMBER := NULL,
        pCODIGO_LISTA_ITEM            NUMBER,
        pCODIGO_LISTA_ITEM_SITUACAO   VARCHAR2,
        pNOME_REFERENCIA              VARCHAR2,
        pVALOR_REFERENCIA             VARCHAR2,
        retornarRegistro              CHAR := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_ITEM_REFERENCIA_R(
        pCODIGO_LISTA_ITEM_REFERENCIA NUMBER );
    PROCEDURE PR_LISTA_ITEM_BIN_REFERENCIA_L(
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_REL_TRANS_CONSOLIDADO_L1(
        pTIPO_CONSOLIDADO         CHAR     := NULL,
        pSUB_TOTAL_FAVORECIDO     CHAR     := NULL,
        pCODIGO_PRODUTO           VARCHAR2 := NULL,
        pFAVORECIDO_ORIGINAL      VARCHAR2 := NULL,
        pCODIGO_TIPO_REGISTRO     VARCHAR2 := NULL,
        pCODIGO_ESTABELECIMENTO   NUMBER   := NULL,
        pCODIGO_MEIO_PAGAMENTO    VARCHAR2 := NULL,
        pLAYOUT_VENDA             CHAR     := NULL,
        pLAYOUT_RECEBIMENTO       CHAR     := NULL,
        pQTDE_TRANS_VENDA         CHAR     := NULL,
        pQTDE_PAG_RECEBIMENTO     CHAR     := NULL,
        pDATA_INICIO_AGENDAMENTO  DATE     := NULL,
        pDATA_FIM_AGENDAMENTO     DATE     := NULL,
        pDATA_INICIO_MOVIMENTO    DATE     := NULL,
        pDATA_FIM_MOVIMENTO       DATE     := NULL,
        pSTATUS_RETORNO_CESSAO    CHAR     := NULL,
        pTIPO_FINANCEIRO_CONTABIL CHAR     := NULL,
        pREFERENCIA               VARCHAR2 := NULL,
        pCONSOLIDA_REFERENCIA     CHAR     := 'N',
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_REL_SALDO_CONCILIACAO_L(
        pFILTRO_DATA_CON_INICIO DATE,
        pFILTRO_DATA_CON_FIM    DATE,
        retcur OUT TP_REFCURSOR,
        RETCURSALDO OUT TP_REFCURSOR);
    PROCEDURE PR_SALDO_CONSOLIDADO_S(
        PDATACALCULOSALDO DATE);
    PROCEDURE PR_SALDO_CONSOLIDADO_B(
        PDATASALDO DATE,
        RETCUR OUT TP_REFCURSOR);
    PROCEDURE PR_TRANSACAO_CHAVE_L(
        PCHAVE_LIKE VARCHAR2 := NULL,
        RETCUR OUT TP_REFCURSOR,
        RETCURCOUNT OUT TP_REFCURSOR);
    PROCEDURE PR_VALIDAR_CESSAO_B(
        PCODIGO_ARQUIVO             NUMBER,
        PCODIGO_PROCESSO            NUMBER,
        PBANCO                      VARCHAR2,
        PAGENCIA                    VARCHAR2,
        PCONTA                      VARCHAR2,
        PCNPJ_FAVORECIDO            VARCHAR2,
        PNSU_HOST                   VARCHAR2,
        PDATA_TRANSACAO_FORMATADA   DATE,
        PCNPJ_ESTABELECIMENTO       VARCHAR2,
        PCOD_AUTORIZACAO            VARCHAR2,
        PNUMERO_PARCELA             VARCHAR2,
        PREPROCESSAR_NSU_CESSIONADO CHAR,
        PRET_CESSAO_ANT OUT NUMBER,
        RETCUR OUT TP_REFCURSOR );
    PROCEDURE PR_CESSIONA_S(
        pCODIGO_TRANSACAO           NUMBER := NULL ,
        pCODIGO_ARQUIVO_RET_CESSAO  NUMBER := NULL ,
        pCODIGO_PROCESSO_RET_CESSAO NUMBER := NULL ,
        pCODIGO_FAVORECIDO          NUMBER := NULL ,
        pCODIGO_CONTA_FAVORECIDO    NUMBER := NULL );
    PROCEDURE PR_PERMISSAO_GRUPO_PERFIL_L(
        pCODIGO_USUARIO VARCHAR2 := NULL,
        RETCUR OUT TP_REFCURSOR,
        RETCURCOUNT OUT TP_REFCURSOR);
    PROCEDURE PR_CANCELAMENTO_ONLINE(
        pCODIGO_PROCESSO NUMBER := NULL,
        pCODIGO_ARQUIVO  NUMBER := NULL );
    PROCEDURE PR_VERIFICA_FAVORECIDO_BANCO(
        pCNPJ_FAVORECIDO string := NULL ,
        pBANCO string           := NULL ,
        pAGENCIA string         := NULL ,
        pCONTA string           := NULL ,
        pPRODUTO string         := NULL ,
        pREFERENCIA string      := NULL ,
        RETCUR OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_CONF_L(
        pCODIGO_LISTA string         := NULL,
        pCODIGO_EMPRESA_GRUPO NUMBER := NULL,
        RETCUR OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_ITEM_CONF_L(
        pCODIGO_LISTA string         := NULL,
        pCODIGO_EMPRESA_GRUPO NUMBER := NULL,
        RETCUR OUT TP_REFCURSOR );
    PROCEDURE PR_LISTA_ITEM_CONF_S(
        pCODIGO_LISTA_CONF string    := NULL,
        pCONF_LISTA string           := NULL,
        pCODIGO_EMPRESA_GRUPO NUMBER := NULL );
    PROCEDURE PR_EXPURGO_ARQUIVO_ITEM(
        RETCUR OUT TP_REFCURSOR );
    PROCEDURE PR_CAMPOS_ENVIO_CCI_S(
        pCODIGO_CAMPO_ENVIO_CCI NUMBER  := NULL,
        pNOME_CAMPO             VARCHAR := NULL,
        pSTATUS_ENVIO           CHAR    := NULL,
        retornarRegistro        CHAR    := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CAMPOS_ENVIO_CCI_L(
        pCODIGO_CAMPO_ENVIO_CCI NUMBER  := NULL,
        pNOME_CAMPO             VARCHAR := NULL,
        pSTATUS_ENVIO           CHAR    := NULL,
        RETCUR OUT TP_REFCURSOR );
    PROCEDURE PR_LOG_DB(
        PDESCRICAO_LOG VARCHAR2,
        PTIPO_ORIGEM   VARCHAR2,
        PCODIGO_ORIGEM NUMBER,
        PDESCRICAOGRANDE_LOG CLOB );
    PROCEDURE PR_LIST_CONF_S(
        pNOME_LISTA           VARCHAR2 ,
        pLISTA_CONF_HEADER    VARCHAR2 ,
        pVALOR_CONF           VARCHAR,
        pCODIGO_EMPRESA_GRUPO NUMBER );
    PROCEDURE PR_TRANSACAO_FAVORECIDO_L(
        pDATA_INCLUSAO DATE,
        pUPD           CHAR,
        pRETORNO OUT NUMBER );
    PROCEDURE PR_TRANSACAO_FAVORECIDO_S(
        pQUERY_PRINCIPAL VARCHAR2,
        pDATA_INCLUSAO   DATE,
        pRETORNO OUT NUMBER );
    PROCEDURE PR_TRATAR_DT_REP_CALC(
        pDATA_REPASSE       DATE,
        pCODIGO_PROCESSO    NUMBER,
        pCODIGO_PRODUTO     NUMBER,
        pTIPO_TRANSACAO     VARCHAR,
        pDATA_TRANSACAO     DATE,
        pDATA_PROCESSAMENTO DATE,
        pDATA_REPASSE_CALCULADA OUT DATE,
        pRETORNO OUT NUMBER );
    PROCEDURE PR_TRATAR_FAVORECIDO(
        pCODIGO_ESTABELECIMENTO NUMBER,
        pCODIGO_PRODUTO         NUMBER,
        pREFERENCIA             CHAR,
        pCODIGO_CONTA_FAVORECIDO OUT NUMBER,
        pCODIGO_FAVORECIDO OUT NUMBER,
        pRETORNO OUT NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CHEQUE_DEVOLVIDO_L(
        pFILTRO_DT_ESTORNO_DE    DATE     := NULL,
        pFILTRO_DT_ESTORNO_ATE   DATE     := NULL,
        pNUMERO_PROTOCOLO        VARCHAR2 := NULL,
        pFILTRO_ENVIO_CCI        VARCHAR2 := NULL,
        pCOD_ESTABELECIMENTO     NUMBER   := NULL,
        pFILTRO_DT_PROTOCOLO_DE  DATE     := NULL,
        pFILTRO_DT_PROTOCOLO_ATE DATE     := NULL,
        pFILTRO_STATUS_PROTOCOLO VARCHAR2 := NULL,
        pFILTRO_CTA_CLIENTE      VARCHAR2 := NULL,
        pFILTRO_TP_ESTORNO       VARCHAR2 := NULL,
        pFILTRO_DT_PROC_DE       DATE     := NULL,
        pFILTRO_DT_PROC_ATE      DATE     := NULL,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CHEQUE_DEVOLVIDO_S(
        pCODIGO_TRANSACAO NUMBER   := NULL,
        pDATA_PROTOCOLO   DATE     := NULL,
        pDATA_ESTORNO     DATE     := NULL,
        pNUMERO_PROTOCOLO VARCHAR2 := NULL,
        pTIPO_ESTORNO     VARCHAR2 := NULL );
    PROCEDURE PR_CHEQUE_DEVOLVIDO_B(
        pCODIGO_TRANSACAO NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TEM_CESSAO_C(
        pCodigoProcesso NUMBER,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_CESSIONA_LOG_S(
        pCODIGO_PROCESSO_RET_CESSAO NUMBER := NULL );
    PROCEDURE PR_CARONA_D(
        PCODIGO_ARQUIVO_ITEM NUMBER :=NULL ,
        PCODIGO_ARQUIVO      NUMBER :=NULL ,
        PCODIGO_PROCESSO     NUMBER :=NULL ,
        PQTD OUT NUMBER );
    PROCEDURE PR_ARQUIVO_CARONA_D(
        PCODIGO_ARQUIVO NUMBER :=NULL,
        PQTD OUT NUMBER);
    PROCEDURE PR_ITEM_CARONA_D(
        PCODIGO_ARQUIVO_ITEM NUMBER := NULL,
        PCODIGO_PROCESSO_ATUAL OUT NUMBER,
        PCODIGO_PROCESSO_ORIGEM OUT NUMBER,
        PQTD OUT NUMBER);
    PROCEDURE PR_TEM_MATERA_C(
        pCodigoProcesso NUMBER ,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_PROCESSO_STATUS_BLOQUEIO_U(
        PCODIGO_PROCESSO NUMBER := NULL);
    PROCEDURE PR_TAXA_RECEBIMENTO_B(
        pCODIGO_TAXA_RECEBIMENTO NUMBER,
        retcur OUT TP_REFCURSOR);
    PROCEDURE PR_TAXA_RECEBIMENTO_S(
        pCODIGO_TAXA_RECEBIMENTO NUMBER   := NULL,
        pCODIGO_EMPRESA_GRUPO    NUMBER   := NULL,
        pCODIGO_REFERENCIA       VARCHAR2 := NULL,
        pCODIGO_MEIO_PAGAMENTO   NUMBER   := NULL,
        pDATA_FIM                DATE     := NULL,
        pDATA_INICIO             DATE     := NULL,
        pPERCENTUAL_RECEBIMENTO  NUMBER   := NULL,
        pVALOR_RECEBIMENTO       NUMBER   := NULL,
        retornarRegistro         CHAR     := 'N',
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TAXA_RECEBIMENTO_L(
        pCODIGO_TAXA_RECEBIMENTO NUMBER   := NULL,
        pCODIGO_EMPRESA_GRUPO    NUMBER   := NULL,
        pCODIGO_REFERENCIA       VARCHAR2 := NULL,
        pCODIGO_MEIO_PAGAMENTO   NUMBER   := NULL,
        pDATA_INICIO             DATE     := NULL,
        pDATA_FIM                DATE     := NULL,
        maxLinhas                INT      :=0,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_TAXA_RECEBIMENTO_R(
        pCODIGO_TAXA_RECEBIMENTO NUMBER );
    PROCEDURE PR_RELATORIO_RECEBIMENTO_L(
        pCODIGO_EMPRESA_GRUPO  NUMBER   := NULL,
        pCODIGO_REFERENCIA     VARCHAR2 := NULL,
        pCODIGO_MEIO_PAGAMENTO NUMBER   := NULL,
        pDATA_INICIO           DATE     := NULL,
        pDATA_FIM              DATE     := NULL,
        maxLinhas              INT      :=0,
        retcur OUT TP_REFCURSOR );
    PROCEDURE PR_DIA_UTIL(
        pDATA         IN DATE,
        pDIAS         IN NUMBER := 0,
        pNOME_PRODUTO IN STRING,
        pDATA_RETORNO OUT DATE );
    PROCEDURE PR_DIA_SEMANA(
        pDATA IN DATE,
        PDIA_SEMANA OUT CHAR );
    PROCEDURE PR_TAXA_REL_INTERCHANGE_L(
        pDATA_MOVIMENTO_INICIO DATE := NULL,
        pDATA_MOVIMENTO_FIM    DATE := NULL,
        retcur OUT TP_REFCURSOR );
  END PKG_SCF;
/
create or replace PACKAGE BODY "PKG_SCF"
AS
  PROCEDURE PR_CONFIG_S(
      pTIPO_OBJETO VARCHAR2 := NULL,
      pCONTEUDO_OBJETO CLOB := NULL,
      retornarRegistro CHAR := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    qtde NUMBER;
  BEGIN
    SELECT COUNT(*) INTO qtde FROM TB_CONFIG WHERE TIPO_OBJETO = pTIPO_OBJETO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_CONFIG
        (
          TIPO_OBJETO,
          CONTEUDO_OBJETO
        )
        VALUES
        (
          pTIPO_OBJETO,
          pCONTEUDO_OBJETO
        );
    ELSE
      UPDATE TB_CONFIG
      SET CONTEUDO_OBJETO = NVL(pCONTEUDO_OBJETO, CONTEUDO_OBJETO)
      WHERE TIPO_OBJETO   = pTIPO_OBJETO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_CONFIG_B( pTIPO_OBJETO => pTIPO_OBJETO, retcur => retcur);
    END IF;
  END PR_CONFIG_S;
  PROCEDURE PR_CONFIG_B(
      pTIPO_OBJETO VARCHAR2,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT TIPO_OBJETO ,
    CONTEUDO_OBJETO FROM TB_CONFIG WHERE TIPO_OBJETO = pTIPO_OBJETO;
  END PR_CONFIG_B;
  PROCEDURE PR_CONFIG_L(
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 100 )
  AS
  BEGIN
    OPEN retcur FOR SELECT TIPO_OBJETO ,
    CONTEUDO_OBJETO FROM TB_CONFIG;
  END PR_CONFIG_L;
  PROCEDURE PR_CONFIG_R(
      pTIPO_OBJETO NUMBER)
  AS
  BEGIN
    DELETE TB_CONFIG WHERE TIPO_OBJETO = pTIPO_OBJETO;
  END PR_CONFIG_R;
  PROCEDURE PR_ARQUIVO_S(
      pCODIGO_ARQUIVO       NUMBER   := NULL,
      pTIPO_ARQUIVO         VARCHAR2 := NULL,
      pLAYOUT_ARQUIVO       VARCHAR2 := NULL,
      pNOME_ARQUIVO         VARCHAR2 := NULL,
      pSTATUS_ARQUIVO       CHAR     := NULL,
      pCODIGO_PROCESSO      NUMBER   := NULL,
      pCHAVE_ARQUIVO        NUMBER   := NULL,
      pCODIGO_EMPRESA_GRUPO NUMBER   := NULL,
      retornarRegistro      CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_ARQUIVO NUMBER;
  BEGIN
    vCODIGO_ARQUIVO    := pCODIGO_ARQUIVO;
    IF vCODIGO_ARQUIVO IS NULL THEN
      SELECT SQ_ARQUIVO.nextval INTO vCODIGO_ARQUIVO FROM dual;
      INSERT
      INTO TB_ARQUIVO
        (
          CODIGO_ARQUIVO,
          TIPO_ARQUIVO,
          NOME_ARQUIVO,
          STATUS_ARQUIVO,
          DATA_INCLUSAO,
          CODIGO_PROCESSO,
          CHAVE_ARQUIVO,
          LAYOUT_ARQUIVO,
          CODIGO_EMPRESA_GRUPO
        )
        VALUES
        (
          vCODIGO_ARQUIVO,
          pTIPO_ARQUIVO,
          pNOME_ARQUIVO,
          pSTATUS_ARQUIVO,
          sysdate,
          pCODIGO_PROCESSO,
          pCHAVE_ARQUIVO,
          pLAYOUT_ARQUIVO,
          pCODIGO_EMPRESA_GRUPO
        );
    ELSE
      UPDATE TB_ARQUIVO
      SET TIPO_ARQUIVO     = pTIPO_ARQUIVO,
        NOME_ARQUIVO       = pNOME_ARQUIVO,
        STATUS_ARQUIVO     = pSTATUS_ARQUIVO,
        CODIGO_PROCESSO    = pCODIGO_PROCESSO,
        CHAVE_ARQUIVO      = pCHAVE_ARQUIVO,
        LAYOUT_ARQUIVO     = pLAYOUT_ARQUIVO
      WHERE CODIGO_ARQUIVO = vCODIGO_ARQUIVO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_ARQUIVO_B( pCODIGO_ARQUIVO => vCODIGO_ARQUIVO, retcur => retcur);
    END IF;
  END PR_ARQUIVO_S;
  PROCEDURE PR_ARQUIVO_B(
      pCODIGO_ARQUIVO NUMBER,
      retornarDetalhe CHAR := 'N',
      retcur OUT TP_REFCURSOR)
  AS
    quantidadeCriticas INT;
    quantidadeLinhas   INT;
  BEGIN
    IF retornarDetalhe = 'S' THEN
      SELECT COUNT(*)
      INTO quantidadeLinhas
      FROM TB_ARQUIVO_ITEM
      WHERE CODIGO_ARQUIVO = pCODIGO_ARQUIVO;
      SELECT COUNT(*)
      INTO quantidadeCriticas
      FROM TB_CRITICA
      WHERE CODIGO_ARQUIVO = pCODIGO_ARQUIVO;
    END IF;
    OPEN retcur FOR SELECT A.CODIGO_ARQUIVO ,
    A.TIPO_ARQUIVO ,
    A.NOME_ARQUIVO ,
    A.DATA_INCLUSAO ,
    A.STATUS_ARQUIVO ,
    A.CODIGO_PROCESSO ,
    NULL
  AS
    STATUS_PROCESSO ,
    quantidadeCriticas QUANTIDADE_CRITICAS ,
    quantidadeLinhas QUANTIDADE_LINHAS ,
    P.STATUS_PROCESSO ,
    A.CHAVE_ARQUIVO ,
    A.LAYOUT_ARQUIVO ,
    A.CODIGO_EMPRESA_GRUPO ,
    B.SIGLA_GRUPO FROM TB_ARQUIVO A left join TB_EMPRESA_GRUPO B ON A.CODIGO_EMPRESA_GRUPO = B.CODIGO_EMPRESA_GRUPO left join TB_PROCESSO P ON P.CODIGO_PROCESSO = a.CODIGO_PROCESSO WHERE A.CODIGO_ARQUIVO = pCODIGO_ARQUIVO;
  END PR_ARQUIVO_B;
  PROCEDURE PR_ARQUIVO_L(
      pFILTRO_NOME_ARQUIVO    VARCHAR2 := NULL,
      pFILTRO_STATUS_ARQUIVO  CHAR     := NULL,
      pFILTRO_DATA_INCLUSAO   DATE     := NULL,
      pFILTRO_CODIGO_PROCESSO NUMBER   := NULL,
      pFILTRO_CHAVE_ARQUIVO   VARCHAR2 := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery VARCHAR2(1000);
  BEGIN
    vQuery                  := '';
    IF pFILTRO_NOME_ARQUIVO IS NOT NULL THEN
      vQuery                := vQuery || 'A.NOME_ARQUIVO = ''' || pFILTRO_NOME_ARQUIVO || ''' and ';
    END IF;
    IF pFILTRO_STATUS_ARQUIVO IS NOT NULL THEN
      vQuery                  := vQuery || 'A.STATUS_ARQUIVO = ''' || pFILTRO_STATUS_ARQUIVO || ''' and ';
    END IF;
    IF pFILTRO_DATA_INCLUSAO IS NOT NULL THEN
      vQuery                 := vQuery || 'trunc(A.DATA_INCLUSAO) = ''' || pFILTRO_DATA_INCLUSAO || ''' and ';
    END IF;
    IF pFILTRO_CODIGO_PROCESSO IS NOT NULL THEN
      vQuery                   := vQuery || 'A.CODIGO_PROCESSO = ''' || pFILTRO_CODIGO_PROCESSO || ''' and ';
    END IF;
    IF pFILTRO_CHAVE_ARQUIVO IS NOT NULL THEN
      vQuery                 := vQuery || 'A.CHAVE_ARQUIVO = ''' || pFILTRO_CHAVE_ARQUIVO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery      := '  
select distinct * from   
(    
select       
A.CODIGO_ARQUIVO    
, A.TIPO_ARQUIVO    
, A.NOME_ARQUIVO    
, A.DATA_INCLUSAO    
, A.STATUS_ARQUIVO    
, A.CODIGO_PROCESSO    
, P.STATUS_PROCESSO    
, A.CHAVE_ARQUIVO    
, A.LAYOUT_ARQUIVO    
, A.CODIGO_EMPRESA_GRUPO    
, B.SIGLA_GRUPO    
from      TB_ARQUIVO        A    
left join TB_EMPRESA_GRUPO  B on  A.CODIGO_EMPRESA_GRUPO = B.CODIGO_EMPRESA_GRUPO    
left join TB_PROCESSO       P on  P.CODIGO_PROCESSO = a.CODIGO_PROCESSO     
' || vQuery || '  
)';
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' where rownum <= ' || maxLinhas;
    END IF;
    vQuery := vQuery || '   
order by CODIGO_ARQUIVO desc  
';
    OPEN retcur FOR vQuery;
  END PR_ARQUIVO_L;
  PROCEDURE PR_ARQUIVO_R(
      pCODIGO_ARQUIVO NUMBER)
  AS
  BEGIN
    DELETE TB_ARQUIVO WHERE CODIGO_ARQUIVO = pCODIGO_ARQUIVO;
  END PR_ARQUIVO_R;
  PROCEDURE PR_ARQUIVO_ITEM_STATUS_A(
      pCODIGO_ARQUIVO NUMBER )
  AS
  BEGIN
    UPDATE TB_ARQUIVO_ITEM
    SET STATUS_ARQUIVO_ITEM      = '1'
    WHERE CODIGO_ARQUIVO         = pCODIGO_ARQUIVO
    AND STATUS_ARQUIVO_ITEM NOT IN ('3', '4', '5');
    UPDATE TB_ARQUIVO_ITEM
    SET STATUS_ARQUIVO_ITEM  = '2'
    WHERE CODIGO_ARQUIVO     = pCODIGO_ARQUIVO
    AND STATUS_ARQUIVO_ITEM  = '1'
    AND CODIGO_ARQUIVO_ITEM IN
      ( SELECT DISTINCT C.CODIGO_ARQUIVO_ITEM
      FROM TB_CRITICA C
      WHERE C.CODIGO_ARQUIVO = pCODIGO_ARQUIVO
      );
  END PR_ARQUIVO_ITEM_STATUS_A;
  PROCEDURE PR_ARQUIVO_ITEM_STATUS_S(
      pFILTRO_CODIGO_ARQUIVO      NUMBER   := NULL,
      pFILTRO_CODIGO_ARQUIVO_ITEM NUMBER   := NULL,
      pFILTRO_TIPO_ARQUIVO_ITEM   VARCHAR2 := NULL,
      pFILTRO_TIPO_CRITICA        VARCHAR2 := NULL,
      pFILTRO_NOME_CAMPO          VARCHAR2 := NULL,
      pFILTRO_STATUS_ARQUIVO_ITEM CHAR     := NULL,
      pSTATUS_ARQUIVO_ITEM        CHAR,
      retcur OUT TP_REFCURSOR )
  AS
    vQUANTIDADE_ALTERADA INT;
    vQUANTIDADE          INT;
    vCODIGO_PROCESSO     NUMBER;
    cursorProcessos TP_REFCURSOR;
    vQuery        VARCHAR2(2000);
    vQueryCritica VARCHAR2(2000);
  BEGIN
    vQuery                    := '';
    vQueryCritica             := '';
    IF pFILTRO_CODIGO_ARQUIVO IS NOT NULL THEN
      vQuery                  := vQuery || 'A.CODIGO_ARQUIVO = ''' || pFILTRO_CODIGO_ARQUIVO || ''' and ';
      vQueryCritica           := vQueryCritica || 'B.CODIGO_ARQUIVO = ''' || pFILTRO_CODIGO_ARQUIVO || ''' and ';
    END IF;
    IF pFILTRO_CODIGO_ARQUIVO_ITEM IS NOT NULL THEN
      vQuery                       := vQuery || 'A.CODIGO_ARQUIVO_ITEM = ''' || pFILTRO_CODIGO_ARQUIVO_ITEM || ''' and ';
      vQueryCritica                := vQueryCritica || 'B.CODIGO_ARQUIVO_ITEM = ''' || pFILTRO_CODIGO_ARQUIVO_ITEM || ''' and ';
    END IF;
    IF pFILTRO_TIPO_ARQUIVO_ITEM IS NOT NULL THEN
      vQuery                     := vQuery || 'A.TIPO_ARQUIVO_ITEM = ''' || pFILTRO_TIPO_ARQUIVO_ITEM || ''' and ';
      vQueryCritica              := vQueryCritica || 'B.TIPO_LINHA = ''' || pFILTRO_TIPO_ARQUIVO_ITEM || ''' and ';
    END IF;
    IF pFILTRO_STATUS_ARQUIVO_ITEM IS NOT NULL THEN
      vQuery                       := vQuery || 'A.STATUS_ARQUIVO_ITEM = ''' || pFILTRO_STATUS_ARQUIVO_ITEM || ''' and ';
    END IF;
    IF pFILTRO_TIPO_CRITICA IS NOT NULL THEN
      vQueryCritica         := vQueryCritica || 'B.TIPO_CRITICA = ''' || pFILTRO_TIPO_CRITICA || ''' and ';
    END IF;
    IF pFILTRO_NOME_CAMPO IS NOT NULL THEN
      vQueryCritica       := vQueryCritica || 'B.NOME_CAMPO = ''' || pFILTRO_NOME_CAMPO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'and ' || vQuery;
    END IF;
    IF LENGTH(vQueryCritica) > 0 THEN
      vQueryCritica         := SUBSTR(vQueryCritica, 0, LENGTH(vQueryCritica) - 4);
      vQueryCritica         := 'where ' || vQueryCritica;
    END IF;
    EXECUTE immediate 'update TB_ARQUIVO_ITEM A                    
set STATUS_ARQUIVO_ITEM = ''' || pSTATUS_ARQUIVO_ITEM || ''' where A.CODIGO_ARQUIVO_ITEM in (                            
select distinct B.CODIGO_ARQUIVO_ITEM                                       
from TB_CRITICA B ' || vQueryCritica || ')                    
and A.STATUS_ARQUIVO_ITEM != ''' || pSTATUS_ARQUIVO_ITEM || ''' ' || vQuery;
    vQUANTIDADE_ALTERADA := sql%rowcount;
    OPEN retcur FOR SELECT vQUANTIDADE_ALTERADA QUANTIDADE FROM dual;
    OPEN cursorProcessos FOR 'select distinct C.CODIGO_PROCESSO from TB_ARQUIVO_ITEM A          
inner join TB_ARQUIVO C on A.CODIGO_ARQUIVO = C.CODIGO_ARQUIVO          
where A.CODIGO_ARQUIVO_ITEM in           
( select distinct B.CODIGO_ARQUIVO_ITEM from TB_CRITICA B ' || vQueryCritica || ') ' || vQuery;
    LOOP
      FETCH cursorProcessos INTO vCODIGO_PROCESSO;
      EXIT
    WHEN cursorProcessos%NOTFOUND;
      PR_PROCESSO_STATUS_BLOQUEIO_U(PCODIGO_PROCESSO => VCODIGO_PROCESSO);
    END LOOP;
    CLOSE cursorProcessos;
  END PR_ARQUIVO_ITEM_STATUS_S;
  PROCEDURE PR_ARQUIVO_ITEM_S(
      pCODIGO_ARQUIVO_ITEM NUMBER := NULL,
      pCODIGO_ARQUIVO      NUMBER := NULL,
      pCONTEUDO_ARQUIVO_ITEM CLOB := NULL,
      pTIPO_ARQUIVO_ITEM   VARCHAR2 := NULL,
      pSTATUS_ARQUIVO_ITEM CHAR     := NULL,
      pCHAVE               VARCHAR2 := NULL,
      pREFERENCIA          VARCHAR2 := NULL,
      retornarRegistro     CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_ARQUIVO_ITEM NUMBER;
  BEGIN
    vCODIGO_ARQUIVO_ITEM    := pCODIGO_ARQUIVO_ITEM;
    IF vCODIGO_ARQUIVO_ITEM IS NULL THEN
      SELECT SQ_ARQUIVO_ITEM.nextval INTO vCODIGO_ARQUIVO_ITEM FROM dual;
      INSERT
      INTO TB_ARQUIVO_ITEM
        (
          CODIGO_ARQUIVO_ITEM,
          CODIGO_ARQUIVO,
          CONTEUDO_ARQUIVO_ITEM,
          TIPO_ARQUIVO_ITEM,
          STATUS_ARQUIVO_ITEM,
          CHAVE,
          REFERENCIA
        )
        VALUES
        (
          vCODIGO_ARQUIVO_ITEM,
          pCODIGO_ARQUIVO,
          pCONTEUDO_ARQUIVO_ITEM,
          pTIPO_ARQUIVO_ITEM,
          pSTATUS_ARQUIVO_ITEM,
          pCHAVE,
          pREFERENCIA
        );
    ELSE
      UPDATE TB_ARQUIVO_ITEM
      SET CODIGO_ARQUIVO        = NVL(pCODIGO_ARQUIVO, CODIGO_ARQUIVO),
        CONTEUDO_ARQUIVO_ITEM   = NVL(pCONTEUDO_ARQUIVO_ITEM, CONTEUDO_ARQUIVO_ITEM),
        TIPO_ARQUIVO_ITEM       = NVL(pTIPO_ARQUIVO_ITEM, TIPO_ARQUIVO_ITEM),
        STATUS_ARQUIVO_ITEM     = NVL(pSTATUS_ARQUIVO_ITEM, STATUS_ARQUIVO_ITEM),
        CHAVE                   = NVL(pCHAVE, CHAVE),
        REFERENCIA              = NVL(pREFERENCIA, REFERENCIA)
      WHERE CODIGO_ARQUIVO_ITEM = vCODIGO_ARQUIVO_ITEM;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_ARQUIVO_ITEM_B( pCODIGO_ARQUIVO_ITEM => vCODIGO_ARQUIVO_ITEM, retcur => retcur);
    END IF;
  END PR_ARQUIVO_ITEM_S;
  PROCEDURE PR_ARQUIVO_ITEM_B(
      pCODIGO_ARQUIVO_ITEM NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_ARQUIVO_ITEM ,
    A.CODIGO_ARQUIVO ,
    A.CONTEUDO_ARQUIVO_ITEM ,
    A.TIPO_ARQUIVO_ITEM ,
    A.STATUS_ARQUIVO_ITEM ,
    A.CHAVE ,
    A.REFERENCIA FROM TB_ARQUIVO_ITEM A WHERE A.CODIGO_ARQUIVO_ITEM = pCODIGO_ARQUIVO_ITEM;
  END PR_ARQUIVO_ITEM_B;
  PROCEDURE PR_ARQUIVO_ITEM_L(
      pCODIGO_ARQUIVO             NUMBER   := NULL,
      pCODIGO_ARQUIVO_ITEM        NUMBER   := NULL,
      pTIPO_ARQUIVO_ITEM          VARCHAR2 := NULL,
      pTIPO_CRITICA               VARCHAR2 := NULL,
      pNOME_CAMPO                 VARCHAR2 := NULL,
      pCODIGO_PROCESSO            NUMBER   := NULL,
      pFILTRARSEMCRITICA          VARCHAR2 := 'N',
      pFILTRARAPENASSOBANALISE    VARCHAR2 := 'N',
      pFILTRARCOMCRITICA          VARCHAR2 := 'N',
      pRECEBERPENDENTEDESBLOQUEIO VARCHAR2 := 'N',
      pFILTRARNAOBLOQUEADOS       VARCHAR2 := 'N',
      pFILTRARAPENASBLOQUEADOS    VARCHAR2 := 'N',
      pIGNORAR_CONTEUDO           VARCHAR2 := 'N',
      ctSTATUS_BLOQUEADO          CHAR     := NULL,
      ctSTATUS_EXCLUIDO           CHAR     := NULL,
      ctSTATUS_CANCELADO          VARCHAR2 := 'N',
      pSTATUS_ARQUIVO_ITEM        CHAR     := NULL,
      pSTATUS_ARQUIVO_ITEM2       CHAR     := NULL,
      pCHAVE                      VARCHAR2 := NULL,
      pCHAVE_INICIO               VARCHAR2 := NULL,
      MAXLINHAS                   INT      := 0,
      LIMITE_MINIMO               NUMBER   := 0,
      LIMITE_MAXIMO               NUMBER   := 0,
      pDTINCINICIAL               DATE     := NULL,
      pDTINCFINAL                 DATE     := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery          VARCHAR(4000);
    vSelect         VARCHAR(4000);
    vWhere          VARCHAR(2000);
    vOrderBy        VARCHAR(100);
    vStatus         VARCHAR2(100);
    vArquivo        VARCHAR2(1000);
    formatoData     VARCHAR2(20);
    vCODIGO_ARQUIVO NUMBER;
    vCursor TP_REFCURSOR;
  BEGIN
    vWhere               := '';
    vQuery               := '';
    vSelect              := '';
    vOrderBy             := '';
    vStatus              := '';
    vArquivo             := '';
    formatoData          := 'yyyy-mm-dd';
    vCODIGO_ARQUIVO      := 0;
    vOrderBy             := '      
order by A.CODIGO_ARQUIVO_ITEM      
';
    vSelect              := '       
select  A.CODIGO_ARQUIVO_ITEM                   
,       A.CODIGO_ARQUIVO                        
,       A.CONTEUDO_ARQUIVO_ITEM                 
,       A.TIPO_ARQUIVO_ITEM                     
,       A.STATUS_ARQUIVO_ITEM                   
,       B.TIPO_ARQUIVO                          
,       B.CODIGO_PROCESSO                       
,       A.CHAVE                                 
,       C.CODIGO_ARQUIVO_ORIGEM      
,       A.REFERENCIA                            
,       B.DATA_INCLUSAO      
from        TB_ARQUIVO             B             
inner join  TB_ARQUIVO_ITEM        A on A.CODIGO_ARQUIVO = B.CODIGO_ARQUIVO       
left join   TB_ITEM_DESBLOQUEIO    C on C.CODIGO_ARQUIVO_ITEM = A.CODIGO_ARQUIVO_ITEM AND C.CODIGO_ARQUIVO_DESTINO = B.CODIGO_ARQUIVO AND C.STATUS_DESBLOQUEIO = ''A''      
';
    IF (pCODIGO_PROCESSO IS NOT NULL) THEN
      vWhere             := vWhere || ' B.CODIGO_PROCESSO = ' || pCODIGO_PROCESSO || ' and ';
    END IF;
    IF (pCODIGO_ARQUIVO IS NOT NULL) THEN
      vWhere            := vWhere || ' A.CODIGO_ARQUIVO = ' || pCODIGO_ARQUIVO || ' and ';
    END IF;
    IF (pCODIGO_ARQUIVO_ITEM IS NOT NULL) THEN
      vWhere                 := vWhere || ' A.CODIGO_ARQUIVO_ITEM = ' || pCODIGO_ARQUIVO_ITEM || ' and ';
    END IF;
    IF (pTIPO_ARQUIVO_ITEM IS NOT NULL) THEN
      vWhere               := vWhere || ' A.TIPO_ARQUIVO_ITEM IN(''' || pTIPO_ARQUIVO_ITEM || ''') and ';
    END IF;
    IF (pSTATUS_ARQUIVO_ITEM IS NOT NULL) THEN
      vWhere                 := vWhere || ' A.STATUS_ARQUIVO_ITEM = ''' || pSTATUS_ARQUIVO_ITEM || ''' and ';
    END IF;
    IF (pSTATUS_ARQUIVO_ITEM2 IS NOT NULL) THEN
      vWhere                  := vWhere || ' A.STATUS_ARQUIVO_ITEM = ''' || pSTATUS_ARQUIVO_ITEM2 || ''' and ';
    END IF;
    IF ( pSTATUS_ARQUIVO_ITEM                IS NULL AND pSTATUS_ARQUIVO_ITEM2 IS NULL) THEN
      IF (NVL(pFILTRARAPENASBLOQUEADOS, 'N') != 'N') THEN
        vWhere                               := vWhere || ' A.STATUS_ARQUIVO_ITEM in (''3'', ''5'') and ';
      END IF;
      IF (NVL(pFILTRARSEMCRITICA, 'N') != 'N') THEN
        vWhere                         := vWhere || ' A.STATUS_ARQUIVO_ITEM in (''0'', ''1'', ''6'', ''7'', ''8'') and ';
      END IF;
      IF (NVL(ctSTATUS_CANCELADO, 'N') != 'N') THEN
        vWhere                         := vWhere || ' A.STATUS_ARQUIVO_ITEM <> ''6'' and ';
      END IF;
      IF (NVL(pFILTRARSEMCRITICA, 'N') = 'N') AND (NVL(pFILTRARNAOBLOQUEADOS, 'N') != 'N') THEN
        vWhere                        := vWhere || ' A.STATUS_ARQUIVO_ITEM != ''' || ctSTATUS_BLOQUEADO || ''' and A.STATUS_ARQUIVO_ITEM != ''' || ctSTATUS_EXCLUIDO || ''' and ';
      END IF;
    END IF;
    IF (pCHAVE IS NOT NULL) THEN
      vWhere   := vWhere || ' A.CHAVE = ''' || pCHAVE || ''' and ';
    END IF;
    IF (pCHAVE_INICIO IS NOT NULL) THEN
      vWhere          := vWhere || ' A.CHAVE like ''' || pCHAVE_INICIO || '%' || ''' and ';
    END IF;
    IF (pDTINCINICIAL IS NOT NULL ) THEN
      vWhere          := vWhere || ' B.DATA_INCLUSAO >= ''' || pDTINCINICIAL || ''' and ';
    END IF;
    IF (pDTINCFINAL IS NOT NULL ) THEN
      vWhere        := vWhere || '  B.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pDTINCFINAL, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'')' || ' and ';
    END IF;
    IF (NVL(pFILTRARCOMCRITICA, 'N') != 'N') THEN
      vWhere                         := vWhere || ' A.CODIGO_ARQUIVO_ITEM in ' || '( ' || ' select  distinct BA.CODIGO_ARQUIVO_ITEM ' || ' from    TB_CRITICA BA                   ' || ' where   BA.CODIGO_ARQUIVO = ' || pCODIGO_ARQUIVO || ' ' ||
      CASE
      WHEN pTIPO_ARQUIVO_ITEM IS NULL THEN
        ''
      ELSE
        ' and BA.TIPO_LINHA = ''' || pTIPO_ARQUIVO_ITEM || ''' '
      END ||
      CASE
      WHEN pTIPO_CRITICA IS NULL THEN
        ''
      ELSE
        ' and BA.TIPO_CRITICA = ''' || pTIPO_CRITICA || ''' '
      END ||
      CASE
      WHEN pNOME_CAMPO IS NULL THEN
        ''
      ELSE
        ' and BA.NOME_CAMPO = ''' || pNOME_CAMPO || ''' '
      END || ' ) and ';
    END IF;
    IF (maxLinhas > 0) THEN
      vWhere     := vWhere || ' rownum <= ' || maxLinhas || ' and ';
    END IF;
    vArquivo                                                                        := NULL;
    IF (PCODIGO_PROCESSO                                                            IS NOT NULL) THEN
      IF (PCODIGO_ARQUIVO                                                           IS NULL) THEN
        OPEN vCursor FOR SELECT CODIGO_ARQUIVO FROM TB_ARQUIVO WHERE CODIGO_PROCESSO = PCODIGO_PROCESSO;
        LOOP
          FETCH vCursor INTO VCODIGO_ARQUIVO;
          EXIT
        WHEN vCursor%NOTFOUND;
          IF NVL(VCODIGO_ARQUIVO,0) > 0 THEN
            IF (vArquivo           IS NULL) THEN
              vArquivo             := vCODIGO_ARQUIVO;
            ELSE
              vArquivo := vArquivo || ' ,' || vCODIGO_ARQUIVO;
            END IF;
          END IF;
        END LOOP;
        CLOSE vCursor;
      END IF;
    END IF;
    IF (vArquivo IS NOT NULL) THEN
      vArquivo   := ' A.CODIGO_ARQUIVO in (' || vArquivo || ') and ';
      vWhere     := vArquivo ||vWhere;
    END IF;
    vArquivo                         := NULL;
    IF (PCODIGO_PROCESSO             IS NULL) THEN
      IF (PCODIGO_ARQUIVO            IS NULL) THEN
        IF (PCODIGO_ARQUIVO_ITEM     IS NULL) THEN
          IF (PSTATUS_ARQUIVO_ITEM   IS NOT NULL OR PSTATUS_ARQUIVO_ITEM2 IS NOT NULL) THEN
            IF (PSTATUS_ARQUIVO_ITEM IS NOT NULL AND PSTATUS_ARQUIVO_ITEM2 IS NOT NULL) THEN
              vStatus                := ' STATUS_ARQUIVO_ITEM in (''' || PSTATUS_ARQUIVO_ITEM || ''', ''' || PSTATUS_ARQUIVO_ITEM2 || ''')';
            END IF;
            IF (PSTATUS_ARQUIVO_ITEM IS NOT NULL AND PSTATUS_ARQUIVO_ITEM2 IS NULL) THEN
              vStatus                := ' STATUS_ARQUIVO_ITEM in (''' || PSTATUS_ARQUIVO_ITEM || ''')';
            END IF;
            IF (PSTATUS_ARQUIVO_ITEM IS NULL AND PSTATUS_ARQUIVO_ITEM2 IS NOT NULL) THEN
              vStatus                := ' STATUS_ARQUIVO_ITEM in (''' || PSTATUS_ARQUIVO_ITEM2 || ''')';
            END IF;
            OPEN vCursor FOR 'SELECT DISTINCT CODIGO_ARQUIVO FROM TB_ARQUIVO_ITEM WHERE ' || vStatus;
            LOOP
              FETCH vCursor INTO VCODIGO_ARQUIVO;
              EXIT
            WHEN vCursor%NOTFOUND;
              IF NVL(VCODIGO_ARQUIVO,0) > 0 THEN
                IF (vArquivo           IS NULL) THEN
                  vArquivo             := vCODIGO_ARQUIVO;
                ELSE
                  vArquivo := vArquivo || ' ,' || vCODIGO_ARQUIVO;
                END IF;
              END IF;
            END LOOP;
            CLOSE vCursor;
          END IF;
        END IF;
      END IF;
    END IF;
    IF (vArquivo IS NOT NULL) THEN
      vArquivo   := ' A.CODIGO_ARQUIVO in (' || vArquivo || ') and ';
      vWhere     := vArquivo ||vWhere;
    END IF;
    vWhere := SUBSTR(vWhere, 0, LENGTH(vWhere) - 4);
    vQuery := vSelect || '      
where       
' || vWhere || '      
' || vOrderBy;
    OPEN RETCUR FOR vQUERY;
  END PR_ARQUIVO_ITEM_L;
  PROCEDURE PR_ARQUIVO_ITEM_R(
      pCODIGO_ARQUIVO NUMBER)
  AS
    VQTD NUMBER;
  BEGIN
    PR_CARONA_D ( PCODIGO_ARQUIVO_ITEM => NULL , PCODIGO_ARQUIVO => PCODIGO_ARQUIVO , PCODIGO_PROCESSO => NULL , PQTD => VQTD );
    DELETE TB_ARQUIVO WHERE CODIGO_ARQUIVO = pCODIGO_ARQUIVO;
  END PR_ARQUIVO_ITEM_R;
  PROCEDURE PR_CRITICA_S(
      pCODIGO_CRITICA      NUMBER   := NULL,
      pCODIGO_PROCESSO     NUMBER   := NULL,
      pCODIGO_ARQUIVO      NUMBER   := NULL,
      pCODIGO_ARQUIVO_ITEM NUMBER   := NULL,
      pDATA_CRITICA        DATE     := NULL,
      pTIPO_CRITICA        VARCHAR2 := NULL,
      pDESCRICAO_CRITICA   VARCHAR2 := NULL,
      pCONTEUDO_CRITICA CLOB        := NULL,
      pTIPO_LINHA      VARCHAR2          := NULL,
      pNOME_CAMPO      VARCHAR2          := NULL,
      retornarRegistro CHAR              := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_CRITICA NUMBER;
  BEGIN
    vCODIGO_CRITICA    := pCODIGO_CRITICA;
    IF vCODIGO_CRITICA IS NULL THEN
      SELECT SQ_CRITICA.nextval INTO vCODIGO_CRITICA FROM dual;
      INSERT
      INTO TB_CRITICA
        (
          CODIGO_CRITICA,
          CODIGO_PROCESSO,
          CODIGO_ARQUIVO,
          CODIGO_ARQUIVO_ITEM,
          DATA_CRITICA,
          TIPO_CRITICA,
          DESCRICAO_CRITICA,
          CONTEUDO_CRITICA,
          TIPO_LINHA,
          NOME_CAMPO
        )
        VALUES
        (
          vCODIGO_CRITICA,
          pCODIGO_PROCESSO,
          pCODIGO_ARQUIVO,
          pCODIGO_ARQUIVO_ITEM,
          pDATA_CRITICA,
          pTIPO_CRITICA,
          pDESCRICAO_CRITICA,
          pCONTEUDO_CRITICA,
          pTIPO_LINHA,
          pNOME_CAMPO
        );
    ELSE
      UPDATE TB_CRITICA
      SET CODIGO_PROCESSO   = pCODIGO_PROCESSO,
        CODIGO_ARQUIVO      = pCODIGO_ARQUIVO,
        CODIGO_ARQUIVO_ITEM = pCODIGO_ARQUIVO_ITEM,
        DATA_CRITICA        = pDATA_CRITICA,
        TIPO_CRITICA        = pTIPO_CRITICA,
        DESCRICAO_CRITICA   = pDESCRICAO_CRITICA,
        CONTEUDO_CRITICA    = pCONTEUDO_CRITICA,
        TIPO_LINHA          = pTIPO_LINHA,
        NOME_CAMPO          = pNOME_CAMPO
      WHERE CODIGO_CRITICA  = vCODIGO_CRITICA;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_CRITICA_B( pCODIGO_CRITICA => vCODIGO_CRITICA, retcur => retcur);
    END IF;
  END PR_CRITICA_S;
  PROCEDURE PR_CRITICA_B(
      pCODIGO_CRITICA NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_CRITICA ,
    A.CODIGO_PROCESSO ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ARQUIVO_ITEM ,
    A.DATA_CRITICA ,
    A.TIPO_CRITICA ,
    A.DESCRICAO_CRITICA ,
    A.CONTEUDO_CRITICA ,
    A.TIPO_LINHA ,
    A.NOME_CAMPO FROM TB_CRITICA A WHERE A.CODIGO_CRITICA = pCODIGO_CRITICA;
  END PR_CRITICA_B;
  PROCEDURE PR_CRITICA_AGRUPAMENTO_L(
      pCODIGO_PROCESSO NUMBER := NULL ,
      pCODIGO_ARQUIVO  NUMBER := NULL ,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery           VARCHAR2(2000);
    vSelect          VARCHAR2(2000);
    vOrder           VARCHAR2(2000);
    vWhere           VARCHAR2(2000);
    vWith            VARCHAR2(2000);
    vCODIGO_PROCESSO NUMBER;
    vCODIGO_ARQUIVO  NUMBER;
  BEGIN
    vQuery              := '';
    vSelect             := '';
    vOrder              := '';
    vWhere              := '';
    vWith               := '';
    vCODIGO_PROCESSO    := PCODIGO_PROCESSO;
    vCODIGO_ARQUIVO     := PCODIGO_ARQUIVO;
    IF PCODIGO_PROCESSO IS NULL AND PCODIGO_ARQUIVO IS NOT NULL THEN
      SELECT CODIGO_PROCESSO
      INTO vCODIGO_PROCESSO
      FROM TB_ARQUIVO
      WHERE CODIGO_ARQUIVO = PCODIGO_ARQUIVO
      AND ROWNUM           = 1;
    END IF;
    IF PCODIGO_ARQUIVO IS NULL AND PCODIGO_PROCESSO IS NOT NULL THEN
      SELECT CODIGO_ARQUIVO
      INTO vCODIGO_ARQUIVO
      FROM TB_ARQUIVO
      WHERE CODIGO_PROCESSO = PCODIGO_PROCESSO
      AND ROWNUM            = 1;
    END IF;
    IF VCODIGO_ARQUIVO IS NOT NULL AND VCODIGO_PROCESSO IS NOT NULL THEN
      vWhere           := 'where   CODIGO_ARQUIVO = ' || VCODIGO_ARQUIVO || ' ' || 'and     CODIGO_PROCESSO = ' || VCODIGO_PROCESSO || '         
';
    ELSE
      IF VCODIGO_ARQUIVO IS NOT NULL THEN
        vWhere           := 'where   CODIGO_ARQUIVO = ' || VCODIGO_ARQUIVO || '               
';
      ELSE
        IF VCODIGO_PROCESSO IS NOT NULL THEN
          vWhere            := 'where   CODIGO_PROCESSO = ' || VCODIGO_PROCESSO || '                   
';
        ELSE
          vWhere := 'where   CODIGO_PROCESSO = 0                   
';
        END IF;
      END IF;
    END IF;
    vWith   := 'WITH CRITICA AS       
( SELECT   /*+ first_rows (100) */                
CODIGO_ARQUIVO, CODIGO_PROCESSO ,TIPO_LINHA ,TIPO_CRITICA ,NOME_CAMPO         
FROM    TB_CRITICA A      
' || vWhere || '      
)      
';
    vSelect := '      
select  grouping (CODIGO_ARQUIVO)   GRUPO_CODIGO_ARQUIVO       
,       grouping (TIPO_LINHA)       GRUPO_TIPO_LINHA       
,       grouping (TIPO_CRITICA)     GRUPO_TIPO_CRITICA       
,       grouping (NOME_CAMPO)       GRUPO_NOME_CAMPO       
,       grouping (CODIGO_ARQUIVO) + grouping (TIPO_LINHA) + grouping (TIPO_CRITICA) + grouping (NOME_CAMPO) NIVEL       
,       CODIGO_ARQUIVO       
,       TIPO_LINHA       
,       TIPO_CRITICA       
,       NOME_CAMPO       
,       count(*)                    QUANTIDADE       
FROM  CRITICA       
';
    vOrder  := '      
group by rollup (CODIGO_ARQUIVO, TIPO_LINHA, TIPO_CRITICA, NOME_CAMPO)      
order by CODIGO_ARQUIVO, TIPO_LINHA, TIPO_CRITICA, NOME_CAMPO      
';
    vQuery  := vWith || vSelect || VOrder;
    OPEN retcur FOR vQuery;
  END PR_CRITICA_AGRUPAMENTO_L;
  PROCEDURE PR_CRITICA_AGRUPAMENTO_DET_B(
      pCODIGO_ARQUIVO NUMBER   := NULL,
      pTIPO_LINHA     VARCHAR2 := NULL,
      pTIPO_CRITICA   VARCHAR2 := NULL,
      pNOME_CAMPO     VARCHAR2 := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQUANTIDADE_LINHAS         NUMBER;
    vQUANTIDADE_CRITICAS       NUMBER;
    vSTATUS_01_VALIDO          NUMBER;
    vSTATUS_02_INVALIDO        NUMBER;
    vSTATUS_03_BLOQUEADO       NUMBER;
    vSTATUS_04_EXCLUIDO        NUMBER;
    vSTATUS_05_PENDDESBLOQUEIO NUMBER;
    vSTATUS_06_CANCELADO       NUMBER;
    vQuery                     VARCHAR2(1000);
  BEGIN
    vQuery         := '';
    IF pTIPO_LINHA IS NOT NULL THEN
      vQuery       := vQuery || 'TIPO_LINHA = ''' || pTIPO_LINHA || ''' and ';
    END IF;
    IF pTIPO_CRITICA IS NOT NULL THEN
      vQuery         := vQuery || 'TIPO_CRITICA = ''' || pTIPO_CRITICA || ''' and ';
    END IF;
    IF pNOME_CAMPO IS NOT NULL THEN
      vQuery       := vQuery || 'NOME_CAMPO = ''' || pNOME_CAMPO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'and ' || vQuery;
    END IF;
    EXECUTE IMMEDIATE '      
select *       
from       
(         
select distinct t.STATUS_ARQUIVO_ITEM, t.CODIGO_ARQUIVO_ITEM         
from TB_ARQUIVO_ITEM t         
inner join TB_CRITICA c         
on t.codigo_arquivo_item = c.codigo_arquivo_item         
where t.CODIGO_ARQUIVO = ' || pCODIGO_ARQUIVO || vQuery || '             
)      
pivot       
(         
count(DISTINCT CODIGO_ARQUIVO_ITEM)         
for STATUS_ARQUIVO_ITEM in (1,2,3,4,5,6)      
)    ' INTO vSTATUS_01_VALIDO ,
    vSTATUS_02_INVALIDO ,
    vSTATUS_03_BLOQUEADO ,
    vSTATUS_04_EXCLUIDO ,
    vSTATUS_05_PENDDESBLOQUEIO ,
    vSTATUS_06_CANCELADO ;
    vQUANTIDADE_LINHAS := vSTATUS_01_VALIDO + vSTATUS_02_INVALIDO + vSTATUS_03_BLOQUEADO + vSTATUS_04_EXCLUIDO + vSTATUS_05_PENDDESBLOQUEIO + vSTATUS_06_CANCELADO;
    EXECUTE immediate 'select count(1)         
from TB_CRITICA         
where CODIGO_ARQUIVO = ''' || pCODIGO_ARQUIVO || ''' ' || vQuery INTO vQUANTIDADE_CRITICAS;
    OPEN retcur FOR SELECT pCODIGO_ARQUIVO
  AS
    CODIGO_ARQUIVO ,
    pTIPO_LINHA
  AS
    TIPO_LINHA ,
    pTIPO_CRITICA
  AS
    TIPO_CRITICA ,
    pNOME_CAMPO
  AS
    NOME_CAMPO ,
    vQUANTIDADE_LINHAS
  AS
    QUANTIDADE_LINHAS ,
    vQUANTIDADE_CRITICAS
  AS
    QUANTIDADE_CRITICAS ,
    vSTATUS_01_VALIDO
  AS
    STATUS01 ,
    vSTATUS_02_INVALIDO
  AS
    STATUS02 ,
    vSTATUS_03_BLOQUEADO
  AS
    STATUS03 ,
    vSTATUS_04_EXCLUIDO
  AS
    STATUS04 ,
    vSTATUS_05_PENDDESBLOQUEIO
  AS
    STATUS05 ,
    vSTATUS_06_CANCELADO
  AS
    STATUS06 FROM dual;
  END PR_CRITICA_AGRUPAMENTO_DET_B;
  PROCEDURE PR_CRITICA_L(
      pFILTRO_CODIGO_ARQUIVO    NUMBER   := NULL,
      pFILTRO_CODIGO_PROCESSO   NUMBER   := NULL,
      pFILTRO_TIPO_CRITICA      VARCHAR2 := NULL,
      pFILTRO_TIPO_LINHA        VARCHAR2 := NULL,
      pFILTRO_NOME_CAMPO        VARCHAR2 := NULL,
      pFILTRO_DESCRICAO_CRITICA VARCHAR2 := NULL,
      pFILTRO_AGRUPADO          INT      := 0,
      pCODIGO_ARQUIVO_ITEM      NUMBER   := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery VARCHAR2(1000);
  BEGIN
    vQuery                    := '';
    IF pFILTRO_CODIGO_ARQUIVO IS NOT NULL THEN
      vQuery                  := vQuery || 'A.CODIGO_ARQUIVO = ''' || pFILTRO_CODIGO_ARQUIVO || ''' and ';
    END IF;
    IF pFILTRO_CODIGO_PROCESSO IS NOT NULL THEN
      vQuery                   := vQuery || 'A.CODIGO_PROCESSO = ''' || pFILTRO_CODIGO_PROCESSO || ''' and ';
    END IF;
    IF pFILTRO_TIPO_CRITICA IS NOT NULL THEN
      vQuery                := vQuery || 'A.TIPO_CRITICA = ''' || pFILTRO_TIPO_CRITICA || ''' and ';
    END IF;
    IF pFILTRO_TIPO_LINHA IS NOT NULL THEN
      vQuery              := vQuery || 'A.TIPO_LINHA = ''' || pFILTRO_TIPO_LINHA || ''' and ';
    END IF;
    IF pFILTRO_NOME_CAMPO IS NOT NULL THEN
      vQuery              := vQuery || 'A.NOME_CAMPO = ''' || pFILTRO_NOME_CAMPO || ''' and ';
    END IF;
    IF pCODIGO_ARQUIVO_ITEM IS NOT NULL THEN
      vQuery                := vQuery || 'A.CODIGO_ARQUIVO_ITEM = ''' || pCODIGO_ARQUIVO_ITEM || ''' and ';
    END IF;
    IF pFILTRO_DESCRICAO_CRITICA IS NOT NULL THEN
      vQuery                     := vQuery || 'A.DESCRICAO_CRITICA = ''' || pFILTRO_DESCRICAO_CRITICA || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    IF (PFILTRO_AGRUPADO = 0) THEN
      vQuery            := '  
SELECT *  
FROM    
(SELECT       
A.CODIGO_CRITICA,      
A.CODIGO_PROCESSO,      
A.CODIGO_ARQUIVO,      
A.CODIGO_ARQUIVO_ITEM,      
A.DATA_CRITICA,      
A.TIPO_CRITICA,      
A.DESCRICAO_CRITICA,      
A.CONTEUDO_CRITICA,      
A.TIPO_LINHA,      
A.NOME_CAMPO    
FROM TB_CRITICA A ' || vQuery || ' ORDER BY CODIGO_CRITICA  )      
';
    ELSE
      vQuery := '  
SELECT *  
FROM    
(SELECT       
count(1) AS CODIGO_CRITICA,      
A.CODIGO_PROCESSO,      
A.CODIGO_ARQUIVO,      
NULL AS CODIGO_ARQUIVO_ITEM,      
NULL AS DATA_CRITICA,      
A.TIPO_CRITICA,      
A.DESCRICAO_CRITICA,      
NULL AS CONTEUDO_CRITICA,      
A.TIPO_LINHA,      
A.NOME_CAMPO    
FROM TB_CRITICA A ' || vQuery || ' GROUP BY      
A.CODIGO_PROCESSO,      
A.CODIGO_ARQUIVO,      
A.TIPO_CRITICA,      
A.DESCRICAO_CRITICA,      
A.TIPO_LINHA,      
A.NOME_CAMPO )      
';
    END IF;
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' where rownum <= ' || maxLinhas;
    END IF;
    OPEN retcur FOR vQuery;
  END PR_CRITICA_L;
  PROCEDURE PR_CRITICA_R(
      pCODIGO_CRITICA NUMBER := NULL,
      pCODIGO_ARQUIVO NUMBER := NULL )
  AS
  BEGIN
    IF (pCODIGO_ARQUIVO IS NOT NULL) THEN
      DELETE TB_CRITICA WHERE (CODIGO_ARQUIVO = pCODIGO_ARQUIVO);
    END IF;
    IF (pCODIGO_CRITICA IS NOT NULL) THEN
      DELETE TB_CRITICA WHERE (CODIGO_CRITICA = pCODIGO_CRITICA);
    END IF;
  END PR_CRITICA_R;
  PROCEDURE PR_TRANSACAO_SUBSTITUI_CONTA(
      pCODIGO_CONTA_FAVORECIDO NUMBER   := NULL ,
      pCODIGO_PRODUTO          NUMBER   := NULL ,
      pREFERENCIA              VARCHAR2 := NULL ,
      pQTDE_ALTERADA OUT NUMBER )
  AS
    vCODIGO_FAVORECIDO NUMBER;
  BEGIN
    SELECT CODIGO_FAVORECIDO
    INTO vCODIGO_FAVORECIDO
    FROM TB_CONTA_FAVORECIDO
    WHERE CODIGO_CONTA_FAVORECIDO = pCODIGO_CONTA_FAVORECIDO;
    UPDATE TB_TRANSACAO
    SET CODIGO_CONTA_FAVORECIDO  = pCODIGO_CONTA_FAVORECIDO
    WHERE CODIGO_FAVORECIDO      = vCODIGO_FAVORECIDO
    AND CODIGO_PRODUTO           = pCODIGO_PRODUTO
    AND REFERENCIA               = pREFERENCIA
    AND CODIGO_CONTA_FAVORECIDO <> pCODIGO_CONTA_FAVORECIDO
    AND DATA_REPASSE_CALCULADA   > sysdate()
    AND STATUS_TRANSACAO         < '2';
    pQTDE_ALTERADA              := SQL%ROWCOUNT;
    COMMIT;
  END PR_TRANSACAO_SUBSTITUI_CONTA;
  PROCEDURE PR_TRANSACAO_S(
      pCODIGO_TRANSACAO OUT NUMBER,
      pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
      pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
      pSTATUS_TRANSACAO             NUMBER   := NULL,
      pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
      pNSU_HOST                     VARCHAR2 := NULL,
      pDATA_TRANSACAO               DATE     := NULL,
      pTIPO_TRANSACAO               VARCHAR2 := NULL,
      pCODIGO_ARQUIVO               NUMBER   := NULL,
      pCODIGO_ARQUIVO2              NUMBER   := NULL,
      pCHAVE                        VARCHAR2 := NULL,
      pCHAVE2                       VARCHAR2 := NULL,
      pCHAVE3                       VARCHAR2 := NULL,
      pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
      pCODIGO_PROCESSO              NUMBER   := NULL,
      pSTATUS_RET_CESSAO            NUMBER   := NULL,
      pCODIGO_PROCESSO_RET_CESSAO   NUMBER   := NULL,
      pCODIGO_ARQUIVO_RET_CESSAO    NUMBER   := NULL,
      pDATA_REPASSE                 DATE     := NULL,
      pCODIGO_PRODUTO               NUMBER   := NULL,
      pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
      pCODIGO_FAVORECIDO            NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
      pDATA_REPASSE_CALCULADA       DATE     := NULL,
      pVALOR_REPASSE                NUMBER   := NULL,
      pDATA_MOVIMENTO               DATE     := NULL,
      pFL_ATUALIZA_TRANSACAO        CHAR     := 'N',
      pTIPO_FINANCEIRO_CONTABIL     CHAR     := NULL,
      pVALOR_BRUTO                  NUMBER   := NULL,
      pVALOR_DESCONTO               NUMBER   := NULL,
      pVALOR_LIQUIDO                NUMBER   := NULL,
      pREFERENCIA                   VARCHAR2 := NULL,
      retornarRegistro              CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vSTATUS_CONCILIACAO      CHAR;
    vDATA_CONCILIACAO        DATE;
    vCODIGO_FAVORECIDO       NUMBER := NULL;
    vCODIGO_CONTA_FAVORECIDO NUMBER := NULL;
    vRET_DT_REP_CALC         NUMBER;
    vDATA_REPASSE_CALCULADA  DATE;
    vRET_TRATAR_FAV          NUMBER;
    vVALOR_LIQUIDO           NUMBER;
    vretcur TP_REFCURSOR;
  BEGIN
    vSTATUS_CONCILIACAO  := '0';
    IF pCODIGO_TRANSACAO IS NULL AND pCHAVE IS NOT NULL THEN
      BEGIN
        SELECT CODIGO_TRANSACAO
        INTO pCODIGO_TRANSACAO
        FROM TB_TRANSACAO
        WHERE CHAVE = pCHAVE;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        pCODIGO_TRANSACAO := NULL;
      END;
    END IF;
    IF pVALOR_LIQUIDO IS NULL OR pVALOR_LIQUIDO = 0 THEN
      vVALOR_LIQUIDO  := pVALOR_BRUTO + NVL(pVALOR_DESCONTO, 0);
    ELSE
      vVALOR_LIQUIDO := pVALOR_LIQUIDO;
    END IF;
    IF pCODIGO_TRANSACAO IS NULL THEN
      SELECT SQ_TRANSACAO.nextval INTO pCODIGO_TRANSACAO FROM dual;
      vCODIGO_CONTA_FAVORECIDO := 0;
      vCODIGO_FAVORECIDO       := 0;
      vRET_TRATAR_FAV          := 0;
      PR_TRATAR_FAVORECIDO ( pCODIGO_ESTABELECIMENTO => pCODIGO_ESTABELECIMENTO, pCODIGO_PRODUTO => pCODIGO_PRODUTO, pREFERENCIA => pREFERENCIA, pCODIGO_CONTA_FAVORECIDO => vCODIGO_CONTA_FAVORECIDO, pCODIGO_FAVORECIDO => vCODIGO_FAVORECIDO, pRETORNO => vRET_TRATAR_FAV, retcur => vretcur );
      vDATA_REPASSE_CALCULADA := pDATA_REPASSE;
      vRET_DT_REP_CALC        := 0;
      PR_TRATAR_DT_REP_CALC( pDATA_REPASSE => pDATA_REPASSE, pCODIGO_PROCESSO => pCODIGO_PROCESSO, pCODIGO_PRODUTO => pCODIGO_PRODUTO, pTIPO_TRANSACAO => pTIPO_TRANSACAO, pDATA_TRANSACAO => pDATA_TRANSACAO, pDATA_PROCESSAMENTO => pDATA_MOVIMENTO, pDATA_REPASSE_CALCULADA => vDATA_REPASSE_CALCULADA, pRETORNO => vRET_DT_REP_CALC );
      INSERT
      INTO TB_TRANSACAO
        (
          CODIGO_TRANSACAO,
          CODIGO_ARQUIVO_ITEM_1,
          CODIGO_ARQUIVO_ITEM_2,
          STATUS_TRANSACAO,
          CODIGO_ARQUIVO,
          CODIGO_ARQUIVO2,
          CODIGO_ESTABELECIMENTO,
          NSU_HOST,
          DATA_TRANSACAO,
          TIPO_TRANSACAO,
          DATA_INCLUSAO,
          CHAVE,
          CHAVE2,
          CHAVE3,
          CODIGO_PAGAMENTO,
          STATUS_CONCILIACAO,
          CODIGO_PROCESSO,
          STATUS_RET_CESSAO,
          CODIGO_PROCESSO_RET_CESSAO,
          CODIGO_ARQUIVO_RET_CESSAO,
          DATA_REPASSE,
          CODIGO_PRODUTO,
          CODIGO_FAVORECIDO_ORIGINAL,
          CODIGO_FAVORECIDO,
          CODIGO_CONTA_FAVORECIDO_ORIG,
          CODIGO_CONTA_FAVORECIDO,
          DATA_REPASSE_CALCULADA,
          VALOR_REPASSE,
          DATA_CONCILIACAO,
          DATA_MOVIMENTO,
          TIPO_FINANCEIRO_CONTABIL,
          VALOR_BRUTO,
          VALOR_DESCONTO,
          VALOR_LIQUIDO,
          REFERENCIA
        )
        VALUES
        (
          pCODIGO_TRANSACAO,
          pCODIGO_ARQUIVO_ITEM_1,
          pCODIGO_ARQUIVO_ITEM_2,
          pSTATUS_TRANSACAO,
          pCODIGO_ARQUIVO,
          pCODIGO_ARQUIVO2,
          pCODIGO_ESTABELECIMENTO,
          pNSU_HOST,
          pDATA_TRANSACAO,
          pTIPO_TRANSACAO,
          sysdate(),
          pCHAVE,
          pCHAVE2,
          pCHAVE3,
          pCODIGO_PAGAMENTO,
          vSTATUS_CONCILIACAO,
          pCODIGO_PROCESSO,
          pSTATUS_RET_CESSAO,
          pCODIGO_PROCESSO_RET_CESSAO,
          pCODIGO_ARQUIVO_RET_CESSAO,
          pDATA_REPASSE,
          pCODIGO_PRODUTO,
          VCODIGO_FAVORECIDO,
          VCODIGO_FAVORECIDO,
          VCODIGO_CONTA_FAVORECIDO,
          VCODIGO_CONTA_FAVORECIDO,
          VDATA_REPASSE_CALCULADA,
          pVALOR_REPASSE,
          vDATA_CONCILIACAO,
          pDATA_MOVIMENTO,
          pTIPO_FINANCEIRO_CONTABIL,
          pVALOR_BRUTO,
          pVALOR_DESCONTO,
          vVALOR_LIQUIDO,
          pREFERENCIA
        );
    ELSE
      IF (pFL_ATUALIZA_TRANSACAO = 'S') THEN
        UPDATE TB_TRANSACAO
        SET CODIGO_ARQUIVO_ITEM_1      = NVL(pCODIGO_ARQUIVO_ITEM_1, CODIGO_ARQUIVO_ITEM_1),
          CODIGO_ARQUIVO_ITEM_2        = NVL(pCODIGO_ARQUIVO_ITEM_2, CODIGO_ARQUIVO_ITEM_2),
          STATUS_TRANSACAO             = NVL(pSTATUS_TRANSACAO, STATUS_TRANSACAO),
          CODIGO_ARQUIVO               = NVL(pCODIGO_ARQUIVO, CODIGO_ARQUIVO),
          CODIGO_ARQUIVO2              = NVL(pCODIGO_ARQUIVO2, CODIGO_ARQUIVO2),
          CODIGO_ESTABELECIMENTO       = NVL(pCODIGO_ESTABELECIMENTO, CODIGO_ESTABELECIMENTO),
          NSU_HOST                     = NVL(pNSU_HOST, NSU_HOST),
          DATA_TRANSACAO               = NVL(pDATA_TRANSACAO, DATA_TRANSACAO),
          TIPO_TRANSACAO               = NVL(pTIPO_TRANSACAO, TIPO_TRANSACAO),
          CHAVE                        = NVL(pCHAVE, CHAVE),
          CHAVE2                       = NVL(pCHAVE2, CHAVE2),
          CHAVE3                       = NVL(pCHAVE3, CHAVE3),
          CODIGO_PAGAMENTO             = NVL(pCODIGO_PAGAMENTO, CODIGO_PAGAMENTO),
          CODIGO_PROCESSO              = NVL(pCODIGO_PROCESSO, CODIGO_PROCESSO),
          STATUS_RET_CESSAO            = NVL(pSTATUS_RET_CESSAO, STATUS_RET_CESSAO),
          CODIGO_PROCESSO_RET_CESSAO   = NVL(pCODIGO_PROCESSO_RET_CESSAO, CODIGO_PROCESSO_RET_CESSAO),
          CODIGO_ARQUIVO_RET_CESSAO    = NVL(pCODIGO_ARQUIVO_RET_CESSAO, CODIGO_ARQUIVO_RET_CESSAO),
          DATA_REPASSE                 = NVL(pDATA_REPASSE, DATA_REPASSE),
          DATA_REPASSE_CALCULADA       = NVL(pDATA_REPASSE_CALCULADA, DATA_REPASSE_CALCULADA),
          VALOR_REPASSE                = NVL(pVALOR_REPASSE, VALOR_REPASSE),
          CODIGO_PRODUTO               = NVL(pCODIGO_PRODUTO, CODIGO_PRODUTO),
          CODIGO_FAVORECIDO_ORIGINAL   = NVL(pCODIGO_FAVORECIDO_ORIGINAL, CODIGO_FAVORECIDO_ORIGINAL),
          CODIGO_FAVORECIDO            = NVL(pCODIGO_FAVORECIDO, CODIGO_FAVORECIDO),
          CODIGO_CONTA_FAVORECIDO_ORIG = NVL(pCODIGO_CONTA_FAVORECIDO_ORIG, CODIGO_CONTA_FAVORECIDO_ORIG),
          CODIGO_CONTA_FAVORECIDO      = NVL(pCODIGO_CONTA_FAVORECIDO,CODIGO_CONTA_FAVORECIDO),
          CODIGO_TRANSACAO             = pCODIGO_TRANSACAO,
          DATA_MOVIMENTO               = NVL(pDATA_MOVIMENTO, DATA_MOVIMENTO),
          TIPO_FINANCEIRO_CONTABIL     = NVL(pTIPO_FINANCEIRO_CONTABIL, TIPO_FINANCEIRO_CONTABIL),
          VALOR_BRUTO                  = NVL(pVALOR_BRUTO, VALOR_BRUTO),
          VALOR_DESCONTO               = NVL(pVALOR_DESCONTO, VALOR_DESCONTO),
          VALOR_LIQUIDO                = NVL(vVALOR_LIQUIDO, VALOR_LIQUIDO),
          STATUS_CONCILIACAO           =
          CASE
            WHEN NVL(pCODIGO_ARQUIVO_ITEM_1, CODIGO_ARQUIVO_ITEM_1) IS NOT NULL
            AND NVL(pCODIGO_ARQUIVO_ITEM_2, CODIGO_ARQUIVO_ITEM_2)  IS NOT NULL
            THEN '1'
            ELSE '0'
          END,
          DATA_CONCILIACAO =
          CASE
            WHEN NVL(pCODIGO_ARQUIVO_ITEM_1, CODIGO_ARQUIVO_ITEM_1) IS NOT NULL
            AND NVL(pCODIGO_ARQUIVO_ITEM_2, CODIGO_ARQUIVO_ITEM_2)  IS NOT NULL
            THEN sysdate()
            ELSE NULL
          END,
          REFERENCIA           = NVL(pREFERENCIA, REFERENCIA)
        WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
      END IF;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_TRANSACAO_B ( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, retcur => retcur );
    END IF;
  END PR_TRANSACAO_S;
  PROCEDURE PR_TRANSACAO_STATUS_S(
      pCODIGO_ARQUIVO_ITEM NUMBER,
      pSTATUS_TRANSACAO    NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    UPDATE tb_transacao
    SET status_transacao        = pSTATUS_TRANSACAO
    WHERE codigo_arquivo_item_1 = pCODIGO_ARQUIVO_ITEM;
  END PR_TRANSACAO_STATUS_S;
  PROCEDURE PR_TRANSACAO_B(
      pCODIGO_TRANSACAO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_TRANSACAO ,
    A.CODIGO_ARQUIVO_ITEM_1 ,
    A.CODIGO_ARQUIVO_ITEM_2 ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ESTABELECIMENTO ,
    A.DATA_INCLUSAO ,
    A.NSU_HOST ,
    A.DATA_TRANSACAO ,
    A.TIPO_TRANSACAO ,
    A.STATUS_TRANSACAO ,
    A.CHAVE ,
    A.CHAVE2 ,
    A.CODIGO_PAGAMENTO ,
    A.STATUS_CONCILIACAO ,
    A.CODIGO_PROCESSO ,
    A.DATA_REPASSE ,
    A.CODIGO_PRODUTO ,
    A.CODIGO_FAVORECIDO_ORIGINAL ,
    A.CODIGO_FAVORECIDO ,
    A.CODIGO_CONTA_FAVORECIDO_ORIG ,
    A.CODIGO_CONTA_FAVORECIDO ,
    A.VALOR_REPASSE ,
    A.DATA_MOVIMENTO ,
    A.REFERENCIA 
    FROM TB_TRANSACAO A WHERE A.CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_B;
  PROCEDURE PR_TRANSACAO_CHAVE_L(
      PCHAVE_LIKE VARCHAR2 := NULL,
      RETCUR OUT TP_REFCURSOR,
      retcurCount OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN RETCUR FOR SELECT a.CHAVE FROM TB_TRANSACAO a WHERE PCHAVE_LIKE     IS NULL OR a.CHAVE LIKE PCHAVE_LIKE;
    OPEN RETCURCOUNT FOR SELECT ROWNUM FROM TB_TRANSACAO a WHERE PCHAVE_LIKE IS NULL OR a.CHAVE LIKE PCHAVE_LIKE;
  END PR_TRANSACAO_CHAVE_L;
  PROCEDURE PR_TRANSACAO_L(
      pFILTRO_CODIGO_FAVORECIDO  NUMBER   := NULL,
      pCODIGO_ARQUIVO            NUMBER   := NULL,
      pTIPO_TRANSACAO            VARCHAR2 := NULL,
      pDATA_INCLUSAO             DATE     := NULL,
      pDATA_INCLUSAO_MAIOR       DATE     := NULL,
      pDATA_INCLUSAO_MENOR       DATE     := NULL,
      pCODIGO_ARQUIVO_RET_CESSAO NUMBER   := NULL,
      pSTATUS_RET_CESSAO         NUMBER   := NULL,
      pNSU_HOST                  VARCHAR2 := NULL,
      pCHAVE                     VARCHAR2 := NULL,
      pCHAVE_LIKE                VARCHAR2 := NULL,
      pCHAVE2                    VARCHAR2 := NULL,
      pFILTRO_RETORNO_CESSAO     CHAR     := 'N',
      pCODIGO_PROCESSO           NUMBER   := NULL,
      pDATA_MOVIMENTO_MAIOR      DATE     := NULL,
      pDATA_MOVIMENTO_MENOR      DATE     := NULL,
      pCODIGO_ESTABELECIMENTO    NUMBER   := NULL,
      pREFERENCIA                VARCHAR2 := NULL, 
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0)
  AS
    vQuery VARCHAR2(3000);
    vLogar CHAR;
  BEGIN
    vQuery := '';
    vLogar := 'N';
    IF pFILTRO_CODIGO_FAVORECIDO IS NOT NULL THEN
      vQuery                     := vQuery || 'A.CODIGO_FAVORECIDO = ''' || pFILTRO_CODIGO_FAVORECIDO || ''' and ';
      vQuery                     := vQuery || 'A.CODIGO_FAVORECIDO_ORIGINAL = ''' || pFILTRO_CODIGO_FAVORECIDO || ''' and ';
    END IF;
    IF pCODIGO_ARQUIVO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_ARQUIVO = ''' || pCODIGO_ARQUIVO || ''' and ';
    END IF;
    IF pTIPO_TRANSACAO IS NOT NULL THEN
      vQuery           := vQuery || 'A.TIPO_TRANSACAO = ''' || pTIPO_TRANSACAO || ''' and ';
    END IF;
    IF pNSU_HOST IS NOT NULL THEN
      vQuery     := vQuery || 'A.NSU_HOST = ''' || pNSU_HOST || ''' and ';
    END IF;
    IF pDATA_INCLUSAO IS NOT NULL THEN
      vLogar := 'S';
      vQuery := vQuery || 'A.DATA_INCLUSAO = ''' || pDATA_INCLUSAO || ''' and '; 
    END IF;
    IF pDATA_INCLUSAO_MAIOR IS NOT NULL THEN
      vLogar := 'S';
      vQuery := vQuery || 'A.DATA_INCLUSAO >= ''' || pDATA_INCLUSAO_MAIOR || ''' and '; 
    END IF;
    IF pDATA_INCLUSAO_MENOR IS NOT NULL THEN
      vLogar := 'S';
      vQuery := vQuery || 'A.DATA_INCLUSAO <= ''' || pDATA_INCLUSAO_MENOR || ''' and '; 
    END IF;
    IF pCHAVE IS NOT NULL THEN
      vQuery  := vQuery || 'A.CHAVE = ''' || pCHAVE || ''' and ';
    END IF;
    IF pCHAVE_LIKE IS NOT NULL THEN
      vQuery       := vQuery || 'A.CHAVE like ''' || pCHAVE_LIKE || ''' and ';
    END IF;
    IF pCHAVE2 IS NOT NULL THEN
      vQuery   := vQuery || 'A.CHAVE2 = ''' || pCHAVE2 || ''' and ';
    END IF;
    IF pSTATUS_RET_CESSAO IS NOT NULL THEN
      vQuery              := vQuery || 'A.STATUS_RET_CESSAO = ''' || pSTATUS_RET_CESSAO || ''' and ';
    END IF;
    IF pCODIGO_ARQUIVO_RET_CESSAO IS NOT NULL THEN
      vQuery                      := vQuery || 'A.CODIGO_ARQUIVO_RET_CESSAO = ''' || pCODIGO_ARQUIVO_RET_CESSAO || ''' and ';
    END IF;
    IF pCODIGO_ESTABELECIMENTO IS NOT NULL THEN
      vQuery                   := vQuery || 'A.CODIGO_ESTABELECIMENTO = ''' || pCODIGO_ESTABELECIMENTO || ''' and ';
    END IF;
    IF pCODIGO_PROCESSO IS NOT NULL THEN
      vQuery            := vQuery || 'A.CODIGO_PROCESSO = ''' || pCODIGO_PROCESSO || ''' and ';
    END IF;
    IF pDATA_MOVIMENTO_MAIOR IS NOT NULL THEN
      vLogar                 := 'S';
      vQuery                 := vQuery || 'A.DATA_MOVIMENTO >= ''' || pDATA_MOVIMENTO_MAIOR || ''' and '; 
    END IF;
    IF pDATA_MOVIMENTO_MENOR IS NOT NULL THEN
      vLogar                 := 'S';
      vQuery                 := vQuery || 'A.DATA_MOVIMENTO <= ''' || pDATA_MOVIMENTO_MENOR || ''' and '; 
    END IF;
    IF pREFERENCIA IS NOT NULL THEN
      vLogar       := 'S';
      vQuery       := vQuery || 'A.pREFERENCIA <= ''' || pREFERENCIA || ''' and '; 
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery :=
    'select * from (                   
select A.CODIGO_TRANSACAO,                          
A.CODIGO_ARQUIVO_ITEM_1,                          
A.CODIGO_ARQUIVO_ITEM_2,                          
A.CODIGO_ARQUIVO,                          
A.CODIGO_ARQUIVO2,                          
A.CODIGO_ESTABELECIMENTO,                          
A.DATA_INCLUSAO,                          
A.NSU_HOST,                          
A.DATA_TRANSACAO,                          
A.TIPO_TRANSACAO,                          
A.STATUS_TRANSACAO,                          
A.CHAVE,                          
A.CHAVE2,                          
A.CODIGO_PAGAMENTO,                          
A.STATUS_CONCILIACAO,                          
A.STATUS_VALIDACAO,                          
A.CODIGO_PROCESSO,                          
A.STATUS_RET_CESSAO,                          
A.CODIGO_PROCESSO_RET_CESSAO,                          
A.CODIGO_ARQUIVO_RET_CESSAO,                          
A.DATA_REPASSE,                          
A.DATA_REPASSE_CALCULADA,                          
A.CODIGO_PRODUTO,                          
A.CODIGO_FAVORECIDO_ORIGINAL,                          
A.CODIGO_FAVORECIDO,                          
A.CODIGO_CONTA_FAVORECIDO_ORIG,                          
A.CODIGO_CONTA_FAVORECIDO,                          
A.VALOR_REPASSE,                          
A.DATA_MOVIMENTO,                          
A.DATA_INCLUSAO as DATA_PROCESSAMENTO,                          
A.REFERENCIA                     
from TB_TRANSACAO A '
    || vQuery || ' order by A.CODIGO_TRANSACAO)';
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' where rownum <= ' || maxLinhas;
    END IF;
    OPEN retcur FOR vQuery;
  END PR_TRANSACAO_L;
  PROCEDURE PR_TRANSACAO_R(
      pCODIGO_TRANSACAO NUMBER)
  AS
  BEGIN
    DELETE TB_TRANSACAO
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_R;
  PROCEDURE PR_TRANSACAO_AJUSTE_S(
      pCODIGO_TRANSACAO OUT NUMBER,
      pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
      pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
      pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
      pNSU_HOST                     VARCHAR2 := NULL,
      pDATA_TRANSACAO               DATE     := NULL,
      pCODIGO_ARQUIVO               NUMBER   := NULL,
      pCODIGO_ARQUIVO2              NUMBER   := NULL,
      pCHAVE                        VARCHAR2 := NULL,
      pCHAVE2                       VARCHAR2 := NULL,
      pCHAVE3                       VARCHAR2 := NULL,
      pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
      pCODIGO_PROCESSO              NUMBER   := NULL,
      pDATA_TRANSACAO_ORIGINAL      DATE     := NULL,
      pTIPO_LANCAMENTO              VARCHAR2 := NULL,
      pDATA_REPASSE                 DATE     := NULL,
      pTIPO_AJUSTE                  VARCHAR2 := NULL,
      pCODIGO_AJUSTE                VARCHAR2 := NULL,
      pMOTIVO_AJUSTE                VARCHAR2 := NULL,
      pVALOR_AJUSTE                 NUMBER   := NULL,
      pVALOR_DESCONTO               NUMBER   := NULL,
      pVALOR_LIQUIDO                NUMBER   := NULL,
      pBANCO                        VARCHAR2 := NULL,
      pAGENCIA                      VARCHAR2 := NULL,
      pCONTA                        VARCHAR2 := NULL,
      pNSU_HOST_ORIGINAL            VARCHAR2 := NULL,
      pCODIGO_PRODUTO               NUMBER   := NULL,
      pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
      pCODIGO_FAVORECIDO            NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
      pDATA_REPASSE_CALCULADA       DATE     := NULL,
      pVALOR_REPASSE                NUMBER   := NULL,
      pSTATUS_RET_CESSAO            NUMBER   := NULL,
      pSTATUS_TRANSACAO             NUMBER   := NULL,
      pDATA_MOVIMENTO               DATE     := NULL,
      pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
      pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
      pNUM_CONTA_CLIENTE        VARCHAR2        := NULL,
      pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
      pNUMERO_CARTAO            VARCHAR2        := NULL,
      pREFERENCIA      VARCHAR2 := NULL, 
      retornarRegistro CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    qtde NUMBER;
    retcur_temp TP_REFCURSOR;
    vVALOR_AJUSTE   NUMBER; 
    vVALOR_DESCONTO NUMBER; 
    vVALOR_LIQUIDO  NUMBER; 
  BEGIN
    PR_TRANSACAO_S ( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, pCODIGO_ARQUIVO_ITEM_1 => pCODIGO_ARQUIVO_ITEM_1, pCODIGO_ARQUIVO_ITEM_2 => pCODIGO_ARQUIVO_ITEM_2, pSTATUS_TRANSACAO => pSTATUS_TRANSACAO, pCODIGO_ESTABELECIMENTO => pCODIGO_ESTABELECIMENTO, pNSU_HOST => pNSU_HOST, pDATA_TRANSACAO => pDATA_TRANSACAO, pCODIGO_ARQUIVO => pCODIGO_ARQUIVO, pCODIGO_ARQUIVO2 => pCODIGO_ARQUIVO2, pTIPO_TRANSACAO => 'AJ', pCHAVE => pCHAVE, pCHAVE2 => pCHAVE2, pCHAVE3 => pCHAVE3, pCODIGO_PAGAMENTO => pCODIGO_PAGAMENTO, pCODIGO_PROCESSO => pCODIGO_PROCESSO, pDATA_REPASSE => pDATA_REPASSE, pCODIGO_PRODUTO => pCODIGO_PRODUTO, pCODIGO_FAVORECIDO_ORIGINAL => pCODIGO_FAVORECIDO_ORIGINAL, pCODIGO_FAVORECIDO => pCODIGO_FAVORECIDO, pCODIGO_CONTA_FAVORECIDO_ORIG => pCODIGO_CONTA_FAVORECIDO_ORIG, pCODIGO_CONTA_FAVORECIDO => pCODIGO_CONTA_FAVORECIDO, pDATA_REPASSE_CALCULADA => pDATA_REPASSE_CALCULADA, pVALOR_REPASSE => pVALOR_REPASSE, pSTATUS_RET_CESSAO => pSTATUS_RET_CESSAO, pDATA_MOVIMENTO => pDATA_MOVIMENTO
    , pFL_ATUALIZA_TRANSACAO => pFL_ATUALIZA_TRANSACAO, pTIPO_FINANCEIRO_CONTABIL => pTIPO_FINANCEIRO_CONTABIL, pVALOR_BRUTO => pVALOR_AJUSTE, 
    pVALOR_DESCONTO => pVALOR_DESCONTO,                                                                                                        
    pVALOR_LIQUIDO => pVALOR_LIQUIDO,                                                                                                          
    pREFERENCIA => pREFERENCIA,                                                                                                                
    retornarRegistro => 'N', retcur => retcur_temp);
    vVALOR_AJUSTE           := NVL(pVALOR_AJUSTE,0);
    vVALOR_DESCONTO         := NVL(pVALOR_DESCONTO,0);
    vVALOR_LIQUIDO          := NVL(pVALOR_LIQUIDO,0);
    IF NVL(vVALOR_LIQUIDO,0) = 0 THEN
      vVALOR_LIQUIDO        := vVALOR_AJUSTE - vVALOR_DESCONTO;
    END IF;
    SELECT COUNT(1)
    INTO qtde
    FROM TB_TRANSACAO_AJUSTE
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_TRANSACAO_AJUSTE
        (
          CODIGO_TRANSACAO,
          NSU_HOST_ORIGINAL,
          DATA_TRANSACAO_ORIGINAL,
          TIPO_LANCAMENTO,
          TIPO_AJUSTE,
          CODIGO_AJUSTE,
          MOTIVO_AJUSTE,
          VALOR_AJUSTE,
          VALOR_DESCONTO,
          VALOR_LIQUIDO,
          BANCO,
          AGENCIA,
          CONTA,
          NUMERO_CONTA_CLIENTE,
          NUMERO_CARTAO
        )
        VALUES
        (
          pCODIGO_TRANSACAO,
          pNSU_HOST_ORIGINAL,
          pDATA_TRANSACAO_ORIGINAL,
          pTIPO_LANCAMENTO,
          pTIPO_AJUSTE,
          pCODIGO_AJUSTE,
          pMOTIVO_AJUSTE,
          vVALOR_AJUSTE,
          vVALOR_DESCONTO,
          vVALOR_LIQUIDO,
          pBANCO,
          pAGENCIA,
          pCONTA,
          pNUM_CONTA_CLIENTE,
          pNUMERO_CARTAO
        );
    ELSE
      IF (pFL_ATUALIZA_TRANSACAO = 'S') THEN
        UPDATE TB_TRANSACAO_AJUSTE
        SET NSU_HOST_ORIGINAL     = NVL(pNSU_HOST_ORIGINAL, NSU_HOST_ORIGINAL),
          DATA_TRANSACAO_ORIGINAL = NVL(pDATA_TRANSACAO_ORIGINAL, DATA_TRANSACAO_ORIGINAL),
          TIPO_LANCAMENTO         = NVL(pTIPO_LANCAMENTO, TIPO_LANCAMENTO),
          TIPO_AJUSTE             = NVL(pTIPO_AJUSTE, TIPO_AJUSTE),
          CODIGO_AJUSTE           = NVL(pCODIGO_AJUSTE, CODIGO_AJUSTE),
          MOTIVO_AJUSTE           = NVL(pMOTIVO_AJUSTE, MOTIVO_AJUSTE),
          VALOR_AJUSTE            = NVL(pVALOR_AJUSTE, VALOR_AJUSTE),
          VALOR_DESCONTO          = NVL(pVALOR_DESCONTO, VALOR_DESCONTO),
          VALOR_LIQUIDO           = NVL(vVALOR_LIQUIDO, VALOR_LIQUIDO), 
          BANCO                   = NVL(pBANCO, BANCO),
          AGENCIA                 = NVL(pAGENCIA, AGENCIA),
          CONTA                   = NVL(pCONTA, CONTA),
          NUMERO_CONTA_CLIENTE    = NVL(pNUM_CONTA_CLIENTE, NUMERO_CONTA_CLIENTE),
          NUMERO_CARTAO           = NVL(pNUMERO_CARTAO, NUMERO_CARTAO) 
        WHERE CODIGO_TRANSACAO    = pCODIGO_TRANSACAO;
      END IF;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_TRANSACAO_AJUSTE_B( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, retcur => retcur);
    END IF;
  END PR_TRANSACAO_AJUSTE_S;
  PROCEDURE PR_TRANSACAO_AJUSTE_B(
      pCODIGO_TRANSACAO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_TRANSACAO ,
    A.CODIGO_ARQUIVO_ITEM_1 ,
    A.CODIGO_ARQUIVO_ITEM_2 ,
    A.STATUS_TRANSACAO ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ESTABELECIMENTO ,
    A.DATA_INCLUSAO ,
    A.NSU_HOST ,
    A.DATA_TRANSACAO ,
    A.TIPO_TRANSACAO ,
    A.CHAVE ,
    A.CHAVE2 ,
    A.CODIGO_PAGAMENTO ,
    A.CODIGO_PROCESSO ,
    A.CODIGO_CONTA_FAVORECIDO_ORIG ,
    A.CODIGO_CONTA_FAVORECIDO ,
    B.NSU_HOST_ORIGINAL ,
    B.DATA_TRANSACAO_ORIGINAL ,
    B.TIPO_LANCAMENTO ,
    A.DATA_REPASSE ,
    B.TIPO_AJUSTE ,
    B.CODIGO_AJUSTE ,
    B.MOTIVO_AJUSTE ,
    B.VALOR_AJUSTE ,
    B.VALOR_DESCONTO ,
    B.VALOR_LIQUIDO ,
    B.BANCO ,
    B.AGENCIA ,
    B.CONTA ,
    A.DATA_MOVIMENTO FROM TB_TRANSACAO A inner join TB_TRANSACAO_AJUSTE B ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO WHERE A.CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_AJUSTE_B;
  PROCEDURE PR_TRANSACAO_AJUSTE_L(
      pCODIGO_ARQUIVO NUMBER := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery VARCHAR2(2000);
  BEGIN
    vQuery := '';
    IF pCODIGO_ARQUIVO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_ARQUIVO = ''' || pCODIGO_ARQUIVO || ''' and ';
    END IF;
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || 'rownum <= ' || maxLinhas || ' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery :=
    'select A.CODIGO_TRANSACAO,                        
A.CODIGO_ARQUIVO_ITEM_1,                        
A.CODIGO_ARQUIVO_ITEM_2,                        
A.STATUS_TRANSACAO,                        
A.CODIGO_ARQUIVO,                        
A.CODIGO_ESTABELECIMENTO,                        
A.DATA_INCLUSAO,                        
A.NSU_HOST,                        
A.DATA_TRANSACAO,                        
A.TIPO_TRANSACAO,                        
A.CHAVE,                        
A.CHAVE2,                        
A.CODIGO_PAGAMENTO,                        
A.CODIGO_PROCESSO,                        
A.CODIGO_FAVORECIDO_ORIGINAL,                        
A.CODIGO_FAVORECIDO,                        
A.CODIGO_CONTA_FAVORECIDO_ORIG,                        
A.CODIGO_CONTA_FAVORECIDO,                        
B.NSU_HOST_ORIGINAL,                        
B.DATA_TRANSACAO_ORIGINAL,                        
B.TIPO_LANCAMENTO,                        
A.DATA_REPASSE,                        
B.TIPO_AJUSTE,                        
B.CODIGO_AJUSTE,                        
B.MOTIVO_AJUSTE,                        
B.VALOR_AJUSTE,                        
B.VALOR_DESCONTO,                        
B.VALOR_LIQUIDO,                        
B.BANCO,                        
B.AGENCIA,                        
B.CONTA,                        
A.DATA_MOVIMENTO                   
from TB_TRANSACAO A             
inner join TB_TRANSACAO_AJUSTE B on                        
A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO '
    || vQuery;
    OPEN retcur FOR vQuery;
  END PR_TRANSACAO_AJUSTE_L;
  PROCEDURE PR_TRANSACAO_AJUSTE_R(
      pCODIGO_TRANSACAO NUMBER)
  AS
  BEGIN
    DELETE TB_TRANSACAO_AJUSTE
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_AJUSTE_R;
  PROCEDURE PR_TRANSACAO_ANULACAO_PAGTO_S(
      pCODIGO_TRANSACAO OUT NUMBER,
      pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
      pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
      pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
      pNSU_HOST                     VARCHAR2 := NULL,
      pDATA_TRANSACAO               DATE     := NULL,
      pCODIGO_ARQUIVO               NUMBER   := NULL,
      pCODIGO_ARQUIVO2              NUMBER   := NULL,
      pCHAVE                        VARCHAR2 := NULL,
      pCHAVE2                       VARCHAR2 := NULL,
      pCHAVE3                       VARCHAR2 := NULL,
      pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
      pCODIGO_PROCESSO              NUMBER   := NULL,
      pNSU_HOST_ORIGINAL            VARCHAR2 := NULL,
      pNSU_TEF_ORIGINAL             VARCHAR2 := NULL,
      pDATA_TRANSACAO_ORIGINAL      DATE     := NULL,
      pCODIGO_AUTORIZACAO_ORIGINAL  VARCHAR2 := NULL,
      pTIPO_LANCAMENTO              VARCHAR2 := NULL,
      pDATA_REEMBOLSO               DATE     := NULL,
      pFORMA_MEIO_PAGAMENTO         VARCHAR2 := NULL,
      pCODIGO_ANULACAO              VARCHAR2 := NULL,
      pMOTIVO_ANULACAO              VARCHAR2 := NULL,
      pVALOR_PAGAMENTO              NUMBER   := NULL,
      pVALOR_DESCONTO               NUMBER   := NULL,
      pVALOR_LIQUIDO                NUMBER   := NULL,
      pNUMERO_CARTAO                VARCHAR2 := NULL,
      pNUMERO_CONTA_CLIENTE         VARCHAR2 := NULL,
      pCODIGO_PRODUTO               NUMBER   := NULL,
      pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
      pCODIGO_FAVORECIDO            NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
      pDATA_REPASSE                 DATE     := NULL,
      pDATA_REPASSE_CALCULADA       DATE     := NULL,
      pVALOR_REPASSE                NUMBER   := NULL,
      pSTATUS_RET_CESSAO            NUMBER   := NULL,
      pSTATUS_TRANSACAO             NUMBER   := NULL,
      pDATA_MOVIMENTO               DATE     := NULL,
      pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
      pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
      pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
      pREFERENCIA               VARCHAR2        := NULL, 
      retornarRegistro          CHAR            := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    qtde NUMBER;
    retcur_temp TP_REFCURSOR;
    vVALOR_PAGAMENTO NUMBER; 
    vVALOR_DESCONTO  NUMBER; 
    vVALOR_LIQUIDO   NUMBER; 
  BEGIN
    PR_TRANSACAO_S ( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, pCODIGO_ARQUIVO_ITEM_1 => pCODIGO_ARQUIVO_ITEM_1, pCODIGO_ARQUIVO_ITEM_2 => pCODIGO_ARQUIVO_ITEM_2, pSTATUS_TRANSACAO => pSTATUS_TRANSACAO, pCODIGO_ESTABELECIMENTO => pCODIGO_ESTABELECIMENTO, pNSU_HOST => pNSU_HOST, pDATA_TRANSACAO => pDATA_TRANSACAO, pCODIGO_ARQUIVO => pCODIGO_ARQUIVO, pCODIGO_ARQUIVO2 => pCODIGO_ARQUIVO2, pTIPO_TRANSACAO => 'AP', pCHAVE => pCHAVE, pCHAVE2 => pCHAVE2, pCHAVE3 => pCHAVE3, pCODIGO_PAGAMENTO => pCODIGO_PAGAMENTO, pCODIGO_PROCESSO => pCODIGO_PROCESSO, pDATA_REPASSE => pDATA_REPASSE, pDATA_REPASSE_CALCULADA => pDATA_REPASSE_CALCULADA, pCODIGO_PRODUTO => pCODIGO_PRODUTO, pCODIGO_FAVORECIDO_ORIGINAL => pCODIGO_FAVORECIDO_ORIGINAL, pCODIGO_FAVORECIDO => pCODIGO_FAVORECIDO, pCODIGO_CONTA_FAVORECIDO_ORIG => pCODIGO_CONTA_FAVORECIDO_ORIG, pCODIGO_CONTA_FAVORECIDO => pCODIGO_CONTA_FAVORECIDO, pVALOR_REPASSE => pVALOR_REPASSE, pSTATUS_RET_CESSAO => pSTATUS_RET_CESSAO, pDATA_MOVIMENTO => pDATA_MOVIMENTO
    , pFL_ATUALIZA_TRANSACAO => pFL_ATUALIZA_TRANSACAO, pTIPO_FINANCEIRO_CONTABIL => pTIPO_FINANCEIRO_CONTABIL, pVALOR_BRUTO => pVALOR_PAGAMENTO, 
    pVALOR_DESCONTO => pVALOR_DESCONTO,                                                                                                           
    pVALOR_LIQUIDO => pVALOR_LIQUIDO,                                                                                                             
    pREFERENCIA => pREFERENCIA,                                                                                                                   
    retornarRegistro => 'N', retcur => retcur_temp);
    vVALOR_PAGAMENTO        := NVL(pVALOR_PAGAMENTO,0);
    vVALOR_DESCONTO         := NVL(pVALOR_DESCONTO,0);
    vVALOR_LIQUIDO          := NVL(pVALOR_LIQUIDO,0);
    IF NVL(vVALOR_LIQUIDO,0) = 0 THEN
      vVALOR_LIQUIDO        := vVALOR_PAGAMENTO - vVALOR_DESCONTO;
    END IF;
    SELECT COUNT(1)
    INTO qtde
    FROM TB_TRANSACAO_ANULACAO_PAGTO
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_TRANSACAO_ANULACAO_PAGTO
        (
          CODIGO_TRANSACAO,
          NSU_HOST_ORIGINAL,
          NSU_TEF_ORIGINAL,
          DATA_TRANSACAO_ORIGINAL,
          CODIGO_AUTORIZACAO_ORIGINAL,
          TIPO_LANCAMENTO,
          DATA_REEMBOLSO,
          FORMA_MEIO_PAGAMENTO,
          CODIGO_ANULACAO,
          MOTIVO_ANULACAO,
          VALOR_PAGAMENTO,
          VALOR_DESCONTO,
          VALOR_LIQUIDO,
          NUMERO_CARTAO,
          NUMERO_CONTA_CLIENTE
        )
        VALUES
        (
          pCODIGO_TRANSACAO,
          pNSU_HOST_ORIGINAL,
          pNSU_TEF_ORIGINAL,
          pDATA_TRANSACAO_ORIGINAL,
          pCODIGO_AUTORIZACAO_ORIGINAL,
          pTIPO_LANCAMENTO,
          pDATA_REEMBOLSO,
          pFORMA_MEIO_PAGAMENTO,
          pCODIGO_ANULACAO,
          pMOTIVO_ANULACAO,
          vVALOR_PAGAMENTO,
          vVALOR_DESCONTO,
          vVALOR_LIQUIDO,
          pNUMERO_CARTAO,
          pNUMERO_CONTA_CLIENTE
        );
    ELSE
      IF (pFL_ATUALIZA_TRANSACAO = 'S') THEN
        UPDATE TB_TRANSACAO_ANULACAO_PAGTO
        SET NSU_HOST_ORIGINAL         = NVL(pNSU_HOST_ORIGINAL, NSU_HOST_ORIGINAL),
          NSU_TEF_ORIGINAL            = NVL(pNSU_TEF_ORIGINAL, NSU_TEF_ORIGINAL),
          DATA_TRANSACAO_ORIGINAL     = NVL(pDATA_TRANSACAO_ORIGINAL, DATA_TRANSACAO_ORIGINAL),
          CODIGO_AUTORIZACAO_ORIGINAL = NVL(pCODIGO_AUTORIZACAO_ORIGINAL, CODIGO_AUTORIZACAO_ORIGINAL),
          TIPO_LANCAMENTO             = NVL(pTIPO_LANCAMENTO, TIPO_LANCAMENTO),
          DATA_REEMBOLSO              = NVL(pDATA_REEMBOLSO, DATA_REEMBOLSO),
          FORMA_MEIO_PAGAMENTO        = NVL(pFORMA_MEIO_PAGAMENTO, FORMA_MEIO_PAGAMENTO),
          CODIGO_ANULACAO             = NVL(pCODIGO_ANULACAO, CODIGO_ANULACAO),
          MOTIVO_ANULACAO             = NVL(pMOTIVO_ANULACAO, MOTIVO_ANULACAO),
          VALOR_PAGAMENTO             = NVL(pVALOR_PAGAMENTO, VALOR_PAGAMENTO),
          VALOR_DESCONTO              = NVL(pVALOR_DESCONTO, VALOR_DESCONTO),
          VALOR_LIQUIDO               = NVL(vVALOR_LIQUIDO, VALOR_LIQUIDO), 
          NUMERO_CARTAO               = NVL(pNUMERO_CARTAO, NUMERO_CARTAO),
          NUMERO_CONTA_CLIENTE        = NVL(pNUMERO_CONTA_CLIENTE, NUMERO_CONTA_CLIENTE)
        WHERE CODIGO_TRANSACAO        = pCODIGO_TRANSACAO;
      END IF;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_TRANSACAO_ANULACAO_PAGTO_B( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, retcur => retcur);
    END IF;
  END PR_TRANSACAO_ANULACAO_PAGTO_S;
  PROCEDURE PR_TRANSACAO_ANULACAO_PAGTO_B(
      pCODIGO_TRANSACAO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_TRANSACAO ,
    A.CODIGO_ARQUIVO_ITEM_1 ,
    A.CODIGO_ARQUIVO_ITEM_2 ,
    A.STATUS_TRANSACAO ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ESTABELECIMENTO ,
    A.DATA_INCLUSAO ,
    A.NSU_HOST ,
    A.DATA_TRANSACAO ,
    A.TIPO_TRANSACAO ,
    A.CHAVE ,
    A.CHAVE2 ,
    A.CODIGO_PAGAMENTO ,
    A.CODIGO_PROCESSO ,
    A.CODIGO_FAVORECIDO_ORIGINAL ,
    A.CODIGO_FAVORECIDO ,
    A.CODIGO_CONTA_FAVORECIDO_ORIG ,
    A.CODIGO_CONTA_FAVORECIDO ,
    B.NSU_HOST_ORIGINAL ,
    B.NSU_TEF_ORIGINAL ,
    B.DATA_TRANSACAO_ORIGINAL ,
    B.CODIGO_AUTORIZACAO_ORIGINAL ,
    B.TIPO_LANCAMENTO ,
    B.DATA_REEMBOLSO ,
    B.FORMA_MEIO_PAGAMENTO ,
    B.CODIGO_ANULACAO ,
    B.MOTIVO_ANULACAO ,
    B.VALOR_PAGAMENTO ,
    B.VALOR_DESCONTO ,
    B.VALOR_LIQUIDO ,
    B.NUMERO_CARTAO ,
    B.NUMERO_CONTA_CLIENTE ,
    A.DATA_MOVIMENTO FROM TB_TRANSACAO A inner join TB_TRANSACAO_ANULACAO_PAGTO B ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO WHERE A.CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_ANULACAO_PAGTO_B;
  PROCEDURE PR_TRANSACAO_ANULACAO_PAGTO_L(
      pCODIGO_ARQUIVO NUMBER := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery VARCHAR2(2000);
  BEGIN
    vQuery := '';
    IF pCODIGO_ARQUIVO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_ARQUIVO = ''' || pCODIGO_ARQUIVO || ''' and ';
    END IF;
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || 'rownum <= ' || maxLinhas || ' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery :=
    'select A.CODIGO_TRANSACAO,                        
A.CODIGO_ARQUIVO_ITEM_1,                        
A.CODIGO_ARQUIVO_ITEM_2,                        
A.STATUS_TRANSACAO,                        
A.CODIGO_ARQUIVO,                        
A.CODIGO_ESTABELECIMENTO,                        
A.DATA_INCLUSAO,                        
A.NSU_HOST,                        
A.DATA_TRANSACAO,                        
A.TIPO_TRANSACAO,                        
A.CHAVE,                        
A.CHAVE2,                        
A.CODIGO_PAGAMENTO,                        
A.CODIGO_PROCESSO,                        
A.CODIGO_FAVORECIDO_ORIGINAL,                        
A.CODIGO_FAVORECIDO,                        
A.CODIGO_CONTA_FAVORECIDO_ORIG,                        
A.CODIGO_CONTA_FAVORECIDO,                        
B.NSU_HOST_ORIGINAL,                        
B.NSU_TEF_ORIGINAL,                        
B.DATA_TRANSACAO_ORIGINAL,                        
B.CODIGO_AUTORIZACAO_ORIGINAL,                        
B.TIPO_LANCAMENTO,                        
B.DATA_REEMBOLSO,                        
B.FORMA_MEIO_PAGAMENTO,                        
B.CODIGO_ANULACAO,                        
B.MOTIVO_ANULACAO,                        
B.VALOR_PAGAMENTO,                        
B.VALOR_DESCONTO,                        
B.VALOR_LIQUIDO,                        
B.NUMERO_CARTAO,                        
B.NUMERO_CONTA_CLIENTE,                        
A.DATA_MOVIMENTO                   
from TB_TRANSACAO A             
inner join TB_TRANSACAO_ANULACAO_PAGTO B on                        
A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO '
    || vQuery;
    OPEN retcur FOR vQuery;
  END PR_TRANSACAO_ANULACAO_PAGTO_L;
  PROCEDURE PR_TRANSACAO_ANULACAO_PAGTO_R(
      pCODIGO_TRANSACAO NUMBER)
  AS
  BEGIN
    DELETE TB_TRANSACAO_ANULACAO_PAGTO
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_ANULACAO_PAGTO_R;
  PROCEDURE PR_TRANSACAO_ANULACAO_VENDA_S(
      pCODIGO_TRANSACAO OUT NUMBER,
      pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
      pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
      pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
      pNSU_HOST                     VARCHAR2 := NULL,
      pDATA_TRANSACAO               DATE     := NULL,
      pCODIGO_ARQUIVO               NUMBER   := NULL,
      pCODIGO_ARQUIVO2              NUMBER   := NULL,
      pCHAVE                        VARCHAR2 := NULL,
      pCHAVE2                       VARCHAR2 := NULL,
      pCHAVE3                       VARCHAR2 := NULL,
      pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
      pCODIGO_PROCESSO              NUMBER   := NULL,
      pNSU_HOST_ORIGINAL            VARCHAR2 := NULL,
      pNSU_TEF_ORIGINAL             VARCHAR2 := NULL,
      pDATA_TRANSACAO_ORIGINAL      DATE     := NULL,
      pCODIGO_AUTORIZACAO_ORIGINAL  VARCHAR2 := NULL,
      pTIPO_LANCAMENTO              VARCHAR2 := NULL,
      pDATA_REEMBOLSO               DATE     := NULL,
      pCODIGO_ANULACAO              VARCHAR2 := NULL,
      pMOTIVO_ANULACAO              VARCHAR2 := NULL,
      pVALOR_ANULACAO               NUMBER   := NULL,
      pVALOR_DESCONTO               NUMBER   := NULL,
      pVALOR_LIQUIDO                NUMBER   := NULL,
      pNUMERO_CARTAO                VARCHAR2 := NULL,
      pNUMERO_PARCELA               INT      := NULL,
      pNUMERO_PARCELA_TOTAL         INT      := NULL,
      pVALOR_PARCELA                NUMBER   := NULL,
      pVALOR_PARCELA_DESCONTO       NUMBER   := NULL,
      pVALOR_PARCELA_LIQUIDO        NUMBER   := NULL,
      pBANCO                        VARCHAR2 := NULL,
      pAGENCIA                      VARCHAR2 := NULL,
      pCONTA                        VARCHAR2 := NULL,
      pNUMERO_CONTA_CLIENTE         VARCHAR2 := NULL,
      pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
      pCODIGO_FAVORECIDO            NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
      pCODIGO_PRODUTO               NUMBER   := NULL,
      pDATA_REPASSE                 DATE     := NULL,
      pDATA_REPASSE_CALCULADA       DATE     := NULL,
      pVALOR_REPASSE                NUMBER   := NULL,
      pSTATUS_RET_CESSAO            NUMBER   := NULL,
      pSTATUS_TRANSACAO             NUMBER   := NULL,
      pDATA_MOVIMENTO               DATE     := NULL,
      pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
      pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
      pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
      pREFERENCIA               VARCHAR2        := NULL, 
      retornarRegistro          CHAR            := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    qtde NUMBER;
    retcur_temp TP_REFCURSOR;
    vVALOR_ANULACAO         NUMBER; 
    vVALOR_DESCONTO         NUMBER; 
    vVALOR_LIQUIDO          NUMBER; 
    vVALOR_PARCELA          NUMBER; 
    vVALOR_PARCELA_DESCONTO NUMBER; 
    vVALOR_PARCELA_LIQUIDO  NUMBER; 
    vVALOR_BRUTO            NUMBER; 
    vVALOR_DESC             NUMBER; 
    vVALOR_LIQ              NUMBER; 
  BEGIN
    IF (pVALOR_PARCELA <> 0) THEN
      vVALOR_BRUTO     := pVALOR_PARCELA;
      vVALOR_DESC      := pVALOR_PARCELA_DESCONTO;
      vVALOR_LIQ       := pVALOR_PARCELA_LIQUIDO;
    ELSE
      vVALOR_BRUTO := pVALOR_ANULACAO;
      vVALOR_DESC  := pVALOR_DESCONTO;
      vVALOR_LIQ   := pVALOR_LIQUIDO;
    END IF;
    PR_TRANSACAO_S ( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, pCODIGO_ARQUIVO_ITEM_1 => pCODIGO_ARQUIVO_ITEM_1, pCODIGO_ARQUIVO_ITEM_2 => pCODIGO_ARQUIVO_ITEM_2, pSTATUS_TRANSACAO => pSTATUS_TRANSACAO, pCODIGO_ESTABELECIMENTO => pCODIGO_ESTABELECIMENTO, pNSU_HOST => pNSU_HOST, pDATA_TRANSACAO => pDATA_TRANSACAO, pCODIGO_ARQUIVO => pCODIGO_ARQUIVO, pCODIGO_ARQUIVO2 => pCODIGO_ARQUIVO2, pTIPO_TRANSACAO => 'AV', pCHAVE => pCHAVE, pCHAVE2 => pCHAVE2, pCHAVE3 => pCHAVE3, pCODIGO_PAGAMENTO => pCODIGO_PAGAMENTO, pCODIGO_PROCESSO => pCODIGO_PROCESSO, pDATA_REPASSE => pDATA_REPASSE, pDATA_REPASSE_CALCULADA => pDATA_REPASSE_CALCULADA, pCODIGO_FAVORECIDO_ORIGINAL => pCODIGO_FAVORECIDO_ORIGINAL, pCODIGO_FAVORECIDO => pCODIGO_FAVORECIDO, pSTATUS_RET_CESSAO => pSTATUS_RET_CESSAO, pCODIGO_CONTA_FAVORECIDO_ORIG => pCODIGO_CONTA_FAVORECIDO_ORIG, pCODIGO_CONTA_FAVORECIDO => pCODIGO_CONTA_FAVORECIDO, pCODIGO_PRODUTO => pCODIGO_PRODUTO, pVALOR_REPASSE => pVALOR_REPASSE, pDATA_MOVIMENTO => pDATA_MOVIMENTO
    , pFL_ATUALIZA_TRANSACAO => pFL_ATUALIZA_TRANSACAO, pTIPO_FINANCEIRO_CONTABIL => pTIPO_FINANCEIRO_CONTABIL, pVALOR_BRUTO => vVALOR_BRUTO, 
    pVALOR_DESCONTO => vVALOR_DESC,                                                                                                           
    pVALOR_LIQUIDO => vVALOR_LIQ,                                                                                                             
    pREFERENCIA => pREFERENCIA,                                                                                                               
    retornarRegistro => 'N', retcur => retcur_temp);
    vVALOR_ANULACAO         := NVL(pVALOR_ANULACAO,0);
    vVALOR_DESCONTO         := NVL(pVALOR_DESCONTO,0);
    vVALOR_LIQUIDO          := NVL(pVALOR_LIQUIDO,0);
    vVALOR_PARCELA          := NVL(pVALOR_PARCELA,0);
    vVALOR_PARCELA_DESCONTO := NVL(pVALOR_PARCELA_DESCONTO,0);
    vVALOR_PARCELA_LIQUIDO  := NVL(pVALOR_PARCELA_LIQUIDO,0);
    IF NVL(vVALOR_LIQUIDO,0) = 0 THEN
      vVALOR_LIQUIDO        := vVALOR_ANULACAO - vVALOR_DESCONTO;
    END IF;
    IF NVL(vVALOR_PARCELA_LIQUIDO,0) = 0 THEN
      vVALOR_PARCELA_LIQUIDO        := vVALOR_PARCELA - vVALOR_PARCELA_DESCONTO;
    END IF;
    SELECT COUNT(1)
    INTO qtde
    FROM TB_TRANSACAO_ANULACAO_VENDA
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_TRANSACAO_ANULACAO_VENDA
        (
          CODIGO_TRANSACAO,
          NSU_HOST_ORIGINAL,
          NSU_TEF_ORIGINAL,
          DATA_TRANSACAO_ORIGINAL,
          CODIGO_AUTORIZACAO_ORIGINAL,
          TIPO_LANCAMENTO,
          DATA_REEMBOLSO,
          CODIGO_ANULACAO,
          MOTIVO_ANULACAO,
          VALOR_ANULACAO,
          VALOR_DESCONTO,
          VALOR_LIQUIDO,
          NUMERO_CARTAO,
          NUMERO_PARCELA,
          NUMERO_PARCELA_TOTAL,
          VALOR_PARCELA,
          VALOR_PARCELA_DESCONTO,
          VALOR_PARCELA_LIQUIDO,
          BANCO,
          AGENCIA,
          CONTA,
          NUMERO_CONTA_CLIENTE
        )
        VALUES
        (
          pCODIGO_TRANSACAO,
          pNSU_HOST_ORIGINAL,
          pNSU_TEF_ORIGINAL,
          pDATA_TRANSACAO_ORIGINAL,
          pCODIGO_AUTORIZACAO_ORIGINAL,
          pTIPO_LANCAMENTO,
          pDATA_REEMBOLSO,
          pCODIGO_ANULACAO,
          pMOTIVO_ANULACAO,
          vVALOR_ANULACAO,
          vVALOR_DESCONTO,
          vVALOR_LIQUIDO,
          pNUMERO_CARTAO,
          pNUMERO_PARCELA,
          pNUMERO_PARCELA_TOTAL,
          vVALOR_PARCELA,
          vVALOR_PARCELA_DESCONTO,
          vVALOR_PARCELA_LIQUIDO,
          pBANCO,
          pAGENCIA,
          pCONTA,
          pNUMERO_CONTA_CLIENTE
        );
    ELSE
      IF (pFL_ATUALIZA_TRANSACAO = 'S') THEN
        UPDATE TB_TRANSACAO_ANULACAO_VENDA
        SET NSU_HOST_ORIGINAL         = NVL(pNSU_HOST_ORIGINAL, NSU_HOST_ORIGINAL),
          NSU_TEF_ORIGINAL            = NVL(pNSU_TEF_ORIGINAL, NSU_TEF_ORIGINAL),
          DATA_TRANSACAO_ORIGINAL     = NVL(pDATA_TRANSACAO_ORIGINAL, DATA_TRANSACAO_ORIGINAL),
          CODIGO_AUTORIZACAO_ORIGINAL = NVL(pCODIGO_AUTORIZACAO_ORIGINAL, CODIGO_AUTORIZACAO_ORIGINAL),
          TIPO_LANCAMENTO             = NVL(pTIPO_LANCAMENTO, TIPO_LANCAMENTO),
          DATA_REEMBOLSO              = NVL(pDATA_REEMBOLSO, DATA_REEMBOLSO),
          CODIGO_ANULACAO             = NVL(pCODIGO_ANULACAO, CODIGO_ANULACAO),
          MOTIVO_ANULACAO             = NVL(pMOTIVO_ANULACAO, MOTIVO_ANULACAO),
          VALOR_ANULACAO              = NVL(pVALOR_ANULACAO, VALOR_ANULACAO),
          VALOR_DESCONTO              = NVL(pVALOR_DESCONTO, VALOR_DESCONTO),
          VALOR_LIQUIDO               = NVL(vVALOR_LIQUIDO, VALOR_LIQUIDO), 
          NUMERO_CARTAO               = NVL(pNUMERO_CARTAO, NUMERO_CARTAO),
          NUMERO_PARCELA              = NVL(pNUMERO_PARCELA, NUMERO_PARCELA),
          NUMERO_PARCELA_TOTAL        = NVL(pNUMERO_PARCELA_TOTAL, NUMERO_PARCELA_TOTAL),
          VALOR_PARCELA               = NVL(pVALOR_PARCELA, VALOR_PARCELA),
          VALOR_PARCELA_DESCONTO      = NVL(pVALOR_PARCELA_DESCONTO, VALOR_PARCELA_DESCONTO),
          VALOR_PARCELA_LIQUIDO       = NVL(vVALOR_PARCELA_LIQUIDO, VALOR_PARCELA_LIQUIDO), 
          BANCO                       = NVL(pBANCO, BANCO),
          AGENCIA                     = NVL(pAGENCIA, AGENCIA),
          CONTA                       = NVL(pCONTA, CONTA),
          NUMERO_CONTA_CLIENTE        = NVL(pNUMERO_CONTA_CLIENTE, NUMERO_CONTA_CLIENTE)
        WHERE CODIGO_TRANSACAO        = pCODIGO_TRANSACAO;
      END IF;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_TRANSACAO_ANULACAO_VENDA_B( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, retcur => retcur);
    END IF;
  END PR_TRANSACAO_ANULACAO_VENDA_S;
  PROCEDURE PR_TRANSACAO_ANULACAO_VENDA_B(
      pCODIGO_TRANSACAO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_TRANSACAO ,
    A.CODIGO_ARQUIVO_ITEM_1 ,
    A.CODIGO_ARQUIVO_ITEM_2 ,
    A.STATUS_TRANSACAO ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ESTABELECIMENTO ,
    A.DATA_INCLUSAO ,
    A.NSU_HOST ,
    A.DATA_TRANSACAO ,
    A.TIPO_TRANSACAO ,
    A.CHAVE ,
    A.CHAVE2 ,
    A.CODIGO_PAGAMENTO ,
    A.CODIGO_PROCESSO ,
    A.CODIGO_FAVORECIDO_ORIGINAL ,
    A.CODIGO_FAVORECIDO ,
    A.CODIGO_CONTA_FAVORECIDO_ORIG ,
    A.CODIGO_CONTA_FAVORECIDO ,
    B.NSU_HOST_ORIGINAL ,
    B.NSU_TEF_ORIGINAL ,
    B.DATA_TRANSACAO_ORIGINAL ,
    B.CODIGO_AUTORIZACAO_ORIGINAL ,
    B.TIPO_LANCAMENTO ,
    B.DATA_REEMBOLSO ,
    B.CODIGO_ANULACAO ,
    B.MOTIVO_ANULACAO ,
    B.VALOR_ANULACAO ,
    B.VALOR_DESCONTO ,
    B.VALOR_LIQUIDO ,
    B.NUMERO_CARTAO ,
    B.NUMERO_PARCELA ,
    B.NUMERO_PARCELA_TOTAL ,
    B.VALOR_PARCELA ,
    B.VALOR_PARCELA_DESCONTO ,
    B.VALOR_PARCELA_LIQUIDO ,
    B.BANCO ,
    B.AGENCIA ,
    B.CONTA ,
    B.NUMERO_CONTA_CLIENTE ,
    A.DATA_MOVIMENTO FROM TB_TRANSACAO A inner join TB_TRANSACAO_ANULACAO_VENDA B ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO WHERE A.CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_ANULACAO_VENDA_B;
  PROCEDURE PR_TRANSACAO_ANULACAO_VENDA_L(
      pCODIGO_ARQUIVO NUMBER := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery VARCHAR2(2000);
  BEGIN
    vQuery := '';
    IF pCODIGO_ARQUIVO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_ARQUIVO = ''' || pCODIGO_ARQUIVO || ''' and ';
    END IF;
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || 'rownum <= ' || maxLinhas || ' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery :=
    'select A.CODIGO_TRANSACAO,                        
A.CODIGO_ARQUIVO_ITEM_1,                        
A.CODIGO_ARQUIVO_ITEM_2,                        
A.STATUS_TRANSACAO,                        
A.CODIGO_ARQUIVO,                        
A.CODIGO_ESTABELECIMENTO,                        
A.DATA_INCLUSAO,                        
A.NSU_HOST,                        
A.DATA_TRANSACAO,                        
A.TIPO_TRANSACAO,                        
A.CHAVE,                        
A.CHAVE2,                        
A.CODIGO_PAGAMENTO,                        
A.CODIGO_PROCESSO,                        
A.CODIGO_FAVORECIDO_ORIGINAL,                        
A.CODIGO_FAVORECIDO,                        
A.CODIGO_CONTA_FAVORECIDO_ORIG,                        
A.CODIGO_CONTA_FAVORECIDO,                        
B.NSU_HOST_ORIGINAL,                        
B.NSU_TEF_ORIGINAL,                        
B.DATA_TRANSACAO_ORIGINAL,                        
B.CODIGO_AUTORIZACAO_ORIGINAL,                        
B.TIPO_LANCAMENTO,                        
B.DATA_REEMBOLSO,                        
B.CODIGO_ANULACAO,                        
B.MOTIVO_ANULACAO,                        
B.VALOR_ANULACAO,                        
B.VALOR_DESCONTO,                        
B.VALOR_LIQUIDO,                        
B.NUMERO_CARTAO,                        
B.NUMERO_PARCELA,                        
B.NUMERO_PARCELA_TOTAL,                        
B.VALOR_PARCELA,                        
B.VALOR_PARCELA_DESCONTO,                        
B.VALOR_PARCELA_LIQUIDO,                        
B.BANCO,                        
B.AGENCIA,                        
B.CONTA,                        
B.NUMERO_CONTA_CLIENTE,                        
A.DATA_MOVIMENTO                   
from TB_TRANSACAO A             
inner join TB_TRANSACAO_ANULACAO_VENDA B on                        
A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO '
    || vQuery;
    OPEN retcur FOR vQuery;
  END PR_TRANSACAO_ANULACAO_VENDA_L;
  PROCEDURE PR_TRANSACAO_ANULACAO_VENDA_R(
      pCODIGO_TRANSACAO NUMBER)
  AS
  BEGIN
    DELETE TB_TRANSACAO_ANULACAO_VENDA
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_ANULACAO_VENDA_R;
  PROCEDURE PR_TRANSACAO_PAGAMENTO_S(
      pCODIGO_TRANSACAO OUT NUMBER,
      pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
      pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
      pSTATUS_TRANSACAO             NUMBER   := NULL,
      pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
      pNSU_HOST                     VARCHAR2 := NULL,
      pDATA_TRANSACAO               DATE     := NULL,
      pCODIGO_ARQUIVO               NUMBER   := NULL,
      pCODIGO_ARQUIVO2              NUMBER   := NULL,
      pCHAVE                        VARCHAR2 := NULL,
      pCHAVE2                       VARCHAR2 := NULL,
      pCHAVE3                       VARCHAR2 := NULL,
      pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
      pCODIGO_PROCESSO              NUMBER   := NULL,
      pNSU_TEF                      VARCHAR2 := NULL,
      pCODIGO_AUTORIZACAO           VARCHAR2 := NULL,
      pTIPO_LANCAMENTO              VARCHAR2 := NULL,
      pDATA_REPASSE                 DATE     := NULL,
      pMEIO_CAPTURA                 VARCHAR2 := NULL,
      pVALOR_PAGAMENTO              NUMBER   := NULL,
      pVALOR_DESCONTO               NUMBER   := NULL,
      pVALOR_LIQUIDO                NUMBER   := NULL,
      pNUMERO_CARTAO                VARCHAR2 := NULL,
      pQUANTIDADE_MEIO_PAGAMENTO    INT      := NULL,
      pMEIO_PAGAMENTO               VARCHAR2 := NULL,
      pMEIO_PAGAMENTO_SEQ           INT      := NULL,
      pMEIO_PAGAMENTO_VALOR         NUMBER   := NULL,
      pBANCO                        VARCHAR2 := NULL,
      pAGENCIA                      VARCHAR2 := NULL,
      pCONTA                        VARCHAR2 := NULL,
      pNUMERO_CONTA_CLIENTE         VARCHAR2 := NULL,
      pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
      pCODIGO_FAVORECIDO            NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
      pCODIGO_PRODUTO               NUMBER   := NULL,
      pDATA_REPASSE_CALCULADA       DATE     := NULL,
      pVALOR_REPASSE                NUMBER   := NULL,
      pSTATUS_RET_CESSAO            NUMBER   := NULL,
      pDATA_MOVIMENTO               DATE     := NULL,
      pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
      pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
      pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
      pREFERENCIA               VARCHAR2        := NULL, 
      retornarRegistro          CHAR            := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    qtde NUMBER;
    retcur_temp TP_REFCURSOR;
    vVALOR_PAGAMENTO NUMBER; 
    vVALOR_DESCONTO  NUMBER; 
    vVALOR_LIQUIDO   NUMBER; 
  BEGIN
    IF (pMEIO_PAGAMENTO_VALOR <> 0) THEN
      vVALOR_PAGAMENTO        := pMEIO_PAGAMENTO_VALOR;
      vVALOR_DESCONTO         := 0;
      vVALOR_LIQUIDO          := pMEIO_PAGAMENTO_VALOR;
    ELSE
      IF NVL(vVALOR_LIQUIDO,0) = 0 THEN
        vVALOR_LIQUIDO        := vVALOR_PAGAMENTO + vVALOR_DESCONTO;
      END IF;
      vVALOR_PAGAMENTO := pVALOR_PAGAMENTO;
      vVALOR_DESCONTO  := pVALOR_DESCONTO;
      vVALOR_LIQUIDO   := pVALOR_LIQUIDO;
    END IF;
    PR_TRANSACAO_S ( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, pCODIGO_ARQUIVO_ITEM_1 => pCODIGO_ARQUIVO_ITEM_1, pCODIGO_ARQUIVO_ITEM_2 => pCODIGO_ARQUIVO_ITEM_2, pSTATUS_TRANSACAO => pSTATUS_TRANSACAO, pCODIGO_ESTABELECIMENTO => pCODIGO_ESTABELECIMENTO, pNSU_HOST => pNSU_HOST, pDATA_TRANSACAO => pDATA_TRANSACAO, pCODIGO_ARQUIVO => pCODIGO_ARQUIVO, pCODIGO_ARQUIVO2 => pCODIGO_ARQUIVO2, pTIPO_TRANSACAO => 'CP', pCHAVE => pCHAVE, pCHAVE2 => pCHAVE2, pCHAVE3 => pCHAVE3, pCODIGO_PAGAMENTO => pCODIGO_PAGAMENTO, pCODIGO_PROCESSO => pCODIGO_PROCESSO, pDATA_REPASSE => pDATA_REPASSE, pCODIGO_FAVORECIDO_ORIGINAL => pCODIGO_FAVORECIDO_ORIGINAL, pCODIGO_FAVORECIDO => pCODIGO_FAVORECIDO, pCODIGO_CONTA_FAVORECIDO_ORIG => pCODIGO_CONTA_FAVORECIDO_ORIG, pCODIGO_CONTA_FAVORECIDO => pCODIGO_CONTA_FAVORECIDO, pCODIGO_PRODUTO => pCODIGO_PRODUTO, pDATA_REPASSE_CALCULADA => pDATA_REPASSE_CALCULADA, pVALOR_REPASSE => pVALOR_REPASSE, pSTATUS_RET_CESSAO => pSTATUS_RET_CESSAO, pDATA_MOVIMENTO => pDATA_MOVIMENTO
    , pFL_ATUALIZA_TRANSACAO => pFL_ATUALIZA_TRANSACAO, pTIPO_FINANCEIRO_CONTABIL => pTIPO_FINANCEIRO_CONTABIL, pVALOR_BRUTO => vVALOR_PAGAMENTO, 
    pVALOR_DESCONTO => vVALOR_DESCONTO,                                                                                                           
    pVALOR_LIQUIDO => vVALOR_LIQUIDO,                                                                                                             
    pREFERENCIA => pREFERENCIA,                                                                                                                   
    retornarRegistro => 'N', retcur => retcur_temp);
    SELECT COUNT(1)
    INTO qtde
    FROM TB_TRANSACAO_PAGAMENTO
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_TRANSACAO_PAGAMENTO
        (
          CODIGO_TRANSACAO,
          NSU_TEF,
          CODIGO_AUTORIZACAO,
          TIPO_LANCAMENTO,
          MEIO_CAPTURA,
          VALOR_PAGAMENTO,
          VALOR_DESCONTO,
          VALOR_LIQUIDO,
          NUMERO_CARTAO,
          QUANTIDADE_MEIO_PAGAMENTO,
          MEIO_PAGAMENTO,
          MEIO_PAGAMENTO_SEQ,
          MEIO_PAGAMENTO_VALOR,
          BANCO,
          AGENCIA,
          CONTA,
          NUMERO_CONTA_CLIENTE
        )
        VALUES
        (
          pCODIGO_TRANSACAO,
          pNSU_TEF,
          pCODIGO_AUTORIZACAO,
          pTIPO_LANCAMENTO,
          pMEIO_CAPTURA,
          pVALOR_PAGAMENTO,
          pVALOR_DESCONTO,
          pVALOR_LIQUIDO,
          pNUMERO_CARTAO,
          pQUANTIDADE_MEIO_PAGAMENTO,
          pMEIO_PAGAMENTO,
          pMEIO_PAGAMENTO_SEQ,
          pMEIO_PAGAMENTO_VALOR,
          pBANCO,
          pAGENCIA,
          pCONTA,
          pNUMERO_CONTA_CLIENTE
        );
    ELSE
      IF (pFL_ATUALIZA_TRANSACAO = 'S') THEN
        UPDATE TB_TRANSACAO_PAGAMENTO
        SET NSU_TEF                 = NVL(pNSU_TEF, NSU_TEF),
          CODIGO_AUTORIZACAO        = NVL(pCODIGO_AUTORIZACAO, CODIGO_AUTORIZACAO),
          TIPO_LANCAMENTO           = NVL(pTIPO_LANCAMENTO, TIPO_LANCAMENTO),
          MEIO_CAPTURA              = NVL(pMEIO_CAPTURA, MEIO_CAPTURA),
          VALOR_PAGAMENTO           = NVL(pVALOR_PAGAMENTO, VALOR_PAGAMENTO),
          VALOR_DESCONTO            = NVL(pVALOR_DESCONTO, VALOR_DESCONTO),
          VALOR_LIQUIDO             = NVL(vVALOR_LIQUIDO, VALOR_LIQUIDO), 
          NUMERO_CARTAO             = NVL(pNUMERO_CARTAO, NUMERO_CARTAO),
          QUANTIDADE_MEIO_PAGAMENTO = NVL(pQUANTIDADE_MEIO_PAGAMENTO, QUANTIDADE_MEIO_PAGAMENTO),
          MEIO_PAGAMENTO            = NVL(pMEIO_PAGAMENTO, MEIO_PAGAMENTO),
          MEIO_PAGAMENTO_SEQ        = NVL(pMEIO_PAGAMENTO_SEQ, MEIO_PAGAMENTO_SEQ),
          MEIO_PAGAMENTO_VALOR      = NVL(pMEIO_PAGAMENTO_VALOR, MEIO_PAGAMENTO_VALOR),
          BANCO                     = NVL(pBANCO, BANCO),
          AGENCIA                   = NVL(pAGENCIA, AGENCIA),
          CONTA                     = NVL(pCONTA, CONTA),
          NUMERO_CONTA_CLIENTE      = NVL(pNUMERO_CONTA_CLIENTE, NUMERO_CONTA_CLIENTE)
        WHERE CODIGO_TRANSACAO      = pCODIGO_TRANSACAO;
      END IF;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_TRANSACAO_PAGAMENTO_B( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, retcur => retcur);
    END IF;
  END PR_TRANSACAO_PAGAMENTO_S;
  PROCEDURE PR_TRANSACAO_PAGAMENTO_B(
      pCODIGO_TRANSACAO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_TRANSACAO ,
    A.CODIGO_ARQUIVO_ITEM_1 ,
    A.CODIGO_ARQUIVO_ITEM_2 ,
    A.STATUS_TRANSACAO ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ESTABELECIMENTO ,
    A.DATA_INCLUSAO ,
    A.NSU_HOST ,
    A.DATA_TRANSACAO ,
    A.TIPO_TRANSACAO ,
    A.CHAVE ,
    A.CHAVE2 ,
    A.CODIGO_PAGAMENTO ,
    A.CODIGO_PROCESSO ,
    A.CODIGO_FAVORECIDO_ORIGINAL ,
    A.CODIGO_FAVORECIDO ,
    A.CODIGO_CONTA_FAVORECIDO_ORIG ,
    A.CODIGO_CONTA_FAVORECIDO ,
    B.NSU_TEF ,
    B.CODIGO_AUTORIZACAO ,
    B.TIPO_LANCAMENTO ,
    B.DATA_REPASSE ,
    B.MEIO_CAPTURA ,
    B.VALOR_PAGAMENTO ,
    B.VALOR_DESCONTO ,
    B.VALOR_LIQUIDO ,
    B.NUMERO_CARTAO ,
    B.QUANTIDADE_MEIO_PAGAMENTO ,
    B.MEIO_PAGAMENTO ,
    B.MEIO_PAGAMENTO_SEQ ,
    B.MEIO_PAGAMENTO_VALOR ,
    B.BANCO ,
    B.AGENCIA ,
    B.CONTA ,
    B.NUMERO_CONTA_CLIENTE ,
    A.DATA_MOVIMENTO FROM TB_TRANSACAO A inner join TB_TRANSACAO_PAGAMENTO B ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO WHERE A.CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_PAGAMENTO_B;
  PROCEDURE PR_TRANSACAO_PAGAMENTO_L(
      pCODIGO_ARQUIVO NUMBER := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery VARCHAR2(2000);
  BEGIN
    vQuery := '';
    IF pCODIGO_ARQUIVO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_ARQUIVO = ''' || pCODIGO_ARQUIVO || ''' and ';
    END IF;
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || 'rownum <= ' || maxLinhas || ' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery :=
    'select A.CODIGO_TRANSACAO,                        
A.CODIGO_ARQUIVO_ITEM_1,                        
A.CODIGO_ARQUIVO_ITEM_2,                        
A.STATUS_TRANSACAO,                        
A.CODIGO_ARQUIVO,                        
A.CODIGO_ESTABELECIMENTO,                        
A.DATA_INCLUSAO,                        
A.NSU_HOST,                        
A.DATA_TRANSACAO,                        
A.TIPO_TRANSACAO,                        
A.CHAVE,                        
A.CHAVE2,                        
A.CODIGO_PAGAMENTO,                        
A.CODIGO_PROCESSO,                        
A.CODIGO_FAVORECIDO_ORIGINAL,                        
A.CODIGO_FAVORECIDO,                        
A.CODIGO_CONTA_FAVORECIDO_ORIG,                        
A.CODIGO_CONTA_FAVORECIDO,                        
B.NSU_TEF,                        
B.CODIGO_AUTORIZACAO,                        
B.TIPO_LANCAMENTO,                        
A.DATA_REPASSE,                        
B.MEIO_CAPTURA,                        
B.VALOR_PAGAMENTO,                        
B.VALOR_DESCONTO,                        
B.VALOR_LIQUIDO,                        
B.NUMERO_CARTAO,                        
B.QUANTIDADE_MEIO_PAGAMENTO,                        
B.MEIO_PAGAMENTO,                        
B.MEIO_PAGAMENTO_SEQ,                        
B.MEIO_PAGAMENTO_VALOR,                        
B.BANCO,                        
B.AGENCIA,                        
B.CONTA,                        
B.NUMERO_CONTA_CLIENTE,                        
A.DATA_MOVIMENTO                   
from TB_TRANSACAO A             
inner join TB_TRANSACAO_PAGAMENTO B on                        
A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO '
    || vQuery;
    OPEN retcur FOR vQuery;
  END PR_TRANSACAO_PAGAMENTO_L;
  PROCEDURE PR_TRANSACAO_PAGAMENTO_R(
      pCODIGO_TRANSACAO NUMBER)
  AS
  BEGIN
    DELETE TB_TRANSACAO_PAGAMENTO
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_PAGAMENTO_R;
  PROCEDURE PR_TRANSACAO_VENDA_S(
      pCODIGO_TRANSACAO OUT NUMBER,
      pCODIGO_ARQUIVO_ITEM_1        NUMBER   := NULL,
      pCODIGO_ARQUIVO_ITEM_2        NUMBER   := NULL,
      pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
      pNSU_HOST                     VARCHAR2 := NULL,
      pDATA_TRANSACAO               DATE     := NULL,
      pCODIGO_ARQUIVO               NUMBER   := NULL,
      pCODIGO_ARQUIVO2              NUMBER   := NULL,
      pCHAVE                        VARCHAR2 := NULL,
      pCHAVE2                       VARCHAR2 := NULL,
      pCHAVE3                       VARCHAR2 := NULL,
      pCODIGO_PAGAMENTO             VARCHAR2 := NULL,
      pCODIGO_PROCESSO              NUMBER   := NULL,
      pNSU_TEF                      VARCHAR2 := NULL,
      pCODIGO_AUTORIZACAO           VARCHAR2 := NULL,
      pDATA_REPASSE                 DATE     := NULL,
      pTIPO_PRODUTO                 VARCHAR2 := NULL,
      pMEIO_CAPTURA                 VARCHAR2 := NULL,
      pVALOR_VENDA                  NUMBER   := NULL,
      pVALOR_DESCONTO               NUMBER   := NULL,
      pVALOR_LIQUIDO                NUMBER   := NULL,
      pNUMERO_CARTAO                VARCHAR2 := NULL,
      pNUMERO_PARCELA               INT      := NULL,
      pNUMERO_PARCELA_TOTAL         INT      := NULL,
      pVALOR_PARCELA                NUMBER   := NULL,
      pVALOR_PARCELA_DESCONTO       NUMBER   := NULL,
      pVALOR_PARCELA_LIQUIDO        NUMBER   := NULL,
      pBANCO                        VARCHAR2 := NULL,
      pAGENCIA                      VARCHAR2 := NULL,
      pCONTA                        VARCHAR2 := NULL,
      pNUMERO_CONTA_CLIENTE         VARCHAR2 := NULL,
      pCODIGO_PRODUTO               INT      := NULL,
      pCODIGO_PLANO                 INT      := NULL,
      pCUPOM_FISCAL                 VARCHAR2 := NULL,
      pMODALIDADE                   VARCHAR2 := NULL,
      pTIPO_LANCAMENTO              VARCHAR2 := NULL,
      pVALOR_COMISSAO               NUMBER   := NULL,
      pSTATUS_RET_CESSAO            NUMBER   := NULL,
      pCODIGO_PROCESSO_RET_CESSAO   NUMBER   := NULL,
      pCODIGO_FAVORECIDO_ORIGINAL   NUMBER   := NULL,
      pCODIGO_FAVORECIDO            NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO_ORIG NUMBER   := NULL,
      pCODIGO_CONTA_FAVORECIDO      NUMBER   := NULL,
      retornarRegistro              CHAR     := 'N',
      pBANCO_ORIGINAL               VARCHAR2 := NULL,
      pAGENCIA_ORIGINAL             VARCHAR2 := NULL,
      pCONTA_ORIGINAL               VARCHAR2 := NULL,
      pCODIGO_ARQUIVO_RET_CESSAO    NUMBER   := NULL,
      pDATA_REPASSE_CALCULADA       DATE     := NULL,
      pSTATUS_TRANSACAO             NUMBER   := NULL,
      pVALOR_REPASSE                NUMBER   := NULL,
      pDATA_MOVIMENTO               DATE     := NULL,
      pNUMERO_LINHA_ARQUIVO_TSYS string      := NULL,
      pFL_ATUALIZA_TRANSACAO    CHAR            := 'N',
      pDATA_PRE_AUTORIZACAO     DATE            := NULL,
      pNSU_HOST_ORIGINAL        VARCHAR2        := NULL,
      pNSU_HOST_REVERSAO        VARCHAR2        := NULL,
      pTIPO_FINANCEIRO_CONTABIL CHAR            := NULL,
      pREFERENCIA               VARCHAR2        := NULL, 
      retcur OUT TP_REFCURSOR )
  AS
    qtde NUMBER;
    retcur_temp TP_REFCURSOR;
    vVALOR_VENDA            NUMBER;
    vVALOR_DESCONTO         NUMBER;
    vVALOR_LIQUIDO          NUMBER;
    vVALOR_PARCELA          NUMBER;
    vVALOR_PARCELA_DESCONTO NUMBER;
    vVALOR_PARCELA_LIQUIDO  NUMBER;
    vVALOR_BRUTO            NUMBER;
    vVALOR_DESC             NUMBER;
    vVALOR_LIQ              NUMBER;
  BEGIN
    IF (pVALOR_PARCELA <> 0) THEN
      vVALOR_BRUTO     := pVALOR_PARCELA;
      vVALOR_DESC      := pVALOR_PARCELA_DESCONTO;
      vVALOR_LIQ       := pVALOR_PARCELA_LIQUIDO;
    ELSE
      vVALOR_BRUTO := pVALOR_VENDA;
      vVALOR_DESC  := pVALOR_DESCONTO;
      vVALOR_LIQ   := pVALOR_LIQUIDO;
    END IF;
    PR_TRANSACAO_S ( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, pCODIGO_ARQUIVO_ITEM_1 => pCODIGO_ARQUIVO_ITEM_1, pCODIGO_ARQUIVO_ITEM_2 => pCODIGO_ARQUIVO_ITEM_2, pSTATUS_TRANSACAO => pSTATUS_TRANSACAO, pCODIGO_ESTABELECIMENTO => pCODIGO_ESTABELECIMENTO, pNSU_HOST => pNSU_HOST, pDATA_TRANSACAO => pDATA_TRANSACAO, pCODIGO_ARQUIVO => pCODIGO_ARQUIVO, pCODIGO_ARQUIVO2 => pCODIGO_ARQUIVO2, pTIPO_TRANSACAO => 'CV', pCHAVE => pCHAVE, pCHAVE2 => pCHAVE2, pCHAVE3 => pCHAVE3, pCODIGO_PAGAMENTO => pCODIGO_PAGAMENTO, pCODIGO_PROCESSO => pCODIGO_PROCESSO, pSTATUS_RET_CESSAO => pSTATUS_RET_CESSAO, pCODIGO_PROCESSO_RET_CESSAO => pCODIGO_PROCESSO_RET_CESSAO, pCODIGO_ARQUIVO_RET_CESSAO => pCODIGO_ARQUIVO_RET_CESSAO, pDATA_REPASSE => pDATA_REPASSE, pCODIGO_PRODUTO => pCODIGO_PRODUTO, pCODIGO_FAVORECIDO_ORIGINAL => pCODIGO_FAVORECIDO_ORIGINAL, pCODIGO_FAVORECIDO => pCODIGO_FAVORECIDO, pCODIGO_CONTA_FAVORECIDO_ORIG => pCODIGO_CONTA_FAVORECIDO_ORIG, pCODIGO_CONTA_FAVORECIDO => pCODIGO_CONTA_FAVORECIDO,
    pDATA_REPASSE_CALCULADA => pDATA_REPASSE_CALCULADA, pVALOR_REPASSE => pVALOR_REPASSE, pDATA_MOVIMENTO => pDATA_MOVIMENTO, pFL_ATUALIZA_TRANSACAO => pFL_ATUALIZA_TRANSACAO, pTIPO_FINANCEIRO_CONTABIL => pTIPO_FINANCEIRO_CONTABIL, pVALOR_BRUTO => vVALOR_BRUTO, 
    pVALOR_DESCONTO => vVALOR_DESC,                                                                                                                                                                                                                                   
    pVALOR_LIQUIDO => vVALOR_LIQ,                                                                                                                                                                                                                                     
    pREFERENCIA => pREFERENCIA,                                                                                                                                                                                                                                       
    retornarRegistro => 'N', retcur => retcur_temp);
    vVALOR_VENDA            := NVL(pVALOR_VENDA,0);
    vVALOR_DESCONTO         := NVL(pVALOR_DESCONTO,0);
    vVALOR_LIQUIDO          := NVL(pVALOR_LIQUIDO,0);
    vVALOR_PARCELA          := NVL(pVALOR_PARCELA,0);
    vVALOR_PARCELA_DESCONTO := NVL(pVALOR_PARCELA_DESCONTO,0);
    vVALOR_PARCELA_LIQUIDO  := NVL(pVALOR_PARCELA_LIQUIDO,0);
    IF NVL(vVALOR_LIQUIDO,0) = 0 THEN
      vVALOR_LIQUIDO        := vVALOR_VENDA - vVALOR_DESCONTO;
    END IF;
    IF NVL(vVALOR_PARCELA_LIQUIDO,0) = 0 THEN
      vVALOR_PARCELA_LIQUIDO        := vVALOR_PARCELA - vVALOR_PARCELA_DESCONTO;
    END IF;
    SELECT COUNT(1)
    INTO qtde
    FROM TB_TRANSACAO_VENDA
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_TRANSACAO_VENDA
        (
          CODIGO_TRANSACAO,
          NSU_TEF,
          CODIGO_AUTORIZACAO,
          TIPO_PRODUTO,
          MEIO_CAPTURA,
          VALOR_VENDA,
          VALOR_DESCONTO,
          VALOR_LIQUIDO,
          NUMERO_CARTAO,
          NUMERO_PARCELA,
          NUMERO_PARCELA_TOTAL,
          VALOR_PARCELA,
          VALOR_PARCELA_DESCONTO,
          VALOR_PARCELA_LIQUIDO,
          BANCO,
          AGENCIA,
          CONTA,
          NUMERO_CONTA_CLIENTE,
          CODIGO_PLANO,
          CUPOM_FISCAL,
          MODALIDADE,
          TIPO_LANCAMENTO,
          VALOR_COMISSAO,
          BANCO_ORIGINAL,
          AGENCIA_ORIGINAL,
          CONTA_ORIGINAL,
          DATA_PRE_AUTORIZACAO,
          NSU_HOST_ORIGINAL,
          NSU_HOST_REVERSAO
        )
        VALUES
        (
          pCODIGO_TRANSACAO,
          pNSU_TEF,
          pCODIGO_AUTORIZACAO,
          pTIPO_PRODUTO,
          pMEIO_CAPTURA,
          vVALOR_VENDA,
          vVALOR_DESCONTO,
          vVALOR_LIQUIDO,
          pNUMERO_CARTAO,
          pNUMERO_PARCELA,
          pNUMERO_PARCELA_TOTAL,
          vVALOR_PARCELA,
          vVALOR_PARCELA_DESCONTO,
          vVALOR_PARCELA_LIQUIDO,
          pBANCO,
          pAGENCIA,
          pCONTA,
          pNUMERO_CONTA_CLIENTE,
          pCODIGO_PLANO,
          pCUPOM_FISCAL,
          pMODALIDADE,
          pTIPO_LANCAMENTO,
          pVALOR_COMISSAO,
          pBANCO_ORIGINAL,
          pAGENCIA_ORIGINAL,
          pCONTA_ORIGINAL,
          pDATA_PRE_AUTORIZACAO,
          pNSU_HOST_ORIGINAL,
          pNSU_HOST_REVERSAO
        );
    ELSE
      IF (pFL_ATUALIZA_TRANSACAO = 'S') THEN
        UPDATE TB_TRANSACAO_VENDA
        SET NSU_TEF              = NVL(pNSU_TEF, NSU_TEF),
          CODIGO_AUTORIZACAO     = NVL(pCODIGO_AUTORIZACAO, CODIGO_AUTORIZACAO),
          TIPO_PRODUTO           = NVL(pTIPO_PRODUTO, TIPO_PRODUTO),
          MEIO_CAPTURA           = NVL(pMEIO_CAPTURA, MEIO_CAPTURA),
          VALOR_VENDA            = NVL(pVALOR_VENDA, VALOR_VENDA),
          VALOR_DESCONTO         = NVL(pVALOR_DESCONTO, VALOR_DESCONTO),
          VALOR_LIQUIDO          = NVL(vVALOR_LIQUIDO, VALOR_LIQUIDO),
          NUMERO_CARTAO          = NVL(pNUMERO_CARTAO, NUMERO_CARTAO),
          NUMERO_PARCELA         = NVL(pNUMERO_PARCELA, NUMERO_PARCELA),
          NUMERO_PARCELA_TOTAL   = NVL(pNUMERO_PARCELA_TOTAL, NUMERO_PARCELA_TOTAL),
          VALOR_PARCELA          = NVL(pVALOR_PARCELA, VALOR_PARCELA),
          VALOR_PARCELA_DESCONTO = NVL(pVALOR_PARCELA_DESCONTO, VALOR_PARCELA_DESCONTO),
          VALOR_PARCELA_LIQUIDO  = NVL(vVALOR_PARCELA_LIQUIDO, VALOR_PARCELA_LIQUIDO), 
          BANCO                  = NVL(pBANCO, BANCO),
          AGENCIA                = NVL(pAGENCIA, AGENCIA),
          CONTA                  = NVL(pCONTA, CONTA),
          NUMERO_CONTA_CLIENTE   = NVL(pNUMERO_CONTA_CLIENTE, NUMERO_CONTA_CLIENTE),
          CODIGO_PLANO           = NVL(pCODIGO_PLANO, CODIGO_PLANO),
          CUPOM_FISCAL           = NVL(pCUPOM_FISCAL, CUPOM_FISCAL),
          MODALIDADE             = NVL(pMODALIDADE, MODALIDADE),
          TIPO_LANCAMENTO        = NVL(pTIPO_LANCAMENTO, TIPO_LANCAMENTO),
          VALOR_COMISSAO         = NVL(pVALOR_COMISSAO, VALOR_COMISSAO),
          BANCO_ORIGINAL         = NVL(pBANCO_ORIGINAL, BANCO_ORIGINAL),
          AGENCIA_ORIGINAL       = NVL(pAGENCIA_ORIGINAL, AGENCIA_ORIGINAL),
          CONTA_ORIGINAL         = NVL(pCONTA_ORIGINAL, CONTA_ORIGINAL),
          DATA_PRE_AUTORIZACAO   = NVL(pDATA_PRE_AUTORIZACAO, DATA_PRE_AUTORIZACAO),
          NSU_HOST_ORIGINAL      = NVL(pNSU_HOST_ORIGINAL, NSU_HOST_ORIGINAL),
          NSU_HOST_REVERSAO      = NVL(pNSU_HOST_REVERSAO, NSU_HOST_REVERSAO)
        WHERE CODIGO_TRANSACAO   = pCODIGO_TRANSACAO;
      END IF;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_TRANSACAO_VENDA_B( pCODIGO_TRANSACAO => pCODIGO_TRANSACAO, retcur => retcur);
    END IF;
  END PR_TRANSACAO_VENDA_S;
  PROCEDURE PR_TRANSACAO_VENDA_B(
      pCODIGO_TRANSACAO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_TRANSACAO ,
    A.CODIGO_ARQUIVO_ITEM_1 ,
    A.CODIGO_ARQUIVO_ITEM_2 ,
    A.STATUS_TRANSACAO ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ESTABELECIMENTO ,
    A.DATA_INCLUSAO ,
    A.NSU_HOST ,
    A.DATA_TRANSACAO ,
    A.TIPO_TRANSACAO ,
    A.CHAVE ,
    A.CHAVE2 ,
    A.CODIGO_PAGAMENTO ,
    A.CODIGO_PROCESSO ,
    A.CODIGO_FAVORECIDO_ORIGINAL ,
    A.CODIGO_FAVORECIDO ,
    A.CODIGO_CONTA_FAVORECIDO_ORIG ,
    A.CODIGO_CONTA_FAVORECIDO ,
    B.NSU_TEF ,
    B.CODIGO_AUTORIZACAO ,
    A.DATA_REPASSE ,
    B.TIPO_PRODUTO ,
    B.MEIO_CAPTURA ,
    B.VALOR_VENDA ,
    B.VALOR_DESCONTO ,
    B.VALOR_LIQUIDO ,
    B.NUMERO_CARTAO ,
    B.NUMERO_PARCELA ,
    B.NUMERO_PARCELA_TOTAL ,
    B.VALOR_PARCELA ,
    B.VALOR_PARCELA_DESCONTO ,
    B.VALOR_PARCELA_LIQUIDO ,
    B.BANCO ,
    B.AGENCIA ,
    B.CONTA ,
    B.NUMERO_CONTA_CLIENTE ,
    A.CODIGO_PRODUTO ,
    B.CODIGO_PLANO ,
    B.CUPOM_FISCAL ,
    B.MODALIDADE ,
    B.TIPO_LANCAMENTO ,
    A.DATA_MOVIMENTO FROM TB_TRANSACAO A inner join TB_TRANSACAO_VENDA B ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO WHERE A.CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_VENDA_B;
  PROCEDURE PR_TRANSACAO_VENDA_L(
      pCODIGO_ARQUIVO     NUMBER  := NULL,
      pDATA_INCLUSAO      DATE    := NULL,
      pCODIGO_PROCESSO    NUMBER  := NULL,
      pSTATUS_CONCILIACAO CHAR    := NULL,
      pCHAVE              VARCHAR := NULL,
      pINFORMAR_ORIGEM    VARCHAR := 'N',
      pCODIGO_PLANO       NUMBER  := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery VARCHAR2(3000);
  BEGIN
    vQuery := '';
    IF pCODIGO_ARQUIVO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_ARQUIVO = ''' || pCODIGO_ARQUIVO || ''' and ';
    END IF;
    IF pDATA_INCLUSAO IS NOT NULL THEN
      vQuery          := vQuery || 'A.DATA_INCLUSAO >= ''' || pDATA_INCLUSAO || ''' and '; 
    END IF;
    IF pCODIGO_PROCESSO IS NOT NULL THEN
      vQuery            := vQuery || 'A.CODIGO_PROCESSO = ''' || pCODIGO_PROCESSO || ''' and ';
    END IF;
    IF pSTATUS_CONCILIACAO IS NOT NULL THEN
      vQuery               := vQuery || 'A.STATUS_CONCILIACAO = ''' || pSTATUS_CONCILIACAO || ''' and ';
    END IF;
    IF pCHAVE IS NOT NULL THEN
      vQuery  := vQuery || 'A.CHAVE = ''' || pCHAVE || ''' and ';
    END IF;
    IF pCODIGO_PLANO IS NOT NULL THEN
      vQuery         := vQuery || 'B.CODIGO_PLANO = ''' || pCODIGO_PLANO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    IF pINFORMAR_ORIGEM = 'N' THEN
      vQuery           := 'inner join TB_TRANSACAO_VENDA B on                              
A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO ' || vQuery;
    ELSE
      vQuery := 'inner join TB_TRANSACAO_VENDA B on                              
A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO                    
left join TB_ARQUIVO_ITEM C on                              
A.CODIGO_ARQUIVO_ITEM_1 = C.CODIGO_ARQUIVO_ITEM                    
left join TB_ARQUIVO D on                              
C.CODIGO_ARQUIVO = D.CODIGO_ARQUIVO ' || vQuery;
    END IF;
    vQuery :=
    'select * from (                   
select A.CODIGO_TRANSACAO,                          
A.CODIGO_ARQUIVO_ITEM_1,                          
A.CODIGO_ARQUIVO_ITEM_2,                          
A.STATUS_TRANSACAO,                          
A.CODIGO_ARQUIVO,                          
A.CODIGO_ESTABELECIMENTO,                          
A.DATA_INCLUSAO,                          
A.NSU_HOST,                          
A.DATA_TRANSACAO,                          
A.TIPO_TRANSACAO,                          
A.CHAVE,                          
A.CHAVE2,                          
A.CODIGO_PAGAMENTO,                          
A.CODIGO_PROCESSO,                          
A.STATUS_CONCILIACAO,                          
A.CODIGO_FAVORECIDO_ORIGINAL,                          
A.CODIGO_FAVORECIDO,                          
A.CODIGO_CONTA_FAVORECIDO_ORIG,                          
A.CODIGO_CONTA_FAVORECIDO,                          
B.NSU_TEF,                          
B.CODIGO_AUTORIZACAO,                          
A.DATA_REPASSE,                          
B.TIPO_PRODUTO,                          
B.MEIO_CAPTURA,                          
B.VALOR_VENDA,                          
B.VALOR_DESCONTO,                          
B.VALOR_LIQUIDO,                          
B.NUMERO_CARTAO,                          
B.NUMERO_PARCELA,                          
B.NUMERO_PARCELA_TOTAL,                          
B.VALOR_PARCELA,                          
B.VALOR_PARCELA_DESCONTO,                          
B.VALOR_PARCELA_LIQUIDO,                          
B.BANCO,                          
B.AGENCIA,                          
B.CONTA,                          
B.NUMERO_CONTA_CLIENTE,                          
A.CODIGO_PRODUTO,                          
B.CODIGO_PLANO,                          
B.CUPOM_FISCAL,                          
B.MODALIDADE,                          
B.TIPO_LANCAMENTO,                          
B.VALOR_COMISSAO,                          
A.DATA_MOVIMENTO                     
from TB_TRANSACAO A '
    || vQuery || ' order by CODIGO_TRANSACAO)';
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' where rownum <= ' || maxLinhas;
    END IF;
    PR_LOG_DB( PDESCRICAO_LOG => vQuery, PTIPO_ORIGEM => 'PR_TRANSACAO_VENDA_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => NULL );
    OPEN retcur FOR vQuery;
  END PR_TRANSACAO_VENDA_L;
  PROCEDURE PR_TRANSACAO_VENDA_R(
      pCODIGO_TRANSACAO NUMBER)
  AS
  BEGIN
    DELETE TB_TRANSACAO_VENDA
    WHERE CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_TRANSACAO_VENDA_R;
  PROCEDURE PR_TRANSACAO_VENDA_AGENDAR(
      pCODIGO_PROCESSO      NUMBER,
      pDATA_PAGAMENTO       DATE,
      pGERAR_APENAS_ORIGEM2 CHAR := 'N' )
  AS
    CURSOR cursorPagamento
    IS
      SELECT DISTINCT A.CODIGO_ESTABELECIMENTO ,
        B.BANCO ,
        B.AGENCIA ,
        B.CONTA
      FROM TB_TRANSACAO A
      INNER JOIN TB_TRANSACAO_VENDA B
      ON A.CODIGO_TRANSACAO            = B.CODIGO_TRANSACAO
      WHERE A.CODIGO_PROCESSO          = pCODIGO_PROCESSO
      AND B.BANCO                     IS NOT NULL
      AND B.AGENCIA                   IS NOT NULL
      AND B.CONTA                     IS NOT NULL
      AND A.CODIGO_ESTABELECIMENTO    IS NOT NULL
      AND (A.TIPO_FINANCEIRO_CONTABIL <> 'C'
      OR A.TIPO_FINANCEIRO_CONTABIL   IS NULL);
    vCODIGO_ESTABELECIMENTO NUMBER;
    vBANCO                  CHAR(3);
    vAGENCIA                VARCHAR2(20);
    vCONTA                  VARCHAR2(20);
    vCODIGO_PAGAMENTO       NUMBER;
    vDATA_PAGAMENTO DATE;
    retcurTemp TP_REFCURSOR;
  BEGIN
    OPEN cursorPagamento;
    LOOP
      FETCH cursorPagamento
      INTO vCODIGO_ESTABELECIMENTO,
        vBANCO,
        vAGENCIA,
        vCONTA;
      EXIT
    WHEN cursorPagamento%NOTFOUND;
      vDATA_PAGAMENTO := pDATA_PAGAMENTO;
      vCODIGO_PAGAMENTO := NULL;
      PR_PAGAMENTO_S ( pCODIGO_PAGAMENTO => vCODIGO_PAGAMENTO, pBANCO => vBANCO, pAGENCIA => vAGENCIA, pCONTA => vCONTA, pDATA_PAGAMENTO => vDATA_PAGAMENTO, pVALOR_PAGAMENTO => NULL, pSTATUS_PAGAMENTO => '0', retornarRegistro => 'N', retcur => retcurTemp );
      UPDATE TB_TRANSACAO A
      SET CODIGO_PAGAMENTO    = vCODIGO_PAGAMENTO
      WHERE CODIGO_TRANSACAO IN
        (SELECT A.CODIGO_TRANSACAO
        FROM TB_TRANSACAO A
        INNER JOIN TB_TRANSACAO_VENDA B
        ON A.CODIGO_TRANSACAO        = B.CODIGO_TRANSACAO
        WHERE A.CODIGO_PROCESSO      = pCODIGO_PROCESSO
        AND A.CODIGO_ESTABELECIMENTO = vCODIGO_ESTABELECIMENTO
        AND B.BANCO                  = vBANCO
        AND B.AGENCIA                = vAGENCIA
        AND B.CONTA                  = vCONTA
        AND (pGERAR_APENAS_ORIGEM2   = 'N'
        OR A.CODIGO_ARQUIVO_ITEM_2  IS NOT NULL)
        );
    END LOOP;
    CLOSE cursorPagamento;
  END;
  PROCEDURE PR_TRANSACAO_NAO_CONCILIADA_L(
      pTIPO_PROCESSO VARCHAR := NULL,
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_TRANSACAO ,
    A.CODIGO_ARQUIVO_ITEM_1 ,
    A.CODIGO_ARQUIVO_ITEM_2 ,
    A.STATUS_TRANSACAO ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ESTABELECIMENTO ,
    A.DATA_INCLUSAO ,
    A.NSU_HOST ,
    A.DATA_TRANSACAO ,
    A.TIPO_TRANSACAO ,
    A.CHAVE ,
    A.CHAVE2 ,
    A.CODIGO_PAGAMENTO ,
    A.CODIGO_PROCESSO ,
    A.STATUS_CONCILIACAO ,
    A.CODIGO_FAVORECIDO_ORIGINAL ,
    A.CODIGO_FAVORECIDO ,
    A.CODIGO_CONTA_FAVORECIDO_ORIG ,
    A.CODIGO_CONTA_FAVORECIDO ,
    B.NSU_TEF ,
    B.CODIGO_AUTORIZACAO ,
    A.DATA_REPASSE ,
    B.TIPO_PRODUTO ,
    B.MEIO_CAPTURA ,
    B.VALOR_VENDA ,
    B.VALOR_DESCONTO ,
    B.VALOR_LIQUIDO ,
    B.NUMERO_CARTAO ,
    B.NUMERO_PARCELA ,
    B.NUMERO_PARCELA_TOTAL ,
    B.VALOR_PARCELA ,
    B.VALOR_PARCELA_DESCONTO ,
    B.VALOR_PARCELA_LIQUIDO ,
    B.BANCO ,
    B.AGENCIA ,
    B.CONTA ,
    B.NUMERO_CONTA_CLIENTE ,
    A.CODIGO_PRODUTO ,
    B.CODIGO_PLANO ,
    B.CUPOM_FISCAL ,
    B.MODALIDADE ,
    B.TIPO_LANCAMENTO ,
    B.VALOR_COMISSAO ,
    E.TIPO_PROCESSO ,
    C.TIPO_ARQUIVO_ITEM ,
    D.TIPO_ARQUIVO ,
    A.DATA_MOVIMENTO FROM TB_TRANSACAO A inner join TB_TRANSACAO_VENDA B ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO left join TB_ARQUIVO_ITEM C ON A.CODIGO_ARQUIVO_ITEM_1 = C.CODIGO_ARQUIVO_ITEM left join TB_ARQUIVO D ON C.CODIGO_ARQUIVO = D.CODIGO_ARQUIVO inner join TB_PROCESSO E ON A.CODIGO_PROCESSO = E.CODIGO_PROCESSO WHERE E.TIPO_PROCESSO = pTIPO_PROCESSO AND A.STATUS_CONCILIACAO = 0; 
  END PR_TRANSACAO_NAO_CONCILIADA_L;
  PROCEDURE PR_USUARIO_S(
      pCODIGO_USUARIO        VARCHAR2 := NULL,
      pNOME_USUARIO          VARCHAR2 := NULL,
      pSENHA                 VARCHAR2 := NULL,
      pASSINATURA_ELETRONICA VARCHAR2 := NULL,
      pSTATUS                NUMBER   := NULL,
      pEMAIL                 VARCHAR2 := NULL,
      pDATA_EXPIRACAO_SENHA  DATE     := NULL,
      pCPF                   VARCHAR2 := NULL,
      pMATRICULA             VARCHAR2 := NULL,
      pBLOQUEADO             CHAR     := NULL,
      pDATA_BLOQUEADO_INICIO DATE     := NULL,
      pDATA_BLOQUEADO_FIM    DATE     := NULL,
      pLOGIN                 VARCHAR  := NULL,
      retornarRegistro       CHAR     := 'N',
      retornarPermissoes     CHAR     := 'N',
      retornarPerfisAcesso   CHAR     := 'N',
      retornarGruposAcesso   CHAR     := 'N',
      retcur OUT TP_REFCURSOR,
      retcurPermissoes OUT TP_REFCURSOR,
      retcurPerfisAcesso OUT TP_REFCURSOR,
      retcurGruposAcesso OUT TP_REFCURSOR,
      retcurGrupoPerfilAcesso OUT TP_REFCURSOR)
  AS
    vCODIGO_USUARIO VARCHAR2(200);
    qtde            NUMBER;
  BEGIN
    vCODIGO_USUARIO := pCODIGO_USUARIO;
    SELECT COUNT(1)
    INTO qtde
    FROM TB_USUARIO
    WHERE CODIGO_USUARIO  = vCODIGO_USUARIO;
    IF qtde               = 0 THEN
      IF vCODIGO_USUARIO IS NULL THEN
        SELECT SQ_USUARIO.nextval INTO vCODIGO_USUARIO FROM dual;
      END IF;
      INSERT
      INTO TB_USUARIO
        (
          CODIGO_USUARIO,
          NOME_USUARIO,
          SENHA,
          ASSINATURA_ELETRONICA,
          STATUS,
          EMAIL,
          DATA_EXPIRACAO_SENHA,
          CPF,
          MATRICULA,
          BLOQUEADO,
          DT_BLOQUEADO_INICIO,
          DT_BLOQUEADO_FIM,
          LOGIN
        )
        VALUES
        (
          vCODIGO_USUARIO,
          pNOME_USUARIO,
          pSENHA,
          pASSINATURA_ELETRONICA,
          pSTATUS,
          pEMAIL,
          pDATA_EXPIRACAO_SENHA,
          pCPF,
          pMATRICULA,
          pBLOQUEADO,
          pDATA_BLOQUEADO_INICIO,
          pDATA_BLOQUEADO_FIM,
          pLOGIN
        );
    ELSE
      UPDATE TB_USUARIO
      SET NOME_USUARIO        = NVL(pNOME_USUARIO, NOME_USUARIO),
        SENHA                 = NVL(pSENHA, SENHA),
        ASSINATURA_ELETRONICA = pASSINATURA_ELETRONICA,
        STATUS                = pSTATUS,               
        EMAIL                 = pEMAIL,                
        DATA_EXPIRACAO_SENHA  = pDATA_EXPIRACAO_SENHA, 
        CPF                   = pCPF,                  
        MATRICULA             = pMATRICULA,            
        BLOQUEADO             = pBLOQUEADO,            
        DT_BLOQUEADO_INICIO   = pDATA_BLOQUEADO_INICIO,
        DT_BLOQUEADO_FIM      = pDATA_BLOQUEADO_FIM,   
        LOGIN                 = NVL(pLOGIN, LOGIN)
      WHERE CODIGO_USUARIO    = vCODIGO_USUARIO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_USUARIO_B( pCODIGO_USUARIO => vCODIGO_USUARIO, retornarPermissoes => retornarPermissoes, retornarPerfisAcesso => retornarPerfisAcesso, retornarGruposAcesso => retornarGruposAcesso, retcur => retcur, retcurPermissoes => retcurPermissoes, retcurPerfisAcesso => retcurPerfisAcesso, retcurGruposAcesso => retcurGruposAcesso, retcurGrupoPerfilAcesso => retcurGrupoPerfilAcesso);
    END IF;
  END PR_USUARIO_S;
  PROCEDURE PR_USUARIO_B(
      pCODIGO_USUARIO           VARCHAR2,
      retornarPermissoes        CHAR := 'N',
      retornarPerfisAcesso      CHAR := 'N',
      retornarGruposAcesso      CHAR := 'N',
      retornarGrupoPerfilAcesso CHAR := 'N',
      retcur OUT TP_REFCURSOR,
      retcurPermissoes OUT TP_REFCURSOR,
      retcurPerfisAcesso OUT TP_REFCURSOR,
      retcurGruposAcesso OUT TP_REFCURSOR,
      retcurGrupoPerfilAcesso OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(2000);
  BEGIN
    vQuery := '     select  A.CODIGO_USUARIO        
,       A.NOME_USUARIO        
,       A.ASSINATURA_ELETRONICA        
,       A.STATUS        
,       A.SENHA        
,       A.EMAIL        
,       A.DATA_EXPIRACAO_SENHA        
,       A.CPF        
,       A.MATRICULA        
,       A.BLOQUEADO        
,       A.DT_BLOQUEADO_INICIO        
,       A.DT_BLOQUEADO_FIM        
,       A.LOGIN        
from    TB_USUARIO A        
where  upper(A.CODIGO_USUARIO) = upper('''|| pCODIGO_USUARIO ||''')' ;
    OPEN retcur FOR vQuery;
    IF retornarPermissoes = 'S' THEN
      vQuery             := '   select  A.CODIGO_PERMISSAO            
,       A.STATUS_PERMISSAO            
from    TB_USUARIO_PERMISSAO_R A            
where   upper(A.CODIGO_USUARIO) = upper('''|| pCODIGO_USUARIO ||''')        
';
      OPEN retcurPermissoes FOR vQuery;
    END IF;
    IF retornarPerfisAcesso = 'S' THEN
      vQuery               := '       select  A.CODIGO_PERFIL          
from    TB_USUARIO_PERFIL_R A          
where   upper(A.CODIGO_USUARIO) = upper('''|| pCODIGO_USUARIO ||''')';
      OPEN retcurPerfisAcesso FOR vQuery;
    END IF;
    IF retornarGruposAcesso = 'S' THEN
      vQuery               := '              
select  A.CODIGO_GRUPO_ACESSO          
from    TB_USUARIO_GRUPO_ACESSO_R A          
where   upper(A.CODIGO_USUARIO) = upper('''|| pCODIGO_USUARIO ||''')';
      OPEN retcurGruposAcesso FOR vQuery;
    END IF;
    IF retornarGrupoPerfilAcesso = 'S' THEN
      vQuery                    := '               select  a.codigo_usuario,                      
a.nome_usuario,                      
permissao_grupo_perfil.codigo_permissao as permissao_grupo_perfil              
from    tb_usuario a                      
inner join tb_usuario_grupo_acesso_r c on                        
upper(c.codigo_usuario) = upper(a.codigo_usuario)                      
inner join tb_grupo_acesso_perfil_r d on                        
d.codigo_grupo_acesso = c.codigo_grupo_acesso                      
inner join tb_perfil_permissao_r permissao_grupo_perfil on                        
permissao_grupo_perfil.codigo_perfil = d.codigo_perfil              
where   upper(A.CODIGO_USUARIO) = upper('''|| pCODIGO_USUARIO ||''')';
      OPEN retcurGrupoPerfilAcesso FOR vQuery;
    END IF;
  END PR_USUARIO_B;
  PROCEDURE PR_USUARIO_L(
      pNOME_USUARIO VARCHAR2 := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 100)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_USUARIO ,
    A.NOME_USUARIO ,
    A.ASSINATURA_ELETRONICA ,
    A.STATUS ,
    A.EMAIL ,
    A.DATA_EXPIRACAO_SENHA ,
    A.CPF ,
    A.MATRICULA ,
    A.BLOQUEADO ,
    A.DT_BLOQUEADO_INICIO ,
    A.DT_BLOQUEADO_FIM ,
    A.LOGIN ,
    NULL
  AS
    SENHA FROM TB_USUARIO A WHERE rownum <= maxLinhas;
  END PR_USUARIO_L;

  PROCEDURE PR_USUARIO_R(
      pCODIGO_USUARIO VARCHAR2 := NULL)
  AS
  BEGIN
    DELETE TB_USUARIO WHERE upper(CODIGO_USUARIO) = upper(pCODIGO_USUARIO);
  END PR_USUARIO_R;
  PROCEDURE PR_PERFIL_S(
      pCODIGO_PERFIL     NUMBER   := NULL,
      pNOME_PERFIL       VARCHAR2 := NULL,
      retornarRegistro   CHAR     := 'N',
      retornarPermissoes CHAR     := 'N',
      retcurPermissoes OUT TP_REFCURSOR,
      retcur OUT TP_REFCURSOR)
  AS
    vCODIGO_PERFIL NUMBER;
  BEGIN
    vCODIGO_PERFIL := pCODIGO_PERFIL;
    IF vCODIGO_PERFIL IS NULL THEN
      SELECT SQ_PERFIL.nextval
      INTO vCODIGO_PERFIL
      FROM dual;
      INSERT
      INTO TB_PERFIL
        (
          CODIGO_PERFIL,
          NOME_PERFIL
        )
        VALUES
        (
          vCODIGO_PERFIL,
          pNOME_PERFIL
        );
    ELSE
      UPDATE TB_PERFIL
      SET NOME_PERFIL     = NVL(pNOME_PERFIL, NOME_PERFIL)
      WHERE CODIGO_PERFIL = vCODIGO_PERFIL;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_PERFIL_B( pCODIGO_PERFIL => vCODIGO_PERFIL, retornarPermissoes => retornarPermissoes, retcurPermissoes => retcurPermissoes, retcur => retcur );
    END IF;
  END PR_PERFIL_S;
  PROCEDURE PR_PERFIL_B(
      pCODIGO_PERFIL     NUMBER,
      retornarPermissoes CHAR := 'N',
      retcur OUT TP_REFCURSOR,
      retcurPermissoes OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_PERFIL ,
    A.NOME_PERFIL FROM TB_PERFIL A WHERE A.CODIGO_PERFIL = pCODIGO_PERFIL;
    IF retornarPermissoes = 'S' THEN
      OPEN retcurPermissoes FOR SELECT A.CODIGO_PERMISSAO ,
      A.STATUS_PERMISSAO FROM TB_PERFIL_PERMISSAO_R A WHERE A.CODIGO_PERFIL = pCODIGO_PERFIL;
    END IF;
  END PR_PERFIL_B;
  PROCEDURE PR_PERFIL_L(
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_PERFIL ,
    A.NOME_PERFIL FROM TB_PERFIL A WHERE rownum <= maxLinhas;
  END PR_PERFIL_L;
  PROCEDURE PR_PERFIL_R(
      pCODIGO_PERFIL NUMBER )
  AS
  BEGIN
    DELETE TB_PERFIL WHERE CODIGO_PERFIL = pCODIGO_PERFIL;
  END PR_PERFIL_R;
  PROCEDURE PR_GRUPO_ACESSO_S(
      pCODIGO_GRUPO_ACESSO NUMBER   := NULL,
      pNOME_GRUPO_ACESSO   VARCHAR2 := NULL,
      retornarRegistro     CHAR     := 'N',
      retornarPermissoes   CHAR     := 'N',
      retornarPerfisAcesso CHAR     := 'N',
      retcurPerfisAcesso OUT TP_REFCURSOR,
      retcurPermissoes OUT TP_REFCURSOR,
      retcur OUT TP_REFCURSOR)
  AS
    vCODIGO_GRUPO_ACESSO NUMBER;
  BEGIN
    vCODIGO_GRUPO_ACESSO := pCODIGO_GRUPO_ACESSO;
    IF vCODIGO_GRUPO_ACESSO IS NULL THEN
      SELECT SQ_GRUPO_ACESSO.nextval
      INTO vCODIGO_GRUPO_ACESSO
      FROM dual;
      INSERT
      INTO TB_GRUPO_ACESSO
        (
          CODIGO_GRUPO_ACESSO,
          NOME_GRUPO_ACESSO
        )
        VALUES
        (
          vCODIGO_GRUPO_ACESSO,
          pNOME_GRUPO_ACESSO
        );
    ELSE
      UPDATE TB_GRUPO_ACESSO
      SET NOME_GRUPO_ACESSO     = NVL(pNOME_GRUPO_ACESSO, NOME_GRUPO_ACESSO)
      WHERE CODIGO_GRUPO_ACESSO = vCODIGO_GRUPO_ACESSO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_GRUPO_ACESSO_B( pCODIGO_GRUPO_ACESSO => vCODIGO_GRUPO_ACESSO, retornarPermissoes => retornarPermissoes, retornarPerfisAcesso => retornarPerfisAcesso, retcurPermissoes => retcurPermissoes, retcurPerfisAcesso => retcurPerfisAcesso, retcur => retcur );
    END IF;
  END PR_GRUPO_ACESSO_S;
  PROCEDURE PR_GRUPO_ACESSO_B(
      pCODIGO_GRUPO_ACESSO NUMBER,
      retornarPermissoes   CHAR := 'N',
      retornarPerfisAcesso CHAR := 'N',
      retcur OUT TP_REFCURSOR,
      retcurPermissoes OUT TP_REFCURSOR,
      retcurPerfisAcesso OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_GRUPO_ACESSO ,
    A.NOME_GRUPO_ACESSO FROM TB_GRUPO_ACESSO A WHERE A.CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO;
    IF retornarPermissoes                                                  = 'S' THEN
      OPEN retcurPermissoes FOR SELECT A.CODIGO_PERMISSAO ,
      A.STATUS_PERMISSAO FROM TB_GRUPO_ACESSO_PERMISSAO_R A WHERE A.CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO;
    END IF;
    IF retornarPerfisAcesso = 'S' THEN
      OPEN retcurPerfisAcesso FOR SELECT A.CODIGO_PERFIL ,
      A.CODIGO_GRUPO_ACESSO FROM TB_GRUPO_ACESSO_PERFIL_R A WHERE A.CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO;
    END IF;
  END PR_GRUPO_ACESSO_B;
  PROCEDURE PR_GRUPO_ACESSO_L(
      pCODIGO_GRUPO_ACESSO NUMBER := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_GRUPO_ACESSO ,
    A.NOME_GRUPO_ACESSO FROM TB_GRUPO_ACESSO A WHERE rownum <= maxLinhas order by A.CODIGO_GRUPO_ACESSO 
    ;
  END PR_GRUPO_ACESSO_L;
  PROCEDURE PR_GRUPO_ACESSO_R(
      pCODIGO_GRUPO_ACESSO NUMBER )
  AS
  BEGIN
    DELETE TB_GRUPO_ACESSO
    WHERE CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO;
  END PR_GRUPO_ACESSO_R;
  PROCEDURE PR_USUARIO_PERMISSAO_S(
      pCODIGO_USUARIO   VARCHAR2 := NULL,
      pCODIGO_PERMISSAO NUMBER   := NULL,
      pSTATUS_PERMISSAO NUMBER   := NULL,
      retornarRegistro  CHAR     := 'N',
      retcur OUT TP_REFCURSOR)
  AS
    qtde NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_USUARIO_PERMISSAO_R
    WHERE upper(CODIGO_USUARIO) = upper(pCODIGO_USUARIO)
    AND CODIGO_PERMISSAO        = pCODIGO_PERMISSAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_USUARIO_PERMISSAO_R
        (
          CODIGO_USUARIO,
          CODIGO_PERMISSAO,
          STATUS_PERMISSAO
        )
        VALUES
        (
          upper(pCODIGO_USUARIO),
          pCODIGO_PERMISSAO,
          pSTATUS_PERMISSAO
        );
    ELSE
      UPDATE TB_USUARIO_PERMISSAO_R
      SET STATUS_PERMISSAO        = NVL(pSTATUS_PERMISSAO, STATUS_PERMISSAO)
      WHERE upper(CODIGO_USUARIO) = upper(pCODIGO_USUARIO)
      AND CODIGO_PERMISSAO        = pCODIGO_PERMISSAO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_USUARIO_PERMISSAO_B( pCODIGO_USUARIO => upper(pCODIGO_USUARIO), pCODIGO_PERMISSAO => pCODIGO_PERMISSAO, retcur => retcur);
    END IF;
  END PR_USUARIO_PERMISSAO_S;
  PROCEDURE PR_USUARIO_PERMISSAO_B(
      pCODIGO_USUARIO   VARCHAR2,
      pCODIGO_PERMISSAO NUMBER := NULL,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_USUARIO ,
    A.CODIGO_PERMISSAO ,
    A.STATUS_PERMISSAO FROM TB_USUARIO_PERMISSAO_R A WHERE upper(A.CODIGO_USUARIO) = upper(pCODIGO_USUARIO) AND A.CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
  END PR_USUARIO_PERMISSAO_B;
  PROCEDURE PR_USUARIO_PERMISSAO_L(
      pCODIGO_USUARIO   VARCHAR2 := NULL,
      pCODIGO_PERMISSAO NUMBER   := NULL ,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_USUARIO ,
    A.CODIGO_PERMISSAO ,
    A.STATUS_PERMISSAO FROM TB_USUARIO_PERMISSAO_R A WHERE (pCODIGO_USUARIO IS NULL OR upper(A.CODIGO_USUARIO) = upper(pCODIGO_USUARIO)) OR (pCODIGO_PERMISSAO IS NULL OR A.CODIGO_PERMISSAO = pCODIGO_PERMISSAO) ORDER BY A.CODIGO_USUARIO,
    A.CODIGO_PERMISSAO 
    ;
  END PR_USUARIO_PERMISSAO_L;
  PROCEDURE PR_USUARIO_PERMISSAO_R(
      pCODIGO_USUARIO   VARCHAR2,
      pCODIGO_PERMISSAO NUMBER )
  AS
  BEGIN
    DELETE TB_USUARIO_PERMISSAO_R A
    WHERE upper(A.CODIGO_USUARIO) = upper(pCODIGO_USUARIO)
    AND A.CODIGO_PERMISSAO        = pCODIGO_PERMISSAO;
  END PR_USUARIO_PERMISSAO_R;
  PROCEDURE PR_USUARIO_GRUPO_ACESSO_S(
      pCODIGO_USUARIO      VARCHAR2 := NULL,
      pCODIGO_GRUPO_ACESSO NUMBER   := NULL,
      retornarRegistro     CHAR     := 'N',
      retcur OUT TP_REFCURSOR)
  AS
    qtde NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_USUARIO_GRUPO_ACESSO_R
    WHERE upper(CODIGO_USUARIO) = upper(pCODIGO_USUARIO)
    AND CODIGO_GRUPO_ACESSO     = pCODIGO_GRUPO_ACESSO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_USUARIO_GRUPO_ACESSO_R
        (
          CODIGO_USUARIO,
          CODIGO_GRUPO_ACESSO
        )
        VALUES
        (
          upper(pCODIGO_USUARIO),
          pCODIGO_GRUPO_ACESSO
        );
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_USUARIO_GRUPO_ACESSO_B( pCODIGO_USUARIO => upper(pCODIGO_USUARIO), pCODIGO_GRUPO_ACESSO => pCODIGO_GRUPO_ACESSO, retcur => retcur);
    END IF;
  END PR_USUARIO_GRUPO_ACESSO_S;
  PROCEDURE PR_USUARIO_GRUPO_ACESSO_B
    (
      pCODIGO_USUARIO      VARCHAR2,
      pCODIGO_GRUPO_ACESSO NUMBER := NULL,
      retcur OUT TP_REFCURSOR
    )
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_USUARIO ,
    A.CODIGO_GRUPO_ACESSO FROM TB_USUARIO_GRUPO_ACESSO_R A WHERE upper
    (
      A.CODIGO_USUARIO
    )
    = upper
    (
      pCODIGO_USUARIO
    )
    AND A.CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO;
  END PR_USUARIO_GRUPO_ACESSO_B;
  PROCEDURE PR_USUARIO_GRUPO_ACESSO_L
    (
      pCODIGO_USUARIO      VARCHAR2 := NULL,
      pCODIGO_GRUPO_ACESSO NUMBER   := NULL,
      retcur OUT TP_REFCURSOR
    )
  AS
    vQuery VARCHAR2
    (
      500
    )
    ;
  BEGIN
    vQuery := '';
    IF pCODIGO_USUARIO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_USUARIO = ''' || pCODIGO_USUARIO || ''' and ';
    END IF;
    IF pCODIGO_GRUPO_ACESSO IS NOT NULL THEN
      vQuery                := vQuery || 'A.CODIGO_GRUPO_ACESSO = ''' || pCODIGO_GRUPO_ACESSO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'SELECT A.CODIGO_GRUPO_ACESSO FROM TB_USUARIO_GRUPO_ACESSO_R A ' || vQuery || ' ORDER BY A.CODIGO_GRUPO_ACESSO'; 
    OPEN retcur FOR vQuery;
  END PR_USUARIO_GRUPO_ACESSO_L;
  PROCEDURE PR_USUARIO_GRUPO_ACESSO_R
    (
      pCODIGO_USUARIO      VARCHAR2,
      pCODIGO_GRUPO_ACESSO NUMBER
    )
  AS
  BEGIN
    DELETE TB_USUARIO_GRUPO_ACESSO_R A
    WHERE upper(A.CODIGO_USUARIO) = upper(pCODIGO_USUARIO)
    AND A.CODIGO_GRUPO_ACESSO     = pCODIGO_GRUPO_ACESSO;
  END PR_USUARIO_GRUPO_ACESSO_R;
  PROCEDURE PR_USUARIO_PERFIL_S(
      pCODIGO_USUARIO  VARCHAR2 := NULL,
      pCODIGO_PERFIL   NUMBER   := NULL,
      retornarRegistro CHAR     := 'N',
      retcur OUT TP_REFCURSOR)
  AS
    qtde NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_USUARIO_PERFIL_R
    WHERE CODIGO_USUARIO = pCODIGO_USUARIO
    AND CODIGO_PERFIL    = pCODIGO_PERFIL;
    IF qtde = 0 THEN
      INSERT
      INTO TB_USUARIO_PERFIL_R
        (
          CODIGO_USUARIO,
          CODIGO_PERFIL
        )
        VALUES
        (
          pCODIGO_USUARIO,
          pCODIGO_PERFIL
        );
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_USUARIO_PERFIL_B( pCODIGO_USUARIO => pCODIGO_USUARIO, pCODIGO_PERFIL => pCODIGO_PERFIL, retcur => retcur);
    END IF;
  END PR_USUARIO_PERFIL_S;
  PROCEDURE PR_USUARIO_PERFIL_B
    (
      pCODIGO_USUARIO VARCHAR2,
      pCODIGO_PERFIL  NUMBER := NULL,
      retcur OUT TP_REFCURSOR
    )
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_USUARIO ,
    A.CODIGO_PERFIL FROM TB_USUARIO_PERFIL_R A WHERE A.CODIGO_USUARIO = pCODIGO_USUARIO AND A.CODIGO_PERFIL = pCODIGO_PERFIL;
  END PR_USUARIO_PERFIL_B;
  PROCEDURE PR_USUARIO_PERFIL_L
    (
      pCODIGO_USUARIO VARCHAR2 := NULL,
      pCODIGO_PERFIL  NUMBER   := NULL ,
      retcur OUT TP_REFCURSOR
    )
  AS
    vQuery VARCHAR2
    (
      500
    )
    ;
  BEGIN
    vQuery := '';
    IF pCODIGO_USUARIO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_USUARIO = ''' || pCODIGO_USUARIO || ''' and ';
    END IF;
    IF pCODIGO_PERFIL IS NOT NULL THEN
      vQuery          := vQuery || 'A.CODIGO_PERFIL = ''' || pCODIGO_PERFIL || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'SELECT A.CODIGO_PERFIL FROM TB_USUARIO_PERFIL_R A ' || vQuery || ' ORDER BY A.CODIGO_PERFIL'; 
    OPEN retcur FOR vQuery;
  END PR_USUARIO_PERFIL_L;
  PROCEDURE PR_USUARIO_PERFIL_R
    (
      pCODIGO_USUARIO VARCHAR2,
      pCODIGO_PERFIL  NUMBER
    )
  AS
  BEGIN
    DELETE TB_USUARIO_PERFIL_R A
    WHERE A.CODIGO_USUARIO = pCODIGO_USUARIO
    AND A.CODIGO_PERFIL    = pCODIGO_PERFIL;
  END PR_USUARIO_PERFIL_R;
  PROCEDURE PR_PERFIL_PERMISSAO_S(
      pCODIGO_PERFIL    NUMBER := NULL,
      pCODIGO_PERMISSAO NUMBER := NULL,
      pSTATUS_PERMISSAO NUMBER := NULL,
      retornarRegistro  CHAR   := 'N',
      retcur OUT TP_REFCURSOR)
  AS
    qtde NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_PERFIL_PERMISSAO_R
    WHERE CODIGO_PERFIL  = pCODIGO_PERFIL
    AND CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_PERFIL_PERMISSAO_R
        (
          CODIGO_PERFIL,
          CODIGO_PERMISSAO,
          STATUS_PERMISSAO
        )
        VALUES
        (
          pCODIGO_PERFIL,
          pCODIGO_PERMISSAO,
          pSTATUS_PERMISSAO
        );
    ELSE
      UPDATE TB_PERFIL_PERMISSAO_R
      SET STATUS_PERMISSAO = NVL(pSTATUS_PERMISSAO, STATUS_PERMISSAO)
      WHERE CODIGO_PERFIL  = pCODIGO_PERFIL
      AND CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_PERFIL_PERMISSAO_B( pCODIGO_PERFIL => pCODIGO_PERFIL, pCODIGO_PERMISSAO => pCODIGO_PERMISSAO, retcur => retcur);
    END IF;
  END PR_PERFIL_PERMISSAO_S;
  PROCEDURE PR_PERFIL_PERMISSAO_B(
      pCODIGO_PERFIL    NUMBER,
      pCODIGO_PERMISSAO NUMBER := NULL,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_PERFIL ,
    A.CODIGO_PERMISSAO ,
    A.STATUS_PERMISSAO FROM TB_PERFIL_PERMISSAO_R A WHERE A.CODIGO_PERFIL = pCODIGO_PERFIL AND A.CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
  END PR_PERFIL_PERMISSAO_B;
  PROCEDURE PR_PERFIL_PERMISSAO_L(
      pCODIGO_PERFIL    NUMBER := NULL,
      pCODIGO_PERMISSAO NUMBER := NULL ,
      retcur OUT TP_REFCURSOR)
  AS
    vQuery VARCHAR2(500);
  BEGIN
    vQuery := '';
    IF pCODIGO_PERFIL IS NOT NULL THEN
      vQuery          := vQuery || 'A.CODIGO_PERFIL = ''' || pCODIGO_PERFIL || ''' and ';
    END IF;
    IF pCODIGO_PERMISSAO IS NOT NULL THEN
      vQuery             := vQuery || 'A.CODIGO_PERMISSAO = ''' || pCODIGO_PERMISSAO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'SELECT A.CODIGO_PERFIL, A.CODIGO_PERMISSAO, A.STATUS_PERMISSAO FROM TB_PERFIL_PERMISSAO_R A ' || vQuery || ' ORDER BY A.CODIGO_PERFIL, A.CODIGO_PERMISSAO'; 
    OPEN retcur FOR vQuery;
  END PR_PERFIL_PERMISSAO_L;
  PROCEDURE PR_PERFIL_PERMISSAO_R(
      pCODIGO_PERFIL    NUMBER,
      pCODIGO_PERMISSAO NUMBER )
  AS
  BEGIN
    DELETE TB_PERFIL_PERMISSAO_R A
    WHERE A.CODIGO_PERFIL  = pCODIGO_PERFIL
    AND A.CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
  END PR_PERFIL_PERMISSAO_R;
  PROCEDURE PR_GRUPO_ACESSO_PERMISSAO_S(
      pCODIGO_GRUPO_ACESSO VARCHAR2 := NULL,
      pCODIGO_PERMISSAO    NUMBER   := NULL,
      pSTATUS_PERMISSAO    NUMBER   := NULL,
      retornarRegistro     CHAR     := 'N',
      retcur OUT TP_REFCURSOR)
  AS
    qtde NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_GRUPO_ACESSO_PERMISSAO_R
    WHERE CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO
    AND CODIGO_PERMISSAO      = pCODIGO_PERMISSAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_GRUPO_ACESSO_PERMISSAO_R
        (
          CODIGO_GRUPO_ACESSO,
          CODIGO_PERMISSAO,
          STATUS_PERMISSAO
        )
        VALUES
        (
          pCODIGO_GRUPO_ACESSO,
          pCODIGO_PERMISSAO,
          pSTATUS_PERMISSAO
        );
    ELSE
      UPDATE TB_GRUPO_ACESSO_PERMISSAO_R
      SET STATUS_PERMISSAO      = NVL(pSTATUS_PERMISSAO, STATUS_PERMISSAO)
      WHERE CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO
      AND CODIGO_PERMISSAO      = pCODIGO_PERMISSAO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_GRUPO_ACESSO_PERMISSAO_B( pCODIGO_GRUPO_ACESSO => pCODIGO_GRUPO_ACESSO, pCODIGO_PERMISSAO => pCODIGO_PERMISSAO, retcur => retcur);
    END IF;
  END PR_GRUPO_ACESSO_PERMISSAO_S;
  PROCEDURE PR_GRUPO_ACESSO_PERMISSAO_B(
      pCODIGO_GRUPO_ACESSO VARCHAR2,
      pCODIGO_PERMISSAO    NUMBER := NULL,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_GRUPO_ACESSO ,
    A.CODIGO_PERMISSAO ,
    A.STATUS_PERMISSAO FROM TB_GRUPO_ACESSO_PERMISSAO_R A WHERE A.CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO AND A.CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
  END PR_GRUPO_ACESSO_PERMISSAO_B;
  PROCEDURE PR_GRUPO_ACESSO_PERMISSAO_L(
      pCODIGO_GRUPO_ACESSO VARCHAR2 := NULL,
      pCODIGO_PERMISSAO    NUMBER   := NULL ,
      retcur OUT TP_REFCURSOR)
  AS
    vQuery VARCHAR2(500);
  BEGIN
    vQuery := '';
    IF pCODIGO_GRUPO_ACESSO IS NOT NULL THEN
      vQuery                := vQuery || 'A.CODIGO_GRUPO_ACESSO = ''' || pCODIGO_GRUPO_ACESSO || ''' and ';
    END IF;
    IF pCODIGO_PERMISSAO IS NOT NULL THEN
      vQuery             := vQuery || 'A.CODIGO_PERMISSAO = ''' || pCODIGO_PERMISSAO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'SELECT A.CODIGO_GRUPO_ACESSO, A.CODIGO_PERMISSAO, A.STATUS_PERMISSAO FROM TB_GRUPO_ACESSO_PERMISSAO_R A ' || vQuery || ' ORDER BY A.CODIGO_GRUPO_ACESSO, A.CODIGO_PERMISSAO '; 
    OPEN retcur FOR vQuery;
  END PR_GRUPO_ACESSO_PERMISSAO_L;
  PROCEDURE PR_GRUPO_ACESSO_PERMISSAO_R(
      pCODIGO_GRUPO_ACESSO VARCHAR2,
      pCODIGO_PERMISSAO    NUMBER )
  AS
  BEGIN
    DELETE TB_GRUPO_ACESSO_PERMISSAO_R A
    WHERE A.CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO
    AND A.CODIGO_PERMISSAO      = pCODIGO_PERMISSAO;
  END PR_GRUPO_ACESSO_PERMISSAO_R;
  PROCEDURE PR_GRUPO_ACESSO_PERFIL_S(
      pCODIGO_GRUPO_ACESSO NUMBER := NULL,
      pCODIGO_PERFIL       NUMBER := NULL,
      retornarRegistro     CHAR   := 'N',
      retcur OUT TP_REFCURSOR)
  AS
    qtde NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_GRUPO_ACESSO_PERFIL_R
    WHERE CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO
    AND CODIGO_PERFIL         = pCODIGO_PERFIL;
    IF qtde = 0 THEN
      INSERT
      INTO TB_GRUPO_ACESSO_PERFIL_R
        (
          CODIGO_GRUPO_ACESSO,
          CODIGO_PERFIL
        )
        VALUES
        (
          pCODIGO_GRUPO_ACESSO,
          pCODIGO_PERFIL
        );
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_GRUPO_ACESSO_PERFIL_B( pCODIGO_GRUPO_ACESSO => pCODIGO_GRUPO_ACESSO, pCODIGO_PERFIL => pCODIGO_PERFIL, retcur => retcur);
    END IF;
  END PR_GRUPO_ACESSO_PERFIL_S;
  PROCEDURE PR_GRUPO_ACESSO_PERFIL_B
    (
      pCODIGO_GRUPO_ACESSO NUMBER,
      pCODIGO_PERFIL       NUMBER := NULL,
      retcur OUT TP_REFCURSOR
    )
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_GRUPO_ACESSO ,
    A.CODIGO_PERFIL FROM TB_GRUPO_ACESSO_PERFIL_R A WHERE A.CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO AND A.CODIGO_PERFIL = pCODIGO_PERFIL;
  END PR_GRUPO_ACESSO_PERFIL_B;
  PROCEDURE PR_GRUPO_ACESSO_PERFIL_L
    (
      pCODIGO_GRUPO_ACESSO NUMBER := NULL,
      pCODIGO_PERFIL       NUMBER := NULL,
      retcur OUT TP_REFCURSOR
    )
  AS
    vQuery VARCHAR2
    (
      500
    )
    ;
  BEGIN
    vQuery := '';
    IF pCODIGO_GRUPO_ACESSO IS NOT NULL THEN
      vQuery                := vQuery || 'A.CODIGO_GRUPO_ACESSO = ''' || pCODIGO_GRUPO_ACESSO || ''' and ';
    END IF;
    IF pCODIGO_PERFIL IS NOT NULL THEN
      vQuery          := vQuery || 'A.CODIGO_PERFIL = ''' || pCODIGO_PERFIL || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'SELECT A.CODIGO_GRUPO_ACESSO, A.CODIGO_PERFIL FROM TB_GRUPO_ACESSO_PERFIL_R A ' || vQuery || ' ORDER BY A.CODIGO_GRUPO_ACESSO, A.CODIGO_PERFIL '; 
    OPEN retcur FOR vQuery;
  END PR_GRUPO_ACESSO_PERFIL_L;
  PROCEDURE PR_GRUPO_ACESSO_PERFIL_R
    (
      pCODIGO_GRUPO_ACESSO NUMBER,
      pCODIGO_PERFIL       NUMBER
    )
  AS
  BEGIN
    DELETE TB_GRUPO_ACESSO_PERFIL_R A
    WHERE A.CODIGO_GRUPO_ACESSO = pCODIGO_GRUPO_ACESSO
    AND A.CODIGO_PERFIL         = pCODIGO_PERFIL;
  END PR_GRUPO_ACESSO_PERFIL_R;
  PROCEDURE PR_LISTA_B(
      pCODIGO_LISTA    NUMBER   := NULL,
      pMNEMONICO_LISTA VARCHAR2 := NULL,
      retornarItens    CHAR     := 'N',
      retcur OUT TP_REFCURSOR,
      retcurItens OUT TP_REFCURSOR)
  AS
    vCODIGO_LISTA NUMBER;
  BEGIN
    vCODIGO_LISTA := pCODIGO_LISTA;
    IF NVL(vCODIGO_LISTA, 0) = 0 THEN
      SELECT CODIGO_LISTA
      INTO vCODIGO_LISTA
      FROM TB_LISTA
      WHERE MNEMONICO_LISTA = pMNEMONICO_LISTA;
    END IF;
    OPEN retcur FOR SELECT CODIGO_LISTA,
    NOME_LISTA,
    MNEMONICO_LISTA FROM TB_LISTA WHERE CODIGO_LISTA = vCODIGO_LISTA;
    IF retornarItens = 'S' THEN
      PR_LISTA_ITEM_L ( pFILTRO_CODIGO_LISTA => vCODIGO_LISTA, retcur => retcurItens);
    END IF;
  END PR_LISTA_B;
  PROCEDURE PR_LISTA_L(
      pNOME_LISTA VARCHAR2 := NULL,
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR SELECT CODIGO_LISTA,
    NOME_LISTA,
    MNEMONICO_LISTA,
    LISTA_CONF_HEADER FROM TB_LISTA WHERE NOME_LISTA = NVL(pNOME_LISTA, NOME_LISTA) order by NOME_LISTA;
  END PR_LISTA_L;
  PROCEDURE PR_LISTA_S(
      pCODIGO_LISTA    NUMBER,
      pNOME_LISTA      VARCHAR2 := NULL,
      pMNEMONICO_LISTA VARCHAR2 := NULL,
      retornarRegistro CHAR     := 'N',
      retornarItens    CHAR     := 'N',
      retcur OUT TP_REFCURSOR,
      retcurItens OUT TP_REFCURSOR )
  AS
    vCODIGO_LISTA NUMBER;
  BEGIN
    vCODIGO_LISTA := pCODIGO_LISTA;
    IF vCODIGO_LISTA IS NULL THEN
      SELECT SQ_LISTA.nextval
      INTO vCODIGO_LISTA
      FROM dual;
      INSERT
      INTO TB_LISTA
        (
          CODIGO_LISTA,
          NOME_LISTA,
          MNEMONICO_LISTA
        )
        VALUES
        (
          vCODIGO_LISTA,
          pNOME_LISTA,
          pMNEMONICO_LISTA
        );
    ELSE
      UPDATE TB_LISTA
      SET NOME_LISTA     = pNOME_LISTA,
        MNEMONICO_LISTA  = pMNEMONICO_LISTA
      WHERE CODIGO_LISTA = vCODIGO_LISTA;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_LISTA_B ( pCODIGO_LISTA => vCODIGO_LISTA, retornarItens => retornarItens, retcur => retcur, retcurItens => retcurItens);
    END IF;
  END PR_LISTA_S;
  PROCEDURE PR_LISTA_R(
      pCODIGO_LISTA NUMBER := NULL )
  AS
  BEGIN
    DELETE TB_LISTA WHERE CODIGO_LISTA = pCODIGO_LISTA;
  END PR_LISTA_R;
  PROCEDURE PR_LISTA_ITEM_B(
      pCODIGO_LISTA_ITEM NUMBER := NULL,
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR SELECT CODIGO_LISTA_ITEM ,
    CODIGO_LISTA ,
    NOME_LISTA_ITEM ,
    MNEMONICO_LISTA_ITEM ,
    ATIVO ,
    VALOR FROM TB_LISTA_ITEM WHERE CODIGO_LISTA_ITEM = pCODIGO_LISTA_ITEM;
  END PR_LISTA_ITEM_B;
  PROCEDURE PR_LISTA_ITEM_L(
      pFILTRO_CODIGO_LISTA NUMBER   := NULL,
      pNOME_LISTA          VARCHAR2 := NULL,
      pVALOR               VARCHAR2 := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(1000);
  BEGIN
    vQuery := '';
    IF pFILTRO_CODIGO_LISTA IS NOT NULL THEN
      vQuery                := vQuery || 'A.CODIGO_LISTA = ''' || pFILTRO_CODIGO_LISTA || ''' and ';
    END IF;
    IF pNOME_LISTA IS NOT NULL THEN
      vQuery       := vQuery || 'B.NOME_LISTA = ''' || pNOME_LISTA || ''' and ';
    END IF;
    IF pVALOR IS NOT NULL THEN
      vQuery  := vQuery || 'A.VALOR = ''' || pVALOR || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'SELECT    
A.CODIGO_LISTA_ITEM  
, A.CODIGO_LISTA  
, A.NOME_LISTA_ITEM  
, A.MNEMONICO_LISTA_ITEM  
, A.ATIVO  
, A.VALOR    

FROM    
TB_LISTA_ITEM A    

INNER JOIN    
TB_LISTA B on B.CODIGO_LISTA = A.CODIGO_LISTA     

' || vQuery || '  
ORDER BY    
A.CODIGO_LISTA        
, A.CODIGO_LISTA_ITEM 
, A.VALOR  
, A.NOME_LISTA_ITEM  
';
    OPEN retcur FOR vQuery;
  END PR_LISTA_ITEM_L;
  PROCEDURE PR_LISTA_ITEM_S(
      pCODIGO_LISTA_ITEM    NUMBER   := NULL,
      pCODIGO_LISTA         NUMBER   := NULL,
      pNOME_LISTA_ITEM      VARCHAR2 := NULL,
      pMNEMONICO_LISTA_ITEM VARCHAR2 := NULL,
      pVALOR                VARCHAR2 := NULL,
      pATIVO                CHAR     := 'S',
      retornarRegistro      CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_LISTA_ITEM NUMBER;
    vCODIGO_LISTA_CONF NUMBER;
    qtde               NUMBER;
    qtde_lista_conf    NUMBER;
    cursor_grupos TP_REFCURSOR;
    vCODIGO_EMPRESA_GRUPO NUMBER;
  BEGIN
    vCODIGO_LISTA_ITEM := pCODIGO_LISTA_ITEM;
    IF (vCODIGO_LISTA_ITEM IS NULL) THEN
      SELECT COUNT(*)
      INTO qtde
      FROM TB_LISTA_ITEM
      WHERE codigo_lista = pCODIGO_LISTA
      AND VALOR          = pVALOR;
      IF qtde            > 0 THEN
        raise_application_error(-20009, 'Valor ja existente. ');
      END IF;
      SELECT SQ_LISTA_ITEM.nextval
      INTO vCODIGO_LISTA_ITEM
      FROM dual;
      INSERT
      INTO TB_LISTA_ITEM
        (
          CODIGO_LISTA_ITEM,
          CODIGO_LISTA,
          NOME_LISTA_ITEM,
          MNEMONICO_LISTA_ITEM,
          ATIVO,
          VALOR
        )
        VALUES
        (
          vCODIGO_LISTA_ITEM,
          pCODIGO_LISTA,
          pNOME_LISTA_ITEM,
          pMNEMONICO_LISTA_ITEM,
          pATIVO,
          pVALOR
        );
      SELECT COUNT(*)
      INTO qtde_lista_conf
      FROM tb_lista_conf A
      INNER JOIN tb_lista_item B
      ON A.CODIGO_LISTA_ITEM = B.CODIGO_LISTA_ITEM
      INNER JOIN tb_lista C
      ON C.CODIGO_LISTA  = B.CODIGO_LISTA
      WHERE C.NOME_LISTA = 'MeioPagamento';
      IF qtde_lista_conf > 0 THEN
        OPEN cursor_grupos FOR SELECT CODIGO_EMPRESA_GRUPO FROM TB_EMPRESA_GRUPO;
        LOOP
          FETCH cursor_grupos INTO vCODIGO_EMPRESA_GRUPO;
          EXIT
        WHEN cursor_grupos%NOTFOUND;
          SELECT SQ_LISTA_CONF.nextval INTO vCODIGO_LISTA_CONF FROM dual;
          INSERT
          INTO tb_lista_conf
            (
              CODIGO_LISTA_CONF,
              CODIGO_LISTA_ITEM,
              CONF_LISTA,
              CODIGO_EMPRESA_GRUPO
            )
            VALUES
            (
              vCODIGO_LISTA_CONF,
              vCODIGO_LISTA_ITEM,
              'S',
              vCODIGO_EMPRESA_GRUPO
            );
        END LOOP;
      END IF;
    ELSE
      SELECT COUNT(*)
      INTO qtde
      FROM TB_LISTA_ITEM
      WHERE codigo_lista     = pCODIGO_LISTA
      AND VALOR              = pVALOR
      AND CODIGO_LISTA_ITEM <> vCODIGO_LISTA_ITEM;
      IF qtde                > 0 THEN
        raise_application_error(-20009, 'Valor ja existente. ');
      END IF;
      UPDATE TB_LISTA_ITEM
      SET CODIGO_LISTA        = pCODIGO_LISTA,
        NOME_LISTA_ITEM       = pNOME_LISTA_ITEM,
        MNEMONICO_LISTA_ITEM  = pMNEMONICO_LISTA_ITEM,
        ATIVO                 = pATIVO,
        VALOR                 = pVALOR
      WHERE CODIGO_LISTA_ITEM = vCODIGO_LISTA_ITEM;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_LISTA_ITEM_B ( pCODIGO_LISTA_ITEM => vCODIGO_LISTA_ITEM, retcur => retcur );
    END IF;
  END PR_LISTA_ITEM_S;
  PROCEDURE PR_LISTA_ITEM_R(
      pCODIGO_LISTA_ITEM NUMBER := NULL )
  AS
  BEGIN
    DELETE TB_LISTA_ITEM
    WHERE CODIGO_LISTA_ITEM = pCODIGO_LISTA_ITEM;
    DELETE tb_lista_conf
    WHERE CODIGO_LISTA_ITEM = pCODIGO_LISTA_ITEM;
  END PR_LISTA_ITEM_R;
  PROCEDURE PR_PAGAMENTO_S(
      pCODIGO_PAGAMENTO OUT NUMBER,
      pBANCO                   VARCHAR2 := NULL,
      pAGENCIA                 VARCHAR2 := NULL,
      pCONTA                   VARCHAR2 := NULL,
      pDATA_PAGAMENTO          DATE     := NULL,
      pVALOR_PAGAMENTO         NUMBER   := NULL,
      pSTATUS_PAGAMENTO        CHAR     := NULL,
      pCODIGO_FAVORECIDO       NUMBER   := NULL,
      pCODIGO_EMPRESA_GRUPO    NUMBER   := NULL,
      pDATA_ENVIO              DATE     := NULL,
      pDATA_GERACAO            DATE     := NULL,
      pDATA_EXCLUSAO           DATE     := NULL,
      pCODIGO_USUARIO_ENVIO    VARCHAR2 := NULL,
      pCODIGO_USUARIO_GERACAO  VARCHAR2 := NULL,
      pCODIGO_USUARIO_EXCLUSAO VARCHAR2 := NULL,
      retornarRegistro         CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vQTD NUMBER; 
  BEGIN
    IF pCODIGO_PAGAMENTO IS NULL THEN
      SELECT SQ_PAGAMENTO.nextval
      INTO pCODIGO_PAGAMENTO
      FROM dual;
      INSERT
      INTO TB_PAGAMENTO
        (
          CODIGO_PAGAMENTO,
          BANCO,
          AGENCIA,
          CONTA,
          DATA_PAGAMENTO,
          VALOR_PAGAMENTO,
          STATUS_PAGAMENTO,
          CODIGO_FAVORECIDO,
          CODIGO_EMPRESA_GRUPO,
          DATA_ENVIO,
          DATA_GERACAO,
          DATA_EXCLUSAO,
          CODIGO_USUARIO_ENVIO,
          CODIGO_USUARIO_GERACAO,
          CODIGO_USUARIO_EXCLUSAO
        )
        VALUES
        (
          pCODIGO_PAGAMENTO,
          pBANCO,
          pAGENCIA,
          pCONTA,
          pDATA_PAGAMENTO,
          pVALOR_PAGAMENTO,
          pSTATUS_PAGAMENTO,
          pCODIGO_FAVORECIDO,
          pCODIGO_EMPRESA_GRUPO,
          pDATA_ENVIO,
          pDATA_GERACAO,
          pDATA_EXCLUSAO,
          pCODIGO_USUARIO_ENVIO,
          pCODIGO_USUARIO_GERACAO,
          pCODIGO_USUARIO_EXCLUSAO
        );
    ELSE
      UPDATE TB_PAGAMENTO
      SET BANCO                 = pBANCO,
        AGENCIA                 = pAGENCIA,
        CONTA                   = pCONTA,
        DATA_PAGAMENTO          = pDATA_PAGAMENTO,
        VALOR_PAGAMENTO         = pVALOR_PAGAMENTO,
        STATUS_PAGAMENTO        = pSTATUS_PAGAMENTO,
        CODIGO_FAVORECIDO       = pCODIGO_FAVORECIDO,
        CODIGO_EMPRESA_GRUPO    = pCODIGO_EMPRESA_GRUPO,
        DATA_ENVIO              = pDATA_ENVIO,
        DATA_GERACAO            = pDATA_GERACAO,
        DATA_EXCLUSAO           = pDATA_EXCLUSAO,
        CODIGO_USUARIO_ENVIO    = pCODIGO_USUARIO_ENVIO,
        CODIGO_USUARIO_GERACAO  = pCODIGO_USUARIO_GERACAO,
        CODIGO_USUARIO_EXCLUSAO = pCODIGO_USUARIO_EXCLUSAO
      WHERE CODIGO_PAGAMENTO    = pCODIGO_PAGAMENTO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_PAGAMENTO_B( pCODIGO_PAGAMENTO => pCODIGO_PAGAMENTO, retcur => retcur);
    END IF;
  END PR_PAGAMENTO_S;
  PROCEDURE PR_PAGAMENTO_B(
      pCODIGO_PAGAMENTO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_PAGAMENTO ,
    A.BANCO ,
    A.DATA_PAGAMENTO ,
    A.VALOR_PAGAMENTO ,
    A.STATUS_PAGAMENTO ,
    A.AGENCIA ,
    A.CONTA ,
    A.CODIGO_FAVORECIDO ,
    B.CNPJ_FAVORECIDO ,
    B.NOME_FAVORECIDO ,
    A.DATA_LIQUIDACAO_MANUAL ,
    A.CODIGO_EMPRESA_GRUPO ,
    A.DATA_ENVIO ,
    A.DATA_GERACAO ,
    A.DATA_EXCLUSAO ,
    A.CODIGO_USUARIO_ENVIO ,
    A.CODIGO_USUARIO_GERACAO ,
    A.CODIGO_USUARIO_EXCLUSAO FROM TB_PAGAMENTO A left join TB_FAVORECIDO B ON A.CODIGO_FAVORECIDO = B.CODIGO_FAVORECIDO WHERE A.CODIGO_PAGAMENTO = pCODIGO_PAGAMENTO;
  END PR_PAGAMENTO_B;
  PROCEDURE PR_PAGAMENTO_L(
      pFILTRO_CODIGO_FAVORECIDO  NUMBER   := NULL,
      pFILTRO_DATA_PAGAMENTO     VARCHAR2 := NULL,
      pFILTRO_DATA_PAGAMENTO_FIM VARCHAR2 := NULL,
      pFILTRO_DATA_LIQUIDACAO    VARCHAR2 := NULL,
      pFILTRO_BANCO              VARCHAR2 := NULL,
      pFILTRO_AGENCIA            VARCHAR2 := NULL,
      pFILTRO_CONTA              VARCHAR2 := NULL,
      pFILTRO_STATUS_PAGAMENTO   CHAR     := NULL,
      pFILTRO_PAGAMENTO_NEGATIVO CHAR     := NULL,
      pFILTRO_DATA_ENVIO_DE      VARCHAR2 := NULL,
      pFILTRO_DATA_ENVIO_ATE     VARCHAR2 := NULL,
      pFILTRO_DATA_GERACAO_DE    VARCHAR2 := NULL,
      pFILTRO_DATA_GERACAO_ATE   VARCHAR2 := NULL,
      pRETORNAR_EXCLUIDOS        CHAR     := '0',
      pRETORNAR_QUANTIDADE       CHAR     := '0',
      pRETORNAR_REFERENCIAS      CHAR     := '0',
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery VARCHAR2(20000); 
    vWhere CLOB;            
    formatoData  VARCHAR2(22); 
    statusFiltro CHAR;
  BEGIN
    vWhere                       := '';
    formatoData                  := 'yyyy-mm-dd HH24:MI:SS/';
    IF pFILTRO_PAGAMENTO_NEGATIVO = 'S' THEN
      statusFiltro               := '1';
    ELSE
      statusFiltro := pFILTRO_STATUS_PAGAMENTO;
    END IF;
    IF pRETORNAR_QUANTIDADE    = '0' THEN
      vQuery                  := '  select rownum, resultado.* from(select A.CODIGO_PAGAMENTO       
,  A.BANCO       
,  A.DATA_LIQUIDACAO_MANUAL       
,  A.DATA_PAGAMENTO       
,  A.VALOR_PAGAMENTO       
,  A.STATUS_PAGAMENTO       
,  A.AGENCIA       
,  A.CONTA       
,  A.CODIGO_FAVORECIDO       
,  B.CNPJ_FAVORECIDO       
,  B.NOME_FAVORECIDO       
,  A.CODIGO_EMPRESA_GRUPO       
,  A.DATA_ENVIO        
,   A.DATA_GERACAO        
,   A.DATA_EXCLUSAO        
,   A.CODIGO_USUARIO_ENVIO        
,   A.CODIGO_USUARIO_GERACAO       
,   A.CODIGO_USUARIO_EXCLUSAO        
,   C.CODIGO_TIPO_DOC_MATERA       
,   C.CODIGO_TIPO_PROD_SERV_MATERA
,   C.CODIGO_CONTA_CONTABIL ';
      IF pRETORNAR_REFERENCIAS = '1' THEN
        vQuery                := vQuery || ' ,   C.CODIGO_REFERENCIA            
,   C.VALOR_PAGAMENTO AS VALOR_PAGAMENTO_REFERENCIA ';
      ELSE
        vQuery := vQuery || ' , null as CODIGO_REFERENCIA            
, null AS VALOR_PAGAMENTO_REFERENCIA ';
      END IF;
      vQuery                  := vQuery || ' from TB_PAGAMENTO A left join TB_FAVORECIDO B           
on A.CODIGO_FAVORECIDO = B.CODIGO_FAVORECIDO';
      IF pRETORNAR_REFERENCIAS = '1' THEN
        vQuery                := vQuery || '  left join TB_PAGAMENTO_REFERENCIA C          
on A.CODIGO_PAGAMENTO = C.CODIGO_PAGAMENTO';
      END IF;
      vQuery := vQuery || ' left join TB_CONTA_FAV_CONFIG_MATERA C           
on A.CODIGO_CONTA_FAVORECIDO = C.CODIGO_CONTA_FAVORECIDO and A.CODIGO_PRODUTO = C.CODIGO_PRODUTO ';
    ELSE
      vQuery := '  select resultado.qtde from (select count(*) as qtde           
from TB_PAGAMENTO A left join TB_FAVORECIDO B           
on A.CODIGO_FAVORECIDO = B.CODIGO_FAVORECIDO';
    END IF;
    IF (pFILTRO_CODIGO_FAVORECIDO IS NOT NULL) THEN
      vWhere                      := vWhere || ' A.CODIGO_FAVORECIDO = ' || pFILTRO_CODIGO_FAVORECIDO || ' and ';
    END IF;
    IF (pFILTRO_DATA_PAGAMENTO IS NOT NULL) THEN
      vWhere                   := vWhere || 'A.DATA_PAGAMENTO >= to_date(''' || pFILTRO_DATA_PAGAMENTO || ' 00:00:00'',''' || formatoData || ''') and ';
    END IF;
    IF (pFILTRO_DATA_PAGAMENTO_FIM IS NOT NULL) THEN
      vWhere                       := vWhere || 'A.DATA_PAGAMENTO <= to_date(''' || pFILTRO_DATA_PAGAMENTO_FIM || ' 23:59:59'',''' || formatoData || ''') and ';
    END IF;
    IF (pFILTRO_DATA_LIQUIDACAO IS NOT NULL) THEN
      vWhere                    := vWhere || ' A.DATA_LIQUIDACAO_MANUAL = to_date(''' || TO_CHAR(pFILTRO_DATA_LIQUIDACAO, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_BANCO IS NOT NULL) THEN
      vWhere          := vWhere || ' A.BANCO = ''' || pFILTRO_BANCO || ''' and ';
    END IF;
    IF (pFILTRO_AGENCIA IS NOT NULL) THEN
      vWhere            := vWhere || ' A.AGENCIA = ''' || pFILTRO_AGENCIA || ''' and ';
    END IF;
    IF (pFILTRO_CONTA IS NOT NULL) THEN
      vWhere          := vWhere || ' A.CONTA = ''' || pFILTRO_CONTA || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_PAGAMENTO IS NOT NULL) THEN
      vWhere                     := vWhere || ' A.STATUS_PAGAMENTO = ' || pFILTRO_STATUS_PAGAMENTO || ' and ';
    END IF;
    IF (pFILTRO_PAGAMENTO_NEGATIVO IS NOT NULL) THEN
      vWhere                       := vWhere || ' A.VALOR_PAGAMENTO < 0 and ';
    END IF;
    IF (pFILTRO_DATA_GERACAO_DE IS NOT NULL) THEN
      vWhere                    := vWhere || 'A.DATA_GERACAO >= to_date(''' || pFILTRO_DATA_GERACAO_DE || ' 00:00:00'',''' || formatoData || ''') and ';
    END IF;
    IF (pFILTRO_DATA_GERACAO_ATE IS NOT NULL) THEN
      vWhere                     := vWhere || 'A.DATA_GERACAO <= to_date(''' || pFILTRO_DATA_GERACAO_ATE || ' 23:59:59'',''' || formatoData || ''') and ';
    END IF;
    IF (pFILTRO_DATA_ENVIO_DE IS NOT NULL) THEN
      vWhere                  := vWhere || 'A.DATA_ENVIO >=  to_date(''' || pFILTRO_DATA_ENVIO_DE || ' 00:00:00'',''' || formatoData || ''') and ';
    END IF;
    IF (pFILTRO_DATA_ENVIO_ATE IS NOT NULL) THEN
      vWhere                   := vWhere || 'A.DATA_ENVIO <= to_date(''' || pFILTRO_DATA_ENVIO_ATE || ' 23:59:59'',''' || formatoData || ''') and ';
    END IF;
    IF (pRETORNAR_EXCLUIDOS = '0') THEN
      vWhere               := vWhere || ' A.STATUS_PAGAMENTO != 2  and ';
    END IF;
    IF LENGTH(vWhere) > 0 THEN
      vWhere         := SUBSTR(vWhere, 0, LENGTH(vWhere) - 4);
      vWhere         := ' WHERE ' || vWhere;
    END IF;
    IF pRETORNAR_REFERENCIAS <> '1' THEN
      vQuery                 := vQuery || vWhere || ' order by A.CODIGO_PAGAMENTO ) resultado ';
    ELSE
      vQuery := vQuery || vWhere || ' order by A.CODIGO_PAGAMENTO, C.VALOR_PAGAMENTO DESC ) resultado ';
    END IF;
    PR_LOG_DB( PDESCRICAO_LOG => vQuery, PTIPO_ORIGEM => 'PR_PAGAMENTO_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => NULL );
    OPEN retcur FOR vQuery;
  END PR_PAGAMENTO_L;
  PROCEDURE PR_PAGAMENTO_R(
      pCODIGO_PAGAMENTO NUMBER)
  AS
  BEGIN
    DELETE TB_PAGAMENTO
    WHERE CODIGO_PAGAMENTO = pCODIGO_PAGAMENTO;
  END PR_PAGAMENTO_R;
  PROCEDURE PR_PAGAMENTO_R2(
      pCODIGO_PAGAMENTO NUMBER)
  AS
  BEGIN
    BEGIN
      UPDATE TB_PAGAMENTO
      SET status_pagamento   = 3
      WHERE CODIGO_PAGAMENTO = pCODIGO_PAGAMENTO;
      COMMIT;
    END;
    BEGIN
      UPDATE TB_TRANSACAO
      SET codigo_pagamento   = NULL,
        status_transacao     = status_pre_pagamento
      WHERE CODIGO_PAGAMENTO = pCODIGO_PAGAMENTO;
      UPDATE TB_PAGAMENTO
      SET status_pagamento   = 2,
        data_exclusao        = sysdate
      WHERE CODIGO_PAGAMENTO = pCODIGO_PAGAMENTO;
      COMMIT;
    END;
  END PR_PAGAMENTO_R2;
  PROCEDURE PR_PAGAMENTO_A(
      pFILTRO_CODIGO_PAGAMENTO NUMBER := NULL,
      pFILTRO_CODIGO_PROCESSO  NUMBER := NULL )
  AS
    CURSOR cursorPagamento
    IS
      SELECT B.CODIGO_PAGAMENTO ,
        SUM(C.VALOR_VENDA) VALOR_VENDA
      FROM TB_TRANSACAO B
      INNER JOIN TB_TRANSACAO_VENDA C
      ON B.CODIGO_TRANSACAO   = C.CODIGO_TRANSACAO
      WHERE B.CODIGO_PROCESSO = pFILTRO_CODIGO_PROCESSO
      GROUP BY B.CODIGO_PAGAMENTO;
    vCODIGO_PAGAMENTO NUMBER;
    vVALOR_PAGAMENTO  NUMBER;
  BEGIN
    DBMS_OUTPUT.put_line('Inicio');
    OPEN cursorPagamento;
    LOOP
      FETCH cursorPagamento
      INTO vCODIGO_PAGAMENTO,
        vVALOR_PAGAMENTO;
      EXIT
    WHEN cursorPagamento%NOTFOUND;
      DBMS_OUTPUT.put_line(vCODIGO_PAGAMENTO || ';' || vVALOR_PAGAMENTO);
      UPDATE TB_PAGAMENTO A
      SET VALOR_PAGAMENTO    = vVALOR_PAGAMENTO
      WHERE CODIGO_PAGAMENTO = vCODIGO_PAGAMENTO;
    END LOOP;
    CLOSE cursorPagamento;
  END PR_PAGAMENTO_A;
  PROCEDURE PR_PAGAMENTO_LIQUIDAR_MANUAL(
      pCODIGO_PAGAMENTO NUMBER,
      pDATA_LIQUIDACAO  DATE )
  AS
  BEGIN
    UPDATE TB_PAGAMENTO A
    SET DATA_LIQUIDACAO_MANUAL = pDATA_LIQUIDACAO,
      STATUS_PAGAMENTO         = 4
    WHERE CODIGO_PAGAMENTO     = pCODIGO_PAGAMENTO;
  END PR_PAGAMENTO_LIQUIDAR_MANUAL;
  PROCEDURE PR_PAGAMENTO_ATUALIZAR_STATUS(
      pCODIGO_PAGAMENTO        NUMBER,
      pSTATUS_PAGAMENTO        CHAR    := NULL,
      pCODIGO_USUARIO_ENVIO    VARCHAR := NULL,
      pCODIGO_USUARIO_EXCLUSAO VARCHAR := NULL )
  AS
  BEGIN
    IF pSTATUS_PAGAMENTO = 1 THEN
      UPDATE TB_PAGAMENTO A
      SET STATUS_PAGAMENTO   = 1,
        DATA_ENVIO           = sysdate(),
        CODIGO_USUARIO_ENVIO = pCODIGO_USUARIO_ENVIO
      WHERE CODIGO_PAGAMENTO = pCODIGO_PAGAMENTO;
    END IF;
    IF pSTATUS_PAGAMENTO = 2 THEN
      UPDATE TB_PAGAMENTO A
      SET STATUS_PAGAMENTO      = 2,
        DATA_EXCLUSAO           = sysdate(),
        CODIGO_USUARIO_EXCLUSAO = pCODIGO_USUARIO_EXCLUSAO
      WHERE CODIGO_PAGAMENTO    = pCODIGO_PAGAMENTO;
    END IF;
  END PR_PAGAMENTO_ATUALIZAR_STATUS;
  PROCEDURE PR_ESTABELECIMENTO_S(
      pCODIGO_ESTABELECIMENTO  NUMBER   := NULL,
      pCNPJ                    VARCHAR2 := NULL,
      pRAZAO_SOCIAL            VARCHAR2 := NULL,
      pBANCO                   VARCHAR2 := NULL,
      pAGENCIA                 VARCHAR2 := NULL,
      pCONTA                   VARCHAR2 := NULL,
      pCODIGO_FAVORECIDO       NUMBER   := NULL,
      pESTABELECIMENTO_CSU     VARCHAR2 := NULL,
      pESTABELECIMENTO_SITEF   VARCHAR2 := NULL,
      pCODIGO_EMPRESA_SUBGRUPO VARCHAR2 := NULL,
      retornarRegistro         CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_ESTABELECIMENTO NUMBER;
    vQTD                    NUMBER;
  BEGIN
    vCODIGO_ESTABELECIMENTO := pCODIGO_ESTABELECIMENTO;
    IF vCODIGO_ESTABELECIMENTO IS NULL AND pCNPJ IS NOT NULL THEN
      BEGIN
        SELECT CODIGO_ESTABELECIMENTO
        INTO vCODIGO_ESTABELECIMENTO
        FROM TB_ESTABELECIMENTO
        WHERE CNPJ = pCNPJ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        vCODIGO_ESTABELECIMENTO := NULL;
      END;
    END IF;
    IF vCODIGO_ESTABELECIMENTO IS NULL THEN
      SELECT SQ_ESTABELECIMENTO.nextval
      INTO vCODIGO_ESTABELECIMENTO
      FROM dual;
      INSERT
      INTO TB_ESTABELECIMENTO
        (
          CODIGO_ESTABELECIMENTO,
          CNPJ,
          RAZAO_SOCIAL,
          BANCO,
          AGENCIA,
          CONTA,
          ESTABELECIMENTO_CSU,
          ESTABELECIMENTO_SITEF,
          CODIGO_EMPRESA_SUBGRUPO
        )
        VALUES
        (
          vCODIGO_ESTABELECIMENTO,
          pCNPJ,
          pRAZAO_SOCIAL,
          pBANCO,
          pAGENCIA,
          pCONTA,
          pESTABELECIMENTO_CSU,
          pESTABELECIMENTO_SITEF,
          pCODIGO_EMPRESA_SUBGRUPO
        );
      IF pCODIGO_FAVORECIDO IS NOT NULL THEN
        INSERT
        INTO TB_ESTABELECIMENTO_FAVORECIDO
          (
            CODIGO_FAVORECIDO,
            CODIGO_ESTABELECIMENTO
          )
          VALUES
          (
            pCODIGO_FAVORECIDO,
            vCODIGO_ESTABELECIMENTO
          );
      END IF;
    ELSE
      UPDATE TB_ESTABELECIMENTO
      SET CNPJ                     = pCNPJ,
        RAZAO_SOCIAL               = pRAZAO_SOCIAL,
        BANCO                      = pBANCO,
        AGENCIA                    = pAGENCIA,
        CONTA                      = pCONTA,
        CODIGO_EMPRESA_SUBGRUPO    = pCODIGO_EMPRESA_SUBGRUPO,
        ESTABELECIMENTO_CSU        = pESTABELECIMENTO_CSU,
        ESTABELECIMENTO_SITEF      = pESTABELECIMENTO_SITEF
      WHERE CODIGO_ESTABELECIMENTO = vCODIGO_ESTABELECIMENTO;
      IF pCODIGO_FAVORECIDO IS NOT NULL THEN
        SELECT COUNT(1)
        INTO vQTD
        FROM TB_ESTABELECIMENTO_FAVORECIDO
        WHERE CODIGO_ESTABELECIMENTO = vCODIGO_ESTABELECIMENTO;
        IF vQTD = 0 THEN
          INSERT
          INTO TB_ESTABELECIMENTO_FAVORECIDO
            (
              CODIGO_FAVORECIDO,
              CODIGO_ESTABELECIMENTO
            )
            VALUES
            (
              pCODIGO_FAVORECIDO,
              vCODIGO_ESTABELECIMENTO
            );
        ELSE
          UPDATE TB_ESTABELECIMENTO_FAVORECIDO
          SET CODIGO_FAVORECIDO        = pCODIGO_FAVORECIDO,
            CODIGO_ESTABELECIMENTO     = vCODIGO_ESTABELECIMENTO
          WHERE CODIGO_ESTABELECIMENTO = vCODIGO_ESTABELECIMENTO;
        END IF;
      END IF;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_ESTABELECIMENTO_B( pCODIGO_ESTABELECIMENTO => vCODIGO_ESTABELECIMENTO, retcur => retcur);
    END IF;
  END PR_ESTABELECIMENTO_S;
  PROCEDURE PR_ESTABELECIMENTO_B(
      pCODIGO_ESTABELECIMENTO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_ESTABELECIMENTO,
    A.CNPJ,
    A.RAZAO_SOCIAL,
    A.BANCO,
    A.AGENCIA,
    A.CONTA,
    A.ESTABELECIMENTO_CSU,
    A.ESTABELECIMENTO_SITEF,
    A.CODIGO_EMPRESA_SUBGRUPO,
    B.NOME_EMPRESA_SUBGRUPO,
    C.CODIGO_EMPRESA_GRUPO,
    C.NOME_EMPRESA_GRUPO,
    D.CODIGO_FAVORECIDO,
    E.CNPJ_FAVORECIDO,
    E.NOME_FAVORECIDO FROM TB_ESTABELECIMENTO A left join TB_EMPRESA_SUBGRUPO B ON A.CODIGO_EMPRESA_SUBGRUPO = B.CODIGO_EMPRESA_SUBGRUPO left join TB_EMPRESA_GRUPO C ON B.CODIGO_EMPRESA_GRUPO = C.CODIGO_EMPRESA_GRUPO left join TB_ESTABELECIMENTO_FAVORECIDO D ON A.CODIGO_ESTABELECIMENTO = D.CODIGO_ESTABELECIMENTO left join TB_FAVORECIDO E ON D.CODIGO_FAVORECIDO = E.CODIGO_FAVORECIDO WHERE A.CODIGO_ESTABELECIMENTO = pCODIGO_ESTABELECIMENTO;
  END PR_ESTABELECIMENTO_B;
  PROCEDURE PR_ESTABELECIMENTO_L(
      pFILTRO_CODIGO_FAVORECIDO NUMBER   := NULL,
      pFILTRO_CNPJ              VARCHAR2 := NULL,
      pFILTRO_RAZAO_SOCIAL      VARCHAR2 := NULL,
      pFILTRO_CODIGO_GRUPO      NUMBER   := NULL,
      pFILTRO_CODIGO_SUBGRUPO   NUMBER   := NULL,
      pFILTRO_CODIGO_CSU        VARCHAR2 := NULL,
      pFILTRO_CODIGO_SITEF      VARCHAR2 := NULL,
      pFILTRO_CNPJ_FAVORECIDO   VARCHAR2 := NULL,
      maxLinhas                 INT      := 0,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(2000);
  BEGIN
    vQuery := '';
    IF pFILTRO_CODIGO_FAVORECIDO IS NOT NULL THEN
      vQuery                     := vQuery || 'D.CODIGO_FAVORECIDO = ''' || pFILTRO_CODIGO_FAVORECIDO || ''' and ';
    END IF;
    IF pFILTRO_CNPJ IS NOT NULL THEN
      vQuery        := vQuery || 'A.CNPJ = ''' || pFILTRO_CNPJ || ''' and ';
    END IF;
    IF pFILTRO_RAZAO_SOCIAL IS NOT NULL THEN
      vQuery                := vQuery || 'upper(A.RAZAO_SOCIAL) like ''' || upper('%' || pFILTRO_RAZAO_SOCIAL || '%') || ''' and ';
    END IF;
    IF pFILTRO_CODIGO_GRUPO IS NOT NULL THEN
      vQuery                := vQuery || 'C.CODIGO_EMPRESA_GRUPO = ''' || pFILTRO_CODIGO_GRUPO || ''' and ';
    END IF;
    IF pFILTRO_CODIGO_SUBGRUPO IS NOT NULL THEN
      vQuery                   := vQuery || 'B.CODIGO_EMPRESA_SUBGRUPO = ''' || pFILTRO_CODIGO_SUBGRUPO || ''' and ';
    END IF;
    IF pFILTRO_CODIGO_CSU IS NOT NULL THEN
      vQuery              := vQuery || 'A.ESTABELECIMENTO_CSU = ''' || pFILTRO_CODIGO_CSU || ''' and ';
    END IF;
    IF pFILTRO_CODIGO_SITEF IS NOT NULL THEN
      vQuery                := vQuery || 'A.ESTABELECIMENTO_SITEF = ''' || pFILTRO_CODIGO_SITEF || ''' and ';
    END IF;
    IF pFILTRO_CNPJ_FAVORECIDO IS NOT NULL THEN
      vQuery                   := vQuery || 'E.CNPJ_FAVORECIDO = ''' || pFILTRO_CNPJ_FAVORECIDO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := '  
select * from   
(  select       
A.CODIGO_ESTABELECIMENTO,      
A.CNPJ,      
A.RAZAO_SOCIAL,      
A.BANCO,      
A.AGENCIA,      
A.CONTA,      
A.ESTABELECIMENTO_CSU,      
A.ESTABELECIMENTO_SITEF,      
A.CODIGO_EMPRESA_SUBGRUPO,      
B.NOME_EMPRESA_SUBGRUPO,      
C.CODIGO_EMPRESA_GRUPO,      
C.NOME_EMPRESA_GRUPO,      
D.CODIGO_FAVORECIDO,      
E.CNPJ_FAVORECIDO,      
E.NOME_FAVORECIDO      
from      TB_ESTABELECIMENTO            A      
left join TB_EMPRESA_SUBGRUPO           B       on A.CODIGO_EMPRESA_SUBGRUPO    = B.CODIGO_EMPRESA_SUBGRUPO      
left join TB_EMPRESA_GRUPO              C       on B.CODIGO_EMPRESA_GRUPO       = C.CODIGO_EMPRESA_GRUPO      
left join TB_ESTABELECIMENTO_FAVORECIDO D       on A.CODIGO_ESTABELECIMENTO     = D.CODIGO_ESTABELECIMENTO      
left join TB_FAVORECIDO                 E       on D.CODIGO_FAVORECIDO          = E.CODIGO_FAVORECIDO  
' || vQuery || '   order by RAZAO_SOCIAL asc  
)  
';
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' where rownum <= ' || maxLinhas;
    END IF;
    OPEN retcur FOR vQuery;
  END PR_ESTABELECIMENTO_L;
  PROCEDURE PR_ESTABELECIMENTO_R(
      pCODIGO_ESTABELECIMENTO NUMBER)
  AS
  BEGIN
    DELETE TB_ESTABELECIMENTO_FAVORECIDO
    WHERE CODIGO_ESTABELECIMENTO = pCODIGO_ESTABELECIMENTO;
    DELETE TB_ESTABELECIMENTO
    WHERE CODIGO_ESTABELECIMENTO = pCODIGO_ESTABELECIMENTO;
  END PR_ESTABELECIMENTO_R;
  PROCEDURE PR_PROCESSO_S(
      pCODIGO_PROCESSO       NUMBER   := NULL,
      pTIPO_PROCESSO         VARCHAR2 := NULL,
      pTIPO_ESTAGIO_ATUAL    VARCHAR2 := NULL,
      pSERIALIZACAO_PROCESSO VARCHAR2 := NULL,
      pSTATUS_PROCESSO       CHAR     := NULL,
      pDATA_STATUS           VARCHAR2 := NULL,
      pSTATUS_ESTAGIO        CHAR     := NULL,
      pDATA_ESTAGIO          DATE     := NULL,
      pSTATUS_BLOQUEIO       CHAR     := NULL,
      pDATA_ATUALIZACAO      DATE     := NULL,
      retornarRegistro       CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_PROCESSO NUMBER;
  BEGIN
    vCODIGO_PROCESSO := pCODIGO_PROCESSO;
    IF vCODIGO_PROCESSO IS NULL THEN
      SELECT SQ_PROCESSO.nextval
      INTO vCODIGO_PROCESSO
      FROM dual;
      INSERT
      INTO TB_PROCESSO
        (
          CODIGO_PROCESSO,
          TIPO_PROCESSO,
          SERIALIZACAO_PROCESSO,
          DATA_INCLUSAO,
          STATUS_PROCESSO,
          DATA_STATUS,
          STATUS_ESTAGIO,
          DATA_ESTAGIO,
          TIPO_ESTAGIO_ATUAL,
          STATUS_BLOQUEIO,
          DATA_ATUALIZACAO
        )
        VALUES
        (
          vCODIGO_PROCESSO,
          pTIPO_PROCESSO,
          pSERIALIZACAO_PROCESSO,
          sysdate(),
          pSTATUS_PROCESSO,
          pDATA_STATUS,
          pSTATUS_ESTAGIO,
          pDATA_ESTAGIO,
          pTIPO_ESTAGIO_ATUAL,
          pSTATUS_BLOQUEIO,
          sysdate()
        );
    ELSE
      UPDATE TB_PROCESSO
      SET TIPO_PROCESSO       = pTIPO_PROCESSO,
        TIPO_ESTAGIO_ATUAL    = pTIPO_ESTAGIO_ATUAL,
        SERIALIZACAO_PROCESSO = pSERIALIZACAO_PROCESSO,
        STATUS_PROCESSO       = pSTATUS_PROCESSO,
        DATA_STATUS           = pDATA_STATUS,
        STATUS_ESTAGIO        = pSTATUS_ESTAGIO,
        DATA_ESTAGIO          = pDATA_ESTAGIO,
        STATUS_BLOQUEIO       = pSTATUS_BLOQUEIO,
        DATA_ATUALIZACAO      = pDATA_ATUALIZACAO
      WHERE CODIGO_PROCESSO   = vCODIGO_PROCESSO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_PROCESSO_B( pCODIGO_PROCESSO => vCODIGO_PROCESSO, retcur => retcur);
    END IF;
  END PR_PROCESSO_S;
  PROCEDURE PR_PROCESSO_B(
      pCODIGO_PROCESSO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_PROCESSO ,
    A.TIPO_PROCESSO ,
    A.TIPO_ESTAGIO_ATUAL ,
    A.SERIALIZACAO_PROCESSO ,
    A.DATA_INCLUSAO ,
    A.STATUS_PROCESSO ,
    A.DATA_STATUS ,
    A.STATUS_ESTAGIO ,
    A.DATA_ESTAGIO ,
    A.STATUS_BLOQUEIO ,
    a.data_atualizacao ,
    B.NOME_ARQUIVO FROM TB_PROCESSO A left join TB_ARQUIVO B ON B.CODIGO_PROCESSO = A.CODIGO_PROCESSO WHERE A.CODIGO_PROCESSO = pCODIGO_PROCESSO;
  END PR_PROCESSO_B;
  PROCEDURE PR_PROCESSO_L(
      pFILTRO_TIPO_PROCESSO       VARCHAR2 := NULL,
      pFILTRO_DATA_INCLUSAO       DATE     := NULL,
      pFILTRO_DATA_INCLUSAO_MAIOR DATE     := NULL,
      pFILTRO_DATA_INCLUSAO_MENOR DATE     := NULL,
      pFILTRO_DATA_STATUS         DATE     := NULL,
      pFILTRO_DATA_STATUS_MAIOR   DATE     := NULL,
      pFILTRO_DATA_STATUS_MENOR   DATE     := NULL,
      pFILTRO_STATUS_PROCESSO     VARCHAR2 := NULL,
      pFILTRO_STATUS_PROCESSO_DIF VARCHAR2 := NULL,
      pFILTRO_STATUS_BLOQUEIO     CHAR     := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    formatoData VARCHAR2(20); 
    vQuery      VARCHAR2(2000);
  BEGIN
    formatoData := 'yyyy-mm-dd';
    vQuery := '';
    IF pFILTRO_TIPO_PROCESSO IS NOT NULL THEN
      IF pFILTRO_TIPO_PROCESSO = 'ProcessoRelatorio' THEN
        vQuery                := vQuery || 'A.TIPO_PROCESSO like (''%' || pFILTRO_TIPO_PROCESSO || '%'') and ';
      ELSE
        IF pFILTRO_TIPO_PROCESSO = 'Processo' THEN
          vQuery                := vQuery || 'A.TIPO_PROCESSO like (''%' || pFILTRO_TIPO_PROCESSO || '%'') and ';
          vQuery                := vQuery || 'A.TIPO_PROCESSO not like (''%ProcessoRelatorio%'') and ';
        ELSE
          vQuery := vQuery || 'A.TIPO_PROCESSO = ''' || pFILTRO_TIPO_PROCESSO || ''' and ';
        END IF;
      END IF;
    END IF;
    IF pFILTRO_DATA_INCLUSAO_MAIOR IS NOT NULL THEN
      vQuery := vQuery || 'A.DATA_INCLUSAO >= ''' || pFILTRO_DATA_INCLUSAO_MAIOR || ''' and '; 
    END IF;
    IF pFILTRO_DATA_INCLUSAO_MENOR IS NOT NULL THEN
      vQuery                       := vQuery || '  A.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_INCLUSAO_MENOR, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'')' || ' and ';
    END IF; 
    IF pFILTRO_DATA_STATUS_MAIOR IS NOT NULL THEN
      vQuery := vQuery || 'A.DATA_STATUS >= ''' || pFILTRO_DATA_STATUS_MAIOR || ''' and '; 
    END IF;
    IF pFILTRO_DATA_STATUS_MENOR IS NOT NULL THEN
      vQuery := vQuery || 'A.DATA_STATUS <= ''' || pFILTRO_DATA_STATUS_MENOR || ''' and '; 
    END IF;
    IF pFILTRO_STATUS_PROCESSO IS NOT NULL THEN
      vQuery                   := vQuery || 'A.STATUS_PROCESSO = ''' || pFILTRO_STATUS_PROCESSO || ''' and ';
    END IF;
    IF pFILTRO_STATUS_PROCESSO_DIF IS NOT NULL THEN
      vQuery                       := vQuery || 'A.STATUS_PROCESSO != ''' || pFILTRO_STATUS_PROCESSO_DIF || ''' and ';
    END IF;
    IF pFILTRO_STATUS_BLOQUEIO IS NOT NULL THEN
      vQuery                   := vQuery || 'A.STATUS_BLOQUEIO = ''' || pFILTRO_STATUS_BLOQUEIO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'select *   
from   
(    
select     
A.CODIGO_PROCESSO  
, A.TIPO_PROCESSO  
, A.TIPO_ESTAGIO_ATUAL  
, A.SERIALIZACAO_PROCESSO  
, A.DATA_INCLUSAO  
, A.STATUS_PROCESSO  
, A.DATA_STATUS  
, A.STATUS_ESTAGIO  
, A.DATA_ESTAGIO  
, A.STATUS_BLOQUEIO  
, A.DATA_ATUALIZACAO  
, ''0'' AS STATUS_PAGAMENTO  
,       ( select listagg(NOME_ARQUIVO, '', '')             
within group (order by CODIGO_ARQUIVO) as list             
from TB_ARQUIVO             
where CODIGO_PROCESSO = A.CODIGO_PROCESSO          
) as    
NOME_ARQUIVO    
from  TB_PROCESSO A  
' || vQuery || ' order by A.CODIGO_PROCESSO desc  
) ';
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' where rownum <= ' || maxLinhas;
    END IF;
    OPEN retcur FOR vQuery;
  END PR_PROCESSO_L;
  PROCEDURE PR_PROCESSO_R(
      pCODIGO_PROCESSO NUMBER)
  AS
  BEGIN
    DELETE TB_PROCESSO
    WHERE CODIGO_PROCESSO = pCODIGO_PROCESSO;
  END PR_PROCESSO_R;
  PROCEDURE PR_LOG_S(
      pCODIGO_LOG     NUMBER   := NULL,
      pDATA_LOG       DATE     := NULL,
      pTIPO_LOG       VARCHAR2 := NULL,
      pCODIGO_USUARIO VARCHAR2 := NULL,
      pDESCRICAO_LOG  VARCHAR2 := NULL,
      pSERIALIZACAO_LOG CLOB   := NULL,
      pTIPO_ORIGEM     VARCHAR2    := NULL,
      pCODIGO_ORIGEM   NUMBER      := NULL,
      retornarRegistro CHAR        := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_LOG NUMBER;
  BEGIN
    vCODIGO_LOG := pCODIGO_LOG;
    IF vCODIGO_LOG IS NULL THEN
      SELECT SQ_LOG.nextval
      INTO vCODIGO_LOG
      FROM dual;
      INSERT
      INTO TB_LOG
        (
          CODIGO_LOG,
          DATA_LOG,
          DATA_BD_LOG,
          TIPO_LOG,
          CODIGO_USUARIO,
          DESCRICAO_LOG,
          SERIALIZACAO_LOG,
          TIPO_ORIGEM,
          CODIGO_ORIGEM
        )
        VALUES
        (
          vCODIGO_LOG,
          pDATA_LOG,
          sysdate(),
          pTIPO_LOG,
          pCODIGO_USUARIO,
          pDESCRICAO_LOG,
          pSERIALIZACAO_LOG,
          pTIPO_ORIGEM,
          pCODIGO_ORIGEM
        );
    ELSE
      UPDATE TB_LOG
      SET DATA_LOG       = pDATA_LOG,
        DATA_BD_LOG      = sysdate(),
        TIPO_LOG         = pTIPO_LOG,
        CODIGO_USUARIO   = pCODIGO_USUARIO,
        DESCRICAO_LOG    = pDESCRICAO_LOG,
        SERIALIZACAO_LOG = pSERIALIZACAO_LOG,
        TIPO_ORIGEM      = pTIPO_ORIGEM,
        CODIGO_ORIGEM    = pCODIGO_ORIGEM
      WHERE CODIGO_LOG   = vCODIGO_LOG;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_LOG_B( pCODIGO_LOG => vCODIGO_LOG, retcur => retcur);
    END IF;
  END PR_LOG_S;
  PROCEDURE PR_LOG_B(
      pCODIGO_LOG NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_LOG ,
    A.DATA_LOG ,
    A.TIPO_LOG ,
    A.CODIGO_USUARIO ,
    A.DESCRICAO_LOG ,
    A.SERIALIZACAO_LOG ,
    A.TIPO_ORIGEM ,
    A.CODIGO_ORIGEM FROM TB_LOG A WHERE A.CODIGO_LOG = pCODIGO_LOG;
  END PR_LOG_B;
  PROCEDURE PR_LOG_L(
      pFILTRO_TIPO_LOG       VARCHAR2 := NULL,
      pFILTRO_DATA_LOG       DATE     := NULL,
      pFILTRO_DATA_LOG_MAIOR DATE     := NULL,
      pFILTRO_DATA_LOG_MENOR DATE     := NULL,
      pFILTRO_CODIGO_USUARIO VARCHAR2 := NULL,
      pFILTRO_TIPO_ORIGEM    VARCHAR2 := NULL,
      pFILTRO_CODIGO_ORIGEM  NUMBER   := NULL,
      retcur OUT TP_REFCURSOR,
      maxLinhas INT := 0 )
  AS
    vQuery      VARCHAR2(2000);
    vHORA000000 CHAR(9); 
    vHORA235959 CHAR(9); 
  BEGIN
    vQuery      := '';
    vHORA000000 := ' 00:00:00';
    vHORA235959 := ' 23:59:59';
    IF pFILTRO_TIPO_LOG IS NOT NULL THEN
      vQuery := vQuery || 'upper(A.TIPO_LOG) like ''' || upper('%' || pFILTRO_TIPO_LOG || '%') || ''' and '; 
    END IF;
    IF pFILTRO_DATA_LOG IS NOT NULL THEN
      vQuery            := vQuery || 'A.DATA_LOG = ''' || pFILTRO_DATA_LOG || ''' and ';
    END IF;
    IF pFILTRO_DATA_LOG_MENOR IS NOT NULL THEN
      vQuery := vQuery || 'A.DATA_LOG >= TO_DATE(''' || pFILTRO_DATA_LOG_MENOR || vHORA000000 || ''', ''dd/mm/yy hh24:mi:ss'')  and '; 
    END IF;
    IF pFILTRO_DATA_LOG_MAIOR IS NOT NULL THEN
      vQuery := vQuery || 'A.DATA_LOG <= TO_DATE(''' || pFILTRO_DATA_LOG_MAIOR || vHORA235959 || ''', ''dd/mm/yy hh24:mi:ss'')  and '; 
    END IF;
    IF pFILTRO_CODIGO_USUARIO IS NOT NULL THEN
      vQuery                  := vQuery || 'upper(A.CODIGO_USUARIO) like ''' || upper('%' || pFILTRO_CODIGO_USUARIO || '%') || ''' and ';
    END IF;
    IF pFILTRO_TIPO_ORIGEM IS NOT NULL THEN
      vQuery := vQuery || 'upper(A.TIPO_ORIGEM) like ''' || upper('%' || pFILTRO_TIPO_ORIGEM || '%') || ''' and '; 
    END IF;
    IF pFILTRO_CODIGO_ORIGEM IS NOT NULL THEN
      vQuery := vQuery || 'A.CODIGO_ORIGEM = ''' || pFILTRO_CODIGO_ORIGEM || ''' and '; 
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'select * from (                    
select A.CODIGO_LOG,                           
A.DATA_LOG,                           
A.DATA_BD_LOG,                           
A.TIPO_LOG,                           
A.CODIGO_USUARIO,                           
A.DESCRICAO_LOG,                           
A.SERIALIZACAO_LOG,                           
A.TIPO_ORIGEM,                           
A.CODIGO_ORIGEM                      
from TB_LOG A ' || vQuery || ' order by A.CODIGO_LOG desc, A.DATA_LOG desc)';
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' where rownum <= ' || maxLinhas;
    END IF;
    OPEN retcur FOR vQuery;
  END PR_LOG_L;
  PROCEDURE PR_LOG_R(
      pCODIGO_LOG NUMBER)
  AS
    vCODIGO_ORIGEM NUMBER        := NULL;
    vTIPO_ORIGEM   VARCHAR2(200) := NULL;
    vDATA_BASE     DATE;
  BEGIN
    IF pCODIGO_LOG IS NOT NULL THEN
      DELETE TB_LOG WHERE CODIGO_LOG = pCODIGO_LOG;
    END IF;
    IF pCODIGO_LOG   IS NULL THEN
      vDATA_BASE     := sysdate - 1;
      vCODIGO_ORIGEM := 0;
      DELETE FROM TB_LOG WHERE CODIGO_ORIGEM IS NULL AND DATA_LOG <= vDATA_BASE;
      DELETE
      FROM TB_LOG
      WHERE CODIGO_ORIGEM = vCODIGO_ORIGEM
      AND DATA_LOG       <= vDATA_BASE;
    END IF;
  END PR_LOG_R;
  PROCEDURE PR_PRODUTO_PLANO_S(
      pCODIGO_PRODUTO NUMBER := NULL,
      pCODIGO_PLANO   NUMBER := NULL )
  AS
    vQTD NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO vQTD
    FROM TB_PRODUTO_PLANO
    WHERE CODIGO_PRODUTO = pCODIGO_PRODUTO
    AND CODIGO_PLANO     = pCODIGO_PLANO;
    IF vQTD              = 0 THEN
      INSERT
      INTO TB_PRODUTO_PLANO
        (
          CODIGO_PRODUTO,
          CODIGO_PLANO
        )
        VALUES
        (
          pCODIGO_PRODUTO,
          pCODIGO_PLANO
        );
    END IF;
  END PR_PRODUTO_PLANO_S;
  PROCEDURE PR_PRODUTO_PLANO_R
    (
      pCODIGO_PRODUTO NUMBER := NULL,
      pCODIGO_PLANO   NUMBER := NULL
    )
  AS
  BEGIN
    DELETE
    FROM TB_PRODUTO_PLANO
    WHERE CODIGO_PRODUTO = pCODIGO_PRODUTO
    AND CODIGO_PLANO     = pCODIGO_PLANO;
  END PR_PRODUTO_PLANO_R;
  PROCEDURE PR_PRODUTO_S(
      pCODIGO_PRODUTO      NUMBER   := NULL,
      pCODIGO_PRODUTO_TSYS VARCHAR2 := NULL,
      pNOME_PRODUTO        VARCHAR2 := NULL,
      pDESCRICAO_PRODUTO   VARCHAR2 := NULL,
      pCEDIVEL             CHAR     := NULL,
      pPRAZO_PGMTO         NUMBER   := NULL,
      pDIAS_VENCIDOS       NUMBER   := NULL,
      pDIAS_VENCIDOS_AJ    NUMBER   := NULL,
      retornarRegistro     CHAR     := 'N',
      retcur OUT TP_REFCURSOR,
      retcurPlanos OUT TP_REFCURSOR )
  AS
    vCODIGO_PRODUTO NUMBER;
    vQTDE_PRODUTOS  NUMBER;
  BEGIN
    vCODIGO_PRODUTO := pCODIGO_PRODUTO;
    IF vCODIGO_PRODUTO IS NULL THEN
      IF pCODIGO_PRODUTO_TSYS IS NOT NULL THEN
        SELECT COUNT(1)
        INTO vQTDE_PRODUTOS
        FROM TB_PRODUTO
        WHERE CODIGO_PRODUTO_TSYS = pCODIGO_PRODUTO_TSYS;
        IF vQTDE_PRODUTOS         > 0 THEN
          SELECT CODIGO_PRODUTO
          INTO vCODIGO_PRODUTO
          FROM TB_PRODUTO
          WHERE CODIGO_PRODUTO_TSYS = pCODIGO_PRODUTO_TSYS;
        END IF;
      END IF;
      IF vCODIGO_PRODUTO IS NULL THEN
        SELECT SQ_PRODUTO.nextval
        INTO vCODIGO_PRODUTO
        FROM dual;
        INSERT
        INTO TB_PRODUTO
          (
            CODIGO_PRODUTO,
            NOME_PRODUTO,
            DESCRICAO_PRODUTO,
            CEDIVEL,
            CODIGO_PRODUTO_TSYS,
            PRAZO_PGMTO,
            DIAS_VENCIDOS,
            DIAS_VENCIDOS_AJ
          )
          VALUES
          (
            vCODIGO_PRODUTO,
            pNOME_PRODUTO,
            pDESCRICAO_PRODUTO,
            pCEDIVEL,
            pCODIGO_PRODUTO_TSYS,
            pPRAZO_PGMTO,
            pDIAS_VENCIDOS,
            pDIAS_VENCIDOS_AJ
          );
      ELSE
        UPDATE TB_PRODUTO
        SET CODIGO_PRODUTO_TSYS = NVL(pCODIGO_PRODUTO_TSYS, CODIGO_PRODUTO_TSYS),
          DESCRICAO_PRODUTO     = NVL(pDESCRICAO_PRODUTO, DESCRICAO_PRODUTO),
          NOME_PRODUTO          = NVL(pNOME_PRODUTO, NOME_PRODUTO),
          CEDIVEL               = NVL(pCEDIVEL, CEDIVEL),
          PRAZO_PGMTO           = NVL(pPRAZO_PGMTO, PRAZO_PGMTO),
          DIAS_VENCIDOS         = NVL(pDIAS_VENCIDOS, DIAS_VENCIDOS),
          DIAS_VENCIDOS_AJ      = NVL(pDIAS_VENCIDOS_AJ, DIAS_VENCIDOS_AJ)
        WHERE CODIGO_PRODUTO    = vCODIGO_PRODUTO;
      END IF;
    ELSE
      UPDATE TB_PRODUTO
      SET CODIGO_PRODUTO_TSYS = NVL(pCODIGO_PRODUTO_TSYS, CODIGO_PRODUTO_TSYS),
        DESCRICAO_PRODUTO     = NVL(pDESCRICAO_PRODUTO, DESCRICAO_PRODUTO),
        NOME_PRODUTO          = NVL(pNOME_PRODUTO, NOME_PRODUTO),
        CEDIVEL               = NVL(pCEDIVEL, CEDIVEL),
        PRAZO_PGMTO           = NVL(pPRAZO_PGMTO, PRAZO_PGMTO),
        DIAS_VENCIDOS         = NVL(pDIAS_VENCIDOS, DIAS_VENCIDOS),
        DIAS_VENCIDOS_AJ      = NVL(pDIAS_VENCIDOS_AJ, DIAS_VENCIDOS_AJ)
      WHERE CODIGO_PRODUTO    = vCODIGO_PRODUTO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_PRODUTO_B( pCODIGO_PRODUTO => vCODIGO_PRODUTO, retcur => retcur, retcurPlanos => retcurPlanos);
    END IF;
  END PR_PRODUTO_S;
  PROCEDURE PR_PRODUTO_B(
      pCODIGO_PRODUTO NUMBER,
      retcur OUT TP_REFCURSOR,
      retcurPlanos OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_PRODUTO ,
    A.CODIGO_PRODUTO_TSYS ,
    A.NOME_PRODUTO ,
    A.DESCRICAO_PRODUTO ,
    A.CEDIVEL ,
    A.PRAZO_PGMTO ,
    A.DIAS_VENCIDOS ,
    A.DIAS_VENCIDOS_AJ FROM TB_PRODUTO A WHERE A.CODIGO_PRODUTO                                = pCODIGO_PRODUTO;
    OPEN retcurPlanos FOR SELECT * FROM TB_PRODUTO_PLANO PP JOIN TB_PLANO P ON PP.CODIGO_PLANO = P.CODIGO_PLANO WHERE PP.CODIGO_PRODUTO = pCODIGO_PRODUTO;
  END PR_PRODUTO_B;
  PROCEDURE PR_PRODUTO_L(
      pNOME_PRODUTO VARCHAR2 := NULL,
      maxLinhas     INT      := 0,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(500);
  BEGIN
    vQuery := '';
    IF pNOME_PRODUTO IS NOT NULL THEN
      vQuery         := vQuery || 'A.NOME_PRODUTO = ''' || pNOME_PRODUTO || ''' and ';
    END IF;
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || 'rownum <= ' || maxLinhas || ' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'select  A.CODIGO_PRODUTO,                         
A.CODIGO_PRODUTO_TSYS,                         
A.NOME_PRODUTO,                         
A.DESCRICAO_PRODUTO,                         
A.CEDIVEL,                         
A.PRAZO_PGMTO,                         
A.DIAS_VENCIDOS,                         
A.DIAS_VENCIDOS_AJ                    
from TB_PRODUTO A ' || vQuery || ' order by A.CODIGO_PRODUTO';
    OPEN retcur FOR vQuery;
  END PR_PRODUTO_L;
  PROCEDURE PR_PRODUTO_R(
      pCODIGO_PRODUTO       NUMBER,
      pREMOVER_ASSOC_PLANOS CHAR )
  AS
  BEGIN
    IF pREMOVER_ASSOC_PLANOS IS NOT NULL AND pREMOVER_ASSOC_PLANOS = 'S' THEN
      DELETE TB_PRODUTO_PLANO WHERE CODIGO_PRODUTO = pCODIGO_PRODUTO;
    END IF;
    DELETE TB_PRODUTO WHERE CODIGO_PRODUTO = pCODIGO_PRODUTO;
  END PR_PRODUTO_R;
  PROCEDURE PR_PLANO_S(
      pCODIGO_PLANO        NUMBER   := NULL,
      pCODIGO_PLANO_TSYS   VARCHAR2 := NULL,
      pNOME_PLANO          VARCHAR2 := NULL,
      pDESCRICAO_PLANO     VARCHAR2 := NULL,
      pQUANTIDADE_PARCELAS NUMBER   := NULL,
      pCOMISSAO            NUMBER   := NULL,
      pATIVO               CHAR     := NULL,
      retornarRegistro     CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_PLANO NUMBER;
    vQTDE_PLANOS  NUMBER;
  BEGIN
    vCODIGO_PLANO := pCODIGO_PLANO;
    IF vCODIGO_PLANO IS NULL THEN
      IF pCODIGO_PLANO_TSYS IS NOT NULL THEN
        SELECT COUNT(1)
        INTO vQTDE_PLANOS
        FROM TB_PLANO
        WHERE CODIGO_PLANO_TSYS = pCODIGO_PLANO_TSYS;
        IF vQTDE_PLANOS         > 0 THEN
          SELECT CODIGO_PLANO
          INTO vCODIGO_PLANO
          FROM TB_PLANO
          WHERE CODIGO_PLANO_TSYS = pCODIGO_PLANO_TSYS;
        END IF;
      END IF;
      IF vCODIGO_PLANO IS NULL THEN
        SELECT SQ_PLANO.nextval
        INTO vCODIGO_PLANO
        FROM dual;
        INSERT
        INTO TB_PLANO
          (
            CODIGO_PLANO,
            NOME_PLANO,
            DESCRICAO_PLANO,
            QUANTIDADE_PARCELAS,
            COMISSAO,
            ATIVO,
            CODIGO_PLANO_TSYS
          )
          VALUES
          (
            vCODIGO_PLANO,
            pNOME_PLANO,
            pDESCRICAO_PLANO,
            pQUANTIDADE_PARCELAS,
            pCOMISSAO,
            pATIVO,
            pCODIGO_PLANO_TSYS
          );
      ELSE
        UPDATE TB_PLANO
        SET CODIGO_PLANO_TSYS = NVL(pCODIGO_PLANO_TSYS, CODIGO_PLANO_TSYS),
          NOME_PLANO          = NVL(pNOME_PLANO, NOME_PLANO),
          DESCRICAO_PLANO     = NVL(pDESCRICAO_PLANO, DESCRICAO_PLANO),
          QUANTIDADE_PARCELAS = NVL(pQUANTIDADE_PARCELAS, QUANTIDADE_PARCELAS),
          COMISSAO            = NVL(pCOMISSAO, COMISSAO),
          ATIVO               = NVL(pATIVO, ATIVO)
        WHERE CODIGO_PLANO    = vCODIGO_PLANO;
      END IF;
    ELSE
      UPDATE TB_PLANO
      SET CODIGO_PLANO_TSYS = NVL(pCODIGO_PLANO_TSYS, CODIGO_PLANO_TSYS),
        NOME_PLANO          = NVL(pNOME_PLANO, NOME_PLANO),
        DESCRICAO_PLANO     = NVL(pDESCRICAO_PLANO, DESCRICAO_PLANO),
        QUANTIDADE_PARCELAS = NVL(pQUANTIDADE_PARCELAS, QUANTIDADE_PARCELAS),
        COMISSAO            = NVL(pCOMISSAO, COMISSAO),
        ATIVO               = NVL(pATIVO, ATIVO)
      WHERE CODIGO_PLANO    = vCODIGO_PLANO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_PLANO_B( pCODIGO_PLANO => vCODIGO_PLANO, retcur => retcur);
    END IF;
  END PR_PLANO_S;
  PROCEDURE PR_PLANO_B(
      pCODIGO_PLANO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_PLANO ,
    A.CODIGO_PLANO_TSYS ,
    A.NOME_PLANO ,
    A.DESCRICAO_PLANO ,
    A.QUANTIDADE_PARCELAS ,
    A.COMISSAO ,
    A.ATIVO FROM TB_PLANO A WHERE A.CODIGO_PLANO = pCODIGO_PLANO;
  END PR_PLANO_B;
  PROCEDURE PR_PLANO_L(
      pATIVO               CHAR     := NULL,
      pNOME_PLANO          VARCHAR2 := NULL,
      pDESCRICAO_PLANO     VARCHAR2 := NULL,
      pQUANTIDADE_PARCELAS NUMBER   := NULL,
      pCOMISSAO            NUMBER   := NULL,
      pCODIGO_PLANO_TSYS   VARCHAR2 := NULL,
      pCODIGO_PRODUTO      NUMBER   := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(1000);
  BEGIN
    vQuery := '';
    IF pATIVO IS NOT NULL THEN
      vQuery  := vQuery || 'A.ATIVO = ''' || pATIVO || ''' and ';
    END IF;
    IF pNOME_PLANO IS NOT NULL THEN
      vQuery       := vQuery || 'A.NOME_PLANO = ''' || pNOME_PLANO || ''' and ';
    END IF;
    IF pCODIGO_PLANO_TSYS IS NOT NULL THEN
      vQuery              := vQuery || 'A.CODIGO_PLANO_TSYS = ''' || pCODIGO_PLANO_TSYS || ''' and ';
    END IF;
    IF pCODIGO_PRODUTO IS NOT NULL THEN
      vQuery := vQuery || 'B.CODIGO_PRODUTO = ''' || pCODIGO_PRODUTO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery ;
    END IF;
    vQuery := '  
select distinct     
A.CODIGO_PLANO  
, A.CODIGO_PLANO_TSYS  
, A.NOME_PLANO  
, A.DESCRICAO_PLANO  
, A.QUANTIDADE_PARCELAS  
, A.COMISSAO  
, A.ATIVO    

from TB_PLANO A  
LEFT JOIN TB_PRODUTO_PLANO B ON B.CODIGO_PLANO = A.CODIGO_PLANO   
' || vQuery || '   
order by A.CODIGO_PLANO_TSYS';
    OPEN retcur FOR vQuery;
  END PR_PLANO_L;
  PROCEDURE PR_PLANO_R(
      pCODIGO_PLANO         NUMBER,
      pREMOVER_ASSOC_PLANOS CHAR )
  AS
    vQuantidade NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO vQuantidade
    FROM tb_produto_plano
    WHERE codigo_plano = pCODIGO_PLANO;
    IF vQuantidade     > 0 THEN
      raise_application_error(-20000, 'O plano ja esta associado a um produto. ');
    END IF;
    SELECT COUNT(*)
    INTO vQuantidade
    FROM tb_transacao_venda
    WHERE codigo_plano = pCODIGO_PLANO;
    IF vQuantidade     > 0 THEN
      raise_application_error(-20000, 'O plano ja esta associado a uma transacao. ');
    END IF;
    IF pREMOVER_ASSOC_PLANOS IS NOT NULL AND pREMOVER_ASSOC_PLANOS = 'S' THEN
      DELETE TB_PRODUTO_PLANO WHERE CODIGO_PLANO = pCODIGO_PLANO;
    END IF;
    DELETE TB_PLANO WHERE CODIGO_PLANO = pCODIGO_PLANO;
  END PR_PLANO_R;
  PROCEDURE PR_DIA_NAO_UTIL_S(
      pCODIGO_DIA_NAO_UTIL        NUMBER   := NULL,
      pDATA_DIA_NAO_UTIL          DATE     := NULL,
      pCLASSIFICACAO_DIA_NAO_UTIL VARCHAR2 := NULL,
      pDESCRICAO_DIA_NAO_UTIL     VARCHAR2 := NULL,
      pATIVO                      CHAR     := NULL,
      retornarRegistro            CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_DIA_NAO_UTIL NUMBER;
  BEGIN
    vCODIGO_DIA_NAO_UTIL := pCODIGO_DIA_NAO_UTIL;
    IF vCODIGO_DIA_NAO_UTIL IS NULL THEN
      SELECT SQ_DIA_NAO_UTIL.nextval
      INTO vCODIGO_DIA_NAO_UTIL
      FROM dual;
      INSERT
      INTO TB_DIA_NAO_UTIL
        (
          CODIGO_DIA_NAO_UTIL,
          DATA_DIA_NAO_UTIL,
          CLASSIFICACAO_DIA_NAO_UTIL,
          DESCRICAO_DIA_NAO_UTIL,
          ATIVO
        )
        VALUES
        (
          vCODIGO_DIA_NAO_UTIL,
          TRUNC(pDATA_DIA_NAO_UTIL),
          pCLASSIFICACAO_DIA_NAO_UTIL,
          pDESCRICAO_DIA_NAO_UTIL,
          pATIVO
        );
    ELSE
      UPDATE TB_DIA_NAO_UTIL
      SET DATA_DIA_NAO_UTIL        = TRUNC(pDATA_DIA_NAO_UTIL),
        CLASSIFICACAO_DIA_NAO_UTIL = pCLASSIFICACAO_DIA_NAO_UTIL,
        DESCRICAO_DIA_NAO_UTIL     = pDESCRICAO_DIA_NAO_UTIL,
        ATIVO                      = pATIVO
      WHERE CODIGO_DIA_NAO_UTIL    = vCODIGO_DIA_NAO_UTIL;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_DIA_NAO_UTIL_B( pCODIGO_DIA_NAO_UTIL => vCODIGO_DIA_NAO_UTIL, retcur => retcur);
    END IF;
  END PR_DIA_NAO_UTIL_S;
  PROCEDURE PR_DIA_NAO_UTIL_B(
      pCODIGO_DIA_NAO_UTIL NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_DIA_NAO_UTIL ,
    A.DATA_DIA_NAO_UTIL ,
    A.CLASSIFICACAO_DIA_NAO_UTIL ,
    A.DESCRICAO_DIA_NAO_UTIL ,
    A.ATIVO FROM TB_DIA_NAO_UTIL A WHERE A.CODIGO_DIA_NAO_UTIL = pCODIGO_DIA_NAO_UTIL;
  END PR_DIA_NAO_UTIL_B;
  PROCEDURE PR_DIA_NAO_UTIL_L(
      pFILTRO_ATIVO      CHAR := NULL,
      pFILTRO_DATA_MAIOR DATE := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(500);
  BEGIN
    vQuery           := '';
    IF pFILTRO_ATIVO IS NOT NULL THEN
      vQuery         := vQuery || 'A.ATIVO = ''' || pFILTRO_ATIVO || ''' and ';
    END IF;
    IF pFILTRO_DATA_MAIOR IS NOT NULL THEN
      vQuery              := vQuery || 'A.DATA_DIA_NAO_UTIL >= ''' || pFILTRO_DATA_MAIOR || ''' and '; 
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'select A.CODIGO_DIA_NAO_UTIL,                        
A.DATA_DIA_NAO_UTIL,                        
A.CLASSIFICACAO_DIA_NAO_UTIL,                        
A.DESCRICAO_DIA_NAO_UTIL,                        
A.ATIVO                   
from TB_DIA_NAO_UTIL A ' || vQuery || 'order by DATA_DIA_NAO_UTIL ';
    OPEN retcur FOR vQuery;
  END PR_DIA_NAO_UTIL_L;
  PROCEDURE PR_DIA_NAO_UTIL_R(
      pCODIGO_DIA_NAO_UTIL NUMBER)
  AS
  BEGIN
    DELETE TB_DIA_NAO_UTIL
    WHERE CODIGO_DIA_NAO_UTIL = pCODIGO_DIA_NAO_UTIL;
  END PR_DIA_NAO_UTIL_R;
  PROCEDURE PR_EMPRESA_GRUPO_S(
      pCODIGO_EMPRESA_GRUPO          NUMBER   := NULL,
      pNOME_EMPRESA_GRUPO            VARCHAR2 := NULL,
      pDESCRICAO_EMPRESA_GRUPO       VARCHAR2 := NULL,
      pENVIAR_NEGATIVO_EMPRESA_GRUPO CHAR     :='N',
      retornarRegistro               CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_EMPRESA_GRUPO NUMBER;
  BEGIN
    vCODIGO_EMPRESA_GRUPO    := pCODIGO_EMPRESA_GRUPO;
    IF vCODIGO_EMPRESA_GRUPO IS NULL THEN
      SELECT SQ_EMPRESA_GRUPO.nextval INTO vCODIGO_EMPRESA_GRUPO FROM dual;
      INSERT
      INTO TB_EMPRESA_GRUPO
        (
          CODIGO_EMPRESA_GRUPO,
          NOME_EMPRESA_GRUPO,
          DESCRICAO_EMPRESA_GRUPO,
          ENVIAR_NEGATIVO_EMPRESA_GRUPO
        )
        VALUES
        (
          vCODIGO_EMPRESA_GRUPO,
          pNOME_EMPRESA_GRUPO,
          pDESCRICAO_EMPRESA_GRUPO,
          pENVIAR_NEGATIVO_EMPRESA_GRUPO
        );
    ELSE
      UPDATE TB_EMPRESA_GRUPO
      SET DESCRICAO_EMPRESA_GRUPO     = pDESCRICAO_EMPRESA_GRUPO,
        NOME_EMPRESA_GRUPO            = pNOME_EMPRESA_GRUPO,
        ENVIAR_NEGATIVO_EMPRESA_GRUPO = pENVIAR_NEGATIVO_EMPRESA_GRUPO
      WHERE CODIGO_EMPRESA_GRUPO      = vCODIGO_EMPRESA_GRUPO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_EMPRESA_GRUPO_B( pCODIGO_EMPRESA_GRUPO => vCODIGO_EMPRESA_GRUPO, retcur => retcur);
    END IF;
  END PR_EMPRESA_GRUPO_S;
  PROCEDURE PR_EMPRESA_GRUPO_B(
      pCODIGO_EMPRESA_GRUPO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_EMPRESA_GRUPO ,
    A.NOME_EMPRESA_GRUPO ,
    A.DESCRICAO_EMPRESA_GRUPO ,
    A.ENVIAR_NEGATIVO_EMPRESA_GRUPO ,
    A.SIGLA_GRUPO 
    FROM TB_EMPRESA_GRUPO A WHERE A.CODIGO_EMPRESA_GRUPO = pCODIGO_EMPRESA_GRUPO;
  END PR_EMPRESA_GRUPO_B;
  PROCEDURE PR_EMPRESA_GRUPO_L(
      pNOME_EMPRESA_GRUPO VARCHAR2 := NULL,
      maxLinhas           INT      :=0,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(500);
  BEGIN
    vQuery                 := '';
    IF pNOME_EMPRESA_GRUPO IS NOT NULL THEN
      vQuery               := vQuery || 'A.NOME_EMPRESA_GRUPO = ''' || pNOME_EMPRESA_GRUPO || ''' and ';
    END IF;
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || 'rownum <= ' || maxLinhas || ' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'select A.CODIGO_EMPRESA_GRUPO,                        
A.NOME_EMPRESA_GRUPO,                        
A.DESCRICAO_EMPRESA_GRUPO,                        
A.ENVIAR_NEGATIVO_EMPRESA_GRUPO,                        
A.SIGLA_GRUPO                   
from TB_EMPRESA_GRUPO A ' || vQuery || ' ORDER BY A.CODIGO_EMPRESA_GRUPO '; 
    OPEN retcur FOR vQuery;
  END PR_EMPRESA_GRUPO_L;
  PROCEDURE PR_EMPRESA_GRUPO_R(
      pCODIGO_EMPRESA_GRUPO NUMBER)
  AS
  BEGIN
    DELETE TB_EMPRESA_GRUPO WHERE CODIGO_EMPRESA_GRUPO = pCODIGO_EMPRESA_GRUPO;
  END PR_EMPRESA_GRUPO_R;
  PROCEDURE PR_NUMERO_SEQUENCIAL_L(
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR SELECT SQ_ARQUIVO_CONTABIL.nextval FROM dual;
  END PR_NUMERO_SEQUENCIAL_L;
  PROCEDURE PR_RELATORIO_VENCIMENTO_L2(
      pCODIGO_EMPRESA_GRUPO     NUMBER   := NULL,
      pCODIGO_EMPRESA_SUB_GRUPO NUMBER   := NULL,
      pCODIGO_FAVORECIDO        NUMBER   := NULL,
      pCODIGO_PRODUTO           VARCHAR2 := NULL,
      pTIPO_REGISTRO            VARCHAR2 := NULL,
      pCONSOLIDA_AG_VC          CHAR     := 'N',
      pCONSOLIDA_MAIOR          CHAR     := 'N',
      pSTATUS_RETORNO_CESSAO    CHAR     := NULL,
      pDATA_INICIO_TRANSACAO    DATE     := NULL,
      pDATA_FIM_TRANSACAO       DATE     := NULL,
      pDATA_INICIO_VENCIMENTO   DATE     := NULL,
      pDATA_FIM_VENCIMENTO      DATE     := NULL,
      pDATA_INICIO_MOVIMENTO    DATE     := NULL,
      pDATA_FIM_MOVIMENTO       DATE     := NULL,
      pDATA_INICIO_AGENDAMENTO  DATE     := NULL,
      pDATA_FIM_AGENDAMENTO     DATE     := NULL,
      pDATA_POSICAO_CARTEIRA    DATE     := NULL,
      pTIPO_FINANCEIRO_CONTABIL CHAR     := NULL,
      pDATA_INICIO_CCI          DATE     := NULL,
      pDATA_FIM_CCI             DATE     := NULL,
      pREFERENCIA               VARCHAR2 := NULL,
      pCONSOLIDA_REFERENCIA     CHAR     := 'N', 
      pCODIGO_PROCESSO          NUMBER   := NULL,
      pGERAR_PAGAMENTOS         CHAR     := 'N', 
      pRETORNAR_PAGOS           CHAR     := 'N', 
      retcur OUT TP_REFCURSOR )
  AS
    vQuery2          VARCHAR2(5000);
    vQuery1          VARCHAR2(5000);
    vSelectRel       VARCHAR2(1000);
    vLeftJoin        VARCHAR2(5000);
    vOrderBy         VARCHAR2(1000);
    vGroupBy         VARCHAR2(1000);
    vCondicao        VARCHAR2(3000) := '';
    vMAIOR_DT_AGENDA DATE;
    vDATA_MOVIMENTO  DATE;
    vCODIGO_LISTA     NUMBER;       
    formatoData       VARCHAR2(20); 
    vGERAR_PAGAMENTOS CHAR(1);
  BEGIN
    PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_RELATORIO_VENCIMENTO_L2', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => vQuery1 || ';' );
    vQuery2 := NULL;
    vOrderBy := NULL;
    vMAIOR_DT_AGENDA  := NULL;
    formatoData       := 'yyyy-mm-dd'; 
    vDATA_MOVIMENTO   := pDATA_POSICAO_CARTEIRA;
    vGERAR_PAGAMENTOS := 'N';
    SELECT CODIGO_LISTA
    INTO vCODIGO_LISTA
    FROM TB_LISTA
    WHERE NOME_LISTA = 'Referencia';
    IF (pGERAR_PAGAMENTOS = 'S') THEN
      vGERAR_PAGAMENTOS  := 'S';
    END IF;
    IF (pGERAR_PAGAMENTOS = 'S' OR pRETORNAR_PAGOS = 'N') THEN
      vCondicao          := vCondicao || '  (A.STATUS_TRANSACAO <> 6 AND A.STATUS_TRANSACAO <> 2) 
';
    ELSE
      vCondicao := vCondicao || '  (A.STATUS_TRANSACAO <> 6 ) 
';
    END IF;
    IF pTIPO_FINANCEIRO_CONTABIL IS NOT NULL THEN
      vCondicao                  := vCondicao || ' AND  nvl(A.TIPO_FINANCEIRO_CONTABIL,''' || pTIPO_FINANCEIRO_CONTABIL || ''') = ''' || pTIPO_FINANCEIRO_CONTABIL || '''  
';
    END IF;
    IF pTIPO_REGISTRO = 'CV-AV' OR pTIPO_REGISTRO IS NULL THEN
      vCondicao      := vCondicao || ' AND   A.TIPO_TRANSACAO IN (''CV'', ''AV'')  
';
    ELSE
      vCondicao := vCondicao || '  AND A.TIPO_TRANSACAO = ''' || pTIPO_REGISTRO || '''  
';
    END IF;
    IF pSTATUS_RETORNO_CESSAO  IS NOT NULL THEN
      IF pSTATUS_RETORNO_CESSAO = 'N' THEN
        vCondicao              := vCondicao || ' AND  (  A.STATUS_RET_CESSAO <> 1      
or        
(  A.DATA_POSICAO_CARTEIRA >  TO_DATE(''' || vDATA_MOVIMENTO || ''') and          
A.STATUS_RET_CESSAO = 1  )      
or      
A.DATA_POSICAO_CARTEIRA is null    
)  
';
      ELSE
        vCondicao := vCondicao || '  AND A.STATUS_RET_CESSAO = 1    
AND A.DATA_POSICAO_CARTEIRA <=  TO_DATE(''' || vDATA_MOVIMENTO || ''')  
';
      END IF;
    END IF;
    IF pCODIGO_EMPRESA_GRUPO IS NOT NULL THEN
      vCondicao              := vCondicao || '  AND G.CODIGO_EMPRESA_GRUPO = ''' || pCODIGO_EMPRESA_GRUPO || '''  
';
    END IF;
    IF pCODIGO_EMPRESA_SUB_GRUPO IS NOT NULL THEN
      vCondicao                  := vCondicao || '  AND F.CODIGO_EMPRESA_SUBGRUPO = ''' || pCODIGO_EMPRESA_SUB_GRUPO || '''  
';
    END IF;
    IF pCODIGO_PRODUTO IS NOT NULL THEN
      vCondicao        := vCondicao || '  AND A.CODIGO_PRODUTO in (' || pCODIGO_PRODUTO || ')  
';
    END IF;
    IF pCODIGO_FAVORECIDO IS NOT NULL THEN
      vCondicao           := vCondicao || '  AND    
CASE WHEN A.DATA_POSICAO_CARTEIRA <= TO_DATE(''' || vDATA_MOVIMENTO || ''')    
THEN A.CODIGO_FAVORECIDO    
ELSE A.CODIGO_FAVORECIDO_ORIGINAL    
END = ''' || pCODIGO_FAVORECIDO || '''  
';
    END IF;
    IF pDATA_INICIO_TRANSACAO IS NOT NULL THEN
      vCondicao               := vCondicao || '  AND A.DATA_TRANSACAO >= ''' || pDATA_INICIO_TRANSACAO || '''  
';
    END IF;
    IF (pDATA_FIM_TRANSACAO IS NOT NULL) THEN
      vCondicao             := vCondicao || '  AND A.DATA_TRANSACAO <= to_date(''' || TO_CHAR(pDATA_FIM_TRANSACAO, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'')  
';
    END IF;
    IF pDATA_INICIO_VENCIMENTO IS NOT NULL THEN
      vCondicao                := vCondicao || '  AND A.DATA_REPASSE >= ''' || pDATA_INICIO_VENCIMENTO || '''  
';
    END IF;
    IF pDATA_FIM_VENCIMENTO IS NOT NULL THEN
      vCondicao             := vCondicao || '  AND A.DATA_REPASSE <= ''' || pDATA_FIM_VENCIMENTO || '''  
';
    END IF;
    IF pDATA_INICIO_MOVIMENTO IS NOT NULL THEN
      vCondicao               := vCondicao || '  AND A.DATA_MOVIMENTO >= ''' || pDATA_INICIO_MOVIMENTO || '''  
';
    END IF;
    IF pDATA_FIM_MOVIMENTO IS NOT NULL THEN
      vCondicao            := vCondicao ||
      '  AND A.DATA_MOVIMENTO <= to_date(''' || TO_CHAR(pDATA_FIM_MOVIMENTO, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'')  
';
    END IF;
    IF pDATA_INICIO_AGENDAMENTO IS NOT NULL THEN
      vCondicao                 := vCondicao || '  AND A.DATA_REPASSE_CALCULADA >= ''' || pDATA_INICIO_AGENDAMENTO || '''  
';
    END IF;
    IF pDATA_FIM_AGENDAMENTO IS NOT NULL THEN
      vCondicao              := vCondicao || '  AND A.DATA_REPASSE_CALCULADA <= ''' || pDATA_FIM_AGENDAMENTO || '''  
';
    END IF;
    IF pDATA_INICIO_CCI IS NOT NULL THEN
      vCondicao         := vCondicao || '  AND A.DATA_POSICAO_CARTEIRA >= ''' || pDATA_INICIO_CCI || '''  
';
    END IF;
    IF pDATA_FIM_CCI IS NOT NULL THEN
      vCondicao      := vCondicao || '  AND A.DATA_POSICAO_CARTEIRA <= ''' || pDATA_FIM_CCI || '''  
';
    END IF;
    IF pREFERENCIA IS NOT NULL THEN
      vCondicao    := vCondicao || '  AND A.REFERENCIA in (' || pREFERENCIA || ')  
';
    END IF;
    vCondicao := '  
WHERE ' || vCondicao;
    vLeftJoin := '    
FROM TB_TRANSACAO A    
left join TB_CONTA_FAVORECIDO B  on B.CODIGO_CONTA_FAVORECIDO =        
CASE WHEN   A.DATA_POSICAO_CARTEIRA <=  TO_DATE(''' || vDATA_MOVIMENTO || ''')          
THEN A.CODIGO_CONTA_FAVORECIDO          
ELSE A.CODIGO_CONTA_FAVORECIDO_ORIG        
END    
left join TB_ESTABELECIMENTO C            on A.CODIGO_ESTABELECIMENTO = C.CODIGO_ESTABELECIMENTO    
left join TB_PRODUTO D                 on A.CODIGO_PRODUTO = D.CODIGO_PRODUTO    
left join TB_FAVORECIDO E               on E.CODIGO_FAVORECIDO =        
CASE WHEN A.DATA_POSICAO_CARTEIRA <=    TO_DATE(''' || vDATA_MOVIMENTO ||
    ''')          
THEN A.CODIGO_FAVORECIDO          
ELSE A.CODIGO_FAVORECIDO_ORIGINAL        
END    
left join TB_EMPRESA_SUBGRUPO F           on C.CODIGO_EMPRESA_SUBGRUPO = F.CODIGO_EMPRESA_SUBGRUPO    
left join TB_EMPRESA_GRUPO G             on F.CODIGO_EMPRESA_GRUPO = G.CODIGO_EMPRESA_GRUPO    
left join TB_LISTA_ITEM H               on A.REFERENCIA = H.VALOR and H.CODIGO_LISTA = ''' || vCODIGO_LISTA || '''  
';
    IF pCONSOLIDA_MAIOR <> 'N' THEN
      vQuery2           := '   SELECT   max(trunc(DATA_REPASSE_CALCULADA))      
FROM  (      
SELECT  trunc(A.DATA_REPASSE_CALCULADA) as DATA_REPASSE_CALCULADA  
' || vLeftJoin || vCondicao || '        
)  
';
      PR_LOG_DB( PDESCRICAO_LOG => vQuery2, PTIPO_ORIGEM => 'PR_RELATORIO_VENCIMENTO_L2', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => NULL );
      EXECUTE IMMEDIATE vQuery2 INTO vMAIOR_DT_AGENDA;
    END IF;
    IF vGERAR_PAGAMENTOS = 'N' THEN
      vQuery1           := 'SELECT              
RELATORIO.* FROM ( ';
    ELSE
      BEGIN
        vQuery1    := '';
        vSelectRel := 'SELECT                      
RELATORIO.CODIGO_EMPRESA_GRUPO,                       
TO_DATE(RELATORIO.DATA_REPASSE_CALCULADA),                      
RELATORIO. CODIGO_FAVORECIDO ,                      
RELATORIO.CODIGO_CONTA_FAVORECIDO,                      
RELATORIO.CODIGO_PRODUTO,                      
RELATORIO.BANCO,                      
RELATORIO.AGENCIA,                      
RELATORIO.CONTA,                      
RELATORIO.VALOR_LIQUIDO,                      
RELATORIO.VALOR_BRUTO,                      
NULL AS DATA_LIQUIDACAO_MANUAL,                      
RELATORIO.REFERENCIA ,                      
RELATORIO.QTDE  FROM ( ' ;
      END;
    END IF;
    vQuery1:= vQuery1 || '     
SELECT      
CASE WHEN A.DATA_POSICAO_CARTEIRA <=    TO_DATE(''' || vDATA_MOVIMENTO || ''')      
THEN A.CODIGO_FAVORECIDO        
ELSE A.CODIGO_FAVORECIDO_ORIGINAL      
END                      AS CODIGO_FAVORECIDO  
,   TO_DATE(' || '''' || vDATA_MOVIMENTO || ''')    AS POSICAO_CARTEIRA  
,    B.BANCO  
,    B.AGENCIA  
,    B.CONTA  
';
    IF vMAIOR_DT_AGENDA IS NOT NULL THEN
      vQuery1           := vQuery1 || ',    TO_DATE(''' || vMAIOR_DT_AGENDA || ''')  AS DATA_REPASSE_CALCULADA   
';
    ELSE
      vQuery1 := vQuery1 || ',    trunc(A.DATA_REPASSE_CALCULADA)       as DATA_REPASSE_CALCULADA   
';
    END IF;
    IF pCONSOLIDA_AG_VC = 'S' THEN
      vQuery1          := vQuery1 || ',    trunc(A.DATA_REPASSE)             as DATA_REPASSE  
';
    END IF;
    vQuery1 := vQuery1 || ',SUM(VALOR_BRUTO) AS VALOR_BRUTO  
,SUM(VALOR_DESCONTO) AS VALOR_COMISSAO  
,SUM(VALOR_LIQUIDO) AS VALOR_LIQUIDO  
';
    vQuery1 := vQuery1 || '  
,    D.NOME_PRODUTO                 AS DESCRICAO_PRODUTO  
,    A.CODIGO_PRODUTO         
,    B.CODIGO_CONTA_FAVORECIDO  
,    E.CNPJ_FAVORECIDO               AS CNPJ  
,    E.NOME_FAVORECIDO  
,   G.CODIGO_EMPRESA_GRUPO  
,    G.DESCRICAO_EMPRESA_GRUPO  
';
    IF pCONSOLIDA_REFERENCIA = 'S' THEN
      vQuery1               := vQuery1 || ',    A.REFERENCIA            as REFERENCIA       
,     H.NOME_LISTA_ITEM        as NOME_REFERENCIA      
';
    END IF;
    vGroupBy := '    
GROUP BY        
CASE WHEN A.DATA_POSICAO_CARTEIRA <=  TO_DATE(''' || vDATA_MOVIMENTO || ''')          
THEN A.CODIGO_FAVORECIDO          
ELSE A.CODIGO_FAVORECIDO_ORIGINAL        
END  
, TO_DATE(' || '''' || vDATA_MOVIMENTO || ''')  
,  B.BANCO  
,  B.AGENCIA  
,  B.CONTA    

';
    IF vMAIOR_DT_AGENDA IS NOT NULL THEN
      vGroupBy          := vGroupBy || ',    TO_DATE(''' || vMAIOR_DT_AGENDA || ''') ';
    ELSE
      vGroupBy := vGroupBy || ',  A.DATA_REPASSE_CALCULADA ';
    END IF;
    IF pCONSOLIDA_AG_VC = 'S' THEN
      vGroupBy         := vGroupBy || ',  trunc(A.DATA_REPASSE)  
';
    END IF;
    vGroupBy := vGroupBy || ',  D.NOME_PRODUTO  
,  A.CODIGO_PRODUTO  
, B.CODIGO_CONTA_FAVORECIDO  
,  E.CNPJ_FAVORECIDO  
,  E.NOME_FAVORECIDO  
,  G.CODIGO_EMPRESA_GRUPO  
,  G.DESCRICAO_EMPRESA_GRUPO  
';
    IF pCONSOLIDA_REFERENCIA = 'S' THEN
      vGroupBy              := vGroupBy || ',  A.REFERENCIA, H.NOME_LISTA_ITEM  
' ;
    END IF;
    IF pCONSOLIDA_REFERENCIA = 'S' THEN
      vOrderBy              := 'A.REFERENCIA, H.NOME_LISTA_ITEM,  
' ;
    END IF;
    vOrderBy := 'ORDER BY ' || vOrderBy || '    
E.CNPJ_FAVORECIDO  
,  E.NOME_FAVORECIDO  
,  B.CONTA  
,  B.AGENCIA  
,  B.BANCO  
, D.NOME_PRODUTO    
)  
RELATORIO  
';
    IF vGERAR_PAGAMENTOS = 'N' THEN
      BEGIN
        vQuery1 := vQuery1 || vLeftJoin || vCondicao || vGroupBy || vOrderBy;
        PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_RELATORIO_VENCIMENTO_L2', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => vQuery1 || ';' );
        OPEN retcur FOR vQuery1;
      END;
    ELSE
      BEGIN
        vQuery1 := vQuery1 || ' , COUNT(*) AS QTDE ';
        PR_GERAR_PAGAMENTOS( pSELECTREL =>vSelectRel, pQUERY => vQuery1, pJOIN => vLeftJoin, pCONDICAO => vCondicao, pGROUPBY => vGroupBy, pORDERBY => vOrderBy, pCODIGO_PROCESSO => pCODIGO_PROCESSO);
      END;
    END IF;
  END PR_RELATORIO_VENCIMENTO_L2;
  PROCEDURE PR_GERAR_PAGAMENTOS(
      pSELECTREL       VARCHAR := NULL, 
      pQUERY           VARCHAR := NULL,
      pJOIN            VARCHAR := NULL,
      pCONDICAO        VARCHAR := NULL,
      pGROUPBY         VARCHAR := NULL,
      pORDERBY         VARCHAR := NULL,
      pCODIGO_PROCESSO NUMBER  := NULL )
  AS
    /*
    O objetivo dessa procedure ï¿½, atravï¿½s do select efetuado para o relatï¿½rio de vencimento, gerar os pagamentos.
    Os pagamentos serï¿½o gerados na tb_pagamento, e, na tb_pagamento_referencia, hï¿½ o rateio do pagamento por cada referï¿½ncia.
    Depois de gerado, altera as transacoes, marcando o codigo de pagamento nelas
    Essa procedure recebe cada parte da query executada no relatorio de vencimentos
    Precisa ser assim, pois ela manipula essas partes para poder gerar as informacoes de pagamento e depois marcar as transacoes
    e utilizada tambem uma tabela temporaria que apï¿½ia o processo
    */
    vExecutar                VARCHAR2(5000);
    vCODIGO_EMPRESA_GRUPO    NUMBER(12, 0);
    vCODIGO_PAGAMENTO        NUMBER(12,0);
    vCODIGO_FAVORECIDO       NUMBER(12,0);
    vCODIGO_CONTA_FAVORECIDO NUMBER(12,0);
    vCODIGO_PRODUTO          NUMBER(12,0);
    vDATA_PAGAMENTO          DATE;
    vBANCO                   VARCHAR2(20 BYTE);
    vAGENCIA                 VARCHAR2(20 BYTE);
    vCONTA                   VARCHAR2(20 BYTE);
    vVALOR_PAGAMENTO         NUMBER(14,2);
    vVALOR_BRUTO             NUMBER(14,2);
    vQTDE                    NUMBER(12,0);
    cursorvencimento TP_REFCURSOR;
    vQTDE_PAGAMENTOS NUMBER(8,2);
  BEGIN
    vQTDE_PAGAMENTOS := 0 ;
    vExecutar := ' INSERT INTO TEMP_PAGAMENTO  (      
CODIGO_EMPRESA_GRUPO ,      
DATA_PAGAMENTO ,      
CODIGO_FAVORECIDO ,      
CODIGO_CONTA_FAVORECIDO,      
CODIGO_PRODUTO,    
BANCO ,     
AGENCIA,     
CONTA ,     
VALOR_PAGAMENTO ,       
VALOR_BRUTO,    
DATA_LIQUIDACAO_MANUAL  ,      
CODIGO_REFERENCIA,      
QTDE_TRANSACOES   
) ';
    vExecutar := ' ' || vExecutar || ' ' || pSELECTREL || pQUERY || pJOIN || pCONDICAO || pGROUPBY || pORDERBY;
    PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_GERAR_PAGAMENTOS', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => 'GERACAO DA TEMPORARIA: ' || vExecutar || ';' );
    EXECUTE IMMEDIATE vExecutar;
    OPEN cursorvencimento FOR SELECT CODIGO_EMPRESA_GRUPO ,
    CODIGO_FAVORECIDO ,
    CODIGO_CONTA_FAVORECIDO ,
    CODIGO_PRODUTO ,
    BANCO ,
    AGENCIA ,
    CONTA ,
    DATA_PAGAMENTO ,
    SUM(valor_pagamento)
  AS
    valor_pagamento ,
    SUM(valor_BRUTO)
  AS
    valor_bruto ,
    SUM(qtde_TRANSACOES)
  AS
    qtde2 FROM TEMP_PAGAMENTO group by codigo_empresa_grupo,
    codigo_favorecido,
    codigo_conta_favorecido,
    CODIGO_produto,
    banco,
    agencia,
    conta,
    DATA_PAGAMENTO;
    LOOP
      FETCH cursorvencimento
      INTO vCODIGO_EMPRESA_GRUPO ,
        vCODIGO_FAVORECIDO ,
        vCODIGO_CONTA_FAVORECIDO ,
        vCODIGO_PRODUTO ,
        vBANCO ,
        vAGENCIA ,
        vCONTA ,
        vDATA_PAGAMENTO ,
        vVALOR_PAGAMENTO ,
        vVALOR_BRUTO ,
        vQTDE;
      EXIT
    WHEN cursorvencimento%NOTFOUND;
      SELECT SQ_LOG.nextval
      INTO vCODIGO_PAGAMENTO
      FROM dual;
      INSERT
      INTO TB_PAGAMENTO
        (
          CODIGO_PAGAMENTO,
          CODIGO_EMPRESA_GRUPO,
          CODIGO_FAVORECIDO,
          CODIGO_CONTA_FAVORECIDO,
          CODIGO_PRODUTO,
          BANCO,
          AGENCIA,
          CONTA,
          DATA_PAGAMENTO,
          VALOR_PAGAMENTO,
          STATUS_PAGAMENTO,
          DATA_GERACAO,
          VALOR_BRUTO,
          QTDE_TRANSACOES
        )
        VALUES
        (
          vCODIGO_PAGAMENTO,
          vCODIGO_EMPRESA_GRUPO,
          vCODIGO_FAVORECIDO,
          vCODIGO_CONTA_FAVORECIDO,
          vCODIGO_PRODUTO,
          vBANCO,
          vAGENCIA,
          vCONTA,
          vDATA_PAGAMENTO,
          vVALOR_PAGAMENTO,
          '0',
          sysdate,
          vVALOR_BRUTO,
          vQTDE
        );
      vQTDE_PAGAMENTOS := vQTDE_PAGAMENTOS + SQL%ROWCOUNT;
      INSERT
      INTO TB_PAGAMENTO_REFERENCIA
        (
          CODIGO_PAGAMENTO,
          CODIGO_REFERENCIA,
          VALOR_PAGAMENTO
        )
        (SELECT vCODIGO_PAGAMENTO AS CODIGO_PAGAMENTO,
            CODIGO_REFERENCIA     AS CODIGO_REFERENCIA,
            SUM(VALOR_PAGAMENTO)  AS VALOR_PAGAMENTO
          FROM TEMP_PAGAMENTO
          WHERE codigo_empresa_grupo  = vCODIGO_EMPRESA_GRUPO
          AND CODIGO_FAVORECIDO       = vCODIGO_FAVORECIDO
          AND CODIGO_CONTA_FAVORECIDO = vCODIGO_CONTA_FAVORECIDO
          AND CODIGO_PRODUTO          = vCODIGO_PRODUTO
          AND BANCO                   = vBANCO
          AND CONTA                   = vCONTA
          AND DATA_PAGAMENTO          = vDATA_PAGAMENTO
          GROUP BY vCODIGO_PAGAMENTO,
            CODIGO_REFERENCIA
        ) ;
      vExecutar := 'UPDATE TB_TRANSACAO SET CODIGO_PAGAMENTO = ' || vCODIGO_PAGAMENTO || ' , STATUS_PRE_PAGAMENTO = STATUS_TRANSACAO, STATUS_TRANSACAO = 2 ' || ' WHERE  CODIGO_TRANSACAO IN ( SELECT A.CODIGO_TRANSACAO  ' || pJOIN || pCONDICAO || '      
AND  g.codigo_empresa_grupo  = ' || vCODIGO_EMPRESA_GRUPO || '' || '    and A.CODIGO_FAVORECIDO = ' || vCODIGO_FAVORECIDO || '' || '    and A.CODIGO_CONTA_FAVORECIDO = ' || vCODIGO_CONTA_FAVORECIDO || '' || '    and A.CODIGO_PRODUTO = ' || vCODIGO_PRODUTO || '' || '    and B.BANCO = ''' || vBANCO || '''' || '    and B.CONTA = ''' || vCONTA || '''' || '    and A.DATA_REPASSE_CALCULADA = TO_DATE(''' || vDATA_PAGAMENTO || '''))';
      EXECUTE IMMEDIATE vExecutar;
    END LOOP;
    CLOSE cursorvencimento;
    COMMIT;
    IF pCODIGO_PROCESSO IS NOT NULL THEN
      PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'ProcessoInfo', PCODIGO_ORIGEM => pCODIGO_PROCESSO, PDESCRICAOGRANDE_LOG => 'Qtde de pagamentos gerados: ' || vQTDE_PAGAMENTOS || ';' );
    END IF;
    IF pCODIGO_PROCESSO IS NOT NULL THEN
      PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'ProcessoInfo', PCODIGO_ORIGEM => pCODIGO_PROCESSO, PDESCRICAOGRANDE_LOG => 'Finalizou a procedure de geracao de pagamentos. ' );
    END IF;
  END PR_GERAR_PAGAMENTOS;
  PROCEDURE PR_EMPRESA_SUBGRUPO_S
    (
      pCODIGO_EMPRESA_SUBGRUPO    NUMBER   := NULL,
      pNOME_EMPRESA_SUBGRUPO      VARCHAR2 := NULL,
      pDESCRICAO_EMPRESA_SUBGRUPO VARCHAR2 := NULL,
      pCODIGO_EMPRESA_GRUPO       NUMBER   := NULL,
      retornarRegistro            CHAR     := 'N',
      retcur OUT TP_REFCURSOR
    )
  AS
    vCODIGO_EMPRESA_SUBGRUPO NUMBER;
  BEGIN
    vCODIGO_EMPRESA_SUBGRUPO := pCODIGO_EMPRESA_SUBGRUPO;
    IF vCODIGO_EMPRESA_SUBGRUPO IS NULL THEN
      SELECT SQ_EMPRESA_SUBGRUPO.nextval
      INTO vCODIGO_EMPRESA_SUBGRUPO
      FROM dual;
      INSERT
      INTO TB_EMPRESA_SUBGRUPO
        (
          CODIGO_EMPRESA_SUBGRUPO,
          NOME_EMPRESA_SUBGRUPO,
          DESCRICAO_EMPRESA_SUBGRUPO,
          CODIGO_EMPRESA_GRUPO
        )
        VALUES
        (
          vCODIGO_EMPRESA_SUBGRUPO,
          pNOME_EMPRESA_SUBGRUPO,
          pDESCRICAO_EMPRESA_SUBGRUPO,
          pCODIGO_EMPRESA_GRUPO
        );
    ELSE
      UPDATE TB_EMPRESA_SUBGRUPO
      SET DESCRICAO_EMPRESA_SUBGRUPO = pDESCRICAO_EMPRESA_SUBGRUPO,
        NOME_EMPRESA_SUBGRUPO        = pNOME_EMPRESA_SUBGRUPO,
        CODIGO_EMPRESA_GRUPO         = pCODIGO_EMPRESA_GRUPO
      WHERE CODIGO_EMPRESA_SUBGRUPO  = vCODIGO_EMPRESA_SUBGRUPO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_EMPRESA_SUBGRUPO_B( pCODIGO_EMPRESA_SUBGRUPO => vCODIGO_EMPRESA_SUBGRUPO, retcur => retcur);
    END IF;
  END PR_EMPRESA_SUBGRUPO_S;
  PROCEDURE PR_EMPRESA_SUBGRUPO_B(
      pCODIGO_EMPRESA_SUBGRUPO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_EMPRESA_SUBGRUPO ,
    A.NOME_EMPRESA_SUBGRUPO ,
    A.DESCRICAO_EMPRESA_SUBGRUPO ,
    A.CODIGO_EMPRESA_GRUPO FROM TB_EMPRESA_SUBGRUPO A WHERE A.CODIGO_EMPRESA_SUBGRUPO = pCODIGO_EMPRESA_SUBGRUPO;
  END PR_EMPRESA_SUBGRUPO_B;
  PROCEDURE PR_EMPRESA_SUBGRUPO_L(
      pNOME_EMPRESA_SUBGRUPO   VARCHAR2 := NULL,
      pCODIGO_EMPRESA_SUBGRUPO NUMBER   := NULL,
      pCODIGO_EMPRESA_GRUPO    NUMBER   := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(1000);
  BEGIN
    vQuery := '';
    IF pCODIGO_EMPRESA_SUBGRUPO IS NOT NULL THEN
      vQuery                    := vQuery || 'A.CODIGO_EMPRESA_SUBGRUPO = ''' || pCODIGO_EMPRESA_SUBGRUPO || ''' and ';
    END IF;
    IF pCODIGO_EMPRESA_GRUPO IS NOT NULL THEN
      vQuery                 := vQuery || 'A.CODIGO_EMPRESA_GRUPO = ''' || pCODIGO_EMPRESA_GRUPO || ''' and ';
    END IF;
    IF pNOME_EMPRESA_SUBGRUPO IS NOT NULL THEN
      vQuery                  := vQuery || 'A.NOME_EMPRESA_SUBGRUPO = ''' || pNOME_EMPRESA_SUBGRUPO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'select A.CODIGO_EMPRESA_SUBGRUPO,                        
A.NOME_EMPRESA_SUBGRUPO,                        
A.DESCRICAO_EMPRESA_SUBGRUPO,                        
A.CODIGO_EMPRESA_GRUPO                   
from TB_EMPRESA_SUBGRUPO A ' || vQuery || ' ORDER BY A.CODIGO_EMPRESA_GRUPO, A.CODIGO_EMPRESA_SUBGRUPO '; 
    OPEN retcur FOR vQuery;
  END PR_EMPRESA_SUBGRUPO_L;
  PROCEDURE PR_EMPRESA_SUBGRUPO_R(
      pCODIGO_EMPRESA_SUBGRUPO NUMBER)
  AS
  BEGIN
    DELETE TB_EMPRESA_SUBGRUPO
    WHERE CODIGO_EMPRESA_SUBGRUPO = pCODIGO_EMPRESA_SUBGRUPO;
  END PR_EMPRESA_SUBGRUPO_R;
  PROCEDURE PR_FAVORECIDO_S(
      pCODIGO_FAVORECIDO    NUMBER   := NULL,
      pNOME_FAVORECIDO      VARCHAR2 := NULL,
      pCNPJ_FAVORECIDO      VARCHAR2 := NULL,
      pFAVORECIDO_ORIGINAL  CHAR     := NULL,
      pFAVORECIDO_BLOQUEADO SMALLINT := 0,
      retornarRegistro      CHAR     := 'N',
      retcurContas OUT TP_REFCURSOR,
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_FAVORECIDO NUMBER;
  BEGIN
    vCODIGO_FAVORECIDO := pCODIGO_FAVORECIDO;
    IF vCODIGO_FAVORECIDO IS NULL THEN
      SELECT SQ_FAVORECIDO.nextval
      INTO vCODIGO_FAVORECIDO
      FROM dual;
      INSERT
      INTO TB_FAVORECIDO
        (
          CODIGO_FAVORECIDO,
          NOME_FAVORECIDO,
          CNPJ_FAVORECIDO,
          FAVORECIDO_ORIGINAL,
          FAVORECIDO_BLOQUEADO
        )
        VALUES
        (
          vCODIGO_FAVORECIDO,
          pNOME_FAVORECIDO,
          pCNPJ_FAVORECIDO,
          NVL(pFAVORECIDO_ORIGINAL, '0'),
          pFAVORECIDO_BLOQUEADO
        );
    ELSE
      UPDATE TB_FAVORECIDO
      SET CNPJ_FAVORECIDO     = NVL(pCNPJ_FAVORECIDO, CNPJ_FAVORECIDO),
        NOME_FAVORECIDO       = NVL(pNOME_FAVORECIDO, NOME_FAVORECIDO),
        FAVORECIDO_ORIGINAL   = NVL(pFAVORECIDO_ORIGINAL, FAVORECIDO_ORIGINAL),
        FAVORECIDO_BLOQUEADO  = NVL(pFAVORECIDO_BLOQUEADO, FAVORECIDO_BLOQUEADO)
      WHERE CODIGO_FAVORECIDO = vCODIGO_FAVORECIDO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_FAVORECIDO_B( pCODIGO_FAVORECIDO => vCODIGO_FAVORECIDO, retornarContas => 'N', retcurContas => retcurContas, retcur => retcur);
    END IF;
  END PR_FAVORECIDO_S;
  PROCEDURE PR_FAVORECIDO_B(
      pCODIGO_FAVORECIDO NUMBER,
      retornarContas     CHAR := 'N',
      retcur OUT TP_REFCURSOR,
      retcurContas OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT rowNum
  AS
    rowNumber ,
    A.CODIGO_FAVORECIDO ,
    A.CNPJ_FAVORECIDO ,
    A.NOME_FAVORECIDO ,
    A.FAVORECIDO_ORIGINAL ,
    A.FAVORECIDO_BLOQUEADO FROM TB_FAVORECIDO A WHERE A.CODIGO_FAVORECIDO = pCODIGO_FAVORECIDO;
    IF retornarContas                                                     = 'S' THEN
      PR_CONTA_FAVORECIDO_L ( pCODIGO_FAVORECIDO => pCODIGO_FAVORECIDO, retcur => retcurContas);
    END IF;
  END PR_FAVORECIDO_B;
  PROCEDURE PR_FAVORECIDO_L(
      pCODIGO_FAVORECIDO NUMBER   := NULL,
      pCNPJ_FAVORECIDO   VARCHAR2 := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(500);
  BEGIN
    vQuery := '';
    IF pCODIGO_FAVORECIDO IS NOT NULL THEN
      vQuery              := vQuery || 'A.CODIGO_FAVORECIDO = ''' || pCODIGO_FAVORECIDO || ''' and ';
    END IF;
    IF pCNPJ_FAVORECIDO IS NOT NULL THEN
      vQuery            := vQuery || 'A.CNPJ_FAVORECIDO = ''' || pCNPJ_FAVORECIDO || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := '  
select   
rownum as rowNumber,  
A.CODIGO_FAVORECIDO,  
A.NOME_FAVORECIDO,  
A.CNPJ_FAVORECIDO,  
A.FAVORECIDO_ORIGINAL,  
A.FAVORECIDO_BLOQUEADO  
from TB_FAVORECIDO A ' || vQuery || ' order by A.CODIGO_FAVORECIDO';
    OPEN retcur FOR vQuery;
  END PR_FAVORECIDO_L;
  PROCEDURE PR_FAVORECIDO_R(
      pCODIGO_FAVORECIDO NUMBER)
  AS
  BEGIN
    DELETE TB_CONTA_FAVORECIDO_PRODUTO
    WHERE CODIGO_CONTA_FAVORECIDO IN
      (SELECT DISTINCT CODIGO_CONTA_FAVORECIDO
      FROM TB_CONTA_FAVORECIDO
      WHERE CODIGO_FAVORECIDO = pCODIGO_FAVORECIDO
      );
    DELETE TB_CONTA_FAVORECIDO
    WHERE CODIGO_FAVORECIDO = pCODIGO_FAVORECIDO;
    DELETE TB_ESTABELECIMENTO_FAVORECIDO
    WHERE CODIGO_FAVORECIDO = pCODIGO_FAVORECIDO;
    DELETE TB_FAVORECIDO
    WHERE CODIGO_FAVORECIDO = pCODIGO_FAVORECIDO;
  END PR_FAVORECIDO_R;
  PROCEDURE PR_CONFIGURACAO_SISTEMA_B(
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_CONFIGURACAO_SISTEMA,
    A.PRAZO_PAGAMENTO,
    A.DIAS_VENCIDO,
    A.DIR_IMP_EXTRATO,
    A.DIR_IMP_EXTRATO_ATACADAO,
    A.DIR_IMP_EXTRATO_GALERIA,
    A.DIR_EXP_CESSAO,
    A.DIR_IMP_RET_CESSAO,
    A.DIR_EXP_MATERA_CONTABIL,
    A.DIR_EXP_MATERA_FINANCEIRO,
    A.DIR_EXP_MATERA_EXTRATO,
    A.FL_ENVIO_MATERA_AUT,
    A.FL_ENVIO_CESSAO_AUT,
    A.HORA_DE_LEITURA_EXTRATO,
    A.HORA_ATE_LEITURA_RET_CESSAO,
    A.DIR_IMP_ATACADAO_TSYS,
    A.DIR_IMP_ATACADAO_SITEF,
    A.DIR_IMP_ATACADAO_MA,
    A.DIR_EXP_ATACADAO_CSUSEM,
    A.DIR_EXP_ATACADAO_CONC,
    A.HORA_DE_LEITURA_ATACADAO,
    A.HORA_ATE_LEITURA_ATACADAO,
    A.DIR_EXP_REL_TRANSACAO,
    A.DIR_EXP_REL_VENDA,
    A.DIR_EXP_REL_VENCIMENTO,
    A.DIR_EXP_REL_CONSOLIDADO,
    A.DIR_EXP_REL_SALDO_CONSILIACAO,
    A.DIR_EXP_REL_RECEBIMENTO,
    A.DIR_EXP_REL_CHEQUES, 
    A.FL_HOMOLOGACAO_TSYS,
    A.FL_REPROCESSAR_NSU_PROCESSADO,
    A.FL_REPROCESSAR_NSU_CESSIONADO,
    A.STATUS_AGENDADOR,
    A.STATUS_EXPURGO,
    A.DIAS_AGUARDAR_EXPURGO,
    A.DIAS_IGNORAR_EXPURGO,
    A.DAT_ULTM_EXPURGO,
    A.DIR_EXP_CESSAO_ATACADAO,
    A.DIR_EXP_CESSAO_GALERIA,
    A.FL_ENVIO_CESSAO_AUT_ATACADAO,
    A.FL_ENVIO_CESSAO_AUT_GALERIA,
    A.LAYOUT_CCI,
    A.LAYOUT_ATACADAO,
    A.LAYOUT_GALERIA FROM TB_CONFIGURACAO_SISTEMA A;
  END PR_CONFIGURACAO_SISTEMA_B;
  PROCEDURE PR_CONFIGURACAO_SISTEMA_S(
      pCODIGO_CONFIGURACAO_SISTEMA   NUMBER   := NULL,
      pPRAZO_PAGAMENTO               NUMBER   := NULL,
      pDIAS_VENCIDO                  NUMBER   := NULL,
      pDIR_IMP_EXTRATO               VARCHAR2 := NULL,
      pDIR_IMP_EXTRATO_ATACADAO      VARCHAR2 := NULL,
      pDIR_IMP_EXTRATO_GALERIA       VARCHAR2 := NULL,
      pDIR_EXP_CESSAO                VARCHAR2 := NULL,
      pDIR_IMP_RET_CESSAO            VARCHAR2 := NULL,
      pDIR_EXP_MATERA_CONTABIL       VARCHAR2 := NULL,
      pDIR_EXP_MATERA_FINANCEIRO     VARCHAR2 := NULL,
      pDIR_EXP_MATERA_EXTRATO        VARCHAR2 :=NULL,
      pFL_ENVIO_MATERA_AUT           CHAR     := NULL,
      pFL_ENVIO_CESSAO_AUT           CHAR     := NULL,
      pHORA_DE_LEITURA_EXTRATO       VARCHAR2 := NULL,
      pHORA_ATE_LEITURA_RET_CESSAO   VARCHAR2 := NULL,
      pDIR_IMP_ATACADAO_TSYS         VARCHAR2 := NULL,
      pDIR_IMP_ATACADAO_SITEF        VARCHAR2 := NULL,
      pDIR_IMP_ATACADAO_MA           VARCHAR2 := NULL,
      pDIR_EXP_ATACADAO_CSUSEM       VARCHAR2 := NULL,
      pDIR_EXP_ATACADAO_CONC         VARCHAR2 := NULL,
      pHORA_DE_LEITURA_ATACADAO      VARCHAR2 := NULL,
      pHORA_ATE_LEITURA_ATACADAO     VARCHAR2 := NULL,
      pDIR_EXP_REL_TRANSACAO         VARCHAR2 := NULL,
      pDIR_EXP_REL_VENDA             VARCHAR2 := NULL,
      pDIR_EXP_REL_VENCIMENTO        VARCHAR2 := NULL,
      pDIR_EXP_REL_CONSOLIDADO       VARCHAR2 := NULL,
      pDIR_EXP_REL_SALDO_CONSILIACAO VARCHAR2 := NULL,
      pDIR_EXP_REL_RECEBIMENTO       VARCHAR2 := NULL,
      pDIR_EXP_REL_CHEQUES           VARCHAR2 := NULL, 
      pFL_HOMOLOGACAO_TSYS           CHAR     := NULL,
      pFL_REPROCESSAR_NSU_PROCESSADO CHAR     := NULL,
      pFL_REPROCESSAR_NSU_CESSIONADO CHAR     := NULL,
      pSTATUS_AGENDADOR              CHAR     := NULL,
      pSTATUS_EXPURGO                CHAR     := NULL,
      pDIAS_AGUARDAR_EXPURGO         NUMBER   := NULL,
      pDIAS_IGNORAR_EXPURGO          NUMBER   := NULL,
      pDAT_ULTM_EXPURGO              DATE     := NULL,
      pDIR_EXP_CESSAO_ATACADAO      VARCHAR2 := NULL,
      pDIR_EXP_CESSAO_GALERIA       VARCHAR2 := NULL,
      pFL_ENVIO_CESSAO_AUT_ATACADAO CHAR     := NULL,
      pFL_ENVIO_CESSAO_AUT_GALERIA  CHAR     := NULL,
      pLAYOUT_CCI                   VARCHAR2 := NULL,
      pLAYOUT_ATACADAO              VARCHAR2 := NULL,
      pLAYOUT_GALERIA               VARCHAR2 := NULL,
      retornarRegistro              CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_CONFIGURACAO_SISTEMA NUMBER;
    qtde                         NUMBER;
  BEGIN
    vCODIGO_CONFIGURACAO_SISTEMA := pCODIGO_CONFIGURACAO_SISTEMA;
    qtde                         := 0;
    SELECT COUNT(*)
    INTO qtde
    FROM TB_CONFIGURACAO_SISTEMA;
    IF qtde = 0 THEN
      SELECT SQ_CONFIGURACAO_SISTEMA.nextval
      INTO vCODIGO_CONFIGURACAO_SISTEMA
      FROM dual;
      INSERT
      INTO TB_CONFIGURACAO_SISTEMA
        (
          CODIGO_CONFIGURACAO_SISTEMA,
          PRAZO_PAGAMENTO,
          DIAS_VENCIDO,
          DIR_IMP_EXTRATO,
          DIR_IMP_EXTRATO_ATACADAO,
          DIR_IMP_EXTRATO_GALERIA,
          DIR_EXP_CESSAO,
          DIR_IMP_RET_CESSAO,
          DIR_EXP_MATERA_CONTABIL,
          DIR_EXP_MATERA_FINANCEIRO,
          DIR_EXP_MATERA_EXTRATO,
          FL_ENVIO_MATERA_AUT,
          FL_ENVIO_CESSAO_AUT,
          HORA_DE_LEITURA_EXTRATO,
          HORA_ATE_LEITURA_RET_CESSAO,
          DIR_IMP_ATACADAO_TSYS ,
          DIR_IMP_ATACADAO_SITEF ,
          DIR_IMP_ATACADAO_MA,
          DIR_EXP_ATACADAO_CSUSEM,
          DIR_EXP_ATACADAO_CONC,
          HORA_DE_LEITURA_ATACADAO,
          HORA_ATE_LEITURA_ATACADAO,
          DIR_EXP_REL_TRANSACAO,
          DIR_EXP_REL_VENDA,
          DIR_EXP_REL_VENCIMENTO,
          DIR_EXP_REL_CONSOLIDADO,
          DIR_EXP_REL_SALDO_CONSILIACAO,
          DIR_EXP_REL_RECEBIMENTO,
          DIR_EXP_REL_CHEQUES, 
          FL_HOMOLOGACAO_TSYS,
          FL_REPROCESSAR_NSU_PROCESSADO,
          FL_REPROCESSAR_NSU_CESSIONADO,
          STATUS_AGENDADOR,
          STATUS_EXPURGO,
          DIAS_AGUARDAR_EXPURGO,
          DIAS_IGNORAR_EXPURGO,
          DAT_ULTM_EXPURGO,
          DIR_EXP_CESSAO_ATACADAO,
          DIR_EXP_CESSAO_GALERIA,
          FL_ENVIO_CESSAO_AUT_ATACADAO,
          FL_ENVIO_CESSAO_AUT_GALERIA,
          LAYOUT_CCI,
          LAYOUT_ATACADAO,
          LAYOUT_GALERIA
        )
        VALUES
        (
          vCODIGO_CONFIGURACAO_SISTEMA,
          pPRAZO_PAGAMENTO,
          pDIAS_VENCIDO,
          pDIR_IMP_EXTRATO,
          pDIR_IMP_EXTRATO_ATACADAO,
          pDIR_IMP_EXTRATO_GALERIA,
          pDIR_EXP_CESSAO,
          pDIR_IMP_RET_CESSAO,
          pDIR_EXP_MATERA_CONTABIL,
          pDIR_EXP_MATERA_FINANCEIRO,
          pDIR_EXP_MATERA_EXTRATO,
          pFL_ENVIO_MATERA_AUT,
          pFL_ENVIO_CESSAO_AUT,
          pHORA_DE_LEITURA_EXTRATO,
          pHORA_ATE_LEITURA_RET_CESSAO,
          pDIR_IMP_ATACADAO_TSYS ,
          pDIR_IMP_ATACADAO_SITEF ,
          pDIR_IMP_ATACADAO_MA,
          pDIR_EXP_ATACADAO_CSUSEM,
          pDIR_EXP_ATACADAO_CONC,
          pHORA_DE_LEITURA_ATACADAO,
          pHORA_ATE_LEITURA_ATACADAO,
          pDIR_EXP_REL_TRANSACAO,
          pDIR_EXP_REL_VENDA,
          pDIR_EXP_REL_VENCIMENTO,
          pDIR_EXP_REL_CONSOLIDADO,
          pDIR_EXP_REL_SALDO_CONSILIACAO,
          pDIR_EXP_REL_RECEBIMENTO,
          pDIR_EXP_REL_CHEQUES, 
          pFL_HOMOLOGACAO_TSYS,
          pFL_REPROCESSAR_NSU_PROCESSADO,
          pFL_REPROCESSAR_NSU_CESSIONADO,
          pSTATUS_AGENDADOR,
          pSTATUS_EXPURGO,
          pDIAS_AGUARDAR_EXPURGO,
          pDIAS_IGNORAR_EXPURGO,
          pDAT_ULTM_EXPURGO,
          pDIR_EXP_CESSAO_ATACADAO,
          pDIR_EXP_CESSAO_GALERIA,
          pFL_ENVIO_CESSAO_AUT_ATACADAO,
          pFL_ENVIO_CESSAO_AUT_GALERIA,
          pLAYOUT_CCI,
          pLAYOUT_ATACADAO,
          pLAYOUT_GALERIA
        );
    ELSE
      UPDATE TB_CONFIGURACAO_SISTEMA
      SET PRAZO_PAGAMENTO             = pPRAZO_PAGAMENTO,
        DIAS_VENCIDO                  = pDIAS_VENCIDO,
        DIR_IMP_EXTRATO               = pDIR_IMP_EXTRATO,
        DIR_IMP_EXTRATO_ATACADAO      = pDIR_IMP_EXTRATO_ATACADAO,
        DIR_IMP_EXTRATO_GALERIA       = pDIR_IMP_EXTRATO_GALERIA,
        DIR_EXP_CESSAO                = pDIR_EXP_CESSAO,
        DIR_IMP_RET_CESSAO            = pDIR_IMP_RET_CESSAO,
        DIR_EXP_MATERA_CONTABIL       = pDIR_EXP_MATERA_CONTABIL,
        DIR_EXP_MATERA_FINANCEIRO     = pDIR_EXP_MATERA_FINANCEIRO,
        DIR_EXP_MATERA_EXTRATO        = pDIR_EXP_MATERA_EXTRATO,
        FL_ENVIO_MATERA_AUT           = pFL_ENVIO_MATERA_AUT,
        FL_ENVIO_CESSAO_AUT           = pFL_ENVIO_CESSAO_AUT,
        HORA_DE_LEITURA_EXTRATO       = pHORA_DE_LEITURA_EXTRATO,
        HORA_ATE_LEITURA_RET_CESSAO   = pHORA_ATE_LEITURA_RET_CESSAO,
        DIR_IMP_ATACADAO_TSYS         = pDIR_IMP_ATACADAO_TSYS,
        DIR_IMP_ATACADAO_SITEF        = pDIR_IMP_ATACADAO_SITEF,
        DIR_IMP_ATACADAO_MA           = pDIR_IMP_ATACADAO_MA,
        DIR_EXP_ATACADAO_CSUSEM       = pDIR_EXP_ATACADAO_CSUSEM,
        DIR_EXP_ATACADAO_CONC         = pDIR_EXP_ATACADAO_CONC,
        HORA_DE_LEITURA_ATACADAO      = pHORA_DE_LEITURA_ATACADAO,
        HORA_ATE_LEITURA_ATACADAO     = pHORA_ATE_LEITURA_ATACADAO,
        DIR_EXP_REL_TRANSACAO         = pDIR_EXP_REL_TRANSACAO,
        DIR_EXP_REL_VENDA             = pDIR_EXP_REL_VENDA,
        DIR_EXP_REL_VENCIMENTO        = pDIR_EXP_REL_VENCIMENTO,
        DIR_EXP_REL_CONSOLIDADO       = pDIR_EXP_REL_CONSOLIDADO,
        DIR_EXP_REL_SALDO_CONSILIACAO = pDIR_EXP_REL_SALDO_CONSILIACAO,
        DIR_EXP_REL_RECEBIMENTO       = pDIR_EXP_REL_RECEBIMENTO,
        DIR_EXP_REL_CHEQUES           = pDIR_EXP_REL_CHEQUES, 
        FL_HOMOLOGACAO_TSYS           = pFL_HOMOLOGACAO_TSYS,
        FL_REPROCESSAR_NSU_PROCESSADO = pFL_REPROCESSAR_NSU_PROCESSADO,
        FL_REPROCESSAR_NSU_CESSIONADO = pFL_REPROCESSAR_NSU_CESSIONADO,
        STATUS_AGENDADOR              = NVL(pSTATUS_AGENDADOR, STATUS_AGENDADOR), 
        STATUS_EXPURGO                = pSTATUS_EXPURGO,
        DIAS_AGUARDAR_EXPURGO         = pDIAS_AGUARDAR_EXPURGO,
        DIAS_IGNORAR_EXPURGO          = pDIAS_IGNORAR_EXPURGO,
        DAT_ULTM_EXPURGO              = pDAT_ULTM_EXPURGO,
        DIR_EXP_CESSAO_ATACADAO      = pDIR_EXP_CESSAO_ATACADAO,
        DIR_EXP_CESSAO_GALERIA       = pDIR_EXP_CESSAO_GALERIA,
        FL_ENVIO_CESSAO_AUT_ATACADAO = pFL_ENVIO_CESSAO_AUT_ATACADAO,
        FL_ENVIO_CESSAO_AUT_GALERIA  = pFL_ENVIO_CESSAO_AUT_GALERIA,
        LAYOUT_CCI                   = pLAYOUT_CCI,
        LAYOUT_ATACADAO              = pLAYOUT_ATACADAO,
        LAYOUT_GALERIA               = pLAYOUT_GALERIA;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_CONFIGURACAO_SISTEMA_B(retcur => retcur);
    END IF;
  END PR_CONFIGURACAO_SISTEMA_S;
  PROCEDURE PR_CONFIGURACAO_SISTEMA_R(
      pCODIGO_CONFIGURACAO_SISTEMA NUMBER)
  AS
  BEGIN
    DELETE TB_CONFIGURACAO_SISTEMA
    WHERE CODIGO_CONFIGURACAO_SISTEMA = pCODIGO_CONFIGURACAO_SISTEMA;
  END PR_CONFIGURACAO_SISTEMA_R;
  PROCEDURE PR_CONFIG_TIPO_REGISTRO_S(
      pCODIGO_CONFIG_TIPO_REGISTRO NUMBER   := NULL,
      pTIPO_ARQUIVO                VARCHAR2 := NULL,
      pTIPO_REGISTRO               VARCHAR2 := NULL,
      pFL_ENVIAR_CCI               CHAR     := NULL,
      pFL_GERAR_AGENDA             CHAR     := NULL,
      pDIAS_REPASSE                INT      := NULL,
      pATIVO                       CHAR     := NULL,
      retornarRegistro             CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_CONFIG_TIPO_REGISTRO NUMBER;
  BEGIN
    vCODIGO_CONFIG_TIPO_REGISTRO := pCODIGO_CONFIG_TIPO_REGISTRO;
    IF vCODIGO_CONFIG_TIPO_REGISTRO IS NULL THEN
      SELECT SQ_CONFIGURACAO_TIPO_REGISTRO.nextval
      INTO vCODIGO_CONFIG_TIPO_REGISTRO
      FROM dual;
      INSERT
      INTO TB_CONFIGURACAO_TIPO_REGISTRO
        (
          CODIGO_CONFIG_TIPO_REGISTRO,
          TIPO_ARQUIVO,
          TIPO_REGISTRO,
          FL_ENVIAR_CCI,
          FL_GERAR_AGENDA,
          DIAS_REPASSE
        )
        VALUES
        (
          vCODIGO_CONFIG_TIPO_REGISTRO,
          pTIPO_ARQUIVO,
          pTIPO_REGISTRO,
          pFL_ENVIAR_CCI,
          pFL_GERAR_AGENDA,
          pDIAS_REPASSE
        );
    ELSE
      UPDATE TB_CONFIGURACAO_TIPO_REGISTRO
      SET CODIGO_CONFIG_TIPO_REGISTRO   = vCODIGO_CONFIG_TIPO_REGISTRO,
        TIPO_ARQUIVO                    = pTIPO_ARQUIVO,
        TIPO_REGISTRO                   = pTIPO_REGISTRO,
        FL_ENVIAR_CCI                   = pFL_ENVIAR_CCI,
        FL_GERAR_AGENDA                 = pFL_GERAR_AGENDA,
        DIAS_REPASSE                    = pDIAS_REPASSE
      WHERE CODIGO_CONFIG_TIPO_REGISTRO = vCODIGO_CONFIG_TIPO_REGISTRO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_CONFIG_TIPO_REGISTRO_B( pCODIGO_CONFIG_TIPO_REGISTRO => vCODIGO_CONFIG_TIPO_REGISTRO, retcur => retcur);
    END IF;
  END PR_CONFIG_TIPO_REGISTRO_S;
  PROCEDURE PR_CONFIG_TIPO_REGISTRO_B(
      pCODIGO_CONFIG_TIPO_REGISTRO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_CONFIG_TIPO_REGISTRO,
    A.TIPO_ARQUIVO,
    A.TIPO_REGISTRO,
    A.FL_ENVIAR_CCI,
    A.FL_GERAR_AGENDA,
    A.DIAS_REPASSE FROM TB_CONFIGURACAO_TIPO_REGISTRO A WHERE A.CODIGO_CONFIG_TIPO_REGISTRO = pCODIGO_CONFIG_TIPO_REGISTRO;
  END PR_CONFIG_TIPO_REGISTRO_B;
  PROCEDURE PR_CONFIG_TIPO_REGISTRO_L(
      pTIPO_ARQUIVO  VARCHAR2 := NULL,
      pFL_ENVIAR_CCI VARCHAR2 := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(500);
  BEGIN
    vQuery := '';
    IF pTIPO_ARQUIVO   IS NOT NULL THEN
      IF pTIPO_ARQUIVO <> 'GRUPO' THEN 
        vQuery         := vQuery || 'A.TIPO_ARQUIVO = ''' || pTIPO_ARQUIVO || ''' and ';
      END IF;
    ELSE                                                              
      vQuery := vQuery || 'A.TIPO_ARQUIVO = ''ArquivoTSYSCCI'' and '; 
    END IF;
    IF pFL_ENVIAR_CCI IS NOT NULL THEN
      vQuery          := vQuery || 'A.FL_ENVIAR_CCI = ''' || pFL_ENVIAR_CCI || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := '  
select     
A.CODIGO_CONFIG_TIPO_REGISTRO  
, A.TIPO_ARQUIVO  
, A.TIPO_REGISTRO  
, A.FL_ENVIAR_CCI  
, A.FL_GERAR_AGENDA  
, A.DIAS_REPASSE  
from     
TB_CONFIGURACAO_TIPO_REGISTRO A   
' || vQuery || ' ORDER BY A.CODIGO_CONFIG_TIPO_REGISTRO, A.TIPO_ARQUIVO, A.TIPO_REGISTRO '; 
    OPEN retcur FOR vQuery;
  END PR_CONFIG_TIPO_REGISTRO_L;
  PROCEDURE PR_CONFIG_TIPO_REGISTRO_R(
      pCODIGO_CONFIG_TIPO_REGISTRO NUMBER)
  AS
  BEGIN
    DELETE TB_CONFIGURACAO_TIPO_REGISTRO
    WHERE CODIGO_CONFIG_TIPO_REGISTRO = pCODIGO_CONFIG_TIPO_REGISTRO;
  END PR_CONFIG_TIPO_REGISTRO_R;
  PROCEDURE PR_RELATORIO_TRANSACAO_L(
      pFILTRO_ORIGEM                 VARCHAR2 := NULL,
      pFILTRO_EMPRESA_GRUPO          NUMBER   := NULL,
      pFILTRO_EMPRESA_SUBGRUPO       NUMBER   := NULL,
      pFILTRO_ESTABELECIMENTO        NUMBER   := NULL,
      pFILTRO_FAVORECIDO             NUMBER   := NULL,
      pFILTRO_PRODUTO                NUMBER   := NULL,
      pFILTRO_PLANO                  NUMBER   := NULL,
      pFILTRO_MEIO_CAPTURA           NUMBER   := NULL,
      pFILTRO_TIPO_REGISTRO          VARCHAR2 := NULL,
      pFILTRO_TIPO_TRANSACAO         VARCHAR2 := NULL,
      pFILTRO_NUMERO_PAGAMENTO       NUMBER   := NULL,
      pFILTRO_CONTA_CLIENTE          VARCHAR2 := NULL, 
      pFILTRO_DATA_PROCESSAMENTO_DE  DATE     := NULL,
      pFILTRO_DATA_PROCESSAMENTO_ATE DATE     := NULL,
      pFILTRO_DATA_TRANSACAO_DE      DATE     := NULL,
      pFILTRO_DATA_TRANSACAO_ATE     DATE     := NULL,
      pFILTRO_DATA_VENCIMENTO_DE     DATE     := NULL,
      pFILTRO_DATA_VENCIMENTO_ATE    DATE     := NULL,
      pFILTRO_DATA_AGENDAMENTO_DE    DATE     := NULL,
      pFILTRO_DATA_AGENDAMENTO_ATE   DATE     := NULL,
      pFILTRO_DATA_ENVIO_MATERA_DE   DATE     := NULL,
      pFILTRO_DATA_ENVIO_MATERA_ATE  DATE     := NULL,
      pFILTRO_DATA_LIQUIDACAO_DE     DATE     := NULL,
      pFILTRO_DATA_LIQUIDACAO_ATE    DATE     := NULL,
      pFILTRO_STATUS_RETORNO         NUMBER   := NULL,
      pFILTRO_STATUS_PAGAMENTO       NUMBER   := NULL,
      pFILTRO_STATUS_CONCILIACAO     NUMBER   := NULL,
      pFILTRO_STATUS_VALIDACAO       NUMBER   := NULL,
      pFILTRO_DATA_MOVIMENTO_DE      DATE     := NULL,
      pFILTRO_DATA_MOVIMENTO_ATE     DATE     := NULL,
      pFILTRO_STATUS_CANC_ON_LINE    CHAR     := NULL,
      pFILTRO_DATA_RET_CESSAO_DE     DATE     := NULL,
      pFILTRO_DATA_RET_CESSAO_ATE    DATE     := NULL,
      pFILTRO_TIPO_FINANC_CTBIL      CHAR     := NULL,
      pFILTRO_NSU_HOST               VARCHAR2 := NULL,
      pFILTRO_REFERENCIA             VARCHAR2 := NULL,
      maxLinhas                      INT      := 0,
      retcur OUT TP_REFCURSOR,
      retcur_query OUT TP_REFCURSOR)
  AS
    vCorpoView VARCHAR2(32000);
    vCorpoAJ CLOB;
    vWhereAJ CLOB;
    vCorpoAV CLOB;
    vWhereAV CLOB;
    vCorpoCV CLOB;
    vWhereCV CLOB;
    vCorpoAP CLOB;
    vWhereAP CLOB;
    vCorpoCP CLOB;
    vWhereCP CLOB;
    formatoData   VARCHAR2(20);
    vCODIGO_LISTA NUMBER; 
  BEGIN
    formatoData := 'yyyy-mm-dd';
    vWhereAJ := '';
    vWhereAP := '';
    vWhereCP := '';
    vWhereCV := '';
    vWhereAV := '';
    SELECT CODIGO_LISTA
    INTO vCODIGO_LISTA
    FROM TB_LISTA
    WHERE NOME_LISTA = 'Referencia';
   vCorpoView :=
    'SELECT   
CODIGO_TRANSACAO,   
CODIGO_AJUSTE,   
TIPO_AJUSTE,   
MOTIVO_AJUSTE,   
TIPO_LANCAMENTO,   
DATA_REPASSE,   
DATA_TRANSACAO_ORIGINAL,   
NSU_HOST_ORIGINAL,   
VALOR_BRUTO,   
VALOR_DESCONTO,   
VALOR_LIQUIDO,   
BANCO,   
AGENCIA,   
CONTA,   
CODIGO_ARQUIVO,   
CODIGO_ESTABELECIMENTO,   
CNPJ,   
CODIGO_PAGAMENTO,   
CODIGO_PROCESSO,   
DATA_TRANSACAO,   
NSU_HOST,   
STATUS_CONCILIACAO,   
STATUS_RETORNO,   
STATUS_TRANSACAO,   
TIPO_TRANSACAO,   
NOME_ESTABELECIMENTO,   
CODIGO_GRUPO,   
NOME_GRUPO,   
CODIGO_SUBGRUPO,   
NOME_SUBGRUPO,   
CODIGO_FAVORECIDO,   
NOME_FAVORECIDO,   
ORIGEM,   
DATA_PROCESSAMENTO,   
NUMERO_PAGAMENTO,   
STATUS_PAGAMENTO,   
CODIGO_ANULACAO,   
CODIGO_AUTORIZACAO_ORIGINAL,   
NUMERO_CARTAO,   
NUMERO_CONTA_CLIENTE,   
MOTIVO_ANULACAO,   
NSU_TEF_ORIGINAL,   
NUMERO_PARCELA,   
NUMERO_PARCELA_TOTAL,   
CODIGO_PRODUTO,   
NOME_PRODUTO,   
VALOR_PARCELA,   
VALOR_PARCELA_DESCONTO,   
VALOR_PARCELA_LIQUIDO,   
MEIO_CAPTURA,   
CODIGO_AUTORIZACAO,   
FORMA_MEIO_PAGAMENTO,   
MEIO_PAGAMENTO,   
MEIO_PAGAMENTO_SEQ,   
MEIO_PAGAMENTO_VALOR,   
QUANTIDADE_MEIO_PAGAMENTO,   
NSU_TEF,   
CODIGO_PLANO,   
NOME_PLANO,   
MODALIDADE,   
CUPOM_FISCAL,   
TIPO_PRODUTO,   
DATA_AGENDAMENTO,   
DATA_ENVIO_CCI,   
DATA_ENVIO_MATERA,   
DATA_GERACAO_AGENDA,   
DATA_LIQUIDACAO,   
DATA_RETORNO_CCI,   
DATA_VENCIMENTO,   
STATUS_IMPORTACAO,   
TIPO_REGISTRO,   
USUARIO_ENVIO_CCI,   
USUARIO_ENVIO_MATERA,   
USUARIO_RETORNO_CCI,   
STATUS_VALIDACAO,   
CODIGO_MEIO_CAPTURA,   
ORIGEM2,   
NOME_ARQUIVO,   
STATUS_ARQUIVO,   
DATA_MOVIMENTO,   
DATA_PRE_AUTORIZACAO,   
TIPO_FINANCEIRO_CONTABIL,   
NSU_HOST_REVERSAO,   
REFERENCIA   
FROM'
    ;
    vCorpoAJ :=
    '   
SELECT   
A.CODIGO_TRANSACAO AS CODIGO_TRANSACAO,   
A.CODIGO_AJUSTE AS CODIGO_AJUSTE,   
A.TIPO_AJUSTE AS TIPO_AJUSTE,   
A.MOTIVO_AJUSTE AS MOTIVO_AJUSTE,   
A.TIPO_LANCAMENTO AS TIPO_LANCAMENTO,   
B.DATA_REPASSE AS DATA_REPASSE,   
A.DATA_TRANSACAO_ORIGINAL AS DATA_TRANSACAO_ORIGINAL,   
A.NSU_HOST_ORIGINAL AS NSU_HOST_ORIGINAL,   
A.VALOR_AJUSTE AS VALOR_BRUTO,   
A.VALOR_DESCONTO AS VALOR_DESCONTO,   
A.VALOR_LIQUIDO AS VALOR_LIQUIDO,   
Z.BANCO AS BANCO,   
Z.AGENCIA AS AGENCIA,   
Z.CONTA AS CONTA,   
B.CODIGO_ARQUIVO AS CODIGO_ARQUIVO,   
B.CODIGO_ESTABELECIMENTO AS CODIGO_ESTABELECIMENTO,   
C.CNPJ AS CNPJ,   
B.CODIGO_PAGAMENTO AS CODIGO_PAGAMENTO,   
B.CODIGO_PROCESSO AS CODIGO_PROCESSO,   
B.DATA_TRANSACAO AS DATA_TRANSACAO,   
B.NSU_HOST AS NSU_HOST,   
B.STATUS_CONCILIACAO AS STATUS_CONCILIACAO,   
B.STATUS_RET_CESSAO AS STATUS_RETORNO,   
B.STATUS_TRANSACAO AS STATUS_TRANSACAO,   
B.TIPO_TRANSACAO AS TIPO_TRANSACAO,   
C.RAZAO_SOCIAL AS NOME_ESTABELECIMENTO,   
E.CODIGO_EMPRESA_GRUPO AS CODIGO_GRUPO,   
E.NOME_EMPRESA_GRUPO AS NOME_GRUPO,   
D.CODIGO_EMPRESA_SUBGRUPO AS CODIGO_SUBGRUPO,   
D.NOME_EMPRESA_SUBGRUPO AS NOME_SUBGRUPO,   
F.CODIGO_FAVORECIDO AS CODIGO_FAVORECIDO,   
G.NOME_FAVORECIDO AS NOME_FAVORECIDO,   
H.TIPO_ARQUIVO AS ORIGEM,   
I.DATA_INCLUSAO AS DATA_PROCESSAMENTO,   
B.CODIGO_PAGAMENTO as NUMERO_PAGAMENTO,   
null AS STATUS_PAGAMENTO,
NULL AS CODIGO_ANULACAO,   
NULL AS CODIGO_AUTORIZACAO_ORIGINAL,   
A.NUMERO_CARTAO AS NUMERO_CARTAO,   
A.NUMERO_CONTA_CLIENTE AS NUMERO_CONTA_CLIENTE,   
NULL AS MOTIVO_ANULACAO,   
NULL AS NSU_TEF_ORIGINAL,   
NULL AS NUMERO_PARCELA,   
NULL AS NUMERO_PARCELA_TOTAL,   
B.CODIGO_PRODUTO AS CODIGO_PRODUTO,   
M.NOME_PRODUTO AS NOME_PRODUTO,   
NULL AS VALOR_PARCELA,   
NULL AS VALOR_PARCELA_DESCONTO,   
NULL AS VALOR_PARCELA_LIQUIDO,   
NULL AS MEIO_CAPTURA,   
NULL AS CODIGO_AUTORIZACAO,   
NULL AS FORMA_MEIO_PAGAMENTO,   
NULL AS MEIO_PAGAMENTO,   
NULL AS MEIO_PAGAMENTO_SEQ,   
NULL AS MEIO_PAGAMENTO_VALOR,   
NULL AS QUANTIDADE_MEIO_PAGAMENTO,   
NULL AS NSU_TEF,   
NULL AS CODIGO_PLANO,   
NULL AS NOME_PLANO,   
NULL AS MODALIDADE,   
NULL AS CUPOM_FISCAL,   
NULL AS TIPO_PRODUTO,   
B.DATA_REPASSE_CALCULADA AS DATA_AGENDAMENTO,   
NULL AS DATA_ENVIO_CCI,   
NULL AS DATA_ENVIO_MATERA,   
B.DATA_REPASSE_CALCULADA AS DATA_GERACAO_AGENDA,   
null AS DATA_LIQUIDACAO, 
NULL AS DATA_RETORNO_CCI,   
NULL AS DATA_VENCIMENTO,   
I.STATUS_PROCESSO AS STATUS_IMPORTACAO,   
B.TIPO_TRANSACAO AS TIPO_REGISTRO,   
NULL AS USUARIO_ENVIO_CCI,   
NULL AS USUARIO_ENVIO_MATERA,   
NULL AS USUARIO_RETORNO_CCI,   
NULL AS STATUS_VALIDACAO ,   
NULL AS CODIGO_MEIO_CAPTURA,   
N.TIPO_ARQUIVO AS ORIGEM2,   
N.NOME_ARQUIVO AS NOME_ARQUIVO,   
N.STATUS_ARQUIVO AS STATUS_ARQUIVO,   
B.DATA_MOVIMENTO AS DATA_MOVIMENTO,   
NULL AS DATA_PRE_AUTORIZACAO,   
B.TIPO_FINANCEIRO_CONTABIL,   
NULL AS NSU_HOST_REVERSAO,   
B.REFERENCIA     

FROM TB_TRANSACAO B   
INNER JOIN TB_TRANSACAO_AJUSTE A ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO   
LEFT JOIN TB_ESTABELECIMENTO C ON B.CODIGO_ESTABELECIMENTO = C.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_EMPRESA_SUBGRUPO D ON C.CODIGO_EMPRESA_SUBGRUPO = D.CODIGO_EMPRESA_SUBGRUPO   
LEFT JOIN TB_EMPRESA_GRUPO E ON D.CODIGO_EMPRESA_GRUPO = E.CODIGO_EMPRESA_GRUPO   
LEFT JOIN TB_ESTABELECIMENTO_FAVORECIDO F ON B.CODIGO_ESTABELECIMENTO = F.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_FAVORECIDO G ON F.CODIGO_FAVORECIDO = G.CODIGO_FAVORECIDO   
LEFT JOIN TB_ARQUIVO H ON B.CODIGO_ARQUIVO = H.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO I ON B.CODIGO_PROCESSO = I.CODIGO_PROCESSO   
LEFT JOIN TB_PRODUTO M ON B.CODIGO_PRODUTO = M.CODIGO_PRODUTO   
LEFT JOIN TB_ARQUIVO N ON B.CODIGO_ARQUIVO2 = N.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO R ON B.CODIGO_PROCESSO_RET_CESSAO = R.CODIGO_PROCESSO   
LEFT JOIN TB_CONTA_FAVORECIDO Z ON B.CODIGO_CONTA_FAVORECIDO = Z.CODIGO_CONTA_FAVORECIDO   
left join TB_LISTA_ITEM TLI               on B.REFERENCIA = TLI.VALOR and TLI.CODIGO_LISTA = '''
    || vCODIGO_LISTA || '''   
';
    vCorpoAP :=
    '   
SELECT   
K.CODIGO_TRANSACAO AS CODIGO_TRANSACAO,   
NULL AS CODIGO_AJUSTE,   
NULL AS TIPO_AJUSTE,   
NULL AS MOTIVO_AJUSTE,   
K.TIPO_LANCAMENTO AS TIPO_LANCAMENTO,   
B.DATA_REPASSE AS DATA_REPASSE,   
K.DATA_TRANSACAO_ORIGINAL AS DATA_TRANSACAO_ORIGINAL,   
K.NSU_HOST_ORIGINAL AS NSU_HOST_ORIGINAL,   
K.VALOR_PAGAMENTO AS VALOR_BRUTO,   
K.VALOR_DESCONTO AS VALOR_DESCONTO,   
K.VALOR_LIQUIDO AS VALOR_LIQUIDO,   
Z.BANCO AS BANCO,   
Z.AGENCIA AS AGENCIA,   
Z.CONTA AS CONTA,   
B.CODIGO_ARQUIVO AS CODIGO_ARQUIVO,   
B.CODIGO_ESTABELECIMENTO AS CODIGO_ESTABELECIMENTO,   
C.CNPJ AS CNPJ,   
B.CODIGO_PAGAMENTO AS CODIGO_PAGAMENTO,   
B.CODIGO_PROCESSO AS CODIGO_PROCESSO,   
B.DATA_TRANSACAO AS DATA_TRANSACAO,   
B.NSU_HOST AS NSU_HOST,   
B.STATUS_CONCILIACAO AS STATUS_CONCILIACAO,   
B.STATUS_RET_CESSAO AS STATUS_RETORNO,   
B.STATUS_TRANSACAO AS STATUS_TRANSACAO,   
B.TIPO_TRANSACAO AS TIPO_TRANSACAO,   
C.RAZAO_SOCIAL AS NOME_ESTABELECIMENTO,   
E.CODIGO_EMPRESA_GRUPO AS CODIGO_GRUPO,   
E.NOME_EMPRESA_GRUPO AS NOME_GRUPO,   
D.CODIGO_EMPRESA_SUBGRUPO AS CODIGO_SUBGRUPO,   
D.NOME_EMPRESA_SUBGRUPO AS NOME_SUBGRUPO,   
F.CODIGO_FAVORECIDO AS CODIGO_FAVORECIDO,   
G.NOME_FAVORECIDO AS NOME_FAVORECIDO,   
H.TIPO_ARQUIVO AS ORIGEM,   
I.DATA_INCLUSAO AS DATA_PROCESSAMENTO,   
B.CODIGO_PAGAMENTO AS NUMERO_PAGAMENTO,   
null AS STATUS_PAGAMENTO, 
K.CODIGO_ANULACAO AS CODIGO_ANULACAO,   
K.CODIGO_AUTORIZACAO_ORIGINAL AS CODIGO_AUTORIZACAO_ORIGINAL,   
K.NUMERO_CARTAO AS NUMERO_CARTAO,   
K.NUMERO_CONTA_CLIENTE AS NUMERO_CONTA_CLIENTE,   
K.MOTIVO_ANULACAO AS MOTIVO_ANULACAO,   
K.NSU_TEF_ORIGINAL AS NSU_TEF_ORIGINAL,   
NULL AS NUMERO_PARCELA,   
NULL AS NUMERO_PARCELA_TOTAL,   
B.CODIGO_PRODUTO AS CODIGO_PRODUTO,   
M.NOME_PRODUTO AS NOME_PRODUTO,   
NULL AS VALOR_PARCELA,   
NULL AS VALOR_PARCELA_DESCONTO,   
NULL AS VALOR_PARCELA_LIQUIDO,   
NULL AS MEIO_CAPTURA,   
NULL AS CODIGO_AUTORIZACAO,   
K.FORMA_MEIO_PAGAMENTO AS FORMA_MEIO_PAGAMENTO,   
NULL AS MEIO_PAGAMENTO,   
NULL AS MEIO_PAGAMENTO_SEQ,   
NULL AS MEIO_PAGAMENTO_VALOR,   
NULL AS QUANTIDADE_MEIO_PAGAMENTO,   
NULL AS NSU_TEF,   
NULL AS CODIGO_PLANO,   
NULL AS NOME_PLANO,   
NULL AS MODALIDADE,   
NULL AS CUPOM_FISCAL,   
NULL AS TIPO_PRODUTO,   
B.DATA_REPASSE_CALCULADA AS DATA_AGENDAMENTO,   
NULL AS DATA_ENVIO_CCI,   
NULL AS DATA_ENVIO_MATERA,   
B.DATA_REPASSE_CALCULADA AS DATA_GERACAO_AGENDA,   
null AS DATA_LIQUIDACAO, 
NULL AS DATA_RETORNO_CCI,   
NULL AS DATA_VENCIMENTO,   
I.STATUS_PROCESSO AS STATUS_IMPORTACAO,   
B.TIPO_TRANSACAO AS TIPO_REGISTRO,   
NULL AS USUARIO_ENVIO_CCI,   
NULL AS USUARIO_ENVIO_MATERA,   
NULL AS USUARIO_RETORNO_CCI,   
NULL AS STATUS_VALIDACAO ,   
NULL AS CODIGO_MEIO_CAPTURA,   
N.TIPO_ARQUIVO AS ORIGEM2,   
N.NOME_ARQUIVO AS NOME_ARQUIVO,   
N.STATUS_ARQUIVO AS STATUS_ARQUIVO,   
B.DATA_MOVIMENTO AS DATA_MOVIMENTO,   
NULL AS DATA_PRE_AUTORIZACAO,   
B.TIPO_FINANCEIRO_CONTABIL,   
NULL AS NSU_HOST_REVERSAO,   
B.REFERENCIA     

FROM  TB_TRANSACAO B   
INNER JOIN TB_TRANSACAO_ANULACAO_PAGTO K ON B.CODIGO_TRANSACAO = K.CODIGO_TRANSACAO   
LEFT JOIN TB_ESTABELECIMENTO C ON B.CODIGO_ESTABELECIMENTO = C.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_EMPRESA_SUBGRUPO D ON C.CODIGO_EMPRESA_SUBGRUPO = D.CODIGO_EMPRESA_SUBGRUPO   
LEFT JOIN TB_EMPRESA_GRUPO E ON D.CODIGO_EMPRESA_GRUPO = E.CODIGO_EMPRESA_GRUPO   
LEFT JOIN TB_ESTABELECIMENTO_FAVORECIDO F ON B.CODIGO_ESTABELECIMENTO = F.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_FAVORECIDO G ON F.CODIGO_FAVORECIDO = G.CODIGO_FAVORECIDO   
LEFT JOIN TB_ARQUIVO H ON B.CODIGO_ARQUIVO = H.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO I ON B.CODIGO_PROCESSO = I.CODIGO_PROCESSO   
LEFT JOIN TB_PRODUTO M ON B.CODIGO_PRODUTO = M.CODIGO_PRODUTO   
LEFT JOIN TB_ARQUIVO N ON B.CODIGO_ARQUIVO2 = N.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO R ON B.CODIGO_PROCESSO_RET_CESSAO = R.CODIGO_PROCESSO   
LEFT JOIN TB_CONTA_FAVORECIDO Z ON B.CODIGO_CONTA_FAVORECIDO = Z.CODIGO_CONTA_FAVORECIDO   
left join TB_LISTA_ITEM TLI               on B.REFERENCIA = TLI.VALOR and TLI.CODIGO_LISTA = '''
    || vCODIGO_LISTA || '''   
';
    vCorpoAV :=
    '   
SELECT   
L.CODIGO_TRANSACAO AS CODIGO_TRANSACAO,   
NULL AS CODIGO_AJUSTE,   
NULL AS TIPO_AJUSTE,   
NULL AS MOTIVO_AJUSTE,   
L.TIPO_LANCAMENTO AS TIPO_LANCAMENTO,   
B.DATA_REPASSE AS DATA_REPASSE,   
L.DATA_TRANSACAO_ORIGINAL AS DATA_TRANSACAO_ORIGINAL,   
L.NSU_HOST_ORIGINAL AS NSU_HOST_ORIGINAL,   
L.VALOR_ANULACAO AS VALOR_BRUTO,   
L.VALOR_DESCONTO AS VALOR_DESCONTO,   
L.VALOR_LIQUIDO AS VALOR_LIQUIDO,   
Z.BANCO AS BANCO,   
Z.AGENCIA AS AGENCIA,   
Z.CONTA AS CONTA,   
B.CODIGO_ARQUIVO AS CODIGO_ARQUIVO,   
B.CODIGO_ESTABELECIMENTO AS CODIGO_ESTABELECIMENTO,   
C.CNPJ AS CNPJ,   
B.CODIGO_PAGAMENTO AS CODIGO_PAGAMENTO,   
B.CODIGO_PROCESSO AS CODIGO_PROCESSO,   
B.DATA_TRANSACAO AS DATA_TRANSACAO,   
B.NSU_HOST AS NSU_HOST,   
B.STATUS_CONCILIACAO AS STATUS_CONCILIACAO,   
B.STATUS_RET_CESSAO AS STATUS_RETORNO,   
B.STATUS_TRANSACAO AS STATUS_TRANSACAO,   
B.TIPO_TRANSACAO AS TIPO_TRANSACAO,   
C.RAZAO_SOCIAL AS NOME_ESTABELECIMENTO,   
E.CODIGO_EMPRESA_GRUPO AS CODIGO_GRUPO,   
E.NOME_EMPRESA_GRUPO AS NOME_GRUPO,   
D.CODIGO_EMPRESA_SUBGRUPO AS CODIGO_SUBGRUPO,   
D.NOME_EMPRESA_SUBGRUPO AS NOME_SUBGRUPO,   
F.CODIGO_FAVORECIDO AS CODIGO_FAVORECIDO,   
G.NOME_FAVORECIDO AS NOME_FAVORECIDO,   
H.TIPO_ARQUIVO AS ORIGEM,   
I.DATA_INCLUSAO AS DATA_PROCESSAMENTO,   
B.CODIGO_PAGAMENTO AS NUMERO_PAGAMENTO,   
null AS STATUS_PAGAMENTO, 
L.CODIGO_ANULACAO AS CODIGO_ANULACAO,   
L.CODIGO_AUTORIZACAO_ORIGINAL AS CODIGO_AUTORIZACAO_ORIGINAL,   
L.NUMERO_CARTAO AS NUMERO_CARTAO,   
L.NUMERO_CONTA_CLIENTE AS NUMERO_CONTA_CLIENTE,   
L.MOTIVO_ANULACAO AS MOTIVO_ANULACAO,   
L.NSU_TEF_ORIGINAL AS NSU_TEF_ORIGINAL,   
L.NUMERO_PARCELA AS NUMERO_PARCELA,   
L.NUMERO_PARCELA_TOTAL AS NUMERO_PARCELA_TOTAL,   
B.CODIGO_PRODUTO AS CODIGO_PRODUTO,   
M.NOME_PRODUTO AS NOME_PRODUTO,   
L.VALOR_PARCELA AS VALOR_PARCELA,   
L.VALOR_PARCELA_DESCONTO AS VALOR_PARCELA_DESCONTO,   
L.VALOR_PARCELA_LIQUIDO AS VALOR_PARCELA_LIQUIDO,   
NULL AS MEIO_CAPTURA,   
NULL AS CODIGO_AUTORIZACAO,   
NULL AS FORMA_MEIO_PAGAMENTO,   
NULL AS MEIO_PAGAMENTO,   
NULL AS MEIO_PAGAMENTO_SEQ,   
NULL AS MEIO_PAGAMENTO_VALOR,   
NULL AS QUANTIDADE_MEIO_PAGAMENTO,   
NULL AS NSU_TEF,   
NULL AS CODIGO_PLANO,   
NULL AS NOME_PLANO,   
NULL AS MODALIDADE,   
NULL AS CUPOM_FISCAL,   
NULL AS TIPO_PRODUTO,   
B.DATA_REPASSE_CALCULADA AS DATA_AGENDAMENTO,   
NULL AS DATA_ENVIO_CCI,   
J.DATA_ENVIO AS DATA_ENVIO_MATERA,   
B.DATA_REPASSE_CALCULADA AS DATA_GERACAO_AGENDA,   
null AS DATA_LIQUIDACAO, 
O.DATA_INCLUSAO AS DATA_RETORNO_CCI,   
NULL AS DATA_VENCIMENTO,   
I.STATUS_PROCESSO AS STATUS_IMPORTACAO,   
B.TIPO_TRANSACAO AS TIPO_REGISTRO,   
J.CODIGO_USUARIO_ENVIO AS USUARIO_ENVIO_CCI,   
NULL AS USUARIO_ENVIO_MATERA,   
NULL AS USUARIO_RETORNO_CCI,   
NULL AS STATUS_VALIDACAO,   
NULL AS CODIGO_MEIO_CAPTURA,   
N.TIPO_ARQUIVO AS ORIGEM2,   
N.NOME_ARQUIVO AS NOME_ARQUIVO,   
N.STATUS_ARQUIVO AS STATUS_ARQUIVO,   
B.DATA_MOVIMENTO AS DATA_MOVIMENTO,   
NULL AS DATA_PRE_AUTORIZACAO,   
B.TIPO_FINANCEIRO_CONTABIL,   
NULL AS NSU_HOST_REVERSAO,   
B.REFERENCIA     

FROM TB_TRANSACAO B   
INNER JOIN TB_TRANSACAO_ANULACAO_VENDA L ON B.CODIGO_TRANSACAO = L.CODIGO_TRANSACAO   
LEFT JOIN TB_ESTABELECIMENTO C ON B.CODIGO_ESTABELECIMENTO = C.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_EMPRESA_SUBGRUPO D ON C.CODIGO_EMPRESA_SUBGRUPO = D.CODIGO_EMPRESA_SUBGRUPO   
LEFT JOIN TB_EMPRESA_GRUPO E ON D.CODIGO_EMPRESA_GRUPO = E.CODIGO_EMPRESA_GRUPO   
LEFT JOIN TB_ESTABELECIMENTO_FAVORECIDO F ON B.CODIGO_ESTABELECIMENTO = F.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_FAVORECIDO G ON F.CODIGO_FAVORECIDO = G.CODIGO_FAVORECIDO   
LEFT JOIN TB_ARQUIVO H ON B.CODIGO_ARQUIVO = H.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO I ON B.CODIGO_PROCESSO = I.CODIGO_PROCESSO   
LEFT JOIN TB_PAGAMENTO J ON B.CODIGO_PAGAMENTO = J.CODIGO_PAGAMENTO   
LEFT JOIN TB_PRODUTO M ON B.CODIGO_PRODUTO = M.CODIGO_PRODUTO   
LEFT JOIN TB_ARQUIVO N ON B.CODIGO_ARQUIVO2 = N.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO O ON B.CODIGO_PROCESSO_RET_CESSAO = O.CODIGO_PROCESSO   
LEFT JOIN TB_CONTA_FAVORECIDO Z ON B.CODIGO_CONTA_FAVORECIDO = Z.CODIGO_CONTA_FAVORECIDO   
left join TB_LISTA_ITEM TLI               on B.REFERENCIA = TLI.VALOR and TLI.CODIGO_LISTA = '''
    || vCODIGO_LISTA || '''   
';
    vCorpoCV :=
    '   
SELECT O.CODIGO_TRANSACAO AS CODIGO_TRANSACAO, NULL AS CODIGO_AJUSTE, NULL AS TIPO_AJUSTE, NULL AS MOTIVO_AJUSTE,   
O.TIPO_LANCAMENTO AS TIPO_LANCAMENTO,   
B.DATA_REPASSE AS DATA_REPASSE, NULL AS DATA_TRANSACAO_ORIGINAL, O.NSU_HOST_ORIGINAL,   
O.VALOR_VENDA AS VALOR_BRUTO,   
O.VALOR_DESCONTO AS VALOR_DESCONTO,   
O.VALOR_LIQUIDO AS VALOR_LIQUIDO,   
Z.BANCO AS BANCO,   
Z.AGENCIA AS AGENCIA,   
Z.CONTA AS CONTA,   
B.CODIGO_ARQUIVO AS CODIGO_ARQUIVO,   
B.CODIGO_ESTABELECIMENTO AS CODIGO_ESTABELECIMENTO,   
C.CNPJ AS CNPJ,   
B.CODIGO_PAGAMENTO AS CODIGO_PAGAMENTO,   
B.CODIGO_PROCESSO AS CODIGO_PROCESSO,   
B.DATA_TRANSACAO AS DATA_TRANSACAO,   
B.NSU_HOST AS NSU_HOST,   
B.STATUS_CONCILIACAO AS STATUS_CONCILIACAO,   
B.STATUS_RET_CESSAO AS STATUS_RETORNO,   
B.STATUS_TRANSACAO AS STATUS_TRANSACAO,   
B.TIPO_TRANSACAO AS TIPO_TRANSACAO,   
C.RAZAO_SOCIAL AS NOME_ESTABELECIMENTO,   
E.CODIGO_EMPRESA_GRUPO AS CODIGO_GRUPO,   
E.NOME_EMPRESA_GRUPO AS NOME_GRUPO,   
D.CODIGO_EMPRESA_SUBGRUPO AS CODIGO_SUBGRUPO,   
D.NOME_EMPRESA_SUBGRUPO AS NOME_SUBGRUPO,   
F.CODIGO_FAVORECIDO AS CODIGO_FAVORECIDO,   
G.NOME_FAVORECIDO AS NOME_FAVORECIDO,   
H.TIPO_ARQUIVO AS ORIGEM,   
I.DATA_INCLUSAO AS DATA_PROCESSAMENTO,   
B.CODIGO_PAGAMENTO AS NUMERO_PAGAMENTO,   
null AS STATUS_PAGAMENTO,  
NULL AS CODIGO_ANULACAO,    
NULL AS CODIGO_AUTORIZACAO_ORIGINAL,   
O.NUMERO_CARTAO AS NUMERO_CARTAO,   
O.NUMERO_CONTA_CLIENTE AS NUMERO_CONTA_CLIENTE, NULL AS MOTIVO_ANULACAO, NULL AS NSU_TEF_ORIGINAL,   
O.NUMERO_PARCELA AS NUMERO_PARCELA,   
O.NUMERO_PARCELA_TOTAL AS NUMERO_PARCELA_TOTAL,   
B.CODIGO_PRODUTO AS CODIGO_PRODUTO,   
M.NOME_PRODUTO AS NOME_PRODUTO,   
O.VALOR_PARCELA AS VALOR_PARCELA,   
O.VALOR_PARCELA_DESCONTO AS VALOR_PARCELA_DESCONTO,   
O.VALOR_PARCELA_LIQUIDO AS VALOR_PARCELA_LIQUIDO,   
meiocaptura.nome_lista_item AS MEIO_CAPTURA,   
O.CODIGO_AUTORIZACAO AS CODIGO_AUTORIZACAO, NULL AS MEIO_PAGAMENTO, NULL AS FORMA_MEIO_PAGAMENTO,   
NULL AS MEIO_PAGAMENTO_SEQ, NULL AS MEIO_PAGAMENTO_VALOR, NULL AS QUANTIDADE_MEIO_PAGAMENTO,   
O.NSU_TEF AS NSU_TEF,   
P.CODIGO_PLANO_TSYS AS CODIGO_PLANO,   
P.NOME_PLANO AS NOME_PLANO,   
O.MODALIDADE AS MODALIDADE,   
O.CUPOM_FISCAL AS CUPOM_FISCAL,   
O.TIPO_PRODUTO AS TIPO_PRODUTO,   
B.DATA_REPASSE_CALCULADA AS DATA_AGENDAMENTO, 
NULL AS DATA_ENVIO_CCI, 
J.DATA_ENVIO AS DATA_ENVIO_MATERA,   
B.DATA_REPASSE_CALCULADA AS DATA_GERACAO_AGENDA,   
null AS DATA_LIQUIDACAO, 
R.DATA_INCLUSAO AS DATA_RETORNO_CCI, NULL AS DATA_VENCIMENTO,   
I.STATUS_PROCESSO AS STATUS_IMPORTACAO, 
B.TIPO_TRANSACAO AS TIPO_REGISTRO, 
J.CODIGO_USUARIO_ENVIO AS USUARIO_ENVIO_CCI,   
NULL AS USUARIO_ENVIO_MATERA,
NULL AS USUARIO_RETORNO_CCI, 
NULL AS STATUS_VALIDACAO,   
O.meio_captura AS CODIGO_MEIO_CAPTURA,   
N.TIPO_ARQUIVO AS ORIGEM2,   
N.NOME_ARQUIVO AS NOME_ARQUIVO,   
N.STATUS_ARQUIVO AS STATUS_ARQUIVO,   
B.DATA_MOVIMENTO AS DATA_MOVIMENTO,   
O.DATA_PRE_AUTORIZACAO AS DATA_PRE_AUTORIZACAO,   
B.TIPO_FINANCEIRO_CONTABIL,   
O.NSU_HOST_REVERSAO,   
B.REFERENCIA     

FROM TB_TRANSACAO B   
INNER JOIN TB_TRANSACAO_VENDA O ON B.CODIGO_TRANSACAO = O.CODIGO_TRANSACAO   
LEFT JOIN TB_ESTABELECIMENTO C ON B.CODIGO_ESTABELECIMENTO = C.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_EMPRESA_SUBGRUPO D ON C.CODIGO_EMPRESA_SUBGRUPO = D.CODIGO_EMPRESA_SUBGRUPO   
LEFT JOIN TB_EMPRESA_GRUPO E ON D.CODIGO_EMPRESA_GRUPO = E.CODIGO_EMPRESA_GRUPO   
LEFT JOIN TB_ESTABELECIMENTO_FAVORECIDO F ON B.CODIGO_ESTABELECIMENTO = F.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_FAVORECIDO G ON F.CODIGO_FAVORECIDO = G.CODIGO_FAVORECIDO   
LEFT JOIN TB_ARQUIVO H ON B.CODIGO_ARQUIVO = H.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO I ON B.CODIGO_PROCESSO = I.CODIGO_PROCESSO   
LEFT JOIN TB_PAGAMENTO J ON B.CODIGO_PAGAMENTO = J.CODIGO_PAGAMENTO   
LEFT JOIN TB_PRODUTO M ON B.CODIGO_PRODUTO = M.CODIGO_PRODUTO   
LEFT JOIN TB_PLANO P ON O.CODIGO_PLANO = P.CODIGO_PLANO   
LEFT JOIN TB_LISTA_ITEM meiocaptura ON meiocaptura.CODIGO_LISTA = 5 AND o.meio_captura = meiocaptura.valor   
LEFT JOIN TB_ARQUIVO N ON B.CODIGO_ARQUIVO2 = N.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO R ON B.CODIGO_PROCESSO_RET_CESSAO = R.CODIGO_PROCESSO   
LEFT JOIN TB_CONTA_FAVORECIDO Z ON B.CODIGO_CONTA_FAVORECIDO = Z.CODIGO_CONTA_FAVORECIDO   
left join TB_LISTA_ITEM TLI               on B.REFERENCIA = TLI.VALOR and TLI.CODIGO_LISTA = '''
    || vCODIGO_LISTA || '''   
';
    vCorpoCP :=
    '   
SELECT N.CODIGO_TRANSACAO AS CODIGO_TRANSACAO, NULL AS CODIGO_AJUSTE, NULL AS TIPO_AJUSTE, NULL AS MOTIVO_AJUSTE,   
N.TIPO_LANCAMENTO AS TIPO_LANCAMENTO,   
B.DATA_REPASSE AS DATA_REPASSE,   
NULL AS DATA_TRANSACAO_ORIGINAL,   
NULL AS NSU_HOST_ORIGINAL,   
N.VALOR_PAGAMENTO AS VALOR_BRUTO,   
N.VALOR_DESCONTO AS VALOR_DESCONTO,   
N.VALOR_LIQUIDO AS VALOR_LIQUIDO,   
Z.BANCO AS BANCO,   
Z.AGENCIA AS AGENCIA,   
Z.CONTA AS CONTA,   
B.CODIGO_ARQUIVO AS CODIGO_ARQUIVO,   
B.CODIGO_ESTABELECIMENTO AS CODIGO_ESTABELECIMENTO,   
C.CNPJ AS CNPJ,   
B.CODIGO_PAGAMENTO AS CODIGO_PAGAMENTO,   
B.CODIGO_PROCESSO AS CODIGO_PROCESSO,   
B.DATA_TRANSACAO AS DATA_TRANSACAO,   
B.NSU_HOST AS NSU_HOST,   
B.STATUS_CONCILIACAO AS STATUS_CONCILIACAO,   
B.STATUS_RET_CESSAO AS STATUS_RETORNO,   
B.STATUS_TRANSACAO AS STATUS_TRANSACAO,   
B.TIPO_TRANSACAO AS TIPO_TRANSACAO,   
C.RAZAO_SOCIAL AS NOME_ESTABELECIMENTO,   
E.CODIGO_EMPRESA_GRUPO AS CODIGO_GRUPO,   
E.NOME_EMPRESA_GRUPO AS NOME_GRUPO,   
D.CODIGO_EMPRESA_SUBGRUPO AS CODIGO_SUBGRUPO,   
D.NOME_EMPRESA_SUBGRUPO AS NOME_SUBGRUPO,   
F.CODIGO_FAVORECIDO AS CODIGO_FAVORECIDO,   
G.NOME_FAVORECIDO AS NOME_FAVORECIDO,   
H.TIPO_ARQUIVO AS ORIGEM,   
I.DATA_INCLUSAO AS DATA_PROCESSAMENTO,   
B.CODIGO_PAGAMENTO AS NUMERO_PAGAMENTO,   
null AS STATUS_PAGAMENTO, 
NULL AS CODIGO_ANULACAO,    
NULL AS CODIGO_AUTORIZACAO_ORIGINAL,   
N.NUMERO_CARTAO AS NUMERO_CARTAO,   
N.NUMERO_CONTA_CLIENTE AS NUMERO_CONTA_CLIENTE, NULL AS MOTIVO_ANULACAO, NULL AS NSU_TEF_ORIGINAL, NULL AS NUMERO_PARCELA, NULL AS NUMERO_PARCELA_TOTAL,   
B.CODIGO_PRODUTO AS CODIGO_PRODUTO,   
M.NOME_PRODUTO AS NOME_PRODUTO, NULL AS VALOR_PARCELA, NULL AS VALOR_PARCELA_DESCONTO, NULL AS VALOR_PARCELA_LIQUIDO,   
meiocaptura.nome_lista_item AS MEIO_CAPTURA,   
N.CODIGO_AUTORIZACAO AS CODIGO_AUTORIZACAO, NULL AS FORMA_MEIO_PAGAMENTO,   
N.MEIO_PAGAMENTO AS MEIO_PAGAMENTO,   
N.MEIO_PAGAMENTO_SEQ AS MEIO_PAGAMENTO_SEQ,   
N.MEIO_PAGAMENTO_VALOR AS MEIO_PAGAMENTO_VALOR,   
N.QUANTIDADE_MEIO_PAGAMENTO AS QUANTIDADE_MEIO_PAGAMENTO,   
N.NSU_TEF AS NSU_TEF, NULL AS CODIGO_PLANO, NULL AS NOME_PLANO, NULL AS MODALIDADE, NULL AS CUPOM_FISCAL, NULL AS TIPO_PRODUTO,   
B.DATA_REPASSE_CALCULADA AS DATA_AGENDAMENTO, NULL AS DATA_ENVIO_CCI, NULL AS DATA_ENVIO_MATERA,   
B.DATA_REPASSE_CALCULADA AS DATA_GERACAO_AGENDA,   
null AS DATA_LIQUIDACAO, 
NULL AS DATA_RETORNO_CCI,    
NULL AS DATA_VENCIMENTO,   
I.STATUS_PROCESSO AS STATUS_IMPORTACAO, 
B.TIPO_TRANSACAO AS TIPO_REGISTRO, 
NULL AS USUARIO_ENVIO_CCI,   
NULL AS USUARIO_ENVIO_MATERA, 
NULL AS USUARIO_RETORNO_CCI, 
NULL AS STATUS_VALIDACAO,   
n.meio_captura AS CODIGO_MEIO_CAPTURA,   
P.TIPO_ARQUIVO AS ORIGEM2,   
P.NOME_ARQUIVO AS NOME_ARQUIVO,   
P.STATUS_ARQUIVO AS STATUS_ARQUIVO,   
B.DATA_MOVIMENTO AS DATA_MOVIMENTO,   
NULL AS DATA_PRE_AUTORIZACAO,   
B.TIPO_FINANCEIRO_CONTABIL,   
NULL AS NSU_HOST_REVERSAO,   
B.REFERENCIA     

FROM TB_TRANSACAO B   
INNER JOIN TB_TRANSACAO_PAGAMENTO N ON B.CODIGO_TRANSACAO = N.CODIGO_TRANSACAO   
LEFT JOIN TB_ESTABELECIMENTO C ON B.CODIGO_ESTABELECIMENTO = C.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_EMPRESA_SUBGRUPO D ON C.CODIGO_EMPRESA_SUBGRUPO = D.CODIGO_EMPRESA_SUBGRUPO   
LEFT JOIN TB_EMPRESA_GRUPO E ON D.CODIGO_EMPRESA_GRUPO = E.CODIGO_EMPRESA_GRUPO   
LEFT JOIN TB_ESTABELECIMENTO_FAVORECIDO F ON B.CODIGO_ESTABELECIMENTO = F.CODIGO_ESTABELECIMENTO   
LEFT JOIN TB_FAVORECIDO G ON F.CODIGO_FAVORECIDO = G.CODIGO_FAVORECIDO   
LEFT JOIN TB_ARQUIVO H ON B.CODIGO_ARQUIVO = H.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO I ON B.CODIGO_PROCESSO = I.CODIGO_PROCESSO   
LEFT JOIN TB_PRODUTO M ON B.CODIGO_PRODUTO = M.CODIGO_PRODUTO   
LEFT JOIN TB_LISTA_ITEM meiocaptura ON meiocaptura.CODIGO_LISTA = 5 AND n.meio_captura = meiocaptura.valor   
LEFT JOIN TB_ARQUIVO P ON B.CODIGO_ARQUIVO2 = P.CODIGO_ARQUIVO   
LEFT JOIN TB_PROCESSO R ON B.CODIGO_PROCESSO_RET_CESSAO = R.CODIGO_PROCESSO   
LEFT JOIN TB_CONTA_FAVORECIDO Z ON B.CODIGO_CONTA_FAVORECIDO = Z.CODIGO_CONTA_FAVORECIDO   
left join TB_LISTA_ITEM TLI               on B.REFERENCIA = TLI.VALOR and TLI.CODIGO_LISTA = '''
    || vCODIGO_LISTA || '''   
';
    vWhereAJ                  := vWhereAJ || ' B.TIPO_TRANSACAO = ''AJ''' || ' and ';
    IF (pFILTRO_EMPRESA_GRUPO IS NOT NULL) THEN
      vWhereAJ                := vWhereAJ || ' E.CODIGO_EMPRESA_GRUPO = ' || pFILTRO_EMPRESA_GRUPO || ' and ';
    END IF;
    IF (pFILTRO_EMPRESA_SUBGRUPO IS NOT NULL) THEN
      vWhereAJ                   := vWhereAJ || ' D.CODIGO_EMPRESA_SUBGRUPO = ' || pFILTRO_EMPRESA_SUBGRUPO || ' and ';
    END IF;
    IF (pFILTRO_ESTABELECIMENTO IS NOT NULL) THEN
      vWhereAJ                  := vWhereAJ || ' C.CODIGO_ESTABELECIMENTO = ' || pFILTRO_ESTABELECIMENTO || ' and ';
    END IF;
    IF (pFILTRO_FAVORECIDO IS NOT NULL) THEN
      vWhereAJ             := vWhereAJ || ' G.CODIGO_FAVORECIDO = ' || pFILTRO_FAVORECIDO || ' and ';
    END IF;
    IF (pFILTRO_TIPO_TRANSACAO IS NOT NULL) THEN
      vWhereAJ                 := vWhereAJ || ' B.TIPO_TRANSACAO = ''' || pFILTRO_TIPO_TRANSACAO || ''' and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_DE IS NOT NULL) THEN
      vWhereAJ                    := vWhereAJ || ' B.DATA_TRANSACAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_ATE IS NOT NULL) THEN
      vWhereAJ                     := vWhereAJ || ' B.DATA_TRANSACAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_DE IS NOT NULL) THEN
      vWhereAJ                        := vWhereAJ || ' I.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_ATE IS NOT NULL) THEN
      vWhereAJ                         := vWhereAJ || ' I.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_ATE IS NOT NULL) THEN
      vWhereAJ                       := vWhereAJ || ' B.DATA_REPASSE_CALCULADA <= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_DE IS NOT NULL) THEN
      vWhereAJ                     := vWhereAJ || ' B.DATA_REPASSE >= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_ATE IS NOT NULL) THEN
      vWhereAJ                      := vWhereAJ || ' B.DATA_REPASSE <= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_DE IS NOT NULL) THEN
      vWhereAJ                    := vWhereAJ || ' B.DATA_MOVIMENTO >= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_ATE IS NOT NULL) THEN
      vWhereAJ                     := vWhereAJ || ' B.DATA_MOVIMENTO <= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_STATUS_RETORNO IS NOT NULL) THEN
      vWhereAJ                 := vWhereAJ || ' B.STATUS_RET_CESSAO = ''' || pFILTRO_STATUS_RETORNO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_PAGAMENTO IS NOT NULL) THEN
      vWhereAJ                   := vWhereAJ || ' J.STATUS_PAGAMENTO = ''' || pFILTRO_STATUS_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_CONCILIACAO IS NOT NULL) THEN
      vWhereAJ                     := vWhereAJ || ' B.STATUS_CONCILIACAO = ''' || pFILTRO_STATUS_CONCILIACAO || ''' and ';
    END IF;
    IF (pFILTRO_ORIGEM IS NOT NULL) THEN
      vWhereAJ         := vWhereAJ || '(H.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''' or ' || ' N.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''') and ';
    END IF;
    IF (pFILTRO_NUMERO_PAGAMENTO IS NOT NULL) THEN
      vWhereAJ                   := vWhereAJ || ' B.CODIGO_PAGAMENTO = ''' || pFILTRO_NUMERO_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_CONTA_CLIENTE IS NOT NULL) THEN
      vWhereAJ                := vWhereAJ || ' A.NUMERO_CONTA_CLIENTE = ''' || pFILTRO_CONTA_CLIENTE || ''' and ';
    END IF;
    IF (pFILTRO_PRODUTO IS NOT NULL) THEN
      vWhereAJ          := vWhereAJ || ' B.CODIGO_PRODUTO = ' || pFILTRO_PRODUTO || ' and ';
    END IF;
    IF (pFILTRO_REFERENCIA IS NOT NULL) THEN
      vWhereAJ             := vWhereAJ || ' B.REFERENCIA in (' || pFILTRO_REFERENCIA || ') and ';
    END IF;
    vWhereAJ := vWhereAJ || ' nvl(H.STATUS_ARQUIVO,0) not in (1, 3) AND nvl(N.STATUS_ARQUIVO,0) not in (1, 3) and '; 
    IF (pFILTRO_STATUS_CANC_ON_LINE IS NOT NULL) THEN
      vWhereAJ                      := vWhereAJ || ' B.STATUS_TRANSACAO in (' || pFILTRO_STATUS_CANC_ON_LINE || ') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_DE IS NOT NULL) THEN
      vWhereAJ                     := vWhereAJ || ' R.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_ATE IS NOT NULL) THEN
      vWhereAJ                      := vWhereAJ || ' R.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF pFILTRO_TIPO_FINANC_CTBIL IS NOT NULL THEN
      vWhereAJ                   := vWhereAJ || 'nvl(B.TIPO_FINANCEIRO_CONTABIL,''' || pFILTRO_TIPO_FINANC_CTBIL || ''') = ''' || pFILTRO_TIPO_FINANC_CTBIL || ''' and ';
    END IF;
    IF pFILTRO_NSU_HOST IS NOT NULL THEN
      vWhereAJ          := vWhereAJ || 'B.NSU_HOST = ''' || pFILTRO_NSU_HOST || ''' and ';
      vWhereAJ          := vWhereAJ || '(B.NSU_HOST = ''' || pFILTRO_NSU_HOST || ''' or ';
      vWhereAJ          := vWhereAJ || 'A.NSU_HOST_ORIGINAL = ''' || pFILTRO_NSU_HOST || ''') and ';
    END IF;
    IF LENGTH(vWhereAJ) > 0 THEN
      vWhereAJ         := SUBSTR(vWhereAJ, 0, LENGTH(vWhereAJ) - 4);
      vWhereAJ         := ' WHERE ' || vWhereAJ;
    END IF;
    vWhereAP                  := vWhereAP || ' B.TIPO_TRANSACAO = ''AP''' || ' and ';
    IF (pFILTRO_EMPRESA_GRUPO IS NOT NULL) THEN
      vWhereAP                := vWhereAP || ' E.CODIGO_EMPRESA_GRUPO = ' || pFILTRO_EMPRESA_GRUPO || ' and ';
    END IF;
    IF (pFILTRO_EMPRESA_SUBGRUPO IS NOT NULL) THEN
      vWhereAP                   := vWhereAP || ' D.CODIGO_EMPRESA_SUBGRUPO = ' || pFILTRO_EMPRESA_SUBGRUPO || ' and ';
    END IF;
    IF (pFILTRO_ESTABELECIMENTO IS NOT NULL) THEN
      vWhereAP                  := vWhereAP || ' C.CODIGO_ESTABELECIMENTO = ' || pFILTRO_ESTABELECIMENTO || ' and ';
    END IF;
    IF (pFILTRO_FAVORECIDO IS NOT NULL) THEN
      vWhereAP             := vWhereAP || ' G.CODIGO_FAVORECIDO = ' || pFILTRO_FAVORECIDO || ' and ';
    END IF;
    IF (pFILTRO_TIPO_TRANSACAO IS NOT NULL) THEN
      vWhereAP                 := vWhereAP || ' B.TIPO_TRANSACAO = ''' || pFILTRO_TIPO_TRANSACAO || ''' and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_DE IS NOT NULL) THEN
      vWhereAP                    := vWhereAP || ' B.DATA_TRANSACAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_ATE IS NOT NULL) THEN
      vWhereAP                     := vWhereAP || ' B.DATA_TRANSACAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_DE IS NOT NULL) THEN
      vWhereAP                        := vWhereAP || ' I.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_ATE IS NOT NULL) THEN
      vWhereAP                         := vWhereAP || ' I.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_DE IS NOT NULL) THEN
      vWhereAP                      := vWhereAP || ' B.DATA_REPASSE_CALCULADA >= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_ATE IS NOT NULL) THEN
      vWhereAP                       := vWhereAP || ' B.DATA_REPASSE_CALCULADA <= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_DE IS NOT NULL) THEN
      vWhereAP                     := vWhereAP || ' B.DATA_REPASSE >= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_ATE IS NOT NULL) THEN
      vWhereAP                      := vWhereAP || ' B.DATA_REPASSE <= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_DE IS NOT NULL) THEN
      vWhereAP                    := vWhereAP || ' B.DATA_MOVIMENTO >= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_ATE IS NOT NULL) THEN
      vWhereAP                     := vWhereAP || ' B.DATA_MOVIMENTO <= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_STATUS_RETORNO IS NOT NULL) THEN
      vWhereAP                 := vWhereAP || ' B.STATUS_RET_CESSAO = ''' || pFILTRO_STATUS_RETORNO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_PAGAMENTO IS NOT NULL) THEN
      vWhereAP                   := vWhereAP || ' J.STATUS_PAGAMENTO = ''' || pFILTRO_STATUS_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_CONCILIACAO IS NOT NULL) THEN
      vWhereAP                     := vWhereAP || ' B.STATUS_CONCILIACAO = ''' || pFILTRO_STATUS_CONCILIACAO || ''' and ';
    END IF;
    IF (pFILTRO_ORIGEM IS NOT NULL) THEN
      vWhereAP         := vWhereAP || '(H.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''' or ' || ' N.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''') and ';
    END IF;
    IF (pFILTRO_NUMERO_PAGAMENTO IS NOT NULL) THEN
      vWhereAP                   := vWhereAP || ' B.CODIGO_PAGAMENTO = ''' || pFILTRO_NUMERO_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_CONTA_CLIENTE IS NOT NULL) THEN
      vWhereAP                := vWhereAP || ' K.NUMERO_CONTA_CLIENTE = ''' || pFILTRO_CONTA_CLIENTE || ''' and ';
    END IF;
    IF (pFILTRO_PRODUTO IS NOT NULL) THEN
      vWhereAP          := vWhereAP || ' B.CODIGO_PRODUTO = ' || pFILTRO_PRODUTO || ' and ';
    END IF;
    IF (pFILTRO_STATUS_CANC_ON_LINE IS NOT NULL) THEN
      vWhereAP                      := vWhereAP || ' B.STATUS_TRANSACAO in (' || pFILTRO_STATUS_CANC_ON_LINE || ') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_DE IS NOT NULL) THEN
      vWhereAP                     := vWhereAP || ' R.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_ATE IS NOT NULL) THEN
      vWhereAP                      := vWhereAP || ' R.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_REFERENCIA IS NOT NULL) THEN
      vWhereAP             := vWhereAP || ' B.REFERENCIA in (' || pFILTRO_REFERENCIA || ') and ';
    END IF;
    vWhereAP := vWhereAP || ' nvl(H.STATUS_ARQUIVO,0) not in (1, 3) AND nvl(N.STATUS_ARQUIVO,0) not in (1, 3) and '; 
    IF pFILTRO_TIPO_FINANC_CTBIL IS NOT NULL THEN
      vWhereAP                   := vWhereAP || 'nvl(B.TIPO_FINANCEIRO_CONTABIL,''' || pFILTRO_TIPO_FINANC_CTBIL || ''') = ''' || pFILTRO_TIPO_FINANC_CTBIL || ''' and ';
    END IF;
    IF pFILTRO_NSU_HOST IS NOT NULL THEN
      vWhereAP          := vWhereAP || '(B.NSU_HOST = ''' || pFILTRO_NSU_HOST || ''' or ';
      vWhereAP          := vWhereAP || 'K.NSU_HOST_ORIGINAL = ''' || pFILTRO_NSU_HOST || ''') and ';
    END IF;
    IF LENGTH(vWhereAP) > 0 THEN
      vWhereAP         := SUBSTR(vWhereAP, 0, LENGTH(vWhereAP) - 4);
      vWhereAP         := ' WHERE ' || vWhereAP;
    END IF;
    vWhereAV                  := vWhereAV || ' B.TIPO_TRANSACAO = ''AV''' || ' and ';
    IF (pFILTRO_EMPRESA_GRUPO IS NOT NULL) THEN
      vWhereAV                := vWhereAV || ' E.CODIGO_EMPRESA_GRUPO = ' || pFILTRO_EMPRESA_GRUPO || ' and ';
    END IF;
    IF (pFILTRO_EMPRESA_SUBGRUPO IS NOT NULL) THEN
      vWhereAV                   := vWhereAV || ' D.CODIGO_EMPRESA_SUBGRUPO = ' || pFILTRO_EMPRESA_SUBGRUPO || ' and ';
    END IF;
    IF (pFILTRO_ESTABELECIMENTO IS NOT NULL) THEN
      vWhereAV                  := vWhereAV || ' C.CODIGO_ESTABELECIMENTO = ' || pFILTRO_ESTABELECIMENTO || ' and ';
    END IF;
    IF (pFILTRO_FAVORECIDO IS NOT NULL) THEN
      vWhereAV             := vWhereAV || ' G.CODIGO_FAVORECIDO = ' || pFILTRO_FAVORECIDO || ' and ';
    END IF;
    IF (pFILTRO_TIPO_TRANSACAO IS NOT NULL) THEN
      vWhereAV                 := vWhereAV || ' B.TIPO_TRANSACAO = ''' || pFILTRO_TIPO_TRANSACAO || ''' and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_DE IS NOT NULL) THEN
      vWhereAV                    := vWhereAV || ' B.DATA_TRANSACAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_ATE IS NOT NULL) THEN
      vWhereAV                     := vWhereAV || ' B.DATA_TRANSACAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_DE IS NOT NULL) THEN
      vWhereAV                        := vWhereAV || ' I.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_ATE IS NOT NULL) THEN
      vWhereAV                         := vWhereAV || ' I.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_LIQUIDACAO_DE IS NOT NULL) THEN
      vWhereAV                     := vWhereAV || ' J.DATA_LIQUIDACAO_MANUAL >= to_date(''' || TO_CHAR(pFILTRO_DATA_LIQUIDACAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_LIQUIDACAO_ATE IS NOT NULL) THEN
      vWhereAV                      := vWhereAV || ' J.DATA_LIQUIDACAO_MANUAL <= to_date(''' || TO_CHAR(pFILTRO_DATA_LIQUIDACAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_DE IS NOT NULL) THEN
      vWhereAV                      := vWhereAV || ' B.DATA_REPASSE_CALCULADA >= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_ATE IS NOT NULL) THEN
      vWhereAV                       := vWhereAV || ' B.DATA_REPASSE_CALCULADA <= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_DE IS NOT NULL) THEN
      vWhereAV                     := vWhereAV || ' B.DATA_REPASSE >= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_ATE IS NOT NULL) THEN
      vWhereAV                      := vWhereAV || ' B.DATA_REPASSE <= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_DE IS NOT NULL) THEN
      vWhereAV                    := vWhereAV || ' B.DATA_MOVIMENTO >= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_ATE IS NOT NULL) THEN
      vWhereAV                     := vWhereAV || ' B.DATA_MOVIMENTO <= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_STATUS_RETORNO IS NOT NULL) THEN
      vWhereAV                 := vWhereAV || ' B.STATUS_RET_CESSAO = ''' || pFILTRO_STATUS_RETORNO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_PAGAMENTO IS NOT NULL) THEN
      vWhereAV                   := vWhereAV || ' J.STATUS_PAGAMENTO = ''' || pFILTRO_STATUS_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_CONCILIACAO IS NOT NULL) THEN
      vWhereAV                     := vWhereAV || ' B.STATUS_CONCILIACAO = ''' || pFILTRO_STATUS_CONCILIACAO || ''' and ';
    END IF;
    IF (pFILTRO_ORIGEM IS NOT NULL) THEN
      vWhereAV         := vWhereAV || '(H.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''' or ' || ' N.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''') and ';
    END IF;
    IF (pFILTRO_NUMERO_PAGAMENTO IS NOT NULL) THEN
      vWhereAV                   := vWhereAV || ' B.CODIGO_PAGAMENTO = ''' || pFILTRO_NUMERO_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_CONTA_CLIENTE IS NOT NULL) THEN
      vWhereAV                := vWhereAV || ' L.NUMERO_CONTA_CLIENTE = ''' || pFILTRO_CONTA_CLIENTE || ''' and ';
    END IF;
    IF (pFILTRO_PRODUTO IS NOT NULL) THEN
      vWhereAV          := vWhereAV || ' B.CODIGO_PRODUTO = ' || pFILTRO_PRODUTO || ' and ';
    END IF;
    /*SCF1701 - ID 29 - Correï¿½ï¿½o do STatus de Envio CCI
    if (pFILTRO_STATUS_CANC_ON_LINE is not null) then
    if (pFILTRO_STATUS_CANC_ON_LINE = 'S') then
    vWhereAV := vWhereAV || ' B.STATUS_TRANSACAO <> 6 and ';
    else
    vWhereAV := vWhereAV || ' B.STATUS_TRANSACAO = 6 and ';
    end if;
    end if;
    */
    IF (pFILTRO_STATUS_CANC_ON_LINE IS NOT NULL) THEN
      vWhereAV                      := vWhereAV || ' B.STATUS_TRANSACAO in (' || pFILTRO_STATUS_CANC_ON_LINE || ') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_DE IS NOT NULL) THEN
      vWhereAV                     := vWhereAV || ' O.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_ATE IS NOT NULL) THEN
      vWhereAV                      := vWhereAV || ' O.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_REFERENCIA IS NOT NULL) THEN
      vWhereAV             := vWhereAV || ' B.REFERENCIA in (' || pFILTRO_REFERENCIA || ') and ';
    END IF;
    vWhereAV := vWhereAV || ' nvl(H.STATUS_ARQUIVO,0) not in (1, 3) AND nvl(N.STATUS_ARQUIVO,0) not in (1, 3) and '; 
    IF pFILTRO_TIPO_FINANC_CTBIL IS NOT NULL THEN
      vWhereAV                   := vWhereAV || 'nvl(B.TIPO_FINANCEIRO_CONTABIL,''' || pFILTRO_TIPO_FINANC_CTBIL || ''') = ''' || pFILTRO_TIPO_FINANC_CTBIL || ''' and ';
    END IF;
    IF pFILTRO_NSU_HOST IS NOT NULL THEN
      vWhereAV          := vWhereAV || '(B.NSU_HOST = ''' || pFILTRO_NSU_HOST || ''' or ';
      vWhereAV          := vWhereAV || 'L.NSU_HOST_ORIGINAL = ''' || pFILTRO_NSU_HOST || ''') and ';
    END IF;
    IF LENGTH(vWhereAV) > 0 THEN
      vWhereAV         := SUBSTR(vWhereAV, 0, LENGTH(vWhereAV) - 4);
      vWhereAV         := ' WHERE ' || vWhereAV;
    END IF;
    vWhereCV                  := vWhereCV || ' B.TIPO_TRANSACAO = ''CV''' || ' and ';
    IF (pFILTRO_EMPRESA_GRUPO IS NOT NULL) THEN
      vWhereCV                := vWhereCV || ' E.CODIGO_EMPRESA_GRUPO = ' || pFILTRO_EMPRESA_GRUPO || ' and ';
    END IF;
    IF (pFILTRO_EMPRESA_SUBGRUPO IS NOT NULL) THEN
      vWhereCV                   := vWhereCV || ' D.CODIGO_EMPRESA_SUBGRUPO = ' || pFILTRO_EMPRESA_SUBGRUPO || ' and ';
    END IF;
    IF (pFILTRO_ESTABELECIMENTO IS NOT NULL) THEN
      vWhereCV                  := vWhereCV || ' C.CODIGO_ESTABELECIMENTO = ' || pFILTRO_ESTABELECIMENTO || ' and ';
    END IF;
    IF (pFILTRO_FAVORECIDO IS NOT NULL) THEN
      vWhereCV             := vWhereCV || ' G.CODIGO_FAVORECIDO = ' || pFILTRO_FAVORECIDO || ' and ';
    END IF;
    IF (pFILTRO_TIPO_TRANSACAO IS NOT NULL) THEN
      vWhereCV                 := vWhereCV || ' B.TIPO_TRANSACAO = ''' || pFILTRO_TIPO_TRANSACAO || ''' and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_DE IS NOT NULL) THEN
      vWhereCV                    := vWhereCV || ' B.DATA_TRANSACAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_ATE IS NOT NULL) THEN
      vWhereCV                     := vWhereCV || ' B.DATA_TRANSACAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_DE IS NOT NULL) THEN
      vWhereCV                        := vWhereCV || ' I.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_ATE IS NOT NULL) THEN
      vWhereCV                         := vWhereCV || ' I.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_LIQUIDACAO_DE IS NOT NULL) THEN
      vWhereCV                     := vWhereCV || ' J.DATA_LIQUIDACAO_MANUAL >= to_date(''' || TO_CHAR(pFILTRO_DATA_LIQUIDACAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_LIQUIDACAO_ATE IS NOT NULL) THEN
      vWhereCV                      := vWhereCV || ' J.DATA_LIQUIDACAO_MANUAL <= to_date(''' || TO_CHAR(pFILTRO_DATA_LIQUIDACAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_DE IS NOT NULL) THEN
      vWhereCV                      := vWhereCV || ' B.DATA_REPASSE_CALCULADA >= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_ATE IS NOT NULL) THEN
      vWhereCV                       := vWhereCV || ' B.DATA_REPASSE_CALCULADA <= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_DE IS NOT NULL) THEN
      vWhereCV                     := vWhereCV || ' B.DATA_REPASSE >= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_ATE IS NOT NULL) THEN
      vWhereCV                      := vWhereCV || ' B.DATA_REPASSE <= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_DE IS NOT NULL) THEN
      vWhereCV                    := vWhereCV || ' B.DATA_MOVIMENTO >= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_ATE IS NOT NULL) THEN
      vWhereCV                     := vWhereCV || ' B.DATA_MOVIMENTO <= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_STATUS_RETORNO IS NOT NULL) THEN
      vWhereCV                 := vWhereCV || ' B.STATUS_RET_CESSAO = ''' || pFILTRO_STATUS_RETORNO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_PAGAMENTO IS NOT NULL) THEN
      vWhereCV                   := vWhereCV || ' J.STATUS_PAGAMENTO = ''' || pFILTRO_STATUS_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_CONCILIACAO IS NOT NULL) THEN
      vWhereCV                     := vWhereCV || ' B.STATUS_CONCILIACAO = ''' || pFILTRO_STATUS_CONCILIACAO || ''' and ';
    END IF;
    IF (pFILTRO_ORIGEM IS NOT NULL) THEN
      vWhereCV         := vWhereCV || '(H.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''' or ' || ' N.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''') and ';
    END IF;
    IF (pFILTRO_NUMERO_PAGAMENTO IS NOT NULL) THEN
      vWhereCV                   := vWhereCV || ' B.CODIGO_PAGAMENTO = ''' || pFILTRO_NUMERO_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_CONTA_CLIENTE IS NOT NULL) THEN
      vWhereCV                := vWhereCV || ' O.NUMERO_CONTA_CLIENTE = ''' || pFILTRO_CONTA_CLIENTE || ''' and ';
    END IF;
    IF (pFILTRO_PRODUTO IS NOT NULL) THEN
      vWhereCV          := vWhereCV || ' B.CODIGO_PRODUTO = ' || pFILTRO_PRODUTO || ' and ';
    END IF;
    IF (pFILTRO_MEIO_CAPTURA IS NOT NULL) THEN
      vWhereCV               := vWhereCV || ' O.MEIO_CAPTURA = ' || pFILTRO_MEIO_CAPTURA || ' and ';
    END IF;
    IF (pFILTRO_PLANO IS NOT NULL) THEN
      vWhereCV        := vWhereCV || ' P.CODIGO_PLANO = ' || pFILTRO_PLANO || ' and ';
    END IF;
    /* SCF1701 - ID 29 - Correï¿½ï¿½o do STatus de Envio CCI
    if (pFILTRO_STATUS_CANC_ON_LINE is not null) then
    if (pFILTRO_STATUS_CANC_ON_LINE = 'S') then
    vWhereCV := vWhereCV || ' B.STATUS_TRANSACAO <> 6 and ';
    else
    vWhereCV := vWhereCV || ' B.STATUS_TRANSACAO = 6 and ';
    end if;
    end if;
    */
    IF (pFILTRO_STATUS_CANC_ON_LINE IS NOT NULL) THEN
      vWhereCV                      := vWhereCV || ' B.STATUS_TRANSACAO in (' || pFILTRO_STATUS_CANC_ON_LINE || ') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_DE IS NOT NULL) THEN
      vWhereCV                     := vWhereCV || ' R.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_ATE IS NOT NULL) THEN
      vWhereCV                      := vWhereCV || ' R.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_REFERENCIA IS NOT NULL) THEN
      vWhereCV             := vWhereCV || ' B.REFERENCIA in (' || pFILTRO_REFERENCIA || ') and ';
    END IF;
    vWhereCV := vWhereCV || ' nvl(H.STATUS_ARQUIVO,0) not in (1, 3) AND nvl(N.STATUS_ARQUIVO,0) not in (1, 3) and '; 
    IF pFILTRO_TIPO_FINANC_CTBIL IS NOT NULL THEN
      vWhereCV                   := vWhereCV || 'nvl(B.TIPO_FINANCEIRO_CONTABIL,''' || pFILTRO_TIPO_FINANC_CTBIL || ''') = ''' || pFILTRO_TIPO_FINANC_CTBIL || ''' and ';
    END IF;
    IF pFILTRO_NSU_HOST IS NOT NULL THEN
      vWhereCV          := vWhereCV || '(B.NSU_HOST = ''' || pFILTRO_NSU_HOST || ''' or ';
      vWhereCV          := vWhereCV || 'O.NSU_HOST_ORIGINAL = ''' || pFILTRO_NSU_HOST || ''' or ';
      vWhereCV          := vWhereCV || 'O.NSU_HOST_REVERSAO = ''' || pFILTRO_NSU_HOST || ''') and ';
    END IF;
    IF LENGTH(vWhereCV) > 0 THEN
      vWhereCV         := SUBSTR(vWhereCV, 0, LENGTH(vWhereCV) - 4);
      vWhereCV         := ' WHERE ' || vWhereCV;
    END IF;
    vWhereCP                  := vWhereCP || ' B.TIPO_TRANSACAO = ''CP''' || ' and ';
    IF (pFILTRO_EMPRESA_GRUPO IS NOT NULL) THEN
      vWhereCP                := vWhereCP || ' E.CODIGO_EMPRESA_GRUPO = ' || pFILTRO_EMPRESA_GRUPO || ' and ';
    END IF;
    IF (pFILTRO_EMPRESA_SUBGRUPO IS NOT NULL) THEN
      vWhereCP                   := vWhereCP || ' D.CODIGO_EMPRESA_SUBGRUPO = ' || pFILTRO_EMPRESA_SUBGRUPO || ' and ';
    END IF;
    IF (pFILTRO_ESTABELECIMENTO IS NOT NULL) THEN
      vWhereCP                  := vWhereCP || ' C.CODIGO_ESTABELECIMENTO = ' || pFILTRO_ESTABELECIMENTO || ' and ';
    END IF;
    IF (pFILTRO_FAVORECIDO IS NOT NULL) THEN
      vWhereCP             := vWhereCP || ' G.CODIGO_FAVORECIDO = ' || pFILTRO_FAVORECIDO || ' and ';
    END IF;
    IF (pFILTRO_TIPO_TRANSACAO IS NOT NULL) THEN
      vWhereCP                 := vWhereCP || ' B.TIPO_TRANSACAO = ''' || pFILTRO_TIPO_TRANSACAO || ''' and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_DE IS NOT NULL) THEN
      vWhereCP                    := vWhereCP || ' B.DATA_TRANSACAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_TRANSACAO_ATE IS NOT NULL) THEN
      vWhereCP                     := vWhereCP || ' B.DATA_TRANSACAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_TRANSACAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_DE IS NOT NULL) THEN
      vWhereCP                        := vWhereCP || ' I.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_PROCESSAMENTO_ATE IS NOT NULL) THEN
      vWhereCP                         := vWhereCP || ' I.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_PROCESSAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_DE IS NOT NULL) THEN
      vWhereCP                      := vWhereCP || ' B.DATA_REPASSE_CALCULADA >= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_AGENDAMENTO_ATE IS NOT NULL) THEN
      vWhereCP                       := vWhereCP || ' B.DATA_REPASSE_CALCULADA <= to_date(''' || TO_CHAR(pFILTRO_DATA_AGENDAMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_DE IS NOT NULL) THEN
      vWhereCP                     := vWhereCP || ' B.DATA_REPASSE >= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_VENCIMENTO_ATE IS NOT NULL) THEN
      vWhereCP                      := vWhereCP || ' B.DATA_REPASSE <= to_date(''' || TO_CHAR(pFILTRO_DATA_VENCIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_DE IS NOT NULL) THEN
      vWhereCP                    := vWhereCP || ' B.DATA_MOVIMENTO >= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_MOVIMENTO_ATE IS NOT NULL) THEN
      vWhereCP                     := vWhereCP || ' B.DATA_MOVIMENTO <= to_date(''' || TO_CHAR(pFILTRO_DATA_MOVIMENTO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_STATUS_RETORNO IS NOT NULL) THEN
      vWhereCP                 := vWhereCP || ' B.STATUS_RET_CESSAO = ''' || pFILTRO_STATUS_RETORNO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_PAGAMENTO IS NOT NULL) THEN
      vWhereCP                   := vWhereCP || ' J.STATUS_PAGAMENTO = ''' || pFILTRO_STATUS_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_STATUS_CONCILIACAO IS NOT NULL) THEN
      vWhereCP                     := vWhereCP || ' B.STATUS_CONCILIACAO = ''' || pFILTRO_STATUS_CONCILIACAO || ''' and ';
    END IF;
    IF (pFILTRO_ORIGEM IS NOT NULL) THEN
      vWhereCP         := vWhereCP || '(H.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''' or ' || ' P.TIPO_ARQUIVO = ''' || pFILTRO_ORIGEM || ''') and ';
    END IF;
    IF (pFILTRO_NUMERO_PAGAMENTO IS NOT NULL) THEN
      vWhereCP                   := vWhereCP || ' B.CODIGO_PAGAMENTO = ''' || pFILTRO_NUMERO_PAGAMENTO || ''' and ';
    END IF;
    IF (pFILTRO_CONTA_CLIENTE IS NOT NULL) THEN
      vWhereCP                := vWhereCP || ' N.NUMERO_CONTA_CLIENTE = ''' || pFILTRO_CONTA_CLIENTE || ''' and ';
    END IF;
    IF (pFILTRO_PRODUTO IS NOT NULL) THEN
      vWhereCP          := vWhereCP || ' B.CODIGO_PRODUTO = ' || pFILTRO_PRODUTO || ' and ';
    END IF;
    IF (pFILTRO_MEIO_CAPTURA IS NOT NULL) THEN
      vWhereCP               := vWhereCP || ' N.MEIO_CAPTURA = ' || pFILTRO_MEIO_CAPTURA || ' and ';
    END IF;
    /*SCF1701 - ID 29 - Correï¿½ï¿½o do STatus de Envio CCI
    if (pFILTRO_STATUS_CANC_ON_LINE is not null) then
    if (pFILTRO_STATUS_CANC_ON_LINE = 'S') then
    vWhereCP := vWhereCP || ' B.STATUS_TRANSACAO <> 6 and ';
    else
    vWhereCP := vWhereCP || ' B.STATUS_TRANSACAO = 6 and ';
    end if;
    end if;
    */
    IF (pFILTRO_STATUS_CANC_ON_LINE IS NOT NULL) THEN
      vWhereCP                      := vWhereCP || ' B.STATUS_TRANSACAO in (' || pFILTRO_STATUS_CANC_ON_LINE || ') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_DE IS NOT NULL) THEN
      vWhereCP                     := vWhereCP || ' R.DATA_INCLUSAO >= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_DE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_DATA_RET_CESSAO_ATE IS NOT NULL) THEN
      vWhereCP                      := vWhereCP || ' R.DATA_INCLUSAO <= to_date(''' || TO_CHAR(pFILTRO_DATA_RET_CESSAO_ATE, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') and ';
    END IF;
    IF (pFILTRO_REFERENCIA IS NOT NULL) THEN
      vWhereCP             := vWhereCP || ' B.REFERENCIA in (' || pFILTRO_REFERENCIA || ') and ';
    END IF;
    vWhereCP := vWhereCP || ' nvl(H.STATUS_ARQUIVO,0) not in (1, 3) AND nvl(P.STATUS_ARQUIVO,0) not in (1, 3) and '; 
    IF pFILTRO_TIPO_FINANC_CTBIL IS NOT NULL THEN
      vWhereCP                   := vWhereCP || 'nvl(B.TIPO_FINANCEIRO_CONTABIL,''' || pFILTRO_TIPO_FINANC_CTBIL || ''') = ''' || pFILTRO_TIPO_FINANC_CTBIL || ''' and ';
    END IF;
    IF pFILTRO_NSU_HOST IS NOT NULL THEN
      vWhereCP          := vWhereCP || '(B.NSU_HOST = ''' || pFILTRO_NSU_HOST || ''' or ';
      vWhereCP          := vWhereCP || 'N.NSU_TEF = ''' || pFILTRO_NSU_HOST || ''') and ';
    END IF;
    IF LENGTH(vWhereCP) > 0 THEN
      vWhereCP         := SUBSTR(vWhereCP, 0, LENGTH(vWhereCP) - 4);
      vWhereCP         := ' WHERE ' || vWhereCP;
    END IF;
    vCorpoAV := vCorpoAV || vWhereAV;
    vCorpoAP := vCorpoAP || vWhereAP;
    vCorpoAJ := vCorpoAJ || vWhereAJ;
    vCorpoCV := vCorpoCV || vWhereCV;
    vCorpoCP := vCorpoCP || vWhereCP;
    IF pFILTRO_TIPO_REGISTRO IS NULL AND pFILTRO_PLANO IS NULL THEN
      vCorpoView             := vCorpoView || ' (' || vCorpoAV || ' union ' || vCorpoAP || ' union ' || vCorpoAJ || ' union ' || vCorpoCP || ' union ' || vCorpoCV || ') tabela';
    END IF;
    IF pFILTRO_TIPO_REGISTRO = 'AV' AND pFILTRO_PLANO IS NULL THEN
      vCorpoView            := vCorpoView || ' (' || vCorpoAV || ') tabela';
    END IF;
    IF pFILTRO_TIPO_REGISTRO = 'AP' AND pFILTRO_PLANO IS NULL THEN
      vCorpoView            := vCorpoView || ' (' || vCorpoAP || ') tabela';
    END IF;
    IF pFILTRO_TIPO_REGISTRO = 'AJ' AND pFILTRO_PLANO IS NULL THEN
      vCorpoView            := vCorpoView || ' (' || vCorpoAJ || ') tabela';
    END IF;
    IF pFILTRO_TIPO_REGISTRO = 'CP' AND pFILTRO_PLANO IS NULL THEN
      vCorpoView            := vCorpoView || ' (' || vCorpoCP || ') tabela';
    END IF;
    IF pFILTRO_TIPO_REGISTRO = 'CV' OR pFILTRO_PLANO IS NOT NULL THEN
      vCorpoView            := vCorpoView || ' (' || vCorpoCV || ') tabela';
    END IF;
    OPEN retcur FOR vCorpoView;
    PR_LOG_DB(PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_RELATORIO_TRANSACAO_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => vCorpoView );
  END PR_RELATORIO_TRANSACAO_L;
  PROCEDURE PR_EXTRATO_PREPARAR_AGENDA(pCODIGO_PROCESSO NUMBER,pREGRA_ATACADAO  VARCHAR := 'N' )
  AS
  BEGIN
    PR_LOG_DB ( PDESCRICAO_LOG => 'Processo: ' || pCODIGO_PROCESSO || ' FUNCIONALIDADE SUSPENSA DESDE AGOSTO/2016' , PTIPO_ORIGEM => 'PR_EXTRATO_AGENDAR' , PCODIGO_ORIGEM => NULL , PDESCRICAOGRANDE_LOG => NULL );
  END PR_EXTRATO_PREPARAR_AGENDA;
  PROCEDURE PR_EXTRATO_AGENDAR(pCODIGO_PROCESSO NUMBER, pREGRA_ATACADAO  VARCHAR := 'N' )
  AS
  BEGIN
    PR_LOG_DB ( PDESCRICAO_LOG => 'Processo: ' || pCODIGO_PROCESSO || ' FUNCIONALIDADE SUSPENSA DESDE AGOSTO/2016' , PTIPO_ORIGEM => 'PR_EXTRATO_AGENDAR' , PCODIGO_ORIGEM => NULL , PDESCRICAOGRANDE_LOG => NULL );
  END PR_EXTRATO_AGENDAR;
  PROCEDURE PR_PAGAMENTO_A2(
      pDATA_PAGAMENTO_MAIOR DATE    := NULL,
      pCODIGO_PROCESSO      NUMBER  := NULL,
      pREGRA_ATACADAO       VARCHAR := NULL )
  AS
  BEGIN
    PR_LOG_DB ( PDESCRICAO_LOG => 'Processo: ' || pCODIGO_PROCESSO || ' FUNCIONALIDADE SUSPENSA DESDE AGOSTO/2016' , PTIPO_ORIGEM => 'PR_PAGAMENTO_A2' , PCODIGO_ORIGEM => NULL , PDESCRICAOGRANDE_LOG => NULL );
  END PR_PAGAMENTO_A2;
  PROCEDURE PR_RELATORIO_VENDAS_L1(
      pCODIGO_EMPRESA_GRUPO         NUMBER   := NULL,
      pCODIGO_EMPRESA_SUBGRUPO      NUMBER   := NULL,
      pCODIGO_BANCO_FAVORECIDO      NUMBER   := NULL,
      pCODIGO_ESTABELECIMENTO       NUMBER   := NULL,
      pTIPO_REGISTRO                VARCHAR2 := NULL,
      pCONSOLIDA_GRUPO              CHAR     := 'N',
      pCONSOLIDA_DATA_VENDA         CHAR     := 'N',
      pCONSOLIDA_DATA_PROCESSAMENTO CHAR     := 'N',
      pCONSOLIDA_BANCO              CHAR     := 'N',
      pCONSOLIDA_ESTABELECIMENTO    CHAR     := 'N',
      pDATA_INICIO_VENDA            DATE     := NULL,
      pDATA_FIM_VENDA               DATE     := NULL,
      pDATA_INICIO_MOVIMENTO        DATE     := NULL,
      pDATA_FIM_MOVIMENTO           DATE     := NULL,
      pDATA_INICIO_CCI              DATE     := NULL,
      pDATA_FIM_CCI                 DATE     := NULL,
      pTIPO_FINANCEIRO_CONTABIL     CHAR     := NULL,
      pREFERENCIA                   VARCHAR2 := NULL, 
      pCONSOLIDA_REFERENCIA         CHAR     := 'N',  
      retcur OUT TP_REFCURSOR )
  AS
    vQuery1               VARCHAR2(4000);
    vQueryTransacaoParte1 VARCHAR2(2000);
    vQueryTransacaoParte2 VARCHAR2(2000);
    vQueryTransacaoParte3 VARCHAR2(2000);
    vQueryTransacaoParte4 VARCHAR2(2000);
    vQueryTransacaoParte5 VARCHAR2(2000);
    vQueryTransacaoParte6 VARCHAR2(2000); 
    vQueryFinal   VARCHAR2(32000);
    vCODIGO_LISTA NUMBER;
  BEGIN
    SELECT CODIGO_LISTA
    INTO vCODIGO_LISTA
    FROM TB_LISTA
    WHERE NOME_LISTA = 'Referencia';
    vQuery1             := '  
SELECT    ';
    IF (pCONSOLIDA_GRUPO = 'S') THEN
      vQuery1           := vQuery1 || '  
NOME_EMPRESA_SUBGRUPO,  
NOME_EMPRESA_GRUPO,  
NOME_PRODUTO,  
';
    END IF;
    IF (pCONSOLIDA_DATA_VENDA = 'S') THEN
      vQuery1                := vQuery1 || '  
Trunc(DATA_TRANSACAO)                     as DATA_TRANSACAO,  
';
    END IF;
    IF (pCONSOLIDA_DATA_PROCESSAMENTO = 'S') THEN
      vQuery1                        := vQuery1 || '  
Trunc(DATA_MOVIMENTO)                     as DATA_MOVIMENTO,  
';
    END IF;
    IF (pCONSOLIDA_BANCO = 'S') THEN
      vQuery1           := vQuery1 || '  
BANCO,  
AGENCIA,  
CONTA,  
';
    END IF;
    IF (pCONSOLIDA_ESTABELECIMENTO = 'S') THEN
      vQuery1                     := vQuery1 || '  
CODIGO_ESTABELECIMENTO,  
RAZAO_SOCIAL,  
';
    END IF;
    IF (pCONSOLIDA_REFERENCIA = 'S') THEN
      vQuery1                := vQuery1 || '  
REFERENCIA,  
NOME_LISTA_ITEM,  
';
    END IF;
    vQuery1                 := vQuery1 || '  
sum(QUANTIDADE)                           AS QUANTIDADE,  
sum(TOTAL_CEDIVEL_NAO_NEGOCIADO)          AS TOTAL_CEDIVEL_NAO_NEGOCIADO,  
sum(TOTAL_CEDIDO)                         AS TOTAL_CEDIDO,  
sum(TOTAL_NAO_CEDIDO)                     AS TOTAL_NAO_CEDIDO,  
sum(VALOR_LIQUIDO)                        AS VALOR_LIQUIDO,  
sum(VALOR_COMISSAO)                       AS TOTAL_COMISSAO,  
sum(VALOR_LIQUIDO - (TOTAL_CEDIDO + TOTAL_NAO_CEDIDO + TOTAL_CEDIVEL_NAO_NEGOCIADO)) AS TOTAL_PENDENTE,    
sum(VALOR_BRUTO)                          AS TOTAL_BRUTO    

FROM(  
';
    vQueryTransacaoParte1   := '  
SELECT  ';
    IF (pCONSOLIDA_GRUPO     = 'S') THEN
      vQueryTransacaoParte1 := vQueryTransacaoParte1 || '  
NOME_EMPRESA_SUBGRUPO,  
NOME_EMPRESA_GRUPO,  
NOME_PRODUTO,  
';
    END IF;
    IF (pCONSOLIDA_DATA_VENDA = 'S') THEN
      vQueryTransacaoParte1  := vQueryTransacaoParte1 || '  
Trunc(DATA_TRANSACAO)                     as DATA_TRANSACAO, ';
    END IF;
    IF (pCONSOLIDA_DATA_PROCESSAMENTO = 'S') THEN
      vQueryTransacaoParte1          := vQueryTransacaoParte1 || '  
Trunc(DATA_MOVIMENTO)                     as DATA_MOVIMENTO,  
';
    END IF;
    IF (pCONSOLIDA_BANCO     = 'S') THEN
      vQueryTransacaoParte1 := vQueryTransacaoParte1 || '  
BANCO,  
AGENCIA,  
CONTA,  
';
    END IF;
    IF (pCONSOLIDA_ESTABELECIMENTO = 'S') THEN
      vQueryTransacaoParte1       := vQueryTransacaoParte1 || '  
CODIGO_ESTABELECIMENTO,  
RAZAO_SOCIAL,  
';
    END IF;
    IF (pCONSOLIDA_REFERENCIA = 'S') THEN
      vQueryTransacaoParte1  := vQueryTransacaoParte1 || '  
REFERENCIA,  
NOME_LISTA_ITEM,  
';
    END IF;
    vQueryTransacaoParte1 := vQueryTransacaoParte1 ||
    '  
count(*)                                  AS QUANTIDADE,  
SUM(  
CASE  WHEN  (CEDIVEL = ''S'' and (STATUS_RET_CESSAO IS NULL OR STATUS_RET_CESSAO <> ''1''))  
THEN (  CASE WHEN VALOR_PARCELA_LIQUIDO = 0          
THEN VALOR_LIQUIDO          
ELSE VALOR_PARCELA_LIQUIDO          
END)  
END)                                      AS TOTAL_CEDIVEL_NAO_NEGOCIADO,    

SUM(  
CASE  WHEN  STATUS_RET_CESSAO =   ''1''  
THEN (  CASE WHEN VALOR_PARCELA_LIQUIDO = 0          
THEN VALOR_LIQUIDO          
ELSE VALOR_PARCELA_LIQUIDO          
END)  
END)                                      AS TOTAL_CEDIDO,    

SUM(  
CASE  WHEN  CEDIVEL = ''N''  
THEN (  CASE WHEN VALOR_PARCELA_LIQUIDO = 0          
THEN VALOR_LIQUIDO          
ELSE VALOR_PARCELA_LIQUIDO          
END)  
END)                                      AS TOTAL_NAO_CEDIDO,    

SUM(  
CASE  WHEN  VALOR_PARCELA_LIQUIDO = 0  
THEN VALOR_LIQUIDO  
ELSE VALOR_PARCELA_LIQUIDO END)           AS VALOR_LIQUIDO,    

SUM(  
CASE  WHEN  VALOR_PARCELA_DESCONTO = 0  
THEN VALOR_DESCONTO  
ELSE VALOR_PARCELA_DESCONTO END)          AS VALOR_COMISSAO,  
'
    ;
    vQueryTransacaoParte3 := vQueryTransacaoParte1 || '  
SUM(  
CASE WHEN VALOR_PARCELA = 0  
THEN VALOR_VENDA  
ELSE VALOR_PARCELA END)                   AS VALOR_BRUTO  
';
    vQueryTransacaoParte4 := vQueryTransacaoParte1 || '  
SUM(  
CASE WHEN VALOR_PARCELA = 0  
THEN VALOR_ANULACAO  
ELSE VALOR_PARCELA END)                   AS VALOR_BRUTO  
'; 
    vQueryTransacaoParte6 := vQueryTransacaoParte1 || '  
SUM(  
CASE WHEN VALOR_PARCELA = 0  
THEN VALOR_AJUSTE  
ELSE VALOR_PARCELA END)                   AS VALOR_BRUTO  
';
    vQueryTransacaoParte2 := '  
STATUS_TRANSACAO <> 6 and  
';
    IF pCODIGO_ESTABELECIMENTO IS NOT NULL THEN
      vQueryTransacaoParte2    := vQueryTransacaoParte2 || 'CODIGO_ESTABELECIMENTO = ''' || pCODIGO_ESTABELECIMENTO || ''' and ';
    END IF;
    IF pCODIGO_EMPRESA_SUBGRUPO IS NOT NULL THEN
      vQueryTransacaoParte2     := vQueryTransacaoParte2 || 'CODIGO_EMPRESA_SUBGRUPO = ''' || pCODIGO_EMPRESA_SUBGRUPO || ''' and ';
    END IF;
    IF pCODIGO_EMPRESA_GRUPO IS NOT NULL THEN
      vQueryTransacaoParte2  := vQueryTransacaoParte2 || 'CODIGO_EMPRESA_GRUPO = ''' || pCODIGO_EMPRESA_GRUPO || ''' and ';
    END IF;
    IF pCODIGO_BANCO_FAVORECIDO IS NOT NULL THEN
      vQueryTransacaoParte2     := vQueryTransacaoParte2 || 'CODIGO_FAVORECIDO = ''' || pCODIGO_BANCO_FAVORECIDO || ''' and ';
    END IF;
    IF pDATA_INICIO_VENDA IS NOT NULL THEN
      vQueryTransacaoParte2 := vQueryTransacaoParte2 || 'DATA_TRANSACAO >= ''' || pDATA_INICIO_VENDA || ''' and '; 
    END IF;
    IF pDATA_FIM_VENDA IS NOT NULL THEN
      vQueryTransacaoParte2 := vQueryTransacaoParte2 || 'DATA_TRANSACAO <= ''' || pDATA_FIM_VENDA || ''' and '; 
    END IF;
    IF pDATA_INICIO_MOVIMENTO IS NOT NULL THEN
      vQueryTransacaoParte2 := vQueryTransacaoParte2 || 'DATA_MOVIMENTO >= ''' || pDATA_INICIO_MOVIMENTO || ''' and '; 
    END IF;
    IF pDATA_FIM_MOVIMENTO IS NOT NULL THEN
      vQueryTransacaoParte2 := vQueryTransacaoParte2 || 'DATA_MOVIMENTO <= ''' || pDATA_FIM_MOVIMENTO || ''' and '; 
    END IF;
    IF pDATA_INICIO_CCI IS NOT NULL THEN
      vQueryTransacaoParte2 := vQueryTransacaoParte2 || 'DATA_RETORNO_CCI >= ''' || pDATA_INICIO_CCI || ''' and '; 
    END IF;
    IF pDATA_FIM_CCI IS NOT NULL THEN
      vQueryTransacaoParte2 := vQueryTransacaoParte2 || 'DATA_RETORNO_CCI <= ''' || pDATA_FIM_CCI || ''' and '; 
    END IF;
    IF pTIPO_FINANCEIRO_CONTABIL IS NOT NULL THEN
      vQueryTransacaoParte2      := vQueryTransacaoParte2 || 'nvl(TIPO_FINANCEIRO_CONTABIL,''' || pTIPO_FINANCEIRO_CONTABIL || ''') = ''' || pTIPO_FINANCEIRO_CONTABIL || ''' and ';
    END IF;
    IF pREFERENCIA          IS NOT NULL THEN
      vQueryTransacaoParte2 := vQueryTransacaoParte2 || 'REFERENCIA in (' || pREFERENCIA || ') and ';
    END IF;
    IF LENGTH(vQueryTransacaoParte2) > 0 THEN
      vQueryTransacaoParte2         := SUBSTR(vQueryTransacaoParte2, 0, LENGTH(vQueryTransacaoParte2) - 4);
      vQueryTransacaoParte2         := '  
WHERE  
' || vQueryTransacaoParte2;
    END IF;
    vQueryTransacaoParte5   := vQueryTransacaoParte5 || '    

Group By  
';
    IF (pCONSOLIDA_GRUPO     = 'S') THEN
      vQueryTransacaoParte5 := vQueryTransacaoParte5 || ' NOME_EMPRESA_SUBGRUPO, NOME_EMPRESA_GRUPO, NOME_PRODUTO, ';
    END IF;
    IF (pCONSOLIDA_REFERENCIA = 'S') THEN
      vQueryTransacaoParte5  := vQueryTransacaoParte5 || ' REFERENCIA, NOME_LISTA_ITEM, ';
    END IF;
    IF (pCONSOLIDA_DATA_VENDA = 'S') THEN
      vQueryTransacaoParte5  := vQueryTransacaoParte5 || ' trunc(DATA_TRANSACAO), ';
    END IF;
    IF (pCONSOLIDA_DATA_PROCESSAMENTO = 'S') THEN
      vQueryTransacaoParte5          := vQueryTransacaoParte5 || ' trunc(DATA_MOVIMENTO), ';
    END IF;
    IF (pCONSOLIDA_BANCO     = 'S') THEN
      vQueryTransacaoParte5 := vQueryTransacaoParte5 || ' BANCO, AGENCIA, CONTA, ';
    END IF;
    IF (pCONSOLIDA_ESTABELECIMENTO = 'S') THEN
      vQueryTransacaoParte5       := vQueryTransacaoParte5 || ' CODIGO_ESTABELECIMENTO, RAZAO_SOCIAL, ';
    END IF;
    vQueryTransacaoParte5 := SUBSTR(vQueryTransacaoParte5, 0, LENGTH(vQueryTransacaoParte5) - 2);
    vQueryFinal := vQuery1;
    IF (pTIPO_REGISTRO = 'CV-AV' OR pTIPO_REGISTRO = 'CV') THEN
      vQueryFinal     := vQueryFinal || vQueryTransacaoParte3 || '  
FROM VW_REL_VENDA_CV  
' || vQueryTransacaoParte2 || vQueryTransacaoParte5;
    END IF;
    IF (pTIPO_REGISTRO = 'CV-AV') THEN
      vQueryFinal     := vQueryFinal || '  
UNION  
';
    END IF;
    IF (pTIPO_REGISTRO = 'CV-AV' OR pTIPO_REGISTRO = 'AV') THEN
      vQueryFinal     := vQueryFinal || vQueryTransacaoParte4 || '  
FROM VW_REL_VENDA_AV  
' || vQueryTransacaoParte2 || vQueryTransacaoParte5;
    END IF;
    IF (pTIPO_REGISTRO = 'AJ') THEN
      vQueryFinal     := vQueryFinal || vQueryTransacaoParte6 || '  
FROM VW_REL_VENDA_AJ  
' || vQueryTransacaoParte2 || vQueryTransacaoParte5;
    END IF;
    vQueryFinal         := vQueryFinal || '  
)  
GROUP BY  
';
    IF (pCONSOLIDA_GRUPO = 'S') THEN
      vQueryFinal       := vQueryFinal || ' NOME_EMPRESA_SUBGRUPO, NOME_EMPRESA_GRUPO, NOME_PRODUTO, ';
    END IF;
    IF (pCONSOLIDA_REFERENCIA = 'S') THEN
      vQueryFinal            := vQueryFinal || ' REFERENCIA, NOME_LISTA_ITEM, ';
    END IF;
    IF (pCONSOLIDA_DATA_VENDA = 'S') THEN
      vQueryFinal            := vQueryFinal || ' trunc(DATA_TRANSACAO), ';
    END IF;
    IF (pCONSOLIDA_DATA_PROCESSAMENTO = 'S') THEN
      vQueryFinal                    := vQueryFinal || ' trunc(DATA_MOVIMENTO), ';
    END IF;
    IF (pCONSOLIDA_BANCO = 'S') THEN
      vQueryFinal       := vQueryFinal || ' BANCO, AGENCIA, CONTA, ';
    END IF;
    IF (pCONSOLIDA_ESTABELECIMENTO = 'S') THEN
      vQueryFinal                 := vQueryFinal || ' CODIGO_ESTABELECIMENTO, RAZAO_SOCIAL, ';
    END IF;
    vQueryFinal := SUBSTR(vQueryFinal, 0, LENGTH(vQueryFinal) - 2);
    PR_LOG_DB(PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_RELATORIO_VENDAS_L1', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => vQueryFinal );
    OPEN retcur FOR vQueryFinal;
  END PR_RELATORIO_VENDAS_L1;
  PROCEDURE PR_CONTA_FAVORECIDO_S(
      pCODIGO_CONTA_FAVORECIDO NUMBER   := NULL,
      pCODIGO_FAVORECIDO       NUMBER   := NULL,
      pAGENCIA                 VARCHAR2 := NULL,
      pBANCO                   VARCHAR2 := NULL,
      pCONTA                   VARCHAR2 := NULL,
      pAGENCIA_CCI             VARCHAR2 := NULL,
      pCONTA_CCI               VARCHAR2 := NULL,
      retornarRegistro         CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_CONTA_FAVORECIDO NUMBER;
  BEGIN
    vCODIGO_CONTA_FAVORECIDO := pCODIGO_CONTA_FAVORECIDO;
    IF vCODIGO_CONTA_FAVORECIDO IS NULL THEN
      SELECT SQ_CONTA_FAVORECIDO.nextval
      INTO vCODIGO_CONTA_FAVORECIDO
      FROM dual;
      INSERT
      INTO TB_CONTA_FAVORECIDO
        (
          CODIGO_CONTA_FAVORECIDO,
          CODIGO_FAVORECIDO,
          AGENCIA,
          CONTA,
          BANCO,
          AGENCIA_CCI,
          CONTA_CCI
        )
        VALUES
        (
          vCODIGO_CONTA_FAVORECIDO,
          pCODIGO_FAVORECIDO,
          pAGENCIA,
          pCONTA,
          pBANCO,
          pAGENCIA_CCI,
          pCONTA_CCI
        );
    ELSE
      UPDATE TB_CONTA_FAVORECIDO
      SET AGENCIA                   = pAGENCIA,
        CONTA                       = pCONTA,
        BANCO                       = pBANCO,
        AGENCIA_CCI                 = pAGENCIA_CCI,
        CONTA_CCI                   = pCONTA_CCI
      WHERE CODIGO_CONTA_FAVORECIDO = vCODIGO_CONTA_FAVORECIDO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_CONTA_FAVORECIDO_B( pCODIGO_CONTA_FAVORECIDO => vCODIGO_CONTA_FAVORECIDO, retcur => retcur);
    END IF;
  END PR_CONTA_FAVORECIDO_S;
  PROCEDURE PR_CONTA_FAVORECIDO_U(
      pCODIGO_CONTA_FAVORECIDO      NUMBER := NULL,
      pCODIGO_CONTA_FAVORECIDO_NOVO NUMBER := NULL,
      pCODIGO_PROCESSO              NUMBER := NULL )
  AS
    vCODIGO_FAVORECIDO NUMBER;
    vDescricao         VARCHAR2(200);
    pQTDE_ALTERADA     NUMBER;
  BEGIN
    SELECT CODIGO_FAVORECIDO
    INTO vCODIGO_FAVORECIDO
    FROM TB_CONTA_FAVORECIDO
    WHERE CODIGO_CONTA_FAVORECIDO = pCODIGO_CONTA_FAVORECIDO;
    UPDATE TB_CONTA_FAVORECIDO_PRODUTO
    SET CODIGO_CONTA_FAVORECIDO   = pCODIGO_CONTA_FAVORECIDO_NOVO
    WHERE CODIGO_CONTA_FAVORECIDO = pCODIGO_CONTA_FAVORECIDO;
    COMMIT;
    FOR Contas IN
    (SELECT     *
    FROM TB_CONTA_FAVORECIDO_PRODUTO
    WHERE CODIGO_CONTA_FAVORECIDO = pCODIGO_CONTA_FAVORECIDO_NOVO
    )
    LOOP
      PR_TRANSACAO_SUBSTITUI_CONTA( Contas.CODIGO_CONTA_FAVORECIDO , Contas.CODIGO_PRODUTO , Contas.REFERENCIA , pQTDE_ALTERADA);
      IF pQTDE_ALTERADA > 0 THEN
        vDescricao     := 'SUBSTITUIR CONTA FAVORECIDO PRODUTO - Codigo Conta [' || Contas.CODIGO_CONTA_FAVORECIDO || '], Codigo Produto [' || Contas.CODIGO_PRODUTO || '] e Codigo Referencia [' || Contas.REFERENCIA || '] do Favorecido [' || vCODIGO_FAVORECIDO || ']. Total substituicoes [' || pQTDE_ALTERADA || '].';
        PR_LOG_DB( PDESCRICAO_LOG => vDescricao, PTIPO_ORIGEM => 'PR_CONTA_FAVORECIDO_U', PCODIGO_ORIGEM => pCODIGO_PROCESSO, PDESCRICAOGRANDE_LOG => NULL );
      END IF;
    END LOOP;
    UPDATE TB_FAVORECIDO
    SET FAVORECIDO_BLOQUEADO = 0
    WHERE CODIGO_FAVORECIDO  = vCODIGO_FAVORECIDO;
    COMMIT;
  END PR_CONTA_FAVORECIDO_U;
  PROCEDURE PR_CONTA_FAVORECIDO_B(
      pCODIGO_CONTA_FAVORECIDO NUMBER ,
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_CONTA_FAVORECIDO ,
    A.CODIGO_FAVORECIDO ,
    A.BANCO ,
    A.AGENCIA ,
    A.CONTA ,
    A.AGENCIA_CCI ,
    A.CONTA_CCI ,
    (SELECT COUNT(1)
    FROM TB_TRANSACAO
    WHERE CODIGO_CONTA_FAVORECIDO = A.CODIGO_CONTA_FAVORECIDO
    )
  AS
    TRANSACOES 
    FROM TB_CONTA_FAVORECIDO A WHERE A.CODIGO_CONTA_FAVORECIDO = pCODIGO_CONTA_FAVORECIDO;
  END PR_CONTA_FAVORECIDO_B;
  PROCEDURE PR_CONTA_FAVORECIDO_L(
      pCODIGO_FAVORECIDO NUMBER   := NULL ,
      pBANCO             VARCHAR2 := NULL ,
      pAGENCIA           VARCHAR2 := NULL ,
      pCONTA             VARCHAR2 := NULL ,
      pAGENCIA_CCI       VARCHAR2 := NULL ,
      pCONTA_CCI         VARCHAR2 := NULL ,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(500);
  BEGIN
    vQuery := '';
    IF pCODIGO_FAVORECIDO IS NOT NULL THEN
      vQuery              := vQuery || 'A.CODIGO_FAVORECIDO = ''' || pCODIGO_FAVORECIDO || ''' and ';
    END IF;
    IF pBANCO IS NOT NULL THEN
      vQuery  := vQuery || 'A.BANCO = ''' || pBANCO || ''' and ';
    END IF;
    IF pAGENCIA IS NOT NULL THEN
      vQuery    := vQuery || 'A.AGENCIA = ''' || pAGENCIA || ''' and ';
    END IF;
    IF pCONTA IS NOT NULL THEN
      vQuery  := vQuery || 'A.CONTA = ''' || pCONTA || ''' and ';
    END IF;
    IF pAGENCIA_CCI IS NOT NULL THEN
      vQuery        := vQuery || 'A.AGENCIA_CCI = ''' || pAGENCIA_CCI || ''' and ';
    END IF;
    IF pCONTA_CCI IS NOT NULL THEN
      vQuery      := vQuery || 'A.CONTA_CCI = ''' || pCONTA_CCI || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := '  
select     
A.CODIGO_CONTA_FAVORECIDO  
, A.CODIGO_FAVORECIDO  
, A.CONTA  
, A.BANCO  
, A.AGENCIA  
, A.AGENCIA_CCI  
, A.CONTA_CCI  
,  
(  
select count(1)  
from TB_TRANSACAO  
where CODIGO_CONTA_FAVORECIDO = A.CODIGO_CONTA_FAVORECIDO  
) AS  TRANSACOES   
from TB_CONTA_FAVORECIDO A   
' || vQuery || 'order by A.CODIGO_FAVORECIDO  
'; 
    OPEN retcur FOR vQuery;
  END PR_CONTA_FAVORECIDO_L;
  PROCEDURE PR_CONTA_FAVORECIDO_R(
      pCODIGO_CONTA_FAVORECIDO NUMBER)
  AS
  BEGIN
    DELETE TB_CONTA_FAVORECIDO
    WHERE CODIGO_CONTA_FAVORECIDO = pCODIGO_CONTA_FAVORECIDO;
  END PR_CONTA_FAVORECIDO_R;
  PROCEDURE PR_CONTA_PRODUTO_L(
      pCODIGO_PRODUTO          NUMBER  := NULL,
      pCODIGO_CONTA_FAVORECIDO NUMBER  := NULL,
      pCODIGO_FAVORECIDO       NUMBER  := NULL,
      pREFERENCIA              VARCHAR2:= NULL, 
      retcur OUT TP_REFCURSOR )
  AS
    vQuery   VARCHAR2(1000);
    vQueryCF VARCHAR2(500);
  BEGIN
    vQuery := '';
    IF pCODIGO_PRODUTO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_PRODUTO = ''' || pCODIGO_PRODUTO || ''' and ';
    END IF;
    IF pCODIGO_CONTA_FAVORECIDO IS NOT NULL THEN
      vQuery                    := vQuery || 'temp.CODIGO_CONTA_FAVORECIDO = ''' || pCODIGO_CONTA_FAVORECIDO || ''' and ';
    END IF;
    IF pREFERENCIA IS NOT NULL THEN 
      vQuery       := vQuery || 'temp.REFERENCIA = ''' || pREFERENCIA || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQueryCF := '';
    IF pCODIGO_FAVORECIDO IS NOT NULL THEN
      vQueryCF            := vQueryCF || 'cf.CODIGO_FAVORECIDO = ''' || pCODIGO_FAVORECIDO || ''' and ';
    END IF;
    IF LENGTH(vQueryCF) > 0 THEN
      vQueryCF         := SUBSTR(vQueryCF, 0, LENGTH(vQueryCF) - 4);
      vQueryCF         := 'where ' || vQueryCF;
    END IF;
    vQueryCF := '  
select     
cfp.CODIGO_PRODUTO  
, cfp.CODIGO_CONTA_FAVORECIDO  
, cfp.REFERENCIA 
, cf.BANCO  
, cf.AGENCIA  
, cf.CONTA  
, cf.AGENCIA_CCI  
, cf.CONTA_CCI  
from TB_CONTA_FAVORECIDO_PRODUTO cfp  
inner join TB_CONTA_FAVORECIDO cf on cf.CODIGO_CONTA_FAVORECIDO = cfp.CODIGO_CONTA_FAVORECIDO  
' || vQueryCF;
    vQuery := '  
select     
rownum as ID  
, a.codigo_produto  
, a.nome_produto  
, temp.codigo_conta_favorecido  
, temp.referencia 
, temp.banco  
, temp.agencia  
, temp.conta  
, temp.agencia_CCI  
, temp.conta_CCI  
from tb_produto a  
inner join (  
' || vQueryCF || ')   
temp on a.codigo_produto = temp.codigo_produto  
' || vQuery || '  
order by     
temp.codigo_conta_favorecido  
, a.codigo_produto    
, temp.referencia   
';
    OPEN retcur FOR vQuery;
  END PR_CONTA_PRODUTO_L;
  PROCEDURE PR_CONTA_PRODUTO_S(
      pCODIGO_CONTA_FAVORECIDO NUMBER   := NULL ,
      pCODIGO_PRODUTO          NUMBER   := NULL ,
      pREFERENCIA              VARCHAR2 := NULL 
      ,
      retornarRegistro CHAR := 'N' ,
      retcur OUT TP_REFCURSOR )
  AS
    qtde INT;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_CONTA_FAVORECIDO_PRODUTO
    WHERE CODIGO_CONTA_FAVORECIDO = pCODIGO_CONTA_FAVORECIDO
    AND CODIGO_PRODUTO            = pCODIGO_PRODUTO
    AND REFERENCIA                = pREFERENCIA; 
    IF qtde = 0 THEN
      INSERT
      INTO TB_CONTA_FAVORECIDO_PRODUTO
        (
          CODIGO_CONTA_FAVORECIDO,
          CODIGO_PRODUTO,
          REFERENCIA
        )
        VALUES
        (
          pCODIGO_CONTA_FAVORECIDO,
          pCODIGO_PRODUTO,
          pREFERENCIA
        );
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_CONTA_PRODUTO_B ( pCODIGO_CONTA_FAVORECIDO => pCODIGO_CONTA_FAVORECIDO , pCODIGO_PRODUTO => pCODIGO_PRODUTO , pREFERENCIA => pREFERENCIA , retcur => retcur );
    END IF;
  END PR_CONTA_PRODUTO_S;
  PROCEDURE PR_CONTA_PRODUTO_B
    (
      pCODIGO_CONTA_FAVORECIDO NUMBER ,
      pCODIGO_PRODUTO          NUMBER ,
      pREFERENCIA              VARCHAR2 
      ,
      retcur OUT TP_REFCURSOR
    )
  AS
  BEGIN
    OPEN retcur FOR SELECT 1
  AS
    ID ,
    c.conta ,
    a.nome_produto ,
    a.codigo_produto ,
    b.codigo_conta_favorecido ,
    b.referencia 
    FROM tb_produto A left join tb_conta_favorecido_produto B ON a.codigo_produto = b.codigo_produto AND b.referencia = pREFERENCIA AND b.codigo_conta_favorecido = pCODIGO_CONTA_FAVORECIDO left join tb_conta_favorecido C ON b.codigo_conta_favorecido = c.codigo_conta_favorecido left join tb_favorecido D ON c.codigo_favorecido = d.codigo_favorecido WHERE A.CODIGO_PRODUTO = pCODIGO_PRODUTO;
  END PR_CONTA_PRODUTO_B;
  PROCEDURE PR_CONTA_PRODUTO_R
    (
      pCODIGO_CONTA_FAVORECIDO NUMBER   := NULL ,
      pCODIGO_PRODUTO          NUMBER   := NULL ,
      pREFERENCIA              VARCHAR2 := NULL
    )
  AS
    vQuery VARCHAR2
    (
      500
    )
    ;
  BEGIN
    vQuery := '';
    IF pCODIGO_CONTA_FAVORECIDO IS NOT NULL THEN
      vQuery                    := vQuery || 'A.CODIGO_CONTA_FAVORECIDO = ''' || pCODIGO_CONTA_FAVORECIDO || ''' and ';
    END IF;
    IF pCODIGO_PRODUTO IS NOT NULL THEN
      vQuery           := vQuery || 'A.CODIGO_PRODUTO = ''' || pCODIGO_PRODUTO || ''' and ';
    END IF;
    IF pREFERENCIA IS NOT NULL THEN 
      vQuery       := vQuery || 'A.REFERENCIA = ''' || pREFERENCIA || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := 'delete from TB_CONTA_FAVORECIDO_PRODUTO A ' || vQuery;
    EXECUTE immediate vQuery;
  END PR_CONTA_PRODUTO_R;
  PROCEDURE PR_ARQUIVO_EXPORTACAO_GERAR
    (
      pSIGLA_REDE     VARCHAR2 := NULL,
      pDATA_MOVIMENTO DATE     := NULL,
      retcur OUT TP_REFCURSOR
    )
  AS
    qtde INT;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_ARQUIVO_EXPORTACAO
    WHERE TRUNC(DATA_MOVIMENTO, 'DDD') = TRUNC(pDATA_MOVIMENTO, 'DDD')
    AND SIGLA_REDE                     = pSIGLA_REDE;
    IF qtde                            > 0 THEN
      UPDATE TB_ARQUIVO_EXPORTACAO
      SET SEQUENCIA                      = (SEQUENCIA + 1)
      WHERE TRUNC(DATA_MOVIMENTO, 'DDD') = TRUNC(pDATA_MOVIMENTO, 'DDD')
      AND SIGLA_REDE                     = pSIGLA_REDE;
    ELSE
      INSERT
      INTO TB_ARQUIVO_EXPORTACAO
        (
          DATA_MOVIMENTO,
          SIGLA_REDE,
          SEQUENCIA
        )
        VALUES
        (
          TRUNC(pDATA_MOVIMENTO, 'DDD'),
          pSIGLA_REDE,
          1
        );
    END IF;
    OPEN retcur FOR SELECT SEQUENCIA FROM TB_ARQUIVO_EXPORTACAO WHERE TRUNC
    (
      DATA_MOVIMENTO, 'DDD'
    )
    = TRUNC
    (
      pDATA_MOVIMENTO, 'DDD'
    )
    AND SIGLA_REDE = pSIGLA_REDE;
  END PR_ARQUIVO_EXPORTACAO_GERAR;
  PROCEDURE PR_PRODUTO_PLANO_L
    (
      pCODIGO_PLANO NUMBER := NULL,
      retcur OUT TP_REFCURSOR
    )
  AS
  BEGIN
    OPEN retcur FOR SELECT codigo_produto,
    codigo_plano FROM tb_produto_plano WHERE (pCODIGO_PLANO IS NULL OR CODIGO_PLANO = pCODIGO_PLANO);
  END PR_PRODUTO_PLANO_L;
  PROCEDURE PR_PERMISSAO_S
    ( 
      pCODIGO_PERMISSAO    NUMBER   := NULL,
      pNOME_PERMISSAO      VARCHAR2 := NULL,
      pDESCRICAO_PERMISSAO VARCHAR2 := NULL,
      retornarRegistro     CHAR     := 'N',
      retcur OUT TP_REFCURSOR
    )
  AS
    qtde NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO qtde
    FROM TB_PERMISSAO
    WHERE CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
    IF qtde = 0 THEN
      INSERT
      INTO TB_PERMISSAO
        (
          CODIGO_PERMISSAO,
          NOME_PERMISSAO,
          DESCRICAO_PERMISSAO
        )
        VALUES
        (
          pCODIGO_PERMISSAO,
          pNOME_PERMISSAO,
          pDESCRICAO_PERMISSAO
        );
    ELSE
      UPDATE TB_PERMISSAO
      SET NOME_PERMISSAO     = pNOME_PERMISSAO,
        DESCRICAO_PERMISSAO  = pDESCRICAO_PERMISSAO
      WHERE CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_PERMISSAO_B( pCODIGO_PERMISSAO => pCODIGO_PERMISSAO, retcur => retcur);
    END IF;
  END PR_PERMISSAO_S;
  PROCEDURE PR_PERMISSAO_B(
      pCODIGO_PERMISSAO NUMBER := NULL,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_PERMISSAO ,
    A.NOME_PERMISSAO ,
    A.DESCRICAO_PERMISSAO FROM TB_PERMISSAO A WHERE A.CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
  END PR_PERMISSAO_B;
  PROCEDURE PR_PERMISSAO_L(
      pCODIGO_PERMISSAO NUMBER := NULL ,
      retcur OUT TP_REFCURSOR)
  AS
    vQUERY VARCHAR2(2000);
  BEGIN
    vQuery := '         
select  A.CODIGO_PERMISSAO        
,       A.NOME_PERMISSAO        
,       A.DESCRICAO_PERMISSAO        
from    TB_PERMISSAO A        
where   (' || pCODIGO_PERMISSAO || ' is null or A.CODIGO_PERMISSAO = ' || pCODIGO_PERMISSAO || ')' || ' ORDER BY A.CODIGO_PERMISSAO ' 
    ;
    PR_LOG_DB( PDESCRICAO_LOG => vQuery, PTIPO_ORIGEM => 'PR_PERMISSAO_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => NULL );
    OPEN retcur FOR vQUERY;
  END PR_PERMISSAO_L;
  PROCEDURE PR_PERMISSAO_R(
      pCODIGO_PERMISSAO NUMBER )
  AS
  BEGIN
    DELETE TB_PERMISSAO A WHERE A.CODIGO_PERMISSAO = pCODIGO_PERMISSAO;
  END PR_PERMISSAO_R;
  PROCEDURE PR_REL_TRANS_INCONSISTENTE_L(
      pCODIGO_ARQUIVO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_CRITICA ,
    A.CODIGO_PROCESSO ,
    A.CODIGO_ARQUIVO ,
    A.CODIGO_ARQUIVO_ITEM ,
    A.DATA_CRITICA ,
    A.TIPO_CRITICA ,
    A.DESCRICAO_CRITICA ,
    A.CONTEUDO_CRITICA ,
    A.TIPO_LINHA ,
    A.NOME_CAMPO ,
    B.TIPO_ARQUIVO_ITEM ,
    B.CONTEUDO_ARQUIVO_ITEM ,
    B.STATUS_ARQUIVO_ITEM FROM TB_CRITICA A inner join TB_ARQUIVO_ITEM B ON A.CODIGO_ARQUIVO_ITEM = B.CODIGO_ARQUIVO_ITEM WHERE A.CODIGO_ARQUIVO = pCODIGO_ARQUIVO;
  END PR_REL_TRANS_INCONSISTENTE_L;
  PROCEDURE PR_REL_TRANS_INCONSISTENTE_B(
      pCNPJ VARCHAR2 :=NULL,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_ESTABELECIMENTO ,
    A.CNPJ ,
    A.RAZAO_SOCIAL ,
    A.BANCO ,
    A.AGENCIA ,
    A.CONTA ,
    A.ESTABELECIMENTO_CSU ,
    A.ESTABELECIMENTO_SITEF ,
    A.CODIGO_EMPRESA_SUBGRUPO ,
    B.CODIGO_EMPRESA_GRUPO ,
    B.NOME_EMPRESA_SUBGRUPO ,
    B.DESCRICAO_EMPRESA_SUBGRUPO ,
    C.NOME_EMPRESA_GRUPO ,
    C.DESCRICAO_EMPRESA_GRUPO ,
    C.ENVIAR_NEGATIVO_EMPRESA_GRUPO ,
    D.CODIGO_FAVORECIDO ,
    D.CODIGO_ESTABELECIMENTO ,
    E.CNPJ_FAVORECIDO ,
    E.NOME_FAVORECIDO FROM TB_ESTABELECIMENTO A inner join TB_EMPRESA_SUBGRUPO B ON A.CODIGO_EMPRESA_SUBGRUPO = B.CODIGO_EMPRESA_SUBGRUPO inner join TB_EMPRESA_GRUPO C ON B.CODIGO_EMPRESA_GRUPO = C.CODIGO_EMPRESA_GRUPO inner join TB_ESTABELECIMENTO_FAVORECIDO D ON A.CODIGO_ESTABELECIMENTO = D.CODIGO_ESTABELECIMENTO inner join TB_FAVORECIDO E ON D.CODIGO_FAVORECIDO = E.CODIGO_FAVORECIDO WHERE A.CNPJ = pCNPJ;
  END PR_REL_TRANS_INCONSISTENTE_B;
  PROCEDURE PR_TEM_CESSAO_C(
      pCodigoProcesso NUMBER,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery1                     VARCHAR2(1000);
    vMsg                        VARCHAR2(2000);
    vcodigo_processo_Ret_cessao NUMBER;
    vCursor TP_REFCURSOR;
  BEGIN
    vQuery1                     := NULL;
    vMsg                        := NULL;
    vcodigo_processo_Ret_cessao := 0;
    vQuery1 := 'select DISTINCT codigo_processo_ret_cessao from tb_transacao where codigo_processo_ret_cessao is not null and codigo_processo = ' || pCodigoProcesso;
    OPEN vCursor FOR vQuery1;
    LOOP
      FETCH vCursor INTO vcodigo_processo_Ret_cessao;
      EXIT
    WHEN vCursor%NOTFOUND;
      IF vMSG IS NULL THEN
        vMsg  := 'Retorno de Cessao [' || vcodigo_processo_Ret_cessao;
      ELSE
        vMsg := rtrim(NVL(vMsg,'')) || ', ' || vcodigo_processo_Ret_cessao;
      END IF;
    END LOOP;
    CLOSE vCursor;
    IF vMsg IS NOT NULL THEN
      vMsg  := 'Extrato ' || pCodigoProcesso || ' contendo ' || vMsg || ']';
    END IF;
    OPEN retcur FOR SELECT rtrim(vMsg)
  AS
    RETORNO FROM dual;
  END PR_TEM_CESSAO_C;
  PROCEDURE PR_TEM_MATERA_C(
      pCodigoProcesso NUMBER,
      retcur OUT TP_REFCURSOR )
  AS
    vMsg    VARCHAR2(2000);
    vQtd    NUMBER;
	vQtd1   NUMBER;
	vQtd2   NUMBER;
    vCursor TP_REFCURSOR;
  BEGIN
	vMsg	:= NULL;
	vQtd    := 0;
    vQtd1   := 0;
    vQtd2   := 0;	
	SELECT 	COUNT(1)
	INTO    vQtd1
	FROM 	TB_TRANSACAO 
	WHERE	status_transacao = '2' 
	and 	NVL(antecipacao, 'N') = 'N'	
	and 	codigo_processo =  pCodigoProcesso;
	SELECT 	COUNT(1)
	INTO	vQtd2
	FROM 	TB_TRANSACAO 
	WHERE	status_transacao = '2' 
	and 	NVL(antecipacao, 'N') = 'S'	
	and		NVL(codigo_antecipacao, 0) <> 0	
	and 	codigo_processo =  pCodigoProcesso;	
	vQtd := vQtd1 + vQtd2;	
    IF ( NVL(vQtd,0) != 0 ) THEN
        vMsg := '[' || vQtd || '] Transacaoes Enviado Matera';	
		IF ( NVL(vQtd1,0) != 0 AND NVL(vQtd2,0) != 0) THEN
			IF ( NVL(vQtd1,0) != 0 ) THEN
				vMsg := vMsg || ' [' || vQtd1 || '] de Pagamento';
			END IF;		
			IF ( NVL(vQtd2,0) != 0 ) THEN
				vMsg := vMsg || ' [' || vQtd2 || '] de Antecipacao';
			END IF;
		ELSE
			IF ( NVL(vQtd1,0) != 0 ) THEN
				vMsg := vMsg || ' de Pagamento';
			END IF;		
			IF ( NVL(vQtd2,0) != 0 ) THEN
				vMsg := vMsg || ' de Antecipacao';
			END IF;		
		END IF;
    END IF;
    IF vMsg IS NOT NULL THEN
      vMsg  := 'Extrato ' || pCodigoProcesso || ' contendo ' || vMsg;
    END IF;
    OPEN retcur FOR SELECT rtrim(vMsg) AS RETORNO FROM dual;
  END PR_TEM_MATERA_C;
  PROCEDURE PR_ESTORNO_EXTRATO_S(
      pCodigoProcesso NUMBER )
  AS
    vMsg VARCHAR2(2000);  
    vCursor TP_REFCURSOR; 
    vContinua CHAR(01);   
    vQTD      NUMBER;
  BEGIN
    PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - INICIO' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
    vContinua := 'S';
    PR_TEM_CESSAO_C( pCodigoProcesso => pCodigoProcesso, retcur => vCursor );
    LOOP
      FETCH vCursor INTO vMsg;
      EXIT
    WHEN vCursor%NOTFOUND;
      IF vMSG IS NOT NULL THEN
        PR_LOG_DB( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - nao efetuada. Motivo: [' || vMsg || ']', PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S', PCODIGO_ORIGEM => pCodigoProcesso, PDESCRICAOGRANDE_LOG => NULL );
        vContinua := 'N';
      END IF;
    END LOOP;
    PR_TEM_MATERA_C( pCodigoProcesso => pCodigoProcesso, retcur => vCursor );
    LOOP
      FETCH vCursor INTO vMsg;
      EXIT
    WHEN vCursor%NOTFOUND;
      IF vMSG IS NOT NULL THEN
        PR_LOG_DB( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - nao efetuada. Motivo: [' || vMsg || ']', PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S', PCODIGO_ORIGEM => pCodigoProcesso, PDESCRICAOGRANDE_LOG => NULL );
        vContinua := 'N';
      END IF;
    END LOOP;
    IF vContinua = 'S' THEN
      DELETE tb_transacao_venda
      WHERE codigo_transacao IN
        (SELECT b.codigo_transacao
        FROM tb_transacao b
        WHERE b.codigo_processo = pCodigoProcesso
        ) ;
      vQTD := sql%rowcount;
      COMMIT;
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Transacoes CV [' || vQTD || '] Excluï¿½das' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
      DELETE TB_TRANSACAO_ANULACAO_VENDA
      WHERE codigo_transacao IN
        (SELECT b.codigo_transacao
        FROM tb_transacao b
        WHERE b.codigo_processo = pCodigoProcesso
        ) ;
      vQTD := sql%rowcount;
      COMMIT;
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Transacoes AV [' || vQTD || '] Excluï¿½das' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
      DELETE TB_TRANSACAO_ANULACAO_PAGTO
      WHERE codigo_transacao IN
        (SELECT b.codigo_transacao
        FROM tb_transacao b
        WHERE b.codigo_processo = pCodigoProcesso
        ) ;
      vQTD := sql%rowcount;
      COMMIT;
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Transacoes AP [' || vQTD || '] Excluï¿½das' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
      DELETE TB_TRANSACAO_PAGAMENTO
      WHERE codigo_transacao IN
        (SELECT b.codigo_transacao
        FROM tb_transacao b
        WHERE b.codigo_processo = pCodigoProcesso
        ) ;
      vQTD := sql%rowcount;
      COMMIT;
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Transacoes CP [' || vQTD || '] Excluï¿½das' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
      DELETE TB_TRANSACAO_AJUSTE
      WHERE codigo_transacao IN
        (SELECT b.codigo_transacao
        FROM tb_transacao b
        WHERE b.codigo_processo = pCodigoProcesso
        ) ;
      vQTD := sql%rowcount;
      COMMIT;
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Transacoes AJ [' || vQTD || '] Excluï¿½das' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
      DELETE tb_transacao
      WHERE codigo_processo = pCodigoProcesso ;
      vQTD                 := sql%rowcount;
      COMMIT;
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Transacoes Header [' || vQTD || '] Excluï¿½das' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
      PR_CARONA_D ( PCODIGO_ARQUIVO_ITEM => NULL , PCODIGO_ARQUIVO => NULL , PCODIGO_PROCESSO => pCodigoProcesso , PQTD => VQTD );
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Arquivo Item [' || vQTD || '] Excluï¿½das' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
      UPDATE tb_arquivo
      SET status_arquivo    = 3
      WHERE codigo_processo = pCodigoProcesso;
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Atualizando status do ARQUIVO para [3] ESTORNADO' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
      UPDATE tb_processo
      SET status_processo   = 2,
        status_estagio      = 0,
        data_estagio        = sysdate,
        tipo_estagio_atual  = ''
      WHERE codigo_processo = pcodigoprocesso;
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - Atualizando status do PROCESSO para [2] ESTORNADO' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
    END IF; 
    PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE EXTRATO - FIM' , PTIPO_ORIGEM => 'PR_ESTORNO_EXTRATO_S' , PCODIGO_ORIGEM => pCodigoProcesso , PDESCRICAOGRANDE_LOG => NULL );
  END PR_ESTORNO_EXTRATO_S;
  PROCEDURE PR_ESTORNO_RETORNO_CESSAO_S(
      pCodigoProcessoCessao NUMBER )
  AS
    vcodigo_processo NUMBER;
    vEXISTE          NUMBER;
    vCursorTrnCessionadas TP_REFCURSOR;
    vCursorTemMatera TP_REFCURSOR;
    vCursorTemCessao TP_REFCURSOR;
    vContinua CHAR(1);
    vEstornou CHAR(1);
    vTemTrn   CHAR(1);
    vMsg      VARCHAR2(2000);
    vQTD      NUMBER;
    vQTDTOT   NUMBER;
  BEGIN
    PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - INICIO' , PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S' , PCODIGO_ORIGEM => pCodigoProcessoCessao , PDESCRICAOGRANDE_LOG => NULL );
    vcodigo_processo := 0;
    vEstornou        := 'N';
    vTemTrn          := 'N';
    OPEN vCursorTrnCessionadas FOR
    SELECT DISTINCT CODIGO_PROCESSO
    FROM TB_TRANSACAO
    WHERE codigo_processo_ret_cessao = pCodigoProcessoCessao;
    LOOP
      FETCH vCursorTrnCessionadas INTO vCodigo_Processo;
      EXIT
    WHEN vCursorTrnCessionadas%NOTFOUND;
      vContinua := 'S';
      PR_TEM_MATERA_C ( pCodigoProcesso => vCodigo_Processo , retcur => vCursorTemMatera );
      LOOP
        FETCH vCursorTemMatera INTO vMsg;
        EXIT
      WHEN vCursorTemMatera%NOTFOUND;
        IF vMSG IS NOT NULL THEN
          PR_LOG_DB( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - Para este processo [' || vCodigo_Processo || '] nao efetuada. Motivo: [' || vMsg || ']', PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S', PCODIGO_ORIGEM => pCodigoProcessoCessao, PDESCRICAOGRANDE_LOG => NULL );
          PR_LOG_DB( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - Do processo [' || pCodigoProcessoCessao || '] nao efetuada. Motivo: [' || vMsg || ']', PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S', PCODIGO_ORIGEM => vCodigo_Processo, PDESCRICAOGRANDE_LOG => NULL );
          vContinua := 'N';
          vTemTrn   := 'S';
        END IF;
      END LOOP;
      CLOSE vCursorTemMatera;
      IF vContinua = 'S' THEN
        vEstornou := 'S';
        vTemTrn   := 'S';
        UPDATE TB_TRANSACAO
        SET STATUS_RET_CESSAO            = 0 ,
          CODIGO_FAVORECIDO              = codigo_favorecido_original ,
          CODIGO_CONTA_FAVORECIDO        = codigo_conta_favorecido_orig ,
          CODIGO_PROCESSO_RET_CESSAO     = NULL ,
          CODIGO_ARQUIVO_RET_CESSAO      = NULL ,
          DATA_POSICAO_CARTEIRA          = NULL
        WHERE CODIGO_PROCESSO_RET_CESSAO = pCodigoProcessoCessao
        AND CODIGO_PROCESSO              = vcodigo_processo
        AND NVL(ANTECIPACAO,'N')        <> 'S';
        vQTD                            := sql%rowcount;
        vQTDTOT                         := +vQTD;
        UPDATE TB_TRANSACAO
        SET STATUS_RET_CESSAO            = 0 ,
          CODIGO_FAVORECIDO              = codigo_favorecido_original ,
          CODIGO_CONTA_FAVORECIDO        = codigo_conta_favorecido_orig ,
          CODIGO_PROCESSO_RET_CESSAO     = NULL ,
          CODIGO_ARQUIVO_RET_CESSAO      = NULL ,
          DATA_POSICAO_CARTEIRA          = NULL ,
          STATUS_TRANSACAO               = STATUS_TRANSACAO_ORIGINAL ,
          DATA_REPASSE_CALCULADA         = DATA_REPASSE_ORIGINAL ,
          STATUS_TRANSACAO_ORIGINAL      = '' ,
          DATA_REPASSE_ORIGINAL          = NULL ,
          ANTECIPACAO                    = '' ,
          CODIGO_ANTECIPACAO             = NULL
        WHERE CODIGO_PROCESSO_RET_CESSAO = pCodigoProcessoCessao
        AND CODIGO_PROCESSO              = vcodigo_processo
        AND NVL(ANTECIPACAO,'N')         = 'S';
        vQTD                            := sql%rowcount;
        vQTDTOT                         := +vQTD;
        PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - Efetuada em transacoes deste processo extrato. Numero do processo de cessao: [' || pCodigoProcessoCessao || '] Atualizacoes [' || vQTD || ']' , PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S' , PCODIGO_ORIGEM => vcodigo_processo , PDESCRICAOGRANDE_LOG => NULL );
        PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - Efetuada em transacoes do processo extrato numero: [' || vcodigo_processo || '] Atualizacoes [' || vQTD || ']' , PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S' , PCODIGO_ORIGEM => pCodigoProcessoCessao , PDESCRICAOGRANDE_LOG => NULL );
        vContinua := 'S';
        PR_TEM_CESSAO_C(pCodigoProcesso => vcodigo_processo, retcur => vCursorTemCessao );
        LOOP
          FETCH vCursorTemCessao INTO vMsg;
          EXIT
        WHEN vCursorTemCessao%NOTFOUND;
          IF vMSG IS NOT NULL THEN
            PR_LOG_DB( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - Efetuado, sem alteracao status deste processo, porque tem cessao de outros arquivos. Motivo: [' || vMsg || ']', PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S', PCODIGO_ORIGEM => vcodigo_processo, PDESCRICAOGRANDE_LOG => NULL );
            vContinua := 'N';
          END IF;
        END LOOP;
        CLOSE vCursorTemCessao;
        IF vContinua = 'S' THEN
          UPDATE tb_processo SET status_processo   = 1,  data_estagio        = sysdate  WHERE codigo_processo = vcodigo_processo;
        END IF;
      END IF;
    END LOOP;
    CLOSE vCursorTrnCessionadas;
    IF ( vTemTrn = 'N' ) THEN
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - nao existiam transacoes cessionadas deste processo' , PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S' , PCODIGO_ORIGEM => pCodigoProcessoCessao , PDESCRICAOGRANDE_LOG => NULL );
    END IF;
	IF ( vEstornou = 'S') THEN
		PR_CARONA_D ( PCODIGO_ARQUIVO_ITEM => NULL , PCODIGO_ARQUIVO => NULL , PCODIGO_PROCESSO => pCodigoProcessoCessao , PQTD => VQTD );
		PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - Arquivo Item [' || vQTD || '] Excluidas' , PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S' , PCODIGO_ORIGEM => pCodigoProcessoCessao , PDESCRICAOGRANDE_LOG => NULL );
		UPDATE tb_arquivo	SET status_arquivo    = 3	WHERE codigo_processo = pCodigoProcessoCessao;
		UPDATE tb_processo	SET status_processo   = 2 ,	  status_estagio      = 3 ,	  data_estagio        = sysdate		WHERE codigo_processo = pCodigoProcessoCessao;
		PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - Transacoes Atualizadas [ ' || vQTDTOT || ']' , PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S' , PCODIGO_ORIGEM => pCodigoProcessoCessao , PDESCRICAOGRANDE_LOG => NULL );
	ELSE
      PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - Nao  efetuado' , PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S' , PCODIGO_ORIGEM => pCodigoProcessoCessao , PDESCRICAOGRANDE_LOG => NULL );
	END IF;
	PR_LOG_DB ( PDESCRICAO_LOG => 'ESTORNO DE CESSAO - FIM' , PTIPO_ORIGEM => 'PR_ESTORNO_RETORNO_CESSAO_S' , PCODIGO_ORIGEM => pCodigoProcessoCessao , PDESCRICAOGRANDE_LOG => NULL );
  END PR_ESTORNO_RETORNO_CESSAO_S;
  PROCEDURE PR_ITEM_DESBLOQUEIO_S(
      pCODIGO_ITEM_DESBLOQUEIO NUMBER := NULL,
      pCODIGO_ARQUIVO_ORIGEM   NUMBER := NULL,
      pCODIGO_ARQUIVO_DESTINO  NUMBER := NULL,
      pCODIGO_ARQUIVO_ITEM     NUMBER := NULL,
      retornarRegistro         CHAR   := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_ITEM_DESBLOQUEIO NUMBER;
    VCODIGO_PROCESSO         NUMBER; 
  BEGIN
    vCODIGO_ITEM_DESBLOQUEIO := pCODIGO_ITEM_DESBLOQUEIO;
    UPDATE TB_ITEM_DESBLOQUEIO
    SET STATUS_DESBLOQUEIO    = 'C'
    WHERE CODIGO_ARQUIVO_ITEM = PCODIGO_ARQUIVO_ITEM;
    IF vCODIGO_ITEM_DESBLOQUEIO IS NULL THEN
      SELECT SQ_ITEM_DESBLOQUEIO.nextval INTO vCODIGO_ITEM_DESBLOQUEIO FROM dual;
      INSERT
      INTO TB_ITEM_DESBLOQUEIO
        (
          CODIGO_ITEM_DESBLOQUEIO,
          CODIGO_ARQUIVO_ORIGEM,
          CODIGO_ARQUIVO_DESTINO,
          CODIGO_ARQUIVO_ITEM,
          DATA_INCLUSAO,
          STATUS_DESBLOQUEIO
        )
        VALUES
        (
          vCODIGO_ITEM_DESBLOQUEIO,
          pCODIGO_ARQUIVO_ORIGEM,
          pCODIGO_ARQUIVO_DESTINO,
          pCODIGO_ARQUIVO_ITEM,
          SYSDATE,
          'A'
        ); 
    ELSE
      UPDATE TB_ITEM_DESBLOQUEIO
      SET CODIGO_ARQUIVO_ORIGEM     = NVL(pCODIGO_ARQUIVO_ORIGEM, CODIGO_ARQUIVO_ORIGEM) ,
        CODIGO_ARQUIVO_DESTINO      = NVL(pCODIGO_ARQUIVO_DESTINO, CODIGO_ARQUIVO_DESTINO) ,
        CODIGO_ARQUIVO_ITEM         = NVL(pCODIGO_ARQUIVO_ITEM, CODIGO_ARQUIVO_ITEM) ,
        STATUS_DESBLOQUEIO          = 'A' 
      WHERE CODIGO_ITEM_DESBLOQUEIO = vCODIGO_ITEM_DESBLOQUEIO;
    END IF;
    VCODIGO_PROCESSO := 0;
    BEGIN
      SELECT A.CODIGO_PROCESSO
      INTO VCODIGO_PROCESSO
      FROM TB_ARQUIVO A
      WHERE A.CODIGO_ARQUIVO = pCODIGO_ARQUIVO_DESTINO;
    END;
    PR_PROCESSO_STATUS_BLOQUEIO_U(PCODIGO_PROCESSO => VCODIGO_PROCESSO);
    VCODIGO_PROCESSO := 0;
    BEGIN
      SELECT A.CODIGO_PROCESSO
      INTO VCODIGO_PROCESSO
      FROM TB_ARQUIVO A
      WHERE A.CODIGO_ARQUIVO = pCODIGO_ARQUIVO_ORIGEM;
    END;
    PR_PROCESSO_STATUS_BLOQUEIO_U(PCODIGO_PROCESSO => VCODIGO_PROCESSO);
    IF retornarRegistro = 'S' THEN
      PR_ITEM_DESBLOQUEIO_B(pCODIGO_ITEM_DESBLOQUEIO => vCODIGO_ITEM_DESBLOQUEIO, retcur => retcur);
    END IF;
  END PR_ITEM_DESBLOQUEIO_S;
  PROCEDURE PR_ITEM_DESBLOQUEIO_B(
      pCODIGO_ITEM_DESBLOQUEIO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_ITEM_DESBLOQUEIO ,
    A.CODIGO_ARQUIVO_ORIGEM ,
    A.CODIGO_ARQUIVO_DESTINO ,
    A.CODIGO_ARQUIVO_ITEM FROM TB_ITEM_DESBLOQUEIO A WHERE A.CODIGO_ITEM_DESBLOQUEIO = pCODIGO_ITEM_DESBLOQUEIO;
  END PR_ITEM_DESBLOQUEIO_B;
  PROCEDURE PR_ITEM_DESBLOQUEIO_L(
      pCODIGO_ITEM_DESBLOQUEIO NUMBER := NULL,
      pCODIGO_ARQUIVO_ORIGEM   NUMBER := NULL,
      pCODIGO_ARQUIVO_DESTINO  NUMBER := NULL,
      pCODIGO_ARQUIVO_ITEM     NUMBER := NULL,
      pSTATUS_DESBLOQUEIO      NUMBER := NULL,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_ITEM_DESBLOQUEIO ,
    A.CODIGO_ARQUIVO_ORIGEM ,
    A.CODIGO_ARQUIVO_DESTINO ,
    A.CODIGO_ARQUIVO_ITEM ,
    A.STATUS_DESBLOQUEIO FROM TB_ITEM_DESBLOQUEIO A WHERE (pCODIGO_ITEM_DESBLOQUEIO IS NULL OR A.CODIGO_ITEM_DESBLOQUEIO = pCODIGO_ITEM_DESBLOQUEIO) AND (pSTATUS_DESBLOQUEIO IS NULL OR A.STATUS_DESBLOQUEIO = pSTATUS_DESBLOQUEIO);
  END PR_ITEM_DESBLOQUEIO_L;
  PROCEDURE PR_PROCESSAR_DESBLOQUEADOS(
      pCODIGO_ARQUIVO_DESTINO NUMBER := NULL,
      pCODIGO_PROCESSO        NUMBER := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQUANTIDADE_ALTERADA   INT;
    vCODIGO_ARQUIVO_ITEM   INT;
    vCODIGO_ARQUIVO_ORIGEM INT;
    vTIPO_PROCESSO         VARCHAR2(100);
    cursorDesbloqueio TP_REFCURSOR;
    vLAYOUT_ARQUIVO         VARCHAR2(10);
    vCODIGO_EMPRESA_GRUPO   NUMBER;
    vQUERY                  VARCHAR(4000);
    vTIPO_ARQUIVO_ITEM      VARCHAR(02);
    vNOME_ARQUIVO_ORIGEM    VARCHAR(200);
    vCODIGO_PROCESSO_ORIGEM NUMBER;
    vDATA_INCLUSAO_ORIGEM   DATE;
    vVALIDAR                CHAR(01);
  BEGIN
    vQUANTIDADE_ALTERADA    := 0;
    vCODIGO_ARQUIVO_ITEM    := 0;
    vCODIGO_ARQUIVO_ORIGEM  := 0;
    vTIPO_PROCESSO          := '';
    vLAYOUT_ARQUIVO         := '';
    vCODIGO_EMPRESA_GRUPO   := '';
    vQUERY                  := '';
    vTIPO_ARQUIVO_ITEM      := '';
    vNOME_ARQUIVO_ORIGEM    := 0;
    vCODIGO_PROCESSO_ORIGEM := 0;
    vVALIDAR                := 'S'; 
    vDATA_INCLUSAO_ORIGEM   := NULL;
    IF (pCODIGO_PROCESSO    IS NULL) THEN
      vVALIDAR              := 'N';
      PR_LOG_DB ( PDESCRICAO_LOG => 'Codigo Processo invalido [' || pCODIGO_PROCESSO || ']' , PTIPO_ORIGEM => 'PR_PROCESSAR_DESBLOQUEADOS' , PCODIGO_ORIGEM => pCODIGO_PROCESSO , PDESCRICAOGRANDE_LOG => NULL );
    END IF;
    IF (pCODIGO_ARQUIVO_DESTINO IS NULL) THEN
      vVALIDAR                  := 'N';
      PR_LOG_DB ( PDESCRICAO_LOG => 'Codigo Arquivo Destino invalido [' || pCODIGO_ARQUIVO_DESTINO || ']' , PTIPO_ORIGEM => 'PR_PROCESSAR_DESBLOQUEADOS' , PCODIGO_ORIGEM => pCODIGO_PROCESSO , PDESCRICAOGRANDE_LOG => NULL );
    END IF;
    IF (vVALIDAR = 'S') THEN
      SELECT TIPO_PROCESSO
      INTO vTIPO_PROCESSO
      FROM TB_PROCESSO
      WHERE CODIGO_PROCESSO = pCODIGO_PROCESSO;
      SELECT LAYOUT_ARQUIVO,
        CODIGO_EMPRESA_GRUPO
      INTO vLAYOUT_ARQUIVO,
        vCODIGO_EMPRESA_GRUPO
      FROM TB_ARQUIVO
      WHERE CODIGO_ARQUIVO        = pCODIGO_ARQUIVO_DESTINO;
      IF (NVL(vTIPO_PROCESSO,'') <> 'ProcessoCessaoInfo') THEN
        vQUERY                   := '    
select      
A.CODIGO_ARQUIVO_ITEM,     
A.CODIGO_ARQUIVO,    
A.TIPO_ARQUIVO_ITEM,    
B.NOME_ARQUIVO,    
C.CODIGO_PROCESSO,    
C.DATA_INCLUSAO    
from TB_ARQUIVO_ITEM A    
join TB_ARQUIVO B on  A.CODIGO_ARQUIVO = B.CODIGO_ARQUIVO    
join TB_PROCESSO C on B.CODIGO_PROCESSO = C.CODIGO_PROCESSO    
where         
A.STATUS_ARQUIVO_ITEM   = ''5''    
and   C.TIPO_PROCESSO         = ''' || vTIPO_PROCESSO || '''' || '    
and   B.LAYOUT_ARQUIVO        = ''' || vLAYOUT_ARQUIVO || '''' || '    
and   B.CODIGO_EMPRESA_GRUPO  = ' || NVL(vCODIGO_EMPRESA_GRUPO, 0) ;
      ELSE
        vQUERY := '    
select      
A.CODIGO_ARQUIVO_ITEM,     
A.CODIGO_ARQUIVO,    
A.TIPO_ARQUIVO_ITEM,    
B.NOME_ARQUIVO,    
C.CODIGO_PROCESSO,    
C.DATA_INCLUSAO    
from TB_ARQUIVO_ITEM A    
join TB_ARQUIVO B on  A.CODIGO_ARQUIVO = B.CODIGO_ARQUIVO    
join TB_PROCESSO C on B.CODIGO_PROCESSO = C.CODIGO_PROCESSO    
where         
A.STATUS_ARQUIVO_ITEM   = ''5''     
and   C.TIPO_PROCESSO         = ''' || vTIPO_PROCESSO || '''';
      END IF;
      OPEN cursorDesbloqueio FOR vQUERY;
      LOOP
        FETCH cursorDesbloqueio
        INTO vCODIGO_ARQUIVO_ITEM ,
          vCODIGO_ARQUIVO_ORIGEM ,
          vTIPO_ARQUIVO_ITEM ,
          vNOME_ARQUIVO_ORIGEM ,
          vCODIGO_PROCESSO_ORIGEM ,
          vDATA_INCLUSAO_ORIGEM;
        EXIT
      WHEN cursorDesbloqueio%NOTFOUND;
        UPDATE TB_ARQUIVO_ITEM A
        SET CODIGO_ARQUIVO          = pCODIGO_ARQUIVO_DESTINO ,
          STATUS_ARQUIVO_ITEM       = '0'
        WHERE A.CODIGO_ARQUIVO_ITEM = vCODIGO_ARQUIVO_ITEM;
        DELETE
        FROM TB_CRITICA
        WHERE CODIGO_ARQUIVO_ITEM = vCODIGO_ARQUIVO_ITEM;
        PR_ITEM_DESBLOQUEIO_S ( pCODIGO_ARQUIVO_ORIGEM => vCODIGO_ARQUIVO_ORIGEM , pCODIGO_ARQUIVO_DESTINO => pCODIGO_ARQUIVO_DESTINO , pCODIGO_ARQUIVO_ITEM => vCODIGO_ARQUIVO_ITEM , retcur => retcur );
        PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_PROCESSAR_DESBLOQUEADOS', PCODIGO_ORIGEM => pCODIGO_PROCESSO, PDESCRICAOGRANDE_LOG => 'CAPTURA DESBLOQUEADO - Tipo Registro: ' || vTIPO_ARQUIVO_ITEM || ' Dados anteriores: Processo: ' || vCODIGO_PROCESSO_ORIGEM || ' ID: ' || vCODIGO_ARQUIVO_ITEM || ' data ' || vDATA_INCLUSAO_ORIGEM || ' Arquivo: ' || vCODIGO_ARQUIVO_ORIGEM || '-' || TRIM(vNOME_ARQUIVO_ORIGEM) );
        PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_PROCESSAR_DESBLOQUEADOS', PCODIGO_ORIGEM => vCODIGO_PROCESSO_ORIGEM, PDESCRICAOGRANDE_LOG => 'DESBLOQUEADO CAPTURADO - Tipo Registro: ' || vTIPO_ARQUIVO_ITEM || ' Dados atuais: Processo: ' || pCODIGO_PROCESSO || ' ID: ' || vCODIGO_ARQUIVO_ITEM || ' Arquivo: ' || pCODIGO_ARQUIVO_DESTINO );
        vQUANTIDADE_ALTERADA := vQUANTIDADE_ALTERADA + 1;
      END LOOP;
      CLOSE cursorDesbloqueio;
    END IF;
    OPEN retcur FOR SELECT vQUANTIDADE_ALTERADA QUANTIDADE FROM dual;
  END PR_PROCESSAR_DESBLOQUEADOS;
  PROCEDURE PR_SESSAO_S(
      pCODIGO_SESSAO VARCHAR2,
      pCONTEUDO_SESSAO BLOB,
      retornarRegistro CHAR := 'N',
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    INSERT
    INTO TB_SESSAO
      (
        codigo_sessao,
        conteudo_sessao,
        DATA_INCLUSAO
      )
      VALUES
      (
        pCODIGO_SESSAO,
        pCONTEUDO_SESSAO,
        sysdate
      );
    IF retornarRegistro = 'S' THEN
      PR_SESSAO_B( pCODIGO_SESSAO => pCODIGO_SESSAO, retcur => retcur);
    END IF;
  END PR_SESSAO_S;
  PROCEDURE PR_SESSAO_B
    (
      pCODIGO_SESSAO VARCHAR2,
      retcur OUT TP_REFCURSOR
    )
  AS
  BEGIN
    OPEN retcur FOR SELECT * FROM TB_SESSAO WHERE codigo_sessao = pCODIGO_SESSAO;
  END PR_SESSAO_B;
  PROCEDURE PR_SESSAO_R
    (
      pCODIGO_SESSAO VARCHAR2
    )
  AS
  BEGIN
    DELETE FROM TB_SESSAO WHERE codigo_sessao = pCODIGO_SESSAO;
  END PR_SESSAO_R;
  PROCEDURE PR_SESSAO_L(
      pDATA_INCLUSAO VARCHAR2,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    OPEN retcur FOR SELECT * FROM TB_SESSAO WHERE (pDATA_INCLUSAO IS NULL OR DATA_INCLUSAO = pDATA_INCLUSAO);
  END PR_SESSAO_L;
  PROCEDURE PR_PROCESSO_ENVIADO_MATERA(
      pcodigo_processo NUMBER,
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR SELECT A.codigo_processo,
    b.status_pagamento FROM TB_TRANSACAO A INNER JOIN TB_PAGAMENTO B ON A.CODIGO_PAGAMENTO = B.CODIGO_PAGAMENTO WHERE A.CODIGO_PROCESSO = pcodigo_processo AND b.status_pagamento = 2;
  END PR_PROCESSO_ENVIADO_MATERA;
  PROCEDURE PR_LISTA_ITEM_REFERENCIA_L(
      pCODIGO_LISTA_ITEM_REFERENCIA NUMBER   := NULL,
      pCODIGO_LISTA_ITEM_SITUACAO   VARCHAR2 := NULL,
      pCODIGO_LISTA_ITEM            NUMBER   := NULL,
      pNOME_REFERENCIA              VARCHAR2 := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(2000);
  BEGIN
    vQuery := '';
    IF pCODIGO_LISTA_ITEM_REFERENCIA IS NOT NULL THEN
      vQuery                         := vQuery || 'A.CODIGO_LISTA_ITEM_REFERENCIA = ''' || pCODIGO_LISTA_ITEM_REFERENCIA || ''' and ';
    END IF;
    IF pCODIGO_LISTA_ITEM_SITUACAO IS NOT NULL THEN
      vQuery                       := vQuery || 'A.CODIGO_LISTA_ITEM_SITUACAO = ''' || pCODIGO_LISTA_ITEM_SITUACAO || ''' and ';
    END IF;
    IF pCODIGO_LISTA_ITEM IS NOT NULL THEN
      vQuery              := vQuery || 'A.CODIGO_LISTA_ITEM = ''' || pCODIGO_LISTA_ITEM || ''' and ';
    END IF;
    IF pNOME_REFERENCIA IS NOT NULL THEN
      vQuery            := vQuery || 'A.NOME_REFERENCIA = ''' || pNOME_REFERENCIA || ''' and ';
    END IF;
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    vQuery := '  
select     
A.CODIGO_LISTA_ITEM_REFERENCIA  
, A.CODIGO_LISTA_ITEM  
, A.CODIGO_LISTA_ITEM_SITUACAO  
, A.NOME_REFERENCIA  
, A.VALOR_REFERENCIA  
,     case A.NOME_REFERENCIA        
when ''MeioPagamento'' then C.CODIGO_LISTA_ITEM        
when ''Produto'' then D.CODIGO_PRODUTO        
end as     
CODIGO_VALOR_REFERENCIA   
from     
TB_LISTA_ITEM_REFERENCIA A  
left join     
TB_LISTA        B on B.NOME_LISTA = A.NOME_REFERENCIA  
left join     
TB_LISTA_ITEM   C on C.CODIGO_LISTA = B.CODIGO_LISTA and C.VALOR = A.VALOR_REFERENCIA  
left join     
TB_PRODUTO      D on D.NOME_PRODUTO = A.VALOR_REFERENCIA   
' || vQuery || '  
ORDER BY    
A.CODIGO_LISTA_ITEM_SITUACAO  
, A.NOME_REFERENCIA  
';
    OPEN retcur FOR vQuery;
  END PR_LISTA_ITEM_REFERENCIA_L;
  PROCEDURE PR_LISTA_ITEM_REFERENCIA_B(
      pCODIGO_LISTA_ITEM_REFERENCIA NUMBER,
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR SELECT A.CODIGO_LISTA_ITEM_REFERENCIA ,
    A.CODIGO_LISTA_ITEM ,
    A.CODIGO_LISTA_ITEM_SITUACAO ,
    A.NOME_REFERENCIA ,
    A.VALOR_REFERENCIA ,
    CASE A.NOME_REFERENCIA
    WHEN 'MeioPagamento' THEN
      C.CODIGO_LISTA_ITEM
    WHEN 'Produto' THEN
      D.CODIGO_PRODUTO
    END
  AS
    CODIGO_VALOR_REFERENCIA FROM TB_LISTA_ITEM_REFERENCIA A left join TB_LISTA B ON B.NOME_LISTA = A.NOME_REFERENCIA left join TB_LISTA_ITEM C ON C.CODIGO_LISTA = B.CODIGO_LISTA AND C.VALOR = A.VALOR_REFERENCIA left join TB_PRODUTO D ON D.NOME_PRODUTO = A.VALOR_REFERENCIA WHERE CODIGO_LISTA_ITEM_REFERENCIA = pCODIGO_LISTA_ITEM_REFERENCIA;
  END PR_LISTA_ITEM_REFERENCIA_B;
  PROCEDURE PR_LISTA_ITEM_REFERENCIA_S(
      pCODIGO_LISTA_ITEM_REFERENCIA NUMBER := NULL,
      pCODIGO_LISTA_ITEM            NUMBER,
      pCODIGO_LISTA_ITEM_SITUACAO   VARCHAR2,
      pNOME_REFERENCIA              VARCHAR2,
      pVALOR_REFERENCIA             VARCHAR2,
      retornarRegistro              CHAR := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_LISTA_ITEM_REFERENCIA NUMBER;
    vCODIGO_LISTA                 NUMBER;
  BEGIN
    vCODIGO_LISTA_ITEM_REFERENCIA := pCODIGO_LISTA_ITEM_REFERENCIA;
    SELECT CODIGO_LISTA
    INTO vCODIGO_LISTA
    FROM TB_LISTA
    WHERE MNEMONICO_LISTA = pCODIGO_LISTA_ITEM_SITUACAO;
    IF vCODIGO_LISTA_ITEM_REFERENCIA IS NULL THEN
      SELECT SQ_LISTA_ITEM_REFERENCIA.nextval
      INTO vCODIGO_LISTA_ITEM_REFERENCIA
      FROM dual;
      INSERT
      INTO TB_LISTA_ITEM_REFERENCIA
        (
          CODIGO_LISTA_ITEM_REFERENCIA,
          CODIGO_LISTA_ITEM,
          CODIGO_LISTA_ITEM_SITUACAO,
          NOME_REFERENCIA,
          VALOR_REFERENCIA,
          CODIGO_LISTA
        )
        VALUES
        (
          vCODIGO_LISTA_ITEM_REFERENCIA,
          pCODIGO_LISTA_ITEM,
          pCODIGO_LISTA_ITEM_SITUACAO,
          pNOME_REFERENCIA,
          pVALOR_REFERENCIA,
          vCODIGO_LISTA
        );
    ELSE
      UPDATE TB_LISTA_ITEM_REFERENCIA
      SET CODIGO_LISTA_ITEM              = pCODIGO_LISTA_ITEM,
        CODIGO_LISTA_ITEM_SITUACAO       = pCODIGO_LISTA_ITEM_SITUACAO,
        NOME_REFERENCIA                  = pNOME_REFERENCIA,
        VALOR_REFERENCIA                 = pVALOR_REFERENCIA,
        CODIGO_LISTA                     = vCODIGO_LISTA
      WHERE CODIGO_LISTA_ITEM_REFERENCIA = vCODIGO_LISTA_ITEM_REFERENCIA;
    END IF;
    IF retornarRegistro = 'S' THEN
      PR_LISTA_ITEM_REFERENCIA_B( pCODIGO_LISTA_ITEM_REFERENCIA => vCODIGO_LISTA_ITEM_REFERENCIA, retcur => retcur);
    END IF;
  END PR_LISTA_ITEM_REFERENCIA_S;
  PROCEDURE PR_LISTA_ITEM_REFERENCIA_R(
      pCODIGO_LISTA_ITEM_REFERENCIA NUMBER )
  AS
  BEGIN
    DELETE
    FROM TB_LISTA_ITEM_REFERENCIA
    WHERE CODIGO_LISTA_ITEM_REFERENCIA = pCODIGO_LISTA_ITEM_REFERENCIA;
  END PR_LISTA_ITEM_REFERENCIA_R;
  PROCEDURE PR_LISTA_ITEM_BIN_REFERENCIA_L(
      retcur OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR SELECT li.valor BIN,
    lir.valor_referencia Referencia FROM tb_lista_item li left join tb_lista_item_referencia lir ON li.codigo_lista_item = lir.codigo_lista_item WHERE li.codigo_lista =
    (SELECT codigo_lista FROM tb_lista tl WHERE tl.nome_lista = 'BIN'
    );
  END PR_LISTA_ITEM_BIN_REFERENCIA_L;
  PROCEDURE PR_REL_TRANS_CONSOLIDADO_L1(
      pTIPO_CONSOLIDADO        CHAR     := NULL,
      pSUB_TOTAL_FAVORECIDO    CHAR     := NULL,
      pCODIGO_PRODUTO          VARCHAR2 := NULL,
      pFAVORECIDO_ORIGINAL     VARCHAR2 := NULL,
      pCODIGO_TIPO_REGISTRO    VARCHAR2 := NULL,
      pCODIGO_ESTABELECIMENTO  NUMBER   := NULL,
      pCODIGO_MEIO_PAGAMENTO   VARCHAR2 := NULL,
      pLAYOUT_VENDA            CHAR     := NULL,
      pLAYOUT_RECEBIMENTO      CHAR     := NULL,
      pQTDE_TRANS_VENDA        CHAR     := NULL,
      pQTDE_PAG_RECEBIMENTO    CHAR     := NULL,
      pDATA_INICIO_AGENDAMENTO DATE     := NULL,
      pDATA_FIM_AGENDAMENTO    DATE     := NULL,
      pDATA_INICIO_MOVIMENTO   DATE     := NULL,
      pDATA_FIM_MOVIMENTO      DATE     := NULL,
      pSTATUS_RETORNO_CESSAO   CHAR     := NULL,
      pTIPO_FINANCEIRO_CONTABIL CHAR     := NULL,
      pREFERENCIA               VARCHAR2 := NULL, 
      pCONSOLIDA_REFERENCIA     CHAR     := 'N',  
      retcur OUT TP_REFCURSOR )
  AS
    vQuery01    VARCHAR2(32000);
    vQuery02    VARCHAR2(32000);
    vCondicao   VARCHAR2(4000);
    vGroupBy    VARCHAR2(4000); 
    vOrderBy    VARCHAR2(4000); 
    formatoData VARCHAR(10);    
    vCODIGO_LISTA NUMBER; 
    vMeioPagto    VARCHAR(1000) :='';
  BEGIN
    vQuery01    := NULL;
    vQuery02    := NULL;
    vCondicao   := NULL;
    vGroupBy    := NULL;
    vOrderBy    := NULL;
    formatoData := 'yyyy-mm-dd'; 
    SELECT CODIGO_LISTA
    INTO vCODIGO_LISTA
    FROM TB_LISTA
    WHERE NOME_LISTA = 'Referencia';
    IF pTIPO_FINANCEIRO_CONTABIL IS NOT NULL THEN 
      IF VCondicao               IS NOT NULL THEN 
        VCondicao                := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
nvl(A.TIPO_FINANCEIRO_CONTABIL,''' || pTIPO_FINANCEIRO_CONTABIL || ''') = ''' || pTIPO_FINANCEIRO_CONTABIL || '''  ';
    END IF;
    IF pDATA_INICIO_AGENDAMENTO IS NOT NULL THEN 
      IF VCondicao              IS NOT NULL THEN 
        VCondicao               := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
A.DATA_REPASSE_CALCULADA >= ''' || pDATA_INICIO_AGENDAMENTO || ''' '; 
    END IF;
    IF pDATA_FIM_AGENDAMENTO IS NOT NULL THEN 
      IF VCondicao           IS NOT NULL THEN 
        VCondicao            := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
A.DATA_REPASSE_CALCULADA <= ''' || pDATA_FIM_AGENDAMENTO || ''' '; 
    END IF;
    IF pDATA_INICIO_MOVIMENTO IS NOT NULL THEN 
      IF VCondicao            IS NOT NULL THEN 
        VCondicao             := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
A.DATA_MOVIMENTO >= ''' || pDATA_INICIO_MOVIMENTO || ''' ';
    END IF;
    IF pDATA_FIM_MOVIMENTO IS NOT NULL THEN 
      IF VCondicao         IS NOT NULL THEN 
        VCondicao          := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao ||      '   
A.DATA_MOVIMENTO <= to_date(''' || TO_CHAR(pDATA_FIM_MOVIMENTO, formatoData || ' hh24:mi:ss') || ''',''' || formatoData || ' hh24:mi:ss'') '; 
    END IF;
    IF pSTATUS_RETORNO_CESSAO IS NOT NULL THEN 
      IF VCondicao            IS NOT NULL THEN 
        VCondicao             := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
A.STATUS_TRANSACAO in (' || pSTATUS_RETORNO_CESSAO || ')';
    END IF;
    IF pFAVORECIDO_ORIGINAL IS NOT NULL THEN 
      IF VCondicao          IS NOT NULL THEN 
        VCondicao           := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
E.CODIGO_FAVORECIDO in (' || pFAVORECIDO_ORIGINAL || ')    ';
    END IF;
    IF pCODIGO_ESTABELECIMENTO IS NOT NULL THEN 
      IF VCondicao             IS NOT NULL THEN 
        VCondicao              := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
C.CODIGO_ESTABELECIMENTO = ''' || pCODIGO_ESTABELECIMENTO || ''' ';
    END IF;
    IF pCODIGO_PRODUTO IS NOT NULL THEN 
      IF VCondicao     IS NOT NULL THEN 
        VCondicao      := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
A.CODIGO_PRODUTO in (' || pCODIGO_PRODUTO || ')   ';
    ELSE
      IF pTIPO_CONSOLIDADO = 'V' THEN      
        IF VCondicao      IS NOT NULL THEN 
          VCondicao       := vCondicao || '  
and ';
        END IF;
        vCondicao := vCondicao || '   
D.NOME_PRODUTO <> ''RECTO''   '; 
      ELSE                       
        IF VCondicao IS NOT NULL THEN 
          VCondicao  := vCondicao || '  
and ';
        END IF;
        vCondicao := vCondicao || '   
D.NOME_PRODUTO = ''RECTO''     '; 
      END IF;
    END IF;
    IF pCODIGO_TIPO_REGISTRO IS NOT NULL THEN 
      IF VCondicao           IS NOT NULL THEN 
        VCondicao            := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
A.TIPO_TRANSACAO in (''' || pCODIGO_TIPO_REGISTRO || ''') ';
    ELSE
      IF pTIPO_CONSOLIDADO = 'V' THEN      
        IF VCondicao      IS NOT NULL THEN 
          VCondicao       := vCondicao || '  
and ';
        END IF;
        vCondicao := vCondicao || '   
A.TIPO_TRANSACAO in (''CV'', ''AV'', ''AJ'') '; 
      ELSE                                      
        IF VCondicao IS NOT NULL THEN           
          VCondicao  := vCondicao || '  
and ';
        END IF;
        vCondicao := vCondicao || '   
A.TIPO_TRANSACAO in (''CP'', ''AP'', ''AJ'') '; 
      END IF;
    END IF;
    IF pCODIGO_MEIO_PAGAMENTO IS NOT NULL THEN 
      IF VCondicao            IS NOT NULL THEN 
        VCondicao             := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '   
(   
TP.MEIO_PAGAMENTO       in (' || pCODIGO_MEIO_PAGAMENTO || ') or   
TAP.FORMA_MEIO_PAGAMENTO   in (' || pCODIGO_MEIO_PAGAMENTO || ')   
)   
';
    END IF;
    IF pREFERENCIA IS NOT NULL THEN
      IF VCondicao IS NOT NULL THEN
        VCondicao  := vCondicao || '  
and ';
      END IF;
      vCondicao := vCondicao || '  A.REFERENCIA in (' || pREFERENCIA || ')';
    END IF;
    vCondicao := '   
WHERE   
' || vCondicao;
    IF pTIPO_CONSOLIDADO = 'V' THEN 
      IF pLAYOUT_VENDA = 'A' OR pLAYOUT_VENDA = 'S' THEN
        IF vQuery01   IS NOT NULL THEN
          vQUERY01    := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  DATA_MOVIMENTO   
';
      END IF;
      IF pSUB_TOTAL_FAVORECIDO = 'S' THEN
        IF vQuery01           IS NOT NULL THEN
          vQUERY01            := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  CNPJ   
,  NOME_FAVORECIDO   
';
      END IF;
      IF pLAYOUT_VENDA <> 'S' THEN
        IF vQuery01    IS NOT NULL THEN
          vQUERY01     := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  NOME_PRODUTO   
';
      END IF;
      IF pQTDE_TRANS_VENDA = 'S' THEN
        IF vQuery01       IS NOT NULL THEN
          vQUERY01        := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  SUM(QTDE_CV) AS QTDE_CV   
,  SUM(QTDE_AV) AS QTDE_AV   
,  SUM(QTDE_AJ) AS QTDE_AJ  ';
      END IF;
      IF pCONSOLIDA_REFERENCIA = 'S' THEN
        IF vQuery01           IS NOT NULL THEN
          vQUERY01            := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  REFERENCIA   
,  NOME_LISTA_ITEM   
';
      END IF;
      vQuery01 := '   
SELECT  
SUM(VALOR_BRUTO_CV)     AS VALOR_BRUTO_CV   
,  SUM(VALOR_BRUTO_AV)     AS VALOR_BRUTO_AV   
,  SUM(VALOR_BRUTO_AJ)     AS VALOR_BRUTO_AJ   
,  SUM(VALOR_COMISSAO_CV)     AS VALOR_COMISSAO_CV   
,  SUM(VALOR_COMISSAO_AV)     AS VALOR_COMISSAO_AV   
,  SUM(VALOR_COMISSAO_AJ)     AS VALOR_COMISSAO_AJ   
,  SUM(TOTAL_LIQUIDO)       AS TOTAL_LIQUIDO   
, ' || vQuery01 || '   
FROM   
(   
';
      IF pLAYOUT_VENDA = 'A' OR pLAYOUT_VENDA = 'S' THEN
        IF vQuery02   IS NOT NULL THEN
          vQUERY02    := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '  trunc(A.DATA_MOVIMENTO) as   DATA_MOVIMENTO   
';
      END IF;
      IF pSUB_TOTAL_FAVORECIDO = 'S' THEN
        IF vQuery02           IS NOT NULL THEN
          vQUERY02            := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '  E.CNPJ_FAVORECIDO     AS   CNPJ   
,  E.NOME_FAVORECIDO     AS  NOME_FAVORECIDO   
';
      END IF;
      IF pCONSOLIDA_REFERENCIA = 'S' THEN
        IF vQuery02           IS NOT NULL THEN
          vQUERY02            := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '  A.REFERENCIA AS REFERENCIA   
,  TLI.NOME_LISTA_ITEM AS NOME_LISTA_ITEM   
';
      END IF;
      IF pLAYOUT_VENDA <> 'S' THEN
        IF vQuery02    IS NOT NULL THEN
          vQUERY02     := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '  D.NOME_PRODUTO      AS  NOME_PRODUTO   
';
      END IF;
      IF pQTDE_TRANS_VENDA = 'S' THEN
        IF vQuery02       IS NOT NULL THEN
          vQUERY02        := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '   
case  when (A.TIPO_TRANSACAO = ''CV'')   
then  (    
CASE WHEN TV.NUMERO_PARCELA IN (0, 1)    
THEN 1    
ELSE 0 END    
)   
else 0    end as QTDE_CV,   

case  when (A.TIPO_TRANSACAO = ''AV'')   
then  (    
CASE WHEN TAV.NUMERO_PARCELA IN (0, 1)    
THEN 1    
ELSE 0 END    
)   
else 0    end as QTDE_AV,   

case  when (A.TIPO_TRANSACAO = ''AJ'')   
then   1   
else   0  end as QTDE_AJ   
';
      END IF;
      vQuery02 :=
      '   
SELECT   

case  when (A.TIPO_TRANSACAO = ''CV'')   
then  
(  
CASE WHEN (TV.VALOR_PARCELA = 0 OR TV.VALOR_PARCELA IS NULL)  
THEN TV.VALOR_VENDA  
ELSE TV.VALOR_PARCELA END  
)   
else 0  end       as VALOR_BRUTO_CV,   

case  when (A.TIPO_TRANSACAO = ''AV'')   
then  
(  
CASE WHEN (TAV.VALOR_PARCELA = 0 or TAV.VALOR_PARCELA IS NULL)  
THEN TAV.VALOR_ANULACAO  
ELSE TAV.VALOR_PARCELA END  
)   
else 0  end       as VALOR_BRUTO_AV,   

case  when (A.TIPO_TRANSACAO = ''AJ'')   
then TAJ.VALOR_AJUSTE   
else 0  end       as VALOR_BRUTO_AJ,   

case  when (A.TIPO_TRANSACAO = ''CV'')   
then  
(  
CASE WHEN (TV.VALOR_PARCELA = 0 OR TV.VALOR_PARCELA IS NULL)  
THEN TV.VALOR_DESCONTO  
ELSE TV.VALOR_PARCELA_DESCONTO END  
)   
else 0   
end           as VALOR_COMISSAO_CV,   

case  when (A.TIPO_TRANSACAO = ''AJ'')   
then  
(  
CASE WHEN TAJ.VALOR_DESCONTO <> 0  
THEN TAJ.VALOR_DESCONTO  
END  
)   
else 0  end       as VALOR_COMISSAO_AJ,   

case  when (A.TIPO_TRANSACAO = ''AV'')   
then  
(  
CASE WHEN (TAV.VALOR_PARCELA = 0 or TAV.VALOR_PARCELA IS NULL)  
THEN TAV.VALOR_DESCONTO  
ELSE TAV.VALOR_PARCELA_DESCONTO END  
)   
else 0   
end           as VALOR_COMISSAO_AV,   

NVL  (  
CASE WHEN (TV.VALOR_PARCELA = 0 OR TV.VALOR_PARCELA IS NULL)  
THEN TV.VALOR_VENDA  
ELSE TV.VALOR_PARCELA END, 0  
)   
+   
NVL  (  
CASE WHEN (TAV.VALOR_PARCELA = 0 or TAV.VALOR_PARCELA IS NULL)  
THEN TAV.VALOR_ANULACAO  
ELSE TAV.VALOR_PARCELA END, 0  
)   
+   
NVL  (TAJ.VALOR_AJUSTE, 0)   
+   
NVL  (  
CASE WHEN (TV.VALOR_PARCELA = 0 OR TV.VALOR_PARCELA IS NULL)  
THEN TV.VALOR_DESCONTO  
ELSE TV.VALOR_PARCELA_DESCONTO END, 0  
)   
+   
NVL  (  
CASE WHEN (TAV.VALOR_PARCELA = 0 or TAV.VALOR_PARCELA IS NULL)  
THEN TAV.VALOR_DESCONTO  
ELSE TAV.VALOR_PARCELA_DESCONTO END, 0  
)   
+   
NVL  (TAJ.VALOR_DESCONTO, 0)      
as TOTAL_LIQUIDO  ,   
'
      || vQuery02 ; 
      vQuery02 := vQuery02 || '   
from     TB_TRANSACAO A   
left join   TB_TRANSACAO_VENDA         TV  on A.CODIGO_TRANSACAO   = TV.CODIGO_TRANSACAO   
left join   TB_TRANSACAO_ANULACAO_VENDA   TAV  on A.CODIGO_TRANSACAO   = TAV.CODIGO_TRANSACAO   
left join   TB_TRANSACAO_AJUSTE       TAJ  on A.CODIGO_TRANSACAO   = TAJ.CODIGO_TRANSACAO   
left join   TB_PRODUTO             D  on D.CODIGO_PRODUTO   = A.CODIGO_PRODUTO   
left join   TB_LISTA_ITEM TLI               on A.REFERENCIA = TLI.VALOR and TLI.CODIGO_LISTA = ''' || vCODIGO_LISTA || '''   
';
      IF pCODIGO_ESTABELECIMENTO IS NOT NULL OR pFAVORECIDO_ORIGINAL IS NOT NULL OR pSUB_TOTAL_FAVORECIDO = 'S' THEN
        vQuery02                 := vQuery02 || '   
left join   TB_ESTABELECIMENTO         C  on C.CODIGO_ESTABELECIMENTO = A.CODIGO_ESTABELECIMENTO   
left join   TB_ESTABELECIMENTO_FAVORECIDO   G  on G.CODIGO_ESTABELECIMENTO = A.CODIGO_ESTABELECIMENTO   
left join   TB_FAVORECIDO           E  on E.CODIGO_FAVORECIDO     = G.CODIGO_FAVORECIDO   ';
      END IF;
      IF pLAYOUT_VENDA = 'A' OR pLAYOUT_VENDA = 'S' THEN
        IF vGroupBy   IS NOT NULL THEN
          vGroupBy    := vGroupBy || ', ';
        END IF;
        vGroupBy := vGroupBy || '   DATA_MOVIMENTO   
';
      END IF;
      IF pSUB_TOTAL_FAVORECIDO = 'S' THEN
        IF vGroupBy           IS NOT NULL THEN
          vGroupBy            := vGroupBy || ', ';
        END IF;
        vGroupBy := vGroupBy || '  CNPJ   
,  NOME_FAVORECIDO   
';
      END IF;
      IF pLAYOUT_VENDA <> 'S' THEN
        IF vGroupBy    IS NOT NULL THEN
          vGroupBy     := vGroupBy || ', ';
        END IF;
        vGroupBy := vGroupBy || '  NOME_PRODUTO   
';
      END IF;
      IF pCONSOLIDA_REFERENCIA = 'S' THEN
        IF vGroupBy           IS NOT NULL THEN
          vGroupBy            := vGroupBy || ', ';
        END IF;
        vGroupBy := vGroupBy || '  REFERENCIA   
,  NOME_LISTA_ITEM   
';
      END IF;
      IF pSUB_TOTAL_FAVORECIDO = 'S' THEN
        IF vOrderBy           IS NOT NULL THEN
          vOrderBy            := vOrderBy || ', ';
        END IF;
        vOrderBy := vOrderBy || '  NOME_FAVORECIDO   
';
      END IF;
      IF pCONSOLIDA_REFERENCIA = 'S' THEN
        IF vOrderBy           IS NOT NULL THEN
          vOrderBy            := vOrderBy || ', ';
        END IF;
        vOrderBy := vOrderBy || '  REFERENCIA   
,  NOME_LISTA_ITEM   
';
      END IF;
      IF pLAYOUT_VENDA = 'A' OR pLAYOUT_VENDA = 'S' THEN
        IF vOrderBy   IS NOT NULL THEN
          vOrderBy    := vOrderBy || ', ';
        END IF;
        vOrderBy := vOrderBy || '  DATA_MOVIMENTO   
';
      END IF;
      IF pLAYOUT_VENDA <> 'S' THEN
        IF vOrderBy    IS NOT NULL THEN
          vOrderBy     := vOrderBy || ', ';
        END IF;
        vOrderBy := vOrderBy || '  NOME_PRODUTO   
';
      END IF;
      IF vGroupBy IS NOT NULL THEN
        vGroupBy  := '   
GROUP BY   
' || vGroupBy;
      END IF;
    ELSE
      IF pQTDE_PAG_RECEBIMENTO = 'S' THEN
        IF vQuery01           IS NOT NULL THEN
          vQUERY01            := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  SUM(QTDE_CP) AS QTDE_CP   
,  SUM(QTDE_AP) AS QTDE_AP   
,  SUM(QTDE_AJ) AS QTDE_AJ   
';
      END IF;
      IF pLAYOUT_RECEBIMENTO = 'T' OR pLAYOUT_RECEBIMENTO = 'A' THEN
        IF vQuery01         IS NOT NULL THEN
          vQUERY01          := vQuery01 || ', ';
        END IF;
        vQuery01   := vQuery01 || '  M.MEIO_PAGAMENTO   
';
        vMeioPagto := 'rel   
LEFT JOIN (SELECT VALOR, VALOR || '' - '' || NOME_LISTA_ITEM MEIO_PAGAMENTO FROM TB_LISTA_ITEM WHERE CODIGO_LISTA = 9) M    
ON M.VALOR = REL.MEIO_PAGAMENTO';
      END IF;
      IF pSUB_TOTAL_FAVORECIDO = 'S' THEN
        IF vQuery01           IS NOT NULL THEN
          vQUERY01            := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  CNPJ   
,  NOME_FAVORECIDO   
';
      END IF;
      IF pLAYOUT_RECEBIMENTO = 'A' OR pLAYOUT_RECEBIMENTO = 'S' THEN
        IF vQuery01         IS NOT NULL THEN
          vQUERY01          := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  DATA_MOVIMENTO   
';
      END IF;
      IF pCONSOLIDA_REFERENCIA = 'S' THEN
        IF vQuery01           IS NOT NULL THEN
          vQUERY01            := vQuery01 || ', ';
        END IF;
        vQuery01 := vQuery01 || '  REFERENCIA    
, NOME_LISTA_ITEM   
';
      END IF;
      vQuery01 := '   
SELECT  
SUM(VALOR_CP)           AS VALOR_CP   
,  SUM(VALOR_AP)           AS VALOR_AP   
,  SUM(VALOR_AJ)           AS VALOR_AJ   
,  SUM(VALOR_TOTAL)         AS VALOR_TOTAL   
, ' || vQuery01 || '   
FROM   
(   
';
      IF pLAYOUT_RECEBIMENTO = 'A' OR pLAYOUT_RECEBIMENTO = 'S' THEN
        IF vQuery02         IS NOT NULL THEN
          vQUERY02          := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '  trunc(A.DATA_MOVIMENTO) as   DATA_MOVIMENTO   ';
      END IF;
      IF pSUB_TOTAL_FAVORECIDO = 'S' THEN
        IF vQuery02           IS NOT NULL THEN
          vQUERY02            := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '  E.CNPJ_FAVORECIDO           AS CNPJ   
,  E.NOME_FAVORECIDO   
';
      END IF;
      IF pCONSOLIDA_REFERENCIA = 'S' THEN
        IF vQuery02           IS NOT NULL THEN
          vQUERY02            := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '  A.REFERENCIA           AS REFERENCIA   
,  TLI.NOME_LISTA_ITEM  AS NOME_LISTA_ITEM   
';
      END IF;
      IF pLAYOUT_RECEBIMENTO = 'T' OR pLAYOUT_RECEBIMENTO = 'A' THEN
        IF vQuery02         IS NOT NULL THEN
          vQUERY02          := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '   
case  when (A.TIPO_TRANSACAO = ''CP'')   
then TP.MEIO_PAGAMENTO   
when (A.TIPO_TRANSACAO = ''AP'')   
then TAP.FORMA_MEIO_PAGAMENTO   
when (A.TIPO_TRANSACAO = ''AJ'')   
then ''1''   
else ''''  end             as MEIO_PAGAMENTO   
';
      END IF;
      IF pQTDE_PAG_RECEBIMENTO = 'S' THEN
        IF vQuery02           IS NOT NULL THEN
          vQUERY02            := vQuery02 || ', ';
        END IF;
        vQuery02 := vQuery02 || '   
case    when (A.TIPO_TRANSACAO = ''CP'')   
then 1   
else 0    end           as QTDE_CP   
,   
case    when (A.TIPO_TRANSACAO = ''AP'')   
then 1   
else 0    end           as QTDE_AP   
,   
case    when (A.TIPO_TRANSACAO = ''AJ'')   
then 1   
else 0    end           as QTDE_AJ   
';
      END IF;
      --=============================================================================================================
      --- QUERY SECUNDARIA 02
      --=============================================================================================================
      vQuery02 := '   
SELECT   

case  when (A.TIPO_TRANSACAO = ''CP'')   
then TP.MEIO_PAGAMENTO_VALOR   
else 0  end             as VALOR_CP   

,   
case  when (A.TIPO_TRANSACAO = ''AP'')   
then TAP.VALOR_PAGAMENTO   
else 0  end             as VALOR_AP   

,   
case  when (A.TIPO_TRANSACAO = ''AJ'')   
then TAJ.VALOR_AJUSTE   
else 0  end             as VALOR_AJ   

,   
NVL(TP.MEIO_PAGAMENTO_VALOR, 0) +   
NVL(TAP.VALOR_PAGAMENTO, 0) +   
NVL(TAJ.VALOR_AJUSTE, 0) as VALOR_TOTAL   

,   
' || vQuery02 ;
      --=============================================================================================================
      --- LAYOUT RELATORIO RECEBIMENTO - A ( analitico por data ) ou S ( sintetico por data )
      --=============================================================================================================
      vQuery02 := vQuery02 || '   
FROM   
TB_TRANSACAO           A   
left join   TB_TRANSACAO_PAGAMENTO       TP  on    TP.CODIGO_TRANSACAO     = A.CODIGO_TRANSACAO   
left join   TB_TRANSACAO_ANULACAO_PAGTO   TAP  on     TAP.CODIGO_TRANSACAO     = A.CODIGO_TRANSACAO   
left join   TB_TRANSACAO_AJUSTE   TAJ  on     TAJ.CODIGO_TRANSACAO     = A.CODIGO_TRANSACAO   
left join   TB_PRODUTO             D  on     D.CODIGO_PRODUTO       = A.CODIGO_PRODUTO   
left join   TB_LISTA_ITEM TLI               on A.REFERENCIA = TLI.VALOR and TLI.CODIGO_LISTA = ''' || vCODIGO_LISTA || '''   
';
      --=============================================================================================================
      --- LAYOUT RELATORIO RECEBIMENTO - A ( analitico por data ) ou S ( sintetico por data )
      --=============================================================================================================
      IF pCODIGO_ESTABELECIMENTO IS NOT NULL OR pFAVORECIDO_ORIGINAL IS NOT NULL OR pSUB_TOTAL_FAVORECIDO = 'S' THEN
        vQuery02                 := vQuery02 || '   
left join   TB_ESTABELECIMENTO         C  on     C.CODIGO_ESTABELECIMENTO   = A.CODIGO_ESTABELECIMENTO   
left join   TB_ESTABELECIMENTO_FAVORECIDO   G  on     G.CODIGO_ESTABELECIMENTO   = A.CODIGO_ESTABELECIMENTO   
left join   TB_FAVORECIDO           E  on     E.CODIGO_FAVORECIDO     = G.CODIGO_FAVORECIDO   
';
      END IF;
      --=============================================================================================================
      --- CRIANDO O AGRUPAMENTO ( Group By )
      --=============================================================================================================
      --- SUBTOTAL/QEUBRA FAVORECIDO
      --=============================================================================================================
      IF pSUB_TOTAL_FAVORECIDO = 'S' THEN
        IF vGroupBy           IS NOT NULL THEN
          vGroupBy            := vGroupBy || ', ';
        END IF;
        VGroupBy := VGroupBy || '  CNPJ   
,   NOME_FAVORECIDO   
';
      END IF;
      --=============================================================================================================
      --- CONSOLIDADO REFERENCIA
      -- ClockWork - Resource - ago/16 - Margarida Vitolo
      --=============================================================================================================
      IF pCONSOLIDA_REFERENCIA = 'S' THEN
        IF vGroupBy           IS NOT NULL THEN
          vGroupBy            := vGroupBy || ', ';
        END IF;
        VGroupBy := VGroupBy || '  REFERENCIA   
,   NOME_LISTA_ITEM   
';
      END IF;
      --=============================================================================================================
      --- LAYOUT RELATORIO RECEBIMENTO - T ( totalizado por meio de pagamento ) ou A ( analitico por data )
      --=============================================================================================================
      IF pLAYOUT_RECEBIMENTO = 'T' OR pLAYOUT_RECEBIMENTO = 'A' THEN
        IF vGroupBy         IS NOT NULL THEN
          vGroupBy          := vGroupBy || ', ';
        END IF;
        VGroupBy := VGroupBy || '  M.MEIO_PAGAMENTO   
';
      END IF;
      --=============================================================================================================
      --- LAYOUT RELATORIO RECEBIMENTO - A ( analitico por data ) OU S ( sintetico por data )
      --=============================================================================================================
      IF pLAYOUT_RECEBIMENTO = 'A' OR pLAYOUT_RECEBIMENTO = 'S' THEN
        IF vGroupBy         IS NOT NULL THEN
          vGroupBy          := vGroupBy || ', ';
        END IF;
        VGroupBy := VGroupBy || '  DATA_MOVIMENTO   
';
      END IF;
      --=============================================================================================================
      --- FINALIZA O GROUP BY RECEBIMENTO
      --=============================================================================================================
      IF vGroupBy IS NOT NULL THEN
        vGroupBy  := '   
GROUP BY   
' || vGroupBy;
      END IF;
      --=============================================================================================================
      --- INICIA O ORDER BY
      --=============================================================================================================
      --- SUBTOTAL/QEUBRA FAVORECIDO
      --=============================================================================================================
      IF pSUB_TOTAL_FAVORECIDO = 'S' THEN
        IF vOrderBy           IS NOT NULL THEN
          vOrderBy            := vOrderBy || ', ';
        END IF;
        vOrderBy := vOrderBy || '  NOME_FAVORECIDO   
';
      END IF;
      --=============================================================================================================
      --- CONSOLIDA REFERENCIA
      -- ClockWork - Resource - ago/16 - Margarida Vitolo
      --=============================================================================================================
      IF pCONSOLIDA_REFERENCIA = 'S' THEN
        IF vOrderBy           IS NOT NULL THEN
          vOrderBy            := vOrderBy || ', ';
        END IF;
        vOrderBy := vOrderBy || '  REFERENCIA   
, NOME_LISTA_ITEM   
';
      END IF;
      --=============================================================================================================
      --- LAYOUT RELATORIO RECEBIMENTO - A ( analitico por data ) OU S ( sintetico por data )
      --=============================================================================================================
      IF pLAYOUT_RECEBIMENTO = 'A' OR pLAYOUT_RECEBIMENTO = 'S' THEN
        IF vOrderBy         IS NOT NULL THEN
          vOrderBy          := vOrderBy || ', ';
        END IF;
        vOrderBy := vOrderBy || '  DATA_MOVIMENTO   
';
      END IF;
      --=============================================================================================================
      --- LAYOUT RELATORIO RECEBIMENTO - T ( totalizado por meio de pagamento ) ou A ( analitico por data )
      --=============================================================================================================
      IF pLAYOUT_RECEBIMENTO = 'T' OR pLAYOUT_RECEBIMENTO = 'A' THEN
        IF vOrderBy         IS NOT NULL THEN
          vOrderBy          := vOrderBy || ', ';
        END IF;
        vOrderBy := vOrderBy || '  M.MEIO_PAGAMENTO   
';
      END IF;
    END IF;
    --PONTO04
    --=============================================================================================================
    --- FINALIZA O ORDER BY
    --=============================================================================================================
    IF vOrderBy IS NOT NULL THEN
      vOrderBy  := '   
ORDER BY   
' || vOrderBy;
    END IF;
    --=============================================================================================================
    --- FINALIZANDO A QUERY
    --=============================================================================================================
    vQuery01 := vQuery01 || vQuery02 || vCondicao || '   
)   
' || vMeioPagto || VGroupBy || VOrderBy;
    PR_LOG_DB( -- SCF-1017
    PDESCRICAO_LOG => vQuery01 || ';', PTIPO_ORIGEM => 'PR_REL_TRANS_CONSOLIDADO_L1', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => NULL );
    --=============================================================================================================
    --- EXECUTA A QUERY
    --=============================================================================================================
    OPEN retcur FOR vQuery01;
  END PR_REL_TRANS_CONSOLIDADO_L1;
--=============================================================================
  PROCEDURE PR_REL_SALDO_CONCILIACAO_L(
      pFILTRO_DATA_CON_INICIO DATE,
      pFILTRO_DATA_CON_FIM    DATE,
      retcur OUT TP_REFCURSOR,
      retcurSaldo OUT TP_REFCURSOR)
  AS
    vQuery VARCHAR2(4000);
  BEGIN
    --==================================================================================================================
    -- Monta a query dinamica
    --==================================================================================================================
    vQuery := 'A.STATUS_TRANSACAO <> 6 and ';
    --==================================================================================================================
    -- Filtros
    --==================================================================================================================
    IF pFILTRO_DATA_CON_INICIO IS NOT NULL THEN
      vQuery                   := vQuery || 'a.DATA_TRANSACAO >= ''' || pFILTRO_DATA_CON_INICIO || ''' and '; --SCF1175 SUBSTITUICAO DA DATA DE CONCILIACAO
    END IF;
    IF pFILTRO_DATA_CON_FIM IS NOT NULL THEN
      vQuery                := vQuery || 'a.DATA_TRANSACAO <= ''' || TRUNC(pFILTRO_DATA_CON_FIM+1) || ''' and '; --SCF1175 SUBSTITUICAO DA DATA DE CONCILIACAO
    END IF;
    --==================================================================================================================
    -- Caso exista algum filtro, remove o ultimo 'and' e adiciona a palavra 'where' no inicio
    --==================================================================================================================
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    --==================================================================================================================
    -- Adiciona o select
    --==================================================================================================================
    --scf1175 alterado de when a.STATUS_CONCILIACAO = ''1'' then D.VALOR_VENDA para when a.STATUS_CONCILIACAO = ''1'' then 0
    --scf1175 alterado de when a.status_conciliacao = ''1'' and b.TIPO_ARQUIVO =''ArquivoSitef'' then d.valor_venda * -1 para when a.status_conciliacao = ''1'' then 0
    vQuery := '  
SELECT  
a.status_conciliacao,  
case  
when    a.STATUS_CONCILIACAO = ''1''      then 0  
when    a.status_conciliacao = ''0''      then d.valor_venda * -1  
end     as valor_conciliacao,  
case  
when    b.TIPO_ARQUIVO =''ArquivoCSU''    then ''CSF - CSU''  
when    b.TIPO_ARQUIVO =''ArquivoTSYS''   then ''CSF - TSYS''  
end     as SITEF_REDE,  
c.tipo_processo,  
a.DATA_TRANSACAO,  
a.data_conciliacao,  
a.nsu_host,  
d.codigo_autorizacao,  
d.valor_venda,  
a.codigo_processo    

FROM        tb_transacao        A    

INNER JOIN  TB_arquivo          B   ON  B.CODIGO_ARQUIVO    = A.CODIGO_ARQUIVO  
INNER JOIN  TB_Processo         C   ON  C.codigo_processo   = B.codigo_processo     AND c.tipo_processo = ''ProcessoAtacadaoInfo''  
INNER JOIN  tb_transacao_venda  D   ON  D.codigo_transacao  = A.codigo_transacao  
' || vQuery ||
    '  
union  
SELECT  
a.status_conciliacao,  
case  
when    a.STATUS_CONCILIACAO = ''1''      then 0  
when    a.status_conciliacao = ''0''      then d.valor_venda * -1  
end     as valor_conciliacao,  
case  
when    b.TIPO_ARQUIVO =''ArquivoSitef''    then ''Sitef''  
end     as SITEF_REDE,  
c.tipo_processo,  
a.DATA_TRANSACAO,  
a.data_conciliacao,  
a.nsu_host,  
d.codigo_autorizacao,  
d.valor_venda,  
a.codigo_processo    

FROM        tb_transacao        A    

INNER JOIN  TB_arquivo          B   ON  B.CODIGO_ARQUIVO    = A.CODIGO_ARQUIVO2  
INNER JOIN  TB_Processo         C   ON  C.codigo_processo   = B.codigo_processo     AND c.tipo_processo = ''ProcessoAtacadaoInfo''  
INNER JOIN  tb_transacao_venda  D   ON  D.codigo_transacao  = A.codigo_transacao  
' || vQuery || '  
order by DATA_CONCILIACAO, DATA_TRANSACAO, NSU_HOST, CODIGO_AUTORIZACAO';
    PR_LOG_DB( -- SCF-1175
    PDESCRICAO_LOG => vQuery || ';', PTIPO_ORIGEM => 'PR_REL_SALDO_CONCILIACAO_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => NULL );
    --==================================================================================================================
    -- Efetua a busca
    --==================================================================================================================
    OPEN retcur FOR vQuery;
    --==================================================================================================================
    -- Busca o saldo consolidado
    --==================================================================================================================
    PR_SALDO_CONSOLIDADO_B ( PDATASALDO => TRUNC(pFILTRO_DATA_CON_INICIO), RETCUR => retcurSaldo );
  END PR_REL_SALDO_CONCILIACAO_L;
--=============================================================================
  PROCEDURE PR_SALDO_CONSOLIDADO_B(
      PDATASALDO DATE,
      RETCUR OUT TP_REFCURSOR )
  AS
    vSaldo  NUMBER;
    VNUMROW NUMBER;
  BEGIN
    vSaldo  := 0;
    VNUMROW := 0;
    --==================================================================================================================
    -- Confere se tem saldo anterior a data inicial
    --==================================================================================================================
    -- 1175
    --      select    COUNT(VALOR_SALDO)
    --      into      vSaldo
    --      from      TB_SALDO_CONCILIADO
    --      where     DATA_SALDO < PDATASALDO and ROWNUM = 1
    --      ORDER BY  DATA_SALDO DESC;
    SELECT SUM(VALOR_SALDO)
    INTO vSaldo
    FROM TB_SALDO_CONCILIADO
    WHERE DATA_SALDO < PDATASALDO-- and ROWNUM = 1
    ORDER BY DATA_SALDO DESC;
    --==================================================================================================================
    -- Se Nao achou, CARREGA SALDO
    --==================================================================================================================
    --      if(vSaldo = 0) then
    --        PR_SALDO_CONSOLIDADO_S(PDATACALCULOSALDO => PDATASALDO);
    --      end if;
    IF vSaldo = 0 THEN --1175
      SELECT NVL(SUM(VLR), 0) AS SLD_VLR
      INTO vSaldo
      FROM
        (SELECT b.valor_venda * -1 AS VLR
        FROM tb_transacao a
        INNER JOIN tb_transacao_venda b
        ON a.codigo_transacao = b.codigo_transacao
        INNER JOIN TB_Processo c
        ON A.codigo_processo       = c.codigo_processo
        AND c.tipo_processo        = 'ProcessoAtacadaoInfo'
        WHERE a.STATUS_CONCILIACAO = 0
        AND DATA_transacao         < PDATASALDO --1175
        );
    END IF;
    --==================================================================================================================
    -- Carrega o valor do saldo anterior
    --==================================================================================================================
    OPEN RETCUR FOR SELECT NVL(vSaldo, 0)
  AS
    SALDO FROM DUAL;
  END PR_SALDO_CONSOLIDADO_B;
--=============================================================================
  PROCEDURE PR_SALDO_CONSOLIDADO_S(
      PDATACALCULOSALDO DATE )
  AS
    VSALDO  NUMBER;
    vNumRow NUMBER;
  BEGIN
    --==================================================================================================================
    --- CONFERE O SALDO NA DATA DO PARAMETRO
    --==================================================================================================================
    SELECT NVL(SUM(VLR), 0) AS SLD_VLR
    INTO VSALDO
    FROM
      (SELECT b.valor_venda * -1 AS VLR
      FROM tb_transacao a
      INNER JOIN tb_transacao_venda b
      ON a.codigo_transacao = b.codigo_transacao
      INNER JOIN TB_Processo c
      ON A.codigo_processo       = c.codigo_processo
      AND c.tipo_processo        = 'ProcessoAtacadaoInfo'
      WHERE a.STATUS_CONCILIACAO = 0
      AND TRUNC(DATA_TRANSACAO)  = TRUNC(PDATACALCULOSALDO) --1175
      );
    --==================================================================================================================
    --- SE TIVER SALDO, GRAVAR
    --==================================================================================================================
    IF VSALDO <> 0 THEN
      vNumRow := 0;
      SELECT COUNT(VALOR_SALDO)
      INTO vNumRow
      FROM TB_SALDO_CONCILIADO
      WHERE TRUNC(DATA_SALDO) = TRUNC(PDATACALCULOSALDO);
      --==================================================================================================================
      -- SE NAO TEM SALDO HOJE, GERA O SALDO
      --==================================================================================================================
      IF vNumRow = 0 THEN
        INSERT
        INTO TB_SALDO_CONCILIADO
          (
            VALOR_SALDO,
            DATA_SALDO
          )
          VALUES
          (
            VSALDO,
            TRUNC(PDATACALCULOSALDO)
          );
      ELSE
        UPDATE TB_SALDO_CONCILIADO
        SET VALOR_SALDO  = VSALDO
        WHERE DATA_SALDO = PDATACALCULOSALDO;
      END IF;
    END IF;
  END PR_SALDO_CONSOLIDADO_S;
--=============================================================================
  PROCEDURE PR_VALIDAR_CESSAO_B(
      PCODIGO_ARQUIVO             NUMBER,
      PCODIGO_PROCESSO            NUMBER,
      PBANCO                      VARCHAR2,
      PAGENCIA                    VARCHAR2,
      PCONTA                      VARCHAR2,
      PCNPJ_FAVORECIDO            VARCHAR2,
      PNSU_HOST                   VARCHAR2,
      PDATA_TRANSACAO_FORMATADA   DATE,
      PCNPJ_ESTABELECIMENTO       VARCHAR2,
      PCOD_AUTORIZACAO            VARCHAR2,
      PNUMERO_PARCELA             VARCHAR2,
      PREPROCESSAR_NSU_CESSIONADO CHAR, -- FLAG (S/N) do parametro "Cessionar / Atualizar NSU Host jaï¿½ cessionado?" da tela "Configuracoes > Paraï¿½metros de Sistema"
      PRET_CESSAO_ANT OUT NUMBER,       -- ENVIAR o codigo do processo se a transacao ja foi cessionada
      RETCUR OUT TP_REFCURSOR          --- Enviar as crï¿½ticas de cessao para esta transacao
    )
  AS
    CURSORTRANSACOES TP_REFCURSOR;
    VCODIGO_FAVORECIDO       NUMBER;
    VCODIGO_CONTA_FAVORECIDO NUMBER;
    VCODIGO_ESTABELECIMENTO  NUMBER;
    vCODIGO_TRANSACAO        NUMBER;
    vLEU_CURSOR              CHAR;
    vCEDIVEL                 CHAR(1);
    vBANCO                   CHAR(3);
    vRET_CESSAO_ANT          NUMBER;
    VCHAVE3                  VARCHAR2(100);
    vVALIDAR_CESSAO_CHAR string(20);
    vAGENCIA        VARCHAR2(20);
    vCONTA          VARCHAR2(20);
    vCHAVE_ITEM     VARCHAR2(200);
    vCODIGO_PRODUTO NUMBER;
    vNOME_PRODUTO   CHAR(6);
    vREFERENCIA     CHAR(3);
    vACHOUPRODUTO   CHAR(1);
    vSTATUS_TRANSACAO TB_TRANSACAO.STATUS_TRANSACAO%TYPE;
  BEGIN
    -- ====================================================================================================================================
    -- Inicializar variavies
    -- ====================================================================================================================================
    VCODIGO_FAVORECIDO       := 0;
    VCODIGO_CONTA_FAVORECIDO := 0;
    VCODIGO_ESTABELECIMENTO  := 0;
    vCODIGO_TRANSACAO        := 0;
    vLEU_CURSOR              := 'N';
    vCEDIVEL                 := '';
    vBANCO                   := '';
    vRET_CESSAO_ANT          := 0;
    VCHAVE3                  := '';
    vVALIDAR_CESSAO_CHAR     := NULL;
    vAGENCIA                 := '';
    vCONTA                   := '';
    vCHAVE_ITEM              := '';
    vCODIGO_PRODUTO          := 0;  
    vNOME_PRODUTO            := ''; 
    vREFERENCIA              := ''; 
    vACHOUPRODUTO            := 'N';
    -- ====================================================================================================================================
    -- recupera o codigo do estabelecimento para compor a chave ---5-ESTABELECIMENTO nao CADASTRADO
    -- ====================================================================================================================================
    BEGIN
      SELECT CODIGO_ESTABELECIMENTO
      INTO VCODIGO_ESTABELECIMENTO
      FROM TB_ESTABELECIMENTO
      WHERE CNPJ = PCNPJ_ESTABELECIMENTO;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      VCODIGO_ESTABELECIMENTO := 0;
      vVALIDAR_CESSAO_CHAR    := NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'') || '5';
    END;
    -- ====================================================================================================================================
    -- recupera o codigo do favorecido ---6-FAVORECIDO nao CADASTRADO
    -- ====================================================================================================================================
    BEGIN
      SELECT CODIGO_FAVORECIDO
      INTO VCODIGO_FAVORECIDO
      FROM TB_FAVORECIDO
      WHERE CNPJ_FAVORECIDO = PCNPJ_FAVORECIDO;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      VCODIGO_FAVORECIDO   := 0;
      vVALIDAR_CESSAO_CHAR := NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'') || '6';
    END;
    -- ====================================================================================================================================
    -- recupera a conta do favorecido ---7-CONTA PARA O BANCO ESPECIFICO DO FAVORECIDO nao CADASTRADO
    -- ====================================================================================================================================
    BEGIN
      SELECT CODIGO_CONTA_FAVORECIDO
      INTO VCODIGO_CONTA_FAVORECIDO
      FROM TB_CONTA_FAVORECIDO
      WHERE CONTA_CCI       = PCONTA
      AND AGENCIA_CCI       = PAGENCIA
      AND BANCO             = PBANCO
      AND CODIGO_FAVORECIDO = VCODIGO_FAVORECIDO;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      VCODIGO_CONTA_FAVORECIDO := 0;
      vVALIDAR_CESSAO_CHAR     := NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'') || '7';
    END;
    -- ====================================================================================================================================
    -- Gera a CHAVE3 ( que por onde ele vai buscar a transacao )
    -- ====================================================================================================================================
    VCHAVE3 := TO_CHAR(PDATA_TRANSACAO_FORMATADA, 'yyyymmdd') || -- Tam 08 DATA_TRANSACAO
    '000000' ||                                                  -- Tam 06 HORA_TRANSACAO
    LPAD(TO_CHAR(VCODIGO_ESTABELECIMENTO), 6, '0') ||            -- Tam 06 CODIGO_ESTABELECIMENTO
    'CV' ||                                                      -- Tam 02 TIPO_REGISTRO
    LPAD(PNSU_HOST, 12, '0') ||                                  -- Tam 12 NSU_HOST
    LPAD(PCOD_AUTORIZACAO, 12, '0') ||                           -- Tam 12 CODIGO_AUTORIZACAO
    LPAD(PNUMERO_PARCELA, 4, '0');                               -- Tam 04 NUMERO_PARCELA
    -- ====================================================================================================================================
    -- SELECIONANDO AS TRANSACOES QUE SERAO CESSIONADAS, CRITERIO PELA CHAVE3 QUE NAO CONSIDERA A HORA
    -- ====================================================================================================================================
    OPEN CURSORTRANSACOES FOR 
	SELECT A.CODIGO_TRANSACAO, C.CEDIVEL, NVL(A.CODIGO_PROCESSO_RET_CESSAO,0), A.CODIGO_PRODUTO, C.CODIGO_PRODUTO_TSYS, A.REFERENCIA, a.STATUS_TRANSACAO 
    FROM TB_TRANSACAO A 
	left join TB_PRODUTO C ON A.CODIGO_PRODUTO = C.CODIGO_PRODUTO 
	WHERE CHAVE3 = VCHAVE3;
    -- ====================================================================================================================================
    -- LENDO TRANSACAO
    -- ====================================================================================================================================
    LOOP
      FETCH CURSORTRANSACOES
      INTO vCODIGO_TRANSACAO ,
        vCEDIVEL ,
        vRET_CESSAO_ANT ,
        vCODIGO_PRODUTO        ,
        vNOME_PRODUTO         ,
        vREFERENCIA         ,
        vSTATUS_TRANSACAO;
      EXIT
    WHEN CURSORTRANSACOES%NOTFOUND;
      vLEU_CURSOR := 'S';
      --===================================================================================================================
      -- se a transacao estiver cessionada e o parametro Nao permite atualizacao ---2-Transacao cessionada
      --===================================================================================================================
      IF PREPROCESSAR_NSU_CESSIONADO = 'N' AND -- CONFIGURADO PARA NAO CESSIONAR TRANSACAO CESSIONADA
        vRET_CESSAO_ANT             <> 0       -- PROCESSO DE RETORNO DE CESSAO ANTERIOR
        THEN
        vVALIDAR_CESSAO_CHAR := NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'') || '2';
      END IF;
      --===================================================================================================================
      -- se nao o produto for cedivel, marcar como PRODUTO Nao CEDIVEL ---4-Produto nao cessionado
      --===================================================================================================================
      IF vCEDIVEL != 'S' -- PRODUTO CESSIONADO nao e cedivel
        THEN
        vVALIDAR_CESSAO_CHAR := NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'') || '4';
      END IF;
      vACHOUPRODUTO := 'S';
      /*BEGIN
        SELECT 'S' ACHOUPRODUTO
        INTO vACHOUPRODUTO
        FROM TB_CONTA_FAVORECIDO_PRODUTO
        WHERE CODIGO_PRODUTO        = vCODIGO_PRODUTO
        AND CODIGO_CONTA_FAVORECIDO = VCODIGO_CONTA_FAVORECIDO
        AND REFERENCIA              = vREFERENCIA
        AND ROWNUM                  = 1;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        vVALIDAR_CESSAO_CHAR := NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'') || '8';
      END; */
      --===================================================================================================================
      -- SCF1701 ID 29 - Valida Status da Transacao ---9-Transacao invalida para Cessionar
      --===================================================================================================================
      IF vSTATUS_TRANSACAO    > '1' THEN
        vVALIDAR_CESSAO_CHAR := NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'') || '9';
      END IF;
      --===================================================================================================================
      -- 1328 ENVIA O CODIGO DA TRANSACAO PARA O .NET
      --===================================================================================================================
      --===================================================================================================================
      -- GRAVA RETORNO DE CESSAO SE TUDO ESTIVER OK
      --===================================================================================================================
      IF vVALIDAR_CESSAO_CHAR IS NULL THEN
        vCHAVE_ITEM           := SUBSTR(TO_CHAR(vCODIGO_TRANSACAO +100000000000000000000),2,20) || SUBSTR(TO_CHAR(VCODIGO_FAVORECIDO +100000000000000000000),2,20) || SUBSTR(TO_CHAR(VCODIGO_CONTA_FAVORECIDO +100000000000000000000),2,20);
      END IF;
    END LOOP;
    CLOSE CURSORTRANSACOES;
    -- ====================================================================================================================================
    -- Transacao inexistente  ---3-Transacao Inexistente
    -- ====================================================================================================================================
    IF vLEU_CURSOR         <> 'S' THEN
      vVALIDAR_CESSAO_CHAR := NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'') || '3';
    END IF;
    -- ====================================================================================================================================
    -- DEVOLVE O RETORNO PARA O PROGRAMA CHAMADOR
    -- ====================================================================================================================================
    pRET_CESSAO_ANT := vRET_CESSAO_ANT;
    OPEN retcur FOR 
	SELECT NVL(RTRIM(vVALIDAR_CESSAO_CHAR),'')  AS    RetValidacao,
    vCHAVE_ITEM  AS    ChaveItem,
    NVL(TO_CHAR(vCODIGO_PRODUTO), '0')  AS    CodigoProduto,
    vNOME_PRODUTO  AS    NomeProduto,
    vREFERENCIA  AS    Referencia,
    vSTATUS_TRANSACAO  AS    Status FROM DUAL;
  END PR_VALIDAR_CESSAO_B;
--=============================================================================
  PROCEDURE PR_CESSIONA_S(
      pCODIGO_TRANSACAO           NUMBER := NULL ,
      pCODIGO_ARQUIVO_RET_CESSAO  NUMBER := NULL ,
      pCODIGO_PROCESSO_RET_CESSAO NUMBER := NULL ,
      pCODIGO_FAVORECIDO          NUMBER := NULL ,
      pCODIGO_CONTA_FAVORECIDO    NUMBER := NULL )
  AS
    vCODIGO_FAVORECIDO_ORIGINAL   NUMBER;
    vCODIGO_CONTA_FAVORECIDO_ORIG NUMBER;
    vDATA_REPASSE_ORIGINAL        DATE;
    vSTATUS_TRANSACAO_ORIGINAL    CHAR(1);
    vCODIGO_ARQUIVO               NUMBER;
    vANTECIPACAO                  CHAR(1);
    vANTECIPACAOARQ               CHAR(1);
    vFAVORECIDO_ORIGINAL          CHAR(1);
  BEGIN
    vCODIGO_FAVORECIDO_ORIGINAL   := 0;
    vCODIGO_CONTA_FAVORECIDO_ORIG := 0;
    vDATA_REPASSE_ORIGINAL        := NULL;
    vSTATUS_TRANSACAO_ORIGINAL    := '';
    vCODIGO_ARQUIVO               := 0;
    vANTECIPACAO                  := 'N';
    vANTECIPACAOARQ               := 'N';
    vFAVORECIDO_ORIGINAL          := '0';
    SELECT NVL(B.FAVORECIDO_ORIGINAL,'0')
    INTO vFAVORECIDO_ORIGINAL
    FROM TB_FAVORECIDO B
    WHERE B.CODIGO_FAVORECIDO = pCODIGO_FAVORECIDO;
    IF (vFAVORECIDO_ORIGINAL  = '1') THEN
      vANTECIPACAO           := 'S';
      SELECT A.CODIGO_FAVORECIDO_ORIGINAL ,
        A.CODIGO_CONTA_FAVORECIDO_ORIG ,
        A.DATA_REPASSE_CALCULADA ,
        A.STATUS_TRANSACAO
      INTO vCODIGO_FAVORECIDO_ORIGINAL ,
        vCODIGO_CONTA_FAVORECIDO_ORIG ,
        vDATA_REPASSE_ORIGINAL ,
        vSTATUS_TRANSACAO_ORIGINAL
      FROM TB_TRANSACAO A
      INNER JOIN TB_FAVORECIDO B
      ON A.CODIGO_FAVORECIDO_ORIGINAL = B.CODIGO_FAVORECIDO
      WHERE CODIGO_TRANSACAO          = pCODIGO_TRANSACAO;
      UPDATE TB_TRANSACAO
      SET STATUS_RET_CESSAO        = 1 ,
        CODIGO_FAVORECIDO          = NVL(pCODIGO_FAVORECIDO ,CODIGO_FAVORECIDO) ,
        CODIGO_CONTA_FAVORECIDO    = NVL(pCODIGO_CONTA_FAVORECIDO ,CODIGO_CONTA_FAVORECIDO) ,
        CODIGO_PROCESSO_RET_CESSAO = NVL(pCODIGO_PROCESSO_RET_CESSAO ,CODIGO_PROCESSO_RET_CESSAO) ,
        CODIGO_ARQUIVO_RET_CESSAO  = NVL(pCODIGO_ARQUIVO_RET_CESSAO ,CODIGO_ARQUIVO_RET_CESSAO) ,
        DATA_POSICAO_CARTEIRA      = TO_DATE(SYSDATE) ,
        STATUS_TRANSACAO_ORIGINAL  = vSTATUS_TRANSACAO_ORIGINAL ,
        DATA_REPASSE_ORIGINAL      = vDATA_REPASSE_ORIGINAL ,
        ANTECIPACAO                = vANTECIPACAO ,
        STATUS_TRANSACAO           = 2 ,
        DATA_REPASSE_CALCULADA     = TO_DATE(SYSDATE-1)
      WHERE CODIGO_TRANSACAO       = pCODIGO_TRANSACAO;
    ELSE
      --==============================================================================================================
      -- ATUALIZA CESSAO
      --==============================================================================================================
      UPDATE TB_TRANSACAO
      SET STATUS_RET_CESSAO        = 1 ,
        CODIGO_FAVORECIDO          = NVL(pCODIGO_FAVORECIDO ,CODIGO_FAVORECIDO) ,
        CODIGO_CONTA_FAVORECIDO    = NVL(pCODIGO_CONTA_FAVORECIDO ,CODIGO_CONTA_FAVORECIDO) ,
        CODIGO_PROCESSO_RET_CESSAO = NVL(pCODIGO_PROCESSO_RET_CESSAO ,CODIGO_PROCESSO_RET_CESSAO) ,
        CODIGO_ARQUIVO_RET_CESSAO  = NVL(pCODIGO_ARQUIVO_RET_CESSAO ,CODIGO_ARQUIVO_RET_CESSAO) ,
        DATA_POSICAO_CARTEIRA      = TO_DATE(SYSDATE)
      WHERE CODIGO_TRANSACAO       = pCODIGO_TRANSACAO;
    END IF;
    --==============================================================================================================
    -- SE NAO ACHOU LOGA
    --==============================================================================================================
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    PR_LOG_DB( PDESCRICAO_LOG => 'CESSAO - ' || 'TB_TRANSACAO - Nao encontrou Transacao ' || pCODIGO_TRANSACAO || ' pCODIGO_ARQUIVO_RET_CESSAO ' || pCODIGO_ARQUIVO_RET_CESSAO || ' pCODIGO_PROCESSO_RET_CESSAO ' || pCODIGO_PROCESSO_RET_CESSAO || ' pCODIGO_FAVORECIDO ' || pCODIGO_FAVORECIDO || ' pCODIGO_CONTA_FAVORECIDO ' || pCODIGO_CONTA_FAVORECIDO, PTIPO_ORIGEM => 'PR_CESSIONA_S', PCODIGO_ORIGEM => pCODIGO_PROCESSO_RET_CESSAO, PDESCRICAOGRANDE_LOG => NULL );
  END PR_CESSIONA_S;
--=============================================================================
  PROCEDURE PR_CESSIONA_LOG_S(
      pCODIGO_PROCESSO_RET_CESSAO NUMBER := NULL )
  AS
    vCURSOR TP_REFCURSOR;
    vCODIGO_PROCESSO NUMBER;
  BEGIN
    --==============================================================================================================
    -- CARREGA OS PROCESSO DA TRANSACAO QUE FORAM CESSIONADOS POR ESTE PROCESSO
    --==============================================================================================================
    OPEN vCURSOR FOR
    SELECT DISTINCT CODIGO_PROCESSO
    FROM tb_transacao
    WHERE CODIGO_PROCESSO_RET_CESSAO = PCODIGO_PROCESSO_RET_CESSAO -- CONFERE OS PROCESSOS QUE FORAM CESSIONADOS
      ;
    --==============================================================================================================
    -- LOOP
    --==============================================================================================================
    LOOP
      FETCH vCURSOR INTO vCODIGO_PROCESSO;
      EXIT
    WHEN vCURSOR%NOTFOUND;
      --==============================================================================================================
      -- LOGA CESSAO e ATUALIZA O PROCESSO PARA STATUS CESSIONADO e TAMBï¿½M ATUALIZA O PROCESSO DE CESSAO
      --==============================================================================================================
      PR_LOG_DB( PDESCRICAO_LOG => 'CESSAO - Efetuada em transacoes do processo extrato numero: ' || vCODIGO_PROCESSO, PTIPO_ORIGEM => 'PR_CESSIONA_S', PCODIGO_ORIGEM => pCODIGO_PROCESSO_RET_CESSAO, PDESCRICAOGRANDE_LOG => NULL );
      PR_LOG_DB( PDESCRICAO_LOG => 'CESSAO - Efetuada em transacoes deste processo extrato. Numero do processo de cessao: ' || pCODIGO_PROCESSO_RET_CESSAO, PTIPO_ORIGEM => 'PR_CESSIONA_S', PCODIGO_ORIGEM => vCODIGO_PROCESSO, PDESCRICAOGRANDE_LOG => NULL );
      UPDATE TB_PROCESSO
      SET STATUS_PROCESSO   = 3
      WHERE CODIGO_PROCESSO = vCODIGO_PROCESSO;
    END LOOP;
    CLOSE vCURSOR;
    --==============================================================================================================
    -- FIM CURSOR
    --==============================================================================================================
  END PR_CESSIONA_LOG_S;
--=============================================================================
  PROCEDURE PR_PERMISSAO_GRUPO_PERFIL_L(
      pCODIGO_USUARIO VARCHAR2 := NULL,
      RETCUR OUT TP_REFCURSOR,
      RETCURCOUNT OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(3000);
  BEGIN
    -- Monta a query dinamica
    vQuery := '';
    -- Filtros
    IF pCODIGO_USUARIO IS NOT NULL THEN
      vQuery           := vQuery || 'a.codigo_usuario = ''' || pCODIGO_USUARIO || ''' and ';
    END IF;
    -- Caso exista algum filtro, remove o ultimo 'and' e adiciona a palavra 'where' no inicio
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    -- Adiciona o select
    vQuery := 'select  a.codigo_usuario,                
a.nome_usuario,                
permissao_grupo_perfil.codigo_permissao as permissao_grupo_perfil                
from    tb_usuario a                  
inner join tb_usuario_grupo_acesso_r c on                    
upper(c.codigo_usuario) = upper(a.codigo_usuario)                  
inner join tb_grupo_acesso_perfil_r d on                    
d.codigo_grupo_acesso = c.codigo_grupo_acesso                  
inner join tb_perfil_permissao_r permissao_grupo_perfil on                    
permissao_grupo_perfil.codigo_perfil = d.codigo_perfil ' || vQuery;
    -- Efetua a busca
    OPEN retcur FOR vQuery;
  END PR_PERMISSAO_GRUPO_PERFIL_L;
--=============================================================================
  PROCEDURE PR_CANCELAMENTO_ONLINE(
      pCODIGO_PROCESSO NUMBER := NULL,
      pCODIGO_ARQUIVO  NUMBER := NULL )
  AS
    vQUERY   VARCHAR2(2000);
    vQTD_REG NUMBER;
  BEGIN
    --=============================================================================
    -- recupera todos os arquivos itens do tipo CV
    --=============================================================================
    vQUERY := '         
insert into TEMP_ARQUIVO_ITEM_COMPROVANTE (CODIGO_ARQUIVO_ITEM, NSU_HOST, DATA_TRANSACAO)        
(select CODIGO_ARQUIVO_ITEM,                
TO_CHAR(SUBSTR(CONTEUDO_ARQUIVO_ITEM, 18,12)), --NSU                
TO_CHAR(SUBSTR(CONTEUDO_ARQUIVO_ITEM, 54, 8)) --DATA_TRANSACAO na posicao 54 (CV/CP)        
from    TB_ARQUIVO_ITEM        
where   CODIGO_ARQUIVO = ' || pCODIGO_ARQUIVO || '     and     STATUS_ARQUIVO_ITEM = ''1''        
and     TIPO_ARQUIVO_ITEM in (''CV'', ''CP'')        
and     TO_CHAR(SUBSTR(CONTEUDO_ARQUIVO_ITEM, 68, 1))  != ''3'')  
';
    EXECUTE IMMEDIATE vQUERY;
    vQTD_REG := sql%rowcount;
    --================================================================================================================
    --COMMITTEMPZERA LOG
    --================================================================================================================
    --PR_LOG_DB ( PDESCRICAO_LOG => 'CANCELAMENTO ONLINE - Qtd de registros inseridos COMPROVANTE [' || vQTD_REG || ']', PTIPO_ORIGEM => 'PR_CANCELAMENTO_ONLINE', PCODIGO_ORIGEM => pCODIGO_PROCESSO, PDESCRICAOGRANDE_LOG => NULL );
    --=============================================================================
    -- recupera todos os arquivos itens do tipo AV
    --=============================================================================
    vQUERY := '         
insert into TEMP_ARQUIVO_ITEM_ANULACAO (CODIGO_ARQUIVO_ITEM, NSU_HOST, DATA_TRANSACAO)        
(select CODIGO_ARQUIVO_ITEM,                
TO_CHAR(SUBSTR(CONTEUDO_ARQUIVO_ITEM, 18,12)), --NSU                
TO_CHAR(SUBSTR(CONTEUDO_ARQUIVO_ITEM, 74, 8)) --DATA_TRANSACAO na posicao 74 (AV/AP)        
from    TB_ARQUIVO_ITEM        
where   CODIGO_ARQUIVO = ' || pCODIGO_ARQUIVO || '     and     STATUS_ARQUIVO_ITEM = ''1''        
and     TIPO_ARQUIVO_ITEM in (''AV'', ''AP'')        
and     TO_CHAR(SUBSTR(CONTEUDO_ARQUIVO_ITEM, 88, 1))  != ''3'')  
';
    EXECUTE IMMEDIATE vQUERY;
    vQTD_REG := sql%rowcount;
    --================================================================================================================
    --COMMITTEMPZERA LOG
    --================================================================================================================
    --PR_LOG_DB ( PDESCRICAO_LOG => 'CANCELAMENTO ONLINE - Qtd de registros inseridos ANULACAO [' || vQTD_REG || ']', PTIPO_ORIGEM => 'PR_CANCELAMENTO_ONLINE', PCODIGO_ORIGEM => pCODIGO_PROCESSO, PDESCRICAOGRANDE_LOG => NULL );
    --================================================================================================================
    -- EFETUA A ALTERAï¿½ï¿½O NOS REGISTROS DA ARQUIVO ITEM PARA STATUS 6 - CANCELAMENTO ON LINE
    --================================================================================================================
    vQUERY := '  
update  TB_ARQUIVO_ITEM  
set     STATUS_ARQUIVO_ITEM = ''6''  
where   CODIGO_ARQUIVO_ITEM in   
(    
select distinct CODIGO_ARQUIVO_ITEM    
FROM     
(        
select A.CODIGO_ARQUIVO_ITEM as CAIc,               
B.CODIGO_ARQUIVO_ITEM as CAIa        
from   TEMP_ARQUIVO_ITEM_COMPROVANTE A               
inner join TEMP_ARQUIVO_ITEM_ANULACAO B on  A.NSU_HOST = B.NSU_HOST and A.DATA_TRANSACAO = B.DATA_TRANSACAO    
)    
unpivot (CODIGO_ARQUIVO_ITEM for COLUNA in (CAIc, CAIa))  
)  
';
    EXECUTE IMMEDIATE vQUERY;
    vQTD_REG := sql%rowcount;
    --================================================================================================================
    -- LOG
    --================================================================================================================
    PR_LOG_DB ( PDESCRICAO_LOG => 'CANCELAMENTO ONLINE - Qtd de registros atualizados ARQUIVO ITEM [' || vQTD_REG || ']', PTIPO_ORIGEM => 'PR_CANCELAMENTO_ONLINE', PCODIGO_ORIGEM => pCODIGO_PROCESSO, PDESCRICAOGRANDE_LOG => NULL );
  END PR_CANCELAMENTO_ONLINE;
--=============================================================================
  PROCEDURE PR_VERIFICA_FAVORECIDO_BANCO(
      pCNPJ_FAVORECIDO string := NULL ,
      pBANCO string           := NULL ,
      pAGENCIA string         := NULL ,
      pCONTA string           := NULL ,
      pPRODUTO string         := NULL ,
      pREFERENCIA string      := NULL ------1339
      ,
      RETCUR OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN RETCUR FOR SELECT COUNT(*)
  AS
    contagem FROM tb_favorecido A inner join tb_conta_favorecido B ON A.CODIGO_FAVORECIDO = B.CODIGO_FAVORECIDO inner join tb_conta_favorecido_produto C ON C.CODIGO_CONTA_FAVORECIDO = B.CODIGO_CONTA_FAVORECIDO WHERE A.CNPJ_FAVORECIDO = pCNPJ_FAVORECIDO AND C.CODIGO_PRODUTO = pPRODUTO AND C.REFERENCIA = pREFERENCIA AND B.BANCO = pBANCO AND B.AGENCIA_CCI = pAGENCIA AND B.CONTA_CCI = pCONTA;
  END PR_VERIFICA_FAVORECIDO_BANCO;
--=============================================================================
  PROCEDURE PR_LISTA_CONF_L(
      pCODIGO_LISTA string         := NULL, -- ecommerce
      pCODIGO_EMPRESA_GRUPO NUMBER := NULL, -- MELHORIAS-2017
      RETCUR OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(3000);
  BEGIN
    IF pCODIGO_LISTA IS NOT NULL THEN
      vQuery         := vQuery || 'C.CODIGO_LISTA = ''' || pCODIGO_LISTA || ''' and ';
    END IF;
    -- MELHORIAS-2017
    IF pCODIGO_EMPRESA_GRUPO IS NOT NULL THEN
      vQuery                 := vQuery || 'A.CODIGO_EMPRESA_GRUPO = ' || pCODIGO_EMPRESA_GRUPO || ' and ';
    END IF;
    -- Caso exista algum filtro, remove o ultimo 'and' e adiciona a palavra 'where' no inicio
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    -- Adiciona o select
    vQuery := 'select  distinct C.CODIGO_LISTA,                                  
C.NOME_LISTA,                                  
C.MNEMONICO_LISTA,                                  
C.LISTA_CONF_HEADER,                                  
A.CODIGO_EMPRESA_GRUPO                 
from    tb_lista_conf A                          
inner join tb_lista_item B on   A.CODIGO_LISTA_ITEM = B.CODIGO_LISTA_ITEM                          
inner join tb_lista C on C.CODIGO_LISTA = B.CODIGO_LISTA ' || vQuery;
    -- Efetua a busca
    OPEN retcur FOR vQuery;
  END PR_LISTA_CONF_L;
--=============================================================================
  PROCEDURE PR_LISTA_ITEM_CONF_L(
      pCODIGO_LISTA string         := NULL,
      pCODIGO_EMPRESA_GRUPO NUMBER := NULL, -- MELHORIAS-2017
      RETCUR OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(3000);
  BEGIN
    IF pCODIGO_LISTA IS NOT NULL THEN
      vQuery         := vQuery || 'C.CODIGO_LISTA = ''' || pCODIGO_LISTA || ''' and ';
    END IF;
    -- MELHORIAS-2017
    IF pCODIGO_EMPRESA_GRUPO IS NOT NULL THEN
      vQuery                 := vQuery || 'A.CODIGO_EMPRESA_GRUPO = ' || pCODIGO_EMPRESA_GRUPO || ' and ';
    END IF;
    -- Caso exista algum filtro, remove o ultimo 'and' e adiciona a palavra 'where' no inicio
    IF LENGTH(vQuery) > 0 THEN
      vQuery         := SUBSTR(vQuery, 0, LENGTH(vQuery) - 4);
      vQuery         := 'where ' || vQuery;
    END IF;
    -- Adiciona o select
    vQuery := '  
select      
A.CODIGO_LISTA_CONF  
, A.CODIGO_LISTA_ITEM  
, C.CODIGO_LISTA  
, B.NOME_LISTA_ITEM  
, C.NOME_LISTA  
, B.VALOR  
, A.CONF_LISTA  
, B.ATIVO  
, C.LISTA_CONF_HEADER  
, A.CODIGO_EMPRESA_GRUPO  
from          tb_lista_conf       A  
inner join    tb_lista_item       B   on  A.CODIGO_LISTA_ITEM   = B.CODIGO_LISTA_ITEM  
inner join    tb_lista            C   on  C.CODIGO_LISTA        = B.CODIGO_LISTA   
' || vQuery;
    -- Efetua a busca
    OPEN retcur FOR vQuery;
  END PR_LISTA_ITEM_CONF_L;
--=============================================================================
  PROCEDURE PR_LISTA_ITEM_CONF_S(
      pCODIGO_LISTA_CONF string    := NULL,
      pCONF_LISTA string           := NULL,
      pCODIGO_EMPRESA_GRUPO NUMBER := NULL )
  AS
  BEGIN
    -- Atualiza o registro
    UPDATE TB_LISTA_CONF
    SET CONF_LISTA          = NVL(pCONF_LISTA, CONF_LISTA),
      CODIGO_EMPRESA_GRUPO  = NVL(pCODIGO_EMPRESA_GRUPO, CODIGO_EMPRESA_GRUPO)
    WHERE CODIGO_LISTA_CONF = pCODIGO_LISTA_CONF;
  END PR_LISTA_ITEM_CONF_S;
--=============================================================================
  PROCEDURE PR_EXPURGO_ARQUIVO_ITEM(
      RETCUR OUT TP_REFCURSOR )
  AS
    vQTD NUMBER;
  BEGIN
    vQTd :=0;
  END PR_EXPURGO_ARQUIVO_ITEM;
--=============================================================================
  PROCEDURE PR_CAMPOS_ENVIO_CCI_S(
      pCODIGO_CAMPO_ENVIO_CCI NUMBER  := NULL,
      pNOME_CAMPO             VARCHAR := NULL,
      pSTATUS_ENVIO           CHAR    := NULL,
      retornarRegistro        CHAR    := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    qtde                    NUMBER;
    vCODIGO_CAMPO_ENVIO_CCI NUMBER;
  BEGIN
    vCODIGO_CAMPO_ENVIO_CCI := pCODIGO_CAMPO_ENVIO_CCI;
    -- Verifica se ja existe configuracoes para o objeto
    SELECT COUNT(*)
    INTO qtde
    FROM TB_CAMPOS_ENVIO_CCI
    WHERE CODIGO_CAMPO_ENVIO_CCI = vCODIGO_CAMPO_ENVIO_CCI;
    -- Verifica se e inclusao ou alteracao
    IF qtde = 0 THEN
      SELECT SQ_CAMPOS_ENVIO_CCI.nextval INTO vCODIGO_CAMPO_ENVIO_CCI FROM dual;
      -- Insere o registro
      INSERT
      INTO TB_CAMPOS_ENVIO_CCI
        (
          CODIGO_CAMPO_ENVIO_CCI,
          NOME_CAMPO,
          STATUS_ENVIO_CCI,
          DATA_ATUALIZACAO
        )
        VALUES
        (
          vCODIGO_CAMPO_ENVIO_CCI,
          pNOME_CAMPO,
          pSTATUS_ENVIO,
          SYSDATE
        );
    ELSE
      -- Atualiza o registro
      UPDATE TB_CAMPOS_ENVIO_CCI
      SET NOME_CAMPO               = NVL(pNOME_CAMPO, NOME_CAMPO),
        STATUS_ENVIO_CCI           = NVL(pSTATUS_ENVIO, STATUS_ENVIO_CCI),
        DATA_ATUALIZACAO           = SYSDATE
      WHERE CODIGO_CAMPO_ENVIO_CCI = vCODIGO_CAMPO_ENVIO_CCI;
    END IF;
    -- Verifica se deve retornar o registro
    IF retornarRegistro = 'S' THEN
      PR_CAMPOS_ENVIO_CCI_L( pCODIGO_CAMPO_ENVIO_CCI => vCODIGO_CAMPO_ENVIO_CCI, retcur => retcur);
    END IF;
  END PR_CAMPOS_ENVIO_CCI_S;
--=============================================================================
  PROCEDURE PR_CAMPOS_ENVIO_CCI_L(
      pCODIGO_CAMPO_ENVIO_CCI NUMBER  := NULL,
      pNOME_CAMPO             VARCHAR := NULL,
      pSTATUS_ENVIO           CHAR    := NULL,
      RETCUR OUT TP_REFCURSOR )
  AS
  BEGIN
    OPEN retcur FOR
    SELECT DISTINCT A.CODIGO_CAMPO_ENVIO_CCI,
      A.NOME_CAMPO,
      A.STATUS_ENVIO_CCI,
      A.DATA_ATUALIZACAO
    FROM TB_CAMPOS_ENVIO_CCI A
    WHERE (pCODIGO_CAMPO_ENVIO_CCI IS NULL
    OR A.CODIGO_CAMPO_ENVIO_CCI     = pCODIGO_CAMPO_ENVIO_CCI)
    AND (pNOME_CAMPO               IS NULL
    OR A.NOME_CAMPO                 = pNOME_CAMPO)
    AND (pSTATUS_ENVIO             IS NULL
    OR A.STATUS_ENVIO_CCI           = pSTATUS_ENVIO);
  END PR_CAMPOS_ENVIO_CCI_L;
--=============================================================================
  PROCEDURE PR_LOG_DB( -- SCF-1017
      PDESCRICAO_LOG VARCHAR2,
      PTIPO_ORIGEM   VARCHAR2,
      PCODIGO_ORIGEM NUMBER,
      PDESCRICAOGRANDE_LOG CLOB )
  AS
    vCODIGO_LOG    NUMBER;
    vCODIGO_ORIGEM NUMBER;
    vDESCRICAO_LOG VARCHAR2(4000);
    vTIPO_ORIGEM   VARCHAR2(200);
    vTIPO_LOG      VARCHAR2(200);
    vDESCRICAOGRANDE_LOG CLOB;
    vPOSICAO  NUMBER;
    vMAXIMO   NUMBER;
    vCONTINUA VARCHAR2(1);
    vSERIALIZACAO CLOB; --scf1324
  BEGIN
    vPOSICAO             := 1;
    vMAXIMO              := 4000;
    vCONTINUA            := 'S';
    vDESCRICAO_LOG       := SUBSTR(PDESCRICAO_LOG,1,4000);
    vCODIGO_ORIGEM       := pcodigo_origem; --scf1324 -- null; -- SCF1125
    vTIPO_LOG            := PTIPO_ORIGEM;
    vTIPO_ORIGEM         := 'ProcessoInfo';
    vDESCRICAOGRANDE_LOG := PDESCRICAOGRANDE_LOG;
    vSERIALIZACAO        := NULL;
    IF VDESCRICAO_LOG    IS NULL THEN
      WHILE vCONTINUA     = 'S'
      LOOP
        VDESCRICAO_LOG    := NULL;
        vDESCRICAO_LOG    := SUBSTR(VDESCRICAOGRANDE_LOG, vPOSICAO, vMAXIMO);
        IF vDESCRICAO_LOG IS NOT NULL THEN
          SELECT SQ_LOG.nextval INTO vCODIGO_LOG FROM dual;
          INSERT
          INTO tb_log
            (
              CODIGO_LOG,
              TIPO_LOG,
              DATA_LOG,
              DESCRICAO_LOG,
              TIPO_ORIGEM,
              CODIGO_ORIGEM,
              DATA_BD_LOG,
              SERIALIZACAO_LOG
            )
            VALUES
            (
              vCODIGO_LOG,
              vTIPO_LOG,
              sysdate(),
              vDESCRICAO_LOG,
              vTIPO_ORIGEM,
              vCODIGO_ORIGEM,
              sysdate(),
              vSERIALIZACAO
            );
        ELSE
          vCONTINUA := 'N';
        END IF;
        vPOSICAO := vPOSICAO + vMAXIMO;
      END LOOP;
    ELSE
      SELECT SQ_LOG.nextval INTO vCODIGO_LOG FROM dual;
      INSERT
      INTO tb_log
        (
          CODIGO_LOG,
          TIPO_LOG,
          DATA_LOG,
          DESCRICAO_LOG,
          TIPO_ORIGEM,
          CODIGO_ORIGEM,
          DATA_BD_LOG,
          SERIALIZACAO_LOG
        )
        VALUES
        (
          vCODIGO_LOG,
          vTIPO_LOG,
          sysdate(),
          vDESCRICAO_LOG,
          vTIPO_ORIGEM,
          vCODIGO_ORIGEM,
          sysdate(),
          vSERIALIZACAO
        );
    END IF;
    COMMIT;
  END PR_LOG_DB;
--=============================================================================
  PROCEDURE PR_LIST_CONF_S
    (
      pNOME_LISTA           VARCHAR2 , -- CONTERA O NOME DA LISTA QUE DEVERA SER CRIADO UMA CONFIGURACAO ESPECIFICA
      pLISTA_CONF_HEADER    VARCHAR2 , -- CONTERA A DESCRICAO DA CONFIGURACAO ESPECIFICA
      pVALOR_CONF           VARCHAR,   -- CONTERA O VALOR QUE SERA CRIADO O LISTA CONF
      pCODIGO_EMPRESA_GRUPO NUMBER
    )
  AS
    Lista_Item_Cursor TP_REFCURSOR;
    vCODIGO_LISTA_CONF    NUMBER;
    vQUERY_PRINCIPAL      VARCHAR2(2000);
    vQUERY_DEL            VARCHAR2(2000);
    vQUERY_UPD            VARCHAR2(2000);
    vQTD_REG              NUMBER;
    vCODIGO_LISTA_ITEM    NUMBER;
    vCODIGO_EMPRESA_GRUPO NUMBER;
  BEGIN
    --================================================================================================================
    -- Gerando o CURSOR que ira ler a LISTA ITEM pelo NOME LISTA vinda do parametro
    --================================================================================================================
    vQUERY_PRINCIPAL := ' SELECT      A.CODIGO_LISTA_ITEM      
FROM        TB_LISTA_ITEM A      
INNER JOIN  TB_LISTA      B   ON  A.CODIGO_LISTA = B.CODIGO_LISTA      
WHERE       B.NOME_LISTA = ''' || pNOME_LISTA || '''';
    --================================================================================================================
    -- Limpando a CONFIGURAAï¿½ao ESPECIFICA existente para a LISTA que sera gerada
    --================================================================================================================
    vQUERY_DEL := '    
DELETE from TB_LISTA_CONF    
WHERE    
codigo_lista_item in   ( ' || vQUERY_PRINCIPAL || ' )';
    EXECUTE IMMEDIATE vQUERY_DEL;
    vQTD_REG := sql%rowcount;
    --================================================================================================================
    -- Criando a CONFIGURAAï¿½ao ESPECIFICA para a LISTA
    --================================================================================================================
    vQUERY_UPD := '    
UPDATE    TB_LISTA    
SET       lista_conf_header   = ''' || pLISTA_CONF_HEADER || '''    
WHERE     nome_lista          = ''' || pNOME_LISTA || '''';
    EXECUTE IMMEDIATE vQUERY_UPD;
    vQTD_REG := sql%rowcount;
    --================================================================================================================
    --Criando registros de codigo de ajuste da tb_lista_item para tb_lista_conf
    --================================================================================================================
    OPEN Lista_Item_Cursor FOR vQuery_Principal;
    LOOP
      FETCH Lista_Item_Cursor INTO vCODIGO_LISTA_ITEM;
      EXIT
    WHEN Lista_Item_Cursor%NOTFOUND;
      --================================================================================================================
      --Carregando PK da LISTA CONF
      --================================================================================================================
      SELECT SQ_LISTA_CONF.nextval
      INTO vCODIGO_LISTA_CONF
      FROM DUAL;
      --================================================================================================================
      -- Incluindo LITA ITEM na LISTA CONF com VALOR do parametro
      --================================================================================================================
      INSERT
      INTO TB_LISTA_CONF VALUES
        (
          vCODIGO_LISTA_CONF,
          vCODIGO_LISTA_ITEM,
          pVALOR_CONF,
          pCODIGO_EMPRESA_GRUPO
        );
    END LOOP;
  END PR_LIST_CONF_S;
--=============================================================================
  PROCEDURE PR_TRANSACAO_FAVORECIDO_L
    (
      pDATA_INCLUSAO DATE, -- e a partir desta data de inclusao que a transacao poderaï¿½ ser atualizada
      pUPD           CHAR, -- informa se e para atualizar as transacoes
      pRETORNO OUT NUMBER  -- devolveraï¿½ retorno 0 para OK e 1 para NOK
    )
  AS
    vQTD_REG         NUMBER;
    vQUERY_PRINCIPAL VARCHAR2(3000);
    vSELECT1         VARCHAR2(1000);
    vSELECT2         VARCHAR2(1000);
  BEGIN
    --================================================================================================================
    -- Iniciando as variaveis
    --================================================================================================================
    vQTD_REG         := NULL;
    vQUERY_PRINCIPAL := NULL;
    vSELECT1         := '      
SELECT      COUNT(1)    
' ;
    vSELECT2         := '      
SELECT        
tr.data_inclusao      
,  tr.data_repasse      
,  tr.data_repasse_calculada      
,  tr.codigo_transacao      
,  tr.codigo_favorecido      
,  tr.codigo_conta_favorecido      
,  tr.codigo_estabelecimento      
,  tr.codigo_produto    
,  tr.referencia     
'; --1341
    --================================================================================================================
    -- Transacoes com DATA_REPASSE_CALCULADA nula
    --================================================================================================================
    vQUERY_PRINCIPAL := vSELECT1 || '    
FROM        TB_TRANSACAO  TR    
WHERE       tr.data_repasse_calculada  is null    
';
    EXECUTE IMMEDIATE vQUERY_PRINCIPAL INTO vQTD_REG;
    PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_TRANSACAO_FAVORECIDO_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => 'Transacoes com data repasse calculada nula [' || vQTD_REG ||']' );
    --================================================================================================================
    -- Gerando o CURSOR que iraï¿½ ler as TRANSAaï¿½aï¿½ES que estao com o campo DATA_REPASSE_CALCULADA nula
    --================================================================================================================
    IF vQTD_REG        IS NOT NULL AND pUPD = 'S' THEN
      vQUERY_PRINCIPAL := vSELECT2 || '    
FROM        TB_TRANSACAO  TR    
WHERE       tr.data_repasse_calculada  is null    
ORDER BY      
tr.codigo_estabelecimento    
,  tr.codigo_produto    
';
      PR_TRANSACAO_FAVORECIDO_S ( pQUERY_PRINCIPAL => vQUERY_PRINCIPAL, pDATA_INCLUSAO => pDATA_INCLUSAO, pRETORNO => pRETORNO );
    END IF;
    --================================================================================================================
    -- Transacoes sem FAVORECIDO
    --================================================================================================================
    vQUERY_PRINCIPAL := vSELECT1 || '    
FROM        TB_TRANSACAO  TR    
WHERE       tr.codigo_favorecido is null  OR tr.codigo_favorecido = 0    
';
    EXECUTE IMMEDIATE vQUERY_PRINCIPAL INTO vQTD_REG;
    PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_TRANSACAO_FAVORECIDO_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => 'Transacoes sem favorecido [' || vQTD_REG ||']' );
    --================================================================================================================
    -- Gerando o CURSOR que iraï¿½ ler as TRANSAaï¿½aï¿½ES que estao sem FAVORECIDO
    --================================================================================================================
    IF vQTD_REG        IS NOT NULL AND pUPD = 'S' THEN
      vQUERY_PRINCIPAL := vSELECT2 || '    
FROM        TB_TRANSACAO  TR    
WHERE       tr.codigo_favorecido is null or tr.codigo_favorecido = 0    
';
      PR_TRANSACAO_FAVORECIDO_S ( pQUERY_PRINCIPAL => vQUERY_PRINCIPAL, pDATA_INCLUSAO => pDATA_INCLUSAO, pRETORNO => pRETORNO );
    END IF;
    --================================================================================================================
    -- Transacoes sem CONTA favorecido
    --================================================================================================================
    vQUERY_PRINCIPAL := vSELECT1 || '    
FROM        TB_TRANSACAO  TR    
WHERE       tr.codigo_conta_favorecido is null  or tr.codigo_conta_favorecido = 0    
';
    EXECUTE IMMEDIATE vQUERY_PRINCIPAL INTO vQTD_REG;
    PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_TRANSACAO_FAVORECIDO_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => 'Transacoes sem conta do favorecido [' || vQTD_REG ||']' );
    --================================================================================================================
    -- Gerando o CURSOR que iraï¿½ ler as TRANSAaï¿½aï¿½ES que estao sem CONTA favorecido
    --================================================================================================================
    IF vQTD_REG        IS NOT NULL AND pUPD = 'S' THEN
      vQUERY_PRINCIPAL := vSELECT2 || '    
FROM        TB_TRANSACAO  TR    
WHERE       tr.codigo_conta_favorecido is null or tr.codigo_conta_favorecido = 0    
';
      PR_TRANSACAO_FAVORECIDO_S ( pQUERY_PRINCIPAL => vQUERY_PRINCIPAL, pDATA_INCLUSAO => pDATA_INCLUSAO, pRETORNO => pRETORNO );
    END IF;
  END PR_TRANSACAO_FAVORECIDO_L;
--=============================================================================
  PROCEDURE PR_TRANSACAO_FAVORECIDO_S
    (
      pQUERY_PRINCIPAL VARCHAR2,
      pDATA_INCLUSAO   DATE, -- e a partir desta data de inclusao que a transacao poderaï¿½ ser atualizada
      pRETORNO OUT NUMBER    -- devolveraï¿½ retorno 0 para OK e 1 para NOK
    )
  AS
    Query_Principal TP_REFCURSOR;
    vQTD_TOT                   NUMBER;
    vQTD                       NUMBER;
    vQTD_REG                   NUMBER;
    vCODIGO_TRANSACAO          NUMBER;
    vCODIGO_FAVORECIDO         NUMBER;
    vCODIGO_CONTA_FAVORECIDO   NUMBER;
    vCODIGO_ESTABELECIMENTO    NUMBER;
    vCODIGO_PRODUTO            NUMBER;
    vCODIGO_FAVORECIDO_S       NUMBER;
    vCODIGO_CONTA_FAVORECIDO_S NUMBER;
    vCODIGO_ESTABELECIMENTO_S  NUMBER;
    vCODIGO_PRODUTO_S          NUMBER;
    vRETORNO                   NUMBER; --scf1185
    vDATA_REPASSE              DATE;
    vDATA_REPASSE_CALCULADA    DATE;
    vDATA_INCLUSAO             DATE;
    vQUERY_PRINCIPAL           VARCHAR2(2000);
    vQUERY_UPD                 VARCHAR2(2000);
    vUPD                       VARCHAR2(2000);
    vREFERENCIA                VARCHAR2(3); ----1339
    vREFERENCIA_S              VARCHAR2(3); ----1341
    vretcur TP_REFCURSOR;                     --1341
  BEGIN
    --================================================================================================================
    -- Iniciando as variaveis
    --================================================================================================================
    vQTD_TOT                   := 0;
    vQTD                       := 0;
    vQTD_REG                   := 0;
    vCODIGO_TRANSACAO          := 0;
    vCODIGO_FAVORECIDO         := 0;
    vCODIGO_CONTA_FAVORECIDO   := 0;
    vCODIGO_ESTABELECIMENTO    := 0;
    vCODIGO_PRODUTO            := 0;
    vCODIGO_FAVORECIDO_S       := 0;
    vCODIGO_CONTA_FAVORECIDO_S := 0;
    vCODIGO_ESTABELECIMENTO_S  := 0;
    vCODIGO_PRODUTO_S          := 0;
    vREFERENCIA_S              := NULL; --1341
    vDATA_REPASSE              := NULL;
    vDATA_REPASSE_CALCULADA    := NULL;
    vDATA_INCLUSAO             := NULL;
    vQUERY_PRINCIPAL           := NULL;
    vQUERY_UPD                 := NULL;
    vUPD                       := NULL;
    vRETORNO                   := 0; --scf1185
    pRETORNO                   := 0;
    vREFERENCIA                := NULL; ----1339
    --================================================================================================================
    -- Iniciando as variaveis
    --================================================================================================================
    vQUERY_PRINCIPAL := pQUERY_PRINCIPAL;
    PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_TRANSACAO_FAVORECIDO_S', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => pQUERY_PRINCIPAL );
    OPEN Query_Principal FOR vQuery_Principal;
    LOOP
      FETCH Query_Principal
      INTO vDATA_INCLUSAO ,
        vDATA_REPASSE ,
        vDATA_REPASSE_CALCULADA ,
        vCODIGO_TRANSACAO ,
        vCODIGO_FAVORECIDO ,
        vCODIGO_CONTA_FAVORECIDO ,
        vCODIGO_ESTABELECIMENTO ,
        vCODIGO_PRODUTO ,
        vREFERENCIA ;
      EXIT
    WHEN Query_Principal%NOTFOUND;
      vUPD := NULL;
      --================================================================================================================
      -- Se DATA REPASSE CALCULADA ESTIVER nula atualizar com DATA REPASSE
      --================================================================================================================
      IF vDATA_REPASSE_CALCULADA IS NULL THEN
        IF vUPD                  IS NOT NULL THEN
          vUPD                   := vUPD || ',';
        END IF;
        vUPD := vUPD || '  
DATA_REPASSE_CALCULADA  =  TO_DATE(''' || vDATA_REPASSE || ''')' ;
      END IF;
      --================================================================================================================
      -- Se DATA INCLUSAO da transacao for MAIOR ou IGUAL ao PARAMETRO
      --================================================================================================================
      IF vDATA_INCLUSAO >= pDATA_INCLUSAO THEN
        --================================================================================================================
        -- Se FAVORECIDO for NULO
        --================================================================================================================
        IF vCODIGO_FAVORECIDO        IS NULL OR vCODIGO_FAVORECIDO = 0 OR vCODIGO_CONTA_FAVORECIDO IS NULL OR vCODIGO_CONTA_FAVORECIDO = 0 THEN
          IF vCODIGO_ESTABELECIMENTO <> vCODIGO_ESTABELECIMENTO_S OR vCODIGO_PRODUTO <> vCODIGO_PRODUTO_S OR vREFERENCIA <> vREFERENCIA_S ---1341
            THEN
            vCODIGO_ESTABELECIMENTO_S := vCODIGO_ESTABELECIMENTO;
            vCODIGO_PRODUTO_S         := vCODIGO_PRODUTO;
            vREFERENCIA_S             := vREFERENCIA; --1341
            --================================================================================================================
            -- Carrega o FAVORECIDO e a CONTA do FAVORECIDO pelos dados da transacao
            --================================================================================================================
            PR_TRATAR_FAVORECIDO ( pCODIGO_ESTABELECIMENTO => vCODIGO_ESTABELECIMENTO_S, pCODIGO_PRODUTO => vCODIGO_PRODUTO_S, pREFERENCIA => vREFERENCIA, ------1339
            pCODIGO_CONTA_FAVORECIDO => vCODIGO_CONTA_FAVORECIDO_S,                                                                                            -- out
            pCODIGO_FAVORECIDO => vCODIGO_FAVORECIDO_S,                                                                                                        -- out
            pRETORNO => vRETORNO,                                                                                                                              --out
            retcur => vretcur                                                                                                                                  --out scf1341
            );
            IF vRETORNO <> 0 THEN
              PR_LOG_DB ( PDESCRICAO_LOG => ' ETB [' || TRIM(vCODIGO_ESTABELECIMENTO) || ']  FAV [' || TRIM(vCODIGO_FAVORECIDO) || '] NaO TEM CONTA PARA PRD [' || TRIM(vCODIGO_PRODUTO) || ']', PTIPO_ORIGEM => 'PR_TRANSACAO_FAVORECIDO_S', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => NULL );
            END IF;
          END IF;
          IF vCODIGO_FAVORECIDO_S IS NOT NULL AND vCODIGO_FAVORECIDO_S <> 0 AND vCODIGO_CONTA_FAVORECIDO_S IS NOT NULL AND vCODIGO_CONTA_FAVORECIDO_S <> 0 THEN
            IF vUPD               IS NOT NULL THEN
              vUPD                := vUPD || ',';
            END IF;
            vUPD := vUPD || '  
CODIGO_FAVORECIDO      =  ' || vCODIGO_FAVORECIDO_S || ',  
CODIGO_CONTA_FAVORECIDO    =  ' || vCODIGO_CONTA_FAVORECIDO_S;
          END IF;
        END IF;
      END IF;
      --================================================================================================================
      -- Se TEM alguma ALTERAaï¿½aO para fazer nesta TRANSACAO
      --================================================================================================================
      IF vUPD      IS NOT NULL THEN
        vQUERY_UPD := '  
UPDATE  TB_TRANSACAO  
SET  
' || vUPD ||'  
WHERE  
CODIGO_TRANSACAO  =  ' || vCODIGO_TRANSACAO;
        EXECUTE IMMEDIATE vQUERY_UPD;
      END IF;
      --================================================================================================================
      -- Soma registros lidos para gerar LOG e efetuar COMMIT
      --================================================================================================================
      vQTD     := vQTD     + 1;
      vQTD_TOT := vQTD_TOT + 1;
      vUPD     := NULL;
      IF vQTD  >= 15000 THEN
        COMMIT;
        PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_TRANSACAO_FAVORECIDO_S', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => '[' || TO_CHAR(vQTD_TOT) || '] Registros Tratados' );
        vQTD := 0;
      END IF;
    END LOOP;
    COMMIT;
    PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_TRANSACAO_FAVORECIDO_S', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => '[' || TO_CHAR(vQTD_TOT) || '] Registros Tratados FIM' );
  END PR_TRANSACAO_FAVORECIDO_S;
--=============================================================================
  PROCEDURE PR_TRATAR_DT_REP_CALC(
      pDATA_REPASSE       DATE,
      pCODIGO_PROCESSO    NUMBER,
      pCODIGO_PRODUTO     NUMBER,
      pTIPO_TRANSACAO     VARCHAR,
      pDATA_TRANSACAO     DATE,
      pDATA_PROCESSAMENTO DATE, --1495
      pDATA_REPASSE_CALCULADA OUT DATE,
      pRETORNO OUT NUMBER )
  AS
    vDATA_DIA               DATE;
    vDATA_BASE              DATE;
    vDATA_REPASSE           DATE;
    vDATA_REPASSE_CALCULADA DATE;
    vDATA_PROCESSAMENTO     DATE; --1495
    vDIAS_VENCIDOS          NUMBER;
    vNOME_PRODUTO           VARCHAR(40);
    vGRUPO_EMPRESA          NUMBER;
    vRETORNO                NUMBER;
    vDIAS_REPASSE           NUMBER;
    vDATA_TRANSACAO         DATE;
    vDIA_SEMANA_TRANSACAO   CHAR;
    vDIA_SEMANA_REPASSE     CHAR;
  BEGIN
    --=================================================================================================================================
    --INICILIZANDO VARIAVEIS
    --==================================================================================================================================
    vDATA_DIA               := TRUNC(sysdate);             -- DATA DE HOJE
    vDATA_BASE              := TRUNC(sysdate);             -- DATA DE HOJE
    vDATA_REPASSE_CALCULADA := TRUNC(sysdate);             -- DATA DE HOJE
    vDATA_REPASSE           := TRUNC(pDATA_REPASSE);       -- DATA DE REPASSE DA TRANSACAO
    vDATA_PROCESSAMENTO     := TRUNC(pDATA_PROCESSAMENTO); -- 1495
    vDIAS_VENCIDOS          := 0;
    vNOME_PRODUTO           := '';
    vGRUPO_EMPRESA          := 0;
    vRETORNO                := 0;
    vDIAS_REPASSE           :=0;
    vDATA_TRANSACAO         := TRUNC(pDATA_TRANSACAO);
    -- ====================================================================================================================================
    -- CONFERE QUAL GRUPO
    -- ====================================================================================================================================
    SELECT B.CODIGO_EMPRESA_GRUPO
    INTO vGRUPO_EMPRESA
    FROM TB_PROCESSO A
    INNER JOIN TB_ARQUIVO B
    ON B.CODIGO_PROCESSO    = A.CODIGO_PROCESSO
    WHERE A.CODIGO_PROCESSO = pCODIGO_PROCESSO
    AND ROWNUM              = 1;
    -- ====================================================================================================================================
    -- CONFERE QUAL ? O NOME DO PRODUTO
    -- ====================================================================================================================================
    SELECT A.NOME_PRODUTO
    INTO vNOME_PRODUTO
    FROM TB_PRODUTO A
    WHERE A.CODIGO_PRODUTO = pCODIGO_PRODUTO;
    -- ====================================================================================================================================
    -- CONFERE DIAS VENCIDOS
    -- ====================================================================================================================================
    BEGIN
      IF vGRUPO_EMPRESA     = 1 THEN     -- GRUPO CARREFOUR - CARREGAR DIAS VENCIDOS CONFORME PRODUTO
        IF (PTIPO_TRANSACAO = 'AJ') THEN -- TRANSACAO AJ DIAS VENCIDOS ESPECIFICO
          SELECT NVL(DIAS_VENCIDOS_AJ,0)
          INTO VDIAS_VENCIDOS
          FROM TB_PRODUTO
          WHERE CODIGO_PRODUTO = PCODIGO_PRODUTO;
        ELSE
          SELECT NVL(DIAS_VENCIDOS,0)
          INTO VDIAS_VENCIDOS
          FROM TB_PRODUTO
          WHERE CODIGO_PRODUTO = PCODIGO_PRODUTO;
        END IF;
      ELSE
        IF vGRUPO_EMPRESA = 2 THEN -- GRUPO ATACADAO - CARREGAR DIAS DE REPASSE DE ACORDO COM O TIPO DE REGISTRO
          SELECT NVL(DIAS_REPASSE ,0)
          INTO vDIAS_REPASSE
          FROM TB_CONFIGURACAO_TIPO_REGISTRO
          WHERE TIPO_ARQUIVO = 'ArquivoTSYSATA'
          AND TIPO_REGISTRO  = PTIPO_TRANSACAO;
        ELSE --- GRUPO N?O ? CARREFOUR E NEM ATACADAO - CARREGAR DIAS VENCIDOS DO PARAMETRO GERAL
          SELECT NVL(DIAS_VENCIDO,0) INTO VDIAS_VENCIDOS FROM TB_CONFIGURACAO_SISTEMA;
        END IF;
      END IF;
    END;
    --=================================================================================================================================
    -- GERANDO DATA BASE
    --==================================================================================================================================
    IF (vGRUPO_EMPRESA = 1 AND vNOME_PRODUTO IN('RCAR', 'CRAF')) THEN -- GRUPO CARREFOUR e PRODUTOS ESPECIFICOS
      vDATA_BASE      := vDATA_DIA            + vDIAS_VENCIDOS;
    ELSE
      vDATA_BASE := vDATA_DIA;
    END IF;
    --=================================================================================================================================
    -- CONFERINDO SE A TRANSACAO ESTA VENCIDA
    --==================================================================================================================================
    IF ( vDATA_REPASSE         > vDATA_BASE) THEN                       -- A VENCER
      vDATA_REPASSE_CALCULADA := vDATA_REPASSE;                         -- CONSIDERA A DATA DE REPASSE VINDA DA TRANSACAO
    ELSE                                                                -- VENCIDA
      IF (vGRUPO_EMPRESA         = 1 AND vNOME_PRODUTO IN('RCAR', 'CRAF')) THEN -- GRUPO CARREFOUR e PRODUTOS ESPECIFICOS
        vDATA_REPASSE_CALCULADA := vDATA_BASE;                                  -- CONSIDERA A DATA BASE GERADA
      ELSE
        vDATA_REPASSE_CALCULADA := vDATA_BASE + vDIAS_VENCIDOS; -- CONSIDERA A DATA BASE + DIAS VENCIDOS
      END IF;
    END IF;
    /*
    --===================================================================================================================================
    -- 1495 INIBINDO COMEï¿½O
    --===================================================================================================================================
    -- MELHORIAS-2017 - SE FOR ATACADAO, DATA DE REPASSE = DATA TRANSACAO + DIAS DE REPASSE POR TIPO DE REGISTRO
    --===================================================================================================================================
    IF vGRUPO_EMPRESA            = 2 THEN
    IF vDIAS_REPASSE           = 0 OR vNOME_PRODUTO <> 'RECTO' THEN
    vDATA_REPASSE_CALCULADA := vDATA_REPASSE;
    vDIAS_REPASSE           := 0;
    ELSE
    vDATA_REPASSE_CALCULADA := vDATA_TRANSACAO;
    END IF;
    ELSE
    vDIAS_REPASSE := 0;
    END IF;
    --===================================================================================================================================
    -- OBTENDO DATA ?TIL
    --===================================================================================================================================
    PR_DIA_UTIL(vDATA_REPASSE_CALCULADA, vDIAS_REPASSE, vDATA_REPASSE_CALCULADA);
    */
    --===================================================================================================================================
    -- 1495 INIBINDO FIM
    --===================================================================================================================================
    IF vGRUPO_EMPRESA          = 2 THEN
      vDATA_REPASSE_CALCULADA := vDATA_PROCESSAMENTO;
    ELSE
      vDIAS_REPASSE := 0;
      vNOME_PRODUTO := '';
    END IF;
    --===================================================================================================================================
    -- OBTENDO DATA UTIL
    --===================================================================================================================================
    PR_DIA_UTIL(vDATA_REPASSE_CALCULADA, vDIAS_REPASSE, vNOME_PRODUTO, vDATA_REPASSE_CALCULADA);
    -- ====================================================================================================================================
    -- RETORNAR A DATA DE REPASSE CALCULADA
    -- ====================================================================================================================================
    pDATA_REPASSE_CALCULADA := vDATA_REPASSE_CALCULADA;
    pRETORNO                := vRETORNO;
  END PR_TRATAR_DT_REP_CALC;
--=============================================================================
  PROCEDURE PR_TRATAR_FAVORECIDO ---1185
    (
      pCODIGO_ESTABELECIMENTO NUMBER,
      pCODIGO_PRODUTO         NUMBER,
      pREFERENCIA             CHAR, ------1339
      pCODIGO_CONTA_FAVORECIDO OUT NUMBER,
      pCODIGO_FAVORECIDO OUT NUMBER,
      pRETORNO OUT NUMBER,
      retcur OUT TP_REFCURSOR -- SCF1341
    )
  AS
    vBANCO           CHAR(3);  -- SCF1341
    vAGENCIA         CHAR(6);  -- SCF1341
    vCONTA           CHAR(12); -- SCF1341
    vBANCOaux        CHAR(4);  -- SCF1341
    vAGENCIAaux      CHAR(7);  -- SCF1341
    vCONTAaux        CHAR(13); -- SCF1341
    vNOME_FAVORECIDO CHAR(40); -- SCF1341
    vCNPJ_FAVORECIDO CHAR(15); -- SCF1341
  BEGIN
    --=========================================================================================================================================
    --INICIA VARIAVEIS
    --=========================================================================================================================================
    vBANCO                   := '000';
    vAGENCIA                 := '000000';
    vCONTA                   := '000000000000';
    vNOME_FAVORECIDO         := '';
    vCNPJ_FAVORECIDO         := '';
    pCODIGO_CONTA_FAVORECIDO := 0;
    pCODIGO_FAVORECIDO       := 0;
    pRETORNO                 := 0;
    --=========================================================================================================================================
    --CARREGAR FAVORECIDO PELO ESTABELECIMENTO RECEBIDO NO PARAMETRO
    --=========================================================================================================================================
    BEGIN
      SELECT E.NOME_FAVORECIDO ,
        E.CNPJ_FAVORECIDO ,
        A.CODIGO_FAVORECIDO ,
        0
      INTO vNOME_FAVORECIDO ---- SCF1341
        ,
        vCNPJ_FAVORECIDO ---- SCF1341
        ,
        pCODIGO_FAVORECIDO ,
        pRETORNO
      FROM TB_ESTABELECIMENTO_FAVORECIDO A
      INNER JOIN TB_FAVORECIDO E
      ON E.CODIGO_FAVORECIDO         = A.CODIGO_FAVORECIDO ---- SCF1341
      WHERE A.CODIGO_ESTABELECIMENTO = pCODIGO_ESTABELECIMENTO
      AND ROWNUM                     = 1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      vCNPJ_FAVORECIDO         := ''; -- SCF1341
      vNOME_FAVORECIDO         := ''; -- SCF1341
      pCODIGO_FAVORECIDO       := 0;
      pCODIGO_CONTA_FAVORECIDO := 0;
      pRETORNO                 := 1;
      vBANCO                   := '000';          -- SCF1341
      vAGENCIA                 := '000000';       -- SCF1341
      vCONTA                   := '000000000000'; -- SCF1341
    END;
    --=========================================================================================================================================
    --CARREGAR BANCO, AGENCIA E CONTA DO FAVORECIDO ENCONTRADO
    --=========================================================================================================================================
    IF (pCODIGO_FAVORECIDO <> 0) THEN
      BEGIN
        SELECT B.CODIGO_CONTA_FAVORECIDO ,
          0 -- retorno
          ,
          LPAD( (REGEXP_REPLACE(D.BANCO, '[^[:alnum:]]') ), 03, '0') ,
          LPAD( (REGEXP_REPLACE(D.AGENCIA, '[^[:alnum:]]') ), 06, '0') ,
          LPAD( (REGEXP_REPLACE(D.CONTA, '[^[:alnum:]]') ), 12, '0')
        INTO pCODIGO_CONTA_FAVORECIDO ,
          pRETORNO ,
          vBANCO -- SCF1341
          ,
          vAGENCIA -- SCF1341
          ,
          vCONTA -- SCF1341
        FROM TB_CONTA_FAVORECIDO D
        INNER JOIN TB_CONTA_FAVORECIDO_PRODUTO B
        ON B.CODIGO_PRODUTO           = pCODIGO_PRODUTO
        AND B.CODIGO_CONTA_FAVORECIDO = D.CODIGO_CONTA_FAVORECIDO
        AND B.REFERENCIA              = pREFERENCIA ---1339
        WHERE D.CODIGO_FAVORECIDO     = pCODIGO_FAVORECIDO
        AND ROWNUM                    = 1;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        pCODIGO_CONTA_FAVORECIDO := 0;
        pRETORNO                 := 1;
        vBANCO                   := '000';          -- SCF1341
        vAGENCIA                 := '000000';       -- SCF1341
        vCONTA                   := '000000000000'; -- SCF1341
      END;
    END IF;
    --=========================================================================================================================================
    --DEVOLVER RESULTADO
    --=========================================================================================================================================
    OPEN retcur FOR SELECT vBANCO BANCO ,
    vAGENCIA AGENCIA ,
    vCONTA CONTA ,
    trim(vNOME_FAVORECIDO) NOME_FAVORECIDO -- SCF1341
    ,
    trim(vCNPJ_FAVORECIDO) CNPJ_FAVORECIDO -- SCF1341
    FROM dual;
  END PR_TRATAR_FAVORECIDO;
--=============================================================================
  PROCEDURE PR_CHEQUE_DEVOLVIDO_L(
      pFILTRO_DT_ESTORNO_DE    DATE     := NULL,
      pFILTRO_DT_ESTORNO_ATE   DATE     := NULL,
      pNUMERO_PROTOCOLO        VARCHAR2 := NULL,
      pFILTRO_ENVIO_CCI        VARCHAR2 := NULL,
      pCOD_ESTABELECIMENTO     NUMBER   := NULL,
      pFILTRO_DT_PROTOCOLO_DE  DATE     := NULL,
      pFILTRO_DT_PROTOCOLO_ATE DATE     := NULL,
      pFILTRO_STATUS_PROTOCOLO VARCHAR2 := NULL,
      pFILTRO_CTA_CLIENTE      VARCHAR2 := NULL,
      pFILTRO_TP_ESTORNO       VARCHAR2 := NULL,
      pFILTRO_DT_PROC_DE       DATE     := NULL,
      pFILTRO_DT_PROC_ATE      DATE     := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    formatoData VARCHAR2(20);
    vQuery      VARCHAR2(5000);
    vSelect     VARCHAR2(2000);
    vWhere      VARCHAR2(2000);
  BEGIN
    formatoData := 'yyyy-mm-dd';
    --==================================================================================================================
    -- Monta o SELECT
    --==================================================================================================================
    vSelect := '  
SELECT   
DATA_MOVIMENTO,  
NUMERO_CONTA_CLIENTE,  
NSU_HOST,  
NSU_HOST_ORIGINAL,  
CODIGO_ESTABELECIMENTO,  
NOME_ESTABELECIMENTO,  
VALOR_REPASSE,  
DATA_PROTOCOLO,  
DATA_ESTORNO,  
NUMERO_PROTOCOLO,  
TIPO_ESTORNO,  
STATUS_ENVIO_CCI,  
STATUS_PROTOCOLO,  
TIPO_TRANSACAO,  
FORMA_MEIO_PAGAMENTO,  
CODIGO_TRANSACAO    

from VW_CHEQUE_DEVOLVIDO   
';
    --==================================================================================================================
    -- Monta o WHERE
    --==================================================================================================================
    vWhere                   := '';
    IF pFILTRO_DT_ESTORNO_DE IS NOT NULL THEN
      vWhere                 := vWhere || ' DATA_ESTORNO >= ''' || pFILTRO_DT_ESTORNO_DE || ''' and ';
    END IF;
    IF pFILTRO_DT_ESTORNO_ATE IS NOT NULL THEN
      vWhere                  := vWhere || ' DATA_ESTORNO <= ''' || pFILTRO_DT_ESTORNO_ATE || ''' and ';
    END IF;
    IF pNUMERO_PROTOCOLO IS NOT NULL THEN
      vWhere             := vWhere || ' NUMERO_PROTOCOLO = ''' || RTRIM(pNUMERO_PROTOCOLO) || ''' and ';
    END IF;
    IF pFILTRO_ENVIO_CCI         IS NOT NULL THEN
      IF RTRIM(pFILTRO_ENVIO_CCI) = '9' THEN
        vWhere                   := vWhere || ' STATUS_ENVIO_CCI <> ''' || RTRIM(1) || ''' and ';
      ELSE
        vWhere := vWhere || ' STATUS_ENVIO_CCI = ''' || RTRIM(1) || ''' and ';
      END IF;
    END IF;
    IF pCOD_ESTABELECIMENTO IS NOT NULL THEN
      vWhere                := vWhere || ' CODIGO_ESTABELECIMENTO = ''' || RTRIM(pCOD_ESTABELECIMENTO) || ''' and ';
    END IF;
    IF pFILTRO_DT_PROTOCOLO_DE IS NOT NULL THEN
      vWhere                   := vWhere || ' DATA_PROTOCOLO >= ''' || pFILTRO_DT_PROTOCOLO_DE || ''' and ';
    END IF;
    IF pFILTRO_DT_PROTOCOLO_ATE IS NOT NULL THEN
      vWhere                    := vWhere || ' DATA_PROTOCOLO <= ''' || pFILTRO_DT_PROTOCOLO_ATE || ''' and ';
    END IF;
    IF pFILTRO_STATUS_PROTOCOLO         IS NOT NULL THEN
      IF RTRIM(pFILTRO_STATUS_PROTOCOLO) = 'S' THEN
        vWhere                          := vWhere || ' nvl(STATUS_PROTOCOLO, ''N'') = ''' || RTRIM('S') || ''' and ';
      ELSE
        vWhere := vWhere || ' nvl(STATUS_PROTOCOLO, ''N'') <> ''' || RTRIM('S') || ''' and ';
      END IF;
    END IF;
    IF pFILTRO_CTA_CLIENTE IS NOT NULL THEN
      vWhere               := vWhere || ' NUMERO_CONTA_CLIENTE = ''' || RTRIM(pFILTRO_CTA_CLIENTE) || ''' and ';
    END IF;
    IF pFILTRO_TP_ESTORNO IS NOT NULL THEN
      vWhere              := vWhere || ' TIPO_ESTORNO = ''' || RTRIM(pFILTRO_TP_ESTORNO) || ''' and ';
    END IF;
    IF pFILTRO_DT_PROC_DE IS NOT NULL THEN
      vWhere              := vWhere || ' DATA_MOVIMENTO >= ''' || pFILTRO_DT_PROC_DE || ''' and ';
    END IF;
    IF pFILTRO_DT_PROC_ATE IS NOT NULL THEN
      vWhere               := vWhere || ' DATA_MOVIMENTO <= ''' || pFILTRO_DT_PROC_ATE || ''' and ';
    END IF;
    --==================================================================================================================
    -- Caso exista algum filtro, remove o ultimo 'and' e adiciona a palavra 'where' no inicio
    --==================================================================================================================
    IF LENGTH(vWhere) > 0 THEN
      vWhere         := SUBSTR(vWhere, 0, LENGTH(vWhere) - 4);
      vWhere         := 'WHERE   
' || RTRIM(vWhere);
    END IF;
    --==================================================================================================================
    -- Cria a QUERY
    --==================================================================================================================
    vQuery := vSELECT || vWhere;
    --==================================================================================================================
    -- Efetua a busca
    --==================================================================================================================
    OPEN retcur FOR vQuery;
    --==================================================================================================================
    -- Log da QUERY
    --==================================================================================================================
    PR_LOG_DB( PDESCRICAO_LOG => NULL, PTIPO_ORIGEM => 'PR_CHEQUE_DEVOLVIDO_L', PCODIGO_ORIGEM => NULL, PDESCRICAOGRANDE_LOG => vQuery );
  END PR_CHEQUE_DEVOLVIDO_L;
--==================================================================================================================
  PROCEDURE PR_CHEQUE_DEVOLVIDO_S(
      pCODIGO_TRANSACAO NUMBER   := NULL,
      pDATA_PROTOCOLO   DATE     := NULL,
      pDATA_ESTORNO     DATE     := NULL,
      pNUMERO_PROTOCOLO VARCHAR2 := NULL,
      pTIPO_ESTORNO     VARCHAR2 := NULL -- 963
    )
  AS
    vCODIGO_TRANSACAO NUMBER(12);
    vDATA_PROTOCOLO   DATE;
    vDATA_ESTORNO     DATE;
    vNUMERO_PROTOCOLO VARCHAR2(14);
    vTIPO_ESTORNO     VARCHAR2(2);
    vSTATUS_PROTOCOLO CHAR;
  BEGIN
    -- Inicializa
    vCODIGO_TRANSACAO := pCODIGO_TRANSACAO;
    vDATA_PROTOCOLO   := pDATA_PROTOCOLO;
    vDATA_ESTORNO     := pDATA_ESTORNO;
    vNUMERO_PROTOCOLO := pNUMERO_PROTOCOLO;
    vTIPO_ESTORNO     := pTIPO_ESTORNO;
    --==================================================================================================================
    -- SCF963
    --==================================================================================================================
    IF (vCODIGO_TRANSACAO IS NOT NULL) AND (vDATA_PROTOCOLO IS NOT NULL) AND (vDATA_ESTORNO IS NOT NULL) AND (vNUMERO_PROTOCOLO IS NOT NULL) THEN
      vSTATUS_PROTOCOLO   := 'S';
    ELSE
      vSTATUS_PROTOCOLO := 'N';
    END IF;
    -- Atualiza
    UPDATE TB_TRANSACAO_ANULACAO_PAGTO
    SET DATA_PROTOCOLO     = vDATA_PROTOCOLO,
      DATA_ESTORNO         = vDATA_ESTORNO,
      NUMERO_PROTOCOLO     = vNUMERO_PROTOCOLO,
      STATUS_PROTOCOLO     = vSTATUS_PROTOCOLO,
      TIPO_ESTORNO         = vTIPO_ESTORNO
    WHERE CODIGO_TRANSACAO = vCODIGO_TRANSACAO;
  END PR_CHEQUE_DEVOLVIDO_S;
--=============================================================================
  PROCEDURE PR_CHEQUE_DEVOLVIDO_B(
      pCODIGO_TRANSACAO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    -- Faz a busca
    OPEN retcur FOR SELECT A.DATA_MOVIMENTO,
    B.NUMERO_CONTA_CLIENTE,
    A.NSU_HOST,
    B.NSU_HOST_ORIGINAL,
    C.CODIGO_ESTABELECIMENTO,
    '[' || C.CNPJ || ']-[' || C.RAZAO_SOCIAL || ']' NOME_ESTABELECIMENTO,
    A.VALOR_REPASSE,
    B.DATA_PROTOCOLO,   -- PODEM SER ALTERADOS
    B.DATA_ESTORNO,     -- PODEM SER ALTERADOS
    B.NUMERO_PROTOCOLO, -- PODEM SER ALTERADOS
    B.TIPO_ESTORNO,     -- PODEM SER ALTERADOS
    A.STATUS_TRANSACAO
  AS
    STATUS_ENVIO_CCI,       -- FILTRO -- igual a 6 nao ENVIADAS para CCI, diferente de 6 ENVIADAS PARA CCI
    B.STATUS_PROTOCOLO,     -- FILTRO
    A.TIPO_TRANSACAO,       -- FILTRO
    B.FORMA_MEIO_PAGAMENTO, -- FILTRO
    A.CODIGO_TRANSACAO      -- nao e filtro, servirï¿½ para p update
    FROM TB_TRANSACAO_ANULACAO_PAGTO B INNER JOIN TB_TRANSACAO A ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO LEFT JOIN TB_ESTABELECIMENTO C ON C.CODIGO_ESTABELECIMENTO = A.CODIGO_ESTABELECIMENTO WHERE B.CODIGO_TRANSACAO = pCODIGO_TRANSACAO;
  END PR_CHEQUE_DEVOLVIDO_B;
--=============================================================================
  PROCEDURE PR_CARONA_D(
      PCODIGO_ARQUIVO_ITEM NUMBER := NULL ,
      PCODIGO_ARQUIVO      NUMBER := NULL ,
      PCODIGO_PROCESSO     NUMBER := NULL ,
      PQTD OUT NUMBER )
  AS
    vCODIGO_ARQUIVO_ITEM    NUMBER;
    vCODIGO_ARQUIVO         NUMBER;
    VCODIGO_PROCESSO        NUMBER;
    VCODIGO_PROCESSO_ATUAL  NUMBER;
    VCODIGO_PROCESSO_ORIGEM NUMBER;
    vQtdDesbloqueio         NUMBER;
    CursorArquivos TP_REFCURSOR;
    VQTDRETORNADO NUMBER;
    VQTD          NUMBER;
  BEGIN
    VCODIGO_ARQUIVO_ITEM      := PCODIGO_ARQUIVO_ITEM;
    VCODIGO_ARQUIVO           := PCODIGO_ARQUIVO;
    VCODIGO_PROCESSO          := PCODIGO_PROCESSO;
    VQTD                      := 0;
    vQtdDesbloqueio           := 0;
    IF ( VCODIGO_ARQUIVO_ITEM IS NOT NULL ) THEN
      PR_ITEM_CARONA_D( PCODIGO_ARQUIVO_ITEM => VCODIGO_ARQUIVO_ITEM, PCODIGO_PROCESSO_ATUAL => VCODIGO_PROCESSO_ATUAL, PCODIGO_PROCESSO_ORIGEM => VCODIGO_PROCESSO_ORIGEM, PQTD => VQTDRETORNADO);
      PR_PROCESSO_STATUS_BLOQUEIO_U(PCODIGO_PROCESSO => VCODIGO_PROCESSO_ATUAL);
      PR_PROCESSO_STATUS_BLOQUEIO_U(PCODIGO_PROCESSO => VCODIGO_PROCESSO_ORIGEM);
      VQTD := VQTDRETORNADO;
    ELSE
      IF ( VCODIGO_ARQUIVO IS NOT NULL ) THEN
        PR_ARQUIVO_CARONA_D(PCODIGO_ARQUIVO => VCODIGO_ARQUIVO, PQTD => VQTDRETORNADO);
        VQTD := VQTDRETORNADO;
      ELSE
        IF ( VCODIGO_PROCESSO IS NOT NULL ) THEN
          OPEN CursorArquivos FOR SELECT CODIGO_ARQUIVO FROM TB_PROCESSO A INNER JOIN TB_ARQUIVO B ON B.CODIGO_PROCESSO = A.CODIGO_PROCESSO WHERE A.CODIGO_PROCESSO = VCODIGO_PROCESSO;
          LOOP
            FETCH CursorArquivos INTO VCODIGO_ARQUIVO;
            EXIT
          WHEN CursorArquivos%NOTFOUND;
            vQtdDesbloqueio :=0;
            SELECT COUNT(*)
            INTO vQtdDesbloqueio
            FROM TB_ITEM_DESBLOQUEIO
            WHERE CODIGO_ARQUIVO_DESTINO = VCODIGO_ARQUIVO
            AND STATUS_DESBLOQUEIO      <> 'C';
            IF (vQtdDesbloqueio         != 0) THEN
              PR_ARQUIVO_CARONA_D(PCODIGO_ARQUIVO => VCODIGO_ARQUIVO, PQTD => VQTDRETORNADO);
              VQTD := VQTD + VQTDRETORNADO;
            END IF;
          END LOOP;
          CLOSE CursorArquivos;
        END IF;
      END IF;
    END IF;
    PQTD := VQTD;
  END PR_CARONA_D;
--=============================================================================
  PROCEDURE PR_ARQUIVO_CARONA_D(
      PCODIGO_ARQUIVO NUMBER := NULL,
      PQTD OUT NUMBER) --1452
  AS
    vCODIGO_ARQUIVO_ITEM    NUMBER;
    vAtualAnt               NUMBER;
    vOrigemAnt              NUMBER;
    VCODIGO_PROCESSO_ATUAL  NUMBER;
    VCODIGO_PROCESSO_ORIGEM NUMBER;
    CursorArquivos TP_REFCURSOR;
    CursorItens TP_REFCURSOR;
    VQTDRETORNADO NUMBER;
    VQTD          NUMBER;
  BEGIN
    vAtualAnt  := NULL;
    vOrigemAnt := NULL;
    VQTD       := 0;
    ---========================================================================
    -- LER ITENS DO ARQUIVO - Carrega o CODIGO_ARQUIVO_ITEM - TB_ARQUIVO_ITEM
    ---========================================================================
    OPEN CursorItens FOR SELECT CODIGO_ARQUIVO_ITEM FROM TB_ARQUIVO_ITEM WHERE CODIGO_ARQUIVO = PCODIGO_ARQUIVO;
    LOOP
      FETCH CursorItens INTO VCODIGO_ARQUIVO_ITEM;
      EXIT
    WHEN CursorItens%NOTFOUND;
      PR_ITEM_CARONA_D( PCODIGO_ARQUIVO_ITEM => VCODIGO_ARQUIVO_ITEM, PCODIGO_PROCESSO_ATUAL => VCODIGO_PROCESSO_ATUAL, PCODIGO_PROCESSO_ORIGEM => VCODIGO_PROCESSO_ORIGEM, PQTD => VQTDRETORNADO);
      VQTD          := VQTD + VQTDRETORNADO;
      IF (vAtualAnt IS NULL) THEN
        vAtualAnt   := VCODIGO_PROCESSO_ATUAL;
      END IF;
      IF (vOrigemAnt IS NULL) THEN
        vOrigemAnt   := VCODIGO_PROCESSO_ORIGEM;
      END IF;
      IF (vAtualAnt <> VCODIGO_PROCESSO_ATUAL) THEN
        PR_PROCESSO_STATUS_BLOQUEIO_U(PCODIGO_PROCESSO => VAtualAnt);
        vAtualAnt := VCODIGO_PROCESSO_ATUAL;
      END IF;
      IF (vOrigemAnt <> VCODIGO_PROCESSO_ORIGEM) THEN
        PR_PROCESSO_STATUS_BLOQUEIO_U(PCODIGO_PROCESSO => VAtualAnt);
        vOrigemAnt := VCODIGO_PROCESSO_ORIGEM;
      END IF;
    END LOOP;
    CLOSE CursorItens;
    PQTD := VQTD;
  END PR_ARQUIVO_CARONA_D;
--=============================================================================
  PROCEDURE PR_ITEM_CARONA_D(
      PCODIGO_ARQUIVO_ITEM NUMBER := NULL,
      PCODIGO_PROCESSO_ATUAL OUT NUMBER,
      PCODIGO_PROCESSO_ORIGEM OUT NUMBER,
      PQTD OUT NUMBER) --1452
  AS
    vTIPO_ARQUIVO_ITEM tb_arquivo_item.tipo_arquivo_item%type;
    vNOME_ARQUIVO_ATUAL tb_arquivo.nome_arquivo%type;
    vDATA_INCLUSAO_ATUAL tb_arquivo.data_inclusao%type;
    vCODIGO_PROCESSO_ATUAL tb_arquivo.codigo_processo%type;
    vCODIGO_ARQUIVO_ATUAL tb_arquivo.codigo_arquivo%type;
    vNOME_ARQUIVO_ORIGEM tb_arquivo.nome_arquivo%type;
    vDATA_INCLUSAO_ORIGEM tb_arquivo.data_inclusao%type;
    vCODIGO_PROCESSO_ORIGEM tb_arquivo.codigo_processo%type;
    vCODIGO_ARQUIVO_ORIGEM tb_arquivo.codigo_arquivo%type;
    vExisteArqOrigem BOOLEAN;
    VTeveCarona      BOOLEAN;
    vVoltouArqOrigem BOOLEAN;
    CursorDesbloqueios TP_REFCURSOR;
    vretcur TP_REFCURSOR;
    VQTD NUMBER;
  BEGIN
    vTIPO_ARQUIVO_ITEM      := NULL;
    vNOME_ARQUIVO_ATUAL     := NULL;
    vDATA_INCLUSAO_ATUAL    := NULL;
    vCODIGO_PROCESSO_ATUAL  := NULL;
    vCODIGO_ARQUIVO_ATUAL   := NULL;
    vNOME_ARQUIVO_ORIGEM    := NULL;
    vDATA_INCLUSAO_ORIGEM   := NULL;
    vCODIGO_PROCESSO_ORIGEM := NULL;
    vCODIGO_ARQUIVO_ORIGEM  := NULL;
    vExisteArqOrigem        := false;
    VTeveCarona             := false;
    vVoltouArqOrigem        := false;
    VQTD                    := 0;
    ---========================================================================
    -- CARREGA INFORMACOES DO ITEM NO ARQUIVO ATUAL
    ---========================================================================
    BEGIN
      SELECT A.TIPO_ARQUIVO_ITEM ,
        B.CODIGO_ARQUIVO ,
        B.NOME_ARQUIVO ,
        B.DATA_INCLUSAO ,
        C.CODIGO_PROCESSO
      INTO vTIPO_ARQUIVO_ITEM ,
        vCODIGO_ARQUIVO_ATUAL ,
        vNOME_ARQUIVO_ATUAL ,
        vDATA_INCLUSAO_ATUAL ,
        vCODIGO_PROCESSO_ATUAL
      FROM TB_ARQUIVO_ITEM A
      LEFT JOIN TB_ARQUIVO B
      ON B.CODIGO_ARQUIVO = A.CODIGO_ARQUIVO
      LEFT JOIN TB_PROCESSO C
      ON C.CODIGO_PROCESSO      = B.CODIGO_PROCESSO
      WHERE CODIGO_ARQUIVO_ITEM = PCODIGO_ARQUIVO_ITEM;
    END;
    ---========================================================================
    -- VER SE ITEM FOI CARONA DESTE ARQUIVO - Carrega o CODIGO_ARQUIVO_ORIGM - TB_ITEM_DESBLOQUEIO
    ---========================================================================
    OPEN CursorDesbloqueios FOR SELECT CODIGO_ARQUIVO_ORIGEM FROM TB_ITEM_DESBLOQUEIO WHERE CODIGO_ARQUIVO_ITEM = PCODIGO_ARQUIVO_ITEM AND CODIGO_ARQUIVO_DESTINO = vCODIGO_ARQUIVO_ATUAL AND STATUS_DESBLOQUEIO <> 'C' ORDER BY CODIGO_ITEM_DESBLOQUEIO DESC;
    LOOP
      FETCH CursorDesbloqueios INTO VCODIGO_ARQUIVO_ORIGEM;
      EXIT
    WHEN CursorDesbloqueios%NOTFOUND;
      vTeveCarona := true; -- ITEM FOI DE CARONA
      ---========================================================================
      -- VER SE ARQUIVO ORIGEM EXISTE - Carrega QTD - TB_ARQUIVO
      ---========================================================================
      BEGIN
        SELECT B.NOME_ARQUIVO ,
          B.DATA_INCLUSAO ,
          C.CODIGO_PROCESSO
        INTO vNOME_ARQUIVO_ORIGEM ,
          vDATA_INCLUSAO_ORIGEM ,
          vCODIGO_PROCESSO_ORIGEM
        FROM TB_ARQUIVO B
        LEFT JOIN TB_PROCESSO C
        ON C.CODIGO_PROCESSO   = B.CODIGO_PROCESSO
        WHERE B.CODIGO_ARQUIVO = VCODIGO_ARQUIVO_ORIGEM;
      END;
      IF (vNOME_ARQUIVO_ORIGEM IS NOT NULL) THEN
        vExisteArqOrigem       := true; -- ARQUIVO ORIGEM EXISTE
        EXIT;
      END IF;
    END LOOP;
    CLOSE CursorDesbloqueios;
    IF (vTeveCarona) THEN
      IF (vExisteArqOrigem) THEN
        ---========================================================================
        -- VOLTAR ITEM PARA ARQUIVO ORIGEM COM STATUS '5'
        ---========================================================================
        UPDATE TB_ARQUIVO_ITEM
        SET STATUS_ARQUIVO_ITEM   = '5' ,
          CODIGO_ARQUIVO          = VCODIGO_ARQUIVO_ORIGEM
        WHERE CODIGO_ARQUIVO_ITEM = PCODIGO_ARQUIVO_ITEM;
        vVoltouArqOrigem         := true;
      ELSE
        ---========================================================================
        -- DEIXAR ITEM NO ARQUIVO ATUAL COM STATUS '5'
        ---========================================================================
        UPDATE TB_ARQUIVO_ITEM
        SET STATUS_ARQUIVO_ITEM   = '5'
        WHERE CODIGO_ARQUIVO_ITEM = PCODIGO_ARQUIVO_ITEM;
      END IF;
      IF (vVoltouArqOrigem = false) THEN
        --=================================================================================================
        -- Logando registro de restauraï¿½ï¿½o de captura
        --=================================================================================================
        PR_LOG_DB ( PDESCRICAO_LOG => NULL , PTIPO_ORIGEM => 'PR_ITEM_CARONA_D' , PCODIGO_ORIGEM => vCODIGO_PROCESSO_ATUAL -- LOG PROCESSO ATUAL
        , PDESCRICAOGRANDE_LOG => 'CAPTURA DESFEITA - Item de CARONA volta a ficar DESBLOQUEADO no mesmo PROCESSO, arquivo origem nao encontrado ' || ' Tipo Registro: [' || vTIPO_ARQUIVO_ITEM || '] Processo: [' || vCODIGO_PROCESSO_ATUAL || '] Data Arq: [' || vDATA_INCLUSAO_ATUAL || '] ID [' || PCODIGO_ARQUIVO_ITEM || '] Arquivo Origem: [' || vCODIGO_ARQUIVO_ORIGEM || '-' || TRIM(vNOME_ARQUIVO_ORIGEM) || ']' );
      ELSE
        --=================================================================================================
        -- Registrando no DESBLOQUEIO que houve retorno do ATUAL para ORIGEM
        --=================================================================================================
        PR_ITEM_DESBLOQUEIO_S ( pCODIGO_ITEM_DESBLOQUEIO => NULL , pCODIGO_ARQUIVO_ORIGEM => vCODIGO_ARQUIVO_ATUAL , pCODIGO_ARQUIVO_DESTINO => vCODIGO_ARQUIVO_ORIGEM , pCODIGO_ARQUIVO_ITEM => PCODIGO_ARQUIVO_ITEM , retornarRegistro => 'N' , retcur => vretcur );
        --=================================================================================================
        -- Logando registro de restauraï¿½ï¿½o de captura processo atual
        --=================================================================================================
        PR_LOG_DB ( PDESCRICAO_LOG => NULL , PTIPO_ORIGEM => 'PR_ITEM_CARONA_D' , PCODIGO_ORIGEM => vCODIGO_PROCESSO_ATUAL -- LOG PROCESSO ATUAL
        , PDESCRICAOGRANDE_LOG => 'CAPTURA DESFEITA - Devolvendo item para ORIGEM como DESBLOQUEADO ' || ' Tipo Registro: [' || vTIPO_ARQUIVO_ITEM || '] Processo Origem: [' || vCODIGO_PROCESSO_ORIGEM || '] Data Arq Origem: [' || vDATA_INCLUSAO_ORIGEM || '] ID [' || PCODIGO_ARQUIVO_ITEM || '] Arquivo Origem: [' || vCODIGO_ARQUIVO_ORIGEM || '-' || TRIM(vNOME_ARQUIVO_ORIGEM) || ']' );
        --=================================================================================================
        -- Logando registro de restauraï¿½ï¿½o de captura processo origem
        --=================================================================================================
        PR_LOG_DB ( PDESCRICAO_LOG => NULL , PTIPO_ORIGEM => 'PR_ITEM_CARONA_D' , PCODIGO_ORIGEM => vCODIGO_PROCESSO_ORIGEM -- LOG PROCESSO ORIGEM
        , PDESCRICAOGRANDE_LOG => 'CAPTURA DESFEITA - Recebendo item do DESTINO como DESBLOQUEADO ' || ' Tipo Registro: [' || vTIPO_ARQUIVO_ITEM || '] Processo Destino: [' || vCODIGO_PROCESSO_ATUAL || '] Data Arq Destino: [' || vDATA_INCLUSAO_ATUAL || '] ID [' || PCODIGO_ARQUIVO_ITEM || '] Arquivo Destino: [' || vCODIGO_ARQUIVO_ATUAL || '-' || TRIM(vNOME_ARQUIVO_ATUAL) || ']' );
      END IF;
    END IF;
    ---========================================================================
    -- DELETAR O ITEM DA ARQUIVO ITEM DO ATUAL
    ---========================================================================
    IF ( vTeveCarona = false ) THEN
      BEGIN
        DELETE
        FROM TB_ARQUIVO_ITEM
        WHERE CODIGO_ARQUIVO_ITEM = PCODIGO_ARQUIVO_ITEM
        AND CODIGO_ARQUIVO        = vCODIGO_ARQUIVO_ATUAL;
      END;
      VQTD := 1;
    ELSE
      VQTD := 0;
    END IF;
    ---========================================================================
    -- PARA ATUALIZAR STATUS BLOQUEIO DO PROCESSO
    ---========================================================================
    PCODIGO_PROCESSO_ATUAL  := VCODIGO_PROCESSO_ATUAL;
    PCODIGO_PROCESSO_ORIGEM := VCODIGO_PROCESSO_ORIGEM;
    PQTD                    := VQTD;
  END PR_ITEM_CARONA_D;
--=============================================================================
  PROCEDURE PR_PROCESSO_STATUS_BLOQUEIO_U(
      PCODIGO_PROCESSO NUMBER := NULL) --1452
  AS
    vQTD             NUMBER;
    VCODIGO_PROCESSO NUMBER;
  BEGIN
    vQTD                 :=0;
    VCODIGO_PROCESSO     := NVL(PCODIGO_PROCESSO,0);
    IF (VCODIGO_PROCESSO <> 0) THEN
      SELECT COUNT(1)
      INTO vQTD
      FROM TB_ARQUIVO_ITEM A
      LEFT JOIN TB_ARQUIVO B
      ON B.CODIGO_ARQUIVO = A.CODIGO_ARQUIVO
      LEFT JOIN TB_PROCESSO C
      ON C.CODIGO_PROCESSO       = B.CODIGO_PROCESSO
      WHERE C.CODIGO_PROCESSO    = PCODIGO_PROCESSO
      AND A.STATUS_ARQUIVO_ITEM IN('3','5');
      --====================================================================
      UPDATE TB_PROCESSO
      SET STATUS_BLOQUEIO =
        CASE
          WHEN NVL(vQTD,0) > 0
          THEN 1
          ELSE 0
        END
      WHERE CODIGO_PROCESSO = PCODIGO_PROCESSO;
    END IF;
  END PR_PROCESSO_STATUS_BLOQUEIO_U;
--==================================================================================================================
  PROCEDURE PR_TAXA_RECEBIMENTO_S(
      pCODIGO_TAXA_RECEBIMENTO NUMBER   := NULL,
      pCODIGO_EMPRESA_GRUPO    NUMBER   := NULL,
      pCODIGO_REFERENCIA       VARCHAR2 := NULL,
      pCODIGO_MEIO_PAGAMENTO   NUMBER   := NULL,
      pDATA_FIM                DATE     := NULL,
      pDATA_INICIO             DATE     := NULL,
      pPERCENTUAL_RECEBIMENTO  NUMBER   := NULL,
      pVALOR_RECEBIMENTO       NUMBER   := NULL,
      retornarRegistro         CHAR     := 'N',
      retcur OUT TP_REFCURSOR )
  AS
    vCODIGO_TAXA_RECEBIMENTO NUMBER;
  BEGIN
    -- Inicializa
    vCODIGO_TAXA_RECEBIMENTO := pCODIGO_TAXA_RECEBIMENTO;
    -- Verifica se e inclusao ou alteracao
    IF vCODIGO_TAXA_RECEBIMENTO IS NULL THEN
      -- Acha proximo numero na sequencia
      SELECT SQ_TAXA_RECEBIMENTO.nextval
      INTO vCODIGO_TAXA_RECEBIMENTO
      FROM dual;
      -- Insere o registro
      INSERT
      INTO TB_TAXA_RECEBIMENTO
        (
          CODIGO_TAXA_RECEBIMENTO,
          CODIGO_EMPRESA_GRUPO,
          CODIGO_REFERENCIA,
          CODIGO_MEIO_PAGAMENTO,
          DATA_INICIO,
          DATA_FIM,
          VALOR_RECEBIMENTO,
          PERCENTUAL_RECEBIMENTO,
          STATUS_TAXA_RECEBIMENTO,
          DATA_ULTIMA_ALTERACAO
        )
        VALUES
        (
          vCODIGO_TAXA_RECEBIMENTO,
          pCODIGO_EMPRESA_GRUPO,
          pCODIGO_REFERENCIA,
          pCODIGO_MEIO_PAGAMENTO,
          pDATA_INICIO,
          pDATA_FIM,
          pVALOR_RECEBIMENTO,
          pPERCENTUAL_RECEBIMENTO,
          0,
          SYSDATE
        );
    ELSE
      -- Atualiza o registro
      UPDATE TB_TAXA_RECEBIMENTO
      SET CODIGO_EMPRESA_GRUPO      = NVL(pCODIGO_EMPRESA_GRUPO, CODIGO_EMPRESA_GRUPO) ,
        CODIGO_REFERENCIA           = NVL(pCODIGO_REFERENCIA, CODIGO_REFERENCIA) ,
        CODIGO_MEIO_PAGAMENTO       = NVL(pCODIGO_MEIO_PAGAMENTO, CODIGO_MEIO_PAGAMENTO) ,
        DATA_INICIO                 = NVL(pDATA_INICIO, DATA_INICIO) ,
        DATA_FIM                    = NVL(pDATA_FIM, DATA_FIM) ,
        VALOR_RECEBIMENTO           = NVL(pVALOR_RECEBIMENTO, VALOR_RECEBIMENTO) ,
        PERCENTUAL_RECEBIMENTO      = NVL(pPERCENTUAL_RECEBIMENTO, PERCENTUAL_RECEBIMENTO) ,
        STATUS_TAXA_RECEBIMENTO     = 0 ,
        DATA_ULTIMA_ALTERACAO       = SYSDATE
      WHERE CODIGO_TAXA_RECEBIMENTO = vCODIGO_TAXA_RECEBIMENTO;
    END IF;
    -- Verifica se deve retornar o registro
    IF retornarRegistro = 'S' THEN
      PR_TAXA_RECEBIMENTO_B( pCODIGO_TAXA_RECEBIMENTO => vCODIGO_TAXA_RECEBIMENTO, retcur => retcur);
    END IF;
  END PR_TAXA_RECEBIMENTO_S;
--==================================================================================================================
  PROCEDURE PR_TAXA_RECEBIMENTO_B(
      pCODIGO_TAXA_RECEBIMENTO NUMBER,
      retcur OUT TP_REFCURSOR)
  AS
  BEGIN
    -- Efetua a busca
    OPEN retcur FOR
  WITH Referencia AS
    (SELECT a.VALOR CODIGO_REFERENCIA,
      a.NOME_LISTA_ITEM DESCRICAO_REFERENCIA
    FROM TB_LISTA_ITEM a
    INNER JOIN TB_LISTA b
    ON a.CODIGO_LISTA  = b.CODIGO_LISTA
    WHERE b.NOME_LISTA = 'Referencia'
    ),
    MeioPagamento AS
    (SELECT a.VALOR CODIGO_MEIO_PAGAMENTO,
      a.NOME_LISTA_ITEM DESCRICAO_MEIO_PAGAMENTO
    FROM TB_LISTA_ITEM a
    INNER JOIN TB_LISTA b
    ON a.CODIGO_LISTA  = b.CODIGO_LISTA
    WHERE b.NOME_LISTA = 'MeioPagamento'
    )
  SELECT A.CODIGO_TAXA_RECEBIMENTO ,
    A.CODIGO_EMPRESA_GRUPO ,
    A.CODIGO_REFERENCIA ,
    A.CODIGO_MEIO_PAGAMENTO ,
    --    TO_CHAR(A.DATA_INICIO, 'DD/MM/YYYY') DATA_INICIO ,
    --    TO_CHAR(A.DATA_FIM, 'DD/MM/YYYY') DATA_FIM ,
    A.DATA_INICIO,
    A.DATA_FIM,
    A.VALOR_RECEBIMENTO ,
    A.PERCENTUAL_RECEBIMENTO ,
    B.NOME_EMPRESA_GRUPO ,
    C.DESCRICAO_REFERENCIA ,
    D.DESCRICAO_MEIO_PAGAMENTO
  FROM TB_TAXA_RECEBIMENTO A
  INNER JOIN TB_EMPRESA_GRUPO B
  ON A.CODIGO_EMPRESA_GRUPO = B.CODIGO_EMPRESA_GRUPO
  INNER JOIN Referencia C
  ON A.CODIGO_REFERENCIA = C.CODIGO_REFERENCIA
  INNER JOIN MeioPagamento D
  ON CAST(A.CODIGO_MEIO_PAGAMENTO AS VARCHAR(100)) = D.CODIGO_MEIO_PAGAMENTO
  WHERE A.STATUS_TAXA_RECEBIMENTO                  = 0
  AND A.CODIGO_TAXA_RECEBIMENTO                    = pCODIGO_TAXA_RECEBIMENTO;
END PR_TAXA_RECEBIMENTO_B;
--==================================================================================================================
  PROCEDURE PR_TAXA_RECEBIMENTO_L(
      pCODIGO_TAXA_RECEBIMENTO NUMBER   := NULL,
      pCODIGO_EMPRESA_GRUPO    NUMBER   := NULL,
      pCODIGO_REFERENCIA       VARCHAR2 := NULL,
      pCODIGO_MEIO_PAGAMENTO   NUMBER   := NULL,
      pDATA_INICIO             DATE     := NULL,
      pDATA_FIM                DATE     := NULL,
      maxLinhas                INT      :=0,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(2000);
  BEGIN
    -- Monta a query dinamica
    vQuery :=
    '        
with  Referencia as            
( SELECT a.VALOR CODIGO_REFERENCIA, a.NOME_LISTA_ITEM DESCRICAO_REFERENCIA      
FROM TB_LISTA_ITEM a      
INNER JOIN TB_LISTA b      
on a.CODIGO_LISTA = b.CODIGO_LISTA      
WHERE b.NOME_LISTA = ''Referencia''    
),    
MeioPagamento AS    
( SELECT a.VALOR CODIGO_MEIO_PAGAMENTO, a.NOME_LISTA_ITEM DESCRICAO_MEIO_PAGAMENTO      
FROM TB_LISTA_ITEM a      
INNER JOIN TB_LISTA b      
on a.CODIGO_LISTA = b.CODIGO_LISTA      
WHERE b.NOME_LISTA = ''MeioPagamento''    
)

select  A.CODIGO_TAXA_RECEBIMENTO        
,       A.CODIGO_EMPRESA_GRUPO        
,       A.CODIGO_REFERENCIA        
,       A.CODIGO_MEIO_PAGAMENTO        
,       A.DATA_INICIO
,       A.DATA_FIM
,       A.VALOR_RECEBIMENTO        
,       A.PERCENTUAL_RECEBIMENTO        
,       B.NOME_EMPRESA_GRUPO        
,       C.DESCRICAO_REFERENCIA        
,       D.DESCRICAO_MEIO_PAGAMENTO        
from    TB_TAXA_RECEBIMENTO A        
inner join                 
TB_EMPRESA_GRUPO B        
on      A.CODIGO_EMPRESA_GRUPO    = B.CODIGO_EMPRESA_GRUPO        
inner join                 
Referencia C        
on      A.CODIGO_REFERENCIA       = C.CODIGO_REFERENCIA        
inner join                 
MeioPagamento D        
ON CAST(A.CODIGO_MEIO_PAGAMENTO AS VARCHAR(100))      = D.CODIGO_MEIO_PAGAMENTO
where   A.CODIGO_TAXA_RECEBIMENTO <> '
    || NVL(pCODIGO_TAXA_RECEBIMENTO, 0);
    -- Filtros
    IF pCODIGO_EMPRESA_GRUPO IS NOT NULL THEN
      vQuery                 := vQuery || ' AND A.CODIGO_EMPRESA_GRUPO = ' || pCODIGO_EMPRESA_GRUPO ;
    END IF;
    IF pCODIGO_REFERENCIA IS NOT NULL THEN
      vQuery              := vQuery || ' AND A.CODIGO_REFERENCIA = ''' || pCODIGO_REFERENCIA || '''';
    END IF;
    IF pCODIGO_MEIO_PAGAMENTO IS NOT NULL THEN
      vQuery                  := vQuery || ' AND A.CODIGO_MEIO_PAGAMENTO = ' || pCODIGO_MEIO_PAGAMENTO ;
    END IF;
    IF pDATA_INICIO IS NOT NULL AND pDATA_FIM IS NOT NULL THEN
      vQuery        := vQuery || ' AND (TO_DATE(''' || TO_CHAR(pDATA_INICIO, 'YYYY-MM-DD') || ''', ''YYYY-MM-DD hh24:mi:ss'') between A.DATA_INICIO AND A.DATA_FIM';
      vQuery        := vQuery || ' OR  TO_DATE(''' || TO_CHAR(pDATA_FIM, 'YYYY-MM-DD hh24:mi:ss') || ''', ''YYYY-MM-DD hh24:mi:ss'') between A.DATA_INICIO AND A.DATA_FIM)';
    ELSE
      vQuery          := vQuery || ' AND A.STATUS_TAXA_RECEBIMENTO = 0';
      IF pDATA_INICIO IS NOT NULL THEN
        vQuery        := vQuery || ' AND A.DATA_INICIO >= TO_DATE(''' || TO_CHAR(pDATA_INICIO, 'YYYY-MM-DD') || ''', ''YYYY-MM-DD hh24:mi:ss'')';
      END IF;
      IF pDATA_FIM IS NOT NULL THEN
        vQuery     := vQuery || ' AND A.DATA_FIM <= TO_DATE(''' || TO_CHAR(pDATA_FIM, 'YYYY-MM-DD hh24:mi:ss') || ''', ''YYYY-MM-DD hh24:mi:ss'')';
      END IF;
    END IF;
    -- Limitador
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' AND rownum <= ' || maxLinhas;
    END IF;
    vQuery := vQuery || ' ORDER BY                
A.CODIGO_EMPRESA_GRUPO        
,       A.CODIGO_REFERENCIA        
,       A.CODIGO_MEIO_PAGAMENTO        
,       A.DATA_INICIO';
    -- Efetua a busca
    OPEN retcur FOR vQuery;
  END PR_TAXA_RECEBIMENTO_L;
--==================================================================================================================
  PROCEDURE PR_TAXA_RECEBIMENTO_R(
      pCODIGO_TAXA_RECEBIMENTO NUMBER )
  AS
  BEGIN
    -- Remove o registro
    UPDATE TB_TAXA_RECEBIMENTO
    SET STATUS_TAXA_RECEBIMENTO   = 1 ,
      DATA_ULTIMA_ALTERACAO       = SYSDATE
    WHERE CODIGO_TAXA_RECEBIMENTO = pCODIGO_TAXA_RECEBIMENTO;
  END PR_TAXA_RECEBIMENTO_R;
--==================================================================================================================
  PROCEDURE PR_RELATORIO_RECEBIMENTO_L(
      pCODIGO_EMPRESA_GRUPO  NUMBER   := NULL,
      pCODIGO_REFERENCIA     VARCHAR2 := NULL,
      pCODIGO_MEIO_PAGAMENTO NUMBER   := NULL,
      pDATA_INICIO           DATE     := NULL,
      pDATA_FIM              DATE     := NULL,
      maxLinhas              INT      :=0,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(5000);
    vCodigoProduto TB_PRODUTO.CODIGO_PRODUTO%TYPE;
  BEGIN
    SELECT CODIGO_PRODUTO
    INTO vCodigoProduto
    FROM TB_PRODUTO
    WHERE NOME_PRODUTO = 'RECTO';
    -- Monta a query dinamica
    vQuery :=
    '            
with  Referencia as                
( SELECT a.VALOR CODIGO_REFERENCIA, a.NOME_LISTA_ITEM DESCRICAO_REFERENCIA      
FROM TB_LISTA_ITEM a      
INNER JOIN TB_LISTA b      
on a.CODIGO_LISTA = b.CODIGO_LISTA      
WHERE b.NOME_LISTA = ''Referencia''    
),    
MeioPagamento AS    
( SELECT a.VALOR CODIGO_MEIO_PAGAMENTO, a.NOME_LISTA_ITEM DESCRICAO_MEIO_PAGAMENTO      
FROM TB_LISTA_ITEM a      
INNER JOIN TB_LISTA b      
on a.CODIGO_LISTA = b.CODIGO_LISTA      
WHERE b.NOME_LISTA = ''MeioPagamento''    
)                      

SELECT  CODIGO_EMPRESA_GRUPO,                    
REFERENCIA,                    
MEIO_PAGAMENTO,                    
NOME_EMPRESA_GRUPO,                    
DESCRICAO_REFERENCIA,                    
DESCRICAO_MEIO_PAGAMENTO,                    
DATA_REPASSE_CALCULADA,                    
CNPJ,                    
RAZAO_SOCIAL,                    
QTDE_RECEBIMENTO,                    
VALOR_RECEBIMENTO,                    
TAXA_RECEBIMENTO,                    
TIPO_TAXA_RECEBIMENTO,                    
CASE TIPO_TAXA_RECEBIMENTO WHEN ''%'' THEN VALOR_RECEBIMENTO/100 ELSE QTDE_RECEBIMENTO END * TAXA_RECEBIMENTO VALOR_TAXA_RECEBIMENTO            
FROM(SELECT  TS.CODIGO_EMPRESA_GRUPO,                    
TT.REFERENCIA,                    
TP.MEIO_PAGAMENTO,                    
E.NOME_EMPRESA_GRUPO,                    
R.DESCRICAO_REFERENCIA,                    
M.DESCRICAO_MEIO_PAGAMENTO,                    
TT.DATA_REPASSE_CALCULADA,                    
TE.CNPJ,                    
TE.RAZAO_SOCIAL,                    
COUNT(DISTINCT NSU_HOST) QTDE_RECEBIMENTO,                    
SUM(TP.VALOR_PAGAMENTO * -1) VALOR_RECEBIMENTO,                    
MAX(CASE TR.PERCENTUAL_RECEBIMENTO WHEN 0 THEN TR.VALOR_RECEBIMENTO ELSE TR.PERCENTUAL_RECEBIMENTO END) TAXA_RECEBIMENTO,                    
MAX(CASE NVL(TR.PERCENTUAL_RECEBIMENTO, -1) WHEN -1 THEN NULL WHEN 0 THEN ''R$ '' ELSE ''%'' END) TIPO_TAXA_RECEBIMENTO            
FROM    TB_TRANSACAO TT            
INNER JOIN                     
TB_ESTABELECIMENTO TE            
ON      TE.CODIGO_ESTABELECIMENTO = TT.CODIGO_ESTABELECIMENTO            
INNER JOIN                     
TB_EMPRESA_SUBGRUPO TS            
ON      TS.CODIGO_EMPRESA_SUBGRUPO = TE.CODIGO_EMPRESA_SUBGRUPO            
INNER JOIN                     
TB_EMPRESA_GRUPO E            
ON      E.CODIGO_EMPRESA_GRUPO     = TS.CODIGO_EMPRESA_GRUPO            
INNER JOIN                     
TB_TRANSACAO_PAGAMENTO TP            
ON      TP.CODIGO_TRANSACAO = TT.CODIGO_TRANSACAO            
LEFT OUTER JOIN                     
TB_TAXA_RECEBIMENTO TR            
ON      CAST(TR.CODIGO_MEIO_PAGAMENTO AS CHAR(1))   = TP.MEIO_PAGAMENTO            
AND     TR.CODIGO_REFERENCIA       = TT.REFERENCIA            
AND     TR.CODIGO_EMPRESA_GRUPO    = TS.CODIGO_EMPRESA_GRUPO            
AND     TR.STATUS_TAXA_RECEBIMENTO = 0            
AND     TT.DATA_REPASSE_CALCULADA BETWEEN TR.DATA_INICIO AND TR.DATA_FIM            
LEFT OUTER JOIN Referencia R            
ON      R.CODIGO_REFERENCIA = TT.REFERENCIA            
LEFT OUTER JOIN MeioPagamento M            
ON CAST(M.CODIGO_MEIO_PAGAMENTO AS CHAR(1))      = TP.MEIO_PAGAMENTO
WHERE   TT.TIPO_TRANSACAO   = ''CP''            
AND     TT.CODIGO_PRODUTO = '
    || vCodigoProduto || '            
AND     TT.STATUS_TRANSACAO < 2';
    -- Filtros
    IF pCODIGO_EMPRESA_GRUPO IS NOT NULL THEN
      vQuery                 := vQuery || ' AND TS.CODIGO_EMPRESA_GRUPO = ' || pCODIGO_EMPRESA_GRUPO ;
    END IF;
    IF pCODIGO_REFERENCIA IS NOT NULL THEN
      vQuery              := vQuery || ' AND TT.REFERENCIA = ''' || pCODIGO_REFERENCIA || '''';
    END IF;
    IF pCODIGO_MEIO_PAGAMENTO IS NOT NULL THEN
      vQuery                  := vQuery || ' AND TP.MEIO_PAGAMENTO = ' || pCODIGO_MEIO_PAGAMENTO ;
    END IF;
    IF pDATA_INICIO IS NOT NULL THEN
      vQuery        := vQuery || ' AND TT.DATA_REPASSE_CALCULADA >= TO_DATE(''' || TO_CHAR(pDATA_INICIO, 'YYYY-MM-DD') || ''', ''YYYY-MM-DD hh24:mi:ss'')';
    END IF;
    IF pDATA_FIM IS NOT NULL THEN
      vQuery     := vQuery || ' AND TT.DATA_REPASSE_CALCULADA <= TO_DATE(''' || TO_CHAR(pDATA_FIM, 'YYYY-MM-DD hh24:mi:ss') || ''', ''YYYY-MM-DD hh24:mi:ss'')';
    END IF;
    -- Limitador
    IF maxLinhas > 0 THEN
      vQuery    := vQuery || ' AND rownum <= ' || maxLinhas;
    END IF;
    vQuery := vQuery || '          
GROUP BY                   
TS.CODIGO_EMPRESA_GRUPO,                  
TT.REFERENCIA,                  
TP.MEIO_PAGAMENTO,                  
E.NOME_EMPRESA_GRUPO,                  
R.DESCRICAO_REFERENCIA,                  
M.DESCRICAO_MEIO_PAGAMENTO,                  
TT.DATA_REPASSE_CALCULADA,                  
TE.CNPJ,                  
TE.RAZAO_SOCIAL) RELATORIO          
ORDER BY                   
NOME_EMPRESA_GRUPO,                  
DESCRICAO_REFERENCIA,                  
DESCRICAO_MEIO_PAGAMENTO,                  
RAZAO_SOCIAL,                  
DATA_REPASSE_CALCULADA';
    PR_LOG_DB ( PDESCRICAO_LOG => NULL , PTIPO_ORIGEM => 'PR_RELATORIO_RECEBIMENTO_L' , PCODIGO_ORIGEM => NULL , PDESCRICAOGRANDE_LOG => vQuery );
    -- Efetua a busca
    OPEN retcur FOR vQuery;
  END PR_RELATORIO_RECEBIMENTO_L;
--==================================================================================================================
--==================================================================================================================
/* TIVE DE USAR PROCEDURE PORQUE A ROTINA DO C# nao SUPORTA FUNCTION */
--==================================================================================================================
  PROCEDURE PR_DIA_UTIL(
      pDATA         IN DATE,
      pDIAS         IN NUMBER := 0,
      pNOME_PRODUTO IN STRING, --1495
      PDATA_RETORNO OUT DATE )
  AS
    vFERIADO      NUMBER := 0;
    vFINALSEMANA  CHAR   := '';
    vDIAS_FIM     NUMBER := 0;
    vDATA_RETORNO DATE;
    vDATA_VALIDA  NUMBER := 0;
  BEGIN
    IF pNOME_PRODUTO = 'RECTO' THEN --1495
      IF pDIAS       = 0 THEN
        vDIAS_FIM   := 0;
      ELSE
        vDIAS_FIM := pDIAS - 1;
      END IF;
      vDATA_RETORNO := pDATA;
      FOR I IN 0..vDIAS_FIM
      LOOP
        IF pDIAS         > 0 THEN
          vDATA_RETORNO := vDATA_RETORNO + 1;
        END IF;
        vDATA_VALIDA       := 0;
        WHILE (vDATA_VALIDA = 0)
        LOOP
          vDATA_VALIDA := 1;
          PR_DIA_SEMANA(vDATA_RETORNO, vFINALSEMANA);
          SELECT COUNT(1)
          INTO vFERIADO
          FROM TB_DIA_NAO_UTIL
          WHERE TRUNC(DATA_DIA_NAO_UTIL) = vDATA_RETORNO
          AND ATIVO                      = 'S'; -- verifica se dia util
          IF (vFERIADO                   > 0 OR vFINALSEMANA IN ('1', '7')) THEN
            vDATA_VALIDA                := 0;
            vDATA_RETORNO               := TRUNC(vDATA_RETORNO + 1);
          END IF;
        END LOOP;
      END LOOP;
    ELSE
      vDATA_RETORNO      := pDATA + pDIAS;
      vDATA_VALIDA       := 0;
      WHILE (vDATA_VALIDA = 0)
      LOOP
        vDATA_VALIDA := 1;
        PR_DIA_SEMANA(vDATA_RETORNO, vFINALSEMANA);
        SELECT COUNT(1)
        INTO vFERIADO
        FROM TB_DIA_NAO_UTIL
        WHERE TRUNC(DATA_DIA_NAO_UTIL) = vDATA_RETORNO
        AND ATIVO                      = 'S'; -- verifica se dia util
        IF (vFERIADO                   > 0 OR vFINALSEMANA IN ('1', '7')) THEN
          vDATA_VALIDA                := 0;
          vDATA_RETORNO               := TRUNC(vDATA_RETORNO + 1);
        END IF;
      END LOOP;
    END IF;
    pDATA_RETORNO := vDATA_RETORNO;
  END PR_DIA_UTIL;
  PROCEDURE PR_DIA_SEMANA(
      pDATA IN DATE,
      PDIA_SEMANA OUT CHAR )
  AS
  BEGIN
    PDIA_SEMANA := TO_CHAR(to_date(pDATA), 'd');
  END PR_DIA_SEMANA;
--==================================================================================================================
-- PR_TAXA_REL_INTERCHANGE_L      JIRA SCF-1521
--==================================================================================================================
  PROCEDURE PR_TAXA_REL_INTERCHANGE_L(
      pDATA_MOVIMENTO_INICIO DATE := NULL,
      pDATA_MOVIMENTO_FIM    DATE := NULL,
      retcur OUT TP_REFCURSOR )
  AS
    vQuery VARCHAR2(5000) := '';
  BEGIN
    vQuery :=
    'WITH q_plano as (        
SELECT distinct a.CHAVE3, C.CODIGO_PLANO_TSYS, c.CODIGO_PLANO        
FROM            
TB_TRANSACAO A        
LEFT JOIN            
TB_TRANSACAO_VENDA B ON B.CODIGO_TRANSACAO = A.CODIGO_TRANSACAO        
LEFT JOIN            
TB_PLANO C ON C.CODIGO_PLANO = B.CODIGO_PLANO        
WHERE            
TIPO_TRANSACAO = ''CV''    
), q_cv AS (        
SELECT distinct a.CODIGO_ESTABELECIMENTO, a.REFERENCIA, a.DATA_MOVIMENTO, a.TIPO_TRANSACAO, a.CODIGO_PRODUTO,         
C.CODIGO_PLANO_TSYS, b.CODIGO_PLANO, b.CODIGO_AUTORIZACAO, b.NUMERO_PARCELA_TOTAL, A.DATA_TRANSACAO, A.NSU_HOST, b.NUMERO_CONTA_CLIENTE,        
b.VALOR_VENDA VALOR_BRUTO, b.VALOR_DESCONTO, b.VALOR_LIQUIDO        

FROM            
TB_TRANSACAO A        
LEFT JOIN            
TB_TRANSACAO_VENDA B ON B.CODIGO_TRANSACAO = A.CODIGO_TRANSACAO        
LEFT JOIN            
TB_PLANO C ON C.CODIGO_PLANO = B.CODIGO_PLANO        
WHERE            
TIPO_TRANSACAO = ''CV''            
AND   DATA_MOVIMENTO BETWEEN to_date('''
    || TO_CHAR(pDATA_MOVIMENTO_INICIO, 'yyyy-MM-dd') || ''', ''yyyy-MM-dd'')            
AND   to_date(''' || TO_CHAR(pDATA_MOVIMENTO_FIM, 'yyyy-MM-dd') || ' 23:59:59' ||
    ''', ''yyyy-MM-dd HH24:MI:SS'')    
), q_av AS (        
SELECT distinct b.CODIGO_ESTABELECIMENTO, b.REFERENCIA, b.DATA_MOVIMENTO, b.TIPO_TRANSACAO, b.CODIGO_PRODUTO,        
c.CODIGO_PLANO_TSYS, c.CODIGO_PLANO, a.CODIGO_AUTORIZACAO_ORIGINAL, a.NUMERO_PARCELA_TOTAL, B.DATA_TRANSACAO,         
b.NSU_HOST, a.NUMERO_CONTA_CLIENTE, a.vALOR_ANULACAO VALOR_BRUTO, a.VALOR_DESCONTO, a.VALOR_LIQUIDO        

FROM             
TB_TRANSACAO_ANULACAO_VENDA A        
INNER JOIN            
TB_TRANSACAO B ON A.CODIGO_TRANSACAO = B.CODIGO_TRANSACAO        

left join q_plano c        
on c.CHAVE3 = (                         
TO_CHAR(A.DATA_TRANSACAO_ORIGINAL, ''YYYYMMDD'')                      
|| ''000000''                       
|| LPAD(TO_CHAR(B.CODIGO_ESTABELECIMENTO), 6, ''0'')                      
|| ''CV''                      
|| LPAD(TO_CHAR(A.NSU_HOST_ORIGINAL), 12, ''0'')                      
|| LPAD(TO_CHAR(A.CODIGO_AUTORIZACAO_ORIGINAL), 12, ''0'')                      
|| LPAD(TO_CHAR(A.NUMERO_PARCELA), 4, ''0'')                       
)        

WHERE            
TIPO_TRANSACAO = ''AV''            
AND   DATA_MOVIMENTO BETWEEN to_date('''
    || TO_CHAR(pDATA_MOVIMENTO_INICIO, 'yyyy-MM-dd') || ''', ''yyyy-MM-dd'')            
AND   to_date(''' || TO_CHAR(pDATA_MOVIMENTO_FIM, 'yyyy-MM-dd') || ' 23:59:59' ||
    ''', ''yyyy-MM-dd HH24:MI:SS'')    
), q_transacao AS (        
SELECT            
*        
FROM            
q_cv        
UNION ALL        
SELECT            
*        
FROM            
q_av    
), q_relatorio AS (        

SELECT         
d.NOME_EMPRESA_GRUPO,        
f.NOME_LISTA_ITEM,        
a.DATA_MOVIMENTO,        
a.TIPO_TRANSACAO,        
i.CODIGO_PRODUTO_TSYS CODIGO_PRODUTO,        
a.CODIGO_PLANO_TSYS,        
b.CNPJ,        
a.CODIGO_AUTORIZACAO,        
a.NUMERO_PARCELA_TOTAL,        
a.DATA_TRANSACAO,        
a.NSU_HOST,        
a.NUMERO_CONTA_CLIENTE,        
a.VALOR_BRUTO,        
a.VALOR_DESCONTO,        
a.VALOR_LIQUIDO,        
ROUND(ABS(a.VALOR_DESCONTO) / ABS(a.VALOR_BRUTO) *100, 5) as TAXA_TSYS,       
CASE             
WHEN g.TRATAR_VALOR = ''A''            
THEN NVL(ROUND((a.VALOR_BRUTO) * (g.PERCENTUAL_TAXA_INTERCHANGE)/100, 2),0) * -1            
WHEN g.TRATAR_VALOR = ''T''            
THEN NVL(TRUNC((a.VALOR_BRUTO )* (g.PERCENTUAL_TAXA_INTERCHANGE)/100, 2),0) * -1             
END AS VALOR_COMISSAO_SCF,        
g.PERCENTUAL_TAXA_INTERCHANGE,        
g.DATA_INICIO,        
g.DATA_FIM,        
CASE             
WHEN g.TRATAR_VALOR = ''A''             
THEN NVL(ROUND(ROUND((a.VALOR_BRUTO * g.PERCENTUAL_TAXA_INTERCHANGE/100) * -1, 2) - a.VALOR_DESCONTO, 2),a.VALOR_DESCONTO)            
WHEN g.TRATAR_VALOR = ''T''            
THEN NVL(ROUND(TRUNC((a.VALOR_BRUTO * g.PERCENTUAL_TAXA_INTERCHANGE/100) * -1, 2) - a.VALOR_DESCONTO, 2),a.VALOR_DESCONTO)            
ELSE a.VALOR_DESCONTO            
END AS DIFERENCA            
FROM            
q_transacao a        
INNER JOIN TB_ESTABELECIMENTO b ON b.CODIGO_ESTABELECIMENTO = a.CODIGO_ESTABELECIMENTO        
INNER JOIN Tb_Empresa_Subgrupo c ON c.CODIGO_EMPRESA_SUBGRUPO = b.CODIGO_EMPRESA_SUBGRUPO        
INNER JOIN TB_EMPRESA_GRUPO d ON d.CODIGO_EMPRESA_GRUPO = c.CODIGO_EMPRESA_GRUPO        
INNER JOIN TB_LISTA e ON e.NOME_LISTA =''Referencia''        
INNER JOIN TB_LISTA_ITEM f ON f.CODIGO_LISTA = e.CODIGO_LISTA and a.REFERENCIA = f.VALOR          
INNER JOIN tb_produto i on i.CODIGO_PRODUTO = a.CODIGO_PRODUTO and i.nome_produto <> ''COBAN''        
LEFT  JOIN TB_TAXA_INTERCHANGE g ON g.CODIGO_EMPRESA_GRUPO = c.CODIGO_EMPRESA_GRUPO        
AND g.REFERENCIA = f.VALOR AND g.CODIGO_PLANO = a.CODIGO_PLANO AND a.DATA_TRANSACAO BETWEEN g.DATA_INICIO AND g.DATA_FIM        
)        
select *        
from q_relatorio        
where diferenca <> 0        
order by NOME_EMPRESA_GRUPO, CNPJ, NOME_LISTA_ITEM, CODIGO_PLANO_TSYS    '
    ;
    PR_LOG_DB ( PDESCRICAO_LOG => NULL , PTIPO_ORIGEM => 'PR_TAXA_REL_INTERCHANGE_L' , PCODIGO_ORIGEM => NULL , PDESCRICAOGRANDE_LOG => vQuery );
    OPEN retcur FOR vQuery;
  END PR_TAXA_REL_INTERCHANGE_L;
END PKG_SCF;
/
