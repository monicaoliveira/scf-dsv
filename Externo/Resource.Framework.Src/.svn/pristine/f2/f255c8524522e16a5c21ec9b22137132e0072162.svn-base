﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    /// <summary>
    /// Classe base para classes de processamento de opções
    /// </summary>
    public class TelnetOptionBase
    {
        /// <summary>
        /// Faz a inicializacao
        /// </summary>
        /// <param name="telnet"></param>
        public void Initialize(Telnet telnet)
        {
            this.Telnet = telnet;
        }

        /// <summary>
        /// Referencia para a classe do Telnet
        /// </summary>
        public Telnet Telnet { get; set; }

        /// <summary>
        /// Retorna a opção que esta classe irá processar
        /// </summary>
        /// <returns></returns>
        public TelnetOptionsEnum GetOption()
        {
            return OnGetOption();
        }

        /// <summary>
        /// Método virtual para retornar a opção que a classe irá processar
        /// </summary>
        /// <returns></returns>
        protected virtual TelnetOptionsEnum OnGetOption()
        {
            return (TelnetOptionsEnum)255;
        }

        /// <summary>
        /// Solicitação para o processamento do token
        /// </summary>
        /// <param name="telnet"></param>
        /// <param name="token"></param>
        public void ProcessToken(TelnetTokenBase token)
        {
            OnProcessToken(token);
        }

        /// <summary>
        /// Método virtual para o processamento do token
        /// </summary>
        /// <param name="telnet"></param>
        /// <param name="token"></param>
        protected virtual void OnProcessToken(TelnetTokenBase token)
        {
        }
    }
}
