﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Dados
{
    /// <summary>
    /// Representa uma crítica na validação da mensagem.
    /// A crítica pode ser apenas informativa ou pode ser um erro na validação.
    /// </summary>
    [Serializable]
    public class CriticaInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da crítica
        /// </summary>
        public string CodigoCritica { get; set; }

        /// <summary>
        /// Descricao da crítica
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Informa um status da crítica
        /// </summary>
        public CriticaStatusEnum Status { get; set; }

        /// <summary>
        /// Data da crítica
        /// </summary>
        public DateTime DataCritica { get; set; }

        /// <summary>
        /// Tipo da crítica (nome da classe)
        /// </summary>
        public string TipoCritica 
        { 
            get { return this.GetType().Name; } 
        }

        /// <summary>
        /// Construtor default
        /// </summary>
        public CriticaInfo()
        {
            this.DataCritica = DateTime.Now;
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoCritica;
        }

        #endregion
    }
}
