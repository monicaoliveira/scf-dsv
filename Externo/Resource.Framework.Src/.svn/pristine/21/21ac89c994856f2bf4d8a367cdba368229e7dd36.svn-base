﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Funções auxiliares de enumeradores
    /// </summary>
    public static class EnumHelper
    {
        public static T Resolve<T>(object value)
        {
            //// Se valor for nulo, nem continua
            //if (value == null)
            //    return default(T);

            // Prepara o retorno
            object returnValue = null;
            
            // Tenta resolver pelo atributo ValorExterno
            try
            {
                returnValue = EnumHelper.Resolve<T>(typeof(ValorExternoAttribute), "Valor", value, true);
            }
            catch
            {
            }

            if (returnValue == null)
            {
                // Se não conseguiu, tenta resolver por description
                try
                {
                    returnValue = EnumHelper.Resolve<T>(typeof(DescriptionAttribute), "Description", value, true);
                }
                catch
                {
                }
            }

            // Se não conseguiu, faz um parse normal
            if (returnValue == null)
                returnValue = Enum.Parse(typeof(T), (string)value);

            // Retorna
            return (T)returnValue;
        }

        /// <summary>
        /// Resolve o valor de um enumerador através do valor de algum atributo dos campos do enumerador
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="attributeType">Tipo do atributo a ser testado em cada campo</param>
        /// <param name="propertyName">Nome da propriedade do atributo a ser testada</param>
        /// <param name="value">Valor a ser testado</param>
        /// <returns></returns>
        public static T Resolve<T>(Type attributeType, string propertyName, object value, bool throwException)
        {
            // Prepara resposta
            FieldInfo selectedField = null;
            
            // Referencia para a propriedade
            PropertyInfo property = attributeType.GetProperty(propertyName);

            // Pede os elementos do enumerador
            foreach (FieldInfo field in typeof(T).GetFields())
            {
                // Pede o atributo
                object attribute =
                    field.GetCustomAttributes(attributeType, false).FirstOrDefault();

                // Se achou o atributo...
                if (attribute != null)
                {
                    // Avalia o valor
                    if (property.GetValue(attribute, null) == null && value == null)
                    {
                        selectedField = field;
                        break;
                    }
                    else if (property.GetValue(attribute, null) != null && property.GetValue(attribute, null).Equals(value))
                    {
                        selectedField = field;
                        break;
                    }
                }
            }

            // Retorna
            if (selectedField != null)
            {
                return (T)selectedField.GetValue(typeof(T));
            }
            else
            {
                if (throwException)
                    throw new Exception(string.Format("Cannot resolve value {0} for enum type {1}", value, typeof(T).FullName));
                else
                    return default(T);
            }
        }

        /// <summary>
        /// Resolve o valor de um enumerador através do valor de algum atributo dos campos do enumerador
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="attributeType">Tipo do atributo a ser testado em cada campo</param>
        /// <param name="propertyName">Nome da propriedade do atributo a ser testada</param>
        /// <param name="value">Valor a ser testado</param>
        /// <returns></returns>
        public static T Resolve<T>(Dictionary<Type, string> atributos, object value)
        {
            // Prepara resposta
            FieldInfo selectedField = null;

            // Pede os elementos do enumerador
            foreach (FieldInfo field in typeof(T).GetFields())
            {
                // Varre os atributos solicitados
                foreach (KeyValuePair<Type, string> atributo in atributos)
                {
                    // Referencia para a propriedade
                    PropertyInfo property = atributo.Key.GetProperty(atributo.Value);

                    // Pede o atributo
                    object attribute =
                        field.GetCustomAttributes(atributo.Key, false).FirstOrDefault();

                    // Se achou o atributo...
                    if (attribute != null)
                    {
                        // Avalia o valor
                        if (property.GetValue(attribute, null).Equals(value))
                        {
                            selectedField = field;
                            break;
                        }
                    }
                }

                // Se achou o campo, sai
                if (selectedField != null)
                    break;
            }

            // Retorna
            if (selectedField != null)
            {
                // Achou, retorna o valor do campo encontrado
                return (T)selectedField.GetValue(typeof(T));
            }
            else
            {
                // Se ainda não achou, tenta resolver normalmente
                if (value is string)
                    return (T)Enum.Parse(typeof(T), (string)value);
                else
                    return (T)Enum.ToObject(typeof(T), value);
            }
        }

        /// <summary>
        /// Resolve o valor de um enumerador através do valor de algum atributo dos campos do enumerador
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="attributeType">Tipo do atributo a ser testado em cada campo</param>
        /// <param name="propertyName">Nome da propriedade do atributo a ser testada</param>
        /// <param name="value">Valor a ser testado</param>
        /// <returns></returns>
        public static object Resolve(Type tipoEnumerador, Dictionary<Type, string> atributos, object value)
        {
            // Prepara resposta
            FieldInfo selectedField = null;

            // Pede os elementos do enumerador
            foreach (FieldInfo field in tipoEnumerador.GetFields())
            {
                // Varre os atributos solicitados
                foreach (KeyValuePair<Type, string> atributo in atributos)
                {
                    // Referencia para a propriedade
                    PropertyInfo property = atributo.Key.GetProperty(atributo.Value);

                    // Pede o atributo
                    object attribute =
                        field.GetCustomAttributes(atributo.Key, false).FirstOrDefault();

                    // Se achou o atributo...
                    if (attribute != null)
                    {
                        // Pega o valor
                        object valorAtributo = property.GetValue(attribute, null);

                        // Testa por nulo
                        if (valorAtributo == null)
                        {
                            if (value == null)
                            {
                                selectedField = field;
                                break;
                            }
                        }
                        // Verifica valor normal
                        else if (valorAtributo.Equals(value))
                        {
                            selectedField = field;
                            break;
                        }
                    }
                }

                // Se achou o campo, sai
                if (selectedField != null)
                    break;
            }

            // Retorna
            if (selectedField != null)
            {
                // Achou, retorna o valor do campo encontrado
                return selectedField.GetValue(tipoEnumerador);
            }
            else
            {
                // Se ainda não achou, tenta resolver normalmente
                if (value == null)
                    return null;
                else if (value is string)
                    return Enum.Parse(tipoEnumerador, (string)value);
                else
                    return Enum.ToObject(tipoEnumerador, value);
            }
        }

        /// <summary>
        /// Se houver, retorna a descricao de um enumerador
        /// Se nao, retorna o texto do enumerador
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object GetEnumDescription(Enum value)
        {
            // Get the Description attribute value for the enum value
            FieldInfo fi = value.GetType().GetField(value.ToString());
            
            // Tenta por valor externo
            ValorExternoAttribute valorExternoAttribute = 
                (ValorExternoAttribute)fi.GetCustomAttributes(
                    typeof(ValorExternoAttribute), false).FirstOrDefault();
            if (valorExternoAttribute != null)
                return valorExternoAttribute.Valor;

            // Tenta pela descricao
            DescriptionAttribute descriptionAttribute = 
                (DescriptionAttribute)fi.GetCustomAttributes(
                    typeof(DescriptionAttribute), false).FirstOrDefault();
            if (descriptionAttribute != null)
                return descriptionAttribute.Description;

            // Não encontrou os atributos anteriores, faz o parse normal
            return value.ToString();
        }

        /// <summary>
        /// Verifica se é possível fazer uma parse dessa string para o enum especificado
        /// </summary>
        /// <typeparam name="TEnum">Enum do tipo que se deseja fazer o parse</typeparam>
        /// <param name="value">Palavra que se tentará parsear para o enum especificado</param>
        /// <returns>Verdadeiro caso seja possível fazer um parse de nome</returns>
        public static bool TryParse<TEnum>(string value)
        {
            if (value == null)
            {
                return false;
            }

            foreach (string name in Enum.GetNames(typeof(TEnum)))
            {
                if (name.ToLower() == value.ToLower())
                {
                    return true;
                }
            }

            return false;
        }
    }
}
