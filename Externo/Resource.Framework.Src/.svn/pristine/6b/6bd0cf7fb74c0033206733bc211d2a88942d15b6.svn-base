﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Conversor de data
    /// </summary>
    public class ConversorData : ConversorBase
    {
        /// <summary>
        /// Construtor default
        /// </summary>
        public ConversorData()
        {
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="formato"></param>
        public ConversorData(string formato)
        {
            this.Formato = formato;
        }

        /// <summary>
        /// Formato da data
        /// </summary>
        private string _formato = null;
        public string Formato 
        {
            get { return _formato; }
            set 
            { 
                _formato = value;
                interpretarFormato();
            }
        }

        /// <summary>
        /// Formato para geracao para string caso não seja gerado com o _cultureInfo
        /// Nos casos de datas sem separador
        /// </summary>
        private string _formatoParaString = null;

        /// <summary>
        /// Caso a conversao deva ser feita por parse, este cultureInfo
        /// tem o formato da data
        /// </summary>
        private CultureInfo _cultureInfo = null;

        // Propriedades Auxiliares
        public int PosicaoDia { get; set; }
        public int TamanhoDia { get; set; }
        public int PosicaoMes { get; set; }
        public int TamanhoMes { get; set; }
        public int PosicaoAno { get; set; }
        public int TamanhoAno { get; set; }

        /// <summary>
        /// Converte de string para objeto
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public override object OnConverterParaObjeto(BookItemCampoInfo campo, string valor)
        {
            // Prepara o retorno
            object retorno = null;

            // Faz parse pelo cultureInfo?
            if (_cultureInfo == null)
            {
                // Pega os valores
                int dia = int.Parse(valor.Substring(this.PosicaoDia, this.TamanhoDia));
                int mes = int.Parse(valor.Substring(this.PosicaoMes, this.TamanhoMes));
                int ano = int.Parse(valor.Substring(this.PosicaoAno, this.TamanhoAno));

                // Faz o parse da data
                retorno = new DateTime(ano, mes, dia);
            }
            else
            {
                // Parse pelo culture
                retorno = DateTime.Parse(valor, _cultureInfo);
            }

            // Retorna
            return retorno;
        }

        /// <summary>
        /// Converte de objeto para string
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public override string OnConverterParaString(BookItemCampoInfo campo, object valor)
        {
            // Prepara o retorno
            string retorno = null;


            
            // Apenas se tem valor
            if (valor != null)
            {
                try
                {
                    // Retorna a data no formato solicitado
                    if (_cultureInfo != null)
                        retorno = Convert.ToDateTime(valor, _cultureInfo).ToString(_cultureInfo);
                    else
                        retorno = Convert.ToDateTime(valor).ToString(_formatoParaString);
                }
                catch (Exception)
                {
                    retorno = valor.ToString();
                }
            }
            else
            {
                // Retorna string vazia
                retorno = "";
            }

            // Retorna
            return retorno;
        }

        /// <summary>
        /// Faz a interpretacao do formato
        /// </summary>
        private void interpretarFormato()
        {
            // Se tem barras (/) utiliza parse da propria data
            // Se não, faz parse próprio
            if (this.Formato.Contains("/"))
            {
                // Cria o culture info
                _cultureInfo = new CultureInfo("pt-br");
                _cultureInfo.DateTimeFormat.ShortDatePattern = this.Formato;
            }
            else
            {
                // Converte para maisculas
                _formato = _formato.ToUpper();

                // Acha as posições
                this.PosicaoDia = this.Formato.IndexOf('D');
                this.PosicaoMes = this.Formato.IndexOf('M');
                this.PosicaoAno = this.Formato.IndexOf('Y');

                // Acha os tamanhos
                this.TamanhoDia = quantidadeRepeticoes(this.Formato, 'D', this.PosicaoDia);
                this.TamanhoMes = quantidadeRepeticoes(this.Formato, 'M', this.PosicaoMes);
                this.TamanhoAno = quantidadeRepeticoes(this.Formato, 'Y', this.PosicaoAno);

                // Gera o formato para string
                _formatoParaString = _formato.Replace("Y", "y").Replace("D", "d");
            }
        }

        /// <summary>
        /// Acha quantidade de repeticoes da string informada
        /// </summary>
        /// <returns></returns>
        private int quantidadeRepeticoes(string texto, char caracter, int posicaoInicial)
        {
            int i = 0;
            while (true)
                if (posicaoInicial + i < texto.Length && texto[posicaoInicial + i] == caracter)
                    i++;
                else
                    break;
            return i;
        }
    }
}
