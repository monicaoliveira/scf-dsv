﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    /// <summary>
    /// Representa dados recebidos pelo telnet
    /// </summary>
    public class TelnetTokenData : TelnetTokenBase
    {
        /// <summary>
        /// Contem as informações lidas do buffer
        /// </summary>
        public List<byte> Data { get; set; }

        /// <summary>
        /// Indica se foi encontrado o comando de fim de informações no buffer
        /// </summary>
        public bool ReadFinalized { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public TelnetTokenData()
        {
            this.Data = new List<byte>();
            this.ReadFinalized = false;
        }

        /// <summary>
        /// Faz a leitura dos dados do buffer.
        /// Lê até encontrar algum byte de controle
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(TelnetStream buffer)
        {
            // Faz a leitura do buffer até o fim ou até um IAC
            while (!buffer.EOF)
            {
                // Tenta ler o próximo byte
                byte byteAtual = buffer.Buffer[buffer.Position];
                byte? proximoByte = buffer.Position + 1 < buffer.Buffer.Count ? new byte?(buffer.Buffer[buffer.Position + 1]) : null;

                // Verifica se é 255 repetido (considerar apenas 1)
                if (byteAtual == (byte)TelnetCommandsEnum.IAC && proximoByte.HasValue && proximoByte.Value == (byte)TelnetCommandsEnum.IAC)
                {
                    // Caracter 255 com escape
                    this.Data.Add(buffer.ReadByte().Value);
                    buffer.ReadByte();  // Ignora um dos caracteres
                }
                else if (byteAtual == (byte)TelnetCommandsEnum.IAC && proximoByte.HasValue)
                {
                    // IAC + comando, chegou ao fim
                    this.ReadFinalized = true;
                    break;
                }
                else
                {
                    // Faz a leitura normalmente
                    this.Data.Add(buffer.ReadByte().Value);
                }
            }
        }

        /// <summary>
        /// Escreve as informações no buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnWriteStream(TelnetStream buffer)
        {
            // Escreve as informações no buffer
            foreach (byte b in this.Data)
            {
                // Escreve o caractere
                buffer.WriteByte(b);

                // Caso seja 255, o caractere tem que ser duplicado
                if (b == 255)
                    buffer.WriteByte(b);
            }
        }

        /// <summary>
        /// Representação em texto
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "DATA: " + ASCIIEncoding.ASCII.GetString(this.Data.ToArray());
        }
    }
}
