﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Orders
{
    /// <summary>
    /// Token para a ordem Set Buffer Address (SBA)
    /// </summary>
    [Tn3270TokenOrder(OrderId = (byte)Tn3270OrdersEnum.SetBufferAddress)]
    public class Tn3270TokenOrderSBA : Tn3270TokenBase
    {
        /// <summary>
        /// Endereço do buffer
        /// </summary>
        public int BufferAddress { get; set; }

        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Faz a leitura do comando
            stream.ReadByte();

            // Lê os 2 bytes de endereço
            byte addr1 = stream.ReadByte().Value;
            byte addr2 = stream.ReadByte().Value;

            // Monta o endereço
            this.BufferAddress = Tn3270Utils.DecodeAddress(new byte[] { addr1, addr2 });
        }

        /// <summary>
        /// Escreve no stream
        /// </summary>
        /// <param name="stream"></param>
        protected override void OnWriteStream(Tn3270Stream stream)
        {
            // Escreve o AID
            stream.WriteByte((byte)Tn3270OrdersEnum.SetBufferAddress);

            // Codifica o endereço
            byte[] address = Tn3270Utils.EncodeAddress(this.BufferAddress);

            // Escreve o endereço
            stream.WriteByte(address[0]);
            stream.WriteByte(address[1]);
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Order {0} {1} ({2},{3})", 
                Tn3270OrdersEnum.SetBufferAddress, 
                this.BufferAddress,
                Tn3270Utils.DecodeCoordinates(this.BufferAddress, 80)[0], 
                Tn3270Utils.DecodeCoordinates(this.BufferAddress, 80)[1]);
        }

    }
}
