﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Implementação do serviço de localização (interface IServicoLocalizador)
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ServicoLocalizador : IServicoLocalizador
    {
        private Dictionary<string, Dictionary<string, ServicoOldInfo>> _servicos = 
            new Dictionary<string, Dictionary<string, ServicoOldInfo>>();
        
        #region IServicoLocalizador Members

        List<ServicoOldInfo> IServicoLocalizador.Consultar()
        {
            try
            {
                List<ServicoOldInfo> lista = new List<ServicoOldInfo>();
                foreach (KeyValuePair<string, Dictionary<string, ServicoOldInfo>> item1 in _servicos)
                    foreach (KeyValuePair<string, ServicoOldInfo> item2 in item1.Value)
                        lista.Add(item2.Value);
                return lista;
            }
            catch (Exception ex)
            {
                Log.EfetuarLog(ex, null, "OMS.Library.Servicos");
                throw ex;
            }
        }

        ServicoOldInfo IServicoLocalizador.Consultar(string servicoInterface)
        {
            try
            {
                return _servicos[servicoInterface][""];
            }
            catch (Exception ex)
            {
                Log.EfetuarLog("servicoInterface: " + servicoInterface + "; " + ex.ToString(), LogTipoEnum.Erro, "OMS.Library.Servicos");
                throw ex;
            }
        }

        ServicoOldInfo IServicoLocalizador.Consultar(string servicoInterface, string id)
        {
            try
            {
                return _servicos[servicoInterface][id];
            }
            catch (Exception ex)
            {
                Log.EfetuarLog("servicoInterface: " + servicoInterface + "; id: " + id + "; " + ex.ToString(), LogTipoEnum.Erro, "OMS.Library.Servicos");
                throw ex;
            }
        }

        void IServicoLocalizador.Registrar(ServicoOldInfo servico)
        {
            try
            {
                lock (_servicos)
                {
                    string nomeServico = servico.NomeInterface.Split(',')[0];

                    string id = servico.ID == null ? "" : servico.ID;
                    if (!_servicos.ContainsKey(nomeServico))
                        _servicos.Add(nomeServico, new Dictionary<string, ServicoOldInfo>());
                    if (_servicos[nomeServico].ContainsKey(id))
                        _servicos[nomeServico][id] = servico;
                    else
                        _servicos[nomeServico].Add(id, servico);

                    // Faz o log do registro
                    Log.EfetuarLog("Serviço registrado: " + Serializador.TransformarEmString(servico), LogTipoEnum.Passagem, "OMS.Library.Servicos");
                }
            }
            catch (Exception ex)
            {
                Log.EfetuarLog(ex, null, "OMS.Library.Servicos");
                throw ex;
            }
        }

        void IServicoLocalizador.Remover(string servicoInterface)
        {
            try
            {
                lock (_servicos)
                    _servicos[servicoInterface].Remove("");
            }
            catch (Exception ex)
            {
                Log.EfetuarLog(ex, null, "OMS.Library.Servicos");
                throw ex;
            }
        }

        void IServicoLocalizador.Remover(string servicoInterface, string id)
        {
            try
            {
                lock (_servicos)
                    _servicos[servicoInterface].Remove(id);
            }
            catch (Exception ex)
            {
                Log.EfetuarLog(ex, null, "OMS.Library.Servicos");
                throw ex;
            }
        }

        #endregion
    }
}
