﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;

using Resource.Framework.Contratos.Email;
using Resource.Framework.Contratos.Email.Dados;
using Resource.Framework.Contratos.Email.Mensagens;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Net;

namespace Resource.Framework.Sistemas.Email
{
    /// <summary>
    /// Implementação do serviço de envio de email
    /// </summary>
    public class ServicoEmail : IServicoEmail
    {
        #region IServicoEmail Members

        /// <summary>
        /// Solicita o Envio de um email
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public EnviarEmailResponse EnviarEmail(EnviarEmailRequest parametros)
        {

            ServicoEmailConfig config = GerenciadorConfig.ReceberConfig<ServicoEmailConfig>();

            if (config == null)
                config = new ServicoEmailConfig();

            // Prepara resposta
            EnviarEmailResponse resposta =
                new EnviarEmailResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
            
            //Faz o envio do email
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(string.IsNullOrEmpty(parametros.Email.Remetente) ? config.Remetente : parametros.Email.Remetente);

            mail.To.Add(new MailAddress(string.IsNullOrEmpty(config.EmailDeTeste) ? parametros.Email.Destinatario : config.EmailDeTeste));

            mail.Subject = parametros.Email.Assunto;
            mail.Body = parametros.Email.Texto;

            try
            {
                //SmtpClient smtp = new SmtpClient();
                SmtpClient smtp = new SmtpClient(string.IsNullOrEmpty(parametros.Host) ? config.Host : parametros.Host);

                smtp.EnableSsl = config.UsarSSL;

                if (config.Porta != null && config.Porta > 0)
                    smtp.Port = config.Porta.Value;

                if (config.UsarCredenciais)
                {
                    smtp.Credentials = new NetworkCredential(config.Remetente, config.SenhaRemetente, config.Dominio);
                }

                smtp.Send(mail);
            }
            catch (SmtpException smtpEx)
            {
                if (smtpEx.InnerException != null)
                    resposta.ProcessarExcessao(smtpEx.InnerException);
                else
                    resposta.ProcessarExcessao(smtpEx);
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                // * Liberando recursos
                mail.Dispose();
            }

            // Retorna
            return resposta;
        }

        #endregion
    }
}
