﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Reflection;
using Resource.Framework.Contratos.Comum;

namespace Resource.Framework.Sistemas.Comum
{
    /// <summary>
    /// Hook para validações antes de processar a mensagem
    /// Implementa validação de seguranca para o host
    /// </summary>
    public class ServicoHostHookValidacaoSeguranca : IServicoHostHookProcessarMensagem
    {
        /// <summary>
        /// Contém a lista de sistemas cliente que devem ter a permissão verificada 
        /// </summary>
        public List<string> VerificarPermissaoSistemasCliente { get; set; }

        /// <summary>
        /// Cache interno para os atributos dos métodos
        /// </summary>
        private Dictionary<MethodInfo, List<object>> _dicionario = new Dictionary<MethodInfo, List<object>>();
        
        #region IServicoHostValidacaoSeguranca Members

        public void Processar(MethodInfo methodInfo, MensagemRequestBase request, MensagemResponseBase response, Exception ex)
        {
            // Verifica se o código sistema cliente (origem) está entre as origens válidas para o processamento deste hook.
            if (request.CodigoSistemaCliente != null && this.VerificarPermissaoSistemasCliente.Contains(request.CodigoSistemaCliente))
            {

                // Inicializa
                List<object> atributos = null;

                // Resultado 
                bool valido = true;
                Type permissaoTestada = null;
                List<Type> permissoesTestadas = new List<Type>();

                // Verifica se tem no cache
                if (!_dicionario.ContainsKey(methodInfo))
                {
                    // Inicializa a lista de atributos
                    atributos = new List<object>();

                    // Verifica se o método, ou se a mensagem, tem validação de permissao
                    atributos.AddRange(
                        methodInfo.GetCustomAttributes(
                            typeof(ValidacaoPermissaoAttribute), true));

                    atributos.AddRange(
                        request.GetType().GetCustomAttributes(
                            typeof(ValidacaoPermissaoAttribute), true));

                    // Adiciona no cache
                    _dicionario.Add(methodInfo, atributos);
                }
                else
                {
                    // Pega lista existente
                    atributos = _dicionario[methodInfo];
                }

                // Faz os testes necessários
                foreach (object atributo in atributos)
                {
                    // Pega com o tipo correto
                    ValidacaoPermissaoAttribute atributo2 = (ValidacaoPermissaoAttribute)atributo;

                    // Valida o atributo isolado
                    if (atributo2.Permissao != null)
                    {
                        valido = valido && SegurancaHelper.VerificarPermissao(request.CodigoSessao, atributo2.Permissao);
                        permissaoTestada = atributo2.Permissao;
                    }

                    // Valida a colecao de atributos. Qualquer permissao encontrada permite a execução
                    if (atributo2.PermissaoListaE != null)
                    {
                        bool achou1 = false;
                        foreach (Type tipo in atributo2.PermissaoListaE)
                        {
                            if (SegurancaHelper.VerificarPermissao(request.CodigoSessao, tipo))
                            {
                                permissoesTestadas.Add(tipo);
                                achou1 = true;
                                break;
                            }
                        }
                        if (!achou1)
                            valido = false;
                    }
                }

                // Está valido?
                if (!valido)
                {
                    // Monta string
                    string mensagem = "Acesso Negado. Esta sessão não possui permissão para executar a mensagem {0}. Permissões testadas: ";

                    // Inclui a permissao unica
                    if (permissaoTestada != null)
                        mensagem += permissaoTestada.Name + "; ";

                    // Inclui o conjunto de permissoes
                    if (permissoesTestadas.Count > 0)
                        mensagem += "[" + string.Join(",", permissoesTestadas.Select(i => i.Name).ToArray()) + "]";

                    // Dispara excessao de acesso negado
                    throw new AcessoNegadoException(string.Format(mensagem, request.GetType().Name));
                }

                // Retorna 
                return;

            }
        }

        #endregion

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoHostHookValidacaoSeguranca()
        {
            this.VerificarPermissaoSistemasCliente = new List<string>();
        }
    }
}
