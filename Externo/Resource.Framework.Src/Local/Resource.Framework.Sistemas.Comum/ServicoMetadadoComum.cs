﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens.Db;

namespace Resource.Framework.Sistemas.Comum
{
    /// <summary>
    /// Interface para o serviço de metadados para bancos de dados.
    /// A motivação inicial é sincronizar os enumeradores com as tabelas
    /// Lista e ListaItem.
    /// </summary>
    public class ServicoMetadadoComum : IServicoMetadadoComum
    {
        #region Cadastro de Lista e Lista Item

        /// <summary>
        /// Lista de listas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarListaResponse ListarLista(ListarListaRequest request)
        {
            // Prepara resposta
            ListarListaResponse resposta = new ListarListaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna a lista
                resposta.Resultado = 
                    PersistenciaHelper.Listar<ListaResumoInfo>(
                        request.CodigoSessao);
            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna resposta
            return resposta;
        }

        /// <summary>
        /// Recebe o detalhe de uma lista
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberListaResponse ReceberLista(ReceberListaRequest request)
        {
            // Prepara resposta
            ReceberListaResponse resposta = new ReceberListaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna a lista
                resposta.ListaInfo =
                    Mensageria.Processar<ReceberObjetoResponse<ListaInfo>>(
                        new ReceberListaDbRequest()
                        {
                            CodigoLista = request.CodigoLista,
                            MnemonicoLista = request.MnemonicoLista,
                            RetornarItensExcluidos = request.RetornarItensExcluidos
                        }).Objeto;
            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna resposta
            return resposta;
        }

        /// <summary>
        /// Recebe o detalhe de diversas listas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberListasResponse ReceberListas(ReceberListasRequest request)
        {
            // Prepara resposta
            ReceberListasResponse resposta = new ReceberListasResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Varre codigos
                if (request.CodigosLista != null)
                    foreach (string codigoLista in request.CodigosLista)
                        resposta.Listas.Add(
                            this.ReceberLista(
                                new ReceberListaRequest() 
                                { 
                                    CodigoSessao = request.CodigoSessao,
                                    CodigoLista = codigoLista
                                }).ListaInfo);

                // Varre os mnemonicos
                if (request.MnemonicosLista != null)
                    foreach (string mnemonicoLista in request.MnemonicosLista)
                        resposta.Listas.Add(
                            this.ReceberLista(
                                new ReceberListaRequest()
                                {
                                    CodigoSessao = request.CodigoSessao,
                                    MnemonicoLista = mnemonicoLista
                                }).ListaInfo);
            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove a lista solicitada
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverListaResponse RemoverLista(RemoverListaRequest request)
        {
            // Prepara resposta
            RemoverListaResponse resposta = new RemoverListaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove a lista
                PersistenciaHelper.Remover<ListaInfo>(
                    request.CodigoSessao, request.CodigoLista);

                // Informa
                resposta.CodigoListaRemovido = request.CodigoLista;
            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna resposta
            return resposta;
        }

        /// <summary>
        /// Salva a lista informada
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarListaResponse SalvarLista(SalvarListaRequest request)
        {
            // Prepara resposta
            SalvarListaResponse resposta = new SalvarListaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva a lista
                SalvarObjetoResponse<ListaInfo> respostaSalvar = 
                    Mensageria.Processar<SalvarObjetoResponse<ListaInfo>>(
                        new SalvarObjetoRequest<ListaInfo>() 
                        { 
                            CodigoSessao = request.CodigoSessao,
                            Objeto = request.ListaInfo
                        });

                // Repassa criticas
                resposta.Criticas.AddRange(respostaSalvar.Criticas);

                // Retorna objeto salvo
                resposta.ListaInfo = respostaSalvar.Objeto;
            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna resposta
            return resposta;
        }

        #endregion

        #region Metadados de Enumeradores

        /// <summary>
        /// Faz a geração e/ou sincronismo dos enumeradores informados
        /// com as tabelas Lista e ListaItem
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public GerarEnumMetadadoResponse GerarEnumMetadado(GerarEnumMetadadoRequest parametros)
        {
            // Prepara resposta
            GerarEnumMetadadoResponse resposta =
                new GerarEnumMetadadoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Varre os enumeradores
            foreach (Type tipo in parametros.ReceberListaEnumeradores())
            {
                // Cria Lista
                ListaInfo lista =
                    new ListaInfo()
                    {
                        Mnemonico = tipo.Name
                    };

                // Tem descricao?
                object[] descs = tipo.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (descs.Length > 0)
                    lista.Descricao = ((DescriptionAttribute)descs[0]).Description;

                // Varre os elementos
                foreach (FieldInfo field in tipo.GetFields(BindingFlags.Static | BindingFlags.GetField | BindingFlags.Public))
                {
                    // Se for obsoleto, ignora
                    if (field.GetCustomAttributes(typeof(ObsoleteAttribute), false).FirstOrDefault() != null)
                        continue;
 
                    // Cria o item
                    ListaItemInfo item =
                        new ListaItemInfo()
                        {
                            Mnemonico = field.Name,
                            Valor = ((int)field.GetValue(null)).ToString()
                        };

                    // Tem descricao?
                    object[] descs2 = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    if (descs2.Length > 0)
                        item.Descricao = ((DescriptionAttribute)descs2[0]).Description;

                    // Adiciona na coleção de itens
                    lista.Itens.Add(item);
                }

                // Adiciona lista na resposta
                resposta.Listas.Add(lista);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Descricao de Tipos

        /// <summary>
        /// Solicita lista de tipos com suas respectivas descrições
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarDescricaoTipoResponse ListarDescricaoTipo(ListarDescricaoTipoRequest request)
        {
            // Prepara resposta
            ListarDescricaoTipoResponse resposta = new ListarDescricaoTipoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Prepara lista de tipos 
                List<Type> tipos = new List<Type>();

                // Pega tipos de assemblies
                foreach (string assemblyStr in request.IncluirAssemblies)
                {
                    // Carrega e adiciona lista de tipos do assembly
                    Assembly assembly = Assembly.Load(assemblyStr);
                    if (assembly != null)
                        tipos.AddRange(assembly.GetTypes());
                }

                // Pega tipos de namespaces
                foreach (string namespaceStr in request.IncluirNamespaces)
                {
                    // Separa namespace do nome do assembly
                    string[] namespaceStr2 = namespaceStr.Split(',');
                    namespaceStr2[0] = namespaceStr2[0].Trim();
                    namespaceStr2[1] = namespaceStr2[1].Trim();

                    // Varre a lista de tipos do assembly
                    Assembly assembly = Assembly.Load(namespaceStr2[1]);
                    if (assembly != null)
                        foreach (Type tipo in assembly.GetTypes())
                            if (tipo.FullName.StartsWith(namespaceStr2[0]))
                                tipos.Add(tipo);
                }

                // Pega tipos diretos
                foreach (string tipoStr in request.IncluirTipos)
                {
                    // Pega o tipo e adiciona na coleção
                    Type tipo = Type.GetType(tipoStr);
                    if (tipo != null)
                        tipos.Add(tipo);
                }

                // Varre a lista de tipos criando as descricoes
                foreach (Type tipo in tipos)
                {
                    // Verifica se tem atributo de descricao e adiciona na resposta
                    object[] attributes = tipo.GetCustomAttributes(typeof(DescriptionAttribute), true);
                    if (attributes.Length > 0)
                        resposta.Resultado.Add(
                            new TipoDescricaoInfo()
                            {
                                Descricao = ((DescriptionAttribute)attributes[0]).Description,
                                NomeTipo = tipo.Name,
                                Tipo = tipo.FullName
                            });
                    else
                        resposta.Resultado.Add(
                            new TipoDescricaoInfo()
                            {
                                Descricao = tipo.Name,
                                NomeTipo = tipo.Name,
                                Tipo = tipo.FullName
                            });
                }
            
            }
            catch (Exception ex)
            {
                // Trata excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Config

        /// <summary>
        /// Salva Config informado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarConfigResponse SalvarConfig(SalvarConfigRequest request)
        {
            // Prepara resposta
            SalvarConfigResponse resposta = new SalvarConfigResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva na persistencia
                resposta.ConfigInfo =
                    PersistenciaHelper.Salvar<ConfigInfo>(
                        request.CodigoSessao, request.ConfigInfo);
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Recebe o config solicitado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberConfigResponse ReceberConfig(ReceberConfigRequest request)
        {
            // Prepara resposta
            ReceberConfigResponse resposta = new ReceberConfigResponse();

            // Bloco de controle
            try
            {
                // Pede para a persistencia
                resposta.ConfigInfo =
                    PersistenciaHelper.Receber<ConfigInfo>(
                        request.CodigoSessao, request.TipoObjeto);
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove o config solicitado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverConfigResponse RemoverConfig(RemoverConfigRequest request)
        {
            // Prepara resposta
            RemoverConfigResponse resposta = new RemoverConfigResponse();

            // Bloco de controle
            try
            {
                // Remove o config solicitado
                PersistenciaHelper.Remover<ConfigInfo>(
                    request.CodigoSessao, request.TipoObjeto);
            }
            catch (Exception ex)
            {
                // Trata excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion
    }
}
