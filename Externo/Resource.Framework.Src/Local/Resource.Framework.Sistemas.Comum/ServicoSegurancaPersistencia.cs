﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Sistemas.Comum
{   public class ServicoSegurancaPersistencia : IServicoSegurancaPersistencia
    {
        private ListaPermissoesHelper _permissoes = new ListaPermissoesHelper();
        private ServicoSegurancaConfig _configSeguranca = GerenciadorConfig.ReceberConfig<ServicoSegurancaConfig>();

        #region Construtores
        public ServicoSegurancaPersistencia()
        {   if (_configSeguranca != null)
                _permissoes.CarregarPermissoes(_configSeguranca.NamespacesPermissoes);
        }
        #endregion

        #region IServicoSegurancaPersistencia Members
        public ListarPerfisResponse ListarPerfis(ListarPerfisRequest parametros)
        {
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
            return
                new ListarPerfisResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    Perfis =
                        Mensageria.Processar<ConsultarObjetosResponse<PerfilInfo>>(
                            new ConsultarObjetosRequest<PerfilInfo>() 
                            { 
                                Condicoes = condicoes 
                            }).Resultado
                };
        }
        public SalvarPerfilResponse SalvarPerfil(SalvarPerfilRequest parametros)
        {
            Mensageria.Processar(
                new SalvarObjetoRequest<PerfilInfo>() 
                { 
                    Objeto = parametros.Perfil 
                });

            return
                new SalvarPerfilResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }
        public ReceberPerfilResponse ReceberPerfil(ReceberPerfilRequest parametros)
        {
            ReceberPerfilResponse resposta =
                new ReceberPerfilResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    Perfil =
                        Mensageria.Processar<ReceberObjetoResponse<PerfilInfo>>(
                            new ReceberObjetoRequest<PerfilInfo>()
                            {
                                CodigoObjeto = parametros.CodigoPerfil
                            }).Objeto
                };
            if (parametros.PreencherColecoesCompletas)
            {
                foreach (PermissaoAssociadaInfo permissaoAssociada in resposta.Perfil.Permissoes)
                    permissaoAssociada.PermissaoInfo =
                        _permissoes.ListaPorCodigo.FirstOrDefault(p=> p.Codigo == permissaoAssociada.CodigoPermissao).Permissao.PermissaoInfo;
            }
            return resposta;
        }
        public RemoverPerfilResponse RemoverPerfil(RemoverPerfilRequest parametros)
        {
            var res = Mensageria.Processar(
                new RemoverObjetoRequest<PerfilInfo>() 
                { 
                    CodigoObjeto = parametros.CodigoPerfil 
                });
            if (res.StatusResposta == MensagemResponseStatusEnum.OK)
            {
                return
                    new RemoverPerfilResponse()
                    {
                        CodigoMensagemRequest = parametros.CodigoMensagem
                    };
            }
            else
            {
                return
                    new RemoverPerfilResponse()
                    {
                        CodigoMensagemRequest = parametros.CodigoMensagem,
                        Erro = res.Erro,
                        StatusResposta = res.StatusResposta,
                        DescricaoResposta = res.DescricaoResposta
                    };
            }
        }
        public ConsultarSessoesResponse ConsultarSessoes(ConsultarSessoesRequest parametros)
        {
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
            return
                new ConsultarSessoesResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    Sessoes = 
                        Mensageria.Processar<ConsultarObjetosResponse<SessaoInfo>>(
                            new ConsultarObjetosRequest<SessaoInfo>() 
                            { 
                                Condicoes = condicoes 
                            }).Resultado
                };
        }
        public SalvarSessaoResponse SalvarSessao(SalvarSessaoRequest parametros)
        {
            Mensageria.Processar(
                new SalvarObjetoRequest<SessaoInfo>() 
                { 
                    Objeto = parametros.Sessao 
                });
            return
                new SalvarSessaoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }
        public ReceberSessaoResponse ReceberSessao(ReceberSessaoRequest parametros)
        {
            return
                new ReceberSessaoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    Sessao =
                        Mensageria.Processar<ReceberObjetoResponse<SessaoInfo>>(
                            new ReceberObjetoRequest<SessaoInfo>() 
                            { 
                                CodigoObjeto = parametros.CodigoSessao 
                            }).Objeto
                };
        }
        public RemoverSessaoResponse RemoverSessao(RemoverSessaoRequest parametros)
        {
            Mensageria.Processar(
                new RemoverObjetoRequest<SessaoInfo>() 
                { 
                    CodigoObjeto = parametros.CodigoSessaoARemover 
                });
            return
                new RemoverSessaoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }
        public ConsultarSistemasClienteResponse ConsultarSistemasCliente(ConsultarSistemasClienteRequest parametros)
        {
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
            return
                new ConsultarSistemasClienteResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    SistemasCliente = 
                        Mensageria.Processar<ConsultarObjetosResponse<SistemaClienteInfo>>(
                            new ConsultarObjetosRequest<SistemaClienteInfo>() 
                            { 
                                Condicoes = condicoes 
                            }).Resultado
                };
        }
        public SalvarSistemaClienteResponse SalvarSistemaCliente(SalvarSistemaClienteRequest parametros)
        {
            Mensageria.Processar(
                new SalvarObjetoRequest<SistemaClienteInfo>() 
                { 
                    Objeto = parametros.SistemaCliente 
                });
            return
                new SalvarSistemaClienteResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }
        public ReceberSistemaClienteResponse ReceberSistemaCliente(ReceberSistemaClienteRequest parametros)
        {
            return
                new ReceberSistemaClienteResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    SistemaCliente =
                        Mensageria.Processar<ReceberObjetoResponse<SistemaClienteInfo>>(
                            new ReceberObjetoRequest<SistemaClienteInfo>() 
                            { 
                                CodigoObjeto = parametros.CodigoSistemaClienteAReceber 
                            }).Objeto
                };
        }
        public RemoverSistemaClienteResponse RemoverSistemaCliente(RemoverSistemaClienteRequest parametros)
        {
            Mensageria.Processar(
                new RemoverObjetoRequest<SistemaClienteInfo>() 
                { 
                    CodigoObjeto = parametros.CodigoSistemaClienteARemover 
                });
            return
                new RemoverSistemaClienteResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }
        public ListarUsuarioGruposResponse ListarUsuarioGrupos(ListarUsuarioGruposRequest parametros)
        {
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
            return
                new ListarUsuarioGruposResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    UsuarioGrupos = 
                        Mensageria.Processar<ConsultarObjetosResponse<UsuarioGrupoInfo>>(
                            new ConsultarObjetosRequest<UsuarioGrupoInfo>() 
                            { 
                                Condicoes = condicoes 
                            }).Resultado
                };
        }
        public SalvarUsuarioGrupoResponse SalvarUsuarioGrupo(SalvarUsuarioGrupoRequest parametros)
        {
            Mensageria.Processar(
                new SalvarObjetoRequest<UsuarioGrupoInfo>() 
                { 
                    Objeto = parametros.UsuarioGrupo 
                });
            return
                new SalvarUsuarioGrupoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }
        public ReceberUsuarioGrupoResponse ReceberUsuarioGrupo(ReceberUsuarioGrupoRequest parametros)
        {
            ReceberUsuarioGrupoResponse resposta =
                new ReceberUsuarioGrupoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    UsuarioGrupo =
                        Mensageria.Processar<ReceberObjetoResponse<UsuarioGrupoInfo>>(
                            new ReceberObjetoRequest<UsuarioGrupoInfo>()
                            {
                                CodigoObjeto = parametros.CodigoUsuarioGrupo
                            }).Objeto
                };
            if (parametros.PreencherColecoesCompletas)
            {
                if (resposta.UsuarioGrupo.Perfis2 == null)
                {
                    resposta.UsuarioGrupo.Perfis2 = new List<PerfilInfo>();
                    foreach (string codigoPerfil in resposta.UsuarioGrupo.Perfis)
                        resposta.UsuarioGrupo.Perfis2.Add(
                            this.ReceberPerfil(
                                new ReceberPerfilRequest()
                                {
                                    CodigoPerfil = codigoPerfil,
                                    PreencherColecoesCompletas = true
                                }).Perfil);
                }
                foreach (PermissaoAssociadaInfo permissaoAssociada in resposta.UsuarioGrupo.Permissoes)
                    permissaoAssociada.PermissaoInfo =
                        _permissoes.ListaPorCodigo.FirstOrDefault(p => p.Codigo == permissaoAssociada.CodigoPermissao).Permissao.PermissaoInfo;
            }
            return resposta;
        }
        public RemoverUsuarioGrupoResponse RemoverUsuarioGrupo(RemoverUsuarioGrupoRequest parametros)
        {
            var res = Mensageria.Processar(
                new RemoverObjetoRequest<UsuarioGrupoInfo>() 
                { 
                    CodigoObjeto = parametros.CodigoUsuarioGrupo 
                });

            if (res.StatusResposta == MensagemResponseStatusEnum.OK)
            {
                return
                    new RemoverUsuarioGrupoResponse()
                    {
                        CodigoMensagemRequest = parametros.CodigoMensagem
                    };
            }
            else
            {
                return
                    new RemoverUsuarioGrupoResponse()
                    {
                        CodigoMensagemRequest = parametros.CodigoMensagem,
                        Erro = res.Erro,
                        StatusResposta = res.StatusResposta,
                        DescricaoResposta = res.DescricaoResposta
                    };
            }
        }
        public ListarUsuariosResponse ListarUsuarios(ListarUsuariosRequest parametros)
        {
            ListarUsuariosResponse resposta = new ListarUsuariosResponse();
            resposta.PreparaResposta(parametros);
            try
            {
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                if (parametros.FiltroNomeUsuario != null)
                    condicoes.Add(new CondicaoInfo("Nome", CondicaoTipoEnum.Igual, parametros.FiltroNomeUsuario));
                if (parametros.FiltroCodigoPermissao != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoPermissao", CondicaoTipoEnum.Igual, parametros.FiltroCodigoPermissao));
                resposta.Usuarios = 
                    PersistenciaHelper.Listar<UsuarioInfo>(
                        resposta.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }
        public SalvarUsuarioResponse SalvarUsuario(SalvarUsuarioRequest parametros)
        {
            UsuarioInfo usuarioOginal = 
                Mensageria.Processar<ReceberObjetoResponse<UsuarioInfo>>(
                    new ReceberObjetoRequest<UsuarioInfo>() 
                    {
                        CodigoObjeto = parametros.Usuario.CodigoUsuario
                    }).Objeto;
            ajustarRelacoesUsuario(parametros.Usuario, usuarioOginal);
            Mensageria.Processar(
                new SalvarObjetoRequest<UsuarioInfo>() 
                { 
                    Objeto = parametros.Usuario 
                });
            return
                new SalvarUsuarioResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }
        private void ajustarRelacoesUsuario(UsuarioInfo usuario, UsuarioInfo usuarioOriginal)
        {
            List<UsuarioRelacaoInfo> relacoesAtualizar = new List<UsuarioRelacaoInfo>();
            List<UsuarioRelacaoInfo> relacoesRemover = new List<UsuarioRelacaoInfo>();
            foreach (UsuarioRelacaoInfo relacao in usuario.Relacoes)
                if (usuarioOriginal != null && localizarRelacao(relacao, usuarioOriginal) == null)
                    relacoesAtualizar.Add(relacao);
            if (usuarioOriginal != null)
                foreach (UsuarioRelacaoInfo relacao in usuarioOriginal.Relacoes)
                    if (localizarRelacao(relacao, usuario) == null)
                        relacoesRemover.Add(relacao);
            foreach (UsuarioRelacaoInfo relacao in relacoesAtualizar)
            {
                string codigoUsuario = 
                    relacao.CodigoUsuario1 != usuario.CodigoUsuario ? relacao.CodigoUsuario1 : relacao.CodigoUsuario2;
                UsuarioInfo usuarioAtualizar =
                    Mensageria.Processar<ReceberObjetoResponse<UsuarioInfo>>(
                        new ReceberObjetoRequest<UsuarioInfo>() 
                        { 
                            CodigoObjeto = codigoUsuario
                        }).Objeto;
                usuarioAtualizar.Relacoes.Add(relacao);
                Mensageria.Processar(
                    new SalvarObjetoRequest<UsuarioInfo>()
                    {
                        Objeto = usuarioAtualizar
                    });
            }
            foreach (UsuarioRelacaoInfo relacao in relacoesRemover)
            {
                string codigoUsuario =
                    relacao.CodigoUsuario1 != usuario.CodigoUsuario ? relacao.CodigoUsuario1 : relacao.CodigoUsuario2;
                UsuarioInfo usuarioRemover =
                    Mensageria.Processar<ReceberObjetoResponse<UsuarioInfo>>(
                        new ReceberObjetoRequest<UsuarioInfo>()
                        {
                            CodigoObjeto = codigoUsuario
                        }).Objeto;
                usuarioRemover.Relacoes.Remove(localizarRelacao(relacao, usuarioRemover));
                Mensageria.Processar(
                    new SalvarObjetoRequest<UsuarioInfo>()
                    {
                        Objeto = usuarioRemover
                    });
            }
        }
        private UsuarioRelacaoInfo localizarRelacao(UsuarioRelacaoInfo relacaoALocalizar, UsuarioInfo usuarioAlvo)
        {
            UsuarioRelacaoInfo relacao =
                (from r in usuarioAlvo.Relacoes
                 where r.CodigoUsuario1 == relacaoALocalizar.CodigoUsuario1 &&
                       r.CodigoUsuario2 == relacaoALocalizar.CodigoUsuario2 &&
                       r.TipoRelacao == relacaoALocalizar.TipoRelacao
                 select r).FirstOrDefault();
            return relacao;
        }
        public ReceberUsuarioResponse ReceberUsuario(ReceberUsuarioRequest parametros)
        {
            ReceberUsuarioResponse resposta =
                new ReceberUsuarioResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
            resposta.PreparaResposta(parametros);
            try
            {
                resposta.Usuario =
                    Mensageria.Processar<ReceberObjetoResponse<UsuarioInfo>>(
                        new ReceberObjetoRequest<UsuarioInfo>()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            CodigoObjeto = parametros.CodigoUsuario
                        }).Objeto;
                if (parametros.PreencherColecoesCompletas)
                {
                    if (resposta.Usuario.Grupos2 == null)
                    {
                        resposta.Usuario.Grupos2 = new List<UsuarioGrupoInfo>();
                        foreach (string codigoGrupo in resposta.Usuario.Grupos)
                            resposta.Usuario.Grupos2.Add(
                                this.ReceberUsuarioGrupo(
                                    new ReceberUsuarioGrupoRequest()
                                    {
                                        CodigoUsuarioGrupo = codigoGrupo,
                                        PreencherColecoesCompletas = true
                                    }).UsuarioGrupo);
                    }
                    if (resposta.Usuario.Perfis2 == null)
                    {
                        resposta.Usuario.Perfis2 = new List<PerfilInfo>();
                        foreach (string codigoPerfil in resposta.Usuario.Perfis)
                            resposta.Usuario.Perfis2.Add(
                                this.ReceberPerfil(
                                    new ReceberPerfilRequest()
                                    {
                                        CodigoPerfil = codigoPerfil,
                                        PreencherColecoesCompletas = true
                                    }).Perfil);
                    }
                    foreach (PermissaoAssociadaInfo permissaoAssociada in resposta.Usuario.Permissoes)
                        permissaoAssociada.PermissaoInfo =
                            _permissoes.ListaPorCodigo.FirstOrDefault(p => p.Codigo == permissaoAssociada.CodigoPermissao).Permissao.PermissaoInfo;
                }
            }
            catch (Exception ex)
            {
                Log.EfetuarLog(ex, parametros);
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }
        public RemoverUsuarioResponse RemoverUsuario(RemoverUsuarioRequest parametros)
        {
            MensagemResponseBase res = Mensageria.Processar(
                new RemoverObjetoRequest<UsuarioInfo>() 
                { 
                    CodigoObjeto = parametros.CodigoUsuario 
                });
            res.CodigoMensagem = parametros.CodigoMensagem;
            if (res.StatusResposta == MensagemResponseStatusEnum.OK)
            {
                return
                    new RemoverUsuarioResponse()
                    {
                        CodigoMensagemRequest = parametros.CodigoMensagem
                    };
            }
            else
            {
                return new RemoverUsuarioResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    StatusResposta = res.StatusResposta,
                    Criticas = res.Criticas,
                    DescricaoResposta = res.DescricaoResposta,
                    Erro = res.Erro
                };
            }
        }
        #endregion
    }
}
