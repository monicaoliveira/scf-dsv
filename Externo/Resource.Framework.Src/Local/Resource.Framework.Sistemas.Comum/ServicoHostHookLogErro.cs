﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum;
using System.Reflection;
using System.Threading;
using Resource.Framework.Library;

namespace Resource.Framework.Sistemas.Comum
{
    /// <summary>
    /// Hook para efetuar logs de erro
    /// Faz logs apenas se já tiver sido autenticado
    /// </summary>
    public class ServicoHostHookLogErro : IServicoHostHookProcessarMensagem
    {
        #region IServicoHostHookProcessarMensagem Members

        public void Processar(MethodInfo methodInfo, MensagemRequestBase request, MensagemResponseBase response, Exception exception)
        {
            // Tem erro ou resposta com erro?
            if (exception != null || (response != null && response.StatusResposta == MensagemResponseStatusEnum.ErroPrograma))
            {
                // Processa assíncrono
                ThreadPool.QueueUserWorkItem(
                    new WaitCallback(
                        delegate
                        {
                            // Log em arquivo
                            LogArquivo logArquivo = new LogArquivo("app", "rotina", "ServicoHostHookLogErro");
                            
                            // Bloco de controle
                            try
                            {
                                // Pega informações da sessao
                                UsuarioInfo usuarioInfo =
                                    SegurancaHelper.ReceberUsuario(request.CodigoSessao);

                                // Cria o log
                                LogErroInfo logErroInfo = new LogErroInfo();
                                logErroInfo.CodigoUsuario = usuarioInfo.CodigoUsuario;
                                logErroInfo.TipoMensagemRequest = request.GetType().Name;

                                // Coloca na descricao o texto da excessao ou o resultado da resposta de erro
                                if (exception != null)
                                    logErroInfo.Descricao = exception.ToString();
                                else
                                    logErroInfo.Descricao = response.DescricaoResposta;

                                // Salva na persistência
                                PersistenciaHelper.Salvar<LogInfo>(request.CodigoSessao, logErroInfo);
                            }
                            finally
                            {
                                // Finaliza log
                                logArquivo.Dispose();
                            }
                        }));
            }

        }

        #endregion
    }
}
