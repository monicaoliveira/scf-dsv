﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Permissoes;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library.Servicos.Dados;
using System.Diagnostics;
using Resource.Framework.Sistemas.Comum; //1413

namespace Resource.Framework.Sistemas.Comum
{   public class ServicoSeguranca : IServicoSeguranca
    {
        #region Variaveis Locais
        private ServicoSegurancaConfig _configSeguranca = GerenciadorConfig.ReceberConfig<ServicoSegurancaConfig>() != null ? GerenciadorConfig.ReceberConfig<ServicoSegurancaConfig>() : new ServicoSegurancaConfig();
        private IServicoSegurancaPersistencia _servicoPersistencia = new ServicoSegurancaPersistencia();
        private ListaPermissoesHelper _permissoes = new ListaPermissoesHelper();
        private Dictionary<string, Sessao> _sessoes = new Dictionary<string, Sessao>();
        private Dictionary<ComplementoAutenticacaoInfo, IComplementoAutenticacao> _complementosAutenticacao = new Dictionary<ComplementoAutenticacaoInfo, IComplementoAutenticacao>();
        #endregion

        #region Construtores
        public ServicoSeguranca()
        {   _permissoes.CarregarPermissoes(_configSeguranca.NamespacesPermissoes); //1477 analise
            foreach (ComplementoAutenticacaoInfo complementoInfo in _configSeguranca.ComplementosAutenticacao)
            {   if (complementoInfo.Habilitado)
                {
                    IComplementoAutenticacao complementoInstancia = (IComplementoAutenticacao)Activator.CreateInstance(complementoInfo.TipoComplemento);
                    _complementosAutenticacao.Add(complementoInfo, complementoInstancia);
                }
            }
         }
        #endregion

        #region IServicoSeguranca Members
        #region Comum
        public MensagemResponseBase ProcessarMensagem(MensagemRequestBase parametros)
        {   Type tipo = parametros.GetType();
            MensagemResponseBase resposta = null;
            try
            {   Log.EfetuarLog(Serializador.TransformarEmString(parametros), LogTipoEnum.Passagem, ModulosOMS.ModuloSeguranca);
                if (tipo == typeof(AssociarPerfilRequest))
                    resposta = this.AssociarPerfil((AssociarPerfilRequest)parametros);
                else if (tipo == typeof(AssociarPermissaoRequest))
                    resposta = this.AssociarPermissao((AssociarPermissaoRequest)parametros);
                else if (tipo == typeof(AssociarUsuarioGrupoRequest))
                    resposta = this.AssociarUsuarioGrupo((AssociarUsuarioGrupoRequest)parametros);
                else if (tipo == typeof(AutenticarUsuarioRequest))
                    resposta = this.AutenticarUsuario((AutenticarUsuarioRequest)parametros);
                else if (tipo == typeof(ListarPerfisRequest))
                    resposta = this.ListarPerfis((ListarPerfisRequest)parametros);
                else if (tipo == typeof(ListarPermissoesRequest))
                    resposta = this.ListarPermissoes((ListarPermissoesRequest)parametros);
                else if (tipo == typeof(ListarUsuarioGruposRequest))
                    resposta = this.ListarUsuarioGrupos((ListarUsuarioGruposRequest)parametros);
                else if (tipo == typeof(ListarUsuariosRequest))
                    resposta = this.ListarUsuarios((ListarUsuariosRequest)parametros);
                else if (tipo == typeof(ReceberPerfilRequest))
                    resposta = this.ReceberPerfil((ReceberPerfilRequest)parametros);
                else if (tipo == typeof(ReceberUsuarioRequest))
                    resposta = this.ReceberUsuario((ReceberUsuarioRequest)parametros);
                else if (tipo == typeof(ReceberUsuarioGrupoRequest))
                    resposta = this.ReceberUsuarioGrupo((ReceberUsuarioGrupoRequest)parametros);
                else if (tipo == typeof(RemoverPerfilRequest))
                    resposta = this.RemoverPerfil((RemoverPerfilRequest)parametros);
                else if (tipo == typeof(RemoverUsuarioRequest))
                    resposta = this.RemoverUsuario((RemoverUsuarioRequest)parametros);
                else if (tipo == typeof(RemoverUsuarioGrupoRequest))
                    resposta = this.RemoverUsuarioGrupo((RemoverUsuarioGrupoRequest)parametros);
                else if (tipo == typeof(SalvarPerfilRequest))
                    resposta = this.SalvarPerfil((SalvarPerfilRequest)parametros);
                else if (tipo == typeof(SalvarUsuarioRequest))
                    resposta = this.SalvarUsuario((SalvarUsuarioRequest)parametros);
                else if (tipo == typeof(SalvarUsuarioGrupoRequest))
                    resposta = this.SalvarUsuarioGrupo((SalvarUsuarioGrupoRequest)parametros);
                else if (tipo == typeof(VerificarPermissaoRequest))
                    resposta = this.VerificarPermissao((VerificarPermissaoRequest)parametros);
                else if (tipo == typeof(ValidarAssinaturaEletronicaRequest))
                    resposta = this.ValidarAssinaturaEletronica((ValidarAssinaturaEletronicaRequest)parametros);
                else if (tipo == typeof(AlterarAssinaturaEletronicaRequest))
                    resposta = this.AlterarAssinaturaEletronica((AlterarAssinaturaEletronicaRequest)parametros);
            }
            catch (Exception ex)
            {
                if (resposta != null)
                {
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
                    resposta.DescricaoResposta = ex.ToString();
                }
                Log.EfetuarLog(ex, parametros, ModulosOMS.ModuloSeguranca);
            }
            return resposta;
        }
        public InicializarSegurancaResponse InicializarSeguranca(InicializarSegurancaRequest parametros)
        {
            UsuarioInfo usuarioInfo = _servicoPersistencia.ReceberUsuario(new ReceberUsuarioRequest(){CodigoUsuario = _configSeguranca.NomeUsuarioAdministrador}).Usuario;
            if (usuarioInfo == null)
            {   UsuarioInfo usuarioAdmin = (UsuarioInfo)Activator.CreateInstance(_configSeguranca.TipoUsuario);
                usuarioAdmin.CodigoUsuario = _configSeguranca.NomeUsuarioAdministrador;
                usuarioAdmin.Senha = CriptografiaHelper.ReceberMD5Hash(_configSeguranca.SenhaUsuarioAdministrador); //1413 NÃO FUNCIONA A DESCRIPTOGRAFIA, ENTAO SEMPRE CRIPTOGRAFAR  _configSeguranca.SalvarSenhaCriptografada ? CriptografiaHelper.ReceberMD5Hash(_configSeguranca.SenhaUsuarioAdministrador) : _configSeguranca.SenhaUsuarioAdministrador; // 1414 _configSeguranca.SenhaUsuarioAdministrador;
                usuarioAdmin.Nome = "Administrador";
                usuarioAdmin.NomeAbreviado = "Admin";
                usuarioAdmin.Permissoes.Add(new PermissaoAssociadaInfo(){CodigoPermissao = _permissoes.ListaPorTipo.FirstOrDefault(p => p.Tipo == typeof(PermissaoAdministrador)).Permissao.PermissaoInfo.CodigoPermissao,Status = PermissaoAssociadaStatusEnum.Permitido});
                _servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest(){Usuario = usuarioAdmin}); //1436 criando usuario ADMIN
            }
            return 
                new InicializarSegurancaResponse() 
                { 
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }

        #endregion

        #region Autenticacao
        public AutenticarUsuarioResponse AutenticarUsuario(AutenticarUsuarioRequest parametros)
        {   AutenticarUsuarioResponse resposta =new AutenticarUsuarioResponse(){CodigoMensagemRequest = parametros.CodigoMensagem};
            resposta.PreparaResposta(parametros);
            // ===================================================================================================================================
            // 1413 - inicio
            // ===================================================================================================================================
            ReceberServicoSegurancaConfigResponse _configReceberServicoSegurancaConfig = Mensageria.Processar<ReceberServicoSegurancaConfigResponse>(new ReceberServicoSegurancaConfigRequest() { });
            if (_configReceberServicoSegurancaConfig.ServicoSegurancaConfig != null)
                _configSeguranca = _configReceberServicoSegurancaConfig.ServicoSegurancaConfig;
            // ===================================================================================================================================
            // 1413 - fim
            // ===================================================================================================================================
            try
            {   UsuarioInfo usuarioInfo = _servicoPersistencia.ReceberUsuario(new ReceberUsuarioRequest(){CodigoUsuario = parametros.CodigoUsuario}).Usuario;
                // ===================================================================================================================================
                // 1413 - INICIO - Tratando o parametro SALVAR SENHA CRIPTOGRAFADA
                // ===================================================================================================================================
                if (usuarioInfo != null)
                {
                // ===================================================================================================================================
                // 1413 - INICIO - Tratando o parametro SALVAR SENHA CRIPTOGRAFADA
                // A FUNÇÃO CriptografiaHelper.Decriptografar(usuarioInfo.Senha); NAO FUNCIONA
                // ENTÃO SEMPRE CONSIDERAR QUE ESTE PARAMETRO É TRUE
                // ===================================================================================================================================
                    string senha = "";
                    if (parametros.Senha.Length != 32)  // NÃO ESTA CRIPTOGRAFADA
                    {   senha = CriptografiaHelper.ReceberMD5Hash(parametros.Senha);
                    }
                    if (usuarioInfo.Senha.Length != 32) // NÃO ESTA CRIPTOGRAFADA
                    {
                        usuarioInfo.Senha = CriptografiaHelper.ReceberMD5Hash(usuarioInfo.Senha);
                    }
                    if (!_configSeguranca.ValidarSenhas || senha == usuarioInfo.Senha)
                    {       Usuario usuario = new Usuario(usuarioInfo);
                            //===============================================================================================================
                            // 1412 inicio - DEFINE SE A SENHA ESTÁ EXPIRADA
                            //===============================================================================================================
                            bool v_SenhaExpirada = false;
                            bool v_ValidarSenhaExpirada = _configSeguranca.ValidarSenhaExpirada;
                            if (v_ValidarSenhaExpirada)
                                v_SenhaExpirada = v_ValidarSenhaExpirada ? (usuarioInfo.DataExpiracaoSenha < DateTime.Now) : false; 
                            //===============================================================================================================
                            // 1412 fim
                            //===============================================================================================================
                            bool permiteAcesso = usuario.PermissoesAssociadas.EhAdministrador || usuario.PermissoesAssociadas.ConsultarPermissao(typeof(PermissaoAcessarSistema));                             // Verifica se é admin ou possui permissão de acesso ao sistema
                            if (permiteAcesso)
                            {
                                SessaoInfo sessaoInfo =
                                    new SessaoInfo()
                                    {
                                        CodigoSistemaCliente = parametros.CodigoSistemaCliente,
                                        CodigoUsuario = usuarioInfo.CodigoUsuario,
                                        DataCriacao = DateTime.Now,
                                        DataUltimaConsulta = DateTime.Now,
                                        EhSessaoDeAdministrador = usuario.PermissoesAssociadas.EhAdministrador,
                                        SenhaExpirada = v_SenhaExpirada //1412 SenhaExpirada = _config.ValidarSenhaExpirada ? (usuarioInfo.DataExpiracaoSenha < DateTime.Now) : false
                                    };
                                Sessao sessao = new Sessao(sessaoInfo) { Usuario = usuario }; // inicia sessao
                                _sessoes.Add(sessao.SessaoInfo.CodigoSessao, sessao); // Adiciona na lista de sessoes

                                if (_configSeguranca.PersistirSessao)
                                    PersistenciaHelper.Salvar<SessaoPersistenciaInfo>(
                                        sessao.SessaoInfo.CodigoSessao, 
                                        new SessaoPersistenciaInfo() 
                                        {
                                            CodigoSessao = sessao.SessaoInfo.CodigoSessao,
                                            Sessao = sessao
                                        });
                                bool resultadoComplementos = true;
                                string Descricao = "";
                                foreach (KeyValuePair<ComplementoAutenticacaoInfo, IComplementoAutenticacao> item in _complementosAutenticacao)
                                {
                                    resultadoComplementos = resultadoComplementos && (item.Value.ComplementarAutenticacao(parametros, sessao.SessaoInfo).StatusResposta == MensagemResponseStatusEnum.OK);
                                    // 1433 recuperando retorno de validação
                                    Descricao = item.Value.ComplementarAutenticacao(parametros, sessao.SessaoInfo).DescricaoResposta;
                                }
                                // ===================================================================================================================================
                                //1436 - salvar usuario desnecessário neste ponto _servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest(){CodigoSessao = sessao.SessaoInfo.CodigoSessao,Usuario = sessao.Usuario.UsuarioInfo}); //1436
                                // ===================================================================================================================================
                                if (resultadoComplementos)
                                {   resposta.Sessao = sessao.SessaoInfo;
                                    resposta.StatusResposta = MensagemResponseStatusEnum.OK;
                                    if (v_SenhaExpirada) //1412 if (_config.ValidarSenhaExpirada == true && sessao.Usuario.UsuarioInfo.DataExpiracaoSenha < DateTime.Now)
                                    {
                                        resposta.SenhaExpirada = true;
                                        resposta.Sessao.SenhaExpirada = true;
                                    }
                                }
                                else
                                {
                                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma; //1477 para .ErroNegocio tem que ter criticas;
                                    resposta.DescricaoResposta = Descricao; //1433 log = "Usuario no período de bloqueio, confira o cadastro de usuarios ";  //1436 "Usuário não possui acesso ao sistema";
                                }
                            }
                            else
                            {
                                resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                                resposta.DescricaoResposta = "Usuário não possui acesso ao sistema, confira com o ADM no cadastro de usuarios";
                            }
                        }
                        else
                        {
                            resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                            resposta.DescricaoResposta = "Senha inválida (confira o CAPSLOCK ou peça RESET de senha)";
                        }
                    }
                    else
                    {
                        resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                        resposta.DescricaoResposta = "Usuário inválido, confira o cadastro de usuarios";
                    }
            }
            catch (Exception ex)
            {   Log.EfetuarLog(ex, parametros, ModulosOMS.ModuloSeguranca);
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }

        #endregion

        #region Permissões

        /// <summary>
        /// Faz a consulta para saber se a sessao possui determinada permissao
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public VerificarPermissaoResponse VerificarPermissao(VerificarPermissaoRequest parametros)
        {
            // Prepara resposta
            VerificarPermissaoResponse resposta =
                new VerificarPermissaoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Bloco de controle
            try
            {
                // Recupera a sessao
                Sessao sessao = null;
                if(parametros.CodigoSessao != null)
                if (receberSessao(parametros.CodigoSessao, false) != null)
                    sessao = receberSessao(parametros.CodigoSessao);

                // Achou a sessao?
                if (sessao != null)
                {
                    // Faz a consulta da permissao
                    if (parametros.CodigoPermissao != null)
                        resposta.Permitido = sessao.Usuario.PermissoesAssociadas.ConsultarPermissao(parametros.CodigoPermissao);
                    else
                        resposta.Permitido = sessao.Usuario.PermissoesAssociadas.ConsultarPermissao(parametros.TipoPermissao);
                }
                else
                {
                    // Informa na resposta
                    resposta.Permitido = false;
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma; //1477 para .ErroNegocio tem que ter criticas;
                    resposta.DescricaoResposta = "Sessão inválida";
                }
            }
            catch (Exception ex)
            {
                // Log
                Log.EfetuarLog(ex, parametros, ModulosOMS.ModuloSeguranca);

                // Informa na resposta
                resposta.DescricaoResposta = ex.ToString();
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Solicita a lista de permissões do sistema
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ListarPermissoesResponse ListarPermissoes(ListarPermissoesRequest parametros)
        {
            List<PermissaoBase> permissoes = new List<PermissaoBase>();
            this._permissoes.ListaPorTipo.ForEach(p => permissoes.Add(p.Permissao)); //1477 analise

            //1477 permissoes = (from p in permissoes orderby p.PermissaoInfo.NomePermissao select p).ToList();
            
            // Retorna a lista de permissões
            return
                new ListarPermissoesResponse()
                {
                    Permissoes = permissoes
                };
        }

        /// <summary>
        /// Solicita a adição de uma permissão a usuários, grupos ou perfis
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public AssociarPermissaoResponse AssociarPermissao(AssociarPermissaoRequest parametros)
        {
            // Prepara resposta
            AssociarPermissaoResponse resposta =
                new AssociarPermissaoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Bloco de controle
            try
            {

                // Verifica se a sessao tem permissao para adicionar permissao
                VerificarPermissaoResponse verificarAdicionarPermissao =
                    this.VerificarPermissao(
                        new VerificarPermissaoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            TipoPermissao = typeof(PermissaoAssociarPermissao)
                        });

                // Tem?
                if (verificarAdicionarPermissao.Permitido)
                {
                    // Localiza a permissao
                    PermissaoBase permissao = null;
                    if (parametros.CodigoPermissao != null)
                        permissao = _permissoes.ListaPorCodigo.FirstOrDefault(p => p.Codigo == parametros.CodigoPermissao).Permissao;
                    else
                        permissao = _permissoes.ListaPorTipo.FirstOrDefault(p => p.Tipo == parametros.TipoPermissao).Permissao;

                    // Cria a associação de permissao
                    PermissaoAssociadaInfo permissaoAssociada =
                        new PermissaoAssociadaInfo()
                        {
                            CodigoPermissao = permissao.PermissaoInfo.CodigoPermissao,
                            Status = parametros.StatusPermissao
                        };

                    // Associa a usuário?
                    if (parametros.CodigoUsuario != null)
                    {
                        // Carrega o usuario
                        UsuarioInfo usuarioInfo = _servicoPersistencia.ReceberUsuario(
                            new ReceberUsuarioRequest()
                            {
                                CodigoUsuario = parametros.CodigoUsuario
                            }).Usuario;

                        // Apenas se tem usuário
                        if (usuarioInfo != null)
                        {
                            // Verifica se cria ou altera a existente
                            PermissaoAssociadaInfo permissaoAssociadaExistente =
                                (from p in usuarioInfo.Permissoes
                                 where p.CodigoPermissao == permissao.PermissaoInfo.CodigoPermissao
                                 select p).FirstOrDefault();
                            if (permissaoAssociadaExistente != null)
                                permissaoAssociadaExistente.Status = permissaoAssociada.Status;
                            else
                                usuarioInfo.Permissoes.Add(permissaoAssociada);
                            _servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest(){CodigoSessao = parametros.CodigoSessao,Usuario = usuarioInfo}); //1436
                        }
                    }

                    // Associa a usuário grupo?
                    if (parametros.CodigoUsuarioGrupo != null)
                    {
                        // Carrega o usuario
                        UsuarioGrupoInfo usuarioGrupoInfo = _servicoPersistencia.ReceberUsuarioGrupo(
                            new ReceberUsuarioGrupoRequest()
                            {
                                CodigoSessao = parametros.CodigoSessao,
                                CodigoUsuarioGrupo = parametros.CodigoUsuario
                            }).UsuarioGrupo;

                        // Apenas se tem usuário
                        if (usuarioGrupoInfo != null)
                        {
                            // Verifica se cria ou altera a existente
                            PermissaoAssociadaInfo permissaoAssociadaExistente =
                                (from p in usuarioGrupoInfo.Permissoes
                                 where p.CodigoPermissao == permissao.PermissaoInfo.CodigoPermissao
                                 select p).FirstOrDefault();
                            if (permissaoAssociadaExistente != null)
                                permissaoAssociadaExistente.Status = permissaoAssociada.Status;
                            else
                                usuarioGrupoInfo.Permissoes.Add(permissaoAssociada);
                            _servicoPersistencia.SalvarUsuarioGrupo(new SalvarUsuarioGrupoRequest(){CodigoSessao = parametros.CodigoSessao,UsuarioGrupo = usuarioGrupoInfo});
                        }
                    }

                    // Associa a perfil?
                    if (parametros.CodigoPerfil != null)
                    {
                        // Carrega o perfil
                        PerfilInfo perfilInfo = _servicoPersistencia.ReceberPerfil(
                            new ReceberPerfilRequest()
                            {
                                CodigoSessao = parametros.CodigoSessao,
                                CodigoPerfil = parametros.CodigoPerfil
                            }).Perfil;

                        // Apenas se tem usuário
                        if (perfilInfo != null)
                        {
                            // Verifica se cria ou altera a existente
                            PermissaoAssociadaInfo permissaoAssociadaExistente =
                                (from p in perfilInfo.Permissoes
                                 where p.CodigoPermissao == permissao.PermissaoInfo.CodigoPermissao
                                 select p).FirstOrDefault();
                            if (permissaoAssociadaExistente != null)
                                permissaoAssociadaExistente.Status = permissaoAssociada.Status;
                            else
                                perfilInfo.Permissoes.Add(permissaoAssociada);
                            _servicoPersistencia.SalvarPerfil(new SalvarPerfilRequest(){CodigoSessao = parametros.CodigoSessao,Perfil = perfilInfo});
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log
                Log.EfetuarLog(ex, parametros, ModulosOMS.ModuloSeguranca);

                // Informa na resposta
                resposta.DescricaoResposta = ex.ToString();
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Solicita a remoção de uma permissão a usuários, grupos ou perfis
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public DesassociarPermissaoResponse DesassociarPermissao(DesassociarPermissaoRequest parametros)
        {
            // Prepara resposta
            DesassociarPermissaoResponse resposta =
                new DesassociarPermissaoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Bloco de controle
            try
            {
                // Verifica se a sessao tem permissao para associar permissao
                VerificarPermissaoResponse verificarAssociarPermissao =
                    this.VerificarPermissao(
                        new VerificarPermissaoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            TipoPermissao = typeof(PermissaoAssociarPermissao)
                        });

                // Tem?
                if (verificarAssociarPermissao.Permitido)
                {
                    // Localiza a permissao
                    PermissaoBase permissao = null;
                    if (parametros.CodigoPermissao != null)
                        permissao = _permissoes.ListaPorCodigo.FirstOrDefault(p => p.Codigo == parametros.CodigoPermissao).Permissao;
                    else
                        permissao = _permissoes.ListaPorTipo.FirstOrDefault(p => p.Tipo == parametros.TipoPermissao).Permissao; ;

                    // Desassocia a usuário?
                    if (parametros.CodigoUsuario != null)
                    {
                        // Carrega o usuario
                        UsuarioInfo usuarioInfo = _servicoPersistencia.ReceberUsuario(
                            new ReceberUsuarioRequest()
                            {
                                CodigoUsuario = parametros.CodigoUsuario
                            }).Usuario;

                        // Apenas se tem usuário
                        if (usuarioInfo != null)
                        {
                            // Verifica se cria ou altera a existente
                            PermissaoAssociadaInfo permissaoAssociadaExistente =
                                (from p in usuarioInfo.Permissoes
                                 where p.CodigoPermissao == permissao.PermissaoInfo.CodigoPermissao
                                 select p).FirstOrDefault();
                            if (permissaoAssociadaExistente != null)
                                usuarioInfo.Permissoes.Remove(permissaoAssociadaExistente);
                            _servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest(){CodigoSessao = parametros.CodigoSessao,Usuario = usuarioInfo}); //1436 atualizando usuario
                        }
                    }

                    // Desassocia a usuário grupo?
                    if (parametros.CodigoUsuarioGrupo != null)
                    {
                        // Carrega o usuario
                        UsuarioGrupoInfo usuarioGrupoInfo = _servicoPersistencia.ReceberUsuarioGrupo(
                            new ReceberUsuarioGrupoRequest()
                            {
                                CodigoSessao = parametros.CodigoSessao,
                                CodigoUsuarioGrupo = parametros.CodigoUsuario
                            }).UsuarioGrupo;

                        // Apenas se tem usuário
                        if (usuarioGrupoInfo != null)
                        {
                            // Verifica se cria ou altera a existente
                            PermissaoAssociadaInfo permissaoAssociadaExistente =
                                (from p in usuarioGrupoInfo.Permissoes
                                 where p.CodigoPermissao == permissao.PermissaoInfo.CodigoPermissao
                                 select p).FirstOrDefault();
                            if (permissaoAssociadaExistente != null)
                                usuarioGrupoInfo.Permissoes.Remove(permissaoAssociadaExistente);

                            // Salva o usuarioGrupo
                            // ** Se o usuário não tiver permissões para salvar usuárioGrupo, aqui
                            // ** não vai salvar.
                            _servicoPersistencia.SalvarUsuarioGrupo(
                                new SalvarUsuarioGrupoRequest()
                                {
                                    CodigoSessao = parametros.CodigoSessao,
                                    UsuarioGrupo = usuarioGrupoInfo
                                });
                        }
                    }

                    // Desassocia a perfil?
                    if (parametros.CodigoPerfil != null)
                    {
                        // Carrega o perfil
                        PerfilInfo perfilInfo = _servicoPersistencia.ReceberPerfil(
                            new ReceberPerfilRequest()
                            {
                                CodigoSessao = parametros.CodigoSessao,
                                CodigoPerfil = parametros.CodigoPerfil
                            }).Perfil;

                        // Apenas se tem usuário
                        if (perfilInfo != null)
                        {
                            // Verifica se cria ou altera a existente
                            PermissaoAssociadaInfo permissaoAssociadaExistente =
                                (from p in perfilInfo.Permissoes
                                 where p.CodigoPermissao == permissao.PermissaoInfo.CodigoPermissao
                                 select p).FirstOrDefault();
                            if (permissaoAssociadaExistente != null)
                                perfilInfo.Permissoes.Remove(permissaoAssociadaExistente);

                            // Salva o perfil
                            // ** Se o usuário não tiver permissões para salvar perfil, aqui
                            // ** não vai salvar.
                            _servicoPersistencia.SalvarPerfil(
                                new SalvarPerfilRequest()
                                {
                                    CodigoSessao = parametros.CodigoSessao,
                                    Perfil = perfilInfo
                                });
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                // Log
                Log.EfetuarLog(ex, parametros, ModulosOMS.ModuloSeguranca);

                // Informa na resposta
                resposta.DescricaoResposta = ex.ToString();
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
            }

            // Retorna
            return resposta;
        }

        public ValidarItemSegurancaResponse ValidarItemSeguranca(ValidarItemSegurancaRequest parametros)
        {
            ValidarItemSegurancaResponse resposta =new ValidarItemSegurancaResponse(){CodigoMensagemRequest = parametros.CodigoMensagem};
            Sessao sessao = receberSessao(parametros.CodigoSessao, false);
            if (sessao != null)
            {
                foreach (ItemSegurancaInfo itemSeguranca in parametros.ItensSeguranca)
                {
                    bool naoTemItem = false;
                    bool temItem = false;

                    if (itemSeguranca.Permissoes != null)
                    {
                        foreach (string permissaoStr in itemSeguranca.Permissoes)
                        {
                            string codigoPermissao = permissaoStr;
                            Type tipoPermissao = Type.GetType(permissaoStr);
                            if (tipoPermissao != null)
                                codigoPermissao = _permissoes.ListaPorTipo.FirstOrDefault(p => p.Tipo == tipoPermissao).Permissao.PermissaoInfo.CodigoPermissao;
                            if (sessao.Usuario.PermissoesAssociadas.ConsultarPermissao(codigoPermissao))
                                temItem = true;
                            else
                                naoTemItem = true;
                        }
                    }
                    if ((itemSeguranca.TipoAtivacao == ItemSegurancaAtivacaoTipoEnum.TodasAsCondicoes && !naoTemItem) ||
                        itemSeguranca.TipoAtivacao != ItemSegurancaAtivacaoTipoEnum.TodasAsCondicoes)
                    {
                        if (itemSeguranca.Perfis != null)
                        {
                            foreach (string codigoPerfil in itemSeguranca.Perfis)
                            {
                                if (sessao.Usuario.Perfis.FirstOrDefault(p=> p.CodigoPerfil == codigoPerfil) != null)
                                    temItem = true;
                                else
                                    naoTemItem = true;
                            }
                        }
                    }
                    if ((itemSeguranca.TipoAtivacao == ItemSegurancaAtivacaoTipoEnum.TodasAsCondicoes && !naoTemItem) ||
                        itemSeguranca.TipoAtivacao != ItemSegurancaAtivacaoTipoEnum.TodasAsCondicoes)
                    {
                        if (itemSeguranca.Grupos != null)
                        {
                            foreach (string codigoGrupo in itemSeguranca.Grupos)
                            {
                                if (sessao.Usuario.UsuariosGrupo.FirstOrDefault(p=> p.CodigoUsuarioGrupo == codigoGrupo) != null)
                                    temItem = true;
                                else
                                    naoTemItem = true;
                            }
                        }
                    }
                    if (itemSeguranca.TipoAtivacao == ItemSegurancaAtivacaoTipoEnum.QualquerCondicao)
                        itemSeguranca.Valido = temItem;
                    else if (itemSeguranca.TipoAtivacao == ItemSegurancaAtivacaoTipoEnum.TodasAsCondicoes)
                        itemSeguranca.Valido = temItem && !naoTemItem;
                    else if (itemSeguranca.TipoAtivacao == ItemSegurancaAtivacaoTipoEnum.NaoValidar)
                        itemSeguranca.Valido = true;
                    resposta.ItensSeguranca.Add(itemSeguranca);
                }
            }
            else
            {
                resposta.DescricaoResposta = "Sessao informada (" + parametros.CodigoSessao + ") inválida.";
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma; //1477 para .ErroNegocio tem que ter criticas;
            }
            return resposta;
        }

        public SalvarPermissoesResponse SalvarPermissoes(SalvarPermissoesRequest parametros)
        {
            ListarPermissoesResponse responseListar =this.ListarPermissoes(new ListarPermissoesRequest(){CodigoSessao = parametros.CodigoSessao});
            foreach (PermissaoBase permissao in responseListar.Permissoes)
                PersistenciaHelper.Salvar<PermissaoInfo>(parametros.CodigoSessao,permissao.PermissaoInfo);
            return 
                new SalvarPermissoesResponse() 
                { 
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }

        #endregion

        #region Usuário

        /// <summary>
        /// Solicita lista de usuários
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ListarUsuariosResponse ListarUsuarios(ListarUsuariosRequest parametros)
        {
            // Prepara a resposta
            ListarUsuariosResponse resposta = new ListarUsuariosResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
            resposta.PreparaResposta(parametros);

            // Bloco de controle
            try
            {
                // Verifica se a sessao tem permissao para salvar usuario
                VerificarPermissaoResponse verificarListarUsuarios =
                    this.VerificarPermissao(
                        new VerificarPermissaoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            TipoPermissao = typeof(PermissaoListarUsuarios)
                        });

                // Se tem a permissao, 
                if (verificarListarUsuarios.Permitido)
                {   resposta = _servicoPersistencia.ListarUsuarios(parametros);
                }

                else
                    resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;
            }
            catch (Exception ex)
            {
                // Loga a mensagem
                Log.EfetuarLog(ex, parametros, ModulosOMS.ModuloSeguranca);

                // Devolve o status na resposta
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
                resposta.DescricaoResposta = ex.ToString();
            }

            // Retorna 
            Log.EfetuarLog("Listar Usuarios 99", LogTipoEnum.Passagem, ModulosOMS.ModuloSeguranca);
            return resposta;
        }

        /// <summary>
        /// Salva o usuário informado
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public SalvarUsuarioResponse SalvarUsuario(SalvarUsuarioRequest parametros) //1436
        {   // ===================================================================================================================================
            // 1413 - inicio
            // ===================================================================================================================================
            ReceberServicoSegurancaConfigResponse _configReceberServicoSegurancaConfig = Mensageria.Processar<ReceberServicoSegurancaConfigResponse>(new ReceberServicoSegurancaConfigRequest() { }); //1413
            if (_configReceberServicoSegurancaConfig.ServicoSegurancaConfig != null)
                _configSeguranca = _configReceberServicoSegurancaConfig.ServicoSegurancaConfig;
            // ===================================================================================================================================
            // 1413 - fim
            // ===================================================================================================================================
            // Prepara a resposta
            SalvarUsuarioResponse resposta = new SalvarUsuarioResponse(){CodigoMensagemRequest = parametros.CodigoMensagem};

            // Verifica se a sessao tem permissao para salvar usuario
            VerificarPermissaoResponse verificarSalvarUsuario =this.VerificarPermissao(new VerificarPermissaoRequest() { CodigoSessao = parametros.CodigoSessao,TipoPermissao = typeof(PermissaoSalvarUsuario)});

            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarSalvarUsuario.Permitido)
            {
                // Carrega usuario
                UsuarioInfo usuarioInfoOriginal = _servicoPersistencia.ReceberUsuario(new ReceberUsuarioRequest() { CodigoSessao = parametros.CodigoSessao,CodigoUsuario = parametros.Usuario.CodigoUsuario}).Usuario;

                //1432 retornado 17/05/2017 parametros.Usuario.DataExpiracaoSenha = usuarioInfoOriginal.DataExpiracaoSenha; //1432 estava sobreponndo a data de expiração de senha sempre
                //1413 if (!_configSeguranca.SalvarSenhaCriptografada && usuarioInfoOriginal != null)  
                //{
                //    parametros.Usuario.Senha = usuarioInfoOriginal.Senha;
                //    parametros.Usuario.AssinaturaEletronica = usuarioInfoOriginal.AssinaturaEletronica;
                //}

                // Tem permissao para associar permissoes?
                if (!this.VerificarPermissao(new VerificarPermissaoRequest(){CodigoSessao = parametros.CodigoSessao,TipoPermissao = typeof(PermissaoAssociarPermissao)}).Permitido)
                {
                    if (usuarioInfoOriginal != null)
                        parametros.Usuario.Permissoes = usuarioInfoOriginal.Permissoes;
                    else
                        parametros.Usuario.Perfis.Clear();
                }

                // Tem permissao para associar perfis?
                if (!this.VerificarPermissao(new VerificarPermissaoRequest(){CodigoSessao = parametros.CodigoSessao,TipoPermissao = typeof(PermissaoAssociarPerfil)}).Permitido)
                {
                    if (usuarioInfoOriginal != null)
                        parametros.Usuario.Perfis = usuarioInfoOriginal.Perfis;
                    else
                        parametros.Usuario.Perfis.Clear();
                }

                // Tem permissao para associar grupos?
                if (!this.VerificarPermissao(new VerificarPermissaoRequest(){CodigoSessao = parametros.CodigoSessao,TipoPermissao = typeof(PermissaoAssociarUsuarioGrupo)}).Permitido)
                {
                    if (usuarioInfoOriginal != null)
                        parametros.Usuario.Grupos = usuarioInfoOriginal.Grupos;
                    else
                        parametros.Usuario.Grupos.Clear();
                }

                //=====================================================================================================
                //1413 NÃO FUNCIONA A DESCRIPTOGRAFIA, ENTAO SEMPRE CRIPTOGRAFAR
                //if (_configSeguranca.SalvarSenhaCriptografada)
                //{
                //    if (!String.IsNullOrEmpty(parametros.Usuario.Senha))
                //    {
                //        parametros.Usuario.Senha = CriptografiaHelper.ReceberMD5Hash(parametros.Usuario.Senha);
                //    }
                //}
                //=====================================================================================================
                if (!String.IsNullOrEmpty(parametros.Usuario.Senha)) //1356
                    parametros.Usuario.Senha = CriptografiaHelper.ReceberMD5Hash(parametros.Usuario.Senha);
                //=====================================================================================================
                //1432 somar DiasExpiracaoSenha somente quando alterar senha
                //=====================================================================================================
                //1413 na alteração do usuario não calculava a data de expiração senha
                //=====================================================================================================
                //System.DateTime today = System.DateTime.Now;
                //parametros.Usuario.DataExpiracaoSenha = today.AddDays(_configSeguranca.DiasExpiracaoSenha);
                //=====================================================================================================
                // Se é um novo grupo, verifica se o mesmo já existe
                if (parametros.Novo == true)
                {   UsuarioInfo usuario = _servicoPersistencia.ReceberUsuario(new ReceberUsuarioRequest(){CodigoUsuario = parametros.Usuario.CodigoUsuario}).Usuario;

                    if (usuario == null)
                    {
                        resposta = _servicoPersistencia.SalvarUsuario(parametros);  //1436
                    }
                    else
                    {
                        resposta = new SalvarUsuarioResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.ErroValidacao,
                            DescricaoResposta = "Usuário já existe"
                        };
                    }
                }
                else
                {
                    resposta = _servicoPersistencia.SalvarUsuario(parametros);
                }
            }

            return resposta;
        }

        public ReceberUsuarioResponse ReceberUsuario(ReceberUsuarioRequest parametros)
        {
            ReceberUsuarioResponse resposta = new ReceberUsuarioResponse(){CodigoMensagemRequest = parametros.CodigoMensagem};
            resposta = _servicoPersistencia.ReceberUsuario(parametros);
            if (resposta.Usuario != null)
            {
                resposta.Usuario.Senha = null;
                resposta.Usuario.AssinaturaEletronica = null;
            }
            foreach (KeyValuePair<ComplementoAutenticacaoInfo, IComplementoAutenticacao> item in _complementosAutenticacao)
                item.Value.ComplementarUsuario(resposta.Usuario);
            return resposta;
        }

        public RemoverUsuarioResponse RemoverUsuario(RemoverUsuarioRequest parametros)
        {
            RemoverUsuarioResponse resposta =new RemoverUsuarioResponse(){CodigoMensagemRequest = parametros.CodigoMensagem};
            VerificarPermissaoResponse verificarPermissao = this.VerificarPermissao(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        TipoPermissao = typeof(PermissaoRemoverUsuario)
                    });
            if (verificarPermissao.Permitido)
                resposta = _servicoPersistencia.RemoverUsuario(parametros);
            else
                resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;
            return resposta;
        }

        /// <summary>
        /// Solicita a validação da assinatura eletrônica
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ValidarAssinaturaEletronicaResponse ValidarAssinaturaEletronica(ValidarAssinaturaEletronicaRequest parametros)
        {
            // Pega a sessao
            Sessao sessao = receberSessao(parametros.CodigoSessao);

            // Valida
            bool valido = sessao.Usuario.UsuarioInfo.AssinaturaEletronica == parametros.AssinaturaEletronica;

            // Retorna
            return 
                new ValidarAssinaturaEletronicaResponse() 
                { 
                    StatusResposta = valido ? MensagemResponseStatusEnum.OK : MensagemResponseStatusEnum.AcessoNaoPermitido,
                    DescricaoResposta = valido ? null : "Assinatura inválida"
                };
        }

        /// <summary>
        /// Solicita alteração da assinatura eletrônica para a sessão
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public AlterarAssinaturaEletronicaResponse AlterarAssinaturaEletronica(AlterarAssinaturaEletronicaRequest parametros)
        {
            // Pega a sessao
            Sessao sessao = receberSessao(parametros.CodigoSessao);

            // Pega o usuário
            UsuarioInfo usuarioInfo = 
                _servicoPersistencia.ReceberUsuario(
                    new ReceberUsuarioRequest() 
                    { 
                        CodigoUsuario = sessao.Usuario.UsuarioInfo.CodigoUsuario
                    }).Usuario;
            usuarioInfo.AssinaturaEletronica = parametros.NovaAssinaturaEletronica;
            sessao.Usuario.UsuarioInfo.AssinaturaEletronica = parametros.NovaAssinaturaEletronica;
            _servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest() { Usuario = usuarioInfo}); //1436 atualizando nova assinatura
            return new AlterarAssinaturaEletronicaResponse();
        }

        public AlterarSenhaResponse AlterarSenha(AlterarSenhaRequest parametros)
        {   AlterarSenhaResponse resposta = new AlterarSenhaResponse();
            // ===================================================================================================================================
            // 1436 - sem senha nova e sem senha antiga não efetuar alteração de senha
            // ===================================================================================================================================
            bool vnovasenhabranco = false;
            bool vsenhaatualbranco = false;

            vnovasenhabranco = (parametros.NovaSenha == null) ? true : vnovasenhabranco;
            vnovasenhabranco = (parametros.NovaSenha == "") ? true : vnovasenhabranco;
            vsenhaatualbranco = (parametros.SenhaAtual == null) ? true : vsenhaatualbranco;
            vsenhaatualbranco = (parametros.SenhaAtual == "") ? true : vsenhaatualbranco;

            if (vnovasenhabranco && vsenhaatualbranco)
            {
                return resposta;
            }
            // ===================================================================================================================================
            // 1413 - inicio
            // ===================================================================================================================================
            ReceberServicoSegurancaConfigResponse _configReceberServicoSegurancaConfig = Mensageria.Processar<ReceberServicoSegurancaConfigResponse>(new ReceberServicoSegurancaConfigRequest() { }); //1413
            if (_configReceberServicoSegurancaConfig.ServicoSegurancaConfig != null)
                _configSeguranca = _configReceberServicoSegurancaConfig.ServicoSegurancaConfig;
            // ===================================================================================================================================
            // 1413 - fim
            // ===================================================================================================================================
            try
            {   string codigoUsuario = null;
            if (parametros.CodigoUsuario != null)   // (V23 28/03/2019 4.Senha Reset) && SegurancaHelper.VerificarPermissao(parametros.CodigoSessao, typeof(PermissaoAdministrador)))
                {   codigoUsuario = parametros.CodigoUsuario; }
                // (28/03/2019 4.Senha Reset)
                //else if (parametros.CodigoUsuario != null && !SegurancaHelper.VerificarPermissao(parametros.CodigoSessao, typeof(PermissaoAdministrador)))
                //{
                //    throw new Exception("O usuário informado não pode alterar senha de outro usuário");
                //}
                else
                {   Sessao sessao = receberSessao(parametros.CodigoSessao);
                    codigoUsuario = sessao.Usuario.UsuarioInfo.CodigoUsuario;
                }
                UsuarioInfo usuarioInfo =_servicoPersistencia.ReceberUsuario(new ReceberUsuarioRequest(){CodigoUsuario = codigoUsuario}).Usuario;
                //==================================================================================================================
                //1432 - estava consistindo senha no cadastro de usuarios mesmo não sendo digitado nenhuma informação
                //==================================================================================================================
                if (parametros.NovaSenha.Length > 0)
                {
                    //if (parametros.CodigoUsuario == null) //1356 NÃO PERMITIA EFETUAR RESET DE SENHA
                    //{
                    //==================================================================================================================
                    //1414 - VALIDANDO SENHA - INICIO - COPIANDO DO [ServicoPrincipal].public SalvarUsuarioSCFResponse SalvarUsuarioSCF(SalvarUsuarioSCFRequest request)
                    //==================================================================================================================
                    if (parametros.NovaSenha.Length < _configSeguranca.TamanhoMinimoSenha)
                    {
                        CriticaInfo critica = new CriticaInfo();
                        critica.Status = CriticaStatusEnum.ErroNegocio;
                        critica.Descricao = "A nova senha deve conter pelo menos " + _configSeguranca.TamanhoMinimoSenha + " caracteres e foi digitada apenas " + parametros.NovaSenha.Length + "!";
                        resposta.Criticas.Add(critica);
                    }
                    if (_configSeguranca.ExigirLetrasNaSenha == true)
                    {
                        if (!this.contemLetras(parametros.NovaSenha))
                        {
                            CriticaInfo critica = new CriticaInfo();
                            critica.Status = CriticaStatusEnum.ErroNegocio;
                            critica.Descricao = "A nova senha deve conter letras!";
                            resposta.Criticas.Add(critica);
                        }
                    }
                    if (_configSeguranca.ExigirNumerosNaSenha == true)
                    {
                        if (!this.contemNumeros(parametros.NovaSenha))
                        {
                            CriticaInfo critica = new CriticaInfo();
                            critica.Status = CriticaStatusEnum.ErroNegocio;
                            critica.Descricao = "A nova senha deve conter Números!";
                            resposta.Criticas.Add(critica);
                        }
                    }
                    if (parametros.SenhaAtual != null)  //1356, quando for RESET DE SENHA não enviara a SENHA ATUAL
                    {
                        parametros.SenhaAtual = CriptografiaHelper.ReceberMD5Hash(parametros.SenhaAtual); //1413 NAO FUNCIONA A DESCRIPTOGRAFIA, ENTÃO SEMPRE CRIPTOGRAFAR _configSeguranca.SalvarSenhaCriptografada ? CriptografiaHelper.ReceberMD5Hash(parametros.SenhaAtual) : parametros.SenhaAtual; // 1414
                        if (usuarioInfo.Senha != parametros.SenhaAtual)
                        {
                            CriticaInfo critica = new CriticaInfo();
                            critica.Status = CriticaStatusEnum.ErroNegocio;
                            critica.Descricao = "A senha atual não confere com a cadastrada.";
                            resposta.Criticas.Add(critica);
                        }
                    }
                    if (resposta.Criticas.Count == 0)
                    {
                        System.DateTime vhoje = System.DateTime.Now;
                        if ( (parametros.SenhaAtual == null) && (parametros.NovaSenha.Length > 0) ) //1432 SENHA ATUAL EM BRANCO MAS TEM SENHA NOVA, É BOTÃO RESET, ENTÃO DEIXAR A SENHA EXPIRADA PARA FORÇAR ALTERAÇÃO
                        {   usuarioInfo.DataExpiracaoSenha = vhoje;
                        }
                        else
                        {   usuarioInfo.DataExpiracaoSenha = vhoje.AddDays(_configSeguranca.DiasExpiracaoSenha);
                        }

                        usuarioInfo.Senha = CriptografiaHelper.ReceberMD5Hash(parametros.NovaSenha); //1413 NAO FUNCIONA A DESCRIPTOGRAFIA, ENTÃO SEMPRE CRIPTOGRAFAR _configSeguranca.SalvarSenhaCriptografada ? CriptografiaHelper.ReceberMD5Hash(parametros.NovaSenha) : parametros.NovaSenha; // 1414 - PARAMETROS DE SEGURANÇA - DiasExpiracaoSenha - VALIDAÇÃO DE SENHA EXPIRADA NÃO CRIPTOGRAFA SENHA NOVA [estava] usuarioInfo.Senha = parametros.NovaSenha
                        _servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest() { Usuario = usuarioInfo }); //1436 atualizando senha
                        Sessao sessao2 = receberSessao(parametros.CodigoSessao);
                        sessao2.SessaoInfo.SenhaExpirada = false;
                    }
                    //==================================================================================================================
                    // 1414 - VALIDANDO SENHA - FIM - COPIANDO DO [ServicoPrincipal].public SalvarUsuarioSCFResponse SalvarUsuarioSCF(SalvarUsuarioSCFRequest request)
                    //==================================================================================================================
                    //}
                }
                //==================================================================================================================
                //1432 - estava consistindo senha no cadastro de usuarios mesmo não sendo digitado nenhuma informação
                //==================================================================================================================
                // 1414 - PARAMETROS DE SEGURANÇA - DiasExpiracaoSenha - VALIDAÇÃO DE SENHA EXPIRADA NÃO CRIPTOGRAFA SENHA NOVA
                // inibindo 
                // ==============================================================================================================
                //System.DateTime today = System.DateTime.Now;
                //usuarioInfo.DataExpiracaoSenha =  today.AddDays(_configSeguranca.DiasExpiracaoSenha);
                //usuarioInfo.Senha = parametros.NovaSenha;
                //_servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest(){Usuario = usuarioInfo}); 
                //Sessao sessao2 = receberSessao(parametros.CodigoSessao);
                //sessao2.SessaoInfo.SenhaExpirada = false;
                // ==============================================================================================================
                // 1414 - FIM
                // ==============================================================================================================
            }
            catch (Exception ex)
            {   Log.EfetuarLog(ex, parametros, ModulosOMS.ModuloSeguranca);
                new AlterarSenhaResponse().ProcessarExcessao(ex);
            }
            return resposta;
        }


        #endregion

        #region UsuarioGrupo

        /// <summary>
        /// Solicita lista de grupos de usuários
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ListarUsuarioGruposResponse ListarUsuarioGrupos(ListarUsuarioGruposRequest parametros)
        {
            // Prepara a resposta
            ListarUsuarioGruposResponse resposta =
                new ListarUsuarioGruposResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se a sessao tem permissao para salvar usuario
            VerificarPermissaoResponse verificarListarUsuarioGrupos =
                this.VerificarPermissao(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        TipoPermissao = typeof(PermissaoListarUsuarioGrupos)
                    });

            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarListarUsuarioGrupos.Permitido)
                resposta = _servicoPersistencia.ListarUsuarioGrupos(parametros);
            else
                resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Solicitação para salvar um grupo de usuário
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public SalvarUsuarioGrupoResponse SalvarUsuarioGrupo(SalvarUsuarioGrupoRequest parametros)
        {
            // Prepara a resposta
            SalvarUsuarioGrupoResponse resposta =
                new SalvarUsuarioGrupoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se a sessao tem permissao para salvar usuario
            VerificarPermissaoResponse verificarSalvarUsuarioGrupo =
                this.VerificarPermissao(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        TipoPermissao = typeof(PermissaoSalvarUsuarioGrupo)
                    });

            

            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarSalvarUsuarioGrupo.Permitido)
            {
                // Carrega usuario
                UsuarioGrupoInfo usuarioGrupoInfoOriginal =
                    _servicoPersistencia.ReceberUsuarioGrupo(
                        new ReceberUsuarioGrupoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            CodigoUsuarioGrupo = parametros.UsuarioGrupo.CodigoUsuarioGrupo
                        }).UsuarioGrupo;

                // Tem permissao para associar permissoes?
                if (!this.VerificarPermissao(
                        new VerificarPermissaoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            TipoPermissao = typeof(PermissaoAssociarPermissao)
                        }).Permitido)
                {
                    if (usuarioGrupoInfoOriginal != null)
                        parametros.UsuarioGrupo.Permissoes = usuarioGrupoInfoOriginal.Permissoes;
                    else
                        parametros.UsuarioGrupo.Perfis.Clear();
                }

                // Tem permissao para associar perfis?
                if (!this.VerificarPermissao(
                        new VerificarPermissaoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            TipoPermissao = typeof(PermissaoAssociarPerfil)
                        }).Permitido)
                {
                    if (usuarioGrupoInfoOriginal != null)
                        parametros.UsuarioGrupo.Perfis = usuarioGrupoInfoOriginal.Perfis;
                    else
                        parametros.UsuarioGrupo.Perfis.Clear();
                }

               
                

                //Se é um novo grupo, verifica se o mesmo já existe
                if (parametros.Novo == true)
                {

                    //Verifica se o grupo já existe
                    UsuarioGrupoInfo usuarioGrupo = _servicoPersistencia.ReceberUsuarioGrupo(new ReceberUsuarioGrupoRequest()
                    {
                        CodigoUsuarioGrupo = parametros.UsuarioGrupo.CodigoUsuarioGrupo
                    }).UsuarioGrupo;

                    if (usuarioGrupo == null)
                    {
                        // Salva o usuário
                        resposta = _servicoPersistencia.SalvarUsuarioGrupo(parametros);
                    }
                    else
                    {
                        resposta = new SalvarUsuarioGrupoResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.ErroValidacao,
                            DescricaoResposta = "Grupo já existe"

                        };
                    }
                }
                else
                {

                    // Salva o usuário
                    resposta = _servicoPersistencia.SalvarUsuarioGrupo(parametros);
                }
            }

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Solicita a associação de um usuário grupo a outra entidade
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public AssociarUsuarioGrupoResponse AssociarUsuarioGrupo(AssociarUsuarioGrupoRequest parametros)
        {
            // Prepara resposta
            AssociarUsuarioGrupoResponse resposta =
                new AssociarUsuarioGrupoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se o grupo existe
            UsuarioGrupoInfo usuarioGrupo = 
                _servicoPersistencia.ReceberUsuarioGrupo(
                    new ReceberUsuarioGrupoRequest() 
                    { 
                        CodigoSessao = parametros.CodigoSessao,
                        CodigoUsuarioGrupo = parametros.CodigoUsuarioGrupo
                    }).UsuarioGrupo;

            // Achou o grupo?
            if (usuarioGrupo != null)
            {

                // Verifica se a sessao tem permissao para adicionar permissao
                VerificarPermissaoResponse verificarPermissaoResponse =
                    this.VerificarPermissao(
                        new VerificarPermissaoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            TipoPermissao = typeof(PermissaoAssociarUsuarioGrupo)
                        });

                // Tem?
                if (verificarPermissaoResponse.Permitido)
                {
                    // Associa a usuário?
                    if (parametros.CodigoUsuario != null)
                    {
                        // Carrega o usuario
                        UsuarioInfo usuarioInfo = _servicoPersistencia.ReceberUsuario(
                            new ReceberUsuarioRequest()
                            {
                                CodigoUsuario = parametros.CodigoUsuario
                            }).Usuario;

                        // Apenas se tem usuário
                        if (usuarioInfo != null)
                        {
                            // Verifica se ainda não possui a associação
                            if (!usuarioInfo.Grupos.Contains(parametros.CodigoUsuarioGrupo))
                                usuarioInfo.Grupos.Add(parametros.CodigoUsuarioGrupo);
                            _servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest(){CodigoSessao = parametros.CodigoSessao,Usuario = usuarioInfo}); //1436 associando usuario ao grupo
                        }
                    }
                }
                else
                {
                    // Informa acesso não permitido
                    resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;
                }
            }
            else
            {
                // Informa erro
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma; //1477 para .ErroNegocio tem que ter criticas;
                resposta.DescricaoResposta = "Grupo de Usuário inválido para associação.";
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Solicita detalhe do grupo de usuário
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ReceberUsuarioGrupoResponse ReceberUsuarioGrupo(ReceberUsuarioGrupoRequest parametros)
        {
            // Prepara a resposta
            ReceberUsuarioGrupoResponse resposta =
                new ReceberUsuarioGrupoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se a sessao tem permissao 
            //VerificarPermissaoResponse verificarPermissao =
            //    this.VerificarPermissao(
            //        new VerificarPermissaoRequest()
            //        {
            //            CodigoSessao = parametros.CodigoSessao,
            //            TipoPermissao = typeof(PermissaoReceberUsuarioGrupo)
            //        });
            VerificarPermissaoResponse verificarPermissao = new VerificarPermissaoResponse();
            verificarPermissao.Permitido = true;


            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarPermissao.Permitido)
                resposta = _servicoPersistencia.ReceberUsuarioGrupo(parametros);
            else
                resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Solicitação remoção do grupo de usuário
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public RemoverUsuarioGrupoResponse RemoverUsuarioGrupo(RemoverUsuarioGrupoRequest parametros)
        {
            // Prepara a resposta
            RemoverUsuarioGrupoResponse resposta =
                new RemoverUsuarioGrupoResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se a sessao tem permissao 
            VerificarPermissaoResponse verificarPermissao =
                this.VerificarPermissao(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        TipoPermissao = typeof(PermissaoRemoverUsuarioGrupo)
                    });

            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarPermissao.Permitido)
                resposta = _servicoPersistencia.RemoverUsuarioGrupo(parametros);
            else
                resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;

            // Retorna 
            return resposta;
        }

        #endregion

        #region Perfil

        /// <summary>
        /// Solicitação de lista de perfis
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ListarPerfisResponse ListarPerfis(ListarPerfisRequest parametros)
        {
            // Prepara a resposta
            ListarPerfisResponse resposta =
                new ListarPerfisResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se a sessao tem permissao para salvar usuario
            VerificarPermissaoResponse verificarListarPerfis =
                this.VerificarPermissao(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        TipoPermissao = typeof(PermissaoListarPerfis)
                    });

            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarListarPerfis.Permitido)
                resposta = _servicoPersistencia.ListarPerfis(parametros);
            else
                resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Solicitação para salvar um perfil
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public SalvarPerfilResponse SalvarPerfil(SalvarPerfilRequest parametros)
        {
            // Prepara a resposta
            SalvarPerfilResponse resposta =
                new SalvarPerfilResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se a sessao tem permissao para salvar usuario
            VerificarPermissaoResponse verificarSalvarPerfil =
                this.VerificarPermissao(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        TipoPermissao = typeof(PermissaoSalvarPerfil)
                    });

            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarSalvarPerfil.Permitido)
            {
                // Carrega usuario
                PerfilInfo perfilInfoOriginal =
                    _servicoPersistencia.ReceberPerfil(
                        new ReceberPerfilRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            CodigoPerfil = parametros.Perfil.CodigoPerfil
                        }).Perfil;

                // Tem permissao para associar permissoes?
                if (!this.VerificarPermissao(
                        new VerificarPermissaoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            TipoPermissao = typeof(PermissaoAssociarPermissao)
                        }).Permitido)
                {
                    if (perfilInfoOriginal != null)
                        parametros.Perfil.Permissoes = perfilInfoOriginal.Permissoes;
                    else
                        parametros.Perfil.Permissoes.Clear();
                }


                //Se é um novo perfil, verifica se o mesmo já existe
                if (parametros.Novo == true)
                {

                    //Verifica se o perfil já existe
                    PerfilInfo perfil = _servicoPersistencia.ReceberPerfil(new ReceberPerfilRequest()
                    {
                        CodigoPerfil = parametros.Perfil.CodigoPerfil

                    }).Perfil;

                    if (perfil == null)
                    {
                        // Salva o perfil
                        resposta = _servicoPersistencia.SalvarPerfil(parametros);
                    }
                    else
                    {
                        resposta = new SalvarPerfilResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.ErroValidacao,
                            DescricaoResposta = "Perfil já existe"
                        };
                    }
                }
                else
                {
                    // Salva o perfil
                    resposta = _servicoPersistencia.SalvarPerfil(parametros);            
                }



                //// Salva o usuário
                //resposta = _servicoPersistencia.SalvarPerfil(parametros);
            }

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Solicita a associação de um perfil com alguma entidade
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public AssociarPerfilResponse AssociarPerfil(AssociarPerfilRequest parametros)
        {
            // Prepara resposta
            AssociarPerfilResponse resposta =
                new AssociarPerfilResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se o grupo existe
            PerfilInfo perfil =
                _servicoPersistencia.ReceberPerfil(
                    new ReceberPerfilRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        CodigoPerfil = parametros.CodigoPerfil
                    }).Perfil;

            // Achou o perfil?
            if (perfil != null)
            {
                // Verifica se a sessao tem permissao para adicionar permissao
                VerificarPermissaoResponse verificarPermissaoResponse =
                    this.VerificarPermissao(
                        new VerificarPermissaoRequest()
                        {
                            CodigoSessao = parametros.CodigoSessao,
                            TipoPermissao = typeof(PermissaoAssociarPerfil)
                        });

                // Tem?
                if (verificarPermissaoResponse.Permitido)
                {
                    // Associa a usuário?
                    if (parametros.CodigoUsuario != null)
                    {
                        // Carrega o usuario
                        UsuarioInfo usuarioInfo = _servicoPersistencia.ReceberUsuario(
                            new ReceberUsuarioRequest()
                            {
                                CodigoUsuario = parametros.CodigoUsuario
                            }).Usuario;

                        // Apenas se tem usuário
                        if (usuarioInfo != null)
                        {
                            // Verifica se ainda não possui a associação
                            if (!usuarioInfo.Perfis.Contains(parametros.CodigoPerfil))
                                usuarioInfo.Perfis.Add(parametros.CodigoPerfil);
                            _servicoPersistencia.SalvarUsuario(new SalvarUsuarioRequest(){CodigoSessao = parametros.CodigoSessao,Usuario = usuarioInfo});//1436 associando usuari ao perfil
                        }
                    }

                    // Associa a grupo de usuários?
                    if (parametros.CodigoUsuarioGrupo != null)
                    {
                        // Carrega o usuarioGrupo
                        UsuarioGrupoInfo usuarioGrupoInfo = _servicoPersistencia.ReceberUsuarioGrupo(
                            new ReceberUsuarioGrupoRequest()
                            {
                                CodigoUsuarioGrupo = parametros.CodigoUsuarioGrupo
                            }).UsuarioGrupo;

                        // Apenas se tem usuárioGrupo
                        if (usuarioGrupoInfo != null)
                        {
                            // Verifica se ainda não possui a associação
                            if (!usuarioGrupoInfo.Perfis.Contains(parametros.CodigoPerfil))
                                usuarioGrupoInfo.Perfis.Add(parametros.CodigoPerfil);

                            // Salva o usuarioGrupo
                            // ** Se o usuário não tiver permissões para salvar usuárioGrupo, aqui
                            // ** não vai salvar.
                            _servicoPersistencia.SalvarUsuarioGrupo(
                                new SalvarUsuarioGrupoRequest()
                                {
                                    CodigoSessao = parametros.CodigoSessao,
                                    UsuarioGrupo = usuarioGrupoInfo
                                });
                        }
                    }
                }
                else
                {
                    // Informa acesso não permitido
                    resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;
                }
            }
            else
            {
                // Informa erro
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                resposta.DescricaoResposta = "Perfil inválido para associação.";
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Solicita detalhe do perfil
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ReceberPerfilResponse ReceberPerfil(ReceberPerfilRequest parametros)
        {
            // Prepara a resposta
            ReceberPerfilResponse resposta =
                new ReceberPerfilResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            /*** Esse trecho será modificado.
            // Verifica se a sessao tem permissao 
            VerificarPermissaoResponse verificarPermissao =
                this.VerificarPermissao(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        TipoPermissao = typeof(PermissaoReceberPerfil)
                    });

            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarPermissao.Permitido)
                resposta = _servicoPersistencia.ReceberPerfil(parametros);
            else
                resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;

            ***/
            resposta = _servicoPersistencia.ReceberPerfil(parametros);

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Solicitação remoção do perfil
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public RemoverPerfilResponse RemoverPerfil(RemoverPerfilRequest parametros)
        {
            // Prepara a resposta
            RemoverPerfilResponse resposta =
                new RemoverPerfilResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Verifica se a sessao tem permissao 
            VerificarPermissaoResponse verificarPermissao =
                this.VerificarPermissao(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = parametros.CodigoSessao,
                        TipoPermissao = typeof(PermissaoRemoverPerfil)
                    });

            // Se tem a permissao, ajusta as propriedades que não podem ser alteradas
            // pela aplicação e repassa a mensagem para o servico de persistencia
            if (verificarPermissao.Permitido)
                resposta = _servicoPersistencia.RemoverPerfil(parametros);
            else
                resposta.StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido;

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Listar as permissoes associadas com o usuario
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ListarPermissoesAssociadasResponse ListarPermissaoAssociada(ListarPermissoesAssociadasRequest parametros)
        {
            // Prepara resposta
            ListarPermissoesAssociadasResponse resposta =
                new ListarPermissoesAssociadasResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Bloco de controle
            try
            {
                // Recupera a sessao
                Sessao sessao = receberSessao(parametros.CodigoSessao, false);

                // Achou a sessao?
                if (sessao != null)
                {
                    List<PermissaoPorTipo> permissoes = sessao.Usuario.PermissoesAssociadas.ListaPorTipo;

                    List<String> permissoesList = new List<string>();

                    foreach(PermissaoPorTipo perm in permissoes)
                    {
                        permissoesList.Add(perm.Tipo.Name);     
                    }

                    resposta.PermissoesAssociadas = permissoesList;

                    
                }
                else
                {
                    // Informa na resposta                    
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    resposta.DescricaoResposta = "Sessão inválida";
                }
            }
            catch (Exception ex)
            {
                // Log
                Log.EfetuarLog(ex, parametros, ModulosOMS.ModuloSeguranca);

                // Informa na resposta
                resposta.DescricaoResposta = ex.ToString();
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Sessao

        /// <summary>
        /// Retorna informações da sessão solicitada
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public ReceberSessaoResponse ReceberSessao(ReceberSessaoRequest parametros)
        {
            // Prepara resposta
            ReceberSessaoResponse resposta = 
                new ReceberSessaoResponse() 
                { 
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };

            // Se existir, pega a sessao
            if (receberSessao(parametros.CodigoSessaoARetornar, false) != null)
                resposta.Sessao = receberSessao(parametros.CodigoSessaoARetornar, false).SessaoInfo;

            // Retorna
            return resposta;
        }

        #endregion

        #region Configuração de Segurança
        public ReceberServicoSegurancaConfigResponse ReceberServicoSegurancaConfig(ReceberServicoSegurancaConfigRequest request)
        {   ReceberServicoSegurancaConfigResponse resposta = new ReceberServicoSegurancaConfigResponse();
            resposta.PreparaResposta(request);
            try
            {   resposta.ServicoSegurancaConfig =ConfigHelper.Receber<ServicoSegurancaConfig>(request.CodigoSessao);
            }
            catch (Exception ex)
            {   resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }

        public SalvarServicoSegurancaConfigResponse SalvarServicoSegurancaConfig(SalvarServicoSegurancaConfigRequest request)
        {   SalvarServicoSegurancaConfigResponse resposta = new SalvarServicoSegurancaConfigResponse();
            resposta.PreparaResposta(request);
            try
            {   ConfigHelper.Salvar<ServicoSegurancaConfig>(request.CodigoSessao,request.ServicoSegurancaConfig);
                resposta.ServicoSegurancaConfig =ConfigHelper.Receber<ServicoSegurancaConfig>(request.CodigoSessao);
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }
        #endregion

        #endregion

        #region Rotinas Locais

        /// <summary>
        /// Retorna a sessao solicitada
        /// De acordo com as configurações, verifica se também deve
        /// consultar a persistência.
        /// Dispara excessao caso nao encontre a sessao
        /// </summary>
        /// <param name="codigoSessao"></param>
        /// <returns></returns>
        private Sessao receberSessao(string codigoSessao)
        {
            // Repassa a chamada
            return this.receberSessao(codigoSessao, true);
        }

        /// <summary>
        /// Retorna a sessao solicitada
        /// De acordo com as configurações, verifica se também deve
        /// consultar a persistência
        /// </summary>
        /// <param name="codigoSessao"></param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        private Sessao receberSessao(string codigoSessao, bool dispararExcessao)
        {
            // Prepara o retorno
            Sessao sessao = null;
            
            // Verifica se está no cache interno
            if (_sessoes.ContainsKey(codigoSessao))
                sessao = _sessoes[codigoSessao];

            // Se não encontrou, verifica se deve procurar na persistência
            if (sessao == null && _configSeguranca.PersistirSessao)
            {
                // Verifica se a sessao está no banco de dados
                SessaoPersistenciaInfo sessaoPersistencia = 
                    PersistenciaHelper.Receber<SessaoPersistenciaInfo>(codigoSessao, codigoSessao);

                // Recupera a sessao
                if (sessaoPersistencia.Sessao != null)
                {
                    // Pega
                    sessao = sessaoPersistencia.Sessao;

                    // Adiciona na colecao
                    _sessoes.Add(sessao.SessaoInfo.CodigoSessao, sessao);
                }

            }

            // Se nao achou a sessao, dispara erro
            if (sessao == null && dispararExcessao)
                throw new Exception("Sessão (" + codigoSessao + ") não encontrada.");

            // Retorna
            return sessao;
        }

        public RemoverSessaoResponse RemoverSessao(RemoverSessaoRequest request)
        {
            RemoverSessaoResponse res = new RemoverSessaoResponse();
            res.PreparaResposta(request);
            PersistenciaHelper.Remover<SessaoPersistenciaInfo>(request.CodigoSessao, request.CodigoSessaoARemover);
            return res;
        }

        public ListarSessaoResponse ListarSessao(ListarSessaoRequest request)
        {
            ListarSessaoResponse res = new ListarSessaoResponse();
            res.PreparaResposta(request);
            var resultado = PersistenciaHelper.Listar<SessaoPersistenciaInfo>(request.CodigoSessao, new List<CondicaoInfo>() { new CondicaoInfo("DataInclusao", CondicaoTipoEnum.Igual, request.FiltroDataInclusao) }, 0);
            List<Sessao> s = new List<Sessao>();
            resultado.ForEach(p => s.Add(p.Sessao));
            res.Resultado = s;
            return res;
        }

        #endregion

        // 1413
        #region Functions Validadoras Letras Numeros
        public bool contemLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
                return true;
            else
                return false;
        }

        public bool contemNumeros(string texto)
        {
            if (texto.Where(c => char.IsNumber(c)).Count() > 0)
                return true;
            else
                return false;
        }

        #endregion
    }
}
