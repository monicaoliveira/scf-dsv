﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using System.Xml.Serialization;

namespace Resource.Framework.Sistemas.Comum
{
    /// <summary>
    /// Configurações para o servico de seguranca
    /// </summary>
    [Serializable]
    public class ServicoSegurancaConfig
    {
        /// <summary>
        /// Indica o nome do usuário administrador, que será criado na primeira
        /// vez que o sistema for executado
        /// </summary>
        public string NomeUsuarioAdministrador { get; set; }

        /// <summary>
        /// Indica a senha do usuário administrador, que será criado na primeira
        /// vez que o sistema for executado
        /// </summary>
        public string SenhaUsuarioAdministrador { get; set; }

        /// <summary>
        /// Indica se o sistema deverá realizar a validação das senhas
        /// </summary>
        public bool ValidarSenhas { get; set; }

        /// <summary>
        /// Lista de strings (no formato namespace, assembly) de onde devem ser carregadas as permissoes
        /// </summary>
        public List<string> NamespacesPermissoes { get; set; }

        /// <summary>
        /// Lista de complementos de autenticação a serem consultados no momento da autenticação
        /// </summary>
        public List<ComplementoAutenticacaoInfo> ComplementosAutenticacao { get; set; }

        /// <summary>
        /// Indica se a autenticação deve validar senha expirada
        /// </summary>
        public Boolean ValidarSenhaExpirada { get; set; }

        /// <summary>
        /// Indica a quantidade de dias  para o cálculo da data de expiração da senha
        /// </summary>
        public int DiasExpiracaoSenha { get; set; }

        /// <summary>   
        /// Indica o tipo do usuário utilizado no sistema
        /// </summary>
        [XmlIgnore]
        public Type TipoUsuario
        {
            get { return Type.GetType(this.TipoUsuarioString); }
            set { this.TipoUsuarioString = value.FullName + ", " + value.Assembly.FullName; }
        }

        /// <summary>
        /// Propriedade auxiliar para serialização do tipo do usuário
        /// </summary>
        [XmlElement("TipoUsuario")]
        public string TipoUsuarioString { get; set; }

        /// <summary>
        /// Indica se a Sessao deve ser persistida
        /// </summary>
        public bool PersistirSessao { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoSegurancaConfig()
        {
            this.NamespacesPermissoes = new List<string>();
            this.ComplementosAutenticacao = new List<ComplementoAutenticacaoInfo>();
            this.ValidarSenhas = true;
            this.ValidarSenhaExpirada = false;
            this.DiasExpiracaoSenha = 90;
            this.TipoUsuario = typeof(UsuarioInfo);
        }
    }
}
