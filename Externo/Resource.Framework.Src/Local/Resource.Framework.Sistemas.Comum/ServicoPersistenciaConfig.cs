﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Sistemas.Comum
{
    /// <summary>
    /// Classe de configuração do serviço de persistencia
    /// </summary>
    [Serializable]
    public class ServicoPersistenciaConfig
    {
        public List<PersistenciaInfo> Persistencias { get; set; }

        public ServicoPersistenciaConfig()
        {
            this.Persistencias = new List<PersistenciaInfo>();
        }
    }
}
