﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Interface.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Interface.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de lista de grupo de comandos
    /// de execução
    /// </summary>
    public class ListarGruposComandoInterfaceResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de grupos encontrados
        /// </summary>
        public List<GrupoComandoInterfaceInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarGruposComandoInterfaceResponse()
        {
            this.Resultado = new List<GrupoComandoInterfaceInfo>();
        }
    }
}
