﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Interface.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de salvar grupo de 
    /// comandos de execução
    /// </summary>
    public class SalvarGrupoComandoInterfaceResponse : MensagemResponseBase
    {
    }
}
