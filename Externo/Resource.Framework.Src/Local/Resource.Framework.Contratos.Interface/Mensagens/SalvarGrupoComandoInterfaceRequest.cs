﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Interface.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Interface.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar grupo de comandos de execução
    /// </summary>
    public class SalvarGrupoComandoInterfaceRequest : MensagemRequestBase
    {
        /// <summary>
        /// Grupo de comandos de interface a ser salvo
        /// </summary>
        public GrupoComandoInterfaceInfo GrupoComandoInterface { get; set; }
    }
}
