﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Interface.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de grupos de comando de execução
    /// </summary>
    public class ListarGruposComandoInterfaceRequest : MensagemRequestBase
    {
    }
}
