﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Interface.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Interface.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de detalhe de grupo
    /// de comandos de execução
    /// </summary>
    public class ReceberGrupoComandoInterfaceResponse : MensagemResponseBase
    {
        /// <summary>
        /// Grupo de comandos de interface encontrado
        /// </summary>
        public GrupoComandoInterfaceInfo GrupoComandoInterface { get; set; }
    }
}
