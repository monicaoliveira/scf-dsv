﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Representa um campo no book
    /// </summary>
    [Serializable]
    public class BookItemCampoInfo : BookItemInfoBase
    {
        /// <summary>
        /// Valor default para o campo
        /// </summary>
        public object DefaultValue { get; set; }

        /// <summary>
        /// Lista de itens filhos
        /// </summary>
        public List<BookItemCampoInfo> Campos { get; set; }

        /// <summary>
        /// Indica o tipo do campo.
        /// Campo ou grupo
        /// </summary>
        public BookItemCampoTipoEnum TipoCampo { get; set; }

        /// <summary>
        /// Indica o tipo da informação:
        /// numerico, data, string, etc.
        /// </summary>
        public BookItemCampoTipoInformacaoEnum TipoInformacao { get; set; }

        /// <summary>
        /// Instância do conversor utilizado para converter os 
        /// valores para string e para objeto
        /// </summary>
        public ConversorBase Conversor { get; set; }

        /// <summary>
        /// Temporario... contem string do tipo da informacao
        /// </summary>
        public string TipoInformacao2 { get; set; }

        /// <summary>
        /// Indica a identação do campo no book
        /// </summary>
        public int Identacao { get; set; }

        /// <summary>
        /// Indica a posicao inicial do item dentro do bloco.
        /// </summary>
        public int PosicaoInicial { get; set; }

        /// <summary>
        /// Nome do campo
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Quantidade de decimais, caso o tipo da informacao seja numerica
        /// </summary>
        public int Decimais { get; set; }

        /// <summary>
        /// Indica o nome do campo que este campo redefine, caso seja uma redefinicao
        /// </summary>
        public string CampoRedefinido { get; set; }

        /// <summary>
        /// Indica a quantidade de repeticoes deste campo
        /// </summary>
        public int QuantidadeRepeticoes { get; set; }

        /// <summary>
        /// Indica o tamanho do campo no arquivo.
        /// Pode não ser o tamanho real, como nos casos de comp-3
        /// </summary>
        public int Tamanho { get; set; }

        /// <summary>
        /// Comentarios da linha
        /// </summary>
        public List<string> Comentarios { get; set; }

        /// <summary>
        /// Referencia para o campo pai
        /// </summary>
        [XmlIgnore]
        public BookItemCampoInfo CampoPai { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public BookItemCampoInfo()
        {
            this.Comentarios = new List<string>();
            this.Campos = new List<BookItemCampoInfo>();
            this.QuantidadeRepeticoes = 1;
        }

        /// <summary>
        /// Construtor 
        /// </summary>
        public BookItemCampoInfo(string nome)
        {
            this.Comentarios = new List<string>();
            this.Campos = new List<BookItemCampoInfo>();
            this.QuantidadeRepeticoes = 1;

            this.Nome = nome;
            this.TipoCampo = BookItemCampoTipoEnum.Grupo;
        }

        /// <summary>
        /// Construtor 
        /// </summary>
        public BookItemCampoInfo(string nome, ConversorBase conversor)
        {
            this.Comentarios = new List<string>();
            this.Campos = new List<BookItemCampoInfo>();
            this.QuantidadeRepeticoes = 1;

            this.Nome = nome;
            this.Conversor = conversor;
        }

        /// <summary>
        /// Construtor 
        /// </summary>
        public BookItemCampoInfo(string nome, BookItemCampoTipoEnum tipoCampo)
        {
            this.Comentarios = new List<string>();
            this.Campos = new List<BookItemCampoInfo>();
            this.QuantidadeRepeticoes = 1;

            this.Nome = nome;
            this.TipoCampo = tipoCampo;
        }

        /// <summary>
        /// Construtor 
        /// </summary>
        public BookItemCampoInfo(string nome, int tamanho)
        {
            this.Comentarios = new List<string>();
            this.Campos = new List<BookItemCampoInfo>();
            this.QuantidadeRepeticoes = 1;

            this.Nome = nome;
            this.Tamanho = tamanho;
            this.TipoCampo = BookItemCampoTipoEnum.Campo;
        }

        /// <summary>
        /// Construtor 
        /// </summary>
        public BookItemCampoInfo(string nome, int tamanho, ConversorBase conversor)
        {
            this.Comentarios = new List<string>();
            this.Campos = new List<BookItemCampoInfo>();
            this.QuantidadeRepeticoes = 1;

            this.Nome = nome;
            this.Tamanho = tamanho;
            this.TipoCampo = BookItemCampoTipoEnum.Campo;
            this.Conversor = conversor;
        }

        /// <summary>
        /// Construtor 
        /// </summary>
        public BookItemCampoInfo(string nome, int tamanho, ConversorBase conversor, object defaultValue)
        {
            this.Comentarios = new List<string>();
            this.Campos = new List<BookItemCampoInfo>();
            this.QuantidadeRepeticoes = 1;

            this.Nome = nome;
            this.Tamanho = tamanho;
            this.TipoCampo = BookItemCampoTipoEnum.Campo;
            this.Conversor = conversor;
            this.DefaultValue = defaultValue;
        }

        /// <summary>
        /// Construtor 
        /// </summary>
        public BookItemCampoInfo(string nome, int tamanho, BookItemCampoInfo campoPai)
        {
            this.Comentarios = new List<string>();
            this.Campos = new List<BookItemCampoInfo>();
            this.QuantidadeRepeticoes = 1;

            this.Nome = nome;
            this.Tamanho = tamanho;
            this.CampoPai = campoPai;
            this.TipoCampo = BookItemCampoTipoEnum.Campo;
        }

        /// <summary>
        /// Faz o calculo do tamanho do grupo ou retorna o tamanho do campo
        /// </summary>
        /// <returns></returns>
        public int ReceberTamanho()
        {
            if (this.TipoCampo == BookItemCampoTipoEnum.Campo)
                return this.Tamanho;
            else
                return this.Campos.Sum(
                    i => 
                        i.CampoRedefinido == null ? 
                            i.ReceberTamanho() * i.QuantidadeRepeticoes 
                            : 0);
        }

        /// <summary>
        /// Retorna o caminho deste campo
        /// </summary>
        /// <returns></returns>
        public string ReceberCaminho()
        {
            // Se tem pai, pede caminho do pai
            if (this.CampoPai != null)
                return this.CampoPai.ReceberCaminho() + "/" + this.Nome;
            else
                return this.Nome;
        }
    }
}
