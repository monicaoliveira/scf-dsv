﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Classe auxiliar com informacoes adicionais do campo
    /// </summary>
    public class BookItemCampoHelper
    {
        /// <summary>
        /// Informacoes do campo
        /// </summary>
        public BookItemCampoInfo BookItemCampoInfo { get; set; }

        /// <summary>
        /// Posicao inicial do campo informado
        /// </summary>
        public int PosicaoInicial { get; set; }
    }
}
