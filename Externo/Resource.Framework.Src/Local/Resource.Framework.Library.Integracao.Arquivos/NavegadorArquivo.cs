﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Permite fazer a navegacao no arquivo
    /// </summary>
    public class NavegadorArquivo
    {
        /// <summary>
        /// Book
        /// </summary>
        public BookInfo Book { get; set; }

        /// <summary>
        /// Arquivo
        /// </summary>
        public Stream Arquivo { get; set; }

        /// <summary>
        /// Stream para leitura do arquivo
        /// </summary>
        public StreamReader Reader { get; set; }

        /// <summary>
        /// Conteudo da linha atual
        /// </summary>
        public string LinhaAtual { get; set; }

        /// <summary>
        /// Conteudo da proxima linha
        /// </summary>
        public string ProximaLinha { get; set; }

        /// <summary>
        /// Indica o numero da linha atual
        /// </summary>
        public int NumeroLinha { get; set; }

        /// <summary>
        /// Construtor. Pede o book e o caminho do arquivo
        /// </summary>
        /// <param name="book"></param>
        /// <param name="caminhoArquivo"></param>
        public NavegadorArquivo(BookInfo book, string caminhoArquivo)
        {
            this.Book = book;
            this.Arquivo = File.Open(caminhoArquivo, FileMode.Open, FileAccess.Read);
            inicializar();
        }

        /// <summary>
        /// Construtor. Pede o book e o stream do arquivo
        /// </summary>
        /// <param name="book"></param>
        /// <param name="arquivo"></param>
        public NavegadorArquivo(BookInfo book, Stream arquivo)
        {
            this.Book = book;
            this.Arquivo = arquivo;
            inicializar();
        }

        /// <summary>
        /// Inicializacao comum aos construtores
        /// </summary>
        private void inicializar()
        {
            this.Reader = new StreamReader(this.Arquivo);
            if (!this.Reader.EndOfStream)
                this.ProximaLinha = this.Reader.ReadLine();
        }

        /// <summary>
        /// Posiciona na proxima linha e informa se conseguiu ler
        /// </summary>
        /// <returns></returns>
        public bool LerProximaLinha()
        {
            // Retorno
            bool retorno = false;
            
            // Tem proxima linha?
            if (this.ProximaLinha != null)
            {
                // A linha atual é a proxima
                this.LinhaAtual = this.ProximaLinha;
                this.NumeroLinha++;

                // Lê a proxima
                if (!this.Reader.EndOfStream)
                    this.ProximaLinha = this.Reader.ReadLine();
                else
                    this.ProximaLinha = null;

                // Retorna
                return true;
            }
            else
            {
                // Limpa a linha atual
                this.LinhaAtual = null;

                // Sinaliza
                retorno = false;
            }

            // Retorna
            return retorno;
        }

        /// <summary>
        /// Indica se esta é a última linha
        /// </summary>
        /// <returns></returns>
        public bool UltimaLinha()
        {
            return this.ProximaLinha == null;
        }

        /// <summary>
        /// Le o valor de um campo
        /// </summary>
        /// <param name="campo"></param>
        /// <returns></returns>
        public object this[string campo]
        {
            get
            {
                // Extrai o valor
                return 
                    InterpretadorArquivos.ExtrairValor(this.Book, campo, this.LinhaAtual);
            }
        }

        /// <summary>
        /// Le o valor de um campo
        /// </summary>
        /// <param name="campo"></param>
        /// <returns></returns>
        public object this[string campo, bool interpretar]
        {
            get
            {
                // Extrai o valor
                return
                    InterpretadorArquivos.ExtrairValor(this.Book, campo, this.LinhaAtual, interpretar);
            }
        }

    }
}
