﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Tipo de interpretacao de valores que irá ser feito no book
    /// </summary>
    public enum BookInterpretacaoValorTipoEnum
    {
        /// <summary>
        /// Interpreta os valores através dos tipos no formato do mainframe que 
        /// estão em cada campo
        /// </summary>
        TiposMainframe,

        /// <summary>
        /// Interpreta os valores através dos conversores associados. Se não houver
        /// conversor, assume tipo string
        /// </summary>
        Conversores
    }
}
