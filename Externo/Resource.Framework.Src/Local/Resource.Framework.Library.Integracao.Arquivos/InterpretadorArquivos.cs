﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    public class InterpretadorArquivos
    {

        /// <summary>
        /// Representacao de um numero positivo com sinal no arquivo do mainframe
        /// </summary>
        private static string numerosPositivos = "{ABCDEFGHI";

        /// <summary>
        /// Representacao de um numero negativo com sinal no arquivo do mainframe
        /// </summary>
        private static string numerosNegativos = "}JKLMNOPQR";

        /// <summary>
        /// Faz a interpretacao do book
        /// </summary>
        /// <param name="nomeArquivo"></param>
        /// <returns></returns>
        public static BookInfo InterpretarBook(string nomeArquivo)
        {
            return InterpretarBook(File.Open(nomeArquivo, FileMode.Open, FileAccess.Read));
        }

        /// <summary>
        /// Faz a interpretacao do book
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public static BookInfo InterpretarBook(Stream arquivo)
        {
            // Inicializa
            BookInfo bookInfo = new BookInfo();
            BookItemCampoInfo campoPai = null;
            BookItemCampoInfo ultimoCampo = null;
            
            // Cria reader
            StreamReader reader = new StreamReader(arquivo);

            // Contador de linhas
            int numeroLinha = 1;

            // Inicializa
            List<string> linhas = new List<string>();

            // Varre as linhas
            while (!reader.EndOfStream)
            {
                // Le a linha
                string linha = reader.ReadLine();

                // Verifica se tem algo na linha
                if (linha.Length <= 6)
                {
                    // Coloca como comentário
                    bookInfo.Itens.Add(
                        new BookItemComentarioInfo()
                        {
                            Texto = linha,
                            LinhaCabecalho = new List<string>() { linha },
                            LinhaNumeros = new List<int>() { numeroLinha++ }
                        });
                }
                // Verifica se é linha de comentario (7a posicao)
                else if (linha[6] == '*')
                {
                    // Comentario
                    bookInfo.Itens.Add( 
                        new BookItemComentarioInfo() 
                        { 
                            Texto = linha.Substring(6, linha.Length - 7),
                            LinhaCabecalho = new List<string>() { linha.Substring(0, 6) },
                            LinhaNumeros = new List<int>() { numeroLinha++ }
                        });
                }
                else
                {
                    // Adiciona na lista de linhas a processar
                    linhas.Add(linha);

                    // Se o comando termina nesta linha, processa
                    if (linha.Contains('.'))
                    {
                        // Cria o campo
                        BookItemCampoInfo bookItemCampoInfo = new BookItemCampoInfo();

                        // Preprocessa as linhas: retira o cabecalho e os comentarios de n linhas
                        string linhaProcessarInteira = "";
                        foreach (string linhaProcessar in linhas)
                        {
                            // Variavel para trabalhar a linha
                            string linhaProcessar2 = linhaProcessar;

                            // Adiciona numero da linha
                            bookItemCampoInfo.LinhaNumeros.Add(numeroLinha++);

                            // Retira header da linha (até coluna 7)
                            bookItemCampoInfo.LinhaCabecalho.Add(linhaProcessar2.Substring(0, 7));
                            linhaProcessar2 = linhaProcessar2.Remove(0, 7);
                            
                            // Retira comentários (a partir da coluna 73)
                            if (linhaProcessar2.Length > (73 - 7))
                            {
                                bookItemCampoInfo.Comentarios.Add(linhaProcessar2.Substring(73 - 7));
                                linhaProcessar2 = linhaProcessar2.Remove(73 - 7);
                            }

                            // Faz com que o ponto (fim do campo) tenha 1 espaço antes dele
                            int posicaoPonto = linhaProcessar2.IndexOf('.');
                            if (posicaoPonto > 0)
                                linhaProcessar2 = linhaProcessar2.Insert(posicaoPonto, " ");

                            // Adiciona na string inteira
                            linhaProcessarInteira += linhaProcessar2;
                        }

                        // Quebra pelos espacos
                        string[] linhaProcessar3 = Regex.Split(linhaProcessarInteira.Trim(), " +");

                        // Processa
                        int i = 0;
                        while (i < linhaProcessar3.Length)
                        {
                            // Pega o valor
                            string linhaProcessar4 = linhaProcessar3[i++].Trim().ToUpper();

                            // Se for '.', termina
                            if (linhaProcessar4 == ".")
                                break;

                            // Se identacao nao foi colocado, é identacao
                            if (bookItemCampoInfo.Identacao == 0)
                            {
                                bookItemCampoInfo.Identacao = int.Parse(linhaProcessar4);
                                continue;
                            }

                            // Se nome nao foi colocado, é nome
                            if (bookItemCampoInfo.Nome == null)
                            {
                                bookItemCampoInfo.Nome = linhaProcessar4;
                                continue;
                            }

                            // Redefinicao?
                            if (linhaProcessar4 == "REDEFINES")
                            {
                                // Proximo elemento contem o nome do elemento redefinido
                                string nomeElemento = linhaProcessar3[i++].Trim().ToUpper();
                                bookItemCampoInfo.CampoRedefinido = nomeElemento;
                                continue;
                            }

                            // Quantidade de Ocorrencias?
                            if (linhaProcessar4 == "OCCURS")
                            {
                                // Proximo elemento contem quantidade de ocorrencias
                                string qtdeOcorrencias = linhaProcessar3[i++].Trim();
                                bookItemCampoInfo.QuantidadeRepeticoes = int.Parse(qtdeOcorrencias);
                                continue;
                                // Se o proximo elemento for a palavra times, ela será ignorada
                            }

                            // Tipo do campo?
                            if (linhaProcessar4 == "PIC")
                            {
                                // Proximo elemento contem informacoes do tipo da informacao
                                string tipoInformacao = linhaProcessar3[i++].Trim();
                                bookItemCampoInfo.TipoInformacao2 = tipoInformacao;
                                continue;
                            }
                        }

                        // Infere o pai deste item
                        if (ultimoCampo != null)
                        {
                            // Verifica identacao
                            if (bookItemCampoInfo.Identacao > ultimoCampo.Identacao)
                            {
                                // Aumentou, é filho do item
                                campoPai = ultimoCampo;
                            }
                            else if (bookItemCampoInfo.Identacao == ultimoCampo.Identacao)
                            {
                                // Nao mudou, é o pai do item anterior
                                campoPai = ultimoCampo.CampoPai;
                            }
                            else
                            {
                                // Diminuiu... acha o pai que tem identacao menor
                                campoPai = ultimoCampo;
                                while (campoPai != null)
                                {
                                    if (campoPai.Identacao < bookItemCampoInfo.Identacao)
                                        break;
                                    campoPai = campoPai.CampoPai;
                                }
                            }
                        }

                        // Indica se é campo ou grupo
                        if (bookItemCampoInfo.TipoInformacao2 == null)
                            bookItemCampoInfo.TipoCampo = BookItemCampoTipoEnum.Grupo;
                        else
                            bookItemCampoInfo.TipoCampo = BookItemCampoTipoEnum.Campo;

                        // Interpreta o tipo de informacao
                        interpretarTipoInformacao(bookItemCampoInfo);

                        // Verifica em que colecao deve adicionar
                        if (campoPai == null)
                        {
                            // Adiciona o campo na colecao do book
                            bookInfo.Itens.Add(bookItemCampoInfo);
                        }
                        else
                        {
                            // Adiciona item na colecao do campo pai
                            bookItemCampoInfo.CampoPai = campoPai;
                            campoPai.Campos.Add(bookItemCampoInfo);
                        }

                        // Limpa o buffer de linhas
                        linhas.Clear();

                        // Informa ultimo campo
                        ultimoCampo = bookItemCampoInfo;
                    }
                }
            }

            // Fecha o arquivo
            arquivo.Close();

            // Insere a lista de campos
            foreach (BookItemCampoInfo item in bookInfo.Itens.FindAll(i => i is BookItemCampoInfo))
                bookInfo.Campos.Add(item);

            // Calcula as posicoes iniciais
            calculaPosicaoTamanhoCampos(0, bookInfo.Campos);

            // Retorna 
            return bookInfo;
        }

        /// <summary>
        /// Calcula a posicao dos campos
        /// </summary>
        /// <param name="campos"></param>
        public static void calculaPosicaoTamanhoCampos(int posicaoInicial, List<BookItemCampoInfo> campos)
        {
            // Varre os campos
            foreach (BookItemCampoInfo campo in campos)
            {
                // Informa posicao inicial atual
                if (campo.CampoPai != null)
                    campo.PosicaoInicial = posicaoInicial;
                else
                    campo.PosicaoInicial = 0;

                // Incrementa posicao inicial
                // Se for redefinicao de campo, mantem aonde está
                if (campo.CampoRedefinido != null)
                    campo.PosicaoInicial -= campo.ReceberTamanho();
                else
                    posicaoInicial += campo.ReceberTamanho();

                // Pede calculo dos campos filhos
                calculaPosicaoTamanhoCampos(0, campo.Campos);

                // Se nao tem tamanho, calcula
                if (campo.TipoCampo == BookItemCampoTipoEnum.Grupo)
                    campo.Tamanho = campo.ReceberTamanho();
            }

        }

        /// <summary>
        /// Interpreta o tipo do campo
        /// </summary>
        /// <param name="x"></param>
        private static void interpretarTipoInformacao(BookItemCampoInfo bookItemCampoInfo)
        {
            // String com o tipo
            string tipoStr = bookItemCampoInfo.TipoInformacao2;

            // Apenas se tem informacao
            if (tipoStr != null)
            {
                // Extrai o tamanho
                int posTamanho = tipoStr.IndexOf("(");
                int posTamanhoFim = tipoStr.IndexOf(")");

                // Tipo da informação
                string tipo = tipoStr.Substring(0, posTamanho);
                switch (tipo)
                {
                    case "X":
                        bookItemCampoInfo.TipoInformacao = BookItemCampoTipoInformacaoEnum.Texto;
                        break;
                    case "9":
                        bookItemCampoInfo.TipoInformacao = BookItemCampoTipoInformacaoEnum.Numerico;

                        // Verifica se tem decimais
                        if (tipoStr.Contains("V"))
                        {
                            string[] tipoStr2 = tipoStr.Split('V');
                            if (tipoStr2.Length == 1)
                            {
                                tipoStr2[1] = tipoStr2[1].Trim();

                                if (tipoStr2[1].Contains("("))
                                {
                                    // Extrai o número de dentro dos parenteses
                                    int posTamanho2 = tipoStr2[1].IndexOf("(");
                                    int posTamanhoFim2 = tipoStr2[1].IndexOf(")");
                                    bookItemCampoInfo.Decimais =
                                        int.Parse(
                                            tipoStr2[1].Substring(posTamanho2 + 1, posTamanhoFim2 - posTamanho2 - 1));
                                }
                                else
                                {
                                    // Quantidade de decimais é a quantidade de 9s
                                    bookItemCampoInfo.Decimais = tipoStr2[1].Length;
                                }
                            }
                            else
                            {
                                tipoStr2[0] = tipoStr2[0].Trim();
                                tipoStr2[1] = tipoStr2[1].Trim();
                                int auxTamanho = 0;
                                if (tipoStr2[0].Contains("("))
                                {
                                    // Extrai o número de dentro dos parenteses
                                    int posTamanho2 = tipoStr2[0].IndexOf("(");
                                    int posTamanhoFim2 = tipoStr2[0].IndexOf(")");
                                    auxTamanho = int.Parse(tipoStr2[0].Substring(posTamanho2 + 1, posTamanhoFim2 - posTamanho2 - 1));
                                }
                                if (tipoStr2[1].Contains("("))
                                {
                                    // Extrai o número de dentro dos parenteses
                                    int posTamanho2 = tipoStr2[1].IndexOf("(");
                                    int posTamanhoFim2 = tipoStr2[1].IndexOf(")");
                                    bookItemCampoInfo.Decimais =
                                        int.Parse(
                                            tipoStr2[1].Substring(posTamanho2 + 1, posTamanhoFim2 - posTamanho2 - 1));
                                }
                                // Tamanho
                                bookItemCampoInfo.Tamanho = auxTamanho + bookItemCampoInfo.Decimais;
                                bookItemCampoInfo.TipoInformacao2 = "V".PadRight(bookItemCampoInfo.Decimais + 1, '9');
                                return;
                            }
                        }

                        break;
                    default:
                        bookItemCampoInfo.TipoInformacao = BookItemCampoTipoInformacaoEnum.Outros;
                        break;
                }
                bookItemCampoInfo.Tamanho = 
                    int.Parse(tipoStr.Substring(posTamanho + 1, posTamanhoFim - posTamanho - 1)) +
                        bookItemCampoInfo.Decimais;

            }
        }

        public static string EscreverLinha(BookInfo book, string campoRaiz, Dictionary<string, object> valores)
        {
            // Faz a tradução dos campos
            Dictionary<BookItemCampoHelper, object> valores2 = new Dictionary<BookItemCampoHelper, object>();
            foreach (KeyValuePair<string, object> item in valores)
                valores2.Add(book.ReceberDetalheCampo(campoRaiz != null ? campoRaiz + "/" + item.Key : item.Key), item.Value);

            // Repassa a chamada
            return InterpretadorArquivos.EscreverLinha(book, campoRaiz, valores2);
        }

        /// <summary>
        /// Faz a montagem completa de uma linha
        /// </summary>
        /// <param name="book"></param>
        /// <param name="valores"></param>
        /// <returns></returns>
        public static string EscreverLinha(BookInfo book, string campoRaiz, Dictionary<BookItemCampoHelper, object> valores)
        {
            // Prepara tamanho da linha
            int tamanho = 0;

            // Pega o campo raiz, ou, se for nulo, calcula o tamanho de todos os campos da raiz
            BookItemCampoInfo campoInfoRaiz = null;
            if (campoRaiz == null)
            {
                foreach (BookItemCampoInfo campo in book.Campos)
                    tamanho += campo.Tamanho;
            }
            else
            {
                campoInfoRaiz = book.ReceberDetalheCampo(campoRaiz).BookItemCampoInfo;
                tamanho = campoInfoRaiz.Tamanho;
            }

            // Cria a linha
            string linha = new string(' ', tamanho);

            // Varre os campos informados setando os valores
            foreach (KeyValuePair<BookItemCampoHelper, object> item in valores)
                linha = InterpretadorArquivos.EscreverValor(book, item.Key, linha, item.Value);

            // Coloca valores default que nao foram informados
            List<BookItemCampoInfo> campos = 
                campoRaiz == null ? book.Campos : campoInfoRaiz.Campos;
            foreach (BookItemCampoInfo campo in campos.Where(i => i.DefaultValue != null))
            {
                // Verifica se existe
                BookItemCampoInfo valor = 
                    (from v in valores
                    where v.Key.BookItemCampoInfo == campo
                    select v.Key.BookItemCampoInfo).FirstOrDefault();

                // Se nao existe, coloca default
                if (valor == null)
                    linha = 
                        InterpretadorArquivos.EscreverValor(
                            book, campo.ReceberCaminho(), linha, campo.DefaultValue);
            }
            
            // Retorna a linha preenchida
            return linha;
        }

        /// <summary>
        /// Faz a montagem completa de uma linha, com separador
        /// </summary>
        /// <returns></returns>
        public static string EscreverLinhaSeparador(string separador, BookInfo book, string campoRaiz, params object[] valores)
        {
            // Refaz a coleção de valores
            Dictionary<string, object> valores2 = new Dictionary<string, object>();
            for (int i = 0; i < valores.Length; i += 2)
                valores2.Add((string)valores[i], valores[i + 1]);

            // Repassa a chamada
            return InterpretadorArquivos.EscreverLinhaSeparador(separador, book, campoRaiz, valores2);
        }

        /// <summary>
        /// Faz a montagem completa de uma linha, com separador
        /// </summary>
        /// <returns></returns>
        public static string EscreverLinhaSeparador(string separador, BookInfo book, string campoRaiz, Dictionary<string, object> valores)
        {
            // Faz a tradução dos campos
            Dictionary<BookItemCampoHelper, object> valores2 = new Dictionary<BookItemCampoHelper, object>();
            foreach (KeyValuePair<string, object> item in valores)
                valores2.Add(book.ReceberDetalheCampo(campoRaiz != null ? campoRaiz + "/" + item.Key : item.Key), item.Value);

            // Repassa a chamada
            return InterpretadorArquivos.EscreverLinhaSeparador(separador, book, campoRaiz, valores2);
        }

        /// <summary>
        /// Faz a montagem completa de uma linha, com separador
        /// </summary>
        /// <returns></returns>
        public static string EscreverLinhaSeparador(string separador, BookInfo book, string campoRaiz, Dictionary<BookItemCampoHelper, object> valores)
        {
            // Prepara tamanho da linha
            int tamanho = 0;

            // Pega o campo raiz, ou, se for nulo, calcula o tamanho de todos os campos da raiz
            BookItemCampoInfo campoInfoRaiz = null;
            if (campoRaiz == null)
            {
                foreach (BookItemCampoInfo campo in book.Campos)
                    tamanho += campo.Tamanho;
            }
            else
            {
                campoInfoRaiz = book.ReceberDetalheCampo(campoRaiz).BookItemCampoInfo;
                tamanho = campoInfoRaiz.Tamanho;
            }

            // Prepara a linha
            StringBuilder linha = new StringBuilder();

            // Monta o dicionario de nomes de campos
            Dictionary<string, object> dicionarioValores = new Dictionary<string,object>();
            foreach (KeyValuePair<BookItemCampoHelper, object> item in valores)
                dicionarioValores.Add(item.Key.BookItemCampoInfo.Nome, item.Value);
            
            // Varre os campos, inserindo os valores
            List<BookItemCampoInfo> campos = campoRaiz == null ? book.Campos : campoInfoRaiz.Campos;
            foreach (BookItemCampoInfo campo in campos)
            {
                // Verifica se informou o valor, senao pega o valor default do campo
                object valor = null;
                if (dicionarioValores.ContainsKey(campo.Nome))
                    valor = dicionarioValores[campo.Nome];
                else
                    valor = campo.DefaultValue;

                // Transforma o valor em string
                string valorFormatado = null;
                if (campo.Conversor != null)
                    valorFormatado = campo.Conversor.ConverterParaString(campo, valor).Trim();
                else
                    valorFormatado = valor != null ? valor.ToString().Trim() : "";

                // Escreve o valor na linha
                linha.Append(valorFormatado + separador);
            }

            // Retira o ultimo separador
            linha.Remove(linha.Length - 1, 1);

            // Retorna 
            return linha.ToString();
        }

        /// <summary>
        /// Escreve o valor informado no campo desejado.
        /// Retorna a linha com o valor preenchido
        /// </summary>
        /// <param name="book"></param>
        /// <param name="campo"></param>
        /// <param name="linha"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string EscreverValor(BookInfo book, BookItemCampoHelper campo, string linha, object valorObj)
        {
            return InterpretadorArquivos.EscreverValor(book, campo, linha, valorObj, true);
        }

        /// <summary>
        /// Escreve o valor informado no campo desejado.
        /// Retorna a linha com o valor preenchido
        /// </summary>
        /// <param name="book"></param>
        /// <param name="campo"></param>
        /// <param name="linha"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string EscreverValor(BookInfo book, BookItemCampoHelper campo, string linha, object valorObj, bool interpretarValor)
        {
            // Pega o tamanho do campo
            int tamanho = campo.BookItemCampoInfo.ReceberTamanho();

            // Transforma o valor em string
            string valor = null;
            if (book.TipoInterpretacaoValor == BookInterpretacaoValorTipoEnum.TiposMainframe)
            {
                // Transforma diretamente em string
                valor = valorObj.ToString();
            }
            else
            {
                // Faz a interpretacao do valor
                if (campo.BookItemCampoInfo.Conversor != null && interpretarValor)
                    valor =
                        campo.BookItemCampoInfo.Conversor.ConverterParaString(
                            campo.BookItemCampoInfo, valorObj);
                else
                    valor = valorObj.ToString();
            }

            // Formata no tamanho correto
            if (valor.Length > tamanho)
                valor = valor.Substring(0, tamanho);
            else if (valor.Length < tamanho)
                valor = valor.PadRight(tamanho);

            if (linha != "") //1477
            {   // Substitui o valor
                if (campo.PosicaoInicial == 0)
                    linha =
                        valor +
                        linha.Substring(tamanho);
                else if (campo.PosicaoInicial + tamanho < linha.Length)
                    linha =
                        linha.Substring(0, campo.PosicaoInicial) +
                        valor +
                        linha.Substring(campo.PosicaoInicial + tamanho);
                else
                    linha =
                        linha.Substring(0, campo.PosicaoInicial) +
                        valor;
            }

            // Retorna
            return linha;
        }

        /// <summary>
        /// Escreve o valor informado no campo desejado.
        /// Retorna a linha com o valor preenchido
        /// </summary>
        /// <param name="book"></param>
        /// <param name="campo"></param>
        /// <param name="linha"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string EscreverValor(BookInfo book, string campo, string linha, object valorObj, bool interpretarValor)
        {
            // Pega informacoes do campo solicitado
            BookItemCampoHelper helper = book.ReceberDetalheCampo(campo);

            // Repassa a chamada
            return InterpretadorArquivos.EscreverValor(book, helper, linha, valorObj, interpretarValor);
        }

        /// <summary>
        /// Escreve o valor informado no campo desejado.
        /// Retorna a linha com o valor preenchido
        /// </summary>
        /// <param name="book"></param>
        /// <param name="campo"></param>
        /// <param name="linha"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string EscreverValor(BookInfo book, string campo, string linha, object valorObj)
        {
            // Repassa a chamada
            return InterpretadorArquivos.EscreverValor(book, campo, linha, valorObj, true);
        }

        public static object ExtrairValor(BookInfo book, string campo, string linha)
        {
            return ExtrairValor(book, campo, linha, false);
        }

        public static object ExtrairValor(BookInfo book, string campo, string linha, bool interpretar)
        {
            // Pega informacoes do campo solicitado
            BookItemCampoHelper helper = book.ReceberDetalheCampo(campo);

            if ((helper.BookItemCampoInfo.ReceberTamanho() + helper.PosicaoInicial) <= linha.Length)
            {

                // Extrai o valor
                string valor = linha.Substring(helper.PosicaoInicial, helper.BookItemCampoInfo.ReceberTamanho());

                // Verifica se deve retornar o valor interpretado ou a string
                if (interpretar)
                {
                    if (book.TipoInterpretacaoValor == BookInterpretacaoValorTipoEnum.TiposMainframe)
                        return interpretarValorMainframe(helper.BookItemCampoInfo, valor);
                    else
                        return interpretarValorConversor(helper.BookItemCampoInfo, valor);
                }
                else
                {
                    return valor;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Interpreta valor utilizando os conversores
        /// </summary>
        /// <param name="itemCampo"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        private static object interpretarValorConversor(BookItemCampoInfo itemCampo, string valor)
        {
            // Prepara o retorno
            object retorno = null;
            
            // Faz a conversao
            if (itemCampo.Conversor != null)
                retorno = itemCampo.Conversor.ConverterParaObjeto(itemCampo, valor);
            else
                retorno = valor;
            
            // Retorna
            return retorno;
        }

        /// <summary>
        /// Faz a interpretacao do valor usando o tipo de campo no formato do mainframe.
        /// Ex: X(9), 9(3)V99, etc
        /// </summary>
        /// <param name="itemCampo"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        private static object interpretarValorMainframe(BookItemCampoInfo itemCampo, string valor)
        {
            // Prepara o retorno
            object retorno = null;

            // Separa o tipo do tamanho
            string[] tipo2 = itemCampo.TipoInformacao2.Split('(');

            // Verifica se o campo é numerico ou string
            if (tipo2[0].Contains("X"))
            {
                // É string, remove espacos do final
                retorno = valor.TrimEnd();
            }
            else
            {
                // É numérico, verifica se tem sinal
                if (tipo2[0].Contains("S"))
                {
                    // Tem sinal, traduz o último digito
                    char ultimoDigito = valor[valor.Length - 1];

                    // Verifica se é um numero positivo ou negativo
                    if (numerosNegativos.Contains(ultimoDigito))
                    {
                        // Acha o indice 
                        int pos = numerosNegativos.IndexOf(ultimoDigito);
                        
                        // Ajusta a string
                        valor = "-" + valor.Substring(0, valor.Length - 2) + pos.ToString();
                    }
                    else
                    {
                        // Acha o indice 
                        int pos = numerosPositivos.IndexOf(ultimoDigito);

                        // Ajusta a string
                        valor = valor.Substring(0, valor.Length - 2) + pos.ToString();
                    }

                    // Interpreta o numero
                    retorno = valor;

                }
                else
                {
                    if (tipo2.Length > 1)
                    {
                        if (tipo2[1].Contains("V99"))
                        {
                            if (!valor.Trim().Equals(string.Empty))
                            {
                                if (valor.IndexOf(',') < 0)
                                {
                                    int x = Convert.ToInt32(valor.Trim());
                                    valor = x.ToString().PadLeft(9, '0');
                                    valor = valor.Insert((valor.Length - 2), ",");
                                    valor = Convert.ToDecimal(valor).ToString("n");
                                }
                            }
                        }
                    }
                    else
                    {
                        if (tipo2[0].Contains("V99"))
                        {
                            int posVirgula = valor.Length - (tipo2[0].Length - 1);
                            valor = valor.Insert(posVirgula, ",");
                        }
                    }
                    // Não tem sinal, retorna o número inteiro interpretado
                    retorno = valor;
                }
            }

            // Retorna
            return retorno;
        }

        /// <summary>
        /// Remove o valor informado do campo desejado na linha da transação.
        /// Retorna a linha com o valor removido
        /// </summary>
        /// <param name="campo"></param>
        /// <param name="linha"></param>
        /// <returns></returns>
        public static string RemoverValor(BookItemCampoHelper campo, string linha)
        {
            // Pega o tamanho do campo
            int tamanho = campo.BookItemCampoInfo.ReceberTamanho();

            // Remove o valor
            if (campo.PosicaoInicial == 0)
                linha =
                    linha.Substring(tamanho);
            else if (campo.PosicaoInicial + tamanho < linha.Length)
                linha =
                    linha.Substring(0, campo.PosicaoInicial) +
                    linha.Substring(campo.PosicaoInicial + tamanho);
            else
                linha =
                    linha.Substring(0, campo.PosicaoInicial);

            // Retorna
            return linha;
        }
        
        /// <summary>
        /// Remove o valor informado do campo desejado na linha da transação.
        /// Retorna a linha com o valor removido
        /// </summary>
        /// <param name="book"></param>
        /// <param name="campo"></param>
        /// <param name="linha"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string RemoverValor(BookInfo book, string campo, string linha)
        {
            // Pega informacoes do campo solicitado
            BookItemCampoHelper helper = book.ReceberDetalheCampo(campo);

            // Repassa a chamada
            return InterpretadorArquivos.RemoverValor(helper, linha);
        }

        /// <summary>
        /// Insere o valor informado do campo desejado na linha da transação.
        /// Retorna a linha com o valor inserido
        /// </summary>
        /// <param name="campo"></param>
        /// <param name="linha"></param>
        /// <returns></returns>
        public static string InserirValor(BookInfo book, BookItemCampoHelper campo, string linha, object valorObj, bool interpretarValor)
        {
            // Pega o tamanho do campo
            int tamanho = campo.BookItemCampoInfo.ReceberTamanho();

            // Transforma o valor em string
            string valor = null;
            if (book.TipoInterpretacaoValor == BookInterpretacaoValorTipoEnum.TiposMainframe)
            {
                // Transforma diretamente em string
                valor = valorObj.ToString();
            }
            else
            {
                // Faz a interpretacao do valor
                if (campo.BookItemCampoInfo.Conversor != null && interpretarValor)
                    valor =
                        campo.BookItemCampoInfo.Conversor.ConverterParaString(
                            campo.BookItemCampoInfo, valorObj);
                else
                    valor = valorObj.ToString();
            }

            // Formata no tamanho correto
            if (valor.Length > tamanho)
                valor = valor.Substring(0, tamanho);
            else if (valor.Length < tamanho)
                valor = valor.PadRight(tamanho);

            // Remove o valor
            if (campo.PosicaoInicial == 0)
                linha = valor + linha;
            else if (campo.PosicaoInicial + tamanho < linha.Length + tamanho)
                linha =
                    linha.Substring(0, campo.PosicaoInicial) + valor +
                    linha.Substring(campo.PosicaoInicial);
            else
                linha = linha + valor;

            // Retorna
            return linha;
        }

        /// <summary>
        /// Insere o valor informado do campo desejado na linha da transação.
        /// Retorna a linha com o valor inserido
        /// </summary>
        /// <param name="book"></param>
        /// <param name="campo"></param>
        /// <param name="linha"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string InserirValor(BookInfo book, string campo, string linha, object valorObj, bool interpretarValor)
        {
            // Pega informacoes do campo solicitado
            BookItemCampoHelper helper = book.ReceberDetalheCampo(campo);

            // Repassa a chamada
            return InterpretadorArquivos.InserirValor(book, helper, linha, valorObj, interpretarValor);
        }
    }
}
