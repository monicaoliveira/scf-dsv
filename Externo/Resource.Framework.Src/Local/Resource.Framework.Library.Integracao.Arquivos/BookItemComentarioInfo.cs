﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Representa um item de comentario.
    /// </summary>
    [Serializable]
    public class BookItemComentarioInfo : BookItemInfoBase
    {
        /// <summary>
        /// Texto do comentário
        /// </summary>
        public string Texto { get; set; }
    }
}
