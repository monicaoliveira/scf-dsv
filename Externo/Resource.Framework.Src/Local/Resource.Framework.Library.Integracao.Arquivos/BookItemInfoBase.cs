﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Representa um item de book.
    /// Este item pode ser um comentário ou um campo.
    /// Classe base para especialização de acordo com o item
    /// </summary>
    [Serializable]
    public class BookItemInfoBase
    {
        /// <summary>
        /// Primeiras 6 posições da linha.
        /// Estas informações são ignoradas no processamento do book.
        /// </summary>
        public List<string> LinhaCabecalho { get; set; }

        /// <summary>
        /// Número das linhas ocupadas por este item.
        /// </summary>
        public List<int> LinhaNumeros { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public BookItemInfoBase()
        {
            this.LinhaCabecalho = new List<string>();
            this.LinhaNumeros = new List<int>();
        }
    }
}
