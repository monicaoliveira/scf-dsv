﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Entidade para representar um book
    /// </summary>
    [Serializable]
    public class BookInfo
    {
        /// <summary>
        /// Itens
        /// </summary>
        public List<BookItemInfoBase> Itens { get; set; }

        /// <summary>
        /// Campos
        /// </summary>
        public List<BookItemCampoInfo> Campos { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public BookInfo()
        {
            this.Itens = new List<BookItemInfoBase>();
            this.Campos = new List<BookItemCampoInfo>();
            this.TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.TiposMainframe;
        }

        /// <summary>
        /// Indica como será feito a interpretação de valores para este book
        /// </summary>
        public BookInterpretacaoValorTipoEnum TipoInterpretacaoValor { get; set; }

        /// <summary>
        /// Faz a navegação no book retornando detalhes do campo
        /// </summary>
        /// <param name="caminhoCampo"></param>
        /// <returns></returns>
        public BookItemCampoHelper ReceberDetalheCampo(string campo)
        {
            // Faz o parse do campo
            string[] campo2 = campo.Split('/');

            // Item do book
            BookItemCampoInfo itemCampo = null;

            // Posicao na linha
            int posicao = 0;

            // Faz a navegacao no book
            foreach (string campo3 in campo2)
            {
                // Indice de grupo
                int indiceGrupo = 0;

                // Variavel para usar no foreach
                string campo4 = campo3;

                // Verifica se é um grupo
                int posGrupo = campo4.IndexOf('[');
                if (posGrupo > -1)
                {
                    indiceGrupo = int.Parse(campo4.Substring(posGrupo + 1).Replace("]", ""));
                    campo4 = campo4.Remove(posGrupo);
                }

                // Navega
                if (itemCampo == null)
                    itemCampo = this.Campos.Find(c => c.Nome == campo4);
                else
                    itemCampo = itemCampo.Campos.Find(c => c.Nome == campo4);

                // Adiciona posicao inicial do campo
                posicao += itemCampo.PosicaoInicial;

                // Calcula a posicao se for grupo
                if (posGrupo > -1)
                    posicao += itemCampo.ReceberTamanho() * indiceGrupo;
            }

            // Retorna
            return 
                new BookItemCampoHelper() 
                { 
                    BookItemCampoInfo = itemCampo,
                    PosicaoInicial = posicao
                };
        }

        /// <summary>
        /// Retorna a lista de campos do book.
        /// Faz a transformação de árvore de campos em lista
        /// </summary>
        /// <returns></returns>
        public List<BookItemCampoInfo> ReceberListaCampos()
        {
            // Prepara a resposta
            List<BookItemCampoInfo> resposta = new List<BookItemCampoInfo>();

            // Varre os filhos pedindo a lista de campos
            foreach (BookItemCampoInfo campoFilho in this.Campos)
                resposta.AddRange(receberListaCamposHelper(campoFilho));

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Ajusta os campos pai
        /// </summary>
        public void AjustaCamposPai()
        {
            ajustaCampoPai(null, this.Campos);
        }

        /// <summary>
        /// Ajusta campos pai
        /// </summary>
        /// <param name="campoPai"></param>
        /// <param name="campos"></param>
        private void ajustaCampoPai(BookItemCampoInfo campoPai, List<BookItemCampoInfo> campos)
        {
            foreach (BookItemCampoInfo bookItem in campos)
            {
                bookItem.CampoPai = campoPai;
                ajustaCampoPai(bookItem, bookItem.Campos);
            }
        }
        
        /// <summary>
        /// Calcula a posicao dos campos
        /// </summary>
        /// <param name="campos"></param>
        public void CalculaPosicaoTamanhoCampos()
        {
            this.AjustaCamposPai();
            calculaPosicaoTamanhoCampos(0, this.Campos);
        }

        /// <summary>
        /// Calcula a posicao dos campos
        /// </summary>
        /// <param name="campos"></param>
        private void calculaPosicaoTamanhoCampos(int posicaoInicial, List<BookItemCampoInfo> campos)
        {
            // Varre os campos
            foreach (BookItemCampoInfo campo in campos)
            {
                // Informa posicao inicial atual
                if (campo.CampoPai != null)
                    campo.PosicaoInicial = posicaoInicial;
                else
                    campo.PosicaoInicial = 0;

                // Incrementa posicao inicial
                // Se for redefinicao de campo, mantem aonde está
                if (campo.CampoRedefinido != null)
                    campo.PosicaoInicial -= campo.ReceberTamanho();
                else
                    posicaoInicial += campo.ReceberTamanho();

                // Pede calculo dos campos filhos
                calculaPosicaoTamanhoCampos(0, campo.Campos);

                // Se nao tem tamanho, calcula
                if (campo.TipoCampo == BookItemCampoTipoEnum.Grupo)
                    campo.Tamanho = campo.ReceberTamanho();
            }

        }

        /// <summary>
        /// Método auxiliar para a construção de lista de campos
        /// </summary>
        /// <param name="campo"></param>
        /// <returns></returns>
        private List<BookItemCampoInfo> receberListaCamposHelper(BookItemCampoInfo campo)
        {
            // Prepara a resposta
            List<BookItemCampoInfo> resposta = new List<BookItemCampoInfo>();

            // Adiciona a si proprio
            resposta.Add(campo);
            
            // Varre os filhos pedindo a lista de campos
            foreach (BookItemCampoInfo campoFilho in campo.Campos)
                resposta.AddRange(receberListaCamposHelper(campoFilho));

            // Retorna
            return resposta;
        }
    }
}
