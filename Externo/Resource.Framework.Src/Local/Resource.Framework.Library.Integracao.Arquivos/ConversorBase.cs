﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Classe base para criação dos conversores
    /// </summary>
    [Serializable]
    public abstract class ConversorBase
    {
        /// <summary>
        /// Converte uma string para objeto
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public object ConverterParaObjeto(BookItemCampoInfo campo, string valor)
        {
            return OnConverterParaObjeto(campo, valor);
        }

        /// <summary>
        /// Converte uma string para objeto
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public virtual object OnConverterParaObjeto(BookItemCampoInfo campo, string valor)
        {
            return null;
        }

        /// <summary>
        /// Converte um objeto para string
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public string ConverterParaString(BookItemCampoInfo campo, object valor)
        {
            return OnConverterParaString(campo, valor);
        }

        /// <summary>
        /// Converte um objeto para string
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public virtual string OnConverterParaString(BookItemCampoInfo campo, object valor)
        {
            return null;
        }
    }
}
