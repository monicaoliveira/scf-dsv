using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Indica possíveis tipos de informação
    /// </summary>
    [Serializable]
    public enum BookItemCampoTipoInformacaoEnum
    {
        Data = 2,
        Numerico = 1,
        Outros = 3,
        Texto = 0
    }
}
