﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Conversor de hora
    /// </summary>
    public class ConversorHora : ConversorBase
    {
        /// <summary>
        /// Construtor default
        /// </summary>
        public ConversorHora()
        {
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="formato"></param>
        public ConversorHora(string formato)
        {
            this.Formato = formato;
        }

        /// <summary>
        /// Formato da data
        /// </summary>
        private string _formato = null;
        public string Formato
        {
            get { return _formato; }
            set
            {
                _formato = value;
                interpretarFormato();
            }
        }

        /// <summary>
        /// Formato para geracao para string caso não seja gerado com o _cultureInfo
        /// Nos casos de datas sem separador
        /// </summary>
        private string _formatoParaString = null;
        public string FormatoParaString
        {
            get
            {
                return this._formatoParaString;
            }
            set
            {
                this._formatoParaString = value;
            }
        }

        /// <summary>
        /// Caso a conversao deva ser feita por parse, este cultureInfo
        /// tem o formato da data
        /// </summary>
        private CultureInfo _cultureInfo = null;
        public CultureInfo CultureInfo
        {
            get
            {
                return this._cultureInfo;
            }
            set
            {
                this._cultureInfo = value;
            }
        }

        // Propriedades Auxiliares
        public int PosicaoHora { get; set; }
        public int TamanhoHora { get; set; }
        public int PosicaoMinuto { get; set; }
        public int TamanhoMinuto { get; set; }
        public int PosicaoSegundo { get; set; }
        public int TamanhoSegundo { get; set; }

        /// <summary>
        /// Converte de string para objeto
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public override object OnConverterParaObjeto(BookItemCampoInfo campo, string valor)
        {
            // Pega os valores
            int hora = int.Parse(valor.Substring(this.PosicaoHora, this.TamanhoHora));
            int minuto = int.Parse(valor.Substring(this.PosicaoMinuto, this.TamanhoMinuto));
            int segundo = int.Parse(valor.Substring(this.PosicaoSegundo, this.TamanhoSegundo));

            // Retorna
            return new TimeSpan(hora, minuto, segundo);
        }

        /// <summary>
        /// Converte de objeto para string
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public override string OnConverterParaString(BookItemCampoInfo campo, object valor)
        {
            // Prepara o retorno
            string retorno = null;

            // Apenas se tem valor
            if (valor != null)
            {
                retorno = ((TimeSpan)valor).ToString();
            }
            else
            {
                // Retorna string vazia
                retorno = "";
            }

            // Retorna
            return retorno;
        }

        /// <summary>
        /// Faz a interpretacao do formato
        /// </summary>
        private void interpretarFormato()
        {
            // Converte para maisculas
            _formato = _formato.ToUpper();

            // Acha as posições
            this.PosicaoHora = this.Formato.IndexOf('H');
            this.PosicaoMinuto = this.Formato.IndexOf('M');
            this.PosicaoSegundo = this.Formato.IndexOf('S');

            // Acha os tamanhos
            this.TamanhoHora = quantidadeRepeticoes(this.Formato, 'H', this.PosicaoHora);
            this.TamanhoMinuto = quantidadeRepeticoes(this.Formato, 'M', this.PosicaoMinuto);
            this.TamanhoSegundo = quantidadeRepeticoes(this.Formato, 'S', this.PosicaoSegundo);
        }

        /// <summary>
        /// Acha quantidade de repeticoes da string informada
        /// </summary>
        /// <returns></returns>
        private int quantidadeRepeticoes(string texto, char caracter, int posicaoInicial)
        {
            int i = 0;
            while (true)
                if (posicaoInicial + i < texto.Length && texto[posicaoInicial + i] == caracter)
                    i++;
                else
                    break;
            return i;
        }
    }
}
