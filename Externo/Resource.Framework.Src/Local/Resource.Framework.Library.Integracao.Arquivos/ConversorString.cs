﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Conversor string
    /// </summary>
    public class ConversorString : ConversorBase
    {
        /// <summary>
        /// Construtor default
        /// </summary>
        public ConversorString()
        {
            this.EfetuarTrim = true;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConversorString(bool efetuarTrim) : this()
        {
            this.EfetuarTrim = true;
        }

        /// <summary>
        /// Indica se deve efetuar trim na string
        /// </summary>
        public bool EfetuarTrim { get; set; }

        /// <summary>
        /// Converte de string para objeto
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public override object OnConverterParaObjeto(BookItemCampoInfo campo, string valor)
        {
            // Retorna
            if (this.EfetuarTrim)
                return valor.Trim();
            else
                return valor;
        }

        /// <summary>
        /// Converte de objeto para string
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public override string OnConverterParaString(BookItemCampoInfo campo, object valor)
        {
            // Se valor for nulo, considera string vazia
            if (valor == null)
                valor = "";
            
            // Retorna o número no formato solicitado
            return valor.ToString().PadRight(campo.Tamanho);
        }

    }
}
