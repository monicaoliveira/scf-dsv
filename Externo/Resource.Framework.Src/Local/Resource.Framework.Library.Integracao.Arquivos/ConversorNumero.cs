﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Resource.Framework.Library.Integracao.Arquivos
{
    /// <summary>
    /// Conversor numérico
    /// </summary>
    public class ConversorNumero : ConversorBase
    {
        /// <summary>
        /// Cultura a ser utilizada
        /// </summary>
        private CultureInfo _culture = new CultureInfo("pt-br");
        
        /// <summary>
        /// Construtor default
        /// </summary>
        public ConversorNumero()
        {
            this.QuantidadeDecimais = 0;
            this.IncluirZerosEsquerda = true;
        }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ConversorNumero(Type tipoDestino) : this()
        {
            this.TipoDestino = tipoDestino;
            this.QuantidadeDecimais = 0;
            this.IncluirZerosEsquerda = true;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConversorNumero(int quantidadeDecimais) : this()
        {
            this.QuantidadeDecimais = quantidadeDecimais;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConversorNumero(Type tipoDestino, int quantidadeDecimais) : this()
        {
            this.QuantidadeDecimais = quantidadeDecimais;
            this.TipoDestino = tipoDestino;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConversorNumero(int quantidadeDecimais, bool incluirZerosEsquerda) : this()
        {
            this.QuantidadeDecimais = quantidadeDecimais;
            this.IncluirZerosEsquerda = incluirZerosEsquerda;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConversorNumero(Type tipoDestino, int quantidadeDecimais, bool incluirZerosEsquerda) : this()
        {
            this.QuantidadeDecimais = quantidadeDecimais;
            this.IncluirZerosEsquerda = incluirZerosEsquerda;
            this.TipoDestino = tipoDestino;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConversorNumero(Type tipoDestino, int quantidadeDecimais, bool incluirZerosEsquerda, bool contemSeparadorDecimal)
            : this()
        {
            this.QuantidadeDecimais = quantidadeDecimais;
            this.IncluirZerosEsquerda = incluirZerosEsquerda;
            this.TipoDestino = tipoDestino;
            this.ContemSeparadorDecimal = contemSeparadorDecimal;
        }

        /// <summary>
        /// Indica o tipo destino do valor
        /// Default para double
        /// </summary>
        public Type TipoDestino { get; set; }
        
        /// <summary>
        /// Indica quantas decimais tem o número
        /// </summary>
        private int _quantidadeDecimais = 0;
        public int QuantidadeDecimais 
        {
            get { return _quantidadeDecimais; }
            set 
            { 
                _quantidadeDecimais = value;
                atualizarFormato();
            }
        }

        /// <summary>
        /// Indica se deve incluir zeros à esquerda na representação em string
        /// </summary>
        public bool IncluirZerosEsquerda { get; set; }

        /// <summary>
        /// Indica se a representação em string contem o separador decimal
        /// </summary>
        public bool ContemSeparadorDecimal { get; set; }

        /// <summary>
        /// Formato para conversao para string
        /// </summary>
        private string _formato = null;

        /// <summary>
        /// Faz a atualizacao do formato
        /// </summary>
        private void atualizarFormato()
        {
            // Monta string de conversao
            _formato = new string('0', this.QuantidadeDecimais + 1);
            if (this.QuantidadeDecimais > 0)
                _formato = _formato.Insert(_formato.Length - this.QuantidadeDecimais, ".");
        }
        
        /// <summary>
        /// Converte de string para objeto
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public override object OnConverterParaObjeto(BookItemCampoInfo campo, string valor)
        {
            // Inclui o separador
            if (!this.ContemSeparadorDecimal && this.QuantidadeDecimais > 0)
                valor = valor.Insert(campo.Tamanho - this.QuantidadeDecimais, ",");

            // Faz o parse
            double valorDouble = valor != "" ? double.Parse(valor, _culture) : 0;

            // Retorna
            if (this.TipoDestino != null && this.TipoDestino != typeof(double))
                return Convert.ChangeType(valorDouble, this.TipoDestino);
            else
                return valorDouble;
        }

        /// <summary>
        /// Converte de objeto para string
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public override string OnConverterParaString(BookItemCampoInfo campo, object valor)
        {
            // Faz a conversao
            double valor2 = valor is double ? (double)valor : Convert.ToDouble(valor);
            string valorStr = valor2.ToString(_formato); 
            
            // Inclui separador decimal?
            if (!this.ContemSeparadorDecimal)
                valorStr = valorStr.Replace(".", "").Replace(",", "");

            // Inclui zeros à esquerda se necessário
            if (this.IncluirZerosEsquerda)
                valorStr = valorStr.PadLeft(campo.Tamanho, '0');

            // Retorna o número no formato solicitado
            return valorStr;
        }

    }
}
