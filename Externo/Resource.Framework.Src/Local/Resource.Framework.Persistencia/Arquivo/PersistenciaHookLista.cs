﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Sistemas.Comum;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;

namespace Resource.Framework.Persistencia.Arquivo
{
    /// <summary>
    /// Trata excessoes para a persistencia de ContratoResumoInfo
    /// </summary>
    public class PersistenciaHookLista : IPersistenciaArquivoHook
    {
        /// <summary>
        /// Referencia para a persistencia em arquivo
        /// </summary>
        private PersistenciaArquivo _persistenciaArquivo = null;

        #region IPersistenciaArquivoHook Members

        /// <summary>
        /// Inicialização disparado pela persistencia arquivo
        /// </summary>
        /// <param name="instancia"></param>
        public void Inicializar(PersistenciaArquivo instancia)
        {
            // Guarda referencia
            _persistenciaArquivo = instancia;

            // Assina os eventos de interesse
            _persistenciaArquivo.EventoReceber +=
                new EventHandler<PersistenciaArquivoEventoEventArgs>(
                    _persistenciaArquivo_EventoReceber);
        }

        /// <summary>
        /// Trata mensagens especificas de detalhe
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _persistenciaArquivo_EventoReceber(object sender, PersistenciaArquivoEventoEventArgs e)
        {
            // Verifica se é alguma mensagem especifica
            if (e.MensagemRequest is ReceberListaDbRequest)
            {
                // Pega a mensagem com o tipo correto
                ReceberListaDbRequest request =
                    (ReceberListaDbRequest)e.MensagemRequest;

                // Faz o filtro pela chave ou pelo mnemonico
                ListaInfo listaInfo = null;
                if (request.CodigoLista != null)
                    listaInfo =
                        _persistenciaArquivo.ReceberObjeto<ListaInfo>(
                            new ReceberObjetoRequest<ListaInfo>()
                            {
                                CodigoObjeto = request.CodigoLista
                            }).Objeto;
                else
                    listaInfo =
                        _persistenciaArquivo.ConsultarObjetos<ListaInfo>(
                            new ConsultarObjetosRequest<ListaInfo>()
                            {
                                Condicoes = new List<CondicaoInfo>() 
                                { 
                                    new CondicaoInfo("Mnemonico", CondicaoTipoEnum.Igual, request.MnemonicoLista)
                                }
                            }).Resultado.FirstOrDefault();

                //Se necessário filtra os ites da lista de itens.
                if (!request.RetornarItensExcluidos)
                {
                    var s = from item in listaInfo.Itens
                            where item.Ativo == true
                            select item;
                    
                    //listaInfo.Itens.Clear();
                    listaInfo.Itens = s.ToList();
                }

                // Retorna
                e.MensagemResponse =
                    new ReceberObjetoResponse<ListaInfo>()
                    {
                        Objeto = listaInfo
                    };
            }
        }

        #endregion
    }
}
