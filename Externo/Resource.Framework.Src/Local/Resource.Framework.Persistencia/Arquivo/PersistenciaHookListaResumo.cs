﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Sistemas.Comum;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Persistencia.Arquivo
{
    /// <summary>
    /// Trata excessoes para a persistencia de ContratoResumoInfo
    /// </summary>
    public class PersistenciaHookListaResumo : IPersistenciaArquivoHook
    {
        /// <summary>
        /// Referencia para a persistencia em arquivo
        /// </summary>
        private PersistenciaArquivo _persistenciaArquivo = null;

        #region IPersistenciaArquivoHook Members

        /// <summary>
        /// Inicialização disparado pela persistencia arquivo
        /// </summary>
        /// <param name="instancia"></param>
        public void Inicializar(PersistenciaArquivo instancia)
        {
            // Guarda referencia
            _persistenciaArquivo = instancia;

            // Assina os eventos de interesse
            _persistenciaArquivo.EventoConsultar +=
                new EventHandler<PersistenciaArquivoEventoEventArgs>(
                    _persistenciaArquivo_EventoConsultar);
        }

        /// <summary>
        /// Trata mensagens especificas de lista
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _persistenciaArquivo_EventoConsultar(object sender, PersistenciaArquivoEventoEventArgs e)
        {
            // Verifica se é alguma mensagem especifica
            if (e.MensagemRequest is ConsultarObjetosRequest<ListaResumoInfo>)
            {
                // Pega a mensagem com o tipo correto
                ConsultarObjetosRequest<ListaResumoInfo> request =
                    (ConsultarObjetosRequest<ListaResumoInfo>)e.MensagemRequest;

                // Busca lista de Contratos
                List<ListaInfo> listas =
                    _persistenciaArquivo.ConsultarObjetos<ListaInfo>(
                        new ConsultarObjetosRequest<ListaInfo>()
                        {
                            Condicoes = request.Condicoes
                        }).Resultado;

                // Transforma em lista de Contrato resumo
                List<ListaResumoInfo> listasResumo = new List<ListaResumoInfo>();
                foreach (ListaInfo listaInfo in listas)
                {
                    // Adiciona novo resumo
                    listasResumo.Add(
                        new ListaResumoInfo()
                        {
                            CodigoLista = listaInfo.CodigoLista,
                            Descricao = listaInfo.Descricao,
                            Mnemonico = listaInfo.Mnemonico
                        });
                }

                // Retorna
                e.MensagemResponse =
                    new ConsultarObjetosResponse<ListaResumoInfo>()
                    {
                        Resultado = listasResumo
                    };
            }
        }

        #endregion
    }
}
