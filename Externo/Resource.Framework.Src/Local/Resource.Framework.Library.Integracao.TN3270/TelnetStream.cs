﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    /// <summary>
    /// Contém uma série de dados (chunk) recebido pelo socket, ou a ser enviado.
    /// Fornece funções para transformação da série de dados em tokens (interpretação 
    /// da série de dados) e vice-versa.
    /// </summary>
    public class TelnetStream
    {
        /// <summary>
        /// Buffer puro recebido pelo socket
        /// </summary>
        public List<byte> Buffer { get; set; }

        /// <summary>
        /// Próxima posição a ser lida.
        /// Caso seja igual ao tamanho do buffer, chegou ao final
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Indica se a leitura dos dados chegou ao final
        /// </summary>
        public bool EOF { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public TelnetStream()
        {
            this.Buffer = new List<byte>();
        }

        /// <summary>
        /// Recebe o buffer de bytes a ser trabalhado
        /// </summary>
        /// <param name="buffer"></param>
        public TelnetStream(byte[] buffer, int size)
        {
            this.Initialize(buffer, size);
        }

        /// <summary>
        /// Faz a inicialização do buffer
        /// </summary>
        /// <param name="buffer"></param>
        public void Initialize(byte[] buffer, int size)
        {
            this.Buffer = new List<byte>(buffer.Take(size));
            this.Position = 0;
            this.EOF = false;
        }

        /// <summary>
        /// Lê o buffer, byte a byte
        /// </summary>
        /// <returns></returns>
        public byte? ReadByte()
        {
            // Resposta
            byte? resposta = null;
            
            // Chegou ao fim?
            if (!this.EOF)
            {
                // Pega o byte
                resposta = this.Buffer[this.Position++];

                // Verifica se chegou ao fim
                if (this.Position >= this.Buffer.Count)
                    this.EOF = true;
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Escreve um byte no buffer e reposiciona
        /// </summary>
        /// <param name="b"></param>
        public void WriteByte(byte b)
        {
            // Adiciona na coleção
            this.Buffer.Add(b);
        }
        
        /// <summary>
        /// Faz a interpretação dos próximos dados retornando um 
        /// token telnet.
        /// </summary>
        /// <returns></returns>
        public TelnetTokenBase GetNextToken()
        {
            // Inicializa
            TelnetTokenBase token = null;

            // Identifica se o que vem é uma estrutura de comando, subcomando, ou dados
            if (this.Buffer[this.Position] == (byte)TelnetCommandsEnum.IAC)
            {
                // Tenta ler o próximo byte
                byte? proximoByte = this.Position + 1 < this.Buffer.Count ? new byte?(this.Buffer[this.Position + 1]) : null;

                // Se não conseguiu ler o próximo byte, veio um IAC sozinho. Isso não é comum.
                if (!proximoByte.HasValue)
                    throw (new Exception("Foi recebido um comando IAC no final do buffer. Não há como interpretar este comando sem argumentos."));
                
                // Verifica se é um comando ou subcomando
                if (proximoByte.Value == (byte)TelnetCommandsEnum.SB)
                    // É um subcomando
                    token = new TelnetTokenSubCommand();
                else
                    // É um comando
                    token = new TelnetTokenCommand();
            }
            else
            {
                // É uma estrutura de dados
                token = new TelnetTokenData();
            }

            // Faz a leitura
            token.ReadStream(this);

            // Retorna
            return token;
        }
    }
}
