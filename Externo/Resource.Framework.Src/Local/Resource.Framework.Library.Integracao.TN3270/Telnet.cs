﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

using Resource.Framework.Library.Integracao.TN3270.Options;

namespace Resource.Framework.Library.Integracao.TN3270
{
    public class Telnet
    {
        private const int _bufferSize = 32767;
        private Socket _socket = null;
        private NetworkStream _socketStream = null;
        private Byte[] _buffer = new byte[_bufferSize];

        private TelnetTokenData _unfinishedTokenData = null;

        public Dictionary<TelnetOptionsEnum, TelnetOptionBase> _optionProcessors = new Dictionary<TelnetOptionsEnum, TelnetOptionBase>();

        /// <summary>
        /// Indica quem vai processar os dados que chegarem
        /// </summary>
        public ITelnetDataProcessor DataProcessor { get; set; }

        public Telnet()
        {
            // Inicializa
            this.AddOptionType(typeof(TelnetOptionBinary));
            this.AddOptionType(typeof(TelnetOptionEcho));
            this.AddOptionType(typeof(TelnetOptionEndOfRecord));
            this.AddOptionType(typeof(TelnetOptionSuppressGoAhead));
            this.AddOptionType(typeof(TelnetOptionTerminalType));
        }

        public void Connect()
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            //EndPoint ep = new IPEndPoint(new IPAddress(new byte[] { 127, 0, 0, 1 }), 23);
            //EndPoint ep = new IPEndPoint(new IPAddress(new byte[] { 192, 168, 0, 11 }), 23);
            EndPoint ep = new IPEndPoint(new IPAddress(new byte[] { 10, 19, 240, 102 }), 23);
            _socket.BeginConnect(ep, new AsyncCallback(connectCallback), null);
        }

        public void Connect(EndPoint ep)
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            _socket.BeginConnect(ep, new AsyncCallback(connectCallback), null);
        }

        public void Disconnect()
        {
            if (_socket != null && _socket.Connected)
                _socket.Close();
        }

        /// <summary>
        /// Adiciona a classe de processamento de opções informada
        /// </summary>
        /// <param name="optionType"></param>
        public void AddOptionType(Type optionType)
        {
            // Cria o tipo informado
            TelnetOptionBase option = (TelnetOptionBase)Activator.CreateInstance(optionType);
            option.Initialize(this);

            // Adiciona no dicionario
            _optionProcessors.Add(option.GetOption(), option);
        }

        private void connectCallback(IAsyncResult ar)
        {
            if (_socket.Connected)
            {
                _socketStream = new NetworkStream(_socket, false);
                _socketStream.BeginRead(_buffer, 0, _buffer.Length, new AsyncCallback(receiveCallback), null);
            }
        }

        private void receiveCallback(IAsyncResult ar)
        {
            // Se o socket nao estiver conectado, sai
            if (!_socket.Connected)
                return;
            try
            {
                // Completa a leitura das informações
                int size = _socketStream.EndRead(ar);

                // Apenas se leu algo
                if (size > 0)
                {
                    // Processa as informações recebidas
                    TelnetStream buffer = new TelnetStream(_buffer, size);
                    while (!buffer.EOF)
                    {
                        // Se há um token de dados não finalizado, pede para completar
                        if (_unfinishedTokenData != null)
                        {
                            // Faz a leitura
                            _unfinishedTokenData.ReadStream(buffer);

                            // Se terminou, pede o processamento
                            if (_unfinishedTokenData.ReadFinalized && this.DataProcessor != null)
                            {
                                // Processa
                                this.DataProcessor.ProcessData(_unfinishedTokenData);

                                // Sinaliza
                                _unfinishedTokenData = null;
                            }
                        }
                        else
                        {
                            // Pega o próximo token
                            TelnetTokenBase token = buffer.GetNextToken();

                            // Mostra no console
                            //Console.WriteLine("RECV: " + token.ToString());

                            // Token de dados?
                            if (token is TelnetTokenData)
                            {
                                // Pega com o tipo correto
                                TelnetTokenData tokenData = (TelnetTokenData)token;

                                // Verifica se os dados foram finalizados
                                if (tokenData.ReadFinalized)
                                {
                                    // Solicita que o processador de dados faça o processamento do token
                                    if (this.DataProcessor != null)
                                        this.DataProcessor.ProcessData(tokenData);

                                    // Indica que não há token de dados aguardando
                                    _unfinishedTokenData = null;
                                }
                                else
                                {
                                    // Mantem este token para a próxima leitura
                                    _unfinishedTokenData = tokenData;
                                }
                            }
                            else
                            {
                                // Pega o tipo da opção
                                TelnetOptionsEnum? tipoOpcao = null;
                                if (token is TelnetTokenCommand)
                                    tipoOpcao = ((TelnetTokenCommand)token).Option;
                                else if (token is TelnetTokenSubCommand)
                                    tipoOpcao = ((TelnetTokenSubCommand)token).Option;

                                // Processa
                                if (_optionProcessors.ContainsKey(tipoOpcao.Value))
                                    _optionProcessors[tipoOpcao.Value].ProcessToken(token);
                            }
                        }
                    }
                }

                // Prepara a nova leitura
                _socketStream.BeginRead(_buffer, 0, _buffer.Length, new AsyncCallback(receiveCallback), null);
            }
            catch (Exception)
            {
                return;
            }

        }

        public void Send(TelnetTokenBase token)
        {
            // Mostra no console 
            //Console.WriteLine("SEND: " + token.ToString());

            // Monta o buffer
            TelnetStream buffer = new TelnetStream();
            token.WriteStream(buffer);

            // Transforma a lista de bytes em array de bytes
            byte[] bf = buffer.Buffer.ToArray();

            // Envia
            _socketStream.Write(bf, 0, bf.Length);
        }

        public void Send(List<TelnetTokenBase> tokens)
        {
            // Monta o buffer
            TelnetStream buffer = new TelnetStream();

            // Varre a lista de tokens
            foreach (TelnetTokenBase token in tokens)
            {
                // Pede para o token salvar no buffer
                token.WriteStream(buffer);

                // Mostra no console 
                //Console.WriteLine("SEND: " + token.ToString());
            }

            // Transforma a lista de bytes em array de bytes
            byte[] bf = buffer.Buffer.ToArray();

            // Envia
            _socketStream.Write(bf, 0, bf.Length);
        }
    }
}
