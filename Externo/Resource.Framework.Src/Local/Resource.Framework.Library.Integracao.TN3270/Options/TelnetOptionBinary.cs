﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options
{
    public class TelnetOptionBinary : TelnetOptionBase
    {
        public ITelnetDataProcessor DataProcessor { get; set; }

        public TelnetOptionBinary()
        {
            //// Cria o processador de dados
            //this.DataProcessor = new Tn3270.Tn3270Processor(this);
        }

        protected override TelnetOptionsEnum OnGetOption()
        {
            return TelnetOptionsEnum.BinaryTransmission;
        }

        protected override void OnProcessToken(TelnetTokenBase token)
        {
            TelnetTokenCommand token2 = token as TelnetTokenCommand;
            if (token2.Verb == TelnetCommandsEnum.WILL)
            {
                this.Telnet.Send(
                    new TelnetTokenCommand()
                    {
                        Verb = TelnetCommandsEnum.DO,
                        Option = TelnetOptionsEnum.BinaryTransmission
                    });
                this.Telnet.Send(
                    new TelnetTokenCommand()
                    {
                        Verb = TelnetCommandsEnum.WILL,
                        Option = TelnetOptionsEnum.BinaryTransmission
                    });
                
                //// Seta o processador de dados do telnet para o processador que foi criado aqui
                //this.Telnet.DataProcessor = this.DataProcessor;
            }
        }
    }
}
