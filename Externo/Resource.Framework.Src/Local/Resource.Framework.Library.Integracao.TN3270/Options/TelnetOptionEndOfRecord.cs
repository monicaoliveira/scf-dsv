﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options
{
    public class TelnetOptionEndOfRecord : TelnetOptionBase
    {
        protected override TelnetOptionsEnum OnGetOption()
        {
            return TelnetOptionsEnum.EndOfRecord;
        }

        protected override void OnProcessToken(TelnetTokenBase token)
        {
            TelnetTokenCommand token2 = token as TelnetTokenCommand;
            if (token2.Verb == TelnetCommandsEnum.WILL)
                this.Telnet.Send(
                    new TelnetTokenCommand()
                    {
                        Verb = TelnetCommandsEnum.DO,
                        Option = TelnetOptionsEnum.EndOfRecord
                    });
            else if (token2.Verb == TelnetCommandsEnum.DO)
                this.Telnet.Send(
                    new TelnetTokenCommand()
                    {
                        Verb = TelnetCommandsEnum.WILL,
                        Option = TelnetOptionsEnum.EndOfRecord
                    });
        }
    }
}
