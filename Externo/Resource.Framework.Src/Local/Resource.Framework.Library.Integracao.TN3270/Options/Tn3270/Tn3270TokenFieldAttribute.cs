﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Atributo para identificar um token de campo
    /// </summary>
    public class Tn3270TokenFieldAttribute : Attribute
    {
        /// <summary>
        /// Identificador do campo
        /// </summary>
        public byte[] FieldId { get; set; }
    }
}
