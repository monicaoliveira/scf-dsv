﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Representação do Token Attention Identifier (AID)
    /// </summary>
    public class Tn3270TokenAID : Tn3270TokenBase
    {
        /// <summary>
        /// Identifica o AID desejado
        /// </summary>
        public Tn3270AIDsEnum AID { get; set; }

        /// <summary>
        /// Endereço referência do buffer, caso não seja Strutured Fields (0x88)
        /// </summary>
        public int BufferAddress { get; set; }

        /// <summary>
        /// Escreve no stream
        /// </summary>
        /// <param name="stream"></param>
        protected override void OnWriteStream(Tn3270Stream stream)
        {
            // Escreve o AID
            stream.WriteByte((byte)this.AID);

            // Caso não seja Structured Fields (0x88), grava o endereço
            if (this.AID != Tn3270AIDsEnum.StructuredField)
            {
                // Codifica o endereço
                byte[] address = Tn3270Utils.EncodeAddress(this.BufferAddress);

                // Escreve o endereço
                stream.WriteByte(address[0]);
                stream.WriteByte(address[1]);
            }
        }
    }
}
