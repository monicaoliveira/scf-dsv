﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Tipos de MDT (indica se o campo foi modificado)
    /// </summary>
    public enum Tn3270FieldMDTEnum : byte
    {
        NotModified = 0,
        Modified = 1
    }
}
