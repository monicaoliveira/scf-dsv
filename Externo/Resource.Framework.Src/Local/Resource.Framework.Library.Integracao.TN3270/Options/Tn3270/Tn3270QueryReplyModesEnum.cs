﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Lista de Query Reply Modes
    /// The Query Reply (Reply Modes) is used to inform the host of the following:
    ///  * The 3270 Data Stream attribute orders that the device supports, in
    ///  both the inbound and the outbound directions.
    ///  * The modes that can be set by the Set Reply Modes structured field.
    /// </summary>
    public enum Tn3270QueryReplyModesEnum : byte
    {
        FieldMode = 0x00,
        ExtendedFieldMode = 0x01,
        CharacterMode = 0x02
    }
}
