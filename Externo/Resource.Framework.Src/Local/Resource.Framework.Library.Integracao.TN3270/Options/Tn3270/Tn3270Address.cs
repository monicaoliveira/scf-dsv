﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Representa um endereço no buffer.
    /// Pode ser manipulado pelo próprio endereço, ou pelo
    /// par de linhas e colunas
    /// </summary>
    public class Tn3270Address
    {
        /// <summary>
        /// Endereço sequencial
        /// </summary>
        public int Address { get; set; }

        /// <summary>
        /// Indica a coluna correspondente ao endereço atual no buffer
        /// </summary>
        public byte Column
        {
            get { return Tn3270Utils.DecodeCoordinates(this.Address, this.Columns)[1]; }
            set { this.Address = Tn3270Utils.EncodeCoordinates(this.Row, value, this.Columns); }
        }

        /// <summary>
        /// Indica a linha correspondente ao endereço atual no buffer
        /// </summary>
        public byte Row
        {
            get { return Tn3270Utils.DecodeCoordinates(this.Address, this.Columns)[0]; }
            set { this.Address = Tn3270Utils.EncodeCoordinates(value, this.Column, this.Columns); }
        }

        /// <summary>
        /// Quantidade de colunas para considerar a quebra de linhas
        /// </summary>
        public byte Columns { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270Address()
        {
            this.Columns = 80;
        }

        /// <summary>
        /// Construtor que recebe a quantidade de colunas a trabalhar
        /// </summary>
        /// <param name="columns"></param>
        public Tn3270Address(byte columns)
        {
            this.Columns = columns;
        }

        /// <summary>
        /// Construtor que recebe o endereço
        /// </summary>
        /// <param name="columns"></param>
        public Tn3270Address(int address)
        {
            this.Address = address;
            this.Columns = 80;
        }

        /// <summary>
        /// Construtor que recebe o endereço
        /// </summary>
        /// <param name="columns"></param>
        public Tn3270Address(int address, byte columns)
        {
            this.Address = address;
            this.Columns = columns;
        }
    }
}
