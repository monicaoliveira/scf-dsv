﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Tipos de entrada de dados para campos
    /// </summary>
    public enum Tn3270FieldEntryTypeEnum : byte
    {
        Alphanumeric = 0,
        Numeric = 1
    }
}
