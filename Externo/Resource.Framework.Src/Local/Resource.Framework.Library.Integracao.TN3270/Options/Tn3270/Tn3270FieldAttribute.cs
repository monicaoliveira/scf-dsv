﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Representa o atributo de um campo.
    /// 
    /// Bits 0-1           2       3      4, 5       6            7
    ///  _____________ _______ _______ _________ ____________ ________
    /// | Graphic     |       |       |         |            |        |
    /// | Converter   | U/P   | A/N   | D/SPD   | Reserved   |   MDT  |
    /// |_____________|_______|_______|_________|____________|________|
    /// 
    /// Obs: Cuidado que a numeração dos bits está invertida.
    /// 
    /// </summary>
    public class Tn3270FieldAttribute
    {
        #region Propriedades

        /// <summary>
        /// Valor do atributo
        /// </summary>
        public byte AttributeValue { get; set; }

        /// <summary>
        /// Proteção
        /// </summary>
        public Tn3270FieldProtectionEnum Protection 
        {
            get 
            { 
                return 
                    Tn3270Utils.TestBit(AttributeValue, 5) ? 
                    Tn3270FieldProtectionEnum.Protected : 
                    Tn3270FieldProtectionEnum.Unprotected; 
            }
            set 
            {
                if (value == Tn3270FieldProtectionEnum.Protected)
                    this.AttributeValue = Tn3270Utils.SetBit(AttributeValue, 5);
                else
                    this.AttributeValue = Tn3270Utils.ResetBit(AttributeValue, 5);
            }
        }

        /// <summary>
        /// Tipo de entrada de dados
        /// </summary>
        public Tn3270FieldEntryTypeEnum EntryType
        {
            get
            {
                return
                    Tn3270Utils.TestBit(AttributeValue, 4) ?
                    Tn3270FieldEntryTypeEnum.Numeric :
                    Tn3270FieldEntryTypeEnum.Alphanumeric;
            }
            set
            {
                if (value == Tn3270FieldEntryTypeEnum.Numeric)
                    this.AttributeValue = Tn3270Utils.SetBit(AttributeValue, 4);
                else
                    this.AttributeValue = Tn3270Utils.ResetBit(AttributeValue, 4);
            }
        }

        /// <summary>
        /// Tipos de aparencia
        /// </summary>
        public Tn3270FieldDisplayTypeEnum DisplayType 
        {
            get
            {
                bool bit2 = Tn3270Utils.TestBit(AttributeValue, 2);
                bool bit3 = Tn3270Utils.TestBit(AttributeValue, 3);
                if (!bit2 && !bit3)         // 00
                    return Tn3270FieldDisplayTypeEnum.Display_NotSelectorPenDetectable;
                else if (!bit2 && bit3)     // 01
                    return Tn3270FieldDisplayTypeEnum.Display_SelectorPenDetectable;
                else if (bit2 && !bit3)     // 10
                    return Tn3270FieldDisplayTypeEnum.IntesifiedDisplay_SelectorPenDetectable;
                else                        // 11
                    return Tn3270FieldDisplayTypeEnum.NonDisplay_NonDetectable;
            }
            set
            {
                switch (value)
                {
                    case Tn3270FieldDisplayTypeEnum.Display_NotSelectorPenDetectable:
                        this.AttributeValue = Tn3270Utils.ResetBit(AttributeValue, 2);
                        this.AttributeValue = Tn3270Utils.ResetBit(AttributeValue, 3);
                        break;
                    case Tn3270FieldDisplayTypeEnum.Display_SelectorPenDetectable:
                        this.AttributeValue = Tn3270Utils.SetBit(AttributeValue, 2);
                        this.AttributeValue = Tn3270Utils.ResetBit(AttributeValue, 3);
                        break;
                    case Tn3270FieldDisplayTypeEnum.IntesifiedDisplay_SelectorPenDetectable:
                        this.AttributeValue = Tn3270Utils.ResetBit(AttributeValue, 2);
                        this.AttributeValue = Tn3270Utils.SetBit(AttributeValue, 3);
                        break;
                    case Tn3270FieldDisplayTypeEnum.NonDisplay_NonDetectable:
                        this.AttributeValue = Tn3270Utils.SetBit(AttributeValue, 2);
                        this.AttributeValue = Tn3270Utils.SetBit(AttributeValue, 3);
                        break;
                }
            }
        }

        /// <summary>
        /// Indica se o campo foi modificado
        /// </summary>
        public Tn3270FieldMDTEnum MDT 
        {
            get
            {
                return
                    Tn3270Utils.TestBit(AttributeValue, 0) ?
                    Tn3270FieldMDTEnum.Modified :
                    Tn3270FieldMDTEnum.NotModified;
            }
            set
            {
                if (value == Tn3270FieldMDTEnum.Modified)
                    this.AttributeValue = Tn3270Utils.SetBit(AttributeValue, 0);
                else
                    this.AttributeValue = Tn3270Utils.ResetBit(AttributeValue, 0);
            }
        }

        #endregion

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270FieldAttribute()
        {
            this.MDT = Tn3270FieldMDTEnum.NotModified;
        }

        /// <summary>
        /// Construtor que recebe o valor do atributo
        /// </summary>
        /// <param name="attribute"></param>
        public Tn3270FieldAttribute(byte attribute)
        {
            this.AttributeValue = attribute;
        }

        /// <summary>
        /// Converte em string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Attr({0},{1},{2},{3},{4})", this.AttributeValue, this.EntryType, this.Protection, this.MDT, this.DisplayType);
        }
    }
}
