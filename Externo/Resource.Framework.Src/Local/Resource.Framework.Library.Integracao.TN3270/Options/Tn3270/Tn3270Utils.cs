﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Utilitários para o Tn3270
    /// </summary>
    public static class Tn3270Utils
    {
        /// <summary>
        /// Tabela para tradução de endereços de 12 bits (2 bytes de 6 bits).
        /// Indica como representar valores de 0 a 63
        /// </summary>
        public static byte[] AddressCodeTable = new byte[] 
        {
	        0x40, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, // 0x00
	        0xC8, 0xC9, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, // 0x08
	        0x50, 0xD1, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7, // 0x10
	        0xD8, 0xD9, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F, // 0x18
	        0x60, 0x61, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, // 0x20
	        0xE8, 0xE9, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, // 0x28
	        0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, // 0x30
	        0xF8, 0xF9, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F, // 0x38
		};

        #region Conversão de Endereços

        /// <summary>
        /// Decodifica o endereço, de acordo com a tabela:
        /// B’00’ 14-bit binary address follows
        /// B’01’ 12-bit coded address follows
        /// B’10’ Reserved
        /// B’11’ 12-bit coded address follows
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static int DecodeAddress(byte[] address)
        {
            // Inicializa
            int retorno = 0;

            // Verifica se é 14 bits
            if ((address[0] & 0xc0) == 0x00)
            {
                // Retira mascara (2 primeiros bits) do primeiro byte e coloca segundo byte completo
                retorno = (int)(((address[0] & 0x3f) << 8) | address[1]);
            }
            else
            {
                // Retira mascara (2 primeitos bits) dos dois bytes
                retorno = (int)(((address[0] & 0x3f) << 6) | (address[1] & 0x3f));
            }

            // Retorna
            return retorno;
        }

        /// <summary>
        /// Codifica o endereço, de acordo com a tabela:
        /// B’00’ 14-bit binary address follows
        /// B’01’ 12-bit coded address follows
        /// B’10’ Reserved
        /// B’11’ 12-bit coded address follows
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static byte[] EncodeAddress(int address)
        {
            // Inicializa
            byte[] retorno = new byte[2];

            // Verifica se o endereço precisa ocupar mais de 12 bits (> 0xfff)
            if (address > 0xfff)
            {
                // Deve ocupar 14 bits
                retorno[0] = (byte)((address >> 8) & 0x3f);     // retorna 8 bits à direita (1a parte) e coloca a mascara
                retorno[1] = (byte)(address & 0xff);            // retorna os 8 primeiros bits
            }
            else
            {
                // Pode ocupar 12 bits
                retorno[0] = (byte)(AddressCodeTable[(byte)(((address >> 6) & 0x3f))] | 0x40);    // Retorna 6 bits à direita (1a parte) coloca a máscara e adiciona 01 no começo
                retorno[1] = (byte)(AddressCodeTable[(byte)((address & 0x3f))] | 0x40);           // retorna os 6 primeiros bits e coloca 01 no começo
            }

            // Retorna
            return retorno;
        }

        #endregion

        #region Conversão de Coordenadas

        /// <summary>
        /// Decodifica coordenadas.
        /// Transforma do endereço no buffer para array linha, coluna.
        /// Considera a quantidade de colunas para fazer a decodificação.
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static byte[] DecodeCoordinates(int address, int columns)
        {
            // Acha linha e coluna
            int coluna = 0;
            Math.DivRem(address, columns, out coluna);
            int linha = address / columns;

            // Retorna
            return new byte[] { (byte)linha, (byte)coluna };
        }

        /// <summary>
        /// Codifica coordenadas.
        /// Transforma de linhas e colunas para o endereço no buffer.
        /// Considera a quantidade de colunas para fazer a codificação.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static int EncodeCoordinates(byte row, byte col, byte columns)
        {
            // Retorna
            return (row * columns) + col;
        }

        #endregion

        #region Conversão de Caracteres

        /// <summary>
        /// Converte as informações de ebcdic para ascii
        /// </summary>
        /// <param name="ebcdic"></param>
        /// <returns></returns>
        public static string ConvertToASCII(byte[] ebcdic)
        {
            // Faz a conversao
            byte[] ascii = new byte[ebcdic.Length];
            for (int i = 0; i < ascii.Length; i++)
                ascii[i] = Tables.ebc2asc[ebcdic[i]];

            // Transforma em string
            return ASCIIEncoding.ASCII.GetString(ascii);
        }

        /// <summary>
        /// Converte a string de ASCII para EBCDIC
        /// </summary>
        /// <param name="ascii"></param>
        /// <returns></returns>
        public static byte[] ConvertToEBCDIC(string ascii)
        {
            // Faz a conversao
            byte[] ebcdic = new byte[ascii.Length];
            for (int i = 0; i < ascii.Length; i++)
                ebcdic[i] = Tables.a_to_e_table[ascii[i]];

            // Retorna
            return ebcdic;
        }

        #endregion

        #region Manipulação de Bits

        /// <summary>
        /// Seta o bit indicado
        /// </summary>
        /// <param name="b"></param>
        /// <param name="bit"></param>
        public static byte SetBit(byte b, byte bit)
        {
            byte mascara = (byte)(Math.Pow(2, bit));
            return (byte)(b | mascara);
        }

        /// <summary>
        /// Resseta o bit indicado
        /// </summary>
        /// <param name="b"></param>
        /// <param name="bit"></param>
        /// <returns></returns>
        public static byte ResetBit(byte b, byte bit)
        {
            byte mascara = (byte)(~(int)(Math.Pow(2, bit)) & 0x00ff);
            return (byte)(b & mascara);
        }

        /// <summary>
        /// Testa o bit indicado
        /// </summary>
        /// <param name="b"></param>
        /// <param name="bit"></param>
        /// <returns></returns>
        public static bool TestBit(byte b, byte bit)
        {
            byte mascara = (byte)(Math.Pow(2, bit));
            return (byte)(b & mascara) == mascara;
        }

        #endregion

        #region Geracao de Imagem

        /// <summary>
        /// Gera imagem do buffer.
        /// Overload que considera alguns valores default, como espacamento 0 e fonte System
        /// </summary>
        /// <param name="g"></param>
        /// <param name="buffer"></param>
        public static void GerarImagemBuffer(Graphics graphics, string buffer)
        {
            // Repassa a chamada
            Tn3270Utils.GerarImagemBuffer(graphics, new Size(0, 0), Color.Black, Color.White, Color.White, "System", buffer);
        }

        /// <summary>
        /// Gera a imagem do conteudo do buffer no objeto graphics informado
        /// </summary>
        public static void GerarImagemBuffer(Graphics graphics, Size espacamento, Color corFrente, Color corFundo, Color corBorda, string nomeFonte, string buffer)
        {
            // Acha o tamanho da figura
            Size tamanhoFigura = 
                new Size(
                    (int)graphics.VisibleClipBounds.Width, 
                    (int)graphics.VisibleClipBounds.Height);
            
            // Acha a quantidade de linhas e colunas
            string[] buffer2 = buffer.Split("\r\n".ToCharArray(), StringSplitOptions.None);
            Size tamanhoTela = new Size(buffer2[0].Length, buffer2.Length);

            // Quebra a string em um array de linhas x colunas
            char[,] conteudo = new char[tamanhoTela.Width, tamanhoTela.Height];
            for (int y = 0; y < buffer2.Length; y++)
                for (int x = 0; x < buffer2[y].Length; x++ )
                    conteudo[x, y] = buffer2[y][x]; 

            // Acha tamanho total e tamanho do item
            Size tamanhoTotal =
                new Size(
                    tamanhoFigura.Width,
                    tamanhoFigura.Height);
            double larguraItem = (double)tamanhoFigura.Width / tamanhoTela.Width;
            double alturaItem = (double)tamanhoFigura.Height / tamanhoTela.Height;

            // Cria box para o tamanho do item. 
            // Será utilizado no loop alterando apenas a posicao, o tamanho será mantido
            Rectangle box =
                new Rectangle(
                    new Point(0, 0),
                    new Size(
                        (int)larguraItem - espacamento.Width,
                        (int)alturaItem - espacamento.Height));

            // Cria demais objetos utilizados para a pintura no loop
            Pen pen = new Pen(Color.Black);

            // Cria fonte e testa o tamanho
            Font font = new Font(nomeFonte, 15);
            SizeF tamanhoString = graphics.MeasureString("X", font);

            // Acha um tamanho que caiba no box do item
            while (tamanhoString.Width - 4 >= larguraItem || tamanhoString.Height - 4 >= alturaItem)
            {
                font = new Font(nomeFonte, font.Size - .5F);
                tamanhoString = graphics.MeasureString("0", font);
            }

            // Faz o calculo para centralizar o item no box
            int ajusteTop = (int)((alturaItem - tamanhoString.Height) / 2) + 1;
            int ajusteLeft = (int)((larguraItem - tamanhoString.Width) / 2) + 1;

            // Pinta com a cor de fundo
            graphics.FillRectangle(
                new SolidBrush(corFundo), 
                new Rectangle(0, 0, tamanhoFigura.Width, tamanhoFigura.Height));

            // Desenha a borda
            graphics.DrawRectangle(
                new Pen(corBorda),
                new Rectangle(0, 0, tamanhoFigura.Width - 1, tamanhoFigura.Height - 1));

            // Cria o pincel
            Brush brush = new SolidBrush(corFrente);

            // Loop de linhas
            for (int linha = 0; linha < tamanhoTela.Height; linha++)
            {
                // Loop de colunas
                for (int coluna = 0; coluna < tamanhoTela.Width; coluna++)
                {
                    // Calcula a posicao do item
                    box.Location = new Point((int)(coluna * larguraItem), (int)(linha * alturaItem));

                    // Desenha o item
                    graphics.DrawString(
                        conteudo[coluna, linha] != '\0' ? conteudo[coluna, linha].ToString() : " ", 
                        font, 
                        brush, 
                        box.Location.X + ajusteLeft, 
                        box.Location.Y + ajusteTop);
                }
            }

        }

        #endregion
    }
}
