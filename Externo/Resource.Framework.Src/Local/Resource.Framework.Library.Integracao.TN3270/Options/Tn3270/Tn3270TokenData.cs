﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Classe para representar dados de um stream 3270.
    /// </summary>
    public class Tn3270TokenData : Tn3270TokenBase
    {
        /// <summary>
        /// Contem as informações lidas do buffer já traduzidas para ASCII
        /// </summary>
        public StringBuilder Data { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270TokenData()
        {
            this.Data = new StringBuilder();
        }

        /// <summary>
        /// Escreve no stream
        /// </summary>
        /// <param name="stream"></param>
        protected override void OnWriteStream(Tn3270Stream stream)
        {
            // Converte a string em ebcdic
            byte[] ebcdic = Tn3270Utils.ConvertToEBCDIC(this.Data.ToString());

            // Faz o loop para adicionar no buffer
            foreach (byte b in ebcdic)
                stream.WriteByte(b);
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Data " + this.Data.ToString());
        }
    }
}
