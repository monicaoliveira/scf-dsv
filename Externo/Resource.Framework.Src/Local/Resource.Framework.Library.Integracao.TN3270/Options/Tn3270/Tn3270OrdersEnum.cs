﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Enumerador de ordens Tn3270
    /// </summary>
    public enum Tn3270OrdersEnum : byte
    {
        /// <summary>
        /// Program Tab (PT)
        /// </summary>
        ProgramTab = 0x05,

        /// <summary>
        /// GraphicsEscape (GE)
        /// </summary>
        GraphicsEscape = 0x08,

        /// <summary>
        /// SetBufferAddress (SBA)
        /// </summary>
        SetBufferAddress = 0x11,

        /// <summary>
        /// EraseUnprotectedToAddress (EUA)
        /// </summary>
        EraseUnprotectedToAddress = 0x12,

        /// <summary>
        /// InsertCursor (IC)
        /// </summary>
        InsertCursor = 0x13,

        /// <summary>
        /// StartField (SF)
        /// </summary>
        StartField = 0x1d,

        /// <summary>
        /// SetAttribute (SA)
        /// </summary>
        SetAttribute = 0x28,

        /// <summary>
        /// StartFieldExtended (SFE)
        /// </summary>
        StartFieldExtended = 0x29,

        /// <summary>
        /// ModifyField (MF)
        /// </summary>
        ModifyField = 0x2c,

        /// <summary>
        /// RepeatToAddress (RA)
        /// </summary>
        RepeatToAddress = 0x3c
    }
}
