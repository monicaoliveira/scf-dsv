﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Fields
{
    /// <summary>
    /// Tipos de operações de read partition
    /// </summary>
    public enum Tn3270TokenFieldReadPartitionTypesEnum : byte
    {
        /// <summary>
        /// Query
        /// </summary>
        Query = 0x02,

        /// <summary>
        /// Query List
        /// </summary>
        QueryList = 0x03,

        /// <summary>
        /// Read Modified All (RMA)
        /// </summary>
        ReadModifiedAll = 0x6e,

        /// <summary>
        /// Read Buffer (RB)
        /// </summary>
        ReadBuffer = 0xf2,

        /// <summary>
        /// Read Modified (RM)
        /// </summary>
        ReadModified = 0xf6
    }
}
