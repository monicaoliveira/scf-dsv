﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Fields
{
    /// <summary>
    /// Token para o field Query Reply Modes
    /// </summary>
    [Tn3270TokenField(FieldId = new byte[] { 0x81, 0x88 })]
    public class Tn3270TokenFieldQueryReplyReplyModes : Tn3270TokenFieldBase
    {
        /// <summary>
        /// Indica o query reply mode desejado
        /// </summary>
        public List<Tn3270QueryReplyModesEnum> QueryReplyModes { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270TokenFieldQueryReplyReplyModes()
        {
            this.QueryReplyModes = new List<Tn3270QueryReplyModesEnum>();
        }

        /// <summary>
        /// Escreve no stream
        /// </summary>
        /// <param name="stream"></param>
        protected override void OnWriteStream(Tn3270Stream stream)
        {
            // Escreve no field data
            this.FieldData = this.QueryReplyModes.Cast<byte>().ToArray();

            // Pede para a base escrever
            base.OnWriteStream(stream);
        }
    }
}
