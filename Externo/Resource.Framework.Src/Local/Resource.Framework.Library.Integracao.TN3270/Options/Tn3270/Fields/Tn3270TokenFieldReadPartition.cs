﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Fields
{
    /// <summary>
    /// Token para o field Read Partition
    /// </summary>
    [Tn3270TokenField(FieldId = new byte[] { 0x01 })]
    public class Tn3270TokenFieldReadPartition : Tn3270TokenFieldBase
    {
        /// <summary>
        /// PID - Partition Identifier
        /// X’00’ - X’7E’ (read operations), or
        /// X’FF’ (query operations)
        /// </summary>
        public byte PartitionIdentifier { get; set; }

        /// <summary>
        /// Type - The type of operation to be performed
        /// X’02’ Query 
        /// X’03’ Query List 
        /// X’6E’ Read Modified All (RMA)
        /// X’F2’ Read Buffer (RB)
        /// X’F6’ Read Modified (RM)
        /// </summary>
        public Tn3270TokenFieldReadPartitionTypesEnum Type { get; set; }

        /// <summary>
        /// Reqtype - Request Type - present only for Type = X’03’
        /// Bits 0 e 1:
        ///     B’00’ QCODE List 
        ///     B’01’ Equivalent + QCODE List
        ///     B’10’ All 
        ///     B’11’ Reserved 
        /// Bits 2 a 7:
        ///     SFID - Reserved
        /// </summary>
        public byte RequestType { get; set; }

        /// <summary>
        /// Qcode - Query Reply Codes for Type X’03’
        /// </summary>
        public byte[] QueryReplyCodes { get; set; }

        /// <summary>
        /// Interpreta o buffer lido
        /// </summary>
        /// <param name="stream"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // O fieldBase faz a leitura do campo
            base.OnReadStream(stream);

            // Interpreta os dados
            this.PartitionIdentifier = this.FieldData[0];
            this.Type = (Tn3270TokenFieldReadPartitionTypesEnum)this.FieldData[1];
            if (this.Type == Tn3270TokenFieldReadPartitionTypesEnum.QueryList)
            {
                // Pega o request type
                this.RequestType = this.FieldData[2];

                // Pega os query reply codes
                this.QueryReplyCodes = new byte[this.FieldData.Length - 3];
                for (int i = 3; i < this.FieldData.Length; i++)
                    this.QueryReplyCodes[i - 3] = this.FieldData[i];
            }
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Field ReadPartition PID {0} Type {1}", this.PartitionIdentifier, this.Type);
        }
    }
}
