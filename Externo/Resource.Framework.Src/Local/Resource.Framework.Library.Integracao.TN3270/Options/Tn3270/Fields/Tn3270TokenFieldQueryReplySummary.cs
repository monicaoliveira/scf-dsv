﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Fields
{
    /// <summary>
    /// Token para o field Query Reply Summary
    /// </summary>
    [Tn3270TokenField(FieldId = new byte[] { 0x81, 0x80 })]
    public class Tn3270TokenFieldQueryReplySummary : Tn3270TokenFieldBase
    {
        /// <summary>
        /// Lista dos query codes suportados
        /// </summary>
        public List<Tn3270QueryCodesEnum> QueryCodes { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270TokenFieldQueryReplySummary()
        {
            this.QueryCodes = new List<Tn3270QueryCodesEnum>();
        }
        
        /// <summary>
        /// Escreve no stream
        /// </summary>
        /// <param name="stream"></param>
        protected override void OnWriteStream(Tn3270Stream stream)
        {
            // Escreve no field data
            this.FieldData = this.QueryCodes.Cast<byte>().ToArray();

            // Pede para a base escrever
            base.OnWriteStream(stream);
        }
    }
}
