﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Representa informações no nivel telnet
    /// </summary>
    public class Tn3270TelnetDataEventArgs : EventArgs
    {
        /// <summary>
        /// Dados telnet recebidos
        /// </summary>
        public TelnetTokenData DataToken { get; set; }
    }
}
