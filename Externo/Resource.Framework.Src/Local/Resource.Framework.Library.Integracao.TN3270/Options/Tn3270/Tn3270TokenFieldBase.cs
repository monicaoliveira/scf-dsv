﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Classe base para os campos estruturados
    /// </summary>
    public class Tn3270TokenFieldBase : Tn3270TokenBase
    {
        /// <summary>
        /// ID do campo
        /// Não há como o sistema inferir o id, por isso
        /// este ID é pego através do atributo de field, caso exista
        /// </summary>
        public byte[] FieldID { get; set; }
        
        /// <summary>
        /// Informações do campo
        /// </summary>
        public byte[] FieldData { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270TokenFieldBase()
        {
            // Pega informações do atributo, caso exista
            Type tipo = this.GetType();
            object[] attrs = tipo.GetCustomAttributes(typeof(Tn3270TokenFieldAttribute), true);
            if (attrs.Length > 0)
                this.FieldID = ((Tn3270TokenFieldAttribute)attrs[0]).FieldId;
            else
                this.FieldID = new byte[] {};
        }

        /// <summary>
        /// Leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Lê os 2 bytes de tamanho
            byte tamanho1 = stream.ReadByte().Value;
            byte tamanho2 = stream.ReadByte().Value;

            // Monta o tamanho
            int tamanho = tamanho1 * 256 + tamanho2;

            // Caso o FieldID tenha sido lido, retira ele dos dados (descarta)
            if (this.FieldID.Length > 0)
                for (int i = 0; i < this.FieldID.Length; i++)
                    stream.ReadByte();

            // Cria o buffer
            this.FieldData = new byte[tamanho - 2 - this.FieldID.Length];
            
            // Retira as informações
            for (int i = 0; i < tamanho - 2 - this.FieldID.Length; i++)
                this.FieldData[i] = stream.ReadByte().Value;
        }

        /// <summary>
        /// Escreve no buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnWriteStream(Tn3270Stream stream)
        {
            // Monta o tamanho
            int tamanho = this.FieldData.Length + 2 + this.FieldID.Length;

            // Faz a separacao dos bytes de tamanho
            byte tamanho1 = (byte)((tamanho & 0xff00) / 0xff);
            byte tamanho2 = (byte)(tamanho & 0x00ff);

            // Escreve tamanho
            stream.WriteByte(tamanho1);
            stream.WriteByte(tamanho2);

            // Escreve ID, caso exista
            if (this.FieldID.Length > 0)
                for (int i = 0; i < this.FieldID.Length; i++)
                    stream.WriteByte(this.FieldID[i]);

            // Escreve as informações
            for (int i = 0; i < this.FieldData.Length; i++)
                stream.WriteByte(this.FieldData[i]);
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Field " + this.FieldID.ToString() + " " + this.FieldData.ToString());
        }
    }
}
