﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Representa um buffer 3270.
    /// O buffer seria o device, ou o elemento que processa as requisições
    /// e mantém a construção das telas, tratamento de tab stops, etc
    /// </summary>
    public class Tn3270Buffer : ICloneable
    {
        /// <summary>
        /// Número máximo de colunas que o buffer poderá ter
        /// </summary>
        private const int _ctMaxColumns = 132;

        /// <summary>
        /// Número máximo de linhas que o buffer poderá ter
        /// </summary>
        private const int _ctMaxRows = 27;

        /// <summary>
        /// Cria o buffer prevendo o máximo de colunas e linhas possível
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Indica quantidade de colunas do buffer
        /// </summary>
        public byte Columns { get; set; }

        /// <summary>
        /// Indica quantidade de linhas do buffer
        /// </summary>
        public byte Rows { get; set; }

        /// <summary>
        /// Indica o endereço atual no buffer
        /// </summary>
        public Tn3270Address CurrentBufferAddress { get; set; }

        /// <summary>
        /// Indica o endereço atual do cursor
        /// </summary>
        public Tn3270Address CurrentCursorAddress { get; set; }

        /// <summary>
        /// Lista de fields da tela atual.
        /// A chave é o endereço do campo no buffer.
        /// </summary>
        public Dictionary<int, Tn3270FieldInfo> Fields { get; set; }

        /// <summary>
        /// Aponta para o campo atual
        /// </summary>
        public Tn3270FieldInfo CurrentField { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270Buffer()
        {
            // Inicializa
            this.CurrentBufferAddress = new Tn3270Address();
            this.CurrentCursorAddress = new Tn3270Address();
            this.Fields = new Dictionary<int, Tn3270FieldInfo>();
            this.Data = new byte[_ctMaxColumns * _ctMaxRows];

            // Seta o tamanho inicial
            this.Columns = 80;
            this.Rows = 24;
        }

        /// <summary>
        /// Transforma as coordenadas de linha e coluna em um endereço
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public int EncodeCoordinates(byte row, byte column)
        {
            return Tn3270Utils.EncodeCoordinates(row, column, this.Columns);
        }

        /// <summary>
        /// Solicita que o buffer faça o processamento do token informado
        /// </summary>
        public void ProcessToken(Tn3270TokenBase token)
        {
            // Pega o tipo do token
            Type tipoToken = token.GetType();

            // Processa de acordo com o tipo
            if (tipoToken == typeof(Tn3270TokenData))
            {
                // Processa dados... adiciona no buffer
                Tn3270TokenData token2 = token as Tn3270TokenData;
                Write(ASCIIEncoding.ASCII.GetBytes(token2.Data.ToString()));
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderEUA))
            {
                // Pega o token com o tipo correto
                Orders.Tn3270TokenOrderEUA token2 = token as Orders.Tn3270TokenOrderEUA;
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderGE))
            {
                // Processa Graphics Escape... adiciona o caracter no buffer
                Orders.Tn3270TokenOrderGE token2 = token as Orders.Tn3270TokenOrderGE;
                Write(new byte[] { token2.Character });
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderIC))
            {
                // Processa Insert Cursor... faz o endereço atual do cursor = endereço atual do buffer
                Orders.Tn3270TokenOrderIC token2 = token as Orders.Tn3270TokenOrderIC;
                this.CurrentCursorAddress = this.CurrentBufferAddress;
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderMF))
            {
                // Pega o token com o tipo correto
                Orders.Tn3270TokenOrderMF token2 = token as Orders.Tn3270TokenOrderMF;
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderPT))
            {
                // Pega o token com o tipo correto
                Orders.Tn3270TokenOrderPT token2 = token as Orders.Tn3270TokenOrderPT;
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderRA))
            {
                // Processa repetir até o endereço
                Orders.Tn3270TokenOrderRA token2 = token as Orders.Tn3270TokenOrderRA;
                while (this.CurrentBufferAddress.Address < token2.BufferAddress)
                    Write(token2.Character);
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderSA))
            {
                // Pega o token com o tipo correto
                Orders.Tn3270TokenOrderSA token2 = token as Orders.Tn3270TokenOrderSA;
                
                // Temporario... finge que colocou o atributo na tela
                this.CurrentBufferAddress.Address++;
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderSBA))
            {
                // Processa Set Buffer Address... faz o endereço corrente o endereço informado
                Orders.Tn3270TokenOrderSBA token2 = token as Orders.Tn3270TokenOrderSBA;
                this.CurrentBufferAddress.Address = token2.BufferAddress;
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderSF))
            {
                // Pega o token com o tipo correto
                Orders.Tn3270TokenOrderSF token2 = token as Orders.Tn3270TokenOrderSF;

                // Se o campo já existir, remove para sobrepor
                if (this.Fields.ContainsKey(this.CurrentBufferAddress.Address))
                    this.Fields.Remove(this.CurrentBufferAddress.Address);
                
                // Adiciona o field na coleção
                this.Fields.Add(
                    this.CurrentBufferAddress.Address,
                    new Tn3270FieldInfo()
                    {
                        Address = new Tn3270Address(this.CurrentBufferAddress.Address),
                        Attribute = token2.FieldAttribute
                    });

                // Reserva no buffer o espaço para o atributo. Representa com um espaço
                this.Write(' ');
            }
            else if (tipoToken == typeof(Orders.Tn3270TokenOrderSFE))
            {
                // Pega o token com o tipo correto
                Orders.Tn3270TokenOrderSFE token2 = token as Orders.Tn3270TokenOrderSFE;
            }
            else if (tipoToken == typeof(Commands.Tn3270TokenCommandEW))
            {
                // Solicita reinicialização do buffer
                this.ResetBuffer();
            }
        }

        /// <summary>
        /// Indica se o endereço informado é o início de um campo
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public bool IsStartOfField(int address)
        {
            // Verifica se tem algum campo iniciando no endereco informado
            Tn3270FieldInfo field =
                (from f in this.Fields
                 where f.Key == address 
                 select f.Value).FirstOrDefault();

            // Indica se encontrou
            return field != null;
        }

        /// <summary>
        /// Retorna o próximo field após o endereço
        /// </summary>
        public Tn3270FieldInfo GetNextField(int address, Tn3270FieldProtectionEnum? protection)
        {
            // Acha o próximo campo desprotegido a partir do endereço
            Tn3270FieldInfo field =
                (from f in this.Fields
                 where f.Key > address &&
                        (!protection.HasValue || f.Value.Attribute.Protection == protection.Value)
                 select f.Value).FirstOrDefault();

            // Se não encontrou nenhum, procura a partir do início
            if (field == null)
                field =
                    (from f in this.Fields
                     where (!protection.HasValue || f.Value.Attribute.Protection == protection.Value)
                     select f.Value).FirstOrDefault();

            // Retorna
            return field;
        }

        /// <summary>
        /// Seta o conteudo de um campo
        /// </summary>
        /// <param name="fieldAddress"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void SetFieldData(int fieldAddress, string value)
        {
            // Se tem campos...
            if (this.Fields.Count > 0)
            {
                // ...acha o campo correspondente para indicar que foi modificado
                Tn3270FieldInfo fieldInfo =
                    this.Fields.OrderByDescending(f => f.Key).Where(f => f.Value.Address.Address <= fieldAddress).FirstOrDefault().Value;
                fieldInfo.Attribute.MDT = Tn3270FieldMDTEnum.Modified;
            }

            // Calcula o tamanho do campo
            int enderecoFimLinha = 0;
            int tamanho = this.GetFieldEndAddress(fieldAddress, ref enderecoFimLinha) - fieldAddress + 1;

            // Ajusta a string
            if (value.Length > tamanho)
                value = value.Substring(0, tamanho);
            else if (value.Length < tamanho)
                value = value.PadRight(tamanho);

            // Escreve
            this.Write(value, fieldAddress);
        }
        
        /// <summary>
        /// Retorna o conteudo de um campo
        /// </summary>
        /// <param name="fieldAddress"></param>
        /// <returns></returns>
        public string GetFieldData(int fieldAddress)
        {
            // Varre a faixa verificando se tem nulos
            int enderecoFimLinha = 0;
            int enderecoFim = this.GetFieldEndAddress(fieldAddress, ref enderecoFimLinha);
            for (int i = fieldAddress + 1; i <= enderecoFim; i++)
            {
                if (Data[i] == 0x00)
                {
                    enderecoFim = i - 1;
                    break;
                }
            }

            // Verifica se tem valor a retornar
            if (enderecoFim > fieldAddress)

                // Pega os valores da faixa a considerar
                return
                    ASCIIEncoding.ASCII.GetString(
                        Data,
                        fieldAddress + 1,
                        enderecoFim - fieldAddress);
            else

                // Nao tem valor a retornar
                return "";
        
        }

        /// <summary>
        /// Retorna o endereço do final do campo
        /// </summary>
        /// <param name="fieldAddress"></param>
        /// <returns></returns>
        public int GetFieldEndAddress(int fieldAddress, ref int enderecoFimLinha)
        {
            // Acha o endereco do próximo campo
            int enderecoProximoCampo = 200 * 200;
            Tn3270FieldInfo nextField = this.GetNextField(fieldAddress, null);
            if (nextField != null && nextField.Address.Address > fieldAddress)
                enderecoProximoCampo = nextField.Address.Address;

            // Acha endereço do fim da linha do campo atual
           enderecoFimLinha =
                Tn3270Utils.EncodeCoordinates(
                    (byte)(Tn3270Utils.DecodeCoordinates(fieldAddress, this.Columns)[0] + 1),
                    0,
                    this.Columns) - 1;

            // Considera o menor endereço
            int enderecoFim = 0;
            if (enderecoProximoCampo < enderecoFimLinha)
            {
                enderecoFim = enderecoProximoCampo - 1;
            }
            else
            {
                enderecoFim = enderecoFimLinha;
            }

            // Retorna
            return enderecoFim;
        }

        /// <summary>
        /// Resseta o buffer.
        /// </summary>
        public void ResetBuffer()
        {
            Data = new byte[_ctMaxColumns * _ctMaxColumns];
            this.CurrentBufferAddress.Address = 0;
            this.CurrentCursorAddress.Address = 0;
            this.Fields.Clear();
            this.CurrentField = null;
        }

        /// <summary>
        /// Retorna a representação do buffer no formato da tela (cols x rows)
        /// </summary>
        /// <returns></returns>
        public string GetCurrentScreen()
        {
            // Inicializa
            StringBuilder tela = new StringBuilder(this.Columns * this.Rows);

            // Varre as linhas
            for (byte i = 0; i < this.Rows; i++)
                tela.Append(ASCIIEncoding.ASCII.GetString(this.Data, Tn3270Utils.EncodeCoordinates(i, 0, this.Columns), this.Columns) + "\r\n").Replace('\0', ' ');

            // Retorna
            return tela.ToString();
        }

        /// <summary>
        /// Retorna o conteudo da linha solicitada
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public string GetBufferLine(int line)
        {
            return this.GetCurrentScreen().Substring(line * (this.Columns + 2), this.Columns);
        }

        /// <summary>
        /// Adiciona informações no buffer na posição atual do buffer
        /// </summary>
        /// <param name="data"></param>
        public void Write(string data)
        {
            Write(ASCIIEncoding.ASCII.GetBytes(data));
        }

        /// <summary>
        /// Adiciona informações no buffer na posição informada
        /// </summary>
        /// <param name="data"></param>
        public void Write(string data, int endereco)
        {
            Write(ASCIIEncoding.ASCII.GetBytes(data), endereco);
        }

        /// <summary>
        /// Adiciona informações no buffer na posição atual do buffer
        /// </summary>
        /// <param name="data"></param>
        public void Write(char data)
        {
            Write(new byte[] { (byte)data });
        }

        /// <summary>
        /// Adiciona informações no buffer na posição atual do buffer
        /// </summary>
        /// <param name="data"></param>
        public void Write(byte data)
        {
            Write(new byte[] { data });
        }
        
        /// <summary>
        /// Adiciona informações no buffer na posição atual do buffer
        /// </summary>
        /// <param name="data"></param>
        public void Write(byte[] data)
        {
            Write(data, this.CurrentBufferAddress.Address);
        }
        
        /// <summary>
        /// Adiciona informações no buffer na posição indicada 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="address"></param>
        public void Write(byte[] data, int address)
        {
            for (int i = 0; i < data.Length; i++)
                Data[address + i] = data[i];
            this.CurrentBufferAddress.Address = address + data.Length;
        }

        #region ICloneable Members

        public object Clone()
        {
            // Cria o novo buffer
            Tn3270Buffer novoBuffer = new Tn3270Buffer();
            novoBuffer.Columns = this.Columns;
            novoBuffer.CurrentBufferAddress = this.CurrentBufferAddress;
            novoBuffer.CurrentCursorAddress = this.CurrentCursorAddress;
            novoBuffer.CurrentField = this.CurrentField;
            novoBuffer.Data = (byte[])this.Data.Clone();
            novoBuffer.Fields = this.Fields;
            novoBuffer.Rows = this.Rows;

            // Retorna
            return novoBuffer;
        }

        #endregion
    }
}
