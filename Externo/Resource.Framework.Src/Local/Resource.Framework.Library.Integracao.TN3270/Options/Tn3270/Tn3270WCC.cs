﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Representa o Write Control Character (WCC)
    /// </summary>
    public class Tn3270WCC
    {
        private byte _currentWCC = 0;

        public Tn3270WCC()
        {
        }

        public Tn3270WCC(byte wcc)
        {
            this.SetWCC(wcc);
        }

        public byte GetWCC()
        {
            return _currentWCC;
        }

        public void SetWCC(byte wcc)
        {
            _currentWCC = wcc;
        }

        public bool Bit0 
        {
            get { return (_currentWCC & 0x01) == 0x01; }
            set { _currentWCC = (byte)(_currentWCC | 0x01); } 
        }

        public bool Bit1
        {
            get { return (_currentWCC & 0x02) == 0x02; }
            set { _currentWCC = (byte)(_currentWCC | 0x02); }
        }

        public bool Bit2
        {
            get { return (_currentWCC & 0x04) == 0x04; }
            set { _currentWCC = (byte)(_currentWCC | 0x04); }
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "WCC(" + _currentWCC.ToString() + ")";
        }
    }
}
