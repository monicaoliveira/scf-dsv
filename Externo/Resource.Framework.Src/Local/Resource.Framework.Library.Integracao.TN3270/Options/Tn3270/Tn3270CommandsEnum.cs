﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Enumerador de comandos do Tn3270
    /// </summary>
    public enum Tn3270CommandsEnum : byte
    {
        /// <summary>
        /// Read Modified All (RMA)
        /// </summary>
        ReadModifiedAll = 0x0e,

        /// <summary>
        /// Erase All Unprotected (EAU)
        /// </summary>
        EraseAllUnprotected = 0x0f,

        /// <summary>
        /// Erase / Write Alternate (EWA)
        /// </summary>
        EraseWriteAlternate = 0x0d,

        /// <summary>
        /// Write (W)
        /// </summary>
        Write = 0x01,

        /// <summary>
        /// Read Buffer (RB)
        /// </summary>
        ReadBuffer = 0x02,

        /// <summary>
        /// Write Structured Field (WSF)
        /// </summary>
        WriteStructuredField = 0x11,

        /// <summary>
        /// Erase Write (EW)
        /// </summary>
        EraseWrite = 0x05,

        /// <summary>
        /// Read Modified (RM)
        /// </summary>
        ReadModified = 0x06
    }
}
