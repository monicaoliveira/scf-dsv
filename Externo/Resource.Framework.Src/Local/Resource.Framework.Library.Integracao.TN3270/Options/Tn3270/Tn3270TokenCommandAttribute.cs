﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Atributo para identificar um token de comando
    /// </summary>
    public class Tn3270TokenCommandAttribute : Attribute
    {
        /// <summary>
        /// Identificador do comando
        /// </summary>
        public byte CommandId { get; set; }

        /// <summary>
        /// Permite que um segundo identificador seja informado para este comando
        /// Obs: O valor 0 indica que não há segundo ID. Não foi utilizado o tipo byte? pois 
        /// este tipo não pode ser usado com atributos
        /// </summary>
        public byte SecondCommandId { get; set; }
    }
}
