﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Lista de Query Codes
    /// </summary>
    public enum Tn3270QueryCodesEnum : byte
    {
        Summary = 0x80,
        UsableArea = 0x81,
        Image = 0x82,
        TextPartitions = 0x83,
        AlphanumericPartitions = 0x84,
        CharacterSets = 0x85,
        Color = 0x86,
        Highlighting = 0x87,
        ReplyModes = 0x88,
        FieldValidation = 0x8a,
        MSRControl = 0x8b,
        FieldOutlining = 0x8c,
        PartitionCharacteristics = 0x8e,
        OEMAuxiliaryDevice = 0x8f,
        FormatPresentation = 0x90,
        DBCSAsia = 0x91,
        SaveRestoreFormat = 0x92,
        FormatStorageAuxiliaryDevice = 0x94,
        DistributedDataManagement = 0x95,
        StoragePools = 0x96,
        DocumentInterchangeArchitecture = 0x97,
        DataChaining = 0x98,
        AuxiliaryDevice = 0x99,
        IPDS3270 = 0x9a,
        ProductDefinedDataStream = 0x9c,
        AnomalyImplementation = 0x9d,
        IBMAuxiliaryDevice = 0x9e,
        BeginEndOfFile = 0x9f,
        DeviceCharacteristics = 0xa0,
        RPQNames = 0xa1,
        DataStreams = 0xa2,
        ImplicitPartition = 0xa6,
        PaperFeedTechniques = 0xa7,
        Transparency = 0xa8,
        SettablePrinterChars = 0xa9,
        IOCAAuxiliaryDevice = 0xaa,
        CooperativeProcRequestor = 0xab,
        Segment = 0xb0,
        Procedure = 0xb1,
        LineType = 0xb2,
        Port = 0xb3,
        GraphicColor = 0xb4,
        ExtendedDrawingRoutine = 0xb5,
        GraphicSymbolSets = 0xb6,
        Null = 0xff
    }
}
