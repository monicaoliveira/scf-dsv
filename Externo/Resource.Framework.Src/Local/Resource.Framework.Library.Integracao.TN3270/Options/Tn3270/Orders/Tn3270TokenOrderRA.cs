﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Orders
{
    /// <summary>
    /// Token para a ordem Repeat to Address (RA)
    /// </summary>
    [Tn3270TokenOrder(OrderId = (byte)Tn3270OrdersEnum.RepeatToAddress)]
    public class Tn3270TokenOrderRA : Tn3270TokenBase
    {
        /// <summary>
        /// Endereço
        /// </summary>
        public int BufferAddress { get; set; }

        /// <summary>
        /// Caracter a ser repetido
        /// </summary>
        public byte[] Character { get; set; }
        
        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Faz a leitura do comando
            stream.ReadByte();

            // Lê os 2 bytes de endereço
            byte addr1 = stream.ReadByte().Value;
            byte addr2 = stream.ReadByte().Value;

            // Monta o endereço
            this.BufferAddress = Tn3270Utils.DecodeAddress(new byte[] { addr1, addr2 });

            // Lê o caractere (se vier o GE, tem que ler o próximo caracter)
            // O caracter vem em EBCDIC... também faz a conversao para ASCII
            byte chr = stream.ReadByte().Value;
            if (chr == (byte)Tn3270OrdersEnum.GraphicsEscape)
                this.Character = new byte[] { chr, (byte)Tn3270Utils.ConvertToASCII(new byte[] { stream.ReadByte().Value })[0] };
            else
                this.Character = new byte[] { (byte)Tn3270Utils.ConvertToASCII(new byte[] { chr })[0] };
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format(
                "TN3270 Order {0} BufferAddress {1} ({3},{4}) Character {2}", 
                Tn3270OrdersEnum.RepeatToAddress, 
                this.BufferAddress, 
                this.Character,                 
                Tn3270Utils.DecodeCoordinates(this.BufferAddress, 80)[0], 
                Tn3270Utils.DecodeCoordinates(this.BufferAddress, 80)[1]);
        }

    }
}
