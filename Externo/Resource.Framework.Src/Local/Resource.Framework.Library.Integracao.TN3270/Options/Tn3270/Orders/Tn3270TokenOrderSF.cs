﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Orders
{
    /// <summary>
    /// Token para a ordem Start Field (SF)
    /// </summary>
    [Tn3270TokenOrder(OrderId = (byte)Tn3270OrdersEnum.StartField)]
    public class Tn3270TokenOrderSF : Tn3270TokenBase
    {
        /// <summary>
        /// Indica o atributo do campo
        /// </summary>
        public Tn3270FieldAttribute FieldAttribute { get; set; }

        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Faz a leitura do comando
            stream.ReadByte();

            // Faz a leitura do atributo
            this.FieldAttribute = new Tn3270FieldAttribute(stream.ReadByte().Value);
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Order {0} {1}", Tn3270OrdersEnum.StartField, this.FieldAttribute);
        }

    }
}
