﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Orders
{
    /// <summary>
    /// Token para a ordem Set Attribute (SA)
    /// </summary>
    [Tn3270TokenOrder(OrderId = (byte)Tn3270OrdersEnum.SetAttribute)]
    public class Tn3270TokenOrderSA : Tn3270TokenBase
    {
        /// <summary>
        /// Indica o tipo do atributo
        /// </summary>
        public byte AttributeType { get; set; }

        /// <summary>
        /// Indica o valor do atributo
        /// </summary>
        public byte AttributeValue { get; set; }

        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Faz a leitura do comando
            stream.ReadByte();

            // Lê o tipo e o valor do atributo
            this.AttributeType = stream.ReadByte().Value;
            this.AttributeValue = stream.ReadByte().Value;
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Order {0} Type {1} Value", Tn3270OrdersEnum.SetAttribute, this.AttributeType, this.AttributeValue);
        }

    }
}
