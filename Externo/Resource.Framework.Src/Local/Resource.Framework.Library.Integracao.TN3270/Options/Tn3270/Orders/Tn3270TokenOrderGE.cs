﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Orders
{
    /// <summary>
    /// Token para a ordem Erase Graphics Escapa (GE)
    /// </summary>
    [Tn3270TokenOrder(OrderId = (byte)Tn3270OrdersEnum.GraphicsEscape)]
    public class Tn3270TokenOrderGE : Tn3270TokenBase
    {
        /// <summary>
        /// Caracter
        /// </summary>
        public byte Character { get; set; }

        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Faz a leitura do comando
            stream.ReadByte();

            // Lê o caracter
            this.Character = stream.ReadByte().Value;
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Order {0} {1}", Tn3270OrdersEnum.GraphicsEscape, this.Character);
        }
    }
}
