﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Orders
{
    /// <summary>
    /// Token para a ordem Erase Unprotected to Address (EUA)
    /// </summary>
    [Tn3270TokenOrder(OrderId = (byte)Tn3270OrdersEnum.EraseUnprotectedToAddress)]
    public class Tn3270TokenOrderEUA : Tn3270TokenBase
    {
        /// <summary>
        /// Endereço
        /// </summary>
        public int StopAddress { get; set; }

        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Faz a leitura do comando
            stream.ReadByte();

            // Lê os 2 bytes de endereço
            byte addr1 = stream.ReadByte().Value;
            byte addr2 = stream.ReadByte().Value;

            // Monta o endereço
            this.StopAddress = Tn3270Utils.DecodeAddress(new byte[] { addr1, addr2 });
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format(
                "TN3270 Order {0} {1} ({2},{3})", 
                Tn3270OrdersEnum.EraseUnprotectedToAddress, 
                this.StopAddress, 
                Tn3270Utils.DecodeCoordinates(this.StopAddress, 80)[0], 
                Tn3270Utils.DecodeCoordinates(this.StopAddress, 80)[1]);
        }
    }
}
