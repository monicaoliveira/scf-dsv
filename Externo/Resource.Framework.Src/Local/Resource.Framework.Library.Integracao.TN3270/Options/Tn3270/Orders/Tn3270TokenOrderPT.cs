﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Orders
{
    /// <summary>
    /// Token para a ordem Program Tab (PT)
    /// </summary>
    [Tn3270TokenOrder(OrderId = (byte)Tn3270OrdersEnum.ProgramTab)]
    public class Tn3270TokenOrderPT : Tn3270TokenBase
    {
        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Faz a leitura do comando
            stream.ReadByte();
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Order {0}", Tn3270OrdersEnum.ProgramTab);
        }

    }
}
