﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Atributo para identificar um token de ordem
    /// </summary>
    public class Tn3270TokenOrderAttribute : Attribute
    {
        /// <summary>
        /// Identificador da ordem
        /// </summary>
        public byte OrderId { get; set; }
    }
}
