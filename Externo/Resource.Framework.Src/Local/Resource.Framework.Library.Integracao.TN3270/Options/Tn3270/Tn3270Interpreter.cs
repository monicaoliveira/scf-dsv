﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Interpretador 3270.
    /// Faz a quebra do stream de entrada em tokens.
    /// </summary>
    public class Tn3270Interpreter
    {
        /// <summary>
        /// Dicionário com os tokens de comandos
        /// </summary>
        private Dictionary<byte, Type> _commands = new Dictionary<byte, Type>();

        /// <summary>
        /// Dicionário com os tokens de ordens
        /// </summary>
        private Dictionary<byte, Type> _orders = new Dictionary<byte, Type>();

        /// <summary>
        /// Dicionário com os tokens de campos
        /// </summary>
        private Dictionary<byte[], Type> _fields = new Dictionary<byte[], Type>();

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270Interpreter()
        {
            // Pega lista de tipos
            Type[] tipos = Assembly.GetExecutingAssembly().GetTypes();
            
            // Varre a lista de tipos
            foreach (Type tipo in tipos)
            {
                // Temporario
                object[] attrs = null;
                
                // É comando?
                attrs = tipo.GetCustomAttributes(typeof(Tn3270TokenCommandAttribute), false);
                if (attrs.Length > 0)
                {
                    // Pega o atributo
                    Tn3270TokenCommandAttribute commandAttribute = (Tn3270TokenCommandAttribute)attrs[0];

                    // Adiciona para o id
                    _commands.Add(commandAttribute.CommandId, tipo);

                    // Tem segundo id?
                    if (commandAttribute.SecondCommandId != 0)
                        _commands.Add(commandAttribute.SecondCommandId, tipo);
                }

                // Cria dicionario com as ordens
                attrs = tipo.GetCustomAttributes(typeof(Tn3270TokenOrderAttribute), false);
                if (attrs.Length > 0)
                    _orders.Add(((Tn3270TokenOrderAttribute)attrs[0]).OrderId, tipo);

                // Cria dicionario com os campos
                attrs = tipo.GetCustomAttributes(typeof(Tn3270TokenFieldAttribute), false);
                if (attrs.Length > 0)
                    _fields.Add(((Tn3270TokenFieldAttribute)attrs[0]).FieldId, tipo);
            }
        }

        /// <summary>
        /// Faz a interpretacao do stream
        /// </summary>
        /// <returns></returns>
        public List<Tn3270TokenBase> InterpretStream(byte[] stream)
        {
            // Flags
            bool comandoJaFoiLido = false;          // Indica se o comando já foi lido
            bool camposEstruturados = false;        // Indica se são campos estruturados

            // Cria um buffer para leitura dos dados
            Tn3270Stream buffer = new Tn3270Stream(stream, stream.Length);

            // Lista de tokens interpretados
            List<Tn3270TokenBase> tokens = new List<Tn3270TokenBase>();

            // Faz o loop de leitura
            while (!buffer.EOF)
            {
                // Se o comando ainda não foi lido, o que vem é um comando
                if (!comandoJaFoiLido)
                {
                    // Faz a leitura do comando e adiciona na lista
                    Tn3270TokenBase tokenCommand = this.GetTokenCommand(buffer);
                    tokens.Add(tokenCommand);

                    // É campo estruturado?
                    if (tokenCommand is Commands.Tn3270TokenCommandWSF)
                        camposEstruturados = true;

                    // Sinaliza
                    comandoJaFoiLido = true;
                }
                else
                {
                    // Está em campos estruturados?
                    if (camposEstruturados)
                    {
                        // Lê o campo e adiciona na coleção
                        Tn3270TokenBase tokenField = this.GetTokenField(buffer);
                        tokens.Add(tokenField);
                    }
                    else
                    {
                        // Verifica se é alguma ordem
                        Tn3270TokenBase tokenOrder = this.GetTokenOrder(buffer);

                        // Achou?
                        if (tokenOrder != null)
                        {
                            // Sim... adiciona na lista
                            tokens.Add(tokenOrder);
                        }
                        else
                        {
                            // Não... é informação... cria o token
                            Tn3270TokenData tokenData = new Tn3270TokenData();

                            // Adiciona os dados de informação até o próximo comando de ordem
                            while (!buffer.EOF && tokenOrder == null)
                            {
                                // Adiciona o dado (já faz a conversão de EBCDIC para ASCII
                                tokenData.Data.Append((char)Tables.ebc2asc[buffer.ReadByte().Value]);

                                // Verifica se o próximo é ordem
                                tokenOrder = this.GetTokenOrder(buffer);
                            }

                            // Adiciona o token de dados na lista
                            tokens.Add(tokenData);

                            // Se encontrou uma ordem (a que fez o while terminar), insere
                            if (tokenOrder != null)
                                tokens.Add(tokenOrder);
                        }
                    }
                }
            }

            // Retorna
            return tokens;
        }

        /// <summary>
        /// Monta o token referente ao comando
        /// </summary>
        /// <returns></returns>
        public Tn3270TokenBase GetTokenCommand(Tn3270Stream buffer)
        {
            // Qual é o comando?
            byte commandID = buffer.PeekByte().Value;

            // Acha o tipo para processar este comando
            Type tipoComando =
                (from c in _commands
                 where c.Key == commandID
                 select c.Value).FirstOrDefault();

            // Cria o objeto
            Tn3270TokenBase tokenComando = (Tn3270TokenBase)Activator.CreateInstance(tipoComando);

            // Faz o parse
            tokenComando.ReadStream(buffer);

            // Retorna
            return tokenComando;
        }

        /// <summary>
        /// Monta o token referente à ordem
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public Tn3270TokenBase GetTokenOrder(Tn3270Stream buffer)
        {
            // Prepara o retorno
            Tn3270TokenBase tokenOrdem = null;
            
            // Qual é a ordem?
            byte? orderID = buffer.PeekByte();

            // Tem byte?
            if (orderID.HasValue)
            {
                // Acha o tipo para processar este comando
                Type tipoOrdem =
                    (from o in _orders
                     where o.Key == orderID.Value
                     select o.Value).FirstOrDefault();

                // Encontrou?
                if (tipoOrdem != null)
                {
                    // Cria o objeto
                    tokenOrdem = (Tn3270TokenBase)Activator.CreateInstance(tipoOrdem);

                    // Faz o parse
                    tokenOrdem.ReadStream(buffer);
                }
            }

            // Retorna
            return tokenOrdem;
        }

        /// <summary>
        /// Monta o token referente ao campo.
        /// Interpreta o campo como:
        /// 0 - tamanho
        /// 1 - tamanho
        /// 2 - id
        /// 3 - possivelmente o 2o byte de id ou dados
        /// 4 em diante - dados
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public Tn3270TokenBase GetTokenField(Tn3270Stream buffer)
        {
            // Qual é o campo?
            byte fieldID1 = buffer.Buffer[buffer.Position + 2];

            // Testa fields de 1 byte (3o byte)
            Type tipoField =
                (from o in _fields
                 where o.Key[0] ==  fieldID1
                 select o.Value).FirstOrDefault();
             
            // Se não achou, testa fields de 2 bytes (3o e 4o bytes)
            if (tipoField == null)
            {
                // Pega o segundo ID 
                byte fieldID2 = buffer.Buffer[buffer.Position + 3];

                // Testa
                tipoField =
                    (from o in _fields
                     where o.Key[0] == fieldID1 &&
                           o.Key[1] == fieldID2 
                     select o.Value).FirstOrDefault();
            }

            // Se não encontrou, cria o field base, pois ele consegue retirar o campo do stream mesmo não conhecendo sua estrutura
            if (tipoField == null)
                tipoField = typeof(Tn3270TokenFieldBase);

            // Cria o objeto
            Tn3270TokenBase tokenField = (Tn3270TokenBase)Activator.CreateInstance(tipoField);

            // Faz o parse
            tokenField.ReadStream(buffer);

            // Retorna
            return tokenField;
        }
    }
}
