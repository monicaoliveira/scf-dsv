﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Processador TN3270
    /// </summary>
    public class Tn3270Processor : ITelnetDataProcessor
    {
        
        /// <summary>
        /// Evento que avisa chegada de dados da camada telnet.
        /// </summary>
        public event EventHandler<Tn3270TelnetDataEventArgs> TelnetDataArrived;

        /// <summary>
        /// Indica o inicio do processamento dos dados
        /// </summary>
        public event EventHandler<Tn3270DataEventArgs> BeginProcessData;

        /// <summary>
        /// Indica o fim do processamento dos dados
        /// </summary>
        public event EventHandler<Tn3270DataEventArgs> EndProcessData;

        /// <summary>
        /// Indica o início do envio de dados
        /// </summary>
        public event EventHandler<Tn3270DataEventArgs> BeginSendData;

        /// <summary>
        /// Indica o fim do envio de dados
        /// </summary>
        public event EventHandler<Tn3270DataEventArgs> EndSendData;

        /// <summary>
        /// Indica o início do envio de dados
        /// </summary>
        public event EventHandler<Tn3270DataEventArgs> BeginSendScreen;

        /// <summary>
        /// Indica o fim do envio de dados
        /// </summary>
        public event EventHandler<Tn3270DataEventArgs> EndSendScreen;

        /// <summary>
        /// Referencia para o option binary
        /// </summary>
        public Telnet Telnet { get; set; }

        /// <summary>
        /// Interpretador 3270
        /// </summary>
        public Tn3270Interpreter Interpreter { get; set; }

        /// <summary>
        /// Representa o buffer de tela
        /// </summary>
        public Tn3270Buffer Buffer { get; set; }

        /// <summary>
        /// Representa o buffer original da tela.
        /// Este buffer não é afetado pelas alterações feitas pelo cliente.
        /// Serve para comparar as informações alteradas com as informações originais
        /// para verificar o que deve ser enviado ao mainframe
        /// </summary>
        public Tn3270Buffer BufferOriginal { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        /// <param name="telnetOptionBinary"></param>
        public Tn3270Processor(Telnet telnet)
        {
            this.Telnet = telnet;
            this.Interpreter = new Tn3270Interpreter();
            this.Buffer = new Tn3270Buffer();
        }

        #region Comandos de mais alto nível do Processador

        /// <summary>
        /// Faz com que o endereço do buffer seja movido para o próximo campo desprotegido
        /// </summary>
        public void MoveToNextField()
        {
            // Pega o endereço atual
            int enderecoAtual = this.Buffer.CurrentBufferAddress.Address;

            // Acha o próximo campo desprotegido a partir do endereço
            Tn3270FieldInfo field = 
                this.Buffer.GetNextField(
                    enderecoAtual, 
                    Tn3270FieldProtectionEnum.Unprotected);

            // Se achou, seta o endereço do buffer
            if (field != null)
                this.Buffer.CurrentBufferAddress.Address = field.Address.Address + 1;
        }

        /// <summary>
        /// Faz o envio de informações para o buffer
        /// </summary>
        /// <param name="text"></param>
        public void WriteBuffer(string text)
        {
            // Adiciona o texto no buffer
            this.Buffer.Write(text);
        }

        /// <summary>
        /// Envia apenas o aid
        /// </summary>
        /// <param name="aid"></param>
        public void SendAID(Tn3270AIDsEnum aid)
        {
            // Lista dos tokens a enviar
            List<Tn3270TokenBase> tokens = new List<Tn3270TokenBase>();

            // Token para o aid
            tokens.Add(
                new Tn3270TokenAID()
                {
                    AID = aid,
                    BufferAddress = this.Buffer.CurrentBufferAddress.Address
                });

            // Faz o envio
            this.SendData(tokens);
        }

        /// <summary>
        /// Faz o envio da tela para o servidor
        /// </summary>
        public void SendScreen(Tn3270AIDsEnum aid)
        {
            this.SendScreen(aid, null);
        }

        /// <summary>
        /// Faz o envio da tela para o servidor
        /// </summary>
        public void SendScreen(Tn3270AIDsEnum aid, string tagEvento)
        {
            // Lista dos tokens a enviar
            List<Tn3270TokenBase> tokens = new List<Tn3270TokenBase>();

            // Token para o aid
            tokens.Add(
                new Tn3270TokenAID() 
                { 
                    AID = aid,
                    BufferAddress = this.Buffer.CurrentBufferAddress.Address
                });

            // Dispara o evento
            if (BeginSendScreen != null)
                BeginSendScreen(
                    this,
                    new Tn3270DataEventArgs()
                    {
                        Tokens = tokens,
                        Tag = tagEvento
                    });

            // Pega lista de campos desprotegidos
            List<KeyValuePair<int, Tn3270FieldInfo>> itens = 
                this.Buffer.Fields.Where(f => f.Value.Attribute.Protection == Tn3270FieldProtectionEnum.Unprotected ||
                    f.Value.Attribute.MDT == Tn3270FieldMDTEnum.Modified).ToList();

            // Tem campos?
            if (itens.Count > 0)
            {
                // Loop nos fields desprotegidos (que puderam ter seus valores alterados)
                foreach (KeyValuePair<int, Tn3270FieldInfo> item in itens)
                {
                    // Verifica se o campo foi alterado ou se está como Display Pen Detectable
                    if (item.Value.Attribute.MDT == Tn3270FieldMDTEnum.Modified)
                    {
                        // Seta o endereço
                        tokens.Add(
                            new Orders.Tn3270TokenOrderSBA()
                            {
                                BufferAddress = item.Value.Address.Address + 1
                            });

                        // Coloca o valor
                        tokens.Add(
                            new Tn3270TokenData()
                            {
                                Data = new StringBuilder(this.Buffer.GetFieldData(item.Key))
                            });
                    }
                }
            }
            else
            {
                // Acha o primeiro nulo
                int enderecoFim = 0;
                for (enderecoFim = 0; enderecoFim < this.Buffer.Data.Length; enderecoFim++)
                    if (this.Buffer.Data[enderecoFim] == 0x00)
                        break;

                // Envia o inicio do buffer
                tokens.Add(
                    new Tn3270TokenData() 
                    { 
                        Data = new StringBuilder(ASCIIEncoding.ASCII.GetString(this.Buffer.Data, 0, enderecoFim))
                    });
            }

            // Envia
            this.SendData(tokens);

            // Dispara o evento
            if (EndSendScreen != null)
                EndSendScreen(
                    this,
                    new Tn3270DataEventArgs()
                    {
                        Tokens = tokens,
                        Tag = tagEvento
                    });
        }

        #endregion

        #region ITelnetDataProcessor Members

        public void ProcessData(TelnetTokenData dataToken)
        {
            this.ProcessData(dataToken, null);
        }


        /// <summary>
        /// Método para o processamento do buffer 3270
        /// </summary>
        /// <param name="dataToken"></param>
        public void ProcessData(TelnetTokenData dataToken, string tagEvento)
        {
            // Dispara o evento
            if (TelnetDataArrived != null)
                TelnetDataArrived(
                    this,
                    new Tn3270TelnetDataEventArgs() 
                    { 
                        DataToken = dataToken                        
                    });

            // Interpreta o stream
            List<Tn3270TokenBase> tokens = 
                this.Interpreter.InterpretStream(dataToken.Data.ToArray());

            // Dispara o evento
            if (BeginProcessData != null)
                BeginProcessData(
                    this,
                    new Tn3270DataEventArgs()
                    {
                        Tokens = tokens,
                        Tag = tagEvento
                    });

            // Processa os tokens
            foreach (Tn3270TokenBase token in tokens)
            {
                // Verifica ações a tomar
                if (token is Fields.Tn3270TokenFieldReadPartition)
                {
                    // Resposta ao read partition
                    new Controls.Tn3270ControlReplyQueryPartition().Execute(this);
                }
                else
                {
                    // Pede para o buffer processar
                    this.Buffer.ProcessToken(token);
                }
            }

            // Copia o buffer para manter informações originais
            this.BufferOriginal = 
                (Tn3270Buffer)this.Buffer.Clone();

            // Dispara o evento
            if (EndProcessData != null)
                EndProcessData(
                    this,
                    new Tn3270DataEventArgs()
                    {
                        Tokens = tokens,
                        Tag = tagEvento
                    });

        }

        #endregion

        /// <summary>
        /// Envia dados
        /// </summary>
        /// <param name="tokens"></param>
        public void SendData(List<Tn3270TokenBase> tokens)
        {
            // Dispara o evento
            if (BeginSendData != null)
                BeginSendData(
                    this,
                    new Tn3270DataEventArgs()
                    {
                        Tokens = tokens
                    });

            // Cria o stream
            Tn3270Stream stream = new Tn3270Stream();

            // Faz os tokens salvarem no stream
            foreach (Tn3270TokenBase token in tokens)
            {
                // Salva no stream
                token.WriteStream(stream);
            }

            // Cria os tokens de dados telnet e o EOR
            List<TelnetTokenBase> tokensTelnet = 
                new List<TelnetTokenBase>() 
                { 
                    new TelnetTokenData() 
                    {
                        Data = stream.Buffer.ToList()
                    },
                    new TelnetTokenCommand()
                    {
                        Verb = TelnetCommandsEnum.EOR
                    }
                };
            
            // Solicita o envio
            this.Telnet.Send(tokensTelnet);

            // Dispara o evento
            if (EndSendData != null)
                EndSendData(
                    this,
                    new Tn3270DataEventArgs()
                    {
                        Tokens = tokens
                    });
        }

    }
}
