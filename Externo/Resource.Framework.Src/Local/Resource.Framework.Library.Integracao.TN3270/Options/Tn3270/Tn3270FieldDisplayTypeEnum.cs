﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Formas que o campo poderá aparecer (visivel, highlight, etc)
    /// </summary>
    public enum Tn3270FieldDisplayTypeEnum : byte
    {
        Display_NotSelectorPenDetectable = 0x00,
        Display_SelectorPenDetectable = 0x01,
        IntesifiedDisplay_SelectorPenDetectable = 0x02,
        NonDisplay_NonDetectable = 0x03
    }
}
