﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Commands
{
    /// <summary>
    /// Token para o comando Write Structured Field (WSF)
    /// </summary>
    [Tn3270TokenCommand(
        CommandId = (byte)Tn3270CommandsEnum.WriteStructuredField,
        SecondCommandId = (byte)Tn3270CommandsSNAEnum.WriteStructuredField)]
    public class Tn3270TokenCommandWSF : Tn3270TokenBase
    {
        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Lê o comando
            stream.ReadByte();
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Command {0}", Tn3270CommandsEnum.WriteStructuredField);
        }

    }
}
