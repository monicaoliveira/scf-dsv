﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Commands
{
    /// <summary>
    /// Token para o comando Erase Write (EW)
    /// </summary>
    [Tn3270TokenCommand(
        CommandId = (byte)Tn3270CommandsEnum.EraseWrite,
        SecondCommandId = (byte)Tn3270CommandsSNAEnum.EraseWrite)]
    public class Tn3270TokenCommandEW : Tn3270TokenBase
    {
        /// <summary>
        /// Representa o WCC
        /// </summary>
        public Tn3270WCC WCC { get; set; }

        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(Tn3270Stream stream)
        {
            // Lê o comando
            stream.ReadByte();

            // Lê o WCC
            this.WCC = new Tn3270WCC(stream.ReadByte().Value);
        }

        /// <summary>
        /// Converte para string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("TN3270 Command {0} {1}", Tn3270CommandsEnum.EraseWrite, this.WCC);
        }

    }
}
