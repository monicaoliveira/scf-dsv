﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Contém uma série de dados (chunk) recebido pelo socket, ou a ser enviado.
    /// Fornece funções para transformação da série de dados em tokens (interpretação 
    /// da série de dados) e vice-versa.
    /// </summary>
    public class Tn3270Stream
    {
        /// <summary>
        /// Buffer puro recebido pelo socket
        /// </summary>
        public List<byte> Buffer { get; set; }

        /// <summary>
        /// Próxima posição a ser lida.
        /// Caso seja igual ao tamanho do buffer, chegou ao final
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Indica se a leitura dos dados chegou ao final
        /// </summary>
        public bool EOF { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public Tn3270Stream()
        {
            this.Buffer = new List<byte>();
        }

        /// <summary>
        /// Recebe o buffer de bytes a ser trabalhado
        /// </summary>
        /// <param name="buffer"></param>
        public Tn3270Stream(byte[] buffer, int size)
        {
            this.Initialize(buffer, size);
        }

        /// <summary>
        /// Faz a inicialização do buffer
        /// </summary>
        /// <param name="buffer"></param>
        public void Initialize(byte[] buffer, int size)
        {
            this.Buffer = new List<byte>(buffer.Take(size));
            this.Position = 0;
            this.EOF = false;
        }

        /// <summary>
        /// Lê o buffer, byte a byte
        /// </summary>
        /// <returns></returns>
        public byte? ReadByte()
        {
            // Resposta
            byte? resposta = null;

            // Chegou ao fim?
            if (!this.EOF)
            {
                // Pega o byte
                resposta = this.Buffer[this.Position++];

                // Verifica se chegou ao fim
                if (this.Position >= this.Buffer.Count)
                    this.EOF = true;
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Lê o byte atual mas mantém a posição
        /// </summary>
        /// <returns></returns>
        public byte? PeekByte()
        {
            // Resposta
            byte? resposta = null;

            // Chegou ao fim?
            if (!this.EOF)
                resposta = this.Buffer[this.Position];

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Lê o próximo byte mantém a posição
        /// </summary>
        /// <returns></returns>
        public byte? PeekNextByte()
        {
            // Resposta
            byte? resposta = null;

            // Chegou ao fim?
            if (!this.EOF && this.Position + 1 < this.Buffer.Count)
                resposta = this.Buffer[this.Position + 1];

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Escreve um byte no buffer e reposiciona
        /// </summary>
        /// <param name="b"></param>
        public void WriteByte(byte b)
        {
            // Adiciona na coleção
            this.Buffer.Add(b);
        }
    }
}
