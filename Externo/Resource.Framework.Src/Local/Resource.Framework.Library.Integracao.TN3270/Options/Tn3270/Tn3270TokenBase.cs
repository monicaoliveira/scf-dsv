﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    public class Tn3270TokenBase
    {
        /// <summary>
        /// Faz a leitura do buffer, e movimenta a posição
        /// de acordo com o necessário
        /// </summary>
        /// <param name="buffer"></param>
        public void ReadStream(Tn3270Stream stream)
        {
            OnReadStream(stream);
        }

        /// <summary>
        /// Método virtual para fazer a leitura do buffer e 
        /// reposicionar.
        /// </summary>
        /// <param name="buffer"></param>
        protected virtual void OnReadStream(Tn3270Stream stream)
        {
        }

        /// <summary>
        /// Faz a gravação no buffer
        /// </summary>
        /// <param name="buffer"></param>
        public void WriteStream(Tn3270Stream stream)
        {
            OnWriteStream(stream);
        }

        /// <summary>
        /// Método virtual para fazer a gravação no buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected virtual void OnWriteStream(Tn3270Stream stream)
        {
        }
    }
}
