﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Attention Identification (AID) List
    /// The AID byte appears only in the inbound (display to application program)
    /// data stream and must be the first byte of the inbound data stream. The
    /// AID indicates the source or type of data that follows in the data stream.
    /// Usually, there is only one AID byte in an RU chain. The exception is an
    /// RU chain containing an Inbound 3270DS structured field that itself can
    /// contain an AID byte.
    /// </summary>
    public enum Tn3270AIDsEnum : byte
    {
        NoAIDGenerated = 0x60,
        NoAIDGeneratedPrinterOnly = 0xe8,
        StructuredField = 0x88,
        ReadPartition = 0x61,
        TriggerAction = 0x7f,
        TestReqAndSysReq = 0xf0,
        PF1Key = 0xf1,
        PF2Key = 0xf2,
        PF3Key = 0xf3,
        PF4Key = 0xf4,
        PF5Key = 0xf5,
        PF6Key = 0xf6,
        PF7Key = 0xf7,
        PF8Key = 0xf8,
        PF9Key = 0xf9,
        PF10Key = 0x7a,
        PF11Key = 0x7b,
        PF12Key = 0x7c,
        PF13Key = 0xc1,
        PF14Key = 0xc2,
        PF15Key = 0xc3,
        PF16Key = 0xc4,
        PF17Key = 0xc5,
        PF18Key = 0xc6,
        PF19Key = 0xc7,
        PF20Key = 0xc8,
        PF21Key = 0xc9,
        PF22Key = 0x4a,
        PF23Key = 0x4b,
        PF24Key = 0x4c,
        PA1Key = 0x6c,
        PA2Key = 0x6e,
        PA3Key = 0x6b,
        ClearKey = 0x6d,
        ClearPartitionKey = 0x6a,
        EnterKey = 0x7d,
        SelectorPenAttention = 0x7e,
        MagneticReaderOperatorID = 0xe6,
        MagneticReaderNumber = 0xe7
    }
}
