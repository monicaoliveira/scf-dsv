﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270.Controls
{
    /// <summary>
    /// Response a uma consulta read partition
    /// </summary>
    public class Tn3270ControlReplyQueryPartition
    {
        /// <summary>
        /// Executa
        /// </summary>
        /// <param name="processor"></param>
        public void Execute(Tn3270Processor processor)
        {
            // Inicializa
            List<Tn3270TokenBase> tokens = new List<Tn3270TokenBase>();

            // Cria o AID
            tokens.Add(
                new Tn3270TokenAID()
                {
                    AID = Tn3270AIDsEnum.StructuredField
                });

            // Cria o sumário
            tokens.Add(
                new Fields.Tn3270TokenFieldQueryReplySummary()
                {
                    QueryCodes =
                        new List<Tn3270QueryCodesEnum>()
                        {
                            Tn3270QueryCodesEnum.Summary,
                            Tn3270QueryCodesEnum.ReplyModes
                        }
                });

            // Cria o reply modes
            tokens.Add(
                new Fields.Tn3270TokenFieldQueryReplyReplyModes()
                {
                    QueryReplyModes =
                        new List<Tn3270QueryReplyModesEnum>()
                        {
                            Tn3270QueryReplyModesEnum.FieldMode,
                            Tn3270QueryReplyModesEnum.ExtendedFieldMode,
                            Tn3270QueryReplyModesEnum.CharacterMode
                        }
                });

            // Envia
            processor.SendData(tokens);
        }
    }
}
