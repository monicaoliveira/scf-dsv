﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Enumerador de comandos do Tn3270
    /// </summary>
    public enum Tn3270CommandsSNAEnum : byte
    {
        /// <summary>
        /// Read Modified All (RMA)
        /// </summary>
        ReadModifiedAll = 0x6e,

        /// <summary>
        /// Erase All Unprotected (EAU)
        /// </summary>
        EraseAllUnprotected = 0x6f,

        /// <summary>
        /// Erase / Write Alternate (EWA)
        /// </summary>
        EraseWriteAlternate = 0x7e,

        /// <summary>
        /// Write (W)
        /// </summary>
        Write = 0xf1,

        /// <summary>
        /// Read Buffer (RB)
        /// </summary>
        ReadBuffer = 0xf2,

        /// <summary>
        /// Write Structured Field (WSF)
        /// </summary>
        WriteStructuredField = 0xf3,

        /// <summary>
        /// Erase Write (EW)
        /// </summary>
        EraseWrite = 0xf5,

        /// <summary>
        /// Read Modified (RM)
        /// </summary>
        ReadModified = 0xf6
    }
}
