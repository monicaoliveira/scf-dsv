﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Tipos de proteção de campos
    /// </summary>
    public enum Tn3270FieldProtectionEnum : byte
    {
        Unprotected = 0,
        Protected = 1
    }
}
