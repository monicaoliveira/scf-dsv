﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options.Tn3270
{
    /// <summary>
    /// Representa um evento de transmissao de dados
    /// </summary>
    public class Tn3270DataEventArgs : EventArgs
    {
        /// <summary>
        /// Lista de tokens a serem processados
        /// </summary>
        public List<Tn3270TokenBase> Tokens { get; set; }

        /// <summary>
        /// String opcional para indicar algum momento especifico da aplicacao
        /// </summary>
        public string Tag { get; set; }
    }
}
