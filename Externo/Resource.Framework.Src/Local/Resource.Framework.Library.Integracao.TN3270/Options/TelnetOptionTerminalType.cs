﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options
{
    public class TelnetOptionTerminalType : TelnetOptionBase
    {
        protected override TelnetOptionsEnum OnGetOption()
        {
            return TelnetOptionsEnum.TerminalType;
        }

        protected override void OnProcessToken(TelnetTokenBase token)
        {
            if (token is TelnetTokenCommand)
            {
                TelnetTokenCommand token2 = token as TelnetTokenCommand;
                if (token2.Verb == TelnetCommandsEnum.DO)
                {
                    this.Telnet.Send(
                        new TelnetTokenCommand()
                        {
                            Verb = TelnetCommandsEnum.WILL,
                            Option = TelnetOptionsEnum.TerminalType
                        });
                }
            }
            else if (token is TelnetTokenSubCommand)
            {
                TelnetTokenSubCommand token2 = token as TelnetTokenSubCommand;
                if (token2.Parameters[0] == 0x01)
                {
                    this.Telnet.Send(
                        new TelnetTokenSubCommand()
                        {
                            Option = TelnetOptionsEnum.TerminalType,
                            Parameters = ASCIIEncoding.ASCII.GetBytes('\0' + "IBM-3278-2-E").ToList()
                        });
                }
            }
        }
    }
}
