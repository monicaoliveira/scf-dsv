﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270.Options
{
    public class TelnetOptionSuppressGoAhead : TelnetOptionBase
    {
        protected override TelnetOptionsEnum OnGetOption()
        {
            return TelnetOptionsEnum.SuppressGoAhead;
        }

        protected override void OnProcessToken(TelnetTokenBase token)
        {
            TelnetTokenCommand token2 = token as TelnetTokenCommand;
            if (token2.Verb == TelnetCommandsEnum.WILL)
                this.Telnet.Send(
                    new TelnetTokenCommand()
                    {
                        Verb = TelnetCommandsEnum.DO,
                        Option = TelnetOptionsEnum.SuppressGoAhead
                    });
        }
    }
}
