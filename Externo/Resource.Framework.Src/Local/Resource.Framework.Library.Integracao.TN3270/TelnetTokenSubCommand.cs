﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    /// <summary>
    /// Representa um subcomando telnet
    /// </summary>
    public class TelnetTokenSubCommand : TelnetTokenBase
    {
        /// <summary>
        /// Opção. Ex: Echo, Binary Transmission, Reconnection, etc.
        /// </summary>
        public TelnetOptionsEnum Option { get; set; }

        /// <summary>
        /// Parametros da opção
        /// </summary>
        public List<byte> Parameters { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public TelnetTokenSubCommand()
        {
            this.Parameters = new List<byte>();
        }

        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(TelnetStream buffer)
        {
            // Faz a leitura 
            buffer.ReadByte();   // IAC, pode ser descartado
            buffer.ReadByte();   // SB, pode ser descartado
            this.Option = (TelnetOptionsEnum)buffer.ReadByte();   // SB, pode ser descartado

            // Loop até encontrar o IAC + SE
            while(true)
            {
                byte b = buffer.ReadByte().Value;
                if (b == (byte)TelnetCommandsEnum.IAC)
                {
                    // Se o próximo byte for um IAC também, é um escape de caracter 255
                    if (buffer.Buffer[buffer.Position] == 255)
                    {
                        // Adiciona o 255 apenas uma vez, ou seja, um é ignorado
                        this.Parameters.Add(buffer.ReadByte().Value);
                    }
                    else if (buffer.Buffer[buffer.Position] == (byte)TelnetCommandsEnum.SE)
                    {
                        // Fim dos parâmetros, posiciona o buffer corretamente
                        buffer.ReadByte();   // IAC
                        buffer.ReadByte();   // SE

                        // Sai do loop
                        break;
                    }
                    else
                    {
                        // Veio um comando diferente de SE para terminar o subcomando. 
                        // Não sei interpretar, gera erro.
                        throw new Exception("Foi recebido um comando diferente de SE para terminar o subcomando. Não há como interpretar.");
                    }
                }
                else
                {
                    // Adiciona na lista de parametros do subcomando
                    this.Parameters.Add(b);
                }
            }
        }

        /// <summary>
        /// Salva o subcomando no buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnWriteStream(TelnetStream buffer)
        {
            // Salva no buffer
            buffer.WriteByte((byte)TelnetCommandsEnum.IAC);
            buffer.WriteByte((byte)TelnetCommandsEnum.SB);
            buffer.WriteByte((byte)this.Option);
            foreach (byte b in this.Parameters)
            {
                // Escreve o caractere
                buffer.WriteByte(b);
                
                // Caso seja 255, o caractere tem que ser duplicado
                if (b == 255)
                    buffer.WriteByte(b);
            }
            buffer.WriteByte((byte)TelnetCommandsEnum.IAC);
            buffer.WriteByte((byte)TelnetCommandsEnum.SE);
        }

        /// <summary>
        /// Representação em texto
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "SB " + this.Option.ToString() + " " + ASCIIEncoding.ASCII.GetString(this.Parameters.ToArray()) + " SE";
        }
    }
}
