﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    /// <summary>
    /// http://www.faqs.org/rfcs/rfc854.html
    /// The following are the defined TELNET commands.  Note that these codes
    /// and code sequences have the indicated meaning only when immediately
    /// preceded by an IAC.
    /// </summary>
    public enum TelnetCommandsEnum : byte
    {

        /// <summary>
        /// End of record (transparent mode)
        /// </summary>
        EOR = 239,

        /// <summary>
        /// SE
        /// End of subnegotiation parameters.
        /// </summary>
        SE = 240,

        /// <summary>
        /// NOP
        /// No operation.
        /// </summary>
        NOP = 241,

        /// <summary>
        /// Data Mark
        /// The data stream portion of a Synch.
        /// This should always be accompanied
        /// by a TCP Urgent notification.
        /// </summary>
        DataMark = 242,

        /// <summary>
        /// Break
        /// NVT character BRK.
        /// </summary>
        BRK = 243,

        /// <summary>
        /// Interrupt Process
        /// The function IP.
        /// </summary>
        IP = 244,

        /// <summary>
        /// Abort output
        /// The function AO.
        /// </summary>
        AO = 245,

        /// <summary>
        /// Are You There
        /// The function AYT.
        /// </summary>
        AYT = 246,

        /// <summary>
        /// Erase character
        /// The function EC.
        /// </summary>
        EC = 247,

        /// <summary>
        /// Erase Line
        /// The function EL.
        /// </summary>
        EL = 248,

        /// <summary>
        /// Go ahead
        /// The GA signal.
        /// </summary>
        GA = 249,

        /// <summary>
        /// SB
        /// Indicates that what follows is
        /// subnegotiation of the indicated
        /// option.
        /// </summary>
        SB = 250,

        /// <summary>
        /// WILL (option code)
        /// Indicates the desire to begin
        /// performing, or confirmation that
        /// you are now performing, the
        /// indicated option.
        /// </summary>
        WILL = 251,

        /// <summary>
        /// WON'T (option code)
        /// Indicates the refusal to perform,
        /// or continue performing, the
        /// indicated option.
        /// </summary>
        WONT = 252,

        /// <summary>
        /// DO (option code)
        /// Indicates the request that the
        /// other party perform, or
        /// confirmation that you are expecting
        /// the other party to perform, the
        /// indicated option.
        /// </summary>
        DO = 253,

        /// <summary>
        /// DON'T (option code)
        /// Indicates the demand that the
        /// other party stop performing,
        /// or confirmation that you are no
        /// longer expecting the other party
        /// to perform, the indicated option.
        /// </summary>
        DONT = 254,

        /// <summary>
        /// IAC
        /// Data Byte 255.
        /// </summary>
        IAC = 255
    }
}
