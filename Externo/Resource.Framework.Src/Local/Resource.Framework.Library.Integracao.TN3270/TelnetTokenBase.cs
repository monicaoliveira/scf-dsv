﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    public class TelnetTokenBase
    {
        /// <summary>
        /// Faz a leitura do buffer, e movimenta a posição
        /// de acordo com o necessário
        /// </summary>
        /// <param name="buffer"></param>
        public void ReadStream(TelnetStream buffer)
        {
            OnReadStream(buffer);
        }

        /// <summary>
        /// Método virtual para fazer a leitura do buffer e 
        /// reposicionar.
        /// </summary>
        /// <param name="buffer"></param>
        protected virtual void OnReadStream(TelnetStream stream)
        {
        }

        /// <summary>
        /// Faz a gravação no buffer
        /// </summary>
        /// <param name="buffer"></param>
        public void WriteStream(TelnetStream buffer)
        {
            OnWriteStream(buffer);
        }

        /// <summary>
        /// Método virtual para fazer a gravação no buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected virtual void OnWriteStream(TelnetStream buffer)
        {
        }
    }
}
