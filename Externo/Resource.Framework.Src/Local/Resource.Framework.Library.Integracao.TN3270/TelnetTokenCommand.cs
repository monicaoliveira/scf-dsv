﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    /// <summary>
    /// Representa um comando telnet
    /// </summary>
    public class TelnetTokenCommand : TelnetTokenBase
    {
        /// <summary>
        /// Verbo: Will, Wont, Do, Dont
        /// </summary>
        public TelnetCommandsEnum Verb { get; set; }

        /// <summary>
        /// Opção. Ex: Echo, Binary Transmission, Reconnection, etc.
        /// </summary>
        public TelnetOptionsEnum Option { get; set; }

        /// <summary>
        /// Faz a leitura do buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnReadStream(TelnetStream buffer)
        {
            // Faz a leitura 
            buffer.ReadByte();   // IAC, pode ser descartado
            this.Verb = (TelnetCommandsEnum)buffer.ReadByte();
            
            // Se for EOR (end of record) não tem mais argumentos
            if (this.Verb != TelnetCommandsEnum.EOR && !buffer.EOF)
                this.Option = (TelnetOptionsEnum)buffer.ReadByte();
        }

        /// <summary>
        /// Salva o comando no buffer
        /// </summary>
        /// <param name="buffer"></param>
        protected override void OnWriteStream(TelnetStream buffer)
        {
            // Salva no buffer
            buffer.WriteByte((byte)TelnetCommandsEnum.IAC);
            buffer.WriteByte((byte)this.Verb);

            // Se for EOR (end of record) não tem mais argumentos
            if (this.Verb != TelnetCommandsEnum.EOR)
                buffer.WriteByte((byte)this.Option);
        }

        /// <summary>
        /// Representação em texto
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Verb.ToString() + " " + this.Option.ToString();
        }
    }
}
