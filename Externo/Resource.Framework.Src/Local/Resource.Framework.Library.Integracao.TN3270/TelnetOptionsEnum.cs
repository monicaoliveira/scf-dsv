﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    /// <summary>
    /// http://www.iana.org/assignments/telnet-options
    /// The Telnet Protocol has a number of options that may be negotiated.
    /// These options are listed here.  "Internet Official Protocol Standards"
    /// (STD 1) provides more detailed information.
    /// </summary>
    public enum TelnetOptionsEnum : byte
    {
        /// <summary>
        /// Binary Transmission
        /// [RFC856]
        /// </summary>
        BinaryTransmission = 0,

        /// <summary>
        /// Echo
        /// [RFC857]
        /// </summary>
        Echo = 1,

        /// <summary>
        /// Reconnection
        /// [NIC50005]
        /// </summary>
        Reconnection = 2,

        /// <summary>
        /// Suppress Go Ahead
        /// [RFC858]
        /// </summary>
        SuppressGoAhead = 3,

        /// <summary>
        /// Approx Message Size Negotiation
        /// [ETHERNET]
        /// </summary>
        ApproxMessageSizeNegotiation = 4,

        /// <summary>
        /// Status
        /// [RFC859]
        /// </summary>
        Status = 5,

        /// <summary>
        /// Timing Mark
        /// [RFC860]
        /// </summary>
        TimingMark = 6,

        /// <summary>
        /// Remote Controlled Trans and Echo
        /// [RFC726]
        /// </summary>
        RemoteControlledTransAndEcho = 7,

        /// <summary>
        /// Output Line Width
        /// [NIC50005]
        /// </summary>
        OutputLineWidth = 8,

        /// <summary>
        /// Output Page Size
        /// [NIC50005]
        /// </summary>
        OutputPageSize = 9,

        /// <summary>
        /// Output Carriage-Return Disposition
        /// [RFC652]
        /// </summary>
        OutputCarriageReturnDisposition = 10,

        /// <summary>
        /// Output Horizontal Tab Stops
        /// [RFC653]
        /// </summary>
        OutputHorizontalTabStops = 11,

        /// <summary>
        /// Output Horizontal Tab Disposition
        /// [RFC654]
        /// </summary>
        OutputHorizontalTabDisposition = 12,

        /// <summary>
        /// Output Formfeed Disposition
        /// [RFC655]
        /// </summary>
        OutputFormfeedDisposition = 13,

        /// <summary>
        /// Output Vertical Tabstops
        /// [RFC656]
        /// </summary>
        OutputVerticalTabstops = 14,

        /// <summary>
        /// Output Vertical Tab Disposition
        /// [RFC657]
        /// </summary>
        OutputVerticalTabDisposition = 15,

        /// <summary>
        /// Output Linefeed Disposition
        /// [RFC658]
        /// </summary>
        OutputLinefeedDisposition = 16,

        /// <summary>
        /// Extended ASCII
        /// [RFC698]
        /// </summary>
        ExtendedASCII = 17,

        /// <summary>
        /// Logout
        /// [RFC727]
        /// </summary>
        Logout = 18,

        /// <summary>
        /// Byte Macro
        /// [RFC735]
        /// </summary>
        ByteMacro = 19,

        /// <summary>
        /// Data Entry Terminal
        /// [RFC1043,RFC732]
        /// </summary>
        DataEntryTerminal = 20,

        /// <summary>
        /// SUPDUP
        /// [RFC736,RFC734]
        /// </summary>
        SUPDUP = 21,

        /// <summary>
        /// SUPDUP Output
        /// [RFC749]
        /// </summary>
        SUPDUPOutput = 22,

        /// <summary>
        /// Send Location
        /// [RFC779]
        /// </summary>
        SendLocation = 23,

        /// <summary>
        /// Terminal Type
        /// [RFC1091]
        /// </summary>
        TerminalType = 24,

        /// <summary>
        /// End of Record
        /// [RFC885]
        /// </summary>
        EndOfRecord = 25,

        /// <summary>
        /// TACACS User Identification
        /// [RFC927]
        /// </summary>
        TACACSUserIdentification = 26,

        /// <summary>
        /// Output Marking
        /// [RFC933]
        /// </summary>
        OutputMarking = 27,

        /// <summary>
        /// Terminal Location Number
        /// [RFC946]
        /// </summary>
        TerminalLocationNumber = 28,

        /// <summary>
        /// Telnet 3270 Regime
        /// [RFC1041]
        /// </summary>
        Telnet3270Regime = 29,

        /// <summary>
        /// X.3 PAD
        /// [RFC1053]
        /// </summary>
        X3PAD = 30,

        /// <summary>
        /// Negotiate About Window Size
        /// [RFC1073]
        /// </summary>
        NegotiateAboutWindowSize = 31,

        /// <summary>
        /// Terminal Speed
        /// [RFC1079]
        /// </summary>
        TerminalSpeed = 32,

        /// <summary>
        /// Remote Flow Control
        /// [RFC1372]
        /// </summary>
        RemoteFlowControl = 33,

        /// <summary>
        /// Linemode
        /// [RFC1184]
        /// </summary>
        Linemode = 34,

        /// <summary>
        /// X Display Location
        /// [RFC1096]
        /// </summary>
        XDisplayLocation = 35,

        /// <summary>
        /// Environment Option
        /// [RFC1408]
        /// </summary>
        EnvironmentOption = 36,

        /// <summary>
        /// Authentication Option
        /// [RFC2941]
        /// </summary>
        AuthenticationOption = 37,

        /// <summary>
        /// Encryption Option
        /// [RFC2946]
        /// </summary>
        EncryptionOption = 38,

        /// <summary>
        /// New Environment Option
        /// [RFC1572]
        /// </summary>
        NewEnvironmentOption = 39,

        /// <summary>
        /// TN3270E
        /// [RFC2355]
        /// </summary>
        TN3270E = 40,

        /// <summary>
        /// XAUTH
        /// [Earhart]
        /// </summary>
        XAUTH = 41,

        /// <summary>
        /// CHARSET
        /// [RFC2066]
        /// </summary>
        CHARSET = 42,

        /// <summary>
        /// Telnet Remote Serial Port (RSP)
        /// [Barnes]
        /// </summary>
        TelnetRemoteSerialPort = 43,

        /// <summary>
        /// Com Port Control Option
        /// [RFC2217]
        /// </summary>
        ComPortControlOption = 44,

        /// <summary>
        /// Telnet Suppress Local Echo
        /// [Atmar]
        /// </summary>
        TelnetSuppressLocalEcho = 45,

        /// <summary>
        /// Telnet Start TLS
        /// [Boe]
        /// </summary>
        TelnetStartTLS = 46,

        /// <summary>
        /// KERMIT
        /// [RFC2840]
        /// </summary>
        KERMIT = 47,

        /// <summary>
        /// SEND-URL
        /// [Croft]
        /// </summary>
        SENDURL = 48,

        /// <summary>
        /// FORWARD_X
        /// [Altman]
        /// </summary>
        FORWARD_X = 49,

        /// <summary>
        /// TELOPT PRAGMA LOGON
        /// [McGregory]
        /// </summary>
        TELOPT_PRAGMA_LOGON = 138,

        /// <summary>
        /// TELOPT SSPI LOGON
        /// [McGregory]
        /// </summary>
        TELOPT_SSPI_LOGON = 139,

        /// <summary>
        /// TELOPT PRAGMA HEARTBEAT
        /// [McGregory]
        /// </summary>
        TELOPT_PRAGMA_HEARTBEAT = 140,

        /// <summary>
        /// Extended-Options-List
        /// [RFC861]
        /// </summary>
        ExtendedOptionsList = 255
    }
}
