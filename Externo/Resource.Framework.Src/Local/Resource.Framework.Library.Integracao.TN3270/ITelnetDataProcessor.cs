﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Integracao.TN3270
{
    /// <summary>
    /// Interface para as classes de processamento de dados
    /// do telnet
    /// </summary>
    public interface ITelnetDataProcessor
    {
        void ProcessData(TelnetTokenData dataToken);
    }
}
