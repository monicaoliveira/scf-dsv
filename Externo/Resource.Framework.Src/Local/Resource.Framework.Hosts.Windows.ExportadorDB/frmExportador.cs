﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace Resource.Framework.Hosts.Windows.ExportadorDB
{
    public partial class frmExportador : Form
    {
        private OracleConnection _cn = null;

        public frmExportador()
        {
            InitializeComponent();
        }

        private void cmdTestarConexao_Click(object sender, EventArgs e)
        {
            try
            {
                OracleConnection cn = new OracleConnection(txtConnectionString.Text);
                cn.Open();
                MessageBox.Show("Conexão efetuada", "OK", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmdListarTabelas_Click(object sender, EventArgs e)
        {
            // Pede a lista de tabelas
            DataTable tb = getTable("select owner || '.' || table_name as table_name, num_rows from all_tables where num_rows > 0");
            DataView dv = new DataView(tb, null, "table_name", DataViewRowState.CurrentRows);

            // Informa arquivos exportados
            tb.Columns.Add("Exportado", typeof(bool));
            tb.Columns.Add("Erro", typeof(string));
            foreach (string arquivo in Directory.GetFiles(txtDiretorioTabelas.Text))
            {
                FileInfo fi = new FileInfo(arquivo);
                string[] nomeArquivo = fi.Name.Split('.');
                string nomeArquivo2 = string.Join(".", nomeArquivo, 0, 2);
                int i = dv.Find(nomeArquivo2);
                //DataRow[] drs = tb.Select("table_name = '" + fi.Name.Replace(".dat", "") + "'");
                if (i >= 0)
                    dv[i]["Exportado"] = true;
            }

            // Mostra no grid
            if (!chkNaoExportados.Checked)
            {
                grdTabelas.DataSource = tb;
            }
            else
            {
                DataView dv2 = new DataView(tb, "Exportado is null or Exportado=false", null, DataViewRowState.CurrentRows);
                grdTabelas.DataSource = dv2;
            }
        }

        private DataTable getTableSchema(string nomeTabela)
        {
            OracleCommand cm =
                new OracleCommand(
                    "select * from " + nomeTabela,
                    this.getConnection());
            OracleDataAdapter da =
                new OracleDataAdapter(cm);
            DataTable tb = new DataTable();
            da.FillSchema(tb, SchemaType.Source);
            return tb;
        }

        private DataTable getTable(string sql)
        {
            OracleCommand cm = 
                new OracleCommand(
                    sql,
                    this.getConnection());
            OracleDataAdapter da = 
                new OracleDataAdapter(cm);
            DataTable tb = new DataTable();
            da.Fill(tb);
            return tb;
        }

        private OracleConnection getConnection()
        {
            if (_cn == null)
            {
                _cn = new OracleConnection(txtConnectionString.Text);
                _cn.Open();

                OracleCommand cm = new OracleCommand("ALTER SESSION SET NLS_DATE_FORMAT = 'YY-MM-DD HH24:MI:SS'", _cn);
                cm.ExecuteNonQuery();
            }
            return _cn;
        }

        private void cmdExportarTabela_Click(object sender, EventArgs e)
        {
            // Pega linha selecionada
            foreach (DataGridViewRow dgvr in grdTabelas.SelectedRows)
            {
                // Pega o datarowview
                DataRowView drv = (DataRowView)dgvr.DataBoundItem;

                // Bloco de controle
                try
                {
                    // Faz a exportacao
                    exportarTabela(drv["TABLE_NAME"].ToString());

                    // Sinaliza item exportado
                    drv.Row["Exportado"] = true;
                    drv.Row["Erro"] = "";
                }
                catch (Exception ex)
                {
                    drv.Row["Erro"] = ex.ToString();
                }
            }

            // Informa
            MessageBox.Show("Tabela(s) exportada com sucesso");
        }

        private void exportarTabela(string tabela)
        {
            // Monta o nome da tabela
            string nomeTabela = tabela;

            string sql = "select * from " + nomeTabela;

            /*
            // Pede o schema da tabela
            DataTable tbSchema = getTableSchema(nomeTabela);

            // Faz select da tabela 
            string sql = "select ";
            foreach (DataColumn dc in tbSchema.Columns)
            {
                if (dc.DataType == typeof(DateTime))
                    sql += string.Format(@"   case 
                                    when extract(year from {0}) > 0 then
                                        to_date(
                                            extract(day from {0}) || '/' || 
                                            extract(month from {0}) || '/' || 
                                            extract(year from {0}), 'dd/mm/yy')
                                    else
                                        null
                                end {0},", dc.ColumnName);
                else
                    sql += dc.ColumnName + ",";
            }
            sql = sql.Substring(0, sql.Length - 1);
            sql += " from " + nomeTabela;
            */

            // Pede o datatable
            DataTable tb = getTable(sql);

            // Salva
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs =
                new FileStream(
                    txtDiretorioTabelas.Text + nomeTabela + ".dat",
                    FileMode.Create, FileAccess.Write);
            bf.Serialize(fs, tb);
            fs.Close();
        }

        private void exportarTabela2(string tabela)
        {
            // Monta o nome da tabela
            string nomeTabela = tabela;

            // Acha quantidade de registros
            string sqlCount = "select count(*) from " + nomeTabela;
            DataTable tbCount = getTable(sqlCount);
            long qtde = Convert.ToInt64(tbCount.Rows[0][0]);
            pgb.Minimum = 0;
            pgb.Maximum = Convert.ToInt32(qtde / 1000);
            pgb.Value = 0;

            // Pede o schema da tabela
            DataTable tbSchema = getTableSchema(tabela);

            // Abre o arquivo
            FileStream fs = 
                new FileStream(
                    txtDiretorioTabelas.Text + "temp." + tabela + ".csv", 
                    FileMode.Create, 
                    FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            
            // Bloco de controle
            try
            {

                // Escreve os titulos
                string titulos = "";
                foreach (DataColumn dc in tbSchema.Columns)
                    titulos += @"""" + dc.ColumnName.Replace(@"""", @"""""") + @""",";
                titulos = titulos.Substring(0, titulos.Length - 1);
                sw.WriteLine(titulos);

                // Faz o select
                string sql = "select * from " + nomeTabela;
                OracleCommand cm =
                    new OracleCommand(
                        sql,
                        this.getConnection());
                OracleDataReader dr = cm.ExecuteReader();
                long i = 1;
                int j = 0;
                while (dr.Read())
                {
                    StringBuilder linha = new StringBuilder("");
                    foreach (DataColumn dc in tbSchema.Columns)
                    {
                        try
                        {
                            linha.Append(@"""" + dr.GetValue(dc.Ordinal).ToString().Replace(@"""", @""""",") + @""",");
                        }
                        catch (Exception ex)
                        {
                            linha.Append(@""""",");
                        }
                    }
                    linha = linha.Remove(linha.Length - 1, 1);
                    sw.WriteLine(linha);
                    i++;
                    j++;
                    if (j > 1000)
                    {
                        Application.DoEvents();
                        pgb.Value = Convert.ToInt32(i / 1000);
                        j = 0;
                        lblQtde.Text = i.ToString();
                        Application.DoEvents();
                        Application.DoEvents();
                    }
                }
            }
            finally
            {
                // Fecha o stream e o arquivo
                sw.Close();
                fs.Close();
            }

            // 

            /*
            // Pede o schema da tabela
            DataTable tbSchema = getTableSchema(nomeTabela);

            // Faz select da tabela 
            string sql = "select ";
            foreach (DataColumn dc in tbSchema.Columns)
            {
                if (dc.DataType == typeof(DateTime))
                    sql += string.Format(@"   case 
                                    when extract(year from {0}) > 0 then
                                        to_date(
                                            extract(day from {0}) || '/' || 
                                            extract(month from {0}) || '/' || 
                                            extract(year from {0}), 'dd/mm/yy')
                                    else
                                        null
                                end {0},", dc.ColumnName);
                else
                    sql += dc.ColumnName + ",";
            }
            sql = sql.Substring(0, sql.Length - 1);
            sql += " from " + nomeTabela;
            */

        }

        private void cmdVerTabelaListar_Click(object sender, EventArgs e)
        {
            lstVerTabela.Items.Clear();
            foreach (string arquivo in Directory.GetFiles(txtDiretorioTabelas.Text))
                lstVerTabela.Items.Add(new FileInfo(arquivo).Name);
        }

        private void cmdVerTabelaAbrir_Click(object sender, EventArgs e)
        {
            FileStream fs = 
                new FileStream(
                    txtDiretorioTabelas.Text + (string)lstVerTabela.SelectedItem, 
                    FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            DataTable tb = (DataTable)bf.Deserialize(fs);
            fs.Close();
            grdVerTabela.DataSource = tb;
        }

        private void lstVerTabela_DoubleClick(object sender, EventArgs e)
        {
            cmdVerTabelaAbrir_Click(sender, e);
        }

        private void cmdExportar2_Click(object sender, EventArgs e)
        {
            // Pega linha selecionada
            foreach (DataGridViewRow dgvr in grdTabelas.SelectedRows)
            {
                // Pega o datarowview
                DataRowView drv = (DataRowView)dgvr.DataBoundItem;

                // Bloco de controle
                try
                {
                    // Faz a exportacao
                    exportarTabela2(drv["TABLE_NAME"].ToString());

                    // Sinaliza item exportado
                    drv.Row["Exportado"] = true;
                    drv.Row["Erro"] = "";
                }
                catch (Exception ex)
                {
                    drv.Row["Erro"] = ex.ToString();
                }
            }

            // Informa
            MessageBox.Show("Tabela(s) exportada com sucesso");
        }

        private string gerarDDL(string tipoObjeto, string nomeObjeto, string schema)
        {
            // Ajusta o tipoobjeto
            tipoObjeto = tipoObjeto.Replace("BODY", "").Trim();

            // Monta o command
            string sql = 
                string.Format(
                    @"begin bafisa.syp_teste(pTipoObjeto => :pTipoObjeto, pNomeObjeto => :pNomeObjeto, pSchema => :pSchema, resultado => :resultado); end;", 
                    tipoObjeto, nomeObjeto, schema);
            OracleCommand cm = new OracleCommand(sql, getConnection());
            cm.BindByName = true;
            cm.Parameters.Add(":pTipoObjeto", OracleDbType.Varchar2, tipoObjeto, ParameterDirection.Input);
            cm.Parameters.Add(":pNomeObjeto", OracleDbType.Varchar2, nomeObjeto, ParameterDirection.Input);
            cm.Parameters.Add(":pSchema", OracleDbType.Varchar2, schema, ParameterDirection.Input);
            cm.Parameters.Add(":resultado", OracleDbType.Clob, ParameterDirection.Output);

            // Executa
            cm.ExecuteNonQuery();

            // Pega o resultado
            return ((OracleClob)cm.Parameters[":resultado"].Value).Value;

        }

        private string gerarDDL2(string tipoObjeto, string nomeObjeto, string schema)
        {
            // Ajusta o tipoobjeto
            tipoObjeto = tipoObjeto.Replace("BODY", "").Trim();

            // Monta o command
            string sql =
                string.Format(
                    @"SELECT DBMS_METADATA.GET_DDL(:pTipoObjeto,:pNomeObjeto,:pSchema) FROM DUAL",
                    tipoObjeto, nomeObjeto, schema);
            OracleCommand cm = new OracleCommand(sql, getConnection());
            cm.BindByName = true;
            cm.Parameters.Add(":pTipoObjeto", OracleDbType.Varchar2, tipoObjeto, ParameterDirection.Input);
            cm.Parameters.Add(":pNomeObjeto", OracleDbType.Varchar2, nomeObjeto, ParameterDirection.Input);
            cm.Parameters.Add(":pSchema", OracleDbType.Varchar2, schema, ParameterDirection.Input);

            // Executa
            string retorno = (string)cm.ExecuteScalar();

            // Pega o resultado
            return retorno;

        }

        private void exportarObjeto(string tipoObjeto, string nomeObjeto, string schema)
        {
            // Pede o ddl
            string ddl = gerarDDL(tipoObjeto, nomeObjeto, schema);

            // Gera o arquivo
            string nomeArquivo = txtDiretorioObjetos.Text + schema + "." + tipoObjeto + "." + nomeObjeto + ".sql";
            //FileStream fs = 
            //    new FileStream(
            //        nomeArquivo, 
            //        FileMode.Create, FileAccess.Write);
            File.WriteAllText(nomeArquivo, ddl);
        }

        private void exportarObjeto2(string tipoObjeto, string nomeObjeto, string schema)
        {
            // Pede o ddl
            string ddl = gerarDDL2(tipoObjeto, nomeObjeto, schema);

            // Gera o arquivo
            string nomeArquivo = txtDiretorioObjetos.Text + schema + "." + tipoObjeto + "." + nomeObjeto + ".sql";
            //FileStream fs = 
            //    new FileStream(
            //        nomeArquivo, 
            //        FileMode.Create, FileAccess.Write);
            File.WriteAllText(nomeArquivo, ddl);
        }

        private void cmdListarObjetos_Click(object sender, EventArgs e)
        {
            // Pede a lista de tabelas
            DataTable tb = getTable("select owner || '.' || object_type || '.' || object_name nome_objeto, owner, object_name, object_type from all_objects");
            DataView dv = new DataView(tb, null, "nome_objeto", DataViewRowState.CurrentRows);

            // Informa arquivos exportados
            tb.Columns.Add("Exportado", typeof(bool));
            tb.Columns.Add("Erro", typeof(string));
            foreach (string arquivo in Directory.GetFiles(txtDiretorioObjetos.Text))
            {
                FileInfo fi = new FileInfo(arquivo);
                string[] nomeArquivo = fi.Name.Split('.');
                string nomeArquivo2 = string.Join(".", nomeArquivo, 0, 3);
                int i = dv.Find(nomeArquivo2);
                if (i >= 0)
                    dv[i]["Exportado"] = true;
            }

            // Mostra no grid
            if (!chkObjetosNaoExportados.Checked)
            {
                grdObjetos.DataSource = tb;
            }
            else
            {
                DataView dv2 = new DataView(tb, "Exportado is null or Exportado=false", null, DataViewRowState.CurrentRows);
                grdObjetos.DataSource = dv2;
            }
        }

        private void cmdExportarObjetos_Click(object sender, EventArgs e)
        {
            // Pega linha selecionada
            foreach (DataGridViewRow dgvr in grdObjetos.SelectedRows)
            {
                // Pega o datarowview
                DataRowView drv = (DataRowView)dgvr.DataBoundItem;

                // Bloco de controle
                try
                {
                    // Faz a exportacao
                    exportarObjeto(drv["OBJECT_TYPE"].ToString(), drv["OBJECT_NAME"].ToString(), drv["OWNER"].ToString());

                    // Sinaliza item exportado
                    drv.Row["Exportado"] = true;
                    drv.Row["Erro"] = "";
                }
                catch (Exception ex)
                {
                    drv.Row["Erro"] = ex.ToString();
                }
            }

            // Informa
            MessageBox.Show("Objeto(s) exportada com sucesso");
        }

        private void cmdExportarObjetos2_Click(object sender, EventArgs e)
        {
            // Pega linha selecionada
            foreach (DataGridViewRow dgvr in grdObjetos.SelectedRows)
            {
                // Pega o datarowview
                DataRowView drv = (DataRowView)dgvr.DataBoundItem;

                // Bloco de controle
                try
                {
                    // Faz a exportacao
                    exportarObjeto2(drv["OBJECT_TYPE"].ToString(), drv["OBJECT_NAME"].ToString(), drv["OWNER"].ToString());

                    // Sinaliza item exportado
                    drv.Row["Exportado"] = true;
                    drv.Row["Erro"] = "";
                }
                catch (Exception ex)
                {
                    drv.Row["Erro"] = ex.ToString();
                }
            }
        }
    }
}
