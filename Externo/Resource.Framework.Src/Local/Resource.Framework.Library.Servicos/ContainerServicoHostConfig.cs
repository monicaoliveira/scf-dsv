﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Configuracoes para o container de servico host
    /// </summary>
    public class ContainerServicoHostConfig
    {
        /// <summary>
        /// Configuracoes para o servico de host
        /// </summary>
        public ServicoHostConfig ServicoHostConfig { get; set; }
    }
}
