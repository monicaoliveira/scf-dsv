﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Armazena e gerencia um servico host.
    /// Implementado como singleton
    /// </summary>
    public class ContainerServicoHost
    {

        /// <summary>
        /// Instancia singleton
        /// </summary>
        private static ContainerServicoHost _instance = null;

        /// <summary>
        /// Construtor privado. Instancia nao pode ser criada diretamente
        /// </summary>
        private ContainerServicoHost()
        {
        }

        /// <summary>
        /// Retorna instancia do host
        /// </summary>
        /// <returns></returns>
        public static ContainerServicoHost GetInstance()
        {
            if (_instance == null)
                _instance = new ContainerServicoHost();
            return _instance;
        }

        /// <summary>
        /// Referencia para o servico
        /// </summary>
        public IServicoHost ServicoHost { get; set; }

        /// <summary>
        /// Inicia o host com as configuracoes do arquivo
        /// </summary>
        public void Iniciar()
        {
            // Le config do arquivo
            ContainerServicoHostConfig config = GerenciadorConfig.ReceberConfig<ContainerServicoHostConfig>();

            // Repassa a chamada
            Iniciar(config);
        }

        /// <summary>
        /// Inicia o host com as configuracoes informadas
        /// </summary>
        /// <param name="config"></param>
        public void Iniciar(ContainerServicoHostConfig config)
        {
            // Cria a instancia do serviço
            this.ServicoHost = new ServicoHost();
            this.ServicoHost.IniciarHost(
                new IniciarHostRequest() 
                { 
                    Config = config.ServicoHostConfig
                });

            // Deve iniciar wcf?

        }

        /// <summary>
        /// Finaliza servico host
        /// </summary>
        public void Finalizar()
        {
            // Finaliza servico
            this.ServicoHost.FinalizarHost(new FinalizarHostRequest());
            this.ServicoHost = null;
        }

    }
}
