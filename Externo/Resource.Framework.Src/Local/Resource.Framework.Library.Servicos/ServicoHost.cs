﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Library.Servicos.Mensagens;
using System.Reflection;
using Resource.Framework.Library.Servicos.Dados;
using System.ServiceModel;
using System.Diagnostics;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Implementação do servico host
    /// Está classe pode ter dois propósitos: Host e Proxy de um Host
    /// Quando é inicializada (IniciarHost) é tratada como um host. Se não
    /// for inicializada, é tratada como um proxy para o host default do processo
    /// </summary>

    // Marcos Matsuoka - Jira SCF 1049
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class ServicoHost : IServicoHost 
    {
        // Create a logger for use in this class
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Coleção dos serviços ativos
        /// </summary>
        private Dictionary<Type, ServicoHelper> _servicos = null;

        /// <summary>
        /// Host WCF, caso este serviço seja exposto via WCF
        /// </summary>
        private ServiceHost _serviceHost = null;

        /// <summary>
        /// Dicionario relacionando mensagens de request com o método para processamento
        /// Indica o tipo da mensagem e mantem a referencia para o método da interface do servico
        /// </summary>
        private Dictionary<Type, OperacaoHelper> _operacoes = null;

        /// <summary>
        /// Instância dos hooks que executam antes da mensagem ser processada
        /// </summary>
        private List<IServicoHostHookProcessarMensagem> _hooksProcessarMensagemAntesExecutar = new List<IServicoHostHookProcessarMensagem>();

        /// <summary>
        /// Instância dos hooks que executam depois da mensagem ser processada
        /// </summary>
        private List<IServicoHostHookProcessarMensagem> _hooksProcessarMensagemDepoisExecutar = new List<IServicoHostHookProcessarMensagem>();

        /// <summary>
        /// Instância dos hooks que executam quando ocorre uma excecao no processamento da mensagem
        /// </summary>
        private List<IServicoHostHookProcessarMensagem> _hooksProcessarMensagemExcecao = new List<IServicoHostHookProcessarMensagem>();

        /// <summary>
        /// Indica se tem a função de host (true) ou proxy (false)
        /// </summary>
        private bool _ehHost = false;
        
        #region IServicoHost Members

        /// <summary>
        /// Inicia o host
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IniciarHostResponse IniciarHost(IniciarHostRequest request)
        {
            // Prepara resposta
            IniciarHostResponse resposta = new IniciarHostResponse();

            // Bloco de controle
            try
            {
                // Log
                //log.Info(request);

                // Caso tenha algum servico, pede finalizacao
                if (_servicos != null)
                    FinalizarHost(new FinalizarHostRequest());

                // Inicializa
                _operacoes = new Dictionary<Type, OperacaoHelper>();
                _servicos = new Dictionary<Type, ServicoHelper>();

                // Pega o config
                ServicoHostConfig config = request.Config;
                if (config == null)
                    config = GerenciadorConfig.ReceberConfig<ServicoHostConfig>();

                // Garante que os assemblies necessarios estão carregados
                List<string> assembliesCarregados =
                    AppDomain.CurrentDomain.GetAssemblies().Select(
                        a => a.FullName.Replace(" ", "")).ToList();
                if (config.CarregarAssemblies != null)
                    foreach (string assemblyStr in config.CarregarAssemblies)
                        if (!assembliesCarregados.Contains(assemblyStr.Replace(" ", "")))
                            AppDomain.CurrentDomain.Load(assemblyStr);
                if (config.IncluirAssembliesString != null)
                    foreach (string assemblyStr in config.IncluirAssembliesString)
                        if (!assembliesCarregados.Contains(assemblyStr.Replace(" ", "")))
                            AppDomain.CurrentDomain.Load(assemblyStr);
                foreach (string tipoStr in config.IncluirTiposString)
                {
                    // Pega o nome do assembly
                    string assemblyStr = tipoStr.Split(',')[1];

                    // Faz a verificacao
                    if (!assembliesCarregados.Contains(assemblyStr.Replace(" ", "")))
                        AppDomain.CurrentDomain.Load(assemblyStr);
                }

                // Classes para execução antes da execução da mensagem
                _hooksProcessarMensagemAntesExecutar = 
                    config.HooksProcessarMensagemAntesExecucao;

                // Classes para execução depois da execução da mensagem
                if (config.HooksProcessarMensagemDepoisExecucao != null)
                    _hooksProcessarMensagemDepoisExecutar =
                        config.HooksProcessarMensagemDepoisExecucao.Select(
                            t =>
                                (IServicoHostHookProcessarMensagem)
                                    Activator.CreateInstance(t)).ToList();

                // Classes para processamento de excecao na execucao da mensagem
                if (config.HooksProcessarMensagemException != null)
                    _hooksProcessarMensagemExcecao =
                        config.HooksProcessarMensagemException.Select(
                            t =>
                                (IServicoHostHookProcessarMensagem)
                                    Activator.CreateInstance(t)).ToList();

                // Lista de tipos a serem processados
                List<Type> tiposConhecidos = new List<Type>();

                // Adiciona a lista de tipos informados
                tiposConhecidos.AddRange(config.IncluirTipos);

                // Varre assemblies adicionando os tipos candidatos
                foreach (Assembly assembly in config.IncluirAssemblies)
                {
                    // Varre os tipos
                    foreach (Type tipo in assembly.GetTypes())
                    {
                        // Pega atributo
                        ServicoHostAttribute servicoHostAttribute =
                            (ServicoHostAttribute)
                                tipo.GetCustomAttributes(typeof(ServicoHostAttribute), true).FirstOrDefault();

                        // Verifica se deve incluir
                        if ((servicoHostAttribute == null && config.TipoVarreduraTipos == ServicoHostTipoVarreduraEnum.DefaultIncluir) ||
                            (servicoHostAttribute != null && servicoHostAttribute.Incluir))
                        {
                            // Inclui
                            tiposConhecidos.Add(tipo);
                        }
                    }
                }

                // Varre os tipos informados adicionando como candidatos
                foreach (Type tipo in tiposConhecidos)
                {
                    // Verifica se é uma classe (não pode ser interface ou outras coisas)
                    if (tipo.IsClass)
                    {
                        // Pega eventual atributo de servicoHost
                        ServicoHostAttribute servicoHostAttribute =
                            (ServicoHostAttribute)
                                tipo.GetCustomAttributes(
                                    typeof(ServicoHostAttribute), true).FirstOrDefault();

                        // Verifica se tem interface de servicos
                        Type tipoInterface =
                            tipo.GetInterfaces().ToList().Find(
                                t => t.GetCustomAttributes(typeof(InterfaceServicoAttribute), true).Length > 0);

                        // Se nao achou, tenta achar uma interface que comece com I + nome servico
                        if (tipoInterface == null)
                            tipoInterface =
                                tipo.GetInterfaces().ToList().Find(
                                    t => t.Name == "I" + tipo.Name);

                        // Achou?
                        if (tipoInterface != null)
                        {
                            // Pega o atributo da interface
                            InterfaceServicoAttribute servicoAttributeInterface =
                                (InterfaceServicoAttribute)
                                    tipoInterface.GetCustomAttributes(
                                        typeof(InterfaceServicoAttribute), true).FirstOrDefault();

                            // Se nao achou, considera default
                            if (servicoAttributeInterface == null)
                                servicoAttributeInterface = new InterfaceServicoAttribute();

                            // Deve incluir?
                            if (servicoAttributeInterface.Incluir)
                            {
                                // Cria lista de métodos, incluindo interfaces filhas
                                List<MethodInfo> metodos =
                                    new List<MethodInfo>(tipoInterface.GetMethods());
                                foreach (var item in tipoInterface.GetInterfaces().Select(t => t.GetMethods()))
                                    metodos.AddRange(item);

                                // Varre a lista de métodos da interface
                                foreach (MethodInfo methodInfo in metodos)
                                {
                                    // Verifica se o método possui a assinatura adequada:
                                    // Se tem 1 parametro e é do tipo MensagemRequestBase
                                    ParameterInfo[] parameters = methodInfo.GetParameters();
                                    if (parameters.Length == 1)
                                    {
                                        // Pega tipo do primeiro parametro e verifica se é aceitavel
                                        Type tipoParametro = parameters[0].ParameterType;
                                        if (tipoParametro.IsSubclassOf(typeof(MensagemRequestBase)))
                                        {
                                            // O método é um candidato, verifica se tem marcacao
                                            InterfaceServicoAttribute servicoAttributeMetodo =
                                                (InterfaceServicoAttribute)
                                                    methodInfo.GetCustomAttributes(
                                                        typeof(InterfaceServicoAttribute), true).FirstOrDefault();

                                            // Se o tipo do parametro for generico, pega a definicao generica
                                            if (tipoParametro.IsGenericType)
                                                tipoParametro = tipoParametro.GetGenericTypeDefinition();

                                            // Deve considerar este método?
                                            if ((servicoAttributeMetodo == null && config.TipoVarreduraOperacoes == ServicoHostTipoVarreduraEnum.DefaultIncluir) ||
                                                (servicoAttributeMetodo != null && servicoAttributeMetodo.Incluir))
                                            {
                                                // Insere, ou, se já existe é um erro
                                                if (!_operacoes.ContainsKey(tipoParametro))
                                                    _operacoes.Add(
                                                        tipoParametro,
                                                        new OperacaoHelper()
                                                        {
                                                            MethodInfo = methodInfo,
                                                            Interface = tipoInterface
                                                        });
                                                else
                                                    throw new Exception(
                                                        string.Format(
                                                            "Mais de um método de serviço ({0}, {1}) processam o mesmo tipo de mensagem ({2}). O serviço de mensageria não consegue rotear esse tipo de mensagem. Ajuste as configurações para não ocorrer essa situação.",
                                                            _operacoes[tipoParametro].MethodInfo.ReflectedType.Name + "." + _operacoes[tipoParametro].MethodInfo.Name,
                                                            methodInfo.ReflectedType.Name + "." + methodInfo.Name,
                                                            tipoParametro.Name));

                                                // Verifica se o helper de servico esta criado
                                                if (!_servicos.ContainsKey(tipoInterface))
                                                {
                                                    // Cria o helper para o servico
                                                    _servicos.Add(
                                                        tipoInterface,
                                                        new ServicoHelper()
                                                        {
                                                            Instancia = null,
                                                            ServicoInfo = servicoHostAttribute != null ? servicoHostAttribute.ServicoInfo : new ServicoInfo(),
                                                            TipoInterface = tipoInterface,
                                                            TipoServico = tipo
                                                        });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Inicia os servicos que devem ser iniciados agora
                foreach (ServicoHelper servicoHelper in _servicos.Values)
                {
                    // Inicia as instancias que devem ser iniciadas agora
                    if (servicoHelper.ServicoInfo.MomentoAtivacao == ServicoMomentoAtivacaoEnum.NoInicioDoHost)
                        servicoHelper.Instancia = receberInstancia(servicoHelper.TipoServico);
                }

                // Verifica se deve expor o servico no wcf
                if (config.ExporWCF)
                {
                    // Faz a ativação WCF
                    _serviceHost = new ServiceHost(typeof(ServicoHost));
                    _serviceHost.OpenTimeout = new TimeSpan(0, 2, 0);
                    _serviceHost.Open();
                }

                // Sinaliza 
                _ehHost = true;
            }
            catch (Exception ex)
            {
                // Log
                //log.Error(request, ex);

                // Repassa erro
                throw new Exception("Erro ao iniciar host", ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Finaliza o host
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public FinalizarHostResponse FinalizarHost(FinalizarHostRequest request)
        {
            // Log
            //log.Info(request);

            // Bloco de controle
            try
            {

                // Se o servico esta exposto como wcf, finaliza
                if (_serviceHost != null)
                {
                    // Finaliza
                    _serviceHost.Close();
                    _serviceHost = null;
                }

                // Para os servicos que possuem interface de controle de estado, sinaliza finalizar
                foreach (KeyValuePair<Type, ServicoHelper> item in _servicos)
                    if (item.Value.Instancia != null && item.Value.Instancia is IServicoControlavel)
                        ((IServicoControlavel)item.Value.Instancia).PararServico();

                // Libera as referencias de servicos
                _servicos = null;

                // Libera as referencias de operacoes
                _operacoes = null;
            }
            catch (Exception ex)
            {
                // Log 
                //log.Error(request, ex);

                // Repassa
                throw new Exception("Erro ao finalizar host", ex);
            }

            // Retorna
            return new FinalizarHostResponse();
        }

        /// <summary>
        /// Processa a mensagem
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public MensagemResponseBase ProcessarMensagem(MensagemRequestBase request)
        {
            // Se for proxy, repassa a mensagem
            if (!_ehHost)
            {
                // Repassa a mensagem
                return 
                    ContainerServicoHost.GetInstance().ServicoHost.ProcessarMensagem(
                        request);
            }

            // Log
            //log.Info(request);

            // Inicializa
            MethodInfo methodInfo = null;

            // Prepara resposta
            MensagemResponseBase resposta = null;
            
            // Bloco de controle
            try
            {
                // Inicializa
                Type tipoMensagemOriginal = null;
                Type tipoMensagem = request.GetType();

                // Tenta achar a operação para o tipo informado ou para os tipos base
                while (methodInfo == null && tipoMensagem != null)
                {
                    // Trata tipos genericos
                    if (tipoMensagem.IsGenericType)
                    {
                        tipoMensagemOriginal = tipoMensagem;
                        tipoMensagem = tipoMensagem.GetGenericTypeDefinition();
                    }

                    // Verifica se conhece o tipo
                    if (_operacoes.ContainsKey(tipoMensagem))
                        methodInfo = _operacoes[tipoMensagem].MethodInfo;
                    else
                        tipoMensagem = tipoMensagem.BaseType;
                }

                // Acha a operacao que ira processar a mensagem
                if (methodInfo != null)
                {
                    // Se for um método generico, resolve o tipo
                    if (methodInfo.IsGenericMethodDefinition)
                        methodInfo = methodInfo.MakeGenericMethod(tipoMensagemOriginal.GetGenericArguments());

                    // Pega referencia para o metodo e para a instancia
                    object instancia = receberInstancia(_operacoes[tipoMensagem].Interface);

                    //// Processa os hooks de antes da execucao da mensagem
                    //if (_hooksProcessarMensagemAntesExecutar != null)
                    //    _hooksProcessarMensagemAntesExecutar.ForEach(
                    //        i => i.Processar(methodInfo, request, null, null));
                    
                    // Executa
                    resposta =
                        (MensagemResponseBase)
                            methodInfo.Invoke(
                                instancia,
                                new object[] { request });
                
                    //// Processa os hooks de depois da execucao da mensagem
                    //if (_hooksProcessarMensagemDepoisExecutar != null)
                    //    _hooksProcessarMensagemDepoisExecutar.ForEach(
                    //        i => i.Processar(methodInfo, request, resposta, null));
                }
                else
                {
                    // Excecao
                    throw new Exception("ServicoHost recebeu solicitação para processar tipo de mensagem não conhecido.");
                }
            }
            catch (Exception ex)
            {
                //// Acha a profundidade das excessoes
                //int profundidade = 0;
                //Exception ex1 = ex.InnerException;
                //while (ex1 != null)
                //{
                //    profundidade++;
                //    ex1 = ex1.InnerException;
                //}

                //// Para evitar recursividade, apenas faz a rotina abaixo se a profundidade nao maior que 5
                //if (profundidade < 5)
                //{
                //    // Se for TargetInvocationException, pega a excecao de dentro
                //    while (ex is TargetInvocationException)
                //        ex = ex.InnerException;

                //    // Processa os hooks de excecao
                //    if (_hooksProcessarMensagemExcecao != null && false)
                //        foreach (var hook in _hooksProcessarMensagemExcecao)
                //            hook.Processar(methodInfo, request, resposta, ex);

                //    // Log 
                //    //log.Error(request, ex);
                //}

                // Repassa
                throw ex;
            }

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Retorna a lista de operações
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarOperacaoResponse ListarOperacoes(ListarOperacaoRequest request)
        {
            // Se for proxy, repassa a mensagem
            if (!_ehHost)
            {
                // Repassa a mensagem
                return
                    ContainerServicoHost.GetInstance().ServicoHost.ListarOperacoes(
                        request);
            }

            // Prepara resposta
            ListarOperacaoResponse resposta =
                new ListarOperacaoResponse()
                {
                    CodigoMensagemRequest = request.CodigoMensagem
                };

            // Monta lista de operacoes suportadas
            foreach (KeyValuePair<Type, OperacaoHelper> item in _operacoes)
            {
                // Pega informacoes
                MethodInfo methodInfo = item.Value.MethodInfo;
                ParameterInfo parameterInfo = methodInfo.GetParameters()[0];
                ServicoHelper servicoHelper = _servicos[item.Value.Interface];

                // Adiciona novo item na colecao. 
                resposta.Operacoes.Add(
                    new OperacaoInfo()
                    {
                        NomeOperacao = methodInfo.Name,
                        TipoInterface = servicoHelper.TipoInterface.FullName + "," + servicoHelper.TipoInterface.Assembly.FullName,
                        TipoMensagemRequest = 
                            parameterInfo.ParameterType.IsGenericType ? 
                            parameterInfo.ParameterType.GetGenericTypeDefinition().FullName + "," + parameterInfo.ParameterType.Assembly.FullName :
                            parameterInfo.ParameterType.FullName + "," + parameterInfo.ParameterType.Assembly.FullName,
                        TipoMensagemResponse = 
                            methodInfo.ReturnType.IsGenericType ?
                            methodInfo.ReturnType.GetGenericTypeDefinition().FullName + "," + methodInfo.ReturnType.Assembly.FullName :
                            methodInfo.ReturnType.FullName + "," + methodInfo.ReturnType.Assembly.FullName,
                        TipoServico = servicoHelper.TipoServico.FullName + "," + servicoHelper.TipoServico.Assembly.FullName
                    });
            }

            // Retorna
            return resposta;
        }

        #endregion

        /// <summary>
        /// Retorna a instancia do tipo solicitado.
        /// </summary>
        /// <returns></returns>
        private object receberInstancia(Type tipoInterface)
        {
            // Pega referencia para o metodo e informacoes do servico
            ServicoHelper servicoHelper = _servicos[tipoInterface];
            object instancia = null;

            // Resolve a instancia
            instancia = servicoHelper.Instancia;
            if (instancia == null)
            {
                // Cria
                instancia = Activator.CreateInstance(servicoHelper.TipoServico);

                // Pede inicializacao se necessario
                if (instancia is IServicoControlavel)
                    ((IServicoControlavel)instancia).IniciarServico();

                // Deve manter a referencia?
                if (servicoHelper.ServicoInfo.TipoAtivacao == ServicoTipoAtivacaoEnum.ManterEstado)
                    servicoHelper.Instancia = instancia;
            }

            // Retorna
            return instancia;
        }
    }
}
