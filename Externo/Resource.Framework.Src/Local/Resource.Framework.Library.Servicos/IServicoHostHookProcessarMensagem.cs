﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Reflection;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Interface para implementação de validação de segurança para a mensageria
    /// </summary>
    public interface IServicoHostHookProcessarMensagem
    {
        void Processar(MethodInfo methodInfo, MensagemRequestBase request, MensagemResponseBase response, Exception exception);
    }
}
