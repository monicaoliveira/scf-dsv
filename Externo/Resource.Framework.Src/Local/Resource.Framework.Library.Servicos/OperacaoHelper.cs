﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Auxiliar para armazenamento de operacoes
    /// </summary>
    public class OperacaoHelper
    {
        /// <summary>
        /// Informacoes do método
        /// </summary>
        public MethodInfo MethodInfo { get; set; }

        /// <summary>
        /// Indica a interface considerada
        /// </summary>
        public Type Interface { get; set; }
    }
}
