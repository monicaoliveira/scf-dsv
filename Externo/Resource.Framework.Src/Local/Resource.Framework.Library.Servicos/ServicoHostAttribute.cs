﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Atributo para configurar visibilidade e comportamento de serviços e operações
    /// </summary>
    public class ServicoHostAttribute : Attribute
    {
        /// <summary>
        /// Indica se deve incluir na varredura
        /// </summary>
        public bool Incluir { get; set; }

        /// <summary>
        /// Se for servico, contem informacoes do servido
        /// </summary>
        public ServicoInfo ServicoInfo { get; set; }
    }
}
