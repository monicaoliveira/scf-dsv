﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Dados
{
    /// <summary>
    /// Momentos de ativacao de servicos
    /// </summary>
    public enum ServicoMomentoAtivacaoEnum
    {
        NoInicioDoHost,
        PrimeiraAcesso
    }
}
