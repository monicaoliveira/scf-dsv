﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Dados
{
    /// <summary>
    /// Contem informacoes sobre um servico
    /// </summary>
    public class ServicoInfo
    {
        /// <summary>
        /// Indica se o servico é stateless ou stateful
        /// </summary>
        public ServicoTipoAtivacaoEnum TipoAtivacao { get; set; }

        /// <summary>
        /// Indica o momento ativacao, se no start do host ou 
        /// sob demanda
        /// </summary>
        public ServicoMomentoAtivacaoEnum MomentoAtivacao { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoInfo()
        {
            this.TipoAtivacao = ServicoTipoAtivacaoEnum.ManterEstado;
            this.MomentoAtivacao = ServicoMomentoAtivacaoEnum.PrimeiraAcesso;
        }
    }
}
