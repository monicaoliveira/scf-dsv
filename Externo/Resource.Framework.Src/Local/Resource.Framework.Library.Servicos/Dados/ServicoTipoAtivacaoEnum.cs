﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Dados
{
    /// <summary>
    /// Tipos de ativacao de servicos
    /// </summary>
    public enum ServicoTipoAtivacaoEnum
    {
        ManterEstado,
        NaoManterEstado
    }
}
