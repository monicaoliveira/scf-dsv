﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library;
using System.IO;
using log4net;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Classe auxiliar de log
    /// </summary>
    public static class LogArquivoHelper
    {
        /// <summary>
        /// Salva o log
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static void LogarPassagem(string codigoSessao, string servico, string entidade, string etapa, TimeSpan duracao)
        {
            LogPassagemConfig config = new LogPassagemConfig();

            if (config.LogarPassagem)
            {
                log4net.Config.XmlConfigurator.Configure();
                ILog loggerPassagens = LogManager.GetLogger("LogPassagemAppender");
                loggerPassagens.Warn(", " + servico + ", " + entidade + ", " + etapa + (duracao != null ? ("," + duracao.ToString()) : null) );
                //logPassagens.Info(", SERVICO: " + servico + ", ENTIDADE: " + entidade + ", ETAPA: " + etapa);
            }
        } 
    }
}
