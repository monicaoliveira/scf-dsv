﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de inicio do host
    /// </summary>
    public class IniciarHostRequest : MensagemRequestBase
    {
        /// <summary>
        /// Configurações para o servico de host
        /// Caso não seja passado, o host irá utilizar informações do arquivo de config
        /// </summary>
        public ServicoHostConfig Config { get; set; }
    }
}
