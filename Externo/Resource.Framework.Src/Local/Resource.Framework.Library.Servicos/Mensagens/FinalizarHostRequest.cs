﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de finalização do host
    /// </summary>
    public class FinalizarHostRequest : MensagemRequestBase
    {
    }
}
