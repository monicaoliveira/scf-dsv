﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de iniciar host
    /// </summary>
    public class IniciarHostResponse : MensagemResponseBase
    {
    }
}
