﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    /// <summary>
    /// Mensagem de response de lista operações
    /// </summary>
    public class ListarOperacaoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de operacoes suportadas
        /// </summary>
        public List<OperacaoInfo> Operacoes { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarOperacaoResponse()
        {
            this.Operacoes = new List<OperacaoInfo>();
        }
    }
}
