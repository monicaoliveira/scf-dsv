﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Configuration;
using System.ServiceModel.Channels;
using System.ServiceModel;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library.Servicos.Dados;
using System.Diagnostics;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Contém informações de cache de sevico host
    /// </summary>
    public class MensageriaHostHelper
    {
        /// <summary>
        /// Nome deste host
        /// </summary>
        public string NomeHost { get; set; }

        /// <summary>
        /// Elemento de configuração do cliente WCF
        /// </summary>
        public ChannelEndpointElement ChannelEndpointElement { get; set; }

        /// <summary>
        /// Instância do proxy para o ServicoHost
        /// </summary>
        public IServicoHost ServicoHost { get; set; }

        /// <summary>
        /// Mensagens que este host processa
        /// </summary>
        public List<Type> Mensagens { get; set; }

        /// <summary>
        /// Indica qual erro ocorreu no momento da conexao
        /// </summary>
        public Exception ErroConexao { get; set; }

        /// <summary>
        /// Channel WCF para conexão com o ServicoHost
        /// </summary>
        public IChannelFactory<IServicoHost> ChannelFactory { get; set; }

        /// <summary>
        /// Construtor que recebe um servico diretamente
        /// </summary>
        public MensageriaHostHelper(IServicoHost servicoHost)
        {
            // Inicializa
            this.Mensagens = new List<Type>();

            // Guarda 
            this.ServicoHost = servicoHost;

            // Define o nome
            this.NomeHost = "(local)";

            // Pede lista de mensagens
            this.AtualizarListaMensagens();
        }

        /// <summary>
        /// Construtor que recebe um endpoint WCF
        /// </summary>
        public MensageriaHostHelper(ChannelEndpointElement channelEndpointElement)
        {
            // Inicializa
            this.Mensagens = new List<Type>();

            // Guarda
            this.ChannelEndpointElement = channelEndpointElement;

            // Propriedades
            this.NomeHost = channelEndpointElement.Name;

            // Tenta conectar
            this.Conectar();
        }

        /// <summary>
        /// Tenta fazer a conexao com o host
        /// </summary>
        public bool Conectar()
        {
            return this.Conectar(true);
        }

        /// <summary>
        /// Tenta fazer a conexao com o host
        /// </summary>
        public bool Conectar(bool atualizarListaMensagens)
        {
            // Bloco de controle
            try
            {
                // Cria o proxy
                this.ChannelFactory = new ChannelFactory<IServicoHost>(this.ChannelEndpointElement.Name);
                this.ServicoHost =
                    this.ChannelFactory.CreateChannel(
                        new EndpointAddress(
                            this.ChannelEndpointElement.Address));

                // Pede a lista de mensagens
                if (atualizarListaMensagens)
                    this.AtualizarListaMensagens();
            }
            catch (Exception ex)
            {
                // Sinaliza
                this.ErroConexao = ex;
                
                // Retorna
                return false;
            }

            // Sinaliza ok
            return true;
        }

        /// <summary>
        /// Atualiza a mensageria com a lista de mensagens que o host processa
        /// </summary>
        public void AtualizarListaMensagens()
        {
            // Remove operações anteriores associadas a este host
            IEnumerable<Type> remover =
                from i in Mensageria.CacheTipos
                where i.Value == this.NomeHost
                select i.Key;
            foreach (Type t in remover)
                Mensageria.CacheTipos.Remove(t);

            // Pede a lista de operações
            ListarOperacaoResponse resposta = 
                this.ServicoHost.ListarOperacoes(new ListarOperacaoRequest());

            // Atualiza a mensageria com a lista de operações processada por este host
            foreach (OperacaoInfo operacaoInfo in resposta.Operacoes)
            {
                // Cria o tipo
                Type tipoMensagemRequest = Type.GetType(operacaoInfo.TipoMensagemRequest);

                // Se conseguiu, adiciona na coleção
                if (tipoMensagemRequest != null)
                {
                    // Verifica se consta na coleção da mensageria
                    if (Mensageria.CacheTipos.ContainsKey(tipoMensagemRequest))
                        Mensageria.CacheTipos[tipoMensagemRequest] = this.NomeHost;
                    else
                        Mensageria.CacheTipos.Add(tipoMensagemRequest, this.NomeHost);
                }
            }
        }

        /// <summary>
        /// Repassa a mensagem para o host, tratando possíveis erros de conexão
        /// </summary>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public MensagemResponseBase ProcessarMensagem(MensagemRequestBase request)
        {
            // Bloco de controle
            try
            {
                // Se usa WCF, verifica se o canal está aberto
                if (this.ChannelFactory != null && (
                        this.ChannelFactory.State == CommunicationState.Closed || 
                        this.ChannelFactory.State == CommunicationState.Faulted))
                    this.Conectar(false);

                // Repassa a mensagem
                return this.ServicoHost.ProcessarMensagem(request);
            }
            catch (Exception ex)
            {
                // Fecha a conexão
                if (this.ChannelFactory != null)
                    this.ChannelFactory.Abort();

                // Repassa a chamada
                throw ex;
            }
        }
    }
}
