﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Classe de configuracoes da mensageria
    /// </summary>
    public class MensageriaConfig
    {
        /// <summary>
        /// Indica se a mensageria deverá consultar
        /// outros hosts que estão na configuração de 
        /// clientes WCF
        /// </summary>
        public bool UtilizarWCF { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public MensageriaConfig()
        {
            this.UtilizarWCF = true;
        }
    }
}
