﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Helper para servico.
    /// Permite armazenar tanto o info do servico como uma possivel instancia
    /// </summary>
    public class ServicoHelper
    {
        /// <summary>
        /// Informacoes do servico
        /// </summary>
        public ServicoInfo ServicoInfo { get; set; }

        /// <summary>
        /// Instancia do servico
        /// </summary>
        public object Instancia { get; set; }

        /// <summary>
        /// Tipo da interface
        /// </summary>
        public Type TipoInterface { get; set; }

        /// <summary>
        /// Tipo do servico
        /// </summary>
        public Type TipoServico { get; set; }
    }
}
