﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Xml.Serialization;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Configuracoes do servico de host
    /// </summary>
    public class ServicoHostConfig
    {
        /// <summary>
        /// Hooks para processamento de exceções
        /// </summary>
        [XmlIgnore]
        public List<Type> HooksProcessarMensagemException
        {
            get
            {
                return
                    this.HooksProcessarMensagemExceptionString != null ?
                        this.HooksProcessarMensagemExceptionString.Select(s => Type.GetType(s)).ToList() :
                        new List<Type>();
            }
            set { this.HooksProcessarMensagemExceptionString = value.Select(t => t.FullName + "," + t.Assembly.FullName).ToList(); }
        }

        /// <summary>
        /// Hooks para processamento de exceções - string
        /// </summary>
        public List<string> HooksProcessarMensagemExceptionString { get; set; }

        /// <summary>
        /// Hooks para processamento antes da execucao da mensagem
        /// </summary>
        public List<IServicoHostHookProcessarMensagem> HooksProcessarMensagemAntesExecucao { get; set; }

        /// <summary>
        /// Hooks para processamento depois da execucao da mensagem
        /// </summary>
        [XmlIgnore]
        public List<Type> HooksProcessarMensagemDepoisExecucao
        {
            get
            {
                return
                    this.HooksProcessarMensagemDepoisExecucaoString != null ?
                        this.HooksProcessarMensagemDepoisExecucaoString.Select(s => Type.GetType(s)).ToList() :
                        new List<Type>();
            }
            set { this.HooksProcessarMensagemDepoisExecucaoString = value.Select(t => t.FullName + "," + t.Assembly.FullName).ToList(); }
        }

        /// <summary>
        /// Hooks para processamento depois da execucao da mensagem - string
        /// </summary>
        public List<string> HooksProcessarMensagemDepoisExecucaoString { get; set; }

        /// <summary>
        /// Indica assemblies que devem ser carregados
        /// </summary>
        public List<string> CarregarAssemblies { get; set; }

        /// <summary>
        /// Indica assemblies que devem ser varridos
        /// Propriedade de auxilio
        /// </summary>
        [XmlIgnore]
        public List<Assembly> IncluirAssemblies 
        {
            get 
            { 
                return 
                    this.IncluirAssembliesString != null ? 
                        this.IncluirAssembliesString.Select(s => Assembly.Load(s)).ToList() :
                        new List<Assembly>(); 
            }
            set { this.IncluirAssembliesString = value != null ? value.Select(a => a.FullName).ToList() : null; }
        }

        /// <summary>
        /// Indica assemblies que devem ser varridos
        /// Propriedade de auxilio
        /// </summary>
        public List<string> IncluirAssembliesString { get; set; }

        /// <summary>
        /// Indica tipos que devem ser incluidos
        /// </summary>
        [XmlIgnore]
        public List<Type> IncluirTipos 
        {
            get 
            { 
                return 
                    this.IncluirTiposString != null ?
                        this.IncluirTiposString.Select(s => Type.GetType(s)).ToList() :
                        new List<Type>(); 
            }
            set { this.IncluirTiposString = value.Select(t => t.FullName + "," + t.Assembly.FullName).ToList(); }
        }

        /// <summary>
        /// Indica tipos que devem ser incluidos
        /// </summary>
        public List<string> IncluirTiposString { get; set; }

        /// <summary>
        /// Indica o tipo de varredura para inclusao de tipos quando
        /// se está lendo o assembly
        /// </summary>
        public ServicoHostTipoVarreduraEnum TipoVarreduraTipos { get; set; }

        /// <summary>
        /// Indica o tipo de varredura para inclusao de operacoes quando
        /// se está varrendo o tipo
        /// </summary>
        public ServicoHostTipoVarreduraEnum TipoVarreduraOperacoes { get; set; }

        /// <summary>
        /// Indica se deve expor esse servico host como um servico wcf
        /// </summary>
        public bool ExporWCF { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoHostConfig()
        {
            this.TipoVarreduraOperacoes = ServicoHostTipoVarreduraEnum.DefaultIncluir;
            this.TipoVarreduraTipos = ServicoHostTipoVarreduraEnum.DefaultIncluir;
            this.CarregarAssemblies = new List<string>();
        }

    }
}
