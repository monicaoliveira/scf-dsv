﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library.Servicos.Dados;
using System.ServiceModel.Configuration;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Resource.Framework.Contratos.Comum;
using System.Reflection;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Classe responsavel pela troca de mensagens
    /// </summary>
    public class Mensageria
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Configuracoes da mensageria
        /// </summary>
        public static MensageriaConfig Config { get; set; }

        /// <summary>
        /// Mantem cache dos tipos de mensagem que a mensageria processa
        /// </summary>
        public static Dictionary<Type, string> CacheTipos { get; set; }

        /// <summary>
        /// Lista de hosts que a mensageria conhece
        /// </summary>
        public static Dictionary<string, MensageriaHostHelper> Hosts { get; set; }

        /// <summary>
        /// Inicializa utilizando informacoes de config
        /// </summary>
        public static void Inicializar()
        {
            Mensageria.Inicializar(GerenciadorConfig.ReceberConfig<MensageriaConfig>());
        }
        
        /// <summary>
        /// Inicializa utilizando o config informado
        /// </summary>
        /// <param name="config"></param>
        public static void Inicializar(MensageriaConfig config)
        {
            // Bloco de controle
            try
            {
                // Log
                log.Info(config);

                // Verifica se tem config
                if (config == null)
                    config = new MensageriaConfig();

                // Pega o config
                Mensageria.Config = config;

                // Inicializa
                Mensageria.CacheTipos = new Dictionary<Type, string>();
                Mensageria.Hosts = new Dictionary<string, MensageriaHostHelper>();

                // Verifica se tem host local para fazer cache dos tipos de mensagem que ele trabalha
                if (ContainerServicoHost.GetInstance().ServicoHost != null)
                {
                    // Cria o helper do host
                    MensageriaHostHelper hostLocalHelper =
                        new MensageriaHostHelper(
                            ContainerServicoHost.GetInstance().ServicoHost);

                    // ... e adiciona item no cache, inicializando-o
                    Mensageria.Hosts.Add(hostLocalHelper.NomeHost, hostLocalHelper);
                }

                // Se deve utilizar WCF, verifica se existem servicos para realizar cache
                if (config.UtilizarWCF)
                {
                    // Pega as configurações do wcf client
                    ClientSection configWCF =
                        (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");

                    // Varre os endpoints ...
                    foreach (ChannelEndpointElement endpoint in configWCF.Endpoints)
                    {
                        // ... verificando se o contrato é de IServicoHost ...
                        if (endpoint.Contract == typeof(IServicoHost).FullName)
                        {
                            // Cria o helper do host
                            MensageriaHostHelper hostHelper =
                                new MensageriaHostHelper(endpoint);

                            // ... e adiciona item no cache, inicializando-o
                            Mensageria.Hosts.Add(hostHelper.NomeHost, hostHelper);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log
                log.Error(config, ex);

                // Repassa excessao
                throw ex;
            }

        }

        /// <summary>
        /// Processa mensagem
        /// Método com generics
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public static T Processar<T>(MensagemRequestBase request) where T : MensagemResponseBase
        {
            return (T)Processar(request);
        }

        /// <summary>
        /// Processa mensagem
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
   
        public static MensagemResponseBase Processar(MensagemRequestBase request)
        {                        
            // Pega o tipo da mensagem
            Type tipoMensagem = request.GetType();
            Type tipoGenerico = 
                request.GetType().IsGenericType == true ?
                request.GetType().GetGenericArguments().First() :
                null;

            // Log
            Dictionary<string, string> logAtributos = new Dictionary<string, string>();
            logAtributos.Add("mensagem", tipoMensagem.Name);
            if (tipoGenerico != null) 
                logAtributos.Add("tipoGenerico", tipoGenerico.Name);
            LogArquivo logArquivo = new LogArquivo("app", logAtributos);
            LogArquivo.Objeto("request", request);

            // Prepara resposta
            MensagemResponseBase resposta = null;
            
            // Bloco de controle
            try
            {
                // Verifica se precisa inicializar
                if (Mensageria.CacheTipos == null)
                    Mensageria.Inicializar();

                // Procura o host que ira processar a mensagem
                MensageriaHostHelper hostHelper = null;

                // Faz loop para a classe e para as classes base
                while (tipoMensagem != null && hostHelper == null)
                {
                    // Se for tipo generico, trata
                    if (tipoMensagem.IsGenericType)
                        tipoMensagem = tipoMensagem.GetGenericTypeDefinition();

                    // Verifica se alguem trata esta mensagem
                    if (Mensageria.CacheTipos.ContainsKey(tipoMensagem))
                        hostHelper = Mensageria.Hosts[Mensageria.CacheTipos[tipoMensagem]];

                    // Se nao achou o host, tenta com a classe pai
                    if (hostHelper == null)
                        tipoMensagem = tipoMensagem.BaseType;
                }

                // Se nao achou, procura no diretorio
                if (hostHelper == null)
                {
                    // Procura o servico host que processa as mensagens de diretorio
                    IServicoHost servicoHostDiretorio = null;

                    // Verifica se tem servico de diretorio no cache local
                    if (Mensageria.CacheTipos != null && Mensageria.CacheTipos.ContainsKey(typeof(IServicoDiretorioServico)))
                        servicoHostDiretorio = ContainerServicoHost.GetInstance().ServicoHost;

                    // Faz a consulta

                    // Pega instancia do host
                }

                // Processa a mensagem
                resposta = hostHelper.ProcessarMensagem(request);
            }
            catch (TargetInvocationException ex)
            {
                // Log
                LogArquivo.LogErro(ex.InnerException);

                // Repassa
                throw ex.InnerException;
            }
            catch (Exception ex)
            {
                // Log
                LogArquivo.LogErro(ex);

                // Repassa
                throw ex;
            }
            finally
            {
                if (resposta != null)
                    LogArquivo.Objeto("response", resposta);
                logArquivo.Dispose();
            }

            // Retorna
            return resposta;
        }
    }
}
