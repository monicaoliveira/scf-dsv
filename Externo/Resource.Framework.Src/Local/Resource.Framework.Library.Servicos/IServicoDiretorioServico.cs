﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Interface para o servico de diretorio
    /// </summary>
    public interface IServicoDiretorioServico
    {
        /// <summary>
        /// Faz consulta de itens do diretorio de servicos de acordo com o filtro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarDiretorioServicoResponse ListarDiretorio(ListarDiretorioServicoRequest request);
    }
}
