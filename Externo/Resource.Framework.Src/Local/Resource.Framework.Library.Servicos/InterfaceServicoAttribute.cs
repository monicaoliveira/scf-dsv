﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Atributo para marcacao de servicos
    /// </summary>
    public class InterfaceServicoAttribute : Attribute
    {
        /// <summary>
        /// Indica se deve incluir este item
        /// </summary>
        public bool Incluir { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public InterfaceServicoAttribute()
        {
            this.Incluir = true;
        }
    }
}
