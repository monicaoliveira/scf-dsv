﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using System.ServiceModel;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Interface para o servico de host
    /// </summary>
    [ServiceContract(Namespace = "http://resource")]
    [ServiceKnownType("RetornarTipos", typeof(LocalizadorTiposHelper))]
    public interface IServicoHost
    {
        /// <summary>
        /// Pede o processamento da mensagem
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        MensagemResponseBase ProcessarMensagem(MensagemRequestBase request);

        /// <summary>
        /// Solicita lista de operacoes que o host suporta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        ListarOperacaoResponse ListarOperacoes(ListarOperacaoRequest request);

        /// <summary>
        /// Solicita o inicio do host
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        IniciarHostResponse IniciarHost(IniciarHostRequest request);

        /// <summary>
        /// Solicita a finalização do host
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        FinalizarHostResponse FinalizarHost(FinalizarHostRequest request);
    }
}
