﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Tipos de varredura
    /// </summary>
    public enum ServicoHostTipoVarreduraEnum
    {
        DefaultIncluir,
        DefaultNaoIncluir
    }
}
