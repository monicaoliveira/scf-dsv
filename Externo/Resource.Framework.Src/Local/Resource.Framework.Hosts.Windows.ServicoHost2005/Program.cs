using System;
using System.Collections.Generic;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library;

namespace Resource.Framework.Hosts.Windows.ServicoHost2005
{
    class Program
    {
        static void Main(string[] args)
        {
            // Pega o config
            ContainerServicoHostConfig config = GerenciadorConfig.ReceberConfig<ContainerServicoHostConfig>();
            if (config == null)
            {
                config = new ContainerServicoHostConfig();
                config.ServicoHostConfig = new ServicoHostConfig();
            }

            // Inicia o host
            Console.WriteLine("Iniciando host...");
            ContainerServicoHost.GetInstance().Iniciar(config);

            // Aguarda
            Console.WriteLine("Host iniciado...");
            Console.ReadLine();

            // Finaliza o host
            Console.WriteLine("Finalizando host...");
            ContainerServicoHost.GetInstance().Finalizar();
            Console.WriteLine("Host finalizado...");
        }
    }
}
