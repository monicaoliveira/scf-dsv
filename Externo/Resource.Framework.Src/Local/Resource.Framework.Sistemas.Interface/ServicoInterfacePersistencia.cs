﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Interface;
using Resource.Framework.Contratos.Interface.Dados;
using Resource.Framework.Contratos.Interface.Mensagens;
using Resource.Framework.Library.Servicos;

namespace Resource.Framework.Sistemas.Interface
{
    /// <summary>
    /// Implementação do serviço de persistencia de interface (visual)
    /// </summary>
    public class ServicoInterfacePersistencia : IServicoInterfacePersistencia
    {
        #region Construtores

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoInterfacePersistencia()
        {
        }

        #endregion

        #region IServicoInterfacePersistencia Members

        #region GrupoComandoInterface

        /// <summary>
        /// Consulta de grupos de comandos de interface de acordo com os filtros informados
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ListarGruposComandoInterfaceResponse ListarGruposComandoInterface(ListarGruposComandoInterfaceRequest parametros)
        {
            // Transforma as condições em lista de condições para a pesquisa na persistencia
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

            // Retorna a lista de acordo com os filtros
            return
                new ListarGruposComandoInterfaceResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    Resultado =
                        Mensageria.Processar<ConsultarObjetosResponse<GrupoComandoInterfaceInfo>>(
                            new ConsultarObjetosRequest<GrupoComandoInterfaceInfo>()
                            {
                                Condicoes = condicoes
                            }).Resultado
                };
        }

        /// <summary>
        /// Salva um GrupoComandoInterface
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public SalvarGrupoComandoInterfaceResponse SalvarGrupoComandoInterface(SalvarGrupoComandoInterfaceRequest parametros)
        {
            // Salva
            Mensageria.Processar(
                new SalvarObjetoRequest<GrupoComandoInterfaceInfo>()
                {
                    Objeto = parametros.GrupoComandoInterface
                });

            // Retorna
            return
                new SalvarGrupoComandoInterfaceResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }

        /// <summary>
        /// Receber o detalhe de um GrupoComandoInterface
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public ReceberGrupoComandoInterfaceResponse ReceberGrupoComandoInterface(ReceberGrupoComandoInterfaceRequest parametros)
        {
            // Retorna o GrupoComandoInterface solicitado
            return
                new ReceberGrupoComandoInterfaceResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem,
                    GrupoComandoInterface =
                        Mensageria.Processar<ReceberObjetoResponse<GrupoComandoInterfaceInfo>>(
                            new ReceberObjetoRequest<GrupoComandoInterfaceInfo>()
                            {
                                CodigoObjeto = parametros.CodigoGrupoComandoInterface
                            }).Objeto
                };
        }

        /// <summary>
        /// Remove um GrupoComandoInterface
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public RemoverGrupoComandoInterfaceResponse RemoverGrupoComandoInterface(RemoverGrupoComandoInterfaceRequest parametros)
        {
            // Remove o GrupoComandoInterface
            Mensageria.Processar(
                new RemoverObjetoRequest<GrupoComandoInterfaceInfo>()
                {
                    CodigoObjeto = parametros.CodigoGrupoComandoInterface
                });

            // Retorna
            return
                new RemoverGrupoComandoInterfaceResponse()
                {
                    CodigoMensagemRequest = parametros.CodigoMensagem
                };
        }

        #endregion

        #endregion
    }
}
