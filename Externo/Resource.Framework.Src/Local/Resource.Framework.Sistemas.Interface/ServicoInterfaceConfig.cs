﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using Resource.Framework.Contratos.Interface.Dados;

namespace Resource.Framework.Sistemas.Interface
{
    /// <summary>
    /// Classe de configurações para o serviço de interface
    /// </summary>
    public class ServicoInterfaceConfig
    {
        /// <summary>
        /// Permite cadastrar na configuração, grupos de comandos de interface
        /// </summary>
        public List<GrupoComandoInterfaceInfo> GruposComandoInterface { get; set; }

        /// <summary>
        /// Indica como será tratada a persitencia de grupos de comandos de interface
        /// </summary>
        public GrupoComandoInterfacePersistenciaTipo TipoPersistenciaGrupoComandoInterface { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoInterfaceConfig()
        {
            this.TipoPersistenciaGrupoComandoInterface = GrupoComandoInterfacePersistenciaTipo.Persistencia;
            this.GruposComandoInterface = new List<GrupoComandoInterfaceInfo>();
        }
    }
}
