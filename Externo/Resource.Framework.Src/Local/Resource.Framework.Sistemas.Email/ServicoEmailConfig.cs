﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Sistemas.Email
{
    public class ServicoEmailConfig
    {
        /// <summary>
        /// Email padrão utilizado para envio de email.
        /// </summary>
        public string Remetente { get; set; }

        /// <summary>
        /// Nome ou IP do servidor SMTP
        /// </summary>
        public string Host{ get; set; }

        /// <summary>
        /// Porta do servidor SMTP
        /// </summary>
        public int? Porta { get; set; }

        /// <summary>
        /// Indica se o servidor usa ou não conexão segura para envio de email.
        /// </summary>
        public bool UsarSSL { get; set; }

        /// <summary>
        /// Senha do sender, necessário caso o servidor SMTP solicite authenticação.
        /// </summary>
        public string SenhaRemetente { get; set; }

        /// <summary>
        /// Nome do dominio onde o usuário sera autenticado.
        /// </summary>
        public string Dominio { get; set; }

        /// <summary>
        /// Indica se é necessário usar credenciais para o envio de email.
        /// Se sim, a propriedade 'SenhaRemetente' deve ser preenchida.
        /// </summary>
        public bool UsarCredenciais { get; set; }

        /// <summary>
        /// E-mail do usuário que vai receber todos os e-mails enviados pelo sistema.
        /// </summary>
        public string EmailDeTeste { get; set; }

    }
}
