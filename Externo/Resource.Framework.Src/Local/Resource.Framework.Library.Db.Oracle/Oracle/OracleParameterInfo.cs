﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Db.Oracle
{
    /// <summary>
    /// Informações sobre um parâmetro de procedure do oracle
    /// </summary>
    public class OracleParameterInfo
    {
        /// <summary>
        /// Nome do parametro
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tipo do paramêtro.
        /// O nome do tipo informado são nomes de tipos do oracle.
        /// Por exemplo: varchar2, int, etc...
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Direção do parametro.
        /// Exemplo: in, out, in out
        /// </summary>
        public string Direction { get; set; }
    }
}
