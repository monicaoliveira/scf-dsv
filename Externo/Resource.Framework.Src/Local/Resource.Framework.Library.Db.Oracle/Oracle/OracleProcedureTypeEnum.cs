﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Db.Oracle
{
    /// <summary>
    /// Tipos de procedures do oracle
    /// </summary>
    public enum OracleProcedureTypeEnum
    {
        StoredProcedure,
        Function
    }
}
