﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using System.Configuration;
using Oracle.DataAccess.Types;

namespace Resource.Framework.Library.Db.Oracle
{
    /// <summary>
    /// Biblioteca para acesso ao banco de dados Oracle
    /// </summary>
    public class OracleDbLib : IDbLib2
    {
        /// <summary>
        /// Parametros da conexão
        /// </summary>
        private OracleDbLibConfig _config = null;

        /// <summary>
        /// Referencia para uma conexão 
        /// </summary>
        private OracleConnection _cn = null;

        public OracleConnection OracleConnection
        {
            get
            {
                return this._cn;
            }
            set
            {
                this._cn = value;
            }
        }

        /// <summary>
        /// Informações sobre as procedures
        /// </summary>
        private Dictionary<string, OracleProcedureInfo> _procedures = null;

        /// <summary>
        /// Construtor default
        /// </summary>
        public OracleDbLib()
        {
            this.Inicializar(null);
        }

        /// <summary>
        /// Construtor que recebe os parametros de inicializacao
        /// </summary>
        /// <param name="parameters"></param>
        public OracleDbLib(OracleDbLibConfig parameters)
        {
            this.Inicializar(parameters);
        }

        #region Funcionalidades Estáticas

        /// <summary>
        /// Instancia estática da classe para ser oferecida como default
        /// </summary>
        private static OracleDbLib _default = null;

        /// <summary>
        /// Acesso estático à instancia default
        /// </summary>
        public static OracleDbLib Default
        {
            get
            {
                if (_default == null)
                    _default = new OracleDbLib();
                return _default;
            }
        }

        #endregion

        #region IDbLib2 Members

        /// <summary>
        /// Pede inicialização da instancia
        /// </summary>
        /// <param name="parametros"></param>
        public void Inicializar(object parametros)
        {
            // Inicializa os parametros
            if (parametros == null)
                parametros = GerenciadorConfig.ReceberConfig<OracleDbLibConfig>();
            if (parametros == null)
                parametros = new OracleDbLibConfig();

            // Guarda referencia
            _config = (OracleDbLibConfig)parametros;
        }

        /// <summary>
        /// Pede finalização da instancia
        /// </summary>
        public void Finalizar()
        {
        }

        /// <summary>
        /// Solicita a execução de uma procedure.
        /// Os parametros são passados em duplas de nome do parametro, valor
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public Dictionary<string, object> ExecutarProcedure(string nomeProcedure, params object[] parametros)
        {
            // Transforma a lista de parametros em um dicionario
            Dictionary<string, object> parametros2 = new Dictionary<string, object>();
            for (int i = 0; i < parametros.Length; i += 2)
                parametros2.Add((string)parametros[i], parametros[i + 1]);

            // Repassa a chamada
            return ExecutarProcedure(nomeProcedure, parametros2);
        }

      
        /// <summary>
        /// Overload da execução de procedure que recebe a lista de parametros através de 
        /// um dicionário.
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public Dictionary<string, object> ExecutarProcedure(string nomeProcedure, Dictionary<string, object> parametros)
        {
            // Inicializa
            OracleCommand cm = null;

            // Bloco de controle
		    try 
            {
                // Garante que a biblioteca está inicializada
                inicializarConexao();

			    // Verifica se tem informacoes da procedure
			    OracleProcedureInfo procedureInfo = null;
			    if (_procedures.ContainsKey(nomeProcedure)) 
				    procedureInfo = _procedures[nomeProcedure];
    			
			    // Verifica se é função ou procedure 
			    bool ehFuncao = false;
			    if (procedureInfo != null && procedureInfo.Type == OracleProcedureTypeEnum.Function)
				    ehFuncao = true;
    			
			    // Monta a string
			    StringBuilder sql = new StringBuilder();
			    if (ehFuncao)
				    sql.Append("begin :retorno = ");
			    else 
				    sql.Append("begin ");
    			
			    // Adiciona o nome da procedure. 
			    // Se não informado pacote, nem usuário (identificado com o '.'), assume o pacote é o padrão.
			    // Se foi informado algo (no nome da procedure tem '.'), apenas coloca o que foi informado
			    if (!nomeProcedure.Contains("."))
				    sql.Append(_config.ProceduresPackageName + "." + nomeProcedure);
			    else
				    sql.Append(nomeProcedure);
    			
			    sql.Append(" (");
    			
			    // Adiciona os parametros na string
			    Dictionary<String, Object> paramsSet = new Dictionary<String, Object>(parametros);
			    foreach (KeyValuePair<String, Object> item in paramsSet) 
				    sql.Append(item.Key + " => :" + item.Key + ", ");
    			
			    // Adiciona os parametros de output
			    if (procedureInfo != null)
                {
				    foreach (OracleParameterInfo parametro in procedureInfo.Parameters.Values) 
					    if (parametro.Direction == "OUT") 
						    sql.Append(parametro.Name + " => :" + parametro.Name + ", ");
			    }
    			
			    // Ajusta a string
			    if (sql.ToString().Trim().EndsWith(",")) 
				    sql.Remove(sql.Length - 2, 2);

			    // Finaliza a string
			    sql.Append("); end;");

			    // Inicia o statement
			    OracleConnection cn = this.ReceberConexao();
			    cm = new OracleCommand(sql.ToString(), cn);
                cm.BindByName = true;

                // Se for função, adiciona o parametro de retorno
                if (ehFuncao) 
                    cm.Parameters.Add(
                        ":retorno", 
                        traduzirTipoSql(procedureInfo.FunctionReturn.Type), 
                        ParameterDirection.ReturnValue);

			    // Adiciona os parametros
                foreach (KeyValuePair<String, Object> item in paramsSet)
                {
                    if (procedureInfo.Parameters.ContainsKey(item.Key.ToUpper()))
                        cm.Parameters.Add(
                            ":" + item.Key, 
                            traduzirTipoSql(procedureInfo.Parameters[item.Key.ToUpper()].Type), 
                            item.Value, 
                            ParameterDirection.Input);
                    else
                        cm.Parameters.Add(":" + item.Key, item.Value);
                }
    			
			    // Adiciona os parametros de output
			    List<OracleParameterInfo> parametrosOutput =
						    new List<OracleParameterInfo>(); 
			    if (procedureInfo != null)
				    foreach (OracleParameterInfo parametro in procedureInfo.Parameters.Values) 
					    if (parametro.Direction == "OUT") 
                        {
						    parametrosOutput.Add(parametro);
                            cm.Parameters.Add(
                                new OracleParameter(
                                    ":" + parametro.Name, 
                                    traduzirTipoSql(parametro.Type), 
                                    ParameterDirection.Output));
					    }

			    // Executa
			    cm.ExecuteNonQuery();
    			
			    // Monta o retorno
			    Dictionary<String, Object> resultado = new Dictionary<String, Object>();
			    if (procedureInfo != null && procedureInfo.Type == OracleProcedureTypeEnum.Function) {
				    if (procedureInfo.FunctionReturn.Type == "REF CURSOR") {
					    resultado.Add("result", ((OracleRefCursor)cm.Parameters[":retorno"].Value).GetDataReader());
				    } else {
					    resultado.Add("result", cm.Parameters[":retorno"].Value);
				    }
			    }
    			
			    // Pega os valores dos parametros de output
			    foreach (OracleParameterInfo parametroOutput in parametrosOutput) 
                {
				    OracleParameterInfo parametro = parametroOutput;
				    if (parametro.Direction == "OUT") 
                    {
					    if (parametro.Type == "REF CURSOR") 
                        {
						    // Verifica se tem retorno
                            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":" + parametroOutput.Name].Value;
                            if (!orc.IsNull)
                            {
                                // Pega o reader
                                OracleDataReader dr = orc.GetDataReader();

                                // Transforma em dataTable
                                DataTable tb = new DataTable();
                                tb.Load(dr);

                                // Fecha o cursor
                                dr.Close();

                                // Retorna no parametro
                                resultado.Add(parametro.Name, tb);
                            }
                            else
                            {
                                resultado.Add(parametro.Name, null);
                            }
					    } 
                        else 
                        {
						    try 
                            {
							    resultado.Add(parametro.Name, cm.Parameters[parametroOutput.Name].Value);
						    } 
                            catch (Exception) 
                            {
							    resultado.Add(parametro.Name, null);
						    }
					    }
				    }
			    }

			    // Libera statement
                cm.Connection.Close();
			    cm.Dispose();
    			
			    // Retorna
			    return resultado;
    			
		    } 
            catch (Exception e) 
            {
                // Libera o cm
                if (cm != null)
                {
                    if (cm.Connection != null && cm.Connection.State == ConnectionState.Open)
                        cm.Connection.Close();
                    cm.Dispose();
                }

                // Repassa o erro
			    throw new DbLibException(e);
		    }
        }

        /// <summary>
        /// Solicita a execução de uma string sql.
        /// Os parametros são passados em duplas de nome do parametro, valor
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public DataSet ExecutarConsulta(string sql, params object[] parametros)
        {
            // Transforma a lista de parametros em um dicionario
            Dictionary<string, object> parametros2 = new Dictionary<string, object>();
            for (int i = 0; i < parametros.Length; i += 2)
                parametros2.Add((string)parametros[i], parametros[i + 1]);

            // Repassa a chamada
            return ExecutarConsulta(sql, parametros2);
        }

        /// <summary>
        /// Overload da execução de string sql que recebe os parametros num dicionário
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public DataSet ExecutarConsulta(string sql, Dictionary<string, object> parametros)
        {
            return null;
        }

        /// <summary>
        /// Monta o command da procedure
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <returns></returns>
        public object GetProcedureCommand(string nomeProcedure)
        {
            // Repassa a chamada
            return this.GetProcedureCommand(nomeProcedure, null, this.ReceberConexao());
        }

        /// <summary>
        /// Monta o command da procedure
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <returns></returns>
        public object GetProcedureCommand(string nomeProcedure, OracleConnection cn)
        {
            // Repassa a chamada
            return this.GetProcedureCommand(nomeProcedure, null, cn);
        }

        /// <summary>
        /// Monta o command da procedure
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <returns></returns>
        public object GetProcedureCommand(string nomeProcedure, List<string> parametros)
        {
            return this.GetProcedureCommand(nomeProcedure, parametros, this.ReceberConexao());
        }

        /// <summary>
        /// Monta o command da procedure
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <returns></returns>
        public object GetProcedureCommand(string nomeProcedure, List<string> parametros, OracleConnection cn)
        {
            // Garante que a biblioteca está inicializada
            inicializarConexao();

            // Inicia o statement
            OracleCommand cm = new OracleCommand();
            cm.Connection = cn;
            cm.BindByName = true;

            // Verifica se tem informacoes da procedure
            OracleProcedureInfo procedureInfo = null;
            if (_procedures.ContainsKey(nomeProcedure))
                procedureInfo = _procedures[nomeProcedure];

            // Se nao informou a lista de parametros, assume todos os parametros
            if (parametros == null)
                parametros = 
                    new List<string>(
                        procedureInfo.Parameters.Select(p => p.Value.Name));

            // Verifica se é função ou procedure 
            bool ehFuncao = false;
            if (procedureInfo != null && procedureInfo.Type == OracleProcedureTypeEnum.Function)
                ehFuncao = true;

            // Monta a string
            StringBuilder sql = new StringBuilder();
            if (ehFuncao)
                sql.Append("begin :retorno = ");
            else
                sql.Append("begin ");

            // Adiciona o nome da procedure. 
            // Se não informado pacote, nem usuário (identificado com o '.'), assume o pacote é o padrão.
            // Se foi informado algo (no nome da procedure tem '.'), apenas coloca o que foi informado
            if (!nomeProcedure.Contains("."))
                sql.Append(_config.ProceduresPackageName + "." + nomeProcedure);
            else
                sql.Append(nomeProcedure);
            sql.Append(" (");

            // Adiciona os parametros de output
            foreach (OracleParameterInfo parametro in procedureInfo.Parameters.Values)
            {
                if (parametro.Direction != "OUT" && parametros.Contains(parametro.Name))
                {
                    // Adiciona na string
                    sql.Append(parametro.Name + " => :" + parametro.Name + ", ");

                    // Adiciona parametro no command
                    cm.Parameters.Add(
                        ":" + parametro.Name,
                        traduzirTipoSql(parametro.Type),
                        null,
                        ParameterDirection.Input);
                }
                else if (parametro.Direction == "OUT")
                {
                    // Adiciona na string
                    sql.Append(parametro.Name + " => :" + parametro.Name + ", ");

                    // Adiciona o parametro
                    cm.Parameters.Add(
                        new OracleParameter(
                            ":" + parametro.Name,
                            traduzirTipoSql(parametro.Type),
                            ParameterDirection.Output));
                }
            }

            // Ajusta a string
            if (sql.ToString().Trim().EndsWith(","))
                sql.Remove(sql.Length - 2, 2);

            // Finaliza a string
            sql.Append("); end;");

            // Se for função, adiciona o parametro de retorno
            if (ehFuncao)
                cm.Parameters.Add(
                    ":retorno",
                    traduzirTipoSql(procedureInfo.FunctionReturn.Type),
                    ParameterDirection.ReturnValue);

            // Informa string no comando
            cm.CommandText = sql.ToString();

            // Retorna
            return cm;
        }

        #endregion

        /// <summary>
        /// Garante que a conexão está inicializada
        /// </summary>
        private void inicializarConexao()
        {
            // Verifica se ainda não foi inicializado
            if (_procedures == null)
            {
                // Cria metadados das procedures
                _procedures = new Dictionary<String, OracleProcedureInfo>();

                // Solicita informações dos argumentos
                OracleConnection cn = this.ReceberConexao();
                OracleDataReader dr = 
                    new OracleCommand(
                        "select * from all_arguments where package_name = '" + _config.ProceduresPackageName + "'", 
                        cn).ExecuteReader();
                
                // Varre
                while (dr.Read())
                {

                    // Pega informacoes necessarias
                    String nomeProcedure = (string)dr["OBJECT_NAME"];
                    String nomeParametro = (string)dr["ARGUMENT_NAME"];
                    String tipoParametro = (string)dr["DATA_TYPE"];
                    String direcao = (string)dr["IN_OUT"];

                    // Recebe ou cria a procedure
                    OracleProcedureInfo procedureInfo = null;
                    if (_procedures.ContainsKey(nomeProcedure))
                    {
                        procedureInfo = _procedures[nomeProcedure];
                    }
                    else
                    {
                        procedureInfo = new OracleProcedureInfo();
                        procedureInfo.Name = nomeProcedure;
                        procedureInfo.Type = OracleProcedureTypeEnum.StoredProcedure;
                        _procedures.Add(nomeProcedure, procedureInfo);
                    }

                    // Cria o parametro
                    OracleParameterInfo parameterInfo = new OracleParameterInfo();
                    parameterInfo.Name = nomeParametro;
                    parameterInfo.Type = tipoParametro;
                    parameterInfo.Direction = direcao;

                    // Adiciona o parametro ou seta retorno da funcao
                    if (nomeParametro == null)
                    {
                        // É função
                        procedureInfo.Type = OracleProcedureTypeEnum.Function;
                        procedureInfo.FunctionReturn = parameterInfo;
                    }
                    else
                    {
                        // Adiciona o parametro
                        if(!procedureInfo.Parameters.ContainsKey(nomeParametro))
                            procedureInfo.Parameters.Add(nomeParametro, parameterInfo);
                    }

                }

                // Fecha o resultset
                cn.Close();
                dr.Close();
            }
        }
        
        /// <summary>
        /// Retorna uma conexão com o banco de dados 
        /// </summary>
        /// <returns></returns>
        public OracleConnection ReceberConexao()
        {
            return OnReceberConexao();
        }

        /// <summary>
        /// Método virtual para retornar conexão com o banco de dados
        /// </summary>
        /// <returns></returns>
        protected virtual OracleConnection OnReceberConexao()
        {
            // Cria a conexão
            OracleConnection _cn = null;
            if (_cn == null)
            {
                // Pega a string de conexão
                string connString = _config.ConnectionString;
                if (connString == null)
                    connString = 
                        ConfigurationManager.ConnectionStrings[_config.ConnectionStringName].ConnectionString;
                //1485
                // Cria
                _cn = new OracleConnection(connString);

                // Abre
                _cn.Open();
            }

            // Retorna
            return _cn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoParametro"></param>
        /// <returns></returns>
        private OracleDbType traduzirTipoSql(String tipoParametro)
        {
            OracleDbType resposta = OracleDbType.Varchar2;
            switch (tipoParametro)
            {
                case "REF CURSOR":
                    resposta = OracleDbType.RefCursor;
                    break;
                case "VARCHAR2":
                    resposta = OracleDbType.Varchar2;
                    break;
                case "FLOAT":
                    resposta = OracleDbType.Double;
                    break;
                case "NUMBER":
                    resposta = OracleDbType.Double;
                    break;
                case "DATE":
                    resposta = OracleDbType.Date;
                    break;
                case "CHAR":
                    resposta = OracleDbType.Char;
                    break;
                case "PL/SQL BOOLEAN":
                    resposta = OracleDbType.Byte;
                    break;
                case "BLOB":
                    resposta = OracleDbType.Blob;
                    break;
                case "CLOB":
                    resposta = OracleDbType.Clob;
                    break;
            }
            return resposta;
        }

        /// <summary>
        /// Converte um valor de enumerador para um valor do banco de dados
        /// </summary>
        /// <typeparam name="T">Tipo do enumerador</typeparam>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static object EnumToDb<T>(object enumValue)
        {
            return (int)enumValue;
        }

        /// <summary>
        /// Converte um valor de banco de dados para um valor de enumerador
        /// </summary>
        /// <typeparam name="T">Tipo do enumerador</typeparam>
        /// <param name="dbValue"></param>
        /// <returns></returns>
        public static T EnumToObject<T>(object dbValue)
        {
            return (T)Enum.ToObject(typeof(T), Convert.ToInt32(dbValue));
        }

    }
}
