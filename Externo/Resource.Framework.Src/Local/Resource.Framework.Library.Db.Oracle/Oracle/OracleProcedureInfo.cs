﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Db.Oracle
{
    /// <summary>
    /// Informações sobre um procedure do oracle
    /// </summary>
    public class OracleProcedureInfo
    {
        /// <summary>
        /// Nome da procedure
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Lista de parametros
        /// </summary>
        public Dictionary<string, OracleParameterInfo> Parameters { get; set; }

        /// <summary>
        /// Tipo da procedure
        /// </summary>
        public OracleProcedureTypeEnum Type { get; set; }

        /// <summary>
        /// Caso a procedure seja uma função, contém informações sobre
        /// o parâmetro de retorno
        /// </summary>
        public OracleParameterInfo FunctionReturn { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public OracleProcedureInfo()
        {
            this.Parameters = new Dictionary<string, OracleParameterInfo>();
            this.Type = OracleProcedureTypeEnum.StoredProcedure;
        }
    }
}
