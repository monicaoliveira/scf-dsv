﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Db.Oracle
{
    /// <summary>
    /// Parametros da biblioteca de acesso a banco de dados oracle
    /// </summary>
    public class OracleDbLibConfig
    {
        /// <summary>
        /// Indica o nome da string de conexão no arquivo de configurações
        /// </summary>
        public string ConnectionStringName { get; set; }

        /// <summary>
        /// Contém a própria string de conexão. 
        /// Se este parâmetro estiver presente a conexão é feita utilizando-se este valor
        /// e ignorando o o ConnectionStringName
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Nome do package que contém as procedures da aplicação
        /// </summary>
        public string ProceduresPackageName { get; set; }

        /// <summary>
        /// Indica se deve ser feito cache de commands de procedures
        /// </summary>
        public bool CacheProcedureCommands { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public OracleDbLibConfig()
        {
            this.ConnectionStringName = "default";
        }
    }
}
