﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Db
{
    /// <summary>
    /// Interface para bibliotecas de acesso a banco de dados relacional.
    /// Evolução em relação à primeira versão para generalizar o retorno de valores das
    /// procedures. Em vez de retornar um dataset, retorna uma coleção de valores, sendo
    /// que quando um cursor é retornado, ele aparece como datatable
    /// </summary>
    public interface IDbLib2
    {
        /// <summary>
        /// Pede inicialização da instancia
        /// </summary>
        /// <param name="parametros"></param>
        void Inicializar(object parametros);

        /// <summary>
        /// Pede finalização da instancia
        /// </summary>
        void Finalizar();

        /// <summary>
        /// Solicita a execução de uma procedure.
        /// Os parametros são passados em duplas de nome do parametro, valor
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        Dictionary<string, object> ExecutarProcedure(string nomeProcedure, params object[] parametros);

        /// <summary>
        /// Overload da execução de procedure que recebe a lista de parametros através de 
        /// um dicionário.
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        Dictionary<string, object> ExecutarProcedure(string nomeProcedure, Dictionary<string, object> parametros);

        /// <summary>
        /// Solicita a execução de uma string sql.
        /// Os parametros são passados em duplas de nome do parametro, valor
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        DataSet ExecutarConsulta(string sql, params object[] parametros);

        /// <summary>
        /// Overload da execução de string sql que recebe os parametros num dicionário
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        DataSet ExecutarConsulta(string sql, Dictionary<string, object> parametros);

        /// <summary>
        /// Monta o command da procedure
        /// </summary>
        /// <param name="nomeProcedure"></param>
        /// <returns></returns>
        object GetProcedureCommand(string nomeProcedure);
        object GetProcedureCommand(string nomeProcedure, List<string> parametros);

    }
}
