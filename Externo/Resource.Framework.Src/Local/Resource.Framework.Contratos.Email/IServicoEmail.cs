﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Email.Mensagens;

namespace Resource.Framework.Contratos.Email
{
    /// <summary>
    /// Interface para os serviço de envio de Email
    /// </summary>
    public interface IServicoEmail
    {
        /// <summary>
        /// Solicita o envio de um Email
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        EnviarEmailResponse EnviarEmail(EnviarEmailRequest parametros);
    }
}
