﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Email.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Email.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de envio de Email
    /// </summary>
    public class EnviarEmailRequest : MensagemRequestBase
    {
        /// <summary>
        /// Classe de dados de email
        /// </summary>
        public EmailInfo Email { get; set; }

        /// <summary>
        /// Host
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Verifica se o ambiente é produção e ignora o email de teste.
        /// </summary>
        public bool EhProducao { get; set; }
    }
}
