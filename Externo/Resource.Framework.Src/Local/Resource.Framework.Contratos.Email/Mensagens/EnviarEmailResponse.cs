﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Email.Mensagens
{
    /// <summary>
    /// Mensagem de resposta ao envio de um email
    /// </summary>
    public class EnviarEmailResponse : MensagemResponseBase
    {
        
    }
}
