﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Email.Dados
{
    /// <summary>
    /// Representa um email 
    /// </summary>
    [Serializable]
    public class EmailInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do Email
        /// </summary>
        public string CodigoEmail { get; set; }

        /// <summary>
        /// Destinatario do Email
        /// </summary>
        public string Destinatario { get; set; }

        /// <summary>
        /// Destinatario do Email com cópia
        /// </summary>
        public string DestinatarioCC { get; set; }

        /// <summary>
        /// Assunto do Email
        /// </summary>
        public string Assunto { get; set; }

        /// <summary>
        /// Texto do Email
        /// </summary>
        public string Texto{ get; set; }

        /// <summary>
        /// Remetente do email.
        /// </summary>
        public string Remetente { get; set; }

        /// <summary>
        /// Indica se o formato é em HTML.
        /// </summary>
        public bool UsarHTML { get; set; }

        /// <summary>
        /// Data/hora de envio.
        /// </summary>
        public DateTime DataHoraEnvio { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public EmailInfo()
        {
            this.DataHoraEnvio = DateTime.Now;
        }


        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoEmail;
        }

        #endregion
    }
}
