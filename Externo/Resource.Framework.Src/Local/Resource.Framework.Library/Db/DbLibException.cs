﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Db
{
    /// <summary>
    /// Excecão da biblioteca DbLib
    /// </summary>
    public class DbLibException : Exception
    {
        public DbLibException()
            : base()
        {
        }

        public DbLibException(string message)
            : base(message)
        {
        }

        public DbLibException(Exception innerException)
            : base(innerException.Message, innerException)
        {
        }

        public DbLibException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
