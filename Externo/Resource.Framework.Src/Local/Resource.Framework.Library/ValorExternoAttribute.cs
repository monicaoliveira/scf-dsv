﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Atributo para relacionar valores externos à entidade
    /// </summary>
    public class ValorExternoAttribute : Attribute
    {
        /// <summary>
        /// Contém o valor externo
        /// </summary>
        public object Valor { get; set; }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="valor"></param>
        public ValorExternoAttribute(object valor)
        {
            this.Valor = valor;
        }
    }
}
