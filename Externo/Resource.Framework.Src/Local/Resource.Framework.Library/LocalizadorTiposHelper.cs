﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Classe de auxilio para o WCF.
    /// Contem o método que retorna os tipos possíveis para serem inclusos na lista de serialização, 
    /// de acordo com o tipo chamador
    /// </summary>
    [Serializable]
    public static class LocalizadorTiposHelper
    {
        /// <summary>
        /// Referencia para o objeto de configurações
        /// </summary>
        private static LocalizadorTiposConfig _config = GerenciadorConfig.ReceberConfig<LocalizadorTiposConfig>();

        /// <summary>
        /// Variável local para fazer o cache dos tipos. Evita que a cada chamada seja recriada a lista
        /// </summary>
        private static Dictionary<Type, List<Type>> _cache = new Dictionary<Type, List<Type>>();

        /// <summary>
        /// Método para retornar a lista de tipos conhecidos. Retorna os tipos de todos as classes de 
        /// dados e de mensagens deste assembly.
        /// </summary>
        /// <returns></returns>
        public static List<Type> RetornarTipos(ICustomAttributeProvider provider)
        {
            // Inicializa
            List<Type> retorno = new List<Type>();
            
            // Pega o tipo chamador
            Type tipoChamador = (Type)provider;

            // Verifica se tem a entrada no cache
            if (!_cache.ContainsKey(tipoChamador))
            {
                // Cria item e adiciona no cache
                retorno = new List<Type>();
                _cache.Add(tipoChamador, retorno);

                // Acha o tipo na lista do config
                LocalizadorGrupoTipoInfo grupo = null;
                List<LocalizadorTipoInfo> lista = new List<LocalizadorTipoInfo>();
                if (_config != null)
                {
                    // Adiciona os grupos
                    grupo =
                        (from g in _config.Grupos
                         where g.TipoChamador == tipoChamador
                         select g).FirstOrDefault();
                    if (grupo != null && grupo.Tipos != null)
                        lista.AddRange(grupo.Tipos);

                    // Adiciona a lista fixa
                    if (_config.ListaFixa != null)
                        lista.AddRange(_config.ListaFixa);
                }

                // Varre adicionando os itens
                foreach (LocalizadorTipoInfo tipoInfo in lista)
                {
                    // Adiciona assembly?
                    if (tipoInfo.IncluirAssembly != null)
                    {
                        // Separa por ;
                        foreach (string assemblyStr in tipoInfo.IncluirAssembly.Split(';'))
                        {
                            // Pega o assembly
                            Assembly assembly = Assembly.Load(assemblyStr);

                            // Varre os tipos adicionando
                            foreach (Type tipo in assembly.GetTypes())
                                if (!tipo.IsGenericType)
                                    retorno.Add(tipo);
                        }
                    }

                    // Adiciona namespace?
                    if (tipoInfo.IncluirNamespace != null)
                    {
                        // Separa por ;
                        foreach (string tipoStr in tipoInfo.IncluirNamespace.Split(';'))
                        {
                            // Pega o assembly
                            string[] ns = tipoStr.Split(',');
                            string ns0 = ns[0].Trim();
                            Assembly assembly = Assembly.Load(ns[1].Trim());

                            // Faz o filtro e inclui
                            retorno.AddRange(
                                from t in assembly.GetTypes()
                                where (t.Namespace == ns0 ||
                                       (tipoInfo.AprofundarNamespace && t.Namespace.StartsWith(ns0)))
                                       && !t.IsGenericType
                                select t);
                        }
                    }

                    // Adiciona tipo?
                    if (tipoInfo.IncluirTipo != null)
                    {
                        // Separa por ;
                        foreach (string tipoStr in tipoInfo.IncluirTipo.Split(';'))
                        {
                            // Pega o tipo
                            Type tipo = Type.GetType(tipoStr);

                            // Inclui
                            retorno.Add(tipo);
                        }
                    }
                }
            }
            else
            {
                // Pega do cache
                retorno = _cache[tipoChamador];
            }

            // Retorna
            return retorno;
        }
    }
}
