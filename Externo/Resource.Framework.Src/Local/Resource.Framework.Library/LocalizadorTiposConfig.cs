﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Classe de configuração para o localizadorTiposHelper.
    /// Permite que seja associado tipos a serem trabalhados a um 
    /// tipo chamador.
    /// </summary>
    [Serializable]
    public class LocalizadorTiposConfig
    {
        /// <summary>
        /// Lista com os grupos chamadores
        /// </summary>
        public List<LocalizadorGrupoTipoInfo> Grupos { get; set; }

        /// <summary>
        /// Lista fixa a ser retornada
        /// </summary>
        public List<LocalizadorTipoInfo> ListaFixa { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public LocalizadorTiposConfig()
        {
            this.Grupos = new List<LocalizadorGrupoTipoInfo>();
            this.ListaFixa = new List<LocalizadorTipoInfo>();
        }
    }
}
