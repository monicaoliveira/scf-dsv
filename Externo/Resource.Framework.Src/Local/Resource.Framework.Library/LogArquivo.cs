﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Xml.Serialization;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Classe para log em arquivo
    /// </summary>
    public class LogArquivo : IDisposable
    {
        #region Rotinas e Variáveis Estáticas

        /// <summary>
        /// Config
        /// </summary>
        private static LogArquivoConfig Config = 
            GerenciadorConfig.ReceberConfig<LogArquivoConfig>() != null ?
            GerenciadorConfig.ReceberConfig<LogArquivoConfig>() :
            new LogArquivoConfig();

        private static void escreverArquivo(DateTime data, int threadId, string texto)
        {
            // Monta o caminho do arquivo
            string caminhoArquivo =
                Path.Combine(
                Config.CaminhoLog,
                string.Format("{0:yyyyMMdd}_{1}.xml", data, threadId));

            // Escreve
            File.AppendAllText(caminhoArquivo, texto);
        }

        public static void LogTag(string tag, bool incluirThread, string texto, params object[] atributos)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;
            
            // Cria dicionario
            Dictionary<string, string> atributoLista = new Dictionary<string, string>();
            for (int i = 0; i < atributos.Length; i += 2)
                atributoLista.Add(atributos[i].ToString(), atributos[i + 1].ToString());

            // Repassa a chamada
            LogTag(tag, incluirThread, texto, atributoLista);
        }

        public static void LogTag(string tag, bool incluirThread, string texto, Dictionary<string, string> atributos)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            // String
            StringBuilder resultado =
                new StringBuilder("<" + tag + " ");

            // Inclui a thread
            int threadId = Thread.CurrentThread.ManagedThreadId;
            if (incluirThread)
                resultado.Append("thread=\"").Append(threadId.ToString()).Append("\" ");

            // Inclui a data
            DateTime data = DateTime.Now;
            resultado.Append("data=\"").Append(data.ToString("o")).Append("\" ");

            // Monta a lista de atributos
            foreach (KeyValuePair<string, string> item in atributos)
                resultado.Append(item.Key).Append("=\"").Append(item.Value).Append("\" ");

            // Tem texto?
            if (texto == null)
                resultado.Append("/>");
            else
                resultado.Append(">").Append(texto).Append("</").Append(tag).Append(">");

            // Escreve no arquivo
            escreverArquivo(data, threadId, resultado.ToString());
        }

        public static void LogTagInicio(string tag, bool incluirThread, bool finalizarTag, params object[] atributos)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            // Cria dicionario
            Dictionary<string, string> atributoLista = new Dictionary<string, string>();
            for (int i = 0; i < atributos.Length; i += 2)
                atributoLista.Add(atributos[i].ToString(), atributos[i + 1].ToString());

            // Repassa a chamada
            LogTagInicio(tag, incluirThread, finalizarTag, atributoLista);
        }

        public static void LogTagInicio(string tag, bool incluirThread, bool finalizarTag, Dictionary<string, string> atributos)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            // String
            StringBuilder resultado = 
                new StringBuilder("<" + tag + " ");

            // Inclui a thread
            int threadId = Thread.CurrentThread.ManagedThreadId;
            if (incluirThread)
                resultado.Append("thread=\"").Append(threadId.ToString()).Append("\" ");

            // Inclui a data
            DateTime data = DateTime.Now;
            resultado.Append("data=\"").Append(data.ToString("o")).Append("\" ");
            
            // Monta a lista de atributos
            foreach (KeyValuePair<string, string> item in atributos)
                resultado.Append(item.Key).Append("=\"").Append(item.Value).Append("\" ");

            // Fecha a tag?
            if (finalizarTag)
                resultado.Append("/");

            // Finaliza
            resultado.Append(">");

            // Escreve no arquivo
            escreverArquivo(data, threadId, resultado.ToString());
        }

        public static void LogTagFim(string tag)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            // Inicializa
            int threadId = Thread.CurrentThread.ManagedThreadId;
            DateTime data = DateTime.Now;

            // String
            StringBuilder resultado =
                new StringBuilder("</" + tag + ">");

            // Escreve no arquivo
            escreverArquivo(data, threadId, resultado.ToString());
        }
        
        /// <summary>
        /// Log de erro
        /// </summary>
        /// <param name="ex"></param>
        public static void LogErro(Exception ex)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            LogArquivo.LogTag(
                "erro", 
                false, 
                ex.ToString());
        }

        public static void Objeto(string tag, object obj)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog || !LogArquivo.Config.LogarObjetos)
                return;

            try
            {
                Type tipo = obj.GetType();
                //XmlSerializer serializer = new XmlSerializer(tipo, LocalizadorTiposHelper.RetornarTipos(typeof(object)).ToArray());
                XmlSerializer serializer = XmlSerializerCache.Create(tipo, LocalizadorTiposHelper.RetornarTipos(typeof(object)).ToArray(), "LocalizadorTiposHelper.RetornarTipos(typeof(object)).ToArray()");
                MemoryStream ms = new MemoryStream();
                serializer.Serialize(ms, obj);
                ms.Position = 0;
                StreamReader reader = new StreamReader(ms);
                reader.ReadLine();

                LogArquivo.LogTag(tag, false, reader.ReadToEnd());
            }
            catch
            {
            }
        }

        #endregion

        #region Rotinas e Variáveis de Instância

        private string _tag = null;
        private DateTime _dataInicio = DateTime.Now;
        private int _threadId = 0;
        private string _caminhoArquivo = null;

        public LogArquivo(string tag, Dictionary<string, string> atributos)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            inicializar(tag);
            LogArquivo.LogTagInicio(tag, true, false, atributos);
        }

        public LogArquivo(string tag, params string[] atributos)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            inicializar(tag);
            LogArquivo.LogTagInicio(tag, true, false, atributos);
        }

        private void inicializar(string tag)
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            _tag = tag;
            _dataInicio = DateTime.Now;
            _threadId = Thread.CurrentThread.ManagedThreadId;
            _caminhoArquivo = Path.Combine(Config.CaminhoLog, string.Format("{0:yyyyMMdd}_{1}.xml", DateTime.Now, _threadId.ToString()));
        }

        public void Dispose()
        {
            // Efetuar log?
            if (!LogArquivo.Config.EfetuarLog)
                return;

            DateTime dataFim = DateTime.Now;
            LogArquivo.LogTag("tempo", false, null, "dataFim", dataFim.ToString("o"), "duracao", ((TimeSpan)(dataFim - _dataInicio)).TotalMilliseconds);
            LogArquivo.LogTagFim(_tag);
        }

        #endregion
    }
}
