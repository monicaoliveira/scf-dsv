﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Utilitarios diversos
    /// </summary>
    public static class Utils
    {


        public static bool IsNumeric(string value)
        {
            //int result = 0;
            //return int.TryParse(value, out result);
            string sRegex = @"^\b\d+\b$";
            return Regex.IsMatch(value, sRegex);
        }

        /// <summary>
        /// Se houver, retorna a descricao de um enumerador
        /// Se nao, retorna o texto do enumerador
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetEnumDescription(Enum value)
        {
            // Get the Description attribute value for the enum value
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }

        /// <summary>
        /// Converte um objeto em sua superclasse
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T ConvertToDerivedClass<T>(this object objetoOrigem)
        {
            // Se for nulo, retorna nulo
            if (objetoOrigem == null)
                return default(T);

            // Verifica se é o mesmo tipo solicitado
            if (objetoOrigem.GetType() == typeof(T))
                return (T)objetoOrigem;
            
            // Serializa o objeto
            Type tipoOrigem = objetoOrigem.GetType();
            MemoryStream ms = new MemoryStream();
            //XmlSerializer serializer1 = new XmlSerializer(objetoOrigem.GetType());
            XmlSerializer serializer1 = XmlSerializerCache.Create(objetoOrigem.GetType());
            serializer1.Serialize(ms, objetoOrigem);

            // Prepara a desserializacao para receber um root com nome diferente
            XmlAttributes myXmlAttributes = new XmlAttributes();

            // Create an XmlRootAttribute and set its element name and namespace.
            XmlRootAttribute myXmlRootAttribute = new XmlRootAttribute();
            myXmlRootAttribute.ElementName = objetoOrigem.GetType().Name;

            // Set the XmlRoot property to the XmlRoot object.
            myXmlAttributes.XmlRoot = myXmlRootAttribute;
            XmlAttributeOverrides myXmlAttributeOverrides =
                                          new XmlAttributeOverrides();

            /* Add the XmlAttributes object to the 
            XmlAttributeOverrides object. */
            myXmlAttributeOverrides.Add(typeof(T), myXmlAttributes);

            // Desserializa no tipo alvo
            ms.Position = 0;
            //XmlSerializer serializer2 = new XmlSerializer(typeof(T), myXmlAttributeOverrides);
            XmlSerializer serializer2 = XmlSerializerCache.Create(typeof(T), myXmlAttributeOverrides, objetoOrigem.GetType().Name);
            T objetoAlvo = (T)serializer2.Deserialize(ms);

            // Retorna
            return objetoAlvo;
        }
        
        public static int DateDiff(char charInterval, DateTime dttFromDate, DateTime dttToDate)
        {
            TimeSpan tsDuration;
            tsDuration = dttToDate - dttFromDate;

            if (charInterval == 'd')
            {
                // Resultado em Dias
                return tsDuration.Days;
            }
            else if (charInterval == 'm')
            {
                // Resultado em Meses
                double dblValue = 12 * (dttFromDate.Year - dttToDate.Year) + dttFromDate.Month - dttToDate.Month;
                return Convert.ToInt32(Math.Abs(dblValue));
            }
            else if (charInterval == 'y')
            {
                // Resultado em Anos
                return Convert.ToInt32((tsDuration.Days) / 365);
            }
            else
            {
                return 0;
            }
        }
    }
}
