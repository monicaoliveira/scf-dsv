﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Configurações para log em arquivo
    /// </summary>
    [Serializable]
    public class LogArquivoConfig
    {
        /// <summary>
        /// Caminho dos arquivos de log
        /// </summary>
        public string CaminhoLog { get; set; }

        /// <summary>
        /// Indica se deve efetuar o log
        /// </summary>
        public bool EfetuarLog { get; set; }

        /// <summary>
        /// Indica se permite o log de objetos
        /// </summary>
        public bool LogarObjetos { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public LogArquivoConfig()
        {
            this.CaminhoLog = @"c:\temp\log";
            this.EfetuarLog = false;
            this.LogarObjetos = false;
        }
    }
}
