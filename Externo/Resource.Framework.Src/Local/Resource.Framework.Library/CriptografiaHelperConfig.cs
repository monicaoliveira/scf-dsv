﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library
{
    [Serializable]
    public class ServicoCriptografiaConfig
    {
        private string _Chave = "";
        public string Chave
        {
            get
            {
                return _Chave;
            }
        }

        public ServicoCriptografiaConfig()
        {
            this._Chave = "AAECAwQFBgcICQoLDA0ODw==";
        }
    }
}
