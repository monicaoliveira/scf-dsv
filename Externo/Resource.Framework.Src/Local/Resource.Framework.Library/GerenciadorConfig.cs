﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Fornece as configurações referentes ao objeto e id solicitados.
    /// </summary>
    public static class GerenciadorConfig
    {
        /// <summary>
        /// Permite que sejam informados configs manuais
        /// </summary>
        private static Dictionary<Type, object> _configuracoesManuais = new Dictionary<Type, object>();

        public static void SetarConfig(object config)
        {
            _configuracoesManuais.Add(config.GetType(), config);
        }

        public static T ReceberConfig<T>()
        {
            T retorno = default(T);
            if (_configuracoesManuais.ContainsKey(typeof(T)))
                retorno = (T)_configuracoesManuais[typeof(T)];
            else
                retorno = (T)ConfigurationManager.GetSection(typeof(T).FullName);

            // Se nao encontrou, cria default
            if (retorno == null)
                retorno = Activator.CreateInstance<T>();

            return retorno;
        }

        public static object ReceberConfig(Type tipo)
        {
            if (_configuracoesManuais.ContainsKey(tipo))
                return _configuracoesManuais[tipo];
            else
                return ConfigurationManager.GetSection(tipo.FullName);
        }

        public static T ReceberConfig<T>(string id)
        {
            if (id != null)
                return (T)ConfigurationManager.GetSection(typeof(T).FullName + "-" + id);
            else
                return ReceberConfig<T>();
        }

        public static object ReceberConfig(Type tipo, string id)
        {
            if (id != null)
                return ConfigurationManager.GetSection(tipo.FullName + "-" + id);
            else
                return ReceberConfig(tipo);
        }
    }
}
