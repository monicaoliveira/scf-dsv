﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Resource.Framework.Library
{   public class ConfigurationHandler : IConfigurationSectionHandler
    {   
        #region IConfigurationSectionHandler Members
        object IConfigurationSectionHandler.Create(object parent, object configContext, System.Xml.XmlNode section)
        {   string[] temp1 = section.Name.Split('-');// Separa id do servico do tipo do servico
            string tipoStr = temp1[0];
            Type tipo = null;// Cria o tipo
            Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in assemblies)
            {
                tipo = assembly.GetType(tipoStr);
                if (tipo != null)
                    break;
            }
            XmlSerializer serializer = XmlSerializerCache.Create(tipo);// Cria o serializador do tipo
            string nodeName = tipoStr.Replace(tipo.Namespace + ".", ""); // Duplica a estrutura para poder passar para o desserializador
            XmlNode node = section.OwnerDocument.CreateElement(nodeName);
            foreach (XmlNode childNode in section.ChildNodes)
                node.AppendChild(childNode.Clone());
            System.Xml.XmlNodeReader reader = new System.Xml.XmlNodeReader(node); // Desserializa
            object info = serializer.Deserialize(reader);
            return info; // Retorna
        }
        #endregion
    }
}
