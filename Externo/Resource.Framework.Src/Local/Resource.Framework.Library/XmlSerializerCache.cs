﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Globalization;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Dicionário de cache para os objetos que serão serializados em XML
    /// Essa Classe foi criada para solucionar o problema de performance nas leituras e 
    /// gravações de Log cujo objeto é serialziado em XML
    /// </summary>
    public static class XmlSerializerCache
    {
        private static readonly object syncRoot = new object();

        private static readonly Dictionary<string, XmlSerializer> cache =
                                new Dictionary<string, XmlSerializer>();

        public static XmlSerializer Create(Type type)
        {
            var key = String.Format(
                      CultureInfo.InvariantCulture,
                      "{0}",
                      type);

            lock (syncRoot)
            {
                if (!cache.ContainsKey(key))
                {
                    cache.Add(key, new XmlSerializer(type));
                }
            }
            return cache[key];
        }

        public static XmlSerializer Create(Type type, Type[] typeList, string typeListName)
        {
            var key = String.Format(
                      CultureInfo.InvariantCulture,
                      "{0}:{1}:{2}",
                      type,
                      typeListName,
                      typeList.Length);
            lock (syncRoot)
            {
                if (!cache.ContainsKey(key))
                {
                    cache.Add(key, new XmlSerializer(type, typeList));
                }
            }
            return cache[key];
        }

        public static XmlSerializer Create(Type type, XmlAttributeOverrides myXmlAttributeOverrides, string myXmlAttributeName)
        {
            var key = String.Format(
                      CultureInfo.InvariantCulture,
                      "{0}:{1}",
                      type,
                      myXmlAttributeName);
            lock (syncRoot)
            {
                if (!cache.ContainsKey(key))
                {
                    cache.Add(key, new XmlSerializer(type, myXmlAttributeOverrides));
                }
            }
            return cache[key];
        }


    }
}
