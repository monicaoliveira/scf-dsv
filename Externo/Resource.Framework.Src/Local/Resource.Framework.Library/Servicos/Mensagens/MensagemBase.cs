﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    /// <summary>
    /// Classe base para implementação de mensagens.
    /// Fornece a identificação única da mensagem através do CodigoMensagem e faz controle de sessão 
    /// através do CodigoSessao.
    /// </summary>
    [Serializable]
    public abstract class MensagemBase : ICodigoEntidade
    {
        /// <summary>
        /// Identificador único da mensagem
        /// </summary>
        public string CodigoMensagem { get; set; }

        /// <summary>
        /// Indica a sessao correspondente da mensagem.
        /// Em casos de solicitação, indica a sessão que está fazendo a solicitação.
        /// Em casos de resposta, indica a sessão que deve ser avisada da resposta.
        /// </summary>
        public string CodigoSessao { get; set; }

        /// <summary>
        /// Data de referencia da mensagem. Geralmente irá indicar a data de criação
        /// da mensagem.
        /// </summary>
        public DateTime DataReferencia { get; set; }

        /// <summary>
        /// Construtor
        /// </summary>
        public MensagemBase()
        {
            // Gera new Guid 
            this.CodigoMensagem = Guid.NewGuid().ToString();
        }
        
        #region ICodigoEntidade Members

        /// <summary>
        /// Retorna o código desta entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return this.CodigoMensagem;
        }

        #endregion

        public override string ToString()
        {
            /*
            XmlSerializer serializer = new XmlSerializer(this.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.Serialize(ms, this);
            ms.Position = 0;
            StreamReader reader = new StreamReader(ms);
            return reader.ReadToEnd();
            */
        
            return this.GetType().Name;

        }
    }
}
