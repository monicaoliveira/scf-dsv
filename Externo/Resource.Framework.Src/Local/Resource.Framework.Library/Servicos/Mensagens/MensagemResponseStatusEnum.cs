using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    public enum MensagemResponseStatusEnum
    {        
        OK = 0
    ,   ErroNegocio = 1
    ,   ErroPrograma = 2
    ,   ErroValidacao = 3
    ,   AcessoNaoPermitido = 4
    ,   SessaoInvalida = 5
    ,   SenhaExpirada = 6
    }
}
