using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    /// <summary>
    /// Lista de status de mensagens
    /// </summary>
    public enum MensagemStatusEnum
    {
        /// <summary>
        /// Indica que esta mensagem foi cancelada por outra mensagem
        /// </summary>
        CanceladaPorOutraMensagem = 4,

        /// <summary>
        /// Indica que está mensagem expirou.
        /// </summary>
        Expirada = 3,

        /// <summary>
        /// Indica que o status não se aplica a este tipo de mensagem.
        /// Mensagens de sinalização irão carregar este status.
        /// </summary>
        NaoSeAplica = 5,

        /// <summary>
        /// Indica que esta mensagem foi respondida por outra.
        /// </summary>
        Respondida = 1,

        /// <summary>
        /// Indica que esta mensagem foi respondida por mais de uma mensagem.
        /// </summary>
        RespondidaMaisDeUmaVez = 2,

        /// <summary>
        /// Indica que a solicitação foi feita. Pode indicar uma espécie de pendência.
        /// </summary>
        SolicitacaoEfetuada = 0
    }
}
