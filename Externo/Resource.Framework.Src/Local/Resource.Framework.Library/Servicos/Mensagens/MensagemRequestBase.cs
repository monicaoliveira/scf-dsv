﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    /// <summary>
    /// Classe base para mensagens de requisição
    /// </summary>
    [Serializable]
    [DataContract]
    public abstract class MensagemRequestBase : MensagemBase
    {
        /// <summary>
        /// Código do Sistema Cliente
        /// </summary>
        [DataMember]
        public string CodigoSistemaCliente { get; set; }

        /// <summary>
        /// Indica se a excessao deve ser repassada
        /// </summary>
        public bool RepassarExcessao { get; set; }

        /// <summary>
        /// Construtor
        /// </summary>
        public MensagemRequestBase()
        {
            this.RepassarExcessao = true;
        }
    }
}
