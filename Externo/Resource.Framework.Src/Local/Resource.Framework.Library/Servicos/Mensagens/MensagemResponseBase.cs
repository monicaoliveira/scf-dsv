﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using Resource.Framework.Library.Servicos.Dados;
using System.Diagnostics;

namespace Resource.Framework.Library.Servicos.Mensagens
{
    /// <summary>
    /// Classe base para mensagens de resposta
    /// </summary>
    [Serializable]
    [DataContract]
    public abstract class MensagemResponseBase : MensagemBase
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Código da mensagem que fez a requisição
        /// </summary>
        [Category("MensagemResponseBase")]
        [DataMember]
        public string CodigoMensagemRequest { get; set; }

        /// <summary>
        /// Indica o status da resposta
        /// </summary>
        [DataMember]
        public MensagemResponseStatusEnum StatusResposta { get; set; }

        /// <summary>
        /// Contem uma descrição da resposta. Caso o status indique algum erro,
        /// esta propriedade irá conter a descrição do erro.
        /// </summary>
        [DataMember]
        public string DescricaoResposta { get; set; }

        /// <summary>
        /// Lista de críticas da mensagem.
        /// As críticas geralmente são colocadas pelas regras do sistema de validação.
        /// </summary>
        [DataMember]
        public List<CriticaInfo> Criticas { get; set; }

        /// <summary>
        /// Data da Resposta
        /// </summary>
        [DataMember]
        public DateTime DataResposta { get; set; }

        /// <summary>
        /// Propriedade de uso geral
        /// </summary>
        public string ResponseTag { get; set; }

        /// <summary>
        /// Indica se a excessao deve ser repassada
        /// </summary>
        public bool RepassarExcessao { get; set; }

        /// <summary>
        /// Permite informar uma excessao
        /// </summary>
        private string _erro = null;
        public string Erro 
        {
            get 
            {
                return _erro;
            }
            set 
            {
                _erro = value;
                this.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
            }
        }

        /// <summary>
        /// Construtor default.
        /// </summary>
        public MensagemResponseBase() : base()
        {
            this.StatusResposta = MensagemResponseStatusEnum.OK;
            this.Criticas = new List<CriticaInfo>();
            this.DataResposta = DateTime.Now;
        }

        /// <summary>
        /// Prepara resposta de acordo com as informações do request
        /// </summary>
        /// <param name="request"></param>
        public void PreparaResposta(MensagemRequestBase request)
        {
            this.CodigoMensagemRequest = request.CodigoMensagem;
            this.CodigoSessao = request.CodigoSessao;
            this.RepassarExcessao = request.RepassarExcessao;
        }

        /// <summary>
        /// Trata a excessao de forma que possa ser serializada na resposta
        /// </summary>
        /// <param name="ex"></param>
        [DebuggerNonUserCode]
        public void ProcessarExcessao(Exception ex)
        {
            log.Error(this, ex);            
            this.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
            this.Erro = ex.ToString();
            this.DescricaoResposta = ex.ToString();
            if (this.RepassarExcessao)
                throw ex;
        }

    }
}
