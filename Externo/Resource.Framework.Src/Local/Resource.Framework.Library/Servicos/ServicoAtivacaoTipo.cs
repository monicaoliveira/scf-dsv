﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Tipos suportados de ativação de serviço.
    /// Local ou WCF.
    /// </summary>
    public enum ServicoAtivacaoTipo
    {
        Local,
        WCF
    }
}
