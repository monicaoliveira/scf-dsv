﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Informações sobre o endpoint wcf
    /// </summary>
    public class ServicoEndPointInfo
    {
        public string Endereco { get; set; }
        public string NomeBindingType { get; set; }
    }
}
