using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Tipos de serializações de eventos
    /// </summary>
    public enum EventoInfoSerializacaoTipoEnum
    {
        Binario = 1,
        Xml = 0,
        XmlProprietario = 2
    }
}
