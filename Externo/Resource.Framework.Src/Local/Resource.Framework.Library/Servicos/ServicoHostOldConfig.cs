﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    public class ServicoHostOldConfig 
    {
        public List<ServicoOldInfo> Servicos { get; set; }

        public ServicoHostOldConfig()
        {
            this.Servicos = new List<ServicoOldInfo>();
        }
    }
}
