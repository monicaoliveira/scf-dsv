﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos.Dados
{
    /// <summary>
    /// Resumo de crítica
    /// </summary>
    public class CriticaResumoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da crítica
        /// </summary>
        public string CodigoCritica { get; set; }

        /// <summary>
        /// Descricao da crítica
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Informa um status da crítica
        /// </summary>
        public CriticaStatusEnum Status { get; set; }

        /// <summary>
        /// Data da crítica
        /// </summary>
        public DateTime DataCritica { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public CriticaResumoInfo()
        {
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoCritica;
        }

        #endregion
    }
}
