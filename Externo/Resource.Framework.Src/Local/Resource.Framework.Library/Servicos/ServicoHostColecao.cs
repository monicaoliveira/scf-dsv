﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Classe estática que contém lista de hosts de serviços.
    /// </summary>
    public class ServicoHostColecao
    {
        private static List<ServicoHostOld> _hosts = new List<ServicoHostOld>();
        public static List<ServicoHostOld> Hosts
        {
            get { return _hosts; }
        }

        public static ServicoHostOld Default
        {
            get 
            {
                if (_hosts.Count == 0)
                    _hosts.Add(new ServicoHostOld());
                return _hosts[0];
            }
        }
    }
}
