﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Resource.Framework.Library.Servicos
{
    /// <summary>
    /// Servico que implementa um diretório de serviços.
    /// Fornece transparencia na ativação dos serviços.
    /// </summary>
    [ServiceContract(Namespace = "http://resource")]
    public interface IServicoLocalizador
    {
        [OperationContract]
        List<ServicoOldInfo> Consultar();

        [OperationContract(Name="ConsultarComServInt")]
        ServicoOldInfo Consultar(string servicoInterface);

        [OperationContract(Name = "ConsultarComServIntEID")]
        ServicoOldInfo Consultar(string servicoInterface, string id);

        [OperationContract]
        void Registrar(ServicoOldInfo servico);

        [OperationContract]
        void Remover(string servicoInterface);

        [OperationContract(Name = "RemoverComID")]
        void Remover(string servicoInterface, string id);
    }
}
