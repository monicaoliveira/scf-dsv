using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Library
{
    /// <summary>
    /// Lista de possíveis comparadores para seleção de nós na árvore
    /// </summary>
    public enum ArvoreComparadorEnum
    {
        ComecaCom = 1,
        Igual = 0
    }
}
