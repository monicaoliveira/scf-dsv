﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net.Layout;
using System.Xml.Serialization;

namespace Resource.Framework.Library
{
    public class Log4NetObjectFormatter : LayoutSkeleton
    {
        public override void Format(System.IO.TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            writer.WriteLine();
            object obj = loggingEvent.MessageObject;
            if (obj != null)
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(writer, obj);
            }
            Exception ex = loggingEvent.ExceptionObject;
            if (ex != null)
            {
                writer.WriteLine(ex.ToString());
            }
        }

        public override void ActivateOptions()
        {
        }
    }
}
