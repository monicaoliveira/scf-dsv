﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library;

namespace Resource.Framework.Hosts.Windows.ServicoHost
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Pega o config
            ContainerServicoHostConfig config = GerenciadorConfig.ReceberConfig<ContainerServicoHostConfig>();
            if (config == null)
                config =
                    new ContainerServicoHostConfig()
                    {
                        ServicoHostConfig = new ServicoHostConfig()
                    };

            // Inicia o host
            Console.WriteLine("Iniciando host...");
            ContainerServicoHost.GetInstance().Iniciar(config);

            // Aguarda
            Console.WriteLine("Host iniciado...");
            Console.ReadLine();

            // Finaliza o host
            Console.WriteLine("Finalizando host...");
            ContainerServicoHost.GetInstance().Finalizar();
            Console.WriteLine("Host finalizado...");
        }
    }
}
