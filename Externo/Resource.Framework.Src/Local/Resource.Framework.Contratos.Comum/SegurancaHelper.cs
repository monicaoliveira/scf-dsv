﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Permissoes;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Classe de auxilio para segurança
    /// </summary>
    public static class SegurancaHelper
    {
        /// <summary>
        /// Verifica se a sessão possui a permissão informada
        /// </summary>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static bool VerificarPermissao(string codigoSessao, Type tipoPermissao)
        {
            // Faz a consulta da permissao
            return
                Mensageria.Processar<VerificarPermissaoResponse>(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        TipoPermissao = tipoPermissao
                    }).Permitido;
        }

        /// <summary>
        /// Verifica se a sessão possui a permissão informada
        /// </summary>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static bool VerificarPermissao(string codigoSessao, string codigoPermissao)
        {
            // Faz a consulta da permissao
            return
                Mensageria.Processar<VerificarPermissaoResponse>(
                    new VerificarPermissaoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoPermissao = codigoPermissao
                    }).Permitido;
        }

        /// <summary>
        /// Retorna informações da sessao informada
        /// </summary>
        /// <param name="codigoSessao"></param>
        /// <returns></returns>
        public static SessaoInfo ReceberSessao(string codigoSessao)
        {
            // Retorna sessao solicitada
            return
                Mensageria.Processar<ReceberSessaoResponse>(
                    new ReceberSessaoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoSessaoARetornar = codigoSessao
                    }).Sessao;
        }

        /// <summary>
        /// Retorna informações do usuário da sessão informada
        /// </summary>
        /// <param name="codigoSessao"></param>
        /// <returns></returns>
        public static UsuarioInfo ReceberUsuario(string codigoSessao)
        {
            // Pega o código do usuário da sessao
            string codigoUsuario = 
                Mensageria.Processar<ReceberSessaoResponse>(
                    new ReceberSessaoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoSessaoARetornar = codigoSessao
                    }).Sessao.CodigoUsuario;
            
            // Faz a consulta do usuário
            return
                Mensageria.Processar<ReceberUsuarioResponse>(
                    new ReceberUsuarioRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoUsuario = codigoUsuario
                    }).Usuario;
        }

        /// <summary>
        /// Retorna lista de usuários
        /// </summary>
        /// <param name="filtroPermissao"></param>
        /// <returns></returns>
        public static List<UsuarioInfo> ListarUsuario(string codigoSessao, Type filtroPermissao)
        {
            // Monta lista de condições
            List<CondicaoInfo> condicaoLista = new List<CondicaoInfo>();

            // Pediu filtro por permissao?
            if (filtroPermissao != null)
                condicaoLista.Add(
                    new CondicaoInfo(
                        "CodigoPermissao", 
                        CondicaoTipoEnum.Igual,
                        ((PermissaoAttribute)filtroPermissao.GetCustomAttributes(typeof(PermissaoAttribute), false).FirstOrDefault()).CodigoPermissao));

            // Pede a lista de usuarios
            List<UsuarioInfo> usuarios = 
                PersistenciaHelper.Listar<UsuarioInfo>(codigoSessao, condicaoLista);

            // Não pode enviar a senha
            usuarios.ForEach(u => u.Senha = null);
            
            // Retorna
            return usuarios;
        }

        /// <summary>
        /// Retorna a lista de usuários baseado em uma lista de permissões
        /// </summary>
        /// <param name="codigoSessao"></param>
        /// <param name="filtroPermissoes"></param>
        /// <returns></returns>
        public static List<UsuarioInfo> ListarUsuario(string codigoSessao, List<Type> filtroPermissoes)
        {
            // Inicia a lista que será retornada
            List<UsuarioInfo> usuarios = new List<UsuarioInfo>();

            // Para cada permissão na lista repassada, adiciona todos os usuários na lista iniciada
            foreach (Type permissao in filtroPermissoes)
            {
                usuarios.AddRange(ListarUsuario(codigoSessao, permissao));
            }

            // Retorna
            return usuarios;
        }
    }
}
