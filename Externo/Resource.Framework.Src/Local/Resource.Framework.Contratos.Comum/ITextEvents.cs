﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.pipeline;

namespace Resource.Framework.Contratos.Comum
{
    public class ITextEvents : PdfPageEventHelper
    {
        // This is the contentbyte object of the writer
        PdfContentByte cb;

        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate;

        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;
        string textoCabec = "";
        string strCSS = "";
        float alturaCabec = 0;
        bool primeiraPagina = true;

        public ITextEvents(string texto, string css)
        {
            textoCabec = texto;
            strCSS = css;
        }

        public float retornaAlturaCabec()
        {
            return alturaCabec;
        }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(50, 50);
            }
            catch (DocumentException)
            {

            }
            catch (System.IO.IOException)
            {

            }
        }

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            ColumnText ct = new ColumnText(writer.DirectContent);
            //Desabilitei porque não usa o CSS na impressão do cabeçalho
            //XMLWorkerHelper.GetInstance().ParseXHtml(new ColumnTextElementHandler(ct), new StringReader(textoCabec));

            ct.SetSimpleColumn(new Rectangle(2, 3, document.PageSize.Width - 3, document.PageSize.Height - 2));

            List<IElement> elements = XMLWorkerHelper.ParseToElementList(textoCabec, strCSS);
            foreach (IElement el in elements)
            {
                if (el.GetType().Name != "PdfDiv")
                    ct.AddElement(el);

                if (el.GetType().Name == "PdfPTable" && alturaCabec == 0)
                {
                    PdfPTable tabelaCabec = el as PdfPTable;
                    alturaCabec = tabelaCabec.TotalHeight;
                }
            }

            if (!primeiraPagina)
            {
                // Se não for primeira página, que sairia em branco, eu imprimo o cabeçalho
                ct.Go();
            }
            primeiraPagina = false;
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            // seta a fonte do objeto PdfContentByte
            cb.SetFontAndSize(bf, 9);

            // escreve a linha para imprimir o numero da página
            string texto = "Página: " + writer.PageNumber.ToString() + " de ";
            float len = bf.GetWidthPoint(texto, 9) + 20;

            // imprime a linha no rodapé
            cb.BeginText();
            cb.SetFontAndSize(bf, 9);
            cb.SetTextMatrix(document.PageSize.Width - len, document.PageSize.GetBottom(10));
            cb.ShowText(texto);
            cb.EndText();
            cb.AddTemplate(footerTemplate, document.PageSize.Width - 20, document.PageSize.GetBottom(10));
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 9);
            footerTemplate.SetTextMatrix(0, 0);
            footerTemplate.ShowText((writer.PageNumber - 1).ToString());
            footerTemplate.EndText();
        }

        public class ColumnTextElementHandler : IElementHandler
        {
            ColumnText ct = null;
            //string textoCabec; //warning 25/02/2019
            //string strCSS; //warning 25/02/2019

            public ColumnTextElementHandler(ColumnText ct)
            {
                this.ct = ct;
            }

            public void Add(IWritable w)
            {
                if (w is WritableElement)
                {
                    foreach (IElement e in ((WritableElement)w).Elements())
                    {
                        ct.AddElement(e);
                    }
                }
            }
        }
        /// <summary>
        /// Função para converter o relatório HTML em PDF
        /// </summary>
        /// <param name="dados"></param>
        /// <param name="strCSS"></param>
        /// <returns></returns>
        public Byte[] ImprimirPDF(string dados, string strCSS, string layoutPagina)
        {
            //Estou usando a função abaixo para que possa sair correto no PDF
            string data = htmlEncodeAcentos(dados);
            string cabec = "";

            //Montando o cabealho do relatório
            int posIni = data.IndexOf("<header>");
            if (posIni > 0)
            {
                int posFim = data.IndexOf("</header>") + 9;
                cabec = data.Substring(posIni, posFim - posIni);
                data = data.Replace(cabec, "");
            }

            // MOntando layout do relatório
            Rectangle tamanhoPagina = iTextSharp.text.PageSize.A4;

            string[] pagina = layoutPagina.Split(',');
            float alturaPagina = 0;
            float larguraPagina = 0;

            if (float.TryParse(pagina[0], out alturaPagina))
            {
                float.TryParse(pagina[1], out larguraPagina);
                tamanhoPagina = new RectangleReadOnly(alturaPagina, larguraPagina);
            }
            else
            {
                if (layoutPagina == "A4,Rotate")
                    tamanhoPagina = iTextSharp.text.PageSize.A4.Rotate();
            }

            //Create a byte array that will eventually hold our final PDF
            Byte[] bytes;

            //Boilerplate iTextSharp setup here
            //Create a stream that we can write to, in this case a MemoryStream
            using (var ms = new MemoryStream())
            {
                //Create an iTextSharp Document which is an abstraction of a PDF but **NOT** a PDF
                using (var doc = new Document(tamanhoPagina, 2, 3, 20, 20)) //Edgar
                {
                    //Create a writer that's bound to our PDF abstraction and our stream
                    using (PdfWriter writer = PdfWriter.GetInstance(doc, ms))
                    {
                        //Criando um Evento ao writer para substituir as funções do Cabeçalho e Rodapé
                        ITextEvents ev = new ITextEvents(cabec, strCSS);
                        writer.PageEvent = ev;

                        //Open the document for writing
                        doc.Open();
                        doc.AddAuthor("ResourceIT");
                        doc.AddCreator("Carrefour - Sistema Consolidador Financeiro");

                        // Recuperando altura do cabeçalho e setando a página do documento
                        float marginTop = ev.retornaAlturaCabec();
                        doc.SetMargins(2, 3, marginTop, 20);

                        // Forçando a impressão do cabeçalho na primeira página
                        doc.NewPage();
                        ev.OnStartPage(writer, doc);

                        try
                        {
                            //Desabilitei por não precisar no formato StringReader
                            //XMLWorker also reads from a TextReader and not directly from a string
                            //using (var srHtml = new StringReader(data))
                            {
                                writer.SetMargins(0, 0, 0, 0);

                                //Parse the HTML
                                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, srHtml);

                                //Substitui a função acima pelo código abaixo para habilitar o estilo repeat-header nas tabelas
                                List<IElement> elements = XMLWorkerHelper.ParseToElementList(data, strCSS);
                                foreach (IElement el in elements)
                                {
                                    if (el.GetType().Name != "PdfDiv")
                                        doc.Add(el);
                                }

                            }
                        }
                        catch (Exception  ){ } //   catch (Exception ex) { } //warning 25/02/2019
                        finally
                        {
                            doc.Close();
                        }
                    }
                }

                //After all of the PDF "stuff" above is done and closed but **before** we
                //close the MemoryStream, grab all of the active bytes from the stream
                bytes = ms.ToArray();
            }
            return bytes;

        }
        /// <summary>
        /// Substitui as letras com acentos por formato HTML
        /// </summary>
        /// <param name="textocomAcentos"></param>
        /// <returns></returns>
        public string htmlEncodeAcentos(string textocomAcentos)
        {
            string textocomAcentosHTML = textocomAcentos;
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "©", "&copy;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "®", "&reg;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Á", "&Aacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "á", "&aacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "À", "&Agrave;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "à", "&agrave;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ã", "&atilde;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "â", "&acirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "å", "&aring;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ã", "&Atilde;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Â", "&Acirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Å", "&Aring;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ä", "&auml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ä", "&Auml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Æ", "&AElig;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "æ", "&aelig;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ç", "&Ccedil;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ç", "&ccedil;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ð", "&eth;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ð", "&ETH;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "é", "&eacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "è", "&egrave;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "É", "&Eacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ê", "&ecirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "È", "&Egrave;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ê", "&Ecirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ë", "&euml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ë", "&Euml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "í", "&iacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ì", "&igrave;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Í", "&Iacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "î", "&icirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ì", "&Igrave;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Î", "&Icirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ï", "&iuml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ï", "&Iuml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ñ", "&Ntilde;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ñ", "&ntilde;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ó", "&Oacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ó", "&oacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ò", "&Ograve;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ò", "&ograve;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ø", "&oslash;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "õ", "&otilde;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ô", "&Ocirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ø", "&Oslash;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Õ", "&Otilde;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ô", "&ocirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ö", "&ouml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ö", "&Ouml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ß", "&szlig;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Þ", "&THORN;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "þ", "&thorn;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ù", "&Ugrave;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ú", "&Uacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ù", "&ugrave;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ú", "&uacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Û", "&Ucirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "û", "&ucirc;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ü", "&Uuml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ü", "&uuml;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "Ý", "&Yacute;");
            textocomAcentosHTML = Regex.Replace(textocomAcentosHTML, "ý", "&yacute;");

            return textocomAcentosHTML;
        }
    }
}
