﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library.Servicos;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Interface para o serviço de mensageria
    /// </summary>
    [ServiceContract(Namespace = "http://resource")]
    [ServiceKnownType("RetornarTipos", typeof(LocalizadorTiposHelper))]
    public interface IServicoMensageria
    {
        /// <summary>
        /// Processa a mensagem solicitada.
        /// Faz o roteamento da mensagem para o devido serviço
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        [OperationContract]
        MensagemResponseBase ProcessarMensagem(MensagemRequestBase parametros);
    }
}
