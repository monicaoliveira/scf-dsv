﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Permissoes;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Classe de auxilio para manter a lista de permissões
    /// </summary>
    [Serializable]
    public class ListaPermissoesHelper
    {
        /// <summary>
        /// Lista de permissoes por código da permissao
        /// </summary>
        public List<PermissaoPorCodigo> ListaPorCodigo { get; set; }

        /// <summary>
        /// Lista de permissoes por tipo da permissao
        /// </summary>
        public List<PermissaoPorTipo> ListaPorTipo { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListaPermissoesHelper()
        {
            this.ListaPorCodigo = new List<PermissaoPorCodigo>();
            this.ListaPorTipo = new List<PermissaoPorTipo>();
        }

        /// <summary>
        /// Adiciona uma lista de permissoes na colecao
        /// </summary>
        /// <param name="permissao"></param>
        public void AdicionarPermissoes(List<PermissaoBase> permissoes)
        {
            // Varre a lista adicionando
            foreach (PermissaoBase permissao in permissoes)
                this.AdicionarPermissao(permissao);
        }

        /// <summary>
        /// Adiciona uma nova permissao nas coleções
        /// </summary>
        /// <param name="permissao"></param>
        public void AdicionarPermissao(PermissaoBase permissao)
        {
            Type tipoPermissao = permissao.GetType();

            
            if(this.ListaPorCodigo.FirstOrDefault(p=> p.Codigo == permissao.PermissaoInfo.CodigoPermissao) == null)
                this.ListaPorCodigo.Add(new PermissaoPorCodigo(){Codigo = permissao.PermissaoInfo.CodigoPermissao, Permissao = permissao});
            if (this.ListaPorTipo.FirstOrDefault(p=> p.Tipo == tipoPermissao) == null)
                this.ListaPorTipo.Add(new PermissaoPorTipo() { Tipo = tipoPermissao, Permissao = permissao });
        }

        /// <summary>
        /// Carrega as pemissoes encontradas nos namespaces informados.
        /// A string do namespace deve estar no formato namespace, assembly
        /// </summary>
        /// <param name="namespaces"></param>
        public void CarregarPermissoes(List<string> namespaces)
        {
            // Varre a lista
            foreach (string ns1 in namespaces)
            {
                // Separa o nome do namespace e o nome do assembly
                string[] ns2 = ns1.Split(',');
                string ns = ns2[0].Trim();
                string nomeAssembly = ns2[1].Trim();

                // Pega referencia ao assembly
                Assembly assembly = Assembly.Load(nomeAssembly);

                // Faz a varredura em cima dos tipos adicionando os que fazem parte do namespace informado
                foreach (Type tipo in assembly.GetTypes())
                    if (tipo.IsSubclassOf(typeof(PermissaoBase)) && tipo.Namespace.StartsWith(ns))
                        this.AdicionarPermissao(
                            (PermissaoBase)
                                Activator.CreateInstance(tipo));
            }
        }

        /// <summary>
        /// Construtor que recebe a lista de permissoes iniciais a serem carregadas
        /// </summary>
        /// <param name="permissoes"></param>
        public ListaPermissoesHelper(List<PermissaoBase> permissoes) : this()
        {
            this.AdicionarPermissoes(permissoes);
        }
    }
}
