﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Classe de auxilio para persistencia
    /// </summary>
    public static class PersistenciaHelper
    {
        /// <summary>
        /// Salva o objeto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static T Salvar<T>(string codigoSessao, T objeto)
        {
            // Salva o objeto
            return                 
                Mensageria.Processar<SalvarObjetoResponse<T>>(
                    new SalvarObjetoRequest<T>() 
                    { 
                        CodigoSessao = codigoSessao,
                        Objeto = objeto
                    }).Objeto;

        }

        /// <summary>
        /// Recebe detalhe do objeto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static T Receber<T>(string codigoSessao, string codigoObjeto)
        {
            // Recebe o objeto
            return
                Mensageria.Processar<ReceberObjetoResponse<T>>(
                    new ReceberObjetoRequest<T>()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoObjeto = codigoObjeto
                    }).Objeto;

        }

        /// <summary>
        /// Remove o objeto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static void Remover<T>(string codigoSessao, string codigoObjeto)
        {
            // Remove o objeto
            Mensageria.Processar<RemoverObjetoResponse<T>>(
                new RemoverObjetoRequest<T>()
                {
                    CodigoSessao = codigoSessao,
                    CodigoObjeto = codigoObjeto
                });
        }

        /// <summary>
        /// Lista todos os objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static List<T> Listar<T>(string codigoSessao)
        {
            // Lista os objetos
            return PersistenciaHelper.Listar<T>(codigoSessao, null);
        }

        /// <summary>
        /// Lista objetos com condições
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static List<T> Listar<T>(string codigoSessao, List<CondicaoInfo> condicoes)
        {
            // Lista os objetos
            return PersistenciaHelper.Listar<T>(codigoSessao, condicoes, 100);
        }

        /// <summary>
        /// Lista objetos com condições
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static List<T> Listar<T>(string codigoSessao, List<CondicaoInfo> condicoes, int maxLinhas)
        {
            // Lista os objeto
            return
                Mensageria.Processar<ConsultarObjetosResponse<T>>(
                    new ConsultarObjetosRequest<T>()
                    {
                        CodigoSessao = codigoSessao,
                        Condicoes = condicoes,
                        MaxLinhas = maxLinhas
                    }).Resultado;
        }
    }
}
