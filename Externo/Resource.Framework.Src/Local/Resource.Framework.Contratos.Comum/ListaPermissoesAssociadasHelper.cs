﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Permissoes;
using Resource.Framework.Library.Servicos;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Classe de auxilio 
    /// </summary>
    [Serializable]
    public class ListaPermissoesAssociadasHelper
    {
        /// <summary>
        /// Lista das permissoes por tipo
        /// </summary>
        public List<PermissaoPorTipo> ListaPorTipo { get; set; }

        /// <summary>
        /// Lista das permissoes por código
        /// </summary>
        public List<PermissaoPorCodigo> ListaPorCodigo { get; set; }

        /// <summary>
        /// Lista de permissoes negadas
        /// </summary>
        public List<Type> PermissoesNegadas { get; set; }

        /// <summary>
        /// Indica se esta lista está representando a lista do administrador
        /// </summary>
        public bool EhAdministrador { get; set; }

        /// <summary>
        /// Contem a lista de permissoes para que as traduções possam ser realizadas
        /// </summary>
        public ListaPermissoesHelper ListaPermissoes { get; set; }
        
        /// <summary>
        /// Construtor default
        /// </summary>
        public ListaPermissoesAssociadasHelper()
        {
            this.ListaPorCodigo = new List<PermissaoPorCodigo>();
            this.ListaPorTipo = new List<PermissaoPorTipo>();
            this.PermissoesNegadas = new List<Type>();
            
            // Recebe a lista de permissoes
            this.ListaPermissoes = 
                new ListaPermissoesHelper(
                    Mensageria.Processar<ListarPermissoesResponse>(
                        new ListarPermissoesRequest()).Permissoes);

        }
        
        /// <summary>
        /// Adiciona a lista informada
        /// </summary>
        public void AdicionarPermissoes(List<PermissaoAssociadaInfo> permissoesAssociadas)
        {
            // Varre as permissoes a serem adicionadas aplicando a regra de associacao
            foreach (PermissaoAssociadaInfo permissaoAssociada in permissoesAssociadas)
                this.AdicionarPermissao(permissaoAssociada);
        }

        public void AdicionarPermissao(PermissaoAssociadaInfo permissaoAssociada)
        {
            // TEMP - Apenas se existir a permissao
            if (this.ListaPermissoes.ListaPorCodigo.FirstOrDefault(p => p.Codigo == permissaoAssociada.CodigoPermissao) == null) return;

            // Acha a permissao base e o tipo
            PermissaoBase permissao = this.ListaPermissoes.ListaPorCodigo.FirstOrDefault(p=> p.Codigo == permissaoAssociada.CodigoPermissao).Permissao;
            Type tipoPermissao = permissao.GetType();
            
            // Se já existe e é negação, remove
            PermissaoPorTipo pt = this.ListaPorTipo.FirstOrDefault(p=> p.Tipo == tipoPermissao);
            bool contemPermissao = pt != null;
            if (contemPermissao && permissaoAssociada.Status == PermissaoAssociadaStatusEnum.Negado)
            {
                this.ListaPorTipo.Remove(pt);
            }
            // Se não existe e é permissão, insere
            else if (!contemPermissao && permissaoAssociada.Status == PermissaoAssociadaStatusEnum.Permitido && !this.PermissoesNegadas.Contains(tipoPermissao))
            {
                this.ListaPorTipo.Add(new PermissaoPorTipo() { Tipo = tipoPermissao, Permissao = permissao });
                this.ListaPorCodigo.Add(new PermissaoPorCodigo() { Codigo = permissao.PermissaoInfo.CodigoPermissao, Permissao = permissao });
            }

            // Se for permissao negada, tem que contar na lista de negação
            if (permissaoAssociada.Status == PermissaoAssociadaStatusEnum.Negado && !this.PermissoesNegadas.Contains(tipoPermissao))
                this.PermissoesNegadas.Add(tipoPermissao);
        }

        /// <summary>
        /// Consulta se o usuário tem a pemissão informada válida
        /// Overload que consulta por tipo da permissao
        /// </summary>
        /// <param name="sessao"></param>
        /// <returns></returns>
        public bool ConsultarPermissao(Type tipoPermissao)
        {
            if (this.EhAdministrador)
                return true;
            PermissaoPorTipo pt = this.ListaPorTipo.FirstOrDefault(prop => prop.Tipo == tipoPermissao);
            if (pt != null)
                return pt.Permissao.ValidarPermissao();
            else
                return false;
        }

        /// <summary>
        /// Consulta se o usuário tem a pemissão informada válida.
        /// Overload que consulta por código da permissao
        /// ** Precisa criar os overloads recebendo sessao para repassar 
        /// ** para o método ValidarPermissao
        /// </summary>
        /// <param name="sessao"></param>
        /// <returns></returns>
        public bool ConsultarPermissao(string codigoPermissao)
        {
            if (this.ListaPorTipo.FirstOrDefault(prop=> prop.Tipo == (typeof(PermissaoAdministrador))) != null)
                return true;
            PermissaoPorCodigo pc = this.ListaPorCodigo.FirstOrDefault(p => p.Codigo == codigoPermissao);
            if (pc != null)
                return pc.Permissao.ValidarPermissao();
            else
                return false;
        }
    }
}
