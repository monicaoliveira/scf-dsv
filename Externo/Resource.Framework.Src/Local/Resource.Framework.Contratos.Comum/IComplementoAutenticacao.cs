﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Classe para implementação de complementos de autenticação.
    /// Permite que um complemento realize outras validações no usuário
    /// e adicione informações pertinentes.
    /// </summary>
    [InterfaceServico(Incluir=false)]
    public interface IComplementoAutenticacao
    {
        /// <summary>
        /// Método disparado no momento da autenticação.
        /// Permite que o complemento adicione informações na sessão do usuário
        /// </summary>
        /// <returns>Mensagem de resposta indicando se a autenticação está ok</returns>
        AutenticarUsuarioResponse ComplementarAutenticacao(AutenticarUsuarioRequest parametros, SessaoInfo sessaoInfo);

        /// <summary>
        /// Faz a complementação do usuário, caso necessário
        /// </summary>
        /// <param name="usuarioInfo"></param>
        void ComplementarUsuario(UsuarioInfo usuarioInfo);

        /// <summary>
        /// Chamada realizada quando o serviço de autenticação não
        /// encontra o usuário solicitado. Permite que o complemento crie e
        /// retorne um usuário.
        /// </summary>
        /// <returns></returns>
        UsuarioInfo LocalizarUsuario(AutenticarUsuarioRequest parametros);
    }
}
