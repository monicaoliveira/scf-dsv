﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Atributo para indicar o campo chave
    /// </summary>
    public class PersistenciaArquivoAttribute : Attribute
    {
        /// <summary>
        /// Indica qual é o campo chave da classe
        /// </summary>
        public string CampoChave { get; set; }
    }
}
