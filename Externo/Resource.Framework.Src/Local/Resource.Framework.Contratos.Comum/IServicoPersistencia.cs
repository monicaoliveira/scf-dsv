﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Framework.Contratos.Comum
{
	/// <summary>
	/// Interface para o serviço de persistencia 
	/// </summary>
    [ServiceContract(Namespace = "http://resource")]
	public interface IServicoPersistencia : IPersistencia
    {
        /// <summary>
        /// Solicita que o serviço adicione a persistencia informada, relacionando
        /// aos tipos de objeto informados
        /// </summary>
        /// <param name="persistenciaInfo"></param>
        void AdicionarPersistencia(PersistenciaInfo persistenciaInfo);        
    }
}
