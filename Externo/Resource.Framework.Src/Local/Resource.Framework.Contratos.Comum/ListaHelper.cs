﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum
{
    public static class ListaHelper
    {
        public static ListaItemInfo ReceberListaItem(string codigoSessao, string mnemonicoLista, string codigoOuMnemonicoListaItem)
        {
            // Prepara o retorno
            ListaItemInfo listaItemInfo = null;
            
            // Acha a lista
            List<ListaInfo> listas = 
                PersistenciaHelper.Listar<ListaInfo>(
                    codigoSessao, 
                    new List<CondicaoInfo>() 
                    { 
                        new CondicaoInfo("Mnemonico", CondicaoTipoEnum.Igual, mnemonicoLista)
                    });

            // Achou?
            if (listas.Count > 0)
            {
                // Referencia para a lista
                ListaInfo lista = listas[0];
                
                // Procura pelo item
                listaItemInfo = lista.Itens.Find(i => i.CodigoListaItem == codigoOuMnemonicoListaItem);
                if (listaItemInfo == null)
                    listaItemInfo = lista.Itens.Find(i => i.Mnemonico == codigoOuMnemonicoListaItem);
            }

            // Retorna
            return listaItemInfo;
        }
    }
}
