﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Reflection;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Implementacao da validação de seguranca para o servicoHost
    /// </summary>
    public class ServicoHostValidacaoPermissao : IServicoHostValidacaoPermissao
    {
        /// <summary>
        /// Cache interno para os atributos dos métodos
        /// </summary>
        private Dictionary<MethodInfo, List<object>> _dicionario = new Dictionary<MethodInfo, List<object>>();
        
        #region IServicoHostValidacaoSeguranca Members

        public bool ValidarSeguranca(MensagemRequestBase request, MethodInfo methodInfo)
        {
            // Inicializa
            List<object> atributos = null;
            
            // Verifica se tem no cache
            if (!_dicionario.ContainsKey(methodInfo))
            {
                // Inicializa a lista de atributos
                atributos = new List<object>();

                // Verifica se o método, ou se a mensagem, tem validação de permissao
                atributos.AddRange(
                    methodInfo.GetCustomAttributes(
                        typeof(ValidacaoPermissaoAttribute), true));
                atributos.AddRange(
                    request.GetType().GetCustomAttributes(
                        typeof(ValidacaoPermissaoAttribute), true));

                // Adiciona no cache
                _dicionario.Add(methodInfo, atributos);
            }
            else
            {
                // Pega lista existente
                atributos = _dicionario[methodInfo];
            }

            // Faz os testes necessários
            foreach (object atributo in atributos)
            {
                // Pega com o tipo correto
                ValidacaoPermissaoAttribute atributo2 = (ValidacaoPermissaoAttribute)atributo;
                
                // Valida o atributo isolado
                if (atributo2.Permissao != null && !SegurancaHelper.VerificarPermissao(request.CodigoSessao, atributo2.Permissao))
                    return false;

                // Valida a colecao de atributos. Qualquer permissao encontrada permite a execução
                if (atributo2.PermissaoLista != null)
                {
                    bool achou1 = false;
                    foreach (Type tipo in atributo2.PermissaoLista)
                    {
                        if (SegurancaHelper.VerificarPermissao(request.CodigoSessao, tipo))
                        {
                            achou1 = true;
                            break;
                        }
                    }
                    if (!achou1)
                        return false;
                }
            }

            // Retorna true como default
            return true;
        }

        #endregion
    }
}
