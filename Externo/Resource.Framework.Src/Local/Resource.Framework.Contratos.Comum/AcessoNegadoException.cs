﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Excecao de acesso negado
    /// </summary>
    public class AcessoNegadoException : Exception
    {
        public AcessoNegadoException()
            : base()
        {
        }

        public AcessoNegadoException(string message)
            : base(message)
        {
        }

        public AcessoNegadoException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
