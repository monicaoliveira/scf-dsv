﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // 22FF518C-C7D3-4ff0-A0CB-96F2476068BB
    [Serializable]
    [Permissao(
        CodigoPermissao = "1",
        NomePermissao = "LOGIN - Acessar Sistema",
        DescricaoPermissao = "Indica se o usuário tem permissão para acessar o sistema")]
    public class PermissaoAcessarSistema : PermissaoBase
    {
    }
}
