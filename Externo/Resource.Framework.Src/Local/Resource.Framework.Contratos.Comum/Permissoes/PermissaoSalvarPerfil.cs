﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // 7ED6A969-8C59-4e62-8BE7-CEB83134FF62
    [Permissao(
        CodigoPermissao = "15",
        NomePermissao = "ADM - Salvar Perfil",
        DescricaoPermissao = "Indica se o usuário tem permissão para salvar pefis, tanto novos quanto existentes")]
    [Serializable]
    public class PermissaoSalvarPerfil : PermissaoBase
    {
    }
}
