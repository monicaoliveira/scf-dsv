﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // 23BBC31B-DD05-4c41-9BC6-5A814FB2D31C
    [Permissao(
        CodigoPermissao = "14",
        NomePermissao = "ADM - Remover Grupo de Usuário",
        DescricaoPermissao = "Indica se o usuário tem permissão para remover um grupo de usuário")]
    [Serializable]
    public class PermissaoRemoverUsuarioGrupo : PermissaoBase
    {
    }
}
