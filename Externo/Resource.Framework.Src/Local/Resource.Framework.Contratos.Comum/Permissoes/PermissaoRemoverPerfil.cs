﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // 54DDD6FF-871B-49cd-B3F6-745BAE281AEB
    [Permissao(
        CodigoPermissao = "12",
        NomePermissao = "ADM - Remover Perfil",
        DescricaoPermissao = "Indica se o usuário tem permissão para remover um perfil")]
    [Serializable]
    public class PermissaoRemoverPerfil : PermissaoBase
    {
    }
}
