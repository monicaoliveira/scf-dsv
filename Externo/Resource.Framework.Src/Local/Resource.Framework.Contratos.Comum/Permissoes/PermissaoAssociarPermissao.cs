﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // F3C957F0-1C4B-4ad5-8D39-C307F7237314
    [Permissao(
        CodigoPermissao = "4",
        NomePermissao = "ADM - Associar Permissão",
        DescricaoPermissao = "Indica se o usuário tem permissão para associar permissões a outras entidades (Usuário, Grupo, Perfil)")]
    [Serializable]
    public class PermissaoAssociarPermissao : PermissaoBase
    {
    }
}
