﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // 626EC9D9-D36E-4378-9761-BCA51F5D76B6
    [Permissao(
        CodigoPermissao = "5",
        NomePermissao = "ADM - Associar Grupo de Usuário",
        DescricaoPermissao = "Indica se o usuário tem permissão para associar grupos de usuários a Usuários")]
    [Serializable]
    public class PermissaoAssociarUsuarioGrupo : PermissaoBase
    {
    }
}
