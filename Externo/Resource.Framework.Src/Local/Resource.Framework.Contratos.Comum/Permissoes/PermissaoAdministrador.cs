﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // 9C5DA26B-8C30-4c1d-AA7A-B7A22CF2CA8F
    [Permissao(
        CodigoPermissao = "2",
        NomePermissao = "ADM - Administrador",
        DescricaoPermissao = "Indica que o usuário tem acesso a todas as funções do sistema")]
    [Serializable]
    public class PermissaoAdministrador : PermissaoBase
    {
    }
}
