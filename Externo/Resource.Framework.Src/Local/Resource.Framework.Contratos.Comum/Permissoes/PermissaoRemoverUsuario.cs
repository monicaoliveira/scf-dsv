﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // B4F0748A-C5F5-4b39-9EC1-FBB15D1C1EA3
    [Permissao(
        CodigoPermissao = "13",
        NomePermissao = "ADM - Remover Usuário",
        DescricaoPermissao = "Indica se o usuário tem permissão para remover um usuário")]
    [Serializable]
    public class PermissaoRemoverUsuario : PermissaoBase
    {
    }
}
