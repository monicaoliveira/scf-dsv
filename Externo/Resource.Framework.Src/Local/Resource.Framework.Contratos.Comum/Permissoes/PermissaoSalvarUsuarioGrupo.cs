﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // 0D08C6D1-3CD1-490d-9868-E2567ED25F7A
    [Permissao(
        CodigoPermissao = "17",
        NomePermissao = "ADM - Salvar Grupo de Usuário",
        DescricaoPermissao = "Indica se o usuário tem permissão para salvar grupos de usuários, tanto novos quanto existentes")]
    [Serializable]
    public class PermissaoSalvarUsuarioGrupo : PermissaoBase
    {
    }
}
