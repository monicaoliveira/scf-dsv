﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // A0FE5EA1-86FF-4efb-A7DE-7A24996E4080
    [Permissao(
        CodigoPermissao = "10",
        NomePermissao = "ADM - Receber Usuário",
        DescricaoPermissao = "Indica se o usuário tem permissão para ler um usuário")]
    [Serializable]
    public class PermissaoReceberUsuario : PermissaoBase
    {
    }
}
