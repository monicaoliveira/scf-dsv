﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Permissoes
{
    // A5488A98-0EB4-49df-8100-2815CC2D4DA2
    [Permissao(
        CodigoPermissao = "8",
        NomePermissao = "ADM - Listar Usuários",
        DescricaoPermissao = "Indica se o usuário tem permissão para listar usuários")]
    [Serializable]
    public class PermissaoListarUsuarios : PermissaoBase
    {
    }
}
