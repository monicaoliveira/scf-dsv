﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Contem informacoes de resumo do log 
    /// </summary>
    [Serializable]
    public class LogResumoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Chave primaria
        /// </summary>
        public string CodigoLog { get; set; }

        /// <summary>
        /// Indica o tipo do log
        /// </summary>
        public string TipoLog { get; set; }

        /// <summary>
        /// Descricao do log
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Código do usuário que gerou o log
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Data e hora de ocorrencia
        /// </summary>
        public DateTime DataLog { get; set; }

        /// <summary>
        /// Data e hora de ocorrencia no Banco de Dados
        /// </summary>
        public DateTime DataBDLog { get; set; }

        /// <summary>
        /// Tipo da origem
        /// </summary>
        public string TipoOrigem { get; set; }

        /// <summary>
        /// Código da origem
        /// </summary>
        public string CodigoOrigem { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public LogResumoInfo()
        {
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoLog;
        }

        #endregion
    }
}
