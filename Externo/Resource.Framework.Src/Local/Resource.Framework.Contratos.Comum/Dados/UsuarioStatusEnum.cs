using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Contém a lista de status de usuário
    /// </summary>
    public enum UsuarioStatusEnum
    {
        Desabilitado = 2,
        Habilitado = 1,
        NaoInformado = 0
    }
}
