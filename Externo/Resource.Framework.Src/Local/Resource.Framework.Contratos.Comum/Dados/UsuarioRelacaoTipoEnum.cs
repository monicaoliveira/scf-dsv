using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Tipos de relações entre usuários
    /// </summary>
    public enum UsuarioRelacaoTipoEnum
    {
        Assessoria = 2,
        Diretoria = 0,
        Gerencia = 1
    }
}
