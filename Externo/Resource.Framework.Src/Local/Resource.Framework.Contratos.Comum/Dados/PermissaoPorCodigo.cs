﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Permissoes;

namespace Resource.Framework.Contratos.Comum.Dados
{
    [Serializable]
    public class PermissaoPorCodigo
    {
        public string Codigo { get; set; }

        public PermissaoBase Permissao { get; set; }
    }
}
