﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Contem informacoes de log de parametrizacao.
    /// Serve de base para as demais classes de log
    /// </summary>
    [Serializable]
    [PersistenciaArquivo(CampoChave = "CodigoLog")]
    public class LogInfo : ICodigoEntidade
    {
        /// <summary>
        /// Chave primaria
        /// </summary>
        public string CodigoLog { get; set; }

        /// <summary>
        /// String de identificacao para auxiliar a leitura do
        /// log por quem não recebe informacoes do tipo da classe
        /// Por exemplo: javascript
        /// </summary>
        public string TipoLog { get; set; }

        /// <summary>
        /// Descricao do log
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Código do usuário que gerou o log
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Data e hora de ocorrencia
        /// </summary>
        public DateTime DataLog { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public LogInfo()
        {
            this.DataLog = DateTime.Now;
            this.TipoLog = this.GetType().Name;
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoLog;
        }

        #endregion
    }
}
