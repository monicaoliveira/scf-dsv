﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Contem informações da associação da permissão com outra entidade.
    /// Esta classe é necessária para, por exemplo, indicar se a associação está 
    /// negando ou permitindo a permissão.
    /// </summary>
    [Serializable]
    public class PerfilAssociadoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da permissão associada
        /// </summary>
        public string CodigoPerfil { get; set; }

        /// <summary>
        /// Detalhe da permissão.
        /// Opcionalmente pode ser preenchido com o PerfilInfo ao invés
        /// de apenas o código como na propriedade CodigoPerfil. Utilizado para facilitar o 
        /// preenchimento das telas cadastrais.
        /// </summary>
        public PermissaoInfo PerfilInfo { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoPerfil;
        }

        #endregion
    }
}
