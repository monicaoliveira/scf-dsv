using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Lista de tipos de condições
    /// </summary>
    public enum CondicaoTipoEnum
    {
        Diferente = 1,
        FazParteDaLista = 6,
        Igual = 0,
        Maior = 2,
        MaiorIgual = 3,
        Menor = 4,
        MenorIgual = 5,
        NaoFazParteDaLista = 7,
        TerminaCom = 8,
        ComecaCom = 9,
        Contem = 10
    }
}
