﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Dados dos veículos.
    /// </summary>
    [Serializable]
    public class VeiculoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Codigo do veiculo;
        /// </summary>
        public string CodigoVeiculo { get; set; }

        /// <summary>
        /// Tipo do veiculo;
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// Fabricante do veiculo;
        /// </summary>
        public string Fabricante { get; set; }

        /// <summary>
        /// Modelo do veiculo;
        /// </summary>
        public string Modelo { get; set; }

        public VeiculoInfo()
        {
            this.CodigoVeiculo = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoVeiculo;
        }

        #endregion
    }
}
