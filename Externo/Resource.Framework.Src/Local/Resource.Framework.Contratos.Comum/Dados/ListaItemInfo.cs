﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Representa um item numa lista de metadados.
    /// Geralmente terá uma relação com um item de
    /// enumerador, ou um tipo de classe, etc.
    /// </summary>
    [Serializable]
    public class ListaItemInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do item da lista
        /// </summary>
        public string CodigoListaItem { get; set; }

        /// <summary>
        /// Código da lista ao qual pertence o item
        /// </summary>
        public string CodigoLista { get; set; }

        /// <summary>
        /// Descricao do elemento.
        /// Enumeradores fornecem esse valor através do atributo descrição,
        /// caso esteja presente.
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Mnemonico do elemento.
        /// Em enumeradores este é o próprio nome do item.
        /// </summary>
        public string Mnemonico { get; set; }

        /// <summary>
        /// Valor do elemento.
        /// Em enumeradores este é o próprio valor do item.
        /// </summary>
        public string Valor { get; set; }

        /// <summary>
        /// Campo usado para exclusão lógica.
        /// Identifica se o item está ativo ou inativo.
        /// </summary>
        public bool Ativo { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListaItemInfo()
        {
            this.CodigoListaItem = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoListaItem;
        }

        #endregion
    }
}
