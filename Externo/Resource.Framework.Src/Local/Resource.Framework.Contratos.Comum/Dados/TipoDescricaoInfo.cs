﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Classe de auxilio para informar a descricao de um tipo de log
    /// </summary>
    [Serializable]
    public class TipoDescricaoInfo
    {
        /// <summary>
        /// Informa o nome do tipo
        /// </summary>
        public string NomeTipo { get; set; }

        /// <summary>
        /// Informa o caminho completo para se chegar no tipo.
        /// Por exemplo: 
        /// Resource.Framework.Contratos.Comum.Dados.LogInfo, Resource.Framework.Contratos
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// Descricao do tipo 
        /// </summary>
        public string Descricao { get; set; }
    }
}
