﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Sistemas.Comum;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Representa uma sessao a ser persistida no banco de dados
    /// </summary>
    [Serializable]
    public class SessaoPersistenciaInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da sessao
        /// </summary>
        public string CodigoSessao { get; set; }

        /// <summary>
        /// Sessao a ser persistida
        /// </summary>
        public Sessao Sessao { get; set; }
        
        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoSessao;
        }

        #endregion
    }
}
