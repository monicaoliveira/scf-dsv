using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Contem os status da associação de permissão. Por exemplo, permitida ou negada.
    /// </summary>
    public enum PermissaoAssociadaStatusEnum
    {
        /// <summary>
        /// Indica uma negação da permissão no contexto
        /// </summary>
        Negado = 1,

        /// <summary>
        /// Indica que a permissão vale para o contexto
        /// </summary>
        Permitido = 0
    }
}
