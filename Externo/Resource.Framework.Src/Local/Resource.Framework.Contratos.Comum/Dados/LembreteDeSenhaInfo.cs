﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Dados
{
    [Serializable]
    public class LembreteDeSenhaInfo
    {
        public UsuarioInfo Usuario { get; set; }
        public string Pergunta { get; set; }
        public string Resposta { get; set; }
    }
}
