﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Classe de log para erro
    /// </summary>
    [Serializable]
    public class LogErroInfo : LogInfo
    {
        /// <summary>
        /// Tipo da mensagem de request
        /// </summary>
        public string TipoMensagemRequest { get; set; }
    }
}
