﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Resumo de lista
    /// </summary>
    [Serializable]
    public class ListaResumoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da lista
        /// </summary>
        public string CodigoLista { get; set; }

        /// <summary>
        /// Descrição da lista
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Mnemonico da lista.
        /// </summary>
        public string Mnemonico { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoLista;
        }

        #endregion
    }
}
