﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// Contem as configurações do robo
    /// </summary>
    [Serializable]
    public class ConfigInfo : ICodigoEntidade
    {
        /// <summary>
        /// Config a ser salvo
        /// </summary>
        public object ObjetoConfig { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.ObjetoConfig.GetType().Name;
        }

        #endregion
    }
}
