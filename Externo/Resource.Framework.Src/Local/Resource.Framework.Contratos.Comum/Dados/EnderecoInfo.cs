﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Dados
{
    /// <summary>
    /// entidade que representa um endereço com os seguintes campos:
    /// País, UF, Cidade, Bairro.
    /// </summary>
    [Serializable]
    public class EnderecoInfo:ICodigoEntidade
    {
        public string CodigoEndereco { get; set; }

        /// <summary>
        /// Representa o país.
        /// </summary>
        public string Pais { get; set; }

        /// <summary>
        /// Representa a unidade federal (UF).
        /// </summary>
        public string UF { get; set; }

        /// <summary>
        /// Representa a Cidade.
        /// </summary>
        public string Cidade { get; set; }

        /// <summary>
        /// Representa o bairro.
        /// </summary>
        public string Bairro { get; set; }

        public EnderecoInfo()
        {
            this.CodigoEndereco = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoEndereco;
        }

        #endregion
    }
}
