﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ExpertPdf.HtmlToPdf;

namespace Resource.Framework.Contratos.Comum
{
    public static class PdfHelper
    {

        /// <summary>
        /// GetPdfConverter
        /// </summary>
        /// <param name="objeto"></param>
        /// <returns></returns>
        private static PdfConverter GetPdfConverter()
        {
            PdfConverter pdfConverter = new PdfConverter();

            //pdfConverter.LicenseKey = "put your license key here";


            pdfConverter.PageWidth = int.Parse("1024");

            // set if the generated PDF contains selectable text or an embedded image - default value is true
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;

            //set the PDF page size 
            pdfConverter.PdfDocumentOptions.PdfPageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4");
            // set the PDF compression level
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = (PdfCompressionLevel)Enum.Parse(typeof(PdfCompressionLevel), "Normal");
            // set the PDF page orientation (portrait or landscape)
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = (PDFPageOrientation)Enum.Parse(typeof(PDFPageOrientation), "Portrait");
            //set the PDF standard used to generate the PDF document
            pdfConverter.PdfStandardSubset = PdfStandardSubset.Full;
            // show or hide header and footer
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            //set the PDF document margins
            pdfConverter.PdfDocumentOptions.LeftMargin = int.Parse("0");
            pdfConverter.PdfDocumentOptions.RightMargin = int.Parse("0");
            pdfConverter.PdfDocumentOptions.TopMargin = int.Parse("0");
            pdfConverter.PdfDocumentOptions.BottomMargin = int.Parse("0");
            // set if the HTTP links are enabled in the generated PDF
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // set if the HTML content is resized if necessary to fit the PDF page width - default is true
            pdfConverter.PdfDocumentOptions.FitWidth = true;
            // set if the PDF page should be automatically resized to the size of the HTML content when FitWidth is false
            pdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;
            // embed the true type fonts in the generated PDF document
            pdfConverter.PdfDocumentOptions.EmbedFonts = false;
            // compress the images in PDF with JPEG to reduce the PDF document size - default is true
            pdfConverter.PdfDocumentOptions.JpegCompressionEnabled = true;
            // set if the JavaScript is enabled during conversion 
            pdfConverter.ScriptsEnabled = pdfConverter.ScriptsEnabledInImage = false;

            // set if the converter should try to avoid breaking the images between PDF pages
            pdfConverter.AvoidImageBreak = false;

            pdfConverter.PdfHeaderOptions.HeaderText = "Title";
            pdfConverter.PdfHeaderOptions.HeaderTextColor = Color.FromKnownColor((KnownColor)Enum.Parse(typeof(KnownColor), "Black"));
            pdfConverter.PdfHeaderOptions.HeaderSubtitleText = "Subtitle";
            pdfConverter.PdfHeaderOptions.DrawHeaderLine = true;
            pdfConverter.PdfHeaderOptions.HeaderHeight = 50;

            pdfConverter.PdfFooterOptions.FooterText = "Footer";
            pdfConverter.PdfFooterOptions.FooterTextColor = Color.FromKnownColor((KnownColor)Enum.Parse(typeof(KnownColor), "Black"));
            pdfConverter.PdfFooterOptions.DrawFooterLine = true;
            pdfConverter.PdfFooterOptions.PageNumberText = "Pagina";
            pdfConverter.PdfFooterOptions.ShowPageNumber = true;
            pdfConverter.PdfFooterOptions.FooterHeight = 50;

            pdfConverter.PdfBookmarkOptions.TagNames = new string[] { "h1", "h2" };

            return pdfConverter;
        }


        public static byte[] GetDownloadBytes( string urlToConvert, string fileName ) 
        {
            byte[] downloadBytes = null;
            PdfConverter pdfConverter = GetPdfConverter();
            downloadBytes = pdfConverter.GetPdfBytesFromUrl(urlToConvert);
            return downloadBytes;
        }
    }
}
