﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using System.Xml.Serialization;

namespace Resource.Framework.Contratos.Comum
{
    [Serializable]
    public class ServicoSegurancaConfig
    {   public string NomeUsuarioAdministrador { get; set; }
        public string SenhaUsuarioAdministrador { get; set; }
        public bool ValidarSenhas { get; set; }
        public List<string> NamespacesPermissoes { get; set; }
        public List<ComplementoAutenticacaoInfo> ComplementosAutenticacao { get; set; }
        public Boolean ValidarSenhaExpirada { get; set; }
        public int DiasExpiracaoSenha { get; set; }
        [XmlIgnore]
        public Type TipoUsuario
        {
            get { return Type.GetType(this.TipoUsuarioString); }
            set { this.TipoUsuarioString = value.FullName + ", " + value.Assembly.FullName; }
        }
        [XmlElement("TipoUsuario")]
        public string TipoUsuarioString { get; set; }
        public bool PersistirSessao { get; set; }
        public int TamanhoMinimoSenha { get; set; }
        public int QuantidadeTentativasBloqueio { get; set; }
        public int QuantidadeBloqueioUltimasSenhas { get; set; }
        public bool BloqueioUltimasSenhas { get; set; }
        public bool ExigirNumerosNaSenha { get; set; }
        public bool ExigirLetrasNaSenha { get; set; }
        public bool SalvarSenhaCriptografada{ get; set; }
        public ServicoSegurancaConfig()
        {
            this.NamespacesPermissoes = new List<string>();
            this.ComplementosAutenticacao = new List<ComplementoAutenticacaoInfo>();
            this.ValidarSenhas = true;
            this.ValidarSenhaExpirada = false;
            this.DiasExpiracaoSenha = 90;
            this.BloqueioUltimasSenhas = false;
            this.ExigirLetrasNaSenha = true;
            this.ExigirNumerosNaSenha = true;
            this.SalvarSenhaCriptografada = true;
            this.TipoUsuario = typeof(UsuarioInfo);
        }
    }
}
