﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum
{
    public static class LogHelper
    {
        public static LogInfo Salvar(string codigoSessao, LogInfo objeto)
        {
            if (codigoSessao != null)
                objeto.CodigoUsuario = SegurancaHelper.ReceberSessao(codigoSessao).CodigoUsuario;
            return
                Mensageria.Processar<SalvarObjetoResponse<LogInfo>>(new SalvarObjetoRequest<LogInfo>() { CodigoSessao = codigoSessao, Objeto = objeto }).Objeto;
        }    
    }
}
