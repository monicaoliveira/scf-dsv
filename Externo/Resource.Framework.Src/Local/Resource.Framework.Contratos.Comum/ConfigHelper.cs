﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Classe de auxilio de config
    /// Permite que o config possa ser encontrado no
    /// app.config ou no banco de dados
    /// </summary>
    public static class ConfigHelper
    {
        /// <summary>
        /// Configuracoes do helper
        /// </summary>
        private static ConfigHelperConfig _config = 
            GerenciadorConfig.ReceberConfig<ConfigHelperConfig>() != null ? 
            GerenciadorConfig.ReceberConfig<ConfigHelperConfig>() :
            new ConfigHelperConfig();

        /// <summary>
        /// Seta o config para esta sessao
        /// Nao salva no banco de dados e não tem
        /// prioridade sobre um config salvo no banco
        /// de dados
        /// </summary>
        /// <param name="config"></param>
        public static void Setar(object config)
        {
            // Informa um config para esta instancia
            GerenciadorConfig.SetarConfig(config);
        }

        /// <summary>
        /// Recebe um config
        /// A prioridade é: banco de dados, local,
        /// arquivo de configuracao
        /// </summary>
        public static object Receber(Type tipo, string codigoSessao)
        {
            // Inicializa
            object obj = null;

            // Deve verificar na persistencia?
            if (_config.UtilizarPersistencia)
            {
                // Pega eventual config na persistencia
                ConfigInfo configInfo =
                    PersistenciaHelper.Receber<ConfigInfo>(
                        codigoSessao, tipo.Name);

                // Se achou, usa este
                if (configInfo != null)
                    obj = configInfo.ObjetoConfig;
            }

            // Se nao achou na persistencia, procura no arquivo
            if (obj == null)
                obj = GerenciadorConfig.ReceberConfig(tipo);

            // Retorna
            return obj;
        }

        /// <summary>
        /// Recebe um config
        /// A prioridade é: banco de dados, local,
        /// arquivo de configuracao
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Receber<T>(string codigoSessao)
        {
            // Inicializa
            T obj = default(T);
            
            // Deve verificar na persistencia?
            if (_config.UtilizarPersistencia)
            {
                // Pega eventual config na persistencia
                ConfigInfo configInfo =
                    PersistenciaHelper.Receber<ConfigInfo>(
                        codigoSessao, typeof(T).Name);

                // Se achou, usa este
                if (configInfo != null)
                    obj = (T)configInfo.ObjetoConfig;
            }
            
            // Se nao achou na persistencia, procura no arquivo
            if (obj == null)
                obj = GerenciadorConfig.ReceberConfig<T>();

            // Retorna
            return obj;
        }

        /// <summary>
        /// Salva um config no banco de dados
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="config"></param>
        public static void Salvar<T>(string codigoSessao, T config)
        {
            // Salva na persistencia
            PersistenciaHelper.Salvar<ConfigInfo>(
                codigoSessao, new ConfigInfo() { ObjetoConfig = config });
        }

        /// <summary>
        /// Remove um config do banco de dados
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void Remover<T>(string codigoSessao)
        {
            // Remove da persistencia
            PersistenciaHelper.Remover<ConfigInfo>(
                codigoSessao, typeof(T).Name);
        }
    }
}
