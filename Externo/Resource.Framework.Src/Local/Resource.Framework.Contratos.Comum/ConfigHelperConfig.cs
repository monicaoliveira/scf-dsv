﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Configuracoes do Helper de Configuracoes
    /// </summary>
    public class ConfigHelperConfig
    {
        /// <summary>
        /// Indica se deve utilizar a persistencia
        /// Em caso afirmativo, a persistencia passa a ter prioridade
        /// sobre os configs
        /// </summary>
        public bool UtilizarPersistencia { get; set; }

        /// <summary>
        /// Construtor
        /// </summary>
        public ConfigHelperConfig()
        {
            this.UtilizarPersistencia = false;
        }
    }
}
