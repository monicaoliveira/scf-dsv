﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Interface para o serviço de metadados para bancos de dados.
    /// A motivação inicial é sincronizar os enumeradores com as tabelas
    /// Lista e ListaItem.
    /// </summary>
    public interface IServicoMetadadoComum
    {
        #region Cadastro de Lista e Lista Item

        /// <summary>
        /// Lista de listas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarListaResponse ListarLista(ListarListaRequest request);

        /// <summary>
        /// Recebe o detalhe de uma lista
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberListaResponse ReceberLista(ReceberListaRequest request);

        /// <summary>
        /// Recebe o detalhe de diversas listas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberListasResponse ReceberListas(ReceberListasRequest request);

        /// <summary>
        /// Remove a lista solicitada
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverListaResponse RemoverLista(RemoverListaRequest request);

        /// <summary>
        /// Salva a lista informada
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarListaResponse SalvarLista(SalvarListaRequest request);

        #endregion

        #region Metadados de Enumeradores

        /// <summary>
        /// Faz a geração e/ou sincronismo dos enumeradores informados
        /// com as tabelas Lista e ListaItem
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        GerarEnumMetadadoResponse GerarEnumMetadado(GerarEnumMetadadoRequest parametros);

        #endregion

        #region Descricao de Tipos

        /// <summary>
        /// Solicita lista de tipos com suas respectivas descrições
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarDescricaoTipoResponse ListarDescricaoTipo(ListarDescricaoTipoRequest request);

        #endregion

        #region Config

        /// <summary>
        /// Salva Config informado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarConfigResponse SalvarConfig(SalvarConfigRequest request);

        /// <summary>
        /// Recebe o config solicitado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberConfigResponse ReceberConfig(ReceberConfigRequest request);

        /// <summary>
        /// Remove o config solicitado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverConfigResponse RemoverConfig(RemoverConfigRequest request);

        #endregion
    }
}
