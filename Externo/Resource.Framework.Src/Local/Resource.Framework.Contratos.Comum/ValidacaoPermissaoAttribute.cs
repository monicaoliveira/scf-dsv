﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum
{
    /// <summary>
    /// Atributo para validação de permissão
    /// </summary>
    public class ValidacaoPermissaoAttribute : Attribute
    {
        /// <summary>
        /// Lista de permissoes
        /// Qualquer permissao da lista permite o acesso
        /// </summary>
        public Type[] PermissaoListaOu { get; set; }

        /// <summary>
        /// Lista de permissoes
        /// Tem que ter todas as pemissoes para ter o acesso
        /// </summary>
        public Type[] PermissaoListaE { get; set; }

        /// <summary>
        /// Permissao necessaria
        /// </summary>
        public Type Permissao { get; set; }
    }
}
