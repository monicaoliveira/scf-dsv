﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    public enum SalvarObjetoTipoSincronismoEnum
    {
        Sincronizar,
        Adicionar,
        Remover
    }
}
