﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de detalhe de lista
    /// </summary>
    public class ReceberListaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista solicitada
        /// </summary>
        public ListaInfo ListaInfo { get; set; }
    }
}
