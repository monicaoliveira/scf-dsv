﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Resposta a uma solicitacao de listas
    /// </summary>
    public class ReceberListasResponse : MensagemResponseBase
    {
        /// <summary>
        /// Listas solicitadas
        /// </summary>
        public List<ListaInfo> Listas { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ReceberListasResponse()
        {
            this.Listas = new List<ListaInfo>();
        }
    }
}
