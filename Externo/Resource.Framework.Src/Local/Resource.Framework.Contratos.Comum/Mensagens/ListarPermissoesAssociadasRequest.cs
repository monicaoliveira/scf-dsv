﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de permissoes do usuario logado
    /// </summary>
    public class ListarPermissoesAssociadasRequest : MensagemRequestBase
    {
    }
}
