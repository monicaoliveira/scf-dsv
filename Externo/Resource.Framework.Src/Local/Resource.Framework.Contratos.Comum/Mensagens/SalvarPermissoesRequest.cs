﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar Permissoes
    /// </summary>
    [Serializable]
    public class SalvarPermissoesRequest : MensagemRequestBase
    {
    }
}
