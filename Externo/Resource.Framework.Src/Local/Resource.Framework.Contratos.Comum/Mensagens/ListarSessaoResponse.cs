﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Sistemas.Comum;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    public class ListarSessaoResponse: MensagemResponseBase
    {
        public List<Sessao> Resultado { get; set; }
    }
}
