﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    public class SalvarEnderecoResponse : MensagemResponseBase
    {
        public EnderecoInfo EnderecoInfo { get; set; }
    }
}
