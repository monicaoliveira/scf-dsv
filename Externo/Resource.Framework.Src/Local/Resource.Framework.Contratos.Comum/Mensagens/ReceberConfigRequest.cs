﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de receber config
    /// </summary>
    public class ReceberConfigRequest : MensagemRequestBase
    {
        /// <summary>
        /// Tipo do objeto de config
        /// </summary>
        public string TipoObjeto { get; set; }
    }
}
