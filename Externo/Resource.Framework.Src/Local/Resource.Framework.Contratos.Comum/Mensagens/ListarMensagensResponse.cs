﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Resposta de uma solicitação de lista de mensagens
    /// </summary>
    public class ListarMensagensResponse : MensagemResponseBase
    {
        public List<MensagemBase> Mensagens { get; set; }
    }
}
