﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de consulta de usuários grupo
    /// </summary>
    [Serializable]
    public class ListarUsuarioGruposResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de usuários grupo encontrada
        /// </summary>
        public List<UsuarioGrupoInfo> UsuarioGrupos { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarUsuarioGruposResponse()
        {
            this.UsuarioGrupos = new List<UsuarioGrupoInfo>();
        }
    }
}
