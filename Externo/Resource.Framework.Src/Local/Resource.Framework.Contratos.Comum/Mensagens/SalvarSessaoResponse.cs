﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Resposta a uma solicitação de salvar sessao
    /// </summary>
    [Serializable]
    public class SalvarSessaoResponse : MensagemResponseBase
    {
    }
}
