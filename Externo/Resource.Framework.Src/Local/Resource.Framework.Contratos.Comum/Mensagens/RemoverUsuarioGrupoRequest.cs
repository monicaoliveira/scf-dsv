﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de usuário grupo
    /// </summary>
    [Serializable]
    public class RemoverUsuarioGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do usuário grupo a remover
        /// </summary>
        public string CodigoUsuarioGrupo { get; set; }
    }
}
