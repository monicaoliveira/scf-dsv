﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de listas
    /// </summary>
    public class ListarListaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Listas encontradas
        /// </summary>
        public List<ListaResumoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarListaResponse()
        {
            this.Resultado = new List<ListaResumoInfo>();
        }
    }
}
