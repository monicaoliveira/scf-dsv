﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de receber config
    /// </summary>
    public class ReceberConfigResponse : MensagemResponseBase
    {
        /// <summary>
        /// ConfigInfo transportando o config desejado
        /// </summary>
        public ConfigInfo ConfigInfo { get; set; }
    }
}
