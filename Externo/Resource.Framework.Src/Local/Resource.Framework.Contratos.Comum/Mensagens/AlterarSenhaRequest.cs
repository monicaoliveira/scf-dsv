﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    [Serializable]
    public class AlterarSenhaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Permite que o administrador informe o codigo do usuario
        /// que terá a senha alterada
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Senha atual do usuário
        /// </summary>
        public string SenhaAtual { get; set; }

        /// <summary>
        /// Nova senha escolhida pelo usuário
        /// </summary>
        public string NovaSenha { get; set; }
    }
}
