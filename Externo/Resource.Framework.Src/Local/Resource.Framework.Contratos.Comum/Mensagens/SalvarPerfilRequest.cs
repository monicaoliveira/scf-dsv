﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar perfil
    /// </summary>
    [Serializable]
    public class SalvarPerfilRequest : MensagemRequestBase
    {
        /// <summary>
        /// Perfil a ser salvo
        /// </summary>
        public PerfilInfo Perfil { get; set; }

        ///Novo?
        public bool Novo { get; set; }
    }
}
