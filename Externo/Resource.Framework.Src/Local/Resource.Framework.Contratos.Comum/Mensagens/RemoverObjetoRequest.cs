﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    [Serializable]
    public class RemoverObjetoRequest<T> : MensagemRequestBase
    {
        /// <summary>
        /// Código do objeto a remover
        /// </summary>
        public string CodigoObjeto { get; set; }
    }
}
