﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de detalhe de usuário
    /// </summary>
    [Serializable]
    public class ReceberUsuarioResponse : MensagemResponseBase
    {
        /// <summary>
        /// Usuario encontrado
        /// </summary>
        public UsuarioInfo Usuario { get; set; }
    }
}
