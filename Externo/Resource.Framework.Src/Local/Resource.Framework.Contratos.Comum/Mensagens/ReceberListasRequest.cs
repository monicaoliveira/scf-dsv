﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de listas
    /// </summary>
    public class ReceberListasRequest : MensagemRequestBase
    {

        public ReceberListasRequest()
        {
            this.CodigosLista = new string[] { };
            this.MnemonicosLista = new string[] { };
        }
        /// <summary>
        /// Lista de codigos de lista
        /// </summary>
        public string[] CodigosLista { get; set; }

        /// <summary>
        /// Lista de mnemonicos de lista
        /// </summary>
        public string[] MnemonicosLista { get; set; }
    }
}
