﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    [Serializable]
    public class SubstituirObjetoRequest<T> : MensagemRequestBase
    {   /// SCF1701 - ID 27
        /// <summary>
        /// Código do objeto a substituir
        /// </summary>
        public string CodigoProduto { get; set; }
        public string CodigoContaFavorecido { get; set; }
        public string CodigoContaFavorecidoNovo { get; set; }
        public string Referencia { get; set; }

        public string CodigoProcesso { get; set; }
    }
}
