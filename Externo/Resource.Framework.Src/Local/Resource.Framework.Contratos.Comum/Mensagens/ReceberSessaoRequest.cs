﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para receber uma sessão
    /// </summary>
    [Serializable]
    public class ReceberSessaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo da sessão desejada
        /// </summary>
        public string CodigoSessaoARetornar { get; set; }
    }
}
