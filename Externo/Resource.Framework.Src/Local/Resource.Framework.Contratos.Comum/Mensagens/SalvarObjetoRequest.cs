﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    [Serializable]
    public class SalvarObjetoRequest<T> : MensagemRequestBase
    {
        /// <summary>
        /// Objeto a ser salvo
        /// </summary>
        public T Objeto { get; set; }
    }
}
