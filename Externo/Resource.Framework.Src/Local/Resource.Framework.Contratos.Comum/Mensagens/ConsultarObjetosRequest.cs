﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    [Serializable]
    public class ConsultarObjetosRequest<T> : MensagemRequestBase
    {
        /// <summary>
        /// Lista de condições para o filtro
        /// </summary>
        public List<CondicaoInfo> Condicoes { get; set; }

        /// <summary>
        /// Número máximo de linhas a retornar
        /// </summary>
        public int MaxLinhas { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ConsultarObjetosRequest()
        {
            this.Condicoes = new List<CondicaoInfo>();
            this.MaxLinhas = 0;
        }
    }
}
