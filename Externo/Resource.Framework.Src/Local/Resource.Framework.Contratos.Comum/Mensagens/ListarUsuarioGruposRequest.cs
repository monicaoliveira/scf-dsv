﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de consulta de usuarios grupo de acordo com o filtro informado
    /// </summary>
    [Serializable]
    public class ListarUsuarioGruposRequest : MensagemRequestBase
    {

    }
}
