﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    public class SubstituirObjetoResponse<T> : MensagemResponseBase
    {
        // SCF1701 - ID 27
        public Int32 QuantidadeAlterada {get;set;}
    }
}
