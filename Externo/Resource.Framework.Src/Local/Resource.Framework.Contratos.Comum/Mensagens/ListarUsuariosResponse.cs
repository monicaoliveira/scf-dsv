﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de consulta de usuários
    /// </summary>
    [Serializable]
    public class ListarUsuariosResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de usuarios encontrados
        /// </summary>
        public List<UsuarioInfo> Usuarios { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarUsuariosResponse()
        {
            this.Usuarios = new List<UsuarioInfo>();
        }
    }
}
