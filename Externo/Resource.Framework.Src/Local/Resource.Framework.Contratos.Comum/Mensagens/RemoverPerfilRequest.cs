﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de requisição de remoção de perfil
    /// </summary>
    [Serializable]
    public class RemoverPerfilRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do perfil a ser removido
        /// </summary>
        public string CodigoPerfil { get; set; }
    }
}
