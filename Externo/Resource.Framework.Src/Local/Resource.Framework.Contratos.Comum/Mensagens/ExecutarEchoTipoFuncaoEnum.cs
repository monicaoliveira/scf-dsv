using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Tipos de chamada para a função executar echo
    /// </summary>
    public enum ExecutarEchoTipoFuncaoEnum
    {
        DesligarTimer = 2,
        EcoarMensagem = 0,
        LigarTimer = 1
    }
}
