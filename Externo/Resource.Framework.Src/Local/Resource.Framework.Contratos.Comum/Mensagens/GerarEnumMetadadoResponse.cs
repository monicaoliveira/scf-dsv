﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de geração de 
    /// metadados em banco de dados
    /// </summary>
    public class GerarEnumMetadadoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado das listas geradas
        /// </summary>
        public List<ListaInfo> Listas { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public GerarEnumMetadadoResponse()
        {
            this.Listas = new List<ListaInfo>();
        }
    }
}
