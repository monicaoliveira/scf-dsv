﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de remover lista
    /// </summary>
    public class RemoverListaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Indica o código da lista removido
        /// </summary>
        public string CodigoListaRemovido { get; set; }
    }
}
