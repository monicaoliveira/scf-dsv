﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe lista
    /// </summary>
    public class ReceberListaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código da lista a retornar
        /// </summary>
        public string CodigoLista { get; set; }

        /// <summary>
        /// Mnemonico da lista a retornar
        /// </summary>
        public string MnemonicoLista { get; set; }

        /// <summary>
        /// Indica se deve retornar os itens excluidos ou apenas os itens Ativos.
        /// </summary>
        public bool RetornarItensExcluidos { get; set; }
    }
}
