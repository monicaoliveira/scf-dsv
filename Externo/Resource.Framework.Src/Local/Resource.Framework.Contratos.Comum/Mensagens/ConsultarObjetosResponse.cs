﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    public class ConsultarObjetosResponse<T> : MensagemResponseBase
    {
        public List<T> Resultado { get; set; }

        public ConsultarObjetosResponse()
        {
            this.Resultado = new List<T>();
        }
    }
}
