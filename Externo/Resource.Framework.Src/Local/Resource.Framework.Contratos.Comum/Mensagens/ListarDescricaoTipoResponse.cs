﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de descricao de tipos
    /// </summary>
    public class ListarDescricaoTipoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista encontrada
        /// </summary>
        public List<TipoDescricaoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarDescricaoTipoResponse()
        {
            this.Resultado = new List<TipoDescricaoInfo>();
        }
    }
}
