﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    public class SalvarVeiculoRequest : MensagemRequestBase
    {
        public VeiculoInfo VeiculoInfo { get; set; }
    }
}
