﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar config
    /// </summary>
    public class SalvarConfigRequest : MensagemRequestBase
    {
        /// <summary>
        /// Config a ser salvo
        /// </summary>
        public ConfigInfo ConfigInfo { get; set; }
    }
}
