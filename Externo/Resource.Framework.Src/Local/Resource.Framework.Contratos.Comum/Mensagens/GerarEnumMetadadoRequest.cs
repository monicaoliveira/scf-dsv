﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de geração de metadado em banco de dados
    /// </summary>
    public class GerarEnumMetadadoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Lista de enumeradores a serem sincronizados
        /// </summary>
        [XmlIgnore]
        public Type[] Enumeradores { get; set; }

        /// <summary>
        /// Lista em string dos enumeradores a serem sincronizados.
        /// Cada item deve ter o formato tipo, assembly
        /// </summary>
        [XmlElement("Enumeradores")]
        public string[] EnumeradoresString { get; set; }

        /// <summary>
        /// Retorna a lista de enumeradores
        /// </summary>
        /// <returns></returns>
        public Type[] ReceberListaEnumeradores()
        {
        
            // Inicializa
            List<Type> tipos = new List<Type>();

            // Adiciona itens informados diretamente
            if (this.Enumeradores != null)
                tipos.AddRange(this.Enumeradores);

            // Varre a lista de strings adicionando os tipos
            if (this.EnumeradoresString != null)
                foreach (string enumeradorString in this.EnumeradoresString)
                {
                    // Pega o tipo
                    Type tipo = Type.GetType(enumeradorString);

                    // Se nao encontrou, usa o resolutor
                    if (tipo == null)
                        tipo = ResolutorTipos.Resolver(enumeradorString);

                    // Se tipo ainda continua nulo é erro
                    if (tipo == null)
                        throw new Exception("Tipo não conhecido: " + enumeradorString);

                    // Adiciona na colecao
                    tipos.Add(tipo);
                }

            // Retorna
            return tipos.ToArray();

        }
    }
}
