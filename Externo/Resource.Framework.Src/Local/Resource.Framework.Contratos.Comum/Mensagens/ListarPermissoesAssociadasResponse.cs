﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Permissoes;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de lista de permissoes do usuario logado
    /// </summary>
    public class ListarPermissoesAssociadasResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de permissoes
        /// </summary>
        public List<String> PermissoesAssociadas { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarPermissoesAssociadasResponse()
        {
            this.PermissoesAssociadas = new List<String>();
        }
    }
}
