﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de receber sistema cliente
    /// </summary>
    public class ReceberSistemaClienteResponse : MensagemResponseBase
    {
        /// <summary>
        /// Sistema cliente encontrado
        /// </summary>
        public SistemaClienteInfo SistemaCliente { get; set; }
    }
}
