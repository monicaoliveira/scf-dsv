﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de requisicao de consulta de sessoes
    /// </summary>
    [Serializable]
    public class ConsultarSessoesRequest : MensagemRequestBase
    {
    }
}
