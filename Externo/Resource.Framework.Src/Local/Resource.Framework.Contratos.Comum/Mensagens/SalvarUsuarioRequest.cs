﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar usuário
    /// </summary>
    [Serializable]
    public class SalvarUsuarioRequest : MensagemRequestBase
    {
        /// <summary>
        /// Usuário a ser salvo
        /// </summary>
        public UsuarioInfo Usuario { get; set; }

        /// <summary>
        /// Indicador de novo usuario
        /// </summary>
        public bool Novo { get; set; }
    }
}
