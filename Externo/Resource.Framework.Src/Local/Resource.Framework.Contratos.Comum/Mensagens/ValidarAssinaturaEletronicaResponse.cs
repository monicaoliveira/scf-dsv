﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Resposta a uma solicitação de validação de assinatura eletronica
    /// </summary>
    public class ValidarAssinaturaEletronicaResponse : MensagemResponseBase
    {
    }
}
