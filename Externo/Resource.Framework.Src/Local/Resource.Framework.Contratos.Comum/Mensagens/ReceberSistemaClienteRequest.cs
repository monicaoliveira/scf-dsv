﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de detalhe de sistema cliente
    /// </summary>
    [Serializable]
    public class ReceberSistemaClienteRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do sistema cliente desejado
        /// </summary>
        public string CodigoSistemaClienteAReceber { get; set; }
    }
}
