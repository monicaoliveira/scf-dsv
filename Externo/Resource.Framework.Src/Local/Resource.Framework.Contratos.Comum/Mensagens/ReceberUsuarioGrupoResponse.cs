﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de detalhe de usuario grupo
    /// </summary>
    [Serializable]
    public class ReceberUsuarioGrupoResponse : MensagemResponseBase
    {
        /// <summary>
        /// UsuarioGrupo solicitado
        /// </summary>
        public UsuarioGrupoInfo UsuarioGrupo { get; set; }
    }
}
