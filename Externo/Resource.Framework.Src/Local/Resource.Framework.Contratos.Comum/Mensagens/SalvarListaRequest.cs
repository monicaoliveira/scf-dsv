﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar lista
    /// </summary>
    public class SalvarListaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Lista a ser salva
        /// </summary>
        public ListaInfo ListaInfo { get; set; }
    }
}
