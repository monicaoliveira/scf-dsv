﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de responsta a uma solicitação de detalhe de sessao
    /// </summary>
    [Serializable]
    public class ReceberSessaoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Sessao encontrada
        /// </summary>
        public SessaoInfo Sessao { get; set; }
    }
}
