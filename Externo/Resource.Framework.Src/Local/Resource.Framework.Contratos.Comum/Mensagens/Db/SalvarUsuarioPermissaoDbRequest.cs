﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens.Db
{
    public class SalvarUsuarioPermissaoDbRequest : SalvarObjetoRequest<PermissaoAssociadaInfo>
    {
        /// <summary>
        /// Indica o codigo do usuario associado à permissao
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Informa a Permissao que deve ser salva
        /// </summary>
        public PermissaoAssociadaInfo PermissaoAssociadaInfo { get; set; }

        
    }
}
