﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens.Db
{
    /// <summary>
    /// Mensagem de banco para receber lista
    /// </summary>
    public class ReceberListaDbRequest : ReceberObjetoRequest<ListaInfo>
    {
        /// <summary>
        /// Código da lista
        /// </summary>
        public string CodigoLista 
        {
            get { return base.CodigoObjeto; }
            set { base.CodigoObjeto = value; } 
        }

        /// <summary>
        /// Mnemonico
        /// </summary>
        public string MnemonicoLista { get; set; }

        /// <summary>
        /// Indica se deve retornar os itens excluidos ou apenas os itens Ativos.
        /// </summary>
        public bool RetornarItensExcluidos { get; set; }
    }
}
