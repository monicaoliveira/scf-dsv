﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens.Db
{
    public class SalvarUsuarioDbRequest : SalvarObjetoRequest<UsuarioInfo>
    {
        /// <summary>
        /// Código do usuario
        /// </summary>
        public UsuarioInfo UsuarioInfo 
        {
            get { return base.Objeto; }
            set { base.Objeto = value; }
        }

        /// <summary>
        /// Indica se deve salvar os grupos do usuario
        /// </summary>
        public bool PreencherGrupos { get; set; }

        /// <summary>
        /// Indica se deve salvar as permissoes do usuario
        /// </summary>
        public bool PreencherPermissoes { get; set; }

        /// <summary>
        /// Indica se deve salvar os perfis do usuario
        /// </summary>
        public bool PreencherPerfis { get; set; }

        public string Matricula { get; set; }
        
    }
}
