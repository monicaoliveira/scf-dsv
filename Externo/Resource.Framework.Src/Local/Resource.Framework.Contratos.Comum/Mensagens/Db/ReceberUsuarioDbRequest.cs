﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens.Db
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe do usuario.
    /// Permite que seja informado as colecoes que devem ser
    /// preenchidas para o usuario.
    /// </summary>
    public class ReceberUsuarioDbRequest : ReceberObjetoRequest<UsuarioInfo>
    {
        /// <summary>
        /// Código do usuario
        /// </summary>
        public string CodigoUsuario
        {
            get { return base.CodigoObjeto; }
            set { base.CodigoObjeto = value; }
        }

        /// <summary>
        /// Indica se deve preencher os grupos do usuario
        /// </summary>
        public bool PreencherGrupos { get; set; }

        /// <summary>
        /// Indica se deve preencher as permissoes do usuario
        /// </summary>
        public bool PreencherPermissoes { get; set; }

        /// <summary>
        /// Indica se deve preencher os perfis do usuario
        /// </summary>
        public bool PreencherPerfis { get; set; }
    }
}
