﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de validação de mensagem
    /// </summary>
    public class ValidarMensagemResponse : MensagemResponseBase
    {
        /// <summary>
        /// Informa o contexto utilizado na validação da mensagem.
        /// </summary>
        public ContextoValidacaoInfo ContextoValidacao { get; set; }
    }
}
