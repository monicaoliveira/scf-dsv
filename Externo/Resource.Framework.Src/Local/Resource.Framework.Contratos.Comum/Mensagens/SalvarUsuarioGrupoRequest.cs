﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar usuario grupo
    /// </summary>
    [Serializable]
    public class SalvarUsuarioGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// UsuarioGrupo a ser salvo
        /// </summary>
        public UsuarioGrupoInfo UsuarioGrupo { get; set; }

        /// <summary>
        /// indica se é um novo ou se é alteração de um grupo já existente
        /// </summary>
        public bool Novo { get; set; }
    }
}
