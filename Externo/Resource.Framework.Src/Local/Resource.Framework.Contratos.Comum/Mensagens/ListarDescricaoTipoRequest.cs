﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de descricao de tipos
    /// </summary>
    public class ListarDescricaoTipoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Indica namespaces cujos tipos devem ser incluidos.
        /// </summary>
        public List<string> IncluirNamespaces { get; set; }

        /// <summary>
        /// Indica assemblies cujos tipos devem ser incluidos
        /// </summary>
        public List<string> IncluirAssemblies { get; set; }

        /// <summary>
        /// Indica tipos que devem ser incluidos
        /// </summary>
        public List<string> IncluirTipos { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarDescricaoTipoRequest()
        {
            this.IncluirNamespaces = new List<string>();
            this.IncluirAssemblies = new List<string>();
            this.IncluirTipos = new List<string>();
        }
    }
}
