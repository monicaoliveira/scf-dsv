﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de alteração de assinatura eletrônica
    /// </summary>
    [Serializable]
    public class AlterarAssinaturaEletronicaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Nova assinatura eletronica
        /// </summary>
        public string NovaAssinaturaEletronica { get; set; }
    }
}
