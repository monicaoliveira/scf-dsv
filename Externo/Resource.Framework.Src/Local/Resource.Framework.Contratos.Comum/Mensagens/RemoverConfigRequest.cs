﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remover config
    /// </summary>
    public class RemoverConfigRequest : MensagemRequestBase
    {
        /// <summary>
        /// Tipo do objeto a ser removido
        /// </summary>
        public string TipoObjeto { get; set; }
    }
}
