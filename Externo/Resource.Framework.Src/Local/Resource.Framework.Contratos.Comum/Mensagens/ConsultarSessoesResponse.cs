﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma consulta de sessoes
    /// </summary>
    [Serializable]
    public class ConsultarSessoesResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de sessoes encontradas
        /// </summary>
        public List<SessaoInfo> Sessoes { get; set; }
    }
}
