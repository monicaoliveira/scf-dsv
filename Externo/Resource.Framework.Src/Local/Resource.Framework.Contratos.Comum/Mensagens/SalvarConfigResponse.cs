﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de salvar config
    /// </summary>
    public class SalvarConfigResponse : MensagemResponseBase
    {
        /// <summary>
        /// Config a salvo
        /// </summary>
        public ConfigInfo ConfigInfo { get; set; }
    }
}
