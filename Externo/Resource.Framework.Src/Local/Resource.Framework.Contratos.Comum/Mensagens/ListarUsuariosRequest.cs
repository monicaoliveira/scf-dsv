﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de consulta de usuários
    /// </summary>
    [Serializable]
    public class ListarUsuariosRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por nome de usuario
        /// </summary>
        public string FiltroNomeUsuario { get; set; }

        /// <summary>
        /// Filtro por permissao que o usuário tem
        /// </summary>
        public string FiltroCodigoPermissao { get; set; }
    }
}
