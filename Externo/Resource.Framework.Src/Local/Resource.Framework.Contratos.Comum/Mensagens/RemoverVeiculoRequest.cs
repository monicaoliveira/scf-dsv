﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    public class RemoverVeiculoRequest : MensagemRequestBase
    {
        public string CodigoVeiculo { get; set; }
    }
}
