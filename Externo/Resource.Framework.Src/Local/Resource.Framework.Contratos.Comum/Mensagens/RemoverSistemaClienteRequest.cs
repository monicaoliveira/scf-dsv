﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de cliente
    /// </summary>
    [Serializable]
    public class RemoverSistemaClienteRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do sistema cliente a remover
        /// </summary>
        public string CodigoSistemaClienteARemover { get; set; }
    }
}
