﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de consulta de sistemas cliente
    /// </summary>
    [Serializable]
    public class ConsultarSistemasClienteResponse : MensagemResponseBase
    {
        /// <summary>
        /// Sistemas cliente encontrados
        /// </summary>
        public List<SistemaClienteInfo> SistemasCliente { get; set; }
    }
}
