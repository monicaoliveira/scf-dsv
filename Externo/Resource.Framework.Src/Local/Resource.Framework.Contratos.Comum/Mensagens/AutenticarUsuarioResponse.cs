﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de autenticação de usuário
    /// </summary>
    public class AutenticarUsuarioResponse : MensagemResponseBase
    {
        /// <summary>
        /// Indica se a senha está expirada.
        /// </summary>
        public Boolean SenhaExpirada { get; set; }

        /// <summary>
        /// Contem informações da sessão criada
        /// </summary>
        public SessaoInfo Sessao { get; set; }
    }
}
