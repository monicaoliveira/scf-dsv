﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de autenticação de usuário
    /// </summary>
    [Serializable]
    public class AutenticarUsuarioRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do usuário a autenticar
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Senha a ser validada
        /// </summary>
        public string Senha { get; set; }

        /// <summary>
        /// Codigo do sistema cliente em que o usuário está realizando o login
        /// </summary>
        public new string CodigoSistemaCliente { get; set; }
    }
}
