﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao para remover lista
    /// </summary>
    public class RemoverListaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código da lista a remover
        /// </summary>
        public string CodigoLista { get; set; }
    }
}
