﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Framework.Contratos.Comum.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de remoção de usuário grupo
    /// </summary>
    [Serializable]
    public class RemoverUsuarioGrupoResponse : MensagemResponseBase
    {
    }
}
