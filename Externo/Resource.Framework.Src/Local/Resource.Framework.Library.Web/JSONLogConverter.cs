﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Framework.Library.Web
{
    public class JSONLogConverter : JsonConverter
    {

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The <see cref="JsonSerializer"/>.</param>
        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is LogInfo)
            {
                writer.WriteValue(value.ToString());
                return;
            }

            writer.WriteRaw("null");
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="serializer">The <see cref="JsonSerializer"/>.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, JsonSerializer serializer)
        {
            if (reader.Value != null || reader.Value.ToString().Trim() != "")
            {
                return Activator.CreateInstance(reader.Value.ToString(), null);
            }

            return null;
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(LogInfo).IsAssignableFrom(objectType);
        }

    }
}
