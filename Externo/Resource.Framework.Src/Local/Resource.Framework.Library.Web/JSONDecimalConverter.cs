﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Globalization;

namespace Resource.Framework.Library.Web
{
    // JSONDateTimeJsonConverter based on Newtonsoft.Json.Converters.IsoDateTimeConverter
    // The JSONDateTimeJsonConverter returns the server's local time.
    // Copyright (c) 2007 James Newton-King

    /// <summary>
    /// Converts a <see cref="DateTime"/> to and from the ISO 8601 date format (e.g. 2008-04-12T12:53) using the server
    /// time. Does not adjust for timezone.
    /// </summary>
    public class JSONDecimalConverter : JsonConverter
    {
        private CultureInfo _culture = new CultureInfo("pt-BR");

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The <see cref="JsonSerializer"/>.</param>
        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is decimal || value is decimal?)
                writer.WriteValue(((decimal)value).ToString(_culture));
            else
                writer.WriteRaw("null");
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="serializer">The <see cref="JsonSerializer"/>.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, JsonSerializer serializer)
        {
            if (reader.Value != null)
                return decimal.Parse(reader.Value.ToString(), _culture);
            else
                return null;
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            return (typeof(decimal).IsAssignableFrom(objectType)
              || typeof(decimal?).IsAssignableFrom(objectType));
        }
    }

}
