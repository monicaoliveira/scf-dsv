﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Dados;
using Newtonsoft.Json.Converters;
using System.Threading;
using System.Globalization;

namespace Resource.Framework.Library.Web
{
    public class PaginaBase : System.Web.UI.Page
    {
        /// <summary>
        /// Conversores para desserializacao JSON
        /// </summary>
        private JsonConverter[] _conversoresDesserializacao = 
        { 
            new JSONDateTimeJsonConverter(),
            new JSONTypeConverter(),
            new JSONDecimalConverter()
        };

        /// <summary>
        /// Conversores para serializacao JSON
        /// </summary>
        private JsonConverter[] _conversoresSerializacao = 
        { 
            new JSONDateTimeJsonConverter(),
            new JSONTypeConverter(),
            new JSONDecimalConverter(),
            new StringEnumConverter()
        };

        protected override void OnLoad(EventArgs e)
        {
            // Log
            LogArquivo logArquivo = 
                new LogArquivo("web", "rotina", "PaginaBase");
            
            // Indica se a sessao está com senha expirada
            bool senhaExpirada = false;
            
            // Realiza cadeia do load
            base.OnLoad(e);

            // Pega valores a serem utilizados
            string paramAcao = this.Request.Form["acao"] != null ? this.Request.Form["acao"] : this.Request["acao"];
            string paramTipoMensagem = Request.Form["TipoMensagem"] != null ? Request.Form["TipoMensagem"] : Request["TipoMensagem"];
            string paramMensagem = Request.Form["Mensagem"] != null ? Request.Form["Mensagem"] : Request["Mensagem"];

            // Log
            LogArquivo.LogTag("request", false, string.Format("<acao>{0}</acao><tipoMensagem>{1}</tipoMensagem><mensagem>{2}</mensagem>", paramAcao, paramTipoMensagem, paramMensagem));

            // Verifica se tem acao
            if (paramAcao != null)
            {
                // Pega o tipo da mensagem
                Type tipoMensagem = ResolutorTipos.Resolver(paramTipoMensagem);

                // Desserializa a mensagem json para o tipo correto
                MensagemRequestBase mensagemRequest = null;

                // Apenas se descobriu o tipo da mensagem
                if (tipoMensagem != null && paramMensagem != null)
                {
                    // Trata erros de serializacao
                    try
                    {
                        // Desserializa a mensagem
                        mensagemRequest =
                            (MensagemRequestBase)
                                JsonConvert.DeserializeObject(paramMensagem,
                                tipoMensagem, _conversoresDesserializacao);
                    }
                    catch (Exception ex)
                    {
                        // Devolve o erro
                        object retornoErro =
                            new MensagemErroResponse()
                            {
                                Erro = string.Format("Erro ao desserializar JSON -> Tipo: {0}, Erro: {1}, JSON: {2}", tipoMensagem, ex.Message, paramMensagem),
                                StatusResposta = MensagemResponseStatusEnum.ErroPrograma
                            };

                        // Serializa a resposta
                        JsonSerializerSettings jsonSerializerSettingsErro =
                            new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                                Converters = _conversoresSerializacao
                            };

                        // Responde
                        Response.Clear();
                        Response.Write(
                            JsonConvert.SerializeObject(
                                retornoErro,
                                Formatting.None,
                                jsonSerializerSettingsErro
                        ));
                        Response.End();

                        // Fim do processamento
                        return;
                    }

                    // Se tem sessao...
                    if (Session["CodigoSessao"] != null)
                    {
                        // Coloca a sessao na mensagem
                        mensagemRequest.CodigoSessao = (string)Session["CodigoSessao"];

                        // Só precisa verificar expiração de senha se a mensagem não for um pedido
                        //   de alteração de senha
                        if (tipoMensagem != typeof(AlterarSenhaRequest))
                        {
                            // Recebe a sessao para verificar se a senha esta expirada]
                            SessaoInfo sessaoInfo =
                                Mensageria.Processar<ReceberSessaoResponse>(
                                    new ReceberSessaoRequest()
                                    {
                                        CodigoSessaoARetornar = mensagemRequest.CodigoSessao
                                    }).Sessao;

                            // Indica se a senha está expirada
                            if (sessaoInfo != null)
                                senhaExpirada = sessaoInfo.SenhaExpirada;
                        }
                    }
                }

                // Tem objeto para extrair?
                //object objeto = null;
                //string jsonObjeto = Request["mensagem"];
                //string tipoObjeto = Request["tipoMensagem"];
                //if (tipoObjeto != null && jsonObjeto != null)
                //    objeto = JsonConvert.DeserializeObject(jsonObjeto, Type.GetType(tipoObjeto));

                // Pede execução da ação
                object retorno = null;
                if (!senhaExpirada)
                    try
                    {
                        retorno = OnExecutarAcao(paramAcao, mensagemRequest);
                    }
                    catch (AcessoNegadoException ex)
                    {
                        retorno =
                            new MensagemErroResponse()
                            {
                                CodigoMensagemRequest = mensagemRequest.CodigoMensagem,
                                CodigoSessao = mensagemRequest.CodigoSessao,
                                StatusResposta = MensagemResponseStatusEnum.AcessoNaoPermitido,
                                Erro = ex.Message,
                                DescricaoResposta = ex.Message
                            };
                    }
                    catch (Exception ex)
                    {
                        retorno =
                            new MensagemErroResponse()
                            {
                                CodigoMensagemRequest = mensagemRequest.CodigoMensagem,
                                CodigoSessao = mensagemRequest.CodigoSessao,
                                StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                                Erro = ex.ToString(),
                                DescricaoResposta = ex.ToString()
                            };
                    }
                else
                    retorno = 
                        new MensagemErroResponse() 
                        {
                            CodigoMensagemRequest = mensagemRequest.CodigoMensagem,
                            CodigoSessao = mensagemRequest.CodigoSessao,
                            StatusResposta = MensagemResponseStatusEnum.SenhaExpirada
                        };

                // Serializa a resposta
                JsonSerializerSettings jsonSerializerSettings = 
                    new JsonSerializerSettings() 
                    { 
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        Converters = _conversoresSerializacao
                    };

                // Serializa resposta
                string resposta = 
                    JsonConvert.SerializeObject(
                        retorno,
                        Formatting.None,
                        jsonSerializerSettings
                    );
                
                // Log
                LogArquivo.LogTag("response", false, resposta);
                logArquivo.Dispose();

                Response.Clear();
                Response.Write(resposta);
                Response.End();
            }
        }

        private void retornarErro(string descricao)
        {
            // Pede execução da ação
            object retorno = 
                new MensagemErroResponse()
                {
                    CodigoSessao = (string)this.Session["codigoSessao"],
                    StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                    DescricaoResposta = descricao
                };

            // Serializa a resposta
            JsonSerializerSettings jsonSerializerSettings =
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    Converters = new List<JsonConverter>() { new JSONDateTimeJsonConverter(), new StringEnumConverter() }
                };

            this.Response.Clear();
            this.Response.Write(
                JsonConvert.SerializeObject(
                    retorno,
                    Formatting.None,
                    jsonSerializerSettings
            ));
            this.Response.End();
        }

        protected virtual object OnExecutarAcao(string acao, object parametros)
        {
            return null;
        }
    }
}
