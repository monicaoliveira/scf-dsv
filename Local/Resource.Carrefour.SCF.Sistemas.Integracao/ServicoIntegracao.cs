﻿//=============================================================================================
// 1341 - Enviar na exportação para grupo ATACADAO os dados bancarios do favorecido do SCF
//=============================================================================================
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Resource.Carrefour.SCF.Contratos.Integracao;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library;
using Resource.Framework.Library.Integracao.Arquivos;
using Resource.Framework.Library.Db;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Globalization;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using System.Text.RegularExpressions;
using System.Web;

//=============================================================================================00
namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    //=============================================================================================01
    public class ServicoIntegracao : IServicoIntegracao
    {
        //=============================================================================================
        // Create a logger for use in this class
        //=============================================================================================
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //=============================================================================================
        #region IServicoIntegracao Members

        //=============================================================================================A02
        public ImportarArquivoResponse ImportarArquivo(ImportarArquivoRequest request)
        {
            //=============================================================================================
            // Pega informações do processo (SCF1142 e SCF1177 - Marcos Matsuoka)
            //=============================================================================================
            ProcessoInfo processo = PersistenciaHelper.Receber<ProcessoInfo>(request.CodigoSessao, request.CodigoProcesso);

            //=============================================================================================
            // Prepara resposta
            //=============================================================================================
            ImportarArquivoResponse resposta = new ImportarArquivoResponse();
            resposta.PreparaResposta(request);

            //=============================================================================================
            // Bloco de controle
            //=============================================================================================A03
            try
            {
                //==========================================================================================
                // Se tem processo, efetua log para o processo
                //==========================================================================================
                if (request.CodigoProcesso != null)
                {
                    var lLog = new LogSCFProcessoEventoInfo();
                    lLog.setCodigoProcesso(request.CodigoProcesso);
                    lLog.setDescricao("IMPORTACAO - Arquivo [" + request.CaminhoArquivo + "] Tipo [" + request.TipoArquivo + "]"); //1333
                    LogHelper.Salvar(null, lLog);
                }
                //==========================================================================================
                // Carrega o layout do arquivo
                //==========================================================================================
                string TipoArquivoLayout = request.TipoArquivo;
                int vtamanhostring = request.TipoArquivo.Length;
                if (vtamanhostring > 11)
                {
                    TipoArquivoLayout = request.TipoArquivo.Substring(0, 11);
                }
                if (TipoArquivoLayout != "ArquivoTSYS")
                    TipoArquivoLayout = request.TipoArquivo;
                //==========================================================================================
                // Cria a instancia do interpretador do arquivo //1333
                //==========================================================================================
                Type tipoArquivo = Type.GetType(typeof(ArquivoTSYS).Namespace + "." + TipoArquivoLayout + "," + typeof(ArquivoTSYS).Assembly.FullName);
                ArquivoBase interpretadorArquivo = (ArquivoBase)Activator.CreateInstance(tipoArquivo);
                //==========================================================================================
                // Importa o arquivo
                //==========================================================================================
                resposta.CodigoArquivo = interpretadorArquivo.ImportarArquivo(request.CaminhoArquivo, request.CodigoProcesso, request.CodigoGrupoEmpresaArquivo).ToString();
                //==========================================================================================
                //1407 - INCORPORANDO ESTA CONFERENCIA NO ArquivoTSYS [validarImportacaoTSYS] SEM IMPORTAR O ARQUIVO
                //==========================================================================================
                /*
                //==========================================================================================
                // Confere se não tem registro A9
                //==========================================================================================
                if (    processo.TipoEstagioAtualString == "ProcessoExtratoEstagioImportacaoTSYS" || 
                        processo.TipoEstagioAtualString == "ProcessoAtacadaoEstagioImportacaoTSYS"
                    )
                {
                    ServicoValidarRegistroSemA9 ServicoValidarRegistroSemA9 = new ServicoValidarRegistroSemA9();
                    ServicoValidarRegistroSemA9.ValidarRegistroSemA9(resposta.CodigoArquivo);
                }
                */
            }
            //=============================================================================================A03
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            //=============================================================================================A03
            return resposta;
        }
        //=============================================================================================A02


        //=============================================================================================B02
        public IniciarProcessamentoDesbloqueiosResponse IniciarProcessamentoDesbloqueios(IniciarProcessamentoDesbloqueiosRequest request)
        {
            //=============================================================================================
            // Prepara variaveis
            //=============================================================================================
            IniciarProcessamentoDesbloqueiosResponse resposta = new IniciarProcessamentoDesbloqueiosResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso != null)
            {
                var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                //=============================================================================================B03
                // Bloco de controle
                //=============================================================================================B03
                try
                {
                    //=============================================================================================
                    // Log
                    //=============================================================================================
                    lLogSCFProcessoEstagioEventoInfo.setDescricao("CAPTURA DESBLOQUEIO - Verificando se há itens desbloqueados para processar ");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    //=============================================================================================
                    // Carrega quantidade de registros desbloqueados que pegarão carona neste processo
                    //=============================================================================================
                    int qtde;
                    using (OracleConnection cn = Procedures.ReceberConexao())
                    {
                        qtde = Procedures.PR_PROCESSAR_DESBLOQUEADOS(cn, request.CodigoArquivo, request.CodigoProcesso);
                    }
                    //=============================================================================================
                    // Log
                    //=============================================================================================
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(String.Format("CAPTURA DESBLOQUEIO - Foram incorporadas {0} linhas desbloqueadas", qtde.ToString()));
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
                //=============================================================================================B03
                catch (Exception ex)
                {
                    resposta.ProcessarExcessao(ex);
                }
                //=============================================================================================B03
            }
            return resposta;
        }
        //=============================================================================================B02


        //=============================================================================================C02
        public ValidarArquivoCessaoResponse ValidarArquivoCessao(ValidarArquivoCessaoRequest request)
        {
            //=============================================================================================
            // prepara a resposta
            //=============================================================================================
            ValidarArquivoCessaoResponse resposta = new ValidarArquivoCessaoResponse();
            resposta.PreparaResposta(request);

            //=============================================================================================C03
            // Bloco de controle
            //=============================================================================================C03
            try
            {
                //=============================================================================================
                // Pede informacoes do arquivo solicitado
                //=============================================================================================
                ArquivoInfo arquivoInfo = PersistenciaHelper.Receber<ArquivoInfo>(request.CodigoSessao, request.CodigoArquivo);
                //=============================================================================================
                // Cria instancia do tipo do arquivo
                //=============================================================================================
                ArquivoBase arquivoBase = (ArquivoBase)Activator.CreateInstance(ResolutorTipos.Resolver(arquivoInfo.TipoArquivo));
                //=============================================================================================
                // Pede a validação
                //=============================================================================================
                resposta.QuantidadeCriticas = arquivoBase.ValidarArquivo(request.CodigoProcesso, request.CodigoArquivo);
            }
            //=============================================================================================C03
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            //=============================================================================================C03

            return resposta;
        }
        //=============================================================================================C02


        //=============================================================================================D02
        public ValidarArquivoResponse ValidarArquivo(ValidarArquivoRequest request)
        {
            //=============================================================================================
            // Prepara resposta
            //=============================================================================================
            ValidarArquivoResponse resposta = new ValidarArquivoResponse();
            resposta.PreparaResposta(request);

            //=============================================================================================D03
            // Bloco de controle
            //=============================================================================================D03
            try
            {
                //=============================================================================================
                // Pede informacoes do arquivo solicitado
                //=============================================================================================
                ArquivoInfo arquivoInfo = PersistenciaHelper.Receber<ArquivoInfo>(request.CodigoSessao, request.CodigoArquivo);
                //=============================================================================================
                // Cria instancia do tipo do arquivo
                //=============================================================================================
                ArquivoBase arquivoBase = (ArquivoBase)Activator.CreateInstance(ResolutorTipos.Resolver(arquivoInfo.TipoArquivo));
                //=============================================================================================
                // Se tipo arquivo não for nulo carrega o layout
                //=============================================================================================
                if (arquivoInfo.TipoArquivo != null)
                    arquivoBase = (ArquivoBase)Activator.CreateInstance(ResolutorTipos.Resolver(arquivoInfo.TipoArquivo), arquivoInfo.LayoutArquivo);
                //=============================================================================================
                // Verifica se foi informado o código do grupo para validação
                // O CÓDIGO DO GRUPO SÓ ESTÁ SENDO INFORMADO CASO ESTEJA SENDO EXECUTADO UM PROCESSO DESDE O INÍCIO. 
                // NECESSÁRIO AJUSTAR O SISTEMA PARA SEMPRE TER O CÓDIGO DO GRUPO
                //=============================================================================================
                if (request.Contexto.ContainsKey("CodigoGrupo") && request.Contexto["CodigoGrupo"].ToString() != "")
                    resposta.QuantidadeCriticas = arquivoBase.ValidarArquivo(request.CodigoProcesso, request.CodigoArquivo, request.Contexto, request.CodigoSessao);
            }
            //=============================================================================================D03
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            //=============================================================================================D03

            return resposta;
        }
        //=============================================================================================D02

        //=============================================================================================E02
        public CancelarArquivoResponse CancelarArquivo(CancelarArquivoRequest request)
        {
            // Prepara a resposta
            CancelarArquivoResponse resposta = new CancelarArquivoResponse();

            // Bloco de controle
            try
            {
                // caso seja para excluir "flegar" os arquivos item como exluidos
                if (request.ExcluiArquivoItem)
                {
                    Mensageria.Processar<CancelarArquivoItemResponse>(
                        new CancelarArquivoItemRequest()
                        {
                            CodigoArquivo = request.CodigoArquivo
                        });
                }

                // Pega o arquivo
                ArquivoInfo arquivoInfo =
                    PersistenciaHelper.Receber<ArquivoInfo>(
                        request.CodigoSessao, request.CodigoArquivo);

                // Cancela
                arquivoInfo.StatusArquivo = ArquivoStatusEnum.Cancelado;

                // Salva
                PersistenciaHelper.Salvar<ArquivoInfo>(
                    request.CodigoSessao, arquivoInfo);

            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================E02

        //=============================================================================================F02
        public CancelarArquivoItemResponse CancelarArquivoItem(CancelarArquivoItemRequest request)
        {
            // Prepara a resposta
            CancelarArquivoItemResponse resposta = new CancelarArquivoItemResponse();

            // Bloco de controle
            try
            {
                // caso venha solicitacao de atualizar apenas um arquivo item
                if (request.CodigoArquivoItem != null)
                {
                    // Pega o arquivo item
                    ArquivoItemInfo arquivoItemInfo =
                        PersistenciaHelper.Receber<ArquivoItemInfo>(
                            request.CodigoSessao, request.CodigoArquivoItem);

                    // Cancela
                    arquivoItemInfo.StatusArquivoItem = ArquivoItemStatusEnum.Excluido;

                    // Salva
                    PersistenciaHelper.Salvar<ArquivoItemInfo>(
                        request.CodigoSessao, arquivoItemInfo);
                }
                // caso venha solicitacao de atualizar todos os itens de um arquivo
                else if (request.CodigoArquivo != null)
                {
                    // cria as condicoes
                    List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                    condicoes.Add(
                        new CondicaoInfo("CodigoArquivo", CondicaoTipoEnum.Igual, request.CodigoArquivo));

                    // pega a lista de itens do arquivo
                    List<ArquivoItemInfo> listaArquivoItem =
                        PersistenciaHelper.Listar<ArquivoItemInfo>(request.CodigoSessao, condicoes);

                    // atualiza o status de cada um dos itens de arquivo
                    foreach (ArquivoItemInfo arquivoItemInfo in listaArquivoItem)
                    {
                        // Cancela
                        arquivoItemInfo.StatusArquivoItem = ArquivoItemStatusEnum.Excluido;

                        // Salva
                        PersistenciaHelper.Salvar<ArquivoItemInfo>(
                            request.CodigoSessao, arquivoItemInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;

        }
        //=============================================================================================F02


        //=============================================================================================G02
        public ProcessoExtratoVerificarCancelamentoOnlineResponse ProcessoExtratoVerificarCancelamentoOnline(ProcessoExtratoVerificarCancelamentoOnlineRequest request)
        {
            // Prepara a resposta
            ProcessoExtratoVerificarCancelamentoOnlineResponse resposta = new ProcessoExtratoVerificarCancelamentoOnlineResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // chama a procedure para ajustar os valores, caso tenha
                Procedures.PR_CANCELAMENTO_ONLINE(new PR_CANCELAMENTO_ONLINE_Request()
                {
                    CodigoProcesso = request.CodigoProcesso, //1391
                    CodigoArquivo = request.CodigoArquivo,
                });
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;

        }
        //=============================================================================================G02


        //=============================================================================================H02
        public ImportarTransacoesTSYSResponse ImportarTransacoesTSYS(ImportarTransacoesTSYSRequest request)
        {
            TransacaoStatusEnum tranStatus = TransacaoStatusEnum.Importado;
            ProcessoInfo p = Mensageria.Processar<ReceberProcessoResponse>(new ReceberProcessoRequest()
            {
                CodigoSessao = request.CodigoSessao,
                CodigoProcesso = request.CodigoProcesso
            }).ProcessoInfo;

            // Retorna a configuracao geral
            ConfiguracaoGeralInfo configGeral =
                PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                    request.CodigoSessao, null);

            // SCF1701 - ID 29
            // Retirado regra inválida
            //if (p != null)
            //{
            //    if (p.TipoProcesso == typeof(ProcessoExtratoInfo).Name)
            //    {
            //        if (configGeral.EnviarCessaoAutomatico)
            //            tranStatus = TransacaoStatusEnum.EnviadoCCI;
            //    }
            //}

            // Prepara a resposta
            ImportarTransacoesTSYSResponse resposta = new ImportarTransacoesTSYSResponse();
            resposta.PreparaResposta(request);

            // Prepara lista de lojas a incluir
            List<string> incluirLojas = null;
            if (request.IncluirLojas != null)
                incluirLojas = new List<string>(request.IncluirLojas.Split(';'));

            // Abre conexao Oracle
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                OracleRefCursor cursor = null;
                OracleRefCursor cursorAuxiliar = null;
                // OracleRefCursor cursorInferirLayout = null; // warning 25/02/2019
                var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();//1328
                //=======================================================================================================================
                // Bloco de controle
                //=======================================================================================================================
                try
                {
                    //=======================================================================================================================
                    // Inicializa as procedures
                    //=======================================================================================================================
                    OracleCommand cmTransacaoVendaS = Procedures.PR_TRANSACAO_VENDA_S_preparar(cn);
                    OracleCommand cmTransacaoPagamentoS = Procedures.PR_TRANSACAO_PAGAMENTO_S_preparar(cn);
                    OracleCommand cmTransacaoAnulacaoVendaS = Procedures.PR_TRANSACAO_ANULACAO_VENDA_S_preparar(cn);
                    OracleCommand cmTransacaoAnulacaoPagtoS = Procedures.PR_TRANSACAO_ANULACAO_PAGTO_S_preparar(cn);
                    OracleCommand cmTransacaoAjusteS = Procedures.PR_TRANSACAO_AJUSTE_S_preparar(cn);
                    //=======================================================================================================================
                    // AMANDA - 20151002
                    // Cria o interpretador para o arquivo da TSYS
                    // ArquivoTSYS arquivoTSYS = new ArquivoTSYS();
                    // Faz o parse do codigo do arquivo para repassar para as procedures
                    //=======================================================================================================================
                    int codigoArquivo = int.Parse(request.CodigoArquivo);
                    //=======================================================================================================================
                    // Pede lista de estabelecimentos e indexa o CodigoEstabelecimento por CNPJ
                    //=======================================================================================================================
                    List<EstabelecimentoInfo> estabelecimentos = PersistenciaHelper.Listar<EstabelecimentoInfo>(request.CodigoSessao);
                    Dictionary<string, int> dicionarioEstabelecimento = new Dictionary<string, int>();
                    foreach (EstabelecimentoInfo estabelecimento in estabelecimentos)
                        if (estabelecimento.CNPJ != null)
                            if (!dicionarioEstabelecimento.ContainsKey(estabelecimento.CNPJ))
                                dicionarioEstabelecimento.Add(estabelecimento.CNPJ, int.Parse(estabelecimento.CodigoEstabelecimento));
                    //=======================================================================================================================
                    // Pede lista de produtos e indexa o CodigoEstabelecimento por descricao
                    //=======================================================================================================================
                    List<ProdutoInfo> produtos = PersistenciaHelper.Listar<ProdutoInfo>(request.CodigoSessao);
                    Dictionary<string, ProdutoInfo> dicionarioProduto = new Dictionary<string, ProdutoInfo>();
                    foreach (ProdutoInfo produto in produtos)
                        dicionarioProduto.Add(produto.NomeProduto, produto);
                    //=======================================================================================================================
                    // pede a lista de referencias
                    //=======================================================================================================================
                    List<ListaItemReferenciaInfo> listaItemReferencias = PersistenciaHelper.Listar<ListaItemReferenciaInfo>(request.CodigoSessao);
                    //=======================================================================================================================
                    // monta o dicionario de referencias para o meio de pagamento
                    //=======================================================================================================================
                    Dictionary<string, string> dicionarioReferenciaProdutoAJ =
                        MontaDicionarioReferencia(
                            ListaItemReferenciaSituacaoEnum.CodigoAjuste,
                            ListaItemReferenciaNomeEnum.Produto,
                            listaItemReferencias
                        );
                    //=======================================================================================================================
                    // monta o dicionario de referencias para o meio de pagamento
                    //=======================================================================================================================
                    Dictionary<string, string> dicionarioReferenciaProdutoAV =
                        MontaDicionarioReferencia(
                            ListaItemReferenciaSituacaoEnum.CodigoAnulacao,
                            ListaItemReferenciaNomeEnum.Produto,
                            listaItemReferencias
                        );
                    //=======================================================================================================================
                    // Verifica se o produto RCAR existe, senão, informa o erro
                    // Este produto é pré-requisito pois é utilizado como fixo no processo Atacadao
                    //=======================================================================================================================
                    if (!dicionarioProduto.ContainsKey("RCAR"))
                        throw new Exception("Não foi encontrado o cadastro do produto RCAR. É necessário o cadastramento deste produto para prosseguir com o processo de importação de transações da TSYS");
                    if (!dicionarioProduto.ContainsKey("RECTO"))
                        throw new Exception("Não foi encontrado o cadastro do produto RECTO. É necessário o cadastramento deste produto para prosseguir com o processo de importação de transações da TSYS");
                    //=======================================================================================================================
                    // Preapra variaveis
                    //=======================================================================================================================
                    int? codigoProdutoAjuste = null;
                    int? codigoProdutoAnulacaoVenda = null;
                    int? codigoProdutoAnulacaoPagamento = null;
                    int? codigoProdutoPagamento = null;
                    int? codigoProdutoVenda = null;
                    //=======================================================================================================================
                    // Faz o cache dos produtos
                    //=======================================================================================================================
                    if (request.TipoProcesso == "Atacadao")
                    {
                        codigoProdutoAjuste = int.Parse(dicionarioProduto["RCAR"].CodigoProduto);
                        codigoProdutoAnulacaoVenda = int.Parse(dicionarioProduto["RCAR"].CodigoProduto);
                        codigoProdutoAnulacaoPagamento = int.Parse(dicionarioProduto["RECTO"].CodigoProduto);
                        codigoProdutoPagamento = int.Parse(dicionarioProduto["RECTO"].CodigoProduto);
                        codigoProdutoVenda = int.Parse(dicionarioProduto["RCAR"].CodigoProduto);
                    }
                    else
                    {
                        codigoProdutoAjuste = int.Parse(dicionarioProduto["RECTO"].CodigoProduto);
                        codigoProdutoAnulacaoPagamento = int.Parse(dicionarioProduto["RECTO"].CodigoProduto); // JIRA SCF-520
                        codigoProdutoPagamento = int.Parse(dicionarioProduto["RECTO"].CodigoProduto); // JIRA SCF-520
                        codigoProdutoVenda = null;
                    }
                    //=======================================================================================================================
                    // status do retorno de cessao inicial para todos os tipos de registro
                    //=======================================================================================================================
                    TransacaoStatusRetornoCessaoEnum statusRetornoCessao = TransacaoStatusRetornoCessaoEnum.NaoRetornado;
                    //=======================================================================================================================
                    // armazenará a data do movimento do registro L0 para criar os registros NSU corretamente
                    //=======================================================================================================================
                    DateTime dataMovimento = new DateTime();
                    //=======================================================================================================================
                    // Pede lista de planos e indexa o CodigoPlanoTSYS 
                    //=======================================================================================================================
                    List<PlanoInfo> planos = PersistenciaHelper.Listar<PlanoInfo>(request.CodigoSessao);
                    Dictionary<string, int> dicionarioPlano = new Dictionary<string, int>();
                    foreach (PlanoInfo plano in planos)
                        dicionarioPlano.Add(plano.CodigoPlanoTSYS, int.Parse(plano.CodigoPlano));
                    //=======================================================================================================================
                    // AMANDA - 20151001
                    // BUSCA O ARQUIVO PARA PEGAR O LAYOUT
                    // Pede detalhe do arquivo
                    //=======================================================================================================================
                    ArquivoInfo arquivoInfo = PersistenciaHelper.Receber<ArquivoInfo>(request.CodigoSessao, request.CodigoArquivo);
                    //=======================================================================================================================
                    // Cria o interpretador para o arquivo da TSYS
                    //=======================================================================================================================
                    ArquivoTSYS arquivoTSYS = new ArquivoTSYS(arquivoInfo.LayoutArquivo);
                    //=======================================================================================================================
                    // AMANDA - FIM
                    //=======================================================================================================================
                    // ROGERIO - Inicializa Lista de BIN X REFERENCIA
                    //=======================================================================================================================
                    Dictionary<string, BinReferenciaInfo> dicionarioBinRef = null;
                    dicionarioBinRef = new Dictionary<string, BinReferenciaInfo>();
                    List<BinReferenciaInfo> BinReferencias =
                            PersistenciaHelper.Listar<BinReferenciaInfo>(null);
                    foreach (BinReferenciaInfo binReferencia in BinReferencias)
                        if (binReferencia.CodigoBin != null)
                        {
                            if (!dicionarioBinRef.ContainsKey(binReferencia.CodigoBin))
                                dicionarioBinRef.Add(binReferencia.CodigoBin, binReferencia);
                        }
                    //=======================================================================================================================
                    //FIM ROGERIO
                    //=======================================================================================================================
                    // Consulta as transacoes do arquivo para achar a L0 e determinar a data de movimento
                    //=======================================================================================================================
                    cursorAuxiliar = Procedures.PR_ARQUIVO_ITEM_L(cn, request.CodigoArquivo, null, true, true, 0, 0, 0, false, true); // traz apenas os itens sem critica e nao bloqueados
                    using (OracleDataReader readerAuxiliar = cursorAuxiliar.GetDataReader())
                    {
                        //=======================================================================================================================
                        //Faz a leitura da data do movimento
                        //=======================================================================================================================
                        while (readerAuxiliar.Read())
                        {
                            //=======================================================================================================================
                            // Pega informacoes da linha
                            //=======================================================================================================================
                            string linha = readerAuxiliar.GetString(readerAuxiliar.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));
                            //=======================================================================================================================
                            //Verifica se a linha é L0 para pegar a data do movinemto
                            //=======================================================================================================================
                            if (arquivoTSYS.InferirTipoLinha(linha).Equals("L0"))
                            {
                                dataMovimento = (DateTime)arquivoTSYS.LerCampo("L0", linha, "DataMovimento", true);
                                break;
                            }
                        }
                        //=======================================================================================================================
                        // Finaliza o cursor
                        //=======================================================================================================================
                        if (cursorAuxiliar != null)
                            cursorAuxiliar.Dispose();
                    }
                    //=======================================================================================================================
                    // Consulta as transacoes do arquivo
                    //=======================================================================================================================
                    cursor = Procedures.PR_ARQUIVO_ITEM_L(cn, request.CodigoArquivo, null, true, true, 0, 0, 0, false, true); // traz apenas os itens sem critica e nao bloqueados
                    using (OracleDataReader reader = cursor.GetDataReader())
                    {
                        //=======================================================================================================================
                        // Sinalizacoes para o log
                        //=======================================================================================================================
                        int quantidadeLog = 0;
                        int numeroLinha = 0;
                        int numeroTransacao = 0;
                        //=======================================================================================================================
                        // Varre as linhas
                        //=======================================================================================================================
                        while (reader.Read())
                        {
                            //=======================================================================================================================
                            // Pega informacoes da linha
                            //=======================================================================================================================
                            string linha = reader.GetString(reader.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));
                            //=======================================================================================================================
                            // Pega o codigo arquivo item
                            //=======================================================================================================================
                            int codigoArquivoItem = Convert.ToInt32(reader.GetDouble(reader.GetOrdinal("CODIGO_ARQUIVO_ITEM")));
                            //=======================================================================================================================
                            // Pega o codigo arquivo item
                            //=======================================================================================================================
                            int statusArquivoItem = Convert.ToInt32(reader["STATUS_ARQUIVO_ITEM"]);
                            //SCF1701 - ID 29
                            //Tratando Status da Transação
                            tranStatus = validaStatusTransacao(statusArquivoItem);

                            //=======================================================================================================================
                            // Verifica o tipo da linha
                            //=======================================================================================================================
                            string tipoTransacao = arquivoTSYS.InferirTipoLinha(linha);
                            //=======================================================================================================================
                            // Se deve filtrar por loja, para os registros diferentes de header e trailer
                            // verifica se a loja deve ser incluida
                            //=======================================================================================================================
                            if ((tipoTransacao == "CV" || tipoTransacao == "CP" ||
                                 tipoTransacao == "AV" || tipoTransacao == "AP" ||
                                 tipoTransacao == "AJ") && incluirLojas != null)
                                if (!incluirLojas.Contains((string)arquivoTSYS.LerCampo(tipoTransacao, linha, "IdentificacaoLoja", false)))
                                    continue;
                            //=======================================================================================================================
                            // Processa de acordo com o tipo da linha
                            //=======================================================================================================================
                            switch (tipoTransacao)
                            {
                                case "L0":
                                    dataMovimento = (DateTime)arquivoTSYS.LerCampo("L0", linha, "DataMovimento", true);
                                    break;

                                case "AJ":
                                    //=======================================================================================================================
                                    // Atualiza numero de transacoes
                                    //=======================================================================================================================
                                    numeroTransacao++;
                                    //=======================================================================================================================
                                    // Prepara campos que serao chamados mais de uma vez
                                    //=======================================================================================================================
                                    DateTime dataTransacaoAJ = (DateTime)arquivoTSYS.LerCampo("AJ", linha, "DataTransacao", true);
                                    dataTransacaoAJ += (TimeSpan)arquivoTSYS.LerCampo("AJ", linha, "HorarioTransacao", true);
                                    double valorTransacaoAJ = (double)arquivoTSYS.LerCampo("AJ", linha, "ValorBruto", true);
                                    string nsuHostAJ = (string)arquivoTSYS.LerCampo("AJ", linha, "NSUHostTransacao", true);
                                    string nsuHostOriginalAJ = (string)arquivoTSYS.LerCampo("AJ", linha, "NSUHostTransacaoOriginal", true);
                                    double valorLiquidoAJ = (double)arquivoTSYS.LerCampo("AJ", linha, "ValorLiquido", true);
                                    string tipoAjusteAJ = (string)arquivoTSYS.LerCampo("AJ", linha, "TipoAjuste", true);
                                    string codigoAjuste = (string)arquivoTSYS.LerCampo("AJ", linha, "CodigoAjuste", true);
                                    //=======================================================================================================================
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Inicio
                                    //=======================================================================================================================
                                    string tipoFinanceiroContabilAJ = arquivoTSYS.VerficarFinanceiroContabil("AJ", linha);
                                    //=======================================================================================================================
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Fim
                                    //=======================================================================================================================

                                    //=======================================================================================================================
                                    // ECOMMERCE - Fernando Bove - Criar variável para colocar o NumeroContaCliente
                                    //=======================================================================================================================
                                    string numeroContaCliente = "";
                                    if (arquivoInfo.LayoutArquivo.Equals("2") || arquivoInfo.LayoutArquivo.Equals("4"))
                                        numeroContaCliente = (string)arquivoTSYS.LerCampo("AJ", linha, "NumeroContaCliente", true);
                                    //=======================================================================================================================
                                    //SCF1148
                                    //=======================================================================================================================
                                    string numeroCartao = (string)arquivoTSYS.LerCampo("AJ", linha, "NumeroCartaoTransacaoOriginal", true);
                                    //=======================================================================================================================
                                    //double fatorMultiplicador = 1;
                                    //if (tipoAjusteAJ == "1")
                                    //    fatorMultiplicador = -1;
                                    // ajusta os fatores multiplicadores
                                    //=======================================================================================================================
                                    double fatorMultiplicadorVLBrutoLiquido = 1;
                                    double fatorMultiplicadorVLDesconto = 1;
                                    //=======================================================================================================================
                                    // ECOMMERCE - Fernando Bove - Ajustar fatores para seguirem o TipoAjuste do arquivo - Inicio
                                    //=======================================================================================================================
                                    //if (codigoAjuste == "0101" || codigoAjuste == "0151")
                                    if (tipoAjusteAJ == "1")
                                        fatorMultiplicadorVLDesconto = -1;
                                    //if (codigoAjuste == "0102" || codigoAjuste == "0152")
                                    else if (tipoAjusteAJ == "2")
                                        fatorMultiplicadorVLBrutoLiquido = -1;
                                    //=======================================================================================================================
                                    // ECOMMERCE - Fernando Bove - Ajustar fatores para seguirem o TipoAjuste do arquivo - Fim
                                    //=======================================================================================================================
                                    // Traduz o estabelecimento
                                    //=======================================================================================================================
                                    int codigoEstabelecimentoAJ = this.traduzirEstabelecimento(request.CodigoSessao, (string)arquivoTSYS.LerCampo("AJ", linha, "IdentificacaoLoja", true), dicionarioEstabelecimento);
                                    //=======================================================================================================================
                                    // ajusta o codigo do produto de acordo com o codigo do ajuste
                                    //=======================================================================================================================
                                    if (dicionarioReferenciaProdutoAJ.Count > 0 && dicionarioReferenciaProdutoAJ.Count > 0 && dicionarioReferenciaProdutoAJ.ContainsKey(codigoAjuste) && dicionarioProduto.ContainsKey(dicionarioReferenciaProdutoAJ[codigoAjuste]))
                                        codigoProdutoAjuste = int.Parse(dicionarioProduto[dicionarioReferenciaProdutoAJ[codigoAjuste].ToString()].CodigoProduto);
                                    //=======================================================================================================================
                                    //SCF1088 - Marcos Matsuoka - INICIO
                                    //> alterando o campo ValorBruto para valorLiquidoAJ e NSUHostTransacaoOriginal para NSUHostTransacao
                                    //string _chaveAJ = ChaveTransacaoHelper.GerarChaveAjuste(dataTransacaoAJ, codigoEstabelecimentoAJ.ToString(), valorTransacaoAJ, nsuHostOriginalAJ); 
                                    //=======================================================================================================================
                                    string strValorLiquidoAJ = "";
                                    //=======================================================================================================================
                                    // Formato e garanto as apenas duas casas decimais
                                    //=======================================================================================================================
                                    strValorLiquidoAJ = string.Format("{0:N}", valorLiquidoAJ);
                                    //=======================================================================================================================
                                    // Limpo o numero de caracteres não numéricos
                                    //=======================================================================================================================
                                    strValorLiquidoAJ = Regex.Replace(strValorLiquidoAJ, "[\\D]", "");
                                    //=======================================================================================================================
                                    // Acerto o tamanho do número
                                    //strValorLiquidoAJ = strValorLiquidoAJ.PadLeft(16, '0');
                                    //=======================================================================================================================
                                    string _chaveAJ = ChaveTransacaoHelper.GerarChaveAjuste(dataTransacaoAJ, codigoEstabelecimentoAJ.ToString(), strValorLiquidoAJ, nsuHostAJ);
                                    //=======================================================================================================================
                                    //SCF1088 - Marcos Matsuoka - FIM
                                    //=======================================================================================================================
                                    BinReferenciaInfo RefBinAJ = dicionarioBinRef[numeroCartao.Substring(3, 6)];
                                    //=======================================================================================================================
                                    // Executa a procedure
                                    //=======================================================================================================================
                                    Procedures.PR_TRANSACAO_AJUSTE_S(cmTransacaoAjusteS,
                                        new PR_TRANSACAO_AJUSTE_S_Request()
                                        {
                                            CodigoTransacao = null,
                                            CodigoArquivo = codigoArquivo,
                                            CodigoArquivoItem1 = codigoArquivoItem,
                                            Chave = _chaveAJ,
                                            Chave3 = _chaveAJ.Substring(0, 8) + "000000" + _chaveAJ.Substring(14),
                                            CodigoEstabelecimento = codigoEstabelecimentoAJ,
                                            CodigoProduto = codigoProdutoAjuste,
                                            CodigoAjuste = (string)arquivoTSYS.LerCampo("AJ", linha, "CodigoAjuste", true),
                                            DataRepasse = (DateTime)arquivoTSYS.LerCampo("AJ", linha, "DataRepasse", true),
                                            DataTransacao = dataTransacaoAJ,
                                            DataTransacaoOriginal = (DateTime)arquivoTSYS.LerCampo("AJ", linha, "DataTransacaoOriginal", true),
                                            ValorAjuste = valorTransacaoAJ * fatorMultiplicadorVLBrutoLiquido,
                                            ValorDesconto = (double)arquivoTSYS.LerCampo("AJ", linha, "ValorDescontoOuComissao", true) * fatorMultiplicadorVLDesconto,
                                            ValorLiquido = valorLiquidoAJ * fatorMultiplicadorVLBrutoLiquido,
                                            NsuHost = nsuHostAJ,
                                            NsuHostOriginal = nsuHostOriginalAJ,
                                            TipoAjuste = tipoAjusteAJ,
                                            MotivoAjuste = (string)arquivoTSYS.LerCampo("AJ", linha, "DescricaoMotivoAjuste", true),
                                            TipoLancamento = (string)arquivoTSYS.LerCampo("AJ", linha, "TipoLancamento", true),
                                            Banco = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("AJ", linha, "Banco", true) : null,
                                            Agencia = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("AJ", linha, "Agencia", true) : null,
                                            Conta = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("AJ", linha, "Conta", true) : null,
                                            RetornarRegistro = false,
                                            CodigoProcesso = request.CodigoProcesso,
                                            ValorRepasse = valorLiquidoAJ,
                                            StatusRetornoCessao = statusRetornoCessao,
                                            StatusTransacao = tranStatus, // SCF1701 - ID 29 - Regra definida com base na Arquivo Item
                                            DataMovimento = dataMovimento,
                                            NumeroLinhaArquivoTsys = Convert.ToString((int)arquivoTSYS.LerCampo("AJ", linha, "NSEQ", true)),
                                            AtualizaTransacao = configGeral.ReprocessarNSUProcessado,
                                            // ECOMMERCE - Fernando Bove - 20151127 - envia o numero da conta do cliente e o indicador financeiro/contabil caso existam - Início
                                            NumeroContaCliente = numeroContaCliente,
                                            NumeroCartao = numeroCartao, //SCF1148 - Marcos Matsuoka
                                            TipoFinanceiroContabil = tipoFinanceiroContabilAJ,
                                            // ECOMMERCE - Fernando Bove - 20151127 - envia o numero da conta do cliente e o indicador financeiro/contabil caso existam - Fim
                                            BINRefTransacao = RefBinAJ.CodigoReferencia // REFERENCIA X BIN - ClockWork (Resource - Rogerio)

                                        });

                                    break;

                                case "AP":
                                    // Atualiza numero de transacoes
                                    numeroTransacao++;

                                    // Prepara campos que serao chamados mais de uma vez
                                    DateTime dataTransacaoAP = (DateTime)arquivoTSYS.LerCampo("AP", linha, "DataTransacao", true);
                                    dataTransacaoAP += (TimeSpan)arquivoTSYS.LerCampo("AP", linha, "HorarioTransacao", true);
                                    string nsuTefOriginalAP = (string)arquivoTSYS.LerCampo("AP", linha, "NSUTefTransacaoOriginal", true);
                                    string nsuHostOriginalAP = (string)arquivoTSYS.LerCampo("AP", linha, "NSUHostTransacaoOriginal", true);
                                    string nsuHostAP = (string)arquivoTSYS.LerCampo("AP", linha, "NSUHostTransacao", true);
                                    double valorLiquidoAP = (double)arquivoTSYS.LerCampo("AP", linha, "ValorLiquidoPagamento", true);
                                    DateTime dataReembolsoEstabelecimento = (DateTime)arquivoTSYS.LerCampo("AP", linha, "DataReembolsoEstabelecimento", true);
                                    string formaMeioPagamentoAP = (string)arquivoTSYS.LerCampo("AP", linha, "FormaMeioPagamento", true);
                                    string codigoAnulacaoAP = (string)arquivoTSYS.LerCampo("AP", linha, "CodigoAnulacao", true);
                                    //string statusAP = (string)arquivoTSYS.LerCampo("AP", linha, "", true);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Inicio
                                    string tipoFinanceiroContabilAP = arquivoTSYS.VerficarFinanceiroContabil("AP", linha);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Fim

                                    //// ajusta a forma de pagamento
                                    //if (dicionarioReferenciaMeioPagamento.Count > 0)
                                    //    formaMeioPagamentoAP = dicionarioReferenciaMeioPagamento[codigoAnulacaoAP].ToString();

                                    // Traduz o estabelecimento
                                    int codigoEstabelecimentoAP = this.traduzirEstabelecimento(request.CodigoSessao, (string)arquivoTSYS.LerCampo("AP", linha, "IdentificacaoLoja", true), dicionarioEstabelecimento);
                                    //DateTime dataCorrigidaTransacaoAP = Convert.ToDateTime(dataTransacaoAP.ToString().Substring(0, 10) + " 00:00:00");
                                    //1285 UTILIZAVA NSU HOST ORIGINAL string _chaveAP = ChaveTransacaoHelper.GerarChaveAnulacaoPagamento(dataTransacaoAP, codigoEstabelecimentoAP.ToString(), nsuHostOriginalAP, (string)arquivoTSYS.LerCampo("AP", linha, "NSEQ", false));
                                    string _chaveAP = ChaveTransacaoHelper.GerarChaveAnulacaoPagamento(dataTransacaoAP, codigoEstabelecimentoAP.ToString(), nsuHostAP, (string)arquivoTSYS.LerCampo("AP", linha, "NSEQ", false)); // 1285 utilizando NSU HOST TRANSACAO

                                    //Numero da Referencia de acordo com o BIN (numero do cartão)
                                    string numeroCartaoAP = (string)arquivoTSYS.LerCampo("AP", linha, "NumeroCartao", true);
                                    BinReferenciaInfo RefBinAP = dicionarioBinRef[numeroCartaoAP.Substring(3, 6)];

                                    // Executa a procedure
                                    Procedures.PR_TRANSACAO_ANULACAO_PAGTO_S(cmTransacaoAnulacaoPagtoS,
                                        new PR_TRANSACAO_ANULACAO_PAGTO_S_Request()
                                        {
                                            CodigoTransacao = null,
                                            CodigoArquivo = codigoArquivo,
                                            CodigoArquivoItem1 = codigoArquivoItem,
                                            Chave = _chaveAP,
                                            Chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoPagamento(dataTransacaoAP, codigoEstabelecimentoAP.ToString(), nsuTefOriginalAP, (string)arquivoTSYS.LerCampo("AP", linha, "NSEQ", false)),
                                            Chave3 = _chaveAP.Substring(0, 8) + "000000" + _chaveAP.Substring(14),
                                            CodigoEstabelecimento = codigoEstabelecimentoAP,
                                            CodigoProduto = codigoProdutoAnulacaoPagamento,
                                            DataTransacao = dataTransacaoAP,
                                            DataTransacaoOriginal = (DateTime)arquivoTSYS.LerCampo("AP", linha, "DataTransacaoOriginal", true),
                                            ValorDesconto = (double)arquivoTSYS.LerCampo("AP", linha, "ValorDesconto", true) * (-1),
                                            ValorLiquido = valorLiquidoAP,
                                            NsuHost = nsuHostAP,
                                            NsuHostOriginal = nsuHostOriginalAP,
                                            TipoLancamento = (string)arquivoTSYS.LerCampo("AP", linha, "TipoLancamento", true),
                                            CodigoAnulacao = (string)arquivoTSYS.LerCampo("AP", linha, "CodigoAnulacao", true),
                                            CodigoAutorizacaoOriginal = (string)arquivoTSYS.LerCampo("AP", linha, "CodigoAutorizacaoTransacaoOriginal", true),
                                            DataReembolso = (DateTime)arquivoTSYS.LerCampo("AP", linha, "DataReembolsoEstabelecimento", true),
                                            //FormaMeioPagamento = (string)arquivoTSYS.LerCampo("AP", linha, "FormaMeioPagamento", true),
                                            FormaMeioPagamento = formaMeioPagamentoAP,
                                            MotivoAnulacao = (string)arquivoTSYS.LerCampo("AP", linha, "DescricaoMotivoAnulacao", true),
                                            NsuTEFOriginal = nsuTefOriginalAP,
                                            NumeroCartao = (string)arquivoTSYS.LerCampo("AP", linha, "NumeroCartao", true),
                                            NumeroContaCliente = (string)arquivoTSYS.LerCampo("AP", linha, "NumeroContaCliente", true),
                                            ValorPagamento = (double)arquivoTSYS.LerCampo("AP", linha, "ValorBrutoPagamento", true),
                                            RetornarRegistro = false,
                                            CodigoProcesso = request.CodigoProcesso,
                                            ValorRepasse = valorLiquidoAP,
                                            DataRepasse = dataReembolsoEstabelecimento,
                                            //DataRepasseCalculada = dataReembolsoEstabelecimento
                                            StatusTransacao = tranStatus, // SCF1701 - ID 29 - Regra definida com base na Arquivo Item
                                            StatusRetornoCessao = statusRetornoCessao,
                                            DataMovimento = dataMovimento,
                                            NumeroLinhaArquivoTsys = Convert.ToString((int)arquivoTSYS.LerCampo("AP", linha, "NSEQ", true)),
                                            AtualizaTransacao = configGeral.ReprocessarNSUProcessado,
                                            // ECOMMERCE - Fernando Bove - envia o indicador financeiro/contabil caso exista - Inicio
                                            TipoFinanceiroContabil = tipoFinanceiroContabilAP,
                                            // ECOMMERCE - Fernando Bove - envia o indicador financeiro/contabil caso exista - Fim
                                            BINRefTransacao = RefBinAP.CodigoReferencia // REFERENCIA X BIN - ClockWork (Resource - Rogerio)

                                        });

                                    break;

                                case "AV":

                                    // Atualiza numero de transacoes
                                    numeroTransacao++;

                                    // Prepara campos que serao chamados mais de uma vez
                                    DateTime dataTransacaoAV = (DateTime)arquivoTSYS.LerCampo("AV", linha, "DataTransacao", true);
                                    dataTransacaoAV += (TimeSpan)arquivoTSYS.LerCampo("AV", linha, "HorarioTransacao", true);
                                    string nsuTefOriginalAV = (string)arquivoTSYS.LerCampo("AV", linha, "NSUTefTransacaoOriginal", true);
                                    string nsuHostOriginalAV = (string)arquivoTSYS.LerCampo("AV", linha, "NSUHostTransacaoOriginal", true);
                                    string nsuHostAV = (string)arquivoTSYS.LerCampo("AV", linha, "NSUHostTransacao", true);
                                    string codAutorizacao = (string)arquivoTSYS.LerCampo("AV", linha, "CodigoAutorizacaoTransacaoOriginal", true);
                                    double valorLiquidoAV = (double)arquivoTSYS.LerCampo("AV", linha, "ValorLiquidoVenda", true) * (-1);
                                    DateTime DataReembolsoEmissor = (DateTime)arquivoTSYS.LerCampo("AV", linha, "DataReembolsoEmissor", true);
                                    int numeroParcelaAV = (int)arquivoTSYS.LerCampo("AV", linha, "NumeroParcela", true);
                                    double valorLiquidoAVParcela = (double)arquivoTSYS.LerCampo("AV", linha, "ValorLiquidoParcela", true) * (-1);
                                    string codigoAnulacaoAV = (string)arquivoTSYS.LerCampo("AV", linha, "CodigoAnulacao", true);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Inicio
                                    string tipoFinanceiroContabilAV = arquivoTSYS.VerficarFinanceiroContabil("AV", linha);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Fim

                                    // Traduz o estabelecimento
                                    int codigoEstabelecimentoAV = this.traduzirEstabelecimento(request.CodigoSessao, (string)arquivoTSYS.LerCampo("AV", linha, "IdentificacaoLoja", true), dicionarioEstabelecimento);
                                    //DateTime dataCorrigidaTransacaoAV = Convert.ToDateTime(dataTransacaoAV.ToString().Substring(0, 10) + " 00:00:00");

                                    // ajusta o codigo do produto de acordo com o codigo do ajuste
                                    if (dicionarioReferenciaProdutoAV.Count > 0)
                                        codigoProdutoAnulacaoVenda = int.Parse(dicionarioProduto[dicionarioReferenciaProdutoAV[codigoAnulacaoAV].ToString()].CodigoProduto);

                                    //string _chaveAV = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoAV, codigoEstabelecimentoAV.ToString(), nsuHostOriginalAV, codAutorizacao, numeroParcelaAV.ToString(), (string)arquivoTSYS.LerCampo("AV", linha, "NSEQ", false)); //SCF1161
                                    string _chaveAV = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoAV, codigoEstabelecimentoAV.ToString(), nsuHostAV, codAutorizacao, numeroParcelaAV.ToString());

                                    //Numero da Referencia de acordo com o BIN (numero do cartão)
                                    string numeroCartaoAV = (string)arquivoTSYS.LerCampo("AV", linha, "NumeroCartao", true);
                                    BinReferenciaInfo RefBinAV = dicionarioBinRef[numeroCartaoAV.Substring(3, 6)];

                                    // Executa a procedure
                                    Procedures.PR_TRANSACAO_ANULACAO_VENDA_S(cmTransacaoAnulacaoVendaS,
                                        new PR_TRANSACAO_ANULACAO_VENDA_S_Request()
                                        {
                                            CodigoTransacao = null,
                                            CodigoArquivo = codigoArquivo,
                                            CodigoArquivoItem1 = codigoArquivoItem,
                                            Chave = _chaveAV,
                                            //Chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoAV, codigoEstabelecimentoAV.ToString(), nsuTefOriginalAV, codAutorizacao, numeroParcelaAV.ToString(), (string)arquivoTSYS.LerCampo("AV", linha, "NSEQ", false)),//SCF1161
                                            Chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoAV, codigoEstabelecimentoAV.ToString(), nsuTefOriginalAV, codAutorizacao, numeroParcelaAV.ToString()),
                                            Chave3 = _chaveAV.Substring(0, 8) + "000000" + _chaveAV.Substring(14),
                                            CodigoEstabelecimento = codigoEstabelecimentoAV,
                                            CodigoProduto = codigoProdutoAnulacaoVenda,
                                            DataTransacao = dataTransacaoAV,
                                            DataTransacaoOriginal = (DateTime)arquivoTSYS.LerCampo("AV", linha, "DataTransacaoOriginal", true),
                                            ValorDesconto = (double)arquivoTSYS.LerCampo("AV", linha, "ValorDescontoOuComissao", true),
                                            ValorLiquido = valorLiquidoAV,
                                            NsuHost = nsuHostAV,
                                            NsuHostOriginal = nsuHostOriginalAV,
                                            TipoLancamento = (string)arquivoTSYS.LerCampo("AV", linha, "TipoLancamento", true),
                                            CodigoAnulacao = (string)arquivoTSYS.LerCampo("AV", linha, "CodigoAnulacao", true),
                                            CodigoAutorizacaoOriginal = (string)arquivoTSYS.LerCampo("AV", linha, "CodigoAutorizacaoTransacaoOriginal", true),
                                            DataReembolso = (DateTime)arquivoTSYS.LerCampo("AV", linha, "DataReembolsoEmissor", true),
                                            MotivoAnulacao = (string)arquivoTSYS.LerCampo("AV", linha, "DescricaoMotivoAnulacao", true),
                                            NsuTEFOriginal = nsuTefOriginalAV,
                                            NumeroCartao = (string)arquivoTSYS.LerCampo("AV", linha, "NumeroCartao", true),
                                            NumeroContaCliente = (string)arquivoTSYS.LerCampo("AV", linha, "NumeroContaCliente", true),
                                            Agencia = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("AV", linha, "Agencia", true) : null,
                                            Banco = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("AV", linha, "Banco", true) : null,
                                            Conta = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("AV", linha, "Conta", true) : null,
                                            NumeroParcela = numeroParcelaAV,
                                            NumeroParcelaTotal = (int)arquivoTSYS.LerCampo("AV", linha, "NumeroTotalParcelas", true),
                                            ValorAnulacao = (double)arquivoTSYS.LerCampo("AV", linha, "ValorBrutoVenda", true) * (-1),
                                            ValorParcela = (double)arquivoTSYS.LerCampo("AV", linha, "ValorBrutoParcela", true) * (-1),
                                            ValorParcelaDesconto = (double)arquivoTSYS.LerCampo("AV", linha, "ValorDescontoParcela", true),
                                            ValorParcelaLiquido = valorLiquidoAVParcela,
                                            RetornarRegistro = false,
                                            CodigoProcesso = request.CodigoProcesso,
                                            ValorRepasse = valorLiquidoAVParcela != 0 ? valorLiquidoAVParcela : valorLiquidoAV,
                                            DataRepasse = DataReembolsoEmissor,
                                            //DataRepasseCalculada = DataReembolsoEmissor
                                            StatusTransacao = tranStatus, // SCF1701 - ID 29 - Regra definida com base na Arquivo Item
                                            StatusRetornoCessao = statusRetornoCessao,
                                            DataMovimento = dataMovimento,
                                            NumeroLinhaArquivoTsys = Convert.ToString((int)arquivoTSYS.LerCampo("AV", linha, "NSEQ", true)),
                                            AtualizaTransacao = configGeral.ReprocessarNSUProcessado,
                                            // ECOMMERCE - Fernando Bove - envia o indicador financeiro/contabil caso exista - Inicio
                                            TipoFinanceiroContabil = tipoFinanceiroContabilAV,
                                            // ECOMMERCE - Fernando Bove - envia o indicador financeiro/contabil caso exista - Fim
                                            BINRefTransacao = RefBinAV.CodigoReferencia // REFERENCIA X BIN - ClockWork (Resource - Rogerio)
                                        });

                                    break;

                                case "RP":

                                    // Atualiza numero de transacoes
                                    numeroTransacao++;

                                    // Prepara campos que serao chamados mais de uma vez
                                    DateTime dataTransacaoRP = (DateTime)arquivoTSYS.LerCampo("RP", linha, "DataTransacao", true);
                                    dataTransacaoRP += (TimeSpan)arquivoTSYS.LerCampo("RP", linha, "HorarioTransacao", true);
                                    string nsuTefOriginalRP = (string)arquivoTSYS.LerCampo("RP", linha, "NSUTefTransacaoOriginal", true);
                                    string nsuHostOriginalRP = (string)arquivoTSYS.LerCampo("RP", linha, "NSUHostTransacaoOriginal", true);
                                    string nsuHostRP = (string)arquivoTSYS.LerCampo("RP", linha, "NSUHostTransacao", true);
                                    string codAutorizacaoRP = (string)arquivoTSYS.LerCampo("RP", linha, "CodigoAutorizacaoTransacaoOriginal", true);
                                    double valorLiquidoRP = (double)arquivoTSYS.LerCampo("RP", linha, "ValorLiquidoVenda", true) * (-1);
                                    DateTime DataReembolsoEmissorRP = (DateTime)arquivoTSYS.LerCampo("RP", linha, "DataReembolsoEmissor", true);
                                    int numeroParcelaRP = (int)arquivoTSYS.LerCampo("RP", linha, "NumeroParcela", true);
                                    double valorLiquidoRPParcela = (double)arquivoTSYS.LerCampo("RP", linha, "ValorLiquidoParcela", true) * (-1);
                                    string codigoAnulacaoRP = (string)arquivoTSYS.LerCampo("RP", linha, "CodigoAnulacao", true);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Inicio
                                    string tipoFinanceiroContabilRP = arquivoTSYS.VerficarFinanceiroContabil("RP", linha);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Fim

                                    // Traduz o estabelecimento
                                    int codigoEstabelecimentoRP = this.traduzirEstabelecimento(request.CodigoSessao, (string)arquivoTSYS.LerCampo("RP", linha, "IdentificacaoLoja", true), dicionarioEstabelecimento);
                                    //DateTime dataCorrigidaTransacaoRP = Convert.ToDateTime(dataTransacaoRP.ToString().Substring(0, 10) + " 00:00:00");

                                    // ajusta o codigo do produto de acordo com o codigo do ajuste
                                    if (dicionarioReferenciaProdutoAV.Count > 0)
                                        codigoProdutoAnulacaoVenda = int.Parse(dicionarioProduto[dicionarioReferenciaProdutoAV[codigoAnulacaoRP].ToString()].CodigoProduto);

                                    //string _chaveRP = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoRP, codigoEstabelecimentoRP.ToString(), nsuHostOriginalRP, codAutorizacaoRP, numeroParcelaRP.ToString(), (string)arquivoTSYS.LerCampo("RP", linha, "NSEQ", false)); SCF1161
                                    string _chaveRP = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoRP, codigoEstabelecimentoRP.ToString(), nsuHostRP, codAutorizacaoRP, numeroParcelaRP.ToString());

                                    //Numero da Referencia de acordo com o BIN (numero do cartão)
                                    string numeroCartaoRP = (string)arquivoTSYS.LerCampo("RP", linha, "NumeroCartao", true);
                                    BinReferenciaInfo RefBinRP = dicionarioBinRef[numeroCartaoRP.Substring(3, 6)];


                                    // Executa a procedure
                                    Procedures.PR_TRANSACAO_ANULACAO_VENDA_S(cmTransacaoAnulacaoVendaS,
                                        new PR_TRANSACAO_ANULACAO_VENDA_S_Request()
                                        {
                                            CodigoTransacao = null,
                                            CodigoArquivo = codigoArquivo,
                                            CodigoArquivoItem1 = codigoArquivoItem,
                                            Chave = _chaveRP,
                                            //Chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoRP, codigoEstabelecimentoRP.ToString(), nsuTefOriginalRP, codAutorizacaoRP, numeroParcelaRP.ToString(), (string)arquivoTSYS.LerCampo("RP", linha, "NSEQ", false)), SCF1161
                                            Chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoRP, codigoEstabelecimentoRP.ToString(), nsuTefOriginalRP, codAutorizacaoRP, numeroParcelaRP.ToString()),
                                            Chave3 = _chaveRP.Substring(0, 8) + "000000" + _chaveRP.Substring(14),
                                            CodigoEstabelecimento = codigoEstabelecimentoRP,
                                            CodigoProduto = codigoProdutoAnulacaoVenda,
                                            DataTransacao = dataTransacaoRP,
                                            DataTransacaoOriginal = (DateTime)arquivoTSYS.LerCampo("RP", linha, "DataTransacaoOriginal", true),
                                            ValorDesconto = (double)arquivoTSYS.LerCampo("RP", linha, "ValorDescontoOuComissao", true),
                                            ValorLiquido = valorLiquidoRP,
                                            NsuHost = nsuHostRP,
                                            NsuHostOriginal = nsuHostOriginalRP,
                                            TipoLancamento = (string)arquivoTSYS.LerCampo("RP", linha, "TipoLancamento", true),
                                            CodigoAnulacao = (string)arquivoTSYS.LerCampo("RP", linha, "CodigoAnulacao", true),
                                            CodigoAutorizacaoOriginal = (string)arquivoTSYS.LerCampo("RP", linha, "CodigoAutorizacaoTransacaoOriginal", true),
                                            DataReembolso = (DateTime)arquivoTSYS.LerCampo("RP", linha, "DataReembolsoEmissor", true),
                                            MotivoAnulacao = (string)arquivoTSYS.LerCampo("RP", linha, "DescricaoMotivoAnulacao", true),
                                            NsuTEFOriginal = nsuTefOriginalRP,
                                            NumeroCartao = (string)arquivoTSYS.LerCampo("RP", linha, "NumeroCartao", true),
                                            NumeroContaCliente = (string)arquivoTSYS.LerCampo("RP", linha, "NumeroContaCliente", true),
                                            Agencia = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("RP", linha, "Agencia", true) : null,
                                            Banco = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("RP", linha, "Banco", true) : null,
                                            Conta = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("RP", linha, "Conta", true) : null,
                                            NumeroParcela = numeroParcelaRP,
                                            NumeroParcelaTotal = (int)arquivoTSYS.LerCampo("RP", linha, "NumeroTotalParcelas", true),
                                            ValorAnulacao = (double)arquivoTSYS.LerCampo("RP", linha, "ValorBrutoVenda", true) * (-1),
                                            ValorParcela = (double)arquivoTSYS.LerCampo("RP", linha, "ValorBrutoParcela", true) * (-1),
                                            ValorParcelaDesconto = (double)arquivoTSYS.LerCampo("RP", linha, "ValorDescontoParcela", true),
                                            ValorParcelaLiquido = valorLiquidoRPParcela,
                                            RetornarRegistro = false,
                                            CodigoProcesso = request.CodigoProcesso,
                                            ValorRepasse = valorLiquidoRPParcela != 0 ? valorLiquidoRPParcela : valorLiquidoRP,
                                            DataRepasse = DataReembolsoEmissorRP,
                                            //DataRepasseCalculada = DataReembolsoEmissor
                                            StatusTransacao = tranStatus, // SCF1701 - ID 29 - Regra definida com base na Arquivo Item
                                            StatusRetornoCessao = statusRetornoCessao,
                                            DataMovimento = dataMovimento,
                                            NumeroLinhaArquivoTsys = Convert.ToString((int)arquivoTSYS.LerCampo("RP", linha, "NSEQ", true)),
                                            // ECOMMERCE - Fernando Bove - envia o indicador financeiro/contabil caso exista - Inicio
                                            TipoFinanceiroContabil = tipoFinanceiroContabilRP,
                                            // ECOMMERCE - Fernando Bove - envia o indicador financeiro/contabil caso exista - Fim
                                            BINRefTransacao = RefBinRP.CodigoReferencia, // REFERENCIA X BIN - ClockWork (Resource - Rogerio)
                                            AtualizaTransacao = configGeral.ReprocessarNSUProcessado
                                        });

                                    break;

                                case "CV":

                                    // pega a modalidade da venda para ajustar o produto
                                    string _modalidade = (string)arquivoTSYS.LerCampo("CV", linha, "ModalidadeVenda", true);

                                    // ajusta o produto de acordo com o produto cadastrado + a modalidade de venda
                                    switch (arquivoTSYS.LerCampo("CV", linha, "ProdutoCSF", true).ToString().TrimEnd())
                                    {
                                        case "RCAR":
                                            if (_modalidade == "1")
                                                codigoProdutoVenda = int.Parse(dicionarioProduto["COBAN"].CodigoProduto);
                                            else
                                                codigoProdutoVenda = int.Parse(dicionarioProduto["RCAR"].CodigoProduto);
                                            break;

                                        case "CRAF":
                                            if (_modalidade == "1")
                                                codigoProdutoVenda = int.Parse(dicionarioProduto["CRAF PULA CICLO"].CodigoProduto);
                                            else
                                                codigoProdutoVenda = int.Parse(dicionarioProduto["CRAF"].CodigoProduto);
                                            break;

                                        case "CRES":
                                            codigoProdutoVenda = int.Parse(dicionarioProduto["CRES"].CodigoProduto);
                                            break;

                                        default:
                                            codigoProdutoVenda = int.Parse(dicionarioProduto["COBAN"].CodigoProduto);
                                            break;
                                    }

                                    // Atualiza numero de transacoes
                                    numeroTransacao++;

                                    // Prepara campos que serao chamados mais de uma vez
                                    DateTime dataTransacaoCV = (DateTime)arquivoTSYS.LerCampo("CV", linha, "DataTransacao", true);
                                    dataTransacaoCV += (TimeSpan)arquivoTSYS.LerCampo("CV", linha, "HorarioTransacao", true);
                                    string nsuTefCV = (string)arquivoTSYS.LerCampo("CV", linha, "NSUTEFTransacao", true);
                                    string nsuHostCV = (string)arquivoTSYS.LerCampo("CV", linha, "NSUHostTransacao", true);
                                    string codAutorizacaoCV = (string)arquivoTSYS.LerCampo("CV", linha, "CodigoAutorizacao", true);
                                    double valorLiquidoCV = (double)arquivoTSYS.LerCampo("CV", linha, "ValorLiquidoVenda", true);
                                    int numeroParcelaCV = (int)arquivoTSYS.LerCampo("CV", linha, "NumeroParcela", true);
                                    double valorLiquidoParcelaCV = (double)arquivoTSYS.LerCampo("CV", linha, "ValorLiquidoParcela", true);
                                    string meioCapturaCV = (string)arquivoTSYS.LerCampo("CV", linha, "MeioCaptura", true);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Inicio
                                    string tipoFinanceiroContabilCV = arquivoTSYS.VerficarFinanceiroContabil("CV", linha);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Fim

                                    //jira SCF-549
                                    if (request.TipoProcesso == "ProcessoAtacadaoInfo")
                                        numeroParcelaCV = 0;

                                    // Traduz o estabelecimento
                                    int codigoEstabelecimentoCV = this.traduzirEstabelecimento(request.CodigoSessao, (string)arquivoTSYS.LerCampo("CV", linha, "IdentificacaoLoja", true), dicionarioEstabelecimento);
                                    //DateTime dataCorrigidaTransacaoCV = Convert.ToDateTime(dataTransacaoCV.ToString().Substring(0, 10));

                                    string _chaveCV = ChaveTransacaoHelper.GerarChaveComprovanteVenda(dataTransacaoCV, codigoEstabelecimentoCV.ToString(), nsuHostCV, codAutorizacaoCV, numeroParcelaCV.ToString());

                                    // ECOMMERCE - INICIO                                    
                                    DateTime dataPreAutorizacao = dataTransacaoCV;
                                    string dataPreAutorizacaoStr;
                                    string nsuHostOriginalCV = "";
                                    string nsuHostReversaoCV = "";

                                    if (arquivoInfo.LayoutArquivo.Equals("2") || arquivoInfo.LayoutArquivo.Equals("3"))
                                    {
                                        nsuHostOriginalCV = arquivoTSYS.LerCampo("CV", linha, "NSUHostTransacaoOriginal", true).ToString();
                                        nsuHostReversaoCV = arquivoTSYS.LerCampo("CV", linha, "NSUHostTransacaoReversao", true).ToString();
                                        dataPreAutorizacaoStr = arquivoTSYS.LerCampo("CV", linha, "DataPreAutorizacao", true).ToString();

                                        if (dataPreAutorizacaoStr.ToString().Trim() != "" && !dataPreAutorizacaoStr.Equals("00000000"))
                                        {
                                            // Pega os valores
                                            int dia = int.Parse(dataPreAutorizacaoStr.Substring(6, 2));
                                            int mes = int.Parse(dataPreAutorizacaoStr.Substring(4, 2));
                                            int ano = int.Parse(dataPreAutorizacaoStr.Substring(0, 4));

                                            // Faz o parse da data
                                            if (dia > 0 && dia < 32 && mes > 0 && mes < 13 && ano > 1900 && ano < 9999)
                                                dataPreAutorizacao = new DateTime(ano, mes, dia);
                                        }

                                    }

                                    //Numero da Referencia de acordo com o BIN (numero do cartão)
                                    string numeroCartaoCV = (string)arquivoTSYS.LerCampo("CV", linha, "NumeroCartao", true);
                                    BinReferenciaInfo RefBinCV = dicionarioBinRef[numeroCartaoCV.Substring(3, 6)];
                                    // ECOMMERCE - FIM


                                    // Executa a procedure
                                    Procedures.PR_TRANSACAO_VENDA_S(cmTransacaoVendaS,
                                        new PR_TRANSACAO_VENDA_S_Request()
                                        {
                                            CodigoTransacao = null,
                                            CodigoArquivo = codigoArquivo,
                                            CodigoArquivoItem1 = codigoArquivoItem,
                                            CodigoEstabelecimento = codigoEstabelecimentoCV,
                                            CodigoProduto = codigoProdutoVenda == null ? int.Parse(dicionarioProduto[arquivoTSYS.LerCampo("CV", linha, "ProdutoCSF", true).ToString().TrimEnd()].CodigoProduto) : codigoProdutoVenda,

                                            // ** Por enquanto não importar o plano pois o arquivo da TSYS precisa ser corrigido
                                            // ** Vamos ignorar o plano temporariamente para podermos homologar. Solicitação do Luciano em 10/08
                                            //CodigoPlano = (int)arquivoTSYS.LerCampo("CV", linha, "CodigoPlano", true),
                                            CodigoPlano = request.TraduzirPlano == true ? (int?)this.traduzirPlano(request.CodigoSessao, arquivoTSYS.LerCampo("CV", linha, "CodigoPlano", false).ToString().TrimEnd(), dicionarioPlano) : null,

                                            //Chave = ChaveTransacaoHelper.GerarChaveComprovanteVenda(dataTransacaoCV, codigoEstabelecimentoCV.ToString(), nsuHostCV, codAutorizacaoCV, numeroParcelaCV.ToString()),
                                            Chave = _chaveCV,
                                            Chave2 = ChaveTransacaoHelper.GerarChaveComprovanteVenda(dataTransacaoCV, codigoEstabelecimentoCV.ToString(), nsuTefCV, codAutorizacaoCV, numeroParcelaCV.ToString()),
                                            //Chave3 = ChaveTransacaoHelper.GerarChaveComprovanteVenda(dataCorrigidaTransacaoCV, codigoEstabelecimentoCV.ToString(), nsuHostCV, codAutorizacaoCV, numeroParcelaCV.ToString()),
                                            Chave3 = _chaveCV.Substring(0, 8) + "000000" + _chaveCV.Substring(14),
                                            CodigoAutorizacao = (string)arquivoTSYS.LerCampo("CV", linha, "CodigoAutorizacao", true),
                                            ValorDesconto = (double)arquivoTSYS.LerCampo("CV", linha, "ValorDesconto", true) * (-1),
                                            ValorLiquido = valorLiquidoCV,
                                            ValorParcela = (double)arquivoTSYS.LerCampo("CV", linha, "ValorBrutoParcela", true),
                                            ValorParcelaDesconto = (double)arquivoTSYS.LerCampo("CV", linha, "ValorDescontoParcela", true) * (-1),
                                            ValorParcelaLiquido = valorLiquidoParcelaCV,
                                            ValorVenda = (double)arquivoTSYS.LerCampo("CV", linha, "ValorBrutoVenda", true),
                                            DataRepasse = (DateTime)arquivoTSYS.LerCampo("CV", linha, "DataRepasseCredito", true),
                                            //DataRepasseCalculada = request.TipoProcesso == "Atacadao" ? null : (DateTime?)arquivoTSYS.LerCampo("CV", linha, "DataRepasseCredito", true),
                                            DataTransacao = dataTransacaoCV,
                                            NumeroCartao = (string)arquivoTSYS.LerCampo("CV", linha, "NumeroCartao", true),
                                            NsuHost = nsuHostCV,
                                            NsuTEF = nsuTefCV,
                                            NumeroContaCliente = (string)arquivoTSYS.LerCampo("CV", linha, "NumeroContaCliente", true),
                                            NumeroParcela = numeroParcelaCV,
                                            NumeroParcelaTotal = (int)arquivoTSYS.LerCampo("CV", linha, "NumeroTotalParcelas", true),
                                            Banco = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("CV", linha, "Banco", true) : null,
                                            Agencia = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("CV", linha, "Agencia", true) : null,
                                            Conta = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("CV", linha, "Conta", true) : null,
                                            CupomFiscal = (string)arquivoTSYS.LerCampo("CV", linha, "CupomFiscal", true),
                                            MeioCaptura = meioCapturaCV,
                                            Modalidade = (string)arquivoTSYS.LerCampo("CV", linha, "ModalidadeVenda", true),
                                            TipoLancamento = (string)arquivoTSYS.LerCampo("CV", linha, "TipoLancamento", true),
                                            TipoProduto = (string)arquivoTSYS.LerCampo("CV", linha, "TipoProduto", true),
                                            RetornarRegistro = false,
                                            CodigoProcesso = request.CodigoProcesso,
                                            ValorRepasse = valorLiquidoParcelaCV > 0 ? valorLiquidoParcelaCV : valorLiquidoCV,
                                            StatusTransacao = tranStatus, // SCF1701 - ID 29 - Regra definida com base na Arquivo Item
                                            // status do retorno de cessao - JIRA SCF-528
                                            StatusRetornoCessao = statusRetornoCessao,
                                            DataMovimento = dataMovimento,
                                            NumeroLinhaArquivoTsys = Convert.ToString((int)arquivoTSYS.LerCampo("CV", linha, "NSEQ", true)),
                                            //AtualizaTransacao = configGeral.ReprocessarNSUProcessado // SCF1072 - Marcos Matsuoka
                                            AtualizaTransacao = p.TipoProcesso == "ProcessoAtacadaoInfo" ? true : configGeral.ReprocessarNSUProcessado, // SCF1072 - Marcos Matsuoka
                                            // ECOMMERCE - Fernando Bove - envia a data de pre autorizacao, NSU_Host_Original, NSU_Host_Reversao e o indicador financeiro/contabil caso exista - Início
                                            DataPreAutorizacao = dataPreAutorizacao,
                                            NsuHostOriginal = string.IsNullOrEmpty(nsuHostOriginalCV) ? "000000000000" : nsuHostOriginalCV,
                                            NsuHostReversao = string.IsNullOrEmpty(nsuHostReversaoCV) ? "000000000000" : nsuHostReversaoCV,
                                            TipoFinanceiroContabil = tipoFinanceiroContabilCV,
                                            BINRefTransacao = RefBinCV.CodigoReferencia, // REFERENCIA X BIN - ClockWork (Resource - Rogerio)
                                            // ECOMMERCE - Fernando Bove - envia a data de pre autorizacao, NSU_Host_Original, NSU_Host_Reversao e o indicador financeiro/contabil caso exista - Fim

                                        });

                                    break;

                                case "CP":

                                    // Atualiza numero de transacoes
                                    numeroTransacao++;

                                    // Prepara campos que serao chamados mais de uma vez
                                    DateTime dataTransacaoCP = (DateTime)arquivoTSYS.LerCampo("CP", linha, "DataTransacao", true);
                                    dataTransacaoCP += (TimeSpan)arquivoTSYS.LerCampo("CP", linha, "HorarioTransacao", true);
                                    string nsuTefCP = (string)arquivoTSYS.LerCampo("CP", linha, "NSUTEFTransacao", true);
                                    string nsuHostCP = (string)arquivoTSYS.LerCampo("CP", linha, "NSUHostTransacao", true);
                                    string codAutorizacaoCP = (string)arquivoTSYS.LerCampo("CP", linha, "CodigoAutorizacao", true);
                                    double valorLiquidoCP = (double)arquivoTSYS.LerCampo("CP", linha, "ValorLiquidoPagamento", true);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Inicio
                                    string tipoFinanceiroContabilCP = arquivoTSYS.VerficarFinanceiroContabil("CP", linha);
                                    // ECOMMERCE - Fernando Bove - 20151127 - variável com o indicador financeiro/contabil - Fim

                                    // Traduz o estabelecimento
                                    int codigoEstabelecimentoCP = this.traduzirEstabelecimento(request.CodigoSessao, (string)arquivoTSYS.LerCampo("CP", linha, "IdentificacaoLoja", true), dicionarioEstabelecimento);
                                    //DateTime dataCorrigidaTransacaoCP = Convert.ToDateTime(dataTransacaoCP.ToString().Substring(0, 10) + " 00:00:00");

                                    //1458 string _chaveCP = ChaveTransacaoHelper.GerarChaveComprovantePagamento(dataTransacaoCP, codigoEstabelecimentoCP.ToString(), nsuHostCP, codAutorizacaoCP, (string)arquivoTSYS.LerCampo("CP", linha, "NSEQ", false));
                                    string sequencialMeioPagamentoCP = (string)arquivoTSYS.LerCampo("CP", linha, "SequencialMeioPagamento", false);
                                    sequencialMeioPagamentoCP = sequencialMeioPagamentoCP.PadLeft(3, '0');
                                    string _chaveCP = ChaveTransacaoHelper.GerarChaveComprovantePagamento(dataTransacaoCP, codigoEstabelecimentoCP.ToString(), nsuHostCP, codAutorizacaoCP, sequencialMeioPagamentoCP);

                                    //Numero da Referencia de acordo com o BIN (numero do cartão)
                                    string numeroCartaoCP = (string)arquivoTSYS.LerCampo("CP", linha, "NumeroCartao", true);
                                    BinReferenciaInfo RefBinCP = dicionarioBinRef[numeroCartaoCP.Substring(3, 6)];

                                    // Executa a procedure
                                    Procedures.PR_TRANSACAO_PAGAMENTO_S(cmTransacaoPagamentoS,
                                        new PR_TRANSACAO_PAGAMENTO_S_Request()
                                        {
                                            CodigoTransacao = null,
                                            CodigoArquivo = codigoArquivo,
                                            CodigoArquivoItem1 = codigoArquivoItem,
                                            Chave = _chaveCP,
                                            //1458 Chave2 = ChaveTransacaoHelper.GerarChaveComprovantePagamento(dataTransacaoCP, codigoEstabelecimentoCP.ToString(), nsuTefCP, codAutorizacaoCP, (string)arquivoTSYS.LerCampo("CP", linha, "NSEQ", false)),
                                            Chave2 = ChaveTransacaoHelper.GerarChaveComprovantePagamento(dataTransacaoCP, codigoEstabelecimentoCP.ToString(), nsuTefCP, codAutorizacaoCP, sequencialMeioPagamentoCP),
                                            Chave3 = _chaveCP.Substring(0, 8) + "000000" + _chaveCP.Substring(14),
                                            CodigoEstabelecimento = this.traduzirEstabelecimento(request.CodigoSessao, (string)arquivoTSYS.LerCampo("CP", linha, "IdentificacaoLoja", true), dicionarioEstabelecimento),
                                            CodigoAutorizacao = (string)arquivoTSYS.LerCampo("CP", linha, "CodigoAutorizacao", true),
                                            ValorDesconto = (double)arquivoTSYS.LerCampo("CP", linha, "ValorDesconto", true),
                                            ValorLiquido = valorLiquidoCP * (-1),
                                            DataRepasse = (DateTime)arquivoTSYS.LerCampo("CP", linha, "DataRepasseCredito", true),
                                            //DataRepasseCalculada = request.TipoProcesso == "Atacadao" ? null : (DateTime?)arquivoTSYS.LerCampo("CP", linha, "DataRepasseCredito", true),
                                            DataTransacao = dataTransacaoCP,
                                            NumeroCartao = (string)arquivoTSYS.LerCampo("CP", linha, "NumeroCartao", true),
                                            NsuHost = nsuHostCP,
                                            NsuTEF = (string)arquivoTSYS.LerCampo("CP", linha, "NSUTEFTransacao", true),
                                            NumeroContaCliente = (string)arquivoTSYS.LerCampo("CP", linha, "NumeroContaCliente", true),
                                            Banco = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("CP", linha, "Banco", true) : null,
                                            Agencia = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("CP", linha, "Agencia", true) : null,
                                            Conta = request.AlterarBancoAgenciaConta ? (string)arquivoTSYS.LerCampo("CP", linha, "Conta", true) : null,
                                            MeioCaptura = (string)arquivoTSYS.LerCampo("CP", linha, "MeioCaptura", true),
                                            TipoLancamento = (string)arquivoTSYS.LerCampo("CP", linha, "TipoLancamento", true),
                                            MeioPagamento = (string)arquivoTSYS.LerCampo("CP", linha, "MeioPagamento", true),
                                            MeioPagamentoSeq = (int)arquivoTSYS.LerCampo("CP", linha, "SequencialMeioPagamento", true),
                                            MeioPagamentoValor = (double)arquivoTSYS.LerCampo("CP", linha, "ValorMeioPagamento", true) * (-1),
                                            QuantidadeMeioPagamento = (int)arquivoTSYS.LerCampo("CP", linha, "NumeroTotalMeioPagamento", true),
                                            ValorPagamento = (double)arquivoTSYS.LerCampo("CP", linha, "ValorTotalPagamento", true) * (-1),
                                            RetornarRegistro = false,
                                            CodigoProcesso = request.CodigoProcesso,
                                            CodigoProduto = codigoProdutoPagamento,
                                            ValorRepasse = valorLiquidoCP * (-1),
                                            StatusTransacao = tranStatus, // SCF1701 - ID 29 - Regra definida com base na Arquivo Item
                                            StatusRetornoCessao = statusRetornoCessao,
                                            DataMovimento = dataMovimento,
                                            NumeroLinhaArquivoTsys = Convert.ToString((int)arquivoTSYS.LerCampo("CP", linha, "NSEQ", true)),
                                            AtualizaTransacao = configGeral.ReprocessarNSUProcessado,
                                            // ECOMMERCE - Fernando Bove - envia o indicador financeiro/contabil caso exista - Inicio
                                            TipoFinanceiroContabil = tipoFinanceiroContabilCP,
                                            BINRefTransacao = RefBinCP.CodigoReferencia, // REFERENCIA X BIN - ClockWork (Resource - Rogerio)
                                            // ECOMMERCE - Fernando Bove - envia o indicador financeiro/contabil caso exista - Fim
                                        });

                                    break;
                            }

                            // Verifica se deve fazer log
                            quantidadeLog++;
                            numeroLinha++;
                            if (request.CodigoProcesso != null && quantidadeLog >= 15000)
                            {
                                // Zero contador
                                quantidadeLog = 0;
                                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("TRANSACAO - {0} linhas / {1} transações processadas", numeroLinha, numeroTransacao)); //1328
                                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                            }

                        }
                        //1328
                        lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                        lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("TRANSACAO - FIM {0} transações processadas", numeroTransacao));
                        LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    }
                }
                catch (Exception ex)
                {
                    // Processa excessao
                    resposta.ProcessarExcessao(ex);
                }
                finally
                {
                    // Finaliza conexao oracle
                    if (cn != null)
                        cn.Dispose();

                    // Finaliza o cursor
                    if (cursor != null)
                        cursor.Dispose();
                }

                // Retorna
                return resposta;
            }
        }
        //=============================================================================================H02


        //=============================================================================================I02
        private int traduzirEstabelecimento(string codigoSessao, string cnpj, Dictionary<string, int> dicionario)
        {
            // ** Apenas para teste
            //// Garante que o estabelecimento esteja criado
            //if (!dicionario.ContainsKey(cnpj))
            //    dicionario.Add(
            //        cnpj,
            //        int.Parse(
            //            PersistenciaHelper.Salvar<EstabelecimentoInfo>(
            //                codigoSessao,
            //                new EstabelecimentoInfo()
            //                {
            //                    CNPJ = cnpj,
            //                    RazaoSocial = cnpj
            //                }).CodigoEstabelecimento));

            // Retorna item do dicionário
            int resultado = 0;
            try
            {
                resultado = dicionario[cnpj];
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi encontrado estabelecimento com o CNPJ (" + cnpj + ") informado.", ex);
            }
            return resultado;
        }
        //=============================================================================================I02

        //=============================================================================================J02
        private int traduzirPlano(string codigoSessao, string codigoPlanoTSYS, Dictionary<string, int> dicionario)
        {

            // Retorna item do dicionário
            int resultado = 0;
            try
            {
                resultado = dicionario["0" + codigoPlanoTSYS];
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi encontrado plano com o CODIGO TSYS (" + codigoPlanoTSYS + ") informado.", ex);
            }
            return resultado;
        }
        //=============================================================================================J02


        //=============================================================================================K02
        private int traduzirProduto(string codigoSessao, string nome, Dictionary<string, int> dicionario)
        {

            // Retorna item do dicionário
            int resultado = 0;
            try
            {
                resultado = dicionario[nome];
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi encontrado produto com o NOME (" + nome + ") informado.", ex);
            }
            return resultado;
        }


        //=============================================================================================L02
        private int traduzirEstabelecimentoCSU(string codigoSessao, string codigoEstabelecimentoCSU, Dictionary<string, int> dicionario)
        {
            int resultado = 0;
            try
            {
                resultado = dicionario[codigoEstabelecimentoCSU];
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi encontrado estabelecimento com o Código CSU (" + codigoEstabelecimentoCSU + ") informado.", ex);
            }
            return resultado;
        }
        //=============================================================================================L02


        //=============================================================================================M02
        private int traduzirEstabelecimentoSitef(string codigoSessao, string codigoEstabelecimentoSitef, Dictionary<string, int> dicionario)
        {
            // Retorna item do dicionário
            int resultado = 0;
            try
            {
                resultado = dicionario[codigoEstabelecimentoSitef];
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi encontrado estabelecimento com o Código Sitef (" + codigoEstabelecimentoSitef + ") informado.", ex);
            }
            return resultado;
        }
        //=============================================================================================M02

        //=============================================================================================N02
        public ImportarTransacoesCSUResponse ImportarTransacoesCSU(ImportarTransacoesCSURequest request)
        {
            // linha
            int codigoArquivoItem = 0; //1447
            int vLinha = 0; //1447
            var lLog = new LogSCFProcessoEventoInfo();
            lLog.setCodigoProcesso(request.CodigoProcesso);
            lLog.setDescricao("Inicio Importacao CSU"); //1447
            LogHelper.Salvar(null, lLog);

            // Prepara a resposta
            ImportarTransacoesCSUResponse resposta = new ImportarTransacoesCSUResponse();
            resposta.PreparaResposta(request);

            // Prepara lista de lojas a incluir
            List<string> incluirLojas = null;
            if (request.IncluirLojas != null)
                incluirLojas = new List<string>(request.IncluirLojas.Split(';'));

            // Declare o reader para que possa ser corretamente finalizado
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;

            // Bloco de controle
            try
            {
                // Inicializa as procedures
                OracleCommand cmTransacaoVendaS = Procedures.PR_TRANSACAO_VENDA_S_preparar(cn);
                OracleCommand cmTransacaoPagamentoS = Procedures.PR_TRANSACAO_PAGAMENTO_S_preparar(cn);
                OracleCommand cmTransacaoAnulacaoVendaS = Procedures.PR_TRANSACAO_ANULACAO_VENDA_S_preparar(cn);
                OracleCommand cmTransacaoAnulacaoPagamentoS = Procedures.PR_TRANSACAO_ANULACAO_PAGTO_S_preparar(cn);
                OracleCommand cmTransacaoAjusteS = Procedures.PR_TRANSACAO_AJUSTE_S_preparar(cn);

                // Pega configuracoes do processo
                ProcessoAtacadaoConfig config = GerenciadorConfig.ReceberConfig<ProcessoAtacadaoConfig>();
                if (config == null)
                    config = new ProcessoAtacadaoConfig();

                // Cria o interpretador para o arquivo da TSYS
                ArquivoCSU arquivoCSU = new ArquivoCSU();

                // Faz o parse do codigo do arquivo para repassar para as procedures
                int codigoArquivo = int.Parse(request.CodigoArquivo);

                // Pede lista de estabelecimentos e indexa o CodigoEstabelecimento por CNPJ
                List<EstabelecimentoInfo> estabelecimentos =
                    PersistenciaHelper.Listar<EstabelecimentoInfo>(request.CodigoSessao);
                Dictionary<string, int> dicionarioEstabelecimento = new Dictionary<string, int>();

                lLog.setDescricao("Conferindo codigo CSU no estabelecimentos"); //1447
                LogHelper.Salvar(null, lLog);
                foreach (EstabelecimentoInfo estabelecimento in estabelecimentos)
                {
                    // Lista dos estabelecimentos CSU separados por ,
                    string[] estabelecimentosCSU = estabelecimento.EstabelecimentoCSU.Split(',');

                    // Adiciona no dicionario
                    foreach (string estabelecimentoCSU in estabelecimentosCSU)
                        if (estabelecimentoCSU != null && estabelecimentoCSU != "" && !dicionarioEstabelecimento.ContainsKey(estabelecimentoCSU))
                            dicionarioEstabelecimento.Add(
                                estabelecimentoCSU,
                                int.Parse(estabelecimento.CodigoEstabelecimento));
                }

                // Produtos - Lista de Produtos
                // Pede lista de produtos e indexa o CodigoEstabelecimento por descricao
                lLog.setDescricao("Carregando produtos, para procurar especificamente o codigo do RCAR"); //1447
                LogHelper.Salvar(null, lLog);
                List<ProdutoInfo> produtos =
                    PersistenciaHelper.Listar<ProdutoInfo>(request.CodigoSessao);

                Dictionary<string, int> dicionarioProduto = new Dictionary<string, int>();
                foreach (ProdutoInfo produto in produtos)
                    dicionarioProduto.Add(produto.NomeProduto, int.Parse(produto.CodigoProduto));

                // Verifica se o produto RCAR existe, senão, informa o erro
                // Este produto é pré-requisito pois é utilizado como fixo no processo Atacadao
                if (!dicionarioProduto.ContainsKey("RCAR"))
                    throw new Exception("Não foi encontrado o cadastro do produto RCAR. É necessário o cadastramento deste produto para prosseguir com o processo de importação de transações da TSYS");

                // Pega o codigo do Produtos RCAR
                // Para o processo do Atacadao, este codigo é fixo RCAR
                int codigoProdutoVenda = dicionarioProduto["RCAR"];

                // Data do Movimento a ser inserido nas transações REGISTRO tipo 0
                DateTime dataMovimento = new DateTime();

                // 1447 Data de Repasse é o dia de hoje
                DateTime dataRepasse = new DateTime();

                // Consulta as transacoes do arquivo
                lLog.setDescricao("Carregando as linhas do arquivo [" + request.CodigoArquivo + "] CSU"); //1447
                LogHelper.Salvar(null, lLog);
                cursor = Procedures.PR_ARQUIVO_ITEM_L(cn, request.CodigoArquivo, null, 0, false);
                OracleDataReader reader = cursor.GetDataReader();

                // Varre as linhas
                while (reader.Read())
                {
                    vLinha += 1; //1447 
                    int vResto = vLinha%15000;
                    if (  vResto == 0)
                    {
                        lLog.setDescricao("[" + vLinha.ToString() + "] linhas importadas"); //1447
                        LogHelper.Salvar(null, lLog);
                    }
                    
                    // Pega informacoes da linha
                    string linha =
                        reader.GetString(reader.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));

                    // Pega o codigo arquivo item
                    codigoArquivoItem =
                        Convert.ToInt32(
                            reader.GetDouble(reader.GetOrdinal("CODIGO_ARQUIVO_ITEM")));

                    // Verifica o tipo da linha
                    string tipoTransacao = arquivoCSU.InferirTipoLinha(linha);

                    // Se deve filtrar por loja, para os registros diferentes de header e trailer
                    // verifica se a loja deve ser incluida
                    if (tipoTransacao != "0" && tipoTransacao != "9" && incluirLojas != null)
                        if (!incluirLojas.Contains((string)arquivoCSU.LerCampo(tipoTransacao, linha, "LOJA", false)))
                            continue;

                    // Processa de acordo com o tipo da linha
                    switch (tipoTransacao)
                    {
                        //Header
                        case "0":
                            dataMovimento = (DateTime)arquivoCSU.LerCampo("0", linha, "DATA-MOVIMENTO", true);
                            dataRepasse = dataMovimento; // 1447
                            lLog.setDescricao("Data do movimento [" + dataMovimento + "]"); //1447
                            LogHelper.Salvar(null, lLog);
                            break;

                        // Comprovante de venda
                        case "1":

                            // Prepara campos que serao usados mais de uma vez
                            DateTime dataTransacaoCV = (DateTime)arquivoCSU.LerCampo("1", linha, "DT-VENDA", true);
                            dataTransacaoCV += (TimeSpan)arquivoCSU.LerCampo("1", linha, "HORA", true);
                            string nsuTefCV = (string)arquivoCSU.LerCampo("1", linha, "NSU", true);
                            string codAutorizacao = (string)arquivoCSU.LerCampo("1", linha, "NUM-AUT", true);
                            //jira SCF-549
                            int numeroParcela1 = 0;//(int)arquivoCSU.LerCampo("1", linha, "NUM-PARC", true);

                            // Traduz o codigo do estabelecimento
                            int codigoEstabelecimento1 = this.traduzirEstabelecimentoCSU(request.CodigoSessao, (string)arquivoCSU.LerCampo("1", linha, "LOJA", false), dicionarioEstabelecimento);

                            // Executa a procedure
                            Procedures.PR_TRANSACAO_VENDA_S(cmTransacaoVendaS,
                                new PR_TRANSACAO_VENDA_S_Request()
                                {
                                    CodigoTransacao = null,
                                    CodigoArquivo = codigoArquivo,
                                    CodigoArquivoItem1 = codigoArquivoItem,
                                    Chave2 = ChaveTransacaoHelper.GerarChaveComprovanteVenda(dataTransacaoCV, codigoEstabelecimento1.ToString(), nsuTefCV, codAutorizacao, numeroParcela1.ToString()),
                                    CodigoEstabelecimento = codigoEstabelecimento1,
                                    CodigoAutorizacao = (string)arquivoCSU.LerCampo("1", linha, "NUM-AUT", true),
                                    ValorDesconto = (double)arquivoCSU.LerCampo("1", linha, "MON-COM", true), // Valor da Comissão - SCF-547
                                    ValorLiquido = (double)arquivoCSU.LerCampo("1", linha, "MON-LIQ", true),
                                    ValorParcela = (double)arquivoCSU.LerCampo("1", linha, "MON-BRU-PRI", true),
                                    ValorParcelaLiquido = (double)arquivoCSU.LerCampo("1", linha, "MON-LIQ-PRI", true),
                                    ValorVenda = (double)arquivoCSU.LerCampo("1", linha, "MON-BRU", true),
                                    DataTransacao = dataTransacaoCV,
                                    NumeroCartao = (string)arquivoCSU.LerCampo("1", linha, "CARTAO", true),
                                    NsuTEF = nsuTefCV,

                                    NumeroParcela = numeroParcela1,
                                    NumeroParcelaTotal = (int)arquivoCSU.LerCampo("1", linha, "NUM-TOT-PARC", true),
                                    Banco = config.Banco,
                                    Agencia = config.Agencia,
                                    Conta = config.Conta,
                                    CupomFiscal = (string)arquivoCSU.LerCampo("1", linha, "CUPON", true),
                                    ValorComissao = (double)arquivoCSU.LerCampo("1", linha, "MON-COM", true),
                                    RetornarRegistro = false,
                                    StatusTransacao = TransacaoStatusEnum.Importado,
                                    CodigoProcesso = request.CodigoProcesso,
                                    DataMovimento = dataMovimento,
                                    AtualizaTransacao = true, // SCF1072 - Marcos Matsuoka
                                    CodigoProduto = codigoProdutoVenda,
                                    DataRepasse = dataRepasse // 1447
                                });

                            break;

                        // Anulação de venda
                        case "2":

                            // Prepara campos que serao usados mais de uma vez
                            DateTime dataTransacaoAV = (DateTime)arquivoCSU.LerCampo("2", linha, "DT-VENDA", true);
                            dataTransacaoAV += (TimeSpan)arquivoCSU.LerCampo("2", linha, "HORA", true);
                            string nsuTefAV = (string)arquivoCSU.LerCampo("2", linha, "NSU", true);
                            string codAutorizacaoAV = (string)arquivoCSU.LerCampo("2", linha, "NUM-AUT", true);
                            int numeroParcela2 = (int)arquivoCSU.LerCampo("2", linha, "NUM-PARC", true);

                            // Traduz o codigo do estabelecimento
                            int codigoEstabelecimento2 = this.traduzirEstabelecimentoCSU(request.CodigoSessao, (string)arquivoCSU.LerCampo("2", linha, "LOJA", false), dicionarioEstabelecimento);

                            // Executa a procedure
                            Procedures.PR_TRANSACAO_ANULACAO_VENDA_S(cmTransacaoAnulacaoVendaS,
                                new PR_TRANSACAO_ANULACAO_VENDA_S_Request()
                                {
                                    CodigoTransacao = null,
                                    CodigoArquivo = codigoArquivo,
                                    CodigoArquivoItem1 = codigoArquivoItem,
                                    Chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(dataTransacaoAV, codigoEstabelecimento2.ToString(), nsuTefAV, codAutorizacaoAV, numeroParcela2.ToString()),
                                    CodigoEstabelecimento = codigoEstabelecimento2,

                                    DataTransacao = dataTransacaoAV,
                                    DataTransacaoOriginal = dataTransacaoAV,
                                    NumeroCartao = (string)arquivoCSU.LerCampo("2", linha, "CARTAO", true),
                                    Banco = config.Banco,
                                    Agencia = config.Agencia,
                                    Conta = config.Conta,
                                    NumeroParcela = numeroParcela2,
                                    NumeroParcelaTotal = (int)arquivoCSU.LerCampo("2", linha, "NUM-TOT-PARC", true),
                                    ValorLiquido = (double)arquivoCSU.LerCampo("2", linha, "MON-LIQ", true),
                                    ValorAnulacao = (double)arquivoCSU.LerCampo("2", linha, "MON-BRU", true),
                                    ValorParcela = (double)arquivoCSU.LerCampo("2", linha, "MON-BRU-PRI", true),
                                    ValorParcelaLiquido = (double)arquivoCSU.LerCampo("2", linha, "MON-LIQ-PRI", true),
                                    RetornarRegistro = false,
                                    CodigoProcesso = request.CodigoProcesso,
                                    DataMovimento = dataMovimento,
                                    StatusTransacao = TransacaoStatusEnum.Importado,
                                    CodigoProduto = codigoProdutoVenda,
                                    DataRepasse = dataRepasse // 1447
                                });

                            break;

                        // Comprovante de pagamento
                        case "3":

                            // Prepara campos que serao usados mais de uma vez
                            DateTime dataTransacaoCP = (DateTime)arquivoCSU.LerCampo("3", linha, "DT-PAGTO", true);
                            dataTransacaoCP += (TimeSpan)arquivoCSU.LerCampo("3", linha, "HORA", true);
                            string nsuTefCP = (string)arquivoCSU.LerCampo("3", linha, "NSU", true);
                            string codAutorizacaoCP = (string)arquivoCSU.LerCampo("3", linha, "NUM-AUT", true);

                            // Traduz o codigo do estabelecimento
                            int codigoEstabelecimento3 = this.traduzirEstabelecimentoCSU(request.CodigoSessao, (string)arquivoCSU.LerCampo("3", linha, "LOJA", false), dicionarioEstabelecimento);
                            string codetb = codigoEstabelecimento3.ToString(); // 1447

                            // Executa a procedure
                            Procedures.PR_TRANSACAO_PAGAMENTO_S(cmTransacaoPagamentoS,
                                new PR_TRANSACAO_PAGAMENTO_S_Request()
                                {
                                    CodigoTransacao = null,
                                    CodigoArquivo = codigoArquivo,
                                    CodigoArquivoItem1 = codigoArquivoItem,
                                    Chave2 = ChaveTransacaoHelper.GerarChaveComprovantePagamento(dataTransacaoCP, codigoEstabelecimento3.ToString(), nsuTefCP, codAutorizacaoCP, (string)arquivoCSU.LerCampo("3", linha, "NSR", true)),
                                    CodigoEstabelecimento = codigoEstabelecimento3,
                                    CodigoAutorizacao = (string)arquivoCSU.LerCampo("3", linha, "NUM-AUT", true),
                                    DataTransacao = dataTransacaoCP,
                                    NumeroCartao = (string)arquivoCSU.LerCampo("3", linha, "CARTAO", true),
                                    NsuTEF = nsuTefCP,
                                    Banco = config.Banco,
                                    Agencia = config.Agencia,
                                    Conta = config.Conta,
                                    MeioPagamento = (string)arquivoCSU.LerCampo("3", linha, "MEIO-PAGTO", true),
                                    MeioPagamentoValor = (double)arquivoCSU.LerCampo("3", linha, "MON-PAGTO", true),
                                    ValorPagamento = (double)arquivoCSU.LerCampo("3", linha, "MON-PAGTO", true),
                                    RetornarRegistro = false,
                                    CodigoProcesso = request.CodigoProcesso,
                                    DataMovimento = dataMovimento,
                                    StatusTransacao = TransacaoStatusEnum.Importado,
                                    CodigoProduto = codigoProdutoVenda,
                                    DataRepasse = dataRepasse // 1447
                                });

                            break;

                        // Anulacao de pagamento
                        case "4":

                            // Prepara campos que serao usados mais de uma vez
                            DateTime dataTransacaoAP = (DateTime)arquivoCSU.LerCampo("4", linha, "DT-PAGTO", true);
                            dataTransacaoAP += (TimeSpan)arquivoCSU.LerCampo("4", linha, "HORA", true);
                            string nsuTefAP = "";

                            // Traduz o codigo do estabelecimento
                            int codigoEstabelecimento4 = this.traduzirEstabelecimentoCSU(request.CodigoSessao, (string)arquivoCSU.LerCampo("4", linha, "LOJA", false), dicionarioEstabelecimento);

                            // Executa a procedure
                            Procedures.PR_TRANSACAO_ANULACAO_PAGTO_S(cmTransacaoAnulacaoPagamentoS,
                                new PR_TRANSACAO_ANULACAO_PAGTO_S_Request()
                                {
                                    CodigoTransacao = null,
                                    CodigoArquivo = codigoArquivo,
                                    CodigoArquivoItem1 = codigoArquivoItem,
                                    Chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoPagamento(dataTransacaoAP, codigoEstabelecimento4.ToString(), nsuTefAP, (string)arquivoCSU.LerCampo("4", linha, "NSR", true)),
                                    CodigoEstabelecimento = codigoEstabelecimento4,

                                    DataTransacao = dataTransacaoAP,
                                    DataTransacaoOriginal = dataTransacaoAP,
                                    NumeroCartao = (string)arquivoCSU.LerCampo("4", linha, "CARTAO", true),
                                    ValorPagamento = (double)arquivoCSU.LerCampo("4", linha, "MON-PGTO", true),
                                    CodigoProcesso = request.CodigoProcesso,
                                    DataMovimento = dataMovimento,
                                    RetornarRegistro = false,
                                    StatusTransacao = TransacaoStatusEnum.Importado,
                                    CodigoProduto = codigoProdutoVenda,
                                    DataRepasse = dataRepasse // 1447
                                });

                            break;
                    }
                }
                lLog.setDescricao("Fim Importacao CSU [" + vLinha.ToString() + "] linhas importadas"); //1447
                LogHelper.Salvar(null, lLog);
            }
            catch (Exception ex)
            {
                // Processa a excessao
                lLog.setCodigoProcesso(request.CodigoProcesso);
                lLog.setDescricao("Codigo Arquivo Item [" + codigoArquivoItem +"] excecao [" + ex + "]"); //1447
                LogHelper.Salvar(null, lLog);
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                // Finaliza conexao
                if (cn != null)
                    cn.Dispose();

                // Finaliza o cursor
                if (cursor != null)
                    cursor.Dispose();
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================N02

        //=============================================================================================O02
        //==================================================================================================================
        // CESSAO -- 1328 MUDANÇA TOTAL
        //==================================================================================================================
        public AtualizarTransacoesRetornoCessaoResponse AtualizarTransacoesRetornoCessao(AtualizarTransacoesRetornoCessaoRequest request)
        {
            //==================================================================================================================
            // Prepara a resposta
            //==================================================================================================================
            AtualizarTransacoesRetornoCessaoResponse resposta = new AtualizarTransacoesRetornoCessaoResponse();
            resposta.PreparaResposta(request);
            //==================================================================================================================
            // Declare o reader para que possa ser corretamente finalizado
            //==================================================================================================================
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;
            //==================================================================================================================
            // recupera os dados a serem passados para a procedure
            //==================================================================================================================
            int CodigoArqRetCessao = Convert.ToInt32(request.CodigoArquivo);
            int CodigoProcRetCessao = Convert.ToInt32(request.CodigoProcesso);
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            //==================================================================================================================
            // Bloco de controle
            //==================================================================================================================
            try
            {
                //==================================================================================================================
                // Log de inicio
                //==================================================================================================================
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CESSAO - Carregando linhas importadas e validas"));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                //==================================================================================================================
                // recebe a configuração geral para verificar se precisa ou nao cessionar transacoes ja cessionadas
                //==================================================================================================================
                ConfiguracaoGeralInfo configuracaoGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(null, null);
                //==================================================================================================================
                // Pega configuracoes do processo
                //==================================================================================================================
                ProcessoAtacadaoConfig config = GerenciadorConfig.ReceberConfig<ProcessoAtacadaoConfig>();
                if (config == null)
                    config = new ProcessoAtacadaoConfig();
                //==================================================================================================================
                // Cria variaveis
                //==================================================================================================================
                int numeroLinha = 0;
                int quantidadeLog = 0;
                int qtdcessionada = 0;
                // int vqtdregistros = 0; // warning 25/02/2019
                //==================================================================================================================
                // Consulta as transacoes do arquivo de retorno de cessao
                //==================================================================================================================
                cursor = Procedures.PR_ARQUIVO_ITEM_L(cn, request.CodigoArquivo, null, true, true, 0, 0, 0, false, true); // traz apenas os itens sem critica e nao bloqueados
                OracleDataReader reader = cursor.GetDataReader();
                //==================================================================================================================
                // Conferindo quantidade de registros carregados
                //==================================================================================================================
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CESSAO - Linhas carregadas, iniciando atualizacao transacao."));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                //==================================================================================================================
                // Varre as linhas
                //==================================================================================================================
                while (reader.Read())
                {
                    numeroLinha++;
                    quantidadeLog++;
                    //==================================================================================================================
                    // Log de 15 em 15 mil registros processados
                    //==================================================================================================================
                    if (quantidadeLog >= 15000)
                    {
                        quantidadeLog = 0;
                        lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                        lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CESSAO - Processamento {0} processadas {1} cessionadas ", numeroLinha, qtdcessionada));
                        LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    }
                    //==================================================================================================================
                    // Pega informacoes da linha
                    //==================================================================================================================
                    string tipoTransacao = reader.GetString(reader.GetOrdinal("TIPO_ARQUIVO_ITEM"));
                    string ChaveItem = "000000000000000000000000000000000000000000000000000000000000";
                    switch (tipoTransacao)
                    {
                        //==================================================================================================================
                        // R1 - Registro de DETALHE do cessao
                        //==================================================================================================================
                        case "R1":
                            //==================================================================================================================
                            // recupera os dados a serem passados para a procedure
                            //==================================================================================================================
                            string StatusItem = reader.GetString(reader.GetOrdinal("STATUS_ARQUIVO_ITEM"));
                            //==================================================================================================================
                            // o campo chave da tabela tera os dados da cessao recuperados na validacao
                            //==================================================================================================================
                            if (!reader.IsDBNull(reader.GetOrdinal("CHAVE")))
                                ChaveItem = reader.GetString(reader.GetOrdinal("CHAVE"));
                            int CodigoTransacao = Convert.ToInt32(ChaveItem.Substring(0, 20));
                            int CodigoFavorecido = Convert.ToInt32(ChaveItem.Substring(20, 20));
                            int CodigoContaFavorecido = Convert.ToInt32(ChaveItem.Substring(40, 20));
                            //==================================================================================================================
                            // Log para debug
                            //==================================================================================================================
                            //lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                            //lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CHAMANDO - log provisorio -STATUS {0} CodigoTransacao {1} CodigoArqRetCessao {2} CodigoProcRetCessao {3} CodigoFavorecido {4} CodigoContaFavorecido {5}", StatusItem, CodigoTransacao, CodigoArqRetCessao, CodigoProcRetCessao, CodigoFavorecido, CodigoContaFavorecido));
                            //LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                            //==================================================================================================================
                            // chama a procedure para processar o retorno de cessao
                            //==================================================================================================================
                            Procedures.PR_CESSIONA_S(CodigoTransacao, CodigoArqRetCessao, CodigoProcRetCessao, CodigoFavorecido, CodigoContaFavorecido);
                            //==================================================================================================================
                            // Log para debug
                            //==================================================================================================================
                            //lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                            //lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("Voltando - log provisorio - CodigoTransacao {0} CodigoArqRetCessao {1} CodigoProcRetCessao {2} CodigoFavorecido {3} CodigoContaFavorecido {4}", CodigoTransacao, CodigoArqRetCessao, CodigoProcRetCessao, CodigoFavorecido, CodigoContaFavorecido));
                            //LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                            qtdcessionada = qtdcessionada + 1;
                            break;
                    }
                }
                //==================================================================================================================
                // chama a procedure para LOGAR a cessao e mudar o status dos processos cessionados
                //==================================================================================================================
                Procedures.PR_CESSIONA_LOG_S(CodigoProcRetCessao);
                //==================================================================================================================
                //1328
                //==================================================================================================================
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CESSAO - FIM - Processamento {0} processadas {1} cessionadas ", numeroLinha, qtdcessionada));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                //==================================================================================================================
                // Reprocessa a agenda para os casos que foram alterados neste processo
                //==================================================================================================================
                //1328 Procedures.PR_PAGAMENTO_A2();
                //==================================================================================================================
            }
            catch (Exception ex)
            {

                //==================================================================================================================
                // Processa a excessao
                //==================================================================================================================
                resposta.ProcessarExcessao(ex);


            }
            finally
            {
                //==================================================================================================================
                // Finaliza conexao
                //==================================================================================================================
                if (cn != null)
                    cn.Dispose();
                //==================================================================================================================
                // Finaliza o cursor
                //==================================================================================================================
                if (cursor != null)
                    cursor.Dispose();
            }
            //==================================================================================================================
            // Retorna
            //==================================================================================================================
            return resposta;
        }
        //=============================================================================================O02

        //=============================================================================================P02
        private ContaFavorecidoInfo retornoCessaoVerificarFavorecido(string pCodigoSessao, string pCNPJFavorecido, string pNomeFavorecido, string pBanco, string pAgencia, string pConta, Dictionary<string, FavorecidoDetalheInfo> dicFavorecidos)
        {

            // Verificar se o favorecido existe, se nao existir, cadastrar um novo
            // Variaveis de controle
            bool existeConta = false;
            FavorecidoDetalheInfo fav;
            ContaFavorecidoInfo contaFavorecido = new ContaFavorecidoInfo();


            // Ver se o favorecido já está no cache de favorecidos
            if (!dicFavorecidos.ContainsKey(pCNPJFavorecido))
            {
                FavorecidoInfo favorecidoInfo;

                // Monta lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                List<ContaFavorecidoInfo> contas;

                condicoes.Add(new CondicaoInfo(
                        "CnpjFavorecido", CondicaoTipoEnum.Igual, pCNPJFavorecido));

                // Pede a lista de Favorecidos
                List<FavorecidoInfo> favorecidos =
                    PersistenciaHelper.Listar<FavorecidoInfo>(pCodigoSessao, condicoes);

                fav = new FavorecidoDetalheInfo();

                // Se nao existir, criar
                if (favorecidos.Count == 0)
                {

                    favorecidoInfo = new FavorecidoInfo();
                    favorecidoInfo.CnpjFavorecido = pCNPJFavorecido;
                    favorecidoInfo.NomeFavorecido = pNomeFavorecido;

                    // criar o favorecido
                    favorecidoInfo = PersistenciaHelper.Salvar<FavorecidoInfo>(pCodigoSessao, favorecidoInfo);
                    fav.CnpjFavorecido = favorecidoInfo.CnpjFavorecido;
                    fav.CodigoFavorecido = favorecidoInfo.CodigoFavorecido;
                    fav.NomeFavorecido = favorecidoInfo.NomeFavorecido;
                    dicFavorecidos.Add(pCNPJFavorecido, fav);


                }
                else
                {
                    // Favorecido está cadastrado, pegar as contas e depois adicionar no cache
                    fav.CodigoFavorecido = favorecidos[0].CodigoFavorecido;
                    fav.NomeFavorecido = favorecidos[0].NomeFavorecido;
                    fav.CnpjFavorecido = favorecidos[0].CnpjFavorecido;

                    // Buscar as contas do Favorecido 
                    condicoes.Clear();
                    condicoes.Add(new CondicaoInfo("CodigoFavorecido", CondicaoTipoEnum.Igual, fav.CodigoFavorecido));

                    // Solicita a lista                
                    contas = PersistenciaHelper.Listar<ContaFavorecidoInfo>(pCodigoSessao, condicoes);

                    foreach (ContaFavorecidoInfo item in contas)
                        fav.ContasFavorecido.Add(item);

                    // Adicionar no cache
                    dicFavorecidos.Add(pCNPJFavorecido, fav);

                }
            }
            else
                fav = dicFavorecidos[pCNPJFavorecido];



            // Verificar se a conta existe no favorecido
            foreach (ContaFavorecidoInfo item in fav.ContasFavorecido)
            {
                if (item.Banco == pBanco && item.Agencia == pAgencia && item.Conta == pConta)
                {
                    existeConta = true;
                    contaFavorecido = item;
                }
            }

            if (!existeConta)
            {

                // Se nao existir, criar
                contaFavorecido = new ContaFavorecidoInfo();
                contaFavorecido.CodigoFavorecido = fav.CodigoFavorecido;
                contaFavorecido.Banco = pBanco;
                contaFavorecido.Agencia = pAgencia;
                contaFavorecido.Conta = pConta;

                // criar a conta do favorecido
                contaFavorecido = PersistenciaHelper.Salvar<ContaFavorecidoInfo>(pCodigoSessao, contaFavorecido);

                fav.ContasFavorecido.Add(contaFavorecido);

                // Substitui o objeto no dicionário
                dicFavorecidos.Remove(fav.CnpjFavorecido);
                dicFavorecidos.Add(fav.CnpjFavorecido, fav);
            }
            return contaFavorecido;
        }
        //=============================================================================================P02


        //=============================================================================================Q02
        //1349 ESTE PROCESSO DELETA TRANSACAO COM STATUS RETORNO CESSAO 2
        //public EstornarLoteRetornoCessaoResponse EstornarLoteRetornoCessao(EstornarLoteRetornoCessaoRequest request)
        //{
        //    // Prepara a resposta
        //    EstornarLoteRetornoCessaoResponse resposta = new EstornarLoteRetornoCessaoResponse();
        //    resposta.PreparaResposta(request);

        //    // Declare o reader para que possa ser corretamente finalizado
        //    OracleConnection cn = Procedures.ReceberConexao();
        //    OracleRefCursor cursor = null;

        //    // Bloco de controle
        //    try
        //    {
        //        bool cancelarEstorno = false;

        //        // Verificar se é permitido efetuar estornoe
        //        // Pegar as transacoes, do arquivo a ser estornado, que estejam com status RetornadoOK, e ver se, em algum deles, 
        //        // há a marcação de transacao enviada para matera. Se houver, nao pode estornar.

        //        // Estornar da seguinte forma
        //        // RetornadoOK -> Colocar o valor do banco, agencia e conta ORIGINAL de volta nos campos banco, agencia e conta e voltar o status para NAORETORNADO
        //        // RetornoNaoIdentificado ->
        //        // ProdutoNaoCedivel -> voltar o status para NAORETORNADO
        //        // TransacaoJaLiquidada -> voltar o status para NAORETORNADO


        //        // Pegar as transacoes, do arquivo a ser estornado, que estejam com status RetornadoOK e com a marcação de transacao enviada para matera
        //        PR_TRANSACAO_L_Request transacaoListaRequest =
        //            new PR_TRANSACAO_L_Request()
        //            {
        //                //ApenasTransacoesJaPagas = true,
        //                CodigoArquivoRetornoCessao = request.CodigoArquivo,
        //                StatusRetornoCessao = TransacaoStatusRetornoCessaoEnum.RetornadoOK
        //            };


        //        cursor = Procedures.PR_TRANSACAO_L(cn, 0, transacaoListaRequest);
        //        OracleDataReader reader = cursor.GetDataReader();


        //        // Se houver algum nestas condiçoes, informar erro e sair
        //        if (reader.Read())
        //        {
        //            cancelarEstorno = true;
        //        }

        //        // Chamar a procedure para estornar
        //        Procedures.PR_ESTORNO_CESSAO_S(cn, request.CodigoArquivo);


        //        //Buscar o arquivo e marcá-lo 
        //        ArquivoInfo arquivoInfo =
        //            PersistenciaHelper.Receber<ArquivoInfo>(
        //                request.CodigoSessao, request.CodigoArquivo);

        //        if (!cancelarEstorno)
        //            arquivoInfo.StatusArquivo = ArquivoStatusEnum.Estornado;

        //        // Salva
        //        PersistenciaHelper.Salvar<ArquivoInfo>(
        //            request.CodigoSessao, arquivoInfo);
        //    }
        //    catch (Exception ex)
        //    {
        //        // Processa a excessao
        //        resposta.ProcessarExcessao(ex);
        //    }
        //    finally
        //    {
        //        // Finaliza conexao
        //        if (cn != null)
        //            cn.Dispose();

        //        // Finaliza o cursor
        //        if (cursor != null)
        //            cursor.Dispose();
        //    }

        //    // Retorna
        //    return resposta;
        //}
        //=============================================================================================Q02

        //=============================================================================================R02
        public ImportarTransacoesSitefResponse ImportarTransacoesSitef(ImportarTransacoesSitefRequest request)
        {
            // Prepara a resposta
            ImportarTransacoesSitefResponse resposta = new ImportarTransacoesSitefResponse();
            resposta.PreparaResposta(request);

            // Declara o cursor para que possa ser corretamente finalizado
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;

            // Bloco de controle
            try
            {
                // Inicializa as procedures
                OracleCommand cmTransacaoVendaS = Procedures.PR_TRANSACAO_VENDA_S_preparar(cn);

                // Pega o config
                ProcessoAtacadaoConfig config =
                    GerenciadorConfig.ReceberConfig<ProcessoAtacadaoConfig>();

                // Cria o interpretador para o arquivo 
                ArquivoSitef arquivoSitef = new ArquivoSitef();

                // Faz o parse do codigo do arquivo para repassar para as procedures
                int codigoArquivo = int.Parse(request.CodigoArquivo);

                // Pede lista de estabelecimentos e indexa o CodigoEstabelecimento por CNPJ
                List<EstabelecimentoInfo> estabelecimentos =
                    PersistenciaHelper.Listar<EstabelecimentoInfo>(request.CodigoSessao);
                Dictionary<string, int> dicionarioEstabelecimento = new Dictionary<string, int>();
                foreach (EstabelecimentoInfo estabelecimento in estabelecimentos)
                {
                    // Lista dos estabelecimentos CSU separados por ,
                    string[] estabelecimentosSitef = estabelecimento.EstabelecimentoSitef.Split(',');

                    // Adiciona no dicionario
                    foreach (string estabelecimentoSitef in estabelecimentosSitef)
                        if (estabelecimentoSitef != null && estabelecimentoSitef != "" && estabelecimentoSitef != "0000000000")
                            dicionarioEstabelecimento.Add(
                                estabelecimentoSitef,
                                int.Parse(estabelecimento.CodigoEstabelecimento));
                }

                // Produtos - Lista de Produtos
                // Pede lista de produtos e indexa o CodigoEstabelecimento por descricao
                List<ProdutoInfo> produtos =
                    PersistenciaHelper.Listar<ProdutoInfo>(request.CodigoSessao);

                Dictionary<string, int> dicionarioProduto = new Dictionary<string, int>();
                foreach (ProdutoInfo produto in produtos)
                    dicionarioProduto.Add(produto.NomeProduto, int.Parse(produto.CodigoProduto));

                // Verifica se o produto RCAR existe, senão, informa o erro
                // Este produto é pré-requisito pois é utilizado como fixo no processo Atacadao
                if (!dicionarioProduto.ContainsKey("RCAR"))
                    throw new Exception("Não foi encontrado o cadastro do produto RCAR. É necessário o cadastramento deste produto para prosseguir com o processo de importação de transações da TSYS");

                // Pega o codigo do Produtos RCAR
                // Para o processo do Atacadao, este codigo é fixo RCAR
                int codigoProdutoVenda = dicionarioProduto["RCAR"];

                // Consulta as transacoes do arquivo
                cursor = Procedures.PR_ARQUIVO_ITEM_L(cn, request.CodigoArquivo, null, 0, false);
                OracleDataReader reader = cursor.GetDataReader();

                // Varre as linhas
                while (reader.Read())
                {
                    // Pega o codigo arquivo item
                    int codigoArquivoItem =
                        Convert.ToInt32(
                            reader.GetDouble(reader.GetOrdinal("CODIGO_ARQUIVO_ITEM")));

                    // Pega informacoes da linha
                    string linha =
                        reader.GetString(reader.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));

                    // Quebra a linha
                    string[] linha2 = linha.Split(';');
                    for (int i = 0; i < linha2.Length; i++)
                        linha2[i] = linha2[i].Replace(@"""", "");

                    // Pega valores que vão ser lidos mais de uma vez
                    DateTime dataTransacao = (DateTime)arquivoSitef.LerCampo(linha2, 1, true);
                    dataTransacao += (TimeSpan)arquivoSitef.LerCampo(linha2, 2, true);
                    string nsuTef = (string)arquivoSitef.LerCampo(linha2, 4, true);
                    string nsuHost = (string)arquivoSitef.LerCampo(linha2, 5, true);
                    string codAutorizacao = (string)arquivoSitef.LerCampo(linha2, 14, true);
                    //int numeroParcela = (int)arquivoSitef.LerCampo(linha2, 15, true);
                    //jira SCF-549
                    //if (numeroParcela == 1)
                    int numeroParcela = 0;

                    // Traduz o codigo do estabelecimento
                    int codigoEstabelecimento = this.traduzirEstabelecimentoSitef(request.CodigoSessao, (string)arquivoSitef.LerCampo(linha2, 0, true), dicionarioEstabelecimento);

                    //SCF1172 - Inicio
                    var NumeroCartaoTrue = (string)arquivoSitef.LerCampo(linha2, 9, true);
                    NumeroCartaoTrue = NumeroCartaoTrue.PadLeft(19, '0');
                    //SCF1172 - fim

                    // Executa a procedure
                    Procedures.PR_TRANSACAO_VENDA_S(cmTransacaoVendaS,
                        new PR_TRANSACAO_VENDA_S_Request()
                        {
                            CodigoTransacao = null,
                            CodigoArquivo2 = codigoArquivo,
                            CodigoArquivoItem2 = codigoArquivoItem,
                            Chave = ChaveTransacaoHelper.GerarChaveComprovanteVenda(dataTransacao, codigoEstabelecimento.ToString(), nsuHost, codAutorizacao, numeroParcela.ToString()),
                            Chave2 = ChaveTransacaoHelper.GerarChaveComprovanteVenda(dataTransacao, codigoEstabelecimento.ToString(), nsuTef, codAutorizacao, numeroParcela.ToString()),
                            CodigoEstabelecimento = codigoEstabelecimento,
                            //CodigoPlano = null,
                            CodigoProduto = codigoProdutoVenda,
                            CodigoAutorizacao = (string)arquivoSitef.LerCampo(linha2, 14, true),
                            ValorVenda = (double)arquivoSitef.LerCampo(linha2, 10, true),
                            DataTransacao = dataTransacao,
                            //NumeroCartao = (string)arquivoSitef.LerCampo(linha2, 9, true), // SCF1172
                            NumeroCartao = NumeroCartaoTrue, //SCF1172
                            NsuHost = nsuHost,
                            NsuTEF = (string)arquivoSitef.LerCampo(linha2, 4, true),
                            NumeroParcela = (int)arquivoSitef.LerCampo(linha2, 15, true),
                            RetornarRegistro = false,
                            CodigoProcesso = request.CodigoProcesso,
                            Banco = config.Banco,
                            Agencia = config.Agencia,
                            StatusTransacao = TransacaoStatusEnum.Importado,
                            Conta = config.Conta,
                            DataRepasse = DateTime.Now,
                            ValorRepasse = (double)arquivoSitef.LerCampo(linha2, 10, true),
                            DataMovimento = dataTransacao,
                            AtualizaTransacao = true // SCF1072 -Marcos Matsuoka
                        });

                }
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                // Finaliza conexao
                if (cn != null)
                    cn.Dispose();

                // Finaliza o cursor
                if (cursor != null)
                    cursor.Dispose();
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================R02

        //=============================================================================================S02
        public ExportarResultadoConciliacaoResponse ExportarResultadoConciliacaoAtacadao(ExportarResultadoConciliacaoRequest request)
        {
            // Prepara resposta
            ExportarResultadoConciliacaoResponse resposta = new ExportarResultadoConciliacaoResponse();
            resposta.PreparaResposta(request);

            // Declara o cursor para que ele possa ser corretamente finalizado
            OracleRefCursor cursor = null;

            // Bloco de controle
            try
            {
                //DateTime dtFinal = DateTime.Now;
                //dtFinal = dtFinal.Subtract(new TimeSpan(3, 0, 0, 0, 0));
                //int statusTransacaoFiltro = (int)TransacaoStatusConciliacaoEnum.NaoConciliado;

                PR_TRANSACAO_NAO_CONCILIADA_L_Request transacaoVendaLrequest = new PR_TRANSACAO_NAO_CONCILIADA_L_Request()
                {
                    TipoProcesso = typeof(ProcessoAtacadaoInfo).Name
                };

                // traz apenas as transacoes nao conciliadas do processo solicitado
                cursor = Procedures.PR_TRANSACAO_NAO_CONCILIADA_L(Procedures.ReceberConexao(), transacaoVendaLrequest);
                OracleDataReader transacoes = cursor.GetDataReader();

                // Cria o arquivo
                StreamWriter fileStream = File.CreateText(request.CaminhoArquivoDestino);

                // Cria interpretador do arquivo
                ArquivoResultadoConciliacao arquivoConciliacao = new ArquivoResultadoConciliacao();

                // Insere o header
                fileStream.WriteLine(
                    arquivoConciliacao.CriarLinha("header"));

                // Varre gerando o detalhe
                int quantidadeRegistros = 0;
                TransacaoStatusConciliacaoEnum status;

                if (transacoes.HasRows)
                {
                    while (transacoes.Read())
                    {
                        quantidadeRegistros++;
                        status = SqlDbLib.EnumToObject<TransacaoStatusConciliacaoEnum>(transacoes["STATUS_CONCILIACAO"]);

                        DateTime dataInclusao = (DateTime)transacoes["DATA_INCLUSAO"];
                        String dataInclusaoString = dataInclusao.Day.ToString() + "/" + dataInclusao.Month.ToString() + "/" + dataInclusao.Year.ToString();

                        DateTime dataTransacao = (DateTime)transacoes["DATA_TRANSACAO"];
                        String dataTransacaoString = dataTransacao.Day.ToString() + "/" + dataTransacao.Month.ToString() + "/" + dataTransacao.Year.ToString();

                        String origem1 = transacoes["CODIGO_ARQUIVO_ITEM_1"].ToString();
                        String origem2 = transacoes["CODIGO_ARQUIVO_ITEM_2"].ToString();

                        String origem;
                        if (origem1 == "")
                            origem = "Sitef";
                        else
                            origem = transacoes["TIPO_ARQUIVO"].ToString();

                        // Gera linha de detalhe
                        fileStream.WriteLine(
                            arquivoConciliacao.CriarLinha(
                                "detalhe",
                                "Data", dataInclusaoString,
                                "NSU_HOST", transacoes["NSU_HOST"],
                                "DATA_TRANSACAO", dataTransacaoString,
                                "CODIGO_AUTORIZACAO", transacoes["CODIGO_AUTORIZACAO"],
                                "StatusConciliacao", status.ToString(),
                                "VALOR_VENDA", transacoes["VALOR_VENDA"],
                                "ORIGEM", origem,
                                "CODIGOPROCESSO", transacoes["CODIGO_PROCESSO"]
                            ));
                    }

                    // Fecha o arquivo
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                // Finaliza o cursor
                if (cursor != null)
                {
                    cursor.Connection.Dispose();
                    cursor.Dispose();
                }
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================S02

        ////=============================================================================================T02
        //public MontarArquivoMateraResponse montarArquivoMatera(MontarArquivoMateraRequest request)
        //{
        //    // Inicializa variaveis
        //    double valorTotal = 0;
        //    int qtdeRegistros = 0;
        //    int qtdeRegistrosGerados = 0;
        //    bool processarNegativo = false;

        //    StreamWriter fileStream = request.FileStream;
        //    ProcessoExtratoValidacaoConfig config = request.Config;
        //    PagamentoDetalheView pagamentoInfo = request.PagamentoInfo;
        //    ArquivoMateraFinanceiro arquivoMatera = (ArquivoMateraFinanceiro)request.ArquivoMatera;
        //    MontarArquivoMateraResponse resposta = new MontarArquivoMateraResponse();

        //    // Atualiza quantidades
        //    qtdeRegistros++;

        //    if (pagamentoInfo.ValorPagamento.HasValue)
        //    {
        //        valorTotal += pagamentoInfo.ValorPagamento.Value;

        //        ListarEstabelecimentoRequest reqEstab = new ListarEstabelecimentoRequest();
        //        reqEstab.FiltroCnpjFavorecido = pagamentoInfo.CNPJ;
        //        reqEstab.CodigoSessao = request.CodigoSessao;

        //        var resEstab = Mensageria.Processar<ListarEstabelecimentoResponse>(reqEstab);
        //        EstabelecimentoInfo estab = null;
        //        if (resEstab.StatusResposta == MensagemResponseStatusEnum.OK)
        //            if (resEstab.Resultado.Count > 0)
        //                estab = resEstab.Resultado[0];

        //        ListarEmpresaGrupoResponse res = Mensageria.Processar<ListarEmpresaGrupoResponse>(new ListarEmpresaGrupoRequest()
        //        {
        //            CodigoSessao = request.CodigoSessao
        //        });

        //        foreach (EmpresaGrupoInfo eg in res.Resultado)
        //        {
        //            if (estab.CodigoEmpresaGrupo == eg.CodigoEmpresaGrupo)
        //            {
        //                if (eg.EnviarNegativoEmpresaGrupo == "S")
        //                {
        //                    processarNegativo = true;
        //                    break;
        //                }
        //            }
        //        }

        //        if ((!processarNegativo && pagamentoInfo.ValorPagamento >= 0) || (processarNegativo && estab.CodigoEmpresaGrupo == config.CodigoGrupoGaleria))
        //        {
        //            qtdeRegistrosGerados++;

        //            // Gera linha de detalhe
        //            fileStream.WriteLine(
        //                arquivoMatera.CriarLinha(
        //                    "detalhe",
        //                    "BancoFavorecido", pagamentoInfo.Banco,
        //                    "AgenciaFavorecido", pagamentoInfo.Agencia,
        //                    "ContaCorrenteFavorecido", pagamentoInfo.Conta,
        //                    "CNPJFavorecido", pagamentoInfo.CNPJ,
        //                    "RazaoSocialFavorecido", pagamentoInfo.NomeFavorecido,
        //                    "DataVencimento", pagamentoInfo.DataPagamento,
        //                    "ValorPagamento", pagamentoInfo.ValorPagamento,
        //                    "IdentificacaoRegistro", qtdeRegistros,
        //                    "CategoriaRegistro", "I",
        //                    "CodigoProduto", "000000",//1447
        //                    "CodigoModalidade", "0",//1447
        //                    "CessaoCredito", "0"));//1447

        //            AtualizarStatusPagamentoDbRequest atualizarPagto = new AtualizarStatusPagamentoDbRequest();
        //            atualizarPagto.CodigoPagamento = pagamentoInfo.CodigoPagamento;
        //            atualizarPagto.Status = PagamentoStatusEnum.Enviado;

        //            // Pede inicializacao do servico de seguranca
        //            Mensageria.Processar(atualizarPagto);
        //        }
        //    }

        //    // Retorna
        //    resposta.QtdeRegistros = qtdeRegistros;
        //    resposta.QtdeRegistrosGerados = qtdeRegistrosGerados;
        //    resposta.ValorTotal = valorTotal;

        //    return resposta;
        //}
        //=============================================================================================T02


        


        /// <summary>
        /// Gera o processo para a exportacao do arquivo de pagamentos (Matera Financeiro)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ProcessarExportacaoPagamentosResponse ProcessarExportacaoPagamentos(ProcessarExportacaoPagamentosRequest request)
        {
            // prepara a resposta
            ProcessarExportacaoPagamentosResponse resposta = new ProcessarExportacaoPagamentosResponse();
            resposta.PreparaResposta(request);

            ProcessoExportacaoPagamentoInfo processoInfo = null;

            if (request.CodigoProcesso == null)
            {
                // cria o processo de exportação
                processoInfo =
                    new ProcessoExportacaoPagamentoInfo()
                    {
                        
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                // busca as configuracoes, para pegar o diretorio de exportacao
                ConfiguracaoGeralInfo configGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(request.CodigoSessao, null);
            
                
                // Executa o processo assíncrono 
                ExportarMateraFinanceiroRequest req2 = 
                    new ExportarMateraFinanceiroRequest {
                     CaminhoArquivoDestino = configGeral.DiretorioExportacaoMateraFinanceiro,
                     CodigoProcesso = request.CodigoProcesso,
                     CodigoSessao = request.CodigoSessao
                };

                
                
                
                ExportarMateraFinanceiroResponse resp2 = ExportarMateraFinanceiro(req2);
                resposta.StatusResposta = resp2.StatusResposta;
                resposta.DescricaoResposta = resp2.DescricaoResposta;
                resposta.RepassarExcessao = resp2.RepassarExcessao;
                
               
       
            }

            // retorna a resposta
            return resposta;
        }


        //=============================================================================================U02
        public ExportarMateraFinanceiroResponse ExportarMateraFinanceiro(ExportarMateraFinanceiroRequest request)
        {
            // Prepara resposta
            ArquivoMateraFinanceiro arquivoMatera = new ArquivoMateraFinanceiro();
            ExportarMateraFinanceiroResponse resposta = new ExportarMateraFinanceiroResponse();
            resposta.PreparaResposta(request);

            // Gerar log de inicio de processo
            Mensageria.Processar<SalvarLogResponse>(
                new SalvarLogRequest()
                {
                    CodigoUsuario = "",
                    Descricao = "Iniciando o processo de exportação.",
                    TipoOrigem = "ProcessoInfo",
                    CodigoOrigem = request.CodigoProcesso,
                    CodigoSessao = request.CodigoSessao
                });


            // busca as configurações de dominios fixos
            // Busca o codigo da lista
            //List<CondicaoInfo>  condicoes = new List<CondicaoInfo>();
            //condicoes.Add(new CondicaoInfo("NomeLista", CondicaoTipoEnum.Igual, "MateraFinancDominioFixo"));
            //List<ListaInfo> listas = PersistenciaHelper.Listar<ListaInfo>(request.CodigoSessao, condicoes);


            List<CondicaoInfo>  condicoes = new List<CondicaoInfo>();
            condicoes.Add(new CondicaoInfo("NomeLista", CondicaoTipoEnum.Igual, "MateraFinancDominioFixo"));

         
            // busca os itens do dominio
            List<ListaItemInfo> dominiosFixos = PersistenciaHelper.Listar<ListaItemInfo>(request.CodigoSessao, condicoes);
            string[] dominios = new string[] { "CNPJ_EMPRESA", "CNPJ_FILIAL", "APELIDO_FILIAL", "NUM_BANCO_DEBITO", "NUM_AGENCIA_DEBITO", "NUM_CONTA_DEBITO", "NUM_BANCO_DEBITO2", "NUM_AGENCIA_DEBITO2", "NUM_CONTA_DEBITO2" };
            string mensagemErro = "";

            foreach (string dominio in dominios)
            {
                if (dominiosFixos.Find(x => (x.Descricao == dominio && x.Ativo == true)) == null)
                    mensagemErro = String.Format("{0} Não foi encontrado o cadastro do domínio {1}", mensagemErro, dominio);
            }

            // se nao tem o domínio, loga o erro e sai da rotina
            if (mensagemErro != "")
            {
                // Lanca o erro de dominios
                throw new ProcessoException(mensagemErro);

                resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                resposta.DescricaoResposta = mensagemErro;
                resposta.RepassarExcessao = true;
                resposta.Erro = mensagemErro;
                return resposta;

            }
          

            // pega o usuario para recuperar o nome e colocar no arquivo gerado
            UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);

            // Inicializa variaveis
            int qtdeRegistrosGerados = 0;
            int qtdeRegistro04 = 0;

            // Bloco de controle
            try
            {
                string status = SqlDbLib.EnumToDb<PagamentoStatusEnum>(PagamentoStatusEnum.Pendente).ToString();
                // Pede a lista de pagamentos a serem feitos
                List<PagamentoInfo> pagamentos =
                    PersistenciaHelper.Listar<PagamentoInfo>(
                        request.CodigoSessao,
                        new List<CondicaoInfo>() 
                        {
                            new CondicaoInfo("FiltroStatusPagamento", CondicaoTipoEnum.Igual, status), // Pendentes
                            new CondicaoInfo("RetornarReferencias", CondicaoTipoEnum.Igual, true)
                        });
             
                
                // Cria interpretador do arquivo
                string caminhoArquivo;

                if (request.CaminhoArquivoDestino.EndsWith(@"\") || request.CaminhoArquivoDestino.EndsWith("/"))
                    caminhoArquivo = request.CaminhoArquivoDestino;
                else
                    caminhoArquivo = request.CaminhoArquivoDestino + @"\";

                string nomeArquivo = caminhoArquivo + arquivoMatera.GerarNomeArquivo();

                // Pega detalhe do processo
                ProcessoInfo processoInfo =
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        request.CodigoSessao, request.CodigoProcesso);

                string codigoPagamentoAnterior = null;

                // inicia a escrita do arquivo
                using (StreamWriter fileStream = File.CreateText(nomeArquivo))
                {
                    // Captura os dominios fixos
                    string cnpjEmpresa = dominiosFixos.Find(x => (x.Descricao == "CNPJ_EMPRESA" && x.Ativo == true)).Valor;
                    string cnpjFilial = dominiosFixos.Find(x => (x.Descricao == "CNPJ_FILIAL" && x.Ativo == true)).Valor;
                    
                    string bancoDebito = "";
                    string agenciaDebito = "";
                    string contaDebito = "";
                    string formaPagamento = "";
                    string indicadorRealTime = "";

                    string bancoDebito1 = dominiosFixos.Find(x => (x.Descricao == "NUM_BANCO_DEBITO" && x.Ativo == true)).Valor;
                    string agenciaDebito1 = dominiosFixos.Find(x => (x.Descricao == "NUM_AGENCIA_DEBITO" && x.Ativo == true)).Valor;
                    string contaDebito1 = dominiosFixos.Find(x => (x.Descricao == "NUM_CONTA_DEBITO" && x.Ativo == true)).Valor;

                    string bancoDebito2 = dominiosFixos.Find(x => (x.Descricao == "NUM_BANCO_DEBITO2" && x.Ativo == true)).Valor;
                    string agenciaDebito2 = dominiosFixos.Find(x => (x.Descricao == "NUM_AGENCIA_DEBITO2" && x.Ativo == true)).Valor;
                    string contaDebito2 = dominiosFixos.Find(x => (x.Descricao == "NUM_CONTA_DEBITO2" && x.Ativo == true)).Valor;

                    //string apelidoFilial = dominiosFixos.Find(x => (x.Descricao == "APELIDO_FILIAL" && x.Ativo == true)).Valor;

                    // RETIRAR O DOMINIO APELIDO FILIAL E MANDAR SEMPRE BRANCO

                    
                    foreach (PagamentoDetalheView pagamentoInfo in pagamentos)
                    {
                        //****
                        //**** Se o pagamento estiver dividido em duas referencias
                        //****  Gerar o registro 01 para o pagamento maior (maior valor)
                        //****  Gerar o registro 04 para o pagamento menor 
                        //**** Se só houver 1 referencia, gerar apenas o registro 01
                        //****
                        
                        if (codigoPagamentoAnterior == null || codigoPagamentoAnterior != pagamentoInfo.CodigoPagamento)
                        {
                            bancoDebito = bancoDebito1;
                            agenciaDebito = agenciaDebito1;
                            contaDebito = contaDebito1;
                            indicadorRealTime = "N";
                            formaPagamento = "DOC";

                            if (pagamentoInfo.Banco == bancoDebito2) {
                                bancoDebito = bancoDebito2;
                                agenciaDebito = agenciaDebito2;
                                contaDebito = contaDebito2;
                                formaPagamento = "CC";
                                indicadorRealTime = "S";
                            }

                            if (pagamentoInfo.Banco == bancoDebito1) {
                                formaPagamento = "CC";
                                indicadorRealTime = "S";
                            }

                            qtdeRegistrosGerados++;
                            // cod_forma_pagto = Se for pagto para 237, é CC, senao é TED
                            //Grava registro tipo 01
                            fileStream.WriteLine(
                                arquivoMatera.CriarLinha(
                                    "Registro01",
                                    "CnpjEmpresa", cnpjEmpresa,
                                    "CNPJFilial", cnpjFilial,
                                    "BancoEDI", bancoDebito,
                                    "AgenciaEDI", agenciaDebito,
                                    "ContaCorrenteEDI", contaDebito,
                                    "CNPJFilialProp", cnpjEmpresa,
                                    "CNPJCPFFornecedor", pagamentoInfo.CNPJ.Substring(1,14),
                                    "CodigoFormaPagto", formaPagamento,
                                    "Banco", pagamentoInfo.Banco,
                                    "Agencia", pagamentoInfo.Agencia,
                                    "ContaCorrente", pagamentoInfo.Conta,
                                    "NumeroDocumento", pagamentoInfo.CodigoPagamento,
                                    "DataEmissao", DateTime.Now,
                                    "DataVencimento", pagamentoInfo.DataPagamento,
                                    "DataProvisao", DateTime.Now,
                                    "ValorPagamento", pagamentoInfo.ValorPagamento,
                                    "Referencia", String.Format("{0:000}", Convert.ToInt16(pagamentoInfo.CodigoReferencia)),
                                    "IndicadorRealTime", indicadorRealTime,
                                    "TipoDocumento", pagamentoInfo.CodigoTipoDocumentoMatera,
                                    "TipoProdutoServico", pagamentoInfo.CodigoTipoProdutoServicoMatera));

                            

                            // atualiza o statusa data de envio do pagamento
                            pagamentoInfo.Status = PagamentoStatusEnum.Enviado;
                            pagamentoInfo.DataEnvio = DateTime.Now;

                            AtualizarStatusPagamentoDbRequest atualizarPagto = new AtualizarStatusPagamentoDbRequest();
                            atualizarPagto.CodigoPagamento = pagamentoInfo.CodigoPagamento;
                            atualizarPagto.Status = PagamentoStatusEnum.Enviado;
                            atualizarPagto.CodigoUsuarioEnvio = usuarioInfo.CodigoUsuario;

                            // todo: chamar a atualizacao da linha
                            Mensageria.Processar(atualizarPagto);

                            codigoPagamentoAnterior = pagamentoInfo.CodigoPagamento;


                        }
                        else
                        {
                            //Grava registro tipo 04
                            qtdeRegistro04++;
                            fileStream.WriteLine(
                                arquivoMatera.CriarLinha(
                                    "Registro04",
                                    "ValorPagamento", pagamentoInfo.ValorReferencia,
                                    "CodigoContaContabil", pagamentoInfo.CodigoContaContabil,
                                    "CNPJFilial", cnpjEmpresa,
                                    "Referencia", String.Format("{0:000}", Convert.ToInt16(pagamentoInfo.CodigoReferencia))));
                        }
                        
                    }

                    // Fecha o arquivo
                    fileStream.Close();
                }

                string mensagemLog;
                if (qtdeRegistrosGerados <= 0)
                {
                    mensagemLog = "Não foi gerado o arquivo Matera pois não foram encontrados pagamentos Pendentes de exportação.";
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    resposta.DescricaoResposta = mensagemLog;
                    resposta.RepassarExcessao = false;
                    File.Delete(nomeArquivo);
                }
                else
                    mensagemLog = String.Format("Foram gerados {0} registros 01 e {1} registros 04. ", qtdeRegistrosGerados, qtdeRegistro04);


                // Gravar log de finalizacao
                Mensageria.Processar<SalvarLogResponse>(
                    new SalvarLogRequest()
                    {
                        CodigoUsuario = "",
                        Descricao = mensagemLog,
                        TipoOrigem = "ProcessoInfo",
                        CodigoOrigem = request.CodigoProcesso,
                        CodigoSessao = request.CodigoSessao
                    });


                if (qtdeRegistrosGerados > 0)
                {
                    // Salva os valores do processo pagamento
                    ((ProcessoExportacaoPagamentoInfo)processoInfo).DataEnvioMatera = DateTime.Now;
                    ((ProcessoExportacaoPagamentoInfo)processoInfo).CodigoUsuarioEnvioMatera =
                        SegurancaHelper.ReceberUsuario(request.CodigoSessao).CodigoUsuario;

                    // Salva o processo
                    PersistenciaHelper.Salvar<ProcessoInfo>(
                        request.CodigoSessao, processoInfo);
                }

                
                
            }
            catch (Exception ex)
            {
                // Gravar log de erro
                Mensageria.Processar<SalvarLogResponse>(
                    new SalvarLogRequest()
                    {
                        CodigoUsuario = "",
                        Descricao = ex.Message + " " + ex.InnerException,
                        TipoOrigem = "ProcessoInfo",
                        CodigoOrigem = request.CodigoProcesso,
                        CodigoSessao = request.CodigoSessao
                    });

                resposta.ProcessarExcessao(ex);

            }
            return resposta;
        }
        //=============================================================================================U02

        //=============================================================================================V02
        public ExportarMateraContabilResponse ExportarMateraContabil(ExportarMateraContabilRequest request)
        {

            // Inicializa variaveis
            DateTime hoje = DateTime.Now;
            long numeroSequencial = 0;
            string nomearquivo = request.CaminhoArquivoDestino;// 1447
            // int vQtdLinhas = 0; //1447 // warning 25/02/2019
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo(); //1447
            // ====================================================================================
            //1447 - LOG
            // ====================================================================================
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(String.Format("Inicio exportacao Matera Contabil - Especifico do arquivo CSU [{0}] ", request.CodigoArquivo));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            // ====================================================================================
            //1447 - FIM
            // ====================================================================================

            // Prepara resposta
            ExportarMateraContabilResponse resposta = new ExportarMateraContabilResponse();
            resposta.PreparaResposta(request);

            // Declara o cursor e cria filestream
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;
            StreamWriter fileStream = null;
            int contadorLinhas = 0;
            // Bloco de controle
            try
            {
                // ==========================================================================================================
                // Cria o arquivo
                // ==========================================================================================================
                fileStream = File.CreateText(nomearquivo);
                ServicoIntegracaoArquivoMateraContabilConfig arquivoMateraContabilConfig = new ServicoIntegracaoArquivoMateraContabilConfig();
                // ==========================================================================================================
                // 1447 - ANALISE
                // Considera a DATA-GERACAO do registro "0" - HEADER do aruqivo CSU, como DT_LANC_CONTABIL do arquivo exportado
                // ==========================================================================================================
                cursor = Procedures.PR_ARQUIVO_ITEM_L(cn, request.CodigoArquivo, "0", 0, false);
                OracleDataReader readerHeader = cursor.GetDataReader();
                DateTime dataGeracaoHeader = new DateTime();
                if (readerHeader.Read())
                {
                    ArquivoCSU arquivoCSU = new ArquivoCSU();
                    string linha = readerHeader.GetString(readerHeader.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));
                    dataGeracaoHeader = (DateTime)arquivoCSU.LerCampo("0", linha, "DATA-GERACAO", true);
                }
                // ====================================================================================
                //1447 - LOG
                // ====================================================================================
                lLogSCFProcessoEstagioEventoInfo.setDescricao("Carregando as transacoes do arquivo ");
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                // ====================================================================================
                ArquivoMateraContabil arquivoMateraContabil = new ArquivoMateraContabil();
                PR_TRANSACAO_VENDA_L_Request transacaoVendaLrequest = new PR_TRANSACAO_VENDA_L_Request(){CodigoArquivo = request.CodigoArquivo};
                cursor = Procedures.PR_TRANSACAO_VENDA_L(cn, 0, transacaoVendaLrequest);
                OracleDataReader reader = cursor.GetDataReader();
                // ====================================================================================
                //1447 - LOG
                // ====================================================================================
                lLogSCFProcessoEstagioEventoInfo.setDescricao(String.Format("Data do Lancamento Contabil [{0}] - Débito e Crédito serão gerados para cada valor BRUTO e COMISSAO diferente de ZEROS da transacao", dataGeracaoHeader));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                // ====================================================================================
                // Varre as linhas
                // ====================================================================================
                while (reader.Read())
                {
                    DateTime dataTransacao = (DateTime)reader["DATA_TRANSACAO"];
                    //Double valorBruto = (Double)reader["VALOR_VENDA"];
                    //Double valorComissao = 0;
                    //double valorBruto = (reader["VALOR_COMISSAO"] != System.DBNull.Value) ? (Double)reader["VALOR_VENDA"] : 0;
                    //if (reader["VALOR_COMISSAO"] != System.DBNull.Value)
                    //    valorComissao = valorComissao = (Double)reader["VALOR_COMISSAO"];
                    //DateTime dataContabil;
                    //if (dataTransacao.Month == hoje.Month)
                    //    dataContabil = dataTransacao;
                    //else
                    //    dataContabil = dataGeracaoHeader;
                    double valorBruto = (reader["VALOR_VENDA"] != System.DBNull.Value) ? (Double)reader["VALOR_VENDA"] : 0;
                    double valorComissao = (reader["VALOR_COMISSAO"] != System.DBNull.Value) ? (Double)reader["VALOR_COMISSAO"] : 0;
                    DateTime dataContabil = (dataTransacao.Month == hoje.Month) ? dataTransacao : dataGeracaoHeader;

                    if (valorBruto > 0)
                    {
                        contadorLinhas++;
                        // ====================================================================================
                        // Gera evento contabil - Valor Bruto - Debito - Conta Contabil 1
                        // ====================================================================================
                        numeroSequencial = retornarNumeroSequencial(numeroSequencial);
                        fileStream.WriteLine(
                            gerarTransacao(
                                cn, arquivoMateraContabil, arquivoMateraContabilConfig, valorBruto,
                                arquivoMateraContabilConfig.COD_CONTA_CONTABIL_1, "D", dataContabil, numeroSequencial, arquivoMateraContabilConfig.HIST_1));
                        // ====================================================================================
                        // Gera evento contabil - Valor Bruto - Credito - Conta Contabil 2
                        // ====================================================================================
                        numeroSequencial = retornarNumeroSequencial(numeroSequencial);
                        fileStream.WriteLine(
                            gerarTransacao(
                                cn, arquivoMateraContabil, arquivoMateraContabilConfig, valorBruto,
                                arquivoMateraContabilConfig.COD_CONTA_CONTABIL_2, "C", dataContabil, numeroSequencial, arquivoMateraContabilConfig.HIST_1));
                    }
                    if (valorComissao > 0)
                    {
                        contadorLinhas++;
                        // ====================================================================================
                        // Gera evento contabil - Valor Comissao - Credito - Conta Contabil 1
                        // ====================================================================================
                        numeroSequencial = retornarNumeroSequencial(numeroSequencial);
                        fileStream.WriteLine(
                            gerarTransacao(
                                cn, arquivoMateraContabil, arquivoMateraContabilConfig, valorComissao,
                                arquivoMateraContabilConfig.COD_CONTA_CONTABIL_1, "C", dataContabil, numeroSequencial, arquivoMateraContabilConfig.HIST_2));
                        // ====================================================================================
                        // Gera evento contabil - Valor Comissao - Debito - Conta Contabil 2
                        // ====================================================================================
                        numeroSequencial = retornarNumeroSequencial(numeroSequencial);
                        fileStream.WriteLine(
                            gerarTransacao(
                                cn, arquivoMateraContabil, arquivoMateraContabilConfig, valorComissao,
                                arquivoMateraContabilConfig.COD_CONTA_CONTABIL_2, "D", dataContabil, numeroSequencial, arquivoMateraContabilConfig.HIST_2));
                    }
                }
                if (contadorLinhas <= 0)
                {
                    string mensagem = "Fim Exportacao Matera Contabil - Não Gerado - Motivo: Não tem dados";
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(mensagem);                    //1447 - LOG
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroPrograma; //1477 para .ErroNegocio tem que ter criticas;
                    resposta.RepassarExcessao = false;
                    resposta.DescricaoResposta = mensagem;

                }
                else
                {
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(String.Format("Fim Exportado Matera Contabil - Gerado com [{0}] linhas - Arquivo [{1}]", contadorLinhas.ToString(), request.CaminhoArquivoDestino)); //1447 - LOG
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }
            finally
            {

                if (fileStream != null)
                    fileStream.Close();
                if (contadorLinhas <= 0)
                    File.Delete(request.CaminhoArquivoDestino);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================V02

        //=============================================================================================X02
        private long retornarNumeroSequencial(long numeroSequencial)
        {
            if (numeroSequencial >= 9999999999)
                return 1;
            else
                return (++numeroSequencial);
        }
        //=============================================================================================X02

        //=============================================================================================Y02
        public string gerarTransacao(OracleConnection cn, ArquivoMateraContabil arquivoMateraContabil, ServicoIntegracaoArquivoMateraContabilConfig config, double valor, string conta, string debitoCredito, DateTime dataTransacao, long numeroSequencial, string descricao)
        {
            // Gera a linha
            return
                arquivoMateraContabil.CriarLinha("detalhe",
                    "COD_EMPRESA", config.COD_EMPRESA,
                    "COD_TIPO_CONTABIL", config.COD_TIPO_CONTABIL,
                    "NUM_GUIA", config.NUM_GUIA,
                    "COD_SIS", config.COD_SIS,
                    "NUM_LANC", numeroSequencial,
                    "COD_FILIAL", config.COD_FILIAL,
                    "COD_CONTA_CONTABIL", conta,
                    "ID_CENTRO_CUSTO", config.ID_CENTRO_CUSTO,
                    "COD_PROJETO", config.COD_PROJETO,
                    "DT_LANC_CONTABIL", dataTransacao,
                    "TIPO_LANC", debitoCredito,
                    "COD_HIST_PADRAO", config.COD_HIST_PADRAO,
                    "REFERENCIA", config.REFERENCIA,
                    "DT_CONVERSAO", config.DT_CONVERSAO,
                    "VLR", valor,
                    "COMPLEMENTO", config.COMPLEMENTO,
                    "HIST", descricao,
                    "IND_RATEIO", config.IND_RATEIO,
                    "COD_CONTA_CONTRA", "".PadLeft(15),
                    "ID_CCUSTO_CONTRA", config.ID_CCUSTO_CONTRA /*,
                    "COD_CENTRO_CUSTO", config.COD_CENTRO_CUSTO,
                    "COD_CENTRO_CUSTO_CONTRA", config.COD_CENTRO_CUSTO_CONTRA */
                );
        }
        //=============================================================================================Y02


        //=============================================================================================Z02
        //==================================================================================================================
        // Faz a exportação do arquivo Extrato para a CCI
        //================================================================================================================== 
        //1333        public ExportarExtratoParaCCIResponse ExportarExtratoParaCCI(ExportarExtratoParaCCIRequest request)
        public ExportarExtratoParaGrupoResponse ExportarExtratoParaGrupo(ExportarExtratoParaGrupoRequest request)
        {
            //==================================================================================================================
            // Prepara resposta
            //============================================================================3======================================
            //1333            ExportarExtratoParaCCIResponse resposta = new ExportarExtratoParaCCIResponse();
            ExportarExtratoParaGrupoResponse resposta = new ExportarExtratoParaGrupoResponse();
            resposta.PreparaResposta(request);

            //==================================================================================================================
            // Bloco de controle
            //==================================================================================================================
            OracleConnection cn = null;
            OracleDataReader dr = null;
            StreamWriter writer = null;

            cn = Procedures.ReceberConexao();
            //SCF 1701 - ID 29
            OracleCommand cmArquivoItemS = Procedures.PR_ARQUIVO_ITEM_S_preparar(cn);
            OracleCommand cmTransacaoStatusS = Procedures.PR_TRANSACAO_STATUS_S_preparar(cn);

            try
            {
                var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                //==================================================================================================================
                // Pega configuracoes
                //==================================================================================================================
                ServicoIntegracaoConfig config = ConfigHelper.Receber<ServicoIntegracaoConfig>(request.CodigoSessao);
                if (config == null)
                    config = new ServicoIntegracaoConfig();

                //==================================================================================================================
                // Resource - Nilo - 28/04/2015 - Alterado para pegar do Usuário Robo do Config
                //==================================================================================================================
                ServicoAgendadorConfig configServico = GerenciadorConfig.ReceberConfig<ServicoAgendadorConfig>();
                if (configServico == null)
                    configServico = new ServicoAgendadorConfig();
                //==================================================================================================================
                // ECOMMERCE - Fernando Bove - 20151007
                // BUSCA O ARQUIVO PARA PEGAR O LAYOUT
                //==================================================================================================================
                // Pede detalhe do arquivo
                //==================================================================================================================
                ArquivoInfo arquivoInfo = PersistenciaHelper.Receber<ArquivoInfo>(request.CodigoSessao, request.CodigoArquivo);
                if (!arquivoInfo.LayoutArquivo.Equals("2") && !arquivoInfo.LayoutArquivo.Equals("5"))
                {
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS - Grupo [{0}] - ARQUIVO NÃO SERÁ EXPORTADO - Código Versão de layout não suportada [{1}] ", arquivoInfo.SiglaGrupo, arquivoInfo.LayoutArquivo));
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    return resposta;
                    // 30/10/2018 throw new Exception("Versão de layout de entrada não suportada na exportação");
                }
                //==================================================================================================================
                // Pega configuracoes gerais do sistema
                //==================================================================================================================
                ConfiguracaoGeralInfo configuracaoGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(request.CodigoSessao, null);
                string layoutArquivo = "2";
                //==================================================================================================================
                // Prepara leitor arquivo Tsys
                //==================================================================================================================
                if (arquivoInfo.SiglaGrupo.Equals("CCI"))
                {
                    layoutArquivo = configuracaoGeral.LayoutExportacaoCCI;
                }
                else if (arquivoInfo.SiglaGrupo.Equals("ATA"))
                {
                    layoutArquivo = configuracaoGeral.LayoutExportacaoATA;
                }
                else if (arquivoInfo.SiglaGrupo.Equals("GAL"))
                {
                    layoutArquivo = configuracaoGeral.LayoutExportacaoGAL;
                }
                ArquivoTSYS arquivoExtrato = new ArquivoTSYS(layoutArquivo);

                //==================================================================================================================
                // Conferindo quais tipos de registro devem ser exportados para o grupo
                //==================================================================================================================
                string ArquivoTsysGrupo = "ArquivoTSYS" + arquivoInfo.SiglaGrupo; //1333
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                condicoes.Add(new CondicaoInfo("TipoArquivo", CondicaoTipoEnum.Igual, ArquivoTsysGrupo));                         // 1333 "TipoArquivo", CondicaoTipoEnum.Igual, "ArquivoTSYS"));
                condicoes.Add(new CondicaoInfo("EnviarCCI", CondicaoTipoEnum.Igual, false));
                //==================================================================================================================
                // Buscar os tipos de registro que não devem ser enviado à CCI
                //==================================================================================================================
                List<ConfiguracaoTipoRegistroInfo> configuracaoTipoRegistroInfos = PersistenciaHelper.Listar<ConfiguracaoTipoRegistroInfo>(request.CodigoSessao, condicoes);
                Dictionary<string, int> dicionarioTipoRegistroExclusao = new Dictionary<string, int>();
                foreach (ConfiguracaoTipoRegistroInfo configuracaoTipoRegistro in configuracaoTipoRegistroInfos)
                {
                    dicionarioTipoRegistroExclusao.Add(configuracaoTipoRegistro.TipoRegistro, int.Parse(configuracaoTipoRegistro.CodigoConfiguracaoTipoRegistro));
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso); //1333
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS - Grupo [{0}] - Configurado para não exportar este tipo de registro: [{1}] ", arquivoInfo.SiglaGrupo, configuracaoTipoRegistro.TipoRegistro)); //1333
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }

                //==================================================================================================================
                // MELHORIAS-2017 - Buscar a configuracao de dias para calcular a data de repasse por tipo de registro - GRUPO ATACADAO
                //==================================================================================================================
                Dictionary<string, int> dicDiasRepasseTipoReg = new Dictionary<string, int>();

                if (arquivoInfo.SiglaGrupo.Equals("ATA"))
                {
                    int diasRepasse = 0;

                    condicoes = new List<CondicaoInfo>();
                    condicoes.Add(new CondicaoInfo("TipoArquivo", CondicaoTipoEnum.Igual, "ArquivoTSYSATA"));
                    List<ConfiguracaoTipoRegistroInfo> diasRepasseTipoRegList = PersistenciaHelper.Listar<ConfiguracaoTipoRegistroInfo>(request.CodigoSessao, condicoes);
                    foreach (ConfiguracaoTipoRegistroInfo diasRepasseTipoReg in diasRepasseTipoRegList)
                    {
                        if (!int.TryParse(diasRepasseTipoReg.DiasRepasse, out diasRepasse))
                            diasRepasse = 0;

                        dicDiasRepasseTipoReg.Add(diasRepasseTipoReg.TipoRegistro, diasRepasse);
                    }
                }

                //==================================================================================================================
                // recupera as listas
                //==================================================================================================================
                List<ListaInfo> listas = PersistenciaHelper.Listar<ListaInfo>(request.CodigoSessao);
                //==================================================================================================================
                // cria as condicoes para pegar as listas de meio de pagamento
                //==================================================================================================================
                condicoes = new List<CondicaoInfo>();
                foreach (ListaInfo lista in listas)
                {
                    if (lista.Descricao == "MeioPagamento")
                        condicoes.Add(new CondicaoInfo("pCODIGO_LISTA", CondicaoTipoEnum.Igual, lista.CodigoLista));
                }

                //==================================================================================================================
                // Buscar os tipos de meios de pagamento que não devem ser enviado à CCI
                //==================================================================================================================
                // melhorias-2017 - adiciona o codigo do grupo na busca. Essa tabela de configuracao, agora está por grupo
                condicoes.Add(new CondicaoInfo("CodigoEmpresaGrupo", CondicaoTipoEnum.Igual, arquivoInfo.CodigoEmpresaGrupo));
                List<ListaConfiguracaoItemInfo> meiosPagamento = PersistenciaHelper.Listar<ListaConfiguracaoItemInfo>(request.CodigoSessao, condicoes);
                Dictionary<string, string> dicionarioMeioPagamentoExclusao = new Dictionary<string, string>();
                foreach (ListaConfiguracaoItemInfo meioPagamento in meiosPagamento)
                    if (meioPagamento.Configuracao == "N")
                    {
                        dicionarioMeioPagamentoExclusao.Add(meioPagamento.Valor, meioPagamento.Configuracao);
                        lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso); //1333
                        lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS - Grupo [{0}] - Configurado para não exportar este meio de pagamento: [{1}-{2}]", arquivoInfo.SiglaGrupo, meioPagamento.Valor, meioPagamento.NomeListaItem)); //1333
                        LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    }
                //==================================================================================================================
                // SCF1139 cria as condicoes para pegar as listas de Codigo de Ajuste
                //==================================================================================================================
                condicoes = new List<CondicaoInfo>();
                foreach (ListaInfo lista in listas)
                {
                    if (lista.Descricao == "CodigoAjuste")
                        condicoes.Add(new CondicaoInfo("pCODIGO_LISTA", CondicaoTipoEnum.Igual, lista.CodigoLista));
                }
                //==================================================================================================================
                // Buscar os codigos de ajuste que não devem ser enviado à CCI
                //==================================================================================================================
                
                // melhorias-2017 - adiciona o codigo do grupo na busca. Essa tabela de configuracao, agora está por grupo
                condicoes.Add(new CondicaoInfo("CodigoEmpresaGrupo", CondicaoTipoEnum.Igual, arquivoInfo.CodigoEmpresaGrupo));
                
                List<ListaConfiguracaoItemInfo> codigosAjuste = PersistenciaHelper.Listar<ListaConfiguracaoItemInfo>(request.CodigoSessao, condicoes);
                Dictionary<string, string> dicionarioCodigoAjusteExclusao = new Dictionary<string, string>();
                foreach (ListaConfiguracaoItemInfo codigoAjuste in codigosAjuste)
                    if (codigoAjuste.Configuracao == "N")
                    {
                        dicionarioCodigoAjusteExclusao.Add(codigoAjuste.Valor, codigoAjuste.Configuracao);
                        lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso); //1333                     
                        lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS - Grupo [{0}] - Configurado para não exportar este codigo de ajuste: [{1}-{2}] ", arquivoInfo.SiglaGrupo.ToString(), codigoAjuste.Valor.ToString(), codigoAjuste.NomeListaItem.ToString())); //1333
                        LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    }
                //==================================================================================================================
                // Cria a classe de calendário
                //==================================================================================================================
                Calendario calendario = new Calendario(request.CodigoSessao, DateTime.Now);
                //==================================================================================================================
                // Data atual a ser considerada no recalculo da data de repasse
                //==================================================================================================================
                DateTime dataAtual = DateTime.Now.Date;
                //==================================================================================================================
                // ECOMMERCE - AMANDA - 20151005
                // Mantem uma variável da quantidade de dias vencidos pois no componente está como 
                // string (não deveria, deveria estar como número)
                //==================================================================================================================
                int quantidadeDiasVencidos = int.Parse(configuracaoGeral.QuantidadeDiasVencidos);
                //==================================================================================================================
                // BUSCAR OS PRODUTOS POIS OS DIAS VENCIDOS SERAO CALCULADOS A PARTIR DO PRODUTO
                //==================================================================================================================
                List<ProdutoInfo> produtos = PersistenciaHelper.Listar<ProdutoInfo>(request.CodigoSessao);
                Dictionary<string, ProdutoInfo> dicionarioProduto = new Dictionary<string, ProdutoInfo>();
                foreach (ProdutoInfo produto in produtos)
                    dicionarioProduto.Add(produto.NomeProduto, produto);
                string nomeProdutoAjuste = null;
                string nomeProdutoAnulacaoVenda = null;
                string nomeProdutoAnulacaoPagamento = null;
                string nomeProdutoPagamento = null;
                string nomeProdutoVenda = null;
                //==================================================================================================================
                // pede a lista de referencias
                //==================================================================================================================
                List<ListaItemReferenciaInfo> listaItemReferencias = PersistenciaHelper.Listar<ListaItemReferenciaInfo>(request.CodigoSessao);
                //==================================================================================================================
                // monta o dicionario de referencias para o meio de pagamento
                //==================================================================================================================
                Dictionary<string, string> dicionarioReferenciaProdutoAV =
                    MontaDicionarioReferencia(
                        ListaItemReferenciaSituacaoEnum.CodigoAnulacao,
                        ListaItemReferenciaNomeEnum.Produto,
                        listaItemReferencias
                    );
                //==================================================================================================================
                // 1495 monta o dicionario de referencias para o meio de pagamento
                //==================================================================================================================
                Dictionary<string, string> dicionarioReferenciaProdutoAJ =
                    MontaDicionarioReferencia(
                        ListaItemReferenciaSituacaoEnum.CodigoAjuste,
                        ListaItemReferenciaNomeEnum.Produto,
                        listaItemReferencias
                    );
                //==================================================================================================================
                // Faz o cache dos produtos                
                //==================================================================================================================
                nomeProdutoAjuste = dicionarioProduto["RECTO"].NomeProduto;
                nomeProdutoAnulacaoPagamento = dicionarioProduto["RECTO"].NomeProduto;
                nomeProdutoPagamento = dicionarioProduto["RECTO"].NomeProduto;
                nomeProdutoVenda = null;
                //==================================================================================================================
                // ECOMMERCE - AMANDA - 20151005
                // Contador de linhas
                //==================================================================================================================
                int quantidadeLinhas = 0;
                int contadorL9 = 0;
                double valorTotal = 0;
                int quantidadeLog = 0;
                //==================================================================================================================
                // No arquivo, deve haver apenas 1 L0 e 1 L9, estes indicadores servem para fazer esta indicacao
                //==================================================================================================================
                bool jaIncluiuL0 = false;
                bool jaIncluiuA0 = false; //1429
                string registroL9 = "";
                string registroA9 = "";
                bool gerouA9 = false;
                bool gerouL9 = false;
                string registroA0 = "";
                string registroL0 = "";
                //bool primeiraLinha = true; // warning 25/02/2019
                //==================================================================================================================
                // Data do primeiro L0
                //==================================================================================================================
                string dataMovimentoL0 = "";
                //==================================================================================================================
                // Obtem o sequencial dos arquivos gerados para CCI
                //==================================================================================================================
                string siglaRede = arquivoInfo.SiglaGrupo;
                //==================================================================================================================
                // Agora, para escrever o A0, devemos ler o primeiro L0 para pegar a data de movimento
                // Indica se deve incluir esta linha
                //==================================================================================================================
                bool incluir = true;
                bool transacao = false;
                DateTime dataMovimento = DateTime.Now;
                //==================================================================================================================
                // 1333 Carregando LINHAS do ARQUIVO importado - 1a VEZ
                //==================================================================================================================
                string codigoArquivo = request.CodigoArquivo;
                string tipoArquivoItem = "L0"; //1333 estava null;
                bool pFiltrarSemCritica = true;
                bool pFiltrarNaoBloqueados = true;
                int maxLinhas = 0;
                int limiteMinimo = 0;
                int limiteMaximo = 0;
                bool pfiltrarStatusCancelado = true;
                bool filtrarSobAnalise = false;
                dr = Procedures.PR_ARQUIVO_ITEM_L(cn, codigoArquivo, tipoArquivoItem, pFiltrarSemCritica, pFiltrarNaoBloqueados, maxLinhas, limiteMinimo, limiteMaximo, pfiltrarStatusCancelado, filtrarSobAnalise).GetDataReader();
                //==================================================================================================================
                // INICIO Loop das linhas para tratar L0
                //==================================================================================================================
                while (dr.Read())
                {
                    //==================================================================================================================
                    // Pega a linha e o tipo da linha
                    //==================================================================================================================
                    string tipoLinha = dr.GetString(dr.GetOrdinal("TIPO_ARQUIVO_ITEM"));

                    if (tipoLinha == "L0")
                    {
                        string linha = dr.GetString(dr.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));

                        //==================================================================================================================
                        // Data do movimento (obtida através da linha L0, usando o padrão "L0YYYYMMDD...")
                        //==================================================================================================================
                        string dataMovimentoString = linha.Substring(2, 8);
                        dataMovimento = DateTime.ParseExact(dataMovimentoString, "yyyyMMdd", CultureInfo.CurrentCulture);
                        break;
                    }
                    // primeiraLinha = false; // warning 25/02/2019
                }
                dr.Close();
                //==================================================================================================================
                // FIM Loop das linhas
                //==================================================================================================================
                int sequencial = Procedures.PR_ARQUIVO_EXPORTACAO_GERAR(cn, siglaRede, dataMovimento);
                string dia = dataMovimento.Day.ToString().PadLeft(2, '0');
                string mes = dataMovimento.Month.ToString().PadLeft(2, '0');
                string ano = dataMovimento.Year.ToString().PadLeft(4, '0');
                //==================================================================================================================
                // Monto o nome do arquivo de saída
                //==================================================================================================================
                string nomeArquivo = (sequencial.ToString().PadLeft(6, '0')) + ".txt";
                nomeArquivo = siglaRede + ano + mes + dia + nomeArquivo;
                //==================================================================================================================
                // Verifico se o ultimo caracter é uma barra, se não for, coloco uma
                //==================================================================================================================
                if (request.CaminhoArquivo.Substring(request.CaminhoArquivo.Length - 1) != @"\")
                    nomeArquivo = @"\" + nomeArquivo;
                //==================================================================================================================
                // Abre o arquivo resultado
                //==================================================================================================================
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso); //1333                     
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS - Grupo [{0}] - Arquivo criado [{1}{2}] ", arquivoInfo.SiglaGrupo.ToString(), request.CaminhoArquivo.ToString(), nomeArquivo.ToString())); //1333
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                writer = File.CreateText(request.CaminhoArquivo + nomeArquivo); //1351
                //==================================================================================================================
                // 1333 Carregando LINHAS do ARQUIVO importado - 2a VEZ
                //==================================================================================================================
                tipoArquivoItem = "L0','A0"; //1333 estava null;
                dr = Procedures.PR_ARQUIVO_ITEM_L(cn, codigoArquivo, tipoArquivoItem, pFiltrarSemCritica, pFiltrarNaoBloqueados, maxLinhas, limiteMinimo, limiteMaximo, pfiltrarStatusCancelado, filtrarSobAnalise).GetDataReader();
                //dr = Procedures.PR_ARQUIVO_ITEM_L(cn, request.CodigoArquivo, null, true, true, 0, 0, 0, true, false).GetDataReader();
                //==================================================================================================================
                // INICIO Loop das linhas para tratar o A0 e L0                
                //==================================================================================================================
                while (dr.Read())
                {
                    // Pega a linha e o tipo da linha
                    string linha = dr.GetString(dr.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));
                    string tipoLinha = dr.GetString(dr.GetOrdinal("TIPO_ARQUIVO_ITEM"));

                    if (tipoLinha == "L0")
                    {
                        // Data do movimento (obtida através da linha L0, usando o padrão "L0YYYYMMDD...")
                        string dataMovimentoString = linha.Substring(2, 8);
                        dataMovimento = DateTime.ParseExact(dataMovimentoString, "yyyyMMdd", CultureInfo.CurrentCulture);

                        // Guarda o 1.o L0
                        registroL0 = linha;

                        if (registroA0 != "")
                        {
                            quantidadeLinhas++;
                            quantidadeLog++;
                            //// Corrige os dados do registro A0 e gera o A0
                            registroA0 = arquivoExtrato.EscreverCampo("A0", registroA0, "DataGeracaoArquivo", dataMovimentoString, false);
                            registroA0 = arquivoExtrato.EscreverCampo("A0", registroA0, "NSEQ", quantidadeLinhas, true);

                            // Corrige o sequencial
                            registroA0 = arquivoExtrato.EscreverCampo("A0", registroA0, "IdMovimento", sequencial);
                            writer.WriteLine(registroA0);
                            jaIncluiuA0 = true; //1429

                            quantidadeLinhas++;
                            quantidadeLog++;
                            registroL0 = arquivoExtrato.EscreverCampo(tipoLinha, linha, "NSEQ", quantidadeLinhas, true);
                            writer.WriteLine(registroL0);
                            jaIncluiuL0 = true;
                        }

                        break;
                    }

                    if (tipoLinha == "A0")
                    {
                        string layout = "001.6d";
                        if (layoutArquivo.Equals("5"))
                            layout = "001.6e";
                        linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "VersaoLayout", layout, true); // Corrige LAYOUT DA LINHA
                        registroA0 = linha;
                    }

                }
                dr.Close();

                if (!jaIncluiuA0) // 1429
                {
                    if (registroA0 != "")
                    {
                        quantidadeLinhas++;
                        quantidadeLog++;

                        registroA0 = arquivoExtrato.EscreverCampo("A0", registroA0, "NSEQ", quantidadeLinhas, true);// Corrige SEQUENCIAL DA LINHA
                        registroA0 = arquivoExtrato.EscreverCampo("A0", registroA0, "IdMovimento", sequencial); // Corrige o ID

                        writer.WriteLine(registroA0);
                        jaIncluiuA0 = true;
                    }
                }
                //==================================================================================================================
                // FIM Loop das linhas para tratar o A0 e L0                
                //==================================================================================================================
                //==================================================================================================================
                // 1333 Carregando LINHAS do ARQUIVO importado - 3a VEZ
                //==================================================================================================================
                tipoArquivoItem = null;
                dr = Procedures.PR_ARQUIVO_ITEM_L(cn, codigoArquivo, tipoArquivoItem, pFiltrarSemCritica, pFiltrarNaoBloqueados, maxLinhas, limiteMinimo, limiteMaximo, pfiltrarStatusCancelado, filtrarSobAnalise).GetDataReader();
                //==================================================================================================================
                // 1353 - LOG FIXO da não exportação de transãçoes CONTABEIS
                // A exemplo da homologação em 11/01/2017 do NSU HOST 100492477823 junto com outros
                // que não foram exportados e não havia indicação do motivo
                //==================================================================================================================
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS - Grupo [{0}] - Regra fixa - Não exportar transações do tipo CONTABIL", arquivoInfo.SiglaGrupo.ToString())); //1333
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                //==================================================================================================================
                // 1353 - LOG FIXO da não exportação de transãçoes com CANCELAMENTO ON LINE status 6
                // A exemplo da homologação em 11/01/2017 do NSU HOST 100492477823 junto com outros
                // que não foram exportados e não havia indicação do motivo
                //==================================================================================================================
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS - Grupo [{0}] - Regra fixa - Não exportar transações com status 6 - Cancelamento On Line", arquivoInfo.SiglaGrupo.ToString())); //1333
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                //==================================================================================================================
                // INICIO Loop das linhas para tratar TODAS
                //==================================================================================================================
                while (dr.Read())
                {
                    //==================================================================================================================
                    // Pega a linha e o tipo da linha
                    //==================================================================================================================
                    string linha = dr.GetString(dr.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));
                    string tipoLinha = dr.GetString(dr.GetOrdinal("TIPO_ARQUIVO_ITEM"));
                    bool tipoLinhaRP = linha.Substring(0, 2).Equals("RP"); //ecommerce
                    string codigoLinha = dr.GetInt64(dr.GetOrdinal("CODIGO_ARQUIVO_ITEM")).ToString();
                    string referencia = null;
                    string banco = "";
                    string agencia = "";
                    string conta = "";
                    string chave = "";
                    //==================================================================================================================
                    // Indica se deve incluir esta linha
                    //==================================================================================================================
                    incluir = true;
                    if (dicionarioTipoRegistroExclusao.ContainsKey(tipoLinha))
                        incluir = false;
                    else if (!tipoLinhaRP && (tipoLinha == "AV" || tipoLinha == "CV"))//ecommerce
                        incluir = !arquivoExtrato.VerficarFinanceiroContabil(tipoLinha, (object)linha).Equals("C");
                    //==================================================================================================================
                    // Zera valores
                    //==================================================================================================================
                    double valor = 0;
                    double valorParcela = 0;
                    //==================================================================================================================
                    // 1394 VARIAVEIS
                    //==================================================================================================================
                    DateTime vDataHoje = DateTime.Now.Date; //1442
                    DateTime vDataBase = DateTime.Now.Date;
                    DateTime vDataRepasse = DateTime.Now.Date;
                    DateTime vDataRepasseCalculada = DateTime.Now.Date;
                    int vDiasVencidosProduto = 0;
                    int vDiasVencidos = 0;

                    // melhorias-2017 - variavel que armazenará a data da transacao, para efetuar o calculo
                    // da data de repasse : Data da transacao + Qtde dias repasse por tipo de registro
                    DateTime? vDataTransacao = null;

                    //==================================================================================================================
                    // Deve incluir?
                    //==================================================================================================================
                    if (incluir)
                    {
                        //==================================================================================================================
                        // SE FOR TRANSACAO CV, CP, AP, AV ou AJ
                        //==================================================================================================================
                        if (tipoLinha != "A9" && tipoLinha != "L0" && tipoLinha != "L9" && tipoLinha != "A0")
                        {
                            referencia = dr.GetString(dr.GetOrdinal("REFERENCIA"));
                            if (tipoLinha != "AP") // REGISTRO AP NÃO TEM BANCO, AGENCIA e CONTA
                            {
                                if (arquivoInfo.SiglaGrupo.Equals("ATA"))
                                {
                                    chave = dr.GetString(dr.GetOrdinal("CHAVE"));
                                    if (chave.IndexOf(tipoLinha) > 0)
                                    {
                                        banco = arquivoExtrato.LerCampo(tipoLinha, linha, "Banco").ToString();
                                        agencia = arquivoExtrato.LerCampo(tipoLinha, linha, "Agencia").ToString();
                                        conta = arquivoExtrato.LerCampo(tipoLinha, linha, "Conta").ToString();
                                    }
                                    else
                                    {
                                        banco = chave.Substring(0, 3);
                                        banco = String.Join("", System.Text.RegularExpressions.Regex.Split(banco, @"[^\d]"));

                                        agencia = chave.Substring(3, 6);
                                        conta = chave.Substring(10, 11);
                                    }
                                }
                                else
                                {
                                    banco = arquivoExtrato.LerCampo(tipoLinha, linha, "Banco").ToString();
                                    agencia = arquivoExtrato.LerCampo(tipoLinha, linha, "Agencia").ToString();
                                    conta = arquivoExtrato.LerCampo(tipoLinha, linha, "Conta").ToString();
                                }
                            }
                            //==================================================================================================================
                            // 1394 - INICIO 
                            // CARREGANDO A DATA DE REPASSE DA TRANSACAO e DIAS VENCIDOS POR PRODUTO
                            // MELHORIAS-2017 - CARREGANDO A DATA DA TRANSACAO, POIS PARA O ATACADAO,
                            // A DATA DE REPASSE CALCULADA SERÁ: DATA DA TRANSACAO + QTDE DE DIAS POR TIPO DE REGISTRO 
                            // (E DEPOIS CALCULAR DIA UTIL)
                            //==================================================================================================================
                            string vNomeProdutoTransacao = null; //1495
                            if (tipoLinha == "CV")
                            {
                                vDataTransacao = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataTransacao", true);

                                vDataRepasse = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataRepasseCredito", true);
                                string _modalidade = (string)arquivoExtrato.LerCampo(tipoLinha, linha, "ModalidadeVenda", true);
                                string vProduto = (arquivoExtrato.LerCampo(tipoLinha, linha, "ProdutoCSF", true).ToString().TrimEnd());
                                switch (vProduto)
                                {
                                    case "RCAR":
                                        if (_modalidade == "1")
                                            nomeProdutoVenda = dicionarioProduto["COBAN"].NomeProduto;
                                        else
                                            nomeProdutoVenda = dicionarioProduto["RCAR"].NomeProduto;
                                        break;

                                    case "CRAF":
                                        if (_modalidade == "1")
                                            nomeProdutoVenda = dicionarioProduto["CRAF PULA CICLO"].NomeProduto;
                                        else
                                            nomeProdutoVenda = dicionarioProduto["CRAF"].NomeProduto;
                                        break;

                                    case "CRES":
                                        nomeProdutoVenda = dicionarioProduto["CRES"].NomeProduto;
                                        break;

                                    default:
                                        nomeProdutoVenda = dicionarioProduto["COBAN"].NomeProduto;
                                        break;
                                }
                                vDiasVencidosProduto = (dicionarioProduto[nomeProdutoVenda].DiasVencidos);
                                vNomeProdutoTransacao = nomeProdutoVenda; //1495

                            }
                            //==================================================================================================================
                            if (tipoLinha == "AV")
                            {
                                vDataTransacao = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataTransacao", true);

                                vDataRepasse = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataReembolsoEmissor", true);
                                string codigoAnulacaoAV = (string)arquivoExtrato.LerCampo(tipoLinha, linha, "CodigoAnulacao", true);
                                nomeProdutoAnulacaoVenda = dicionarioProduto[dicionarioReferenciaProdutoAV[codigoAnulacaoAV].ToString()].NomeProduto;
                                vDiasVencidosProduto = (dicionarioProduto[nomeProdutoAnulacaoVenda].DiasVencidos);
                                vNomeProdutoTransacao = nomeProdutoAnulacaoVenda; //1495
                            }
                            //==================================================================================================================
                            if (tipoLinha == "CP")
                            {
                                vDataTransacao = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataTransacao", true);

                                vDataRepasse = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataRepasseCredito", true);
                                vDiasVencidosProduto = (dicionarioProduto[nomeProdutoPagamento].DiasVencidos);
                                vNomeProdutoTransacao = nomeProdutoPagamento; //1495
                            }
                            //==================================================================================================================
                            if (tipoLinha == "AP")
                            {
                                vDataTransacao = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataTransacao", true);

                                vDataRepasse = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataReembolsoEstabelecimento", true);
                                vDiasVencidosProduto = (dicionarioProduto[nomeProdutoAnulacaoPagamento].DiasVencidos);
                                vNomeProdutoTransacao = nomeProdutoAnulacaoPagamento; //1495
                            }
                            //==================================================================================================================
                            if (tipoLinha == "AJ")
                            {
                                vDataTransacao = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataTransacao", true);

                                vDataRepasse = (DateTime)arquivoExtrato.LerCampo(tipoLinha, linha, "DataRepasse", true);
                                vDiasVencidosProduto = (dicionarioProduto[nomeProdutoAjuste].DiasVencidosAJ);
                                string codigoAjusteAJ = (string)arquivoExtrato.LerCampo(tipoLinha, linha, "CodigoAjuste", true); //1495
                                nomeProdutoAjuste = dicionarioProduto[dicionarioReferenciaProdutoAJ[codigoAjusteAJ].ToString()].NomeProduto; //1495
                                vNomeProdutoTransacao = nomeProdutoAjuste; //1495
                            }
                            //==================================================================================================================
                            // CARREGANDO OS DIAS VENCIDOS UNICO ( GERAL ou POR PRODUTO )
                            //==================================================================================================================
                            if (!arquivoInfo.SiglaGrupo.Equals("CCI"))
                                vDiasVencidos = quantidadeDiasVencidos;
                            else
                                vDiasVencidos = vDiasVencidosProduto;
                            //==================================================================================================================
                            // 1394 REDEFINIDA
                            //==================================================================================================================
                            //Gerando a Data Base
                            //SE GRUPO for “CARREFOUR” E  ( (PRODUTO for “RCAR” e MODALIDADE “0”)  ou ( PRODUTO for “CRAF”) )
                            //SIM:          
                            //A data base conterá a DATA DO DIA + QUANTIDADE DE DIAS DE VENCIMENTO por produto e tipo de registro
                            //[ DATA BASE  = Sysdate + Quantidade de dias VENCIDO por produto e tipo de registro ]
                            //NÃO:         
                            //A data base conterá = DATA DO DIA
                            //[ DATA BASE = Sysdate ]
                            //==================================================================================================================
                            //1442 vDataBase = DateTime.Now.Date;
                            if ((arquivoInfo.SiglaGrupo.Equals("CCI")) && (nomeProdutoVenda == "RCAR" || nomeProdutoVenda == "CRAF"))
                            {
                                vDataBase = vDataHoje.AddDays(vDiasVencidos); //1442 vDataBase = DateTime.Now.AddDays(vDiasVencidos);
                            }
                            else
                            {
                                vDataBase = vDataHoje; //1442
                            }
                            //==================================================================================================================
                            // 1394 REDEFINIDA
                            //==================================================================================================================
                            //Conferindo se a transação está vencida
                            //SE DATA REPASSE DA TRANSACAO for menor igual DATA BASE ( está vencida )
                            //  NÃO:
                            //  A data da agenda será a data de repasse, a transação está à vencer
                            //  [ DATA AGENDA = DATA REPASSE ]                             
                            //  SIM:        
                            //  A transação está vencida. 
                            //  Conferir o grupo e o produto para considerar a data base ou somar os dias vencidos
                            //  SE GRUPO for “CARREFOUR” E ( (PRODUTO for “RCAR” e MODALIDADE “0”)  ou ( PRODUTO for “CRAF”) )
                            //      SIM:          
                            //      A data de agenda será a data base 
                            //      [ DATA AGENDA =  DATA BASE ]                             
                            //      NÃO:         
                            //      SE GRUPO for “CARREFOUR”
                            //          SIM:    
                            //          A data de agenda será a data base somando dias vencidos por produto e tipo de registro
                            //          [ DATA AGENDA =  DATA BASE + dias VENCIDOS por produto e tipo registro ]                             
                            //          NÃO:   
                            //          A data de agenda será a data base somando dias vencidos do parâmetro geral
                            //          [ DATA AGENDA =  DATA BASE + dias VENCIDOS geral ]
                            //==================================================================================================================
                            if (vDataRepasse > vDataBase) /// A VENCER
                            {
                                vDataRepasseCalculada = vDataRepasse;
                            }
                            else
                            {
                                //1442 vDataRepasseCalculada = vDataBase.AddDays(vDiasVencidos);
                                if ((arquivoInfo.SiglaGrupo.Equals("CCI")) && (nomeProdutoVenda == "RCAR" || nomeProdutoVenda == "CRAF"))
                                {
                                    vDataRepasseCalculada = vDataBase;
                                }
                                else
                                {
                                    vDataRepasseCalculada = vDataHoje.AddDays(vDiasVencidos); //1442     ( os dias vencidos ja foi tratado acima para conter o GERAL ou por PRODUTO )
                                }
                            }

                            //1495
                            /*
                            //==================================================================================================================
                            // MELHORIAS-2017 - Se for ATACADAO, Calculando a data de repasse, que será:
                            // data da transacao + dias de repasse por tipo de registro e depois aplicar regra de dias uteis
                            //==================================================================================================================
                            int vDiasRepasse = 0;
                            if (arquivoInfo.SiglaGrupo.Equals("ATA"))
                            {
                                vDiasRepasse = dicDiasRepasseTipoReg[tipoLinha];
                                if (vDiasRepasse == 0 || nomeProdutoPagamento != "RECTO")
                                {
                                    vDataRepasseCalculada = vDataRepasse;
                                    vDiasRepasse = 0;
                                }
                                else if (vDataTransacao != null)
                                    vDataRepasseCalculada = (DateTime)vDataTransacao;
                            }
                            */
                            //==================================================================================================================
                            // 1495 redefinicao para atacadao
                            //==================================================================================================================
                            int vDiasRepasse = 0;
                            bool vdiasuteis = false;
                            if (arquivoInfo.SiglaGrupo.Equals("ATA"))
                            {
                                vDataRepasseCalculada = dataMovimento; //1495
                                vDiasRepasse = dicDiasRepasseTipoReg[tipoLinha];
                                if (vNomeProdutoTransacao == "RECTO")
                                    vdiasuteis = true;
                                else
                                    vdiasuteis = false;
                            }
                            //==================================================================================================================
                            // Obtendo a data útil da data de agenda
                            //==================================================================================================================
                            bool vconsiderarDataInformada = true;
                            bool vferiadoEhDiaUtil = false;
                            bool vfimDeSemanaEhDiaUtil = false;
                            vDataRepasseCalculada = calendario.ProximoDiaUtil(vDataRepasseCalculada, vDiasRepasse, vconsiderarDataInformada, vferiadoEhDiaUtil, vfimDeSemanaEhDiaUtil, vdiasuteis); 
                            //==================================================================================================================
                            // 1394 - FIM
                            //==================================================================================================================
                        }
                        switch (tipoLinha)
                        {
                            //==================================================================================================================
                            case "AV":
                                //==================================================================================================================
                                transacao = true;
                                if (tipoLinhaRP)
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "CodigoRegistro", "AV", true);
                                //if(layoutArquivo.Equals("5"))
                                //    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Referencia", referencia); //1394
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "DataReembolsoEmissor", vDataRepasseCalculada, true); //1394
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Banco", banco);
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Agencia", agencia);
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Conta", conta);
                                break;
                            //==================================================================================================================
                            case "AJ":
                                //==================================================================================================================
                                string codigoAjusteAJ = (string)arquivoExtrato.LerCampo(tipoLinha, linha, "CodigoAjuste", true);
                                if (dicionarioCodigoAjusteExclusao.ContainsKey(codigoAjusteAJ))
                                    incluir = false;
                                if (incluir)
                                {
                                    transacao = true;
                                    valor = double.Parse(arquivoExtrato.LerCampo(tipoLinha, linha, "ValorBruto").ToString());
                                    //if (layoutArquivo.Equals("5"))
                                    //	linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Referencia", referencia); // 1394
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "DataRepasse", vDataRepasseCalculada, true); //1394
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Banco", banco);
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Agencia", agencia);
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Conta", conta);
                                }
                                break;
                            //==================================================================================================================
                            case "AP":
                                //==================================================================================================================
                                string meioPagamentoAP = (string)arquivoExtrato.LerCampo(tipoLinha, linha, "FormaMeioPagamento", true);
                                if (dicionarioMeioPagamentoExclusao.ContainsKey(meioPagamentoAP))
                                    incluir = false;
                                if (incluir)
                                {
                                    transacao = true;
                                    valor = double.Parse(arquivoExtrato.LerCampo(tipoLinha, linha, "ValorBrutoPagamento").ToString());
                                    //if (layoutArquivo.Equals("5"))
                                    //	linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Referencia", referencia); //1394
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "DataReembolsoEstabelecimento", vDataRepasseCalculada, true);
                                }
                                break;
                            //==================================================================================================================
                            case "CV":
                                //==================================================================================================================
                                transacao = true;
                                valorParcela = double.Parse(arquivoExtrato.LerCampo(tipoLinha, linha, "ValorBrutoParcela").ToString());
                                valor = double.Parse(arquivoExtrato.LerCampo(tipoLinha, linha, "ValorBrutoVenda").ToString());
                                if (valorParcela > 0)
                                    valor = valorParcela;
                                //if (layoutArquivo.Equals("5"))
                                //    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Referencia", referencia); //1394
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "DataRepasseCredito", vDataRepasseCalculada, true); //1394
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Banco", banco);
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Agencia", agencia);
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Conta", conta);
                                break;
                            //==================================================================================================================
                            case "CP":
                                //==================================================================================================================
                                string meioPagamentoCP = (string)arquivoExtrato.LerCampo(tipoLinha, linha, "MeioPagamento", true);
                                if (dicionarioMeioPagamentoExclusao.ContainsKey(meioPagamentoCP))
                                    incluir = false;
                                if (incluir)
                                {
                                    transacao = true; //1351
                                    valor = (double)arquivoExtrato.LerCampo(tipoLinha, linha, "ValorTotalPagamento", true);
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "DataRepasseCredito", vDataRepasseCalculada, true); //1394
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Banco", banco);
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Agencia", agencia);
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Conta", conta);
                                }
                                break;
                            //==================================================================================================================
                            case "A9":
                                //==================================================================================================================
                                quantidadeLinhas++;
                                quantidadeLog++;
                                if (registroL9.Length != 37)
                                    registroL9 = " ".PadRight(37);
                                registroL9 = arquivoExtrato.EscreverCampo("L9", registroL9, "CodigoRegistro", "L9");
                                registroL9 = arquivoExtrato.EscreverCampo("L9", registroL9, "TotalRegistrosTransacao", contadorL9);
                                registroL9 = arquivoExtrato.EscreverCampo("L9", registroL9, "TotalValoresCredito", valorTotal);
                                registroL9 = arquivoExtrato.EscreverCampo("L9", registroL9, "NSEQ", quantidadeLinhas);
                                writer.WriteLine(registroL9);
                                gerouL9 = true;
                                registroA9 = linha;
                                break;
                            //==================================================================================================================
                            case "L0":
                                //==================================================================================================================
                                //1429 if (!jaIncluiuL0)
                                if ( (!jaIncluiuL0) && (!jaIncluiuA0) ) //1429
                                {
                                    quantidadeLinhas++;
                                    quantidadeLog++;
                                    dataMovimentoL0 = arquivoExtrato.LerCampo(tipoLinha, linha, "DataMovimento").ToString();
                                    registroA0 = arquivoExtrato.EscreverCampo("A0", registroA0, "DataGeracaoArquivo", dataMovimentoL0, false); //// Corrige os dados do registro A0 e gera o A0
                                    registroA0 = arquivoExtrato.EscreverCampo("A0", registroA0, "NSEQ", quantidadeLinhas); //// Corrige os dados do registro A0 e gera o A0
                                    registroA0 = arquivoExtrato.EscreverCampo("A0", registroA0, "IdMovimento", sequencial); // Corrige o sequencial
                                    writer.WriteLine(registroA0);
                                }
                                incluir = !jaIncluiuL0;
                                jaIncluiuL0 = true;
                                break;

                            case "L9":
                                registroL9 = linha;
                                incluir = false;
                                break;

                            case "A0":
                                registroA0 = linha;
                                incluir = false;
                                break;

                            default:
                                break;
                        }
                        if (incluir)
                        {
                            quantidadeLinhas++;
                            quantidadeLog++;
                            valorTotal = valorTotal + valor;
                            if (tipoLinha == "CV" || tipoLinha == "CP" || tipoLinha == "AJ" ||
                                tipoLinha == "AV" || tipoLinha == "AP")
                            {
                                contadorL9++;
                                //1417 - INICIO MOVENDO
                                if (layoutArquivo.Equals("5"))
                                    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Referencia", referencia); //1394 MOVENDO PARA LOCAL UNICO
                                //1417 - FIM MOVENDO
                            }
                            if (tipoLinha == "A9")
                            {
                                linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "TotalRegistros", quantidadeLinhas);
                                gerouA9 = true;
                                incluir = false;
                            }
                            linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "NSEQ", quantidadeLinhas);
                            //1417 - INICIO INIBINDO
                            //if (layoutArquivo.Equals("5"))
                            //    linha = arquivoExtrato.EscreverCampo(tipoLinha, linha, "Referencia", referencia); //1394 MOVENDO PARA LOCAL UNICO
                            //1417 - FIM INIBINDO
                            if (arquivoInfo.LayoutArquivo.Equals("1") || arquivoInfo.LayoutArquivo.Equals("3"))
                            {
                                ArquivoTSYS arquivoExtratoAJ = new ArquivoTSYS("2");
                                linha = arquivoExtratoAJ.InserirValor(tipoLinha, linha, "NumeroContaCliente", "00000000000000", false);
                            }
                            if (tipoLinha.Equals("CV") && (arquivoInfo.LayoutArquivo.Equals("2") || arquivoInfo.LayoutArquivo.Equals("3")))
                            {
                                linha = arquivoExtrato.RemoverValor(tipoLinha, linha, "NSUHostTransacaoReversao");
                                linha = arquivoExtrato.RemoverValor(tipoLinha, linha, "NSUHostTransacaoOriginal");
                                linha = arquivoExtrato.RemoverValor(tipoLinha, linha, "DataPreAutorizacao");
                            }
                            //==================================================================================================================
                            // Escreve a linha no arquivo TEXTO
                            //==================================================================================================================
                            writer.WriteLine(linha);
                            if (quantidadeLog++ > 15000)
                            {
                                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS - Grupo {0} - {1} transações exportadas", arquivoInfo.SiglaGrupo, quantidadeLinhas));
                                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                                quantidadeLog = 0;
                            }
                        }
                    }
                    //SCF 1701 - ID 29
                    //Tratamento de Status da Arquivo Item
                    string codigoArquivoItem = codigoLinha;
                    bool retornarRegistro = false;
                    ArquivoItemStatusEnum statusArquivoItem = (incluir == true ? ArquivoItemStatusEnum.EnviadoGrupo : ArquivoItemStatusEnum.NaoEnviadoPorRegra);
                    Procedures.PR_ARQUIVO_ITEM_S(cmArquivoItemS, codigoArquivoItem, long.Parse(codigoArquivo), null, tipoLinha, retornarRegistro, statusArquivoItem, null, null);

                    if (request.ProcessoManual)
                    {
                        //SCF1701 - ID 29
                        //Tratando Status da Transação
                        int stsArquivoItem = (int)statusArquivoItem;
                        int stsTransacao = (int)validaStatusTransacao(stsArquivoItem);

                        Procedures.PR_TRANSACAO_STATUS_S(cmTransacaoStatusS, codigoArquivoItem, stsTransacao);
                    }
                }
                //==================================================================================================================
                // se acabou de ler, ver se gerou o L9. Se nao gerou, gera agora 
                //==================================================================================================================
                if (!gerouL9)
                {
                    quantidadeLinhas++;
                    if (registroL9.Length != 37)
                        registroL9 = " ".PadRight(37);
                    registroL9 = arquivoExtrato.EscreverCampo("L9", registroL9, "CodigoRegistro", "L9");
                    registroL9 = arquivoExtrato.EscreverCampo("L9", registroL9, "TotalRegistrosTransacao", contadorL9);
                    registroL9 = arquivoExtrato.EscreverCampo("L9", registroL9, "TotalValoresCredito", valorTotal);
                    registroL9 = arquivoExtrato.EscreverCampo("L9", registroL9, "NSEQ", quantidadeLinhas);
                    writer.WriteLine(registroL9);
                }
                //==================================================================================================================
                // se acabou de ler, ver se gerou o A9. Se nao gerou, gera agora 
                //==================================================================================================================
                if (!gerouA9)
                {
                    registroA9 = " ".PadRight(20);
                    quantidadeLinhas++;
                    registroA9 = arquivoExtrato.EscreverCampo("A9", registroA9, "CodigoRegistro", "A9");
                    registroA9 = arquivoExtrato.EscreverCampo("A9", registroA9, "TotalRegistros", quantidadeLinhas);
                    registroA9 = arquivoExtrato.EscreverCampo("A9", registroA9, "NSEQ", quantidadeLinhas);
                    writer.WriteLine(registroA9);
                }
                writer.Close();
                dr.Close();
                cn.Close();
                //==================================================================================================================
                // FIM Loop das linhas para tratar TODAS
                //==================================================================================================================
                if (transacao) //1351
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS FIM - Grupo {0} - {1} transações exportadas", arquivoInfo.SiglaGrupo, (quantidadeLinhas - 4)));
                else
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("EXPORTACAO TSYS FIM - Grupo {0} - {1} linhas exportadas sem transacoes", arquivoInfo.SiglaGrupo, quantidadeLinhas));
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(request.CodigoProcesso);
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                //==================================================================================================================                
                // Pega detalhe do processo
                //==================================================================================================================               
                ProcessoInfo processoInfo = PersistenciaHelper.Receber<ProcessoInfo>(request.CodigoSessao, arquivoInfo.CodigoProcesso);
                //==================================================================================================================
                // Ajusta as informações de envio para o CCI
                //==================================================================================================================                
                if (processoInfo is ProcessoExtratoInfo)
                {
                    ((ProcessoExtratoInfo)processoInfo).DataEnvioCCI = DateTime.Now;
                    if (!String.IsNullOrEmpty(request.CodigoSessao))
                        ((ProcessoExtratoInfo)processoInfo).CodigoUsuarioEnvioCCI = SegurancaHelper.ReceberUsuario(request.CodigoSessao).CodigoUsuario;
                    else
                        ((ProcessoExtratoInfo)processoInfo).CodigoUsuarioEnvioCCI = configServico.UsuarioRobo;
                    PersistenciaHelper.Salvar<ProcessoInfo>(request.CodigoSessao, processoInfo);
                }
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
                if (dr != null)
                    dr.Close();
                if (cn != null)
                    cn.Close();
            }
            return resposta;
        }
        //=============================================================================================Z02

        //=============================================================================================AA02
        private string retiraAcentos(string strcomAcentos) //scf845
        {
            string strsemAcentos = strcomAcentos;
            strsemAcentos = Regex.Replace(strsemAcentos, "[áàâãª]", "a");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÁÀÂÃ]", "A");
            strsemAcentos = Regex.Replace(strsemAcentos, "[éèê]", "e");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÉÈÊ]", "e");
            strsemAcentos = Regex.Replace(strsemAcentos, "[íìî]", "i");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÍÌÎ]", "I");
            strsemAcentos = Regex.Replace(strsemAcentos, "[óòôõº]", "o");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÓÒÔÕ]", "O");
            strsemAcentos = Regex.Replace(strsemAcentos, "[úùû]", "u");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÚÙÛ]", "U");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ç]", "c");
            strsemAcentos = Regex.Replace(strsemAcentos, "[Ç]", "C");
            strsemAcentos = Regex.Replace(strsemAcentos, "[Ñ]", "N"); //scf948
            strsemAcentos = Regex.Replace(strsemAcentos, "[ñ]", "n"); //scf948
            strsemAcentos = Regex.Replace(strsemAcentos, "[£]", ""); //SCF1200
            strsemAcentos = Regex.Replace(strsemAcentos, "[§]", ""); //SCF1200
            return strsemAcentos;
        }
        //=============================================================================================AA02

        //=============================================================================================AB02
        public SimularArquivoRetornoCessaoResponse SimularArquivoRetornoCessao(SimularArquivoRetornoCessaoRequest request)
        {
            // Prepara resposta
            SimularArquivoRetornoCessaoResponse resposta = new SimularArquivoRetornoCessaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            OracleConnection cn = null;
            OracleDataReader dr = null;
            StreamWriter writer = null;
            try
            {
                // Pega configuracoes
                ServicoIntegracaoConfig config =
                    ConfigHelper.Receber<ServicoIntegracaoConfig>(request.CodigoSessao);
                if (config == null)
                    config = new ServicoIntegracaoConfig();

                // Prepara leitor arquivo Tsys
                ArquivoRetornoCessao arquivoRetorno = new ArquivoRetornoCessao();

                ArquivoTSYS arquivoTSYS = new ArquivoTSYS();


                // Solicita as linhas do arquivo informado
                cn = Procedures.ReceberConexao();
                dr = Procedures.PR_ARQUIVO_ITEM_L(
                        cn, request.CodigoArquivo, null, false, true, 0, 0, 0, true, false).GetDataReader();


                // Data de hoje
                DateTime hoje = DateTime.Now;
                string dia = hoje.Day.ToString().PadLeft(2, '0');
                string mes = hoje.Month.ToString().PadLeft(2, '0');
                string ano = hoje.Year.ToString().PadLeft(4, '0');
                // Monto o nome do arquivo de saída
                string nomeArquivo = "Simular" + ano + mes + dia + ".txt";

                // Verifico se o ultimo caracter é uma barra, se não for, coloco uma
                if (request.CaminhoArquivo.Substring(request.CaminhoArquivo.Length - 1) != @"\")
                {
                    nomeArquivo = @"\" + nomeArquivo;
                }

                // Abre o arquivo resultado
                writer = File.CreateText(request.CaminhoArquivo + nomeArquivo);

                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                condicoes.Add(
                    new CondicaoInfo(
                        "TipoArquivo", CondicaoTipoEnum.Igual, "ArquivoTSYS"));
                condicoes.Add(
                    new CondicaoInfo(
                        "EnviarCCI", CondicaoTipoEnum.Igual, false));


                // Buscar os tipos de registro que não devem ser enviado à CCI
                List<ConfiguracaoTipoRegistroInfo> configuracaoTipoRegistroInfos =
                    PersistenciaHelper.Listar<ConfiguracaoTipoRegistroInfo>(request.CodigoSessao, condicoes);

                Dictionary<string, int> dicionarioTipoRegistroExclusao = new Dictionary<string, int>();
                foreach (ConfiguracaoTipoRegistroInfo configuracaoTipoRegistro in configuracaoTipoRegistroInfos)
                    dicionarioTipoRegistroExclusao.Add(configuracaoTipoRegistro.TipoRegistro, int.Parse(configuracaoTipoRegistro.CodigoConfiguracaoTipoRegistro));

                // Contador de linhas
                int quantidadeLinhas = 0;
                // int contadorL9 = 0; // warning 25/02/2019
                // double valorTotal = 0; // warning 25/02/2019
                string produtoCSF = "";
                string registroL9 = "";

                // No arquivo, deve haver apenas 1 L0 e 1 L9, estes indicadores serve para fazer esta indicacao
                bool primeiraVez = true;


                // Loop das linhas
                while (dr.Read())
                {

                    // Pega a linha e o tipo da linha
                    string linha = dr.GetString(dr.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));
                    string tipoLinha = dr.GetString(dr.GetOrdinal("TIPO_ARQUIVO_ITEM"));
                    string codigoLinha = dr.GetInt64(dr.GetOrdinal("CODIGO_ARQUIVO_ITEM")).ToString();

                    // Indica se deve incluir esta linha
                    bool incluir = true;

                    if (dicionarioTipoRegistroExclusao.ContainsKey(tipoLinha))
                        incluir = false;


                    string valor = "";
                    string valorBruto = "";
                    string valorDesconto = "";
                    string estabelecimento = "";
                    string nsuHost = "";
                    string dataTransacao = "";
                    string numeroParcela = "";
                    string numeroTotalParcelas = "";
                    string plano = "";
                    string codigoAutorizacao = "";


                    // Deve incluir?
                    if (incluir)
                    {
                        // vai ver se tem critica para este item
                        // Trata por tipo de linha
                        switch (tipoLinha)
                        {
                            case "AJ":
                                valor = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorLiquido").ToString();
                                estabelecimento = arquivoTSYS.LerCampo(tipoLinha, linha, "IdentificacaoLoja").ToString();
                                estabelecimento = arquivoTSYS.LerCampo(tipoLinha, linha, "IdentificacaoLoja").ToString();
                                produtoCSF = "9999";
                                nsuHost = arquivoTSYS.LerCampo(tipoLinha, linha, "NSUHostTransacao").ToString();
                                dataTransacao = arquivoTSYS.LerCampo(tipoLinha, linha, "DataTransacao").ToString();
                                numeroParcela = "000";
                                numeroTotalParcelas = "000";
                                plano = "00000";
                                valorBruto = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorBruto").ToString();
                                valorDesconto = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorDescontoOuComissao").ToString();
                                incluir = false;


                                break;

                            case "AP":
                                valor = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorLiquidoPagamento").ToString();
                                estabelecimento = arquivoTSYS.LerCampo(tipoLinha, linha, "IdentificacaoLoja").ToString();
                                produtoCSF = "9999";
                                nsuHost = arquivoTSYS.LerCampo(tipoLinha, linha, "NSUHostTransacao").ToString();
                                dataTransacao = arquivoTSYS.LerCampo(tipoLinha, linha, "DataTransacao").ToString();
                                numeroParcela = "000";
                                numeroTotalParcelas = "000";
                                plano = "00000";
                                valorBruto = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorBrutoPagamento").ToString();
                                valorDesconto = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorDesconto").ToString();
                                codigoAutorizacao = arquivoTSYS.LerCampo(tipoLinha, linha, "CodigoAutorizacaoTransacaoOriginal").ToString();
                                incluir = false;
                                break;

                            case "CV":
                                valor = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorLiquidoVenda").ToString();
                                produtoCSF = arquivoTSYS.LerCampo(tipoLinha, linha, "ProdutoCSF").ToString().Trim();
                                estabelecimento = arquivoTSYS.LerCampo(tipoLinha, linha, "IdentificacaoLoja").ToString();
                                nsuHost = arquivoTSYS.LerCampo(tipoLinha, linha, "NSUHostTransacao").ToString();
                                dataTransacao = arquivoTSYS.LerCampo(tipoLinha, linha, "DataTransacao").ToString();
                                numeroParcela = arquivoTSYS.LerCampo(tipoLinha, linha, "NumeroParcela").ToString();
                                numeroTotalParcelas = arquivoTSYS.LerCampo(tipoLinha, linha, "NumeroTotalParcelas").ToString();
                                plano = arquivoTSYS.LerCampo(tipoLinha, linha, "CodigoPlano").ToString();
                                valorBruto = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorBrutoVenda").ToString();
                                valorDesconto = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorDesconto").ToString();
                                codigoAutorizacao = arquivoTSYS.LerCampo(tipoLinha, linha, "CodigoAutorizacao").ToString();

                                break;

                            case "AV":
                                valor = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorLiquidoVenda").ToString();
                                produtoCSF = "9999";
                                estabelecimento = arquivoTSYS.LerCampo(tipoLinha, linha, "IdentificacaoLoja").ToString();
                                nsuHost = arquivoTSYS.LerCampo(tipoLinha, linha, "NSUHostTransacao").ToString();
                                dataTransacao = arquivoTSYS.LerCampo(tipoLinha, linha, "DataTransacao").ToString();
                                numeroParcela = arquivoTSYS.LerCampo(tipoLinha, linha, "NumeroParcela").ToString();
                                numeroTotalParcelas = arquivoTSYS.LerCampo(tipoLinha, linha, "NumeroTotalParcelas").ToString();
                                numeroParcela = "000";
                                numeroTotalParcelas = "000";
                                plano = "00000";
                                valorBruto = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorBrutoVenda").ToString();
                                valorDesconto = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorDescontoOuComissao").ToString();
                                codigoAutorizacao = arquivoTSYS.LerCampo(tipoLinha, linha, "CodigoAutorizacaoTransacaoOriginal").ToString();

                                incluir = false;
                                break;

                            case "CP":
                                valor = arquivoTSYS.LerCampo(tipoLinha, linha, "ValorLiquidoPagamento").ToString();
                                estabelecimento = arquivoTSYS.LerCampo(tipoLinha, linha, "IdentificacaoLoja").ToString();
                                produtoCSF = "9999";
                                nsuHost = arquivoTSYS.LerCampo(tipoLinha, linha, "NSUHostTransacao").ToString();
                                dataTransacao = arquivoTSYS.LerCampo(tipoLinha, linha, "DataTransacao").ToString();
                                numeroParcela = "000";
                                numeroTotalParcelas = "000";
                                plano = "00000";
                                valorBruto = new String('0', 16);
                                valorDesconto = new String('0', 16);
                                codigoAutorizacao = arquivoTSYS.LerCampo(tipoLinha, linha, "CodigoAutorizacao").ToString();

                                incluir = false;

                                break;

                            case "A9":
                                incluir = false;
                                break;

                            case "A0":
                                incluir = false;
                                break;

                            case "L0":
                                incluir = false;
                                break;

                            case "L9":
                                incluir = false;
                                break;

                            default:
                                break;
                        }

                        if (incluir)
                        {
                            if (primeiraVez)
                            {
                                // gerar o header
                                registroL9 = new String(' ', 144);

                                // Atualiza a quantidade de linhas
                                quantidadeLinhas++;
                                registroL9 = arquivoRetorno.EscreverCampo("R0", registroL9, "CodigoRegistro", "R0");
                                registroL9 = arquivoRetorno.EscreverCampo("R0", registroL9, "DataProcessamento", DateTime.Now, true);
                                registroL9 = arquivoRetorno.EscreverCampo("R0", registroL9, "HoraProcessamento", "999999");
                                registroL9 = arquivoRetorno.EscreverCampo("R0", registroL9, "Filler", " ".PadRight(127));
                                quantidadeLinhas++;
                                writer.WriteLine(registroL9);
                                primeiraVez = false;
                            }

                            registroL9 = new String(' ', 179);

                            // Atualiza a quantidade de linhas
                            quantidadeLinhas++;
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "CodigoRegistro", "R1");
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "IdentificacaoLoja", estabelecimento);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "NSUHostTransacao", nsuHost);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "CodigoAutorizacao", codigoAutorizacao);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "DataTransacao", dataTransacao);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "NumeroTotalPercelas", numeroTotalParcelas);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "NumeroParcela", numeroParcela);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "ProdutoCSF", produtoCSF);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "CodigoPlano", plano);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "ValorBrutoVenda", valorBruto);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "ValorDesconto", valorDesconto);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "ValorLiquidoVenda", valor);
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "CNPJFavorecido", "999999999999999");
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "NomeFavorecido", "favorecido teste 99");
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "Banco", "999");
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "Agencia", "999999");
                            registroL9 = arquivoRetorno.EscreverCampo("R1", registroL9, "Conta", "99999999999");




                            // Escreve a linha
                            writer.WriteLine(registroL9);
                        }
                    }
                }



                // se acabou de ler, ver se gerou o L9. Se nao gerou, gera agora                
                quantidadeLinhas++;
                registroL9 = " ".PadRight(144);

                registroL9 = arquivoRetorno.EscreverCampo("R9", registroL9, "CodigoRegistro", "R9");
                registroL9 = arquivoRetorno.EscreverCampo("R9", registroL9, "TotalRegistros", quantidadeLinhas);
                registroL9 = arquivoRetorno.EscreverCampo("R9", registroL9, "TotalValorBruto", "00000000000000000");
                registroL9 = arquivoRetorno.EscreverCampo("R9", registroL9, "TotalValorDesconto", "00000000000000000");
                registroL9 = arquivoRetorno.EscreverCampo("R9", registroL9, "TotalValorLiquido", "00000000000000000");
                writer.WriteLine(registroL9);



                // Fecha o arquivo
                writer.Close();

                // Fecha conexao
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                // Fecha o arquivo
                if (writer != null)
                    writer.Close();
                // Fecha o cursor
                if (dr != null)
                    dr.Close();
                // Fecha a conexao
                if (cn != null)
                    cn.Close();
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AB02

        //=============================================================================================AC02
        public ExportarCSUSemAtacadaoResponse ExportarCSUSemAtacadao(ExportarCSUSemAtacadaoRequest request)
        {
            // Prepara resposta
            ExportarCSUSemAtacadaoResponse resposta = new ExportarCSUSemAtacadaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Pega configuracoes
                ServicoIntegracaoConfig config =
                    ConfigHelper.Receber<ServicoIntegracaoConfig>(request.CodigoSessao);
                if (config == null)
                    config = new ServicoIntegracaoConfig();

                // Pega a lista de estabelecimentos que devem ser ignorados
                List<EstabelecimentoInfo> estabelecimentos =
                    PersistenciaHelper.Listar<EstabelecimentoInfo>(
                        request.CodigoSessao, new List<CondicaoInfo>() 
                        { 
                            //new CondicaoInfo("CodigoCnpj", CondicaoTipoEnum.Igual, config.CodigoLojaAtacadaoTSYS) 
                            new CondicaoInfo("CodigoSubGrupo", CondicaoTipoEnum.Igual, "2") 
                        });

                // Monta dicionario com os códigos
                List<string> codigoEstabelecimentos =
                    (from e in estabelecimentos
                     select e.EstabelecimentoCSU).Distinct().ToList();

                // Prepara leitor CSU
                ArquivoCSU arquivoCSU = new ArquivoCSU();

                // Solicita as linhas do arquivo informado
                OracleConnection cn = Procedures.ReceberConexao();
                OracleDataReader dr =
                    Procedures.PR_ARQUIVO_ITEM_L(
                        cn, request.CodigoArquivo, null, 0, false).GetDataReader();

                // Abre o arquivo resultado
                StreamWriter writer = File.CreateText(request.CaminhoArquivo);

                // Contador de linhas
                int quantidadeLinhas = 0;

                // Loop das linhas
                while (dr.Read())
                {
                    // Pega a linha e o tipo da linha
                    string linha = dr.GetString(dr.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));
                    string tipoLinha = dr.GetString(dr.GetOrdinal("TIPO_ARQUIVO_ITEM"));

                    // Indica se deve incluir esta linha
                    bool incluir = false;

                    // Trata por tipo de linha
                    switch (tipoLinha)
                    {
                        case "0":
                            incluir = true;
                            break;
                        case "9":
                            linha = arquivoCSU.EscreverCampo(tipoLinha, linha, "TOT-LINHAS", quantidadeLinhas + 1, true);
                            incluir = true;
                            break;
                        default:
                            if (!codigoEstabelecimentos.Contains((string)arquivoCSU.LerCampo(tipoLinha, linha, "LOJA")))
                                incluir = true;
                            else
                                incluir = false;
                            break;
                    }

                    // Deve incluir?
                    if (incluir)
                    {
                        // Atualiza a quantidade de linhas
                        quantidadeLinhas++;

                        // Corrige o NSR do registro
                        linha = arquivoCSU.EscreverCampo(tipoLinha, linha, "NSR", quantidadeLinhas, true);

                        // Escreve a linha
                        writer.WriteLine(linha);
                    }
                }

                // Fecha o arquivo
                writer.Close();

                // Fecha conexao
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AC02

        //=============================================================================================AD02
        public ImportarEstabelecimentoResponse ImportarEstabelecimento(ImportarEstabelecimentoRequest request)
        {
            // Prepara a resposta
            ImportarEstabelecimentoResponse resposta = new ImportarEstabelecimentoResponse();
            resposta.PreparaResposta(request);

            // Lista de críticas
            List<CriticaInfo> criticas = new List<CriticaInfo>();

            // Declara o cursor para que possa ser corretamente finalizado
            OracleConnection cn = Procedures.ReceberConexao();

            // Leitor de arquivos
            StreamReader reader = null;

            // Bloco de controle
            try
            {

                // Abre o arquivo
                ArquivoEstabelecimento arquivo = new ArquivoEstabelecimento();

                // Recebe caminho do arquivo
                try
                {
                    reader = new StreamReader(
                    File.Open(@request.CaminhoArquivo, FileMode.Open));

                    // Cache de grupos, subgrupos e favorecidos
                    Dictionary<string, EmpresaGrupoInfo> cacheGrupos = new Dictionary<string, EmpresaGrupoInfo>();
                    Dictionary<string, EmpresaSubGrupoInfo> cacheSubGrupos = new Dictionary<string, EmpresaSubGrupoInfo>();
                    Dictionary<string, FavorecidoInfo> cacheFavorecidos = new Dictionary<string, FavorecidoInfo>();

                    // Varre arquivo lendo campos
                    int contador = 0;
                    while (!reader.EndOfStream)
                    {
                        try
                        {
                            // Lê e interpreta a linha
                            string linha = reader.ReadLine();
                            contador++;

                            // Obtem o favorecido
                            string cnpjFavorecido = arquivo.LerCampo("detalhe", linha, "Favorecido").ToString().Trim();
                            FavorecidoInfo favorecidoInfo = null;
                            if (cacheFavorecidos.ContainsKey(cnpjFavorecido))
                            {
                                favorecidoInfo = cacheFavorecidos[cnpjFavorecido];
                            }
                            else
                            {
                                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                                condicoes.Add(new CondicaoInfo("CnpjFavorecido", CondicaoTipoEnum.Igual, cnpjFavorecido));
                                List<FavorecidoInfo> favorecidos = PersistenciaHelper.Listar<FavorecidoInfo>(
                                        request.CodigoSessao, condicoes);
                                if (favorecidos != null && favorecidos.Count > 0)
                                {
                                    favorecidoInfo = favorecidos[0];
                                    // Adiciono no cache
                                    cacheFavorecidos.Add(cnpjFavorecido, favorecidoInfo);
                                }
                                // Se não achou o favorecido, critica
                                if (favorecidoInfo == null)
                                {
                                    criticas.Add(new CriticaInfo()
                                    {
                                        Status = CriticaStatusEnum.ErroNegocio,
                                        Descricao = "Linha: " + contador.ToString().PadLeft(4, '0') + ", Motivo: "
                                            + "Não existe favorecido com o CNPJ " + cnpjFavorecido
                                    }
                                    );
                                }
                            }


                            // Obtem grupo e subgrupo
                            string grupo = ((string)arquivo.LerCampo("detalhe", linha, "Grupo")).Trim();
                            string subGrupo = ((string)arquivo.LerCampo("detalhe", linha, "SubGrupo")).Trim();
                            EmpresaGrupoInfo grupoInfo = null;
                            EmpresaSubGrupoInfo subGrupoInfo = null;

                            // Primeiro verifico se já existe no cache
                            if (cacheGrupos.ContainsKey(grupo))
                            {
                                grupoInfo = cacheGrupos[grupo];
                            }
                            // Depois verifico se o subgrupo existe e corresponde ao grupo
                            if (grupoInfo != null && cacheSubGrupos.ContainsKey(subGrupo))
                            {
                                if (cacheSubGrupos[subGrupo].CodigoEmpresaGrupo == grupoInfo.CodigoEmpresaGrupo)
                                {
                                    subGrupoInfo = cacheSubGrupos[subGrupo];
                                }
                            }

                            // Verifica se já existe o grupo informado
                            if (grupoInfo == null)
                            {
                                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                                condicoes.Add(new CondicaoInfo("NomeEmpresaGrupo", CondicaoTipoEnum.Igual, grupo));
                                List<EmpresaGrupoInfo> grupos = PersistenciaHelper.Listar<EmpresaGrupoInfo>(
                                        request.CodigoSessao, condicoes);
                                if (grupos != null && grupos.Count > 0)
                                {
                                    grupoInfo = grupos[0];
                                }
                                // Se não achou, cria um novo grupo
                                if (grupoInfo == null)
                                {
                                    grupoInfo = new EmpresaGrupoInfo()
                                    {
                                        NomeEmpresaGrupo = grupo
                                    };
                                    grupoInfo = PersistenciaHelper.Salvar<EmpresaGrupoInfo>(
                                        request.CodigoSessao, grupoInfo);
                                }
                                // Adiciono no cache
                                if (grupoInfo != null)
                                {
                                    cacheGrupos.Add(grupo, grupoInfo);
                                }
                            }

                            // Verifica se já existe o subGrupo informado
                            if (subGrupoInfo == null)
                            {
                                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                                condicoes.Add(new CondicaoInfo("CodigoEmpresaGrupo", CondicaoTipoEnum.Igual, grupoInfo.CodigoEmpresaGrupo));
                                condicoes.Add(new CondicaoInfo("NomeEmpresaSubGrupo", CondicaoTipoEnum.Igual, subGrupo));
                                List<EmpresaSubGrupoInfo> subGrupos = PersistenciaHelper.Listar<EmpresaSubGrupoInfo>(
                                        request.CodigoSessao, condicoes);
                                if (subGrupos != null && subGrupos.Count > 0)
                                {
                                    subGrupoInfo = subGrupos[0];
                                }
                                // Se não achou, cria um novo subGrupo
                                if (subGrupoInfo == null)
                                {
                                    subGrupoInfo = new EmpresaSubGrupoInfo()
                                    {
                                        CodigoEmpresaGrupo = grupoInfo.CodigoEmpresaGrupo,
                                        NomeEmpresaSubGrupo = subGrupo
                                    };
                                    subGrupoInfo = PersistenciaHelper.Salvar<EmpresaSubGrupoInfo>(
                                        request.CodigoSessao, subGrupoInfo);
                                }
                                // Adiciono no cache
                                if (subGrupoInfo != null)
                                {
                                    cacheSubGrupos.Add(subGrupo, subGrupoInfo);
                                }
                            }

                            EstabelecimentoInfo estabelecimento = new EstabelecimentoInfo()
                            {
                                CNPJ = (string)arquivo.LerCampo("detalhe", linha, "CNPJ"),
                                RazaoSocial = ((string)arquivo.LerCampo("detalhe", linha, "RazaoSocial")).Trim(),
                                CodigoFavorecido = favorecidoInfo != null ? favorecidoInfo.CodigoFavorecido : null,
                                CodigoEmpresaGrupo = grupoInfo.CodigoEmpresaGrupo,
                                CodigoEmpresaSubGrupo = subGrupoInfo.CodigoEmpresaSubGrupo,
                                EstabelecimentoCSU = (string)arquivo.LerCampo("detalhe", linha, "Estabelecimento_CSU"),
                                EstabelecimentoSitef = (string)arquivo.LerCampo("detalhe", linha, "Estabelecimento_SITEF")
                            };

                            // Salva no banco
                            PersistenciaHelper.Salvar<EstabelecimentoInfo>(
                                request.CodigoSessao, estabelecimento);
                        }
                        catch
                        {
                            resposta.RepassarExcessao = false;
                            resposta.StatusResposta = MensagemResponseStatusEnum.OK;
                            resposta.ErroLayout = true;
                            return resposta;
                        }
                    }

                    reader.Close();
                }
                catch
                {
                    resposta.RepassarExcessao = false;
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    resposta.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = "O caminho não foi encontrado.",
                            DataCritica = DateTime.Now
                        });

                    //resposta.Erro = "O caminho não foi encontrado.";
                    return resposta;
                }
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            // Retorna
            resposta.Criticas = criticas;
            return resposta;
        }
        //=============================================================================================AD02

        //=============================================================================================AE02
        public ListarCriticaResponse ListarCritica(ListarCriticaRequest request) //1282
        {   ListarCriticaResponse resposta = new ListarCriticaResponse();
            resposta.PreparaResposta(request);

            try
            {
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                if (!string.IsNullOrEmpty(request.FiltroCodigoArquivo))
                    condicoes.Add(new CondicaoInfo("CodigoArquivo", CondicaoTipoEnum.Igual, request.FiltroCodigoArquivo));

                if (!string.IsNullOrEmpty(request.FiltroCodigoArquivoItem))
                    condicoes.Add(new CondicaoInfo("CodigoArquivoItem", CondicaoTipoEnum.Igual, request.FiltroCodigoArquivoItem));

                if (!string.IsNullOrEmpty(request.FiltroCodigoProcesso))
                    condicoes.Add(new CondicaoInfo("CodigoProcesso", CondicaoTipoEnum.Igual, request.FiltroCodigoProcesso));

                if (!string.IsNullOrEmpty(request.FiltroTipoCritica))
                    condicoes.Add(new CondicaoInfo("TipoCritica", CondicaoTipoEnum.Igual, request.FiltroTipoCritica));

                if (!string.IsNullOrEmpty(request.FiltroTipoLinha))
                    condicoes.Add(new CondicaoInfo("TipoLinha", CondicaoTipoEnum.Igual, request.FiltroTipoLinha));

                if (!string.IsNullOrEmpty(request.FiltroNomeCampo))
                    condicoes.Add(new CondicaoInfo("NomeCampo", CondicaoTipoEnum.Igual, request.FiltroNomeCampo));

                if (!string.IsNullOrEmpty(request.FiltroDescricaoCritica)) //1282
                    condicoes.Add(new CondicaoInfo("DescricaoCritica", CondicaoTipoEnum.Igual, request.FiltroDescricaoCritica));

                condicoes.Add(new CondicaoInfo("RetornaAgrupado", CondicaoTipoEnum.Igual, (request.RetornarAgrupado ? 1 : 0)));// 1282

                List<CriticaSCFResumoInfo> resultado = PersistenciaHelper.Listar<CriticaSCFResumoInfo>(
                        request.CodigoSessao, condicoes, request.MaxLinhas); //1282 PR_CRITICA_L

                //=============================================================================================
                // 1282 inibindo
                //=============================================================================================
                //if (request.RetornarAgrupado)
                //{
                //    var agrupados = from vTabDetail in resultado
                //                    where !string.IsNullOrEmpty(vTabDetail.NomeCampo)
                //                    group vTabDetail by new { vTabDetail.DescricaoCritica, vTabDetail.NomeCampo } into vTabGroup
                //                    select new { Campos = vTabGroup.Key, Dados = vTabGroup }; //1282 incluindo o qtd

                //    foreach (var item in agrupados)
                //    {
                //        resposta.ResultadoAgrupado.Add(new CriticaSCFResumoInfoAgrupado()
                //        {
                //            Criticas = item.Dados.Distinct(new DescricaoCriticaDistinct()).ToList<CriticaSCFResumoInfo>()
                //        ,
                //            Descricao = item.Campos.DescricaoCritica
                //        ,
                //            NomeCampo = item.Campos.NomeCampo
                //        ,
                //            QtdRegistros = item.Dados.Count()//1282
                //        });
                //    }
                //}
                //=============================================================================================
                // 1282 refazendo
                //=============================================================================================
                if (request.RetornarAgrupado)
                {
                    foreach (var item in resultado)
                    {
                        resposta.ResultadoAgrupado.Add(new CriticaSCFResumoInfoAgrupado()
                        {   Descricao = item.DescricaoCritica
                        ,   NomeCampo = item.NomeCampo
                        ,   TipoCritica = item.TipoCritica
                        ,   TipoLinha = item.TipoLinha
                        ,   QtdRegistros = Convert.ToInt32(item.CodigoCritica)
                        });
                    }
                }


                // Solicita a lista
                resposta.Resultado = resultado;

            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AE02

        //=============================================================================================AF02
        public ReceberCriticaResponse ReceberCritica(ReceberCriticaRequest request)
        {
            // Prepara resposta
            ReceberCriticaResponse resposta = new ReceberCriticaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Pede a lista para a persistencia
                resposta.CriticaInfo =
                    PersistenciaHelper.Receber<CriticaInfo>(
                        request.CodigoSessao, request.CodigoCritica);
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AF02

        //=============================================================================================AG02
        public ListarDescricaoCriticaResponse ListarDescricaoCritica(ListarDescricaoCriticaRequest request)
        {
            // Prepara resposta
            ListarDescricaoCriticaResponse resposta = new ListarDescricaoCriticaResponse();

            // Bloco de controle
            try
            {
                // Pede a lista
                resposta.Resultado =
                    Mensageria.Processar<ListarDescricaoTipoResponse>(
                        new ListarDescricaoTipoRequest()
                        {
                            IncluirNamespaces =
                                new List<string>() 
                                { 
                                    typeof(CriticaSCFResumoInfo).Namespace + "," + typeof(CriticaSCFResumoInfo).Assembly.FullName
                                }
                        }).Resultado;

                // Retira o que não é critica
                resposta.Resultado.RemoveAll(r => !r.NomeTipo.StartsWith("Critica"));
            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AG02

        //=============================================================================================AH02
        public MontarCriticaAgrupamentoResponse MontarCriticaAgrupamento(MontarCriticaAgrupamentoRequest request)
        {
            // Prepara resposta
            MontarCriticaAgrupamentoResponse resposta = new MontarCriticaAgrupamentoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // Filtro por arquivo
                if (request.CodigoArquivo != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoArquivo", CondicaoTipoEnum.Igual, request.CodigoArquivo));

                // Filtro por processo
                if (request.CodigoProcesso != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoProcesso", CondicaoTipoEnum.Igual, request.CodigoProcesso));

                // Pede lista 
                List<CriticaAgrupamentoInfo> agrupamentos =
                    PersistenciaHelper.Listar<CriticaAgrupamentoInfo>(
                        request.CodigoSessao, condicoes);

                // Deve transformar em árvore?
                if (request.RetornarArvore)
                {
                    // Indexa
                    var temp = from a in agrupamentos orderby a.Chave select new { a.Chave, a.ChavePai };
                    Dictionary<string, CriticaAgrupamentoInfo> dicionarioAgrupamentos =
                        agrupamentos.ToDictionary(i => i.Chave);

                    // Varre colocando os elementos nos locais corretos e adicionando na resposta os elementos da raiz
                    foreach (KeyValuePair<string, CriticaAgrupamentoInfo> itemAgrupamento in dicionarioAgrupamentos)
                        if (itemAgrupamento.Value.ChavePai != null)
                            dicionarioAgrupamentos[itemAgrupamento.Value.ChavePai].Filhos.Add(itemAgrupamento.Value);
                        else
                            resposta.Resultado.Add(itemAgrupamento.Value);
                }
                else
                {
                    // Retorna lista plana
                    resposta.Resultado = agrupamentos;
                }
            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AH02

        //=============================================================================================AI02
        public ImportarProdutoPlanoResponse ImportarProdutoPlano(ImportarProdutoPlanoRequest request)
        {
            log.Info("Inicianco importacao de arquivo produto/plano...");
            // Prepara a resposta
            ImportarProdutoPlanoResponse resposta = new ImportarProdutoPlanoResponse();
            resposta.PreparaResposta(request);

            // Declara o cursor para que possa ser corretamente finalizado
            OracleConnection cn = Procedures.ReceberConexao();

            // Leitor
            StreamReader reader = null;

            // Lista de críticas
            List<CriticaInfo> criticas = new List<CriticaInfo>();

            // Bloco de controle
            try
            {
                // Abre o arquivo
                ArquivoProdutoPlano arquivo = new ArquivoProdutoPlano();

                // Recebe caminho do arquivo
                try
                {
                    reader = new StreamReader(
                        File.Open(@request.CaminhoArquivo, FileMode.Open));

                    // Varre arquivo lendo campos
                    int contador = 0;
                    while (!reader.EndOfStream)
                    {
                        // Lê e interpreta a linha
                        contador++;
                        string linha = reader.ReadLine();

                        // Monta o produto
                        ProdutoInfo produto = new ProdutoInfo()
                        {
                            CodigoProdutoTSYS = ((string)arquivo.LerCampo("detalhe", linha, "COD_PRODUTO_TSYS")).Trim(),
                            NomeProduto = ((string)arquivo.LerCampo("detalhe", linha, "COD_PRODUTO_TSYS")).Trim(),
                            Cedivel = ((string)arquivo.LerCampo("detalhe", linha, "PRODUTO_CEDIVEL") == "S")
                        };
                        // Validação
                        if (produto.CodigoProdutoTSYS == null || produto.CodigoProdutoTSYS == "")
                        {
                            criticas.Add(new CriticaInfo()
                            {
                                Status = CriticaStatusEnum.ErroNegocio,
                                Descricao = "Linha: " + contador.ToString().PadLeft(4, '0') + ", Motivo: "
                                    + "Código TSYS do produto não informado. Produto/plano não importado!"
                            }
                            );
                        }
                        else
                        {
                            // Monta o plano
                            PlanoInfo plano = new PlanoInfo()
                            {
                                CodigoPlanoTSYS = ((string)arquivo.LerCampo("detalhe", linha, "COD_PLANO_TSYS")).Trim(),
                                NomePlano = ((string)arquivo.LerCampo("detalhe", linha, "NOME_PLANO")).Trim(),
                                DescricaoPlano = ((string)arquivo.LerCampo("detalhe", linha, "DESCRICAO_PLANO")).Trim(),
                                Comissao = (double)arquivo.LerCampo("detalhe", linha, "PERCENTUAL_COMISSAO", true),
                                QuantidadeParcelas = (int)arquivo.LerCampo("detalhe", linha, "NUMERO_PARCELAS", true),
                                Ativo = true
                            };
                            // Validação
                            if (plano.CodigoPlanoTSYS == null || plano.CodigoPlanoTSYS == ""
                                || plano.NomePlano == null || plano.NomePlano == "")
                            {
                                criticas.Add(new CriticaInfo()
                                {
                                    Status = CriticaStatusEnum.ErroNegocio,
                                    Descricao = "Linha: " + contador.ToString().PadLeft(4, '0') + ", Motivo: "
                                        + "Código TSYS e/ou nome do plano não informado(s). Produto/plano não importado!"
                                }
                                );
                            }
                            else
                            {
                                PlanoInfo planoSalvo = Mensageria.Processar<SalvarObjetoResponse<PlanoInfo>>(
                                    new SalvarObjetoRequest<PlanoInfo>()
                                    {
                                        CodigoSessao = request.CodigoSessao,
                                        Objeto = plano
                                    }).Objeto;

                                produto.CodigoPlanos.Add(planoSalvo.CodigoPlano);

                                Mensageria.Processar<SalvarObjetoResponse<ProdutoInfo>>(
                                    new SalvarProdutoDbRequest()
                                    {
                                        CodigoSessao = request.CodigoSessao,
                                        Objeto = produto,
                                        TipoSincronismo = SalvarObjetoTipoSincronismoEnum.Adicionar
                                    });
                            }
                        }
                    }
                }
                catch
                {
                    resposta.RepassarExcessao = false;
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    resposta.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = "O caminho não foi encontrado.",
                            DataCritica = DateTime.Now
                        });

                    //resposta.Erro = "O caminho não foi encontrado.";
                    return resposta;
                }

            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            // Retorna
            resposta.Criticas = criticas;
            return resposta;
        }
        //=============================================================================================AI02

        //=============================================================================================AJ02
        public ListarArquivoItemPorTipoLinhaResponse ListarArquivoItemPorTipoLinha(ListarArquivoItemPorTipoLinhaRequest request)
        {
            // Prepara resposta
            ListarArquivoItemPorTipoLinhaResponse resposta = new ListarArquivoItemPorTipoLinhaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {

                // Monta lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // Filtra por código de arquivo
                condicoes.Add(new CondicaoInfo(
                    "CodigoArquivo", CondicaoTipoEnum.Igual, request.CodigoArquivo));

                // Filtra por código de arquivo
                condicoes.Add(new CondicaoInfo(
                    "CodigoArquivoItem", CondicaoTipoEnum.Igual, request.CodigoArquivoItem));

                // Filtra por tipo de linha
                condicoes.Add(new CondicaoInfo(
                    "TipoArquivoItem", CondicaoTipoEnum.Igual, request.TipoLinha));

                // Filtra por tipo de critica
                condicoes.Add(new CondicaoInfo(
                    "TipoCritica", CondicaoTipoEnum.Igual, request.TipoCritica));

                // Filtra por nome de campo de crítica
                condicoes.Add(new CondicaoInfo(
                    "NomeCampo", CondicaoTipoEnum.Igual, request.NomeCampo));

                // Filtra por nome de campo de crítica
                condicoes.Add(new CondicaoInfo(
                    "FiltrarSemCritica", CondicaoTipoEnum.Igual, request.FiltrarSemCritica));

                // Filtra por nome de campo de crítica
                condicoes.Add(new CondicaoInfo(
                    "FiltrarComCritica", CondicaoTipoEnum.Igual, request.FiltrarComCritica));

                bool filtrarComCritica = false;
                if (request.FiltrarComCritica) // if (request.FiltrarComCritica != null) // warning 25/02/2019
                {
                    filtrarComCritica = request.FiltrarComCritica;
                }

                // Pede a lista de arquivoItem
                resposta.Resultado =
                    Mensageria.Processar<ConsultarObjetosResponse<ArquivoItemResumoInfo>>(
                        new ListarArquivoItemDbRequest()
                        {
                            FiltrarComCritica = filtrarComCritica,
                            Condicoes = condicoes,
                            IncluirConteudo = true,
                            MaxLinhas = 100
                        }).Resultado;

                // Se pediu valores...
                if (request.IncluirValores)
                {
                    // Pede detalhe do arquivo
                    ArquivoInfo arquivoInfo =
                        PersistenciaHelper.Receber<ArquivoInfo>(
                            request.CodigoSessao, request.CodigoArquivo);

                    // Cria o processador do arquivo
                    ArquivoBase processadorArquivo =
                        (ArquivoBase)
                            Activator.CreateInstance(
                                ResolutorTipos.Resolver(
                                    arquivoInfo.TipoArquivo));

                    // AMANDA - ECOMMERCE - 20151002
                    if (arquivoInfo.LayoutArquivo != null)
                    {
                        processadorArquivo =
                        (ArquivoBase)
                            Activator.CreateInstance(
                                ResolutorTipos.Resolver(
                                    arquivoInfo.TipoArquivo), arquivoInfo.LayoutArquivo);
                    }

                    // Insere cabecalho
                    resposta.Cabecalho = processadorArquivo.ReceberNomeCampos(request.TipoLinha);

                    // Varre os itens
                    foreach (ArquivoItemResumoInfo item in resposta.Resultado)
                    {
                        // Varre as colunas
                        item.Valores = new string[processadorArquivo.ReceberQuantidadeColunas(item.TipoArquivoItem)];

                        for (int i = 0; i < processadorArquivo.ReceberQuantidadeColunas(item.TipoArquivoItem); i++)
                            item.Valores[i] = (string)processadorArquivo.LerCampo(item.ConteudoArquivoItem, resposta.Cabecalho[i], false);

                        // Deve manter o conteudo?
                        if (!request.IncluirConteudo)
                            item.ConteudoArquivoItem = null;
                    }
                }
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AJ02

        //=============================================================================================AK02
        public ReceberCriticaAgrupamentoDetalheResponse ReceberCriticaAgrupamentoDetalhe(ReceberCriticaAgrupamentoDetalheRequest request)
        {
            // Prepara resposta
            ReceberCriticaAgrupamentoDetalheResponse resposta = new ReceberCriticaAgrupamentoDetalheResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Pede a lista para a persistencia
                resposta.CriticaAgrupamentoDetalheInfo =
                    Mensageria.Processar<ReceberObjetoResponse<CriticaAgrupamentoDetalheInfo>>(
                        new ReceberCriticaAgrupamentoDetalheDbRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            CodigoArquivo = request.CodigoArquivo,
                            TipoLinha = request.TipoLinha,
                            TipoCritica = request.TipoCritica,
                            NomeCampo = request.NomeCampo
                        }).Objeto;
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AK02

        //=============================================================================================AL02
        public SalvarArquivoItemDetalheResponse SalvarArquivoItemDetalhe(SalvarArquivoItemDetalheRequest request) //1282 analise
        {

            //=============================================================================================
            //1282 - bloqueando instruções não utilizadas - INICIO
            //=============================================================================================
            //Dictionary<string, BinReferenciaInfo> dicionarioBinRef = null;
            //string valorRefBin = null;

            //// Inicializa
            //dicionarioBinRef = new Dictionary<string, BinReferenciaInfo>();
            //List<BinReferenciaInfo> BinReferencias =
            //        PersistenciaHelper.Listar<BinReferenciaInfo>(null);
            //foreach (BinReferenciaInfo binReferencia in BinReferencias)
            //    if (binReferencia.CodigoBin != null)
            //    {
            //        if (!dicionarioBinRef.ContainsKey(binReferencia.CodigoBin))
            //            dicionarioBinRef.Add(binReferencia.CodigoBin, binReferencia);
            //    }
            //=============================================================================================
            //1282 - bloqueando instruções não utilizadas - FIM
            //=============================================================================================

            // Prepara resposta
            SalvarArquivoItemDetalheResponse resposta = new SalvarArquivoItemDetalheResponse();
            resposta.PreparaResposta(request);
            try
            {
                // Pede o detalhe deste arquivo item
                ArquivoItemInfo arquivoItemInfo = PersistenciaHelper.Receber<ArquivoItemInfo>(request.CodigoSessao, request.CodigoArquivoItem);

                // Pede o detalhe do arquivo
                ArquivoInfo arquivoInfo =PersistenciaHelper.Receber<ArquivoInfo>(request.CodigoSessao, arquivoItemInfo.CodigoArquivo);

                // Cria instancia do processador do arquivo
                ArquivoBase processadorArquivo =(ArquivoBase)Activator.CreateInstance(ResolutorTipos.Resolver(arquivoInfo.TipoArquivo));

                // Faz a alteracao dos valores solicitados
                for (int i = 0; i < request.NomeCampos.Length; i++) // 1282 ocorria erro neste ponto porque não estava sendo tratado o nome do campo
                    arquivoItemInfo.ConteudoArquivoItem = processadorArquivo.EscreverCampo
                    (   arquivoItemInfo.TipoArquivoItem
                    ,   arquivoItemInfo.ConteudoArquivoItem
                    ,   request.NomeCampos[i]
                    ,   request.Valores[i]
                    , false
                    );

                // Verifica se foi informado um status
                if (request.StatusArquivoItem != null)
                    arquivoItemInfo.StatusArquivoItem = (ArquivoItemStatusEnum)request.StatusArquivoItem;

                // Verifica se foi informado uma Referencia
                for (int i = 0; i < request.NomeCampos.Length; i++)
                    resposta.ArquivoItemInfo = PersistenciaHelper.Salvar<ArquivoItemInfo>(request.CodigoSessao, arquivoItemInfo);

            }
            catch (Exception ex)
            {   resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }
        //=============================================================================================AL02

        //=============================================================================================AM02
        public AlterarStatusArquivoItemResponse AlterarStatusArquivoItem(AlterarStatusArquivoItemRequest request)
        {
            // Prepara resposta
            AlterarStatusArquivoItemResponse resposta = new AlterarStatusArquivoItemResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Faz a chamada da procedure e coloca na resposta a quantidade de registros alterados
                resposta.QuantidadeRegistrosAlterados =
                    Procedures.PR_ARQUIVO_ITEM_STATUS_S(
                        request.FiltroCodigoArquivo,
                        request.FiltroCodigoArquivoItem,
                        request.FiltroTipoLinha,
                        request.FiltroTipoCritica,
                        request.FiltroNomeCampo,
                        request.FiltroStatusArquivoItem,
                        request.StatusArquivoItem);
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AM02

        //=============================================================================================AN02
        public ListarRelatorioTransacaoInconsistenteResponse ListarRelatorioTransacaoInconsistente(ListarRelatorioTransacaoInconsistenteRequest request)
        {
            // Prepara Resposta
            ListarRelatorioTransacaoInconsistenteResponse resposta = new ListarRelatorioTransacaoInconsistenteResponse();

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            // Abre conexao Oracle para consulta de críticas
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;

            // Abre conexao Oracle
            // Para consulta de estabelecimento grupos e subgrupos
            OracleConnection cn2 = null;
            OracleRefCursor cursor2 = null;

            // Cria um dicionario para armazenar os itens criados
            Dictionary<string, RelatorioTransacaoInfo> dicionarioTransacoes = new Dictionary<string, RelatorioTransacaoInfo>();
            int qtdreg = 0; //1463
            int qtdreglim = 0; //1463

            // Bloco de Controle
            try
            {
                // Data do movimento
                DateTime dataMovimento = new DateTime();

                // Cria instancia do tipo do arquivo
                ArquivoTSYS arquivoTSYS = new ArquivoTSYS();
                ArquivoRetornoCessao arquivoRetornoCessao = new ArquivoRetornoCessao(); //1463

                // Consulta as críticas dos arquivo_item
                cursor = Procedures.PR_REL_TRANS_INCONSISTENTE_L(cn, int.Parse(request.CodigoArquivo));
                OracleDataReader reader = cursor.GetDataReader();
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Retornando do banco de dados", request.CodigoSessao, request.CodigoProcessoRelatorio);

                //Faz a chamada para obter a linha L0 do arquivo para obter a Data do Movimento
                List<ArquivoItemResumoInfo> listaArquivoItem =
                    Mensageria.Processar<ListarArquivoItemPorTipoLinhaResponse>
                    (   new ListarArquivoItemPorTipoLinhaRequest()
                        {   CodigoSessao = request.CodigoSessao
                        ,   CodigoArquivo = request.CodigoArquivo
                        ,   TipoLinha = "L0"
                        }
                    ).Resultado;

                if (listaArquivoItem.Count > 0)
                {
                    // Data do movimento (obtida através da linha L0, usando o padrão "L0YYYYMMDD...")
                    string dataMovimentoString = listaArquivoItem[0].ConteudoArquivoItem.Substring(2, 8);
                    dataMovimento = DateTime.ParseExact(dataMovimentoString, "yyyyMMdd", CultureInfo.CurrentCulture);
                }
                else
                {   //1463 SE FOR RETORNO DE CESSAO
                    //Faz a chamada para obter a linha R0 do arquivo para obter a Data do Movimento
                    listaArquivoItem =
                        Mensageria.Processar<ListarArquivoItemPorTipoLinhaResponse>
                        (   new ListarArquivoItemPorTipoLinhaRequest()
                            {   CodigoSessao = request.CodigoSessao
                            ,   CodigoArquivo = request.CodigoArquivo
                            ,   TipoLinha = "R0"
                            }
                        ).Resultado;
                    if (listaArquivoItem.Count > 0)
                    {
                        // Data do Processamento obtida através da linha R0, usando o padrão "R0YYYYMMDD...")
                        string dataMovimentoString = listaArquivoItem[0].ConteudoArquivoItem.Substring(2, 8);
                        dataMovimento = DateTime.ParseExact(dataMovimentoString, "yyyyMMdd", CultureInfo.CurrentCulture);
                    }
                }
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Carregou data de movimento: " + dataMovimento, request.CodigoSessao, request.CodigoProcessoRelatorio); //1463

                // Varre as linhas
                while (reader.Read())
                {
                    qtdreg = qtdreg + 1; //1463
                    qtdreglim = qtdreg + 1; //1463
                    if ( qtdreglim > 1000) //1463
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(qtdreg + " já gravados no dicionario" + dataMovimento, request.CodigoSessao, request.CodigoProcessoRelatorio);
                        qtdreglim = 0;
                    }

                    // Flag para indicar se é registro novo
                    bool novoRegistro = false;

                    // Pega o tipo da linha
                    string tipoLinha = (string)reader["TIPO_ARQUIVO_ITEM"];

                    // Verifica se deve considerar este tipo de linha
                    if (!(  tipoLinha == "AV" ||
                            tipoLinha == "AP" ||
                            tipoLinha == "AJ" ||
                            tipoLinha == "CV" ||
                            tipoLinha == "CP" ||
                            tipoLinha == "R1"           //1463
                          )
                       )
                       continue;

                    // Pega informacoes da linha
                    string linha = (string)reader["CONTEUDO_ARQUIVO_ITEM"];
                    string codigoArquivoItem = reader["CODIGO_ARQUIVO_ITEM"].ToString();
                    string statusArquivoItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(reader["STATUS_ARQUIVO_ITEM"].ToString()).ToString(); //1463

                    // Se ainda não existe o item, cria
                    if (!dicionarioTransacoes.ContainsKey(codigoArquivoItem))
                    {
                        // Cria novo registro
                        dicionarioTransacoes.Add
                        (   codigoArquivoItem
                        ,   new RelatorioTransacaoInfo()
                            {   CodigoArquivoItem = codigoArquivoItem
                            ,   TipoTransacao = tipoLinha
                            }
                        );

                        // Informa que é registro novo
                        novoRegistro = true;
                    }

                    // Pega o item existente
                    RelatorioTransacaoInfo relatorioTransacaoInfo =  dicionarioTransacoes[codigoArquivoItem];

                    relatorioTransacaoInfo.DataMovimento = dataMovimento;
                    relatorioTransacaoInfo.StatusTransacao = statusArquivoItem; //1463
                    relatorioTransacaoInfo.CodigoProcesso = request.CodigoProcesso; //1463

                    // Se for um registro novo, preenche todas as informações
                    if (novoRegistro)
                    {
                        // Preenche de acordo com o tipo da linha
                        switch (tipoLinha)
                        {
                        //1463 - INICIO
                            case "R1":
                                relatorioTransacaoInfo.CodigoArquivoItem = codigoArquivoItem;
                                relatorioTransacaoInfo.TipoTransacao = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "CodigoRegistro");
                                relatorioTransacaoInfo.CodigoProduto = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "ProdutoCSF");
                                relatorioTransacaoInfo.CodigoPlano = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "CodigoPlano");
                                relatorioTransacaoInfo.NomeFavorecido = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "NomeFavorecido");
                                relatorioTransacaoInfo.CnpjFavorecido = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "CNPJFavorecido");
                                relatorioTransacaoInfo.CnpjEstabelecimento = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "IdentificacaoLoja");
                                relatorioTransacaoInfo.CodigoAutorizacao = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "CodigoAutorizacao");
                                relatorioTransacaoInfo.Agencia = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "Agencia");
                                relatorioTransacaoInfo.Banco = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "Banco");
                                relatorioTransacaoInfo.Conta = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "Conta");
                                relatorioTransacaoInfo.DataTransacao = new DateTime?(interpretarValor<DateTime>(arquivoRetornoCessao, linha, tipoLinha, "DataTransacao"));
                                relatorioTransacaoInfo.NsuHost = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "NSUHostTransacao");
                                relatorioTransacaoInfo.NumeroParcela = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "NumeroParcela");
                                relatorioTransacaoInfo.NumerParcelaTotal = interpretarValor<string>(arquivoRetornoCessao, linha, tipoLinha, "NumeroTotalPercelas");
                                relatorioTransacaoInfo.ValorBruto = interpretarValor<decimal>(arquivoRetornoCessao, linha, tipoLinha, "ValorBrutoVenda");
                                relatorioTransacaoInfo.ValorDesconto = interpretarValor<decimal>(arquivoRetornoCessao, linha, tipoLinha, "ValorDesconto");
                                relatorioTransacaoInfo.ValorLiquido = interpretarValor<decimal>(arquivoRetornoCessao, linha, tipoLinha, "ValorLiquidoVenda");

                                break;
                            //1463 - FIM

                            case "AV":
                                relatorioTransacaoInfo.CnpjEstabelecimento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "IdentificacaoLoja");
                                relatorioTransacaoInfo.CodigoAutorizacaoOriginal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CodigoAutorizacaoTransacaoOriginal");
                                relatorioTransacaoInfo.Agencia =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Agencia");
                                relatorioTransacaoInfo.Banco =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Banco");
                                relatorioTransacaoInfo.CodigoAnulacao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CodigoAnulacao");
                                relatorioTransacaoInfo.CodigoArquivoItem = codigoArquivoItem;
                                relatorioTransacaoInfo.Conta =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Conta");
                                relatorioTransacaoInfo.DataTransacao =
                                    new DateTime?(interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataTransacao"));
                                relatorioTransacaoInfo.DataTransacaoOriginal =
                                    new DateTime?(interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataTransacaoOriginal"));
                                relatorioTransacaoInfo.HoraTransacao =
                                    relatorioTransacaoInfo.DataTransacao +
                                    interpretarValor<TimeSpan>(arquivoTSYS, linha, tipoLinha, "HorarioTransacao");
                                relatorioTransacaoInfo.MotivoAnulacao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "DescricaoMotivoAnulacao");
                                relatorioTransacaoInfo.NsuHost =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUHostTransacao");
                                relatorioTransacaoInfo.NsuHostOriginal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUHostTransacaoOriginal");
                                relatorioTransacaoInfo.NsuTefOriginal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUTefTransacaoOriginal");
                                relatorioTransacaoInfo.NumeroCartao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroCartao");
                                relatorioTransacaoInfo.NumeroContaCliente =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroContaCliente");
                                relatorioTransacaoInfo.NumeroParcela =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroParcela");
                                relatorioTransacaoInfo.NumerParcelaTotal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroTotalParcelas");
                                relatorioTransacaoInfo.TipoLancamento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "TipoLancamento");
                                relatorioTransacaoInfo.ValorBruto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorBrutoVenda");
                                relatorioTransacaoInfo.ValorDesconto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorDescontoOuComissao");
                                relatorioTransacaoInfo.ValorLiquido =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorLiquidoVenda");
                                relatorioTransacaoInfo.ValorParcela =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorBrutoParcela");
                                relatorioTransacaoInfo.ValorParcelaDesconto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorDescontoParcela");
                                relatorioTransacaoInfo.ValorParcelaLiquido =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorLiquidoParcela");
                                break;

                            case "AP":

                                // Valor fixo 
                                relatorioTransacaoInfo.NomeProduto = "RECTO";

                                relatorioTransacaoInfo.CnpjEstabelecimento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "IdentificacaoLoja");
                                relatorioTransacaoInfo.CodigoAnulacao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CodigoAnulacao");
                                relatorioTransacaoInfo.CodigoArquivoItem = codigoArquivoItem;
                                relatorioTransacaoInfo.CodigoAutorizacaoOriginal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CodigoAutorizacaoTransacaoOriginal");
                                relatorioTransacaoInfo.DataReembolsoEstabelecimento =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataReembolsoEstabelecimento"));
                                relatorioTransacaoInfo.DataTransacaoOriginal =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataTransacaoOriginal"));
                                relatorioTransacaoInfo.DataTransacao =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataTransacao"));
                                relatorioTransacaoInfo.FormaMeioPagamento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "FormaMeioPagamento");
                                relatorioTransacaoInfo.HoraTransacao =
                                    relatorioTransacaoInfo.DataTransacao +
                                    interpretarValor<TimeSpan>(arquivoTSYS, linha, tipoLinha, "HorarioTransacao");
                                relatorioTransacaoInfo.MotivoAnulacao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "DescricaoMotivoAnulacao");
                                relatorioTransacaoInfo.NsuHost =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUHostTransacao");
                                relatorioTransacaoInfo.NsuHostOriginal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUHostTransacaoOriginal");
                                relatorioTransacaoInfo.NsuTefOriginal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUTefTransacaoOriginal");
                                relatorioTransacaoInfo.NumeroCartao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroCartao");
                                relatorioTransacaoInfo.NumeroContaCliente =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroContaCliente");
                                relatorioTransacaoInfo.TipoLancamento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "TipoLancamento");
                                relatorioTransacaoInfo.ValorBruto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorBrutoPagamento");
                                relatorioTransacaoInfo.ValorDesconto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorDesconto");
                                relatorioTransacaoInfo.ValorLiquido =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorLiquidoPagamento");
                                break;

                            case "AJ":
                                relatorioTransacaoInfo.CnpjEstabelecimento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "IdentificacaoLoja");
                                relatorioTransacaoInfo.Agencia =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Agencia");
                                relatorioTransacaoInfo.Banco =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Banco");
                                relatorioTransacaoInfo.Conta =
                                   interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Conta");
                                relatorioTransacaoInfo.CodigoArquivoItem = codigoArquivoItem;
                                relatorioTransacaoInfo.NsuHost =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUHostTransacao");
                                relatorioTransacaoInfo.NsuHostOriginal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUHostTransacaoOriginal");
                                relatorioTransacaoInfo.DataTransacaoOriginal =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataTransacaoOriginal"));
                                relatorioTransacaoInfo.DataTransacao =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataTransacao"));
                                relatorioTransacaoInfo.HoraTransacao =
                                    relatorioTransacaoInfo.DataTransacao +
                                    interpretarValor<TimeSpan>(arquivoTSYS, linha, tipoLinha, "HorarioTransacao");
                                relatorioTransacaoInfo.TipoLancamento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "TipoLancamento");
                                relatorioTransacaoInfo.DataRepasse =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataRepasse"));
                                relatorioTransacaoInfo.TipoAjuste =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "TipoAjuste");
                                relatorioTransacaoInfo.CodigoAjuste =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CodigoAjuste");
                                relatorioTransacaoInfo.MotivoAjuste =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "DescricaoMotivoAjuste");
                                relatorioTransacaoInfo.ValorBruto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorBruto");
                                relatorioTransacaoInfo.ValorDesconto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorDescontoOuComissao");
                                relatorioTransacaoInfo.ValorLiquido =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorLiquido");
                                relatorioTransacaoInfo.NumeroCartaoTransacaoOriginal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroCartaoTransacaoOriginal");
                                //SCF1114 - Marcos Matsuoka - inicio
                                relatorioTransacaoInfo.NumeroContaCliente =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroContaCliente");
                                //SCF1114 - Marcos Matsuoka - inicio
                                break;

                            case "CP":

                                // Valor fixo 
                                relatorioTransacaoInfo.NomeProduto = "RECTO";

                                relatorioTransacaoInfo.CnpjEstabelecimento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "IdentificacaoLoja");
                                relatorioTransacaoInfo.Agencia =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Agencia");
                                relatorioTransacaoInfo.Banco =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Banco");
                                relatorioTransacaoInfo.Conta =
                                   interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Conta");
                                relatorioTransacaoInfo.CodigoArquivoItem = codigoArquivoItem;
                                relatorioTransacaoInfo.NsuHost =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUHostTransacao");
                                relatorioTransacaoInfo.NsuTef =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUTEFTransacao");
                                relatorioTransacaoInfo.CodigoAutorizacao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CodigoAutorizacao");
                                relatorioTransacaoInfo.DataTransacao =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataTransacao"));
                                relatorioTransacaoInfo.HoraTransacao =
                                    relatorioTransacaoInfo.DataTransacao +
                                    interpretarValor<TimeSpan>(arquivoTSYS, linha, tipoLinha, "HorarioTransacao");
                                relatorioTransacaoInfo.TipoLancamento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "TipoLancamento");
                                relatorioTransacaoInfo.DataRepasse =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataRepasseCredito"));
                                relatorioTransacaoInfo.MeioCaptura =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "MeioCaptura");
                                relatorioTransacaoInfo.ValorBruto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorTotalPagamento");
                                relatorioTransacaoInfo.ValorDesconto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorDesconto");
                                relatorioTransacaoInfo.ValorLiquido =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorLiquidoPagamento");
                                relatorioTransacaoInfo.NumeroCartao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroCartao");
                                relatorioTransacaoInfo.MeioPagamentoSequencia =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "SequencialMeioPagamento");
                                relatorioTransacaoInfo.MeioPagamento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "MeioPagamento");
                                relatorioTransacaoInfo.MeioPagamentoValor =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorMeioPagamento");
                                relatorioTransacaoInfo.QuantidadeMeioPagamento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroTotalMeioPagamento");
                                relatorioTransacaoInfo.NumeroContaCliente =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroContaCliente");
                                break;
                            case "CV":
                                relatorioTransacaoInfo.CnpjEstabelecimento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "IdentificacaoLoja");
                                relatorioTransacaoInfo.Agencia =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Agencia");
                                relatorioTransacaoInfo.Banco =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Banco");
                                relatorioTransacaoInfo.CodigoArquivoItem = codigoArquivoItem;
                                relatorioTransacaoInfo.CodigoAutorizacao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CodigoAutorizacao");
                                relatorioTransacaoInfo.NomeProduto =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "ProdutoCSF");
                                relatorioTransacaoInfo.CodigoPlano =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CodigoPlano");
                                relatorioTransacaoInfo.Conta =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "Conta");
                                relatorioTransacaoInfo.CupomFiscal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "CupomFiscal");
                                relatorioTransacaoInfo.DataRepasse =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataRepasseCredito"));
                                relatorioTransacaoInfo.DataTransacao =
                                    new DateTime?(
                                        interpretarValor<DateTime>(arquivoTSYS, linha, tipoLinha, "DataTransacao"));
                                relatorioTransacaoInfo.HoraTransacao =
                                        relatorioTransacaoInfo.DataTransacao +
                                        interpretarValor<TimeSpan>(arquivoTSYS, linha, tipoLinha, "HorarioTransacao");
                                relatorioTransacaoInfo.MeioCaptura =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "MeioCaptura");
                                relatorioTransacaoInfo.Modalidade =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "ModalidadeVenda");
                                relatorioTransacaoInfo.NsuHost =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUHostTransacao");
                                relatorioTransacaoInfo.NsuTef =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NSUTEFTransacao");
                                relatorioTransacaoInfo.NumeroCartao =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroCartao");
                                relatorioTransacaoInfo.NumeroContaCliente =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroContaCliente");
                                relatorioTransacaoInfo.NumeroParcela =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroParcela");
                                relatorioTransacaoInfo.NumerParcelaTotal =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "NumeroTotalParcelas");
                                relatorioTransacaoInfo.TipoLancamento =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "TipoLancamento");
                                relatorioTransacaoInfo.TipoProduto =
                                    interpretarValor<string>(arquivoTSYS, linha, tipoLinha, "TipoProduto");
                                relatorioTransacaoInfo.ValorBruto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorBrutoVenda");
                                relatorioTransacaoInfo.ValorDesconto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorDesconto");
                                relatorioTransacaoInfo.ValorLiquido =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorLiquidoVenda");
                                relatorioTransacaoInfo.ValorParcela =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorBrutoParcela");
                                relatorioTransacaoInfo.ValorParcelaDesconto =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorDescontoParcela");
                                relatorioTransacaoInfo.ValorParcelaLiquido =
                                    interpretarValor<decimal>(arquivoTSYS, linha, tipoLinha, "ValorLiquidoParcela");
                                break;
                        }

                        // Corrige produto dos registros AJ
                        if (tipoLinha == "AJ" && relatorioTransacaoInfo.CodigoAjuste != null)
                        {
                            // Pega o valor do codigo lista item
                            ListarListaItemResponse listaItem =
                               Mensageria.Processar<ListarListaItemResponse>(
                                   new ListarListaItemRequest()
                                   {
                                       Valor = relatorioTransacaoInfo.CodigoAjuste,
                                       NomeLista = "CodigoAjuste"
                                   });


                            // Recebe o valor da referencia
                            if (listaItem.Resultado.Count > 0)
                            {
                                ListarListaItemReferenciaResponse listaItemReferencia =
                                Mensageria.Processar<ListarListaItemReferenciaResponse>(
                                    new ListarListaItemReferenciaRequest()
                                    {
                                        FiltroCodigoListaItem = listaItem.Resultado[0].CodigoListaItem,
                                        FiltroCodigoListaItemSituacao = "CodigoAjuste"
                                    });

                                if (listaItemReferencia.Resultado.Count > 0)
                                    relatorioTransacaoInfo.NomeProduto = listaItemReferencia.Resultado[0].ValorReferencia;
                            }

                            else
                                relatorioTransacaoInfo.NomeProduto = "Produto não cadastrado.";
                        }


                        // Corrige produto dos registros AV
                        if (tipoLinha == "AV" && relatorioTransacaoInfo.CodigoAnulacao != null)
                        {
                            // Pega o valor do codigo lista item
                            ListarListaItemResponse listaItem =
                               Mensageria.Processar<ListarListaItemResponse>(
                                   new ListarListaItemRequest()
                                   {
                                       Valor = relatorioTransacaoInfo.CodigoAnulacao,
                                       NomeLista = "CodigoAnulacao"
                                   });

                            // Recebe o valor da referencia
                            if (listaItem.Resultado.Count > 0)
                            {
                                ListarListaItemReferenciaResponse listaItemReferencia =
                                Mensageria.Processar<ListarListaItemReferenciaResponse>(
                                    new ListarListaItemReferenciaRequest()
                                    {
                                        FiltroCodigoListaItem = listaItem.Resultado[0].CodigoListaItem,
                                        FiltroCodigoListaItemSituacao = "CodigoAnulacao"
                                    });

                                if (listaItemReferencia.Resultado.Count > 0)
                                    relatorioTransacaoInfo.NomeProduto = listaItemReferencia.Resultado[0].ValorReferencia;
                            }

                            else
                                relatorioTransacaoInfo.NomeProduto = "Produto não cadastrado.";
                        }
                        // Consulta os estabelecimentos grupos e subgrupos dos arquivo_item
                        cn2 = Procedures.ReceberConexao();
                        cursor2 = Procedures.PR_REL_TRANS_INCONSISTENTE_B(cn2, relatorioTransacaoInfo.CnpjEstabelecimento.Trim());
                        OracleDataReader reader2 = cursor2.GetDataReader();

                        if (reader2.Read())
                        {
                            relatorioTransacaoInfo.CodigoEmpresaGrupo = reader2["CODIGO_EMPRESA_GRUPO"].ToString();
                            relatorioTransacaoInfo.CodigoEmpresaSubgrupo = reader2["CODIGO_EMPRESA_SUBGRUPO"].ToString();
                            relatorioTransacaoInfo.CodigoEstabelecimento = reader2["CODIGO_ESTABELECIMENTO"].ToString();
                            relatorioTransacaoInfo.NomeEmpresaGrupo = reader2["NOME_EMPRESA_GRUPO"].ToString();
                            relatorioTransacaoInfo.NomeEmpresaSubgrupo = reader2["NOME_EMPRESA_SUBGRUPO"].ToString();
                            relatorioTransacaoInfo.NomeEstabelecimento = reader2["RAZAO_SOCIAL"].ToString();
                            if (tipoLinha != "R1") //1463
                            {
                                relatorioTransacaoInfo.CodigoFavorecido = reader2["CODIGO_FAVORECIDO"].ToString();
                                relatorioTransacaoInfo.NomeFavorecido = reader2["NOME_FAVORECIDO"].ToString();
                                relatorioTransacaoInfo.CnpjFavorecido = reader2["CNPJ_FAVORECIDO"].ToString(); //1463
                            }
                        }

                        // Fecha cursor2
                        cursor2.Dispose();
                        cn2.Dispose();

                    }

                    // Insere a nova crítica
                    relatorioTransacaoInfo.Criticas.Add(
                        new CriticaSCFResumoInfo()
                        {
                            CodigoCritica = reader["CODIGO_CRITICA"].ToString(),
                            TipoCritica = reader["TIPO_CRITICA"].ToString(),
                            DescricaoCritica = reader["DESCRICAO_CRITICA"].ToString(),
                            DataCritica = new DateTime?((DateTime)reader["DATA_CRITICA"])
                        });
                }

                LogSCFProcessoEstagioEventoInfo.EfetuarLog(qtdreg + " total gravados no dicionario, retornando para o excel", request.CodigoSessao, request.CodigoProcessoRelatorio);
                // Informa a lista de transacoes na resposta
                resposta.Resultado = dicionarioTransacoes.Select(r => r.Value).ToList();
            }
            catch (Exception ex)
            {
                // Tratar erro
                resposta.ProcessarExcessao(ex);
                throw new Exception("Erro ao tentar listar ítens do relatório.", ex);
            }
            finally
            {
                // Finaliza o cursor
                if (cursor2 != null)
                    cursor2.Dispose();
                if (cursor != null)
                    cursor.Dispose();

                // Finaliza conexao oracle
                if (cn2 != null)
                    cn2.Dispose();
                if (cn != null)
                    cn.Dispose();
            }

            // Resposta
            return resposta;
        }
        //=============================================================================================AN02


        //=============================================================================================AO02
        private T interpretarValor<T>(ArquivoBase arquivo, string linha, string tipoLinha, string nomeCampo)
        {
            // Prepara o retorno
            T valor = default(T);

            // Bloco de controle
            try
            {
                // Tenta pegar o valor solicitado com o tipo informado
                valor =
                    (T)Convert.ChangeType(
                        arquivo.LerCampo(tipoLinha, linha, nomeCampo, true), typeof(T));
            }
            catch (Exception) // catch (Exception ex) // warning 25/02/2019
            {

            }

            // Retorna o valor
            return valor;
        }
        //=============================================================================================AO02

        //=============================================================================================AP02
        public ListarRelatorioTransacaoResponse ListarRelatorioTransacao(ListarRelatorioTransacaoRequest request)
        {
            // Prepara resposta
            ListarRelatorioTransacaoResponse resposta = new ListarRelatorioTransacaoResponse();
            resposta.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pela origem
                if (request.FiltroOrigem != null)
                    condicoes.Add(new CondicaoInfo(
                        "Origem", CondicaoTipoEnum.Igual, request.FiltroOrigem));

                //Permite ao usuário buscar pelo grupo
                if (request.FiltroEmpresaGrupo != null)
                    condicoes.Add(new CondicaoInfo(
                        "CodigoEmpresaGrupo", CondicaoTipoEnum.Igual, request.FiltroEmpresaGrupo));

                //Permite ao usuário buscar pela subgrupo
                if (request.FiltroEmpresaSubgrupo != null)
                    condicoes.Add(new CondicaoInfo(
                        "CodigoEmpresaSubgrupo", CondicaoTipoEnum.Igual, request.FiltroEmpresaSubgrupo));

                //Permite ao usuário buscar pela estabelecimento
                if (request.FiltroEstabelecimento != null)
                    condicoes.Add(new CondicaoInfo(
                        "CodigoEstabelecimento", CondicaoTipoEnum.Igual, request.FiltroEstabelecimento));

                //Permite ao usuário buscar pelo favorecido
                if (request.FiltroFavorecido != null)
                    condicoes.Add(new CondicaoInfo(
                        "CodigoFavorecido", CondicaoTipoEnum.Igual, request.FiltroFavorecido));

                //Permite ao usuário buscar pelo produto
                if (request.FiltroProduto != null && request.FiltroProduto != "")
                    condicoes.Add(new CondicaoInfo(
                        "CodigoProduto", CondicaoTipoEnum.Igual, request.FiltroProduto));

                //Permite ao usuário buscar pelo plano
                if (request.FiltroPlano != null)
                    condicoes.Add(new CondicaoInfo(
                        "CodigoPlano", CondicaoTipoEnum.Igual, request.FiltroPlano));

                //Permite ao usuário buscar pelo meio de captura
                if (request.FiltroMeioCaptura != null)
                    condicoes.Add(new CondicaoInfo(
                        "MeioCaptura", CondicaoTipoEnum.Igual, request.FiltroMeioCaptura));

                //Permite ao usuário buscar pelo tipo da transação
                if (request.FiltroTipoRegistro != null)
                    condicoes.Add(new CondicaoInfo(
                        "TipoTransacao", CondicaoTipoEnum.Igual, request.FiltroTipoRegistro));

                //Permite ao usuário buscar pelo número do pagamento
                if (request.FiltroNumeroPagamento != null)
                    condicoes.Add(new CondicaoInfo(
                        "NumeroPagamento", CondicaoTipoEnum.Igual, request.FiltroNumeroPagamento));

                //Permite ao usuário buscar pela Conta do Cliente 1438
                if (request.FiltroContaCliente != null)
                    condicoes.Add(new CondicaoInfo(
                        "ContaCliente", CondicaoTipoEnum.Igual, request.FiltroContaCliente));

                // DataInclusao inicial
                if (request.FiltroDataTransacaoDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataTransacao",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataTransacaoDe.Value));

                // DataInclusao final
                if (request.FiltroDataTransacaoAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataTransacao",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataTransacaoAte.Value + ts));

                // DataInclusao inicial
                if (request.FiltroDataTransacao.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataTransacao",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataTransacaoDe.Value));

                // DataInclusao inicial
                if (request.FiltroDataProcessamentoDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataProcessamento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataProcessamentoDe.Value));

                // DataInclusao final
                if (request.FiltroDataProcessamentoAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataProcessamento",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataProcessamentoAte.Value + ts));

                // DataInclusao inicial
                if (request.FiltroDataProcessamento.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataProcessamento",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataProcessamentoDe.Value));

                // DataMovimento inicial
                if (request.FiltroDataMovimentoDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataMovimento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataMovimentoDe.Value));

                // DataInclusao final
                if (request.FiltroDataMovimentoAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataMovimento",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataMovimentoAte.Value + ts));

                // DataInclusao inicial
                if (request.FiltroDataMovimento.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataMovimento",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataMovimentoDe.Value));


                // DataInclusao inicial
                if (request.FiltroDataVencimentoDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataVencimento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataVencimentoDe.Value));

                // DataInclusao final
                if (request.FiltroDataVencimentoAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataVencimento",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataVencimentoAte.Value + ts));

                // DataInclusao inicial
                if (request.FiltroDataVencimento.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataVencimento",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataVencimentoDe.Value));

                // DataInclusao inicial
                if (request.FiltroDataAgendamentoDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataAgendamento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataAgendamentoDe.Value));

                // DataInclusao final
                if (request.FiltroDataAgendamentoAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataAgendamento",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataAgendamentoAte.Value + ts));

                // DataInclusao inicial
                if (request.FiltroDataAgendamento.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataAgendamento",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataAgendamentoDe.Value));

                // DataInclusao inicial
                if (request.FiltroDataEnvioMateraDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataEnvioMatera",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataEnvioMateraDe.Value));

                // DataInclusao final
                if (request.FiltroDataEnvioMateraAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataEnvioMatera",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataEnvioMateraAte.Value + ts));

                // DataInclusao inicial
                if (request.FiltroDataEnvioMatera.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataEnvioMatera",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataEnvioMateraDe.Value));

                // DataInclusao inicial
                if (request.FiltroDataLiquidacaoDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataLiquidacao",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataLiquidacaoDe.Value));

                // DataInclusao final
                if (request.FiltroDataLiquidacaoAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataLiquidacao",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataLiquidacaoAte.Value + ts));

                // DataInclusao inicial
                if (request.FiltroDataLiquidacao.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataLiquidacao",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataLiquidacaoDe.Value));

                //Permite ao usuário buscar pelo status de retorno da CCI
                if (request.FiltroStatusRetorno != null)
                    condicoes.Add(new CondicaoInfo(
                        "StatusRetorno", CondicaoTipoEnum.Igual, request.FiltroStatusRetorno));

                //Permite ao usuário buscar pelo status do pagamento
                if (request.FiltroStatusPagamento != null)
                    condicoes.Add(new CondicaoInfo(
                        "StatusPagamento", CondicaoTipoEnum.Igual, request.FiltroStatusPagamento));

                //Permite ao usuário buscar pelo status da conciliacao
                if (request.FiltroStatusConciliacao != null)
                    condicoes.Add(new CondicaoInfo(
                        "StatusConciliacao", CondicaoTipoEnum.Igual, request.FiltroStatusConciliacao));

                //Permite ao usuário buscar pelo status da validação
                if (request.FiltroStatusValidacao != null)
                    condicoes.Add(new CondicaoInfo(
                        "StatusValidacao", CondicaoTipoEnum.Igual, request.FiltroStatusValidacao));

                // permite ao usuário buscar pelo status de transações canceladas on line
                // NR-033
                if (request.FiltroStatusCancelamentoOnLine != null)
                    condicoes.Add(new CondicaoInfo(
                        "StatusCancelamentoOnLine", CondicaoTipoEnum.Igual, request.FiltroStatusCancelamentoOnLine));

                // DataRetornoCessao inicial
                if (request.FiltroDataRetornoCessaoDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataRetornoCessaoDe",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataRetornoCessaoDe.Value));

                // DataInclusao final
                if (request.FiltroDataRetornoCessaoAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataRetornoCessaoAte",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataRetornoCessaoAte.Value + ts));

                // ECOMMERCE - Fernando Bove - 20160105
                //Permite ao usuário buscar pelo tipo financeiro ou contábil
                if (request.FiltroFinanceiroContabil != null)
                    condicoes.Add(new CondicaoInfo(
                        "FiltroFinanceiroContabil", CondicaoTipoEnum.Igual, request.FiltroFinanceiroContabil));

                // ECOMMERCE - Fernando Bove - 20160105
                //Permite ao usuário buscar pelo NSU Host da Transação
                if (request.FiltroNSUHost != null)
                    condicoes.Add(new CondicaoInfo(
                        "FiltroNSUHost", CondicaoTipoEnum.Igual, request.FiltroNSUHost));

                //ClockWork - Marcos Matsuoka
                if (request.FiltroReferencia != null)
                    condicoes.Add(new CondicaoInfo(
                        "FiltroReferencia", CondicaoTipoEnum.Igual, request.FiltroReferencia));


                //Permite ao usuário buscar pelo status da validação
                if (request.MaxLinhas != null)
                    condicoes.Add(new CondicaoInfo(
                        "maxLinhas", CondicaoTipoEnum.Igual, request.MaxLinhas));

                if (request.VerificarQuantidadeLinhas == true)
                {
                    ListarRelatorioTransacaoLinhasDbResponse res = new ListarRelatorioTransacaoLinhasDbResponse();
                    res.PreparaResposta(request);

                    try
                    {
                        res = (ListarRelatorioTransacaoLinhasDbResponse)Mensageria.Processar<ConsultarObjetosResponse<RelatorioTransacaoInfo>>(
                                new ListarRelatorioTransacaoLinhasDbRequest()
                                {
                                    CodigoSessao = request.CodigoSessao,
                                    VerificarQuantidadeLinhas = true,
                                    Condicoes = condicoes
                                });

                        resposta.QuantidadeLinhas = res.QuantidadeLinhas;
                        return resposta;
                    }
                    catch (Exception ex)
                    {
                        // Trata erro
                        resposta.ProcessarExcessao(ex);
                    }
                }

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<RelatorioTransacaoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        //=============================================================================================AP02

        #endregion


        //=============================================================================================AQ02
        public GerarRelatorioTransacaoInconsistenteExcelResponse GerarRelatorioTransacaoInconsistenteExcel(GerarRelatorioTransacaoInconsistenteExcelRequest request)
        {
            // prepara a resposta
            GerarRelatorioTransacaoInconsistenteExcelResponse resposta = new GerarRelatorioTransacaoInconsistenteExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioTransacaoInconsistenteExcelInfo processoInfo =
                    new ProcessoRelatorioTransacaoInconsistenteExcelInfo()
                    {
                        FiltroStatusValidacao = request.FiltroStatusValidacao,
                        CodigoArquivo = request.CodigoArquivo
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioTransacaoInconsistenteExcel(request);
            }

            // retorna a resposta
            return resposta;
        }
        //=============================================================================================AQ02

        //=============================================================================================AR02
        private GerarRelatorioTransacaoInconsistenteExcelResponse gerarRelatorioTransacaoInconsistenteExcel(GerarRelatorioTransacaoInconsistenteExcelRequest request)
        {
            // Prepara a resposta
            GerarRelatorioTransacaoInconsistenteExcelResponse resposta = new GerarRelatorioTransacaoInconsistenteExcelResponse();

            // Controle
            // Recebe as configurações dos parâmetros a o caminho onde deve salvar o arquivo
            // O caminho desse relatório é o mesmo caminho utilizado no relatório de transaçoes
            Resource.Carrefour.SCF.Contratos.Principal.Mensagens.ReceberConfiguracaoGeralResponse resConfig =
                Mensageria.Processar<Resource.Carrefour.SCF.Contratos.Principal.Mensagens.ReceberConfiguracaoGeralResponse>(
                    new Resource.Carrefour.SCF.Contratos.Principal.Mensagens.ReceberConfiguracaoGeralRequest()
                    {
                    });

            // Recebe informaçoes do arquivo
            ArquivoInfo arquivoInfo = PersistenciaHelper.Receber<ArquivoInfo>(request.CodigoSessao, request.CodigoArquivo);

            // Recebe informçaoes do processo do arquivo
            ProcessoInfo processoInfo = PersistenciaHelper.Receber<ProcessoInfo>(request.CodigoSessao, arquivoInfo.CodigoProcesso);

            // Pegando caminho configurado em parâmetros do sistema (O mesmo do relatorio de transações)
            if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao == "")
            {
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                resposta.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                resposta.DescricaoResposta = resposta.Erro;

                resposta.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = resposta.Erro,
                        DataCritica = DateTime.Now
                    });

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(resposta.Erro, request.CodigoSessao, request.CodigoProcesso);
                return resposta;
            }
            else
            {
                // Verificar se o ultimo caracter é uma barra, se não for, coloco uma
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao.Length - 1) == @"\")
                    resposta.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao;
                else
                    resposta.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao + "\\";
            }

            // pega o usuario para recuperar o nome e colocar no arquivo gerado
            UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);

            // Data para nomeação do arquivo
            DateTime dataAtual = DateTime.Now;

            string nome = "";
            if (usuarioInfo.Nome.Contains(" "))
                nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
            else
                nome = usuarioInfo.Nome;

            string caminho = resposta.CaminhoArquivoGerado
                //SCF1248+ @"\" + nome + @"_RelInconsistencia_"
                + nome + @"_RelInconsistencia_"
                + dataAtual.Year.ToString("0000") + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                + "_" + request.CodigoProcesso.ToString() + ".csv";

            // Cria arquivo de saída
            if (!System.IO.Directory.Exists(resposta.CaminhoArquivoGerado))
            {
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                resposta.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe [" + resposta.CaminhoArquivoGerado + "]";
                resposta.DescricaoResposta = resposta.Erro;

                resposta.Criticas.Add(
                new CriticaInfo()
                {
                    Descricao = resposta.Erro,
                    DataCritica = DateTime.Now
                });

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    resposta.Erro, request.CodigoSessao, request.CodigoProcesso);

                return resposta;
            }
            else
            {
                using (StreamWriter arquivoSaida = File.CreateText(caminho))
                {
                    try
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Inconsistencias do processo: " + arquivoInfo.CodigoProcesso + " do arquivo: " + arquivoInfo.CodigoArquivo, request.CodigoSessao, request.CodigoProcesso); // 1463
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Arquivo criado: " + caminho, request.CodigoSessao, request.CodigoProcesso);
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Realizando consulta dos dados no banco de dados", request.CodigoSessao, request.CodigoProcesso);

                        // Faz a busca e recebe uma lista de transações inconsistentes agrupadas por codigo de arquivo item
                        ListarRelatorioTransacaoInconsistenteResponse resultado =
                            Mensageria.Processar<ListarRelatorioTransacaoInconsistenteResponse>(
                                new ListarRelatorioTransacaoInconsistenteRequest()
                                {
                                    CodigoArquivo = request.CodigoArquivo
                                ,   CodigoProcesso = arquivoInfo.CodigoProcesso //1463
                                ,   CodigoProcessoRelatorio = request.CodigoProcesso //1463
                                });

                        // Monta o csv
                        // Cabeçalho
                        arquivoSaida.WriteLine(
                            "Origem;" +
                            "Data Processamento;" +
                            "Status Importacao;" +
                            "Grupo;" +
                            "Subgrupo;" +
                            "Estabelecimento;" +
                            "Identificacao da Loja;" +
                            "NSHU_HOST;" +
                            "Data Transacao;" +
                            "Hora;" +
                            "Status Transacao;" +
                            "Codigo Registro;" +
                            "NSU_TEF;" +
                            "Codigo Autorizacao;" +
                            "Tipo Lancamento;" +
                            "Data Vencimento;" +
                            "Tipo Produto;" +
                            "Meio_Captura;" +
                            "Valor_Bruto;" +
                            "Valor_Desconto;" +
                            "Valor_Liquido;" +
                            "Nro_Cartao;" +
                            "Nro_Parcela;" +
                            "Nro_Parcela_Total;" +
                            "Valor_Parcela;" +
                            "Valor_Parcela_Desconto;" +
                            "Valor_Parcela_Liquido;" +
                            "Banco;" +
                            "Agencia;" +
                            "Conta;" +
                            "Numero conta cliente;" +
                            "Produto;" +
                            "Plano;" +
                            "Cupom_Fiscal;" +
                            "Modalidade;" +
                            "NSU_HOST_Original;" +
                            "NSU_TEF_Original;" +
                            "Data Transacao Original;" +
                            "Codigo Autorizacao Original;" +
                            "Codigo Anulacao;" +
                            "Motivo Anulacao;" +
                            "Quantidade Total Meio Pagamento;" +
                            "Meio Pagamento;" +
                            "Forma Meio Pagamento;" +
                            "Meio Pagamento Sequencia;" +
                            "Meio Pagamento Valor;" +
                            "Tipo Ajuste;" +
                            "Codigo Ajuste;" +
                            "Motivo Ajuste;" +
                            "Data Geracao Agenda;" +
                            "Numero Pagamento;" +
                            "Codigo da Critica;" +
                            "Tipo da Critica;" +
                            "Descricao;" +
                            "Data Critica;"
                            );

                        // Separador utilizado para definir as colunas no excel
                        string separador = ";";

                        // Quantidade de linhas para log
                        int quantidadeLog = 0;

                        // Varrendo a lista recebida
                        foreach (RelatorioTransacaoInfo transacao in resultado.Resultado)
                        {
                            // Varrendo as criticas de cada arquivo item
                            foreach (CriticaSCFResumoInfo critica in transacao.Criticas)
                            {
                                if (resposta.QuantidadeLinhasGeradas == 0)
                                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Consulta dos inconsistentes finalizada", request.CodigoSessao, request.CodigoProcesso);//1463 estava vencimentos

                                // Tratamento da descrição da crítica
                                string crt = critica.DescricaoCritica;
                                string descricaoCritica;

                                string NumeroCartao = ""; //scf1276

                                if (crt.Contains(";"))
                                    descricaoCritica = crt.Split(';')[0];
                                else
                                    descricaoCritica = crt;

                                //===================================================================================================
                                //scf1276 - inicio
                                //===================================================================================================
                                if (transacao.NumeroCartao == null)
                                    NumeroCartao = transacao.NumeroCartaoTransacaoOriginal;
                                else
                                    NumeroCartao = transacao.NumeroCartao;
                                //===================================================================================================
                                //scf1276 - fim
                                //===================================================================================================

                                arquivoSaida.WriteLine(
                                    arquivoInfo.TipoArquivo + separador +
                                    //String.Format("{0:dd/MM/yyyy}", processoInfo.DataInclusao) + separador +
                                    String.Format("{0:dd/MM/yyyy}", transacao.DataMovimento) + separador +
                                    processoInfo.StatusProcesso + separador +
                                    transacao.NomeEmpresaGrupo + separador +
                                    transacao.NomeEmpresaSubgrupo + separador +
                                    transacao.NomeEstabelecimento + separador +
                                    "=\"" + transacao.CnpjEstabelecimento + "\";" +
                                    transacao.NsuHost + separador +
                                    transacao.DataTransacao.Value.ToString("dd/MM/yyyy hh:mm:ss").Substring(0, 10) + separador +
                                    transacao.DataTransacao.Value.ToString("dd/MM/yyyy hh:mm:ss").Substring(11, 8) + separador +
                                    transacao.StatusTransacao + separador +
                                    transacao.TipoTransacao + separador +
                                    transacao.NsuTef + separador +
                                    transacao.CodigoAutorizacao + separador +
                                    transacao.TipoLancamento + separador +
                                    String.Format("{0:dd/MM/yyyy}", transacao.DataRepasse) + separador +
                                    transacao.TipoProduto + separador +
                                    transacao.MeioCaptura + separador +
                                    String.Format(new CultureInfo("PT-BR"), "{0:N2}", transacao.ValorBruto) + separador +
                                    String.Format(new CultureInfo("PT-BR"), "{0:N2}", transacao.ValorDesconto) + separador +
                                    String.Format(new CultureInfo("PT-BR"), "{0:N2}", transacao.ValorLiquido) + separador +
                                    //===================================================================================================
                                    //scf1276 - inicio
                                    //===================================================================================================
                                    NumeroCartao + separador +
                                    //===================================================================================================
                                    //scf1276 - inicio
                                    //===================================================================================================
                                    transacao.NumeroParcela + separador +
                                    transacao.NumerParcelaTotal + separador +
                                    String.Format(new CultureInfo("PT-BR"), "{0:N2}", transacao.ValorParcela) + separador +
                                    String.Format(new CultureInfo("PT-BR"), "{0:N2}", transacao.ValorParcelaDesconto) + separador +
                                    String.Format(new CultureInfo("PT-BR"), "{0:N2}", transacao.ValorParcelaLiquido) + separador +
                                    transacao.Banco + separador +
                                    transacao.Agencia + separador +
                                    transacao.Conta + separador +
                                    transacao.NumeroContaCliente + separador +
                                    transacao.NomeProduto + separador +
                                    transacao.CodigoPlano + separador +
                                    transacao.CupomFiscal + separador +
                                    transacao.Modalidade + separador +
                                    transacao.NsuHostOriginal + separador +
                                    transacao.NsuTefOriginal + separador +
                                    String.Format("{0:dd/MM/yyyy}", transacao.DataTransacaoOriginal) + separador +
                                    transacao.CodigoAutorizacaoOriginal + separador +
                                    transacao.CodigoAnulacao + separador +
                                    transacao.MotivoAnulacao + separador +
                                    transacao.QuantidadeMeioPagamento + separador +
                                    transacao.MeioPagamento + separador +
                                    transacao.FormaMeioPagamento + separador +
                                    transacao.MeioPagamentoSequencia + separador +
                                    String.Format(new CultureInfo("PT-BR"), "{0:N2}", transacao.MeioPagamentoValor) + separador +
                                    transacao.TipoAjuste + separador +
                                    transacao.CodigoAjuste + separador +
                                    transacao.MotivoAjuste + separador +
                                    transacao.DataGeracaoAgenda + separador +
                                    transacao.NumeroPagamento + separador +
                                    critica.CodigoCritica + separador +
                                    critica.TipoCritica + separador +
                                    descricaoCritica + separador +
                                    String.Format("{0:dd/MM/yyyy}", critica.DataCritica)
                                  );

                                resposta.QuantidadeLinhasGeradas++;
                                quantidadeLog++;

                                // Sinaliza
                                if (quantidadeLog >= 15000)
                                {
                                    // Sinaliza
                                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                                        resposta.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                                    // Zera linhas do log
                                    quantidadeLog = 0;
                                }
                            }
                        }

                        // Informa total de linhas
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                            "Total de linhas geradas: " + resposta.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);
                    }
                    catch (Exception ex1)
                    {
                        // Tratar erro na busca da lista
                        resConfig.ProcessarExcessao(ex1);
                        throw new Exception("Erro ao tentar gerar o relatório.", ex1);
                    }
                }
            }

            // Retorno vazio
            return resposta;
        }
        //=============================================================================================AR02

        //=============================================================================================1463
        public GerarRelatorioInconsistenteAgrupadoExcelResponse GerarRelatorioInconsistenteAgrupadoExcel(GerarRelatorioInconsistenteAgrupadoExcelRequest request)
        {
            // prepara a resposta
            GerarRelatorioInconsistenteAgrupadoExcelResponse resposta = new GerarRelatorioInconsistenteAgrupadoExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioInconsistenteAgrupadoExcelInfo processoInfo =
                    new ProcessoRelatorioInconsistenteAgrupadoExcelInfo()
                    {   CodigoArquivo = request.CodigoArquivo
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioInconsistenteAgrupadoExcel(request);
            }

            // retorna a resposta
            return resposta;
        }

        private GerarRelatorioInconsistenteAgrupadoExcelResponse gerarRelatorioInconsistenteAgrupadoExcel(GerarRelatorioInconsistenteAgrupadoExcelRequest request)
        {
            // ======================================================================
            // Carrega informações do resposta, arquivo, processo e usuario, data atual
            // ======================================================================
            GerarRelatorioInconsistenteAgrupadoExcelResponse resposta = new GerarRelatorioInconsistenteAgrupadoExcelResponse();
            ArquivoInfo arquivoInfo = PersistenciaHelper.Receber<ArquivoInfo>(request.CodigoSessao, request.CodigoArquivo);
            ProcessoInfo processoInfo = PersistenciaHelper.Receber<ProcessoInfo>(request.CodigoSessao, arquivoInfo.CodigoProcesso);
            UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
            DateTime dataAtual = DateTime.Now;
            Boolean vOK = true;
            string nome = "";
            if (usuarioInfo.Nome.Contains(" "))
                nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
            else
                nome = usuarioInfo.Nome;
            // ======================================================================
            // Valida diretorio
            // ======================================================================
            Resource.Carrefour.SCF.Contratos.Principal.Mensagens.ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<Resource.Carrefour.SCF.Contratos.Principal.Mensagens.ReceberConfiguracaoGeralResponse>(new Resource.Carrefour.SCF.Contratos.Principal.Mensagens.ReceberConfiguracaoGeralRequest() { });
            if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao == "")
            {
                vOK = false;
                resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                resposta.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                resposta.DescricaoResposta = resposta.Erro;
                resposta.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = resposta.Erro,
                        DataCritica = DateTime.Now
                    });
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(resposta.Erro, request.CodigoSessao, request.CodigoProcesso);
                return resposta;
            }
            else
            {
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao.Length - 1) == @"\")
                    resposta.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao;
                else
                    resposta.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao + "\\";
                // ======================================================================
                // Gera o caminho do arquivo e valida se existe
                // ======================================================================
                if (!System.IO.Directory.Exists(resposta.CaminhoArquivoGerado))
                {
                    vOK = false;
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    resposta.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + resposta.CaminhoArquivoGerado + "]";
                    resposta.DescricaoResposta = resposta.Erro;
                    resposta.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = resposta.Erro,
                        DataCritica = DateTime.Now
                    });
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(resposta.Erro, request.CodigoSessao, request.CodigoProcesso);
                    return resposta;
                }
            }
            // ======================================================================
            // TUDO OK
            // ======================================================================
            if (vOK)
            {
                string caminho =
                resposta.CaminhoArquivoGerado
                + nome + @"_RelInconsistenciaAgrupado_"
                + dataAtual.Year.ToString("0000") + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                + "_" + request.CodigoProcesso.ToString() + ".csv";

                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Inconsistencias agrupadas do processo: " + arquivoInfo.CodigoProcesso + " do arquivo: " + arquivoInfo.CodigoArquivo, request.CodigoSessao, request.CodigoProcesso); // 1463
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Arquivo criado: " + caminho, request.CodigoSessao, request.CodigoProcesso);
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Realizando consulta dos dados no banco de dados", request.CodigoSessao, request.CodigoProcesso);

                using (StreamWriter arquivoSaida = File.CreateText(caminho))
                {
                    try
                    {
                        ListarCriticaResponse response = new ListarCriticaResponse();
                        response.PreparaResposta(request);

                        var criticas = ListarCritica(new ListarCriticaRequest()
                        {   CodigoSessao = request.CodigoSessao
                        ,   FiltroCodigoProcesso = arquivoInfo.CodigoProcesso
                        ,   FiltroCodigoArquivo = arquivoInfo.CodigoArquivo
                        ,   RetornarAgrupado = true
                        ,   MaxLinhas = 0
                        });

                        // ======================================================================
                        // Monta o csv - Cabeçalho
                        // ======================================================================
                        arquivoSaida.WriteLine
                        (   "Codigo Processo;"
                        +   "Tipo Processo;"
                        +   "Codigo Arquivo;"
                        +   "Nome Arquivo;"
                        +   "Tipo Arquivo;"
                        +   "Qtd;" 
                        +   "Descricao Crítica;"
                        +   "Tipo Linha;"
                        +   "Tipo Critica;"
                        +   "Nome Campo;"
                        );
                        string separador = ";";
                        int quantidadeLog = 0;
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Consulta dos inconsistentes agrupados finalizada", request.CodigoSessao, request.CodigoProcesso);

                        foreach (var itemCritica in criticas.ResultadoAgrupado)
                        {
                            // ======================================================================
                            // Monta o csv - detalhe
                            // ======================================================================
                            arquivoSaida.WriteLine
                            (   processoInfo.CodigoProcesso + separador
                            +   processoInfo.TipoProcesso + separador
                            +   arquivoInfo.CodigoArquivo + separador
                            +   arquivoInfo.NomeArquivo + separador
                            +   arquivoInfo.TipoArquivo + separador
                            +   itemCritica.QtdRegistros + separador
                            +   itemCritica.Descricao + separador 
                            +   itemCritica.TipoLinha + separador 
                            +   itemCritica.TipoCritica + separador 
                            +   itemCritica.NomeCampo + separador
                            );

                            resposta.QuantidadeLinhasGeradas++;
                            quantidadeLog++;

                            // Sinaliza
                            if (quantidadeLog >= 15000)
                            {
                                LogSCFProcessoEstagioEventoInfo.EfetuarLog(resposta.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);
                                quantidadeLog = 0;
                            }
                        }

                        // Informa total de linhas
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Total de linhas geradas: " + resposta.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);
                    }
                    catch (Exception ex1)
                    {
                        resConfig.ProcessarExcessao(ex1);
                        throw new Exception("Erro ao tentar gerar o relatório.", ex1);
                    }
                }
            }

            // Retorno vazio
            return resposta;
        }
        //=============================================================================================1463

        //=============================================================================================AS02
        public AtualizarEmMassaResponse AtualizarEmMassa(AtualizarEmMassaRequest request) //1282 analise
        {
            //=============================================================================================
            //1282 inibido
            //=============================================================================================.
            //AtualizarEmMassaResponse response = new AtualizarEmMassaResponse();
            //response.PreparaResposta(request);

            //var criticas = ListarCritica(new ListarCriticaRequest()
            //{   CodigoSessao = request.CodigoSessao
            //,   FiltroCodigoArquivo = request.CodigoArquivo
            //,   FiltroTipoCritica = request.TipoCritica
            //,   FiltroTipoLinha = request.TipoLinha
            //,   FiltroNomeCampo = request.NomeCampo
            //,   FiltroCodigoProcesso = request.CodigoProcesso
            //,   MaxLinhas = 0
            //});
            //var agrupamento = from c in criticas.Resultado
            //                  where !string.IsNullOrEmpty(c.NomeCampo)
            //                  group c by new { c.DescricaoCritica, c.NomeCampo, c.TipoLinha } into g
            //                  select new { Chave = g.Key, Criticas = g };
            //for (int i = 0; i < request.NomesCampos.Length; i++) // EXISTIA A LISTA DE TODAS AS CRITICAS, FOI ALTERADO PARA CONTER APENAS OS DE TIPO CRÍTICA = CriticaSCFDominio
            //{
            //    string nomeCampo = request.NomesCampos[i]; // o campo NUMERO DE CARTÃO ORIGINAL PARA AJ estava incorreto
            //    string tipoLinha = request.TiposLinhas[i];
            //    string novoValor = request.NovosValores[i]; //1282
            //    string descricaoCritica = request.DescricoesCriticas[i]; //1282
            //    if (!string.IsNullOrEmpty(novoValor)) //1282 só continua se tiver valor informado na tela de alteração massiva
            //    {
            //        foreach (var item in agrupamento)
            //        {
            //            //1282 if (item.Chave.NomeCampo == nomeCampo && item.Chave.TipoLinha == tipoLinha)
            //            if (item.Chave.NomeCampo == nomeCampo && item.Chave.TipoLinha == tipoLinha && item.Chave.DescricaoCritica == descricaoCritica)
            //                foreach (var itemCritica in item.Criticas)
            //                {
            //                    //1282 if (itemCritica.NomeCampo == nomeCampo && itemCritica.TipoLinha == tipoLinha) //1282 if (itemCritica.NomeCampo == nomeCampo)
            //                    if (itemCritica.NomeCampo == nomeCampo && itemCritica.TipoLinha == tipoLinha && itemCritica.DescricaoCritica == descricaoCritica) //1282 if (itemCritica.NomeCampo == nomeCampo)
            //                    {
            //                        //1282 if (!string.IsNullOrEmpty(request.NovosValores[i]))
            //                        //{
            //                            SalvarArquivoItemDetalheRequest reqSalva = new SalvarArquivoItemDetalheRequest();
            //                            reqSalva.CodigoArquivoItem = itemCritica.CodigoArquivoItem;
            //                            reqSalva.CodigoSessao = request.CodigoSessao;
            //                            reqSalva.NomeCampos = new string[] { nomeCampo };
            //                            reqSalva.Valores = new string[] { request.NovosValores[i] };
            //                            var res = SalvarArquivoItemDetalhe(reqSalva);
            //                        //}
            //                    }
            //                }
            //        }
            //    } //1282
            //}
            //=============================================================================================
            //1282 refeito
            //=============================================================================================
            AtualizarEmMassaResponse response = new AtualizarEmMassaResponse();
            response.PreparaResposta(request);

            for (int i = 0; i < request.NomesCampos.Length; i++) 
            {   string novoValor = request.NovosValores[i];

                if (!string.IsNullOrEmpty(novoValor))
                {   string nomeCampo = request.NomesCampos[i];
                    string tipoLinha = request.TiposLinhas[i];
                    string tipoCritica = request.TiposCriticas[i];
                    string descricaoCritica = request.DescricoesCriticas[i]; 

                    var criticas = ListarCritica(new ListarCriticaRequest()
                    {   CodigoSessao            = request.CodigoSessao
                    ,   FiltroCodigoProcesso    = request.CodigoProcesso
                    ,   FiltroCodigoArquivo     = request.CodigoArquivo
                    ,   FiltroTipoCritica       = tipoCritica
                    ,   FiltroTipoLinha         = tipoLinha
                    ,   FiltroNomeCampo         = nomeCampo
                    ,   FiltroDescricaoCritica  = descricaoCritica
                    ,   MaxLinhas               = 0
                    });

                    foreach (var itemCritica in criticas.Resultado)
                    {
                        SalvarArquivoItemDetalheRequest reqSalva = new SalvarArquivoItemDetalheRequest();
                        reqSalva.CodigoArquivoItem = itemCritica.CodigoArquivoItem;
                        reqSalva.CodigoSessao = request.CodigoSessao;
                        reqSalva.NomeCampos = new string[] { nomeCampo };
                        reqSalva.Valores = new string[] { novoValor };
                        var res = SalvarArquivoItemDetalhe(reqSalva);
                    }
                }
            }

            return response;
        }
        //=============================================================================================AS02

        //=============================================================================================AT02
        private Dictionary<string, string> MontaDicionarioReferencia(ListaItemReferenciaSituacaoEnum situacao, ListaItemReferenciaNomeEnum NomeReferencia, List<ListaItemReferenciaInfo> listaReferencias)
        {
            Dictionary<string, string> dicionarioRetorno = new Dictionary<string, string>();
            foreach (ListaItemReferenciaInfo listaReferenciaInfo in listaReferencias)
                if (listaReferenciaInfo.CodigoListaItemSituacao == situacao.ToString() && listaReferenciaInfo.NomeReferencia == NomeReferencia.ToString() && dicionarioRetorno.ContainsKey(listaReferenciaInfo.listaItemInfo.Valor) == false)
                    dicionarioRetorno.Add(listaReferenciaInfo.listaItemInfo.Valor, listaReferenciaInfo.ValorReferencia);

            return dicionarioRetorno;
        }
        //=============================================================================================AT02


        //=============================================================================================AU02
        private ProcessaTransacaoGeralResponse ProcessaTransacaoGeral(ProcessaTransacaoGeralRequest request)
        {
            // prepara a resposta
            ProcessaTransacaoGeralResponse resposta = new ProcessaTransacaoGeralResponse();
            resposta.PreparaResposta(request);
            return resposta;
        }
        //=============================================================================================AU02
        public TransacaoStatusEnum validaStatusTransacao(int statusArquivoItem)
        {
            TransacaoStatusEnum transacaoStatus = TransacaoStatusEnum.Importado;

            switch (statusArquivoItem)
            {
                case 0: //Se o statusArquivoItem for 0 ou 1 gravará como Importado
                case 1:
                    transacaoStatus = TransacaoStatusEnum.Importado;
                    break;

                case 8:
                    transacaoStatus = TransacaoStatusEnum.EnviadoCCI;
                    break;

                default:
                    transacaoStatus = SqlDbLib.EnumToObject<TransacaoStatusEnum>(statusArquivoItem);
                    break;
            }

            return transacaoStatus;
        }
    }
    //=============================================================================================01
}
//=============================================================================================00

