﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    //Classe criada para atender o Jira SCF1188 - Marcos Matsuoka
    public class ServicoValidarCartaoCriptografado
    {
        public void CartaoCriptografado(bool firstLine, string cartao, string nsuHost, string codArqItem, string codigoArquivo, string codigoProcesso, OracleCommand cmSalvarCritica, ref int quantidadeCriticas, string TipoLinha, string NomeCampo) //1478string tipoCartao)
        {
            string criptografia = cartao.Substring(9, 6);
            string vMensagem = "Numero do cartao nao criptografado. Cartao numero [" + cartao + "] NsuHost [" + nsuHost + "] Tipo Linha [" + TipoLinha + "] Parte Não Criptografada [" + criptografia + "] ( posicao 10 com tamanho 6 ) "; //1478
            if (criptografia != "******" && criptografia != "      ") //scf1200 acrescentando se não for brancos
            {
                if (firstLine == true)
                {
                    //1478throw new Exception("Numero do cartao nao criptografado");
                    throw new Exception("Primeira Transacao do arquivo." + vMensagem);
                }
                else
                {
                    CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
                    {
                        //1478 Descricao = string.Format("Numero do cartao nao criptografado"),
                        Descricao = string.Format(vMensagem),
                    };
                    criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                    criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                    criticaArquivoInfo.CodigoArquivoItem = codArqItem;
                    criticaArquivoInfo.TipoLinha = TipoLinha;
                    criticaArquivoInfo.NomeCampo = NomeCampo; //1478 tipoCartao;

                    Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);

                    quantidadeCriticas++;
                }
            }
        }
    }
}
