﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Resource.Carrefour.SCF.Persistencia.Db;
using System.Data;
using Oracle.DataAccess.Client;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Db;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Oracle.DataAccess.Types;
using System.Text.RegularExpressions;
using Resource.Carrefour.SCF.Sistemas.Integracao.Regras;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    public abstract class ArquivoBase : IArquivoBase 
    {
        public bool isHomologacaoTSYS { get; set; } // PARAMETRO SCF - Homologacao TSYS que será desativada
        public bool atualizaNSU { get; set; } // PARAMETRO SCF - Atualizar NSU - Informa se pode atualizar transacao já existente no SCF
        public ArquivoBase()
        {
            ConfiguracaoGeralInfo configuracaoGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(null, null);    // preenche o bool informando se é homologação TSYS
            this.isHomologacaoTSYS = configuracaoGeral.HomologacaoTSYS;
            this.atualizaNSU = configuracaoGeral.ReprocessarNSUProcessado;
        }

        //===================================================================================================================
        #region Manipulação de Linhas
        //===================================================================================================================	

        //===================================================================================================================
        // Infere o tipo da linha ( parm string )
        //===================================================================================================================	        
        public string InferirTipoLinha(string linha)
        {
            return this.OnInferirTipoLinha(linha);
        }

        //===================================================================================================================
        // Infere o tipo da linha ( parm object )
        //===================================================================================================================	        
        public string InferirTipoLinha(object linha)
        {
            return this.OnInferirTipoLinha(linha);
        }

        //===================================================================================================================
        // Método virtual para inferir o tipo da linha
        //===================================================================================================================	        
        protected virtual string OnInferirTipoLinha(object linha)
        {
            return null;
        }

        //===================================================================================================================
        // Cria uma nova linha recebendo os parametros por array
        //===================================================================================================================	        
        public string CriarLinha(string tipoLinha, params object[] valores)
        {
            Dictionary<string, object> valores2 = new Dictionary<string, object>(); // Refaz a coleção de valores
            for (int i = 0; i < valores.Length; i += 2)
                valores2.Add((string)valores[i], valores[i + 1]);
            return this.CriarLinha(tipoLinha, valores2); // Repassa a chamada
        }

        //===================================================================================================================
        /// Cria uma nova linha recebendo os parametros por dicionario
        //===================================================================================================================	        
        public string CriarLinha(string tipoLinha, Dictionary<string, object> valores)
        {
            return this.OnCriarLinha(tipoLinha, valores); // Chama o método virtual
        }

        //===================================================================================================================
        /// Método virtual para a criação da linha
        //===================================================================================================================	                    
        protected virtual string OnCriarLinha(string tipoLinha, Dictionary<string, object> valores)
        {
            return null;
        }

        //===================================================================================================================
        /// Faz a leitura do campo - Infere o tipo da linha através do método InferirTipoLinha - Passa interpretar campo como false
        //===================================================================================================================	                    
        public object LerCampo(object linha, string nomeCampo)
        {
            return this.LerCampo(this.InferirTipoLinha((string)linha), (string)linha, nomeCampo, false); // Repassa a chamada
        }

        //===================================================================================================================
        /// Faz a leitura do campo - Infere o tipo da linha através do método InferirTipoLinha
        //===================================================================================================================	                    
        public object LerCampo(object linha, string nomeCampo, bool interpretar)
        {
            return this.LerCampo(this.InferirTipoLinha((string)linha), (string)linha, nomeCampo, interpretar); // Repassa a chamada
        }

        //===================================================================================================================
        /// Faz a leitura do campo - Infere o tipo da linha através do método InferirTipoLinha
        //===================================================================================================================
        public object LerCampo(object linha, int indiceCampo, bool interpretar)
        {
            return this.LerCampo(this.InferirTipoLinha(linha), linha, indiceCampo, interpretar); // Repassa a chamada
        }

        //===================================================================================================================
        /// Faz a leitura do campo - Passa interpretar campo como false
        //===================================================================================================================
        public object LerCampo(string tipoLinha, object linha, string nomeCampo)
        {
            return this.LerCampo(tipoLinha, linha, nomeCampo, false); // Repassa a chamada
        }

        //===================================================================================================================
        /// Faz a leitura de uma Campo
        //===================================================================================================================
        public object LerCampo(string tipoLinha, object linha, string nomeCampo, bool interpretar)
        {
            return this.OnLerCampo(tipoLinha, linha, nomeCampo, interpretar); // Chama o método virtual
        }

        //===================================================================================================================
        /// Faz a leitura de uma Campo
        //===================================================================================================================        
        public object LerCampo(string tipoLinha, object linha, int indiceCampo, bool interpretar)
        {
            return this.OnLerCampo(tipoLinha, linha, indiceCampo, interpretar); // Chama o método virtual
        }

        //===================================================================================================================
        /// Método virtual de leitura de Campo
        //===================================================================================================================
        protected virtual object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
            return null;
        }

        //===================================================================================================================
        /// Escreve um valor na Campo - Retorna a linha com o novo valor da Campo
        //===================================================================================================================
        public string EscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {
            return this.OnEscreverCampo(tipoLinha, linha, nomeCampo, valor, interpretarValor); // Chama o método virtual
        }

        //===================================================================================================================
        /// Escreve um valor na Campo - Retorna a linha com o novo valor da Campo
        //===================================================================================================================		
        public string EscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor)
        {
            return this.EscreverCampo(tipoLinha, linha, nomeCampo, valor, true); // Chama o método virtual
        }

        //===================================================================================================================
        /// Método virtual para escrever valor em uma Campo - Retorna a linha com o novo valor da Campo
        //===================================================================================================================		
        protected virtual string OnEscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {
            return null;
        }

        //===================================================================================================================
        /// Tira um valor de Campo na Linha - Retorna a linha com o novo valor da Campo
        //===================================================================================================================		
        public string RemoverValor(string tipoLinha, object linha, object nomeCampo)
        {
            return this.OnRemoverValor(tipoLinha, linha, nomeCampo); // Chama o método virtual
        }

        //===================================================================================================================		
        /// Método virtual para tirar valor de um Campo da Linha - Retorna a linha sem o valor do Campo
        //===================================================================================================================		
        protected virtual string OnRemoverValor(string tipoLinha, object linha, object nomeCampo)
        {
            return null;
        }

        //===================================================================================================================		
        /// Insere um valor de Campo na Linha - Retorna a linha com o novo valor de Campo
        //===================================================================================================================		
        public string InserirValor(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {
            return this.OnInserirValor(tipoLinha, linha, nomeCampo, valor, interpretarValor); // Chama o método virtual
        }

        //===================================================================================================================		
        /// Método virtual para inserir valor de um Campo da Linha - Retorna a linha com o valor do Campo
        //===================================================================================================================		
        protected virtual string OnInserirValor(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {
            return null;
        }

        //===================================================================================================================		
        #endregion
        //===================================================================================================================		
        #region Importacao
        //===================================================================================================================		
        /// Faz a importacao do arquivo 
        //===================================================================================================================		
        public long ImportarArquivo(string arquivo, string codigoProcesso, string codigoEmpresaArquivo)
        {
            return this.OnImportarArquivo(arquivo, codigoProcesso, null, codigoEmpresaArquivo);
        }

        //===================================================================================================================		
        /// Faz a importacao do arquivo TSYS
        //===================================================================================================================		
        public long ImportarArquivo(string arquivo, string codigoProcesso, string chaveArquivo, string codigoEmpresaArquivo)
        {
            return this.OnImportarArquivo(arquivo, codigoProcesso, chaveArquivo, codigoEmpresaArquivo);
        }

        //===================================================================================================================		
        /// Faz a importacao do arquivo especificando o layout
        //===================================================================================================================		
        public long ImportarArquivo(string arquivo, string codigoProcesso, string chaveArquivo, string layoutArquivo, string codigoEmpresaArquivo)
        {
            return this.OnImportarArquivo(arquivo, codigoProcesso, chaveArquivo, layoutArquivo, codigoEmpresaArquivo);
        }

        //===================================================================================================================		
        /// Método virtual para importar arquivo, sem especificar o layout
        //===================================================================================================================		
        protected virtual long OnImportarArquivo(string arquivo, string codigoProcesso, string chaveArquivo, string codigoEmpresaArquivo)
        {
            return OnImportarArquivo(arquivo, codigoProcesso, chaveArquivo, null, codigoEmpresaArquivo);
        }

        //===================================================================================================================		
        /// IMPORTAÇÃO DE ARQUIVO NO SCF - Linhas do TXT para TB_ARQUIVO_ITEM
        //===================================================================================================================		
        protected virtual long OnImportarArquivo(string arquivo, string codigoProcesso, string chaveArquivo, string layoutArquivo, string CodigoGrupoEmpresaArquivo)
        {
            long codigoArquivo = 0; // Prepara o retorno
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo(); 
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao("IMPORTACAO - Inicio");
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);

            OracleConnection cn = Procedures.ReceberConexao();// Abre conexao
            try             // Bloco de controle
            {
                FileInfo fileInfo = new FileInfo(arquivo);
                using (StreamReader reader = new StreamReader(arquivo, Encoding.GetEncoding("ISO-8859-1"))) // Abre o arquivo solicitado com ENCODING ISO 8859
                {
                    DataRow drArquivo = Procedures.PR_ARQUIVO_S(cn, null, this.GetType().Name, fileInfo.Name, "0", true, codigoProcesso, chaveArquivo, layoutArquivo, CodigoGrupoEmpresaArquivo);
                    codigoArquivo = (long)drArquivo["CODIGO_ARQUIVO"]; // Cria o registro do arquivo	

                    using (OracleCommand cmArquivoItemS = Procedures.PR_ARQUIVO_ITEM_S_preparar(cn)) // Inicializa command de item de arquivo
                    {
                        int numeroLinha = 0;    // quantidade de linhas importadas
                        int quantidadeLog = 0; // quantidade de linhas para logar
                        string linha = null; // conteúdo da linha do arquivo TXT

                        Dictionary<string, BinReferenciaInfo> dicionarioBinRef = null;
                        dicionarioBinRef = new Dictionary<string, BinReferenciaInfo>();
                        List<BinReferenciaInfo> BinReferencias = PersistenciaHelper.Listar<BinReferenciaInfo>(null);
                        foreach (BinReferenciaInfo binReferencia in BinReferencias)
                            if (binReferencia.CodigoBin != null)
                            {   if (!dicionarioBinRef.ContainsKey(binReferencia.CodigoBin))
                                    dicionarioBinRef.Add(binReferencia.CodigoBin, binReferencia);
                            }

                        while ((linha = reader.ReadLine()) != null) // Varre as linhas adicionando no banco
                        {
                            numeroLinha++;
                            quantidadeLog++;
                            linha = retiraAcentos(linha.Trim()); 
                            this.ImportarLinha(cmArquivoItemS, codigoArquivo, numeroLinha, linha.ToString(), dicionarioBinRef); // grava no arquivo_item
                            if (codigoProcesso != null && quantidadeLog >= 15000) // Gera LOG 
                            {
                                quantidadeLog = 0;
                                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("IMPORTACAO - Código Arquivo  [{0}] - {1} linhas", codigoArquivo, numeroLinha));
                                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                            }
                        }
                        reader.Close();
                        lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                        lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("IMPORTACAO - Código Arquivo  [{0}] -  total linhas [{1}] FIM", codigoArquivo, numeroLinha));
                        LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cn != null) // Finaliza conexao
                    cn.Dispose();
            }
            return codigoArquivo;   // Retorna
        }

        //===================================================================================================================									
        /// Função para remover acentos de strings
        //===================================================================================================================		
        private string retiraAcentos(string strcomAcentos) //SCF845
        {
            string strsemAcentos = strcomAcentos;
            strsemAcentos = Regex.Replace(strsemAcentos, "[áàâãª]", "a");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÁÀÂÃ]", "A");
            strsemAcentos = Regex.Replace(strsemAcentos, "[éèê]", "e");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÉÈÊ]", "e");
            strsemAcentos = Regex.Replace(strsemAcentos, "[íìî]", "i");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÍÌÎ]", "I");
            strsemAcentos = Regex.Replace(strsemAcentos, "[óòôõº]", "o");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÓÒÔÕ]", "O");
            strsemAcentos = Regex.Replace(strsemAcentos, "[úùû]", "u");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÚÙÛ]", "U");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ç]", "c");
            strsemAcentos = Regex.Replace(strsemAcentos, "[Ç]", "C");
            strsemAcentos = Regex.Replace(strsemAcentos, "[Ñ]", "N"); //SCF948
            strsemAcentos = Regex.Replace(strsemAcentos, "[ñ]", "n"); //SCF948
            strsemAcentos = Regex.Replace(strsemAcentos, "[£]", ""); //SCF1200
            strsemAcentos = Regex.Replace(strsemAcentos, "[§]", ""); //SCF1200
            return strsemAcentos;
        }


        //===================================================================================================================		
        /// Faz a importacao da linha
        //===================================================================================================================		
        //1407 public void ImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha)
        public void ImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha, Dictionary<string, BinReferenciaInfo> dicionarioBinRef)
        {
            this.OnImportarLinha(cm, codigoArquivo, numeroLinha, linha, dicionarioBinRef);//1407
        }

        //===================================================================================================================		
        /// Método virtual para importação da linha
        //===================================================================================================================		
        //1407 protected virtual void OnImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha)
        protected virtual void OnImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha, Dictionary<string, BinReferenciaInfo> dicionarioBinRef)
        {
        }
        //===================================================================================================================		
        #endregion
        //===================================================================================================================		

        //===================================================================================================================		
        #region Validação
        //===================================================================================================================		
        /// Retorna a lista de regras para o arquivo
        //===================================================================================================================		
        public List<RegraSCFBase> ReceberRegras()
        {
            return this.OnReceberRegras();
        }

        //===================================================================================================================		
        /// Método virtual para retornar a lista de regras
        //===================================================================================================================		
        protected virtual List<RegraSCFBase> OnReceberRegras()
        {
            return null;
        }

        //===================================================================================================================		
        /// Faz a validação do arquivo com o conjunto de regras informadas - Pede a lista de regras para o método ReceberRegras
        //===================================================================================================================		
        public int ValidarArquivo(string codigoProcesso, string codigoArquivo)
        {
            return this.OnValidarArquivo(codigoProcesso, codigoArquivo, new Dictionary<string, object>(), this.ReceberRegras(), null);
        }

        //===================================================================================================================		
        /// Faz a validação do arquivo com o conjunto de regras informadas - Pede a lista de regras para o método ReceberRegras -Retorna a quantidade de críticas geradas
        //===================================================================================================================		
        public int ValidarArquivo(string codigoProcesso, string codigoArquivo, Dictionary<string, object> contexto)
        {
            return this.OnValidarArquivo(codigoProcesso, codigoArquivo, contexto, this.ReceberRegras(), null);
        }


        //===================================================================================================================		
        /// Faz a validação do arquivo com o conjunto de regras informadas - Retorna a quantidade de críticas geradas
        //===================================================================================================================		
        public int ValidarArquivo(string codigoProcesso, string codigoArquivo, Dictionary<string, object> contexto, List<RegraSCFBase> regras)
        {
            return this.OnValidarArquivo(codigoProcesso, codigoArquivo, contexto, regras, null); // Chama o método virtual
        }

        //===================================================================================================================		
        /// Faz a validação do arquivo com o conjunto de regras informadas - Pede a lista de regras para o método ReceberRegras - Retorna a quantidade de críticas geradas</returns>
        //===================================================================================================================		
        public int ValidarArquivo(string codigoProcesso, string codigoArquivo, Dictionary<string, object> contexto, string codigoSessao)
        {
            return this.OnValidarArquivo(codigoProcesso, codigoArquivo, contexto, this.ReceberRegras(), codigoSessao); // Chama o método virtual
        }

        //===================================================================================================================		
        /// OnValidarArquivo - Fase VALIDACAO
        //===================================================================================================================		
        protected int OnValidarArquivo(string codigoProcesso, string codigoArquivo, Dictionary<string, object> contexto, List<RegraSCFBase> regras, string codigoSessao)
        {
            //===================================================================================================================		
            // CONEXAO BANCO
            //===================================================================================================================		
            OracleConnection cn = Procedures.ReceberConexao();            // Pega conexao com o banco
            OracleCommand cmSalvarCritica = Procedures.PR_CRITICA_S_preparar(cn);             // Pega o command para salvar criticas
            Procedures.PR_CRITICA_R(null, codigoArquivo);            // Limpa a lista de críticas atual do arquivo
            //===================================================================================================================		
            // CHAMANDO A PROCEDURE PARA CARREGAR LINHAS IMPORTADAS DO ARQUIVO
            //===================================================================================================================		
            OracleCommand cmArquivoItemS = Procedures.PR_ARQUIVO_ITEM_S_preparar(cn);            // Prepara o command para salvar arquivoItem
            OracleDataReader reader = Procedures.PR_ARQUIVO_ITEM_L(cn, codigoArquivo, null, false, false, 0, 0, 0, false, false).GetDataReader(); // Pede as linhas do arquivo
            //===================================================================================================================		
            // INICIALIZA VARIAVEIS
            //===================================================================================================================		
            bool habloqueios = false; 
            string vNSEQ = "000000000";
            long vQtdRegra = 0; 
            long vQtdLinhasCorrigidas = 0;
            long vQtdLinhasBloqueadas = 0; 
            long numeroLinha = 0;
            string vnomeCampo = null; 
            int quantidadeLog = 0;
            // ===================================================================
            // ATENCAO
            // ===================================================================
            // Este campo contem a soma das criticas da validação
            // quando preenchido deixa o processo no estágio AGUARDANDO VALIDAÇÃO
            // ===================================================================
            int quantidadeCriticas = 0;
            // ===================================================================
            // ATENCAO
            // ===================================================================
            // Este campo conterá sem o valor da qtd de criticas que a regra apurou
            // ===================================================================
            int vlinhacomcritica = 0;
            // ===================================================================
            int sequencialNSU = 0;
            bool tipoLinhaRP = false;
            bool linhaBloqueada = false; 
            bool firstLine = true;             
            string nsuHostTran = null; //CH0000053039 - inicio
            string nsuHostOrig = null;
            double valorParcela = 0;
            double valorVenda = 0;
            // bool LinhaValida = true; // warning 25/02/2019
            bool cancelamentoParcial = false;             
            int fatorCancelamentoParcial = 0; //CH0000053039 - fim
            ProcessoInfo processo = PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, codigoProcesso);             // Pega informações do processo/
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo(); //SCF1175
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Configurando base de criticas"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            List<CriticaSCFArquivoInfo> criticasArquivo = new List<CriticaSCFArquivoInfo>();
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Carregando Itens de Referencia"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            //1285 List<ListaItemReferenciaInfo> listaItemReferencialInfo = PersistenciaHelper.Listar<ListaItemReferenciaInfo>(codigoSessao);            //ECOMMERCE - Fernando Bove - 20151211: Inclusão da lista de ítens de referência
            List<ListaItemReferenciaInfo> listaItemReferencias = PersistenciaHelper.Listar<ListaItemReferenciaInfo>(null);             //20161027 movi de lá de baixo para cá - motivo: melhorar performance da validação             // pede a lista de referencias
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Configurando Cancelamento Parcial"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            IDictionary<string, CancelamentoParcialStruct> arrayCancParcialDetail = new Dictionary<string, CancelamentoParcialStruct>();
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Configurando Sumarizacao de Cancelamento Parcial"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            IDictionary<string, double> arrayCancParcialValHeader = new Dictionary<string, double>(); //CH0000053039
            IDictionary<string, ArquivoItemStatusEnum> arrayCancParcialStsHeader = new Dictionary<string, ArquivoItemStatusEnum>(); //CH0000053039
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Carregando Produtos AV"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            Dictionary<string, string> dicionarioProdutosAV = null;
            dicionarioProdutosAV = new Dictionary<string, string>();
            //===================================================================================================================		
            // Monta a mensagem de validacao do arquivo
            //===================================================================================================================		
            RegraSCFValidarArquivoRequest requestValidarArquivo =
                new RegraSCFValidarArquivoRequest()
                {
                    Arquivo = this,
                    CodigoArquivo = codigoArquivo,
                    Contexto = contexto
                };
            //===================================================================================================================		
            // Para as regras com escopo de arquivo, dispara Inicializacao
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Carregando Regras de Arquivo"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            requestValidarArquivo.Criticas = new List<CriticaSCFArquivoInfo>();
            vQtdRegra = 0;
            foreach (RegraSCFBase regra in regras)
            {
                regra.IniciarValidacaoArquivo(requestValidarArquivo);
                vQtdRegra++;
            }
            //===================================================================================================================		
            // 1407 - INICIALIZANDO AS REGRAS APENAS UMA VEZ
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Inicializando Regras da Linha"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            RegraSCFValidarLinhaRequest requestValidarLinha = new RegraSCFValidarLinhaRequest();
            foreach (RegraSCFBase regra in regras)
            {
                regra.IniciarValidacaoLinha(requestValidarLinha);
            }
            //===================================================================================================================		
            // Persiste as críticas de inicio de arquivo
            //===================================================================================================================		
            foreach (CriticaSCFArquivoInfo criticaArquivoInfo in requestValidarArquivo.Criticas)
            {
                criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
            }
            //===================================================================================================================		
            // INICIO - LEITURA DO ARQUIVO TEXTO IMPORTADO
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO INICIO - São [{0}] regras a serem conferidas", vQtdRegra));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            while (reader.Read())
            {
                //===================================================================================================================		
                // ADICIONA QUANTIDADES
                //===================================================================================================================		
                quantidadeLog++;
                numeroLinha++;
                vlinhacomcritica = 0; //scf1320
                linhaBloqueada = false; //SCF1303
                //===================================================================================================================		
                // CARREGA VARIAVEIS
                //===================================================================================================================		
                string codigoArquivoItem = reader.GetDouble(reader.GetOrdinal("CODIGO_ARQUIVO_ITEM")).ToString();
                string linha = reader.GetString(reader.GetOrdinal("CONTEUDO_ARQUIVO_ITEM")); // Pega a linha
                string tipoLinha = reader.GetString(reader.GetOrdinal("TIPO_ARQUIVO_ITEM")); // Acha o tipo da linha
                string ChaveArquivoItem = ""; // 1328
                if (reader["CHAVE"] != DBNull.Value)
                    ChaveArquivoItem = reader.GetString(reader.GetOrdinal("CHAVE"));
                string ReferenciaArquivoItem = ""; // 1328
                if (reader["REFERENCIA"] != DBNull.Value)
                    ReferenciaArquivoItem = reader.GetString(reader.GetOrdinal("REFERENCIA"));
                ArquivoItemStatusEnum statusLinha = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(reader["STATUS_ARQUIVO_ITEM"]); // Status da linha
                tipoLinhaRP = linha.Substring(0, 2).Equals("RP");	//1410
                vNSEQ = null; // 06/12/2018 erro no processo de CESSAO (string)this.LerCampo(tipoLinha, linha, "NSEQ", true).ToString();
                nsuHostTran = null; // CH0000053039 inicio
                nsuHostOrig = null;
                valorParcela = 0;
                valorVenda = 0;
                cancelamentoParcial = false; 
                // LinhaValida = true;   // warning 25/02/2019
                fatorCancelamentoParcial = 0; // CH0000053039 fim
                //=================================================================================================================
                // 1408
                // No continuar processo, os registros que foram bloqueados manualmente não SÃO identificados como bloqueado
                // Porque a flag LINHABLOQUEADA nasce como FALSE
                // Alteração: CONFERIR O STATUS DA LINHA e se estiver BLOQUEADA alterar a flag LINHABLOQUEADA
                //=================================================================================================================
                if (statusLinha == ArquivoItemStatusEnum.Bloqueado)
                    linhaBloqueada = true;
                //=================================================================================================================
                string conteudoArquivoItem = linha;
                double codigoArquivoOriginal = 0;
                if (reader["CODIGO_ARQUIVO_ORIGEM"] != DBNull.Value)
                    codigoArquivoOriginal = reader.GetInt64(reader.GetOrdinal("CODIGO_ARQUIVO_ORIGEM"));
                else
                    codigoArquivoOriginal = reader.GetInt64(reader.GetOrdinal("CODIGO_ARQUIVO"));
                //===================================================================================================================		
                // CH0000053039 - Movendo instruções
                //===================================================================================================================		
                string tipoLancamento = "";
                if (tipoLinha.Equals("CV") || tipoLinha.Equals("AV"))
                    tipoLancamento = (string)this.LerCampo(tipoLinha, linha, "TipoLancamento", true);
                //===================================================================================================================		
                string produto = "";
                if (tipoLinha.Equals("CV"))
                    produto = (string)this.LerCampo(tipoLinha, linha, "ProdutoCSF", true);
                //===================================================================================================================		
                string codigoAnulacao = "";
                if (tipoLinha.Equals("AV"))
                {
                    codigoAnulacao = ((string)this.LerCampo(tipoLinha, linha, "CodigoAnulacao", true));
                    foreach (ListaItemReferenciaInfo listaReferenciaInfo in listaItemReferencias)
                        if (listaReferenciaInfo.CodigoListaItemSituacao == "CodigoAnulacao" &&
                                listaReferenciaInfo.NomeReferencia == "Produto" &&
                                dicionarioProdutosAV.ContainsKey(listaReferenciaInfo.listaItemInfo.Valor) == false
                            )
                            dicionarioProdutosAV.Add(listaReferenciaInfo.listaItemInfo.Valor, listaReferenciaInfo.ValorReferencia);
                }
                //===================================================================================================================		
                // INICIO - TRATANDO ARQUIVOS TSYS
                //===================================================================================================================		
                if (processo.TipoProcesso == "ProcessoExtratoInfo" ||  processo.TipoProcesso == "ProcessoAtacadaoInfo" )
                {
                    //===================================================================================================================		
                    // INICIO - Consiste NUMERO DO CARTAO e BIN
                    //===================================================================================================================		
                    //1410 if (tipoLinha.Equals("AP") || tipoLinha.Equals("AV") || tipoLinha.Equals("CP") || tipoLinha.Equals("CV"))
                    if (tipoLinha.Equals("AP") || tipoLinha.Equals("AV") || tipoLinha.Equals("CP") || tipoLinha.Equals("CV") || tipoLinha.Equals("AJ")) //1410
                    {
                        string numeroCartao = "";
                        string nsuHost = "";
                        if (tipoLinha.Equals("AJ"))
                        {
                            numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartaoTransacaoOriginal", true);
                            nsuHost = (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", true);
                            vnomeCampo = "NumeroCartaoTransacaoOriginal"; 
                        }
                        else
                        {
                            numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartao", true);
                            nsuHost = (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", true);
                            vnomeCampo = "NumeroCartao"; 
                        }
                        ServicoValidarCartaoCriptografado servicoValidarCartaoCriptografado = new ServicoValidarCartaoCriptografado();
                        servicoValidarCartaoCriptografado.CartaoCriptografado(firstLine, numeroCartao, nsuHost, codigoArquivoItem, codigoArquivo, codigoProcesso, cmSalvarCritica, ref vlinhacomcritica, tipoLinha, vnomeCampo); // 1478 tipoCartao);

                        if (tipoLinha.Equals("AJ"))
                        {
                            ServicoValidarBinVazioZerado ServicoValidarBinVazioZerado = new ServicoValidarBinVazioZerado();
                            ServicoValidarBinVazioZerado.BinVazioZerado(numeroCartao, codigoArquivoItem, codigoArquivo, codigoProcesso, statusLinha, cmSalvarCritica, ref vlinhacomcritica, processo, codigoSessao, out linhaBloqueada); //SCF1303
                        }
                        if (firstLine == true)
                            firstLine = false;
                    }
                    //===================================================================================================================		
                    // FIM - Consiste NUMERO DO CARTAO e BIN DO AJ
                    //===================================================================================================================		
                    //===================================================================================================================		
                    // INICIO - TRATANDO CANCELAMENTO PARCIAL - Identificando registro 
                    //===================================================================================================================		
                    if (     (   tipoLinhaRP || 			// Tipo Linha RP OU
                                tipoLinha.Equals("CV") || 			// Tipo Linha CV OU
                                tipoLinha.Equals("AV")				// Tipo Linha AV
                            )
                        &&											// E....

                            (tipoLancamento.Equals("3")			// Tipo Lancamento 3 ( ecommerce )
                            )
                        && 											// E...
                            (
                                tipoLinhaRP || 			// Tipo Linha RP 
                                (									// OU..
                                    tipoLinha.Equals("AV") && 		// Tipo Linha AV E
                                    (
                                    dicionarioProdutosAV[codigoAnulacao] == "CRAF" || // Produto CRAF OU
                                    dicionarioProdutosAV[codigoAnulacao] == "CRES"	// Produto CRES
                                    )
                                ) ||			// OU..
                                (tipoLinha.Equals("CV") && 		// Tipo Linha CV E
                                    (
                                        produto.Equals("CRAF") ||   // Produto CRAF OU
                                        produto.Equals("CRES")		// Produto CRES
                                    )
                                )
                            )
                        )
                    {
                        cancelamentoParcial = true; //CH0000053039 inicio
                        nsuHostTran = (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", true);
                        nsuHostOrig = (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacaoOriginal", true);
                        valorParcela = (double)this.LerCampo(tipoLinha, linha, "ValorLiquidoParcela", true);
                        valorVenda = (double)this.LerCampo(tipoLinha, linha, "ValorLiquidoVenda", true);
                        fatorCancelamentoParcial = (tipoLinhaRP || tipoLinha.Equals("CV")) ? 1 : -1;  //CH0000053039 fim - Será 1 ( positivo ) se for RP ou CV, será -1 ( negativo ) se for AV
                    }
                    //===================================================================================================================		
                    // FIM - TRATANDO CANCELAMENTO PARCIAL - Somando os valores por NSU HOST ORIGINAL
                    //===================================================================================================================		
                }
                //===================================================================================================================		
                // FIM - TRATANDO REGISTROS AV, CV, AP, CP, AJ e RP
                //===================================================================================================================		
                //===================================================================================================================		
                // Para LINHA RP mandar como AV e quando voltar trocar para RP
                //===================================================================================================================		
                if (tipoLinhaRP)
                {
                    int vTamanho = conteudoArquivoItem.ToString().Length;
                    vTamanho = vTamanho - 2;
                    string vPedaco = conteudoArquivoItem.Substring(2, vTamanho);
                    conteudoArquivoItem = ("AV" + vPedaco);
                }
                //===================================================================================================================		
                // Monta a mensagem
                //===================================================================================================================		
                requestValidarLinha =
                    new RegraSCFValidarLinhaRequest()
                    {                               Criticas = new List<CriticaSCFArquivoItemInfo>()
                        ,                           Arquivo = this
                        ,                           CodigoProcesso = codigoProcesso
                        ,                           CodigoArquivo = codigoArquivo
                        ,                           CodigoArquivoItem = codigoArquivoItem
                        ,                           Linha = conteudoArquivoItem
                        ,                           TipoLinha = tipoLinha
                        ,                           Contexto = contexto
                        ,                           SequencialNSU = sequencialNSU
                        ,                           Status = statusLinha
                        ,                           CodigoArquivoOriginal = codigoArquivoOriginal.ToString()
                        ,                           listaItemReferenciaInfo = listaItemReferencias //1285 listaItemReferencialInfo
                        ,                           Referencia = ReferenciaArquivoItem //SCF1340
                        ,                           NSEQ = vNSEQ //1353
                    };
                //===================================================================================================================		
                // Aplica regras
                //===================================================================================================================		
                requestValidarLinha.LinhaCorrigida = false;
                requestValidarLinha.LinhaAtualizada = false; // para não considerar na quantidade de linhas corrigidas
                vQtdRegra = 0;
                foreach (RegraSCFBase regra in regras)
                {
                    requestValidarLinha.Executou_Regra = false; //1408
                    regra.ValidarLinha(requestValidarLinha);
                }
                //===================================================================================================================		
                // Voltando da aplicacao de REGRAS, atualizando variaveis
                //===================================================================================================================		
                sequencialNSU = requestValidarLinha.SequencialNSU; //1410
                vlinhacomcritica += requestValidarLinha.Criticas.Count; //1320
                //===================================================================================================================		
                // Atualiza o status da linha caso esteja valida mas tem critica
                //===================================================================================================================		
                if (statusLinha != ArquivoItemStatusEnum.Bloqueado &&
                    statusLinha != ArquivoItemStatusEnum.PendenteDesbloqueio &&
                    statusLinha != ArquivoItemStatusEnum.Excluido &&
                    statusLinha != ArquivoItemStatusEnum.Cancelado)
                {
                    if (vlinhacomcritica == 0)
                        statusLinha = ArquivoItemStatusEnum.Valido;
                    else
                        statusLinha = ArquivoItemStatusEnum.Invalido;
                }
                //===================================================================================================================		
                // Para LINHA RP mandar como AV e quando voltar trocar para RP
                //===================================================================================================================		
                if (tipoLinhaRP)
                {
                    int vTamanho = requestValidarLinha.Linha.ToString().Length;
                    vTamanho = vTamanho - 2;
                    string vPedaco = requestValidarLinha.Linha.Substring(2, vTamanho);
                    requestValidarLinha.Linha = ("RP" + vPedaco);
                }
                //===================================================================================================================		
                // INICIO - CH0000053039 - Aplicando Regras Especificas de Cancelamento parcial onde um dos componentes pode estar com critica
                //===================================================================================================================		
                if (cancelamentoParcial)
                {
                    double vValorTransacao = (valorParcela != 0 ? valorParcela : valorVenda); // valor da parcela se transacao tiver parcela, senão, sera o valor da venda                    
                    double vValorCancParcial = Math.Round(fatorCancelamentoParcial * vValorTransacao, 2); // valor do cancelamento parcial, será o valor da transação X o fator que será 1 ou -1, ficando negativo quando for AV e positivo quando for RP e CV
                    //===================================================================================================================		
                    // HEADER - 
                    // 1o Array (arrayCancParcialValHeader) contendo [NsuHostOriginal]  e [Soma dos Valores das Transacoes] 
                    // 2o Array (arrayCancParcialStsHeader) contendo [NsuHostOriginal]  e [Status da Linha]
                    //===================================================================================================================		
                    if (arrayCancParcialValHeader.ContainsKey(nsuHostOrig))     // atualiza somando valor e atualizando status
                    {
                        arrayCancParcialValHeader[nsuHostOrig] = Math.Round(arrayCancParcialValHeader[nsuHostOrig] + vValorCancParcial, 2); // somando valor da transação

                        if (arrayCancParcialStsHeader[nsuHostOrig] == ArquivoItemStatusEnum.Bloqueado) // se o status da composicao for BLOQUEADO ligar flag 
                            linhaBloqueada = true;

                        if (statusLinha != ArquivoItemStatusEnum.Valido) // status da transacao é diferente de VALIDO ? 
                        {
                            if (arrayCancParcialStsHeader[nsuHostOrig] != statusLinha)
                            {
                                if (arrayCancParcialStsHeader[nsuHostOrig] == ArquivoItemStatusEnum.Bloqueado &&
                                     arrayCancParcialStsHeader[nsuHostOrig] == ArquivoItemStatusEnum.PendenteDesbloqueio &&
                                     arrayCancParcialStsHeader[nsuHostOrig] == ArquivoItemStatusEnum.Excluido &&
                                     arrayCancParcialStsHeader[nsuHostOrig] == ArquivoItemStatusEnum.Cancelado)
                                {
                                    statusLinha = arrayCancParcialStsHeader[nsuHostOrig]; // se o status da compsicao for um dos acima, ele substitui o da linha

                                    if (vlinhacomcritica != 0)
                                        vlinhacomcritica = vlinhacomcritica -1;
                                }
                                else
                                    arrayCancParcialStsHeader[nsuHostOrig] = statusLinha; // se não o status da linha subtistui o da composicao
                            }
                        }
                        else
                        {
                            requestValidarLinha.LinhaAtualizada = true; // ligar flag para atualizar na arquivo item
                            statusLinha = arrayCancParcialStsHeader[nsuHostOrig]; // o status da llinha será subtituido pelo status da composição
                        }
                    }
                    else                                                                                      // cria com valor
                    {
                        arrayCancParcialValHeader.Add(nsuHostOrig, vValorCancParcial); // 1o Array
                        arrayCancParcialStsHeader.Add(nsuHostOrig, statusLinha); // 2o Array
                    }
                    //===================================================================================================================		
                    // DETAIL - Incluindo a transação do cancelamento parcial
                    //===================================================================================================================		
                    CancelamentoParcialStruct cancelamentoParcialStruct =  new CancelamentoParcialStruct
                        (   codigoArquivoItem
                        ,   nsuHostTran
                        ,   nsuHostOrig
                        ,   tipoLinha
                        ,   vValorCancParcial
                        ,   statusLinha
                        );
                    arrayCancParcialDetail.Add(codigoArquivoItem, cancelamentoParcialStruct); // DETAIL - transacao da composição atual
                    //===================================================================================================================		
                    // Tratando a quantidade de criticas e gerando critica
                    // Quando a linha estava VALIDA mas a composicao NAO
                    //===================================================================================================================		
                    if (vlinhacomcritica == 0 && statusLinha != ArquivoItemStatusEnum.Valido)
                    {
                        if (statusLinha != ArquivoItemStatusEnum.Bloqueado &&
                            statusLinha != ArquivoItemStatusEnum.PendenteDesbloqueio &&
                            statusLinha != ArquivoItemStatusEnum.Excluido &&
                            statusLinha != ArquivoItemStatusEnum.Cancelado)
                            vlinhacomcritica++;

                        string vDescricao = string.Format("[Cancelamento parcial com critica em alguma transação de sua composição - NSUHOST Original {0}] ", nsuHostOrig);
                        CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
                        {
                            Descricao = vDescricao,
                            TipoLinha = tipoLinha,
                            NomeCampo = "ValorBrutoVenda"
                        };
                        criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                        criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                        criticaArquivoInfo.CodigoArquivoItem = codigoArquivoItem;
                        Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
                    }
                }
                //===================================================================================================================		
                // FIM - CH0000053039 - Tratando cancelamento parcial onde um dos componentes pode estar com critica
                //===================================================================================================================		
                //===================================================================================================================		
                // CH0000053039 -Atualiza o status da linha caso esteja valida mas tem critica
                //===================================================================================================================		
                if (statusLinha != ArquivoItemStatusEnum.Bloqueado &&
                    statusLinha != ArquivoItemStatusEnum.PendenteDesbloqueio &&
                    statusLinha != ArquivoItemStatusEnum.Excluido &&
                    statusLinha != ArquivoItemStatusEnum.Cancelado)
                {
                    if (vlinhacomcritica == 0)
                        statusLinha = ArquivoItemStatusEnum.Valido;
                    else
                        statusLinha = ArquivoItemStatusEnum.Invalido;
                }
                //===================================================================================================================		
                // Confere se linha foi bloqueada automaticamente no processo de validacao
                //===================================================================================================================		
                if (!linhaBloqueada && requestValidarLinha.LinhaBloqueada)    //1371
                    linhaBloqueada = requestValidarLinha.LinhaBloqueada;    // 1328 - SE LINHA FOI BLOQUEADA PELO CONJUNTO DE REGRAS, ALTERAR A VARIAVEL STATUS LINHA

                if (linhaBloqueada)
                {
                    statusLinha = ArquivoItemStatusEnum.Bloqueado;	// SE LINHA FOI BLOQUEADA PELA REGRA DO AJ, ALTERAR A VARIAVEL STATUS LINHA
                    vQtdLinhasBloqueadas++;
                }
                //===================================================================================================================		
                // Atualiza a quantidade de críticas, caso a crítica deva ser considerada
                //===================================================================================================================		
                if (statusLinha != ArquivoItemStatusEnum.Bloqueado &&
                    statusLinha != ArquivoItemStatusEnum.Excluido &&
                    statusLinha != ArquivoItemStatusEnum.PendenteDesbloqueio &&
                    statusLinha != ArquivoItemStatusEnum.Cancelado &&
                    vlinhacomcritica > 0)
                    quantidadeCriticas += vlinhacomcritica;
                //===================================================================================================================		
                // Persiste as críticas da linha das regras sem ser cancelamento parcial
                //===================================================================================================================		
                foreach (CriticaSCFArquivoInfo criticaArquivoInfo in requestValidarLinha.Criticas)
                {
                    criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                    criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                    ((CriticaSCFArquivoItemInfo)criticaArquivoInfo).CodigoArquivoItem = codigoArquivoItem;
                    Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
                }
                //===================================================================================================================		
                // TRATA O STATUS BLOQUEIO DO PROCESSO
                //===================================================================================================================		
                if (linhaBloqueada && !habloqueios) //1371
                {
                    habloqueios = true;
                    processo.StatusBloqueio = ProcessoStatusBloqueioEnum.HaBloqueios;
                    processo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processo));
                }
                //===================================================================================================================		
                // Se corrigiu a linha, salva o arquivoItem
                //===================================================================================================================		
                bool retornarRegistro = false;
                if (requestValidarLinha.ChaveItem != null)
                    ChaveArquivoItem = requestValidarLinha.ChaveItem;
                if (requestValidarLinha.LinhaCorrigida || requestValidarLinha.LinhaAtualizada) //1408
                {
                    conteudoArquivoItem = requestValidarLinha.Linha; //1396
                    Procedures.PR_ARQUIVO_ITEM_S
                    (    cmArquivoItemS
                        , codigoArquivoItem
                        , Convert.ToInt64(codigoArquivo)
                        , conteudoArquivoItem
                        , tipoLinha
                        , retornarRegistro
                        , statusLinha
                        , ChaveArquivoItem
                        , ReferenciaArquivoItem
                    );
                    if (requestValidarLinha.LinhaCorrigida)     //1408
                        vQtdLinhasCorrigidas++;
                }
                //===================================================================================================================		
                // GERA LOG DE VALIDACAO DE 15.000 EM 15.000
                //===================================================================================================================		
                if (codigoProcesso != null && quantidadeLog >= 15000)
                {
                    quantidadeLog = 0;
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - {0} linhas validadas - Qtd Critica {1} - Qtd Bloqueadas {2} - Qtd Atualizadas {3}", numeroLinha, quantidadeCriticas, vQtdLinhasBloqueadas, vQtdLinhasCorrigidas));
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
            }
            //===================================================================================================================		
            // FIM - LEITURA DO ARQUIVO TEXTO IMPORTADO
            //===================================================================================================================		
            // 1408 - EFETUA VALIDAÇÃO DO CANCELAMENTO PARCIAL
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Validando Cancelamento Parcial"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            vlinhacomcritica = 0;
            //===================================================================================================================		
            // Cancelamento Parcial Tratando
            //===================================================================================================================
            this.ValidarCancelamentoParcial
            (   arrayCancParcialDetail
            ,   arrayCancParcialValHeader
            ,   arrayCancParcialStsHeader
            ,   codigoArquivo
            ,   codigoProcesso
            ,   cmSalvarCritica
            ,   cmArquivoItemS //CH0000053039
            ,   ref vlinhacomcritica
            ,   ref vQtdLinhasBloqueadas);
            quantidadeCriticas += vlinhacomcritica;
            //===================================================================================================================		
            // Para as regras com escopo de arquivo, dispara Finalizacao
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Finalizando Validacoes de Arquivo"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            requestValidarArquivo.Criticas = new List<CriticaSCFArquivoInfo>();
            foreach (RegraSCFBase regra in regras.Where(r => r.Escopo == RegraSCFEscopoEnum.Arquivo))
                regra.FinalizarValidacaoArquivo(requestValidarArquivo);
            //===================================================================================================================		
            // Persiste as críticas de fim de arquivo
            //===================================================================================================================		
            foreach (CriticaSCFArquivoInfo criticaArquivoInfo in requestValidarArquivo.Criticas)
            {
                criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                quantidadeCriticas++;
                Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
            }
            //===================================================================================================================		
            // Ajusta os status dos arquivo itens
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Ajustando Status das Linhas"));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            Procedures.PR_ARQUIVO_ITEM_STATUS_A(codigoArquivo);
            //===================================================================================================================		
            // Contabiliza os totais gerando o log
            //===================================================================================================================		
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO FIM - Total de linhas validadas [{0}] Total Criticas [{1}] Total Bloqueadas [{2}] Total Atualizadas [{3}]", numeroLinha, quantidadeCriticas, vQtdLinhasBloqueadas, vQtdLinhasCorrigidas));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            //===================================================================================================================		
            // Finaliza os commands
            //===================================================================================================================		
            if (cn != null)
                cn.Close();
            GC.Collect(); // LIMPEZA DO MANAGED HEAP
            //===================================================================================================================		
            // Retorna
            //===================================================================================================================		
            return quantidadeCriticas;
        }

        private string substring(string p, int p_2, int p_3)
        {
            throw new NotImplementedException();
        }


        //===================================================================================================================		
        // CH0000053039 - Restruturando a logica para tratar bloqueio, desbloqueio e linhas invalidas mesmo que o valor somado fique OK
        //===================================================================================================================		
        protected void ValidarCancelamentoParcial(
              IDictionary<string, CancelamentoParcialStruct> arrayCancParcialDetail
            , IDictionary<string, double> arrayCancParcialValHeader
            , IDictionary<string, ArquivoItemStatusEnum> arrayCancParcialStsHeader
            , string codigoArquivo
            , string codigoProcesso
            , OracleCommand cmSalvarCritica
            , OracleCommand cmArquivoItemS // CH0000053039
            , ref int vlinhacomcritica
            , ref long vQtdLinhasBloqueadas) // CH0000053039
        {
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            string vDescricao = null;
            string HeaderSituacao = null;
            //===================================================================================================================		
            // Lendo o array pelo DETAIL e carregando informações do HEADER
            //===================================================================================================================
            foreach (string DetailCodigoArquivoItem in arrayCancParcialDetail.Keys)
            {
                //===================================================================================================================		
                // Movendo dados da transação de cancelamento parcial para variaveis
                //===================================================================================================================
                string DetailNSUHost = arrayCancParcialDetail[DetailCodigoArquivoItem].NSUHost;
                string DetailNSUHostOriginal = arrayCancParcialDetail[DetailCodigoArquivoItem].NSUHostOriginal;
                string DetailTipoTransacao = arrayCancParcialDetail[DetailCodigoArquivoItem].TipoTransacao;
                double DetailValorParcela = arrayCancParcialDetail[DetailCodigoArquivoItem].ValorParcela; 
                ArquivoItemStatusEnum DetailStatusLinhaCancParcial = arrayCancParcialDetail[DetailCodigoArquivoItem].StatusLinhaCancParcial; // carrega o status da transação para ver se subtrai da quatnidade de criticas
                //===================================================================================================================		
                // Carregando o VALOR e o STATUS da composição deste cancelamento parcial
                //===================================================================================================================
                ArquivoItemStatusEnum HeaderSts = arrayCancParcialStsHeader[DetailNSUHostOriginal]; // carrega o status da composição
                double HeaderVal = arrayCancParcialValHeader[DetailNSUHostOriginal]; // carrega o valor da composição

                //===================================================================================================================		
                // Consistindo se o VALOR da composição demonstra erro, isto é, sendo diferente de ZERO, se for gerar inconsistencia da composição
                //===================================================================================================================
                if (HeaderVal != 0) // VALOR DA COMPOSIÇÃO - NOK
                {
                    //===================================================================================================================		
                    // Gerando critica
                    //===================================================================================================================
                    vDescricao = string.Format("[Cancelamento parcial c/dif {0}] [NSU Host Original {1}] [Vlr Trn {2}] [Tip Trn {3}] [Host Trn {4}]", HeaderVal, DetailNSUHost, DetailValorParcela, DetailTipoTransacao, DetailNSUHost);
                    CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
                    {
                        Descricao = vDescricao,
                        TipoLinha = DetailTipoTransacao,
                        NomeCampo = "ValorBrutoVenda"
                    };
                    criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                    criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                    criticaArquivoInfo.CodigoArquivoItem = DetailCodigoArquivoItem;
                    Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
                    //===================================================================================================================		
                    // Atualizando o status da composição
                    //===================================================================================================================
                    if (HeaderSts != ArquivoItemStatusEnum.Bloqueado &&
                        HeaderSts != ArquivoItemStatusEnum.Excluido &&
                        HeaderSts != ArquivoItemStatusEnum.PendenteDesbloqueio &&
                        HeaderSts != ArquivoItemStatusEnum.Cancelado)
                    {
                        HeaderSts = ArquivoItemStatusEnum.Invalido;
                        vlinhacomcritica++;
                    }
                }
                //===================================================================================================================		
                // Linha do cancelamento parcial com status diferente da composicao
                //===================================================================================================================
                if (HeaderSts != DetailStatusLinhaCancParcial)
                {
                    //===================================================================================================================		
                    // Tirando a quantidade de critica do status da linha, se atender a regra abaixo
                    //===================================================================================================================
                    if (DetailStatusLinhaCancParcial != ArquivoItemStatusEnum.Bloqueado &&
                        DetailStatusLinhaCancParcial != ArquivoItemStatusEnum.Excluido &&
                        DetailStatusLinhaCancParcial != ArquivoItemStatusEnum.PendenteDesbloqueio &&
                        DetailStatusLinhaCancParcial != ArquivoItemStatusEnum.Cancelado)
                        vlinhacomcritica = vlinhacomcritica -1;
                    //===================================================================================================================		
                    // Somando a quantidade de bloqueios se a situacao da composicao for bloqueado
                    //===================================================================================================================
                    if (HeaderSts == ArquivoItemStatusEnum.Bloqueado)
                        vQtdLinhasBloqueadas++;
                    //===================================================================================================================		
                    // Somando a quantidade de critica do status da composicao, se atender a regra abaixo
                    //===================================================================================================================
                    if (HeaderSts != ArquivoItemStatusEnum.Bloqueado &&
                        HeaderSts != ArquivoItemStatusEnum.Excluido &&
                        HeaderSts != ArquivoItemStatusEnum.PendenteDesbloqueio &&
                        HeaderSts != ArquivoItemStatusEnum.Cancelado)
                        vlinhacomcritica++;
                    //===================================================================================================================		
                    // Sobrepondo o status da linha com o status da composição
                    //===================================================================================================================
                    DetailStatusLinhaCancParcial = HeaderSts;
                    //===================================================================================================================		
                    // Atualiza o status da linha na arquivo item
                    //===================================================================================================================
                    Procedures.PR_ARQUIVO_ITEM_S
                    (cmArquivoItemS
                        , DetailCodigoArquivoItem // codigo arquivo item
                        , Convert.ToInt64(codigoArquivo) // codigo arquivo
                        , null // conteudo arquivo item
                        , DetailTipoTransacao // tipo arquivo item
                        , false // retorna registro
                        , HeaderSts // status do arquivo item
                        , null  // chave
                        , null // referencia
                    );
                }
            }
            //===================================================================================================================		
            // HEADER  Lendo o array para efetuar LOG
            //===================================================================================================================		
            foreach (string HeaderNsuHostOriginal in arrayCancParcialValHeader.Keys)
            {
                HeaderSituacao = "OK";
                //===================================================================================================================		
                // Carregando o VALOR e o STATUS da composição
                //===================================================================================================================
                double HeaderVal = arrayCancParcialValHeader[HeaderNsuHostOriginal];
                ArquivoItemStatusEnum HeaderSts = arrayCancParcialStsHeader[HeaderNsuHostOriginal];
                //===================================================================================================================		
                // Consistindo se o VALOR da composição demonstra erro, isto é, sendo diferente de ZERO, se for gerar inconsistencia da composição
                //===================================================================================================================
                if (HeaderVal == 0) // VALOR DA COMPOSIÇÃO - OK
                {
                    //===================================================================================================================		
                    // Log de erro
                    //===================================================================================================================
                    if (HeaderSts != ArquivoItemStatusEnum.Valido) // STATUS DA COMPOSIÇÃO - NIOK
                        HeaderSituacao = "NOK - Alguma transação com critica";
                }
                else
                {
                    //===================================================================================================================		
                    // Log de erro e Atualizando o status da composição
                    //===================================================================================================================
                    HeaderSituacao = "NOK - Com diferença no valor";
                    if (HeaderSts != ArquivoItemStatusEnum.Bloqueado &&
                        HeaderSts != ArquivoItemStatusEnum.Excluido &&
                        HeaderSts != ArquivoItemStatusEnum.Cancelado)
                        HeaderSts = ArquivoItemStatusEnum.Invalido;
                }
                //===================================================================================================================		
                // Gerar LOG
                //===================================================================================================================
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("VALIDACAO - Validando Cancelamento Parcial - [NSU Host Original {0} Status Composição {1} -  {2}", HeaderNsuHostOriginal, HeaderSts, HeaderSituacao ));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            }
        }

        //===================================================================================================================		
        /// Retorna a quantidade de colunas do tipo de linha informado
        //===================================================================================================================		        
        public int ReceberQuantidadeColunas(string tipoLinha)
        {
            return this.OnReceberQuantidadeColunas(tipoLinha);
        }

        //===================================================================================================================		
        /// Método virtual para quantidade de colunas
        //===================================================================================================================		
        protected virtual int OnReceberQuantidadeColunas(string tipoLinha)
        {
            return 0;
        }

        //===================================================================================================================		
        /// Retorna os nomes dos campos do tipo de linha informado
        //===================================================================================================================		
        public string[] ReceberNomeCampos(string tipoLinha)
        {
            return this.OnReceberNomeCampos(tipoLinha);
        }

        //===================================================================================================================		
        /// Método virtual para receber nomes de campos
        //===================================================================================================================		
        protected virtual string[] OnReceberNomeCampos(string tipoLinha)
        {
            return null;
        }

        //===================================================================================================================		
        #endregion
        //===================================================================================================================		
        #region Verificação Cancelamento OnLine
        //===================================================================================================================		
        /// Faz a validação do arquivo com o conjunto de regras informadas
        //===================================================================================================================		
        public int ValidarCancelamentoOnLine(string codigoProcesso, string codigoArquivo)
        {
            return this.OnValidarCancelamentoOnLine(codigoProcesso, codigoArquivo); // Chama o método virtual
        }

        //===================================================================================================================		
        /// Faz a validação do arquivo com o conjunto de regras informadas   
        //===================================================================================================================
		//1391 - No atendimento deste chamado identifiquei que este processo não é usado
        // *** ATENÇÃO: As procedures mencionadas aqui PR_CANCELAMENTO_CV_AV e PR_CANCELAMENTO_CP_AP não existem ***
        //===================================================================================================================		
        protected int OnValidarCancelamentoOnLine(string codigoProcesso, string codigoArquivo)
        {
            OracleConnection cn = Procedures.ReceberConexao(); // Declare o reader para que possa ser corretamente finalizado
            OracleRefCursor cursor = null;
            //===================================================================================================================		
            // LOG DA FASE - CANCELAMENTO ONLINE
            //===================================================================================================================		            
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            //1391
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CANCELAMENTO ONLINE - Inicio - Codigo Arquivo [{0}]", codigoArquivo));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            //1391
            //===================================================================================================================		
            // AP
            //===================================================================================================================		            
            cursor =// Pede a lista de itens AP a serem verificados
                Procedures.PR_ARQUIVO_ITEM_L
                (cn, new PR_ARQUIVO_ITEM_L_Request()
                {
                    CodigoArquivo = codigoArquivo,
                    TipoArquivoItem = "AP",
                }
                );
            OracleDataReader readerAP = cursor.GetDataReader();
            int contadorAP = 0;
            int contadorAPTemp = 0;

            while (readerAP.Read()) // varre os APs para verificar se precisa de correção para os CPs
            {
                contadorAP++;
                contadorAPTemp++;

                string NSUHostString = ((string)LerCampo("AP", (object)readerAP["CONTEUDO_ARQUIVO_ITEM"], "NSUHostTransacaoOriginal", false)).Trim(); // extrai o NSU Host
                string DataTransacao = ((string)LerCampo("AP", (object)readerAP["CONTEUDO_ARQUIVO_ITEM"], "DataTransacao", false)).Trim(); // extrai a data

                PR_CANCELAMENTO_CP_AP_L_Request requestAP = new PR_CANCELAMENTO_CP_AP_L_Request()                 // chama a procedure para ajustar os valores, caso tenha
                {
                    CodigoArquivo = Convert.ToInt32(codigoArquivo),
                    NSU = NSUHostString,
                    Data = DataTransacao
                };
                Procedures.PR_CANCELAMENTO_CP_AP(requestAP);

                if (contadorAP >= 10) // Registra LOG de 10 em 10 registros
                {
                    contadorAP = 0;
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CANCELAMENTO ONLINE - {0} registros AP verificados", contadorAPTemp));
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
            }
            //===================================================================================================================		
            // LOG DA FASE - Anulacao Pagamento
            //===================================================================================================================		            
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CANCELAMENTO ONLINE - Fim {0} registros Anulacao Pagamento verificados", contadorAPTemp));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            cursor.Dispose(); // limpa o cursor
            cursor = null;
            //===================================================================================================================		
            // AV
            //===================================================================================================================		            
            cursor =
                Procedures.PR_ARQUIVO_ITEM_L
                (cn, new PR_ARQUIVO_ITEM_L_Request()
                {
                    CodigoArquivo = codigoArquivo,
                    TipoArquivoItem = "AV",
                }
                );
            OracleDataReader readerAV = cursor.GetDataReader();

            int contadorAV = 0;
            int contadorAVTemp = 0;
            while (readerAV.Read()) // varre os AVs para verificar se precisa de correção para os CVs
            {
                contadorAV++;
                contadorAVTemp++;

                string NSUHostString = ((string)LerCampo("AV", (object)readerAV["CONTEUDO_ARQUIVO_ITEM"], "NSUHostTransacaoOriginal", false)).Trim(); // extrai o NSU Host
                string DataTransacao = ((string)LerCampo("AV", (object)readerAV["CONTEUDO_ARQUIVO_ITEM"], "DataTransacao", false)).Trim(); // extrai a data

                PR_CANCELAMENTO_CV_AV_L_Request requestAV = new PR_CANCELAMENTO_CV_AV_L_Request() // chama a procedure para ajustar os valores, caso tenha
                {
                    CodigoArquivo = Convert.ToInt32(codigoArquivo),
                    NSU = NSUHostString,
                    Data = DataTransacao
                };
                Procedures.PR_CANCELAMENTO_CV_AV(requestAV);

                if (contadorAV >= 10) // Registra LOG de 10 em 10 registros tratados
                {
                    contadorAV = 0;
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CANCELAMENTO ONLINE - {0} registros AV verificados", contadorAVTemp));
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }

            }
            cursor.Dispose(); // limpa o cursor
            cursor = null;
            //===================================================================================================================		
            // LOG DA FASE - Anulacao Venda
            //===================================================================================================================		            
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CANCELAMENTO ONLINE - Fim {0} registros Anulacao Vendas verificados", contadorAVTemp));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            //===================================================================================================================		
            // LOG DA FASE - Total
            //===================================================================================================================		            
            int contagemTotal = contadorAPTemp + contadorAVTemp; // insere a contagem total
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CANCELAMENTO ONLINE - {0} registros totais verificados", contagemTotal));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            return 0;
        }
        //===================================================================================================================		
        #endregion
        //===================================================================================================================

    }
}