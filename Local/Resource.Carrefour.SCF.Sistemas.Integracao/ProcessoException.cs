﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Excessao de processo
    /// </summary>
    public class ProcessoException : Exception
    {
        public ProcessoException()
            : base()
        {
        }

        public ProcessoException(string message)
            : base(message)
        {
        }
    }
}
