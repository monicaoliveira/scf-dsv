﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Atributo de estagio
    /// </summary>
    public class EstagioAttribute : Attribute
    {
        /// <summary>
        /// Indica o tipo do processo a que este estágio se refere
        /// </summary>
        public Type TipoProcesso { get; set; }

        /// <summary>
        /// Indica o tipo do próximo estágio
        /// Caso seja nulo, assume estágio final
        /// </summary>
        public Type TipoProximoEstagio { get; set; }

        /// <summary>
        /// Indica o tipo do estágio anterior
        /// Caso seja nulo, assume estágio inicial
        /// </summary>
        public Type TipoEstagioAnterior { get; set; }
    }
}
