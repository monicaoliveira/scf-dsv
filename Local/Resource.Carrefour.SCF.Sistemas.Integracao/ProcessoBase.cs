﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Classe base para o processo
    /// </summary>
    public abstract class ProcessoBase
    {
        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Indica o estágio atual do processo
        /// </summary>
        public EstagioBase EstagioAtual { get; set; }

        /// <summary>
        /// Navega para proximo estagio
        /// </summary>
        public void IrProximoEstagio(string codigoSessao)
        {
            // Se o status do processo for diferente de EmAndamento, é erro
            if (this.Status != ProcessoStatusEnum.EmAndamento)
                throw new ProcessoException("Status do processo inválido para ir para próximo estágio");

            // Verifica se deve pedir a validação do estágio
            if (this.EstagioValido == null)
                this.EstagioValido = this.EstagioAtual.Validar(codigoSessao);

            // Verifica se pode prosseguir
            if (this.EstagioValido == true)
            {
                // Pede o processamento
                this.EstagioAtual.Processar(codigoSessao);

                // Vai para o proximo estagio
                this.criarEstagio("proximoEstagio", codigoSessao);
            }
        }

        /// <summary>
        /// Navega para estagio anterior
        /// </summary>
        public void IrEstagioAnterior(string codigoSessao)
        {
            // Se o status do processo for cancelado, é erro
            if (this.Status == ProcessoStatusEnum.Cancelado)
                throw new ProcessoException("Status do processo inválido para ir para estágio anterior");

            // Se o processo estiver finalizado, navega para o ultimo estagio
            // Navega para o estagio anterior
            if (this.Status == ProcessoStatusEnum.Finalizado)
                this.criarEstagio("ultimoEstagio", codigoSessao);
            else
                this.criarEstagio("estagioAnterior", codigoSessao);
        }

        /// <summary>
        /// Pede a validação do estágio
        /// </summary>
        public bool ValidarEstagio(string codigoSessao)
        {
            this.EstagioValido = this.EstagioAtual.Validar(codigoSessao);
            return this.EstagioValido.Value;
        }

        /// <summary>
        /// Indica se o estágio atual é valido para prosseguir
        /// </summary>
        public bool? EstagioValido { get; set; }

        /// <summary>
        /// Status do processo
        /// </summary>
        public ProcessoStatusEnum Status { get; set; }

        /// <summary>
        /// Método de inicialização do processo
        /// </summary>
        /// <param name="tipoProcesso"></param>
        /// <returns></returns>
        public void IniciarProcesso(string codigoSessao)
        {
            OnIniciarProcesso(codigoSessao);
        }

        /// <summary>
        /// Método virtual de inicialização do processo
        /// No comportamento padrão, acha o estágio inicial do processo, cria a instância
        /// e pede para inicializar
        /// </summary>
        protected virtual void OnIniciarProcesso(string codigoSessao)
        {
            // Cria o estagio inicial
            this.criarEstagio("estagioInicial", codigoSessao);

            // Seta o status
            this.Status = ProcessoStatusEnum.EmAndamento;
        }

        /// <summary>
        /// Procura pelo estagio solicitado
        /// </summary>
        /// <param name="tipoProcura"></param>
        /// <returns></returns>
        private void criarEstagio(string tipoProcura, string codigoSessao)
        {
            // Resseta flag de estagio validado
            this.EstagioValido = null;
            
            // Prepara o retorno
            Type tipoEstagio = null;
            
            // Acha o estagio inicial deste processo
            foreach (Type tipo in Assembly.GetExecutingAssembly().GetTypes())
            {
                // Verifica se tem o atributo
                EstagioAttribute estagioAttribute =
                    (EstagioAttribute)tipo.GetCustomAttributes(
                        typeof(EstagioAttribute), false).FirstOrDefault();

                // Se tem, verifica se é o estagio inicial deste processo
                if (estagioAttribute != null && estagioAttribute.TipoProcesso == this.GetType())
                {
                    // Processa de acordo com a solicitacao
                    switch(tipoProcura)
                    {
                        case "estagioInicial":
                            if (estagioAttribute.TipoEstagioAnterior == null)
                                tipoEstagio = tipo;
                            break;
                        case "ultimoEstagio":
                            if (estagioAttribute.TipoProximoEstagio == null)
                            {
                                // Atribui o estagio
                                tipoEstagio = tipo;

                                // Retorna o status do processo como ativo
                                this.Status = ProcessoStatusEnum.EmAndamento;
                            }
                            break;
                        case "proximoEstagio":
                            if (tipo == this.EstagioAtual.GetType())
                            {
                                // Atribui o estagio
                                tipoEstagio = estagioAttribute.TipoProximoEstagio;

                                // Se o tipo do estagio for nulo, chegou ao fim do processo
                                if (tipoEstagio == null)
                                    this.Status = ProcessoStatusEnum.Finalizado;
                            }
                            break;
                        case "estagioAnterior":
                            if (tipo == this.EstagioAtual.GetType())
                            {
                                // Atribui o estagio
                                tipoEstagio = estagioAttribute.TipoEstagioAnterior;

                                // Se o estagio for nulo, cancela o processo
                                if (tipoEstagio == null)
                                    this.Status = ProcessoStatusEnum.Cancelado;
                            }
                            break;
                    }
                }

                // Se achou retorno, sai do loop
                if (tipoEstagio != null)
                    break;
            }

            // Se achou o tipo...
            this.EstagioAtual = null;
            if (tipoEstagio != null)
            {
                // ...cria a instancia
                this.EstagioAtual = (EstagioBase)Activator.CreateInstance(tipoEstagio);
                this.EstagioAtual.Processo = this;

                // Inicializa ou cancela
                switch (tipoProcura)
                {
                    case "estagioInicial":
                    case "proximoEstagio":
                        this.EstagioAtual.Inicializar(codigoSessao);
                        break;
                    case "ultimoEstagio":
                    case "estagioAnterior":
                        this.EstagioAtual.Cancelar(codigoSessao);
                        break;
                }
            }
        }
    }
}
