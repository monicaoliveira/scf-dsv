﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    
    public class RegraSCFEspecificaCV : RegraSCFLinhaBase
    {
        #region Atributos da Regra

        #endregion

        #region Execução da Validação

        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
        // private Dictionary<string, PlanoInfo> _valores = null; // warning 25/02/2019
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha) 
            {
                request.Executou_Regra = true; //1407
                string numeroParcelaString = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroParcela", false)).Trim();
                string valorBrutoParcelaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoParcela", false)).Trim();
                string valorDescontoParcelaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDescontoParcela", false)).Trim();
                int numeroParcela = int.Parse(numeroParcelaString);
                double valorBrutoParcela = double.Parse(valorBrutoParcelaString);
                double valorDescontoParcela = double.Parse(valorDescontoParcelaString);
                string numeroTotalParcelasString = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroTotalParcelas", false)).Trim();// SCF1118 - Marcos Matsuoka - tratamento para numero de parcelas > total de parcelas - inicio
                int totalParcelas = int.Parse(numeroTotalParcelasString);
                if (numeroParcela > totalParcelas)
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = "NUMERO DA PARCELA > TOTAL DE PARCELAS",
                            TipoLinha = request.TipoLinha
                        });
                }
                if (numeroParcela > 0 && valorBrutoParcela == 0)
                {
                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("Número parcela ({0}) > 0 e valor bruto da parcela = 0", numeroParcela),
                                TipoLinha = request.TipoLinha,
                                NomeCampo = "ValorBrutoParcela" 
                            });
                }
                string valorLiquidoParcelaString = request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoParcela", false).ToString().Trim();
                double valorLiquidoParcela = double.Parse(valorLiquidoParcelaString);
                if (valorLiquidoParcela != valorBrutoParcela - valorDescontoParcela)
                {
                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("Valor líquido parcela ({0}) <> valor bruto parcela ({1}) - valor desconto parcela ({2})", valorLiquidoParcela, valorBrutoParcela, valorDescontoParcela),
                                TipoLinha = request.TipoLinha,
                                NomeCampo = "ValorLiquidoParcela"
                            });
                }
                string valorLiquidoVendaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoVenda", false)).Trim();
                string valorDescontoVendaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDesconto", false)).Trim();
                string valorBrutoVendaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoVenda", false)).Trim();
                double valorLiquidoVenda = double.Parse(valorLiquidoVendaString);
                double valorDescontoVenda = double.Parse(valorDescontoVendaString);
                double valorBrutoVenda = double.Parse(valorBrutoVendaString);
                if (valorLiquidoVenda != valorBrutoVenda - valorDescontoVenda)
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("VALOR LIQUIDO ({0}) <> VALOR BRUTO ({1}) - VALOR DESCONTO ({2})", valorLiquidoVenda, valorBrutoVenda, valorDescontoVenda),
                            NomeCampo = this.TipoLinha,
                            TipoLinha = request.TipoLinha
                        });
                }
                string tipoLancamento =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "TipoLancamento", false)).Trim();
                if (tipoLancamento == "3")
                {
                    if (((ArquivoTSYS)request.Arquivo).Layout.Equals("2") || ((ArquivoTSYS)request.Arquivo).Layout.Equals("3"))
                    {
                        string nsuHostTransacao =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacao", false)).Trim();
                        string nsuHostTransacaoOriginal =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacaoOriginal", false)).Trim();
                        string nsuHostTransacaoReversao =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacaoReversao", false)).Trim();

                        if (nsuHostTransacaoOriginal.Trim() == "" || nsuHostTransacaoOriginal.Equals("000000000000") || nsuHostTransacaoOriginal.Trim().Length != 12)
                        {
                            request.Criticas.Add(
                                new CriticaSCFEspecifica
                                {
                                    Descricao = string.Format("NSUHostTransacaoOriginal é um campo obrigatório para o Tipo de Lancamento = 3."),
                                    NomeCampo = "NSUHostTransacaoOriginal",
                                    TipoLinha = request.TipoLinha
                                });
                        }

                        if (nsuHostTransacaoReversao.Trim() == "" || nsuHostTransacaoReversao.Equals("000000000000") || nsuHostTransacaoReversao.Trim().Length != 12)
                        {
                            request.Criticas.Add(
                                new CriticaSCFEspecifica
                                {
                                    Descricao = string.Format("NSUHostTransacaoReversao é um campo obrigatório para o Tipo de Lancamento = 3."),
                                    NomeCampo = "NSUHostTransacaoReversao",
                                    TipoLinha = request.TipoLinha
                                });
                        }

                        if (nsuHostTransacao.Trim() == "" || nsuHostTransacao.Equals("000000000000") || nsuHostTransacao.Trim().Length != 12)
                        {
                            request.Criticas.Add(
                                new CriticaSCFEspecifica
                                {
                                    Descricao = string.Format("NSUHostTransacao é um campo obrigatório para o Tipo de Lancamento = 3."),
                                    NomeCampo = "NSUHostTransacao",
                                    TipoLinha = request.TipoLinha
                                });
                        }
                    }
                }
            }
        }
        #endregion
    }
}
