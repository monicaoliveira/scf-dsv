﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353
using Resource.Framework.Contratos.Comum;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFCorrecaoCP : RegraSCFLinhaBase
    {
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (request.TipoLinha == "L0")
            {
                this.DataMovimento = ((string)request.Arquivo.LerCampo("L0", (object)request.Linha, "DataMovimento", false));
                request.Executou_Regra = true; //1407
            }
            else if (request.TipoLinha == "A0")
            {
                this.IDMovimento = ((string)request.Arquivo.LerCampo("A0", (object)request.Linha, "IdMovimento", false));
                request.Executou_Regra = true; //1407
            }
            //================================================================================================================//
            //1408 else if (request.TipoLinha == this.TipoLinha)
            //Não efetuar correção da linha caso possua crítica;
            //================================================================================================================//
            else if (request.TipoLinha == this.TipoLinha && request.Criticas.Count == 0)
            {
                request.Executou_Regra = true; //1407
                string zeros = new string('0', 12);
                string novoNSU;
                string nsuTefTransacao = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUTEFTransacao", false));
                string nsuHostTransacao = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUHostTransacao", false));

                //1458 - inicio
                int seqMeioPagto = 0;
                string sequencialMeioPagamento = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "SequencialMeioPagamento", false));
                sequencialMeioPagamento = sequencialMeioPagamento.PadLeft(3, '0');
                if (sequencialMeioPagamento != null)
                {
                    seqMeioPagto = int.Parse(sequencialMeioPagamento); //sequencialMeioPagamento;
                }
                //1458 - fim

                if (nsuTefTransacao == zeros || nsuHostTransacao == zeros)
                {
                    if (seqMeioPagto <= 1) //1458
                    {
                        novoNSU = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);

                        if (nsuTefTransacao == zeros)
                        {
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUTEFTransacao", novoNSU);
                        }
                        if (nsuHostTransacao == zeros)
                        {
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacao", novoNSU);
                        }
                    }
                    else //1458
                    {
                        novoNSU = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, false);//1458

                        if (nsuTefTransacao == zeros)
                        {
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUTEFTransacao", novoNSU);
                        }
                        if (nsuHostTransacao == zeros)
                        {
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacao", novoNSU);
                        }
                    }
                    request.LinhaCorrigida = true;
                }
            }
        }
    }
}
