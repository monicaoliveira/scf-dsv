﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353
using Resource.Framework.Contratos.Comum; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFString : RegraSCFColunaBase
    {
        public bool EhObrigatorio { get; set; }
        public string IgualConstante { get; set; }
        public string DiferenteConstante { get; set; }

        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                string valor = ((string) request.Arquivo.LerCampo( request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                if (EhObrigatorio && valor == "")   // OBRIGATORIO MAS NÃO ESTA PREENCHIDO - Critica
                {
                    request.Criticas.Add(
                        new CriticaSCFObrigatoriedade
                            {
                                CodigoArquivo = request.CodigoArquivo,
                                CodigoArquivoItem = request.CodigoArquivoItem,
                                CampoNaoPreenchido = true,
                                Valor = valor ,
                                NomeCampo = this.NomeCampo,
                                TipoLinha = request.TipoLinha
                            }
                        );

                }
                else
                {
                    if (IgualConstante != null && IgualConstante != valor) // PREENCHIDO MAS NÃO É IGUAL A CONSTANTE
                        request.Criticas.Add(
                            new CriticaSCFConstante
                            {
                                CodigoArquivo = request.CodigoArquivo,
                                CodigoArquivoItem = request.CodigoArquivoItem,
                                TipoValidacaoConstante = TipoValidacaoConstanteEnum.Igual,
                                Valor = valor,
                                NomeCampo = this.NomeCampo + '=' + IgualConstante, //1353
                                TipoLinha = request.TipoLinha
                            }
                        );

                    if (DiferenteConstante != null && DiferenteConstante == valor) // PREENCHIDO MAS É IGUAL A DIFERENTE CONSTANTE
                        request.Criticas.Add(
                            new CriticaSCFConstante
                            {
                                CodigoArquivo = request.CodigoArquivo,
                                CodigoArquivoItem = request.CodigoArquivoItem,
                                TipoValidacaoConstante = TipoValidacaoConstanteEnum.Diferente,
                                Valor = valor,
                                NomeCampo = this.NomeCampo + '/' + DiferenteConstante, //1353
                                TipoLinha = request.TipoLinha
                            }
                        );                  
                }
                
            }
        }
    }

}
