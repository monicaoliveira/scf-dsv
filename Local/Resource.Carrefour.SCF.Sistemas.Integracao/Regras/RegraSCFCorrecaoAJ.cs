﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353
using Resource.Framework.Contratos.Comum;


namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFCorrecaoAJ : RegraSCFLinhaBase
    {
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            
            
            if (request.TipoLinha == "L0")
            {   this.DataMovimento = ((string)request.Arquivo.LerCampo("L0", (object)request.Linha, "DataMovimento", false));
                request.Executou_Regra = true; //1407                   
            }
            else if (request.TipoLinha == "A0")
            {   this.IDMovimento = ((string)request.Arquivo.LerCampo("A0", (object)request.Linha, "IdMovimento", false));
                request.Executou_Regra = true; //1407                   
            }
            //================================================================================================================//
            //1408 else if (request.TipoLinha == this.TipoLinha)
            //Não efetuar correção da linha caso possua crítica;
            //================================================================================================================//
            else if (request.TipoLinha == this.TipoLinha && request.Criticas.Count == 0)
            {
                request.Executou_Regra = true; //1407                
                string nsuHostTransacao = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUHostTransacao", false));
                string nsuHostTransacaoOriginal = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUHostTransacaoOriginal", false));
                string tipoLancamento = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "TipoLancamento", false)).Trim();
                string novoNSUHost;
                string zeros = new string('0', 12);

                if ((nsuHostTransacao == zeros || nsuHostTransacaoOriginal == zeros) && !tipoLancamento.Equals("5"))
                {
                    //SCF 1473
                    //if (isHomologacaoTSYS)
                    //{
                    //    request.Criticas.Add(
                    //        new CriticaSCFObrigatoriedade()
                    //        {
                    //            CodigoArquivo = request.CodigoArquivo,
                    //            CodigoArquivoItem = request.CodigoArquivoItem,
                    //            CampoNaoPreenchido = true,
                    //            NomeCampo = "NSUHost",
                    //            TipoLinha = request.TipoLinha
                    //        });
                    //}

                    //else
                    //{
                        if (nsuHostTransacao == zeros) // SCF1088 - Marcos Matsuoka 
                        {
                            novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false); 
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacao", novoNSUHost);
                            request.LinhaCorrigida = true;
                        }
                        if (nsuHostTransacaoOriginal == zeros) // SCF1088 - Marcos Matsuoka 
                        {
                            novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacaoOriginal", novoNSUHost);
                            request.LinhaCorrigida = true; //scf1303 desinibindo
                        }
                    //SCF 1473
                    //}
                }
            }
        }
    }
}
