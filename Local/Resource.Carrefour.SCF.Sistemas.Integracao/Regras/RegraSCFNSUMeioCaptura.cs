﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFNSUMeioCaptura : RegraSCFColunaBase
    {
        #region Atributos da Regra
        public int Tamanho { get; set; }

        #endregion

        #region Execução da Validação
        // private List<string> _valores = null; // warning 25/02/2019
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
        
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha)
            {   request.Executou_Regra = true; //1407
                string valor = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                string meioCaptura =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "MeioCaptura", false)).Trim();
                if (meioCaptura != "5" && meioCaptura != "8")
                {
                    if (valor == "0".PadRight(Tamanho, '0') || valor == "")
                    {
                        request.Criticas.Add(
                            //1262
                                //new CriticaSCFMeioCaptura
                                //{
                                //    Valor = valor,
                                //    NomeCampo = this.NomeCampo,
                                //    TipoLinha = request.TipoLinha
                                //}
                            //1262
	                            new CriticaSCFDominio()
	                            {
		                            CodigoArquivo = request.CodigoArquivo,
		                            CodigoArquivoItem = request.CodigoArquivoItem,
		                            ValorNaoEncontrado = valor,
		                            NomeCampo = this.NomeCampo,
		                            TipoLinha = request.TipoLinha
	                            }
                         );
                    }
                }
            }
        }
        #endregion
    }
}
