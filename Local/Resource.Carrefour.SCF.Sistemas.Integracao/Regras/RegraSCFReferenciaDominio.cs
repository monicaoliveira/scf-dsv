﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFReferenciaDominio : RegraSCFColunaBase
    {
        public ListaItemReferenciaNomeEnum NomeReferencia { get; set; }
        public ListaItemReferenciaSituacaoEnum SituacaoReferencia { get; set; }
        private Dictionary<string, ListaItemReferenciaInfo> Referencias;

        // ======================================================================================================
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {   Referencias = new Dictionary<string, ListaItemReferenciaInfo>();
            // ======================================================================================================
            // lista de condicoes
            // ======================================================================================================
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
            condicoes.Add( new CondicaoInfo( "CodigoListaItemSituacao", CondicaoTipoEnum.Igual, SituacaoReferencia.ToString()));
            condicoes.Add( new CondicaoInfo( "NomeReferencia", CondicaoTipoEnum.Igual, NomeReferencia.ToString()));
            // ======================================================================================================            
            // preenche a lista de referências
            // ======================================================================================================           
            List<ListaItemReferenciaInfo> listaReferencias = PersistenciaHelper.Listar<ListaItemReferenciaInfo>(null, condicoes);
            // ======================================================================================================            
            // Monta o dicionario de retorno
            // ======================================================================================================            
            foreach (ListaItemReferenciaInfo listaItemReferenciaInfo in listaReferencias)
                if (!Referencias.ContainsKey(listaItemReferenciaInfo.listaItemInfo.Valor))
                    Referencias.Add(listaItemReferenciaInfo.listaItemInfo.Valor, listaItemReferenciaInfo);
        }
        // ======================================================================================================


        // ======================================================================================================
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                string valor = ((string) request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                if (!Referencias.ContainsKey(valor))
                {   request.Criticas.Add( new CriticaSCFReferenciaDominio()      
                    {       TipoLinha           = request.TipoLinha
                    ,       NomeCampo           = this.NomeCampo
                    ,       ValorNaoEncontrado  = valor
                    ,       SituacaoReferencia  = SituacaoReferencia
                    ,       NomeReferencia      = NomeReferencia
                    });
                }
            }
        }
    }
}
