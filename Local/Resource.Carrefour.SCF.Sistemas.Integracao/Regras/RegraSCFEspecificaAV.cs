﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFEspecificaAV : RegraSCFLinhaBase
    {
        #region Atributos da Regra
        #endregion

        #region Execução da Validação
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }

        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                string numeroParcelaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroParcela", false)).Trim();
                string numeroTotalParcelasString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroTotalParcelas", false)).Trim();
                int parcela = int.Parse(numeroParcelaString);
                int totalParcelas = int.Parse(numeroTotalParcelasString);
                if (parcela > totalParcelas)
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = "Numero parcela maior que o TOTAL de parcelas",
                            TipoLinha = request.TipoLinha
                        });
                }
                string valorBrutoParcelaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoParcela", false)).Trim();
                double valorBrutoParcela = double.Parse(valorBrutoParcelaString);
                if (parcela > 0 && valorBrutoParcela == 0)
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("Numero de Parcela maior que ZEROS, mas Valor Bruto Parcela iqual a ZEROS", parcela),
                            TipoLinha = request.TipoLinha
                        });
                }
                string valorLiquidoParcelaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoParcela", false)).Trim();
                string valorDescontoParcelaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDescontoParcela", false)).Trim();
                double valorLiquidoParcela = double.Parse(valorLiquidoParcelaString);
                double valorDescontoParcela = double.Parse(valorDescontoParcelaString);
                if (valorLiquidoParcela != valorBrutoParcela - valorDescontoParcela)
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("Valor Liquido Parcela Invalido.[ PARCELA: Valor Liqudio ({0}) <> ( Valor Bruto ({1}) - Valor Deconto ({2}) )", valorLiquidoParcela, valorBrutoParcela, valorDescontoParcela),
                            TipoLinha = request.TipoLinha
                        });
                }
                string valorLiquidoVendaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoVenda", false)).Trim();
                string valorDescontoVendaString = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDescontoOuComissao", false)).Trim();//SCF-1118 - Marcos Matsuoka - estava ValorDescontoParcela - inicio
                string valorBrutoVendaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoVenda", false)).Trim();
                double valorLiquidoVenda = double.Parse(valorLiquidoVendaString);
                double valorDescontoVenda = double.Parse(valorDescontoVendaString);
                double valorBrutoVenda = double.Parse(valorBrutoVendaString);
                if (valorLiquidoVenda != valorBrutoVenda - valorDescontoVenda)
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("Valor Liquido Invalido. [TRANSACAO: Valor Liquido {0}) <> (Valor Bruto ({1}) - Valor Desconto ({2}) )", valorLiquidoVenda, valorBrutoVenda, valorDescontoVenda),
                            NomeCampo = this.TipoLinha,
                            TipoLinha = request.TipoLinha
                        });
                }
                string tipoLancamento =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "TipoLancamento", false)).Trim();
                if (tipoLancamento == "3")
                {
                    string nsuHostTransacao =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacao", false)).Trim();
                    string nsuHostTransacaoOriginal =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacaoOriginal", false)).Trim();

                    if (nsuHostTransacaoOriginal.Trim() == "" || nsuHostTransacaoOriginal.Equals("000000000000") || nsuHostTransacaoOriginal.Trim().Length != 12)
                    {
                        request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("NSU HOST da Transacao Original é campo obrigatório para o Tipo de Lancamento = 3."),
                                NomeCampo = "NSUHostTransacaoOriginal",
                                TipoLinha = request.TipoLinha
                            });
                    }

                    if (nsuHostTransacao.Trim() == "" || nsuHostTransacao.Equals("000000000000") || nsuHostTransacao.Trim().Length != 12)
                    {
                        request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("NSU HOST da Transacao é campo obrigatório para o Tipo de Lancamento = 3."),
                                NomeCampo = "NSUHostTransacao",
                                TipoLinha = request.TipoLinha
                            });
                    }
                }
                string codigoAnulacaoArquivo = (string)request.Arquivo.LerCampo("AV", request.Linha, "CodigoAnulacao", true);
                //string meioPagamentoBase = ""; // warning 25/02/2019
                //string nomemeioPagamentoBase = ""; // warning 25/02/2019
                string produtoAnulacaoBase = "";
                foreach (ListaItemReferenciaInfo listaItemRefInfo in request.listaItemReferenciaInfo)
                {
                    //1367 - INIBINDO CONFORME SOLICITADO PELO CARREFOUR NA HOMOLOGACAO
                    //if (listaItemRefInfo.CodigoListaItemSituacao == ListaItemReferenciaSituacaoEnum.CodigoAnulacao.ToString() && listaItemRefInfo.NomeReferencia == ListaItemReferenciaNomeEnum.MeioPagamento.ToString()
                    //    && codigoAnulacaoArquivo.Trim() == listaItemRefInfo.listaItemInfo.Valor.Trim())
                    //{
                    //    meioPagamentoBase = listaItemRefInfo.ValorReferencia.Trim();
                    //    nomemeioPagamentoBase = listaItemRefInfo.listaItemInfo.Descricao.Trim();
                    //}
                    // SFC1340 - Validação do Produto do AV
                    if (listaItemRefInfo.CodigoListaItemSituacao == ListaItemReferenciaSituacaoEnum.CodigoAnulacao.ToString() && listaItemRefInfo.NomeReferencia == ListaItemReferenciaNomeEnum.Produto.ToString()
                        && codigoAnulacaoArquivo.Trim() == listaItemRefInfo.listaItemInfo.Valor.Trim())
                        produtoAnulacaoBase = listaItemRefInfo.ValorReferencia.Trim();
                }
                //1367 - INIBINDO CONFORME SOLICITADO PELO CARREFOUR NA HOMOLOGACAO
                // SCF1340 - Aplicar regra de Meio de Pagamento               
                //if (string.IsNullOrEmpty(meioPagamentoBase))
                //{
                //    request.Criticas.Add(
                //        new CriticaSCFEspecifica
                //        {
                //            Descricao = string.Format("Codigo de anulacao ({0}) sem meio de pagamento associado no sistema.", codigoAnulacaoArquivo),
                //            NomeCampo = "CodigoAnulacao",
                //            TipoLinha = request.TipoLinha
                //        });
                //}                

                // SCF1340 - Aplicar regra de Produto                
                if (string.IsNullOrEmpty(produtoAnulacaoBase))
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("Codigo Anulacao [{0}] sem produto associado.", codigoAnulacaoArquivo),
                            NomeCampo = "CodigoAnulacao",
                            TipoLinha = request.TipoLinha
                        });
                }
                else
                {
                    request.NomeProduto = produtoAnulacaoBase;
                }
            }
        }
    }
        #endregion
}
