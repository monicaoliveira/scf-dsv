﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFValorMaiorQue : RegraSCFColunaBase
    {
        #region Atributos da Regra
        public string NomeCampo2 { get; set; }
        #endregion

        #region Execução da Validação
        // private List<string> _valores = null; // warning 25/02/2019

        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
        
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   if (this.TipoLinha == request.TipoLinha) 
            {
                request.Executou_Regra = true; //1407
                string valorString = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                string valorString2 =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo2, false)).Trim();

                double valor = double.Parse(valorString);
                double valor2 = double.Parse(valorString2);
                if (valor > valor2 )
                {

                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                TipoLinha = this.TipoLinha,
                                Descricao = this.NomeCampo + " é maior que " + this.NomeCampo2
                            }
                     );
                }
            }
        }

        #endregion
    }
}
