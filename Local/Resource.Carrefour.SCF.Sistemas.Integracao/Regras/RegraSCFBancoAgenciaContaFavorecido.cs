﻿// ===================================================================================
// SCF1340 - Reutilizando para esta tarefa, antes não era chamada
// ===================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

//=========================================================================================================================01
namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    //=========================================================================================================================02
    public class RegraSCFBancoAgenciaContaFavorecido : RegraSCFColunaBase
    {
        //=========================================================================================================================03
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
        //=========================================================================================================================03
        //=========================================================================================================================04
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            //=========================================================================================================================
            // Apenas para o tipo da linha desejado
            //=========================================================================================================================
            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true;  //1407

                int codEstabelecimento = Convert.ToInt32(request.CodigoEstabelecimento);
                int codproduto = Convert.ToInt32(request.CodigoProduto);
                string vReferencia = request.Referencia;
                string nomeEstabelecimento = request.NomeEstabelecimento;
                string nomeproduto = request.NomeProduto;
                string CNPJ = request.CNPJEstabelecimento;

                int codContaFavorecido = 0;
                int codFavorecido = 0;
                int retorno = 0;
                string codBanco = "";   // SCF1341
                string codAgencia = ""; // SCF1341
                string codConta = "";   // SCF1341
                string nomFav = "";   // SCF1353
                string CNPJFav = "";   // SCF1353
                
                //=========================================================================================================================
                // faz a pesquisa na procedure
                //=========================================================================================================================
                Procedures.PR_TRATAR_FAVORECIDO
                (       
                        out nomFav         //1353
                ,       out CNPJFav        //1353
                ,       out codContaFavorecido
                ,       out codFavorecido
                ,       out retorno
                ,       out codBanco        // SCF1341
                ,       out codAgencia      // SCF1341
                ,       out codConta        // SCF1341
                ,       codproduto
                ,       codEstabelecimento
                ,       vReferencia
                );           //SCF1340

                if (retorno == 1)
                {
                    request.ChaveItem = "";
                    if (codFavorecido != 0)     //1353
                    {   request.Criticas.Add(
                        //1282 new CriticaSCFDominio()
                        new CriticaSCFEspecifica() //1282
                        {
                            CodigoArquivo = request.CodigoArquivo
                        ,   CodigoArquivoItem = request.CodigoArquivoItem
                        //1262 ,   ValorNaoEncontrado = codEstabelecimento.ToString()
                        ,   NomeCampo = this.NomeCampo
                        ,   TipoLinha = request.TipoLinha
                        ,   Descricao = string.Format("Favorecido ({0}-{1}) x Produto ({2}-{3}) x Referencia ({4}) sem associacao no sistema", CNPJFav, nomFav, codproduto, nomeproduto, vReferencia)
                        });
                    }
                    else
                    {   
                        //1463//request.ChaveItem = "";
                        ////request.Criticas.Add(
                        //////1282 new CriticaSCFDominio()
                        ////new CriticaSCFEspecifica() //1282
                        ////{
                        ////    CodigoArquivo = request.CodigoArquivo
                        ////,   CodigoArquivoItem = request.CodigoArquivoItem
                        //////1262,   ValorNaoEncontrado = codEstabelecimento.ToString()
                        ////,   NomeCampo = this.NomeCampo
                        ////,   TipoLinha = request.TipoLinha
                        ////,
                        ////    Descricao = string.Format("Estabelecimento ({0}-{1}) e Produto ({2}-{3}) e Referencia ({4}) sem cadastro de Favorecido no sistema", CNPJ, nomeEstabelecimento, codproduto, nomeproduto, vReferencia)
                        ////});
                    }
                }
                else
                {
                    request.ChaveItem = codBanco + codAgencia + codConta;
                    request.CNPJFavorecido = CNPJFav; //1353
                    request.NomeFavorecido = nomFav;//1353
                    //1408 request.LinhaCorrigida = true;
                    request.LinhaAtualizada = true; //1408
                }
            }
        }
        //=========================================================================================================================04
    }
    //=========================================================================================================================02
}
//=========================================================================================================================01
