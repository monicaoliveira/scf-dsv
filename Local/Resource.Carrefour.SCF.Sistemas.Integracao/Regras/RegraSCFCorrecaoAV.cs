﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353
using Resource.Framework.Contratos.Comum; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFCorrecaoAV : RegraSCFLinhaBase
    {
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   if (request.TipoLinha == "L0")
            {
                this.DataMovimento = ((string)request.Arquivo.LerCampo("L0", (object)request.Linha, "DataMovimento", false));
                request.Executou_Regra = true; //1407
            }
            else if (request.TipoLinha == "A0")
            {
                this.IDMovimento = ((string)request.Arquivo.LerCampo("A0", (object)request.Linha, "IdMovimento", false));
                request.Executou_Regra = true; //1407
            }
            //================================================================================================================//
            //1408             else if (request.TipoLinha == this.TipoLinha)
            //Não efetuar correção da linha caso possua crítica;
            //================================================================================================================//
            else if (request.TipoLinha == this.TipoLinha && request.Criticas.Count == 0)
            {
                request.Executou_Regra = true; //1407
                string tipoLancamento = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "TipoLancamento", false));

                if (!tipoLancamento.Equals("3"))
                {
                    string nsuHostTransacaoOriginal = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUHostTransacaoOriginal", false));
                    string nsuTefTransacaoOriginal = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUTefTransacaoOriginal", false));
                    string nsuHostTransacao = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUHostTransacao", false));

                    int numeroTotalParcelas = ((int)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroTotalParcelas", true));
                    int numeroParcela = ((int)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroParcela", true));

                    bool primeiraParcelaHost = true;
                    bool primeiraParcelaTef = true;
                    string zeros = new string('0', 12);
                    string novoNSUHostOriginal = nsuHostTransacaoOriginal;
                    string novoNSUTefOriginal = nsuTefTransacaoOriginal;
                    string novoNSUHost = nsuHostTransacao; // SCF1036 - Marcos Matsuoka

                    if (nsuHostTransacaoOriginal == zeros)
                    {
                        //SCF 1473
                        //if (isHomologacaoTSYS)
                        //{
                        //    request.Criticas.Add(
                        //        new CriticaSCFObrigatoriedade()
                        //        {
                        //            CodigoArquivo = request.CodigoArquivo,
                        //            CodigoArquivoItem = request.CodigoArquivoItem,
                        //            CampoNaoPreenchido = true,
                        //            NomeCampo = "NSUHost",
                        //            TipoLinha = request.TipoLinha
                        //        });
                        //}
                        //else
                        //{
                            if (numeroTotalParcelas == 0)
                            {
                                novoNSUHostOriginal = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                            }
                            else if (numeroParcela == 0)
                            {
                                novoNSUHostOriginal = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                                primeiraParcelaHost = false;
                            }
                            else if (numeroParcela == 1 && primeiraParcelaHost)
                            {
                                novoNSUHostOriginal = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                                primeiraParcelaHost = false;
                            }
                            else if (numeroParcela <= numeroTotalParcelas)
                            {
                                novoNSUHostOriginal = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, false);
                            }
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacaoOriginal", novoNSUHostOriginal);
                            request.LinhaCorrigida = true;
                        //SCF 1473
                        //}
                    }

                    if (nsuHostTransacao == zeros)
                    {
                        //SCF 1473
                        //if (isHomologacaoTSYS)
                        //{
                        //    request.Criticas.Add(
                        //        new CriticaSCFObrigatoriedade()
                        //        {
                        //            CodigoArquivo = request.CodigoArquivo,
                        //            CodigoArquivoItem = request.CodigoArquivoItem,
                        //            CampoNaoPreenchido = true,
                        //            NomeCampo = "NSUHost",
                        //            TipoLinha = request.TipoLinha
                        //        });
                        //}
                        //else
                        //{
                            if (numeroTotalParcelas == 0)
                                novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                            else if (numeroParcela == 0)
                            {
                                novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                                primeiraParcelaHost = false;
                            }

                            else if (numeroParcela == 1 && primeiraParcelaHost)
                            {
                                novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                                primeiraParcelaHost = false;
                            }

                            else if (numeroParcela <= numeroTotalParcelas)
                                novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, false);

                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacao", novoNSUHost);
                            request.LinhaCorrigida = true;
                    //SCF 1473
                        //}
                    }
                    if (nsuTefTransacaoOriginal == zeros)
                    {
                        //SCF 1473
                        //if (isHomologacaoTSYS)
                        //{
                        //    request.Criticas.Add(
                        //        new CriticaSCFObrigatoriedade()
                        //        {
                        //            CodigoArquivo = request.CodigoArquivo,
                        //            CodigoArquivoItem = request.CodigoArquivoItem,
                        //            CampoNaoPreenchido = true,
                        //            NomeCampo = "NSUHost",
                        //            TipoLinha = request.TipoLinha
                        //        });
                        //}

                        //else
                        //{
                            if (numeroTotalParcelas == 0)
                            {
                                novoNSUTefOriginal = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, true);
                            }
                            else
                            {
                                if (numeroParcela == 0 && nsuHostTransacaoOriginal != zeros)
                                {
                                    novoNSUTefOriginal = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, true);
                                    primeiraParcelaTef = false;
                                }

                                else if (numeroParcela == 1 && primeiraParcelaTef && nsuHostTransacaoOriginal != zeros)
                                {
                                    novoNSUTefOriginal = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, true);
                                    primeiraParcelaTef = false;
                                }

                                else if (numeroParcela <= numeroTotalParcelas)
                                {
                                    novoNSUTefOriginal = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, false);
                                }
                            }

                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUTefTransacaoOriginal", novoNSUTefOriginal);
                            request.LinhaCorrigida = true;
                        //SCF 1473
                        //}
                    }
                }
            }
        }
    }
}
