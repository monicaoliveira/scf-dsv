﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

//=========================================================================================================================================01
namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    //=========================================================================================================================================02
    public class RegraSCFTratarFavorecido   //1185
    {
        private Dictionary<string, int> _dicionarioEstabelecimento = null;
        private Dictionary<string, int> _dicionarioProduto = null;
        //=========================================================================================================================================03
        public RegraSCFTratarFavorecido()
        {
        }
        //=========================================================================================================================================03
        //=========================================================================================================================================04
        public RegraSCFTratarFavorecido(Dictionary<string, int> dicionarioEstabelecimento, Dictionary<string, int> dicionarioProduto)
        {
            this._dicionarioEstabelecimento = dicionarioEstabelecimento;
            this._dicionarioProduto = dicionarioProduto;
        }
        //=========================================================================================================================================04
        //=========================================================================================================================================05
        public void Validar(string produto, string estabelecimento, string codArqItem, string codigoArquivo, string codigoProcesso, string codigoSessao, string nsuHost, OracleCommand cmSalvarCritica, ref int quantidadeCriticas, string vReferencia) //1339
        {
// 1407 nao utilizada esta regra
//            request.Executou_Regra = true; //1407
//            int codContaFavorecido = 0;
//            int codFavorecido = 0;
//            int codEstabelecimento = 0;
//            int codproduto = 0;
//            int retorno = 0;
//            string razaoSocial = "";

//            //=========================================================================================================================================
//            codEstabelecimento = (!_dicionarioEstabelecimento.ContainsKey(estabelecimento)) ? 0 : _dicionarioEstabelecimento[estabelecimento];
//            if (codEstabelecimento != 0)
//            {
//                EstabelecimentoInfo razao = PersistenciaHelper.Receber<EstabelecimentoInfo>(codigoSessao, Convert.ToString(codEstabelecimento));                    
//                razaoSocial = razao.RazaoSocial;
//                codproduto = (!_dicionarioProduto.ContainsKey(produto)) ? 0 : _dicionarioProduto[produto];
//            }
//            else 
//            {
//                CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
//                {
//                    Descricao = string.Format("Não foi encontrado código de estabelecimento com o CNPJ ({0}).", estabelecimento),
//                };
//                criticaArquivoInfo.CodigoProcesso = codigoProcesso;
//                criticaArquivoInfo.CodigoArquivo = codigoArquivo;
//                criticaArquivoInfo.CodigoArquivoItem = codArqItem;
//                Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
//                quantidadeCriticas++;
//            }
//            //=========================================================================================================================================
//            if (codproduto == 0)
//            {
//                CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
//                {
//                    Descricao = string.Format("Não foi encontrado código de produto ({0}) para o produto ({1}) do Estabelecimento ({2}).", codproduto, produto, estabelecimento),
//                };
//                criticaArquivoInfo.CodigoProcesso = codigoProcesso;
//                criticaArquivoInfo.CodigoArquivo = codigoArquivo;
//                criticaArquivoInfo.CodigoArquivoItem = codArqItem;
//                Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);

//                quantidadeCriticas++;
//            }
//            //=========================================================================================================================================
//            if (codEstabelecimento != 0 && codproduto != 0)
//            {   string codBanco = "";   // SCF1341
//                string codAgencia = ""; // SCF1341
//                string codConta = "";   // SCF1341
//                string nomFav = "";   // SCF1353
//                string CNPJFav = "";   // SCF1353
//                //=========================================================================================================================
//                // procura banco, agencia e conta pelo estabelecimento x produto x referencia
//                //=========================================================================================================================
//                Procedures.PR_TRATAR_FAVORECIDO
//                ( out nomFav         //1353
//                , out CNPJFav        //1353
//                , out codContaFavorecido
//                , out codFavorecido
//                , out retorno
//                , out codBanco        // SCF1341
//                , out codAgencia      // SCF1341
//                , out codConta        // SCF1341
//                , codproduto
//                , codEstabelecimento
//                , vReferencia
//                );           //SCF1340
//                if (retorno == 1)
//                {   if (codFavorecido != 0)     //1353
//                    {   CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
//                        {
//                            Descricao = string.Format("Favorecido ({0}-{1}) x Produto ({2}) x Referencia ({3}) sem associacao no sistema.", CNPJFav, nomFav, codproduto, vReferencia)
//                        };
//                        criticaArquivoInfo.CodigoProcesso = codigoProcesso;
//                        criticaArquivoInfo.CodigoArquivo = codigoArquivo;
//                        criticaArquivoInfo.CodigoArquivoItem = codArqItem;
//                        Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);

//                        quantidadeCriticas++;
//                    }
//                    else
//                    {   CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
//                        {
//                            Descricao = string.Format("Estabelecimento ({0}) e Produto ({1}) e Referencia ({2}) sem cadastro de Favorecido no sistema.", estabelecimento, codproduto, vReferencia)
//                        };
//                        criticaArquivoInfo.CodigoProcesso = codigoProcesso;
//                        criticaArquivoInfo.CodigoArquivo = codigoArquivo;
//                        criticaArquivoInfo.CodigoArquivoItem = codArqItem;
//                        Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);

//                        quantidadeCriticas++;
//                    }
//                }
//            }
        }
        //=========================================================================================================================================05
    }
    //=========================================================================================================================================02
}
//=========================================================================================================================================01
