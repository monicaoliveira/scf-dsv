﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    /// <summary>
    /// Regra para validação de domínios
    /// </summary>
    public class RegraSCFValorMaiorIgualQue : RegraSCFColunaBase
    {
        #region Atributos da Regra

        /// <summary>
        /// Campo a ser comparado
        /// </summary>
        public string NomeCampo2 { get; set; }


        #endregion

        #region Execução da Validação

        /// <summary>
        /// Lista de valores a serem validados
        /// </summary>
        private List<string> _valores = null;

        /// <summary>
        /// Prepara a regra
        /// </summary>
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {


        }

        /// <summary>
        /// Faz a validação da linha
        /// </summary>
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            // Apenas para o tipo da linha desejado
            if (this.TipoLinha == request.TipoLinha)
            {
                // Extrai o valor
                string valorString =
                    ((string)
                        request.Arquivo.LerCampo(
                            request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();

                // Vai pegar o valor do campo 2
                string valorString2 =
                    ((string)
                        request.Arquivo.LerCampo(
                            request.TipoLinha, (object)request.Linha, this.NomeCampo2, false)).Trim();

                double valor = double.Parse(valorString);
                double valor2 = double.Parse(valorString2);

                // Para essa regra funcionar, os dois campos devem ser número
                // todo: como verificar se é numero mesmo?
                // Faz a Crítica
                if (valor >= valor2)
                {

                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                TipoLinha = this.TipoLinha,
                                Descricao = this.NomeCampo + " é maior que " + this.NomeCampo2
                            }
                     );

                }
            }
        }

        #endregion
    }
}
