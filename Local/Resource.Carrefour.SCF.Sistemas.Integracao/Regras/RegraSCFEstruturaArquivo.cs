﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFEstruturaArquivo : RegraSCFLinhaBase
    {

        #region Atributos da Regra
        #endregion

        #region Execução da Validação
        private bool _temA0 = false;
        private bool _temA9 = false;
        private bool _L0Iniciado = false;

        private double _qtdeRegistrosLote;        
        private double _qtdeRegistrosArquivo;

        private double _valorCVAcumulado;
        private double _valorCPAcumulado;
        private double _valorAJDebAcumulado;
        private double _valorAJCredAcumulado;
        private double _valorAVAcumulado;
        private double _valorAPAcumulado;
       

        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }

        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            //if (    request.Status != Resource.Carrefour.SCF.Contratos.Principal.Dados.ArquivoItemStatusEnum.PendenteDesbloqueio  &&  request.CodigoArquivo == request.CodigoArquivoOriginal) 
            //{
            //    _qtdeRegistrosLote++;
            //    _qtdeRegistrosArquivo++;
            //}

            _qtdeRegistrosLote++;
            if  (request.CodigoArquivo == request.CodigoArquivoOriginal) //Codigo do arquivo Original igual ao Codigo do arquivo atual, isto é, é dele mesmo 
                _qtdeRegistrosArquivo++;

            switch (request.TipoLinha)
            {

                case "A0":
                    request.Executou_Regra = true; //1407
                    _temA0 = true;
                    break;

                case "L0":
                    request.Executou_Regra = true; //1407
                    _qtdeRegistrosLote--;
                    if (_L0Iniciado)
                    {
                        request.Criticas.Add(new CriticaSCFEstruturaArquivo
                        {      CodigoArquivo = request.CodigoArquivo,
                               CodigoArquivoItem = request.CodigoArquivoItem,
                               TipoLinha = request.TipoLinha,
                               Descricao = "Há um L0 sendo iniciado antes de terminar o anterior."
                        });
                    }

                    _L0Iniciado = true;
                    break;

                case "L9":
                    request.Executou_Regra = true; //1407
                    _qtdeRegistrosLote--;
                    _L0Iniciado = false;
                    _qtdeRegistrosLote = 0;
                    _valorAJCredAcumulado = 0;
                    _valorAJDebAcumulado = 0;
                    _valorAPAcumulado = 0;
                    _valorAVAcumulado = 0;
                    _valorCPAcumulado = 0;
                    _valorCVAcumulado = 0;
                    break;

                case "AP":
                    request.Executou_Regra = true; //1407
                    string valor = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoPagamento", false)).Trim();
                     _valorAPAcumulado = _valorAPAcumulado + double.Parse(valor);
                    break;

                case "CV":
                    request.Executou_Regra = true; //1407
                    string valorCV = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoVenda", false)).Trim();
                    _valorCVAcumulado = _valorCVAcumulado + double.Parse(valorCV);
                    break;

                case "CP":
                    request.Executou_Regra = true; //1407
                    string valorCP = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorTotalPagamento", false)).Trim();
                    _valorCPAcumulado = _valorCPAcumulado + double.Parse(valorCP);
                    break;

                case "AV":
                    request.Executou_Regra = true; //1407
                    string valorAV = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoVenda", false)).Trim();
                    _valorAVAcumulado = _valorAVAcumulado + double.Parse(valorAV);
                    break;
                
                case "AJ":
                    request.Executou_Regra = true; //1407
                    string valorAJ = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBruto", false)).Trim();
                    string tipoAjuste =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "TipoAjuste", false)).Trim();
                    if (tipoAjuste == "1")
                        _valorAJCredAcumulado = _valorAJCredAcumulado + double.Parse(valorAJ);
                    if (tipoAjuste == "2")
                        _valorAJDebAcumulado = _valorAJDebAcumulado + double.Parse(valorAJ);
                    break;

                case "A9":
                    request.Executou_Regra = true; //1407
                    _temA9 = true;
                    string qtdeRegistrosA9 =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "TotalRegistros", false)).Trim();
                    if (_qtdeRegistrosArquivo != double.Parse(qtdeRegistrosA9))
                    {
                        request.Criticas.Add(
                           new CriticaSCFEstruturaArquivo
                           {
                               CodigoArquivo = request.CodigoArquivo,
                               CodigoArquivoItem = request.CodigoArquivoItem,
                               TipoLinha = request.TipoLinha,
                               Descricao = string.Format("Quantidade de registros do Arquivo ({0}) não confere com o A9 ({1})", _qtdeRegistrosArquivo, double.Parse(qtdeRegistrosA9))
                           }
                       );

                    }

                    break;

                default:
                    request.Executou_Regra = true; //1407
                    request.Criticas.Add(
                       new CriticaSCFEstruturaArquivo
                       {
                           CodigoArquivo = request.CodigoArquivo,
                           CodigoArquivoItem = request.CodigoArquivoItem,
                           TipoLinha = request.TipoLinha,
                           Descricao = string.Format("Tipo de registro ({0}) inválido", request.TipoLinha)
                       }
                   );
                    break;
            }
        }

        protected override void OnFinalizarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            if (!_temA0)
            {
                request.Executou_Regra = true; //1407
                request.Criticas.Add(
                   new CriticaSCFEstruturaArquivo
                   {
                       CodigoArquivo = request.CodigoArquivo,
                       TipoLinha = this.TipoLinha,
                       Descricao = "Não veio o tipo de registro A0."
                   }
               );
            }
            if (!_temA9)
            {
                request.Executou_Regra = true; //1407
                request.Criticas.Add(
                   new CriticaSCFEstruturaArquivo
                   {
                       CodigoArquivo = request.CodigoArquivo,
                       TipoLinha = this.TipoLinha,
                       Descricao = "Não veio o tipo de registro A9."
                   }
               );
            }
        }
    }
        #endregion
}
