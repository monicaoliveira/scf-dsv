﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Types;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Integracao.Arquivos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFNSUExistente : RegraSCFColunaBase
    {
        private Dictionary<string, EstabelecimentoInfo> _valores = null;
        private ConversorData _conversorData;
        private ConversorHora _conversorHora;
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            // Inicializa
            _valores = new Dictionary<string, EstabelecimentoInfo>();
            List<EstabelecimentoInfo> estabelecimentos =PersistenciaHelper.Listar<EstabelecimentoInfo>(null);
            foreach (EstabelecimentoInfo estabelecimento in estabelecimentos)
                if (estabelecimento.CNPJ != null)
                {
                    if (!_valores.ContainsKey(estabelecimento.CNPJ))
                        _valores.Add(estabelecimento.CNPJ, estabelecimento);
                }

            _conversorData = new ConversorData("yyyymmdd");
            _conversorHora = new ConversorHora("hhmmss");
        }

        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                bool existeTransacao = false;
                string campoEncontradoNaChave = null;
                string cnpjEstabelecimento =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "IdentificacaoLoja", false)).Trim();
                if (_valores.ContainsKey(cnpjEstabelecimento))
                {
                    EstabelecimentoInfo estabelecimento = _valores[cnpjEstabelecimento];
                    string valorNSUHost = null;
                    string valorNSUTef = null;
                    string numeroParcela = null;
                    double valor = 0;
                    string chave = null;
                    string chave2 = null;
                    string dataTransacao =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "DataTransacao", false)).Trim();
                    string horaTransacao =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "HorarioTransacao", false)).Trim();
                    DateTime DataTransacaoComposta = (DateTime)_conversorData.ConverterParaObjeto(null, dataTransacao);
                    TimeSpan HoraTransacao = (TimeSpan)_conversorHora.ConverterParaObjeto(null, horaTransacao);

                    DataTransacaoComposta = DataTransacaoComposta + HoraTransacao;                     // compoe a data da transacao
                    string codigoAutorizacao = null;

                    if (request.TipoLinha == "CV" || request.TipoLinha == "CP")
                    {
                        codigoAutorizacao =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "CodigoAutorizacao", false)).Trim();
                        valorNSUHost =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacao", false)).Trim();
                        valorNSUTef =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUTEFTransacao", false)).Trim();

                    }
                    else if (request.TipoLinha == "AV" || request.TipoLinha == "AP" || request.TipoLinha == "RP")
                    {
                        codigoAutorizacao =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "CodigoAutorizacaoTransacaoOriginal", false)).Trim();
                        valorNSUHost =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacao", false)).Trim();
                        valorNSUTef =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUTefTransacaoOriginal", false)).Trim();
                    }
                    else if (request.TipoLinha == "AJ")
                    {
                        valorNSUHost =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacao", false)).Trim();
                    }

                    if (request.TipoLinha != "CP" && request.TipoLinha != "AP" && request.TipoLinha != "AJ")
                    {
                        numeroParcela =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroParcela", false)).Trim();
                    }
                    
                    string nSEQ =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSEQ", false)).Trim();
                    
                    if (request.TipoLinha == "AJ")
                    {
                        string sValor =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquido", false)).Trim();
                        valor = double.Parse(sValor);
                    }

                    switch (request.TipoLinha)
                    {
                        case "CV":
                            chave = ChaveTransacaoHelper.GerarChaveComprovanteVenda(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUHost, codigoAutorizacao, numeroParcela);
                            if (valorNSUTef != null) chave2 = ChaveTransacaoHelper.GerarChaveComprovanteVenda(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUTef, codigoAutorizacao, numeroParcela);
                            break;

                        case "AV":
                            chave = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUHost, codigoAutorizacao, numeroParcela);
                            if (valorNSUTef != null) chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUTef, codigoAutorizacao, numeroParcela);
                            break;

                        case "CP":
                            //1458 chave = ChaveTransacaoHelper.GerarChaveComprovantePagamento(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUHost, codigoAutorizacao, nSEQ);
                            //1458 if (valorNSUTef != null) chave2 = ChaveTransacaoHelper.GerarChaveComprovantePagamento(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUTef, codigoAutorizacao, nSEQ);
                            string sequencialMeioPagamento = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "SequencialMeioPagamento", false));
                            sequencialMeioPagamento = sequencialMeioPagamento.PadLeft(3, '0');
                            chave = ChaveTransacaoHelper.GerarChaveComprovantePagamento(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUHost, codigoAutorizacao, sequencialMeioPagamento);
                            if (valorNSUTef != null) chave2 = ChaveTransacaoHelper.GerarChaveComprovantePagamento(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUTef, codigoAutorizacao, sequencialMeioPagamento);
                            break;

                        case "AP":
                            chave = ChaveTransacaoHelper.GerarChaveAnulacaoPagamento(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUHost, nSEQ);
                            if (valorNSUTef != null) chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoPagamento(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUTef, nSEQ);
                            break;

                        case "AJ":
                            chave = ChaveTransacaoHelper.GerarChaveAjuste(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valor, valorNSUHost);
                            break;

                        case "RP":
                            chave = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUHost, codigoAutorizacao, numeroParcela);
                            if (valorNSUTef != null) chave2 = ChaveTransacaoHelper.GerarChaveAnulacaoVenda(DataTransacaoComposta, estabelecimento.CodigoEstabelecimento, valorNSUTef, codigoAutorizacao, numeroParcela);
                            break;

                    }
                    List<CondicaoInfo> condicoesTransacao = new List<CondicaoInfo>();                    // cria a lista de condicoes para o valor do NSU
                    condicoesTransacao.Add(new CondicaoInfo("Chave", CondicaoTipoEnum.Igual, chave));
                    List<TransacaoResumoInfo> transacoes =PersistenciaHelper.Listar<TransacaoResumoInfo>(null, condicoesTransacao);
                    if (transacoes != null && transacoes.Count > 0)
                    {
                        existeTransacao = true;
                        campoEncontradoNaChave = "NSU_HOST [";
                    }
                    else if (chave2 != null)
                    {
                        condicoesTransacao = new List<CondicaoInfo>();
                        condicoesTransacao.Add(new CondicaoInfo("Chave2", CondicaoTipoEnum.Igual, chave2));
                    }
                    if (existeTransacao)
                    {   List<CondicaoInfo> condicoesArquivo = new List<CondicaoInfo>();
                        condicoesArquivo.Add(new CondicaoInfo("CodigoProcesso", CondicaoTipoEnum.Igual, transacoes[0].CodigoProcesso));
                        List<ArquivoInfo> arquivo =PersistenciaHelper.Listar<ArquivoInfo>(null, condicoesArquivo);
                        StringBuilder sb = new StringBuilder();
                        sb.Append(campoEncontradoNaChave);
                        sb.Append(valorNSUHost);
                        sb.Append("] já processado em [");
                        if (arquivo[0].ChaveArquivo != null && arquivo[0].ChaveArquivo != "")   //1410
                            sb.Append(Convert.ToDateTime(_conversorData.ConverterParaObjeto(null, arquivo[0].ChaveArquivo)).ToString("dd/MM/yyyy"));
                        else
                            sb.Append(arquivo[0].DataInclusao);

                        if (arquivo != null && arquivo.Count > 0)
                        {
                            sb.Append(" ");
                            sb.Append(arquivo[0].NomeArquivo);
                        }
                        sb.Append("] Processo [" + arquivo[0].CodigoProcesso ); //1407

                        if (campoEncontradoNaChave.Contains("HOST"))
                            sb.Append("] Codigo Transacao [" + transacoes[0].CodigoTransacao );
                        else if (chave2 != null)
                            sb.Append("] Codigo Transacao [" + transacoes[0].CodigoTransacao );

                        if (transacoes[0].StatusRetornoCessao == TransacaoStatusRetornoCessaoEnum.RetornadoOK)
                        {   List<CondicaoInfo> condicoesProcesso = new List<CondicaoInfo>();
                            condicoesArquivo.Add(new CondicaoInfo("CodigoProcessoRetornoCessao", CondicaoTipoEnum.Igual, transacoes[0].CodigoProcessoRetornoCessao));
                            List<ProcessoResumoInfo> processo =PersistenciaHelper.Listar<ProcessoResumoInfo>(null, condicoesProcesso);
                            sb.Append("] e cessionado em [");
                            sb.Append(Convert.ToDateTime(processo[0].DataInclusao).ToString("dd/MM/yyyy"));
                        }
                        sb.Append("]"); //1407
                        request.Criticas.Add(
                            new CriticaSCFEspecifica()
                            {
                                CodigoArquivo = request.CodigoArquivo,
                                CodigoArquivoItem = request.CodigoArquivoItem,
                                TipoLinha = request.TipoLinha,
                                NomeCampo = this.NomeCampo,
                                Descricao = sb.ToString()
                            });
                    }
                }
            }
        }
    }
}
