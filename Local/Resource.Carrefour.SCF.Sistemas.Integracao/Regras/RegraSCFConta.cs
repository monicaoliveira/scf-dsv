﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFConta : RegraSCFColunaBase
    {
        #region Execução da Validação
        private List<string> _valores = null;
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            _valores = new List<string>();
            List<ContaFavorecidoInfo> favorecidos = PersistenciaHelper.Listar<ContaFavorecidoInfo>(null);
            foreach (ContaFavorecidoInfo favorecido in favorecidos)
                if (!_valores.Contains(favorecido.Conta_CCI))
                    _valores.Add(favorecido.Conta_CCI);

        }

        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            // Apenas para o tipo da linha desejado
            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true;  //1407
                string valor =  ((string) request.Arquivo.LerCampo( request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                if (!_valores.Contains(valor))
                    request.Criticas.Add(
                        new CriticaSCFDominio()
                        {
                            CodigoArquivo = request.CodigoArquivo,
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            ValorNaoEncontrado = valor,
                            NomeCampo = this.NomeCampo,
                            TipoLinha = request.TipoLinha
                        });
            }
        }
        #endregion
    }
}
