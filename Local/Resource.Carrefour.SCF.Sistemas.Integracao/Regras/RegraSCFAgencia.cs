﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFAgencia : RegraSCFColunaBase
    {
        #region Execução da Validação
        private List<string> _valores = null;
        //===========================================================================================================================
        // CARREGA LISTA DE FAVORECIDO, CODIGO CONTA, BANCO, AGENCIA E CONTA e QTD TRANSACOES QUE USAM - PR_CONTA_FAVORECIDO_L
        //===========================================================================================================================
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {   _valores = new List<string>();
            List<ContaFavorecidoInfo> favorecidos = PersistenciaHelper.Listar<ContaFavorecidoInfo>(null);
            foreach (ContaFavorecidoInfo favorecido in favorecidos)
                if (!_valores.Contains(favorecido.Agencia_CCI))
                    _valores.Add(favorecido.Agencia_CCI);
        }

        //===========================================================================================================================
        // Confere se o VALOR do CAMPO vindo na linha está cadastrado
        //===========================================================================================================================
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   if (this.TipoLinha == request.TipoLinha)
        {       string valor = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                request.Executou_Regra = true;  //1407
                if (!_valores.Contains(valor))
                    request.Criticas.Add(
                        new CriticaSCFDominio()
                        {
                            CodigoArquivo = request.CodigoArquivo,
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            ValorNaoEncontrado = valor,
                            NomeCampo = this.NomeCampo,
                            TipoLinha = request.TipoLinha
                        });
            }
        }
        #endregion
    }
}
