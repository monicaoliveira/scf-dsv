﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using System.Text.RegularExpressions;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFEspecificaCP : RegraSCFLinhaBase
    {

        #region Atributos da Regra
        #endregion

        #region Execução da Validação

        // private List<string> _valores = null; // warning 25/02/2019
        private double _acumTotalMeioPagamento;
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
        
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                string meioCaptura =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "MeioCaptura", false)).Trim();
                string NSUHostString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacao", false)).Trim();
                if ((meioCaptura != "5" && meioCaptura != "8") && (NSUHostString == "" || NSUHostString == "000000000000"))
                {
                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("MEIO DE CAPTURA <> 5, 8 ({0}) e NSU HOST NAO PREENCHIDO", meioCaptura),
                                NomeCampo = this.TipoLinha,
                                TipoLinha = request.TipoLinha
                            });
                }
                string valorLiquidoString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoPagamento", false)).Trim();
                string valorBrutoString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorTotalPagamento", false)).Trim();
                string valorDescontoString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDesconto", false)).Trim();
                double valorLiquido = double.Parse(valorLiquidoString);
                double valorBruto = double.Parse(valorBrutoString);
                double valorDesconto = double.Parse(valorDescontoString);
                if (valorLiquido != valorBruto - valorDesconto)
                {
                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("VALOR LIQUIDO ({0}) <> VALOR BRUTO ({1}) - VALOR DESCONTO ({2})", valorLiquido, valorBruto, valorDesconto),
                                TipoLinha = request.TipoLinha
                            });
                }
                string numeroMeioPagamento =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroTotalMeioPagamento", false)).Trim();
                string sequencialMeioPagamento =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "SequencialMeioPagamento", false)).Trim();
                if (Int16.Parse(sequencialMeioPagamento) > Int16.Parse(numeroMeioPagamento))
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("SEQUENCIAL DO MEIO DE PAGAMENTO ({0}) > QTDE DE MEIOS DE PAGAMENTOS ({1})", sequencialMeioPagamento, numeroMeioPagamento),
                            TipoLinha = request.TipoLinha,
                            NomeCampo = "SequencialMeioPagamento"
                        });
                }
                string valorTotalMeioPagamento =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorTotalPagamento", false)).Trim();
                string valorDesteMeioPagamento =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorMeioPagamento", false)).Trim();
                Regex isnumber = new Regex("[0-9]");
                if (!isnumber.IsMatch(valorDesteMeioPagamento))
                {
                    // critica
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("ValorMeioPagamento ({0}) não numérico", valorDesteMeioPagamento),
                            TipoLinha = request.TipoLinha,
                            NomeCampo = "ValorMeioPagamento"
                        });

                }
                else
                {
                    if (!isnumber.IsMatch(valorTotalMeioPagamento))
                    {
                        request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("ValorTotalPagamento ({0}) não numérico", valorTotalMeioPagamento),
                                TipoLinha = request.TipoLinha,
                                NomeCampo = "ValorTotalPagamento"
                            });
                    }
                    else
                    {
                        double numeroValorDesteMeioPagamento = Double.Parse(valorDesteMeioPagamento);
                        double numeroValorTotalMeioPagamento = Double.Parse(valorTotalMeioPagamento);

                        if (Int16.Parse(sequencialMeioPagamento) == 1)
                        {
                            _acumTotalMeioPagamento = 0;
                        }
                        if (Int16.Parse(sequencialMeioPagamento) == Int16.Parse(numeroMeioPagamento))
                        {
                            _acumTotalMeioPagamento = _acumTotalMeioPagamento + numeroValorDesteMeioPagamento;

                            // verifica se o total está batendo
                            if (_acumTotalMeioPagamento != numeroValorTotalMeioPagamento)
                            {
                                // critica
                                request.Criticas.Add(
                                    new CriticaSCFEspecifica
                                    {
                                        Descricao = string.Format("Valor total do Meio de Pagamento ({0}) não confere a linha ({1})", _acumTotalMeioPagamento, numeroValorTotalMeioPagamento),
                                        TipoLinha = request.TipoLinha,
                                        NomeCampo = "ValorTotalPagamento"
                                    });
                            }
                            _acumTotalMeioPagamento = 0;
                        }
                        else
                            _acumTotalMeioPagamento = _acumTotalMeioPagamento + numeroValorDesteMeioPagamento;

                    }
                }
            }
         }
    }
     #endregion
}

