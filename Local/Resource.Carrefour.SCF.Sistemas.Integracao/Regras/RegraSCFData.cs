﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Integracao.Arquivos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353
using Resource.Framework.Contratos.Comum; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{   public class RegraSCFData : RegraSCFColunaBase
    {   public string Formato { get; set; }
        public TipoValidacaoVazioDataEnum TipoValidacaoVazioData { get; set; }
        public bool EhObrigatorio { get; set; }
        public DateTime? CampoMaiorQueEstaData { get; set; }

        private ConversorData _conversor; 

        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {   _conversor = new ConversorData(this.Formato);
        }

        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   bool erroObrigatoriedade = false;

            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                string valor =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                if (EhObrigatorio)
                {   if (TipoValidacaoVazioData == TipoValidacaoVazioDataEnum.Vazio)
                    {
                        if (valor == "")
                            erroObrigatoriedade = true;                     
                    }
                    else
                    {
                        if (TipoValidacaoVazioData == TipoValidacaoVazioDataEnum.Zeros)
                        {
                            if (valor == "00000000")
                                erroObrigatoriedade = true;                    
                        }
                    }
                    if (erroObrigatoriedade)
                        request.Criticas.Add(
                                new CriticaSCFDataMaiorQue

                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    Valor = valor,
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = this.TipoLinha
                                }
                         );
                }
                
                if (!erroObrigatoriedade)
                {   try
                    {
                        DateTime valorData;
                        valorData =  (DateTime)_conversor.ConverterParaObjeto(null, valor);
                        if (this.CampoMaiorQueEstaData != null && valorData > this.CampoMaiorQueEstaData)
                        {
                            request.Criticas.Add(
                                new CriticaSCFObrigatoriedade

                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    CampoNaoPreenchido = true,
                                    Valor = valor,
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = this.TipoLinha
                                }
                         );
                        }
                    }
                    catch (Exception ) // catch (Exception ex) // warning 25/02/2019
                    {
                        request.Criticas.Add(
                            new CriticaSCFData
                            {
                                CodigoArquivo = request.CodigoArquivo,
                                CodigoArquivoItem = request.CodigoArquivoItem,
                                FormatoInvalido = true,
                                Valor = valor,
                                NomeCampo = this.NomeCampo,
                                TipoLinha = request.TipoLinha
                            }
                        );
    
                    }
                }
            }
        }
    }
}
