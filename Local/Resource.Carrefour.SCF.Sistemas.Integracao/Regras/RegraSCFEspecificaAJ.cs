﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFEspecificaAJ : RegraSCFLinhaBase
    {
        #region Atributos da Regra
        #endregion

        #region Execução da Validação
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
        
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                string valorLiquidoString = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquido", false)).Trim();
                string valorBrutoString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBruto", false)).Trim();
                string valorDescontoString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDescontoOuComissao", false)).Trim();
                double valorLiquido = double.Parse(valorLiquidoString);
                double valorBruto = double.Parse(valorBrutoString);
                double valorDesconto = double.Parse(valorDescontoString);

                if (valorLiquido != valorBruto - valorDesconto)
                {
                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("Valor Liquido invalido. [Valor Liquido ({0}) <> ( Valor Bruto ({1}) - Valor Desconto ({2}))", valorLiquido, valorBruto, valorDesconto),
                                NomeCampo = this.TipoLinha,
                                TipoLinha = request.TipoLinha
                            });
                }
               
                string tipoLancamento =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "TipoLancamento", false)).Trim();
                if (tipoLancamento == "5")
                {
                    string nsuHostTransacaoOriginal =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NSUHostTransacaoOriginal", false)).Trim();
                    if (nsuHostTransacaoOriginal.Trim() == "" || nsuHostTransacaoOriginal.Equals("000000000000") || nsuHostTransacaoOriginal.Trim().Length != 12)
                    {
                        request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("NSU Host Transacao Original ({0}) é um campo obrigatório para o Tipo de Lancamento = 5.", nsuHostTransacaoOriginal), //SCF-1137 Estava Descricao = string.Format("NSUHostTransacaoOriginal é um campo obrigatório para o Tipo de Lancamento = 5."),
                                NomeCampo = "NSUHostTransacaoOriginal",
                                TipoLinha = request.TipoLinha
                            });
                    }

                    if (((ArquivoTSYS)request.Arquivo).Layout.Equals("2") || ((ArquivoTSYS)request.Arquivo).Layout.Equals("4"))
                    {   string numeroContaCliente = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroContaCliente", false)).Trim();
                        if (numeroContaCliente.Trim() == "" || numeroContaCliente.Equals("00000000000000") || numeroContaCliente.Trim().Length != 14)
                        {
                            request.Criticas.Add(
                                new CriticaSCFEspecifica
                                {
                                    Descricao = string.Format("Numero Conta do Cliente ({0}) e um campo obrigatorio para o Tipo de Lancamento = 5.", numeroContaCliente), // SCF-1137 Estava Descricao = string.Format("Numero Conta do Cliente e um campo obrigatorio para o Tipo de Lancamento = 5."),
                                    NomeCampo = "NumeroContaCliente",
                                    TipoLinha = request.TipoLinha
                                });
                        }
                    }
                }

                string tipoAjusteArquivo = (string)request.Arquivo.LerCampo("AJ", request.Linha, "TipoAjuste", true);
                string codigoAjusteArquivo = (string)request.Arquivo.LerCampo("AJ", request.Linha, "CodigoAjuste", true);
                string tipoAjusteBase = "";
                string produtoAjusteBase = "";

                foreach(ListaItemReferenciaInfo listaItemRefInfo in request.listaItemReferenciaInfo)
                {
                    if (listaItemRefInfo.CodigoListaItemSituacao == ListaItemReferenciaSituacaoEnum.CodigoAjuste.ToString() && listaItemRefInfo.NomeReferencia == ListaItemReferenciaNomeEnum.TipoAjuste.ToString()
                        && codigoAjusteArquivo.Trim() == listaItemRefInfo.listaItemInfo.Valor.Trim())
                        tipoAjusteBase = listaItemRefInfo.ValorReferencia.Trim();

                    if (listaItemRefInfo.CodigoListaItemSituacao == ListaItemReferenciaSituacaoEnum.CodigoAjuste.ToString() && listaItemRefInfo.NomeReferencia == ListaItemReferenciaNomeEnum.Produto.ToString()
                        && codigoAjusteArquivo.Trim() == listaItemRefInfo.listaItemInfo.Valor.Trim())
                        produtoAjusteBase = listaItemRefInfo.ValorReferencia.Trim();
                }
                if (string.IsNullOrEmpty(produtoAjusteBase))
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("Codigo de Ajuste ({0}) da transação ({1}) sem produto associado no sistema", codigoAjusteArquivo, request.TipoLinha),
                            NomeCampo = "CodigoAjuste",
                            TipoLinha = request.TipoLinha
                        });
                }
                else
                {
                    request.NomeProduto = produtoAjusteBase;
                }
                if (string.IsNullOrEmpty(tipoAjusteBase))
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("Codigo de Ajuste ({0}) da transação ({1}) não possui Tipo de Ajuste associado no sistema", codigoAjusteArquivo, request.TipoLinha), //SCF-1137 Estava Descricao = string.Format("Codigo de Ajuste da transação não possui Tipo de Ajuste cadastrado no sistema."),
                            NomeCampo = "CodigoAjuste", //SCF-1137 Estava "TipoAjuste"
                            TipoLinha = request.TipoLinha
                        });
                }
                else if (!(tipoAjusteArquivo.Trim() == tipoAjusteBase))
                {
                    request.Criticas.Add(
                        new CriticaSCFEspecifica
                        {
                            Descricao = string.Format("Tipo de Ajuste ({0}) informado no arquivo não confere com o associado no Codigo de Ajuste ({1}) da transação ({2})", tipoAjusteArquivo, codigoAjusteArquivo, request.TipoLinha), //SCF-1137 Estava Descricao = string.Format("Tipo de Ajuste informado no arquivo não persiste com o cadastrado para o Codigo de Ajuste da transação."),
                            NomeCampo = "TipoAjuste",
                            TipoLinha = request.TipoLinha
                        });
                }

            }
        }
        #endregion
    }
}
