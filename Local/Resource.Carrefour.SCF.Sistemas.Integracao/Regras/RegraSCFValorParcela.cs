﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFValorParcela : RegraSCFColunaBase
    {
        #region Atributos da Regra

        public string NomeCampo { get; set; }
        public string NomeCampo2 { get; set; }
        public string NomeCampo3 { get; set; }
        public string NomeCampo4 { get; set; }

        #endregion

        #region Execução da Validação

        // private List<string> _valores = null; // warning 25/02/2019
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                string valorString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                string valorString2 =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo2, false)).Trim();
                string valorString3 =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo3, false)).Trim();                
                string valorString4 =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo4, false)).Trim();

                double valor = double.Parse(valorString);
                double valor2 = double.Parse(valorString2);
                double valor3 = double.Parse(valorString3); 
                double valor4 = double.Parse(valorString4);

                if (valor > 0 && valor2 > valor3)
                {
                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                TipoLinha = this.TipoLinha,
                                Descricao = this.NomeCampo2 + " é  maior que valor bruto parcela" 
                            }
                     );

                }
                if (valor > 0 && valor4 > valor3)                 
                {
                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                TipoLinha = this.TipoLinha,
                                Descricao = this.NomeCampo4 + " é  maior que valor bruto parcela"
                            }
                     );
                }
            }
        }

        #endregion
    }
}
