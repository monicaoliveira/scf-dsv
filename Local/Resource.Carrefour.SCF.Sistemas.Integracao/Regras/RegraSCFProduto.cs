﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    //=================================================================================================================================
    public class RegraSCFProduto : RegraSCFColunaBase
    {
        #region Atributos da Regra
        // List<ProdutoInfo> produtos; // warning 25/02/2019
        #endregion

        #region Execução da Validação
        private Dictionary<string, ProdutoInfo> _valores = null;  // SCF1340
        //=================================================================================================================================
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            //=================================================================================================================================
            // Inicializa
            // SCF1340 - Alteração no tipo da variavel _valor
            //=================================================================================================================================
            _valores = new Dictionary<string, ProdutoInfo>();
            List<ProdutoInfo> produtos = PersistenciaHelper.Listar<ProdutoInfo>(null);
            foreach (ProdutoInfo produto in produtos)
                    _valores.Add(produto.NomeProduto, produto);

        }
        //=================================================================================================================================
        
        //=================================================================================================================================
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   //=================================================================================================================================
            // Apenas para o tipo da linha desejado
            //=================================================================================================================================
            if (this.TipoLinha == request.TipoLinha) 
            {
                string valor = null;
                string modalidadeVenda = null;

                //1385 - inicio =================================================================================================================================
                if (this.TipoLinha == "CV")
                {
                    request.Executou_Regra = true; //1407
                    valor = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                    modalidadeVenda = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ModalidadeVenda", false)).Trim();
                    if (modalidadeVenda == "1")
                    {
                        if (valor == "RCAR")
                        {
                            valor = "COBAN";
                        }
                        else
                        {
                            valor = "SEM PRODUTO";
                        }
                    }
                }
                //1385 - fim =================================================================================================================================
                if (this.TipoLinha == "CP" || this.TipoLinha == "AP") // SCF1340
                {
                    valor = "RECTO"; // SCF1340
                }
                if (this.TipoLinha == "AJ" || this.TipoLinha == "AV") // SCF1340
                {
                    valor = request.NomeProduto; // SCF1340
                }
                //=================================================================================================================================
                // Se o produto ficou NULO atribui o conteudo SEM PRODUTO
                //=================================================================================================================================
                if (valor == null)
                    valor = "SEM PRODUTO"; //1351
                //=================================================================================================================================
                // Se a lista não contém o valor, gera a crítica
                //=================================================================================================================================
                if (!_valores.ContainsKey(valor)) // SCF1340
                    request.Criticas.Add(
                        new CriticaSCFDominio()
                        {   CodigoArquivo       = request.CodigoArquivo
                        ,   CodigoArquivoItem   = request.CodigoArquivoItem
                        ,   ValorNaoEncontrado  = valor
                        ,   NomeCampo           = this.NomeCampo
                        ,   TipoLinha           = request.TipoLinha
                        });
                else
                {
                    request.CodigoProduto = _valores[valor].CodigoProduto; // SCF1340
                    request.NomeProduto = _valores[valor].CodigoProdutoTSYS; // SCF1340
                }
            }
        }

        #endregion
    }
}
