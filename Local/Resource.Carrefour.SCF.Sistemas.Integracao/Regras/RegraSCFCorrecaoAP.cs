﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFCorrecaoAP : RegraSCFLinhaBase
    {
        private Dictionary<string, string> _valores = null;

        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            List<ListaItemReferenciaInfo> listaItemReferencias = PersistenciaHelper.Listar<ListaItemReferenciaInfo>(null);

            _valores = new Dictionary<string, string>();
            foreach (ListaItemReferenciaInfo listaReferenciaInfo in listaItemReferencias)
                if (listaReferenciaInfo.CodigoListaItemSituacao == "CodigoAnulacao"
                    && listaReferenciaInfo.NomeReferencia == "MeioPagamento"
                    && _valores.ContainsKey(listaReferenciaInfo.listaItemInfo.Valor) == false)
                    _valores.Add(listaReferenciaInfo.listaItemInfo.Valor, listaReferenciaInfo.ValorReferencia);
        }
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            
            if (request.TipoLinha == "L0")
            {   this.DataMovimento = ((string)request.Arquivo.LerCampo("L0", (object)request.Linha, "DataMovimento", false));
                request.Executou_Regra = true;  //1407
            }
            else if (request.TipoLinha == "A0")
            {   this.IDMovimento = ((string)request.Arquivo.LerCampo("A0", (object)request.Linha, "IdMovimento", false));
                request.Executou_Regra = true;  //1407
            }
            //================================================================================================================//
            //1408 else if (request.TipoLinha == this.TipoLinha)
            //Não efetuar correção da linha caso possua crítica;
            //================================================================================================================//
            else if (request.TipoLinha == this.TipoLinha && request.Criticas.Count == 0)
            {
                request.Executou_Regra = true;  //1407
                string nsuHostTransacaoOriginal = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUHostTransacaoOriginal", false));
                string nsuTefTransacaoOriginal = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUTefTransacaoOriginal", false));
                string nsuHostTransacao = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUHostTransacao", false));
                string formaMeioPagamentoAP = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "FormaMeioPagamento", false));
                string codigoAnulacaoAP = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "CodigoAnulacao", false));
                string novoNSU;
                string zeros = new string('0', 12);
                if (nsuHostTransacaoOriginal == zeros || nsuTefTransacaoOriginal == zeros || nsuHostTransacao == zeros)
                {
                    //SCF 1473
                    //if (isHomologacaoTSYS)
                    //{
                    //    request.Criticas.Add(
                    //        new CriticaSCFObrigatoriedade()
                    //        {
                    //            CodigoArquivo = request.CodigoArquivo,
                    //            CodigoArquivoItem = request.CodigoArquivoItem,
                    //            CampoNaoPreenchido = true,
                    //            NomeCampo = "NSUTef",
                    //            TipoLinha = request.TipoLinha
                    //        });
                    //}
                    //else
                    //{
                        //===================================================================================================
                        //1448 - inicio
                        //===================================================================================================
                        //novoNSU = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                        //request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacaoOriginal", novoNSU);
                        //request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUTefTransacaoOriginal", novoNSU);
                        //request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacao", novoNSU);
                        //request.LinhaCorrigida = true;

                        if (nsuHostTransacaoOriginal == zeros)
                        {
                            novoNSU = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacaoOriginal", novoNSU);
                        }
                        if (nsuTefTransacaoOriginal == zeros)
                        {
                            novoNSU = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUTefTransacaoOriginal", novoNSU);
                        }
                        if ((nsuHostTransacao == zeros))
                        {
                            novoNSU = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacao", novoNSU);
                        }
                        request.LinhaCorrigida = true;
                        //===================================================================================================
                        // 1448 fim
                        //===================================================================================================
                    //SCF 1473
                    //}
                }

                //SCF 1473
                //if (!isHomologacaoTSYS)
                //{
                    string novoMeioPagamento = null;
                    if (_valores.Count > 0 && _valores.ContainsKey(codigoAnulacaoAP))
                    {
                        novoMeioPagamento = _valores[codigoAnulacaoAP].ToString();
                        if (novoMeioPagamento != formaMeioPagamentoAP)
                        {
                            request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "FormaMeioPagamento", novoMeioPagamento);
                            request.LinhaCorrigida = true;
                        }
                    }
                //SCF 1473
                //}
            }
        }
    }
}
