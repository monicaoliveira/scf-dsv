﻿// ==========================================================================================================
// 1328 - Refazendo esta classe e a procedure
// Objetivo é validar a transação para ser cessionada, os retornos com erro poderão ser:
//
// 2 - VALIDAÇÃO DE TRANSACAO JÁ CESSIONADA 
// 3 - VALIDAÇÃO DE TRANSACAO INEXISTENTE 
// 4 - VALIDAÇÃO DE TRANSACAO COM PRODUTO QUE NÃO PODE SER CESSIONADO
// 5 - ESTABELECIMENTO NÃO CADASTRADO 
// 6 - FAVORECIDO NÃO CADASTRADO 
// 7 - CONTA PARA O BANCO ESPECIFICO DO FAVORECIDO NÃO CADASTRADO
//
// Atualiza o arquivo item no campo CHAVE contendo o codigo da transacao, codigo do favorecido e 
// o codigo da conta do favorecido
//
// Bloqueia automaticamente as linhas que deram inconsistencia
// ==========================================================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library;
using System.IO;
using System.Data;
using Oracle.DataAccess.Client;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Oracle.DataAccess.Types;


namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegrasSCFValidarTransacao : RegraSCFColunaBase
    {
        public string Banco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string CnpjFavorecido { get; set; }
        public string NSUHostTransacao { get; set; }
        public string DataTransacao { get; set; }
        public string IdentificacaoLoja { get; set; }
        public string CodigoAutorizacao { get; set; }
        public string NumeroParcela { get; set; }
        public string NomeFavorecido { get; set; }
        public bool ReprocessarNSUCessionado { get; set; }  // 06/12/2018 - Não atualizava transações cessionadas mesmo com FLAG igual a sim
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            // ==========================================================================================================
            // Apenas para o tipo da linha desejado = "R1"
            // ==========================================================================================================
            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                // ==========================================================================================================
                // Carrega informação para efetuar log
                // ==========================================================================================================
                string NomeFavorecido = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeFavorecido, false)).Trim();
                // ==========================================================================================================
                // Parametros de Envio - Carrega informações 
                // ==========================================================================================================
                int CodProc = Convert.ToInt32(request.CodigoProcesso);
                int CodArq = Convert.ToInt32(request.CodigoArquivo);
                int CodArqItem = Convert.ToInt32(request.CodigoArquivoItem);
                ArquivoItemStatusEnum StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(request.Status); // Status da linha
                string Banco = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.Banco, false)).Trim();
                string Agencia = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.Agencia, false)).Trim();
                string Conta = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.Conta, false)).Trim();
                string CnpjFavorecido = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.CnpjFavorecido, false)).Trim();
                string NsuHost = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NSUHostTransacao, false)).Trim();
                string dtTransacao = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.DataTransacao, false)).Trim();
                DateTime DataTransacao = DateTime.ParseExact(dtTransacao, "yyyyMMdd", null);
                string CnpjEstabelecimento = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.IdentificacaoLoja, false)).Trim();
                string CodAutorizacao = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.CodigoAutorizacao, false)).Trim();
                string NumeroParcela = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NumeroParcela, false)).Trim();
                //bool FlagAtualizarCessionada = request.ReprocessarTransacao;  // 06/12/2018 - Não atualizava transações cessionadas mesmo com FLAG igual a sim
                bool FlagAtualizarCessionada = ReprocessarNSUCessionado;  // 06/12/2018 - Não atualizava transações cessionadas mesmo com FLAG igual a sim
                // ==========================================================================================================
                // Parametros de Retorno - Limpa informações 
                // ==========================================================================================================
                int CodProcRetCessao = 0;
                string RetValidacao = "";
                string ChaveItem = "000000000000000000000000000000000000000000000000000000000000";
                string CodigoProduto = ""; //1343
                string NomeProduto = ""; //1343
                string Referencia = ""; //1343
                string StatusTransacao = ""; //SCF 1701 ID 29
                // ==========================================================================================================
                // Executa a procedure 
                // ==========================================================================================================
                Procedures.PR_VALIDAR_CESSAO_B(
                    CodProc, CodArq, Banco, Agencia, Conta, CnpjFavorecido, NsuHost, DataTransacao, CnpjEstabelecimento, CodAutorizacao, NumeroParcela, FlagAtualizarCessionada,
                    out CodProcRetCessao, out RetValidacao, out ChaveItem, out CodigoProduto, out NomeProduto, out Referencia, out StatusTransacao ); //1343
                // ==========================================================================================================
                // Conferindo retorno da validacao
                // ==========================================================================================================
                request.ChaveItem = ChaveItem;
                //1408 request.LinhaCorrigida = true;
                request.LinhaAtualizada = true; //1408
                request.CodigoProcesso = CodigoProduto; //1343
                request.NomeProduto = NomeProduto; //1343
                request.Referencia = Referencia; //1343
                if (ChaveItem != "")
                    request.CodigoFavorecido = ChaveItem.Substring(20, 20); //1343
                request.CNPJFavorecido = CnpjFavorecido; //1343
                request.NomeFavorecido = NomeFavorecido; //1343

                if (RetValidacao.LastIndexOf('2') >= 0)
                {
                    request.LinhaBloqueada = true;
                    StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(3); // bloqueado
                    request.Criticas.Add(
                        new CriticaSCFEspecifica()
                        {
                            CodigoArquivo = request.CodigoArquivo,
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            NomeCampo = "NSUHostTransacao",
                            TipoLinha = request.TipoLinha,
                            Descricao = string.Format("Transacao ja cessionada - NSU da Cessao [{0}] Processo que ja cessionou [{1}].", NsuHost, CodProcRetCessao),
                            DataCritica = DateTime.Now

                        });
                }
                if (RetValidacao.LastIndexOf('3') >= 0)
                {
                    request.LinhaBloqueada = true;
                    StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(3); // bloqueado
                    request.Criticas.Add(
                        new CriticaSCFEspecifica()
                        {
                            CodigoArquivo = request.CodigoArquivo,
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            NomeCampo = "NSUHostTransacao",
                            TipoLinha = request.TipoLinha,
                            DataCritica = DateTime.Now,
                            //1463 Descricao = string.Format("Transacao nao encontrada - NSU da Cessao [{0}].", NsuHost) 
                            Descricao = "Transacao nao encontrada"
                        });
                }
                if (RetValidacao.LastIndexOf('4') >= 0)
                {
                    request.LinhaBloqueada = true;
                    StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(3); // bloqueado
                    request.Criticas.Add(
                        new CriticaSCFEspecifica()
                        {
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            NomeCampo = "ProdutoCSF",
                            TipoLinha = request.TipoLinha,
                            DataCritica = DateTime.Now,
                            //1463 Descricao = string.Format("Transacao com Produto [{0}] nao cedivel - NSU da Cessao [{1}].", NomeProduto, NsuHost)
                            Descricao = string.Format("Transacao com Produto [{0}] nao cedivel", NomeProduto)
                        });
                }
                if (RetValidacao.LastIndexOf('5') >= 0)
                {
                    request.LinhaBloqueada = true;
                    StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(3); // bloqueado
                    request.Criticas.Add(
                        new CriticaSCFEspecifica()
                        {
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            NomeCampo = "IdentificacaoLoja",
                            TipoLinha = request.TipoLinha,
                            DataCritica = DateTime.Now,
                            Descricao = string.Format("Estabelecimento nao cadastrado - CNPJ [{0}].", CnpjEstabelecimento)
                        });
                }
                if (RetValidacao.LastIndexOf('6') >= 0)
                {
                    request.LinhaBloqueada = true;
                    StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(3); // bloqueado
                    request.Criticas.Add(
                        new CriticaSCFEspecifica()
                        {
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            NomeCampo = "CNPJFavorecido",
                            TipoLinha = request.TipoLinha,
                            DataCritica = DateTime.Now,
                            Descricao = string.Format("Favorecido nao cadastrado - CNPJ/Nome do Favorecido da Cessao [{0}-{1}].", NomeFavorecido, CnpjFavorecido)
                        });
                }

                if (RetValidacao.LastIndexOf('7') >= 0)
                {
                    request.LinhaBloqueada = true;
                    StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(3); // bloqueado
                    request.Criticas.Add(
                        new CriticaSCFEspecifica()
                        {
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            NomeCampo = "CNPJFavorecido",
                            TipoLinha = request.TipoLinha,
                            DataCritica = DateTime.Now,
                            Descricao = string.Format("Conta do Favorecido nao cadastrada - CNPJ/Nome Favorecido da Cessao  [{0}]-[{1}] /Banco [{2}]/Agencia [{3}]/Conta [{4}].", NomeFavorecido, CnpjFavorecido, Banco, Agencia, Conta)
                        });
                }

                // SCF1343 - Validação de Produto e Referencia
                if (RetValidacao.LastIndexOf('8') >= 0)
                {
                    request.LinhaBloqueada = true;
                    
                    StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(3); // bloqueado
                    request.Criticas.Add(
                        new CriticaSCFEspecifica()
                        {
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            NomeCampo = "CNPJFavorecido",
                            TipoLinha = request.TipoLinha,
                            DataCritica = DateTime.Now,
                           //1463 Descricao = string.Format("Produto {0} e Referencia {1} nao cadastrado para o Favorecido ({2}-{3}) - NSU da Cessao [{4}].",  request.NomeProduto.Trim(), request.Referencia.Trim(), NomeFavorecido.Trim(), CnpjFavorecido, NsuHost)
                            Descricao = string.Format("Produto {0} e Referencia {1} nao cadastrado para o Favorecido ({2}-{3})", request.NomeProduto.Trim(), request.Referencia.Trim(), NomeFavorecido.Trim(), CnpjFavorecido)
                        });
                }

                // SCF1701 ID 29 - Validação de Status da Transação
                if (RetValidacao.LastIndexOf('9') >= 0)
                {
                    request.LinhaBloqueada = true;

                    StatusArqItem = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(3); // bloqueado
                    request.Criticas.Add(
                        new CriticaSCFEspecifica()
                        {
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            NomeCampo = "NSUHostTransacao",
                            TipoLinha = request.TipoLinha,
                            DataCritica = DateTime.Now,
                            Descricao = string.Format("Transação com Status {0}-{1} não permite ser Cessionada.", StatusTransacao, SqlDbLib.EnumToObject<TransacaoStatusEnum>(StatusTransacao).ToString())
                        });
                }
            }
        }
    }
}

