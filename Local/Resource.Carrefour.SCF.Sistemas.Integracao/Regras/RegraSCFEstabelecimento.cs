﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFEstabelecimento : RegraSCFColunaBase
    {
        #region Atributos da Regra
        #endregion

        #region Execução da Validação
        private Dictionary<string, EstabelecimentoInfo> _valores = null;
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            _valores = new Dictionary<string, EstabelecimentoInfo>();
            List<EstabelecimentoInfo> estabelecimentos =PersistenciaHelper.Listar<EstabelecimentoInfo>(null);
            foreach (EstabelecimentoInfo estabelecimento in estabelecimentos)
                if (estabelecimento.CNPJ != null)
                {
                    if(!_valores.ContainsKey(estabelecimento.CNPJ))
                        _valores.Add(estabelecimento.CNPJ, estabelecimento);
                }
        }
        
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha) 
            {
                request.Executou_Regra = true; //1407
                string valor = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                request.CNPJEstabelecimento = valor; //1385 _valores[valor].CNPJ; // SCF1340
                request.NomeEstabelecimento = "NÃO CADASTRADO"; //1385
                if (!_valores.ContainsKey(valor))
                {
                    request.Criticas.Add(
                        new CriticaSCFDominio()
                        {
                            CodigoArquivo = request.CodigoArquivo,
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            ValorNaoEncontrado = valor,
                            NomeCampo = this.NomeCampo,
                            TipoLinha = request.TipoLinha
                        });
                }
                else
                {   request.CodigoEstabelecimento = _valores[valor].CodigoEstabelecimento; 
                    request.NomeEstabelecimento = _valores[valor].RazaoSocial; // SCF1340
                    if (_valores[valor].CodigoFavorecido == null)
                    {
                        request.Criticas.Add(
                            //1282 new CriticaSCFDominio()
                            new CriticaSCFEspecifica()
                            {
                                CodigoArquivo = request.CodigoArquivo,
                                CodigoArquivoItem = request.CodigoArquivoItem,
                                //1262 ValorNaoEncontrado = valor,
                                NomeCampo = this.NomeCampo,
                                TipoLinha = request.TipoLinha,
                                Descricao = string.Format("Favorecido nao cadastrado para o Estabelecimento/CNPJ {0}", _valores[valor].CNPJ) //1353
                            });
                    }
                    else if (this.TipoLinha != "R1" && _valores[valor].CodigoEmpresaGrupo != request.Contexto["CodigoGrupo"].ToString())
                    {
                        if (_valores[valor].CodigoFavorecido == null)
                        {

                            request.Criticas.Add(
                                //1282 new CriticaSCFDominio()
                                new CriticaSCFEspecifica()
                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    //1262 ValorNaoEncontrado = valor,
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = request.TipoLinha,
                                    Descricao = string.Format("Favorecido nao cadastrado para o Estabelecimento/CNPJ {0}", _valores[valor].CNPJ) //1353
                                });
                        }
                        else
                        {
                            request.Criticas.Add(
                                new CriticaSCFEspecifica
                                {
                                    // SCF1159 Descricao = "ESTABELECIMENTO NAO PERTENCE AO GRUPO DO ARQUIVO IMPORTADO",
                                    Descricao = string.Format("Estabelecimento {0} nao pertence ao grupo do arquivo importado", _valores[valor].CNPJ ), //1353),
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = request.TipoLinha
                                });
                        }
                    }
                }
            }
        }

        #endregion
    }
}
