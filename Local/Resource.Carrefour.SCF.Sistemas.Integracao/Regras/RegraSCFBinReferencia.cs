﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    // ======================================================================================================
    public class RegraSCFBinReferencia : RegraSCFColunaBase
    {
        #region Atributos da Regra

        #endregion

        #region Execução da Validação

        // ======================================================================================================
        private Dictionary<string, BinReferenciaInfo> dicionarioBinRef = null;


        // ======================================================================================================
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            // ======================================================================================================
            // Inicializa
            // ======================================================================================================
            dicionarioBinRef = new Dictionary<string, BinReferenciaInfo>();
            List<BinReferenciaInfo> BinReferencias = PersistenciaHelper.Listar<BinReferenciaInfo>(null);
            foreach (BinReferenciaInfo binReferencia in BinReferencias)
                if (binReferencia.CodigoBin != null)
                {   if (!dicionarioBinRef.ContainsKey(binReferencia.CodigoBin))
                        dicionarioBinRef.Add(binReferencia.CodigoBin, binReferencia);
                }
        }
        // ======================================================================================================

        // ======================================================================================================
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            // ======================================================================================================
            // Apenas para o tipo da linha desejado
            // ======================================================================================================
            if (this.TipoLinha == request.TipoLinha)
            {   ProcessoInfo processoInfo = new ProcessoInfo();

                    request.Executou_Regra = true;  //1407

                    // ======================================================================================================
                    // Extrai o valor
                    // ======================================================================================================
                    string valor = ((string) request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();

                    if (valor != "")
                        valor = valor.Substring(3, 6);

                    if (valor.Trim() == "" || valor == "000000")
                    {   //1351if (request.TipoLinha != "AJ")
                        {   request.Criticas.Add(
                                   new CriticaSCFDominio()
                                   {   CodigoArquivo = request.CodigoArquivo,
                                       CodigoArquivoItem = request.CodigoArquivoItem,
                                       ValorNaoEncontrado = valor,
                                       NomeCampo = this.NomeCampo, //1282 "NumeroCartao", OCORRIA ERRO NO MOMENTO DE ALTERAÇÃO EM MASSA
                                       TipoLinha = request.TipoLinha,
                                       //1351 Descricao = string.Format("Bin ({0}) não cadastrado.", valor.ToString())
                                       Descricao = string.Format("Numero Cartao [{0}] invalido.", valor.ToString())
                                   });
                        }
                        // ======================================================================================================
                        // AJ - Bloqueio automatico
                        // ======================================================================================================
                        if (request.TipoLinha == "AJ")
                        {   Procedures.PR_ARQUIVO_ITEM_STATUS_S(request.CodigoArquivo, request.CodigoArquivoItem, null, null, null, null,ArquivoItemStatusEnum.Bloqueado);
                            request.LinhaBloqueada = true;
                        }
                    }
                    // ======================================================================================================
                    // Se o BIN não esta cadastrado no dominio, gera a crítica
                    // ======================================================================================================
                    else if (!dicionarioBinRef.ContainsKey(valor))
                    {
                        //1351 if (request.TipoLinha != "AJ")
                        {   request.Criticas.Add(
                            //1282 new CriticaSCFDominio()
                                   new CriticaSCFEspecifica() //1282
                                   {   CodigoArquivo = request.CodigoArquivo,
                                       CodigoArquivoItem = request.CodigoArquivoItem,
                                       //1262ValorNaoEncontrado = valor,
                                       NomeCampo = this.NomeCampo, //1282 "NumeroCartao", OCORRIA ERRO NO MOMENTO DE ALTERAÇÃO EM MASSA
                                       TipoLinha = request.TipoLinha,
                                       Descricao = string.Format("Bin ({0}) não cadastrado.", valor.ToString())
                                   });
                        }
                        // ======================================================================================================
                        // AJ - Bloqueio automatico
                        // ======================================================================================================
                        if (request.TipoLinha == "AJ")
                        {
                            Procedures.PR_ARQUIVO_ITEM_STATUS_S(request.CodigoArquivo,request.CodigoArquivoItem, null,null,null,null, ArquivoItemStatusEnum.Bloqueado);
                            request.LinhaBloqueada = true;
                        }
                    }
                    else
                    {
                        // ======================================================================================================
                        // Verifica se há referencia cadastrado para esse bin
                        // ======================================================================================================
                        BinReferenciaInfo bin = dicionarioBinRef[valor];
                        if (bin.CodigoReferencia == "")
                        {
                            //1351if (request.TipoLinha != "AJ")
                            {   request.Criticas.Add(
                                //1282 new CriticaSCFDominio()
                                   new CriticaSCFEspecifica() //1282
                                    {
                                        CodigoArquivo = request.CodigoArquivo,
                                        CodigoArquivoItem = request.CodigoArquivoItem,
                                        NomeCampo = this.NomeCampo, //1282 "NumeroCartao", OCORRIA ERRO NO MOMENTO DE ALTERAÇÃO EM MASSA
                                        TipoLinha = request.TipoLinha,
                                        Descricao = string.Format("Referência do BIN ({0}) não cadastrada", valor.ToString())
                                    });
                            }
                            // ======================================================================================================
                            // AJ - Bloqueio automatico
                            // ======================================================================================================
                            if (request.TipoLinha == "AJ")
                            {   Procedures.PR_ARQUIVO_ITEM_STATUS_S(request.CodigoArquivo, request.CodigoArquivoItem, null, null, null, null, ArquivoItemStatusEnum.Bloqueado);
                                request.LinhaBloqueada = true;
                            }
                        }
                    }
                    if (dicionarioBinRef.ContainsKey(valor))
                    {   BinReferenciaInfo bin2 = dicionarioBinRef[valor];
                        if (bin2.CodigoReferencia != "")
                        {   // ======================================================================================================
                            // Pega o arquivo item
                            // ======================================================================================================
                            ArquivoItemInfo arquivoItemInfo =   PersistenciaHelper.Receber<ArquivoItemInfo>(null, request.CodigoArquivoItem);
                            // ======================================================================================================
                            // Update na Tabela Tb_Arquivo_Item alterando a referencia
                            // ======================================================================================================
                            arquivoItemInfo.ReferenciaBinArquivoItem = bin2.CodigoReferencia;
                            request.Referencia = bin2.CodigoReferencia; //1351
                            // ======================================================================================================
                            // Salva
                            // ======================================================================================================
                            PersistenciaHelper.Salvar<ArquivoItemInfo>(null, arquivoItemInfo);
                        }
                    }
                }
            }
        }

        #endregion
    }

