﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353
using Resource.Framework.Contratos.Comum; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFCorrecaoCVParcela : RegraSCFLinhaBase
    {
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            //=========================================================================================
            // 1408 
            // Alterando a condição incluindo:
            // Linha não pode ter critica			
            //=========================================================================================
            if ( request.TipoLinha == "CV" && request.Criticas.Count == 0 )
            {
                request.Executou_Regra = true; //1407
                string numeroParcelaTotalString = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroTotalParcelas", false)).Trim();
                string produtoCSF = Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ProdutoCSF", true), typeof(string)).ToString();
                decimal valorLiquidoParcela = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoParcela", true), typeof(decimal));
                decimal valorBrutoParcela = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoParcela", true), typeof(decimal));
                decimal valorDescontoParcela = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDescontoParcela", true), typeof(decimal));
                decimal valorBrutoVenda = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoVenda", true), typeof(decimal));
                decimal valorDescontoVenda = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDesconto", true), typeof(decimal));
                decimal valorLiquidoVenda = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoVenda", true), typeof(decimal));
                //=========================================================================================
                // 1408 
                // Alterando para conferir se o valor bruto é maior que o desconto
                // Retirando instruções de conexão com banco de dados
                //=========================================================================================
                if ((numeroParcelaTotalString != "000" && numeroParcelaTotalString != "001") && (valorLiquidoParcela != valorBrutoParcela - valorDescontoParcela) && (valorBrutoParcela >= valorDescontoParcela))
                {
                    string linha = request.Linha;

                    int numeroTotalParcelas = ((int)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroTotalParcelas", true));
                    int numeroParcela = (int)request.Arquivo.LerCampo(request.TipoLinha, linha, "NumeroParcela", true);

                    decimal valorDesconto = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDesconto", true), typeof(decimal));
                    decimal descontoParcela = Math.Floor((valorDesconto / numeroTotalParcelas) * 100) / 100;
                    decimal descontoTotalVenda = descontoParcela * numeroTotalParcelas;
                    decimal diferenca = valorDesconto - descontoTotalVenda;
                    decimal descontoUltimaParcela = descontoParcela + diferenca;


                    if (numeroParcela == numeroTotalParcelas)
                    {
                        linha = request.Arquivo.EscreverCampo(request.TipoLinha, linha, "ValorLiquidoParcela", Convert.ChangeType(valorBrutoParcela - descontoUltimaParcela, typeof(double)), true);
                        linha = request.Arquivo.EscreverCampo(request.TipoLinha, linha, "ValorDescontoParcela", Convert.ChangeType(descontoUltimaParcela, typeof(double)), true);
                    }
                    else
                    {
                        linha = request.Arquivo.EscreverCampo(request.TipoLinha, linha, "ValorLiquidoParcela", Convert.ChangeType(valorBrutoParcela - descontoParcela, typeof(double)), true);
                        linha = request.Arquivo.EscreverCampo(request.TipoLinha, linha, "ValorDescontoParcela", Convert.ChangeType(descontoParcela, typeof(double)), true);
                    }
                    request.Linha = linha;
                    request.LinhaCorrigida = true;
                }
                //=========================================================================================
                // 1408 
                // Retirando instruções de conexão com banco de dados
                //=========================================================================================
                else if (produtoCSF == "CRAF" && valorBrutoParcela == 0 && valorDescontoParcela == 0 && valorLiquidoParcela == 0)
                {
                    string linha = "";

                    linha = request.Arquivo.EscreverCampo(request.TipoLinha, request.Linha, "ValorLiquidoParcela", Convert.ChangeType(valorLiquidoVenda, typeof(double)), true);
                    linha = request.Arquivo.EscreverCampo(request.TipoLinha, linha, "ValorDescontoParcela", Convert.ChangeType(valorDescontoVenda, typeof(double)), true);
                    linha = request.Arquivo.EscreverCampo(request.TipoLinha, linha, "ValorBrutoParcela", Convert.ChangeType(valorBrutoVenda, typeof(double)), true);

                    request.Linha = linha;
                    request.LinhaCorrigida = true;
                }
            }
        }
    }
}
