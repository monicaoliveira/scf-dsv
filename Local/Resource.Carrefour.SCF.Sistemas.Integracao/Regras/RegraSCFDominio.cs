﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFDominio : RegraSCFColunaBase
    {
        #region Atributos da Regra
        public string NomeLista { get; set; }
        public string Lista { get; set; }

        #endregion

        #region Execução da Validação
        private List<string> _valores = null;
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            _valores = new List<string>();
            string[] valores = null;
            if (this.Lista != null)
            {
                valores = this.Lista.Split(';');
                foreach (string valor in valores)
                    _valores.Add(valor.Trim());
            }
            if (this.NomeLista != null)
            {
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                condicoes.Add(new CondicaoInfo("NomeLista", CondicaoTipoEnum.Igual, this.NomeLista));
                List<ListaItemInfo> listas =PersistenciaHelper.Listar<ListaItemInfo>(null, condicoes);

                foreach (ListaItemInfo listaItem in listas)
                {
                    _valores.Add(listaItem.Valor);
                }
            }

        }
        
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha) 
            {
                request.Executou_Regra = true; //1407
                string valor = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                if (!_valores.Contains(valor))
                    request.Criticas.Add(
                        new CriticaSCFDominio() 
                        { 
                            CodigoArquivo = request.CodigoArquivo,
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            ValorNaoEncontrado = valor ,
                            NomeCampo = this.NomeCampo,
                            TipoLinha = request.TipoLinha
                        });
            }
        }

        #endregion
    }
}
