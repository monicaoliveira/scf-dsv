﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353
using Resource.Framework.Contratos.Comum; //1353


namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFCorrecaoCV : RegraSCFLinhaBase
    {
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   if (request.TipoLinha == "L0")
            {   this.DataMovimento = ((string)request.Arquivo.LerCampo("L0", (object)request.Linha, "DataMovimento", false));
                request.Executou_Regra = true; //1407
            }
            else if (request.TipoLinha == "A0")
            {   this.IDMovimento = ((string)request.Arquivo.LerCampo("A0", (object)request.Linha, "IdMovimento", false));
                request.Executou_Regra = true; //1407
            }
            //================================================================================================================//
            //1408 else if (request.TipoLinha == this.TipoLinha)
            //Não efetuar correção da linha caso possua crítica;
            //================================================================================================================//
            else if (request.TipoLinha == this.TipoLinha && request.Criticas.Count == 0)
            {
                request.Executou_Regra = true; //1407
                string tipoLancamento = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "TipoLancamento", false));
                bool primeiraParcela = true;
                bool primeiraParcelaTef = true;
                string zeros = new string('0', 12);
                string zerosCupom = new string('0', 10);

                string nsuTefTransacao = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUTEFTransacao", false));
                string nsuHostTransacao = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "NSUHostTransacao", false));
                string meioCaptura = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "MeioCaptura", false));
                string cupomFiscal = ((string)request.Arquivo.LerCampo(this.TipoLinha, (object)request.Linha, "CupomFiscal", false));

                int numeroTotalParcelas = ((int)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroTotalParcelas", true));
                int numeroParcela = ((int)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "NumeroParcela", true));

                string novoNSUHost = nsuHostTransacao;
                string novoNSUTef = nsuTefTransacao;

                if (!tipoLancamento.Equals("3"))
                {   if (meioCaptura != "5" || meioCaptura != "8")
                    {
                        if (nsuHostTransacao == zeros)
                        {
                            //SCF 1473
                            //if (isHomologacaoTSYS)
                            //{
                            //    request.Criticas.Add(
                            //        new CriticaSCFObrigatoriedade()
                            //        {
                            //            CodigoArquivo = request.CodigoArquivo,
                            //            CodigoArquivoItem = request.CodigoArquivoItem,
                            //            CampoNaoPreenchido = true,
                            //            NomeCampo = "NSUHost",
                            //            TipoLinha = request.TipoLinha
                            //        });
                            //}

                            //else
                            //{
                                if (numeroTotalParcelas == 0)
                                {   novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                                }
                                else if (numeroParcela == 0)
                                {   novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                                    primeiraParcela = false;
                                }
                                else if (numeroParcela == 1 && primeiraParcela)
                                {   novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, true, false);
                                    primeiraParcela = false;
                                }
                                else if (numeroParcela <= numeroTotalParcelas)
                                {   novoNSUHost = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, false);
                                }
                                request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUHostTransacao", novoNSUHost);
                                request.LinhaCorrigida = true;
                            //SCF 1473
                            //}
                        }

                        if (nsuTefTransacao == zeros)
                        {
                            //SCF 1473
                            //if (isHomologacaoTSYS)
                            //{
                            //    request.Criticas.Add(
                            //        new CriticaSCFObrigatoriedade()
                            //        {
                            //            CodigoArquivo = request.CodigoArquivo,
                            //            CodigoArquivoItem = request.CodigoArquivoItem,
                            //            CampoNaoPreenchido = true,
                            //            NomeCampo = "NSUHost",
                            //            TipoLinha = request.TipoLinha
                            //        });
                            //}
                            //else
                            //{
                                if (numeroTotalParcelas == 0)
                                {
                                    novoNSUTef = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, true);
                                }
                                else
                                {
                                    if (numeroParcela == 0 && nsuHostTransacao != zeros)
                                    {
                                        novoNSUTef = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, true);
                                        primeiraParcelaTef = false;
                                    }
                                    else if (numeroParcela == 1 && primeiraParcelaTef && nsuHostTransacao != zeros)
                                    {
                                        novoNSUTef = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, true);
                                        primeiraParcelaTef = false;
                                    }
                                    else if (numeroParcela <= numeroTotalParcelas)
                                    {
                                        novoNSUTef = this.RegraSCFCorrecaoNSU(request, this.DataMovimento, this.IDMovimento, false, false);
                                    }
                                }

                                request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "NSUTEFTransacao", novoNSUTef);
                                request.LinhaCorrigida = true;

                            //SCF 1473
                            //}
                        }
                    }
                }

                if ((cupomFiscal == zerosCupom || cupomFiscal.Trim() == "") &&
                    meioCaptura != "2" &&
                    meioCaptura != "3" &&
                    meioCaptura != "5" &&
                    meioCaptura != "6" &&
                    meioCaptura != "8" &&
                    meioCaptura != "9")
                {
                    string novoCupomFiscal = novoNSUHost.Substring(2);
                    request.Linha = request.Arquivo.EscreverCampo(this.TipoLinha, request.Linha, "CupomFiscal", novoCupomFiscal);
                    request.LinhaCorrigida = true;

                }
            }
        }
    }
}