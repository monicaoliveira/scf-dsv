﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Types;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFCorrecaoAVParcela : RegraSCFLinhaBase
    {
        private Dictionary<string, string> _valores = null;

        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {   List<ListaItemReferenciaInfo> listaItemReferencias = PersistenciaHelper.Listar<ListaItemReferenciaInfo>(null);
            _valores = new Dictionary<string, string>();
            foreach (ListaItemReferenciaInfo listaReferenciaInfo in listaItemReferencias)
                if (listaReferenciaInfo.CodigoListaItemSituacao == "CodigoAnulacao"
                    && listaReferenciaInfo.NomeReferencia == "Produto"
                    && _valores.ContainsKey(listaReferenciaInfo.listaItemInfo.Valor) == false)
                    _valores.Add(listaReferenciaInfo.listaItemInfo.Valor, listaReferenciaInfo.ValorReferencia);
        }

        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   //================================================================================================================//
            //1408 if (request.TipoLinha == "AV")
            //Não efetuar correção da linha caso possua crítica;
            //================================================================================================================//
            if (request.TipoLinha == "AV" && request.Criticas.Count == 0)
            {
                request.Executou_Regra = true; //1407
                string codigoAnulacao = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "CodigoAnulacao", true));
                if (_valores.Count > 0 && _valores.ContainsKey(codigoAnulacao) && _valores[codigoAnulacao] == "CRAF")
                {
                    decimal valorLiquidoParcela = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoParcela", true), typeof(decimal));
                    decimal valorBrutoParcela = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoParcela", true), typeof(decimal));
                    decimal valorDescontoParcela = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDescontoParcela", true), typeof(decimal));

                    if (valorBrutoParcela == 0 &&
                        valorDescontoParcela == 0 &&
                        valorLiquidoParcela == 0)
                    {
                        decimal valorBrutoVenda = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoVenda", true), typeof(decimal));
                        decimal valorDescontoVenda = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDescontoOuComissao", true), typeof(decimal));
                        decimal valorLiquidoVenda = (decimal)Convert.ChangeType(request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoVenda", true), typeof(decimal));

                        string linha = "";

                        linha = request.Arquivo.EscreverCampo(request.TipoLinha, request.Linha, "ValorLiquidoParcela", Convert.ChangeType(valorLiquidoVenda, typeof(double)), true);
                        linha = request.Arquivo.EscreverCampo(request.TipoLinha, linha, "ValorDescontoParcela", Convert.ChangeType(valorDescontoVenda, typeof(double)), true);
                        linha = request.Arquivo.EscreverCampo(request.TipoLinha, linha, "ValorBrutoParcela", Convert.ChangeType(valorBrutoVenda, typeof(double)), true);

                        request.Linha = linha;
                        request.LinhaCorrigida = true;
                    }
                }
            }
        }
    }
}
