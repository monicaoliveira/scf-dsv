﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Integracao.Arquivos;
using System.Text.RegularExpressions;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353
using Resource.Framework.Contratos.Comum; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFNumero : RegraSCFColunaBase
    {
        
        public bool EhObrigatorio { get; set; }
        public int Tamanho { get; set; }
        public bool ValidaNegativo { get; set; }

        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            bool erroObrigatoriedade = false;
            bool erroNumero = false;
            if (this.TipoLinha == request.TipoLinha)
            {
                request.Executou_Regra = true; //1407
                string valor =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                if (EhObrigatorio)
                {
                    if (valor == "0".PadRight(Tamanho, '0') || valor == "" )
                    {
                        erroObrigatoriedade = true;
                        request.Criticas.Add(
                                new CriticaSCFObrigatoriedade

                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    CampoNaoPreenchido = true,
                                    Valor = valor,
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = request.TipoLinha
                                }
                         );                        
                    }
                }
                
                if (!erroObrigatoriedade)
                {
                    Regex isnumber = new Regex("[0-9]");
                    if (!isnumber.IsMatch(valor))
                    {
                        erroNumero = true;
                        request.Criticas.Add(
                            new CriticaSCFNumero
                            {
                                CodigoArquivo = request.CodigoArquivo,
                                CodigoArquivoItem = request.CodigoArquivoItem,
                                FormatoInvalido = true,
                                Valor = valor,
                                NomeCampo = this.NomeCampo,
                                TipoLinha = request.TipoLinha
                            }
                        );
                    }

                    if (!erroNumero && ValidaNegativo)
                    {
                        Regex isNegativeNumber = new Regex("-[0-9]");
                        if (isNegativeNumber.IsMatch(valor))
                        {
                            request.Criticas.Add(
                                new CriticaSCFEspecifica()
                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    TipoLinha = request.TipoLinha,
                                    NomeCampo = this.NomeCampo,
                                    Descricao = "O valor não pode ser menor que 0."
                                });
                        }
                    }
                }

            }
        }
    }
}
