﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{
    public class RegraSCFPlano : RegraSCFColunaBase
    {
        #region Atributos da Regra
        #endregion

        #region Execução da Validação

        private Dictionary<string, PlanoInfo> _valores = null;
        private Dictionary<string, string> _valoresProdutos = null;
        private List<string> _valoresPlanosProdutos = null;

        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            _valores = new Dictionary<string, PlanoInfo>();
            List<PlanoInfo> planos =PersistenciaHelper.Listar<PlanoInfo>(null);
            foreach (PlanoInfo plano in planos)
                if (!_valores.ContainsKey(plano.CodigoPlanoTSYS))
                    _valores.Add(plano.CodigoPlanoTSYS, plano);

            _valoresProdutos = new Dictionary<string, string>();
            List<ProdutoInfo> produtos =PersistenciaHelper.Listar<ProdutoInfo>(null);
            foreach (ProdutoInfo produto in produtos)
                if (!_valoresProdutos.ContainsKey(produto.CodigoProdutoTSYS))
                    _valoresProdutos.Add(produto.CodigoProdutoTSYS, produto.CodigoProduto);

            _valoresPlanosProdutos = new List<string>();
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;

            cursor = Procedures.PR_PRODUTO_PLANO_L(cn);
            OracleDataReader reader = cursor.GetDataReader();
            while (reader.Read())
            {
                string codigoProduto = reader["CODIGO_PRODUTO"].ToString();
                string codigoPlano = reader["CODIGO_PLANO"].ToString();

                // Guarda na lista
                _valoresPlanosProdutos.Add(codigoProduto+codigoPlano);
            }
        }
        
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {   var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            
            if (this.TipoLinha == request.TipoLinha) 
            {
                request.Executou_Regra = true; //1407
                string valor = "0"+((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, this.NomeCampo, false)).Trim();
                string valorDescontoVendaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDesconto", false)).Trim();
                double valorDescontoVenda = (double.Parse(valorDescontoVendaString))/100;
                string modalidadeVenda =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ModalidadeVenda", false)).Trim();
                string valorBrutoVendaString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoVenda", false)).Trim();
                double valorBrutoVenda = (double.Parse(valorBrutoVendaString))/100;
                if (!_valores.ContainsKey(valor))
                    request.Criticas.Add(
                        new CriticaSCFDominio()
                        {
                            CodigoArquivo = request.CodigoArquivo,
                            CodigoArquivoItem = request.CodigoArquivoItem,
                            ValorNaoEncontrado = valor,
                            NomeCampo = this.NomeCampo,
                            TipoLinha = request.TipoLinha,
                            Descricao = string.Format("Campo {0}; Plano {1} não cadastrado", this.NomeCampo, valor)

                        });
                    
                else
                {   string codigoTSYSProduto =((string)request.Arquivo.LerCampo("CV", (object)request.Linha, "ProdutoCSF", false)).Trim();
                    PlanoInfo plano = _valores[valor];
                    if (!_valoresProdutos.ContainsKey(codigoTSYSProduto))
                    {
                        request.Criticas.Add(
                                new CriticaSCFDominio()
                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    ValorNaoEncontrado = valor,
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = request.TipoLinha,
                                    Descricao = string.Format("Campo {0}; Produto {1} não encontrado para validação do plano", this.NomeCampo, codigoTSYSProduto)

                                });
                    }
                    else
                    {
                        string codigoProdutoInterno = _valoresProdutos[codigoTSYSProduto];
                        if (!_valoresPlanosProdutos.Contains(codigoProdutoInterno + plano.CodigoPlano))
                        {
                            request.Criticas.Add(
                                new CriticaSCFDominio()
                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    ValorNaoEncontrado = valor,
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = request.TipoLinha,
                                    Descricao = string.Format("Campo {0}; Produto {1} ({2}) não associado ao Plano {3}.", this.NomeCampo, codigoProdutoInterno, codigoTSYSProduto, plano.CodigoPlanoTSYS)
                                });
                        }
                    }
                    if (plano.CodigoPlanoTSYS == valor && modalidadeVenda == "0")
                    {
                        if (plano.Comissao == 0 && plano.Comissao != valorDescontoVenda)
                            request.Criticas.Add(
                                new CriticaSCFEspecifica
                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    Descricao = string.Format("Plano ({0}) sem comissao - Transacao com comissao - Produto ({1}) Modalidade ({2})", valor, codigoTSYSProduto, modalidadeVenda),
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = request.TipoLinha
                                });
                    }
                    if (plano.CodigoPlanoTSYS == valor && modalidadeVenda == "0")
                    {
                        if (plano.Comissao != 0 && valorDescontoVenda == 0 && valorBrutoVenda >= 2) //SCF1298
                            request.Criticas.Add(
                                new CriticaSCFEspecifica
                                {
                                    CodigoArquivo = request.CodigoArquivo,
                                    CodigoArquivoItem = request.CodigoArquivoItem,
                                    Descricao = string.Format("Plano ({0}) com comissao - Transacao sem comissao - Produto ({1}) Modalidade ({2})", valor, codigoTSYSProduto, modalidadeVenda),
                                    NomeCampo = this.NomeCampo,
                                    TipoLinha = request.TipoLinha
                                });
                    }
                }
            }
        }
        #endregion
    }
}
