﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1353

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Regras
{

    public class RegraSCFEspecificaAP : RegraSCFLinhaBase 
    {
        #region Atributos da Regra

        #endregion

        #region Execução da Validação
        protected override void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
        
        protected override void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            if (this.TipoLinha == request.TipoLinha)
            {   request.Executou_Regra = true; //1407
                string valorLiquidoPagamentoString = ((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorLiquidoPagamento", false)).Trim();
                string valorBrutoPagamentoString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorBrutoPagamento", false)).Trim();
                string valorDescontoPagamentoString =((string)request.Arquivo.LerCampo(request.TipoLinha, (object)request.Linha, "ValorDesconto", false)).Trim();

                double valorLiquidoPagamento = double.Parse(valorLiquidoPagamentoString);
                double valorBrutoPagamento = double.Parse(valorBrutoPagamentoString);
                double valorDescontoPagamento = double.Parse(valorDescontoPagamentoString);

                if (valorLiquidoPagamento != valorBrutoPagamento - valorDescontoPagamento)
                {
                    request.Criticas.Add(
                            new CriticaSCFEspecifica
                            {
                                Descricao = string.Format("Valor Liquido invalido. [Valor Liquido ({0}) <> ( Valor Bruto ({1}) - Valor Desconto ({2}))", valorLiquidoPagamento, valorBrutoPagamento, valorDescontoPagamento),
                                NomeCampo = this.TipoLinha,
                                TipoLinha = request.TipoLinha
                            });
                }
            }
        }

        #endregion
    }
}
