﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Sistemas.Integracao.Regras;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using System.IO;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Classe de auxilio para segurança
    /// </summary>
    public static class RegrasSCFHelper
    {
        /// <summary>
        /// Retorna o conjunto de regras de acordo com o Layout
        /// </summary>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public static List<RegraSCFBase> RegrasTSYS(ArquivoTSYS arquivo)
        {

            // Cria a lista de regras
            List<RegraSCFBase> regras = new List<RegraSCFBase>();

            // Regra para tratamento da estrutura do arquivo
            regras.Add(
                new RegraSCFEstruturaArquivo
                {
                    Escopo = RegraSCFEscopoEnum.Arquivo
                });

            //===================================================================================================================
            #region A0
            //===================================================================================================================
            string tipoLinha = "A0";

            // CODIGO DE REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = "A0",
                    EhObrigatorio = true
                });

            // VERSÃO DO LAYOUT
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "VersaoLayout",
                    NomeLista = "VersaoLayout"
                });


            // IDENTIFICAÇÃO DA AUTORIZADORA
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoAutorizadora",
                    NomeLista = "IdentificacaoAutorizada"
                });


            // DATA DE GERAÇÃO DO ARQUIVO
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataGeracaoArquivo",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros

                });


            // HORA GERACAO ARQUIVO
            BookItemCampoHelper campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "HoraGeracaoArquivo");

            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "HoraGeracaoArquivo",
                    EhObrigatorio = false,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });

            // ID DO MOVIMENTO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "IdMovimento");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdMovimento",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });


            // NOME DA ADMINISTRADORA
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NomeAdministradora",
                    EhObrigatorio = true
                });

            // NSEQ DO REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSEQ",
                    EhObrigatorio = true,
                    IgualConstante = "000000001"
                });
            #endregion

            //===================================================================================================================
            #region L0
            //===================================================================================================================
            tipoLinha = "L0";

            // CODIGO DE REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = tipoLinha,
                    EhObrigatorio = true
                });

            // DATA DO MOVIMENTO
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataMovimento",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    //CampoMaiorQueEstaData = new DateTime().

                });

            // IDENTIFICACAO DA MOEDA
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoMoeda",
                    NomeLista = "IdentificacaoMoeda"
                });

            // IDENTIFICACAO DO TEF
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoTEF",
                    NomeLista = "IdentificacaoTEF"
                });
            #endregion

            //===================================================================================================================
            #region CV
            //===================================================================================================================
            tipoLinha = "CV";

            // Regra de correção dos NSU's
            regras.Add(
                new RegraSCFCorrecaoCV()
                {
                    TipoLinha = tipoLinha
                });

            // CODIGO DE REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = tipoLinha,
                    EhObrigatorio = true
                });


            // DATA DA TRANSAÇÃO
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataTransacao",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    CampoMaiorQueEstaData = DateTime.Now

                });

            //// TIPO DE LANÇAMENTO
            //regras.Add(
            //    new RegraSCFString()
            //    {
            //        NomeCampo = "TipoLancamento",
            //        TipoLinha = tipoLinha,
            //        IgualConstante = "1",
            //        EhObrigatorio = true
            //    });


            // 20150928 - Ecommerce 
            // Retirada da validacao fixa do Tipo de Lancamento (no caso do CV, a regra já estava inibida - vide acima) 
            // Passar a validar o cadastrado, de acordo com o tipo de registro
            // Tipo de Lancamento
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "TipoLancamento",
                    NomeLista = "TipoLancamentoCV"
                });

            // 20150928 - Ecommerce 

            // DATA DE REPASSE DO CREDITO
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataRepasseCredito",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros

                });

            // NÚMERO DO CARTÃO
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao",
                    EhObrigatorio = true
                });
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao",
                    DiferenteConstante = new string('0', 19)
                });

            // BANCO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Banco");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Banco",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });

            // AGENCIA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Agencia");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Agencia",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // CONTA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Conta");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Conta",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NÚMERO DA CONTA CLIENTE
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroContaCliente");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroContaCliente",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NSEQ DO REGISTRO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSEQ");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSEQ",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // TIPO DO PRODUTO
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "TipoProduto",
                    NomeLista = "TipoProduto"
                });

            // MEIO DE CAPTURA
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "MeioCaptura",
                    NomeLista = "MeioCaptura"
                });

            // VALOR BRUTO DA VENDA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorBrutoVenda");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorBrutoVenda",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });


            // VALOR LÍQUIDO DA VENDA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorLiquidoVenda");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorLiquidoVenda",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NUMERO PARCELA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroParcela");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroParcela",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // VALOR BRUTO PARCELA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorBrutoParcela");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorBrutoParcela",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // VALOR DO DESCONTO DA PARCELA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorDescontoParcela");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorDescontoParcela",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // MODALIDADE DE VENDA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ModalidadeVenda");
            regras.Add(
                    new RegraSCFNumero()
                    {
                        TipoLinha = tipoLinha,
                        NomeCampo = "ModalidadeVenda",//1426 ??"ValorDescontoParcela",
                        Tamanho = campo.BookItemCampoInfo.Tamanho

                    });


            // VALOR DO DESCONTO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorDesconto");

            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorDesconto",
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });

            regras.Add(
                new RegraSCFValorMaiorQue()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorDesconto",
                    NomeCampo2 = "ValorBrutoVenda"
                });

            regras.Add(
                new RegraSCFValorMaiorQue()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorLiquidoVenda",
                    NomeCampo2 = "ValorBrutoVenda"
                });

            regras.Add(
                 new RegraSCFEspecificaCV()
                 {
                     Escopo = RegraSCFEscopoEnum.Linha,
                     TipoLinha = tipoLinha

                 });


            // produto
            regras.Add(
                new RegraSCFProduto()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ProdutoCSF"

                });

            // MEIO DE CAPTURA
            regras.Add(
                new RegraSCFPlano()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "CodigoPlano"
                });

            // Estabelecimento
            regras.Add(
                new RegraSCFEstabelecimento()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"

                });

            regras.Add(
                new RegraSCFValorParcela()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroParcela",
                    NomeCampo2 = "ValorDescontoParcela",
                    NomeCampo3 = "ValorBrutoParcela",
                    NomeCampo4 = "ValorLiquidoParcela"
                });

            // Regra de correção da parcela (se nao for homologacao TSYS)
            //if (!arquivo.isHomologacaoTSYS)
            //{
                regras.Add(
                    new RegraSCFCorrecaoCVParcela()
                    {
                        TipoLinha = tipoLinha
                    });
            //}

            // insere a regra para nao atualizar o NSU, caso o parametro esteja como nao
            if (!arquivo.atualizaNSU) // CV
            {

                regras.Add(
                    new RegraSCFNSUExistente()
                    {
                        TipoLinha = tipoLinha
                    });
            }

            // BIN Referencia CV
            regras.Add(
                new RegraSCFBinReferencia()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao"

                });

            //SCF1340 - REGRA PARA VALIDAR CONTA FAVORECIDO
            regras.Add(
                new RegraSCFBancoAgenciaContaFavorecido()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"
                });


            #endregion

            //===================================================================================================================
            #region CP
            //===================================================================================================================
            tipoLinha = "CP";

            regras.Add(
                new RegraSCFCorrecaoCP()
                {
                    TipoLinha = tipoLinha
                });

            // CODIGO DE REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = tipoLinha,
                    EhObrigatorio = true
                });

            // NSUHostTransacao
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUHostTransacao");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSUHostTransacao",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NSUTEFTransacao
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUTEFTransacao");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSUTEFTransacao",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // DATA DA TRANSAÇÃO
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataTransacao",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    CampoMaiorQueEstaData = DateTime.Now

                });

            // 20150928 - Ecommerce 
            // Retirada da validacao fixa do Tipo de Lancamento e 
            // Passar a validar o cadastrado, de acordo com o tipo de registro
            // TIPO DE LANÇAMENTO

            /*regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "TipoLancamento",
                    TipoLinha = tipoLinha,
                    IgualConstante = "1",
                    EhObrigatorio = true
                });

            */

            // Tipo de Lancamento
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "TipoLancamento",
                    NomeLista = "TipoLancamentoCP"
                });

            // 20150928 - Ecommerce 




            // DATA DE REPASSE DO CREDITO
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataRepasseCredito",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros

                });

            // VALOR TOTAL DO PAGAMENTO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorTotalPagamento");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorTotalPagamento",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NÚMERO DO CARTÃO
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao",
                    EhObrigatorio = true
                });
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao",
                    DiferenteConstante = new string('0', 19)
                });

            // NÚMERO TOTAL DE MEIOS DE PAGAMENTO UTILIZADOS NESSE PAGAMENTO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroTotalMeioPagamento");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroTotalMeioPagamento",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });


            // BANCO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Banco");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Banco",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });

            // AGENCIA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Agencia");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Agencia",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // CONTA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Conta");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Conta",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NÚMERO DA CONTA CLIENTE
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroContaCliente");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroContaCliente",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NSEQ DO REGISTRO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSEQ");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSEQ",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // MEIO DE CAPTURA
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "MeioCaptura",
                    NomeLista = "MeioCaptura"
                });


            // MEIO DE PAGAMENTO
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "MeioPagamento",
                    NomeLista = "MeioPagamento"
                });

            // Estabelecimento
            regras.Add(
                new RegraSCFEstabelecimento()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"

                });

            ///////// REGRAS ESPECIFICAS
            // VALOR DO DESCONTO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorDesconto");

            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorDesconto",
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });

            regras.Add(
                new RegraSCFValorMaiorQue()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorDesconto",
                    NomeCampo2 = "ValorTotalPagamento"
                });

            regras.Add(
                new RegraSCFValorMaiorQue()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorLiquidoPagamento",
                    NomeCampo2 = "ValorTotalPagamento"
                });


            regras.Add(
                 new RegraSCFEspecificaCP()
                 {
                     Escopo = RegraSCFEscopoEnum.Linha,
                     TipoLinha = tipoLinha
                 });

            // insere a regra para nao atualizar o NSU, caso o parametro esteja como nao
            if (!arquivo.atualizaNSU)
            {
                regras.Add(
                    new RegraSCFNSUExistente()
                    {
                        TipoLinha = tipoLinha,
                        NomeCampo = "NSUHostTransacao"
                    });
            }

            // BIN Referencia CP
            regras.Add(
                new RegraSCFBinReferencia()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao"

                });

            //SCF1340 - REGRA PARA VALIDAR PRODUTO
            regras.Add(
                new RegraSCFProduto()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ProdutoCSF"

                });

            //SCF1340 - REGRA PARA VALIDAR CONTA FAVORECIDO
            regras.Add(
                new RegraSCFBancoAgenciaContaFavorecido()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"
                });


            #endregion

            //===================================================================================================================
            #region AJ
            //===================================================================================================================
            tipoLinha = "AJ";

            // Regra de correção dos NSU's (quando não é homologacao TSYS)
            regras.Add(
                new RegraSCFCorrecaoAJ()
                {
                    TipoLinha = tipoLinha
                });

            // CODIGO DE REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = tipoLinha,
                    EhObrigatorio = true
                });

            // NSU HOST DA TRANSAÇÃO ORIGINAL
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUHostTransacaoOriginal");
            //regras.Add(
            //    new RegraSCFNumero()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "NSUHostTransacaoOriginal",
            //        EhObrigatorio = true,
            //        Tamanho = campo.BookItemCampoInfo.Tamanho

            //    });

            // NSU HOST DA TRANSAÇÃO
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUHostTransacao");
            //regras.Add(
            //    new RegraSCFNumero()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "NSUHostTransacao",
            //        EhObrigatorio = true,
            //        Tamanho = campo.BookItemCampoInfo.Tamanho

            //    });

            // DATA DA TRANSAÇÃO ORIGINAL

            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataTransacaoOriginal",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    CampoMaiorQueEstaData = DateTime.Now

                });

            // DATA DA TRANSAÇÃO ORIGINAL
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataTransacao",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    CampoMaiorQueEstaData = DateTime.Now

                });

            // 20150928 - Ecommerce 
            // Retirada da validacao fixa do Tipo de Lancamento e 
            // Passar a validar o cadastrado, de acordo com o tipo de registro
            // TIPO DE LANÇAMENTO

            /*regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "TipoLancamento",
                    TipoLinha = tipoLinha,
                    IgualConstante = "1",
                    EhObrigatorio = true
                });

            */

            // Tipo de Lancamento
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "TipoLancamento",
                    NomeLista = "TipoLancamentoAJ"
                });

            // 20150928 - Ecommerce 

            // DATA DE REPASSE
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataRepasse",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros
                });

            // DESCRIÇÃO DO MOTIVO DO AJUSTE
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DescricaoMotivoAjuste",
                    EhObrigatorio = true
                });


            // BANCO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Banco");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Banco",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });

            // AGENCIA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Agencia");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Agencia",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // CONTA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Conta");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Conta",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });


            //=================================================================================================================================
            // 1262 INIBIDO ESTÁ CONTIDO NOS REGRAS ESPECIFICAS DO AJ
            //=================================================================================================================================
            // Este codigo abaixo esta incorreto
            // Informa o tamanho do campo "NumeroCartaoTransacaoOriginal" mas envia o campo "Conta"
            // Informa do descritivo NUMERO DA CONTA CLIENTE mas envia campo "Conta" já tratado acima
            //=================================================================================================================================
            // NÚMERO DA CONTA CLIENTE
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroCartaoTransacaoOriginal");
            //regras.Add(
            //    new RegraSCFNumero()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "Conta",
            //        EhObrigatorio = true,
            //        Tamanho = campo.BookItemCampoInfo.Tamanho
            //    });
            //=================================================================================================================================


            // NSEQ DO REGISTRO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSEQ");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSEQ",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // TIPO DE AJUSTE
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "TipoAjuste",
                    NomeLista = "TipoAjuste"
                });

            // CÓDIGO DO AJUSTE
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "CodigoAjuste",
                    NomeLista = "CodigoAjuste"
                });

            // Referencias para o dominio
            //regras.Add(
            //    new RegraSCFReferenciaDominio()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "CodigoAjuste",
            //        SituacaoReferencia = ListaItemReferenciaSituacaoEnum.CodigoAjuste,
            //        NomeReferencia = ListaItemReferenciaNomeEnum.Produto
            //    });

            // VALOR BRUTO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorBruto");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorBruto",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            regras.Add(
                 new RegraSCFEspecificaAJ()
                 {
                     Escopo = RegraSCFEscopoEnum.Linha,
                     TipoLinha = tipoLinha
                 });


            // ValorLiquido
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorLiquido");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorLiquido",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });

            // ValorBruto
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorBruto");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorBruto",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });


            //// TODO: ACREDITO QUE ESTA REGRA ESTEJA ERRADA, VER COM A MONICA
            //// ESTÁ COMO MANDATORIO, MAS NO CAMPO REGRAS DIZ QUE É ERRO APENAS SE FOR < 0
            // ValorDescontoOuComissao
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorDescontoOuComissao");
            //regras.Add(
            //    new RegraSCFNumero()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "ValorDescontoOuComissao",
            //        EhObrigatorio = true,
            //        Tamanho = campo.BookItemCampoInfo.Tamanho
            //    });

            // estabelecimento
            regras.Add(
                new RegraSCFEstabelecimento()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"

                });

            // insere a regra para nao atualizar o NSU, caso o parametro esteja como nao
            if (!arquivo.atualizaNSU)
            {
                regras.Add(
                    new RegraSCFNSUExistente()
                    {
                        TipoLinha = tipoLinha,
                        NomeCampo = "NSUHostTransacao"
                    });
            }

            // BIN Referencia AJ
            RegraSCFBinReferencia REGRABIN =
                new RegraSCFBinReferencia()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartaoTransacaoOriginal"

                };
            regras.Add(REGRABIN);

            //SCF1340 - REGRA PARA VALIDAR PRODUTO
            regras.Add(
                new RegraSCFProduto()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ProdutoCSF"

                });

            //SCF1340 - REGRA PARA VALIDAR CONTA FAVORECIDO
            regras.Add(
                new RegraSCFBancoAgenciaContaFavorecido()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"
                });

            #endregion

            //===================================================================================================================
            #region AV
            //===================================================================================================================
            tipoLinha = "AV";

            // Regra de correção dos NSU's
            regras.Add(
                new RegraSCFCorrecaoAV()
                {
                    TipoLinha = tipoLinha
                });

            // CODIGO DE REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = tipoLinha,
                    EhObrigatorio = true
                });

            // NSU HOST DA TRANSAÇÃO ORIGINAL
            /*
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUHostTransacaoOriginal");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSUHostTransacaoOriginal",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });
             */

            // NSU TEF DA TRANSAÇÃO ORIGINAL

            // TODO: A validação abaixo é para o campo NSUHostTransacao ou NSUTefTransacaoOriginal??? Esta regra está duplicada com a regra abaixo de NSU HOST DA TRANSACAO

            /*
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUTefTransacaoOriginal");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSUHostTransacao",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });
             */


            // DATA DA TRANSAÇÃO ORIGINAL
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataTransacaoOriginal",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    CampoMaiorQueEstaData = DateTime.Now
                });

            // NSU HOST DA TRANSAÇÃO
            /*
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUHostTransacao");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSUHostTransacao",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });
             */

            // CÓDIGO DE AUTORIZAÇÃO DA TRANSAÇÃO ORIGINAL
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "CodigoAutorizacaoTransacaoOriginal");
            //regras.Add(
            //    new RegraSCFNumero()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "CodigoAutorizacaoTransacaoOriginal",
            //        EhObrigatorio = true,
            //        Tamanho = campo.BookItemCampoInfo.Tamanho

            //    });


            // DATA DA TRANSAÇÃO
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataTransacao",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    CampoMaiorQueEstaData = DateTime.Now
                });


            // 20150928 - Ecommerce 
            // Retirada da validacao fixa do Tipo de Lancamento e 
            // Passar a validar o cadastrado, de acordo com o tipo de registro
            // TIPO DE LANÇAMENTO

            /*regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "TipoLancamento",
                    TipoLinha = tipoLinha,
                    IgualConstante = "1",
                    EhObrigatorio = true
                });

            */

            // Tipo de Lancamento
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "TipoLancamento",
                    NomeLista = "TipoLancamentoAV"
                });

            // 20150928 - Ecommerce 


            // DATA DE REEMBOLSO AO EMISSOR
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataReembolsoEmissor",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros
                });

            // DESCRIÇÃO DO MOTIVO DA ANULAÇÃO
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DescricaoMotivoAnulacao",
                    EhObrigatorio = true
                });

            // NÚMERO DO CARTÃO
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao",
                    EhObrigatorio = true
                });
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao",
                    DiferenteConstante = new string('0', 19)
                });

            // NÚMERO DO CARTÃO
            //regras.Add(
            //    new RegraSCFString()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "NumeroCartao",
            //        EhObrigatorio = true
            //    });


            // BANCO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Banco");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Banco",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });

            // AGENCIA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Agencia");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Agencia",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // CONTA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Conta");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "Conta",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NÚMERO DA CONTA CLIENTE
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroContaCliente");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroContaCliente",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });


            // NSEQ DO REGISTRO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSEQ");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSEQ",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // CÓDIGO DA ANULAÇÃO
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "CodigoAnulacao",
                    NomeLista = "CodigoAnulacao"
                });

            // Referencia para o dominio
            //regras.Add(
            //    new RegraSCFReferenciaDominio()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "CodigoAnulacao",
            //        SituacaoReferencia = ListaItemReferenciaSituacaoEnum.CodigoAnulacao,
            //        NomeReferencia = ListaItemReferenciaNomeEnum.Produto
            //    });

            // VALOR BRUTO DA VENDA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorBrutoVenda");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorBrutoVenda",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });


            //// TODO: ACREDITO QUE ESTA REGRA ESTEJA ERRADA, VER COM A MONICA
            //// ESTÁ COMO MANDATORIO, MAS NO CAMPO REGRAS DIZ QUE É ERRO APENAS SE FOR < 0
            // VALOR DESCONTO OU COMISSAO
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorDescontoOuComissao");
            //regras.Add(
            //    new RegraSCFNumero()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "ValorDescontoOuComissao",
            //        EhObrigatorio = true,
            //        Tamanho = campo.BookItemCampoInfo.Tamanho

            //    });

            // VALOR LÍQUIDO DA VENDA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorLiquidoVenda");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorLiquidoVenda",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            regras.Add(
                new RegraSCFValorMaiorQue()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorLiquidoVenda",
                    NomeCampo2 = "ValorBrutoVenda"
                });

            // NUMERO PARCELA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroParcela");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroParcela",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // NÚMERO TOTAL DE PARCELAS
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroTotalParcelas");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroTotalParcelas",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // VALOR BRUTO DA PARCELA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorBrutoParcela");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorBrutoParcela",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // VALOR DO DESCONTO DA PARCELA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorDescontoParcela");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorDescontoParcela",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // VALOR LÍQUIDO DA PARCELA
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorLiquidoParcela");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorLiquidoParcela",
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // estabelecimento
            regras.Add(
                new RegraSCFEstabelecimento()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"

                });

            regras.Add(
                 new RegraSCFEspecificaAV()
                 {
                     Escopo = RegraSCFEscopoEnum.Linha,
                     TipoLinha = tipoLinha
                 });

            // correcao de parcelas
            //if (!arquivo.isHomologacaoTSYS)
                regras.Add(
                    new RegraSCFCorrecaoAVParcela()
                    {
                        TipoLinha = tipoLinha
                    });

            // insere a regra para nao atualizar o NSU, caso o parametro esteja como nao
            if (!arquivo.atualizaNSU)
            {
                regras.Add(
                    new RegraSCFNSUExistente()
                    {
                        TipoLinha = tipoLinha,
                        NomeCampo = "NSUHostTransacao"
                    });
            }

            // BIN Referencia AV
            regras.Add(
                new RegraSCFBinReferencia()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao"

                });

            //SCF1340 - REGRA PARA VALIDAR PRODUTO
            regras.Add(
                new RegraSCFProduto()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ProdutoCSF"

                });

            //SCF1340 - REGRA PARA VALIDAR CONTA FAVORECIDO
            regras.Add(
                new RegraSCFBancoAgenciaContaFavorecido()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"
                });
            #endregion

            //===================================================================================================================
            #region AP
            //===================================================================================================================
            tipoLinha = "AP";

            // Regra de correção dos NSU's
            regras.Add(
                new RegraSCFCorrecaoAP()
                {
                    TipoLinha = tipoLinha
                });

            // NSU HOST DA TRANSAÇÃO ORIGINAL
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUHostTransacaoOriginal");
            // regras.Add(
            //     new RegraSCFNumero()
            //     {
            //         TipoLinha = tipoLinha,
            //         NomeCampo = "NSUHostTransacaoOriginal",
            //         EhObrigatorio = true,
            //         Tamanho = campo.BookItemCampoInfo.Tamanho

            //     });

            // NSU TEF DA TRANSAÇÃO ORIGINAL
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUTefTransacaoOriginal");
            // regras.Add(
            //     new RegraSCFNumero()
            //     {
            //         TipoLinha = tipoLinha,
            //         NomeCampo = "NSUHostTransacao",
            //         EhObrigatorio = true,
            //         Tamanho = campo.BookItemCampoInfo.Tamanho

            //     });


            // NSU HOST DA TRANSAÇÃO
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSUHostTransacao");
            //regras.Add(
            //    new RegraSCFNumero()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "NSUHostTransacao",
            //        EhObrigatorio = true,
            //        Tamanho = campo.BookItemCampoInfo.Tamanho

            //    });

            // CODIGO DE REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = tipoLinha,
                    EhObrigatorio = true
                });



            // DATA DA TRANSAÇÃO ORIGINAL
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataTransacaoOriginal",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    CampoMaiorQueEstaData = DateTime.Now
                });


            // CÓDIGO DE AUTORIZAÇÃO DA TRANSAÇÃO ORIGINAL
            // VOLTAR:
            //campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "CodigoAutorizacaoTransacaoOriginal");
            //regras.Add(
            //    new RegraSCFNumero()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "CodigoAutorizacaoTransacaoOriginal",
            //        EhObrigatorio = true,
            //        Tamanho = campo.BookItemCampoInfo.Tamanho

            //    });



            // DATA DA TRANSAÇÃO
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataTransacao",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros,
                    CampoMaiorQueEstaData = DateTime.Now
                });


            // 20150928 - Ecommerce 
            // Retirada da validacao fixa do Tipo de Lancamento e 
            // Passar a validar o cadastrado, de acordo com o tipo de registro
            // TIPO DE LANÇAMENTO

            /*regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "TipoLancamento",
                    TipoLinha = tipoLinha,
                    IgualConstante = "1",
                    EhObrigatorio = true
                });

            */

            // Tipo de Lancamento
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "TipoLancamento",
                    NomeLista = "TipoLancamentoAP"
                });

            // 20150928 - Ecommerce 


            // DATA DE REEMBOLSO AO EMISSOR
            regras.Add(
                new RegraSCFData()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "DataReembolsoEstabelecimento",
                    EhObrigatorio = true,
                    Formato = "YYYYMMDD",
                    TipoValidacaoVazioData = TipoValidacaoVazioDataEnum.Zeros
                });

            //// TODO: ACREDITO QUE ESTA REGRA ESTEJA ERRADA, VER COM A MONICA
            //// ESTÁ COMO OPCIONAL, MAS NO CAMPO REGRAS DIZ QUE É ERRO SE FOR BRANCOS
            // DESCRIÇÃO DO MOTIVO DA ANULAÇÃO
            //regras.Add(
            //    new RegraSCFString()
            //    {
            //        TipoLinha = tipoLinha,
            //        NomeCampo = "DescricaoMotivoAnulacao",
            //        EhObrigatorio = true
            //    });

            // VALOR BRUTO DO PAGAMENTO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorBrutoPagamento");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorBrutoPagamento",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            regras.Add(
                new RegraSCFValorMaiorQue()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorLiquidoPagamento",
                    NomeCampo2 = "ValorBrutoPagamento"
                });



            // NÚMERO DO CARTÃO
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao",
                    EhObrigatorio = true
                });
            regras.Add(
                new RegraSCFString()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao",
                    DiferenteConstante = new string('0', 19)
                });

            // NÚMERO DA CONTA CLIENTE
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NumeroContaCliente");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroContaCliente",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });


            // NSEQ DO REGISTRO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSEQ");
            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NSEQ",
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho

                });

            // Regra removida 29/11/2011 em pois será ajustada de acordo com a referência de domínio
            // FORMA DE MEIO DE PAGAMENTO
            //if (!arquivo.isHomologacaoTSYS)
                regras.Add(
                    new RegraSCFDominio()
                    {
                        TipoLinha = tipoLinha,
                        NomeCampo = "FormaMeioPagamento",
                        NomeLista = "MeioPagamento"
                    });

            // CÓDIGO DA ANULAÇÃO
            regras.Add(
                new RegraSCFDominio()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "CodigoAnulacao",
                    NomeLista = "CodigoAnulacao"
                });

            // Referencia do dominio
            //if (!isHomologacaoTSYS)
            //    regras.Add(
            //        new RegraSCFReferenciaDominio()
            //        {
            //            TipoLinha = tipoLinha,
            //            NomeCampo = "CodigoAnulacao",
            //            SituacaoReferencia = ListaItemReferenciaSituacaoEnum.CodigoAnulacao,
            //            NomeReferencia = ListaItemReferenciaNomeEnum.MeioPagamento
            //        });

            // VALOR DO DESCONTO
            campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "ValorDesconto");

            regras.Add(
                new RegraSCFNumero()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ValorDesconto",
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });


            regras.Add(
                 new RegraSCFEspecificaAP()
                 {
                     Escopo = RegraSCFEscopoEnum.Linha,
                     TipoLinha = tipoLinha
                 });

            // estabelecimento
            regras.Add(
                new RegraSCFEstabelecimento()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"

                });

            // insere a regra para nao atualizar o NSU, caso o parametro esteja como nao
            if (!arquivo.atualizaNSU)
            {
                regras.Add(
                    new RegraSCFNSUExistente()
                    {
                        TipoLinha = tipoLinha,
                        NomeCampo = "NSUHostTransacao"
                    });
            }

            // BIN Referencia AP
            regras.Add(
                new RegraSCFBinReferencia()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "NumeroCartao"

                });

            //SCF1340 - REGRA PARA VALIDAR PRODUTO
            regras.Add(
                new RegraSCFProduto()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "ProdutoCSF"

                });

            //SCF1340 - REGRA PARA VALIDAR CONTA FAVORECIDO
            regras.Add(
                new RegraSCFBancoAgenciaContaFavorecido()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"
                });

            #endregion

            //===================================================================================================================
            #region A9
            //===================================================================================================================
            tipoLinha = "A9";

            // CODIGO DE REGISTRO
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = tipoLinha,
                    EhObrigatorio = true
                });
            #endregion

            // Retorna
            return regras;
        }
    }


}
