﻿/* ALTERADO
 * 1407 - Alterando para ler apenas o tipo de registro A9
 *
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Persistencia.Db;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    public class ServicoValidarRegistroSemA9
    {
        public void ValidarRegistroSemA9(string codigoArquivo)
        {
            OracleConnection cn = Procedures.ReceberConexao();

            string v_tipoArquivoItem = "A9";
            int v_maxLinhas = 0;
            bool v_filtrarSobAnalise = false;
            bool linhaA9 = false;

			PR_ARQUIVO_ITEM_L_Request requestArquivoItem =
			new PR_ARQUIVO_ITEM_L_Request()
			{	CodigoArquivo = codigoArquivo				
			,	TipoArquivoItem = v_tipoArquivoItem
            ,   MaxLinhas = v_maxLinhas
			,	FiltrarSobAnalise = v_filtrarSobAnalise 				          
			};


            OracleDataReader readerA9 = Procedures.PR_ARQUIVO_ITEM_L
			(	cn
			,	requestArquivoItem
			).GetDataReader();
			

            while (readerA9.Read())
            {   string linha =  readerA9.GetString(readerA9.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));
                string tipoLinha =readerA9.GetString(readerA9.GetOrdinal("TIPO_ARQUIVO_ITEM"));
                if (tipoLinha == "A9")
                {
                    linhaA9 = true;
                }
            }

            if (linhaA9 == false)
            {
                throw new Exception("Arquivo incompleto, linha A9 não recebida. ");
            }
        }
    }
}
/* ORIGINAL
 * 1407 - Alterando para ler apenas o tipo de registro A9
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Persistencia.Db;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    //Classe criada para atender o Jira SCF1142 - Marcos Matsuoka
    //Alteração do nome do arquivo para atender tambem ao processo Extrato (De: ProcessoAtacadaoEstagioValidacaoTsys Para: ServicoValidarRegistroSemA9)

    public class ServicoValidarRegistroSemA9
    {

        public void ValidarRegistroSemA9(string codigoArquivo)
        {
            bool linhaA9 = false;

            // Pega conexao com o banco
            OracleConnection cn = Procedures.ReceberConexao();

            // Pede as linhas do arquivo
            OracleDataReader readerA9 =
                Procedures.PR_ARQUIVO_ITEM_L(
                    cn, codigoArquivo, null, false, false, 0, 0, 0, false, false).GetDataReader();

            // Varre as linhas para achar a linha A9
            while (readerA9.Read())
            {
                string linha =
                    readerA9.GetString(
                        readerA9.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));

                // Acha o tipo da linha
                string tipoLinha =
                    readerA9.GetString(
                        readerA9.GetOrdinal("TIPO_ARQUIVO_ITEM"));

                if (tipoLinha == "A9")
                {
                    linhaA9 = true;
                }
            }

            if (linhaA9 == false)
            {
                throw new Exception("Arquivo incompleto, linha A9 não recebida. ");
            }
        }
    }
}

*/