﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Regra de linha
    /// </summary>
    public class RegraSCFLinhaBase : RegraSCFBase
    {
        /// <summary>
        /// Tipo da linha a que a regra se refere
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Data do movimento
        /// </summary>
        public string DataMovimento { get; set; }

        /// <summary>
        /// ID do movimento
        /// </summary>
        public string IDMovimento { get; set; }
    }
}
