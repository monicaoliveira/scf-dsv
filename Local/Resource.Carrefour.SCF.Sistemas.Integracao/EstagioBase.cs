﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Classe base de estágio
    /// </summary>
    public abstract class EstagioBase
    {
        /// <summary>
        /// Referencia para o processo
        /// </summary>
        public ProcessoBase Processo { get; set; }

        /// <summary>
        /// Método de inicializacao
        /// </summary>
        public void Inicializar(string codigoSessao)
        {
            OnInicializar(codigoSessao);
        }

        /// <summary>
        /// Método virtual para inicializacao
        /// </summary>
        protected virtual void OnInicializar(string codigoSessao)
        {
        }

        /// <summary>
        /// Método para processar
        /// </summary>
        public void Processar(string codigoSessao)
        {
            OnProcessar(codigoSessao);
        }

        /// <summary>
        /// Método virtual para processar
        /// </summary>
        protected virtual void OnProcessar(string codigoSessao)
        {
        }

        /// <summary>
        /// Método para cancelar o que foi feito
        /// </summary>
        public void Cancelar(string codigoSessao)
        {
            OnCancelar(codigoSessao);
        }

        /// <summary>
        /// Método virtual para cancelar
        /// </summary>
        protected virtual void OnCancelar(string codigoSessao)
        {
        }

        /// <summary>
        /// Valida o estágio, indicando se pode ser realizado o processamento
        /// </summary>
        /// <returns></returns>
        public bool Validar(string codigoSessao)
        {
            return OnValidar(codigoSessao);
        }

        /// <summary>
        /// Método virtual para validação
        /// </summary>
        /// <returns></returns>
        protected virtual bool OnValidar(string codigoSessao)
        {
            return true;
        }
    }
}
