﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    /// <summary>
    /// Interpretador de arquivo Atacadao
    /// </summary>
    public class ArquivoSitef : ArquivoBase
    {
        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Dicionário para achar posição dos campos
        /// A chave é o nome do campo e o valor é a posição na coleção de 
        /// campos de this.BookInfo
        /// </summary>
        private Dictionary<string, int> _campos = new Dictionary<string, int>();

        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoSitef()
        {
            // Monta o book do arquivo
            this.BookInfo = new BookInfo();

            // Adiciona os campos
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Loja", new ConversorString()));                     // 0
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Data", new ConversorData("dd/MM/yyyy")));           // 1
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Hora", new ConversorHora("HH:MM:SS")));             // 2
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Pdv", new ConversorString()));                      // 3
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Nsu", new ConversorString()));                      // 4
            this.BookInfo.Campos.Add(new BookItemCampoInfo("NsuHost", new ConversorString()));                  // 5
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Rede", new ConversorString()));                     // 6
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Produto", new ConversorString()));                  // 7
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Transacao", new ConversorString()));                // 8
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Documento", new ConversorString()));                // 9
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Valor", new ConversorNumero(typeof(double), 2, false, true)));  // 10
            this.BookInfo.Campos.Add(new BookItemCampoInfo("EstadoTransacao", new ConversorString()));          // 11
            this.BookInfo.Campos.Add(new BookItemCampoInfo("CodigoResponsavel", new ConversorString()));        // 12
            this.BookInfo.Campos.Add(new BookItemCampoInfo("DocumentoCancelado", new ConversorString()));       // 13
            this.BookInfo.Campos.Add(new BookItemCampoInfo("CodigoAutor", new ConversorString()));              // 14
            this.BookInfo.Campos.Add(new BookItemCampoInfo("NumeroParcela", new ConversorNumero(typeof(int)))); // 15
            this.BookInfo.Campos.Add(new BookItemCampoInfo("DataLancamento", new ConversorData("dd/MM/yyyy"))); // 16
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Operador", new ConversorString()));                 // 17
            this.BookInfo.Campos.Add(new BookItemCampoInfo("UsuarioPend", new ConversorString()));              // 18
            this.BookInfo.Campos.Add(new BookItemCampoInfo("DataPend", new ConversorData("dd/MM/yyyy")));       // 19
            this.BookInfo.Campos.Add(new BookItemCampoInfo("HoraPend", new ConversorString()));                 // 20
            this.BookInfo.Campos.Add(new BookItemCampoInfo("TempoRespostaRede", new ConversorString()));        // 21
            this.BookInfo.Campos.Add(new BookItemCampoInfo("Bandeira", new ConversorString()));                 // 22

            // Cria um dicionario para traduzir o nome para a posição
            for (int i = 0; i < this.BookInfo.Campos.Count; i++)
                _campos.Add(this.BookInfo.Campos[i].Nome, i);
        }

        protected override object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
            // Inicializa
            int posicao = 0;
            string[] linha2 = null;
            
            // Se passou o nome do campo, acha a posição
            if (campo is string)
                posicao = _campos[(string)campo];
            else
                posicao = (int)campo;

            // Verifica se a linha já veio quebrada
            if (linha is string[])
                linha2 = (string[])linha;
            else
                linha2 = ((string)linha).Split(',');

            // Acha o valor
            object resposta = linha2[posicao];

            // Verifica se deve fazer o parse
            if (interpretar)
            {
                // Pega o campo
                BookItemCampoInfo itemCampo = this.BookInfo.Campos[posicao];

                // Faz o parse
                resposta =
                    itemCampo.Conversor.ConverterParaObjeto(itemCampo, (string)resposta);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Método que faz a importacao da linha
        /// </summary>
        /// <param name="codigoArquivo"></param>
        /// <param name="numeroLinha"></param>
        /// <param name="linha"></param>
        protected override void OnImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha, Dictionary<string, BinReferenciaInfo> dicionarioBinRef)//1407
        {
            // Insere no banco
            if (numeroLinha > 1 && linha != "")
            {
                //1407 inicio inibindo
                /*
                // Salva o arquivo item
                Procedures.PR_ARQUIVO_ITEM_S(
                cm,
                null,
                codigoArquivo,
                linha,
                null,
                false,
                ArquivoItemStatusEnum.PendenteValidacao,
                null,null);
                */
                //1407 fim inibindo

                //1407 inicio novo
                string codigoArquivoItem = null;
                string conteudoArquivoItem = linha;
                string tipoArquivoItem = "CV";
                bool retornarRegistro = false;
                ArquivoItemStatusEnum statusArquivoItem = ArquivoItemStatusEnum.PendenteValidacao;
                string chave = null;
                string referencia = null;
                Procedures.PR_ARQUIVO_ITEM_S
                (   cm
                ,   codigoArquivoItem
                ,   codigoArquivo
                ,   conteudoArquivoItem
                ,   tipoArquivoItem
                ,   retornarRegistro
                ,   statusArquivoItem
                ,   chave
                ,   referencia
                );
                //1407 inicio novo
            }
        }

        /// <summary>
        /// Método virtual para quantidade de colunas
        /// </summary>
        /// <param name="tipoLinha"></param>
        /// <returns></returns>
        protected override int OnReceberQuantidadeColunas(string tipoLinha)
        {
            return this.BookInfo.Campos.Count;
        }
    }
}
