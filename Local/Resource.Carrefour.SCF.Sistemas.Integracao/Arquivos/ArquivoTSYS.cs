﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Sistemas.Integracao.Regras;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using System.IO;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    /// <summary>
    /// Interpretador de arquivo TSYS
    /// </summary>
    public class ArquivoTSYS : ArquivoBase
    {
        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Layout atual
        /// </summary>
        public string Layout { get; set; }

        /// <summary>
        /// Informa se o layout já foi determinado por um registro
        /// </summary>
        private bool DeterminacaoLayout { get; set; }
        private bool DeterminacaoLayoutCV { get; set; }
        private bool DeterminacaoLayoutAJ { get; set; }

       
        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoTSYS()
        {
            // Escolhe qual dos dois layouts
            this.BookInfo = Layout2();

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();
        }

        public ArquivoTSYS(string layout)
        {
            switch (layout)
            {
                case "1":
                {
                    this.BookInfo = Layout1(); // "001.6c";
                    break;
                }
                case "2": 
                {
                    this.BookInfo = Layout2();// "001.6d";
                    break;
                }
                case "3":
                {
                    this.BookInfo = Layout3(); //001.6d // CV ESTA COM OS CAMPOS NOVOS DO ECOMMERCE, MAS AJ NAO
                    break;
                }
                case "4":
                {
                    this.BookInfo = Layout4(); //001.6d // AJ ESTA COM OS CAMPOS NOVOS DO ECOMMERCE, MAS cv NAO
                    break;
                }
                case "5":
                {
                    this.BookInfo = Layout5();//"001.6e"
                    break;
                }

            }
            

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();
        }


        private BookInfo Layout1()
        {
            this.Layout = "1";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            // Layout A0
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Início
            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);
            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Fim

            return BookInfo;
        }

        private BookInfo Layout2()
        {
            this.Layout = "2";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            // Layout A0
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Início
            campoCV.Campos.Add(new BookItemCampoInfo("DataPreAutorizacao", 8, new ConversorString()));
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Fim

            // Fernando Bove - carrega o NSU_Host original e o de Reversão caso exista - Ecommerce - Início
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoReversao", 12, new ConversorString()));
            // Fernando Bove - carrega o NSU_Host original e o de Reversão caso exista - Ecommerce - Fim

            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Início
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14));
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Fim
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Início
            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);
            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Fim

            return BookInfo;
        }

        private BookInfo Layout3()
        {
            this.Layout = "3";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            // Layout A0
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Início
            campoCV.Campos.Add(new BookItemCampoInfo("DataPreAutorizacao", 8, new ConversorString()));
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Fim

            // Fernando Bove - carrega o NSU_Host original e o de Reversão caso exista - Ecommerce - Início
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoReversao", 12, new ConversorString()));
            // Fernando Bove - carrega o NSU_Host original e o de Reversão caso exista - Ecommerce - Fim

            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));            
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Início
            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);
            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Fim

            return BookInfo;
        }

        private BookInfo Layout4()
        {
            this.Layout = "4";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            // Layout A0
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));            
            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Início
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14));
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Fim
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Início
            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);
            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Fim

            return BookInfo;
        }

        private BookInfo Layout5()
        {
            this.Layout = "5";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataPreAutorizacao", 8, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoReversao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Referencia", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Referencia", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14));
            campoAJ.Campos.Add(new BookItemCampoInfo("Referencia", 3, new ConversorString()));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Referencia", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("Referencia", 3, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);

            return BookInfo;
        }
        /// <summary>
        /// Retorna a lista de regras para validar o arquivo
        /// </summary>
        /// <returns></returns>
        protected override List<RegraSCFBase> OnReceberRegras()
        {
            return RegrasSCFHelper.RegrasTSYS(this);
        }

        /// <summary>
        /// Dado uma linha do arquivo, infere o tipo
        /// </summary>
        /// <param name="linha"></param>
        /// <returns></returns>
        protected override string OnInferirTipoLinha(object linha)
        {
            if (linha.ToString().Length > 2)//1410
                return ((string)linha).Substring(0, 2);
            else
                return "";
        }

        //private BookInfo Layout1 

        /// <summary>
        /// Implementação do método que retorna valor do campo
        /// </summary>
        protected override object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
            // Usa o interpretador de arquivos para extrair o valor
            return InterpretadorArquivos.ExtrairValor(this.BookInfo, tipoLinha + "/" + (string)campo, (string)linha, interpretar);
        }

        protected override long OnImportarArquivo(string arquivo, string codigoProcesso, string chaveArquivo, string codigoEmpresaArquivo)
        {   chaveArquivo = validarImportacaoTSYS(arquivo, codigoProcesso, null);   // Primeiro, verifica se é permitido importar o arquivo
            if (chaveArquivo != "")
                return base.OnImportarArquivo(arquivo, codigoProcesso, chaveArquivo, this.Layout, codigoEmpresaArquivo);
            else return 0;
        }

        //=========================================================================================================================================
        // 1407 - TRATANDO IMPORTACAO PARA NÃO CARREGAR O REFERENCIA BIN A CADA LINHA LIDA
        //=========================================================================================================================================
        //1407 protected override void OnImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha)
        protected override void OnImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha, Dictionary<string, BinReferenciaInfo> dicionarioBinRef)
        {
    
            if (linha != "")
            {
                string chave = null;
                string ReferenciaBin = null;
                string numeroCartao;
                string tipoLinha = this.InferirTipoLinha(linha);
                switch (tipoLinha)
                //=========================================================================================================================================
                // 1448 AQUI É FEITA A DEFINICAO DO LAYOUT É CHAMADO DIRETO O PR_ARQUIVO_S
                //=========================================================================================================================================
                {
                //=========================================================================================================================================
                // TIPO DE REGISTRO CV - COMPROVANTE DE VENDA
                //=========================================================================================================================================

                case "CV":
                        {   if (linha.Length < 272 && this.Layout != "1")
                            {
                                if (!this.DeterminacaoLayoutCV)
                                {
                                    if (!this.DeterminacaoLayout)
                                    {
                                        // NEM CV NEM AJ ESTAO COM OS CAMPOS NOVOS DO ECOMMERCE
                                        this.Layout = "1";
                                        this.BookInfo = Layout1();
                                        // Ajusta posicoes e tamanhos
                                        this.BookInfo.CalculaPosicaoTamanhoCampos();
                                    }
                                    else
                                    {
                                        // AJ ESTA COM OS CAMPOS NOVOS DO ECOMMERCE, MAS cv NAO
                                        this.Layout = "4";
                                        this.BookInfo = Layout4();
                                        // Ajusta posicoes e tamanhos
                                        this.BookInfo.CalculaPosicaoTamanhoCampos();
                                    }
                                    this.DeterminacaoLayout = true;
                                    this.DeterminacaoLayoutCV = true;
                                    OracleConnection cn = Procedures.ReceberConexao();
                                    ArquivoInfo arquivo = PersistenciaHelper.Receber<ArquivoInfo>("", codigoArquivo.ToString());
                                    Procedures.PR_ARQUIVO_S(cn,codigoArquivo.ToString(), arquivo.TipoArquivo, arquivo.NomeArquivo, arquivo.StatusArquivo,false, arquivo.CodigoProcesso, arquivo.ChaveArquivo, this.Layout, null);
                                }
                                else
                                {   if (!this.Layout.Equals("1") && !this.Layout.Equals("4") )                                         
                                        throw new Exception(string.Format(("IMPORTACAO - Problema no arquivo, há registros CV com layouts diferentes neste arquivo.")));
                                }
                            }
                            else if (linha.Length >= 272 && (this.Layout != "2" && this.Layout != "3"))
                            {
                                if (!this.DeterminacaoLayoutCV)
                                {
                                    // CV ESTA COM OS CAMPOS NOVOS DO ECOMMERCE, MAS AJ NAO
                                    this.Layout = "3";
                                    this.BookInfo = Layout3();
                                    this.BookInfo.CalculaPosicaoTamanhoCampos();
                                    this.DeterminacaoLayout = true;
                                    this.DeterminacaoLayoutCV = true;
                                    OracleConnection cn = Procedures.ReceberConexao();
                                    ArquivoInfo arquivo = PersistenciaHelper.Receber<ArquivoInfo>("", codigoArquivo.ToString());
                                    Procedures.PR_ARQUIVO_S(cn,codigoArquivo.ToString(), arquivo.TipoArquivo, arquivo.NomeArquivo, arquivo.StatusArquivo,false, arquivo.CodigoProcesso, arquivo.ChaveArquivo, this.Layout, null);
                                    this.DeterminacaoLayout = true;
                                    this.DeterminacaoLayoutCV = true;
                                }
                                else
                                {   if (!this.Layout.Equals("2") && !this.Layout.Equals("3"))
                                        throw new Exception(string.Format(("IMPORTACAO - Problema no arquivo, há registros CV com layouts diferentes neste arquivo.")));
                                }
                            }
                            else
                            {
                                this.DeterminacaoLayout = true;
                                this.DeterminacaoLayoutCV = true;
                            }
                            numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartao", true);
                            numeroCartao = numeroCartao.Substring(3, 6);                                   
                            if (dicionarioBinRef.ContainsKey(numeroCartao))
                            {
                                BinReferenciaInfo RefBin = dicionarioBinRef[numeroCartao];
                                ReferenciaBin = RefBin.CodigoReferencia;
                            }
                            chave =
                                ChaveTransacaoHelper.GerarChaveComprovanteVendaArquivo(
                                    (string)this.LerCampo(tipoLinha, linha, "DataTransacao", false),
                                    (string)this.LerCampo(tipoLinha, linha, "HorarioTransacao", false),
                                    (string)this.LerCampo(tipoLinha, linha, "IdentificacaoLoja", false),
                                    (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", false),
                                    (string)this.LerCampo(tipoLinha, linha, "CodigoAutorizacao", false),
                                    (string)this.LerCampo(tipoLinha, linha, "NumeroParcela", false),
                                    (string)this.LerCampo(tipoLinha, linha, "NSEQ", false));
                        }
                        break;
                //=========================================================================================================================================
                // TIPO DE REGISTRO AV - ANULACAO DA VENDA
                //=========================================================================================================================================
                case "AV":
                        numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartao", true);
                        numeroCartao = numeroCartao.Substring(3, 6);
                        if (dicionarioBinRef.ContainsKey(numeroCartao))
                        {
                            BinReferenciaInfo RefBin = dicionarioBinRef[numeroCartao];
                            ReferenciaBin = RefBin.CodigoReferencia;
                        }

                        chave =
                            ChaveTransacaoHelper.GerarChaveAnulacaoVendaArquivo(
                                (string)this.LerCampo(tipoLinha, linha, "DataTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "HorarioTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "IdentificacaoLoja", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "CodigoAutorizacaoTransacaoOriginal", false),
                                (string)this.LerCampo(tipoLinha, linha, "NumeroParcela", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSEQ", false));
                        break;
                //=========================================================================================================================================
                // TIPO DE REGISTRO CP - COMPROVANTE DE PAGAMENTO
                //=========================================================================================================================================
                case "CP":
                        numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartao", true);
                        numeroCartao = numeroCartao.Substring(3, 6);
                        if (dicionarioBinRef.ContainsKey(numeroCartao))
                        {
                            BinReferenciaInfo RefBin = dicionarioBinRef[numeroCartao];
                            ReferenciaBin = RefBin.CodigoReferencia;
                        }
                        chave =
                            ChaveTransacaoHelper.GerarChaveComprovantePagamentoArquivo(
                                (string)this.LerCampo(tipoLinha, linha, "DataTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "HorarioTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "IdentificacaoLoja", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "CodigoAutorizacao", false),
                                //1458 (string)this.LerCampo(tipoLinha, linha, "NSEQ", false));
                                (string)this.LerCampo(tipoLinha, linha, "SequencialMeioPagamento", false)); //1458
                        break;
                //=========================================================================================================================================
                // TIPO DE REGISTRO AP - ANULACAO DE PAGAMENTO
                //=========================================================================================================================================
                case "AP":
                        //Busca Referencia do Bin
                        numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartao", true);
                        numeroCartao = numeroCartao.Substring(3, 6);
                        if (dicionarioBinRef.ContainsKey(numeroCartao))
                        {
                            BinReferenciaInfo RefBin = dicionarioBinRef[numeroCartao];
                            ReferenciaBin = RefBin.CodigoReferencia;
                        }
                        chave =
                            ChaveTransacaoHelper.GerarChaveAnulacaoPagamentoArquivo(
                                (string)this.LerCampo(tipoLinha, linha, "DataTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "HorarioTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "IdentificacaoLoja", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSEQ", false));
                        break;
                //=========================================================================================================================================
                // TIPO DE REGISTRO RP - EQUIVALENTE AO AV - CANCELAMENTO PARCIAL
                //=========================================================================================================================================
                case "RP":
                        numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartao", true);
                        numeroCartao = numeroCartao.Substring(3, 6);
                        if (dicionarioBinRef.ContainsKey(numeroCartao))
                        {
                            BinReferenciaInfo RefBin = dicionarioBinRef[numeroCartao];
                            ReferenciaBin = RefBin.CodigoReferencia;
                        }
                        chave =
                            ChaveTransacaoHelper.GerarChaveAnulacaoVendaArquivo(
                                (string)this.LerCampo(tipoLinha, linha, "DataTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "HorarioTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "IdentificacaoLoja", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", false),
                                (string)this.LerCampo(tipoLinha, linha, "CodigoAutorizacaoTransacaoOriginal", false),
                                (string)this.LerCampo(tipoLinha, linha, "NumeroParcela", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSEQ", false));
                        break;
                //=========================================================================================================================================
                // TIPO DE REGISTRO AJ - AJUSTE DE VENDA OU PAGAMENTO
                //=========================================================================================================================================
                case "AJ":
                        {   if (linha.Length < 217 && this.Layout != "1")
                            {
                                if (!this.DeterminacaoLayoutAJ)
                                {
                                    if (!this.DeterminacaoLayout)
                                    {
                                        // NEM CV NEM AJ ESTAO COM OS CAMPOS NOVOS DO ECOMMERCE
                                        this.Layout = "1";
                                        this.BookInfo = Layout1();
                                        // Ajusta posicoes e tamanhos
                                        this.BookInfo.CalculaPosicaoTamanhoCampos();
                                    }
                                    else
                                    {
                                        // CV ESTA COM OS CAMPOS NOVOS DO ECOMMERCE, MAS AJ NAO
                                        this.Layout = "3";
                                        this.BookInfo = Layout3();
                                        // Ajusta posicoes e tamanhos
                                        this.BookInfo.CalculaPosicaoTamanhoCampos();
                                    }
                                    this.DeterminacaoLayout = true;
                                    this.DeterminacaoLayoutAJ = true;

                                    OracleConnection cn = Procedures.ReceberConexao();
                                    ArquivoInfo arquivo = PersistenciaHelper.Receber<ArquivoInfo>("", codigoArquivo.ToString());
                                    Procedures.PR_ARQUIVO_S(cn,codigoArquivo.ToString(), arquivo.TipoArquivo, arquivo.NomeArquivo, arquivo.StatusArquivo,false, arquivo.CodigoProcesso, arquivo.ChaveArquivo, this.Layout, null);
                                }
                                else
                                {   if (!this.Layout.Equals("1") && !this.Layout.Equals("3"))
                                        throw new Exception(string.Format(("IMPORTACAO - Problema no arquivo, há registros AJ com layouts diferentes neste arquivo.")));
                                }
                            }
                            else if (linha.Length >= 217 && (this.Layout != "2" && this.Layout != "4"))
                            {
                                if (!this.DeterminacaoLayoutAJ)
                                {   this.Layout = "4";
                                    this.BookInfo = Layout4();
                                    this.BookInfo.CalculaPosicaoTamanhoCampos();

                                    this.DeterminacaoLayout = true;
                                    this.DeterminacaoLayoutAJ = true;

                                    OracleConnection cn = Procedures.ReceberConexao();
                                    ArquivoInfo arquivo = PersistenciaHelper.Receber<ArquivoInfo>("", codigoArquivo.ToString());
                                    Procedures.PR_ARQUIVO_S(cn,codigoArquivo.ToString(), arquivo.TipoArquivo, arquivo.NomeArquivo, arquivo.StatusArquivo,false, arquivo.CodigoProcesso, arquivo.ChaveArquivo, this.Layout, null);

                                    this.DeterminacaoLayout = true;
                                    this.DeterminacaoLayoutAJ = true;
                                }
                                else
                                {   if (!this.Layout.Equals("2") && !this.Layout.Equals("4"))
                                        throw new Exception(string.Format(("IMPORTACAO - Problema no arquivo, há registros AJ com layouts diferentes neste arquivo.")));
                                }
                            }
                            else
                            {
                                this.DeterminacaoLayout = true;
                                this.DeterminacaoLayoutAJ = true;
                            }
                            numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartaoTransacaoOriginal", true);
                            numeroCartao = numeroCartao.Substring(3, 6);
                            if (dicionarioBinRef.ContainsKey(numeroCartao))
                            {
                                BinReferenciaInfo RefBin = dicionarioBinRef[numeroCartao];
                                ReferenciaBin = RefBin.CodigoReferencia;
                            }
                            chave =
                                ChaveTransacaoHelper.GerarChaveAjusteArquivo(
                                    (string)this.LerCampo(tipoLinha, linha, "DataTransacao", false),
                                    (string)this.LerCampo(tipoLinha, linha, "HorarioTransacao", false),
                                    (string)this.LerCampo(tipoLinha, linha, "IdentificacaoLoja", false),
                                    (string)this.LerCampo(tipoLinha, linha, "ValorBruto", false),
                                    (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", false));
                            break;
                        }

                }

                //=========================================================================================================================================
                // GRAVA TB_ARQUIVO_ITEM
                //=========================================================================================================================================
                //1407 inicio novo
                string codigoArquivoItem = null;
                string conteudoArquivoItem = linha;
                string tipoArquivoItem = this.InferirTipoLinha(linha);
                bool retornarRegistro = false;
                ArquivoItemStatusEnum statusArquivoItem = ArquivoItemStatusEnum.PendenteValidacao;

                string referencia = ReferenciaBin;
                Procedures.PR_ARQUIVO_ITEM_S
                (   cm
                ,   codigoArquivoItem
                ,   codigoArquivo
                ,   conteudoArquivoItem
                ,   tipoArquivoItem
                ,   retornarRegistro
                ,   statusArquivoItem
                ,   chave
                ,   referencia
                );
                //1407 inicio novo
            }
        }


        protected override string OnEscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {   return InterpretadorArquivos.EscreverValor(this.BookInfo, tipoLinha + "/" + nomeCampo, (string)linha, valor, interpretarValor);
        }

        protected override int OnReceberQuantidadeColunas(string tipoLinha)
        {   return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Count;
        }

         protected override string[] OnReceberNomeCampos(string tipoLinha)
        {   return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Select(c => c.Nome).ToArray();// Retorna lista com os nomes dos campos do tipo de linha informado
        }

        protected override string OnRemoverValor(string tipoLinha, object linha, object campo)
        {   return InterpretadorArquivos.RemoverValor(this.BookInfo, tipoLinha + "/" + (string)campo, (string)linha); // Usa o interpretador de arquivos para extrair o valor
        }

        protected override string OnInserirValor(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {   return InterpretadorArquivos.InserirValor(this.BookInfo, tipoLinha + "/" + nomeCampo, (string)linha, valor, interpretarValor);// Usa o interpretador de arquivos para extrair o valor
        }

        //ECOMMERCE FASE 2 INICIO
        public string VerficarFinanceiroContabil(string tipoRegistro, object linha)
        {
            string FinanceiroContabil = "";

            if (tipoRegistro.Equals("RP"))
                FinanceiroContabil = "F";
            else
            {
                string tipoLancamento = this.LerCampo(tipoRegistro, linha, "TipoLancamento", false).ToString().Trim();

                if (tipoLancamento == "3")
                {
                    switch (tipoRegistro)
                    {
                        case "AV":
                            string codigoAnulacao = this.LerCampo(tipoRegistro, linha, "CodigoAnulacao", false).ToString().Trim();
                            
                            //SCF1111 - Marcos Matsuoka - Inclusão do dicionario para fazer o tratamento para CRAF e CRES - inicio
                            Dictionary<string, string> dicionarioProdutos = null;
                            dicionarioProdutos = new Dictionary<string, string>();
                            // pede a lista de referencias
                            List<ListaItemReferenciaInfo> listaItemReferencias = PersistenciaHelper.Listar<ListaItemReferenciaInfo>(null);

                            // preenche o dicionario a ser utilizado
                            foreach (ListaItemReferenciaInfo listaReferenciaInfo in listaItemReferencias)
                                if (listaReferenciaInfo.CodigoListaItemSituacao == "CodigoAnulacao"
                                    && listaReferenciaInfo.NomeReferencia == "Produto"
                                    && dicionarioProdutos.ContainsKey(listaReferenciaInfo.listaItemInfo.Valor) == false)
                                    dicionarioProdutos.Add(listaReferenciaInfo.listaItemInfo.Valor, listaReferenciaInfo.ValorReferencia);
                            //SCF1111 - Marcos Matsuoka - Inclusão do dicionario para fazer o tratamento para CRAF e CRES - fim

                            //if (codigoAnulacao.Equals("0202") || codigoAnulacao.Equals("0204") || codigoAnulacao.Equals("0304"))
                            if (dicionarioProdutos[codigoAnulacao] == "CRAF" || dicionarioProdutos[codigoAnulacao] == "CRES")
                            {
                                FinanceiroContabil = "C";
                            }
                            //1423                    // else //SCF1111 - Marcos Matsuoka - inclusão para tratamento do FinanceiroContabil = "";
                            //{
                            //     FinanceiroContabil = null;
                            //}
                            break;

                        case "CV":
                            string produtoCSF = this.LerCampo(tipoRegistro, linha, "ProdutoCSF", false).ToString().Trim();
                            if (produtoCSF.Equals("CRAF") || produtoCSF.Equals("CRES"))
                            {
                                FinanceiroContabil = "C";
                            }
                            //1423                      //else //SCF1111 - Marcos Matsuoka - inclusão para tratamento do FinanceiroContabil = "";
                            //{
                            //    FinanceiroContabil = null;
                            //}
                            break;
                    }
                }
            }

            return FinanceiroContabil;
        }
        //ECOMMERCE FASE 2 FIM

        //============================================================================================
        // 1407 - INICIO
        // CONFERINDO SE TEM O REGISTRO A9 ANTES DE IMPORTAR O ARQUIVO
        //============================================================================================
        private string validarImportacaoTSYS(string arquivo, string codigoProcesso, string codigoSessao)
        {   int qtdeLinhasLidas = 0;
            string chaveArquivo = "";
            //1407 bool temErro = false;
            //1407 bool sair = false;
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();//1444       
            OracleConnection cn = Procedures.ReceberConexao();
            ProcessoInfo processoAtual = PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, codigoProcesso);
            try
            {   FileInfo fileInfo = new FileInfo(arquivo);
                using (StreamReader reader = File.OpenText(arquivo))
                {   
                    string linha;
                    string dataMovimento = null;//1429
                    string idMovimento = null;
                    string codigoArquivo = ""; //1444
                    string VersaoLayout =""; //30/10/2018

                    // bool linhaA0 = false; //1429 // warning 25/02/2019
                    bool linhaL0 = false; //1429
                    bool linhaCV = false; //1429
                    bool linhaAV = false; //1429
                    bool linhaCP = false; //1429
                    bool linhaAP = false; //1429
                    bool linhaAJ = false; //1429
                    // bool linhaL9 = false; //1429 // warning 25/02/2019
                    bool linhaA9 = false; //1407

                    ArquivoTSYS arquivoTSYS = this;

                    //===============================================================================================================
                    // LENDO ARQUIVO TXT
                    //===============================================================================================================
                    while ((linha = reader.ReadLine()) != null)
                    {   
                        qtdeLinhasLidas++;
                        string tipoTransacao = arquivoTSYS.InferirTipoLinha(linha);

                        if (tipoTransacao == "A0")
                        {
                            // linhaA0 = true; //1429    // warning 25/02/2019
                            dataMovimento = (string)arquivoTSYS.LerCampo("A0", linha, "DataGeracaoArquivo"); //1429
                            idMovimento = (string)arquivoTSYS.LerCampo("A0", linha, "IdMovimento");
                            if (idMovimento == null || idMovimento == "")
                                throw new Exception(string.Format("IMPORTACAO VALIDACAO - Problemas com o campo ID do Movimento deste arquivo {0}", idMovimento));
                            else
                                chaveArquivo = dataMovimento + idMovimento;
                            VersaoLayout = (string)arquivoTSYS.LerCampo("A0", linha, "VersaoLayout"); //30/10/2018
                        }

                        if (tipoTransacao == "L0")
                        {
                            linhaL0 = true; //1429
                            dataMovimento = (string)arquivoTSYS.LerCampo("L0", linha, "DataMovimento");
                            chaveArquivo = dataMovimento + idMovimento;
                        }

                        if (tipoTransacao == "CV") //1429
                            linhaCV = true;

                        if (tipoTransacao == "AV") //1429
                            linhaAV = true;

                        if (tipoTransacao == "CP") //1429
                            linhaCP = true;

                        if (tipoTransacao == "AP") //1429
                            linhaAP = true;

                        if (tipoTransacao == "AJ") //1429
                            linhaAJ = true;

                        //if (tipoTransacao == "L9") //1429
                            // linhaL9 = true; //warning 25/02/2019

                        if (tipoTransacao == "A9") //1407
                            linhaA9 = true;

                    }
                    reader.Close();

                    //===============================================================================================================
                    // 1429 - TRATANDO FALTA DE REGISTROS DO ARQUIVO
                    //===============================================================================================================
                    string vMSGARQ = "IMPORTACAO VALIDACAO - ";
                    bool Vazio = false;

                    if ( (linhaAJ == false) && (linhaCV == false) && (linhaCP == false) && (linhaAV == false) && (linhaAP == false) ) // SEM MOVIMENTACAO
                    {
                        Vazio = true;
                        vMSGARQ = vMSGARQ + "[ Arquivo VAZIO, sem transacoes (CV, AV, CP, AP e AJ) ] ";
                    }

                    if (linhaL0 == false) // SEM L0
                    {
                        if (Vazio)
                        {
                            vMSGARQ = vMSGARQ + "[ Arquivo sem (L0) ] ";
                        }
                        else
                        {
                            string semL0 = "IMPORTACAO VALIDACAO - Arquivo incompleto, linha L0 não recebida.";
                            throw new Exception(semL0);
                        }
                    }

                    if (linhaA9 == false)
                    {
                        string semA9 = "IMPORTACAO VALIDACAO - Arquivo incompleto, linha A9 não recebida.";
                        throw new Exception(semA9);
                    }

                    //===============================================================================================================
                    // 1429 - TRATANDO EXISTENCIA DO ARQUIVO
                    //===============================================================================================================
                    List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                    condicoes.Add(new CondicaoInfo("ChaveArquivo", CondicaoTipoEnum.Igual, chaveArquivo));
                    List<ArquivoInfo> arquivos = PersistenciaHelper.Listar<ArquivoInfo>(codigoSessao, condicoes);

                    if (arquivos.Count > 0)
                    {
                        foreach (ArquivoInfo item in arquivos)
                        {
                            if (item.StatusArquivo != ArquivoStatusEnum.Cancelado && item.StatusArquivo != ArquivoStatusEnum.Estornado)
                            {
                                ProcessoInfo processo = PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, item.CodigoProcesso);
                                if ((processo != null && processoAtual != null) && processo.TipoProcesso == processoAtual.TipoProcesso)
                                {
                                    if (processoAtual.CodigoProcesso == processo.CodigoProcesso) //SCF1131 - Marcos Matsuoka 
                                    {
                                        codigoArquivo = item.CodigoArquivo; //1444
                                        Procedures.PR_ARQUIVO_ITEM_R(codigoArquivo);
                                        lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                                        lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("IMPORTACAO TRATAMENTO - Arquivo [{0}], anterior deste processo, foi EXCLUIDO", codigoArquivo));
                                        LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                                    }
                                    else
                                    {
                                        throw new Exception(string.Format("IMPORTACAO VALIDACAO - Este arquivo [{0}] de Identificação [{1}] já foi importado no Processo: [{2}] ", fileInfo.Name, chaveArquivo, item.CodigoProcesso)); // SCF1289                                                 
                                    }
                                }
                            }
                        }
                    }
                    //===============================================================================================================
                    //vMSGARQ = vMSGARQ + "[ Chave do arquivo (data + id ) [" + chaveArquivo + "] Contem [" + qtdeLinhasLidas.ToString() + " linhas ] ";
                    vMSGARQ = "IMPORTACAO - Versão Layout [" + VersaoLayout + "] Data Processamento  [" + dataMovimento + "]  ID Movimento [" + idMovimento + "] Contem [" + qtdeLinhasLidas.ToString() + " linhas ] ";
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(vMSGARQ);
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }
            string retorno;
            //1407
            //if (temErro)
            //    retorno = "";
            //else
            retorno = chaveArquivo;
            return retorno;
        }
    }
}