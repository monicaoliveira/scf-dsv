﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    /// <summary>
    /// Interpretador de arquivo de produtos/planos
    /// </summary>
    public class ArquivoProdutoPlano : ArquivoBase
    {
        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoProdutoPlano()
        {
            // Monta o book do arquivo
            this.BookInfo = new BookInfo();
            this.BookInfo.TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores;

            // Adiciona os campos do Detalhe
            BookItemCampoInfo detalhe = new BookItemCampoInfo("detalhe");

            detalhe.Campos.Add(new BookItemCampoInfo("COD_PRODUTO_TSYS", 6, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("PRODUTO_CEDIVEL", 1, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("COD_PLANO_TSYS", 6, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("NOME_PLANO", 40, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("DESCRICAO_PLANO", 40, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("PERCENTUAL_COMISSAO", 5, new ConversorNumero(typeof(double), 2, true, false)));
            detalhe.Campos.Add(new BookItemCampoInfo("NUMERO_PARCELAS", 5, new ConversorNumero(typeof(int), 0, true)));

            this.BookInfo.Campos.Add(detalhe);

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();
        }

        /// <summary>
        /// Implementação do método que retorna valor do campo
        /// </summary>
        /// <param name="tipoLinha"></param>
        /// <param name="linha"></param>
        /// <param name="campo"></param>
        /// <param name="interpretar"></param>
        /// <returns></returns>
        protected override object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
            return InterpretadorArquivos.ExtrairValor(this.BookInfo, tipoLinha + "/" + (string)campo, (string)linha, interpretar);
        }
    }
}
