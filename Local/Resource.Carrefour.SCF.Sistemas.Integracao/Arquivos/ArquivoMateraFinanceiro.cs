﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    /// <summary>
    /// Interpretador de arquivo Matera - Financeiro
    /// </summary>
    public class ArquivoMateraFinanceiro : ArquivoBase
    {
        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Nome do arquivo no filesystem
        /// </summary>
        public string NomeArquivo { get; set; }

        /// <summary>
        /// Cache de campos do Registro 01
        /// </summary>
        private Dictionary<string, BookItemCampoHelper> _cacheRegistro01 { get; set; }

        /// <summary>
        /// Cache de campos do Registro 04
        /// </summary>
        private Dictionary<string, BookItemCampoHelper> _cacheRegistro04 { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoMateraFinanceiro()
        {
            // Monta o book do arquivo
            this.BookInfo = new BookInfo();
            this.BookInfo.TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores;

            // Registro 01
            BookItemCampoInfo registro01 = new BookItemCampoInfo("Registro01");
            registro01.Campos.Add(new BookItemCampoInfo("TipoRegistro", 2, new ConversorString(), "01"));
            registro01.Campos.Add(new BookItemCampoInfo("IDEmpresa", 7, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("ApelidoEmpresa", 20, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("CnpjEmpresa", 15, new ConversorString(), ""));                         // buscar de dominio
            registro01.Campos.Add(new BookItemCampoInfo("IDFornecedor", 7, new ConversorString(), " "));  
            registro01.Campos.Add(new BookItemCampoInfo("ApelidoFornecedor", 20, new ConversorString()));                       // de acordo com o favorecido
            registro01.Campos.Add(new BookItemCampoInfo("CNPJCPFFornecedor", 15, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("CodigoFormaPagto", 3, new ConversorString()));                         // tem regra para colocar. Se for 237, é CC, se nao é TED
            registro01.Campos.Add(new BookItemCampoInfo("IDContaCorrenteDestino", 8, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("Banco", 5, new ConversorString()));                                    // de acordo com o favorecido
            registro01.Campos.Add(new BookItemCampoInfo("Agencia", 8, new ConversorString()));                                  // de acordo com o favorecido
            registro01.Campos.Add(new BookItemCampoInfo("ContaCorrente", 15, new ConversorString()));                           // de acordo com o favorecido
            registro01.Campos.Add(new BookItemCampoInfo("Cobranca", 15, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("TipoDocumento", 5, new ConversorString()));                            // buscar de configuracao
            registro01.Campos.Add(new BookItemCampoInfo("NumeroDocumento", 20, new ConversorString()));                         // nro do pagamento
            registro01.Campos.Add(new BookItemCampoInfo("TipoProdutoServico", 5, new ConversorString()));                              // buscar configuracao
            registro01.Campos.Add(new BookItemCampoInfo("DataEmissao", 8, new ConversorData("DDMMYYYY")));                      // data a geracao do arquivo
            registro01.Campos.Add(new BookItemCampoInfo("DataVencimento", 8, new ConversorData("DDMMYYYY")));                   // data do pagamento
            registro01.Campos.Add(new BookItemCampoInfo("DataProvisao", 8, new ConversorData("DDMMYYYY")));                     // data da geracao do arquivo
            registro01.Campos.Add(new BookItemCampoInfo("ValorPagamento", 15, new ConversorNumero(typeof(double), 2, true)));   // valor do pagamento
            registro01.Campos.Add(new BookItemCampoInfo("ValorTributavel", 15, new ConversorString(), "0"));
            registro01.Campos.Add(new BookItemCampoInfo("ValorTributavelINSSRF", 15, new ConversorString(), "0"));
            registro01.Campos.Add(new BookItemCampoInfo("CodigoSite", 10, new ConversorString(), "SP"));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorCalculoImposto", 1, new ConversorString(), "N"));
            registro01.Campos.Add(new BookItemCampoInfo("IDFilial", 7, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("ApelidoFilial", 20, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("CNPJFilial", 15, new ConversorString()));                              // buscar em dominio
            registro01.Campos.Add(new BookItemCampoInfo("IDCentroCusto", 6, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("CodigoCentroCusto", 20, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("CodigoContaContabil", 15, new ConversorString()));                     // buscar configuracao
            registro01.Campos.Add(new BookItemCampoInfo("CodigoProjeto", 8, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("TituloAdiantamento", 8, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("IDContaCorrenteEDI", 8, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("BancoEDI", 5, new ConversorString()));                                 // buscar em dominio
            registro01.Campos.Add(new BookItemCampoInfo("AgenciaEDI", 8, new ConversorString()));                               // buscar em dominio
            registro01.Campos.Add(new BookItemCampoInfo("ContaCorrenteEDI", 15, new ConversorString()));                        // buscar em dominio
            registro01.Campos.Add(new BookItemCampoInfo("CodigoBarra", 80, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("IDMoedaOriginal", 3, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("MoedaOriginal", 30, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("Referencia", 30, new ConversorString()));                              // de acordo com o pagamento - referencia principal
            registro01.Campos.Add(new BookItemCampoInfo("AgenciaOperacao", 8, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("Descricao", 2000, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("Observacao", 50, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorAgrupaLancamento", 1, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("CodigoDestinatarioDOC", 3, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("ValorTributavelISS", 15, new ConversorString(), "0"));
            registro01.Campos.Add(new BookItemCampoInfo("IDFilialProp", 7, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("ApelidoFilialProp", 20, new ConversorString(), " "));                  // buscar em dominio
            registro01.Campos.Add(new BookItemCampoInfo("CNPJFilialProp", 15, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("ValorTributavelPISRF", 15, new ConversorString(), "0"));
            registro01.Campos.Add(new BookItemCampoInfo("ValorTributavelCOFINSRF", 15, new ConversorString(), "0"));
            registro01.Campos.Add(new BookItemCampoInfo("ValorTributavelCSLLRF", 15, new ConversorString(), "0"));
            registro01.Campos.Add(new BookItemCampoInfo("IDTerceiro", 7, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("ApelidoTerceiro", 20, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("CNPJCPFTerceiro", 15, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("ValorTributavelINSS", 15, new ConversorString(), "0"));
            registro01.Campos.Add(new BookItemCampoInfo("UsuarioCadastro", 30, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorDispensaPISRF", 1, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorDispensaCOFINSRF", 1, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorDispensaCSLLSRF", 1, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("SerieNF", 3, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorEDIConcessionaria", 1, new ConversorString(), "S"));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorDispensaIRRF", 1, new ConversorString(), "N"));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorRealTime", 1, new ConversorString()));                        // se for 237 = S, se nao é N
            registro01.Campos.Add(new BookItemCampoInfo("Pais", 30, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("UF", 2, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("DEscricaoUM", 60, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("ValorTributavelISSJADIC", 15, new ConversorString(), "0"));
            registro01.Campos.Add(new BookItemCampoInfo("IndicadorDespensaAntecipada", 1, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("QtdeMesesApropriacao", 3, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("AgenciaCadastro", 8, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("NumeroContrato", 20, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("AssociaAdiantamento", 1, new ConversorString(), " "));
            registro01.Campos.Add(new BookItemCampoInfo("CodSisOrigem", 3, new ConversorString(), "IMB"));
            this.BookInfo.Campos.Add(registro01);

            // Registro 04
            BookItemCampoInfo registro04 = new BookItemCampoInfo("Registro04");
            registro04.Campos.Add(new BookItemCampoInfo("TipoRegistro", 2, new ConversorString(), "04"));
            registro04.Campos.Add(new BookItemCampoInfo("Percentual", 6, new ConversorString(), " "));
            registro04.Campos.Add(new BookItemCampoInfo("IDFilial", 7, new ConversorString(), " "));
            registro04.Campos.Add(new BookItemCampoInfo("ApelidoFilial", 20, new ConversorString()));                           // Buscar em dominio
            registro04.Campos.Add(new BookItemCampoInfo("CNPJFilial", 15, new ConversorString(), " "));
            registro04.Campos.Add(new BookItemCampoInfo("IDCentroCusto", 6, new ConversorString(), " "));
            registro04.Campos.Add(new BookItemCampoInfo("CodigoCentroCusto", 20, new ConversorString(), ""));
            registro04.Campos.Add(new BookItemCampoInfo("CodigoContaContabil", 15, new ConversorString(), "49985002003"));
            registro04.Campos.Add(new BookItemCampoInfo("CodigoProjeto", 8, new ConversorString(), " "));
            registro04.Campos.Add(new BookItemCampoInfo("Referencia", 30, new ConversorString()));                              // codigo da referencia secundaria
            registro04.Campos.Add(new BookItemCampoInfo("ValorPagamento", 15, new ConversorNumero(typeof(double), 2, true)));   // valor da referencia secundaria
            registro04.Campos.Add(new BookItemCampoInfo("ContaProvisao", 15, new ConversorString()));                           // buscar em configuracao
            registro04.Campos.Add(new BookItemCampoInfo("HistoricoManual", 200, new ConversorString(), " "));                   
            registro04.Campos.Add(new BookItemCampoInfo("TipoProdutoServico", 5, new ConversorString()));                       // o mesmo do registro 01
            this.BookInfo.Campos.Add(registro04);

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();

            // Prepara cache do Registro 01
            _cacheRegistro01 = new Dictionary<string, BookItemCampoHelper>();
            foreach (BookItemCampoInfo campo in registro01.Campos)
                _cacheRegistro01.Add(campo.Nome, this.BookInfo.ReceberDetalheCampo(campo.ReceberCaminho()));

            // Prepara cache do Registro 04
            _cacheRegistro04 = new Dictionary<string, BookItemCampoHelper>();
            foreach (BookItemCampoInfo campo in registro04.Campos)
                _cacheRegistro04.Add(campo.Nome, this.BookInfo.ReceberDetalheCampo(campo.ReceberCaminho()));

        }

        /// <summary>
        /// Método virtual para criação de linha
        /// </summary>
        protected override string OnCriarLinha(string tipoLinha, Dictionary<string, object> valores)
        {
            // Monta parametros com campos recuperados do cache
            Dictionary<BookItemCampoHelper, object> valores2 = new Dictionary<BookItemCampoHelper, object>();
            foreach (KeyValuePair<string, object> item in valores)
                switch (tipoLinha)
                {
                    case "Registro01":
                        valores2.Add(_cacheRegistro01[item.Key], item.Value);
                        break;
                    case "Registro04":
                        valores2.Add(_cacheRegistro04[item.Key], item.Value);
                        break;
                }

            // Cria a linha e retorna
            return InterpretadorArquivos.EscreverLinha(this.BookInfo, tipoLinha, valores2);
        }

        public string GerarNomeArquivo()
        {
            // Pega a data e hora atual
            DateTime hoje = DateTime.Now;

            // Separa os dados
            string dia = hoje.Day.ToString().PadLeft(2, '0');
            string mes = hoje.Month.ToString().PadLeft(2, '0');
            string ano = hoje.Year.ToString();

            string hora = hoje.Hour.ToString().PadLeft(2, '0');
            string minuto = hoje.Minute.ToString().PadLeft(2, '0');

            // Adiciona os dados ao nome do arquivo
            return "MATERA_" + ano + mes + dia + "_" + hora + minuto + ".txt";
        }

        /// <summary>
        /// Método virtual para quantidade de colunas
        /// </summary>
        /// <param name="tipoLinha"></param>
        /// <returns></returns>
        protected override int OnReceberQuantidadeColunas(string tipoLinha)
        {
            return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Count;
        }
    }
}
