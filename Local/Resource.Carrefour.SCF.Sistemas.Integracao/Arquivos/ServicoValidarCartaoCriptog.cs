﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    public class ServicoValidarCartaoCriptog : ArquivoBase
    {
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Layout atual
        /// </summary>
        public string Layout { get; set; }

        /// <summary>
        /// Informa se o layout já foi determinado por um registro
        /// </summary>
        private bool DeterminacaoLayout { get; set; }
        private bool DeterminacaoLayoutCV { get; set; }
        private bool DeterminacaoLayoutAJ { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoValidarCartaoCriptog()
        {
            // Escolhe qual dos dois layouts
            this.BookInfo = Layout2();

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();
        }

        public ServicoValidarCartaoCriptog(string layout)
        {
            switch (layout)
            {
                case "1":
                {
                  this.BookInfo = Layout1();
                    break;
                }
                case "2":
                {
                    this.BookInfo = Layout2();
                    break;
                }
                case "3":
                {
                    this.BookInfo = Layout3();
                    break;
                }
                case "4":
                {
                    this.BookInfo = Layout4();
                    break;
                }
                    
            }
            

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();
        }


        private BookInfo Layout1()
        {
            this.Layout = "1";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            // Layout A0
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Início
            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);
            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Fim

            return BookInfo;
        }

        private BookInfo Layout2()
        {
            this.Layout = "2";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            // Layout A0
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Início
            campoCV.Campos.Add(new BookItemCampoInfo("DataPreAutorizacao", 8, new ConversorString()));
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Fim

            // Fernando Bove - carrega o NSU_Host original e o de Reversão caso exista - Ecommerce - Início
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoReversao", 12, new ConversorString()));
            // Fernando Bove - carrega o NSU_Host original e o de Reversão caso exista - Ecommerce - Fim

            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Início
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14));
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Fim
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Início
            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);
            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Fim

            return BookInfo;
        }

        private BookInfo Layout3()
        {
            this.Layout = "3";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            // Layout A0
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Início
            campoCV.Campos.Add(new BookItemCampoInfo("DataPreAutorizacao", 8, new ConversorString()));
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Fim

            // Fernando Bove - carrega o NSU_Host original e o de Reversão caso exista - Ecommerce - Início
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoReversao", 12, new ConversorString()));
            // Fernando Bove - carrega o NSU_Host original e o de Reversão caso exista - Ecommerce - Fim

            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));            
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Início
            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);
            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Fim

            return BookInfo;
        }

        private BookInfo Layout4()
        {
            this.Layout = "4";

            // Monta o book do arquivo
            BookInfo =
                new BookInfo()
                {
                    TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
                };

            // Layout A0
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8, new ConversorData("YYYYMMDD")));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6, new ConversorHora("HHMMSS")));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6, new ConversorNumero(0, true)));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoA0);

            // Layout L0
            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, new ConversorData("YYYYMMDD")));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero()));
            BookInfo.Campos.Add(campoL0);

            // Layout L9
            BookItemCampoInfo campoL9 = new BookItemCampoInfo("L9");
            campoL9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalRegistrosTransacao", 9, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("TotalValoresCredito", 17, new ConversorNumero(0, true)));
            campoL9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoL9);

            // Layout A9
            BookItemCampoInfo campoA9 = new BookItemCampoInfo("A9");
            campoA9.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA9.Campos.Add(new BookItemCampoInfo("TotalRegistros", 9, new ConversorNumero(0, true)));
            campoA9.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(0, true)));
            BookInfo.Campos.Add(campoA9);

            // Layout CV
            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorNumero(typeof(int))));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, new ConversorString()));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, new ConversorString()));            
            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCV);

            // Layout CP
            BookItemCampoInfo campoCP = new BookItemCampoInfo("CP");
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoCP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, new ConversorData("YYYYMMDD")));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorTotalPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroTotalMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("MeioPagamento", 1, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("SequencialMeioPagamento", 3, new ConversorNumero(typeof(int))));
            campoCP.Campos.Add(new BookItemCampoInfo("ValorMeioPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoCP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoCP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoCP);

            // Layout AJ
            BookItemCampoInfo campoAJ = new BookItemCampoInfo("AJ");
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoAJ.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("DataRepasse", 8, new ConversorData("YYYYMMDD")));
            campoAJ.Campos.Add(new BookItemCampoInfo("TipoAjuste", 1));
            campoAJ.Campos.Add(new BookItemCampoInfo("CodigoAjuste", 4));
            campoAJ.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAjuste", 30));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorBruto", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("ValorLiquido", 16, new ConversorNumero(typeof(double), 2)));
            campoAJ.Campos.Add(new BookItemCampoInfo("Banco", 3));
            campoAJ.Campos.Add(new BookItemCampoInfo("Agencia", 6));
            campoAJ.Campos.Add(new BookItemCampoInfo("Conta", 11));
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroCartaoTransacaoOriginal", 19));
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Início
            campoAJ.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14));
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Fim
            campoAJ.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAJ);

            // Layout AV
            BookItemCampoInfo campoAV = new BookItemCampoInfo("AV");
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoAV.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoAV.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAV);

            // Layout AP
            BookItemCampoInfo campoAP = new BookItemCampoInfo("AP");
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoAP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DataReembolsoEstabelecimento", 8, new ConversorData("YYYYMMDD")));
            campoAP.Campos.Add(new BookItemCampoInfo("FormaMeioPagamento", 1, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorBrutoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("ValorLiquidoPagamento", 16, new ConversorNumero(typeof(double), 2)));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoAP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoAP);

            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Início
            // Layout RP
            BookItemCampoInfo campoRP = new BookItemCampoInfo("RP");
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUTefTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacaoOriginal", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAutorizacaoTransacaoOriginal", 12, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, new ConversorHora("HHMMSS")));
            campoRP.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DataReembolsoEmissor", 8, new ConversorData("YYYYMMDD")));
            campoRP.Campos.Add(new BookItemCampoInfo("CodigoAnulacao", 4, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("DescricaoMotivoAnulacao", 30, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoOuComissao", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, new ConversorNumero(typeof(int))));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, new ConversorNumero(typeof(double), 2)));
            campoRP.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, new ConversorString()));
            campoRP.Campos.Add(new BookItemCampoInfo("NSEQ", 9, new ConversorNumero(typeof(int))));
            BookInfo.Campos.Add(campoRP);
            // Fernando Bove - Tipo de Registro RP que será convertido em AV - Ecommerce - Fim

            return BookInfo;
        }

        /// <summary>
        /// Implementa o método virtual para ler um campo
        /// </summary>
        protected override object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
            // Usa o interpretador de arquivos para extrair o valor
            return InterpretadorArquivos.ExtrairValor(this.BookInfo, tipoLinha + "/" + (string)campo, (string)linha, interpretar);
        }

        /// <summary>
        /// Altera o valor de um campo
        /// </summary>
        protected override string OnEscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {
            return InterpretadorArquivos.EscreverValor(this.BookInfo, tipoLinha + "/" + nomeCampo, (string)linha, valor, interpretarValor);
        }

        public override int ValidarArquivo(string codigoProcesso, string codigoArquivo, Dictionary<string, object> contexto, string codigoSessao)
        {
            bool firstLine = true;
            //string nsuHost = "";
            //string numeroCartao = "";

            // inicializa a qtd de criticas
            int qtdCriticas = 0;

            // Pega conexao com o banco
            OracleConnection cn = Procedures.ReceberConexao();

            // Pega o command para salvar criticas
            OracleCommand cmSalvarCritica = Procedures.PR_CRITICA_S_preparar(cn);

            // Prepara o command para salvar arquivoItem
            OracleCommand cmArquivoItemS = Procedures.PR_ARQUIVO_ITEM_S_preparar(cn);

            // Pede as linhas do arquivo
            OracleDataReader reader =
                Procedures.PR_ARQUIVO_ITEM_L(
                    cn, codigoArquivo, null, false, false, 0, 0, 0, false, false).GetDataReader();


            while (reader.Read())
            {
                // Pega a linha
                string linha =
                    reader.GetString(
                        reader.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));

                // Acha o tipo da linha
                string tipoLinha =
                    reader.GetString(
                        reader.GetOrdinal("TIPO_ARQUIVO_ITEM"));



                if (tipoLinha.Equals("AJ") || tipoLinha.Equals("AP") || tipoLinha.Equals("AV") || tipoLinha.Equals("CP") || tipoLinha.Equals("CV"))
                {
                    //string numeroCartao = "";
                    //string nsuHost = "";
                    if (tipoLinha.Equals("AJ"))
                    {
                        string numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartaoTransacaoOriginal", true);
                        string nsuHost = (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", true);
                        qtdCriticas = this.CartaoCriptografado(firstLine, numeroCartao, nsuHost, codigoArquivo, codigoProcesso, cmSalvarCritica);
                    }
                    if (tipoLinha.Equals("AP") || tipoLinha.Equals("AV") || tipoLinha.Equals("CP") || tipoLinha.Equals("CV"))
                    {
                        string numeroCartao = (string)this.LerCampo(tipoLinha, linha, "NumeroCartao", true);
                        string nsuHost = (string)this.LerCampo(tipoLinha, linha, "NSUHostTransacao", true);
                        qtdCriticas += this.CartaoCriptografado(firstLine, numeroCartao, nsuHost, codigoArquivo, codigoProcesso, cmSalvarCritica);
                    }

                    //this.CartaoCriptografado(firstLine, numeroCartao, nsuHost, codigoArquivo, codigoProcesso, cmSalvarCritica, ref quantidadeCriticas);

                    if (firstLine == true)
                    {
                        firstLine = false;
                    }
                }
            }

            // Passa pela validação default
            base.ValidarArquivo(codigoProcesso, codigoArquivo, contexto, codigoSessao);

            // Retorna a quantidade de críticas
            return qtdCriticas;
        }

        protected int CartaoCriptografado(bool firstLine, string cartao, string nsuHost, string codigoArquivo, string codigoProcesso,
            OracleCommand cmSalvarCritica)
        {
            string criptografia = cartao.Substring(10, 6);
            int qtdCriticas = 0;

            if (criptografia != "******")
            {
                if (firstLine == true)
                {
                    throw new Exception("NUMERO DO CARTÃO não CRIPTOGRAFADO!");

                }
                else
                {

                    CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
                    {
                        Descricao = string.Format("NUMERO DO CARTÃO não CRIPTOGRAFADO da Transação com NSU HOST {(0)}", nsuHost),
                    };
                    criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                    criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                    //criticaArquivoInfo.CodigoArquivoItem = cancelamento[key].CodigoArquivoItem;
                    Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
                }
                qtdCriticas++;
            }

            return qtdCriticas;

        }
    }
}
