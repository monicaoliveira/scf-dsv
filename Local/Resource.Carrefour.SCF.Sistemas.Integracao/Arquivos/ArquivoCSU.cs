﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    /// <summary>
    /// Interpretador de arquivo CSU
    /// </summary>
    public class ArquivoCSU : ArquivoBase
    {
        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Retorna bookInfo 
        /// </summary>
        public BookInfo RetornarBookInfo()
        {
            return this.BookInfo;
        }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoCSU()
        {
            // Monta o book do arquivo
            this.BookInfo = new BookInfo();
            this.BookInfo.TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores;

            // Layout Header
            BookItemCampoInfo campo0 = new BookItemCampoInfo("0");
            campo0.Campos.Add(new BookItemCampoInfo("TIPO-REGISTRO", 1, new ConversorString()));
            campo0.Campos.Add(new BookItemCampoInfo("DATA-GERACAO", 8, new ConversorData("YYYYMMDD")));
            campo0.Campos.Add(new BookItemCampoInfo("DATA-MOVIMENTO", 8, new ConversorData("YYYYMMDD")));
            campo0.Campos.Add(new BookItemCampoInfo("VERSAO", 4, new ConversorString()));
            campo0.Campos.Add(new BookItemCampoInfo("LOGO", 2, new ConversorString()));
            campo0.Campos.Add(new BookItemCampoInfo("FILLER", 361, new ConversorString()));
            campo0.Campos.Add(new BookItemCampoInfo("NSA", 7, new ConversorString()));
            //1447 campo0.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorNumero(0, true)));
            campo0.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorString()));
            this.BookInfo.Campos.Add(campo0);

            // Layout Venda
            BookItemCampoInfo campo1 = new BookItemCampoInfo("1");
            campo1.Campos.Add(new BookItemCampoInfo("TIPO-REGISTRO", 1, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("LOJA", 10, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("CADEIA", 7, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("AGRUPAMENTO", 7, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("UNIAO", 7, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("PRODUTO", 4, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("ORIGEM", 1, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("CARTAO", 19, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("DOSSIE", 14, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("CPF", 11, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("DT-VENDA", 8, new ConversorData("YYYYMMDD")));
            campo1.Campos.Add(new BookItemCampoInfo("DT-CONF", 8, new ConversorData("YYYYMMDD")));
            campo1.Campos.Add(new BookItemCampoInfo("NUM-PARC", 1, new ConversorNumero(typeof(int))));
            campo1.Campos.Add(new BookItemCampoInfo("NUM-TOT-PARC", 3, new ConversorNumero(typeof(int))));
            campo1.Campos.Add(new BookItemCampoInfo("MON-BRU", 15, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("MON-COM", 15, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("MON-LIQ", 15, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("MON-LIQ-PRI", 15, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("MON-LIQ-DEM", 15, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("IOF", 15, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("DT-PREV-REP", 8, new ConversorData("YYYYMMDD")));
            campo1.Campos.Add(new BookItemCampoInfo("SEG-CARDIF", 1, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("PF", 5, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("NSU", 9, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("NUM-AUT", 12, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("CUPON", 10, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("FILLER", 23, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("FLAG-PARC-DIF", 1, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("MON-BRU-PRI", 15, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("MON-BRU-DEM", 15, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("HORA", 6, new ConversorHora("HHMMSS")));
            campo1.Campos.Add(new BookItemCampoInfo("REGISTRO", 1, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("FILLER", 94, new ConversorString()));
            //1447campo1.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorNumero(0, true)));
            campo1.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorString()));
            this.BookInfo.Campos.Add(campo1);

            // Layout Anulação de Venda
            BookItemCampoInfo campo2 = new BookItemCampoInfo("2");
            campo2.Campos.Add(new BookItemCampoInfo("TIPO-REGISTRO", 1, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("LOJA", 10, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("CADEIA", 7, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("AGRUPAMENTO", 7, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("UNIAO", 7, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("PRODUTO", 4, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("ORIGEM", 1, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("CARTAO", 19, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("DOSSIE", 14, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("CPF", 11, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("DT-VENDA", 8, new ConversorData("YYYYMMDD")));
            campo2.Campos.Add(new BookItemCampoInfo("DT-CONF", 8, new ConversorData("YYYYMMDD")));
            campo2.Campos.Add(new BookItemCampoInfo("NUM-PARC", 1, new ConversorNumero(typeof(int))));
            campo2.Campos.Add(new BookItemCampoInfo("NUM-TOT-PARC", 3, new ConversorNumero(typeof(int))));
            campo2.Campos.Add(new BookItemCampoInfo("MON-BRU", 15, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("MON-COM", 15, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("MON-LIQ", 15, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("MON-LIQ-PRI", 15, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("MON-LIQ-DEM", 15, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("IOF", 15, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("DT-PREV-REP", 8, new ConversorData("YYYYMMDD")));
            campo2.Campos.Add(new BookItemCampoInfo("SEG-CARDIF", 1, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("PF", 5, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("NSU", 9, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("NUM-AUT", 12, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("CUPON", 10, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("FILLER", 23, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("FLAG-PARC-DIF", 1, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("MON-BRU-PRI", 15, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("MON-BRU-DEM", 15, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("HORA", 6, new ConversorHora("HHMMSS")));
            campo2.Campos.Add(new BookItemCampoInfo("REGISTRO", 1, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("FILLER", 94, new ConversorString()));
            //1447campo2.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorNumero(0, true)));
            campo2.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorString()));
            this.BookInfo.Campos.Add(campo2);

            // Layout Pagamento de Fatura
            BookItemCampoInfo campo3 = new BookItemCampoInfo("3");
            campo3.Campos.Add(new BookItemCampoInfo("TIPO-REGISTRO", 1, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("LOJA", 10, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("CADEIA", 7, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("AGRUPAMENTO", 7, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("UNIAO", 7, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("ORIGEM", 1, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("CARTAO", 19, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("DOSSIE", 14, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("CPF", 11, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("DT-PAGTO", 8, new ConversorData("YYYYMMDD")));
            campo3.Campos.Add(new BookItemCampoInfo("DT-CONF", 8, new ConversorData("YYYYMMDD")));
            campo3.Campos.Add(new BookItemCampoInfo("NSU", 9, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("NUM-AUT", 12, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("MEIO-PAGTO", 1, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("MON-PAGTO", 15, new ConversorNumero(typeof(double), 2)));
            campo3.Campos.Add(new BookItemCampoInfo("REG-PAGTO", 2, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("FILLER", 158, new ConversorString()));
            campo3.Campos.Add(new BookItemCampoInfo("HORA", 6, new ConversorHora("HHMMSS")));
            campo3.Campos.Add(new BookItemCampoInfo("FILLER", 95, new ConversorString()));
            //1447 campo3.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorNumero(0, true)));
            campo3.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorString())); //1447
            this.BookInfo.Campos.Add(campo3);

            // Layout Anulação de Pagamento 
            BookItemCampoInfo campo4 = new BookItemCampoInfo("4");
            campo4.Campos.Add(new BookItemCampoInfo("TIPO-REGISTRO", 1, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("LOJA", 10, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("CADEIA", 7, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("AGRUPAMENTO", 7, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("UNIAO", 7, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("ORIGEM", 1, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("CARTAO", 19, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("DOSSIE", 14, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("CPF", 11, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("DT-PAGTO", 8, new ConversorData("YYYYMMDD")));
            campo4.Campos.Add(new BookItemCampoInfo("DT-CONF", 8, new ConversorData("YYYYMMDD")));
            campo4.Campos.Add(new BookItemCampoInfo("MON-PGTO", 15, new ConversorNumero(typeof(double), 2)));
            campo4.Campos.Add(new BookItemCampoInfo("FILLER", 182, new ConversorString()));
            campo4.Campos.Add(new BookItemCampoInfo("HORA", 6, new ConversorHora("HHMMSS")));
            campo4.Campos.Add(new BookItemCampoInfo("FILLER", 95, new ConversorString()));
            //1447 campo4.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorNumero(0, true)));
            campo4.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorString()));
            this.BookInfo.Campos.Add(campo4);

            // Layout Trailer
            BookItemCampoInfo campo9 = new BookItemCampoInfo("9");
            campo9.Campos.Add(new BookItemCampoInfo("TIPO-REGISTRO", 1, new ConversorString()));
            campo9.Campos.Add(new BookItemCampoInfo("TOT-LINHAS", 10, new ConversorNumero(0, true)));
            //1447campo9.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorNumero(0, true)));
            campo9.Campos.Add(new BookItemCampoInfo("NSR", 9, new ConversorString()));
            this.BookInfo.Campos.Add(campo9);

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();
        }

        /// <summary>
        /// Dado uma linha do arquivo, infere o tipo
        /// </summary>
        /// <param name="linha"></param>
        /// <returns></returns>
        protected override string OnInferirTipoLinha(object linha)
        {
            //if (linha.ToString().Length > 2)//1444
            //    return ((string)linha).Substring(0, 2);
            //else
            //    return "";
            if (linha.ToString().Length > 2) // 17/09/2018 executando teste de CSU, ocorrendo erro. Tipo Registro (tipo transacao) é tamanho 1
                return ((string)linha).Substring(0, 1);
            else
                return "";
        }

        /// <summary>
        /// Implementa o método virtual para ler um campo
        /// </summary>
        protected override object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
            // Usa o interpretador de arquivos para extrair o valor
            return InterpretadorArquivos.ExtrairValor(this.BookInfo, tipoLinha + "/" + (string)campo, (string)linha, interpretar);
        }

        /// <summary>
        /// Altera o valor de um campo
        /// </summary>
        protected override string OnEscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {
            return InterpretadorArquivos.EscreverValor(this.BookInfo, tipoLinha + "/" + nomeCampo, (string)linha, valor, interpretarValor);
        }

        /// <summary>
        /// Método que faz a importacao da linha
        /// </summary>
        /// <param name="codigoArquivo"></param>
        /// <param name="numeroLinha"></param>
        /// <param name="linha"></param>
        protected override void OnImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha, Dictionary<string, BinReferenciaInfo> dicionarioBinRef)//1407
        {
            // Insere no banco
            if (linha != "")
            {
                // Infere a chave
                string chave = null;
                string tipoLinha = this.InferirTipoLinha(linha);
                switch (tipoLinha)
                {
                    // Comprovante de venda
                    case "1":
                        chave = 
                            ChaveTransacaoHelper.GerarChaveComprovanteVendaArquivo(
                                (string)this.LerCampo(tipoLinha, linha, "DT-VENDA", false),
                                (string)this.LerCampo(tipoLinha, linha, "HORA", false),
                                (string)this.LerCampo(tipoLinha, linha, "LOJA", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSU", false),
                                (string)this.LerCampo(tipoLinha, linha, "NUM-AUT", false),
                                (string)this.LerCampo(tipoLinha, linha, "NUM-PARC", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSR", false));
                        break;
                    case "2":
                        chave =
                            ChaveTransacaoHelper.GerarChaveAnulacaoVendaArquivo(
                                (string)this.LerCampo(tipoLinha, linha, "DT-VENDA", false),
                                (string)this.LerCampo(tipoLinha, linha, "HORA", false),
                                (string)this.LerCampo(tipoLinha, linha, "LOJA", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSU", false),
                                (string)this.LerCampo(tipoLinha, linha, "NUM-AUT", false),
                                (string)this.LerCampo(tipoLinha, linha, "NUM-PARC", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSR", false));
                        break;
                    case "3":
                        chave =
                            ChaveTransacaoHelper.GerarChaveComprovantePagamentoArquivo(
                                (string)this.LerCampo(tipoLinha, linha, "DT-PAGTO", false),
                                (string)this.LerCampo(tipoLinha, linha, "HORA", false),
                                (string)this.LerCampo(tipoLinha, linha, "LOJA", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSU", false),
                                (string)this.LerCampo(tipoLinha, linha, "NUM-AUT", false),
                                (string)this.LerCampo(tipoLinha, linha, "NSR", false));
                        break;
                    case "4":
                        chave =
                            ChaveTransacaoHelper.GerarChaveAnulacaoPagamentoArquivo(
                                (string)this.LerCampo(tipoLinha, linha, "DT-PAGTO", false),
                                (string)this.LerCampo(tipoLinha, linha, "HORA", false),
                                (string)this.LerCampo(tipoLinha, linha, "LOJA", false),
                                "",
                                (string)this.LerCampo(tipoLinha, linha, "NSR", false));
                        break;
                }
                //1407 INICIO INIBINDO
                /*
                // Salva o arquivo_item
                Procedures.PR_ARQUIVO_ITEM_S(
                    cm,
                    null,
                    codigoArquivo,
                    linha,
                    this.InferirTipoLinha(linha),
                    false, ArquivoItemStatusEnum.PendenteValidacao, chave, null);
                */
                //1407 FIM INIBINDO

                //1407 inicio novo
                string codigoArquivoItem = null;
                string conteudoArquivoItem = linha;
                string tipoArquivoItem = this.InferirTipoLinha(linha);
                bool retornarRegistro = false;
                ArquivoItemStatusEnum statusArquivoItem = ArquivoItemStatusEnum.PendenteValidacao;
                string referencia = null;
                Procedures.PR_ARQUIVO_ITEM_S
                (cm
                , codigoArquivoItem
                , codigoArquivo
                , conteudoArquivoItem
                , tipoArquivoItem
                , retornarRegistro
                , statusArquivoItem
                , chave
                , referencia
                );
                //1407 inicio novo
            }
        }

        /// <summary>
        /// Método virtual para quantidade de colunas
        /// </summary>
        /// <param name="tipoLinha"></param>
        /// <returns></returns>
        protected override int OnReceberQuantidadeColunas(string tipoLinha)
        {
            return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Count;
        }
    }
}
