﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    /// <summary>
    /// Interpretador de arquivo Matera - Contabil
    /// </summary>
    public class ArquivoMateraContabil : ArquivoBase
    {
        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Cache de campos do detalhe
        /// </summary>
        private Dictionary<string, BookItemCampoHelper> _cacheDetalhe { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoMateraContabil()
        {
            // Monta o book do arquivo
            this.BookInfo = new BookInfo();
            this.BookInfo.TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores;

            // Adiciona os campos do Detalhe
            BookItemCampoInfo detalhe = new BookItemCampoInfo("detalhe");

            detalhe.Campos.Add(new BookItemCampoInfo("COD_EMPRESA", 7, new ConversorNumero(typeof(int), 0, true)));
            detalhe.Campos.Add(new BookItemCampoInfo("COD_TIPO_CONTABIL", 1, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("NUM_GUIA", 8, new ConversorNumero(typeof(int), 0, true)));
            detalhe.Campos.Add(new BookItemCampoInfo("COD_SIS", 3, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("NUM_LANC", 6, new ConversorNumero(typeof(int), 0, true)));
            detalhe.Campos.Add(new BookItemCampoInfo("COD_FILIAL", 7, new ConversorNumero(typeof(int), 0, true)));
            detalhe.Campos.Add(new BookItemCampoInfo("COD_CONTA_CONTABIL", 15, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("ID_CENTRO_CUSTO", 6, new ConversorNumero(typeof(int), 0, true)));
            detalhe.Campos.Add(new BookItemCampoInfo("COD_PROJETO", 8, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("DT_LANC_CONTABIL", 8, new ConversorData("YYYYMMDD")));
            detalhe.Campos.Add(new BookItemCampoInfo("TIPO_LANC", 1, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("COD_HIST_PADRAO", 4, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("REFERENCIA", 15, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("DT_CONVERSAO", 8, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("VLR", 13, new ConversorNumero(typeof(double), 2,true,false)));
            detalhe.Campos.Add(new BookItemCampoInfo("COMPLEMENTO", 20, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("HIST", 240, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("IND_RATEIO", 1, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("COD_CONTA_CONTRA", 15, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("ID_CCUSTO_CONTRA", 6, new ConversorNumero(typeof(int), 0, true)));
            //detalhe.Campos.Add(new BookItemCampoInfo("COD_CENTRO_CUSTO", 20, new ConversorString()));
            //detalhe.Campos.Add(new BookItemCampoInfo("COD_CENTRO_CUSTO_CONTRA", 20, new ConversorString()));
            this.BookInfo.Campos.Add(detalhe);

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();

            // Prepara cache do detalhe
            _cacheDetalhe = new Dictionary<string, BookItemCampoHelper>();
            foreach (BookItemCampoInfo campo in detalhe.Campos)
                _cacheDetalhe.Add(campo.Nome, this.BookInfo.ReceberDetalheCampo(campo.ReceberCaminho()));
        
        }

        /// <summary>
        /// Implementa o método virtual para gerar a linha
        /// Como só tem 1 tipo, ignora o parametro tipo linha
        /// </summary>
        protected override string OnCriarLinha(string tipoLinha, Dictionary<string, object> valores)
        {
            // Monta parametros com campos recuperados do cache
            Dictionary<BookItemCampoHelper, object> valores2 = new Dictionary<BookItemCampoHelper, object>();
            foreach (KeyValuePair<string, object> item in valores)
                valores2.Add(_cacheDetalhe[item.Key], item.Value);

            // Faz a chamada
            return InterpretadorArquivos.EscreverLinha(this.BookInfo, "detalhe", valores2);
        }

        /// <summary>
        /// Método virtual para quantidade de colunas
        /// </summary>
        /// <param name="tipoLinha"></param>
        /// <returns></returns>
        protected override int OnReceberQuantidadeColunas(string tipoLinha)
        {
            return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Count;
        }

    }
}
