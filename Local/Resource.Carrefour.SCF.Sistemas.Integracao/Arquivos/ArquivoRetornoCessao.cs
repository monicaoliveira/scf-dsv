﻿using System;
using System.Collections.Generic;
using Resource.Framework.Library.Integracao.Arquivos;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Sistemas.Integracao.Regras;
using Resource.Framework.Library;
//1328
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum;
using System.IO;
using System.Data;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

//1444
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    //=========================================================================================================
    /// Interpretador de Arquivo Retorno de Cessao
    //=========================================================================================================
    public class ArquivoRetornoCessao : ArquivoBase
    {
        public string Layout { get; set; } //1444
        public BookInfo BookInfo { get; set; }    /// Layout do arquivo
        public BookInfo RetornarBookInfo()        /// Retorna BookInfo
        {
            return this.BookInfo;
        }
        private Dictionary<string, int> _campos = new Dictionary<string, int>(); // Dicionário para achar posição dos campos, a chave é o nome do campo e o valor é a posição na coleção de campos de this.BookInfo

        //=========================================================================================================
        // CESSAO - Construtor default 
        //=========================================================================================================
        public ArquivoRetornoCessao() 
        {
            //=========================================================================================================
            // Monta o book do arquivo
            //=========================================================================================================
            this.Layout = "1"; //1444
            this.BookInfo = new BookInfo();
            this.BookInfo.TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores;
            //=========================================================================================================
            // Layout Header
            //=========================================================================================================
            BookItemCampoInfo campo0 = new BookItemCampoInfo("R0");
            campo0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2 , new ConversorString()));
            campo0.Campos.Add(new BookItemCampoInfo("DataProcessamento", 8, new ConversorData("YYYYMMDD")));
            campo0.Campos.Add(new BookItemCampoInfo("HoraProcessamento", 6, new ConversorString()));
            campo0.Campos.Add(new BookItemCampoInfo("Filler", 127, new ConversorString()));
            this.BookInfo.Campos.Add(campo0);
            //=========================================================================================================
            // Layout Detalhe
            //=========================================================================================================
            BookItemCampoInfo campo1 = new BookItemCampoInfo("R1");
            campo1.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, new ConversorData("YYYYMMDD")));
            campo1.Campos.Add(new BookItemCampoInfo("NumeroTotalPercelas", 3, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, new ConversorNumero(typeof(double), 2)));
            campo1.Campos.Add(new BookItemCampoInfo("CNPJFavorecido", 15, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("NomeFavorecido", 30, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("Banco", 3, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("Agencia", 6, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("Conta", 11, new ConversorString()));
            this.BookInfo.Campos.Add(campo1);
            //=========================================================================================================
            // Layout Trailler
            //=========================================================================================================
            BookItemCampoInfo campo2 = new BookItemCampoInfo("R9");
            campo2.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("TotalRegistros", 7, new ConversorString()));
            campo2.Campos.Add(new BookItemCampoInfo("TotalValorBruto", 17, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("TotalValorDesconto", 17, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("TotalValorLiquido", 17, new ConversorNumero(typeof(double), 2)));
            campo2.Campos.Add(new BookItemCampoInfo("Filler", 84, new ConversorString()));
            this.BookInfo.Campos.Add(campo2);
            //=========================================================================================================
            // Cria um dicionario para traduzir o nome para a posição
            //=========================================================================================================
            for (int i = 0; i < this.BookInfo.Campos.Count; i++)
                _campos.Add(this.BookInfo.Campos[i].Nome, i);
            //=========================================================================================================
            // Ajusta posições e tamanhos
            //=========================================================================================================
            this.BookInfo.CalculaPosicaoTamanhoCampos();
        }

        //=========================================================================================================
        /// CESSAO - Lista de Regras
        //=========================================================================================================
        protected override List<RegraSCFBase> OnReceberRegras()
        {
            //=========================================================================================================
            // Cria a lista de regras
            //=========================================================================================================
            List<RegraSCFBase> regras = new List<RegraSCFBase>();
            //=========================================================================================================
            // tipo da linha
            //=========================================================================================================
            string tipoLinha = "R1";
            //=========================================================================================================
            // 01a REGRA - CODIGO DE REGISTRO
            //=========================================================================================================
            regras.Add(
                new RegraSCFString()
                {
                    NomeCampo = "CodigoRegistro",
                    TipoLinha = tipoLinha,
                    IgualConstante = tipoLinha,
                    EhObrigatorio = true
                });
            //=========================================================================================================
            // 02a REGRA - CNPJ do Estabelecimento
            //=========================================================================================================
            BookItemCampoHelper campo = this.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "IdentificacaoLoja");
            regras.Add(
                new RegraSCFNumero()
                {
                    NomeCampo = "IdentificacaoLoja",
                    TipoLinha = tipoLinha,
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });
            //=========================================================================================================
            // 03a REGRA - Estabelecimento
            //=========================================================================================================
            regras.Add(
                new RegraSCFEstabelecimento()
                {
                    TipoLinha = tipoLinha,
                    NomeCampo = "IdentificacaoLoja"

                });
            //=========================================================================================================
            // 04a REGRA - Banco
            //=========================================================================================================
            campo = this.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Banco");
            regras.Add(
                new RegraSCFNumero()
                {
                    NomeCampo = "Banco",
                    TipoLinha = tipoLinha,
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });
            //=========================================================================================================
            // 05a REGRA - Agencia
            //=========================================================================================================
            campo = this.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Agencia");
            regras.Add(
                new RegraSCFNumero()
                {
                    NomeCampo = "Agencia",
                    TipoLinha = tipoLinha,
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });
            //=========================================================================================================
            // 06a REGRA - Agencia
            //=========================================================================================================
            regras.Add(
                new RegraSCFAgencia()
                {
                    NomeCampo = "Agencia",
                    TipoLinha = tipoLinha,
                });
            //=========================================================================================================
            // 07a REGRA - Conta
            //=========================================================================================================
            campo = this.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "Conta");
            regras.Add(
                new RegraSCFNumero()
                {
                    NomeCampo = "Conta",
                    TipoLinha = tipoLinha,
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });
            //=========================================================================================================
            // 08a REGRA - Conta
            //=========================================================================================================
            regras.Add(
                new RegraSCFConta()
                {
                    NomeCampo = "Conta",
                    TipoLinha = tipoLinha,
                });
            //=========================================================================================================
            // 09a REGRA - CNPJ do Favorecido
            //=========================================================================================================
            campo = this.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "CNPJFavorecido");
            regras.Add(
                new RegraSCFNumero()
                {
                    NomeCampo = "CNPJFavorecido",
                    TipoLinha = tipoLinha,
                    EhObrigatorio = true,
                    Tamanho = campo.BookItemCampoInfo.Tamanho
                });
            //=========================================================================================================
            // 10a REGRA - Valida a tranacao que será cessionada
            //=========================================================================================================
            ConfiguracaoGeralInfo configuracaoGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(null, null); //1328
            //bool ReprocessarTransacao = configuracaoGeral.ReprocessarNSUCessionado; //1328     // 06/12/2018 - Não atualizava transações cessionadas mesmo com FLAG igual a sim       
            regras.Add( 
                new RegrasSCFValidarTransacao() 
                {
                    TipoLinha = tipoLinha,
                    Banco = "Banco",
                    Agencia = "Agencia",
                    Conta = "Conta",
                    CnpjFavorecido = "CNPJFavorecido",
                    NSUHostTransacao = "NSUHostTransacao",
                    DataTransacao = "DataTransacao",
                    IdentificacaoLoja = "IdentificacaoLoja",
                    CodigoAutorizacao = "CodigoAutorizacao",
                    NumeroParcela = "NumeroParcela",
                    NomeFavorecido = "NomeFavorecido",
                    ReprocessarNSUCessionado = configuracaoGeral.ReprocessarNSUCessionado  // 06/12/2018 - Não atualizava transações cessionadas mesmo com FLAG igual a sim
                });
            //=========================================================================================================
            // Retorna a lista de regras
            //=========================================================================================================
            return regras;
        }

        //=========================================================================================================
        /// Dado uma linha do arquivo, infere o tipo
        //=========================================================================================================
        protected override string OnInferirTipoLinha(object linha)
        {
            if (linha.ToString().Length > 2)//1444
                return ((string)linha).Substring(0, 2);
            else
                return "";
        }

        //=========================================================================================================
        /// Retorna nomes dos campos
        //=========================================================================================================
        protected override string[] OnReceberNomeCampos(string tipoLinha)
        {
            //=========================================================================================================
            // Retorna lista com os nomes dos campos do tipo de linha informado
            //=========================================================================================================
            return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Select(c => c.Nome).ToArray();
        }

        //=========================================================================================================
        ///// Implementa o método virtual para ler um campo
        //=========================================================================================================
        protected override object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
            //=========================================================================================================
            // Usa o interpretador de arquivos para extrair o valor
            //=========================================================================================================
            return InterpretadorArquivos.ExtrairValor(this.BookInfo, tipoLinha + "/" + (string)campo, (string)linha, interpretar);
        }

        //=========================================================================================================
        /// Infere o tipo da linha e retorna valor de um campo
        //=========================================================================================================
        public object RetornarValor(string nomeCampo, string linha)
        {
            return this.RetornarValor(this.InferirTipoLinha(linha), nomeCampo, linha, false);
        }

        //=========================================================================================================
        /// Infere o tipo da linha e retorna valor de um campo
        //=========================================================================================================
        public object RetornarValor(string nomeCampo, string linha, bool interpretar)
        {
            return this.RetornarValor(this.InferirTipoLinha(linha), nomeCampo, linha, interpretar);
        }

        //=========================================================================================================
        /// Retorna o valor do campo solicitado
        //=========================================================================================================
        public object RetornarValor(string tipoLinha, string nomeCampo, string linha)
        {
            return InterpretadorArquivos.ExtrairValor(this.BookInfo, tipoLinha + "/" + nomeCampo, linha, false);
        }
        //=========================================================================================================
        /// Retorna o valor do campo solicitado
        //=========================================================================================================
        public object RetornarValor(string tipoLinha, string nomeCampo, string linha, bool interpretar)
        {
            return InterpretadorArquivos.ExtrairValor(this.BookInfo, tipoLinha + "/" + nomeCampo,  linha, interpretar);
        }
        //=========================================================================================================
        /// 1444 Método que trata o arquivo
        //=========================================================================================================
        protected override long OnImportarArquivo(string arquivo, string codigoProcesso, string chaveArquivo, string codigoEmpresaArquivo)
        {
            chaveArquivo = validarImportacaoCESSAO(arquivo, codigoProcesso, null);   // Primeiro, verifica se é permitido importar o arquivo
            if (chaveArquivo != "")
                return base.OnImportarArquivo(arquivo, codigoProcesso, chaveArquivo, this.Layout, codigoEmpresaArquivo);
            else return 0;
        }
        //=========================================================================================================
        /// Método que faz a importacao da linha
        //=========================================================================================================
        protected override void OnImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha, Dictionary<string, BinReferenciaInfo> dicionarioBinRef)//1407
        {
            //=========================================================================================================
            // Insere no banco
            //=========================================================================================================
            if (linha != "")
            {
                //1407 inicio inibindo
                /*
                Procedures.PR_ARQUIVO_ITEM_S(cm, null,codigoArquivo, linha,this.InferirTipoLinha(linha), false, ArquivoItemStatusEnum.PendenteValidacao,null, null);
                */
                //1407 fim inibindo
    
                //1407 inicio novo
                string codigoArquivoItem = null;
                string conteudoArquivoItem = linha;
                string tipoArquivoItem = this.InferirTipoLinha(linha);
                bool retornarRegistro = false;
                ArquivoItemStatusEnum statusArquivoItem = ArquivoItemStatusEnum.PendenteValidacao;
                string chave = null;
                string referencia = null;
                Procedures.PR_ARQUIVO_ITEM_S
                (cm
                , codigoArquivoItem
                , codigoArquivo
                , conteudoArquivoItem
                , tipoArquivoItem
                , retornarRegistro
                , statusArquivoItem
                , chave
                , referencia
                );
                //1407 inicio novo
            }

        }
        //=========================================================================================================
        /// Método virtual para quantidade de colunas
        //=========================================================================================================
        protected override int OnReceberQuantidadeColunas(string tipoLinha)
        {
            return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Count;
        }
        //=========================================================================================================
        /// Altera o valor de um campo
        //=========================================================================================================
        protected override string OnEscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor)
        {
            return InterpretadorArquivos.EscreverValor(this.BookInfo, tipoLinha + "/" + nomeCampo, (string)linha, valor, interpretarValor);
        }
        //============================================================================================
        // 1444 - INICIO
        //============================================================================================
        private string validarImportacaoCESSAO(string arquivo, string codigoProcesso, string codigoSessao)
        {
            int qtdeLinhasLidas = 0;
            string chaveArquivo = "";
            bool linhaR0 = false;
            bool linhaR1 = false;
            bool linhaR9 = false;
            string codigoArquivo = "";
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            OracleConnection cn = Procedures.ReceberConexao();
            ProcessoInfo processoAtual = PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, codigoProcesso);           
            try
            {
                FileInfo fileInfo = new FileInfo(arquivo);
                using (StreamReader reader = File.OpenText(arquivo))
                {
                    string vMSGARQ = "";
                    string linha = "";
                    // bool continua = true; // warning 25/02/2019
                    ArquivoRetornoCessao arquivoCESSAO = this;

                    while ( (linha = reader.ReadLine()) != null)
                    {
                        qtdeLinhasLidas++;
                        string tipoTransacao = arquivoCESSAO.InferirTipoLinha(linha);

                        if (tipoTransacao == "R9")
                        {
                            linhaR9 = true;
                        }

                        if (tipoTransacao == "R1")
                        {
                            linhaR1 = true;
                        }

                        if (tipoTransacao == "R0")
                        {
                            linhaR0 = true;
                            // 1496 -  arquivo de retorno de cessão não vem com a hora correta informada no registro "R0" gerando assim erro no dia 24/10/2017 em producao
                            // impedindo de executar as outras cessoes. Solucao pegar pelo nome do arquivo a data e hora.
                            string dataProcessamento = (string)arquivoCESSAO.LerCampo("R0", linha, "DataProcessamento");
                            string horaProcessamento = (string)arquivoCESSAO.LerCampo("R0", linha, "HoraProcessamento");
                            //chaveArquivo = dataProcessamento + horaProcessamento;
                            chaveArquivo = fileInfo.Name.Substring(13, 14); //1496 
                            vMSGARQ = "Data Processamento (R0) [" + dataProcessamento + "]  Hora Processamento (R0) [" + horaProcessamento + "] Chave (nome do arquivo) [" + chaveArquivo + " ] ";
                            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                            condicoes.Add(new CondicaoInfo("ChaveArquivo", CondicaoTipoEnum.Igual, chaveArquivo));
                            List<ArquivoInfo> arquivos = PersistenciaHelper.Listar<ArquivoInfo>(codigoSessao, condicoes);
                            if (arquivos.Count > 0)
                            {
                                foreach (ArquivoInfo item in arquivos)
                                {
                                    if (item.StatusArquivo != ArquivoStatusEnum.Cancelado && item.StatusArquivo != ArquivoStatusEnum.Estornado)
                                    {
                                        ProcessoInfo processo = PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, item.CodigoProcesso);
                                        if ((processo != null && processoAtual != null) && processo.TipoProcesso == processoAtual.TipoProcesso)
                                        {
                                            if (processoAtual.CodigoProcesso == processo.CodigoProcesso)
                                            {
                                                codigoArquivo = item.CodigoArquivo;
                                                Procedures.PR_ARQUIVO_ITEM_R(codigoArquivo);
                                                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                                                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("IMPORTACAO TRATAMENTO CESSAO - Arquivo [{0}], anterior deste processo, foi EXCLUIDO", codigoArquivo));
                                                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                                            }
                                            else
                                            {
                                                throw new Exception(string.Format("IMPORTACAO VALIDACAO CESSAO- Este arquivo [{0}] de Identificação [{1}] já foi importado no Processo: [{2}] " + vMSGARQ, fileInfo.Name, chaveArquivo, item.CodigoProcesso));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    reader.Close();
                    if (!linhaR0)
                    {
                        throw new Exception(string.Format("IMPORTACAO VALIDACAO CESSAO - Este arquivo [{0}] de Identificação [{1}] não possue linha R0 " + vMSGARQ, fileInfo.Name, chaveArquivo));
                    }
                    if (!linhaR1)
                    {
                        throw new Exception(string.Format("IMPORTACAO VALIDACAO CESSAO - Este arquivo [{0}] de Identificação [{1}] não possue linha R1 " + vMSGARQ , fileInfo.Name, chaveArquivo));
                    }
                    if (!linhaR9)
                    {
                        throw new Exception(string.Format("IMPORTACAO VALIDACAO CESSAO- Este arquivo [{0}] de Identificação [{1}] não possue linha R9 " + vMSGARQ, fileInfo.Name, chaveArquivo));
                    }
                    vMSGARQ = "IMPORTACAO VALIDACAO CESSAO- " + vMSGARQ + " - Contem [" + qtdeLinhasLidas.ToString() + "]";
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                    //lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("IMPORTACAO VALIDACAO - Arquivo contem [{0}] linhas", qtdeLinhasLidas));
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(vMSGARQ);
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }
            string retorno;
            retorno = chaveArquivo;
            return retorno;
            //============================================================================================
            // 1444 - INICIO
            //============================================================================================
        }
    }
}
