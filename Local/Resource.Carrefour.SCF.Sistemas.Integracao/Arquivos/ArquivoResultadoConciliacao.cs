﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    /// <summary>
    /// Interpretador de arquivo Resultado conciliacao
    /// </summary>
    public class ArquivoResultadoConciliacao : ArquivoBase
    {
        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Cache de campos do header
        /// </summary>
        private Dictionary<string, BookItemCampoHelper> _cacheHeader { get; set; }

        /// <summary>
        /// Cache de campos do detalhe
        /// </summary>
        private Dictionary<string, BookItemCampoHelper> _cacheDetalhe { get; set; }

        /// <summary>
        /// Cache de campos do trailer
        /// </summary>
        private Dictionary<string, BookItemCampoHelper> _cacheTrailer { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoResultadoConciliacao()
        {
            // Monta o book do arquivo
            this.BookInfo = new BookInfo();
            this.BookInfo.TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores;

            //// Header
            BookItemCampoInfo header = new BookItemCampoInfo("header");
            header.Campos.Add(new BookItemCampoInfo("INCLUSAO", 10, new ConversorString(), "Data"));
            header.Campos.Add(new BookItemCampoInfo("sep1", 1, new ConversorString(), ";"));
            
            //header.Campos.Add(new BookItemCampoInfo("Chave", 5, new ConversorString(), "Chave"));
            //header.Campos.Add(new BookItemCampoInfo("sep2", 1, new ConversorString(), ";"));

            header.Campos.Add(new BookItemCampoInfo("NSU_HOST", 20, new ConversorString(), "NSU_HOST"));
            header.Campos.Add(new BookItemCampoInfo("sep3", 1, new ConversorString(), ";"));
            
            header.Campos.Add(new BookItemCampoInfo("DATA_TRANSACAO", 15, new ConversorString(), "DATA_TRANSACAO"));
            header.Campos.Add(new BookItemCampoInfo("sep4", 1, new ConversorString(), ";"));

            header.Campos.Add(new BookItemCampoInfo("CODIGO_AUTORIZACAO", 20, new ConversorString(), "AUTORIZACAO"));
            header.Campos.Add(new BookItemCampoInfo("sep5", 1, new ConversorString(), ";"));
            
            header.Campos.Add(new BookItemCampoInfo("VALOR", 20, new ConversorString(), "Valor"));
            header.Campos.Add(new BookItemCampoInfo("sep8", 1, new ConversorString(), ";"));

            //header.Campos.Add(new BookItemCampoInfo("StatusTransacao", 20, new ConversorString(), "STATUS_TRANSACAO"));
            //header.Campos.Add(new BookItemCampoInfo("sep6", 1, new ConversorString(), ";"));

            header.Campos.Add(new BookItemCampoInfo("StatusConciliacao", 20, new ConversorString(), "STATUS_CONCILIACAO"));
            header.Campos.Add(new BookItemCampoInfo("sep7", 1, new ConversorString(), ";"));

            header.Campos.Add(new BookItemCampoInfo("HOrigem", 20, new ConversorString(), "ORIGEM"));
            header.Campos.Add(new BookItemCampoInfo("sep9", 1, new ConversorString(), ";"));

            header.Campos.Add(new BookItemCampoInfo("Processo", 20, new ConversorString(), "PROCESSO"));
            header.Campos.Add(new BookItemCampoInfo("sep10", 1, new ConversorString(), ";"));


            this.BookInfo.Campos.Add(header);

            // Detalhe
            BookItemCampoInfo detalhe = new BookItemCampoInfo("detalhe");
            detalhe.Campos.Add(new BookItemCampoInfo("Data", 10, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("sep1", 1, new ConversorString(), ";"));
            
            //detalhe.Campos.Add(new BookItemCampoInfo("Chave", 30, new ConversorString()));
            //detalhe.Campos.Add(new BookItemCampoInfo("sep2", 1, new ConversorString(), ";"));
            
            detalhe.Campos.Add(new BookItemCampoInfo("NSU_HOST", 20, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("sep3", 1, new ConversorString(), ";"));

            detalhe.Campos.Add(new BookItemCampoInfo("DATA_TRANSACAO", 15, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("sep4", 1, new ConversorString(), ";"));

            detalhe.Campos.Add(new BookItemCampoInfo("CODIGO_AUTORIZACAO", 20, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("sep5", 1, new ConversorString(), ";"));

            detalhe.Campos.Add(new BookItemCampoInfo("VALOR_VENDA", 20, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("sep8", 1, new ConversorString(), ";"));

            //detalhe.Campos.Add(new BookItemCampoInfo("StatusTransacao", 20, new ConversorString()));
            //detalhe.Campos.Add(new BookItemCampoInfo("sep6", 1, new ConversorString(), ";"));
            
            detalhe.Campos.Add(new BookItemCampoInfo("StatusConciliacao", 20, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("sep7", 1, new ConversorString(), ";"));

            detalhe.Campos.Add(new BookItemCampoInfo("ORIGEM", 20, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("sep9", 1, new ConversorString(), ";"));

            detalhe.Campos.Add(new BookItemCampoInfo("CODIGOPROCESSO", 20, new ConversorString()));
            detalhe.Campos.Add(new BookItemCampoInfo("sep10", 1, new ConversorString(), ";"));

            this.BookInfo.Campos.Add(detalhe);

            // Ajusta posicoes e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();

            //// Prepara cache do header
            _cacheHeader = new Dictionary<string,BookItemCampoHelper>();
            foreach (BookItemCampoInfo campo in header.Campos)
                _cacheHeader.Add(campo.Nome, this.BookInfo.ReceberDetalheCampo(campo.ReceberCaminho()));

            // Prepara cache do detalhe
            _cacheDetalhe = new Dictionary<string, BookItemCampoHelper>();
            foreach (BookItemCampoInfo campo in detalhe.Campos)
                _cacheDetalhe.Add(campo.Nome, this.BookInfo.ReceberDetalheCampo(campo.ReceberCaminho()));

        }

        /// <summary>
        /// Método virtual para criação de linha
        /// </summary>
        protected override string OnCriarLinha(string tipoLinha, Dictionary<string, object> valores)
        {
            // Monta parametros com campos recuperados do cache
            Dictionary<BookItemCampoHelper, object> valores2 = new Dictionary<BookItemCampoHelper, object>();
            foreach (KeyValuePair<string, object> item in valores)
                valores2.Add(_cacheDetalhe[item.Key], item.Value);

            // Cria a linha e retorna
            return InterpretadorArquivos.EscreverLinha(this.BookInfo, tipoLinha, valores2);
        }

        /// <summary>
        /// Método virtual para quantidade de colunas
        /// </summary>
        /// <param name="tipoLinha"></param>
        /// <returns></returns>
        protected override int OnReceberQuantidadeColunas(string tipoLinha)
        {
            return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Count;
        }

    }
}
