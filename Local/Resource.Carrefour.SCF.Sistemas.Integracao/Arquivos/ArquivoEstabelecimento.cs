﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Integracao.Arquivos;

namespace Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos
{
    public class ArquivoEstabelecimento : ArquivoBase
    {
        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public BookInfo BookInfo { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoEstabelecimento()
        {
            this.BookInfo = new BookInfo()
            {
                TipoInterpretacaoValor = BookInterpretacaoValorTipoEnum.Conversores
            };

            BookItemCampoInfo campo1 = new BookItemCampoInfo("detalhe");
            campo1.Campos.Add(new BookItemCampoInfo("CNPJ", 15, new ConversorNumero(typeof(int))));
            campo1.Campos.Add(new BookItemCampoInfo("RazaoSocial", 20, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("Favorecido", 15, new ConversorNumero(typeof(int))));
            campo1.Campos.Add(new BookItemCampoInfo("Grupo", 40, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("SubGrupo", 20, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("Estabelecimento_CSU", 10, new ConversorString()));
            campo1.Campos.Add(new BookItemCampoInfo("Estabelecimento_SITEF", 10, new ConversorString()));
            this.BookInfo.Campos.Add(campo1);

            // Ajusta posições e tamanhos
            this.BookInfo.CalculaPosicaoTamanhoCampos();
        }

        /// <summary>
        /// Implementação do método que retorna valor do campo
        /// </summary>
        /// <param name="tipoLinha"></param>
        /// <param name="linha"></param>
        /// <param name="campo"></param>
        /// <param name="interpretar"></param>
        /// <returns></returns>
        protected override object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
           return InterpretadorArquivos.ExtrairValor(this.BookInfo, tipoLinha + "/" + (string)campo, (string)linha, interpretar);
        }

        /// <summary>
        /// Método virtual para quantidade de colunas
        /// </summary>
        /// <param name="tipoLinha"></param>
        /// <returns></returns>
        protected override int OnReceberQuantidadeColunas(string tipoLinha)
        {
            return this.BookInfo.ReceberDetalheCampo(tipoLinha).BookItemCampoInfo.Campos.Count;
        }
    }
}

