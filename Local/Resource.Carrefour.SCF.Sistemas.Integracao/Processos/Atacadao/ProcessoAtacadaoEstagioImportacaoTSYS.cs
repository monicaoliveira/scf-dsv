﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Importacao de Arquivo TSYS
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioImportacaoCSU))]
    public class ProcessoAtacadaoEstagioImportacaoTSYS : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioImportacaoTSYS(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo TSYS
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS != null)
            {
                // Faz a importacao do arquivo
                ImportarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ImportarArquivoResponse>(
                        new ImportarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS,
                            TipoArquivo = "ArquivoTSYS",
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso
                        });

                // Informa codigo do arquivo no processo
                this.ProcessoAtacadaoInfo.CodigoArquivoTSYS = respostaArquivo.CodigoArquivo;

                // Gera transacoes do arquivo importado
                ImportarTransacoesTSYSResponse respostaTransacaoes =
                    Mensageria.Processar<ImportarTransacoesTSYSResponse>(
                        new ImportarTransacoesTSYSRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoAtacadaoInfo.CodigoArquivoTSYS,
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso
                        });
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoTSYS = null;
        }
    }
}

