﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Importacao CSU
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioImportacaoTSYS),
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioImportacaoSitef))]
    public class ProcessoAtacadaoEstagioImportacaoCSU : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioImportacaoCSU(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo CSU
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CaminhoArquivoCSU != null)
            {
                // Faz a importacao do arquivo
                ImportarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ImportarArquivoResponse>(
                        new ImportarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoCSU,
                            TipoArquivo = "ArquivoCSU",
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso
                        });

                // Informa codigo do arquivo no processo
                this.ProcessoAtacadaoInfo.CodigoArquivoCSU = respostaArquivo.CodigoArquivo;

                // Gera transacoes do arquivo importado
                ImportarTransacoesCSUResponse respostaTransacaoes =
                    Mensageria.Processar<ImportarTransacoesCSUResponse>(
                        new ImportarTransacoesCSURequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoAtacadaoInfo.CodigoArquivoCSU,
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso
                        });
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoCSU = null;
        }
    }
}
