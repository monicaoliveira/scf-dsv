﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Exportacao de Matera Financeiro
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo), 
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioImportacaoSitef),
        TipoProximoEstagio = null)]
    public class ProcessoAtacadaoEstagioExportacaoMateraFinanceiro : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioExportacaoMateraFinanceiro(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento exportação do arquivo matera financeiro
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede para processar apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CodigoArquivoMateraFinanceiro != null)
            {
                // Pede a exportacao do arquivo da matera
                ExportarMateraFinanceiroResponse respostaExportar =
                    Mensageria.Processar<ExportarMateraFinanceiroResponse>(
                        new ExportarMateraFinanceiroRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivoDestino = this.ProcessoAtacadaoInfo.CaminhoArquivoMateraFinanceiro
                        });
            }
        }

        /// <summary>
        /// Processa o cancelamento da exportacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoMateraFinanceiro = null;
        }
    }
}
