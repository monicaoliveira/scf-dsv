﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Regra de coluna
    /// </summary>
    public class RegraSCFColunaBase : RegraSCFBase
    {
        /// <summary>
        /// Tipo da linha a que a regra se refere
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Nome do campo a que a regra se refere
        /// </summary>
        public string NomeCampo { get; set; }

    }
}
