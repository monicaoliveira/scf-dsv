﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Mensagem local para validação de linha
    /// </summary>
    public class RegraSCFValidarLinhaRequest
    {
        public List<CriticaSCFArquivoItemInfo> Criticas { get; set; } 
        public ArquivoBase Arquivo { get; set; }
        public Dictionary<string, object> Contexto { get; set; }
        public ArquivoItemStatusEnum Status { get; set; }
        public List<ListaItemReferenciaInfo> listaItemReferenciaInfo { get; set; }
        
        public int SequencialNSU { get; set; }
        
        public string CodigoProcesso { get; set; }
        public string CodigoArquivo { get; set; }
        public string CodigoArquivoOriginal { get; set; }
        public string CodigoArquivoItem { get; set; }
        public string TipoLinha { get; set; }
        public string Linha { get; set; }
        public string ChaveItem { get; set; } // 1328
        public string Referencia { get; set; } //1339
        public string CodigoEstabelecimento { get; set; } //1339
        public string CNPJEstabelecimento { get; set; } //1339
        public string NomeEstabelecimento { get; set; } //1339
        public string CodigoProduto { get; set; } //1339
        public string NomeProduto { get; set; } //1339
        public string CodigoFavorecido { get; set; } //1343
        public string CNPJFavorecido { get; set; } //1343
        public string NomeFavorecido { get; set; } //1343
        public string NSEQ { get; set; } //1353
        public string Grupo { get; set; }
       
        public bool Executou_Regra { get; set;} //1407
        public bool LinhaAtualizada { get; set; } // 1408
        public bool LinhaCorrigida { get; set; }
        public bool LinhaBloqueada { get; set; } //1328
        public bool ReprocessarTransacao { get; set; } //1328
        
    }
}
