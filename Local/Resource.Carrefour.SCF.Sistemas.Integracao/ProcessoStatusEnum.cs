﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Status de processo
    /// </summary>
    public enum ProcessoStatusEnum
    {
        EmAndamento,
        Cancelado,
        Finalizado
    }
}
