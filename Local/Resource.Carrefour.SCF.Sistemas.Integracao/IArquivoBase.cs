﻿using System;
using System.Collections.Generic;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;//1407

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    interface IArquivoBase
    {
        string CriarLinha(string tipoLinha, params object[] valores);
        string CriarLinha(string tipoLinha, System.Collections.Generic.Dictionary<string, object> valores);
        string EscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor);
        string EscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor, bool interpretarValor);
        long ImportarArquivo(string arquivo, string codigoProcesso, string codigoEmpresaArquivo);
        //1407 void ImportarLinha(Oracle.DataAccess.Client.OracleCommand cm, long codigoArquivo, int numeroLinha, string linha);
        void ImportarLinha(Oracle.DataAccess.Client.OracleCommand cm, long codigoArquivo, int numeroLinha, string linha, Dictionary<string, BinReferenciaInfo> dicionarioBinRef);
        string InferirTipoLinha(object linha);
        string InferirTipoLinha(string linha);
        object LerCampo(string tipoLinha, object linha, int indiceCampo, bool interpretar);
        object LerCampo(string tipoLinha, object linha, string nomeCampo, bool interpretar);
        object LerCampo(object linha, string nomeCampo, bool interpretar);
        object LerCampo(object linha, string nomeCampo);
        object LerCampo(string tipoLinha, object linha, string nomeCampo);
        object LerCampo(object linha, int indiceCampo, bool interpretar);
        System.Collections.Generic.List<RegraSCFBase> ReceberRegras();
        int ValidarArquivo(string codigoProcesso, string codigoArquivo, Dictionary<string, object> contexto, List<RegraSCFBase> regras);
        int ValidarArquivo(string codigoProcesso, string codigoArquivo, Dictionary<string, object> contexto);
    }
}
