﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    public class RegraSCFValidarArquivoRequest
    {
        public List<CriticaSCFArquivoInfo> Criticas { get; set; }
        public ArquivoBase Arquivo { get; set; }
        public Dictionary<string, object> Contexto { get; set; }

        public string Grupo { get; set; }
        public string CodigoArquivo { get; set; }
        public bool Executou_Regra { get; set; } //1407

    }
}
