﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Sistemas;


namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    public class ServicoValidarBinVazioZerado
    {
        //1262  Código duplicado no RegraSCFBinReferencia
        private Dictionary<string, BinReferenciaInfo> dicionarioBinRef = null;

        public void BinVazioZerado(string cartao, string codArqItem, string codigoArquivo, string codigoProcesso, ArquivoItemStatusEnum? status,
                                        OracleCommand cmSalvarCritica, ref int quantidadeCriticas, ProcessoInfo processoInfo, string codigoSessao, out bool linhaBloqueada) //SCF1303
        {
            linhaBloqueada = false; //SCF1303

            // Inicializa
            dicionarioBinRef = new Dictionary<string, BinReferenciaInfo>();
            List<BinReferenciaInfo> BinReferencias =
                    PersistenciaHelper.Listar<BinReferenciaInfo>(null);
            foreach (BinReferenciaInfo binReferencia in BinReferencias)
                if (binReferencia.CodigoBin != null)
                {
                    if (!dicionarioBinRef.ContainsKey(binReferencia.CodigoBin))
                        dicionarioBinRef.Add(binReferencia.CodigoBin, binReferencia);
                }



            string cartaoBin = cartao.Substring(3, 6);

            if (cartaoBin.Trim() == "" || cartaoBin == "000000")
            {
                CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
                {
                    Descricao = string.Format("Transação sem número de BIN", cartaoBin),
                };
                criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                criticaArquivoInfo.CodigoArquivoItem = codArqItem;
                criticaArquivoInfo.TipoLinha = "AJ";
                criticaArquivoInfo.NomeCampo = "NumeroCartaoTransacaoOriginal";

                Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);

                Procedures.PR_ARQUIVO_ITEM_STATUS_S(
                      codigoArquivo, codArqItem, null, null, null, null,
                      ArquivoItemStatusEnum.Bloqueado);

                linhaBloqueada = true; //SCF1303

                processoInfo.StatusBloqueio = ProcessoStatusBloqueioEnum.HaBloqueios;
                processoInfo.Atualizar(
                    PersistenciaHelper.Salvar<ProcessoInfo>(
                        codigoSessao, processoInfo));

                quantidadeCriticas++;
            }
            else if (!dicionarioBinRef.ContainsKey(cartaoBin))
            {
                CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica //1262 new CriticaSCFDominio
                {
                    Descricao = string.Format("Bin ({0}) não cadastrado.", cartaoBin),
                };
                criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                criticaArquivoInfo.CodigoArquivoItem = codArqItem;
                criticaArquivoInfo.TipoLinha = "AJ";
                criticaArquivoInfo.NomeCampo = "NumeroCartaoTransacaoOriginal";

                Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);

                Procedures.PR_ARQUIVO_ITEM_STATUS_S(
                      codigoArquivo, codArqItem, null, null, null, null,
                      ArquivoItemStatusEnum.Bloqueado);

                linhaBloqueada = true; //SCF1303

                processoInfo.StatusBloqueio = ProcessoStatusBloqueioEnum.HaBloqueios;
                processoInfo.Atualizar(
                    PersistenciaHelper.Salvar<ProcessoInfo>(
                        codigoSessao, processoInfo));

                quantidadeCriticas++;
            }
            else
            {
                // Verifica se há referencia cadastrado para esse bin
                BinReferenciaInfo bin = dicionarioBinRef[cartaoBin];
                if (bin.CodigoReferencia == "")
                {
                    CriticaSCFArquivoItemInfo criticaArquivoInfo = new CriticaSCFEspecifica
                    {
                        Descricao = string.Format("Referência do BIN ({0}) não cadastrada", cartaoBin),
                    };
                    criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                    criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                    criticaArquivoInfo.CodigoArquivoItem = codArqItem;
                    criticaArquivoInfo.TipoLinha = "AJ";
                    criticaArquivoInfo.NomeCampo = "NumeroCartaoTransacaoOriginal";

                    Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);

                    Procedures.PR_ARQUIVO_ITEM_STATUS_S(
                          codigoArquivo, codArqItem, null, null, null, null,
                          ArquivoItemStatusEnum.Bloqueado);

                    linhaBloqueada = true; //SCF1303

                    processoInfo.StatusBloqueio = ProcessoStatusBloqueioEnum.HaBloqueios;
                    processoInfo.Atualizar(
                        PersistenciaHelper.Salvar<ProcessoInfo>(
                            codigoSessao, processoInfo));

                    quantidadeCriticas++;
                }
            }
        }
    }
}
