﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Contém funções para tratamento de calendário
    /// </summary>
    public class Calendario
    {
        /// <summary>
        /// Código da sessao a utilizar
        /// </summary>
        public string CodigoSessao { get; set; }

        /// <summary>
        /// Data inicial no qual serão carregados os dias não úteis
        /// </summary>
        public DateTime DataInicial { get; set; }

        /// <summary>
        /// Lista de dias não úteis
        /// </summary>
        public List<DateTime> DiasNaoUteis { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public Calendario(string codigoSessao, DateTime dataInicial)
        {
            // Inicializa
            this.CodigoSessao = codigoSessao;
            this.DataInicial = dataInicial;

            // Pede a lista de dias não úteis a partir da data informada
            this.DiasNaoUteis =
                Mensageria.Processar<ListarDiaNaoUtilResponse>(
                    new ListarDiaNaoUtilRequest() 
                    { 
                        CodigoSessao = this.CodigoSessao, 
                        FiltroAtivo = true, 
                        FiltroDataMaior = dataInicial 
                    }).Resultado.Select(d => d.DataDiaNaoUtil.Value.Date).ToList();
        }

        /// <summary>
        /// Faz o calculo de dia útil.
        /// Considera finais de semana e feriados como dias não uteis.
        /// Considera a data informada como data candidata de retorno, ou seja, se ela for um dia útil, retorna ela própria
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public DateTime ProximoDiaUtil(DateTime data)
        {
            // Repassa a chamada
            return this.ProximoDiaUtil(data, 0, true, false, true, false); //1495
        }

        /// <summary>
        /// Calcula o próximo dia útil
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public DateTime ProximoDiaUtil(DateTime data, int dias, bool considerarDataInformada, bool feriadoEhDiaUtil, bool fimDeSemanaEhDiaUtil, bool diasuteis) //1495
        {
            if (diasuteis) //1495 considerar DIAS UTEIS
            {
                // Flag indicando se a data é valida
                bool dataValida = false;

                // Se nao deve considerar a data informada, pega a próxima
                if (!considerarDataInformada)
                    data = data.AddDays(1);

                for (int i = 0; i < dias; i++)
                {
                    if (dias > 0)
                        data = data.AddDays(1);

                    dataValida = false;
                    // Procura por um dia útil
                    while (!dataValida)
                    {
                        // Inicialmente assume que a data é valida
                        dataValida = true;

                        // Remove o horário da data eleita
                        DateTime data2 = data.Date;

                        // Deve criticar fim de semana?
                        if (!fimDeSemanaEhDiaUtil && (data2.DayOfWeek == DayOfWeek.Saturday || data2.DayOfWeek == DayOfWeek.Sunday))
                            dataValida = false;

                        // Deve criticas feriado?
                        if (!feriadoEhDiaUtil && (this.DiasNaoUteis.Contains(data2)))
                            dataValida = false;

                        // Se não é a data, incrementa a data
                        if (!dataValida)
                            data = data.AddDays(1);
                    }
                }
            }
            else // if (diasuteis) //1495 considerar DIAS CORRIDOS
            {
                // Flag indicando se a data é valida
                data = data.AddDays(dias); // SOMA OS DIAS NA DATA

                // Procura por um dia útil
                bool dataValida = false;
                while (!dataValida)
                {
                    // Inicialmente assume que a data é valida
                    dataValida = true;

                    // Remove o horário da data eleita
                    DateTime data2 = data.Date;

                    // Deve criticar fim de semana?
                    if (!fimDeSemanaEhDiaUtil && (data2.DayOfWeek == DayOfWeek.Saturday || data2.DayOfWeek == DayOfWeek.Sunday))
                        dataValida = false;

                    // Deve criticas feriado?
                    if (!feriadoEhDiaUtil && (this.DiasNaoUteis.Contains(data2)))
                        dataValida = false;

                    // Se não é a data, incrementa a data
                    if (!dataValida)
                        data = data.AddDays(1);
                }
            }
            // Retorna a data encontrada
            return data;
        }
    }
}
