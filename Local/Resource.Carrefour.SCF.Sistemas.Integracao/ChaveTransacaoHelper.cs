﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    public static class ChaveTransacaoHelper
    {
        //=========================================================================================================
        // CHAVE CV 
        //=========================================================================================================
        //Campo CHAVE, CHAVE2, CHAVE3 TB_TRANSACAO
        //=========================================================================================================
        public static string GerarChaveComprovanteVenda(DateTime dataTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string numeroParcela)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + codigoEstabelecimento.PadLeft(6, '0') + "CV" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + numeroParcela.PadLeft(4, '0');
        }
        //=========================================================================================================
        //Campo CHAVE TB_ARQUIVO_ITEM
        //=========================================================================================================
        public static string GerarChaveComprovanteVendaArquivo(string dataTransacao, string horaTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string numeroParcela, string nSeq)
        {
            return dataTransacao.PadRight(8, ' ') + horaTransacao.PadRight(6, ' ') + codigoEstabelecimento.PadLeft(6, '0') + "CV" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + numeroParcela.PadLeft(4, '0') + nSeq.PadLeft(9, '0');
        }
        //=========================================================================================================
        /// CHAVE CP
        //=========================================================================================================
        //Campo CHAVE, CHAVE2, CHAVE3 TB_TRANSACAO
        //=========================================================================================================
        //1458 public static string GerarChaveComprovantePagamento(DateTime dataTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string nSeq)
        public static string GerarChaveComprovantePagamento(DateTime dataTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string sequencialMeioPagamento)
        {
            //1458 return dataTransacao.ToString("yyyyMMddHHmmss") + codigoEstabelecimento.PadLeft(6, '0') + "CP" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + nSeq.PadLeft(9, '0');
            return dataTransacao.ToString("yyyyMMddHHmmss") + codigoEstabelecimento.PadLeft(6, '0') + "CP" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + sequencialMeioPagamento.PadLeft(3, '0');
        }
        //=========================================================================================================
        //Campo CHAVE TB_ARQUIVO_ITEM
        //=========================================================================================================
        //1458 public static string GerarChaveComprovantePagamentoArquivo(string dataTransacao, string horaTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string nSeq)
        public static string GerarChaveComprovantePagamentoArquivo(string dataTransacao, string horaTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string sequencialMeioPagamento)
        {
            //1458 return dataTransacao.PadRight(8, ' ') + horaTransacao.PadRight(6, ' ')  + codigoEstabelecimento.PadLeft(6, '0') + "CP" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + nSeq.PadLeft(9, '0');
            return dataTransacao.PadRight(8, ' ') + horaTransacao.PadRight(6, ' ') + codigoEstabelecimento.PadLeft(6, '0') + "CP" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + sequencialMeioPagamento.PadLeft(3, '0');
        }
        //=========================================================================================================
        /// CHAVE AV
        //=========================================================================================================
        //Campo CHAVE, CHAVE2, CHAVE3 TB_TRANSACAO
        //=========================================================================================================
        //SCF1161
        //public static string GerarChaveAnulacaoVenda(DateTime dataTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string numeroParcela, string nSeq)
        //{
        //    return dataTransacao.ToString("yyyyMMddHHmmss") + codigoEstabelecimento.PadLeft(6, '0') + "AV" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + numeroParcela.PadLeft(4, '0') + nSeq.PadLeft(9, '0');
        //}
        public static string GerarChaveAnulacaoVenda(DateTime dataTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string numeroParcela)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + codigoEstabelecimento.PadLeft(6, '0') + "AV" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + numeroParcela.PadLeft(4, '0');
        }
        //=========================================================================================================
        //Campo CHAVE TB_ARQUIVO_ITEM
        //=========================================================================================================
        public static string GerarChaveAnulacaoVendaArquivo(string dataTransacao, string horaTransacao, string codigoEstabelecimento, string nsu, string codigoAutorizacao, string numeroParcela, string nSeq)
        {
            return dataTransacao.PadRight(8, ' ') + horaTransacao.PadRight(6, ' ') + codigoEstabelecimento.PadLeft(6, '0') + "AV" + nsu.PadLeft(12, '0') + codigoAutorizacao.PadLeft(12, '0') + numeroParcela.PadLeft(4, '0') + nSeq.PadLeft(9, '0') ;
        }
        //=========================================================================================================
        /// CHAVE AP
        //=========================================================================================================
        //Campo CHAVE, CHAVE2, CHAVE3 TB_TRANSACAO
        //=========================================================================================================
        public static string GerarChaveAnulacaoPagamento(DateTime dataTransacao, string codigoEstabelecimento, string nsu, string nSeq)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + codigoEstabelecimento.PadLeft(6, '0') + "AP" + nsu.PadLeft(12, '0') + nSeq.PadLeft(9, '0');
        }
        //=========================================================================================================
        //Campo CHAVE TB_ARQUIVO_ITEM
        //=========================================================================================================
        public static string GerarChaveAnulacaoPagamentoArquivo(string dataTransacao, string horaTransacao, string codigoEstabelecimento, string nsu, string nSeq)
        {
            return dataTransacao.PadRight(8, ' ') + horaTransacao.PadRight(6, ' ') + codigoEstabelecimento.PadLeft(6, '0') + "AP" + nsu.PadLeft(12, '0') + nSeq.PadLeft(9, '0');
        }
        //=========================================================================================================
        /// CHAVE AJ
        //=========================================================================================================
        //Campo CHAVE, CHAVE2, CHAVE3 TB_TRANSACAO - Parametro valor como numero
        //=========================================================================================================
        public static string GerarChaveAjuste(DateTime dataTransacao, string codigoEstabelecimento, double valor, string nsu)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + codigoEstabelecimento.PadLeft(6, '0') + "AJ" + valor.ToString() + nsu.PadLeft(12, '0');
        }
        //=========================================================================================================
        //Campo CHAVE, CHAVE2, CHAVE3 TB_TRANSACAO - Parametro valor como string
        //=========================================================================================================
        public static string GerarChaveAjuste(DateTime dataTransacao, string codigoEstabelecimento, string valor, string nsu)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + codigoEstabelecimento.PadLeft(6, '0') + "AJ" + valor + nsu.PadLeft(12, '0');
        }
        //=========================================================================================================
        //Campo CHAVE TB_ARQUIVO_ITEM - Parametro valor como string
        //=========================================================================================================
        public static string GerarChaveAjusteArquivo(string dataTransacao, string horaTransacao, string codigoEstabelecimento, string valor, string nsu)
        {
            return dataTransacao.PadRight(8, ' ') + horaTransacao.PadRight(6, ' ') + codigoEstabelecimento.PadLeft(6, '0') + "AJ" + valor.PadLeft(15, '0') + nsu.PadLeft(12, '0');
        }
    }
}
