﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Types;
using Resource.Framework.Library.Db;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    /// <summary>
    /// Classe base para uma regra
    /// </summary>
    public class RegraSCFBase
    {
        /// <summary>
        /// Construtor default
        /// </summary>
        public RegraSCFBase()
        {
            this.Escopo = RegraSCFEscopoEnum.Linha;
            //this.isHomologacaoTSYS =
            //    PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(null, null).HomologacaoTSYS;
        }

        /// <summary>
        /// Configuracao geral
        /// </summary>
        //public bool isHomologacaoTSYS { get; set; }

        /// <summary>
        /// Escopo da regra
        /// </summary>
        public RegraSCFEscopoEnum Escopo { get; set; }

        ///// <summary>
        ///// Sequencial do NSU a ser salvo
        ///// </summary>
        //public int SequencialNSU { get; set; }

        /// <summary>
        /// Inicio da validação
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public void IniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            // Repassa a chamada
            this.OnIniciarValidacaoArquivo(request);
        }

        /// <summary>
        /// Método virtual para início de validação
        /// </summary>
        /// <param name="criticas"></param>
        /// <param name="arquivo"></param>
        protected virtual void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }

        /// <summary>
        /// Inicia validação da linha
        /// </summary>
        public void IniciarValidacaoLinha(RegraSCFValidarLinhaRequest request)
        {
            // Repassa a chamada
            this.OnIniciarValidacaoLinha(request);
        }

        /// <summary>
        /// Método virtual para inicio de validação da linha
        /// </summary>
        protected virtual void OnIniciarValidacaoLinha(RegraSCFValidarLinhaRequest request)
        {
        }

        /// <summary>
        /// Realiza a validação
        /// </summary>
        public void ValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            // Repassa a chamada
            this.OnValidarLinha(request);
        }

        /// <summary>
        /// Método virtual para a validação
        /// </summary>
        protected virtual void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
        }

        /// <summary>
        /// Finaliza validação da linha
        /// </summary>
        public void FinalizarValidacaoLinha(RegraSCFValidarLinhaRequest request)
        {
            // Repassa a chamada
            this.OnFinalizarValidacaoLinha(request);
        }

        /// <summary>
        /// Método virtual para finalização de validação da linha
        /// </summary>
        protected virtual void OnFinalizarValidacaoLinha(RegraSCFValidarLinhaRequest request)
        {
        }

        /// <summary>
        /// Finaliza a validação do arquivo
        /// </summary>
        public void FinalizarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            // Repassa a chamada
            this.OnFinalizarValidacaoArquivo(request);
        }

        /// <summary>
        /// Método virtual para finalizar validação do arquivo
        /// </summary>
        protected virtual void OnFinalizarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }

        /// <summary>
        /// Corrige NSU HOST, NSU TEF e cupom fiscal
        /// </summary>
        /// <param name="request"></param>
        public string RegraSCFCorrecaoNSU(RegraSCFValidarLinhaRequest request, string dataMovimento, string idMovimento,  bool incrementarNSUHost, bool increntarNSUTef)
        {
            // Incrementa
            if(incrementarNSUHost)
                request.SequencialNSU += 1;

            if(increntarNSUTef)
                request.SequencialNSU += 1;

            //1351 - inicio
            if (idMovimento == "" || idMovimento == null)
                idMovimento = "999999";
            if (dataMovimento == "" || dataMovimento == null)
                dataMovimento = "99999999";
            if (request.SequencialNSU == 0) // || (request.SequencialNSU == null)) // warning 25/02/2019
                request.SequencialNSU = 1;
            //1351 - fim

            // Corrige
            string novoNSU = dataMovimento.Substring(2) + idMovimento.Substring(4,2) + request.SequencialNSU.ToString().PadLeft(4, '0');
            return novoNSU;
        }
    }
}