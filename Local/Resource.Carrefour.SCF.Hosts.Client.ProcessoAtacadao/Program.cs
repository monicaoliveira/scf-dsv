﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Hosts.Client.ProcessoAtacadao
{
    class Program
    {
        static void Main(string[] args)
        {
            // Inicializa framework
            inicializarFramework();

            // Autentica
            AutenticarUsuarioResponse respostaAutenticar =
                Mensageria.Processar<AutenticarUsuarioResponse>(
                    new AutenticarUsuarioRequest() 
                    { 
                        CodigoUsuario = "Admin"
                    });
            string codigoSessao = respostaAutenticar.Sessao.CodigoSessao;

            // Pede a execucao do processo
            Mensageria.Processar(
                new ExecutarProcessoAtacadaoRequest() 
                { 
                    CodigoSessao = codigoSessao,
                    ExecutarAssincrono = true
                });

        }

        private static void inicializarFramework()
        {
            // Config de mensageria
            GerenciadorConfig.SetarConfig(
                new MensageriaConfig() 
                { 
                    UtilizarWCF = true
                });

            // Localizador de tipos
            GerenciadorConfig.SetarConfig(
                new LocalizadorTiposConfig()
                {
                    ListaFixa = 
                        new List<LocalizadorTipoInfo>()
                        {
                            new LocalizadorTipoInfo()
                            {
                                IncluirNamespace = 
                                    "Resource.Framework.Contratos.Comum.Dados, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Framework.Contratos.Comum.Mensagens, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Mensagens, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Integracao.Dados, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Integracao.Mensagens, Resource.Carrefour.SCF.Contratos.Principal"
                            }
                        }
                });
        }
    }
}
