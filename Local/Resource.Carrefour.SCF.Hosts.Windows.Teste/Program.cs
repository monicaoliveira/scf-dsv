﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Sistemas.Comum;
using System.Reflection;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Sistemas.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using System.Data;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library.Integracao.Arquivos;
using System.IO;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using System.Globalization;
using Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Integracao;
using Resource.Carrefour.SCF.Sistemas.Integracao.Regras;

namespace Resource.Carrefour.SCF.Hosts.Windows.Teste
{
    class Program
    {
        static void Main(string[] args)
        {
            double valorDesconto = 0.26;
            int numeroTotalParcelas = 10;
            double descontoParcela = Math.Floor((valorDesconto / numeroTotalParcelas) * 100) / 100;
            double descontoTotalVenda = descontoParcela * numeroTotalParcelas;
            double diferenca = valorDesconto - descontoTotalVenda;
            double descontoUltimaParcela = descontoParcela + diferenca;

            string x = CriptografiaHelper.ReceberMD5Hash("123");

            Console.WriteLine(x);
            Console.Read();

            //return;

            inicializarFramework();

            string codigoSessao = 
                Mensageria.Processar<AutenticarUsuarioResponse>(
                    new AutenticarUsuarioRequest() 
                    { 
                        CodigoUsuario = "Admin",
                        Senha = "123"
                    }).Sessao.CodigoSessao;

            Console.WriteLine("Início");
            DateTime dtInicial = DateTime.Now;

            //testeCriticas(codigoSessao);
            //testeExportarCCI(codigoSessao);
            //testeSimularCCI(codigoSessao);
            //testeRegras(codigoSessao);
            //testeExtratoAgendar(codigoSessao);
            //testeExportarCSUSemAtacadao(codigoSessao);
            //testeExportacaoMateraContabil(codigoSessao);
            //testeExportacaoMatera(codigoSessao);
            //testeExportacaoMateraContabil(codigoSessao);
            //testeGeracaoArquivoMateraContabil(codigoSessao);
            //testeImportacaoArquivo(codigoSessao);
            //testeImportacaoArquivo2(codigoSessao)
            //testeImportacaoTransacao(codigoSessao);
            //testeImportacaoTransacao2(codigoSessao);
            //testeGeracaoArquivoMateraFinanceiro(codigoSessao);
            //testeProcesso(codigoSessao);
            //testeImportacaoArquivoCSU(codigoSessao);
            //testeImportacaoArquivoCCI(codigoSessao);
            //testeImportacaoTransacaoCSU(codigoSessao);
            //testeExportacaoMatera(codigoSessao);
            //testeResultadoConciliacao(codigoSessao);
            //testeExportacaoCSU(codigoSessao);

            //testeImportacaoArquivoCCI(codigoSessao);
            //testeAtualizarRetornoCessao(codigoSessao);
            //testeEstornarRetornoCessao(codigoSessao);
            //testeProcessoCessao(codigoSessao);

            //testeExportacaoMateraContabil(codigoSessao);

            
            DateTime dtFinal = DateTime.Now;
            TimeSpan diff = dtFinal - dtInicial;
            Console.WriteLine("Fim: " + diff.TotalSeconds.ToString());
            Console.ReadLine();
        }

        private static void testeCriticas(string codigoSessao)
        {
        }

        public static void testeImportacaoEstabelecimento(string codigoSessao)
        {
            Mensageria.Processar<ImportarEstabelecimentoResponse>(
                new ImportarEstabelecimentoRequest()
                {
                    CodigoSessao = codigoSessao,
                    CaminhoArquivo = @"C:\Temp\estabelecimento.txt",
                });
        }


        private static void testeExportarCSUSemAtacadao(string codigoSessao)
        {
            // Importa arquivo
            //ImportarArquivoResponse respostaImportar =
            //    Mensageria.Processar<ImportarArquivoResponse>(
            //        new ImportarArquivoRequest() 
            //        { 
            //             CodigoSessao = codigoSessao,
            //             CaminhoArquivo = @"C:\Projetos\Resource\Carrefour\SCF\Docs.Cliente\TESTES Dados\MACSU_CARREFOUR_250711.TXT",
            //             TipoArquivo = "ArquivoCSU"
            //        });

            // Solicita exportacao sem CSU
            ExportarCSUSemAtacadaoResponse respostaExportar =
                Mensageria.Processar<ExportarCSUSemAtacadaoResponse>(
                    new ExportarCSUSemAtacadaoRequest() 
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = "1",
                        CaminhoArquivo = @"C:\Projetos\Resource\Carrefour\SCF\Temp\csuSemAtacadao.txt"
                    });
        }

        private static void testeExtratoAgendar(string codigoSessao)
        {
            AgendarPagamentoExtratoResponse respostaAgendar =
                Mensageria.Processar<AgendarPagamentoExtratoResponse>(
                    new AgendarPagamentoExtratoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoProcesso = "3"
                    });
        }

        private static void testeRegras(string codigoSessao)
        {
            // Cria arquivo
            ArquivoTSYS arquivo = new ArquivoTSYS();

            // Cria conjunto de regras
            List<RegraSCFBase> regras = new List<RegraSCFBase>();
            //regras.Add(
            //    new RegraSCFDominio() 
            //    { 
            //        Escopo = RegraSCFEscopoEnum.Linha, 
            //        TipoLinha = "1",
            //        NomeCampo = "ORIGEM",
            //        Lista = "2"
            //    });

            //string tipoLinha = "A0";
            //BookItemCampoHelper campo = arquivo.BookInfo.ReceberDetalheCampo(tipoLinha + "/" + "NSEQ");

            //regras.Add(
            //    new RegraSCFString()
            //    {
            //        Escopo = RegraSCFEscopoEnum.Linha,
            //        TipoLinha = "L0",
            //        NomeCampo = "IdentificacaoMoeda",
            //        EhObrigatorio = true  
            //    });

            // Pede a validação
            //int criticas = arquivo.ValidarArquivo("39", "44", null);
        }

        private static void testeExportarCCI(string codigoSessao)
        {
            // Solicita exportacao Extrato para CCI
            ExportarExtratoParaGrupoResponse respostaExportar =
                Mensageria.Processar<ExportarExtratoParaGrupoResponse>(
                    new ExportarExtratoParaGrupoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = "65",
                        CaminhoArquivo = @"C:\Projetos\Resource\Carrefour\SCF\Arquivos\Extrato\Resultado"
                    });
        }


        private static void testeSimularCCI(string codigoSessao)
        {
            // Solicita exportacao Extrato para CCI
            SimularArquivoRetornoCessaoResponse respostaExportar =
                Mensageria.Processar<SimularArquivoRetornoCessaoResponse>(
                    new SimularArquivoRetornoCessaoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = "1",
                        CaminhoArquivo = @"C:\Projetos\Resource\Carrefour\SCF\Arquivos\Extrato\EnvioCCI"
                    });
        }


        private static void testeStream()
        {
            FileStream fileStream = File.Create(@"c:\temp\teste.txt");
            StreamWriter writer = new StreamWriter(fileStream);
            writer.WriteLine("teste");
            writer.WriteLine("teste");
            writer.WriteLine("teste");
            writer.WriteLine("teste");
            writer.Close();
            
        }

        private static void testeResultadoConciliacao(string codigoSessao)
        {
            ProcessoAtacadaoInfo processoAtacadaoInfo =
                (ProcessoAtacadaoInfo)
                    PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, "9");

            ServicoIntegracao service = new ServicoIntegracao();
            service.ExportarResultadoConciliacaoAtacadao(
                new ExportarResultadoConciliacaoRequest()
                {
                    CaminhoArquivoDestino = @"C:\Projetos\Resource\Carrefour\SCF\Arquivos\Atacadao\resultado\testeResultado.csv",
                    CodigoProcesso = "2"

                });
                 
        }

        private static void testeExportacaoMatera(string codigoSessao)
        {
            ExportarMateraFinanceiroResponse resposta = 
                Mensageria.Processar<ExportarMateraFinanceiroResponse>(
                    new ExportarMateraFinanceiroRequest() 
                    { 
                        CodigoSessao = codigoSessao,
                        CaminhoArquivoDestino = @"C:\Projetos\Resource\Carrefour\SCF\Temp\",
                        DataPagamento = new DateTime(2011, 07, 14)
                    });
        }

        //private static void testeExportacaoCSU(string codigoSessao)
        //{
        //    ExportarTransacoesCSUsemAtacadaoResponse resposta =
        //        Mensageria.Processar<ExportarTransacoesCSUsemAtacadaoResponse>(
        //            new ExportarTransacoesCSUsemAtacadaoRequest()
        //            {
        //                CodigoSessao = codigoSessao,
        //                CaminhoArquivoDestino = @"C:\Projetos\Resource\Carrefour\SCF\Temp\testeCSUSaida.txt",
        //                CodigoArquivo = "22"
        //            });
        //}

		private static void testeExportacaoMateraContabil(string codigoSessao)
        {
            ExportarMateraContabilResponse resposta =
                Mensageria.Processar<ExportarMateraContabilResponse>(
                    new ExportarMateraContabilRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CaminhoArquivoDestino = @"C:\Projetos\Resource\Carrefour\SCF\Temp\testeExportacaoMateraContabil2.txt",
                        CodigoArquivo = "39"
                    });
        }

        private static void testeAtualizarRetornoCessao(string codigoSessao)
        {
            AtualizarTransacoesRetornoCessaoResponse resposta =
                Mensageria.Processar<AtualizarTransacoesRetornoCessaoResponse>(
                    new AtualizarTransacoesRetornoCessaoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = "21",
                        CodigoProcesso = "21" 
                    });
        }

        private static void testeEstornarRetornoCessao(string codigoSessao)
        {
            EstornarLoteRetornoCessaoResponse resposta =
                Mensageria.Processar<EstornarLoteRetornoCessaoResponse>(
                    new EstornarLoteRetornoCessaoRequest()
                    {
                        CodigoArquivo = "7"
                    });
        }


 		private static void testeGeracaoArquivoMateraContabil(string codigoSessao)
        {
            ArquivoMateraContabil arquivoContabil = new ArquivoMateraContabil();
            string linha = InterpretadorArquivos.EscreverLinha(arquivoContabil.BookInfo, "detalhe", new Dictionary<BookItemCampoHelper, object>());
        }

        private static void testeGeracaoArquivoMateraFinanceiro(string codigoSessao)
        {
            ArquivoMateraFinanceiro arquivo = new ArquivoMateraFinanceiro();
            string linha = InterpretadorArquivos.EscreverLinha(arquivo.BookInfo, "header", new Dictionary<BookItemCampoHelper, object>());
        }

        private static void testeProcesso(string codigoSessao)
        {
            //ProcessoAtacadaoInfo processoAtacadaoInfo =
            //    (ProcessoAtacadaoInfo)
            //        PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, "9");

            ProcessoAtacadaoInfo processoAtacadaoInfo =
                new ProcessoAtacadaoInfo()
                {
                    CaminhoArquivoCSU = @"C:\Projetos\Resource\Carrefour\SCF\Docs.Cliente\TESTES Dados\MACSU_CARREFOUR_150411.TXT",
                    CaminhoArquivoMateraFinanceiro = @"C:\Projetos\Resource\Carrefour\SCF\Temp\testeExportacaoMatera.txt",
                    CaminhoArquivoSitef = @"C:\Projetos\Resource\Carrefour\SCF\Docs.Cliente\TESTES Dados\LAYOUT ATACADO_02_TXT.TXT",
                    CaminhoArquivoTSYS = @"C:\Projetos\Resource\Carrefour\SCF\Docs.Cliente\TESTES Dados\R06_CL7496_BR_FinancialReconciliationFile_001O_20101128_20101128_234550.txt"
                };

            GerenciadorProcesso.IniciarProcesso(codigoSessao, processoAtacadaoInfo);

            GerenciadorProcesso.Processar(codigoSessao, processoAtacadaoInfo);
        }

        private static void testeProcessoCessao(string codigoSessao)
        {
               ProcessoCessaoInfo processoCessaoInfo =
                new ProcessoCessaoInfo()
                {
                    CaminhoArquivoRetornoCessao = @"C:\Projetos\Resource\Carrefour\SCF\temp\testeRetornocessao.txt"
                };

            GerenciadorProcesso.IniciarProcesso(codigoSessao, processoCessaoInfo);

            GerenciadorProcesso.Processar(codigoSessao, processoCessaoInfo);
        }

        private static void testeImportacaoTransacao2(string codigoSessao)
        {
            ImportarTransacoesSitefResponse resposta =
                Mensageria.Processar<ImportarTransacoesSitefResponse>(
                    new ImportarTransacoesSitefRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = (47).ToString()
                    });
        }

        private static void testeImportacaoTransacao(string codigoSessao)
        {
            ImportarTransacoesTSYSResponse respostaTransacaoes =
                    Mensageria.Processar<ImportarTransacoesTSYSResponse>(
                        new ImportarTransacoesTSYSRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = "1",
                            CodigoProcesso = "1",
                            AlterarBancoAgenciaConta = false
                        });
        }

        private static void testeArquivo()
        {
            BookInfo book = new BookInfo();
            
            BookItemCampoInfo campoA0 = new BookItemCampoInfo("A0");
            campoA0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2));
            campoA0.Campos.Add(new BookItemCampoInfo("VersaoLayout", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("DataGeracaoArquivo", 8));
            campoA0.Campos.Add(new BookItemCampoInfo("HoraGeracaoArquivo", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("IdMovimento", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NomeAdministradora", 30));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoAutorizadora", 4));
            campoA0.Campos.Add(new BookItemCampoInfo("IdentificacaoDestinatario", 6));
            campoA0.Campos.Add(new BookItemCampoInfo("NSEQ", 9));
            book.Campos.Add(campoA0);

            BookItemCampoInfo campoL0 = new BookItemCampoInfo("L0");
            campoL0.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, campoL0));
            campoL0.Campos.Add(new BookItemCampoInfo("DataMovimento", 8, campoL0));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoMoeda", 2, campoL0));
            campoL0.Campos.Add(new BookItemCampoInfo("IdentificacaoTEF", 1, campoL0));
            campoL0.Campos.Add(new BookItemCampoInfo("NSEQ", 9, campoL0));
            book.Campos.Add(campoL0);

            BookItemCampoInfo campoCV = new BookItemCampoInfo("CV");
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoRegistro", 2, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("IdentificacaoLoja", 15, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUHostTransacao", 12, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("NSUTEFTransacao", 12, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoAutorizacao", 12, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("DataTransacao", 8, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("HorarioTransacao", 6, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoLancamento", 1, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("DataRepasseCredito", 8, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("TipoProduto", 1, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("MeioCaptura", 1, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoVenda", 16, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDesconto", 16, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoVenda", 16, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroCartao", 19, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroParcela", 3, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroTotalParcelas", 3, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorBrutoParcela", 16, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorDescontoParcela", 16, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("ValorLiquidoParcela", 16, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("Banco", 3, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("Agencia", 6, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("Conta", 11, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("NumeroContaCliente", 14, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("ProdutoCSF", 6, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("CodigoPlano", 5, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("CupomFiscal", 10, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("ModalidadeVenda", 1, campoCV));
            campoCV.Campos.Add(new BookItemCampoInfo("NSEQ", 9, campoCV));
            book.Campos.Add(campoCV);

            book.CalculaPosicaoTamanhoCampos();

            StreamReader sr = File.OpenText(@"C:\Projetos\Resource\Carrefour\SCF\Docs.Cliente\Reconciliation File - Carrefour\R06_CL7496_BR_FinancialReconciliationFile_001O_20101128_20101128_234550.txt");

            string a0 = sr.ReadLine();
            string a0teste = 
                (string)InterpretadorArquivos.ExtrairValor(book, @"A0/VersaoLayout", a0);

            string l0 = sr.ReadLine();
            string l0teste =
                (string)InterpretadorArquivos.ExtrairValor(book, @"L0/DataMovimento", l0);
        }

        private static void testeImportacaoTransacaoCSU(string codigoSessao)
        {
            ImportarTransacoesCSUResponse resposta =
                Mensageria.Processar<ImportarTransacoesCSUResponse>(
                    new ImportarTransacoesCSURequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = (61).ToString()
                    });
        }

        private static void testeImportacaoArquivoCSU(string codigoSessao)
        {
            Mensageria.Processar<ImportarArquivoResponse>(
                new ImportarArquivoRequest()
                {
                    CodigoSessao = codigoSessao,
                    CaminhoArquivo = @"C:\Projetos\Resource\Carrefour\SCF\Docs.Cliente\TESTES Dados\MACSU_CARREFOUR_150411.TXT",
                    TipoArquivo = "ArquivoCSU"
                });
        }

        private static void testeImportacaoArquivoCCI(string codigoSessao)
        {
            Mensageria.Processar<ImportarArquivoResponse>(
                new ImportarArquivoRequest()
                {
                    CodigoSessao = codigoSessao,
                    CaminhoArquivo = @"C:\Projetos\Resource\Carrefour\SCF\temp\testeRetornocessao.txt",
                    TipoArquivo = "ArquivoRetornoCessao"
                });
        }

        private static void testeImportacaoArquivoSitef(string codigoSessao)
        {
            Mensageria.Processar<ImportarArquivoResponse>(
                new ImportarArquivoRequest()
                {
                    CodigoSessao = codigoSessao,
                    CaminhoArquivo = @"C:\Projetos\Resource\Carrefour\SCF\Docs.Cliente\LAYOUT ATACADAO\LAYOUT ATACADO.csv",
                    TipoArquivo = "ArquivoSitef"
                });
        }

        private static void testeImportacaoArquivo(string codigoSessao)
        {
            Mensageria.Processar<ImportarArquivoResponse>(
                new ImportarArquivoRequest()
                {
                    CodigoSessao = codigoSessao,
                    CaminhoArquivo = @"C:\Projetos\Resource\Carrefour\SCF\Docs.Cliente\Reconciliation File - Carrefour\R06_CL7496_BR_FinancialReconciliationFile_001O_20101128_20101128_234550.txt",
                    TipoArquivo = "ArquivoTSYS"
                });
        }

        private static void testeOracle3()
        {
            // Pega o comando
            OracleCommand cm = 
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_S");

            // Atribui os valores
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = null;
            cm.Parameters[":pCODIGO_LAYOUT"].Value = null;
            cm.Parameters[":pNOME_ARQUIVO"].Value = "Arq2";
            cm.Parameters[":pSTATUS_ARQUIVO"].Value = "0";
            cm.Parameters[":retornarRegistro"].Value = "S";

            // Executa
            cm.ExecuteNonQuery();
        }

        private static void testeOracle2()
        {
            Procedures.PR_ARQUIVO_S(Procedures.ReceberConexao(), null, null, "Arq1", "A", true, null, null, null,null);
        }

        private static void testeOracle()
        {
            string connectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.0.101)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=Marc20100930));User Id=scf;Password=scf;";
            OracleConnection cn = new OracleConnection(connectionString);
            cn.Open();

            OracleCommand cm = new OracleCommand("select * from TB_TESTE", cn);
            OracleDataAdapter da = new OracleDataAdapter(cm);
            DataTable tb = new DataTable();
            da.Fill(tb);

        }

        private static void inicializarFramework()
        {
            // Configuracoes de persistencia
            GerenciadorConfig.SetarConfig(
                new ServicoPersistenciaConfig()
                {
                    Persistencias =
                        new List<PersistenciaInfo>()
                        {
                            new PersistenciaInfo() 
                            {
                                Default = true,
                                TipoPersistencia = typeof(PersistenciaDb)
                            }
                        }
                });

            // Configuracao de conexao com banco de dados
            GerenciadorConfig.SetarConfig(
                new OracleDbLibConfig() 
                    {
                        ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.0.101)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=marc20100930)));User Id=scf;Password=scf;",
                        ProceduresPackageName = "PKG_SCF"
                    });

            // Configuracoes de seguranca
            GerenciadorConfig.SetarConfig(
                new ServicoSegurancaConfig()
                {
                    ValidarSenhas = false,
                    NomeUsuarioAdministrador = "Admin",
                    NamespacesPermissoes =
                        new List<string>() 
                        {
                            "Resource.Framework.Contratos.Comum.Permissoes, Resource.Framework.Contratos.Comum"
                        }
                });

            // Configuracoes do resolutor de tipos
            GerenciadorConfig.SetarConfig(
                new ResolutorTiposConfig() 
                {
                    AprofundarNamespaces = true,
                    IncluirNamespaces = 
                        new List<string>() 
                        { 
                            "Resource.Carrefour.SCF.Sistemas.Principal.Processos, Resource.Carrefour.SCF.Sistemas.Principal",
                            "Resource.Carrefour.SCF.Contratos.Principal.Dados, Resource.Carrefour.SCF.Contratos.Principal",
                            "Resource.Carrefour.SCF.Contratos.Integracao.Dados, Resource.Carrefour.SCF.Contratos.Integracao"
                        }
                });

            // Inicia os servicos
            ContainerServicoHost containerServicoHost = ContainerServicoHost.GetInstance();
            containerServicoHost.Iniciar(
                new ContainerServicoHostConfig()
                {
                    ServicoHostConfig =
                        new ServicoHostConfig()
                        {
                            TipoVarreduraTipos = ServicoHostTipoVarreduraEnum.DefaultIncluir,
                            IncluirTipos =
                                new List<Type>() 
                                { 
                                    typeof(ServicoSeguranca),
                                    typeof(ServicoPersistencia)
                                },
                            IncluirAssemblies =
                                new List<Assembly>()
                                {
                                    typeof(ServicoPrincipal).Assembly,
                                    typeof(ServicoIntegracao).Assembly
                                }
                        }
                });

            // Pede inicializacao da seguranca
            Mensageria.Processar(new InicializarSegurancaRequest());
        }

        private static void testeAjusteProcessos()
        {

        }

    }
}
