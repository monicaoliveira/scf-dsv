﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    [Serializable]
    public class ServicoAgendadorConfig
    {        
        /// <summary>
        /// Usuario que vai rodar o serviço agendador.
        /// </summary>
        public string UsuarioRobo { get; set; }

        /// <summary>
        /// Senha do usuario.
        /// </summary>
        public string Senha { get; set; }
    }
}
