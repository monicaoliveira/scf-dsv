﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Configuracoes do processo atacadao, parte de localização e 
    /// movimentacao de arquivos
    /// </summary>
    public class ProcessoAtacadao2Config
    {
        /// <summary>
        /// Caminho do arquivo CSU
        /// </summary>
        public string CaminhoArquivoCSU { get; set; }

        /// <summary>
        /// Caminho do arquivo Sitef
        /// </summary>
        public string CaminhoArquivoSitef { get; set; }

        /// <summary>
        /// Caminho do arquivo TSYS
        /// </summary>
        public string CaminhoArquivoTSYS { get; set; }

        /// <summary>
        /// Diretorio onde será gerado o resultado do processo
        /// </summary>
        public string DiretorioResultadoProcesso { get; set; }

        /// <summary>
        /// Indica se o processo deve mover os arquivos de origem para dentro
        /// do diretório de resultado
        /// </summary>
        public bool MoverArquivosOrigem { get; set; }
    }
}
