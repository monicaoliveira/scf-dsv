﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Configuracoes do processo atacadao
    /// </summary>
    public class ProcessoAtacadaoConfig
    {
        /// <summary>
        /// Banco que deve ser inserido nas transacoes
        /// </summary>
        public string Banco { get; set; }

        /// <summary>
        /// Agencia que deve ser inserido nas transacoes
        /// </summary>
        public string Agencia { get; set; }

        /// <summary>
        /// Conta que deve ser inserido nas transacoes
        /// </summary>
        public string Conta { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ProcessoAtacadaoConfig()
        {
            this.Banco = "000";
            this.Agencia = "0000";
            this.Conta = "00000";
        }
    }
}
