﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// Jira: SCF - 1520
    [Serializable]
    public class ListarRelatorioInterchangeRequest : MensagemRequestBase
    {
        public DateTime FiltroDataMovimentoDe { get; set; }
        public DateTime FiltroDataMovimentoAte { get; set; }
    }
}
