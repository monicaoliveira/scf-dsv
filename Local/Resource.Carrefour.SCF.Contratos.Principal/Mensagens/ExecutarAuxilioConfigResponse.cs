﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de config
    /// </summary>
    public class ExecutarAuxilioConfigResponse : MensagemResponseBase
    {
        /// <summary>
        /// Tipo do config
        /// </summary>
        public string TipoConfig { get; set; }

        /// <summary>
        /// XML do Config
        /// </summary>
        public string ConfigXML { get; set; }
    }
}
