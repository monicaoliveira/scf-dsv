﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de subgrupos
    /// </summary>
    [Serializable]
    public class ListarTaxaRecebimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro pelo código do Grupo
        /// </summary>
        public string FiltroCodigoGrupo { get; set; }

        /// <summary>
        /// Filtro pelo código do Referencia
        /// </summary>
        public string FiltroCodigoReferencia { get; set; }

        /// <summary>
        /// Filtro pelo código do Meio de Pagamento
        /// </summary>
        public string FiltroCodigoMeioPagamento { get; set; }
        
        /// <summary>
        /// Filtro pela Data de Incio
        /// </summary>
        public DateTime? FiltroDataInicio { get; set; }

        /// <summary>
        /// Filtro pela Data de Fim
        /// </summary>
        public DateTime? FiltroDataFim { get; set; }

        /// <summary>
        /// Filtro diferente do Codigo da Taxa de Recebimento
        /// </summary>
        public string FiltroCodigoTaxaRecebimento { get; set; }
    }
}
