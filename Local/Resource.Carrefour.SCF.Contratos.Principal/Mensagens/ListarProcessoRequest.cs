﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de processos
    /// </summary>
    public class ListarProcessoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por data de inclusao inicial
        /// </summary>
        public DateTime? FiltroDataInclusao { get; set; }
        
        /// <summary>
        /// Filtro por data de inclusao inicial
        /// </summary>
        public DateTime? FiltroDataInclusaoInicial { get; set; }

        /// <summary>
        /// Filtro por data de inclusao final
        /// </summary>
        public DateTime? FiltroDataInclusaoFinal { get; set; }

        /// <summary>
        /// Filtro por Tipo de Processo
        /// </summary>
        public string FiltroTipoProcesso { get; set; }

        /// <summary>
        /// Filtro por status do processo igual
        /// </summary>
        public ProcessoStatusEnum? FiltroStatusProcesso { get; set; }

        /// <summary>
        /// Filtro por status do procesos diferente
        /// </summary>
        public ProcessoStatusEnum? FiltroStatusProcessoDiferente { get; set; }

        /// <summary>
        /// Filtro por status de bloqueio
        /// </summary>
        public ProcessoStatusBloqueioEnum? FiltroStatusBloqueio { get; set; }
    }
}
