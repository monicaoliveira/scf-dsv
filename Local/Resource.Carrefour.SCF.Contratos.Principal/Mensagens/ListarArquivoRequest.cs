﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de arquivos
    /// </summary>
    [Serializable]
    public class ListarArquivoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro pelo nome do arquivo
        /// </summary>
        public string FiltroNomeArquivo { get; set; }

        /// <summary>
        /// Filtro pelo tipo do arquivo
        /// </summary>
        public string FiltroTipoArquivo { get; set; }
        
        /// <summary>
        /// Filtro por data de inclusao igual
        /// </summary>
        public DateTime? FiltroDataInclusao { get; set; }

        /// <summary>
        /// Filtro por status do arquivo
        /// </summary>
        public ArquivoStatusEnum? FiltroStatusArquivo { get; set; }

        /// <summary>
        /// Filtro pelo código do processo relacionado
        /// </summary>
        public string FiltroCodigoProcesso { get; set; }

        /// <summary>
        /// Filtra os arquivos pendentes
        /// </summary>
        public bool FiltroArquivosPendentes { get; set; }
    }
}
