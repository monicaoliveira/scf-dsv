﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de planos
    /// </summary>
    [Serializable]
    public class GerarRelatorioTransacaoExcelRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por status
        /// </summary>
        public ObjetoJsonRelTransacoesInfo Param { get; set; }

        /// <summary>
        /// Opcionalmente pode-se passar o código do processo relacionado
        /// </summary>
        public string CodigoProcesso { get; set; }

    }
}
