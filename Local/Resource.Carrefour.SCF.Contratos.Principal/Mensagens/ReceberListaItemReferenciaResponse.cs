﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta do detalhe da referência do domínio
    /// </summary>
    [Serializable]
    public class ReceberListaItemReferenciaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado da consulta
        /// </summary>
        public ListaItemReferenciaInfo ListaItemReferenciaInfo { get; set; }
    }
}
