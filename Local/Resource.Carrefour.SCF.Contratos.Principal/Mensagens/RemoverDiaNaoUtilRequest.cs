﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de dias não úteis
    /// </summary>
    [Serializable]
    public class RemoverDiaNaoUtilRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de dias nao úteis a ser removido
        /// </summary>
        public string CodigoDiaNaoUtil { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
