﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de remoção de uma Taxa de Recebimento
    /// </summary>
    [Serializable]
    public class RemoverTaxaRecebimentoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código da Taxa de Recebimento removido
        /// </summary>
        public string CodigoTaxaRecebimento { get; set; }
    }
}
