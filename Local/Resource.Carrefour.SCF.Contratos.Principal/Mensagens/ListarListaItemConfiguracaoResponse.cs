﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class ListarListaItemConfiguracaoResponse : MensagemResponseBase
    {

        public List<ListaConfiguracaoItemInfo> Resultado { get; set; }
    }
}
