﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de Grupo
    /// </summary>
    [Serializable]
    public class ReceberEmpresaGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do Grupo solicitado
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }
    }
}
