﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    [Serializable]
    public class SalvarProcessoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Processo salvo
        /// </summary>
        public ProcessoInfo ProcessoInfo { get; set; }
    }
}
