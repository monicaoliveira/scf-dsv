﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de lista item
    /// </summary>
    [Serializable]
    public class RemoverListaItemResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código da lista item a ser removido
        /// </summary>
        public string CodigoListaItemRemovido { get; set; }
    }
}
