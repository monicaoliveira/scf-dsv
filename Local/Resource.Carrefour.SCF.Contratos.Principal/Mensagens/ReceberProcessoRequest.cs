﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    [Serializable]
    public class ReceberProcessoRequest : MensagemRequestBase
    {

        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
