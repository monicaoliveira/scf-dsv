﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Grupos
    /// </summary>
    [Serializable]
    public class ListarEmpresaGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por código do grupo
        /// </summary>
        public string FiltroCodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Filtro por nome do grupo
        /// </summary>
        public string FiltroNomeEmpresaGrupo { get; set; }

        //MaxLinhas
        public int? FiltroMaxLinhas { get; set; }

    }
}
