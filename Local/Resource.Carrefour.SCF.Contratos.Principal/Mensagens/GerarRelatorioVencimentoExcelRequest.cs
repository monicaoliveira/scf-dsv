﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de planos
    /// </summary>
    [Serializable]
    public class GerarRelatorioVencimentoExcelRequest : MensagemRequestBase
    {
        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public string TipoArquivo;

        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public string Link;

        /// <summary>
        /// Filtro por status
        /// </summary>
        public ListarRelatorioVencimentoRequest Request { get; set; }

        /// <summary>
        /// Opcionalmente pode-se passar o código do processo relacionado
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
