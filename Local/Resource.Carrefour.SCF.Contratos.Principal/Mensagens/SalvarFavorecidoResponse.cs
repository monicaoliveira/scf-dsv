﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de salvar Estabelecimento
    /// </summary>
    [Serializable]
    public class SalvarFavorecidoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Estabelecimento salvo
        /// </summary>
        public FavorecidoInfo FavorecidoInfo { get; set; }
    }
}
