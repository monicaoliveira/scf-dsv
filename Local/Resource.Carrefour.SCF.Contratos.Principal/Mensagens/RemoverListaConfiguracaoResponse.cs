﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de remocao de uma configuração de uma lista fixa
    /// </summary>
    public class RemoverListaConfiguracaoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Codigo do objeto removido
        /// </summary>
        public string CodigoListaConfiguracaoRemovido { get; set; }
    }
}
