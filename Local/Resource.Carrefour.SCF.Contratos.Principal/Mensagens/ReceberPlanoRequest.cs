﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de planos
    /// </summary>
    [Serializable]
    public class ReceberPlanoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do dia útil solicitado
        /// </summary>
        public string CodigoPlano { get; set; }
    }
}
