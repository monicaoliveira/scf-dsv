﻿//<%--Jira 963 - Marcos Matsuoka--%>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de Tipo de Estorno
    /// </summary>
    [Serializable]
    public class ListarTipoEstornoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<ListaItemInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarTipoEstornoResponse()
        {
            this.Resultado = new List<ListaItemInfo>();
        }
    }
}
