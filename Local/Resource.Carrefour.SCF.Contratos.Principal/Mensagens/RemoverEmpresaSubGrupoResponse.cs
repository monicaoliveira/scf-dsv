﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de remoção de um subgrupo
    /// </summary>
    [Serializable]
    public class RemoverEmpresaSubGrupoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do subgrupo removido
        /// </summary>
        public string CodigoEmpresaSubGrupo { get; set; }
    }
}
