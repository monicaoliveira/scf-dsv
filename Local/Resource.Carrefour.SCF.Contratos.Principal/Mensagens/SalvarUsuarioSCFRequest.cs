﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar usuário
    /// </summary>
    [Serializable]
    public class SalvarUsuarioSCFRequest : MensagemRequestBase
    {
        /// <summary>
        /// Usuário a ser salvo
        /// </summary>
        public UsuarioSCFInfo UsuarioSCF { get; set; }

        /// <summary>
        /// Indicador de novo usuario
        /// </summary>
        public bool Novo { get; set; }
    }
}
