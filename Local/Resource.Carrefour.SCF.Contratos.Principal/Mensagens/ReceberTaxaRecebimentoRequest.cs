﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de detalhe da Taxa de Recebimento
    /// </summary>
    [Serializable]
    public class ReceberTaxaRecebimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código da Taxa de Recebimento
        /// </summary>
        public string CodigoTaxaRecebimento{ get; set; }
    }
}
