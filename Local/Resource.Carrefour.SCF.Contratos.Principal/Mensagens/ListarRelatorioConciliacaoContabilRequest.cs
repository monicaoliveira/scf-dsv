﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mesagem de solicitação do relatório de conciliação contábil
    /// </summary>
    [Serializable]
    public class ListarRelatorioConciliacaoContabilRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro de data incial do relatório
        /// </summary>
        public DateTime FiltroDataInicioConciliacao { get; set; }

        /// <summary>
        /// Filtro de data final do relatório
        /// </summary>
        public DateTime FiltroDataFimConciliacao { get; set; }
    }
}
