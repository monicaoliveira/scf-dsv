﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar ListaItem
    /// </summary>
    [Serializable]
    public class SalvarListaItemRequest : MensagemRequestBase
    {
        /// <summary>
        /// ListaItem
        /// </summary>
        public ListaItemInfo ListaItemInfo { get; set; }
    }
}
