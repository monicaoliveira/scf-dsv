﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de execução do processo atacadão 
    /// </summary>
    public class ExecutarProcessoAtacadao2Response : MensagemResponseBase
    {
        /// <summary>
        /// Código do processo gerado
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
