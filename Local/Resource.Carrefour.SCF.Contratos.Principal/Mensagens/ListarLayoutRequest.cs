﻿using Resource.Framework.Library.Servicos.Mensagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Layouts
    /// </summary>
    [Serializable]
    public class ListarLayoutRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro para limitar a quantidade de registro retornada
        /// </summary>
        public int? FiltroMaxLinhas { get; set; }
    }
}
