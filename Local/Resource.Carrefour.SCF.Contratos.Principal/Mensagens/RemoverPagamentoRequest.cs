﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de Estabelecimento
    /// </summary>
    [Serializable]
    public class RemoverPagamentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do Pagamento a ser removido
        /// </summary>
        public string CodigoPagamento { get; set; }

        /// <summary>
        /// Remoção Lógica?
        /// </summary>
        public bool RemocaoLogica { get; set; }

        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
