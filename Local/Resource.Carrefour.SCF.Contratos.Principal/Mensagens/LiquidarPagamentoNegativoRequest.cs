﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar dias não úteis
    /// </summary>
    [Serializable]
    public class LiquidarPagamentoNegativoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Pagamento negativo a ser salvo
        /// </summary>
        public string codigoPagamento{ get; set; }

        /// <summary>
        /// Data de liquidacao salvo
        /// </summary>
        public DateTime dataLiquidacao { get; set; }


    }
}
