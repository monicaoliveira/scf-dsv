﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Estabelecimentos
    /// </summary>
    [Serializable]
    public class ListarRelatorioConsolidadoRequest : MensagemRequestBase
    {    
 
        //
        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public string TipoArquivo;

        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public string Link;
        //

        /// <summary>
        /// Filtro por VerificarQuantidadeLinhas
        /// </summary>
        public bool? VerificarQuantidadeLinhas;

        /// <summary>
        /// Filtro por RetornarRelatorio
        /// </summary>
        public bool RetornarRelatorio;

        /// <summary>
        /// Filtro por CodLog
        /// </summary>
        public string CodLog;

        /// <summary>
        /// Filtro por TipoRelatorio
        /// </summary>
        public string TipoRelatorio;

        /// <summary>
        /// Filtro por FiltroTipoConsolidado
        /// </summary>
        public string FiltroTipoConsolidado;

        /// <summary>
        /// Filtro por FiltroQuebraFavorecido
        /// </summary>
        public bool FiltroQuebraFavorecido;

        /// <summary>
        /// Filtro por FiltroEmpresaGrupo
        /// </summary>
        public string FiltroEstabelecimento;

        /// <summary>
        /// Filtro por FiltroDescricaoEstabelecimento
        /// </summary>
        public string FiltroDescricaoEstabelecimento;

        /// <summary>
        /// Filtro por FiltroBanco
        /// </summary>
        public string FiltroProduto;

        /// <summary>
        /// Filtro por FiltroCodigoTipoRegistro
        /// </summary>
        public string FiltroCodigoTipoRegistro;

        /// <summary>
        /// Filtro por FiltroDescricaoTipoRegistro
        /// </summary>
        public string FiltroDescricaoTipoRegistro;

        /// <summary>
        /// Filtro por FiltroFavorecidoOriginal
        /// </summary>
        public string FiltroFavorecidoOriginal;

        /// <summary>
        /// Filtro por FiltroMeioPagamento
        /// </summary>
        public string FiltroMeioPagamento;

        /// <summary>
        /// Filtro por MeioPagamento
        /// </summary>
        public string MeioPagamento;

        /// <summary>
        /// Filtro por LabelMeioPagamento
        /// </summary>
        public string LabelMeioPagamento;

        /// <summary>
        /// Filtro por FiltroLayoutVenda
        /// </summary>
        public string FiltroLayoutVenda;

        /// <summary>
        /// Filtro por FiltroLayoutRecebimento
        /// </summary>
        public string FiltroLayoutRecebimento;

        /// <summary>
        /// Filtro por FiltroQtdeVenda
        /// </summary>
        public bool FiltroQtdeVenda;

        /// <summary>
        /// Filtro por FiltroQtdeRecebimento
        /// </summary>
        public bool FiltroQtdeRecebimento;

        /// <summary>
        /// Filtro por FiltroDataInicioMovimento
        /// </summary>
        public Nullable<DateTime> FiltroDataInicioMovimento;

        /// <summary>
        /// Filtro por FiltroDataFimMovimento
        /// </summary>
        public Nullable<DateTime> FiltroDataFimMovimento;

        /// <summary>
        /// Filtro Data do agendamento De
        /// </summary>
        public DateTime? FiltroDataAgendamentoDe;

        /// <summary>
        /// Filtro Data do agendamento Até
        /// </summary>
        public DateTime? FiltroDataAgendamentoAte;

        /// <summary>
        /// Filtro por codigo de status CCI
        /// </summary>
        public string FiltroCodigoStatusCCI { get; set; }

        /// <summary>
        /// Filtro por tipo financeiro ou contábil
        /// ECOMMERCE - Fernando Bove - 20160105
        /// </summary>
        public string FiltroFinanceiroContabil { get; set; }

        /// <summary>
        /// Filtro por referencia
        /// </summary>
        public string FiltroReferencia { get; set; }

        /// <summary>
        /// Filtro para quebra por referencia
        /// </summary>
        public bool FiltroQuebraReferencia { get; set; }
    }
}
