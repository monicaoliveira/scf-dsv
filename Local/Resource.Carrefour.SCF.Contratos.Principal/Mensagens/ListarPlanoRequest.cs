﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de planos
    /// </summary>
    [Serializable]
    public class ListarPlanoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por status
        /// </summary>
        public bool FiltroAtivo { get; set; }

        /// <summary>
        /// Filtro por codigo TSYS
        /// </summary>
        public string FiltroCodigoPlanoTSYS { get; set; }

        /// <summary>
        /// Filtro por produto
        /// </summary>
        public string FiltroCodigoProduto { get; set; }
    }
}
