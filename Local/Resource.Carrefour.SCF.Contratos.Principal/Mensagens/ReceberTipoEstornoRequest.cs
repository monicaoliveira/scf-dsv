﻿//<%--Jira 963 - Marcos Matsuoka--%>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de Estabelecimento
    /// </summary>
    [Serializable]
    public class ReceberTipoEstornoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do Estabelecimento solicitado
        /// </summary>
        public string TipoEstorno { get; set; }
    }
}
