﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de agendamento de pagamentos seguindo as
    /// regras do processo atacadão
    /// </summary>
    public class AgendarPagamentoAtacadaoResponse : MensagemResponseBase
    {
    }
}
