﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de Grupos
    /// </summary>
    [Serializable]
    public class ListarEmpresaGrupoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<EmpresaGrupoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarEmpresaGrupoResponse()
        {
            this.Resultado = new List<EmpresaGrupoInfo>();
        }
    }
}
