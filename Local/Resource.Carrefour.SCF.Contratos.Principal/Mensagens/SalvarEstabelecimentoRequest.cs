﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar Estabelecimento
    /// </summary>
    [Serializable]
    public class SalvarEstabelecimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Estabelecimento a ser salva
        /// </summary>
        public EstabelecimentoInfo EstabelecimentoInfo { get; set; }
    }
}
