﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de salvar Produto
    /// </summary>
    [Serializable]
    public class SalvarProdutoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Produto salvo
        /// </summary>
        public ProdutoInfo ProdutoInfo { get; set; }
    }
}
