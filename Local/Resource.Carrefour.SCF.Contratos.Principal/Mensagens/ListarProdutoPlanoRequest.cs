﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class ListarProdutoPlanoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtra pelo código do plano
        /// </summary>
        public string FiltroCodigoPlano { get; set; }
    }
}
