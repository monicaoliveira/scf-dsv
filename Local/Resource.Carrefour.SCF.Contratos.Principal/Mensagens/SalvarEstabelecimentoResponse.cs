﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de salvar Estabelecimento
    /// </summary>
    [Serializable]
    public class SalvarEstabelecimentoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Estabelecimento salvo
        /// </summary>
        public EstabelecimentoInfo EstabelecimentoInfo { get; set; }
    }
}
