﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Estabelecimentos
    /// </summary>
    [Serializable]
    public class ListarRelatorioConferenciaVendasRequest : MensagemRequestBase
    {

        /// <summary>
        /// Filtro por Grupo
        /// </summary>
        public bool VerificarQuantidadeLinhas { get; set; }

        /// <summary>
        /// Filtro por Grupo
        /// </summary>
        public string FiltroEmpresaGrupo { get; set; }

        /// <summary>
        /// Filtro por FiltroEmpresaSubgrupo
        /// </summary>
        public string FiltroEmpresaSubgrupo { get; set; }

        /// <summary>
        /// Filtro por FiltroFavorecido
        /// </summary>
        public string FiltroFavorecido { get; set; }

        /// <summary>
        /// Filtro por FiltroEstabelecimento
        /// </summary>
        public string FiltroEstabelecimento { get; set; }

        /// <summary>
        /// Filtro por Referencia
        /// </summary>
        public string FiltroReferencia { get; set; }

        /// <summary>
        /// Filtro por FiltroDataInicioVendas
        /// </summary>
        public Nullable<DateTime> FiltroDataInicioVendas { get; set; }
        
        /// <summary>
        /// Filtro por FiltroDataFimVendas
        /// </summary>
        public Nullable<DateTime> FiltroDataFimVendas { get; set; }

        /// <summary>
        /// Filtro por FiltroDataFimVendas
        /// </summary>
        public Nullable<DateTime> FiltroDataVendas { get; set; }

        /// <summary>
        /// Filtro de inicio de Data de Processamento
        /// </summary>
        public Nullable<DateTime> FiltroDataMovimentoInicio { get; set; }

        /// <summary>
        /// Filtro de fim de Data de Processamento
        /// </summary>
        public Nullable<DateTime> FiltroDataMovimentoFim { get; set; }

        /// <summary>
        /// Filtro de Data de Processamento
        /// </summary>
        public Nullable<DateTime> FiltroDataMovimento { get; set; }

        /// <summary>
        /// Filtro de inicio de Data de Processamento
        /// </summary>
        public Nullable<DateTime> FiltrotxtDataRetornoCCIInicio { get; set; }

        /// <summary>
        /// Filtro de fim de Data de Processamento
        /// </summary>
        public Nullable<DateTime> FiltrotxtDataRetornoCCIFim { get; set; }

        /// <summary>
        /// Consolidação por Grupo, SubGrupo e Produto
        /// </summary>
        public bool FiltroConsolidaGrupoSubGrupoProduto { get; set; }

        /// <summary>
        /// Consolidação por Data da Venda
        /// </summary>
        public bool FiltroConsolidaDataVenda { get; set; }

        /// <summary>
        /// Consolidação por Data de processamento
        /// </summary>
        public bool FiltroConsolidaDataProcessamento { get; set; }

        /// <summary>
        /// Consolidação por banco, agência e conta corrente
        /// </summary>
        public bool FiltroConsolidaBancoAgenciaConta { get; set; }

        /// <summary>
        /// Consolidação por estabelecimento
        /// </summary>
        public bool FiltroConsolidaEstabelecimento { get; set; }

        /// <summary>
        /// Consolidação por referencia
        /// </summary>
        public bool FiltroConsolidaReferencia{ get; set; }

        /// <summary>
        /// Filtro por tipo de registro
        /// </summary>
        public string FiltroTipoRegistro { get; set; }

        /// <summary>
        /// Filtro por tipo de registro
        /// </summary>
        public string TipoArquivo { get; set; }

        /// <summary>
        /// Filtro por tipo financeiro ou contábil
        /// ECOMMERCE - Fernando Bove - 20160105
        /// </summary>
        public string FiltroFinanceiroContabil { get; set; }

        /// <summary>
        /// Filtro por tipo de registro
        /// </summary>
        public string Link { get; set; }

    }
}
