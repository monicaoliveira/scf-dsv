﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class ListarListaItemConfiguracaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro pelo código da lista
        /// </summary>
        public string FiltroCodigoLista { get; set; }

        // <summary>
        /// Filtro pelo Grupo
        /// </summary>
        public string FiltroCodigoEmpresaGrupo { get; set; }

    }
}
