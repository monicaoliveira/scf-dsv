﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar dias não úteis
    /// </summary>
    [Serializable]
    public class SalvarDiaNaoUtilRequest : MensagemRequestBase
    {
        /// <summary>
        /// Dia não útil a ser salvo
        /// </summary>
        public DiaNaoUtilInfo DiaNaoUtilInfo { get; set; }
    }
}
