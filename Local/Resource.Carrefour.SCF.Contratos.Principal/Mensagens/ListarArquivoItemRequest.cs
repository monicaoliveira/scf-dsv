﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{

    [Serializable]
    public class ListarArquivoItemRequest : MensagemRequestBase
    {
        public string FiltroCodigoProcesso { get; set; }
        public int?  FiltroMaxLinhas { get; set;}
        public ArquivoItemStatusEnum? FiltroStatusArquivoItem { get; set; }
        public bool FiltroIgnorarConteudo { get; set; }
        public bool FiltroSomenteBloqueados { get; set; }
        public DateTime? FiltroDataInclusaoInicial { get; set; } //1308
        public DateTime? FiltroDataInclusaoFinal { get; set; } //1308
    }
}
