﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de criação de processo de extrato
    /// </summary>
    public class ExecutarProcessoExpurgoRequest : MensagemRequestBase
    {

        public string CodigoProcesso { get; set; }

        public bool ExecutarAgora { get; set; }

        /// <summary>
        /// Indica se deve executar sincrono ou assincrono
        /// </summary>
        public bool ExecutarAssincrono { get; set; }

    }
}
