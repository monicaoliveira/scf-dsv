﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de requisição da lista de configuração
    /// </summary>
    public class ListarListaConfiguracaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código da entidade
        /// </summary>
        public string FiltroCodigoListaConfiguracao { get; set; }

        /// <summary>
        /// Código da entidade
        /// </summary>
        public string FiltroCodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Código da lista Item
        /// </summary>
        public string FiltroCodigoListaItem { get; set; }

        /// <summary>
        /// Codigo da lista
        /// </summary>
        public string FiltroCodigoLista { get; set; }
    }
}
