﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de execução de query
    /// </summary>
    public class ExecutarAuxilioQueryRequest : MensagemRequestBase
    {
        /// <summary>
        /// Query a ser executada
        /// </summary>
        public string Query { get; set; }
    }
}
