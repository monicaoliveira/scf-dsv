﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de configuracao de tipos de registro
    /// </summary>
    [Serializable]
    public class ReceberConfiguracaoTipoRegistroRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo da configutacao do tipo de registro
        /// </summary>
        public string CodigoConfiguracaoTipoRegistro { get; set; }
    }
}
