﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar conta
    /// </summary>
    [Serializable]
    public class SubstituirContaFavorecidoRequest : MensagemRequestBase
    {
        // SCF1701 - ID 27
        public string CodigoFavorecido { get; set; }
        public string CodigoContaFavorecido { get; set; }
        public string CodigoContaFavorecidoNovo { get; set; }
    }
}
