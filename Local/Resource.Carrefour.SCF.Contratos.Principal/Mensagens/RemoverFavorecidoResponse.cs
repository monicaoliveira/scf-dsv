﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de estabelecimento
    /// </summary>
    [Serializable]
    public class RemoverFavorecidoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do estabelecimento a ser removido
        /// </summary>
        public string CodigoFavorecidoRemovido { get; set; }
    }
}
