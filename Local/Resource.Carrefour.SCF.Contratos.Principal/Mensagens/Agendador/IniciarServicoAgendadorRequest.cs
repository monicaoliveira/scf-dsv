﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador
{
    public class IniciarServicoAgendadorRequest : MensagemRequestBase
    {
        public string OrigemChamada { get; set; } //1430 identificar de ONDE esta chamando o INICIAR SERVICO CAPTURA
        public string CodigoProcesso { get; set; }
        public int IntervaloExecucao { get; set; }
        public IniciarServicoAgendadorRequest()
        {
            this.IntervaloExecucao = 15;
        }
        
    }
}
