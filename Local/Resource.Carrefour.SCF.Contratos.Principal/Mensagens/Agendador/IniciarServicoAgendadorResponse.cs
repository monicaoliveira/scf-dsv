﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador
{
    /// <summary>
    /// Mensagem de resposta à solicitacao de início do serviço que vai ficar monitorando o diretorio e iniciar o processo
    /// Atacadao automaticamente
    /// </summary>
    public class IniciarServicoAgendadorResponse : MensagemResponseBase
    {
        /// <summary>
        /// Informacoes sobre o servico
        /// </summary>
        public ServicoAgendadorStatusEnum StatusServicoAgendadorInfo { get; set; }

        
    }
}
