﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de Grupo
    /// </summary>
    [Serializable]
    public class ReceberRelatorioVencimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro grupo
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Filtro banco
        /// </summary>
        public string CodigoFavorecido { get; set; }

        /// <summary>
        /// Filtro produto
        /// </summary>
        public string CodigoProduto { get; set; }

        /// <summary>
        /// Filtro periodo inicio
        /// </summary>
        public DateTime DataInicio { get; set; }

        /// <summary>
        /// Filtro periodo fim
        /// </summary>
        public DateTime DataFim { get; set; }
    }
}
