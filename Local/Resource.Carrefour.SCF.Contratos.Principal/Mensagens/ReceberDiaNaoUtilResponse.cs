﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de detalhe de dias não úteis
    /// </summary>
    [Serializable]
    public class ReceberDiaNaoUtilResponse : MensagemResponseBase
    {
        /// <summary>
        /// Detalhe do dia não útil
        /// </summary>
        public DiaNaoUtilInfo DiaNaoUtilInfo { get; set; }
    }
}
