﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de exportacao de arquivo matera financeiro
    /// </summary>
    public class ProcessarExclusaoPagamentosResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do processo gerado para o processamento assincrono da Exclusao
        /// </summary>
        public string CodigoProcesso { get; set; }

    }
}
