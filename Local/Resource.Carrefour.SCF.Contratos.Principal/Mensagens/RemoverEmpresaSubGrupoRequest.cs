﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação remoção de um subgrupo
    /// </summary>
    [Serializable]
    public class RemoverEmpresaSubGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do subgrupo
        /// </summary>
        public string CodigoEmpresaSubGrupo { get; set; }
    }
}
