﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{   [Serializable]
    public class RemoverContaFavorecidoProdutoRequest : MensagemRequestBase
    {   public string CodigoContaFavorecido { get; set; }
        public string CodigoProduto { get; set; }
        public string Referencia { get; set; } //1353
    }
}