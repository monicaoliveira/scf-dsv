﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitacao de início do serviço que vai ficar monitorando o diretorio e iniciar o processo
    /// Atacadao automaticamente
    /// </summary>
    public class ReceberStatusServicoExpurgoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Informacoes sobre o servico
        /// </summary>
        public ServicoExpurgoInfo ServicoExpurgoInfo { get; set; }
    }
}
