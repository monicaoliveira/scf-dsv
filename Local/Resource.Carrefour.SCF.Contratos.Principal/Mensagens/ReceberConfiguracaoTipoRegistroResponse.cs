﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de detalhe de dias não úteis
    /// </summary>
    [Serializable]
    public class ReceberConfiguracaoTipoRegistroResponse : MensagemResponseBase
    {
        /// <summary>
        /// Detalhe da configuracao do tipo de registro
        /// </summary>
        public ConfiguracaoTipoRegistroInfo ConfiguracaoTipoRegistroInfo { get; set; }
    }
}
