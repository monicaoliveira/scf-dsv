﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de config
    /// </summary>
    public class ExecutarAuxilioConfigRequest : MensagemRequestBase
    {
        /// <summary>
        /// Tipo do config
        /// </summary>
        public string TipoConfig { get; set; }
    }
}
