﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// Jira: SCF - 1520
    [Serializable]
    public class GerarRelatorioInterchangeExcelRequest : MensagemRequestBase
    {
        public string CodigoProcesso { get; set; }

        public DateTime FiltroDataMovimentoDe { get; set; }

        public DateTime FiltroDataMovimentoAte { get; set; }
    }
}
