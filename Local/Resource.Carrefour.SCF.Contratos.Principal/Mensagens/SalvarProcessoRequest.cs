﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    [Serializable]
    public class SalvarProcessoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Processo a ser Salvo
        /// </summary>
        public ProcessoInfo ProcessoInfo { get; set; }
    }
}
