﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de subgrupos
    /// </summary>
    [Serializable]
    public class ListarEmpresaSubGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro pelo código do subgrupo
        /// </summary>
        public string FiltroCodigoEmpresaSubGrupo { get; set; }

        /// <summary>
        /// Filtro pelo código do grupo
        /// </summary>
        public string FiltroCodigoEmpresaGrupo { get; set; }
    }
}
