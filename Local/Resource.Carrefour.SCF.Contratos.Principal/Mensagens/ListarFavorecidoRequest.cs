﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de favorecidos
    /// </summary>
    [Serializable]
    public class ListarFavorecidoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro pelo código do favorecido
        /// </summary>
        public string FiltroCodigoFavorecido { get; set; }

        /// <summary>
        /// Filtro por CNPJ
        /// </summary>
        public string FiltroCNPJFavorecido { get; set; }

        //MaxLinhas
        public int? FiltroMaxLinhas {get; set; }
    }
}
