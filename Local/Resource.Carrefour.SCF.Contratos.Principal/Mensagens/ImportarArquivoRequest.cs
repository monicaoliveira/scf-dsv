﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de importação de arquivo
    /// </summary>
    public class ImportarArquivoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Layout do Arquivo
        /// </summary>
        public string TipoArquivo { get; set; }

        /// <summary>
        /// Caminho do arquivo a ser importado
        /// </summary>
        public string CaminhoArquivo { get; set; }

        /// <summary>
        /// Código do processo que está solicitando a importação do arquivo
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
