﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de retorno da remoção da referencia de dominio
    /// </summary>
    [Serializable]
    public class RemoverListaItemReferenciaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Codigo da referencia removida
        /// </summary>
        public string CodigoListaItemReferenciaRemovido { get; set; }
    }
}
