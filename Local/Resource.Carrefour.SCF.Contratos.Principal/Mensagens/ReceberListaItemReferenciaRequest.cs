﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação do detalhe da referência
    /// </summary>
    [Serializable]
    public class ReceberListaItemReferenciaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo da referência
        /// </summary>
        public string CodigoListaItemReferencia { get; set; }
    }
}
