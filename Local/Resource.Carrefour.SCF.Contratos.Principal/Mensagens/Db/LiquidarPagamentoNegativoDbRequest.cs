﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    /// <summary>
    /// Mensagem de solicitação para atualizar pagamentos
    /// </summary>
    public class LiquidarPagamentoNegativoDbRequest : SalvarObjetoRequest<PagamentoInfo>
    {
        /// <summary>
        /// Informa o pagamento que deve ser marcado como liquidado
        /// </summary>
        public string CodigoPagamento { get; set; }

        /// <summary>
        /// Informa a data da liquidacao do pagamento que deve ser marcado como liquidado
        /// </summary>
        public DateTime DataLiquidacao { get; set; }



    }
}
