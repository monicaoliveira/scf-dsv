﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    public class RemoverProdutoDbRequest : RemoverObjetoRequest<ProdutoInfo>
    {
        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }

        public bool RemoverAssociacoesPlanos { get; set; }
    }
}
