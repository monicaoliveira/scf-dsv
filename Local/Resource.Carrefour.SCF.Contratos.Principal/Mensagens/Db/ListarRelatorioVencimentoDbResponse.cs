﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    public class ListarRelatorioVencimentoDbResponse : ConsultarObjetosResponse<RelatorioVencimentoInfo>
    {
        public int QuantidadeLinhas { get; set; }
    }
}
