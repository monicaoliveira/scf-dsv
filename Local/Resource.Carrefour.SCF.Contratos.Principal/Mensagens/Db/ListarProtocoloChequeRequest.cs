﻿//963
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class ListarProtocoloChequeRequest : MensagemRequestBase
    {
        public string TipoArquivo; //1465
        public string Link; //1465
        public string TipoRelatorio; //1465
        public DateTime? FiltroDataEstornoDe { get; set; }
        public DateTime? FiltroDataEstornoAte { get; set; }
        public DateTime? FiltroDataEstorno { get; set; }
        public DateTime? FiltroDataProcessamentoDe { get; set; }
        public DateTime? FiltroDataProcessamentoAte { get; set; }
        public DateTime? FiltroDataProcessamento { get; set; }
        public DateTime? FiltroDataRegProtocoloDe { get; set; }
        public DateTime? FiltroDataRegProtocoloAte { get; set; }
        public DateTime? FiltroDataRegProtocolo { get; set; }
        public string FiltroEnvioCci { get; set; }
        public string FiltroNumeroProtocolo { get; set; }
        public string FiltroEstabelecimento { get; set; }
        public string FiltroStatusProtocolo { get; set; }
        public string FiltroContaCliente { get; set; }
        public string FiltroTipoEstorno { get; set; }

    }
}

