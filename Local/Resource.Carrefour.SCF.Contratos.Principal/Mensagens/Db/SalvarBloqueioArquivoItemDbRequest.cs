﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    public class SalvarBloqueioArquivoItemDbRequest : SalvarObjetoRequest<ArquivoItemResumoInfo>
    {
        public List<string> CodigosArquivoItemBloqueados { get; set; }
        public List<string> CodigosArquivoItemDesbloqueados { get; set; }
    }
}
