﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    /// <summary>
    /// Request de banco de dados de arquivo
    /// </summary>
    public class ReceberArquivoDbRequest : ReceberObjetoRequest<ArquivoInfo>
    {
        /// <summary>
        /// Indica se deve ser retornado o detalhe
        /// </summary>
        public bool RetornarDetalhe { get; set; }
    }
}
