﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    public class SubstituirContaFavorecidoDbRequest : SubstituirObjetoRequest<ContaFavorecidoInfo>
    {
        // SCF1701 - ID 27
    }

    public class SubstituirContaFavorecidoProdutoDbRequest : SubstituirObjetoRequest<ContaFavorecidoProdutoInfo>
    {
        // SCF1701 - ID 27
    }
}
