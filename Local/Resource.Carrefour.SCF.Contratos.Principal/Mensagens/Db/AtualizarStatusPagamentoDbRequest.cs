﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    /// <summary>
    /// Mensagem de solicitação para atualizar pagamentos
    /// </summary>
    public class AtualizarStatusPagamentoDbRequest : SalvarObjetoRequest<PagamentoInfo>
    {
        /// <summary>
        /// Informa o pagamento que deve ser marcado como liquidado
        /// </summary>
        public string CodigoPagamento { get; set; }

        /// <summary>
        /// Informa o Status do Pagamento
        /// </summary>
        public DateTime DataLiquidacao { get; set; }

        /// <summary>
        /// Status do pagamento
        /// </summary>
        public PagamentoStatusEnum Status { get; set; }

        /// <summary>
        /// Uusuario envio / usuario exclusao
        /// </summary>
        public string CodigoUsuarioEnvio { get; set; }
        public string CodigoUsuarioExclusao { get; set; }




    }
}
