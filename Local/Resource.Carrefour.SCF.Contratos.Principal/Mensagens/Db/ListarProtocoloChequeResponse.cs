﻿//963
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class ListarProtocoloChequeResponse : MensagemResponseBase
    {
        public List<ProcessoProtocoloChequeInfo> Resultado { get; set; } //1465
        public ListarProtocoloChequeResponse()
        {
            this.Resultado = new List<ProcessoProtocoloChequeInfo>();
        }
    }
}