﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    public class SalvarProdutoDbRequest : SalvarObjetoRequest<ProdutoInfo>
    {
        public bool RetornarDetalhe;

        public SalvarObjetoTipoSincronismoEnum TipoSincronismo;

        public SalvarProdutoDbRequest()
        {
            this.TipoSincronismo = SalvarObjetoTipoSincronismoEnum.Sincronizar;
        }
    }
}
