﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    /// <summary>
    /// Mensagem de solicitação para atualizar pagamentos
    /// </summary>
    public class AtualizarPagamentosDbRequest : SalvarObjetoRequest<PagamentoInfo>
    {
        /// <summary>
        /// Filtra os pagamentos a serem recalculados por codigo de pagamento
        /// </summary>
        public string FiltroCodigoPagamento { get; set; }

        /// <summary>
        /// Filtra os pagamentos a serem recalculados por codigo de arquivo
        /// </summary>
        public string FiltroCodigoArquivo { get; set; }
    }
}
