﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db
{
    public class RemoverContaFavorecidoDbRequest : RemoverObjetoRequest<ContaFavorecidoProdutoInfo>
    {
        public string CodigoContaFavorecido { get; set; }
        public string CodigoProduto { get; set; }
        public string Referencia { get; set; }  //1339
    }
}
