﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de agendamento de pagamento seguindo regras de extrato
    /// </summary>
    public class AgendarPagamentoExtrato2Request : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do processo
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
