﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta do relatório de conciliação contábil
    /// </summary>
    [Serializable]
    public class ListarRelatorioConciliacaoContabilResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<RelatorioConciliacaoContabilInfo> Resultado { get; set; }

        /// <summary>
        /// Quantidade de linhas do relatório
        /// </summary>
        public int QuantidadeLinhas { get; set; }

        public decimal SaldoAnterior { get; set; }

        public decimal SaldoFinal { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarRelatorioConciliacaoContabilResponse()
        {
            this.Resultado = new List<RelatorioConciliacaoContabilInfo>();
        }
    }
}
