﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de Produto
    /// </summary>
    [Serializable]
    public class ReceberProdutoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do Produto solicitado
        /// </summary>
        public string CodigoProduto { get; set; }
    }
}
