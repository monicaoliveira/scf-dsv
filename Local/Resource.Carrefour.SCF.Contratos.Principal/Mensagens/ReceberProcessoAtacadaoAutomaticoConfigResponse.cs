﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de config do processo atacadao
    /// </summary>
    public class ReceberProcessoAtacadaoAutomaticoConfigResponse : MensagemResponseBase
    {
        /// <summary>
        /// Config solicitado
        /// </summary>
        public ProcessoAtacadaoAutomaticoConfig Config { get; set; }
    }
}
