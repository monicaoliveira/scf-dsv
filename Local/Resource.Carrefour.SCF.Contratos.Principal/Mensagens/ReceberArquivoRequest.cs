﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de detalhe do arquivo
    /// </summary>
    [Serializable]
    public class ReceberArquivoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo desejado
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Indica se deve retornar o ArquivoDetalheView
        /// </summary>
        public bool RetornarDetalhe { get; set; }
    }
}
