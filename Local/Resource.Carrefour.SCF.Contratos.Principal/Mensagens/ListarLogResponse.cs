﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de lista de logs
    /// </summary>
    public class ListarLogResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de logs encontrado
        /// </summary>
        public List<LogResumoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarLogResponse()
        {
            this.Resultado = new List<LogResumoInfo>();
        }
    }
}
