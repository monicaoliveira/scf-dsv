﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao da ConfiguracaoTipoRegistro
    /// </summary>
    [Serializable]
    public class RemoverConfiguracaoTipoRegistroRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de ConfiguracaoTipoRegistro a ser removido
        /// </summary>
        public string CodigoConfiguracaoTipoRegistro { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
