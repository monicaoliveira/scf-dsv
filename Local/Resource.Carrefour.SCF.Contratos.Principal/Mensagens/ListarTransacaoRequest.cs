﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de transações
    /// </summary>
    public class ListarTransacaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por status de retorno de cessao
        /// </summary>
        public TransacaoStatusRetornoCessaoEnum? FiltroStatusRetornoCessao { get; set; }

        /// <summary>
        /// Indica se deve realizar o filtro de status de retorno de cessao
        /// </summary>
        public bool FiltroRetornoCessao { get; set; }

        /// <summary>
        /// Filtro por data inclusao maior
        /// </summary>
        public DateTime? FiltroDataInclusaoMaior { get; set; }

        /// <summary>
        /// Filtro por data inclusao menor
        /// </summary>
        public DateTime? FiltroDataInclusaoMenor { get; set; }

        /// <summary>
        /// Filtra pelo código do processo.
        /// </summary>
        public string FiltroCodigoProcesso { get; set; }

        /// <summary>
        /// Filtra pela chave
        /// </summary>
        public string Chave { get; set; }

        /// <summary>
        /// Filtra pela chave 2
        /// </summary>
        public string Chave2 { get; set; }

        /// <summary>
        /// Maximo de linhas a serem retornadas
        /// </summary>
        public int MaxLinhas { get; set; }
    }
}
