﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de config do processo atacadao
    /// </summary>
    public class ReceberProcessoCessaoAutomaticoConfigRequest : MensagemRequestBase
    {
    }
}
