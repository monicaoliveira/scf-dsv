﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta de solicitação de execução de query
    /// </summary>
    public class ExecutarAuxilioQueryResponse : MensagemResponseBase
    {
        /// <summary>
        /// Cabecalho
        /// </summary>
        public string[] Cabecalho { get; set; }
        
        /// <summary>
        /// Resultado
        /// </summary>
        public string[] Resultado { get; set; }
    }
}
