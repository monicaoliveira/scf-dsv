﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de lista de arquivo item
    /// </summary>
    [Serializable]
    public class ListarArquivoItemResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado encontrado
        /// </summary>
        public List<ArquivoItemResumoInfo> Resultado { get; set; }
    }
}
