﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de detalhe do arquivo
    /// </summary>
    [Serializable]
    public class ReceberArquivoResponse : MensagemResponseBase
    {
        public ArquivoInfo ArquivoInfo { get; set; }
    }
}
