﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de planos
    /// </summary>
    [Serializable]
    public class RemoverPlanoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do plano a ser removido
        /// </summary>
        public string CodigoPlanoRemovido { get; set; }
    }
}
