﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de planos
    /// </summary>
    /// Jira: SCF - 1520
    [Serializable]
     public class ListarRelatorioInterchangeResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<RelatorioInterChangeInfo> Resultado { get; set; }

        /// <summary>
        /// Quantidade de linhas
        /// </summary>
        public int QuantidadeLinhas { get; set; }

        /// <summary>
        /// Codigo Log
        /// </summary>
        public string CodLog { get; set; }

        /// <summary>
        /// Construtor Default
        /// </summary>
        public ListarRelatorioInterchangeResponse()
        {
            this.Resultado = new List<RelatorioInterChangeInfo>();
        }
    }
}
