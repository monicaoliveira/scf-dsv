﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de salvar dias não úteis
    /// </summary>
    [Serializable]
    public class LiquidarPagamentoNegativoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Pagamento negativo a ser salvo
        /// </summary>
        public PagamentoInfo PagamentoNegativoInfo { get; set; }
    }
}
