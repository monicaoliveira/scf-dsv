﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar Grupo
    /// </summary>
    [Serializable]
    public class SalvarEmpresaGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Grupo a ser salvo
        /// </summary>
        public EmpresaGrupoInfo EmpresaGrupoInfo { get; set; }
    }
}
