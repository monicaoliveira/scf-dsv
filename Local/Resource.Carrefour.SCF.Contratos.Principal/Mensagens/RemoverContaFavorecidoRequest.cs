﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de Estabelecimento
    /// </summary>
    [Serializable]
    public class RemoverContaFavorecidoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de grupo a ser removido
        /// </summary>
        public string CodigoContaFavorecido { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
