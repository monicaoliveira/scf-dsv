﻿//<%--Jira 963 - Marcos Matsuoka--%>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de detalhe de Tipo Estorno
    /// </summary>
    [Serializable]
    public class ReceberTipoEstornoResponse : MensagemResponseBase
    {
        public string TipoEstorno { get; set; }
    }
}
