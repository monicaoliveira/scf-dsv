﻿using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de lista de Layouts
    /// </summary>
    [Serializable]
    public class ListarLayoutResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado da busca de layouts
        /// </summary>
        public List<LayoutInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarLayoutResponse()
        {
            this.Resultado = new List<LayoutInfo>();
        }
    }
}
