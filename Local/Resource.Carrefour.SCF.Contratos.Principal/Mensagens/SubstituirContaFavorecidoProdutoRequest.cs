﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{   [Serializable]
    public class SubstituirContaFavorecidoProdutoRequest : MensagemRequestBase
    {   // SCF1701 - ID 27
        public string CodigoFavorecido { get; set; }
        public List<SubstituirContaFavorecidoProdutoLista> ListaFavorecidoProduto { get; set; }
    }

    public class SubstituirContaFavorecidoProdutoLista
    {   // SCF1701 - ID 27
        public string CodigoContaFavorecido { get; set; }
        public string CodigoProduto { get; set; }
        public string Referencia { get; set; }
        public int Transacoes { get; set; }
    }
}