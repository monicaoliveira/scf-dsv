﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de detalhe de log
    /// </summary>
    public class ReceberLogResponse : MensagemResponseBase
    {
        /// <summary>
        /// Log encontrado
        /// </summary>
        public LogInfo LogInfo { get; set; }
    }
}
