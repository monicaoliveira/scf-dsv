﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de produtos
    /// </summary>
    [Serializable]
    public class ListarContaFavorecidoProdutoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<ContaFavorecidoProdutoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarContaFavorecidoProdutoResponse()
        {
            this.Resultado = new List<ContaFavorecidoProdutoInfo>();
        }
    }
}
