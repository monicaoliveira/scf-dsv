﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitacao de exportação de um arquivo
    /// </summary>
    [Serializable]
    public class ExportarArquivoResponse : MensagemResponseBase
    {

        
    }
}
