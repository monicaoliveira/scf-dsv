﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista item referencia
    /// </summary>
    [Serializable]
    public class ListarListaItemReferenciaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro pelo codigo lista item referencia
        /// </summary>
        public string FiltroCodigoListaItemReferencia { get; set; }

        /// <summary>
        /// Filtro pela situacao da referencia
        /// </summary>
        public string FiltroCodigoListaItemSituacao { get; set; }

        /// <summary>
        /// Filtro pelo código lista item (valor dominio)
        /// </summary>
        public string FiltroCodigoListaItem { get; set; }

        /// <summary>
        /// Filtro pelo nome da referencia
        /// </summary>
        public string FiltroNomeReferencia { get; set; }
    }
}
