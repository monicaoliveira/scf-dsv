﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de dias não úteis
    /// </summary>
    [Serializable]
    public class ReceberDiaNaoUtilRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do dia não útil solicitado
        /// </summary>
        public string CodigoDiaNaoUtil { get; set; }
    }
}
