﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Produtos
    /// </summary>
    [Serializable]
    public class ListarProdutoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por status Cedível
        /// </summary>
        public string FiltroCedivel { get; set; }

        public int? FiltroMaxLinhas { get; set;}
    }
}
