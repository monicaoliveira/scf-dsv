﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Resposta à solicitacao de inicializacao de listas do sistema
    /// </summary>
    [Serializable]
    public class InicializarListasResponse : MensagemResponseBase
    {
    }
}
