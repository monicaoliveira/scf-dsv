﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de Grupo
    /// </summary>
    [Serializable]
    public class RemoverEmpresaGrupoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do Grupo a ser removido
        /// </summary>
        public string CodigoEmpresaGrupoRemovido { get; set; }
    }
}
