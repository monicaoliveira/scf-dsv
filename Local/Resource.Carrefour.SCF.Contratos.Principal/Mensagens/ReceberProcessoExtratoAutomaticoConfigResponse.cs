﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem resposta a uma solicitação de configurações para o processo extrato automático
    /// </summary>
    public class ReceberProcessoExtratoAutomaticoConfigResponse : MensagemResponseBase
    {
        /// <summary>
        /// Config solicitado
        /// </summary>
        public ProcessoExtratoAutomaticoConfig Config { get; set; }
    }
}
