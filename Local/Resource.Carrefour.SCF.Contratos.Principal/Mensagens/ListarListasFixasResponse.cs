﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Resposta para iniciar as listas Fixas
    /// </summary>
    public class ListarListasFixasResponse : MensagemResponseBase
    {
        public List<string> Resultado { get; set; }

        public ListarListasFixasResponse()
        {
            this.Resultado = new List<string>();
        }
    }
}
