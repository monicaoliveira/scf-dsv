﻿//<%--Jira 963 - Marcos Matsuoka--%>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Tipo de Estorno
    /// </summary>
    [Serializable]
    public class ListarTipoEstornoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por status
        /// </summary>
        public string FiltroTipoEstorno { get; set; }


    };
}

