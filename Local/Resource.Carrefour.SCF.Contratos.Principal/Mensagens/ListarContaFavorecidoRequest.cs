﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de ContaFavorecido
    /// </summary>
    [Serializable]
    public class ListarContaFavorecidoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por codigo
        /// </summary>
        public string FiltroCodigoFavorecido { get; set; }

        /// <summary>
        /// Filtro por banco
        /// </summary>
        public string FiltroBanco { get; set; }

        /// <summary>
        /// Filtro por agencia
        /// </summary>
        public string FiltroAgencia { get; set; }

        /// <summary>
        /// Filtro por conta
        /// </summary>
        public string FiltroConta { get; set; }

        /// <summary>
        /// Filtro por agencia CCI
        /// </summary>
        public string FiltroAgencia_CCI { get; set; }

        /// <summary>
        /// Filtro por conta CCI
        /// </summary>
        public string FiltroConta_CCI { get; set; }
        
    }
}
