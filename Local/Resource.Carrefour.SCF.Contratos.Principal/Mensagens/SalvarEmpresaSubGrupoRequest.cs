﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar subgrupo
    /// </summary>
    [Serializable]
    public class SalvarEmpresaSubGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// EmpresaSubGrupoIInfo salvo
        /// </summary>
        public EmpresaSubGrupoInfo EmpresaSubGrupoInfo { get; set; }
    }
}
