﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de planos
    /// </summary>
    [Serializable]
    public class RemoverPlanoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de plano a ser removido
        /// </summary>
        public string CodigoPlano { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
