﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de início do serviço que vai ficar monitorando o diretorio e iniciar o processo
    /// Atacadao automaticamente
    /// </summary>
    public class IniciarServicoExpurgoRequest : MensagemRequestBase
    {

        /// <summary>
        /// Intervalo entre as execucoes, em segundos
        /// </summary>
        public int IntervaloExecucao { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public IniciarServicoExpurgoRequest()
        {
            this.IntervaloExecucao = 60 * 60;
        }
    }
}
