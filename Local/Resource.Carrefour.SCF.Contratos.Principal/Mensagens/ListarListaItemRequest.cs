﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Requisição para iniciar as listas Fixas
    /// </summary>
    public class ListarListaItemRequest : MensagemRequestBase
    {
        public int CodigoLista{ get; set; }

        public string NomeLista { get; set; }

        public string Valor { get; set; }
    }
}
