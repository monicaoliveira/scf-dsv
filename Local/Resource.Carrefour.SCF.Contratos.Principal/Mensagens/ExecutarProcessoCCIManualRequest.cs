﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de criação de processo de geracao de CCI Manual
    /// </summary>
    public class ExecutarProcessoCCIManualRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do arquivo que deverá ser gerado o CCI
        /// </summary>
        public string CodigoArquivoTSYS { get; set; }

        /// <summary>
        /// Caminho do arquivo a ser gerado
        /// </summary>
        public string CaminhoGeracaoArquivo { get; set; }

        /// <summary>
        /// Indica se deve executar sincrono ou assincrono
        /// </summary>
        public bool ExecutarAssincrono { get; set; }

        /// <summary>
        /// indicar para mover o arquivo de origem
        /// </summary>
        public bool MoverArquivoOrigem { get; set; }
    }
}
