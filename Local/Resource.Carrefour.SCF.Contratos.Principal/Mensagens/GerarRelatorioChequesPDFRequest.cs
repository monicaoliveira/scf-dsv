﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    [Serializable]
    public class GerarRelatorioChequesPDFRequest : MensagemRequestBase //1465
    {
        public string TipoArquivo;
        public string Link;
        public ListarProtocoloChequeRequest Request { get; set; }
        public string CodigoProcesso { get; set; }
    }
}
