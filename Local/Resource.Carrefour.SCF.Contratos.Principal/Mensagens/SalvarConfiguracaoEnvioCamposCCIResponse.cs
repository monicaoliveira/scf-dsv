﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao para salvar configurações de tipo de registro
    /// </summary>
    [Serializable]
    public class SalvarConfiguracaoEnvioCamposCCIResponse : MensagemResponseBase
    {
        /// <summary>
        /// Configurações salvas
        /// </summary>
        public ConfiguracaoEnvioCamposCCIInfo ConfiguracaoEnvioCamposCCIInfo { get; set; }
    }
}

