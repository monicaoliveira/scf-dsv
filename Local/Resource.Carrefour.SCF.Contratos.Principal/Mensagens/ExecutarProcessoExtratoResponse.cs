﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de criação de processo de extrato
    /// </summary>
    public class ExecutarProcessoExtratoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do processo criado
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Indica se o processo foi finalizado
        /// </summary>
        public bool ProcessoFinalizado { get; set; }
    }
}
