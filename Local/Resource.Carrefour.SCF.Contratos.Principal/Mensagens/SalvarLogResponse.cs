﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class SalvarLogResponse : MensagemResponseBase
    {
        /// <summary>
        /// Retorno
        /// </summary>
        public LogSCFRelatorioInfo retorno { get; set; }
    }
}
