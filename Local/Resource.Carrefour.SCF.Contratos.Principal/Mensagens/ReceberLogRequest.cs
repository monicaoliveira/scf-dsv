﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de detalhe de log
    /// </summary>
    public class ReceberLogRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do log
        /// </summary>
        public string CodigoLog { get; set; }
    }
}
