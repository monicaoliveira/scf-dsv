﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Estabelecimentos
    /// </summary>
    [Serializable]
    public class ListarEstabelecimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por status
        /// </summary>
        public string FiltroAtivo { get; set; }

        /// <summary>
        /// Filtro por grupo (empresa)
        /// </summary>
        public string FiltroCodigoCnpj { get; set; }

        /// <summary>
        /// Filtra pelo CNPJ do favorecido
        /// </summary>
        public string FiltroCnpjFavorecido { get; set; }


        /// <summary>
        /// Filtro por subgrupo (empresa)
        /// </summary>
        public string FiltroRazaoSocial { get; set; }

        /// <summary>
        /// Filtro por subgrupo (empresa)
        /// </summary>
        public string FiltroCodigoCsu { get; set; }

        /// <summary>
        /// Filtro por subgrupo (empresa)
        /// </summary>
        public string FiltroCodigoSitef { get; set; }
        /// <summary>
        /// Max Linhas
        /// </summary>
        public int? FiltroMaxLinhas { get; set; }
    };
}
