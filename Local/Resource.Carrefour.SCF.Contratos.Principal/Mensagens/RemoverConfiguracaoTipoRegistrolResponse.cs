﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de dias não úteis
    /// </summary>
    [Serializable]
    public class RemoverConfiguracaoTipoRegistroResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código da configuracao do tipo de registro
        /// </summary>
        public string CodigoConfiguracaoTipoRegistro { get; set; }
    }
}
