﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de execução de processo
    /// </summary>
    public class ExecutarProcessoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do processo a ser executado
        /// Caso seja informado este campo, será executado um processo
        /// existente
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// ProcessoInfo para gerar um novo processo
        /// Caso seja informado este campo, será criado um novo processo
        /// do tipo informado
        /// </summary>
        public ProcessoInfo ProcessoInfo { get; set; }

        /// <summary>
        /// Indica se a execução do processo deve ser assíncrona
        /// </summary>
        public bool ExecutarAssincrono { get; set; }

        /// <summary>
        /// Indica se deve validar se o processo já está em andamento
        /// </summary>
        public bool ValidaProcessoEmAndamento { get; set; }

        /// Jira 968 - Marcos Matsuoka
        /// <summary>
        /// Será usado quando o processo for liberado ou continuado
        /// </summary>
        public string Descricao { get; set; }
    }
}
