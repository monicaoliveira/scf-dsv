﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de requisição para salvar uma configuração de lista fixa
    /// </summary>
    public class SalvarListaConfiguracaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Objeto a ser salvo
        /// </summary>
        public ListaConfiguracaoInfo Objeto { get; set; }
    }
}
