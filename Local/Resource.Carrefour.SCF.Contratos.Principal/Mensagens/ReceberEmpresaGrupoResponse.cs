﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de detalhe de Grupo
    /// </summary>
    [Serializable]
    public class ReceberEmpresaGrupoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Detalhe do Grupo
        /// </summary>
        public EmpresaGrupoInfo EmpresaGrupoInfo { get; set; }
    }
}
