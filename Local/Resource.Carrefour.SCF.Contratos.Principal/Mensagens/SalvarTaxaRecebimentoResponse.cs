﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação para salvar Taxa de Recebimento
    /// </summary>
    [Serializable]
    public class SalvarTaxaRecebimentoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Taxa de Recebimento Info
        /// </summary>
        public TaxaRecebimentoInfo TaxaRecebimentoInfo { get; set; }
    }
}
