﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class EstornarLoteExtratoRequest : MensagemRequestBase
    {
        public string CodigoProcesso { get; set; }
    }
}
