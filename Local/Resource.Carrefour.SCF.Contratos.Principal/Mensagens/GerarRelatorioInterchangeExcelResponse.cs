﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// Jira: SCF - 1520
    [Serializable]
    public class GerarRelatorioInterchangeExcelResponse: MensagemResponseBase
    {
        public string CaminhoArquivoGerado { get; set; }
        public int QuantidadeLinhasGeradas { get; set; }
        public string CodigoProcesso { get; set; }    
    }
}
