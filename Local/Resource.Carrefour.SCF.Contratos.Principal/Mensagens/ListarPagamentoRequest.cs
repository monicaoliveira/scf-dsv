﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Pagamento negativo
    /// </summary>
    [Serializable]
    public class ListarPagamentoRequest : MensagemRequestBase
    {
        ///// <summary>
        ///// Listar pagamento negativo (Deve ser S ou N)
        ///// </summary>
        // 2017-Melhorias - o conceito de pagamentos mudou
        //public string ListarPagamentoNegativo { get; set; }

        /// <summary>
        /// Filtro pelo estabelecimento
        /// </summary>
        public string FiltroEstabelecimento { get; set; }

        /// <summary>
        /// Filtro pela data inicial de transacao 
        /// </summary>
        public DateTime? FiltroDataPagamentoInicio { get; set; }

        /// <summary>
        /// Filtro pela data final de transacao 
        /// </summary>
        public DateTime? FiltroDataPagamentoFim { get; set; }

        /// <summary>
        /// Filtro pela data final de transacao 
        /// </summary>
        public DateTime? FiltroDataPagamento { get; set; }

        /// <summary>
        /// Filtro por status do pagamento
        /// </summary>
        public string FiltroStatusPagamento { get; set; }

        /// <summary>
        /// Filtro pela Data de Liquidacao
        /// </summary>
        public DateTime? FiltroDataLiquidacao { get; set; }

        /// <summary>
        /// Filtro pelo Favorecido
        /// </summary>
        public string FiltroCodigoFavorecido { get; set; }

        /// <summary>
        /// Filtro pela data de envio (inicio e fim)
        /// </summary>
        public DateTime? FiltroDataEnvioDe { get; set; }
        public DateTime? FiltroDataEnvioAte { get; set; }

        /// <summary>
        /// Filtro pela data de geracao (inicio e fim)
        /// </summary>
        public DateTime? FiltroDataGeracaoDe { get; set; }
        public DateTime? FiltroDataGeracaoAte { get; set; }


        /// <summary>
        /// Filtro pelo banco / agencia / conta de pagamento
        /// </summary>
        public string FiltroBanco { get; set; }
        public string FiltroAgencia { get; set; }
        public string FiltroConta { get; set; }


        /// <summary>
        /// Apenas Quantidade = indica se é para retornar apenas a quantidade e nao os registros. Se estiver True, o retorno será apenas a qtde
        /// </summary>
        public bool RetornaApenasQuantidade { get; set; }


    }
}
