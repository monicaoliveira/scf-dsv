﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção da conta do favorecido
    /// </summary>
    [Serializable]
    public class RemoverContaFavorecidoProdutoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código da conta do favorecido
        /// </summary>
        public string CodigoContaFavorecidoProdutoRemovido { get; set; }
    }
}
