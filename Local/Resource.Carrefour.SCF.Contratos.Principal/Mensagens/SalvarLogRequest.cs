﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class SalvarLogRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo Log
        /// </summary>
        public string CodigoLog { get; set; }

        /// <summary>
        /// Codigo do usuário
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Descrição
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Data do log
        /// </summary>
        public DateTime DataLog { get; set; }

        /// <summary>
        /// Código da origem
        /// </summary>
        public string CodigoOrigem { get; set; }

        /// <summary>
        /// Tipo da origem
        /// </summary>
        public string TipoOrigem { get; set; }

        public SalvarLogRequest()
        {
            DataLog = new DateTime();
            DataLog = DateTime.Now;
        }
    }
}
