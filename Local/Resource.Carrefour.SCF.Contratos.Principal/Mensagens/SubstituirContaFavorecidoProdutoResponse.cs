﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de substituicao da conta do favorecido
    /// </summary>
    [Serializable]
    public class SubstituirContaFavorecidoProdutoResponse : MensagemResponseBase
    {   /// SCF1701 - ID 27
        /// <summary>
        /// Código da conta do favorecido
        /// </summary>
        public string CodigoContaFavorecidoProduto { get; set; }

        /// <summary>
        /// Indica o código do processo gerado
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
