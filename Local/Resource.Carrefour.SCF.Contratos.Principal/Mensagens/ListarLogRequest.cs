﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de logs
    /// </summary>
    public class ListarLogRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por tipo de log
        /// </summary>
        public string FiltroTipoLog { get; set; }

        /// <summary>
        /// Filtro por código de origem
        /// </summary>
        public string FiltroCodigoOrigem { get; set; }

        /// <summary>
        /// Filtro por tipo de origem
        /// </summary>
        public string FiltroTipoOrigem { get; set; }

        /// <summary>
        /// Filtro por codigo de usuario
        /// </summary>
        public string FiltroCodigoUsuario { get; set; }

        /// <summary>
        /// Filtro por data de log a partir de
        /// </summary>
        public DateTime? FiltroDataInicio { get; set; }

        /// <summary>
        /// Filtro por data de log até
        /// </summary>
        public DateTime? FiltroDataFim { get; set; }

        /// <summary>
        /// Filtro por data de log até
        /// </summary>
        public DateTime? FiltroData { get; set; }

        /// <summary>
        /// Permitir especificar a quantidade de registros a ser retornada
        /// </summary>
        public int QuantidadeRegistros { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarLogRequest()
        {
            this.QuantidadeRegistros = 100;
        }
    }
}
