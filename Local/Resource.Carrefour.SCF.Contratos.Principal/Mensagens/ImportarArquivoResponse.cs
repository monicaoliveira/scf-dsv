﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de importação de arquivo
    /// </summary>
    public class ImportarArquivoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do Arquivo gerado
        /// </summary>
        public string CodigoArquivo { get; set; }
    }
}
