﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de detalhe de planos
    /// </summary>
    [Serializable]
    public class ReceberPlanoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Detalhe do plano
        /// </summary>
        public PlanoInfo PlanoInfo { get; set; }
    }
}
