﻿//963
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{   public class SalvarProtocoloChequeRequest : MensagemRequestBase
    {   public DateTime? DataRegProtocolo { get; set; }
        public DateTime? DataEstorno { get; set; }
        public string CodigoTransacao { get; set; }
        public string NumeroProtocolo { get; set; }
        public string TipoEstorno { get; set; } //963
    }
}