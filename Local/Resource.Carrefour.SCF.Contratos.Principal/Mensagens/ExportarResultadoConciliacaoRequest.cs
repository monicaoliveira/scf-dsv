﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de exportacao de arquivo resultado de conciliacao (Atacadão)
    /// </summary>
    public class ExportarResultadoConciliacaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Caminho do arquivo que será gerado
        /// </summary>
        public string CaminhoArquivoDestino { get; set; }


    }
}
