﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de desbloqueio e bloqueio de item de arquivo.
    /// </summary>
    public class SalvarArquivoItemRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigos dos itens bloqueados.
        /// </summary>
        public List<string> CodigosArquivoItemBloqueados { get; set; }

        /// <summary>
        /// Codigos dos itens desbloqueados.
        /// </summary>
        public List<string> CodigosArquivoItemDesbloqueados { get; set; }
    }
}
