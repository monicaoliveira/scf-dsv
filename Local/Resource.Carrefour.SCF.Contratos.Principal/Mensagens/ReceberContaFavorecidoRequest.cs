﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de Estabelecimento
    /// </summary>
    [Serializable]
    public class ReceberContaFavorecidoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo da conta do favorecido
        /// </summary>
        public string CodigoContaFavorecido { get; set; }
    }
}
