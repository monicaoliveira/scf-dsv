﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de referencia
    /// </summary>
    [Serializable]
    public class RemoverListaItemReferenciaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do objeto a ser removido
        /// </summary>
        public string CodigoListaItemReferencia { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
