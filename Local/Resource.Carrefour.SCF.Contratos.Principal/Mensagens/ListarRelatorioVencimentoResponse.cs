﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de planos
    /// </summary>
    [Serializable]
    public class ListarRelatorioVencimentoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<RelatorioVencimentoInfo> Resultado { get; set; }

        /// <summary>
        /// Resultado
        /// </summary>
        public int QuantidadeLinhas { get; set; }

        /// <summary>
        /// Resultado
        /// </summary>
        public string CodLog { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarRelatorioVencimentoResponse()
        {
            this.Resultado = new List<RelatorioVencimentoInfo>();
        }
    }
}
