﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de configurações para o processo extrato automático
    /// </summary>
    public class ReceberProcessoExtratoAutomaticoConfigRequest : MensagemRequestBase
    {
    }
}
