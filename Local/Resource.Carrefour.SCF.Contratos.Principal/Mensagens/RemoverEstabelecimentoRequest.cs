﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de Estabelecimento
    /// </summary>
    [Serializable]
    public class RemoverEstabelecimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de grupo a ser removido
        /// </summary>
        public string CodigoEstabelecimento { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
