﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    [Serializable]
    public class GerarRelatorioChequesExcelResponse : MensagemResponseBase //1465
    {
        public string CaminhoArquivoGerado { get; set; }
        public int QuantidadeLinhasGeradas { get; set; }
        public string CodigoProcesso { get; set; }
    }
}

