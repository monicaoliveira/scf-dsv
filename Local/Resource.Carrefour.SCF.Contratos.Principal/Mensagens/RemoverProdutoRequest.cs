﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de produto
    /// </summary>
    [Serializable]
    public class RemoverProdutoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de grupo a ser removido
        /// </summary>
        public string CodigoProduto { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
