﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;


namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Resposta para iniciar as listas Fixas
    /// </summary>
    public class ListarListaItemResponse : MensagemResponseBase
    {
        public List<ListaItemInfo> Resultado { get; set; }

        public ListarListaItemResponse()
        {
            this.Resultado = new List<ListaItemInfo>();
        }
    }
}
