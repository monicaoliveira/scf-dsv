﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de lista item
    /// </summary>
    [Serializable]
    public class RemoverListaItemRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de lista item a ser removido
        /// </summary>
        public string CodigoListaItem { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
