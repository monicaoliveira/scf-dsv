﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de criação de processo de extrato
    /// </summary>
    public class ExecutarProcessoExtratoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Opcionalmente pode receber o config na mensagem
        /// </summary>
        public ProcessoExtratoAutomaticoConfig Config { get; set; }

        /// <summary>
        /// Indica se deve executar sincrono ou assincrono
        /// </summary>
        public bool ExecutarAssincrono { get; set; }

        /// <summary>
        /// indicar para mover o arquivo de origem
        /// </summary>
        public bool MoverArquivoOrigem { get; set; }
    }
}
