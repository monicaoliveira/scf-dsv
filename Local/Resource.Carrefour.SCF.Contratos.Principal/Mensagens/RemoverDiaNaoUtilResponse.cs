﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de dias não úteis
    /// </summary>
    [Serializable]
    public class RemoverDiaNaoUtilResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do dia não útil a ser removido
        /// </summary>
        public string CodigoDiaNaoUtilRemovido { get; set; }
    }
}
