﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de exportação de um arquivo
    /// </summary>
    [Serializable]
    public class ExportarArquivoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Pede código do arquivo a ser exportado
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Caminho do arquivo a ser gerado
        /// </summary>
        public string CaminhoArquivo { get; set; }
    }
}
