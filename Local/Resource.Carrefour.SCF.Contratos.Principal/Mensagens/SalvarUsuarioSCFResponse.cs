﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de salvar usuário
    /// </summary>
    [Serializable]
    public class SalvarUsuarioSCFResponse : MensagemResponseBase
    {
        public UsuarioSCFInfo usuarioSCFInfo;
    }
}
