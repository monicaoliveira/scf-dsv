﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de detalhe de configurações gerais
    /// </summary>
    [Serializable]
    public class ReceberConfiguracaoGeralResponse : MensagemResponseBase
    {
        /// <summary>
        /// Detalhe da configuração geral atual
        /// </summary>
        public ConfiguracaoGeralInfo ConfiguracaoGeralInfo { get; set; }
    }
}
