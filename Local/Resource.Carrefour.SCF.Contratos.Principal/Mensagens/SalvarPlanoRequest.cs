﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar planos
    /// </summary>
    [Serializable]
    public class SalvarPlanoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Plano a ser salvo
        /// </summary>
        public PlanoInfo PlanoInfo { get; set; }
    }
}
