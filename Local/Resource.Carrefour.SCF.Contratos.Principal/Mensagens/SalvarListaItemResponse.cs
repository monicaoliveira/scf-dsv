﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de salvar ListaInfo
    /// </summary>
    [Serializable]
    public class SalvarListaItemResponse : MensagemResponseBase
    {
        /// <summary>
        /// ListaItem salvo
        /// </summary>
        public ListaItemInfo ListaItemInfo { get; set; }
    }
}
