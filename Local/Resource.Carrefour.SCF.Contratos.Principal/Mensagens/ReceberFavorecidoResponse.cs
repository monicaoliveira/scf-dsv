﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de detalhe de Estabelecimento
    /// </summary>
    [Serializable]
    public class ReceberFavorecidoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Detalhe do Estabelecimento
        /// </summary>
        public FavorecidoInfo FavorecidoInfo { get; set; }
    }
}
