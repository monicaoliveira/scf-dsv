﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de detalhe do subgrupo
    /// </summary>
    [Serializable]
    public class ReceberEmpresaSubGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do subgrupo
        /// </summary>
        public string CodigoEmpresaSubGrupo { get; set; }
    }
}
