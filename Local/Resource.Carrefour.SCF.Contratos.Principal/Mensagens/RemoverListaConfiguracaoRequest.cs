﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de uma remoção de configuração de lista fixa
    /// </summary>
    public class RemoverListaConfiguracaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do objeto a ser removido
        /// </summary>
        public string CodigoConfiguracaoLista { get; set; }
    }
}
