﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de execução do processo atacadão fazendo a movimentação de
    /// arquivos e criação de diretórios com resultado do processo, e utilizando os caminhos
    /// indicados em configuração
    /// </summary>
    public class ExecutarProcessoAtacadao2Request : MensagemRequestBase
    {
    }
}
