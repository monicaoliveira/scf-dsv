using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

//==========================================================================================01
namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    [Serializable]
	//======================================================================================02
    public class ListarContaFavorecidoProdutoRequest : MensagemRequestBase
    {
        public string FiltroCodigoProduto { get; set; }
        public string FiltroCodigoFavorecido { get; set; }
		public string FiltroReferencia { get; set; } //1339
    }
	//======================================================================================02
}
//==========================================================================================01