﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar conta
    /// </summary>
    [Serializable]
    public class SalvarContaFavorecidoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Conta a ser salvar
        /// </summary>
        public ContaFavorecidoInfo ContaFavorecidoInfo { get; set; }
    }
}
