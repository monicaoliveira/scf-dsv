﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação remoção de um Taxa de Recebimento 
    /// </summary>
    [Serializable]
    public class RemoverTaxaRecebimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do Taxa de Recebimento 
        /// </summary>
        public string CodigoTaxaRecebimento { get; set; }
    }
}
