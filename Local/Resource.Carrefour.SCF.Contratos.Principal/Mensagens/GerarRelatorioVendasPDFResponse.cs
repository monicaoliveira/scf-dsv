﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de planos
    /// </summary>
    [Serializable]
    public class GerarRelatorioVendasPDFResponse : MensagemResponseBase
    {
        /// <summary>
        /// Indica o caminho do arquivo gerado
        /// </summary>
        public string CaminhoArquivoGerado { get; set; }

        /// <summary>
        /// Quantidade de linhas geradas no relatorio
        /// </summary>
        public int QuantidadeLinhasGeradas { get; set; }

        /// <summary>
        /// Indica o código do processo gerado
        /// </summary>
        public string CodigoProcesso { get; set; }


    }
}

