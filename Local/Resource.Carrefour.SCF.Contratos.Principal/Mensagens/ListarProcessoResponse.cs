﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de lista de processos
    /// </summary>
    public class ListarProcessoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado encontrado
        /// </summary>
        public List<ProcessoResumoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarProcessoResponse()
        {
            this.Resultado = new List<ProcessoResumoInfo>();
        }
    }
}
