﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de lista de subgrupos
    /// </summary>
    [Serializable]
    public class ListarEmpresaSubGrupoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado da lista de subgrupos
        /// </summary>
        public List<EmpresaSubGrupoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarEmpresaSubGrupoResponse()
        {
            this.Resultado = new List<EmpresaSubGrupoInfo>();
        }
    }
}
