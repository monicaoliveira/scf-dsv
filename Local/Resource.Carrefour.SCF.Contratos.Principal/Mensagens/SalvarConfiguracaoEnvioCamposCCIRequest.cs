﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar as configurações de tipo de registro
    /// </summary>
    [Serializable]
    public class SalvarConfiguracaoEnvioCamposCCIRequest : MensagemRequestBase
    {
        /// <summary>
        /// Configurações a ser salvas
        /// </summary>
        public ConfiguracaoEnvioCamposCCIInfo ConfiguracaoEnvioCamposCCIInfo { get; set; }
    }
}

