﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem resposta a uma solicitacao de inicializacao de listas do sistema
    /// </summary>
    [Serializable]
    public class InicializarListasRequest : MensagemRequestBase
    {
        /// <summary>
        /// Url do host da aplicação
        /// Usado para inferir qual ambiente a aplicação está sendo utilizada
        /// (desenvolvimento, homologação, produção)
        /// </summary>
        public string Url { get; set; }
    }
}
