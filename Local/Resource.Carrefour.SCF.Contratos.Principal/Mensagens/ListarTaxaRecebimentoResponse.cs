﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de lista de subgrupos
    /// </summary>
    [Serializable]
    public class ListarTaxaRecebimentoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado da lista de Taxas de Recebimento
        /// </summary>
        public List<TaxaRecebimentoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarTaxaRecebimentoResponse()
        {
            this.Resultado = new List<TaxaRecebimentoInfo>();
        }
    }
}
