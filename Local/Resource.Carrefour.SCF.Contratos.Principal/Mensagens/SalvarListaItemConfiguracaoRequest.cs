﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class SalvarListaItemConfiguracaoRequest : MensagemRequestBase
    {
        public string CodigoLista { get; set; }

        public string Descricao { get; set; }

        public List<ListaConfiguracaoItemInfo> Itens { get; set; }
    }
}
