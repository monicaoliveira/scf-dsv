﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    [Serializable]
    public class ListarConfiguracaoTipoRegistroRequest : MensagemRequestBase
    {
        public string FiltroTipoArquivo { get; set; } //1361
        //1361 public bool FiltroAtivo { get; set; }
    }
}
