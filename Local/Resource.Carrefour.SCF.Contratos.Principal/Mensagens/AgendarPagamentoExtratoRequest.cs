﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de agendamento de pagamentos seguindo as
    /// regras do processo extrato
    /// </summary>
    public class AgendarPagamentoExtratoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do processo no qual devem ser agendado os pagamentos
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Codigo do arquivo do processo.
        /// </summary>
        public string CodigoArquivo { get; set; }
    }
}
