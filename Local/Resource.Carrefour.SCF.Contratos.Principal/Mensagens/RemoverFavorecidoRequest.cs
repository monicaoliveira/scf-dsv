﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de Estabelecimento
    /// </summary>
    [Serializable]
    public class RemoverFavorecidoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de grupo a ser removido
        /// </summary>
        public string CodigoFavorecido { get; set; }

        /// <summary>
        /// Indica se e para forcar a remocao
        /// </summary>
        public bool ForcarRemocao { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
