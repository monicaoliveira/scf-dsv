﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de cancelamento de processo
    /// </summary>
    public class CancelarProcessoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do processo a ser cancelado
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Indica se deve executar assíncrono
        /// </summary>
        public bool ExecutarAssincrono { get; set; }
    }
}
