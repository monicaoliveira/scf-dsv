﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de salvar conta
    /// </summary>
    [Serializable]
    public class SubstituirContaFavorecidoResponse : MensagemResponseBase
    {
        /// SCF1701 - ID 27
        /// <summary>
        /// Código da conta do favorecido
        /// </summary>
        public string CodigoContaFavorecidoProduto { get; set; }
    
        /// <summary>
        /// Indica o código do processo gerado
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
