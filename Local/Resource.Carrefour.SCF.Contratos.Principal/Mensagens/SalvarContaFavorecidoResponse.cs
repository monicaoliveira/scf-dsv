﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de salvar conta
    /// </summary>
    [Serializable]
    public class SalvarContaFavorecidoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Conta salva
        /// </summary>
        public ContaFavorecidoInfo ContaFavorecidoInfo { get; set; }
    }
}
