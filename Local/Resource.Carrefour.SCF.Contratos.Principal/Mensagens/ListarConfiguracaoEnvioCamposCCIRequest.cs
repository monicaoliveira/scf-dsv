﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de configuracoes
    /// </summary>
    [Serializable]
    public class ListarConfiguracaoEnvioCamposCCIRequest : MensagemRequestBase 
    {
        /// <summary>
        /// Filtro por Codigo do Campo
        /// </summary>
        public int FiltroCodigoCampo { get; set; }

        /// <summary>
        /// Filtro por Nome do Campo
        /// </summary>
        public string FiltroNomeCampo { get; set; }
        
        /// <summary>
        /// Filtro por Status de Envio ao CCI
        /// </summary>
        public bool FiltroEnviarCCI { get; set; }
    }
}
