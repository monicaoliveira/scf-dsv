﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de configuracoes por tipo de registro
    /// </summary>
    [Serializable]
    public class ListarConfiguracaoEnvioCamposCCIResponse : MensagemResponseBase 
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<ConfiguracaoEnvioCamposCCIInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarConfiguracaoEnvioCamposCCIResponse()
        {
            this.Resultado = new List<ConfiguracaoEnvioCamposCCIInfo>();
        }
    }
}
