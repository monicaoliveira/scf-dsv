﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de Estabelecimento
    /// </summary>
    [Serializable]
    public class ReceberEstabelecimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do Estabelecimento solicitado
        /// </summary>
        public string CodigoEstabelecimento { get; set; }
    }
}
