﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de retorno de desbloqueio e bloqueio de item de arquivo.
    /// </summary>
    public class SalvarArquivoItemResponse : MensagemResponseBase
    {
    }
}
