﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de lista de favorecidos
    /// </summary>
    [Serializable]
    public class ListarFavorecidoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado da busca de favorecidos
        /// </summary>
        public List<FavorecidoInfo> Resultado { get; set; }

          /// <summary>
        /// Construtor default
        /// </summary>
        public ListarFavorecidoResponse()
        {
            this.Resultado = new List<FavorecidoInfo>();
        }
    }
}
