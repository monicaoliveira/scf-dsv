﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de execução do processo cessao fazendo a ação de
    /// arquivos e criação de diretórios com resultado do processo, e utilizando os caminhos
    /// indicados em configuração
    /// </summary>
    public class ExecutarProcessoCessaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Opcionalmente pode receber o config na mensagem
        /// </summary>
        public ProcessoCessaoAutomaticoConfig Config { get; set; }
        
        /// <summary>
        /// Indica se deve executar sincrono ou assincrono
        /// </summary>
        public bool ExecutarAssincrono { get; set; }

        /// <summary>
        /// indicar para mover o arquivo de origem
        /// </summary>
        public bool MoverArquivoOrigem { get; set; }
    
    }
}
