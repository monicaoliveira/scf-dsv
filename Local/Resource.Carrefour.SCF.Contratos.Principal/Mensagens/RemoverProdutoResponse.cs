﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remoção de produto
    /// </summary>
    [Serializable]
    public class RemoverProdutoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do produto a ser removido
        /// </summary>
        public string CodigoProdutoRemovido { get; set; }
    }
}
