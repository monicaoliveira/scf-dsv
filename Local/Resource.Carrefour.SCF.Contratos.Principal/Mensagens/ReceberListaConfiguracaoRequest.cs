﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de requisição para receber uma configuração de uma lista fixa
    /// </summary>
    public class ReceberListaConfiguracaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do objeto a ser consultado
        /// </summary>
        public string CodigoObjeto { get; set; }
    }
}
