﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de salvar as configurações gerais
    /// </summary>
    [Serializable]
    public class SalvarConfiguracaoGeralRequest : MensagemRequestBase
    {
        /// <summary>
        /// Configurações a ser salvas
        /// </summary>
        public ConfiguracaoGeralInfo ConfiguracaoGeralInfo { get; set; }
    }
}
