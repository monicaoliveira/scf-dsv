﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de execução de processo
    /// </summary>
    public class ExecutarProcessoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Código do processo executado
        /// </summary>
        public string CodigoProcesso { get; set; }
        
        /// <summary>
        /// Para execuções síncronas, indica se o processo foi finalizado
        /// </summary>
        public bool ProcessoFinalizado { get; set; }

        /// <summary>
        /// Status do processo
        /// </summary>
        public ProcessoStatusEnum StatusProcesso { get; set; }

        /// <summary>
        /// Status do estágio
        /// </summary>
        public EstagioStatusEnum StatusEstagio { get; set; }

        /// <summary>
        /// Caso tenha sido pedido a execucao assincrona, retorna
        /// o código da tarefa assíncrona gerada
        /// </summary>
        public string CodigoTarefa { get; set; }
    }
}
