﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de lista de descricoes de log
    /// </summary>
    public class ListarDescricaoLogResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de descricao dos tipos de log
        /// </summary>
        public List<TipoDescricaoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarDescricaoLogResponse()
        {
            this.Resultado = new List<TipoDescricaoInfo>();
        }
    }
}
