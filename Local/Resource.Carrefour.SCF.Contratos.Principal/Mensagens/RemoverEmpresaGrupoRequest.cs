﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de remocao de Grupo
    /// </summary>
    [Serializable]
    public class RemoverEmpresaGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do cadastro de grupo a ser removido
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Ativar?
        /// </summary>
        public bool Ativar { get; set; }
    }
}
