﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    [Serializable]
    public class ReceberListaItemResponse : MensagemResponseBase
    {
        public ListaItemInfo ListaItemInfo { get; set; }
    }
}
