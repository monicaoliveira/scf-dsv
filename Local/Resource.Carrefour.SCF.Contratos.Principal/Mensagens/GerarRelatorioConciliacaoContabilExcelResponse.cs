﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta do relatório de conciliação contábil
    /// </summary>
    [Serializable]
    public class GerarRelatorioConciliacaoContabilExcelResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<RelatorioConciliacaoContabilInfo> Resultado { get; set; }

        /// <summary>
        /// Quantidade de linhas do relatório
        /// </summary>
        public int QuantidadeLinhas { get; set; }

        public decimal SaldoAnterior { get; set; }

        public decimal SaldoFinal { get; set; }

        public string CaminhoArquivoGerado { get; set; }

        /// <summary>
        /// Indica o código do processo gerado
        /// </summary>
        public string CodigoProcesso { get; set; }

        public string ErrorMessage { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public GerarRelatorioConciliacaoContabilExcelResponse()
        {
            this.Resultado = new List<RelatorioConciliacaoContabilInfo>();
        }
    }
}
