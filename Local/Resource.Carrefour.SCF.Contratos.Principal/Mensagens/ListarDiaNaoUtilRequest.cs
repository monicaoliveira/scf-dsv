﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de dias não úteis
    /// </summary>
    [Serializable]
    public class ListarDiaNaoUtilRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por status
        /// </summary>
        public bool? FiltroAtivo { get; set; }

        /// <summary>
        /// Filtro por dia de feriado maior que o informado
        /// </summary>
        public DateTime? FiltroDataMaior { get; set; }
    }
}
