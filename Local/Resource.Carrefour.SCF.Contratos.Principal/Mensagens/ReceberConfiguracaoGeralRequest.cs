﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de configurações gerais
    /// </summary>
    [Serializable]
    public class ReceberConfiguracaoGeralRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código da configuração geral
        /// </summary>
        //public string CodigoConfiguracaoGeral { get; set; }
    }
}
