﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta de um lista item referencia
    /// </summary>
    [Serializable]
    public class ListarListaItemReferenciaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<ListaItemReferenciaInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarListaItemReferenciaResponse()
        {
            this.Resultado = new List<ListaItemReferenciaInfo>();
        }
    }
}
