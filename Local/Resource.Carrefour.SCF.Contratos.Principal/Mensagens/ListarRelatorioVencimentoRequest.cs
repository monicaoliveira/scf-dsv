﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de lista de Estabelecimentos
    /// </summary>
    [Serializable]
    public class ListarRelatorioVencimentoRequest : MensagemRequestBase
    {

        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public string TipoArquivo;

        /// <summary>
        /// Gerar pagamentos
        /// </summary>
        public bool GerarPagamentos;

        /// <summary>
        /// Informa o codigo do processo - será utilizado para gravar logs na geracao de pagamento
        /// </summary>
        public string CodigoProcesso;


        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public string Link;

        /// <summary>
        /// Filtro por FiltroEmpresaGrupo
        /// </summary>
        public bool VerificarQuantidadeLinhas;

        /// <summary>
        /// Filtro por RetornarRelatorio
        /// </summary>
        public bool RetornarRelatorio;

       
        /// <summary>
        /// Filtro por TipoRelatorio
        /// </summary>
        public string TipoRelatorio;

        /// <summary>
        /// Filtro por FiltroEmpresaGrupo
        /// </summary>
        public string FiltroEmpresaGrupo;

        /// <summary>
        /// Filtro por sub grupo
        /// </summary>
        public string FiltroEmpresaSubGrupo;

        /// <summary>
        /// Filtro por FiltroBanco
        /// </summary>
        public string FiltroBanco;

        /// <summary>
        /// Filtro por FiltroProduto
        /// </summary>
        public string FiltroProduto { get; set; }

        /// <summary>
        /// Filtro por Referecncia
        /// </summary>
        public string FiltroReferencia { get; set; }

        /// <summary>
        /// Filtro por FiltroProduto
        /// </summary>
        public string FiltroFavorecido;

        /// <summary>
        /// Filtro por tipo de registro
        /// </summary>
        public string FiltroTipoRegistro;

        /// <summary>
        /// Indica se haverá consolidacao por agendamento e vencimento
        /// </summary>
        public bool FiltroConsolidaAgendamentoVencimento;

        /// <summary>
        /// Indica se haverá consolidacao por maior data de agendamento
        /// </summary>
        public bool FiltroConsolidaMaiorData;

        /// <summary>
        /// Indica se haverá consolidacao por referência
        /// </summary>
        public bool FiltroConsolidaReferencia;

        /// <summary>
        /// Indica se deve retornar as transacoes de retorno de cessao
        /// </summary>
        public string FiltroStatusRetornoCessao { get; set; }

        /// <summary>
        /// Filtro por FiltroDataPosicao
        /// </summary>
        public Nullable<DateTime> FiltroDataPosicao;
        
        /// <summary>
        /// Filtro por FiltroDataInicio
        /// </summary>
        public Nullable<DateTime> FiltroDataInicioVencimento;
        
        /// <summary>
        /// Filtro por FiltroDataFim
        /// </summary>
        public Nullable<DateTime> FiltroDataFimVencimento;

        /// <summary>
        /// Filtro por FiltroDataInicio
        /// </summary>
        public Nullable<DateTime> FiltroDataInicioTransacao;

        /// <summary>
        /// Filtro por FiltroDataFim
        /// </summary>
        public Nullable<DateTime> FiltroDataFimTransacao;

        /// <summary>
        /// Filtro por FiltroDataInicio
        /// </summary>
        public Nullable<DateTime> FiltroDataVencimento;

        /// <summary>
        /// Filtro por FiltroDataFim
        /// </summary>
        public Nullable<DateTime> FiltroDataTransacao;

        /// <summary>
        /// Filtro por FiltroDataInicioMovimento
        /// </summary>
        public Nullable<DateTime> FiltroDataInicioMovimento;

        /// <summary>
        /// Filtro por FiltroDataFimMovimento
        /// </summary>
        public Nullable<DateTime> FiltroDataFimMovimento;

        /// <summary>
        /// Filtro por FiltroDataFim
        /// </summary>
        public Nullable<DateTime> FiltroDataMovimento;

        /// <summary>
        /// Filtro Data do agendamento De
        /// </summary>
        public DateTime? FiltroDataAgendamentoDe;

        /// <summary>
        /// Filtro Data do agendamento Até
        /// </summary>
        public DateTime? FiltroDataAgendamentoAte;

        /// <summary>
        /// Filtro Tipo Financeiro Contabil
        /// ECOMMERCE - Fernando Bove - 20160104
        /// </summary>
        public string FiltroFinanceiroContabil;

        //SCF1170
        /// <summary>
        /// Filtro de inicio de Data de Processamento
        /// </summary>
        public Nullable<DateTime> FiltrotxtDataRetornoCCIInicio { get; set; }

        //SCF1170
        /// <summary>
        /// Filtro de fim de Data de Processamento
        /// </summary>
        public Nullable<DateTime> FiltrotxtDataRetornoCCIFim { get; set; }

        /// <summary>
        /// Indica se deve retornar transações já pagas
        /// </summary>
        public bool FiltroRetornarTransacaoPaga { get; set; }
    }
}
