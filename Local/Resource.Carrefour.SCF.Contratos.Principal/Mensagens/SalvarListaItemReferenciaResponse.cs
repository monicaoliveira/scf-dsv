﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    public class SalvarListaItemReferenciaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista Item Referencia Info
        /// </summary>
        public ListaItemReferenciaInfo ListaItemReferenciaInfo { get; set; }
    }
}
