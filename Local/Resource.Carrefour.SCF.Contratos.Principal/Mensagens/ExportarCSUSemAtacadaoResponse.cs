﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem resposta a uma solicitação de exportacao csu sem atacadão
    /// </summary>
    public class ExportarCSUSemAtacadaoResponse : MensagemResponseBase
    {
    }
}
