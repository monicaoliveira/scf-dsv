﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de importar transacoes sitef
    /// </summary>
    public class ImportarTransacoesSitefRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo no qual devem ser geradas as transações
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do processo que está solicitando a importação das transações
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
