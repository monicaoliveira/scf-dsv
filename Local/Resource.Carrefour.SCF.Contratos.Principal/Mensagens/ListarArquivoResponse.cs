﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de lista de arquivos
    /// </summary>
    [Serializable]
    public class ListarArquivoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado encontrado
        /// </summary>
        public List<ArquivoInfo> Resultado { get; set; }
    }
}
