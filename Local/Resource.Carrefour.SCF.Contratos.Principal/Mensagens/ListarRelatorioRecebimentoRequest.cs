﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao do Relatório de Recebimentos
    /// </summary>
    [Serializable]
    public class ListarRelatorioRecebimentoRequest : MensagemRequestBase
    {

        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public string TipoArquivo;

        /// <summary>
        /// Link do relatório
        /// </summary>
        public string Link;

        /// <summary>
        /// Filtro por TipoRelatorio
        /// </summary>
        public string TipoRelatorio;

        /// <summary>
        /// Filtro por Grupo
        /// </summary>
        public string FiltroGrupo;

        /// <summary>
        /// Filtro por Referencia
        /// </summary>
        public string FiltroReferencia;

        /// <summary>
        /// Filtro por Meio de Pagamento
        /// </summary>
        public string FiltroMeioPagamento;

        /// <summary>
        /// Filtro por Grupo
        /// </summary>
        public string NomeGrupo;

        /// <summary>
        /// Filtro por Referencia
        /// </summary>
        public string DescricaoReferencia;

        /// <summary>
        /// Filtro por Meio de Pagamento
        /// </summary>
        public string DescricaoMeioPagamento;

        /// <summary>
        /// Filtro por Data Repasse Inicio
        /// </summary>
        public Nullable<DateTime> FiltroDataRepasseInicio { get; set; }

        /// <summary>
        /// Filtro por Data Repasse Fim
        /// </summary>
        public Nullable<DateTime> FiltroDataRepasseFim { get; set; }
    }
}
