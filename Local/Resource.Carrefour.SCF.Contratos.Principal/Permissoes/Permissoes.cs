﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Permissoes;

namespace Resource.Carrefour.SCF.Contratos.Principal.Permissoes
{
    #region Permissoes Menu

    //==================================================================
    // CONSULTAS
    //==================================================================
    [Permissao(
    CodigoPermissao = "101",
    NomePermissao = "Menu Consulta",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Consulta.")]
    [Serializable]
    public class PermissaoMenuConsulta : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "102",
    NomePermissao = "Menu Consulta - Lista de Processos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Lista de Processos.")]
    [Serializable]
    public class PermissaoMenuListaProcesso : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "103",
    NomePermissao = "Menu Consulta - Lista de Arquivos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Lista de Arquivos.")]
    [Serializable]
    public class PermissaoMenuListaArquivo : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "104",
    NomePermissao = "Menu Consulta - Pagamentos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Pagamentos.")]
    [Serializable]
    public class PermissaoMenuPagamentos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "105",
    NomePermissao = "Menu Consulta - Bloqueios",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Bloqueios.")]
    [Serializable]
    public class PermissaoMenuBloqueios : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "106",
    NomePermissao = "Menu Consulta - Retorno de Cessao",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Retorno Cessao.")]
    [Serializable]
    public class PermissaoMenuRetornoCessao : PermissaoBase
    {
    }

    //SCF1138 - Marcos Matsuoka
    [Permissao(
    CodigoPermissao = "140",
    NomePermissao = "Menu Consulta - Manutenção Processos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Manutenção de Processos.")]
    [Serializable]
    public class PermissaoMenuManutencaoProcesso : PermissaoBase
    {
    }

    //==================================================================
    // PROCESSOS
    //==================================================================
    [Permissao(
    CodigoPermissao = "107",
    NomePermissao = "Menu Processos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Processos.")]
    [Serializable]
    public class PermissaoMenuProcessos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "108",
    NomePermissao = "Menu Processos - Manual Proc Atacadao",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Processo Atacadao.")]
    [Serializable]
    public class PermissaoMenuProcessoAtacadao : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "109",
    NomePermissao = "Menu Processos - Manual Proc Extrato",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Processo Extrato.")]
    [Serializable]
    public class PermissaoMenuProcessoExtrato : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "110",
    NomePermissao = "Menu Processos - Manual Proc Cessao",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Processo Cessao.")]
    [Serializable]
    public class PermissaoMenuProcessoCessao : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "111",
    NomePermissao = "Menu Processos - Manual Proc Pagamento",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Processo Pagamento.")]
    [Serializable]
    public class PermissaoMenuProcessoPagamento : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "112",
    NomePermissao = "Menu Processos - Tratar Negativos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Tratar Negativos.")]
    [Serializable]
    public class PermissaoMenuTratarNegativos : PermissaoBase
    {
    }

    //==================================================================
    // RELATORIOS
    //==================================================================
    [Permissao(
    CodigoPermissao = "113",
    NomePermissao = "Menu Relatorios",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Relatorios.")]
    [Serializable]
    public class PermissaoMenuRelatorios : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "114",
    NomePermissao = "Menu Relatorios - Transacoes",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Transacoes.")]
    [Serializable]
    public class PermissaoMenuTransacoes : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "134",
    NomePermissao = "Menu Relatorios - Consolidado",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Consolidado.")]
    [Serializable]
    public class PermissaoMenuConsolidado : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "115",
    NomePermissao = "Menu Relatorios - Vencimentos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Vencimentos.")]
    [Serializable]
    public class PermissaoMenuVencimentos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "116",
    NomePermissao = "Menu Relatorios - Vendas",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Vendas.")]
    [Serializable]
    public class PermissaoMenuVendas : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "136",
    NomePermissao = "Menu Relatorios - Saldo Conciliacao",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Saldo de conciliação.")]
    [Serializable]
    public class PermissaoMenuSaldoConciliacao : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "143",
    NomePermissao = "Menu Relatorios - Recebimento",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Recebimento.")]
    [Serializable]
    public class PermissaoMenuRelatorioRecebimento : PermissaoBase
    {
    }

    //==================================================================
    // CONFIGURACOES
    //==================================================================
    [Permissao(
    CodigoPermissao = "117",
    NomePermissao = "Menu Configuracoes",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Configuracoes.")]
    [Serializable]
    public class PermissaoMenuConfiguracoes : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "118",
    NomePermissao = "Menu Configuracoes - Parametros",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Parametros.")]
    [Serializable]
    public class PermissaoMenuParametros : PermissaoBase
    {
    }


    [Permissao(
    CodigoPermissao = "119",
    NomePermissao = "Menu Configuracoes - Favorecidos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Favorecidos.")]
    [Serializable]
    public class PermissaoMenuFavorecidos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "120",
    NomePermissao = "Menu Configuracoes - Dominios",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Dominios.")]
    [Serializable]
    public class PermissaoMenuDominios : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "139",
    NomePermissao = "Menu Configuracoes - Config. Dominios",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Configuração de Domínios.")]
    [Serializable]
    public class PermissaoMenuConfiguracaoDominio : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "121",
    NomePermissao = "Menu Configuracoes - Grupos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Grupos.")]
    [Serializable]
    public class PermissaoMenuConfigGrupos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "122",
    NomePermissao = "Menu Configuracoes - Subgrupos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Subgrupos.")]
    [Serializable]
    public class PermissaoMenuSubgrupos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "123",
    NomePermissao = "Menu Configuracoes - Planos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Planos.")]
    [Serializable]
    public class PermissaoMenuPlanos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "124",
    NomePermissao = "Menu Configuracoes - Dias Nao Uteis",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Dias Nao Uteis.")]
    [Serializable]
    public class PermissaoMenuDiasNaoUteis: PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "125",
    NomePermissao = "Menu Configuracoes - Produtos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Produtos.")]
    [Serializable]
    public class PermissaoMenuProdutos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "126",
    NomePermissao = "Menu Configuracoes - Estabelecimentos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Estabelecimentos.")]
    [Serializable]
    public class PermissaoMenuEstabelecimentos : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "127",
    NomePermissao = "Menu Configuracoes - Tipo Registro",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Tipo Registro.")]
    [Serializable]
    public class PermissaoMenuTipoRegistro : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "137",
    NomePermissao = "Menu Configuracoes - Referenc Domínio",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Referências de Domínios.")]
    [Serializable]
    public class PermissaoMenuReferenciaDominio : PermissaoBase
    {
    }

    //1477
    [Permissao(
    CodigoPermissao = "141",
    NomePermissao = "Menu Configuracoes - Cheque Devolvido",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Configuração de Domínios.")]
    [Serializable]
    public class PermissaoMenuChequeDevolvido : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "142",
    NomePermissao = "Menu Configuracoes - Taxa de Recebimento",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Taxa de Recebimento.")]
    [Serializable]
    public class PermissaoTaxaRecebimento : PermissaoBase
    {
    }

    //==================================================================
    // ADMINISTRATIVO
    //==================================================================

    [Permissao(
    CodigoPermissao = "128",
    NomePermissao = "Menu Administrativo",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Administrativo.")]
    [Serializable]
    public class PermissaoMenuAdministrativo : PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "129",
    NomePermissao = "Menu Administrativo - Usuarios",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Usuarios.")]
    [Serializable]
    public class PermissaoMenuUsuarios: PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "130",
    NomePermissao = "Menu Administrativo - Grupos",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Grupos.")]
    [Serializable]
    public class PermissaoMenuAdmGrupos: PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "131",
    NomePermissao = "Menu Administrativo - Perfis",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Perfis.")]
    [Serializable]
    public class PermissaoMenuPerfis: PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "132",
    NomePermissao = "Menu Administrativo - Log",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Log.")]
    [Serializable]
    public class PermissaoMenuLog: PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "133",
    NomePermissao = "Menu Administrativo - Captura Arquivo",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Servico.")]
    [Serializable]
    public class PermissaoMenuServico: PermissaoBase
    {
    }

    [Permissao(
    CodigoPermissao = "135",
    NomePermissao = "Menu Administrativo - Parm Serguranca",
    DescricaoPermissao = "Indica se o usuário tem permissão para ter acesso ao item de menu Parâmetro de Sergurança.")]
    [Serializable]
    public class PermissaoMenuParametroSeguranca : PermissaoBase
    {
    }

    #endregion
}
