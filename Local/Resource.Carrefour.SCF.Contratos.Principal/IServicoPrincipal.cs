﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Interface do servico principal
    /// </summary>
    public interface IServicoPrincipal
    {
        #region Inicialização

        /// <summary>
        /// Faz a inicialização do sistema SCF
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        InicializarSCFResponse InicializarSCF(InicializarSCFRequest request);

        #endregion

        #region Listas

        /// <summary>
        /// Faz a inicialização das listas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        InicializarListasResponse InicializarListas(InicializarListasRequest request);

        /// <summary>
        /// Lista listas fixas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarListasFixasResponse ListarListasFixas(ListarListasFixasRequest request);

        /// <summary>
        /// Ativa/Inativa um item das listas genérica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverListaItemResponse RemoverListaItem(RemoverListaItemRequest request);

        /// <summary>
        /// Salva item Lista
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarListaItemResponse SalvarListaItem(SalvarListaItemRequest request);

        /// <summary>
        /// Lista a lista de itens de acordo com as condicoes passadas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarListaItemResponse ListarListaItem(ListarListaItemRequest request);

        /// <summary>
        /// Recebe dados de um item da lista
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberListaItemResponse ReceberListaItem(ReceberListaItemRequest request);

        #endregion

        #region Produto

        /// <summary>
        /// Lista Produtos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarProdutoResponse ListarProduto(ListarProdutoRequest request);

        /// <summary>
        /// Recebe um Produto
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberProdutoResponse ReceberProduto(ReceberProdutoRequest request);
        
        /// <summary>
        /// Remove um Produto
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverProdutoResponse RemoverProduto(RemoverProdutoRequest request);

        /// <summary>
        /// Salva um Produto
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarProdutoResponse SalvarProduto(SalvarProdutoRequest request);

        #endregion

        #region Favorecido

        /// <summary>
        /// Lista Favorecidos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarFavorecidoResponse ListarFavorecido(ListarFavorecidoRequest request);

        /// <summary>
        /// Recebe um Favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberFavorecidoResponse ReceberFavorecido(ReceberFavorecidoRequest request);

        /// <summary>
        /// Remove um Favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverFavorecidoResponse RemoverFavorecido(RemoverFavorecidoRequest request);

        /// <summary>
        /// Salva um Favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarFavorecidoResponse SalvarFavorecido(SalvarFavorecidoRequest request);

        #endregion

        #region ContaFavorecido

        /// <summary>
        /// Lista Favorecidos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarContaFavorecidoResponse ListarContaFavorecido(ListarContaFavorecidoRequest request);

        /// <summary>
        /// Recebe um Favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberContaFavorecidoResponse ReceberContaFavorecido(ReceberContaFavorecidoRequest request);

        /// <summary>
        /// Remove um Favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverContaFavorecidoResponse RemoverContaFavorecido(RemoverContaFavorecidoRequest request);

        /// <summary>
        /// Salva um Favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarContaFavorecidoResponse SalvarContaFavorecido(SalvarContaFavorecidoRequest request);
        // SCF1701 - ID 27
        SubstituirContaFavorecidoResponse SubstituirContaFavorecido(SubstituirContaFavorecidoRequest request);

        #endregion

        #region ContaFavorecidoProduto

        SalvarContaFavorecidoProdutoResponse SalvarContaFavorecidoProduto(SalvarContaFavorecidoProdutoRequest request);

        ListarContaFavorecidoProdutoResponse ListarContaFavorecidoProduto(ListarContaFavorecidoProdutoRequest request);

        RemoverContaFavorecidoProdutoResponse RemoverContaFavorecidoProduto(RemoverContaFavorecidoProdutoRequest request);
        // SCF1701 - ID 27
        SubstituirContaFavorecidoProdutoResponse SubstituirContaFavorecidoProduto(SubstituirContaFavorecidoProdutoRequest request);
        #endregion

        #region EmpresaGrupo
        /// <summary>
        /// Lista Grupos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarEmpresaGrupoResponse ListarEmpresaGrupo(ListarEmpresaGrupoRequest request);

        /// <summary>
        /// Recebe um Grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberEmpresaGrupoResponse ReceberEmpresaGrupo(ReceberEmpresaGrupoRequest request);

        /// <summary>
        /// Remover um Grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverEmpresaGrupoResponse RemoverEmpresaGrupo(RemoverEmpresaGrupoRequest request);

        /// <summary>
        /// Salva um Grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarEmpresaGrupoResponse SalvarEmpresaGrupo(SalvarEmpresaGrupoRequest request);
        #endregion

        #region EmpresaSubGrupo
        /// <summary>
        /// Lista subgrupos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarEmpresaSubGrupoResponse ListarEmpresaSubGrupo(ListarEmpresaSubGrupoRequest request);

        /// <summary>
        /// Recebe um subgrupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberEmpresaSubGrupoResponse ReceberEmpresaSubGrupo(ReceberEmpresaSubGrupoRequest request);

        /// <summary>
        /// Remover um subgrupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverEmpresaSubGrupoResponse RemoverEmpresaSubGrupo(RemoverEmpresaSubGrupoRequest request);

        /// <summary>
        /// Salva um subgrupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarEmpresaSubGrupoResponse SalvarEmpresaSubGrupo(SalvarEmpresaSubGrupoRequest request);
        #endregion

        #region Estabelecimento

        /// <summary>
        /// Lista Estabelecimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarEstabelecimentoResponse ListarEstabelecimento(ListarEstabelecimentoRequest request);

        /// <summary>
        /// Recebe um Estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberEstabelecimentoResponse ReceberEstabelecimento(ReceberEstabelecimentoRequest request);

        /// <summary>
        /// Remove um Estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverEstabelecimentoResponse RemoverEstabelecimento(RemoverEstabelecimentoRequest request);

        /// <summary>
        /// Salva um Estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarEstabelecimentoResponse SalvarEstabelecimento(SalvarEstabelecimentoRequest request);

        #endregion

        #region DiaNaoUtil
        /// <summary>
        /// Lista dias não úteis
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarDiaNaoUtilResponse ListarDiaNaoUtil(ListarDiaNaoUtilRequest request);

        /// <summary>
        /// Recebe um dias não útil
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberDiaNaoUtilResponse ReceberDiaNaoUtil(ReceberDiaNaoUtilRequest request);

        /// <summary>
        /// Remove um dias não útil
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverDiaNaoUtilResponse RemoverDiaNaoUtil(RemoverDiaNaoUtilRequest request);

        /// <summary>
        /// Salva um dias não útil
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarDiaNaoUtilResponse SalvarDiaNaoUtil(SalvarDiaNaoUtilRequest request);
        #endregion

        #region Plano
        /// <summary>
        /// Lista planos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarPlanoResponse ListarPlano(ListarPlanoRequest request);

        /// <summary>
        /// Recebe um plano
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberPlanoResponse ReceberPlano(ReceberPlanoRequest request);

        /// <summary>
        /// Remove um plano
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverPlanoResponse RemoverPlano(RemoverPlanoRequest request);

        /// <summary>
        /// Salva um plano
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarPlanoResponse SalvarPlano(SalvarPlanoRequest request);
        #endregion

        #region ConfiguracaoGeral
        /// <summary>
        /// Recebe a configuração atual do sistemas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberConfiguracaoGeralResponse ReceberConfiguracaoGeral(ReceberConfiguracaoGeralRequest request);

        /// <summary>
        /// Salva nova configuração
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarConfiguracaoGeralResponse SalvarConfiguracaoGeral(SalvarConfiguracaoGeralRequest request);
        #endregion

        #region Testes
        TesteResponse Teste(TesteRequest request);
        #endregion

        #region Pagamento

        /// <summary>
        /// Agendamento de Pagamentos seguindo regras do atacadão
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        AgendarPagamentoAtacadaoResponse AgendarPagamentoAtacadao(AgendarPagamentoAtacadaoRequest request);

        

        #endregion

        #region Processo

        /// <summary>
        /// Salva um Processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarProcessoResponse SalvarProcesso(SalvarProcessoRequest request);

        /// <summary>
        /// Faz a execução do processo atacadão usando informações de configurações 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExecutarProcessoAtacadaoResponse ExecutarProcessoAtacadao(ExecutarProcessoAtacadaoRequest request);

        /// <summary>
        /// Faz a execução do processo extrato usando informações de configurações 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExecutarProcessoExtratoResponse ExecutarProcessoExtrato(ExecutarProcessoExtratoRequest request);

        /// <summary>
        /// Efetua o expurgo do arquivo_item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExecutarProcessoExpurgoResponse ExecutarProcessoExpurgo(ExecutarProcessoExpurgoRequest request);

        /// <summary>
        /// Faz a execução do processo CCI Manual usando informações 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExecutarProcessoCCIManualResponse ExecutarProcessoCCIManual(ExecutarProcessoCCIManualRequest request);

        /// <summary>
        /// Executa um processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExecutarProcessoResponse ExecutarProcesso(ExecutarProcessoRequest request);

        /// <summary>
        /// Solicita detalhe de processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberProcessoResponse ReceberProcesso(ReceberProcessoRequest request);

        /// <summary>
        /// Lista de processos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarProcessoResponse ListarProcesso(ListarProcessoRequest request);

        /// <summary>
        /// Retorna configuracao do processo atacadao
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberProcessoAtacadaoAutomaticoConfigResponse ReceberProcessoAtacadaoAutomaticoConfig(ReceberProcessoAtacadaoAutomaticoConfigRequest request);

        /// <summary>
        /// Retorna configuracao do processo extrato
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberProcessoExtratoAutomaticoConfigResponse ReceberProcessoExtratoAutomaticoConfig(ReceberProcessoExtratoAutomaticoConfigRequest request);

        /// <summary>
        /// Cancela o processo solicitado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CancelarProcessoResponse CancelarProcesso(CancelarProcessoRequest request);

        ExecutarProcessoCessaoResponse ExecutarProcessoCessao(ExecutarProcessoCessaoRequest request);

        ReceberProcessoCessaoAutomaticoConfigResponse ReceberProcessoCessaoAutomaticoConfig(ReceberProcessoCessaoAutomaticoConfigRequest request);


        #endregion

        #region Arquivo

        /// <summary>
        /// Lista de arquivos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarArquivoResponse ListarArquivo(ListarArquivoRequest request);

        /// <summary>
        /// Recebe arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberArquivoResponse ReceberArquivo(ReceberArquivoRequest request);

        /// <summary>
        /// Exporta o arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExportarArquivoResponse ExportarArquivo(ExportarArquivoRequest request);

        /// <summary>
        /// Lista de itens de arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarArquivoItemResponse ListarArquivoItem(ListarArquivoItemRequest request);

        /// <summary>
        /// Salva itens de arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarArquivoItemResponse SalvarArquivoItem(SalvarArquivoItemRequest request);

        #endregion

        #region Log

        /// <summary>
        /// Lista de logs
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarLogResponse ListarLog(ListarLogRequest request);

        /// <summary>
        /// Solicita detalhe de log
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberLogResponse ReceberLog(ReceberLogRequest request);

        /// <summary>
        /// Lista as descricoes de log
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarDescricaoLogResponse ListarDescricaoLog(ListarDescricaoLogRequest request);

        #endregion

        #region Auxilio

        /// <summary>
        /// Solicitacao de execucao de query
        /// </summary>
        ExecutarAuxilioQueryResponse ExecutarAuxilioQuery(ExecutarAuxilioQueryRequest request);

        /// <summary>
        /// Solicitacao de config
        /// </summary>
        ExecutarAuxilioConfigResponse ExecutarAuxilioConfig(ExecutarAuxilioConfigRequest request);

        #endregion

        #region Relatório Conferencia Vendas
        /// <summary>
        /// Lista ConferenciaVendas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarRelatorioConferenciaVendasResponse ListarRelatorioConferenciaVendas(ListarRelatorioConferenciaVendasRequest request);
        #endregion
         
        #region Relatório Transacoes Excel

        GerarRelatorioTransacaoExcelResponse GerarRelatorioTransacaoExcel(GerarRelatorioTransacaoExcelRequest request);

        #endregion

        #region Relatório Venda Excel

        GerarRelatorioVendaExcelResponse GerarRelatorioVendaExcel(GerarRelatorioVendaExcelRequest request);

        #endregion

        #region Relatório Vencimento Excel
        GerarRelatorioVencimentoPDFResponse GerarRelatorioVencimentoPDF(GerarRelatorioVencimentoPDFRequest request);
        GerarRelatorioVendasPDFResponse GerarRelatorioVendasPDF(GerarRelatorioVendasPDFRequest request);
        GerarRelatorioVencimentoExcelResponse GerarRelatorioVencimentoExcel(GerarRelatorioVencimentoExcelRequest request);

        #endregion

        #region Relatório Vencimento
        /// <summary>
        /// ListarRelatorioVencimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarRelatorioVencimentoResponse ListarRelatorioVencimento(ListarRelatorioVencimentoRequest request);
        #endregion

        #region Relatório Consolidado
        /// <summary>
        /// Gerar relatório consolidado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GerarRelatorioConsolidadoExcelResponse GerarRelatorioConsolidadoExcel(GerarRelatorioConsolidadoExcelRequest request);
        GerarRelatorioConsolidadoPDFResponse GerarRelatorioConsolidadoPDF(GerarRelatorioConsolidadoPDFRequest request);
        /// <summary>
        /// Lista relatório consolidado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarRelatorioConsolidadoResponse ListarRelatorioConsolidado(ListarRelatorioConsolidadoRequest request);
        #endregion

        #region Relatório Log
        /// <summary>
        /// Gerar relatório log
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GerarRelatorioLogExcelResponse GerarRelatorioLogExcel(GerarRelatorioLogExcelRequest request);
        #endregion

        #region ConfiguracaoTipoRegistro

        ListarConfiguracaoTipoRegistroResponse ListarConfiguracaoTipoRegistro(ListarConfiguracaoTipoRegistroRequest request);

        SalvarConfiguracaoTipoRegistroResponse SalvarConfiguracaoTipoRegistro(SalvarConfiguracaoTipoRegistroRequest request);

        RemoverConfiguracaoTipoRegistroResponse RemoverDiaNaoUtil(RemoverConfiguracaoTipoRegistroRequest request);

        ReceberConfiguracaoTipoRegistroResponse ReceberConfiguracaoTipoRegistro(ReceberConfiguracaoTipoRegistroRequest request);
        
        #endregion

        /// 963        
        #region CadastroCheques 
        ListarProtocoloChequeResponse ListarProtocoloCheque(ListarProtocoloChequeRequest request);
        SalvarProtocoloChequeResponse SalvarProtocoloCheque(SalvarProtocoloChequeRequest request);
        GerarRelatorioChequesExcelResponse GerarRelatorioChequesExcel(GerarRelatorioChequesExcelRequest request); //1465
        GerarRelatorioChequesPDFResponse GerarRelatorioChequesPDF(GerarRelatorioChequesPDFRequest request); //1465
        #endregion


        #region ConfiguracaoEnvioCamposCCI

        ListarConfiguracaoEnvioCamposCCIResponse ListarConfiguracaoEnvioCamposCCI(ListarConfiguracaoEnvioCamposCCIRequest request);

        SalvarConfiguracaoEnvioCamposCCIResponse SalvarConfiguracaoEnvioCamposCCI(SalvarConfiguracaoEnvioCamposCCIRequest request);

        //RemoverConfiguracaoEnvioCamposCCIResponse RemoverConfiguracaoEnvioCamposCCI(RemoverConfiguracaoEnvioCamposCCIRequest request);

        //ReceberConfiguracaoEnvioCamposCCIResponse ReceberConfiguracaoEnvioCamposCCI(ReceberConfiguracaoEnvioCamposCCIRequest request);

        #endregion

        #region Pagamento

        /// <summary>
        /// Listar os pagamentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarPagamentoResponse ListarPagamento(ListarPagamentoRequest request);

        // DESABILITANDO A CHAMADA DO REMOVER PAGAMENTO DIRETAMENTE
        // ESSA CHAMADA PRECISA SER FEITA DE FORMA ASSINCRONA POIS IRA AFETAR A TABELA DE TRANSACOES
        // RemoverPagamentoResponse RemoverPagamento(RemoverPagamentoRequest request);

        /// <summary>
        /// Processo de geracao dos pagamentos através do relatório de vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GerarPagamentobyRelatorioVenctoResponse GerarPagamentobyRelatorioVencto(GerarPagamentobyRelatorioVenctoRequest request);

        /// <summary>
        /// Mensagem para exclusao do pagamento de forma assincrona
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ProcessarExclusaoPagamentosResponse ProcessarExclusaoPagamentos(ProcessarExclusaoPagamentosRequest request);

        /// <summary>
        /// Listar os pagamentos cancelados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarCancelamentoPagamentoOnlineResponse ListarPagamentoCancelamentoOnline(ListarCancelamentoPagamentoOnlineRequest request);

         /* 2017-MELHORIAS - RETIRANDO ESSE SERVICO, POIS O CONCEITO DE PAGAMENTOS MUDOU
         
        /// <summary>
        /// Listar pagamentos negativos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        LiquidarPagamentoNegativoResponse LiquidarPagamentoNegativo(LiquidarPagamentoNegativoRequest request);
        */

        /// <summary>
        /// Agendamento das transacoes do extrato
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        AgendarPagamentoExtratoResponse AgendarPagamentoExtrato(AgendarPagamentoExtratoRequest request);

        /// <summary>
        /// Agendamento das transacoes do extrato (versão 2)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        AgendarPagamentoExtrato2Response AgendarPagamentoExtrato2(AgendarPagamentoExtrato2Request request);

        // MELHORIAS-2017 - ESSE PROCESSO FOI SUBSTITUIDO POR OUTRO
        ///// <summary>
        ///// Execução do processo de pagamento
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //ExecutarProcessoPagamentoResponse ExecutarProcessoPagamento(ExecutarProcessoPagamentoRequest request);

        #endregion

        #region Transacao

        /// <summary>
        /// Lista de transações
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarTransacaoResponse ListarTransacao(ListarTransacaoRequest request);

        #endregion

        #region UsuarioSCF

        /// <summary>
        /// Salva o usuário informado
        /// </summary>
        /// <param name="parametros"></param>
        /// <returns></returns>
        SalvarUsuarioSCFResponse SalvarUsuarioSCF(SalvarUsuarioSCFRequest request);

        #endregion

        EstornarLoteExtratoResponse EstornarLoteExtrato(EstornarLoteExtratoRequest req);

        /// <summary>
        /// Verifica se o processo extrato ja foi enviado pra matera 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        ProcessoEnviadoMateraResponse ProcessoEnviadoMatera(ProcessoEnviadoMateraRequest req);

        /// <summary>
        /// Lista de refencias de domínio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarListaItemReferenciaResponse ListarListaItemReferencia(ListarListaItemReferenciaRequest request);

        /// <summary>
        /// 1328 - Lista do dominio TIPO DE ESTORNO 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarTipoEstornoResponse ListarTipoEstorno(ListarTipoEstornoRequest request); //<%--Jira 963 - Marcos Matsuoka--%>

        /// <summary>
        /// Recebe o detalhe de uma referência de domínio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberListaItemReferenciaResponse ReceberListaItemReferencia(ReceberListaItemReferenciaRequest request);

        /// <summary>
        /// Salva uma referencia de domínio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarListaItemReferenciaResponse SalvarListaItemReferencia(SalvarListaItemReferenciaRequest request);

        /// <summary>
        /// Remnove uma referencia de domínio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverListaItemReferenciaResponse RemoverListaItemReferencia(RemoverListaItemReferenciaRequest request);

        ListarRelatorioConciliacaoContabilResponse ListarRelatorioConciliacaoContabil(ListarRelatorioConciliacaoContabilRequest request);

        GerarRelatorioConciliacaoContabilExcelResponse GerarRelatorioConciliacaoContabilExcel(GerarRelatorioConciliacaoContabilExcelRequest request);

        ListarProdutoPlanoResponse ListarProdutoPlano(ListarProdutoPlanoRequest request);

        /// <summary>
        /// Lista as configurações de listas fixas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarListaConfiguracaoResponse ListarListaConfiguracao(ListarListaConfiguracaoRequest request);

        /// <summary>
        /// Salva as configurações de listas fixas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarListaConfiguracaoResponse SalvarListaConfiguracao(SalvarListaConfiguracaoRequest request);

        /// <summary>
        /// Recebe uma configuração de lista fixa
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberListaConfiguracaoResponse ReceberListaConfiguracao(ReceberListaConfiguracaoRequest request);

        /// <summary>
        /// Remove uma configuração de lista fixa
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RemoverListaConfiguracaoResponse RemoverListaConfiguracao(RemoverListaConfiguracaoRequest request);

        ListarListaItemConfiguracaoResponse ListarListaItemConfiguracao(ListarListaItemConfiguracaoRequest request);

        SalvarListaItemConfiguracaoResponse SalvarListaItemConfiguracao(SalvarListaItemConfiguracaoRequest request);

        SalvarLogResponse SalvarLog(SalvarLogRequest request);

        ListarLayoutResponse ListarLayouts(ListarLayoutRequest request);

        #region TaxaRecebimento
        ListarTaxaRecebimentoResponse ListarTaxaRecebimento(ListarTaxaRecebimentoRequest request);
        ReceberTaxaRecebimentoResponse ReceberTaxaRecebimento(ReceberTaxaRecebimentoRequest request);
        RemoverTaxaRecebimentoResponse RemoverTaxaRecebimento(RemoverTaxaRecebimentoRequest request);
        SalvarTaxaRecebimentoResponse SalvarTaxaRecebimento(SalvarTaxaRecebimentoRequest request);
        GerarRelatorioRecebimentoExcelResponse GerarRelatorioRecebimentoExcel(GerarRelatorioRecebimentoExcelRequest request);
        GerarRelatorioRecebimentoPDFResponse GerarRelatorioRecebimentoPDF(GerarRelatorioRecebimentoPDFRequest request);
        ListarRelatorioRecebimentoResponse ListarRelatorioRecebimento(ListarRelatorioRecebimentoRequest request);
        #endregion

        #region TaxaInterChange
        GerarRelatorioInterchangeExcelResponse GerarRelatorioInterchangeExcel(GerarRelatorioInterchangeExcelRequest request);
        ListarRelatorioInterchangeResponse ListarRelatorioInterchange(ListarRelatorioInterchangeRequest request);
        #endregion
        
     }
}
