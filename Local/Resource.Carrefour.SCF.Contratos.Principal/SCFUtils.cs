﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    public class SCFUtils
    {
        public static string CompletarCNPJ(string pCNPJ)
        {
            string lCnpj = pCNPJ.Trim();

            while (lCnpj.Length < 15)
            {
                lCnpj = lCnpj.Insert(0, "0");
            }

            return lCnpj;
        }
    }
}
