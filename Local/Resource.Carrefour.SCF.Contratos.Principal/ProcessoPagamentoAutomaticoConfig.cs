﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Configuracoes do processo de pagamento
    /// </summary>
    public class ProcessoPagamentoAutomaticoConfig
    {
        /// <summary>
        /// Caminho do arquivo de saida matera financeiro 
        /// </summary>
        public string CaminhoArquivoMateraFinanceiro { get; set; }

        /// <summary>
        /// Envia arquivo matera somente do processo Extrato
        /// </summary>
        public bool ProcessarExtrato { get; set; }

        /// <summary>
        /// Envia arquivo matera somento do processo Atacadão
        /// </summary>
        public bool ProcessarAtacadao { get; set; }
    }
}
