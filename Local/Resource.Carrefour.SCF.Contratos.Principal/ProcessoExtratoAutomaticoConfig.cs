﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Configuracoes do processo extrato
    /// </summary>
    public class ProcessoExtratoAutomaticoConfig
    {
        /// <summary>
        /// Caminho do arquivo TSYS Carrefour
        /// </summary>
        public string CaminhoArquivoTSYS { get; set; }

        /// <summary>
        /// Caminho do arquivo TSYS Atacadão
        /// </summary>
        public string CaminhoArquivoTSYSAtacadao { get; set; }

        /// <summary>
        /// Caminho do arquivo TSYS Galeria
        /// </summary>
        public string CaminhoArquivoTSYSGaleria { get; set; }

        /// <summary>
        /// Caminho do arquivo CCI
        /// </summary>
        public string CaminhoArquivoCCI { get; set; }

        /// <summary>
        /// Caminho do arquivo ATA
        /// </summary>
        public string CaminhoArquivoATA { get; set; }

        /// <summary>
        /// Caminho do arquivo GAL
        /// </summary>
        public string CaminhoArquivoGAL { get; set; }
    }
}
