﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Interface do servico agendador
    /// </summary>
    public interface IServicoAgendador
    {
     
        #region Agendador

        
        /// <summary>
        /// Solicita o inicio do servico agendador
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        IniciarServicoAgendadorResponse IniciarServicoAgendador(IniciarServicoAgendadorRequest request);
        

        /// <summary>
        /// Solicita a finalizacao do servico de parametrizacoes agendadas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        PararServicoAgendadorResponse PararServicoAgendador(PararServicoAgendadorRequest request);


        /// <summary>
        /// Solicita informacoes sobre o servico de parametrizacoes agendadas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberStatusServicoAgendadorResponse ReceberStatusServicoAgendador(ReceberStatusServicoAgendadorRequest request);



        #endregion

     
    }
}
