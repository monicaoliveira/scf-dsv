﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Configurações do serviço de intregração
    /// </summary>
    public class ServicoIntegracaoConfig
    {
        /// <summary>
        /// Código do atacadão no arquivo da CSU
        /// </summary>
        public string CodigoLojaAtacadaoCSU { get; set; }

        /// <summary>
        /// Código do atacadão no arquivo da TSYS
        /// </summary>
        public string CodigoLojaAtacadaoTSYS { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoIntegracaoConfig()
        {
            this.CodigoLojaAtacadaoCSU = "2003000023";
            this.CodigoLojaAtacadaoTSYS = "045543915000262";
        }
    }
}
