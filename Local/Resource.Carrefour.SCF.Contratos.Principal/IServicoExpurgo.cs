﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Interface do servico agendador
    /// </summary>
    public interface IServicoExpurgo
    {
     
        #region Expurgo

        
        /// <summary>
        /// Solicita o inicio do servico agendador
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        IniciarServicoExpurgoResponse IniciarServicoExpurgo(IniciarServicoExpurgoRequest request);
        

        /// <summary>
        /// Solicita a finalizacao do servico de parametrizacoes agendadas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        PararServicoExpurgoResponse PararServicoExpurgo(PararServicoExpurgoRequest request);


        /// <summary>
        /// Solicita informacoes sobre o servico de parametrizacoes agendadas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberStatusServicoExpurgoResponse ReceberStatusServicoExpurgo(ReceberStatusServicoExpurgoRequest request);



        #endregion

     
    }
}
