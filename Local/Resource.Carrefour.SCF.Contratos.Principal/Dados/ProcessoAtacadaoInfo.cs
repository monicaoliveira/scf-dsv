﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações do processo atacadão
    /// </summary>
    [Serializable]
    public class ProcessoAtacadaoInfo : ProcessoInfo
    {
        /// Explicacao sobre a estrutura de diretorios do arquivos
        /// 
        /// O serviço pressupõe que a utilizacao dos diretorios da seguinte maneira:
        /// DiretorioPrincipal -> Contem o arquivo a ser processado
        /// DiretorioPrincipal\EmProcessamento -> Contem o arquivo que está sendo processado no momento
        /// DiretorioPrincipal\Processados -> Contem os arquivos processados
        /// 
        /// Ao iniciar serviço de agendamento, ele verifica se os diretórios existem
        /// *** Estas regras não foram aplicadas ao processamento manual

        /// <summary>
        /// Caminho do arquivo TSYS que será importado
        /// </summary>
        public string CaminhoArquivoTSYS { get; set; }

        /// <summary>
        /// Caminho do arquivo CSU que será importado
        /// </summary>
        public string CaminhoArquivoCSU { get; set; }

        /// <summary>
        /// Caminho do arquivo CSU sem Atacadão para onde será exportado
        /// Deve ser passado apenas o diretorio, pois o sistema irá montar o nome do arquivo
        /// </summary>
        public string CaminhoArquivoCSUSemAtacadao { get; set; }


        /// <summary>
        /// Caminho do arquivo Sitef que será importado
        /// </summary>
        public string CaminhoArquivoSitef { get; set; }

        /// <summary>
        /// Caminho do arquivo Matera Financeiro que será gerado 
        /// Deve ser passado apenas o diretorio, pois o sistema irá montar o nome do arquivo
        /// </summary> 
        public string CaminhoArquivoMateraFinanceiro { get; set; }


        /// <summary>
        /// Caminho do arquivo Matera Contabil que será gerado
        /// Deve ser passado apenas o diretorio, pois o sistema irá montar o nome do arquivo
        /// </summary>
        public string CaminhoArquivoMateraContabil { get; set; }


        /// <summary>
        /// Caminho do arquivo do resultado da conciliacao
        /// Deve ser passado apenas o diretorio, pois o sistema irá montar o nome do arquivo
        /// </summary>
        public string CaminhoArquivoResultadoConciliacao { get; set; }

        /// <summary>
        /// Código do arquivo TSYS gerado
        /// </summary>
        public string CodigoArquivoTSYS { get; set; }

        /// <summary>
        /// Código do arquivo CSU gerado
        /// </summary>
        public string CodigoArquivoCSU { get; set; }

        /// <summary>
        /// Código do arquivo Sitef gerado
        /// </summary>
        public string CodigoArquivoSitef { get; set; }

        /// <summary>
        /// Código do arquivo Matera Financeiro gerado
        /// </summary>
        public string CodigoArquivoMateraFinanceiro { get; set; }

        /// <summary>
        /// Indica se é para mover o arquivo para Processado
        /// </summary>
        public bool MoverArquivoProcessado { get; set; }

        /// <summary>
        /// Data da geração do arquivo matera
        /// </summary>
        public DateTime? DataEnvioMatera { get; set; }

        /// <summary>
        /// Código do usuário Matera
        /// </summary>
        public string CodigoUsuarioEnvioMatera { get; set; }

        /// <summary>
        /// Atualiza o processo atual com informacoes de outro processo
        /// </summary>
        /// <param name="processoInfo"></param>
        public void Atualizar(ProcessoInfo processoInfo)
        {
            base.Atualizar(processoInfo);

            // atualiza os dados
            this.DataEnvioMatera = ((ProcessoAtacadaoInfo)processoInfo).DataEnvioMatera;
            this.CodigoUsuarioEnvioMatera = ((ProcessoAtacadaoInfo)processoInfo).CodigoUsuarioEnvioMatera;
        }
    }
}
