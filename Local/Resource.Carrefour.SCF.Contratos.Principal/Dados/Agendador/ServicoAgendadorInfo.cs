﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador
{
    /// <summary>
    /// Contem informações sobre o serviço agendador
    /// </summary>
    [Serializable]
    public class ServicoAgendadorInfo
    {
        public int QuantidadeInicializacoes { get; set; }
        public int QuantidadeExecucoes { get; set; }
        public ServicoAgendadorStatusEnum StatusServico { get; set; }
        public DateTime? DataProximaExecucao { get; set; }
        public string CodigoProcessoAtacadao { get; set; }
        public string CodigoProcessoExtrato { get; set; }
        public string CodigoProcessoRetonoCessao { get; set; }
        public string ObservacaoAtacadao { get; set; }
        public string ObservacaoExtrato { get; set; }
        public string ObservacaoRetornoCessao { get; set; }
        public string ObservacaoGeral { get; set; }
        public bool Validacoes { get; set; } //1350
        public string ValidacoesMsg { get; set; } //1350
        public ServicoAgendadorInfo()
        {
            this.StatusServico = ServicoAgendadorStatusEnum.Parado;
        }
    }
}