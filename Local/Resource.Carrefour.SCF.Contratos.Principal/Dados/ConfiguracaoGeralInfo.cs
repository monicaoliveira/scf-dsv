﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados 
{
    /// <summary>
    /// Contém informações sobre as configurações gerais do sistema
    /// </summary>
    [Serializable]
    public class ConfiguracaoGeralInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da configuração
        /// </summary>
        public string CodigoConfiguracaoGeral { get; set; }        
        
        /// <summary>
        /// Define o prazo de pagamento
        /// </summary>
        public string PrazoPagamento { get; set; }

        /// <summary>
        /// Quantidade de dias vencidos
        /// </summary>
        public string QuantidadeDiasVencidos { get; set; }

        /// <summary>
        /// Caminho do diretório para importação no TSYS CARREFOUR
        /// </summary>
        public string DiretorioImportacaoExtratoTSYSCarrefour { get; set; }

        /// <summary>
        /// Caminho do diretório para importação no TSYS ATACADAO
        /// </summary>
        public string DiretorioImportacaoExtratoTSYSAtacadao { get; set; }

        /// <summary>
        /// Caminho do diretório para importação no TSYS GALERIA
        /// </summary>
        public string DiretorioImportacaoExtratoTSYSGaleria { get; set; }

        /// <summary>
        /// Caminho do diretório para exportação no CCI
        /// </summary>
        public string DiretorioExportacaoCCI { get; set; }

        /// <summary>
        /// Caminho do diretório para exportação no ATA
        /// </summary>
        public string DiretorioExportacaoATA { get; set; }

        /// <summary>
        /// Caminho do diretório para exportação no GAL
        /// </summary>
        public string DiretorioExportacaoGAL { get; set; }

        /// <summary>
        /// Csminho do diretório para importação da cessão
        /// </summary>
        public string DiretorioImportacaoCessao { get; set; }

        /// <summary>
        /// Diretório para exportação do pagamento
        /// </summary>
        public string DiretorioExportacaoPagamento { get; set; }

        /// <summary>
        /// Verifica se é para enviar cessão e agenda
        /// </summary>
        public bool EnviarCessaoAgenda { get; set; }

        /// <summary>
        /// Verifica se é para enviar negativos
        /// </summary>
        public bool EnviarNegativo { get; set; }

        /// <summary>
        /// Diretório para importação do arquivo TSYS - Atacadao
        /// </summary>
        public string DiretorioImportacaoAtacadaoTSYS { get; set; }

        /// <summary>
        /// Diretório para importação do arquivo MA - Atacadao
        /// </summary>
        public string DiretorioImportacaoAtacadaoMA { get; set; }

        /// <summary>
        /// Diretório para importacao do arquivo Sitef - atacadao 
        /// </summary>
        public string DiretorioImportacaoAtacadaoSITEF { get; set; }


        /// <summary>
        /// Diretório para exportação do arquivo de conciliacao - Atacadao
        /// </summary>
        public string DiretorioExportacaoAtacadaoConciliacao { get; set; }

        /// <summary>
        /// Diretório para exportação do arquivo de csu sem atacadao - Atacadao
        /// </summary>
        public string DiretorioExportacaoAtacadaoCSUSemAtacadao { get; set; }

        /// <summary>
        /// Hora de processamento dos arquivos do atacadao
        /// </summary>
        public String HoraLeituraDeAtacadao { get; set; }

        /// <summary>
        /// Hora ate processamento dos arquivos do atacadao
        /// </summary>
        public String HoraLeituraAteAtacadao { get; set; }


        ///// <summary>
        ///// Verifica se é para enviar matera automaticamente
        ///// </summary>
        //public bool EnviarMatera { get; set; }

        ///// <summary>
        ///// Verifica se é para enviar cessão automaticamente
        ///// </summary>
        //public bool EnviarCessao { get; set; }

        /// <summary>
        /// Hora de leitura no TSYS
        /// </summary>
        public String  HoraLeituraDeTSYS { get; set; }

        /// <summary>
        /// Hora de leitura no TSYS
        /// </summary>
        public String HoraLeituraAteTSYS { get; set; }

        /// <summary>
        /// Hora de leitura no CCI
        /// </summary>
        public String HoraLeituraDeCCI { get; set; }
        public String HoraLeituraAteCCI { get; set; }


        /// <summary>
        /// Diretorio de exportacao do arquivo matera contabil
        /// </summary>
        public string DiretorioExportacaoMateraContabil { get; set; }

        /// <summary>
        /// Diretorio de exportacao do arquivo matera financeiro (Atacadão)
        /// </summary>
        public string DiretorioExportacaoMateraFinanceiro { get; set; }

        /// <summary>
        /// Diretorio de exportacao do arquivo matera extrato
        /// </summary>
        public string DiretorioExportacaoMateraExtrato { get; set; }

        /// <summary>
        /// Indicador de envio arquivo cessao automaticamente para Carrefour
        /// /// </summary>
        public bool EnviarCessaoAutomatico { get; set; }

        /// <summary>
        /// Indicador de envio arquivo cessao automaticamente para Atacadão
        /// /// </summary>
        public bool EnviarCessaoAutomaticoAtacadao { get; set; }

        /// <summary>
        /// Indicador de envio arquivo cessao automaticamente para Galeria
        /// /// </summary>
        public bool EnviarCessaoAutomaticoGaleria { get; set; }

        /// <summary>
        /// Indicador de envio arquivo cessao automaticamente
        /// </summary>
        public bool EnviarMateraFinanceiroAutomatico { get; set; }

        /// <summary>
        /// Diretório para exportação do relatório de transação
        /// </summary>
        public string DiretorioExportacaoRelatorioTransacao { get; set; }

        /// <summary>
        /// Diretório para exportação do relatório de transação
        /// </summary>
        public string DiretorioExportacaoRelatorioVenda { get; set; }

        /// <summary>
        /// Diretório para exportação do relatório de transação
        /// </summary>
        public string DiretorioExportacaoRelatorioVencimento { get; set; }

        /// <summary>
        /// Diretório para exportação do relatório consolidado
        /// </summary>
        public string DiretorioExportacaoRelatorioConsolidado { get; set; }

        /// <summary>
        /// Diretório para exportação do relatório consolidado
        /// </summary>
        public string DiretorioExportacaoRelatorioSaldoConciliacao { get; set; }

        /// <summary>
        /// Diretório para exportação do relatório recebimento
        /// </summary>
        public string DiretorioExportacaoRelatorioRecebimento { get; set; }
        public string DiretorioExportacaoRelatorioCheques { get; set; } //1465

        /// <summary>
        /// Homologação da TSYS
        /// </summary>
        public bool HomologacaoTSYS { get; set; }

        /// <summary>
        /// Indica se deve reprocessar registros com NSU ja processados
        /// </summary>
        public bool ReprocessarNSUProcessado { get; set; }

        /// <summary>
        /// Indica se deve reprocessar registros com NSU ja cessionado
        /// </summary>
        public bool ReprocessarNSUCessionado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ConfiguracaoGeralInfo()
        {
            this.CodigoConfiguracaoGeral = Guid.NewGuid().ToString();
        }

        /// <summary>
        ///  Status do derviço agendador
        /// </summary>
        public ServicoAgendadorStatusEnum StatusServicoAgendador { get; set; }
        public ServicoExpurgoStatusEnum StatusServicoExpurgo { get; set; }
        public int DiasIgnorarExpurgo { get; set; }
        public int DiasAguardarExpurgo { get; set; }
        public DateTime? DataUltimoExpurgo { get; set; }
        //SCF1120 - Marcos Matsuoka - inicio
        public DateTime? ProxExecucao { get; set; }
        public DateTime? DePeriodo { get; set; }
        public DateTime? AtePeriodo { get; set; }
        //SCF1120 - Marcos Matsuoka - fim
        /// <summary>
        ///  Layout do arquivo de Extrato a ser exportado para a empresa Carrefour
        /// </summary>
        public string LayoutExportacaoCCI { get; set; }
        /// <summary>
        ///  Layout do arquivo de Extrato a ser exportado para a empresa Atacadão
        /// </summary>
        public string LayoutExportacaoATA { get; set; }
        /// <summary>
        ///  Layout do arquivo de Extrato a ser exportado para a empresa Galeria
        /// </summary>
        public string LayoutExportacaoGAL { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoConfiguracaoGeral;
        }

        #endregion
    }
}
