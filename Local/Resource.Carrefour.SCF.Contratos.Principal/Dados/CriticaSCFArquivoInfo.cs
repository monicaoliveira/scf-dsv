﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Representa uma crítica de arquivo
    /// </summary>
    public class CriticaSCFArquivoInfo : CriticaInfo
    {
        /// <summary>
        /// Código do arquivo a que a crítica se refere
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do processo associado
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
