﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status do Retorno de Cessao da Transacao
    /// </summary>,
    public enum TransacaoStatusRetornoCessaoEnum
    {
        NaoRetornado = 0,
        RetornadoOK = 1,
        RetornoNaoIdentificado = 2,
        ProdutoNaoCedivel = 3,
        TransacaoJaLiquidada = 4
    }
}
