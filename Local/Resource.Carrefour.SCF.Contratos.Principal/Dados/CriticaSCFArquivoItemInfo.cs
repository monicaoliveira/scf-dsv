﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Crítica de arquivoItem
    /// </summary>
    public class CriticaSCFArquivoItemInfo : CriticaSCFArquivoInfo
    {
        /// <summary>
        /// Código do ArquivoItem
        /// </summary>
        public string CodigoArquivoItem { get; set; }
    }
}
