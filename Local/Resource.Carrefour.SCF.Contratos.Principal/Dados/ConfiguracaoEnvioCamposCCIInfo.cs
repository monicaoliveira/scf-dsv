﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre as configuracoes de tipo de registro
    /// </summary>
    [Serializable]
    public class ConfiguracaoEnvioCamposCCIInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do campo
        /// </summary>
        public string CodigoCampoEnvioCCI { get; set; }

        /// <summary>
        /// Descrição do Campo
        /// </summary>
        public string NomeCampoEnvioCCI  { get; set; }

        /// <summary>
        /// Flag indicativa de envio à CCI
        /// </summary>
        public bool EnviarCCI { get; set; }


        /// <summary>
        /// Construtor default
        /// </summary>
        public ConfiguracaoEnvioCamposCCIInfo()
        {
            this.CodigoCampoEnvioCCI = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoCampoEnvioCCI;
        }

        #endregion
    }
}
