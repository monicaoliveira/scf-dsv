﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de transacao
    /// </summary>
    public enum TransacaoStatusEnum
    {
        [Description("Importado")]
        Importado = 0,
        [Description("Enviado para Grupo")]
        EnviadoCCI = 1,
        [Description("Enviado para Matera")]
        EnviadoMatera = 2,
        [Description("Cancelado Online")]
        CanceladoOnline = 6,
        [Description("Não Enviado para Grupo por Regra")]
        NaoEnviadoPorRegra = 7
    }
}
