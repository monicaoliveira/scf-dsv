﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os produtos
    /// </summary>
    [Serializable]
    public class RelatorioConferenciaReferenciaInfo : ICodigoEntidade
    {

        public string Referencia { get; set; }

        public List<RelatorioConferenciaVendasInfo> Vendas { get; set; }


        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return null;
        }
    }
}
