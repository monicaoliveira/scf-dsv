﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ProcessoRelatorioChequesExcelInfo : ProcessoInfo //1465
    {
        public ListarProtocoloChequeRequest Param { get; set; }
    }
}
