﻿using Resource.Framework.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public class LayoutInfo : ICodigoEntidade
    {
        public int CodigoLayout { get; set; }

        public string NomeLayout { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoLayout.ToString();
        }

        #endregion
    }
}
