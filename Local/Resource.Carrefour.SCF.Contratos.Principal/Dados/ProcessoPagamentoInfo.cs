﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações do processo pagamento
    /// </summary>
    [Serializable]
    public class ProcessoPagamentoInfo : ProcessoInfo
    {
        /// <summary>
        /// Caminho de geracao do arquivo financeiro da Matera
        /// </summary>
        public string CaminhoArquivoMateraFinanceiro { get; set; }

        /// <summary>
        /// Código do arquivo de exportacao matera financeiro
        /// </summary>
        public string CodigoArquivoMateraFinanceiro { get; set; }

        /// <summary>
        /// Data de envio ao Matera
        /// </summary>
        public DateTime? DataEnvioMatera { get; set; }

        /// <summary>
        /// Usuário de envio ao Matera
        /// </summary>
        public string CodigoUsuarioEnvioMatera { get; set; }

        /// <summary>
        /// Indica se deve enviar pagamentos do processo Extrato
        /// </summary>
        public bool ProcessarExtrato { get; set; }

        /// <summary>
        /// Indica se deve enviar pagamentos do processo Atacadão
        /// </summary>
        public bool ProcessarAtacadao { get; set; }

        /// <summary>
        /// Atualiza o processo atual com informacoes de outro processo
        /// </summary>
        /// <param name="processoInfo"></param>
        public void Atualizar(ProcessoInfo processoInfo)
        {
            base.Atualizar(processoInfo);

            // atualiza os dados
            this.CodigoUsuarioEnvioMatera = ((ProcessoPagamentoInfo)processoInfo).CodigoUsuarioEnvioMatera;
            this.DataEnvioMatera = ((ProcessoPagamentoInfo)processoInfo).DataEnvioMatera;
            this.ProcessarExtrato = ((ProcessoPagamentoInfo)processoInfo).ProcessarExtrato;
            this.ProcessarAtacadao = ((ProcessoPagamentoInfo)processoInfo).ProcessarAtacadao;
        }
    }
}
