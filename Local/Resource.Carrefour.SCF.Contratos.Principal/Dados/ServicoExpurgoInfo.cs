﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contem informações sobre o serviço agendador
    /// </summary>
    [Serializable]
    public class ServicoExpurgoInfo
    {
        /// <summary>
        /// Status do servico
        /// </summary>
        public ServicoExpurgoStatusEnum StatusServico { get; set; }

        /// <summary>
        /// Data da proxima execucao do servico, para verificar os diretorios
        /// </summary>
        public Nullable<DateTime> DataProximaExecucao { get; set; }

        public Nullable<DateTime> DataUltimaExecucao { get; set; }

        /// <summary>
        /// Observacoes gerais do servico
        /// </summary>
        public string ObservacaoGeral { get; set; }

        /// <summary>
        /// Informação para logar os eventos do expurgo
        /// </summary>
        public string ObservacaoExpurgo { get; set; }

        /// <summary>
        /// Código do processo criado
        /// </summary>
        public string CodigoProcessoExpurgo { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoExpurgoInfo()
        {
            this.StatusServico = ServicoExpurgoStatusEnum.Parado;
        }
    }
}