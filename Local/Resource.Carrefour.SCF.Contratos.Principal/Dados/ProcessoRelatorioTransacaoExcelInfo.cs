﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contem informações para o processo de relatório de transacoes
    /// </summary>
    public class ProcessoRelatorioTransacaoExcelInfo : ProcessoInfo
    {
        /// <summary>
        /// Filtro para a geração do relatório
        /// </summary>
        public ObjetoJsonRelTransacoesInfo Param { get; set; }
    }
}
