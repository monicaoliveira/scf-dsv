﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de Validação de Transacao
    /// </summary>
    public enum TransacaoStatusValidacaoEnum
    {
        PendenteValidacao = 0,
        Valido = 1,
        Invalido = 2
    }
}
