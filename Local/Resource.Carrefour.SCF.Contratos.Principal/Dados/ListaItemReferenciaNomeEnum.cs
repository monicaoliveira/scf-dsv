﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public enum ListaItemReferenciaNomeEnum
    {
        MeioPagamento,
        Produto,
        TipoAjuste,
        Referencia // ClockWork - Resource - Ago/16 - Margarida Vitolo
    }
}
