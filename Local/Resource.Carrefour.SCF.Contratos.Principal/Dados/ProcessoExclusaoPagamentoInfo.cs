﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ProcessoExclusaoPagamentoInfo : ProcessoInfo
    {
        /// <summary>
        /// Código do Pagamento a ser removido
        /// </summary>
        public string CodigoPagamento { get; set; }
       

        /// <summary>
        /// Atualiza o processo atual com informacoes de outro processo
        /// </summary>
        /// <param name="processoInfo"></param>
        public void Atualizar(ProcessoInfo processoInfo)
        {
            base.Atualizar(processoInfo);

           

        }
    }
}
