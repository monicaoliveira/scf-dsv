﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using System.Xml.Serialization;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Classe base dos infos de processo
    /// </summary>
    [Serializable]
    public class ProcessoResumoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Tipo do estágio atual
        /// </summary>
        public string TipoEstagioAtual { get; set; }

        /// <summary>
        /// Tipo do processo
        /// </summary>
        public string TipoProcesso { get; set; }

        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Código do Sessao - Jira SCF1070 - Marcos Matsuoka
        /// </summary>
        public string CodigoSessao { get; set; }

        /// <summary>
        /// Nome do arquivo do processo
        /// </summary>
        public string NomeArquivo { get; set; }

        /// <summary>
        /// Indica se o estágio atual é valido para prosseguir
        /// </summary>
        public bool? EstagioValido { get; set; }

        /// <summary>
        /// Status do processo
        /// </summary>
        public ProcessoStatusEnum StatusProcesso { get; set; }

        /// <summary>
        /// Status do estágio
        /// </summary>
        public EstagioStatusEnum StatusEstagio { get; set; }        

        /// <summary>
        /// Arquivo SITEF
        /// </summary>
        public string ArquivoCCI { get; set; }

        /// <summary>
        /// Data de alteração do estágio
        /// </summary>
        public DateTime? DataEstagio { get; set; }

        /// <summary>
        /// Data de inclusao do processo
        /// </summary>
        public DateTime? DataInclusao { get; set; }

        /// <summary>
        /// Data da ultima alteracao do status do processo
        /// </summary>
        public DateTime? DataStatus { get; set; }

        /// <summary>
        /// Status de linhas bloqueadas
        /// </summary>
        public ProcessoStatusBloqueioEnum StatusBloqueio { get; set; }

        /// <summary>
        /// Indica se já foi enviado MATERA ou não
        /// </summary>
        public bool EnviadoMatera { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ProcessoResumoInfo()
        {
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoProcesso;
        }

        #endregion
    }
}
