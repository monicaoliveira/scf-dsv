﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Representa um ArquivoItem
    /// </summary>
    [Serializable]
    public class ArquivoItemResumoInfo : ICodigoEntidade
    {
        public string CodigoArquivoItem { get; set; }
        public string CodigoArquivo { get; set; }
        public string CodigoProcesso { get; set; }  //1353
        public string TipoArquivo { get; set; }
        public string TipoArquivoItem { get; set; }
        public string ConteudoArquivoItem { get; set; }
        public string[] Valores { get; set; }
        public ArquivoItemStatusEnum StatusArquivoItem { get; set; }
        public DateTime? DataInclusao { get; set; } // 1308
        public ArquivoItemResumoInfo()
        {
            this.StatusArquivoItem = ArquivoItemStatusEnum.PendenteValidacao;
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoArquivoItem;
        }

        #endregion
    }
}
