﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações sobre bancos favorecidos
    /// </summary>
    [Serializable]
    public class FavorecidoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Teste para verificar quantidade de linhas
        /// </summary>
        public int Linha { get; set; }

        /// <summary>
        /// Código do favorecido
        /// </summary>
        public string CodigoFavorecido { get; set; }

        /// <summary>
        /// Nome do favorecido
        /// </summary>
        public string NomeFavorecido { get; set; }

        /// <summary>
        /// CNPJ favorecido
        /// </summary>
        public string CnpjFavorecido { get; set; }

        /// <summary>
        /// Indica se é o favorecido original
        /// </summary>
        public bool FavorecidoOriginal { get; set; }

        /// <summary>
        /// Indica se o favorecido está Bloqueado para edição
        /// </summary>
        public bool FavorecidoBloqueado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public FavorecidoInfo()
        {
            this.CodigoFavorecido = Guid.NewGuid().ToString();
        }
        
        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoFavorecido;
        }

        #endregion
    }
}
