﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Classe de transporte para o tipo do arquivo
    /// Ex: ArquivoCCI, ArquivoCSU, etc
    /// </summary>
    [Serializable]
    public class ArquivoTipoInfo
    {
        /// <summary>
        /// Indica o tipo do arquivo
        /// </summary>
        public string TipoArquivo { get; set; }

        /// <summary>
        /// Descricao do tipo do arquivo
        /// </summary>
        public string DescricaoTipoArquivo { get; set; }
    }
}
