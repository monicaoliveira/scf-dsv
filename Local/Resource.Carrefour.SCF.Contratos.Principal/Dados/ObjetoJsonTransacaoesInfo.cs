﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ObjetoJsonRelTransacoesInfo
    {
        public string CodigoArquivo { get; set; }
        public string TipoArquivo { get; set; }
        public string CodigoEmpresaGrupo { get; set; }
        public string NomeEmpresaGrupo { get; set; }
        public string CodigoEmpresaSubGrupo { get; set; }
        public string NomeEmpresaSubGrupo { get; set; }
        public string CodigoEstabelecimento { get; set; }
        public string RazaoSocial { get; set; }
        public string CodigoFavorecido { get; set; }
        public string NomeFavorecido { get; set; }
        public string CodigoProduto { get; set; }
        public string NomeProduto { get; set; }
        public string CodigoPlano { get; set; }
        public string NomePlano { get; set; }
        public string CodigoMeioCaptura { get; set; }
        public string MeioCaptura { get; set; }
        public string CodigoTipoRegistro { get; set; }
        public string TipoRegistro { get; set; }
        public string CodigoTipoTransacao { get; set; }
        public string TipoTransacao { get; set; }
        public string NumeroPagamento { get; set; }
        public string ContaCliente { get; set; } //1438
        public DateTime? DataProcessamentoDe { get; set; }
        public DateTime? DataProcessamentoAte { get; set; }
        public DateTime? DataMovimentoDe { get; set; }
        public DateTime? DataMovimentoAte { get; set; }
        public DateTime? DataTransacaoDe { get; set; }
        public DateTime? DataTransacaoAte { get; set; }
        public DateTime? DataVencimentoDe { get; set; }
        public DateTime? DataVencimentoAte { get; set; }
        public DateTime? DataAgendamentoDe { get; set; }
        public DateTime? DataAgendamentoAte { get; set; }
        public DateTime? DataEnvioMateraDe { get; set; }
        public DateTime? DataEnvioMateraAte { get; set; }
        public DateTime? DataLiquidacaoDe { get; set; }
        public DateTime? DataLiquidacaoAte { get; set; }
        public string CodigoStatusRetorno { get; set; }
        public string DescricaoStatusRetorno { get; set; }
        public string CodigoStatusPagamento { get; set; }
        public string DescricaoStatusPagamento { get; set; }
        public string CodigoStatusConciliacao { get; set; }
        public string DescricaoStatusConciliacao { get; set; }
        public string CodigoStatusValidacao { get; set; }
        public string DescricaoStatusValidacao { get; set; }
        // NR-033
        //Status da Transação
        public string CodigoStatusCancelamentoOnLine { get; set; }
        public string DescricaoStatusTransacaoCancelamentoOnline { get; set; }
        public DateTime? DataRetornoCessaoDe { get; set; }
        public DateTime? DataRetornoCessaoAte { get; set; }

        //ECOMMERCE - Fernando Bove - 20160105
        public string FiltroFinanceiroContabil { get; set; }

        //ECOMMERCE - Fernando Bove - 20160106
        public string FiltroNSUHost { get; set; }

        //ClockWork - Marcos Matsuoka
        //1312 public string FiltroReferencia { get; set; }
        //1312
        public string FiltroReferenciaCodigo { get; set; }
        public string FiltroReferenciaDescricao { get; set; }
    }
}
