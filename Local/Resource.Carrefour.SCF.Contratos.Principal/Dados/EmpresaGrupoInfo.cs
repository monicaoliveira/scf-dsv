﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
     /// <summary>
    /// Contém informações sobre os grupos
    /// </summary>
    [Serializable]
    public class EmpresaGrupoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do Grupo
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Nome do Grupo
        /// </summary>
        public string NomeEmpresaGrupo { get; set; }

        /// <summary>
        /// Descrição do Grupo
        /// </summary>
        public string DescricaoEmpresaGrupo { get; set; }

        /// <summary>
        /// Determina se deve Enviar Negativo do Grupo
        /// </summary>
        public string EnviarNegativoEmpresaGrupo { get; set; }
        
        /// <summary>
        /// Construtor default
        /// </summary>
        public EmpresaGrupoInfo()
        {
            this.CodigoEmpresaGrupo = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoEmpresaGrupo;
        }

        #endregion
    }
}
