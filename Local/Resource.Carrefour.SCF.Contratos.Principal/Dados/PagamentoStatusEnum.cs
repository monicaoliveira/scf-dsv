﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de pagamento
    /// </summary>
    public enum PagamentoStatusEnum
    {
        Pendente = 0,
        Enviado = 1,
        Excluido = 2,
        EmExclusao = 3 // status que indica que o pagamento esta em processamento de exclusao
    }
}
