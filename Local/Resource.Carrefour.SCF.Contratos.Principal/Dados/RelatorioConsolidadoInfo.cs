﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os produtos
    /// </summary>
    [Serializable]
    public class RelatorioConsolidadoInfo : ICodigoEntidade
    {
        /// <summary>
        /// ValorBrutoCV
        /// </summary>
        public Decimal ValorBrutoCV { get; set; }

        /// <summary>
        /// ValorBrutoAV
        /// </summary>
        public Decimal ValorBrutoAV { get; set; }

        /// <summary>
        /// ValorBrutoAJ
        /// </summary>
        public Decimal ValorBrutoAJ { get; set; }

        /// <summary>
        /// ValorComissaoCV
        /// </summary>
        public Decimal ValorComissaoCV { get; set; }

        /// <summary>
        /// ValorComissaoAV
        /// </summary>
        public Decimal ValorComissaoAV { get; set; }

        /// <summary>
        /// ValorComissaoAJ
        /// </summary>
        public Decimal ValorComissaoAJ { get; set; }

        /// <summary>
        /// TotalLiquido
        /// </summary>
        public Decimal TotalLiquido { get; set; }

        /// <summary>
        /// ValorCP
        /// </summary>
        public Decimal ValorCP { get; set; }

        /// <summary>
        /// ValorAP
        /// </summary>
        public Decimal ValorAP { get; set; }

        /// <summary>
        /// ValorAJ
        /// </summary>
        public Decimal ValorAJ { get; set; }

        /// <summary>
        /// ValorTotal
        /// </summary>
        public Decimal ValorTotal { get; set; }

        /// <summary>
        /// QtdeCV
        /// </summary>
        public int QtdeCV { get; set; }

        /// <summary>
        /// QtdeAV
        /// </summary>
        public int QtdeAV { get; set; }

        /// <summary>
        /// QtdeAJ
        /// </summary>
        public int QtdeAJ { get; set; }

        /// <summary>
        /// QtdeCP
        /// </summary>
        public int QtdeCP { get; set; }

        /// <summary>
        /// QtdeAP
        /// </summary>
        public int QtdeAP { get; set; }

        /// <summary>
        /// CNPJ
        /// </summary>
        public string CNPJ { get; set; }

        /// <summary>
        /// NomeFavorecido
        /// </summary>
        public string NomeFavorecido { get; set; }

        /// <summary>
        /// DataProcessamento
        /// </summary>
        public DateTime? DataProcessamento { get; set; }

        /// <summary>
        /// NomeProduto
        /// </summary>
        public string NomeProduto { get; set; }

        /// <summary>
        /// MeioPagamento
        /// </summary>
        public string MeioPagamento { get; set; }

        /// <summary>
        /// Código da Referência
        /// </summary>
        public string CodigoReferencia { get; set; }

        /// <summary>
        /// Nome da Referência
        /// </summary>
        public string NomeReferencia { get; set; }

        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return null;
        }
    }
}