﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de processo
    /// </summary>
    public enum ServicoExpurgoStatusEnum
    {
        Iniciado = 1,
        Parado = 0
    }
}
