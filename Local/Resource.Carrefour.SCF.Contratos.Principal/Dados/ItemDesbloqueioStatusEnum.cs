﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de arquivoItem
    /// </summary>
    public enum ItemDesbloqueioStatusEnum
    {
        [Description("Aguardando Processamento")]
        AguardandoProcessamento = 0,
        [Description("Processado")]
        Processado = 1,
        [Description("Desbloqueio Cancelado")]
        ItemDesbloqueioStatusEnum = 9
    }
}
