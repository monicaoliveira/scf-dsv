﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public class ContaFavorecidoProdutoInfo : ICodigoEntidade
    {
        public string ID { get; set; }

        public string CodigoContaFavorecido { get; set; }

        public string CodigoProduto { get; set; }

        public string NomeProduto { get; set; }

        public string Banco { get; set; }

        public string Agencia { get; set; }

        public string Conta { get; set; }

        // SCF-978 - Fernando - inclusao de agencia e conta CCI - Inicio
        public string Agencia_CCI { get; set; }
        public string Conta_CCI { get; set; }
        // SCF-978 - Fernando - inclusao de agencia e conta CCI - Fim

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoContaFavorecido;
        }

        #endregion

    }
}
