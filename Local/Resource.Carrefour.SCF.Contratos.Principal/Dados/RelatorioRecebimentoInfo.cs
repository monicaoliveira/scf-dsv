﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os produtos
    /// </summary>
    [Serializable]
    public class RelatorioRecebimentoInfo : ICodigoEntidade
    {
        /// <summary>
        /// NomeGrupo
        /// </summary>
        public string NomeGrupo { get; set; }

        /// <summary>
        /// DescricaoReferencia
        /// </summary>
        public string DescricaoReferencia { get; set; }

        /// <summary>
        /// DescricaoReferencia
        /// </summary>
        public string DescricaoMeioPagamento    { get; set; }

        /// <summary>
        /// DescricaoEmpresaGrupo
        /// </summary>
        public DateTime? DataAgendamento { get; set; }

        /// <summary>
        /// Estabelecimento
        /// </summary>
        public EstabelecimentoInfo Estabelecimento { get; set; }

        /// <summary>
        /// Valor Recebimento
        /// </summary>
        public Int32 QtdeRecebimento { get; set; }

        /// <summary>
        /// Valor Recebimento
        /// </summary>
        public Decimal ValorRecebimento { get; set; }

        /// <summary>
        /// Valor Total da Taxa Recebimento
        /// </summary>
        public Decimal ValorTaxaRecebimento { get; set; }

        /// <summary>
        /// Valor Taxa de Recebimento
        /// </summary>
        public Decimal TaxaRecebimento { get; set; }

        /// <summary>
        /// Tipo de Taxa utilizada, R$ ou %
        /// </summary>
        public string TipoTaxaRecebimento {get; set;}

        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return null;
        }
    }
}
