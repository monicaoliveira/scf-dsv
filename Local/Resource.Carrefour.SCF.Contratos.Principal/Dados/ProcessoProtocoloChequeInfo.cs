﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using System.Xml.Serialization;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public class ProcessoProtocoloChequeInfo : ICodigoEntidade //963 //1465
    {
        public DateTime? DataEstorno { get; set; }
        public DateTime? DataProcessamento { get; set; }
        public DateTime? DataRegProtocolo { get; set; }
        public Decimal Valor { get; set; }        
        public string EnvioCci { get; set; }
        public string NumeroProtocolo { get; set; }
        public string Estabelecimento { get; set; }
        public string StatusProtocolo { get; set; }
        public string ContaCliente { get; set; }
        public string TipoEstorno { get; set; }
        public string NsuHost { get; set; }
        public string NsuHostOriginal { get; set; }
        public string TipoTransacao { get; set; }
        public string CodigoTransacao{ get; set; }
        public ProcessoProtocoloChequeInfo()
        {
        }

        #region ICodigoEntidade Members
        public string ReceberCodigo()
        {   return null;
        }
        #endregion
    }
}
