﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ProcessoRelatorioConciliacaoContabilExcelInfo : ProcessoInfo
    {
        /// <summary>
        /// Filtro de data incial do relatório
        /// </summary>
        public DateTime FiltroDataInicioConciliacao { get; set; }

        /// <summary>
        /// Filtro de data final do relatório
        /// </summary>
        public DateTime FiltroDataFimConciliacao { get; set; }
    }
}
