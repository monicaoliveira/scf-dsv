﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os produtos
    /// </summary>
    [Serializable]
    public class RelatorioLogInfo : ICodigoEntidade
    {
        /// <summary>
        /// CodigoLog
        /// </summary>
        public Decimal CodigoLog { get; set; }

        /// <summary>
        /// DataLog
        /// </summary>
        public DateTime? DataLog { get; set; }

        /// <summary>
        /// DataBDLog
        /// </summary>
        public DateTime? DataBDLog { get; set; }

        /// <summary>
        /// TipoLog
        /// </summary>
        public string TipoLog { get; set; }

        /// <summary>
        /// CodigoUsuario
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// DescricaoLog
        /// </summary>
        public string DescricaoLog { get; set; }

        /// <summary>
        /// SerializacaoLog
        /// </summary>
        public string SerializacaoLog { get; set; }

        /// <summary>
        /// TipoOrigem
        /// </summary>
        public string TipoOrigem { get; set; }

        /// <summary>
        /// CodigoOrigem
        /// </summary>
        public Decimal CodigoOrigem { get; set; }
       
        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return null;
        }
    }
}