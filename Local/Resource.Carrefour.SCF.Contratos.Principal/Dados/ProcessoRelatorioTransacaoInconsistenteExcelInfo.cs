﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ProcessoRelatorioTransacaoInconsistenteExcelInfo : ProcessoInfo
    {
        /// <summary>
        /// Código do arquivo a no qual se deseja o relatório
        /// Esta informação é obrigatória
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Filtro por status da validação
        /// </summary>
        public string FiltroStatusValidacao { get; set; }
    }
}
