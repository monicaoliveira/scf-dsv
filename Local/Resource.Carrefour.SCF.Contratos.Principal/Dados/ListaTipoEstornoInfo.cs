﻿//<%--Jira 963 - Marcos Matsuoka--%>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ListaTipoEstornoInfo
    {
        public string CodigoListaItemConfiguracao { get; set; }

        public string CodigoListaItem { get; set; }

        public string CodigoLista { get; set; }

        public string NomeListaItem { get; set; }

        public string Configuracao { get; set; }

        public string Valor { get; set; }

        public bool Ativo { get; set; }

        public string ColunaConfiguravelHeader { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoListaItemConfiguracao;
        }

        #endregion
    }
}
