﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resource.Framework.Contratos.Comum.Dados;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public class UsuarioSCFInfo : UsuarioInfo
    {
        //public string CodigoUsuario { get; set; } //1437
        //public Nullable<DateTime> DataExpiracaoSenha { get; set; } //1437
        //public string Nome { get; set; } //1437
        public string Cpf { get; set; }
        public string Matricula { get; set; }
        public string Login { get; set; }
        public Boolean Bloqueado { get; set; }
        public Nullable<DateTime> DataBloqueadoInicio { get; set; }
        public Nullable<DateTime> DataBloqueadoFim { get; set; }
    }
}
