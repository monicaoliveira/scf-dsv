﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações do Favorecido Produto
    /// </summary>
    [Serializable]
    public class ProcessoFavorecidoProdutoSubstituirContaInfo : ProcessoInfo
    {
        // SCF1701 - ID 27
        public string CodigoFavorecido { get; set; }
        public List<ProcessoFavorecidoProdutoLista> ListaFavorecidoProduto { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ProcessoFavorecidoProdutoSubstituirContaInfo()
        {
            this.ListaFavorecidoProduto = new List<ProcessoFavorecidoProdutoLista>();
        }
    }

    public class ProcessoFavorecidoProdutoLista 
    {
        // SCF1701 - ID 27
        public string CodigoContaFavorecido { get; set; }
        public string CodigoProduto { get; set; }
        public string Referencia { get; set; }
        public int Transacoes { get; set; }
    }
}
