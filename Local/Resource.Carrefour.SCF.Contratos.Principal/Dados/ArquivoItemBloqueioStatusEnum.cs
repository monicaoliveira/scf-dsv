﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de arquivoItem (somente referentes a bloqueios)
    /// </summary>
    public enum ArquivoItemBloqueioStatusEnum
    {
        [Description("Bloqueado")]
        Bloqueado = ArquivoItemStatusEnum.Bloqueado,
        [Description("Pendente de desbloqueio")]
        PendenteDesbloqueio = ArquivoItemStatusEnum.PendenteDesbloqueio,
    }
}
