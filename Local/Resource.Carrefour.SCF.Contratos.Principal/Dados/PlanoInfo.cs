﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os planos
    /// </summary>
    [Serializable]
    public class PlanoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do plano
        /// </summary>
        public string CodigoPlano { get; set; }

        /// <summary>
        /// Código do plano TSYS
        /// </summary>
        public string CodigoPlanoTSYS { get; set; }

        /// <summary>
        /// Nome do plano
        /// </summary>
        public string NomePlano { get; set; }

        /// <summary>
        /// Descrição do plano
        /// </summary>
        public string DescricaoPlano { get; set; }

        /// <summary>
        /// Quantidade de parcelas do plano
        /// </summary>
        public int? QuantidadeParcelas { get; set; }

        /// <summary>
        /// Comissão do plano
        /// </summary>
        public double? Comissao { get; set; }

        /// <summary>
        /// Identifica se o plano está ativo ou inativo.
        /// </summary>
        public bool Ativo { get; set; }

        public List<string> Produtos { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public PlanoInfo()
        {
            this.CodigoPlano = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoPlano;
        }

        #endregion
    }
}
