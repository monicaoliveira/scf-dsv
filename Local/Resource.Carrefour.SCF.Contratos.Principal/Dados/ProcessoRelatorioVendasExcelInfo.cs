﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ProcessoRelatorioVendasExcelInfo : ProcessoInfo
    {
        /// <summary>
        /// Filtro para a geracao do relatório
        /// </summary>
        public ListarRelatorioConferenciaVendasRequest Param { get; set; }
    }
}
