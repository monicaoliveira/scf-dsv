﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ProcessoRelatorioRecebimentoExcelInfo : ProcessoInfo
    {
        /// <summary>
        /// Filtro para a geração do relatório
        /// </summary>
        public ListarRelatorioRecebimentoRequest Param { get; set; }
    }
}
