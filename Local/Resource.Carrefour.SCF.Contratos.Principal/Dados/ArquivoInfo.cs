﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Classe base dos infos de arquivo
    /// </summary>
    [Serializable]
    public class ArquivoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Nome do arquivo
        /// </summary>
        public string NomeArquivo { get; set; }

        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public string TipoArquivo { get; set; }

        /// <summary>
        /// Data de inclusão do arquivo
        /// </summary>
        public DateTime? DataInclusao { get; set; }

        /// <summary>
        /// Código do processo relacionado ao arquivo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Status do arquivo
        /// </summary>
        public ArquivoStatusEnum StatusArquivo { get; set; }

        /// <summary>
        /// Chave do arquivo
        /// </summary>
        public string ChaveArquivo { get; set; }

        /// <summary>
        /// Layout do arquivo
        /// </summary>
        public string LayoutArquivo { get; set; }

        /// <summary>
        /// Código da Empresa
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Sigla do Grupo
        /// </summary>
        public string SiglaGrupo { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ArquivoInfo()
        {
            this.CodigoArquivo = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoArquivo;
        }

        #endregion
    }
}
