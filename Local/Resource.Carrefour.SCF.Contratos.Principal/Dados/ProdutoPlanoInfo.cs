﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Definição de uma relação entre Produtos e Planos. Um produto pode ter vários 
    /// planos diferentes e um plano pode pertencer a vários produtos diferentes.
    /// </summary>
    [Serializable]
    public class ProdutoPlanoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do Produto
        /// </summary>
        public string codigoProduto;

        /// <summary>
        /// Código do Plano
        /// </summary>
        public string codigoPlano;

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return codigoPlano.ToString();
        }

        #endregion
    }
}
