﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de arquivo
    /// </summary>
    [Serializable]
    public enum ArquivoStatusEnum
    {
        Ativo = 0,
        Cancelado = 1,
        ProcessadoComInconsistencias = 2,
        Estornado = 3
    }
}
