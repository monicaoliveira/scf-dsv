﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações da Lista Item Referencia
    /// </summary>
    [Serializable]
    public class ListaItemReferenciaInfo : ICodigoEntidade
    {
        /// <summary>
        /// Codigo (ID)
        /// </summary>
        public string CodigoListaItemReferencia { get; set; }

        /// <summary>
        /// Código lista item
        /// </summary>
        public string CodigoListaItem { get; set; }

        /// <summary>
        /// Situação
        /// </summary>
        public string CodigoListaItemSituacao { get; set; }

        /// <summary>
        /// Nome da referência
        /// </summary>
        public string NomeReferencia { get; set; }

        /// <summary>
        /// Valor da referência
        /// </summary>
        public string ValorReferencia { get; set; }

        /// <summary>
        /// Lista item referente à referencia
        /// </summary>
        public ListaItemInfo listaItemInfo { get; set; }

        /// <summary>
        /// Lista do domínio
        /// </summary>
        public string DominioLista { get; set; }

        /// <summary>
        /// Valor do domínio na lista item
        /// </summary>
        public string DominioListaValor { get; set; }

        /// <summary>
        /// Dados do dominio
        /// </summary>
        public string Dominio { get; set; }

        /// <summary>
        /// Codigo da lista do valor de referencia
        /// </summary>
        public string CodigoValorReferencia { get; set; }

        /// <summary>
        /// Tipo de Ajuste
        /// </summary>
        public string TipoAjuste { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoListaItemReferencia;
        }

        #endregion
    }
}
