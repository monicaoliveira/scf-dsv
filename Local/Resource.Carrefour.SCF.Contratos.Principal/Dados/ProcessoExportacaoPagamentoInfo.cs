﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ProcessoExportacaoPagamentoInfo : ProcessoInfo
    {
        /// <summary>
        /// Caminho de geracao do arquivo financeiro da Matera
        /// </summary>
        public string CaminhoArquivoMateraFinanceiro { get; set; }

        /// <summary>
        /// Código do arquivo de exportacao matera financeiro
        /// </summary>
        public string CodigoArquivoMateraFinanceiro { get; set; }

        /// <summary>
        /// Data de envio ao Matera
        /// </summary>
        public DateTime? DataEnvioMatera { get; set; }

        /// <summary>
        /// Usuário de envio ao Matera
        /// </summary>
        public string CodigoUsuarioEnvioMatera { get; set; }

       

        /// <summary>
        /// Atualiza o processo atual com informacoes de outro processo
        /// </summary>
        /// <param name="processoInfo"></param>
        public void Atualizar(ProcessoInfo processoInfo) 
        {
            base.Atualizar(processoInfo);

            // atualiza os dados
            this.CodigoUsuarioEnvioMatera = ((ProcessoExportacaoPagamentoInfo)processoInfo).CodigoUsuarioEnvioMatera;
            this.DataEnvioMatera = ((ProcessoExportacaoPagamentoInfo)processoInfo).DataEnvioMatera;

        }
    }
}
