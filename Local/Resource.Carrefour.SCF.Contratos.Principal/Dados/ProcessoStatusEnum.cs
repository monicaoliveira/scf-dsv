﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de processo
    /// </summary>
    public enum ProcessoStatusEnum
    {
        EmAndamento = 0,
        Finalizado = 1,
        Estornado = 2,
        Cessionado = 3, // SCF1273
        Cancelado = 9
    }
}
