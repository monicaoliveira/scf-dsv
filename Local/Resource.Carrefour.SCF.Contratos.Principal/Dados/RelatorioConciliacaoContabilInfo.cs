﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações do relatório de conciliação contábil
    /// </summary>
    [Serializable]
    public class RelatorioConciliacaoContabilInfo : ICodigoEntidade
    {
        /// <summary>
        /// Status da conciliacao
        /// </summary>
        public TransacaoStatusConciliacaoEnum StatusConciliacao { get; set; }

        /// <summary>
        /// Saldo acumulado
        /// </summary>
        public decimal SaldoAcumulado { get; set; }

        /// <summary>
        /// Valor da transação
        /// </summary>
        public decimal ValorConciliacao { get; set; }

        /// <summary>
        /// Data da conciliação
        /// </summary>
        public DateTime? DataConciliacao { get; set; }

        /// <summary>
        /// Data da transação
        /// </summary>
        public DateTime DataTransacao { get; set; }

        /// <summary>
        /// NSU Host da transação
        /// </summary>
        public string NSUHost { get; set; }

        /// <summary>
        /// Código de autorização da transação
        /// </summary>
        public string CodigoAutorizacao { get; set; }

        /// <summary>
        /// Valor da transação
        /// </summary>
        public decimal ValorTransacao { get; set; }

        /// <summary>
        /// Sitef ou rede da transação
        /// </summary>
        public string SitefRede { get; set; }

        /// <summary>
        /// Código do processo da transação
        /// </summary>
        public int CodigoProcesso { get; set; }

        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return null;
        }
    }
}
