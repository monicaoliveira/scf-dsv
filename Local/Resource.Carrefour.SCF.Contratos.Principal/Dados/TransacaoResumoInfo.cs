﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Resumo de transacao
    /// </summary>
    public class TransacaoResumoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da transação
        /// </summary>
        public string CodigoTransacao { get; set; }

        /// <summary>
        /// CodigoProcesso
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// CodigoArquivo1
        /// </summary>
        public string CodigoArquivo1 { get; set; }

        /// <summary>
        /// CodigoArquivoItem1
        /// </summary>
        public string CodigoArquivoItem1 { get; set; }

        /// <summary>
        /// CodigoArquivo2
        /// </summary>
        public string CodigoArquivo2 { get; set; }

        /// <summary>
        /// CodigoArquivoItem2
        /// </summary>
        public string CodigoArquivoItem2 { get; set; }

        /// <summary>
        /// CodigoEstabelecimento
        /// </summary>
        public string CodigoEstabelecimento { get; set; }

        /// <summary>
        /// CodigoPagamento
        /// </summary>
        public string CodigoPagamento { get; set; }

        /// <summary>
        /// Chave1
        /// </summary>
        public string Chave1 { get; set; }

        /// <summary>
        /// Chave2
        /// </summary>
        public string Chave2 { get; set; }

        /// <summary>
        /// TipoTransacao
        /// </summary>
        public string TipoTransacao { get; set; }

        /// <summary>
        /// NsuHost
        /// </summary>
        public string NsuHost { get; set; }

        /// <summary>
        /// DataInclusao
        /// </summary>
        public DateTime? DataInclusao { get; set; }

        /// <summary>
        /// DataTransacao
        /// </summary>
        public DateTime? DataTransacao { get; set; }

        /// <summary>
        /// StatusTransacao
        /// </summary>
        public TransacaoStatusEnum StatusTransacao { get; set; }

        /// <summary>
        /// StatusConciliacao
        /// </summary>
        public TransacaoStatusConciliacaoEnum StatusConciliacao { get; set; }

        /// <summary>
        /// StatusRetornoCessao
        /// </summary>
        public TransacaoStatusRetornoCessaoEnum StatusRetornoCessao { get; set; }

        /// <summary>
        /// StatusValidacao
        /// </summary>
        public TransacaoStatusValidacaoEnum StatusValidacao { get; set; }

        /// <summary>
        /// CodigoProcessoRetornoCessao
        /// </summary>
        public string CodigoProcessoRetornoCessao { get; set; }

        /// <summary>
        /// CodigoArquivoRetornoCessao
        /// </summary>
        public string CodigoArquivoRetornoCessao { get; set; }

        /// <summary>
        /// DataRepasse
        /// </summary>
        public DateTime? DataRepasse { get; set; }

        /// <summary>
        /// DataRepasseCalculada
        /// </summary>
        public DateTime? DataRepasseCalculada { get; set; }

        /// <summary>
        /// CodigoProduto
        /// </summary>
        public string CodigoProduto { get; set; }

        /// <summary>
        /// CodigoFavorecidoOriginal
        /// </summary>
        public string CodigoFavorecidoOriginal { get; set; }

        /// <summary>
        /// CodigoFavorecido
        /// </summary>
        public string CodigoFavorecido { get; set; }

        /// <summary>
        /// CodigoContaFavorecidoOriginal
        /// </summary>
        public string CodigoContaFavorecidoOriginal { get; set; }

        /// <summary>
        /// CodigoContaFavorecido
        /// </summary>
        public string CodigoContaFavorecido { get; set; }

        /// <summary>
        /// Data do Movimento - Registro L0
        /// </summary>
        public DateTime? DataMovimento { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoTransacao;
        }

        #endregion
    }
}
