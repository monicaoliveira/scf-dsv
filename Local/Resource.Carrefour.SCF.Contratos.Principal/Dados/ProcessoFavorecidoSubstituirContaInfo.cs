﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações do Favorecido
    /// </summary>
    [Serializable]
    public class ProcessoFavorecidoSubstituirContaInfo : ProcessoInfo
    {
        // SCF1701 - ID 27
        public string CodigoFavorecido { get; set; }
        public string CodigoContaFavorecido { get; set; }
        public string CodigoContaFavorecidoNovo { get; set; }
    }
}
