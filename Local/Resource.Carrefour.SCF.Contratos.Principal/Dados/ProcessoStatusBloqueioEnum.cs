﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Indica se há registros bloqueados no processo
    /// </summary>
    public enum ProcessoStatusBloqueioEnum
    {
        NaoHaBloqueios = 0,
        HaBloqueios = 1
    }
}
