﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// Jira: SCF - 1520
    public class ProcessoRelatorioInterchangeExcelInfo : ProcessoInfo
    {
        /// <summary>
        /// Filtro para a geração do relatório
        /// </summary>
        public ListarRelatorioInterchangeRequest Param { get; set; }
    }
}
