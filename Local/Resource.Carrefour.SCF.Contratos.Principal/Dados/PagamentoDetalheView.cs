﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Complemento de informações de pagamento
    /// </summary>
    [Serializable]
    public class PagamentoDetalheView : PagamentoInfo
    {
        /// <summary>
        /// CNPJ do estabelecimento
        /// </summary>
        public string CNPJ { get; set; }

        /// <summary>
        /// Nome do favorecido
        /// </summary>
        public string NomeFavorecido { get; set; }

        /// <summary>
        /// Código do grupo
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Descrição do grupo
        /// </summary>
        public string DescricaoEmpresaGrupo { get; set; }

        /// <summary>
        /// Informacoes da referencia (apenas quando solicitar os pagamentos divididos por referencia)
        /// </summary>
        public double? ValorReferencia { get; set; }
        public string CodigoReferencia { get; set; }

        /// <summary>
        /// Informacoes da configuracao do Matera Financeiro
        /// </summary>
        public string CodigoTipoDocumentoMatera { get; set; }
        public string CodigoTipoProdutoServicoMatera { get; set; }
        public string CodigoContaContabil { get; set; }

    }
}
