﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações do processo atacadão
    /// </summary>

    [Serializable]
    public class ProcessoCessaoInfo : ProcessoInfo
    {
        /// <summary>
        /// Caminho do arquivo CCI que será importado
        /// </summary>
        public string CaminhoArquivoRetornoCessao { get; set; }

        /// <summary>
        /// Código do arquivo de retorno de cessao 
        /// </summary>
        public string CodigoArquivoRetCessao { get; set; }

        /// <summary>
        /// Indica se o arquivo deve ser movido para o diretorio de processado
        /// </summary>
        public bool MoverArquivoProcessado { get; set; }

        /// <summary>
        /// Código do usuário que pediu retorno de CCI
        /// </summary>
        public string CodigoUsuarioRetornoCCI { get; set; }

        /// <summary>
        /// Data do retorno do CCI
        /// </summary>
        public DateTime? DataRetornoCCI { get; set; }


        /// <summary>
        /// Atualiza o processo atual com informacoes de outro processo
        /// </summary>
        /// <param name="processoInfo"></param>
        public void Atualizar(ProcessoInfo processoInfo)
        {
            base.Atualizar(processoInfo);

            // atualiza os dados
            this.CodigoUsuarioRetornoCCI = ((ProcessoCessaoInfo)processoInfo).CodigoUsuarioRetornoCCI;
            this.DataRetornoCCI = ((ProcessoCessaoInfo)processoInfo).DataRetornoCCI;
        }
    }
}
