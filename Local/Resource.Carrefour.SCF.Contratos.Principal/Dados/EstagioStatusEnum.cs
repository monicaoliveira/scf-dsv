﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de Estagio
    /// </summary>
    public enum EstagioStatusEnum
    {
        Parado = 0,
        EmExecucao = 1,
        EmCancelamento = 2,
        Finalizado = 3,
        EmValidacao = 4,
        AguardandoValidacao = 5,
        Erro = 9
    }
}
