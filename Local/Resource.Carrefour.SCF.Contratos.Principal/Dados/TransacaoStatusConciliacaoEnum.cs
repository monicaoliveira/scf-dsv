﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de conciliacao
    /// </summary>
    public enum TransacaoStatusConciliacaoEnum
    {
        NaoConciliado = 0,
        Conciliado = 1
    }
}
