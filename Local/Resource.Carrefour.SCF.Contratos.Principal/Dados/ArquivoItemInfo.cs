﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public class ArquivoItemInfo : ICodigoEntidade
    {
        public string CodigoArquivoItem { get; set; }
        public string CodigoArquivo { get; set; }
        public string TipoArquivoItem { get; set; }
        public string ConteudoArquivoItem { get; set; }
        public ArquivoItemStatusEnum StatusArquivoItem { get; set; }
        public string ReferenciaBinArquivoItem { get; set; }
        public DateTime? DataInclusaoInicial { get; set; } //1308
        public DateTime? DataInclusaoFinal { get; set; } //1308

        public ArquivoItemInfo()
        {
            this.StatusArquivoItem = ArquivoItemStatusEnum.PendenteValidacao;
        }
        #region ICodigoEntidade Members
        public string ReceberCodigo()
        {
            return this.CodigoArquivoItem;
        }
        #endregion
    }
}
