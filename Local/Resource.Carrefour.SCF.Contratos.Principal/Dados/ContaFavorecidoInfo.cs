﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public class ContaFavorecidoInfo : ICodigoEntidade
    {
        public string CodigoContaFavorecido { get; set; }
        public string CodigoFavorecido { get; set; }
        public string Conta { get; set; }
        public string Agencia { get; set; }
        public string Banco { get; set; }
        public string Agencia_CCI { get; set; } //978
        public string Conta_CCI { get; set; } //978
        public string Referencia { get; set; } //1339
        public string Transacoes { get; set; } //1339

        public ContaFavorecidoInfo()
        {
            this.CodigoContaFavorecido = Guid.NewGuid().ToString();
        }
        
        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoContaFavorecido;
        }

        #endregion
    }
}
