﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Status de arquivoItem
    /// </summary>
    public enum ArquivoItemStatusEnum
    {
        [Description("Pendente de validação")]
        PendenteValidacao = 0,
        [Description("Válido")]
        Valido = 1,
        [Description("Inválido")]
        Invalido = 2,
        [Description("Bloqueado")]
        Bloqueado = 3,
        [Description("Excluído")]
        Excluido = 4,
        [Description("Pendente de desbloqueio")]
        PendenteDesbloqueio = 5,
        [Description("Cancelado")]
        Cancelado = 6,
        [Description("NaoEnviadoPorRegra")]
        NaoEnviadoPorRegra = 7,
        [Description("EnviadoGrupo")]
        EnviadoGrupo = 8
    }
}
