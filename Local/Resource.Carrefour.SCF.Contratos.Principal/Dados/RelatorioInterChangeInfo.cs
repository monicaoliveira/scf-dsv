﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{

    /// Jira: SCF - 1520
    [Serializable]
    public class RelatorioInterChangeInfo :ICodigoEntidade
    {
        /// <summary>
        /// NomeGrupo
        /// </summary>
        public string NomeGrupo { get; set; }

        /// <summary>
        /// Referencia
        /// </summary>
        public string Referencia { get; set; }

        /// <summary>
        /// Data de processamento
        /// </summary>
        public DateTime? DataProcessamento { get; set; }

        /// <summary>
        /// Tipo da Transacao
        /// </summary>
        public string TipoTransacao { get; set; }

        /// <summary>
        /// Codigo do Produto
        /// </summary>
        public string CodigoProduto { get; set; }

        /// <summary>
        /// Codigo do Plano
        /// </summary>
        public string CodigoPlano { get; set; }

        /// <summary>
        /// CNPJ da Loja
        /// </summary>
        public EstabelecimentoInfo Estabelecimento { get; set; }

        /// <summary>
        /// Número da autorização da transação
        /// </summary>
        public string NumeroAutorizacaoTransacao { get; set; }

        /// <summary>
        /// Quantidade de Parcelas
        /// </summary>
        public string QuantidadeParcelas { get; set; }

        /// <summary>
        /// Data e Hora da Transacao
        /// </summary>
        public DateTime? DataTransacao { get; set; }

        /// <summary>
        /// Numero do NSU_HOST
        /// </summary>
        public string Numero_NSU_HOST { get; set; }

        /// <summary>
        /// Numero da Conta do Cliente (TSYS)
        /// </summary>
        public string NumeroContaClienteTSYS { get; set; }

        /// <summary>
        /// Valor Bruto da Venda
        /// </summary>
        public decimal ValorBrutoVenda { get; set; }

        /// <summary>
        /// Valor da Comissão Aplicado
        /// </summary>
        public decimal ValorComissaoAplicado { get; set; }

        /// <summary>
        /// Valor Líquido
        /// </summary>
        public decimal ValorLiquido { get; set; }

        /// <summary>
        /// Taxa TSYS %
        /// </summary>
        public decimal TaxaTSYS { get; set; }

        /// <summary>
        /// Valor da Comissão SCF
        /// </summary>
        public decimal ValorComissaoSCF { get; set; }

        /// <summary>
        /// Taxa Interchange
        /// </summary>
        public decimal? TaxaInterchange { get; set; }

        /// <summary>
        /// Vigência Inical
        /// </summary>
        public DateTime? VigenciaInical { get; set; }

        /// <summary>
        /// Vigência Final
        /// </summary>
        public DateTime? VigenciaFinal { get; set; }

        /// <summary>
        /// Diferença
        /// </summary>
        public decimal Diferenca { get; set; }


        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return null;
        }

    }
}
