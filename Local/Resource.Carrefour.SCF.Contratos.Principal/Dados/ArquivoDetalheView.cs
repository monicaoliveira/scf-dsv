﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações detalhadas de arquivo
    /// </summary>
    [Serializable]
    public class ArquivoDetalheView : ArquivoInfo
    {
        /// <summary>
        /// Quantidade de linhas do arquivo
        /// </summary>
        public int QuantidadeLinhas { get; set; }

        /// <summary>
        /// Quantidade de críticas do arquivo
        /// </summary>
        public int QuantidadeCriticas { get; set; }

        /// <summary>
        /// Status do Processo
        /// </summary>
        public ProcessoStatusEnum StatusProcesso { get; set; }
    }
}
