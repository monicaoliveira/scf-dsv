﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informacoes de pagamento
    /// </summary>
    [Serializable]
    public class PagamentoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Codigo do pagamento
        /// </summary>
        public string CodigoPagamento { get; set; }

        /// <summary>
        /// Favorecido que será pago
        /// </summary>
        public string CodigoFavorecido { get; set; }

        /// <summary>
        /// Banco para o deposito
        /// </summary>
        public string Banco { get; set; }

        /// <summary>
        /// Agencia para o deposito
        /// </summary>
        public string Agencia { get; set; }

        /// <summary>
        /// Conta para o deposito
        /// </summary>
        public string Conta { get; set; }

        /// <summary>
        /// Data que deverá ser feito o pagamento
        /// </summary>
        public DateTime? DataPagamento { get; set; }

        /// <summary>
        /// Valor do pagamento a ser feito
        /// </summary>
        public double? ValorPagamento { get; set; }

        /// <summary>
        /// Status do pagamento
        /// </summary>
        public PagamentoStatusEnum Status { get; set; }

        /// <summary>
        /// Data da liquidacao manual
        /// </summary>
        public DateTime? DataLiquidacaoManual { get; set; }

        /// <summary>
        /// Data da geracao
        /// </summary>
        public DateTime? DataGeracao { get; set; }

        /// <summary>
        /// Data do envio do pagamento para a Matera
        /// </summary>
        public DateTime? DataEnvio { get; set; }


        /// <summary>
        /// Data do envio da exclusao
        /// </summary>
        public DateTime? DataExclusao { get; set; }


        /// <summary>
        /// Construtor default
        /// </summary>
        public PagamentoInfo()
        {
            this.CodigoPagamento = Guid.NewGuid().ToString();
            this.Status = PagamentoStatusEnum.Pendente;
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoPagamento;
        }

        #endregion
    }
}
