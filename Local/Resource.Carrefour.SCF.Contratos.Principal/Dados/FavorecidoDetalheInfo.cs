﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public class FavorecidoDetalheInfo : FavorecidoInfo
    {
        public List<ContaFavorecidoInfo> ContasFavorecido {get; set;}

        /// <summary>
        /// Construtor default
        /// </summary>
        public FavorecidoDetalheInfo()
        {
            this.ContasFavorecido = new List<ContaFavorecidoInfo>();
        }
    }
}
