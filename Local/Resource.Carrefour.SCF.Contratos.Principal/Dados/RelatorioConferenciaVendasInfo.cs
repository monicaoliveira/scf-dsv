﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os produtos
    /// </summary>
    [Serializable]
    public class RelatorioConferenciaVendasInfo : ICodigoEntidade
    {
         //<summary>
         //DescricaoEmpresaGrupo
         //</summary>
        public DateTime? DataVenda { get; set; }

        /// <summary>
        /// DescricaoEmpresaGrupo
        /// </summary>
        public string NomeEmpresaGrupo { get; set; }
        
        /// <summary>
        /// DescricaoEmpresaGrupo
        /// </summary>
        public string NomeEmpresaSubgrupo { get; set; }

        /// <summary>
        /// Estabelecimento
        /// </summary>
        public EstabelecimentoInfo Estabelecimento { get; set; }

        /// <summary>
        /// Valor líquido
        /// </summary>
        public Decimal ValorLiquido { get; set; }

        /// <summary>
        /// Codigo pagamento
        /// </summary>
        public string StatusRetorno { get; set; }

        /// <summary>
        /// Codigo pagamento
        /// </summary>
        public string CodigoTransacao { get; set; }

        /// <summary>
        /// Codigo pagamento
        /// </summary>
        public string NomeFavorecido { get; set; }

        /// <summary>
        /// Quantidade
        /// </summary>
        public Decimal Quantidade { get; set; }

        /// <summary>
        /// Valor bruto
        /// </summary>
        public Decimal TotalBruto { get; set; }

        /// <summary>
        /// Valor da comissão
        /// </summary>
        public Decimal TotalComissao { get; set; }

        /// <summary>
        /// Valor líquido
        /// </summary>
        public Decimal TotalCedivelNaoNegociado { get; set; }

        /// <summary>
        /// Valor líquido
        /// </summary>
        public Decimal TotalCedido { get; set; }

        /// <summary>
        /// Valor líquido
        /// </summary>
        public Decimal TotalNaoCedido { get; set; }

        /// <summary>
        /// Valor líquido
        /// </summary>
        public Decimal TotalPendente { get; set; }

        /// <summary>
        /// Valor líquido
        /// </summary>
        public ProdutoInfo Produto{ get; set; }

        /// <summary>
        /// Data do Processamento
        /// </summary>
        public DateTime? DataMovimento { get; set; }

        /// <summary>
        /// Referencia
        /// </summary>
        public ListaItemInfo Referencia { get; set; }



        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return null;
        }
    }
}
