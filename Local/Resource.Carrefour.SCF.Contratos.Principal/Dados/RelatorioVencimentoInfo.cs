﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os produtos
    /// </summary>
    [Serializable]
    public class RelatorioVencimentoInfo : ICodigoEntidade
    {
        /// <summary>
        /// DescricaoEmpresaGrupo
        /// </summary>
        public DateTime? DataVencimento { get; set; }

        /// <summary>
        /// DataPosicaoCarteira
        /// </summary>
        public DateTime? DataPosicaoCarteira { get; set; }

        /// <summary>
        /// DescricaoEmpresaGrupo
        /// </summary>
        public DateTime? DataAgendamento { get; set; }

        /// <summary>
        /// DescricaoEmpresaGrupo
        /// </summary>
        public string DescricaoEmpresaGrupo { get; set; }        
        
        /// <summary>
        /// Estabelecimento
        /// </summary>      
        public string CodigoEmpresaGrupo { get; set; }
        
        /// <summary>
        /// Estabelecimento
        /// </summary>
        public EstabelecimentoInfo Estabelecimento { get; set; }

        /// <summary>
        /// Produto
        /// </summary>
        public ProdutoInfo Produto { get; set; }

        /// <summary>
        /// Referencia
        /// </summary>
        public ListaItemInfo Referencia { get; set; }


        /// <summary>
        /// Valor líquido
        /// </summary>
        public Decimal ValorLiquido { get; set; }

        /// <summary>
        /// Valor Bruto
        /// </summary>
        public Decimal ValorBruto { get; set; }

        /// <summary>
        /// Valor comissao
        /// </summary>
        public Decimal ValorComissao { get; set; }

        /// <summary>
        /// Codigo pagamento
        /// </summary>
        public string CodigoPagamento { get; set; }

        /// <summary>
        /// DescricaoReferenciaAgrupado
        /// </summary>
        public string DescricaoReferenciaAgrupado { get; set; }

        /// <summary>
        /// CodigoReferenciaAgrupado
        /// </summary>      
        public string CodigoReferenciaAgrupado { get; set; }        
        
        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return null;
        }
    }
}
