﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações do processo de extrato
    /// </summary>
    [Serializable]
    public class ProcessoExpurgoArquivoItemInfo : ProcessoInfo
    {

        /// <summary>
        /// Data de envio para o cci
        /// </summary>
        public DateTime? DataUltimoExpurgo { get; set; }

        /// <summary>
        /// Atualiza o processo atual com informacoes de outro processo
        /// </summary>
        /// <param name="processoInfo"></param>
        public void Atualizar(ProcessoInfo processoInfo)
        {
            base.Atualizar(processoInfo); 
        }
    }
}
