﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações de subgrupos
    /// </summary>
    [Serializable]
    public class EmpresaSubGrupoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do subgrupo
        /// </summary>
        public string CodigoEmpresaSubGrupo { get; set; }

        /// <summary>
        /// Nome do subgrupo
        /// </summary>
        public string NomeEmpresaSubGrupo { get; set; }

        /// <summary>
        /// Descrição do subgrupo
        /// </summary>
        public string DescricaoEmpresaSubGrupo { get; set; }

        /// <summary>
        /// Código do grupo
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public EmpresaSubGrupoInfo()
        {
            this.CodigoEmpresaSubGrupo = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoEmpresaSubGrupo;
        }

        #endregion
    }
}
