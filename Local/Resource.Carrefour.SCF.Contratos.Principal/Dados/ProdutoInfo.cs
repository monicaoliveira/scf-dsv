﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os produtos
    /// </summary>
    [Serializable]
    public class ProdutoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do produto
        /// </summary>
        public string CodigoProduto { get; set; }

        /// <summary>
        /// Código do produto TSYS
        /// </summary>
        public string CodigoProdutoTSYS { get; set; }

        /// <summary>
        /// Nome do produto
        /// </summary>
        public string NomeProduto { get; set; }

        /// <summary>
        /// Descrição do produto
        /// </summary>
        public string DescricaoProduto { get; set; }

        /// <summary>
        /// Produto é cedivel?
        /// </summary>
        public bool Cedivel { get; set; }

        public int PrazoPagamento { get; set; }
        public int DiasVencidos { get; set; }
        public int DiasVencidosAJ { get; set; }

        public List<string> CodigoPlanos { get; set; }
        
        /// <summary>
        /// Construtor default
        /// </summary>
        public ProdutoInfo()
        {
            //this.CodigoProduto = Guid.NewGuid().ToString();
            this.CodigoPlanos = new List<string>();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoProduto;
        }

        #endregion
    }
}
