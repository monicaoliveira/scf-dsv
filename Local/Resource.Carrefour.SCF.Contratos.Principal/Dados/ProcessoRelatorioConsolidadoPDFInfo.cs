﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ProcessoRelatorioConsolidadoPDFInfo : ProcessoInfo
    {
        /// <summary>
        /// Filtro para a geração do relatório
        /// </summary>
        public ListarRelatorioConsolidadoRequest Param { get; set; }
    }
}
