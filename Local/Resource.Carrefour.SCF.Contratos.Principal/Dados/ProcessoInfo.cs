﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.Threading;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Classe base dos infos de processo
    /// </summary>
    [Serializable]
    public class ProcessoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Indica o tipo do estágio atual do processo
        /// </summary>
        [XmlIgnore]
        public Type TipoEstagioAtual 
        {
            get { return ResolutorTipos.Resolver(this.TipoEstagioAtualString); }
            set { this.TipoEstagioAtualString = value != null ? value.Name : null; }
        }

        /// <summary>
        /// Propriedade auxiliar para serializacao
        /// </summary>
        public string TipoEstagioAtualString { get; set; }

        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Tipo do processo
        /// </summary>
        public string TipoProcesso { get; set; }

        /// <summary>
        /// Nome do arquivo
        /// </summary>
        public string NomeArquivo { get; set; }

        /// <summary>
        /// Indica se o estágio atual é valido para prosseguir
        /// </summary>
        public bool? EstagioValido { get; set; }

        /// <summary>
        /// Status do processo
        /// </summary>
        public ProcessoStatusEnum StatusProcesso { get; set; }

        /// <summary>
        /// Status do estágio
        /// </summary>
        private EstagioStatusEnum _statusEstagio = EstagioStatusEnum.Parado;
        public EstagioStatusEnum StatusEstagio 
        {
            get { return _statusEstagio; }
            set 
            { 
                if (_statusEstagio != value)
                    this.DataEstagio = DateTime.Now;
                _statusEstagio = value;
            }
        }

        /// <summary>
        /// Data de alteração do estágio
        /// </summary>
        public DateTime? DataEstagio { get; set; }

        /// <summary>
        /// Data de inclusao do processo
        /// </summary>
        public DateTime? DataInclusao { get; set; }

        /// <summary>
        /// Data da ultima alteracao do status do processo
        /// </summary>
        public DateTime? DataStatus { get; set; }

        /// <summary>
        /// Data de atualizacao do processo
        /// </summary>
        public DateTime? DataAtualizacao { get; set; }

        /// <summary>
        /// Indica se há itens bloqueados no processo
        /// </summary>
        public ProcessoStatusBloqueioEnum StatusBloqueio { get; set; }
        
        /// <summary>
        /// Construtor default
        /// </summary>
      
        /// <summary>
        /// Houve Bloqueio automatico de Regra AJ
        /// </summary>
        public bool BloqueioAutomaticoRegraAJ { get; set; }
        
        public ProcessoInfo()
        {
            this.CodigoProcesso = Guid.NewGuid().ToString();
            //this.StatusBloqueio = ProcessoStatusBloqueioEnum.NaoHaBloqueios;
  
            //if (this.manualResetEvent == null)
            //    manualResetEvent = new ManualResetEvent(false);
        }

        /// <summary>
        /// Atualiza o processo atual com informacoes de outro processo
        /// </summary>
        /// <param name="processoInfo"></param>
        public void Atualizar(ProcessoInfo processoInfo)
        {
            this.CodigoProcesso = processoInfo.CodigoProcesso;
            this.DataInclusao = processoInfo.DataInclusao;
            this.DataStatus = processoInfo.DataStatus;
            this.DataAtualizacao = DateTime.Now;
            this.StatusBloqueio = processoInfo.StatusBloqueio;
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoProcesso;
        }

        #endregion
    }
}
