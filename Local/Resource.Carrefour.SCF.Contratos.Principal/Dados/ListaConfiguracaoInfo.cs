﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public class ListaConfiguracaoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Codigo da lista
        /// </summary>
        public string CodigoLista { get; set; }

        /// <summary>
        /// Codigo EMPRESA GRUPO
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Dado da configuração
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Nome do item da lista
        /// </summary>
        public string Mnemonico { get; set; }

        public string ColunaConfiguravelHeader { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return null;
        }

        #endregion
    }
}
