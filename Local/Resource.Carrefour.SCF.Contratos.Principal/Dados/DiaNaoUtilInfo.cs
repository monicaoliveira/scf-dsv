﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre os dias não úteis do ano
    /// </summary>
    [Serializable]
    public class DiaNaoUtilInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do dia não útil
        /// </summary>
        public string CodigoDiaNaoUtil { get; set; }

        /// <summary>
        /// Data do dia não útil
        /// </summary>
        public DateTime? DataDiaNaoUtil { get; set; }

        /// <summary>
        /// Descrição do dia não útil
        /// </summary>
        public string DescricaoDiaNaoUtil { get; set; }

        /// <summary>
        /// Classificação do dia não útil
        /// </summary>
        public string ClassificacaoDiaNaoUtil { get; set; }

        /// <summary>
        /// Identifica se o dia está ativo ou inativo.
        /// </summary>
        public bool Ativo { get; set; }


        /// <summary>
        /// Construtor default
        /// </summary>
        public DiaNaoUtilInfo()
        {
            this.CodigoDiaNaoUtil = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoDiaNaoUtil;
        }

        #endregion
    }
}
