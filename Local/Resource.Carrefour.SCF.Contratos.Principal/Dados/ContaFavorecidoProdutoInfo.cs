using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

//=============================================================================================01
namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
	//=============================================================================================02
    [Serializable]
    public class ContaFavorecidoProdutoInfo : ICodigoEntidade
    {
        public string ID { get; set; }
        public string CodigoContaFavorecido { get; set; }
        public string CodigoProduto { get; set; }
        public string NomeProduto { get; set; }
        public string Banco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string Agencia_CCI { get; set; } //978
        public string Conta_CCI { get; set; }	//978
        public string Referencia { get; set; }	//1339
		//=============================================================================================03
        #region ICodigoEntidade Members
        public string ReceberCodigo()
        {
            return CodigoContaFavorecido;
        }
        #endregion
		//=============================================================================================03
    }
	//=============================================================================================02
}
//=============================================================================================01
