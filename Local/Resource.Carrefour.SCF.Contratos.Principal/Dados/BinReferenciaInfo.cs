﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações de BinReferencia
    /// </summary>
    [Serializable]
    public class BinReferenciaInfo : ICodigoEntidade
    {
        /// <summary>
        /// Codigo do BIN
        /// </summary>
        public string CodigoBin { get; set; }

        /// <summary>
        /// Codigo da Referencia
        /// </summary>
        public string CodigoReferencia { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        /// 
        public BinReferenciaInfo()
        {
            this.CodigoBin = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoBin;
        }


        #endregion
    }
}
