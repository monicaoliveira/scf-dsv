﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public enum ListaItemReferenciaSituacaoEnum
    {
        CodigoAnulacao = 10,
        CodigoAjuste = 8,
        BIN = 18 // ClockWork - Resource - Ago/16 - Margarida Vitolo
    }
}
