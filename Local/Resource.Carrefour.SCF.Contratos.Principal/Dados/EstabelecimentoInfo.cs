﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações de estabelecimento
    /// </summary>
    [Serializable]
    public class EstabelecimentoInfo : ICodigoEntidade
    {
        public string CodigoEstabelecimento { get; set; }
        public string CodigoFavorecido { get; set; }
        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }
        public string Banco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string CodigoEmpresaSubGrupo { get; set; }
        public string CodigoEmpresaGrupo { get; set; }
        public string EstabelecimentoCSU { get; set; }
        public string EstabelecimentoSitef { get; set; }
        public bool Ativo { get; set; }
        public string NomeFavorecido { get; set; } //1351
        public string NomeGrupo { get; set; } //1351
        public string NomeSubGrupo { get; set; } //1351
        public EstabelecimentoInfo()
        {   this.CodigoEstabelecimento = Guid.NewGuid().ToString();
            this.Ativo = true;
        }
        #region ICodigoEntidade Members
        public string ReceberCodigo()
        {   return this.CodigoEstabelecimento;
        }
        #endregion
    }
}
