﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém informações sobre as configuracoes de tipo de registro
    /// </summary>
    [Serializable]
    public class ConfiguracaoTipoRegistroInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da configuracao
        /// </summary>
        public string CodigoConfiguracaoTipoRegistro { get; set; }

        /// <summary>
        /// Tipo do arquivo a que se refere o parametro
        /// </summary>
        public string TipoArquivo  { get; set; }

        /// <summary>
        /// Tipo de registro a que se refere o parametro
        /// </summary>
        public string TipoRegistro { get; set; }

        /// <summary>
        /// Flag indicativa de envio à CCI
        /// </summary>
        public bool EnviarCCI { get; set; }

        /// <summary>
        /// Flag indicativa de geracao de agenda
        /// </summary>
        public bool GerarAgenda{ get; set; }

        /// Quantidade de dias de repasse para calcular por tipo de registro - Processo Extrato - Arquivo Atacadao
        public string DiasRepasse { get; set;  }


        /// <summary>
        /// Construtor default
        /// </summary>
        public ConfiguracaoTipoRegistroInfo()
        {
            this.CodigoConfiguracaoTipoRegistro = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoConfiguracaoTipoRegistro;
        }

        #endregion
    }
}
