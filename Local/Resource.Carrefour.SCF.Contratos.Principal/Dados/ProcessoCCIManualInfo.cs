﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações do processo de extrato
    /// </summary>
    [Serializable]
    public class ProcessoCCIManualInfo : ProcessoInfo
    {
        
        /// <summary>
        /// Caminho do arquivo CCI que será gerado
        /// </summary>
        public string CaminhoArquivoCCI { get; set; }

        /// <summary>
        /// Código do arquivo TSYS 
        /// </summary>
        public string CodigoArquivoTSYS { get; set; }
                
        /// <summary>
        /// Código do arquivo CCI gerado
        /// </summary>
        public string CodigoArquivoCCI { get; set; }

        /// <summary>
        /// Indica se é para mover os arquivos processados para diretorio processados
        /// </summary>
        public bool MoverArquivoProcessado { get; set; }

        /// <summary>
        /// Data de envio para o cci
        /// </summary>
        public DateTime? DataEnvioCCI { get; set; }

        /// <summary>
        /// Código do usuário CCI
        /// </summary>
        public string CodigoUsuarioEnvioCCI { get; set; }

        
    }
}
