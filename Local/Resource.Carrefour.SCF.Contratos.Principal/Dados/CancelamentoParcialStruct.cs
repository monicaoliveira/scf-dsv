// CH0000053039 Mudando o nome da StatusLinhaCP para StatusLinhaCancParcial para não confundir com o registro do tipo "CP"
using System;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    public struct CancelamentoParcialStruct
    {
        public string NSUHost;
        public string NSUHostOriginal;
        public string TipoTransacao;
        public string CodigoArquivoItem;
        public double ValorParcela;
        public ArquivoItemStatusEnum StatusLinhaCancParcial; 

        public CancelamentoParcialStruct
        (       string codigoArquivoItem
            ,   string nsuHost
            ,   string nsuHostOrig
            ,   string tipoTransacao
            ,   double valorParcela
            ,   ArquivoItemStatusEnum statusLinha
        )
        {
            NSUHost                          = nsuHost;
            NSUHostOriginal              = nsuHostOrig;
            TipoTransacao                = tipoTransacao;
            ValorParcela                    = valorParcela;
            CodigoArquivoItem          = codigoArquivoItem;
            StatusLinhaCancParcial   = statusLinha; 
        }
    }
}