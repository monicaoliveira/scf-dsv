﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações de subgrupos
    /// </summary>
    [Serializable]
    public class TaxaRecebimentoInfo : ICodigoEntidade
    {
        public string CodigoTaxaRecebimento { get; set; }
        public string CodigoGrupo { get; set; }
        public string CodigoReferencia { get; set; }
        public string CodigoMeioPagamento { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
        public decimal ValorRecebimento { get; set; }
        public decimal PercentualRecebimento { get; set; }
        public string NomeGrupo { get; set; }
        public string DescricaoReferencia { get; set; }
        public string DescricaoMeioPagamento { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public TaxaRecebimentoInfo()
        {
            this.CodigoTaxaRecebimento = Guid.NewGuid().ToString();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoTaxaRecebimento;
        }

        #endregion
    }
}
