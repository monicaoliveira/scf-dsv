﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    [Serializable]
    public class ProdutoDetalheView : ProdutoInfo
    {
        public List<PlanoInfo> Planos { get; set; }

        public ProdutoDetalheView()
        {
            this.Planos = new List<PlanoInfo>();
        }
    }
}
