﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Resource.Framework.Contratos.Comum;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    //==========================================================================================================================
    /// Registra um evento de processo
    //==========================================================================================================================
//    [Description("Description - LogSCFProcessoEvento")] 
    
    public class LogSCFProcessoEventoInfo : LogSCFProcessoEstagioEventoInfo // LogSCFProcessoInfo
    {
        public LogSCFProcessoEventoInfo() : base() { }

        public static void GerarLogSCFProcessoEvento(string descricao, string codigoSessao, string codigoProcesso)
        {
            if (descricao != null)
            {
                /* 1328
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEventoInfo()
                {
                    Descricao = descricao
                };
                lLogSCFProcessoEventoInfo.setCodigoProcesso(codigoProcesso);
                LogHelper.Salvar( codigoSessao,lLogSCFProcessoEventoInfo);// Indica que pulou este estágio
                */
                var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format(descricao));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            }
        }
    }
}
