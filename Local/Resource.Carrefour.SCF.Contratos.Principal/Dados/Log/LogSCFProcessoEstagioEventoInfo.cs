﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Resource.Framework.Contratos.Comum;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    [Description("LogSCFProcessoEstagioEventoInfo")]
    public class LogSCFProcessoEstagioEventoInfo : LogSCFProcessoInfo
    {
        public static void EfetuarLog(string descricao, string codigoSessao, string codigoProcesso) /// Cria e salva um log deste tipo
        {   var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();            
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(descricao);
            LogHelper.Salvar( codigoSessao, lLogSCFProcessoEstagioEventoInfo);
        }
        public LogSCFProcessoEstagioEventoInfo() : base() { }

        public string TipoEstagio
        {   get; set;
        }
        public void setTipoEstagio(string value)
        {   TipoEstagio = value;
            this.montarDescricao();
        }
        protected override void OnCodigoProcessoAlterado()
        {   this.montarDescricao();
        }
        private string _descricaoEvento = null;
        
        public string DescricaoEvento 
        {   get 
            { 
                return this._descricaoEvento; 
            } 
        }
        public void setDescricao(string value)
        {
            this._descricaoEvento = value;
            this.montarDescricao();
        }
        private void montarDescricao()
        {   //1353 inibindo
            //this.Descricao =
            //string.Format
            //(
            //    "Processo {0}, Evento: {1}, Estágio: {2}"
            //,   this.CodigoProcesso
            //,   this.DescricaoEvento
            //,   this.TipoEstagio
            //);
            //1353 alterando - inicio
            if (this.TipoEstagio != null)
            {
                this.Descricao =
                string.Format
                (
                    "Processo {0}, Evento: {1},  Estágio: {2}"
                , this.CodigoProcesso
                , this.DescricaoEvento
                , this.TipoEstagio
                );
            }
            else
            {
                this.Descricao =
                string.Format
                (
                    "Processo {0}, Evento: {1}"
                , this.CodigoProcesso
                , this.DescricaoEvento
                );
            }
            //1353 alterando - fim
        }
    }
}
