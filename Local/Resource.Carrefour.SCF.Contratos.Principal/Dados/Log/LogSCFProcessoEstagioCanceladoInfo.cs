﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    /// <summary>
    /// Log de cancelamento de estagio
    /// </summary>
    [Description("Estágio Cancelado")]
    public class LogSCFProcessoEstagioCanceladoInfo : LogSCFProcessoInfo
    {
        public LogSCFProcessoEstagioCanceladoInfo() : base() { }
        /// <summary>
        /// Tipo do estagio
        /// </summary>
        
        public string TipoEstagio
        {
            get; set;
        }

        /// <summary>
        /// Atribui valor à propriedade TipoEstagio e executa o método montarDescricao()
        /// </summary>
        /// <param name="value">Tipo do Estagio</param>
        public void setTipoEstagio(string value)
        {
            TipoEstagio = value;
            this.montarDescricao();
        }


        /// <summary>
        /// Trata código do processo alterado
        /// </summary>
        protected override void OnCodigoProcessoAlterado()
        {
            this.montarDescricao();
        }

        /// <summary>
        /// Monta a descricao do log
        /// </summary>
        private void montarDescricao()
        {
            this.Descricao =
                string.Format(
                    "Processo {0}; Estágio Cancelado: {1}",
                    this.CodigoProcesso,
                    this.TipoEstagio);
        }
    }
}
