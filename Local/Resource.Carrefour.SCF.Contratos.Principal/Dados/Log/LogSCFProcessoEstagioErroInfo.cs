﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    /// <summary>
    /// Erro no processamento de um estágio
    /// </summary>
    [Description("Erro em estágio")]
    [Serializable]
    public class LogSCFProcessoEstagioErroInfo : LogSCFProcessoInfo
    {

        public LogSCFProcessoEstagioErroInfo() : base() { }
        /// <summary>
        /// Erro
        /// </summary>
        public string Erro { get; set; }

        /// <summary>
        /// Tipo do estagio
        /// </summary>
        
        public string TipoEstagio
        {
            get; set;
        }

        /// <summary>
        /// Atribui valor à propriedade TipoEstagio e executa o método montarDescricao()
        /// </summary>
        /// <param name="value">Tipo do Estagio</param>
        public void setTipoEstagio(string value)
        {
            TipoEstagio = value;
            this.montarDescricao();
        }


        /// <summary>
        /// Trata código do processo alterado
        /// </summary>
        protected override void OnCodigoProcessoAlterado()
        {
            this.montarDescricao();
        }

        /// <summary>
        /// Monta a descricao do log
        /// </summary>
        private void montarDescricao()
        {
            this.Descricao =
                string.Format(
                    "Processo {0}, Erro no Estágio: {1}, Descrição do Erro: {2}",
                    this.CodigoProcesso,
                    this.TipoEstagio,
                    this.Erro);
        }
    }
}
