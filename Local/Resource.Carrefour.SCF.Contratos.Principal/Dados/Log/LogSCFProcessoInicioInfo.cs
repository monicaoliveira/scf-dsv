﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    /// <summary>
    /// Log de início de processo
    /// </summary>
    [Description("Início de Processo")]
    public class LogSCFProcessoInicioInfo : LogSCFProcessoInfo
    {
        public LogSCFProcessoInicioInfo() : base() { }
    }
}
