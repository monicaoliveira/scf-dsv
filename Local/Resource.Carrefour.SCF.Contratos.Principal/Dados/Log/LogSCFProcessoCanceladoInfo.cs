﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    /// <summary>
    /// Log de cancelamento de processo
    /// </summary>
    [Description("Processo Cancelado")]
    public class LogSCFProcessoCanceladoInfo : LogSCFProcessoInfo
    {
        /// <summary>
        /// Trata código do processo alterado
        /// </summary>
        protected override void OnCodigoProcessoAlterado()
        {
            this.montarDescricao();
        }

        /// <summary>
        /// Monta a descricao do log
        /// </summary>
        private void montarDescricao()
        {
            this.Descricao =
                string.Format(
                    "Processo {0} Cancelado",
                    this.CodigoProcesso);
        }

        public LogSCFProcessoCanceladoInfo() : base() { }
    }
}
