﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    /// <summary>
    /// Log SCF
    /// </summary>
    [Serializable]
    public class LogSCFInfo : LogInfo
    {
        /// <summary>
        /// Código da origem
        /// </summary>
        public string CodigoOrigem { get; set; }

        /// <summary>
        /// Tipo da origem
        /// </summary>
        public string TipoOrigem { get; set; }
    }
}
