﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    /// <summary>
    /// Log de fim de processo
    /// </summary>
    [Description("Fim de Processo")]
    public class LogSCFProcessoFimInfo : LogSCFProcessoInfo
    {
        public LogSCFProcessoFimInfo() : base() { }
    }
}
