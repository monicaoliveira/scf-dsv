﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados.Log
{
    /// <summary>
    /// Log de processo
    /// </summary>
    [Serializable]
    public class LogSCFProcessoInfo : LogSCFInfo
    {
        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso 
        {
            get { return this.CodigoOrigem; }
        }

        private void SetCodigoProcesso(string value)
        {
            this.CodigoOrigem = value;
            this.OnCodigoProcessoAlterado(); 
        }

        /// <summary>
        /// Sinalização de processo alterado
        /// </summary>
        protected virtual void OnCodigoProcessoAlterado()
        {
            // Verifica se tem atributo de descricao
            DescriptionAttribute description =
                (DescriptionAttribute)
                    this.GetType().GetCustomAttributes(
                        typeof(DescriptionAttribute),
                        false).FirstOrDefault();

            // Monta a descricao do log
            string descricao = description != null ? description.Description : this.GetType().Name;

            // Escreve a descricao
            this.Descricao = string.Format("Processo: {0}, Log: {1}", this.CodigoProcesso, descricao);
        }

        /// <summary>
        /// Construtor default
        /// </summary>
        public void setCodigoProcesso(string codigoProcesso)
        {
            this.TipoOrigem = "ProcessoInfo";
            this.SetCodigoProcesso(codigoProcesso);
        }

        public LogSCFProcessoInfo():base() { }
    }
}
