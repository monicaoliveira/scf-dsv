﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Informações do processo de extrato
    /// </summary>
    [Serializable]
    public class ProcessoExtratoInfo : ProcessoInfo
    {
        /// <summary>
        /// Caminho do arquivo TSYS Carrefour que será importado
        /// </summary>
        public string CaminhoArquivoTSYS { get; set; }

        /// <summary>
        /// Caminho do arquivo TSYS Atacadao que será importado
        /// </summary>
        public string CaminhoArquivoTSYSAtacadao { get; set; }
        
        /// <summary>
        /// Caminho do arquivo TSYS Galeria que será importado
        /// </summary>
        public string CaminhoArquivoTSYSGaleria { get; set; }

        /// <summary>
        /// Caminho do arquivo CCI que será gerado
        /// </summary>
        public string CaminhoArquivoCCI { get; set; }

        /// <summary>
        /// Caminho do arquivo ATA que será gerado
        /// </summary>
        public string CaminhoArquivoATA { get; set; }

        /// <summary>
        /// Caminho do arquivo GAL que será gerado
        /// </summary>
        public string CaminhoArquivoGAL { get; set; }

        /// <summary>
        /// Código do arquivo TSYS Carrefour gerado
        /// </summary>
        public string CodigoArquivoTSYS { get; set; }

                /// <summary>
        /// Código do arquivo TSYS Atacadao gerado
        /// </summary>
        public string CodigoArquivoTSYSAtacadao { get; set; }

        /// <summary>
        /// Código do arquivo TSYS Galeria gerado
        /// </summary>
        public string CodigoArquivoTSYSGaleria { get; set; }

        /// <summary>
        /// Código do arquivo CCI gerado
        /// </summary>
        public string CodigoArquivoCCI { get; set; }

        /// <summary>
        /// Código do arquivo ATA gerado
        /// </summary>
        public string CodigoArquivoATA { get; set; }

        /// <summary>
        /// Código do arquivo GAL gerado
        /// </summary>
        public string CodigoArquivoGAL { get; set; }

        /// <summary>
        /// Indica se é para mover os arquivos processados para diretorio processados
        /// </summary>
        public bool MoverArquivoProcessado { get; set; }

        /// <summary>
        /// Data de envio para o cci
        /// </summary>
        public DateTime? DataEnvioCCI { get; set; }

        /// <summary>
        /// Código do usuário CCI
        /// </summary>
        public string CodigoUsuarioEnvioCCI { get; set; }

        /// <summary>
        /// Código do processo que gerou arquivo matera
        /// </summary>
        public string CodigoProcessoMatera { get; set; }

        /// <summary>
        /// Data da geração do arquivo matera
        /// </summary>
        public DateTime? DataEnvioMatera { get; set; }

        /// <summary>
        /// Código do usuário Matera
        /// </summary>
        public string CodigoUsuarioEnvioMatera { get; set; }

        /// <summary>
        /// Atualiza o processo atual com informacoes de outro processo
        /// </summary>
        /// <param name="processoInfo"></param>
        public void Atualizar(ProcessoInfo processoInfo)
        {
            base.Atualizar(processoInfo);

            // atualiza os dados
            this.CodigoUsuarioEnvioCCI = ((ProcessoExtratoInfo)processoInfo).CodigoUsuarioEnvioCCI;
            this.DataEnvioCCI = ((ProcessoExtratoInfo)processoInfo).DataEnvioCCI;
            this.DataEnvioMatera = ((ProcessoExtratoInfo)processoInfo).DataEnvioMatera;
            this.CodigoUsuarioEnvioMatera = ((ProcessoExtratoInfo)processoInfo).CodigoUsuarioEnvioMatera;
            this.CodigoProcessoMatera = ((ProcessoExtratoInfo)processoInfo).DataEnvioMatera != null ? this.CodigoProcesso : null;
        }
    }
}
