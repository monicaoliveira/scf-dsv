﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Contém a hora de leitura de arquivos do TSYS e CCI
    /// </summary>
    [Serializable]
    public class HoraLeituraInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da hora de leitura
        /// </summary>
        public string CodigoHoraLeitura { get; set; }
        
        /// <summary>
        /// Hora de início da leitura
        /// </summary>
        public DateTime? De { get; set; }

        /// <summary>
        /// Hora de término da leitura
        /// </summary>
        public DateTime? Ate { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public HoraLeituraInfo()
        {
            this.CodigoHoraLeitura = Guid.NewGuid().ToString();
        }
        
        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return CodigoHoraLeitura;
        }

        #endregion
    }
}
