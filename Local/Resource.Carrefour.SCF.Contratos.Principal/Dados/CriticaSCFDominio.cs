﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Carrefour.SCF.Contratos.Principal.Dados
{
    /// <summary>
    /// Crítica para a regra de validação de domínio
    /// </summary>
    public class CriticaSCFDominio : CriticaSCFArquivoItemInfo
    {
        /// <summary>
        /// Indica o valor que não foi encontrado
        /// </summary>
        public string ValorNaoEncontrado { get; set; }
    }
}
