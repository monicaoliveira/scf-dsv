﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Configuração dos grupos na validação do processo extrato
    /// </summary>
    public class ProcessoExtratoValidacaoConfig
    {
        /// <summary>
        /// Código do grupo Carrefour
        /// </summary>
        public string CodigoGrupoCarrefour { get; set; }

        /// <summary>
        /// Código do grupo Atacadão
        /// </summary>
        public string CodigoGrupoAtacadao { get; set; }

        /// <summary>
        /// Código do grupo Galeria
        /// </summary>
        public string CodigoGrupoGaleria { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ProcessoExtratoValidacaoConfig()
        {
            this.CodigoGrupoCarrefour = "1";
            this.CodigoGrupoAtacadao = "2";
            this.CodigoGrupoGaleria = "3";
        }
    }
}
