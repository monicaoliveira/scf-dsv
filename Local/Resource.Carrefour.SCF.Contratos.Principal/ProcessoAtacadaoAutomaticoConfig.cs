﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Principal
{
    /// <summary>
    /// Configuracoes do processo atacadao, parte de localização e 
    /// movimentacao de arquivos
    /// </summary>
    public class ProcessoAtacadaoAutomaticoConfig
    {
        /// <summary>
        /// Caminho do arquivo CSU
        /// </summary>
        public string CaminhoArquivoCSU { get; set; }

        /// <summary>
        /// Caminho do arquivo Sitef
        /// </summary>
        public string CaminhoArquivoSitef { get; set; }

        /// <summary>
        /// Caminho do arquivo TSYS
        /// </summary>
        public string CaminhoArquivoTSYS { get; set; }

        /// <summary>
        /// Caminho do arquivo Matera Financeiro
        /// </summary>
        public string CaminhoArquivoMateraFinanceiro { get; set; }

        /// <summary>
        /// Caminho do arquivo Matera Contabil que será gerado
        /// </summary>
        public string CaminhoArquivoMateraContabil { get; set; }


        /// <summary>
        /// Caminho do relatorio de conciliacao
        /// </summary>
        public string CaminhoArquivoResultadoConciliacao { get; set; }

        /// <summary>
        /// Diretorio onde será gerado o resultado do processo
        /// </summary>
        public string DiretorioResultadoProcesso { get; set; }

        /// <summary>
        /// Indica se o processo deve mover os arquivos de origem para dentro
        /// do diretório de resultado
        /// </summary>
        public bool MoverArquivosOrigem { get; set; }

        /// <summary>
        /// Caminho do arquivo CSU sem Atacadão para onde será exportado
        /// </summary>
        public string CaminhoArquivoCSUSemAtacadao { get; set; }
    }
}
