﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="site_configuracao_subgrupo_lista.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_configuracao_subgrupo_lista" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="site_configuracao_subgrupo_lista.js" type="text/javascript"></script>
    <script src="site_configuracao_subgrupo_detalhe.js" type="text/javascript"></script>
    <link href="site_configuracao_subgrupo_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_configuracao_subgrupo_detalhe.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="site_subgrupo_lista">
    
        <!--Lista-->
        <div class="lista">
            <table id="tbEmpresaSubGrupo"></table>
            <div class="botoes">
                <button id="btnIncluirSubGrupo">Novo</button>
                <button id="btnEditarSubGrupo">Editar</button>
                <button id="btnRemoverSubGrupo">Remover</button>
                <button id="btnAtualizarLista">Atualizar Lista</button>
            </div>
         </div>
       
    </div>
</asp:Content>
