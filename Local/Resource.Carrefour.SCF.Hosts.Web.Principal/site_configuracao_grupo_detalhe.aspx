﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_grupo_detalhe.js"></script>
    <link href="site_configuracao_grupo_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Cadastro de Grupos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    <div id="site_configuracao_grupo_detalhe" style="margin: 20px;">
        
        <table>
            <tr>
                <td>
                    <label>Nome do Grupo:</label>
                </td>
                <td>
                    <input type="text" id="txtNomeEmpresaGrupo" class="forms[property[NomeEmpresaGrupo];required]" maxlength="40" />
                </td>
            </tr>
            
            <tr>
                <td>
                    <label>Descrição do Grupo:</label>
                </td>
                <td>
                    <!--<input type="text" id="txtDescricaoEmpresaGrupo" class="forms[property[DescricaoEmpresaGrupo]]" maxlength="100" /> --> 
                    <textarea  id="txtDescricaoEmpresaGrupo" rows="2" cols="42" class="forms[property[DescricaoEmpresaGrupo]]" maxlength="100" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Enviar Negativo:</label>
                </td>
                <td>
                    <select id="cmbEnviarNegativoEmpresaGrupo" class="forms[property[EnviarNegativoEmpresaGrupo];required]" >
                        <option value="?">(selecione)</option>
                        <option value="S">Sim</option>
                        <option value="N">Não</option>
                    </select>
                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>
