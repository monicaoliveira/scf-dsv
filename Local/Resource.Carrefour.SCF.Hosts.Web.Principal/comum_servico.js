﻿/// <reference path="../../lib/jquery/dev/jquery-1.4.1-vsdoc.js" />

var margemRodape = 320;
var margemEsquerda = 60;

$(window).resize(function() {

    $("#conteudo").height(($(window).height() - $("#cabecalho").height() - (2 * $("#rodape").height()) - 180).toString() + "px");

});

$(document).ready(function() {
    
    jQuery.extend($.fn.fmatter , {
        jsonDate: function(cellvalue, options, rowdata) {
            return fJsonDate(cellvalue, options.DateFormat);
        }
    });
    jQuery.extend($.fn.fmatter.jsonDate , {
        unformat: function(cellvalue, options) {
        return null;
    }
    });

	// Seta a cultura default
	$.culture = $.cultures["pt-BR"];

    /*
    $.format.locale({
        number: {
            groupingSeparator: '.',
            decimalSeparator: ','
        }
    });
    */

    $("#janelaErro").dialog({
        autoOpen: false,
        resizable: true,
        height: 400,
        width: 600,
        modal: true,
        buttons: {
            "Fechar": function()
            {
                $(this).dialog("close");
                return false;
            }
        },
        title: "Erros na solicitação"
    });
    
    $(window).resize();
});

function executarServicoPagina(pagina, tipoMensagem, mensagem, callback) {

    // Mostro janela de "carregando"
    showLoadingDialog();

    // Guarda o callback
    if (!$.callbacks)
        $.callbacks = {};
    var callbackId = tipoMensagem + new Date().getTime();
    $.callbacks[callbackId] = callback;

    // Executa a chamada ajax
    $.ajax({
        type: "POST",
        url: pagina,
        dataType: "json",
        data: {
            Acao: "executar",
            TipoMensagem: tipoMensagem,
            Mensagem: $.toJSON(mensagem),
            RequestTag: callbackId
        },
        cache: false,
        success: executarServicoCallback,
        error: function(jqXHR, textStatus, errorThrown) {
            hideLoadingDialog();
            alert("Erro na chamada HTTP: " + (textStatus ? textStatus.toString() : "") + ";" + (errorThrown ? errorThrown.toString() : "") + ";" + (jqXHR ? jqXHR.toString() : ""));
        }
    });
}


function executarServico(tipoMensagem, mensagem, callback) {

    // Mostro janela de "carregando"
    showLoadingDialog();
    
    // Guarda o callback
    if (!$.callbacks)
        $.callbacks = {};
    var callbackId = tipoMensagem + new Date().getTime();
    $.callbacks[callbackId] = callback;
    
    // Executa a chamada ajax
    $.ajax({
        type: "POST",
        url: "comum_servico.aspx",
        dataType: "json",
        data: {
            Acao: "executar",
            TipoMensagem: tipoMensagem,
            Mensagem: $.toJSON(mensagem),
            RequestTag: callbackId
        },
        cache: false,
        success: executarServicoCallback,
        error: function(jqXHR, textStatus, errorThrown) {
            hideLoadingDialog();
            alert("Erro na chamada HTTP: " + (textStatus ? textStatus.toString() : "") + ";" + (errorThrown ? errorThrown.toString() : "") + ";" + (jqXHR ? jqXHR.toString() : ""));
        }
    });
}

function executarServicoCallback(data) {
    if (data.StatusResposta != "OK")
    {
        $("#sessaoInvalida").hide();
        $("#erroNegocio").hide();
        $("#erroPrograma").hide();
        $("#acessoNaoPermitido").hide();
        if (data.StatusResposta == "ErroNegocio" || data.StatusResposta == "ErroValidacao")
        {
            // Possui criticas?
            if (data.Criticas && data.Criticas[0]) {
                var criticasForms = new Array();
                for (var i = 0; i < data.Criticas.length; i++) {
                    criticasForms.push({
					    tab: "",
					    message: data.Criticas[i].Descricao
				    });
				}
				// Mostro dialog de criticas
				$.Forms.showValidationForm({ errors: criticasForms });

				return;
            }
            // $("#erroNegocio").show();
        }
        else if (data.StatusResposta == "ErroPrograma")
        {
            $("#erroPrograma").show();
            if (data.DescricaoResposta)
                $("#txtErroPrograma").val(data.DescricaoResposta);
            else
                $("#txtErroPrograma").val(data.Erro);
        }
        else if (data.StatusResposta == "AcessoNaoPermitido")
        {
            $("#acessoNaoPermitido").show();
        }
        else if (data.StatusResposta == "SessaoInvalida")
        {
            $("#sessaoInvalida").show();
        }
        else if (data.StatusResposta == "SenhaExpirada") {
            $("#senhaExpirada").show();
        }
        $("#janelaErro").dialog("open");
        //alert("Ocorreram erros na solicitação ao servidor: " + data.DescricaoResposta);
    }
    else if (data.ResponseTag && data.ResponseTag != null) {
        if (typeof $.callbacks[data.ResponseTag] == "string")
            eval($.callbacks[data.ResponseTag] + "(data);");
        else
            $.callbacks[data.ResponseTag](data);
        delete $.callbacks[data.ResponseTag];
    }

    // Escondo janela de "carregando"
    hideLoadingDialog();
}

function carregarTabela(jTable, colecao, propriedadePK) {
    // Inicializa
    var propriedade, valor;

    // Prepara tbody
    var table = jTable;
    table.find("tbody").remove();
    table.append("<tbody></tbody>");
    var tbody = table.find("tbody");

    // Pega lista de colunas
    var ths = table.find("thead td");

    // Varre a lista 
    for (var i = 0; i < colecao.length; i++) {

        // Pega o item
        var item = colecao[i];

        // Prepara as colunas
        var tds = "";

        // Adiciona as colunas
        ths.each(function() {
            propriedade = $(this).attr("propriedade");
            eval("valor = item." + propriedade + ";");
            tds += "<td>" + valor + "</td>";
        });

        // Cria a linha e insere os tds
        var tr = $(document.createElement("tr"));
        tr.append(tds);

        // Adiciona chave primaria na tabela
        if (propriedadePK)
            eval("tr.attr('pk', item." + propriedadePK + ");");

        // Insere no corpo da tabela
        tbody.append(tr);
    }
}

function fJsonDate(jsonDate, formato) {
    var data = fJsonDateObject(jsonDate);
    if (!formato) formato = "dd/MM/yyyy";
    data = $.format(data, formato);
    return data;
}

function fJsonDateObject(jsonDate) {
    if (jsonDate.length == 10)
        jsonDate += " 00:00:00";
    var dia = $.trim(jsonDate.split("/")[0]);
    var mes = $.trim(jsonDate.split("/")[1]);
    var ano = $.trim(jsonDate.split(" ")[0].split("/")[2]);
    var hora = $.trim(jsonDate.split(":")[0].substr(10));
    var minuto = $.trim(jsonDate.split(":")[1]);
    var segundo = $.trim(jsonDate.split(":")[2]);

    var data = new Date(ano, mes - 1, dia, hora, minuto, segundo);
    return data;
}


function valida_cpf(cpf) {
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
        return false;
    for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1)) {
        digitos_iguais = 0;
        break;
    }
    if (!digitos_iguais) {
        numeros = cpf.substring(0, 9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
            soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        numeros = cpf.substring(0, 10);
        soma = 0;
        for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}

function valida_cnpj(cnpj) {
    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;
    if (cnpj.length != 14)
        return false;
    for (i = 0; i < cnpj.length - 1; i++)
        if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
        digitos_iguais = 0;
        break;
    }
    if (!digitos_iguais) {
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0, tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}


/**
* Faz o parse do endereço da página e retorna o parâmetro solicitado
*/
function getParameter(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return null;
    else
        return results[1];
}

/**
* Preenche um determinado combo.
* @param combo Elemento <select> HTML.
* @param itens objeto ListaInfo
*/
//function preencherCombo(combo, enumerador) {
//    
//    // Limpo o combo (exceto o primeiro item)
//    combo.children().each(
//          function(index) {
//                if (index != 0)
//                      $(this).remove();
//          }
//    );
//    
//    // Para cada item, adiciono no combo
//    for (var i = 0; i < enumerador.Itens.length; i++) {
//                
//          // Se tem descricao utiliza, se nao, usa o mnemonico
//          var descricao = enumerador.Itens[i].Descricao;
//          if (descricao == null)
//              descricao = enumerador.Itens[i].Mnemonico;
//          
//          // Adiciona o item no bomo
//          combo.append("<option value='" + enumerador.Itens[i].Valor + "'>"
//                      + descricao + "</option>");
//          
//    }
//}
