﻿$(document).ready(function() {

    // Namespace
    if (!$.relatorio)
        $.relatorio = {};

    // Funcoes do namespace 
    $.relatorio.filtroPeriodo = {

        variaveis: {},

        //=================================================================================
        // load: funcoes de inicializacao da tela
        //=================================================================================
        load: function(callback) {

            // Insere o subtítulo
            $("#subtitulo").html("Relatórios > Relatório de Saldo de Conciliação Contábil");

            //------------------------------------------------------------------
            // Estilo dos botões
            //-------------------------------------------------------------------
            $("#site_relatorio_saldo_conciliacao button").button();

            //-------------------------------------------------------------------
            // Inicializa forms
            //-------------------------------------------------------------------
            $("#site_relatorio_saldo_conciliacao").Forms("initializeFields");

            //-------------------------------------------------------------------
            // BOTÃO VISUALIZAR HTML
            //-------------------------------------------------------------------
            $("#btnVisualizarHtml").click(function() {
                $.relatorio.filtroPeriodo.receberNumeroDeLinhas(function() {
                    $.relatorio.filtroPeriodo.gerarRelatorioHtml();
                });
            });

            //-------------------------------------------------------------------
            // BOTÃO GERAR PDF
            //-------------------------------------------------------------------
            $("#btnGerarRelatorioPDF").click(function() {

                /********************/
                /**** TEMPORARIO ****/
                /*********************/

                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Relatório em processo de desenvolvimento.",
                    callbackSim: function() {
                        return true;
                    },
                    callbackNao: function() {
                        return true;
                    }
                });

                /********************/

                /*
                var parametros = $.relatorio.filtroPeriodo.gerarUrl();
                if (parametros == "") {
                parametros = "?";
                }
                window.open("site_relatorio_saldo_conciliacao.aspx" + parametros + "&acao=pdf", "_self");
                */
            });

            //-------------------------------------------------------------------
            // BOTÃO GERAR EXCEL
            //-------------------------------------------------------------------
            $("#btnGerarRelatorioExcel").click(function() {
                $.relatorio.filtroPeriodo.gerarRelatorioExcel();

            });
        },

        preencherRequest: function() {
            var request = {};
            $("#filtroPeriodo").Forms("getData", { dataObject: request });

            if (request.FiltroDataInicioConciliacao == null)
                request.FiltroDataInicioConciliacao = "";

            if (request.FiltroDataFimConciliacao == null)
                request.FiltroDataFimConciliacao = "";

           //request.FiltroDataInicioConciliacao = $("#txtPeriodoConciliacaoDe").val();
           //request.FiltroDataFimConciliacao = $("#txtPeriodoConciliacaoAte").val();
            return request;
        },

        //=================================================================================
        // gerarRelatorioExcel: gera o relatório em Excel de transações conforme filtros
        //=================================================================================
        gerarRelatorioExcel: function() {

            //----------------------------------------------
            // Preenche request
            //----------------------------------------------
            var request = $.relatorio.filtroPeriodo.preencherRequest();

            if (request && request.FiltroDataInicioConciliacao != "" && request.FiltroDataFimConciliacao != "") {

                if (new Date(request.FiltroDataFimConciliacao) < new Date(request.FiltroDataInicioConciliacao)) {
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "O campo Data Início não pode ser maior que a Data Fim.",
                        callbackOK: function() {
                            return false;
                        }
                    });
                } else {
                    // Faz a chamada do servico
                    executarServico("GerarRelatorioConciliacaoContabilExcelRequest", request, function(data) {
                        if (data.ErrorMessage) {
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: data.ErrorMessage,
                                callbackOK: function() {
                                    return false;
                                }
                            });
                        }
                        else {
                            popup({
                                titulo: "Relatório Excel",
                                icone: "sucesso",
                                mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
                                callbackOK: function() { }
                            });
                        }
                    });
                }
            } else {
                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Os campos Data Início e Data Fim são obrigatórios",
                    callbackOK: function() {
                        return false;
                    }
                });
            }
        },


        //============================================================================================
        // gerarVisualizacaoHTML: gera a visualização em HTML de saldo de conciliação conforme filtros
        //============================================================================================
        gerarRelatorioHtml: function(callback) {

            //----------------------------------------------
            // Preenche request
            //----------------------------------------------
            var request = $.relatorio.filtroPeriodo.preencherRequest();

            if (request && request.FiltroDataInicioConciliacao != "" && request.FiltroDataFimConciliacao != "") {
                if (new Date(request.FiltroDataFimConciliacao) < new Date(request.FiltroDataInicioConciliacao)) {
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "O campo Data Início não pode ser maior que a Data Fim.",
                        callbackOK: function() {
                            return false;
                        }
                    });
                }
                else {
                    var link = "relatorio_saldo_conciliacao.aspx";
                    link += $.relatorio.filtroPeriodo.gerarUrl();

                    window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');
                }
            }
            else {
                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Os campos Data Início e Data Fim são obrigatórios",
                    callbackOK: function() {
                        return false;
                    }
                });
            }
            // Chama callback
            if (callback)
                callback();
        },

        gerarUrl: function(callback) {

            var link = "";

            if ($("#txtPeriodoConciliacaoDe").val() != "") {
                link = link + "DataInicioConciliacao=" + $("#txtPeriodoConciliacaoDe").val() + "&";
            }
            if ($("#txtPeriodoConciliacaoAte").val() != "") {
                link = link + "DataFimConciliacao=" + $("#txtPeriodoConciliacaoAte").val() + "&";
            }

            if (link != "") {
                link = "?" + link.substr(0, link.length - 1);
            }

            // Chama callback
            if (callback)
                callback();

            return link;
        },

        receberNumeroDeLinhas: function(callback) {

            if (callback)
                callback();


        }
    }

    // Chama load da página
    $.relatorio.filtroPeriodo.load();
});