/// <reference path="../../lib/jquery/dev/jquery-1.4.1-vsdoc.js" />
/// <reference path="comum_servico.aspx.js" />

var permissoesCarregadas = false;
var perfisCarregados = false;
var gruposCarregados = false;
var bNovoUsuario = false;

$(document).ready(function() 
{   $("button").button();
    $("input[type='button']").button();
    $("#usuario_detalhe").Forms("initializeFields");
    $("#subtitulo").html("ADMINISTRATIVO [Cadastro de Usuários]");

    $("#usuario_edicao").tabs();
    $("#cmdAtualizarLista").click(atualizarLista);
    $("#cmdAdicionarPermissao").click(adicionarPermissao);
    $("#cmdAdicionarPerfil").click(adicionarPerfil);
    $("#cmdAdicionarGrupo").click(adicionarGrupo);
    $("#txtLogin").attr("disabled", true);//1436
    $("#txtnome").focus();//1436

    //================================================================================================
    // NOVO USUARIO
    //================================================================================================
    $("#cmdNovoUsuario").click(function() 
    {
        $("#txtLogin").attr("disabled", false);
        $("#txtLogin").focus(); //1436
        $("#usuario_detalhe").Forms("clearData");

        $("#usuario_detalhe #senha").removeAttr("disabled");
        $("#usuario_detalhe #codigoUsuario").removeAttr("disabled");
        //1436$("#usuario_detalhe #data_de_expiracao").attr("disabled", "disabled");
        //1436$("#usuario_detalhe #dias_restantes").attr("disabled", "disabled");
        
        $("#tbUsuarioPermissaoLista").clearGridData();
        $("#tbUsuarioGrupoLista").clearGridData();
        $("#tbUsuarioPerfilLista").clearGridData();

        //1436bNovoUsuario = true;
        $("#usuario_edicao").dialog({title: "USUARIO - Inclusao"});  //1477
        $("#usuario_edicao").dialog("open");

        bNovoUsuario = true;

        return false;
    });

    //================================================================================================
    // ALTERAR SENHA
    //================================================================================================
    $("#cmdResetarSenha").click(function() 
    {
        var codigoUsuario = $("#tbUsuarioLista").getGridParam("selrow");
        bNovoUsuario = false;
        if (codigoUsuario)
        {
            $("#resetar_senha").dialog( //1477
            {   title: 
                "USUARIO [" 
                + $("#tbUsuarioLista").getGridParam("selrow")
                + "] - Editar"
            });

            $("#resetar_senha").dialog("open");
        }
        else 
        {
            $("#textoAlerta").html("Selecione um usuário.");
            $("#mensagemAlerta").dialog(
            {   title: "RESET SENHA - Atenção"            
            ,   autoOpen: true
            ,   resizable: false
            ,   height: 100
            ,   width: 200
            ,   modal: true
            ,   buttons: 
                {   "Ok": 
                    function() 
                    {
                        $(this).dialog("close");
                        return false;
                    }
                }
            });
        }
        return false;
    });

    //================================================================================================
    // EDITAR USUARIO
    //================================================================================================
    $("#cmdEditarUsuario").click(function() 
    {
        var codigoUsuario = $("#tbUsuarioLista").getGridParam("selrow");
        bNovoUsuario = false;
        if (codigoUsuario) {
            $("#txtLogin").attr("disabled", true);
            selecionarUsuario(codigoUsuario);
        }
        else {
            $("#textoAlerta").html("Selecione um Usuário.");
            $("#mensagemAlerta").dialog({
                autoOpen: true,
                resizable: false,
                height: 150,
                modal: true,
                buttons: {
                    "Ok": function() {
                        $(this).dialog("close");
                        return false;
                    }
                },
                title: "Atenção"
            });
        }
        return false;
    });
    
    //================================================================================================
    // REMOVER USUARIO
    //================================================================================================
    $("#cmdRemoverUsuario").click(function() 
    {
        var codigoUsuario = $("#tbUsuarioLista").getGridParam("selrow");
        if (codigoUsuario) 
        {
            $("#confirmacaoExclusao").dialog( //1477
            {   title: 
                "USUARIO [" 
                + $("#tbUsuarioLista").getGridParam("selrow")
                + "] - Remover"
            });
            $("#confirmacaoExclusao").dialog({
                autoOpen: true,
                resizable: false,
                height: 200,
                modal: true,
                buttons: {
                    "Sim": function() {
                        var codigoUsuario = $("#tbUsuarioLista").getGridParam("selrow");
                        executarServico("RemoverUsuarioRequest",{ CodigoUsuario: codigoUsuario },"atualizarLista");
                        $(this).dialog("close");
                        return false;
                    },
                    "Não": function() {
                        $(this).dialog("close");
                        return false;
                    }
                }
                //1477,title: "Remover Usuário"
            });
        }
        else 
        {
            $("#textoAlerta").html("Selecione um Usuário.");
            $("#mensagemAlerta").dialog({
                autoOpen: true,
                resizable: false,
                height: 150,
                modal: true,
                buttons: {
                    "Ok": function() {
                        $(this).dialog("close");
                        return false;
                    }
                },
                title: "Atenção"
            });
        }
        return false;
    });

    //================================================================================================
    // REMOVER PERMISSAO
    //================================================================================================
    $("#cmdRemoverPermissao").click(function() 
    {
        var codigoUsuarioPermissao = $("#tbUsuarioPermissaoLista").getGridParam("selrow")
        if (codigoUsuarioPermissao)
            $("#tbUsuarioPermissaoLista").delRowData($("#tbUsuarioPermissaoLista").getGridParam("selrow"));
        else 
        {
            $("#textoAlerta").html("Selecione uma Permissão.");
            $("#mensagemAlerta").dialog({
                autoOpen: true,
                resizable: false,
                height: 150,
                modal: true,
                buttons: {
                    "Ok": function() {
                        $(this).dialog("close");
                        return false;
                    }
                },
                title: "Atenção"
            });
        }
        return false;
    });

    //================================================================================================
    // REMOVER PERFIL
    //================================================================================================
    $("#cmdRemoverPerfil").click(function() 
    {
        var codigoUsuarioPerfil = $("#tbUsuarioPerfilLista").getGridParam("selrow");
        if (codigoUsuarioPerfil)
            $("#tbUsuarioPerfilLista").delRowData($("#tbUsuarioPerfilLista").getGridParam("selrow"));
        else 
        {
            $("#textoAlerta").html("Selecione um Perfil.");
            $("#mensagemAlerta").dialog({
                autoOpen: true,
                resizable: false,
                height: 150,
                modal: true,
                buttons: {
                    "Ok": function() {
                        $(this).dialog("close");
                        return false;
                    }
                },
                title: "Atenção"
            });
        }
        return false;
    });

    //================================================================================================
    // REMOVER GRUPO
    //================================================================================================
    $("#cmdRemoverGrupo").click(function() 
    {   var codigoUsuarioGrupo = $("#tbUsuarioGrupoLista").getGridParam("selrow");

        if (codigoUsuarioGrupo)
            $("#tbUsuarioGrupoLista").delRowData($("#tbUsuarioGrupoLista").getGridParam("selrow"));
        else 
        {
            $("#textoAlerta").html("Selecione um Usuario.");
            $("#mensagemAlerta").dialog(
            {   autoOpen: true
            ,   title: "Atenção"
            ,   resizable: false
            ,   height: 150
            ,   modal: true
            ,   buttons: 
                {   "Ok": function() 
                    {   $(this).dialog("close");
                        return false;
                    }
                }
            });
        }
        return false;
    });

    //================================================================================================
    // CONFIRMAR EXCLUSAO
    //================================================================================================
    $("#confirmacaoExclusao").dialog(
    {   autoOpen: false
    ,   resizable: false
    ,   height: 200
    ,   modal: true
    ,   buttons: 
        {   "Sim": function() 
            {   $(this).dialog("close");
                return false;
            }
        ,   "Não": function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });

    //================================================================================================
    // EDICAO USUARIO
    //================================================================================================
    $("#usuario_edicao").dialog(
    {   autoOpen: false
    //1477,   title: "USUARIO [" + $("#usuario_detalhe #txtnome").val() + "] - Editar"
    ,   height: 400
    ,   width: 600
    ,   modal: true
    ,   buttons: 
        {   'Salvar': function() 
            {   salvarUsuario();
                return false;
            }
        ,   'Cancelar': function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });

    //================================================================================================
    // LISTA DE PERMISSAO
    //================================================================================================
    $("#permissao_lista").dialog( //1477
    {   autoOpen: false
    ,   title: "USUARIO - Adicionar Permissão"
    ,   height: 350
    ,   width: 500
    ,   modal: true
    ,   close: function() {}
    ,   buttons: 
        {   'Incluir': function() 
            {   var rowid = $("#tbPermissaoLista").getGridParam("selrow");
                if (rowid) 
                {   var rowdata = $("#tbPermissaoLista").getRowData(rowid);
                    rowdata.Status = $("#cmbTipoPermissao").val();
                    $("#tbUsuarioPermissaoLista").addRowData(rowid,rowdata,"last","after");
                    $(this).dialog("close");
                    return false;
                } else 
                {   popup(
                    {   titulo: "Atenção"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar uma linha!"
                    ,   callbackOK: function() { }
                    });
                }
            }
        ,   'Cancelar': function() 
            {
                $(this).dialog("close");
                return false;
            }
        }
    });

    //================================================================================================
    // LISTA DE PERMISSAO
    //================================================================================================
    $("#perfil_lista").dialog( //1477
    {   autoOpen: false
    ,   title: "USUARIO - Adicionar Perfil"
    ,   height: 350
    ,   width: 500
    ,   modal: true
    ,   close: function() {}
    ,   buttons: 
        {   'Incluir': function() 
            {   var rowid = $("#tbPerfilLista").getGridParam("selrow");
                if (rowid) 
                {   var rowdata = $("#tbPerfilLista").getRowData(rowid);
                    $("#tbUsuarioPerfilLista").addRowData(rowid,rowdata,"last","after");
                    $(this).dialog("close");
                    return false;
                } else 
                {   popup(
                    {   titulo: "Atenção"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar uma linha!"
                    ,   callbackOK: function() { }
                    });
                }
            }
        ,   'Cancelar': function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });

    //================================================================================================
    // LISTA DE GRUPO
    //================================================================================================
    $("#grupo_lista").dialog( //1477
    {   title: "USUARIO - Adicionar Grupo"
    ,   autoOpen: false
    ,   height: 350
    ,   width: 500
    ,   modal: true
    ,   close: function() {}
    ,   buttons: 
        {   'Incluir': function() 
            {   var rowid = $("#tbGrupoLista").getGridParam("selrow");
                if (rowid) 
                {
                    var rowdata = $("#tbGrupoLista").getRowData(rowid);
                    $("#tbUsuarioGrupoLista").addRowData(rowid,rowdata,"last","after");
                    $(this).dialog("close");
                    return false;
                } 
                else 
                {
                    popup(
                    {   titulo: "GRUPO"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar uma linha!"
                    ,   callbackOK: function() { }
                    });
                }
            }
        ,   'Cancelar': 
            function() 
            {
                $(this).dialog("close");
                return false;
            }
        }
    });

    //================================================================================================
    // TELA DO RESET
    //================================================================================================
    $("#resetar_senha").dialog(
    {   title: "ALTERAR SENHA - Usuario" 
    ,   autoOpen: false
    ,   height: 180
    ,   width: 300
    ,   modal: true
    ,   buttons: 
        {   'Alterar':
            function() 
            {   resetarSenha();
                $(this).dialog("close");
                return false;
            }
        ,   'Cancelar': 
            function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });

    //================================================================================================
    // GRID PRINCIPAL - Lista dos usuarios
    //================================================================================================
    $("#tbUsuarioLista").jqGrid(
    {       datatype: "clientSide"
    ,       mtype: "GET"
    //1436,       colNames: ["Codigo Usuario", "Nome"}
    ,       colNames: ["ID", "Login", "Nome", "CPF", "Matricula", "Bloqueio Inicio", "Bloqueio Fim", "Expiração Senha", "Bloqueado?"] //1436
    ,       colModel: 
            [
                { name: "CodigoUsuario", index: "CodigoUsuario", width: 100 , key: true },
                { name: "Login", index: "Login", width: 100 },
                { name: "Nome", index: "Nome", width: 250 },
                { name: "Cpf", index: "Cpf", width: 150 },
                { name: "Matricula", index: "Matricula", width: 150 },
                { name: "DataBloqueadoInicio", index: "DataBloqueadoInicio", width: 130 },
                { name: "DataBloqueadoFim", index: "DataBloqueadoFim", width: 130 },
                { name: "DataExpiracaoSenha", index: "DataExpiracaoSenha", width: 130 },
                { name: "Bloqueado", index: "Bloqueado", width: 100 }                
            ]
    ,       caption: ""
    //1436 inicio    
    //       height: 150
    //       width: 900
    ,       shrinkToFit: false //1436
    //1436 fim
    //1437 inicio
    //,       sortname: "Nome"    
    ,       sortorder: "asc"
	,       viewrecords: true   
    ,       rowNum: 100000	 
	//1437 fim
    ,       ondblClickRow: function(rowid, status) 
            {   //1437 inicio
                //selecionarUsuario(rowid);
                //return false;
                var codigoUsuario = $("#tbUsuarioLista").getGridParam("selrow"); //1437
                if (codigoUsuario) 
                {   
                selecionarUsuario(codigoUsuario);                 
                }
                //1437 fim
            }
    });
    
    //================================================================================================
    // GRID AUXILIAR - Permissoes
    //================================================================================================
    $("#tbPermissaoLista").jqGrid(      //1477
    {   datatype: "clientSide"
    ,   mtype: "GET"
    ,   height: 200 //1477
    ,   width: 450 //1477
    ,   shrinkToFit: false //1477 
    ,   viewrecords: true
    ,   caption: ""    
    ,   colNames: ["CD", "Permissão"]
    ,   colModel: 
        [   { name: "CodigoPermissao", index: "CodigoPermissao", width: 100, align: "center", key: true }
        ,   { name: "NomePermissao", index: "NomePermissao", width: 300 } 
        ]
    });

    //================================================================================================
    // GRID AUXILIAR - Perfis
    //================================================================================================
    $("#tbPerfilLista").jqGrid( //1477
    {   datatype: "clientSide"
    ,   height: 200 //1477
    ,   width: 450 //1477
    ,   shrinkToFit: false //1477     
    ,   mtype: "GET"
    ,   viewrecords: true
    ,   caption: ""
    ,   colNames: ["CD", "Perfil"]
    ,   colModel: 
        [   { name: "CodigoPerfil", index: "CodigoPerfil", width: 100, align: "center", key: true }
        ,   { name: "NomePerfil", index: "NomePerfil", width: 300 }
        ]
    });

    //================================================================================================
    // GRID AUXILIAR - Grupo
    //================================================================================================
    $("#tbGrupoLista").jqGrid(
    {   datatype: "clientSide"
    ,   mtype: "GET"
    ,   viewrecords: true
    ,   height: 200 //1477
    ,   width: 450 //1477
    ,   shrinkToFit: false //1477       
    ,   caption: ""
    ,   colNames: ["CD", "Nome"]
    ,   colModel: 
        [   { name: "CodigoUsuarioGrupo", index: "CodigoUsuarioGrupo", width: 100, align: "center", key: true }
        ,   { name: "NomeUsuarioGrupo", index: "NomeUsuarioGrupo", width: 300 }
        ]
    });

    //================================================================================================
    // GRID OFICIAL - Permissao Usuario
    //================================================================================================
    $("#tbUsuarioPermissaoLista").jqGrid( //1477
    {   datatype: "clientSide"
    ,   mtype: "GET"
    ,   viewrecords: true
    ,   caption: ""
    ,   colNames: ["CD", "Permissão", "Status"]
    ,   colModel: 
        [   { name: "CodigoPermissao", index: "CodigoPermissao", width: 100, align: "center", key: true }
        ,   { name: "NomePermissao", index: "NomePermissao", width: 300 }
        ,   { name: "Status", index: "Status", width: 120, align: "right", edittype: "select", formatter: "select", editoptions: { value: "0:Permitido;1:Negado"} }
        ]
    });

    //================================================================================================
    // GRID OFICIAL - Perfil Usuario
    //================================================================================================
    $("#tbUsuarioPerfilLista").jqGrid( //1477
    {   datatype: "clientSide"
    ,   mtype: "GET"
    ,   viewrecords: true
    ,   caption: ""
    ,   colNames: ["CD", "Perfil"]
    ,   colModel: 
        [   { name: "CodigoPerfil", index: "CodigoPerfil", width: 100, align: "center", key: true }
        ,   { name: "NomePerfil", index: "NomePerfil", width: 300 }
        ]
    });

    //================================================================================================
    // GRID OFICIAL - Grupo Usuario
    //================================================================================================
    $("#tbUsuarioGrupoLista").jqGrid(//1477
    {   datatype: "clientSide"
    ,   mtype: "GET"
    ,   viewrecords: true
    ,   caption: ""    
    ,   colNames: ["CD", "Nome"]
    ,   colModel: 
        [   { name: "CodigoUsuarioGrupo", index: "CodigoUsuarioGrupo", width: 100, align: "center", key: true }
        ,   { name: "NomeUsuarioGrupo", index: "NomeUsuarioGrupo", width: 300 }
        ]
    });

    //================================================================================================
    // Funcao
    //================================================================================================
    atualizarLista();

    //================================================================================================
    // RESIZE
    //================================================================================================
    $(window).bind("resize", function() {
        $("#tbUsuarioLista").setGridWidth($(window).width() - margemEsquerda);
        $("#tbUsuarioLista").setGridHeight($(window).height() - margemRodape);
    }).trigger("resize");

});

//================================================================================================
function atualizarLista() {
    executarServico("ListarUsuariosRequest", {}, "atualizarListaCallback");
}
//================================================================================================
function atualizarListaCallback(data) {
    $("#tbUsuarioLista").clearGridData();
    for (var i = 0; i < data.Usuarios.length; i++)
        $("#tbUsuarioLista").addRowData(data.Usuarios[i].CodigoUsuario, data.Usuarios[i], "last", "after");
}
//================================================================================================
function selecionarUsuario(codigoUsuario) 
{   //1437 inicio
//    executarServico(
//        "ReceberUsuarioRequest",
//        {
//            CodigoUsuario: codigoUsuario,
//            PreencherColecoesCompletas: true
//        },
//        "selecionarUsuarioCallback");
    if (codigoUsuario)
    {
    executarServico(
        "ReceberUsuarioRequest",
        {
            CodigoUsuario: codigoUsuario,
            PreencherColecoesCompletas: true
        },
        "selecionarUsuarioCallback");
    }
    else 
    {   popup
        ({   
            titulo: "Atenção"
        ,   icone: "erro"
        ,   mensagem: "Favor selecionar uma linha"
        ,   callbackOK: function() { }
        });
    }                
    //1437 inicio
}

//================================================================================================
function selecionarUsuarioCallback(data) {
//1436 - inicio    
// O CAMPO data_de_expiracao não efetuava match com o campo DataExpiracaoSenha deixando-o em branco
// sendo nulo a informação DataExpiracaoSenha ficava com o dia toda vez que o cadastro de usuario sogria manutenção
//    var today = new Date(); 
//    $("#usuario_detalhe #codigoUsuario").val(data.Usuario.CodigoUsuario);
//    $("#usuario_detalhe #codigoUsuario").attr("disabled", "disabled");
//    //$("#usuario_detalhe #matricula").val(data.Usuario.Matricula);
//    $("#usuario_detalhe #nome").val(data.Usuario.Nome);
//    $("#usuario_detalhe #nomeAbreviado").val(data.Usuario.NomeAbreviado);
//    $("#usuario_detalhe #senha").val(data.Usuario.Senha);
//    $("#usuario_detalhe #assinaturaEletronica").val(data.Usuario.AssinaturaEletronica);
//    $("#usuario_detalhe #origem").val(data.Usuario.Origem);
//    $("#usuario_detalhe #data_de_expiracao").val(fJsonDate(data.Usuario.DataExpiracaoSenha, "dd/MM/yyyy"));
//    $("#usuario_detalhe #data_de_expiracao").attr("disabled", "disabled");
//    $("#usuario_detalhe #dias_restantes").val(getDiasRestantes(fJsonDateObject(data.Usuario.DataExpiracaoSenha), new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0)));
//    $("#usuario_detalhe #dias_restantes").attr("disabled", "disabled");
//1436 - fim

    $("#usuario_detalhe").Forms("setData", { dataObject: data.Usuario });        

    $("#tbUsuarioPermissaoLista").clearGridData();
    for (var i = 0; i < data.Usuario.Permissoes.length; i++) {
        var permissao = data.Usuario.Permissoes[i];
        var permissaoInfo = {
            CodigoPermissao: permissao.CodigoPermissao,
            NomePermissao: permissao.PermissaoInfo.NomePermissao,
            Status: permissao.Status
        };
        if (permissaoInfo.Status == "Negado")
            permissaoInfo.Status = "1";
        if (permissaoInfo.Status == "Permitido")
            permissaoInfo.Status = "0";

        $("#tbUsuarioPermissaoLista").addRowData(data.Usuario.Permissoes[i].CodigoPermissao, permissaoInfo, "last", "after");
    }

    $("#tbUsuarioGrupoLista").clearGridData();
    for (var i = 0; i < data.Usuario.Grupos2.length; i++)
        $("#tbUsuarioGrupoLista").addRowData(data.Usuario.Grupos2[i].CodigoUsuarioGrupo, data.Usuario.Grupos2[i], "last", "after");

    $("#tbUsuarioPerfilLista").clearGridData();
    for (var i = 0; i < data.Usuario.Perfis2.length; i++)
        $("#tbUsuarioPerfilLista").addRowData(data.Usuario.Perfis2[i].CodigoPerfil, data.Usuario.Perfis2[i], "last", "after");

    $("#usuario_edicao").dialog( //1477
        {title: 
            "USUARIO [" 
            + $("#usuario_detalhe #txtLogin").val() 
            + " - "
            + $("#usuario_detalhe #txtnome").val() 
            + "] - Editar"});
    $("#usuario_edicao").dialog("open");
}

//================================================================================================
function salvarUsuario() {

    var usuarioSCF = {};
    var request = {};

    $("#usuario_detalhe").Forms("getData", { dataObject: usuarioSCF });
    
    usuarioSCF.Permissoes = new Array();
    var permissoesIds = $("#tbUsuarioPermissaoLista").getDataIDs();
    for (var i = 0; i < permissoesIds.length; i++) 
    {
        usuarioSCF.Permissoes[i] = new Object();
        usuarioSCF.Permissoes[i].CodigoPermissao = permissoesIds[i];
        usuarioSCF.Permissoes[i].Status = $("#tbUsuarioPermissaoLista").getCell(permissoesIds[i], "Status");
    }

    usuarioSCF.Grupos = new Array();
    var gruposIds = $("#tbUsuarioGrupoLista").getDataIDs();
    for (var i = 0; i < gruposIds.length; i++)
        usuarioSCF.Grupos[i] = gruposIds[i];

    usuarioSCF.Perfis = new Array();
    var perfisIds = $("#tbUsuarioPerfilLista").getDataIDs();
    for (var i = 0; i < perfisIds.length; i++)
        usuarioSCF.Perfis[i] = perfisIds[i];

    request.Novo = bNovoUsuario;
    request.UsuarioSCF = usuarioSCF
    request.UsuarioSCF.Bloqueado = usuarioSCF.Bloqueado;
    
//1436 - INICIO
    if(!usuarioSCF.DataExpiracaoSenha) // sem data de expiração de senha
    {
        var today = new Date(); 
        usuarioSCF.DataExpiracaoSenha = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0); // será a data do dia
    }        
    if(bNovoUsuario) // novo cadastro
    {
        if (!usuarioSCF.Senha) // sem senha digitada
            usuarioSCF.Senha = usuarioSCF.Login; // será o LOGIN conforme foi digitado
    }        
//1436 - FIM    

    if(usuarioSCF.Nome)
        request.UsuarioSCF.Nome = usuarioSCF.Nome;

    if (usuarioSCF.Cpf)
        request.UsuarioSCF.Cpf = usuarioSCF.Cpf;

    if (usuarioSCF.Matricula)
        request.UsuarioSCF.Matricula = usuarioSCF.Matricula;

    if (usuarioSCF.Login) 
    {
        request.UsuarioSCF.CodigoUsuario = usuarioSCF.Login;
        request.UsuarioSCF.Login = usuarioSCF.Login;
    }
    
    if (usuarioSCF.Senha)
        request.UsuarioSCF.Senha = usuarioSCF.Senha;

    if (usuarioSCF.DataBloqueadoInicio)
        request.UsuarioSCF.DataBloqueadoInicio = usuarioSCF.DataBloqueadoInicio;


    if (usuarioSCF.DataBloqueadoFim)
        request.UsuarioSCF.DataBloqueadoFim = usuarioSCF.DataBloqueadoFim;
        
    if (usuarioSCF.DataExpiracaoSenha)//1436
        request.UsuarioSCF.DataExpiracaoSenha = usuarioSCF.DataExpiracaoSenha;
        
    executarServico(
            "SalvarUsuarioSCFRequest",
            request,
            "salvarUsuarioCallback");
}

//================================================================================================
function salvarUsuarioCallback(data) {

    var strCriticas = "";

    if (data && data.Criticas.length > 0) {
        for (var i = 0; i < data.Criticas.length; i++) {
            strCriticas = strCriticas + (i + 1) + " - " + data.Criticas[i].Descricao + "<br />";
        }
        popup({
            titulo: "Erro ao Salvar Usuário",
            icone: "erro",
            mensagem: "Ocorreu o erro abaixo na tentativa de salvar Usuário: <br /><br />" + strCriticas,
            callbackOK: function() {
            }
        });
    } else {
        // Mostra popup de sucesso
    popup({
        titulo: "Salvar Usuário",
        icone: "sucesso",
        mensagem: "Usuário salvo com Sucesso!",
        callbackOK: function() {
            $("#usuario_edicao").dialog("close");
            atualizarLista();
        }
    });       
    }                    
}

// =====================================================================================
// 1356 - INICIO - EFETUAR O RESET DA SENHA UTILIZANDO AS CONFIGURACOES DE SEGURANCA
// =====================================================================================
function resetarSenha() 
{
    var nova_senha = $("#nova_senha").val();
    $("#nova_senha").val("");//1356
    var codigoUsuario = $("#tbUsuarioLista").getGridParam("selrow");
    var senha_atual = null;
    var v_OK = "S";
    if (!codigoUsuario && v_OK == "S") 
    {
        popup(
        {   titulo: "SENHA RESET - Erro"
        ,   mensagem: "Favor selecionar a linha do usuario que deseja efetuar o SENHA RESET"
        ,   icone: "erro"
        ,   callbackOK: function() { }
        });
        v_OK = "N";
    }
    if (!nova_senha && v_OK == "S")
    {
        popup(
        {   titulo: "SENHA RESET - Erro"
        ,   mensagem: "Favor digitar a nova senha"
        ,   icone: "erro"
        ,   callbackOK: function() { }
        });
        v_OK = "N";
    }
    if (v_OK == "S")
    {   popup(
        {   titulo: "SENHA RESET - Confirmação"
        ,   mensagem: "Confirma RESET de senha do usuario [" + codigoUsuario + "] ?"
        ,   icone: "alerta"
        ,   callbackSim: function() { confirmadoresetarSenha(codigoUsuario, nova_senha) }
        ,   callbackNao: function() { }
        });                
    }
}

//================================================================================================
function confirmadoresetarSenha(codigoUsuario, nova_senha) 
{   var v_Descricao = "";
    executarServico("AlterarSenhaRequest", {CodigoUsuario: codigoUsuario, NovaSenha: nova_senha}, function(data)
    {   if (data && data.Criticas.length > 0) 
        {   for (var i = 0; i < data.Criticas.length; i++) 
            {   v_Descricao = v_Descricao + (i + 1) + "-" + data.Criticas[i].Descricao + "<br \>";
            }
            popup(
            {   titulo: "SENHA RESET - Erro"
            ,   mensagem: v_Descricao
            ,   icone: "erro"
            ,   callbackOK: function() {alterar_senha_limpar();}
            });                
        }    
        else
        {
            popup(
            {   titulo: "SENHA RESET - SUCESSO"
            ,   mensagem: "Usuario com senha resetada"
            ,   icone: "sucesso"
            ,   callbackOK: function() {alterar_senha_limpar();window.location = "login.aspx"}
            });                
        }
    }); 
}
// =====================================================================================
// 1356 - FIM - EFETUAR O RESET DA SENHA UTILIZANDO AS CONFIGURACOES DE SEGURANCA
// =====================================================================================

//================================================================================================
function adicionarPermissao() {
    if (!permissoesCarregadas) {
        carregarPermissoes();
        permissoesCarregadas = true;
    }
    $("#permissao_lista").dialog("open");
}

//================================================================================================
function carregarPermissoes() {
    executarServico("ListarPermissoesRequest", {}, "carregarPermissoesCallback");
}
//================================================================================================
function carregarPermissoesCallback(data) {
    $("#tbPermissaoLista").clearGridData();
    for (var i = 0; i < data.Permissoes.length; i++)
        $("#tbPermissaoLista").addRowData(data.Permissoes[i].PermissaoInfo.CodigoPermissao, data.Permissoes[i].PermissaoInfo, "last", "after");
}
//================================================================================================
function adicionarPerfil() {
    if (!perfisCarregados) {
        carregarPerfis();
        perfisCarregados = true;
    }
    $("#perfil_lista").dialog("open");
}
//================================================================================================
function carregarPerfis() {
    executarServico("ListarPerfisRequest", {}, "carregarPerfisCallback");
}
//================================================================================================
function carregarPerfisCallback(data) {
    $("#tbPerfilLista").clearGridData();
    for (var i = 0; i < data.Perfis.length; i++)
        $("#tbPerfilLista").addRowData(data.Perfis[i].CodigoPerfil, data.Perfis[i], "last", "after");
}
//================================================================================================
function adicionarGrupo() {
    if (!gruposCarregados) {
        carregarGrupos();
        gruposCarregados = true;
    }
    $("#grupo_lista").dialog("open");
}
//================================================================================================
function carregarGrupos() {
    executarServico("ListarUsuarioGruposRequest", {}, "carregarGruposCallback");
}
//================================================================================================
function carregarGruposCallback(data) {
    $("#tbGrupoLista").clearGridData();
    for (var i = 0; i < data.UsuarioGrupos.length; i++)
        $("#tbGrupoLista").addRowData(data.UsuarioGrupos[i].CodigoUsuarioGrupo, data.UsuarioGrupos[i], "last", "after");
}
//1436
//function getDiasRestantes(dataExpiracao, dataAtual) {

//    var one_day = 1000 * 60 * 60 * 24;
//    var restantes = Math.ceil((dataExpiracao.getTime() - dataAtual.getTime()) / (one_day));
//    return restantes - 1;
//}

