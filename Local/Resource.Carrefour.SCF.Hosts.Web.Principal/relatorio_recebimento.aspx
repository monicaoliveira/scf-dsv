﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorio_recebimento.aspx.cs" 
EnableSessionState="false" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.relatorio_recebimento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Relatório de Recebimentos</title>
    <link href="relatorio_recebimento.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <header>
        <table>
            <tr>
                <td colspan="8">
                    <h1>RELATÓRIO DE RECEBIMENTOS<asp:Label runat="server" ID="lblTitulo"></asp:Label></h1>  
                </td>
                <td colspan="2" align="right">
                    <label class="negrito pequeno">Data/Hora: </label> 
                    <asp:Label class="pequeno" runat="server" ID="lblDataHora"></asp:Label>
                </td>                                                                       
            </tr>                        
            <tr>
                <td colspan="10"><br />
                    <label class="negrito medio">Filtros:</label>
                </td>
            </tr>                        
            <tr>
                <td colspan="2" valign="top">
                    <label class="negrito medio">Grupo: </label> 
                    <asp:Label runat="server" ID="lblFiltroGrupo"></asp:Label>
                </td>
                <td colspan="2" valign="top">
                    <label class="negrito medio">Referência: </label> 
                    <asp:Label runat="server" ID="lblFiltroReferencia"></asp:Label>
                </td>
                <td colspan="2" valign="top">
                    <label class="negrito medio">Meio de Pagamento: </label> 
                    <asp:Label runat="server" ID="lblFiltroMeioPagamento"></asp:Label>
                </td>
                <td colspan="4" valign="top">
                    <label class="negrito medio">Período de Agendamento de: </label> 
                    <asp:Label runat="server" ID="lblFiltroDataInicio"></asp:Label>
                    <label class="negrito medio">até: </label> 
                    <asp:Label runat="server" ID="lblFiltroDataFim"></asp:Label>
                </td>
            </tr>          
            <tr runat="server" id="trMensagem" >
                <td colspan="10" class="grupo" style="width:100%; border-top:solid 3px #4682B4;">
                    <asp:Label class="pequeno" runat="server" ID="lblMensagem"></asp:Label>
                </td>
            </tr>
        </table>
    </header>
    <asp:Repeater runat="server" ID="RecebimentosRepeater">
        <HeaderTemplate>
            <table>
                <thead>
                    <tr>
                        <th colspan="10" class="linhaGrossa">
                        </th>
                    </tr>
                    <tr>
                        <th class="negrito medio centro fixoTexto">Grupo</th>
                        <th class="negrito medio centro fixoTexto">Referência</th>
                        <th class="negrito medio centro">CNPJ</th>
                        <th class="negrito medio esquerda">Estabelecimento</th>
                        <th class="negrito medio centro  fixoValor">Data de<br />Agendamento</th>
                        <th class="negrito medio centro  ">Meio de<br />Pagamento</th>
                        <th class="negrito medio direita fixoValor">Qtde<br />Recebimentos</th>
                        <th class="negrito medio direita fixoValor">Valor<br />Recebimentos</th>
                        <th class="negrito medio direita fixoValor">Valor Taxa<br />Recebimentos</th>
                        <th class="negrito medio direita fixoValor">Taxa<br />Recebimento</th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td class="pequeno  centro"><%# Eval("NomeGrupo")%></td>
                    <td class="pequeno  centro"><%# Eval("DescricaoReferencia")%></td>
                    <td class="pequeno  centro"><%# Eval("Estabelecimento.CNPJ")%></td>
                    <td class="pequeno  esquerda"><%# Eval("Estabelecimento.RazaoSocial")%></td>
                    <td class="pequeno  centro"><%# Convert.ToDateTime(Eval("DataAgendamento")).ToString("dd/MM/yyyy")%></td>
                    <td class="pequeno  centro"><%# Eval("DescricaoMeioPagamento")%></td>
                    <td class="pequeno direita"><%# Convert.ToDecimal(Eval("QtdeRecebimento")).ToString("N0")%></td>
                    <td class="pequeno direita"><%# Convert.ToDecimal(Eval("ValorRecebimento")).ToString("N2")%></td>
                    <td class="pequeno direita"><%# Convert.ToDecimal(Eval("ValorTaxaRecebimento")).ToString("N2")%></td>
                    <td class="pequeno direita"><%# FormataTaxa(Eval("TaxaRecebimento").ToString(), Eval("TipoTaxaRecebimento").ToString())%></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
                <tr>
                    <td colspan="6" class="medio negrito direita">Total Geral:</td>
                    <td class="medio negrito direita"><asp:Label runat="server" ID="lblTotalQtdeRecebimento"></asp:Label></td>
                    <td class="medio negrito direita"><asp:Label runat="server" ID="lblTotalValorRecebimento"></asp:Label></td>
                    <td class="medio negrito direita"><asp:Label runat="server" ID="lblTotalValorTaxaRecebimento"></asp:Label></td>
                    <td class="medio negrito direita"></td>
                </tr>
                </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    </form>
</body>
</html>
