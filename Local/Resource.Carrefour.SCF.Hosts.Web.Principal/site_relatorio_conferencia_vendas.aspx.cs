﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using System.Drawing;
using Resource.Framework.Contratos.Comum;
using System.Text;
using System.IO;
using System.Data;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using System.Text.RegularExpressions;
using Resource.Framework.Library.Servicos.Mensagens;


namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class site_relatorio_conferencia_vendas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["acao"] == null && !IsPostBack)
            {
                MensagemResponseBase retorno = null;
                //-------------------------------------------------------------------------------------------
                // CARREGA COMBO DE GRUPOS 
                //-------------------------------------------------------------------------------------------
                try
                {
                    if (Session["CodigoSessao"] != null)
                    {

                        // Recebe lista de grupos
                        ListarEmpresaGrupoResponse grupos =
                            Mensageria.Processar<ListarEmpresaGrupoResponse>(
                                new ListarEmpresaGrupoRequest()
                                {
                                    CodigoSessao = Session["CodigoSessao"].ToString()
                                });

                        // Preenche combo
                        rptCmbGrupo.DataSource = grupos.Resultado;
                        rptCmbGrupo.DataBind();

                        //-------------------------------------------------------------------------------------------
                        // CARREGA COMBO DE FAVORECIDOS 
                        //-------------------------------------------------------------------------------------------
                   
                        // Recebe lista de grupos
                        ListarFavorecidoResponse favorecidos =
                            Mensageria.Processar<ListarFavorecidoResponse>(
                                new ListarFavorecidoRequest()
                                {
                                    CodigoSessao = Session["CodigoSessao"].ToString()
                                });

                        // Preenche combo
                        rptCmbFavorecido.DataSource = favorecidos.Resultado;
                        rptCmbFavorecido.DataBind();

                        //-------------------------------------------------------------------------------------------
                        // CARREGA COMBO DE ESTABELECIMENTOS 
                        //-------------------------------------------------------------------------------------------

                        // Recebe lista de grupos
                        ListarEstabelecimentoResponse estabelecimentos =
                            Mensageria.Processar<ListarEstabelecimentoResponse>(
                                new ListarEstabelecimentoRequest()
                                {
                                    CodigoSessao = Session["CodigoSessao"].ToString(),

                                });

                        // Preenche combo
                        rptCmbEstabelecimento.DataSource = estabelecimentos.Resultado;
                        rptCmbEstabelecimento.DataBind();
                    }
                    else
                    {
                        retorno = new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                            DescricaoResposta = "Sessão inválida"
                        };
                    }
                }
                catch (Exception ex)
                {
                    retorno = new MensagemErroResponse()
                    {
                        StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                        DescricaoResposta = ex.ToString()
                    };

                }

            }
        }

        protected string GetUrlRelatorio()
        {
            /* **************************************
             * PARA TESTAR O PLUGIN GERADOR DE PDF 
             * É NECESSÁRIO ALTERAR O VALOR DA PORTA 
             * DE ACORDO COM A SUA MÁQUINA
             * **************************************/

            string url = "http://localhost:55143/relatorio_conferencia_vendas.aspx";

            string parametros = "";
            parametros += this.Request["CodigoEmpresaSubgrupo"] != "?" ? this.Request["CodigoEmpresaSubgrupo"] + "&" : null;
            parametros += this.Request["CodigoEstabelecimento"] != "?" ? this.Request["CodigoEstabelecimento"] + "&" : null;
            parametros += this.Request["CodigoFavorecido"] != "?" ? this.Request["CodigoFavorecido"] + "&" : null;
            parametros += this.Request["DataInicioVenda"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioVenda"]) + "&" : null;
            parametros += this.Request["DataFimVenda"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimVenda"]) + "&" : null;            

            if (parametros != "")
                url += "?" + parametros.Substring(0, parametros.Length - 1);

            return url;
        }
    }
}
