﻿$(document).ready(function() {

    // Namespace de arquivo
    if (!$.arquivoItem)
        $.arquivoItem = {};

    // Funcoes do namespace arquivo.lista
    $.arquivoItem.lista = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  abrir: abre o dialog de lista de arquivo item
        // --------------------------------------------------------------------
        abrir: function(filtro, callback) {

            // Garante que a tela está carregada
            $.arquivoItem.lista.load(function() {

                // Funcao de resize
                var funcaoResize = function() {
                    $("#tbArquivoItem").setGridWidth($("#site_arquivo_item_lista").width() - 15);
                    $("#tbArquivoItem").setGridHeight($("#site_arquivo_item_lista").height() - 180);
                };

                // Inicializa o dialog
                $("#site_arquivo_item_lista").dialog({
                    autoOpen: false,
                    height: 450,
                    width: 950,
                    modal: true,
                    title: "Lista de Linhas de Arquivo",
                    buttons: {
                        "Fechar": function() {
                            // Fecha o dialog
                            $("#site_arquivo_item_lista").dialog("close");
                        }
                    },
                    resize: funcaoResize
                });

                // Mostra o dialog
                $("#site_arquivo_item_lista").dialog("open");

                // Pede a lista
                $.arquivoItem.lista.listar(filtro, function() {

                    // Pede o resize
                    funcaoResize();

                    // Se tem, chama o callback
                    if (callback)
                        callback();
                });

            });
        },

        // ----------------------------------------------------------------------
        // load: garante que o HTML da esteja carregado e inicializado
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        load: function(callback) {


            // Verifica se o html esta carregado
            if ($("#site_arquivo_item_lista").length == 0) {

                // Obtenho os valores do combo de status
                carregarListas("ArquivoItemStatusEnum", function() {

                    // Obtem o valor no formato do JqGrid
                    $.arquivoItem.lista.variaveis.arquivoItemStatusEnum = retornarGridSelect("ArquivoItemStatusEnum");

                    // Pede o html e insere na área temporaria
                    $("#areaTemporaria").load("site_arquivo_item_lista.aspx #site_arquivo_item_lista", function() {

                        // Copia o elemento para a área comum
                        $("#site_arquivo_item_lista").appendTo("#areaComum");

                        // Completa o load
                        $.arquivoItem.lista.load2(function() {

                            // Chama o callback
                            if (callback)
                                callback();

                        });

                    });

                });

            } else {
                // Chama o callback
                if (callback)
                    callback();
            }

        },

        // --------------------------------------------------------------------
        //  load2: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load2: function(callback) {

            // Layout dos botões
            $("#site_arquivo_item_lista button").button();

            // Botao criticas
            $("#site_arquivo_item_lista .cmdCriticas").click(function() {

                // Verifica se tem arquivo selecionado
                var codigoArquivoItem = $("#tbArquivoItem").jqGrid("getGridParam", "selrow");
                if (codigoArquivoItem) {

                    // Retira o prefixo do código
                    codigoArquivoItem = codigoArquivoItem.replace("arquivoItem_", "");

                    // Lista as criticas deste arquivo item
                    $.critica.lista.abrir({ FiltroCodigoArquivoItem: codigoArquivoItem });

                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }

            });

            // Callback
            if (callback)
                callback();
        },

        // --------------------------------------------------------------------
        //  limparLista: limpa a lista de itens
        // --------------------------------------------------------------------
        limparLista: function() {
            // Descarrega o grid
            $("#tbArquivoItem").jqGrid("clearGridData");
        },

        // --------------------------------------------------------------------
        //  listar: preenche a lista de arquivo item
        // --------------------------------------------------------------------
        listar: function(filtro, callback) {

            // Indica que deve incluir valores
            filtro.IncluirValores = true;
            filtro.FiltrarComCritica = true;

            // Limpa as linhas do grid
            $("#tbArquivoItem").jqGrid("clearGridData");

            // Pede a lista de itens
            executarServico(
                "ListarArquivoItemPorTipoLinhaRequest",
                filtro,
                function(data) {

                    // Descarrega o grid
                    $("#tbArquivoItem").jqGrid("GridUnload");

                    // Prepara as colunas
                    var colNames = [];
                    var colModel = [];

                    // Adiciona colunas fixas
                    colNames.push("");
                    colModel.push({ name: "Acao", width: 50, editable: false, align: "center" });
                    colNames.push("CodigoArquivoItem");
                    colModel.push({ name: "CodigoArquivoItem", width: 120, editable: false });
                    colNames.push("Status");
                    //1371
                   // colModel.push({ name: "StatusArquivoItem", width: 175, editable: true, edittype: "select", editoptions: { value: $.arquivoItem.lista.variaveis.arquivoItemStatusEnum}});
                    colModel.push({ name: "StatusArquivoItem", width: 175, editable: false});

                    // Adiciona as colunas
                    for (var i = 0; i < data.Cabecalho.length; i++) {

                        // Caption
                        colNames.push(data.Cabecalho[i]);

                        // Restante...
                        // Não deixa editar a primeira coluna, que é o tipo da linha
                        var colModelItem = { name: "col" + i.toString(), width: 120, editable: i > 0 ? true : false };
                        colModel.push(colModelItem);

                    }

                    // Inicializa o grid
                    $("#tbArquivoItem").jqGrid({
                        datatype: "clientArray",
                        colNames: colNames,
                        colModel: colModel,
                        height: 170,
                        width: 1050,
                        shrinkToFit: false
                    });

                    // Prepara modelo dos botoes de acao
                    var acao = "<input type='image' src='img/pen.png' style='width: 16px; height: 16px;' title='Editar' class='cmdEditar' />&nbsp;";
                    acao += "<input type='image' src='img/save_1.png' style='width: 16px; height: 16px;' title='Salvar' class='cmdSalvar' />&nbsp;";
                    acao += "<input type='image' src='img/button_white_remove.png' style='width: 16px; height: 16px;' title='Cancelar' class='cmdCancelar' />&nbsp;";

                    // Adiciona as linhas
                    for (var i = 0; i < data.Resultado.length; i++) {

                        // Prepara a linha
                        var item = data.Resultado[i];
                        var objLinha = { CodigoArquivoItem: data.Resultado[i].CodigoArquivoItem, StatusArquivoItem: data.Resultado[i].StatusArquivoItem, Acao: acao };
                        for (var j = 0; j < item.Valores.length; j++) {
                            objLinha["col" + j.toString()] = item.Valores[j];
                        }

                        // Adiciona a linha
                        $("#tbArquivoItem").jqGrid("addRowData", "arquivoItem_" + data.Resultado[i].CodigoArquivoItem, objLinha);

                        // Trata os eventos dos botoes da linha
                        $("#arquivoItem_" + data.Resultado[i].CodigoArquivoItem + " .cmdEditar").click($.arquivoItem.lista.editarLinha);
                        $("#arquivoItem_" + data.Resultado[i].CodigoArquivoItem + " .cmdSalvar").click($.arquivoItem.lista.salvarLinha).hide();
                        $("#arquivoItem_" + data.Resultado[i].CodigoArquivoItem + " .cmdCancelar").click($.arquivoItem.lista.cancelarLinha).hide();
                    }

                    // Callback
                    if (callback)
                        callback();

                });
        },

        // --------------------------------------------------------------------
        //  editarLinha: edita uma linha
        // --------------------------------------------------------------------
        editarLinha: function() {

            // Pega o tr 
            var tr = $(this).closest("tr");

            // Coloca a linha em edição
            $("#tbArquivoItem").jqGrid("editRow", tr.attr("id"), false, function(rowid) {

                // Para cada input criado...
                $("input", tr).each(function(index, element) {

                    // Salva os valores originais
                    $(element).attr("original", $(element).val());

                    // Trata evento de change
                    $(element).change(function() {
                        if ($(this).val() != $(this).attr("original"))
                            $(this).addClass("alterado");
                        else
                            $(this).removeClass("alterado");
                    });

                });

            });

            // Esconde o ícone de edição e mostra o de salvar e cancelar
            $(".cmdEditar", tr).hide();
            $(".cmdSalvar", tr).show();
            $(".cmdCancelar", tr).show();
        },

        // --------------------------------------------------------------------
        //  salvarLinha: salva uma linha
        // --------------------------------------------------------------------
        salvarLinha: function() {

            // Pega o tr 
            var tr = $(this).closest("tr");

            // Pega o nome das colunas para montar o request
            var colNames = $("#tbArquivoItem").jqGrid("getGridParam", "colNames");

            // Prepara o request
            var request = { CodigoArquivoItem: $(tr).attr("id").replace("arquivoItem_", ""), StatusArquivoItem: null, NomeCampos: [], Valores: [] };
            request.StatusArquivoItem = $("#arquivoItem_" + request.CodigoArquivoItem + "_StatusArquivoItem").val();

            // Monta lista de itens alterados. Cria coleção com o nome dos campos e os valores alterados
            $("input.alterado", tr).each(function(index, element) {
                request.NomeCampos.push(colNames[parseInt($(element).attr("name").replace("col", "")) + 3]);
                request.Valores.push($(element).val());
            });

            // Chama o servico
            executarServico("SalvarArquivoItemDetalheRequest", request, function(data) {

                // Salva valores na linha e tira de edicao
                $("#tbArquivoItem").jqGrid("saveRow", $(tr).attr("id"), null, "clientArray");

                // Esconde o de salvar e cancelar e mostra o de editar
                $(".cmdEditar", tr).show();
                $(".cmdSalvar", tr).hide();
                $(".cmdCancelar", tr).hide();

                // Mensagem de sucesso
                popup({
                    titulo: "Item Salvo com Sucesso",
                    icone: "sucesso",
                    mensagem: "Linha salva com sucesso",
                    callbackOK: function() { }
                });

            });
        },

        // --------------------------------------------------------------------
        //  cancelarLinha: cancela a edição de uma linha
        // --------------------------------------------------------------------
        cancelarLinha: function() {

            // Pega o tr 
            var tr = $(this).closest("tr");

            // Coloca a linha em edição
            $("#tbArquivoItem").jqGrid("restoreRow", tr.attr("id"));

            // Esconde o de salvar e cancelar e mostra o de editar
            $(".cmdEditar", tr).show();
            $(".cmdSalvar", tr).hide();
            $(".cmdCancelar", tr).hide();
        }

    };

    // Dispara o load, caso tenha sido chamado diretamente
    if (window.location.href.indexOf("site_arquivo_item_lista") > 0) {
        // Inicializa a tela
        $.arquivoItem.lista.load();
    }

});