﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_arquivo_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_arquivo_item_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_item_lista.js" type="text/javascript"></script>
    <script src="site_critica_lista.js" type="text/javascript"></script>
    <script src="site_critica_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_alteracao_massiva.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <!-- Detalhe de Arquivo -->
    <div id="site_arquivo_detalhe">
        
        <!-- Título de Resumo -->
        <div class="titulo">Detalhes do Arquivo</div>
        
        <!-- Resumo do Arquivo -->
        <div class="resumoArquivo">
        
            <table>
                <tr><td><span>Nome do Arquivo:</span></td><td colspan="4"><input type="text" class="txtNomeArquivo forms[property[NomeArquivo]]" /></td></tr>
                <tr><td><span>Código do Arquivo:</span></td><td colspan="4"><input type="text" id="txtCodigoArquivo" class="forms[property[CodigoArquivo]]" />
                </td></tr><tr><td><span>Tipo do Arquivo:</span></td><td><input type="text" id="txtTipoArquivo" class="forms[property[TipoArquivo]]" /></td><td class="separador"> <%--SCF1279 (id="txtTipoArquivo")--%>
                </td><td><span>Data de Inclusão:</span></td><td><input type="text" class="forms[property[DataInclusao]]" /></td><td class="separador">
                </td></tr><tr><td><span>Código do Processo:</span></td><td><input type="text" id="txtCodigoProcesso" class="forms[property[CodigoProcesso]]" /></td><td class="separador">
                </td><td><span>Status do Arquivo:</span></td><td><input type="text" class="forms[property[StatusArquivo]]" /></td><td class="separador">
                </td></tr><tr><td><span>Total de Linhas:</span></td><td><input type="text" class="forms[property[QuantidadeLinhas]]" /></td><td class="separador">
                </td><td><span>Total de Críticas:</span></td><td><input type="text" class="forms[property[QuantidadeCriticas]]" /></td><td class="separador">
                </td></tr>
            </table>
        
        </div>
        
        <!-- Título de Críticas -->
        <div class="titulo">Resumo de Críticas</div>
        
        <!-- Layout da tela -->
        <table>
            <tr valign="top"><td>
            
                <!-- TreeView com estatisticas de críticas -->
                <div class="treeview" style="width: 290px; height: 250px; overflow-y: scroll;">
                    <ul class="filetree tvwCriticas">
                    </ul>
                </div>
            
            </td><td>
            
                <!-- detalhe -->
                <div class="abas">
                
                    <!-- Abas -->
                    <ul>
                        <li><a href="#site_arquivo_detalhe_aba_resumo">Resumo</a></li>
                    </ul>
                
                    <!-- Aba de Resumo -->
                    <div id="site_arquivo_detalhe_aba_resumo">
                        <table>
                            <tr><td><span>Quantidade de Linhas</span></td><td><input type="text" class="forms[property[QuantidadeLinhas]]" /></td></tr>
                            <tr><td><span>Quantidade de Críticas</span></td><td><input type="text" class="forms[property[QuantidadeCriticas]]" /></td></tr>
                            <tr><td><span>Linhas Bloqueadas</span></td><td><input type="text" class="forms[property[QuantidadeLinhasBloqueadas]]" /></td></tr>
                            <tr><td><span>Linhas Excluídas</span></td><td><input type="text" class="forms[property[QuantidadeLinhasExcluidas]]" /></td></tr>
                            <tr><td><span>Linhas Pendentes de Desbloqueio</span></td><td><input type="text" class="forms[property[QuantidadeLinhasPendentesDesbloqueio]]" /></td></tr>
                            <tr><td><span>Linhas Pendentes de Validação</span></td><td><input type="text" class="forms[property[QuantidadeLinhasPendentesValidacao]]" /></td></tr>
                            <tr><td><span>Linhas Inválidas</span></td><td><input type="text" class="forms[property[QuantidadeLinhasInvalidas]]" /></td></tr>
                        </table>
                        <button class="cmdLinhas">Linhas</button>
                        <button class="cmdCriticas">Críticas</button>
                        <button class="cmdAlterarEmMassa">Alteração Massiva</button>
                        <br />
                        <button class="cmdBloquear">Bloquear</button>
                        <button class="cmdDesbloquear">Desbloquear</button>
                        <button class="cmdExcluir">Excluir</button>
                    </div>

                </div>
                
            </td></tr>
        </table>
        
    </div>
    
</asp:Content>
