﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class relatorio_vencimento_flat : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Faz o log
            Mensageria.Processar<SalvarLogResponse>(
                new SalvarLogRequest()
                {
                    Descricao = "Relatório de Vencimento Flat - Iniciando a geração do relatório flat."
                });


            // Faz o log
            Mensageria.Processar<SalvarLogResponse>(
                new SalvarLogRequest()
                {
                    Descricao = "Relatório de Vencimento Flat - Recuperação das informações do banco de dados."
                });
            string CodigoSessaoTratado = null;
            if (Session["CodigoSessao"] != null)
            {
                CodigoSessaoTratado = (string)this.Session["CodigoSessao"];
            }
            else
            {
                CodigoSessaoTratado = "Sem Sessao";
            }
            // recupera as informacoes do banco de dados
            // Pede o detalhe da pessoa
            ListarRelatorioVencimentoResponse resposta =
                    Mensageria.Processar<ListarRelatorioVencimentoResponse>(
                        new ListarRelatorioVencimentoRequest()
                        {
                            CodigoSessao = CodigoSessaoTratado,
                            FiltroFavorecido = this.Request["CodigoFavorecido"],
                            FiltroEmpresaGrupo = this.Request["CodigoEmpresaGrupo"],
                            FiltroEmpresaSubGrupo = this.Request["CodigoEmpresaSubGrupo"],
                            FiltroProduto = this.Request["CodigoProduto"],
                            FiltroTipoRegistro = this.Request["TipoRegistro"],
                            FiltroConsolidaAgendamentoVencimento = Convert.ToBoolean(this.Request["ConsolidaAgendamentoVencimento"]),
                            FiltroConsolidaMaiorData = Convert.ToBoolean(this.Request["ConsolidacaoMaiorAgendamento"]),
                            FiltroDataInicioTransacao = this.Request["DataInicioTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioTransacao"]) : null,
                            FiltroDataFimTransacao = this.Request["DataFimTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimTransacao"]) : null,
                            FiltroDataInicioVencimento = this.Request["DataInicioVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioVencimento"]) : null,
                            FiltroDataFimVencimento = this.Request["DataFimVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimVencimento"]) : null,
                            FiltroDataInicioMovimento = this.Request["DataInicioMovimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioMovimento"]) : null,
                            FiltroDataFimMovimento = this.Request["DataFimMovimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimMovimento"]) : null,
                            FiltroDataAgendamentoDe = this.Request["DataAgendamentoDe"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataAgendamentoDe"]) : null,
                            FiltroDataAgendamentoAte = this.Request["DataAgendamentoAte"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataAgendamentoAte"]) : null
                        });

            // Faz o log
            Mensageria.Processar<SalvarLogResponse>(
                new SalvarLogRequest()
                {
                    Descricao = "Relatório de Vencimento Flat - Finalizada a recuperação das informações do banco de dados."
                });

            dgRelatorioVencimento.DataSource = resposta.Resultado;
            dgRelatorioVencimento.DataBind();

            // Faz o log
            Mensageria.Processar<SalvarLogResponse>(
                new SalvarLogRequest()
                {
                    Descricao = "Relatório de Vencimento Flat - Geração do relatório finalizado."
                });
        }
    }
}
