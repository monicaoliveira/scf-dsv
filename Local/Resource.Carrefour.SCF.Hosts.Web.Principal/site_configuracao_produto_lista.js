﻿$(document).ready(function() {

    // Cria namespace 
    if (!$.produto)
        $.produto = {};

    // Funcoes do namespace 
    $.produto.lista = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Produto");

            // Estilo dos botões
            $("#site_configuracao_produto_lista button").button();

            // Inicializa forms
            $("#site_configuracao_produto_lista").Forms("initializeFields");

            // Inicializa grid de produto 
            $("#tbProduto").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 320,
                colNames: ["Código", "Produto", "Descrição", "Produto TSYS", "Cedível", "Prazo de Pagamento", "Dias vencidos \n (CP/AP/CV/AV)", "Dias Vencidos (AJ)" ],
                colModel: [
                    { name: "CodigoProduto", index: "CodigoProduto", sorttype: "integer", hidden: true, key: true },
                    { name: "NomeProduto", index: "NomeProduto", sorttype: "text", width: 120 },
                    { name: "DescricaoProduto", index: "DescricaoProduto" },
                    { name: "CodigoProdutoTSYS", index: "CodigoProdutoTSYS",  width: 120},
                    { name: "Cedivel", index: "Cedivel", sorttype: "text", formatter: formatterBoolean, width: 70 },
                    { name: "PrazoPagamento", index: "PrazoPagamento" },
                    { name: "DiasVencidos", index: "DiasVencidos", width: 200 },
                    { name: "DiasVencidosAJ", index: "DiasVencidosAJ" },
                ],
                scroll: true,
                ondblClickRow: function(rowid, iRow, iCol, e) {

                    // Pega o código do produto
                    var codigoProduto = $("#tbProduto").getGridParam("selrow");

                    if (codigoProduto) {

                        // Abre dialog de detalhe do produto
                        $.produto.detalhe.abrir(codigoProduto, null, function() {
                            $.produto.lista.listarProduto(function() { });
                        });
                    }

                    else {

                        // Pede para selecionar a linha
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Favor selecionar uma linha!",
                            callbackOK: function() { }
                        });
                    }
                }
            });

            // BOTÃO NOVO PRODUTO
            $("#cmdNovoProduto").click(function() {

                // Abre dialog de detalhe do produto
                $.produto.detalhe.abrir(null, null, function() {
                    //$("#tbProduto").jqGrid("addRowData", data.ProdutoInfo.CodigoProduto, data.ProdutoInfo);
                    $.produto.lista.listarProduto(function() { });
                });
            });

            // BOTÃO EDITAR PRODUTO
            $("#cmdEditarProduto").click(function() {

                // Pega o código do produto
                var codigoProduto = $("#tbProduto").getGridParam("selrow");

                if (codigoProduto) {

                    // Abre dialog de detalhe do produto
                    $.produto.detalhe.abrir(codigoProduto, null, function() {
                        $.produto.lista.listarProduto(function() { });
                    });
                }
                else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // BOTÃO REMOVER PRODUTO
            $("#cmdRemoverProduto").click(function() {

                // Pega o código do produto
                var codigoProduto = $("#tbProduto").getGridParam("selrow");

                executarServico(
                "ReceberProdutoRequest",
                { CodigoProduto: codigoProduto },
                function(data) {
                    if (data.ProdutoInfo) {

                        if (codigoProduto) {

                            // pede confirmação de exclusão
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "Tem certeza que deseja remover este produto?",
                                callbackSim: function() {

                                    // Pede para remover
                                    $.produto.detalhe.remover(codigoProduto, function() {

                                        // Mostra popup de sucesso
                                        popup({
                                            titulo: "Remover Produto",
                                            icone: "sucesso",
                                            mensagem: "Produto removido com sucesso!",
                                            callbackOK: function() {
                                                $.produto.lista.listarProduto(function() { });
                                            }
                                        });
                                    });
                                },
                                callbackNao: function() {

                                }
                            });
                        }
                        else {

                            // Pede para selecionar a linha
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "Favor selecionar uma linha!",
                                callbackOK: function() {

                                }
                            });
                        }
                    }
                    else {
                        //Avisa o usuario que o produto já foi excluido.
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Produto já foi excluido!",
                            callbackOK: function() {
                                $.produto.lista.listarProduto(function() { });
                            }
                        });
                    }

                });
            });


            // BOTÃO ATUALIZAR LISTA
            $("#cmdAtualizarListaProduto").click(function() {

                // Atualiza lista de produtos
                $.produto.lista.listarProduto(function() { });
            });

            // PEDE LISTA DE PRODUTOS
            $.produto.lista.listarProduto(function() { });

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbProduto").setGridWidth($(window).width() - margemEsquerda);
                $("#tbProduto").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");
        },

        //------------------------------------------------------------------------
        // listarProduto: Pede listas de produtos ao serviço
        //------------------------------------------------------------------------
        listarProduto: function(callback) {

            // Cria request
            var request = {};

            request.FiltroMaxLinhas = 100;

            // Pede lista de produtos
            executarServico(
            "ListarProdutoRequest",
            request,
            function(data) {

                // Limpa grid
                $("#tbProduto").clearGridData();
                for (var i = 0; i < data.Resultado.length; i++)
                    $("#tbProduto").jqGrid("addRowData", data.Resultado[i].CodigoProduto, data.Resultado[i]);

                // Chama callback
                if (callback)
                    callback();
            });
        }
    };

    //



    // Pede a inicializacao
    $.produto.lista.load();
});