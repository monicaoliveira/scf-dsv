﻿$(document).ready(function() {

    // Namespace
    if (!$.estabelecimento)
        $.estabelecimento = {};

    // Funcoes do namespace 
    $.estabelecimento.detalhe = {

        variaveis: {},

        // ----------------------------------------------------------------------
        // load: funcoes de inicializacao da tela
        // ----------------------------------------------------------------------
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_configuracao_estabelecimento_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_configuracao_estabelecimento_detalhe.aspx #site_configuracao_estabelecimento_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_configuracao_estabelecimento_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.estabelecimento.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();
                    });
                });

            } else {

                // Chama o callback
                if (callback)
                    callback();
            }
        },

        // --------------------------------------------------------------------
        //  load2: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load2: function() {

            // Inicializa forms
            $("#site_configuracao_estabelecimento_detalhe").Forms("initializeFields");

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Estabelecimentos");

            // Estilo dos botões
            $("#site_configuracao_estabelecimento_detalhe button").button();

            // Inicializa o dialog do detalhe
            $("#site_configuracao_estabelecimento_detalhe").dialog({
                autoOpen: false,
                height: 200,
                width: 640,
                modal: true,
                title: "Detalhe do Estabelecimento",
                buttons: {
                    "Salvar": function() {
                        // Pede para salvar
                        $.estabelecimento.detalhe.salvar(function() {

                            // Mostra popup de sucesso
                            popup({
                                titulo: "Salvar Estabelecimento",
                                icone: "sucesso",
                                mensagem: "Estabelecimento salvo com sucesso!",
                                callbackOK: function() {

                                    // Fecha dialog de detalhe
                                    $("#site_configuracao_estabelecimento_detalhe").dialog("close");

                                    $.estabelecimento.lista.listar();
                                }
                            });
                        });
                    },

                    "Fechar": function() {
                        // Fecha o dialog
                        $("#site_configuracao_estabelecimento_detalhe").dialog("close");
                    }
                }
            });

            // -------------------------------------------------------------------
            // Evento change do combo de grupo 
            // -------------------------------------------------------------------
            $("#cmbGrupo").change(function() {

                // Pega código do grupo, se tiver
                var codigoEmpresaGrupo = $("#cmbGrupo").val();
                if (codigoEmpresaGrupo != "?") {
                    // Carrega combo de subgrupos correspondente ao grupo
                    $.estabelecimento.detalhe.carregarSubGrupos(codigoEmpresaGrupo);
                }

            });

            // Preenche os combos
            $.estabelecimento.detalhe.carregarCombosGrupoFavorecido();
        },

        // --------------------------------------------------------------------
        // Carrega os combos de grupo e favorecidos
        // (ATENÇÃO: esta função é chamada externamente)
        // --------------------------------------------------------------------
        carregarCombosGrupoFavorecido: function(callback) {
            // Preenche combo de grupo
            $.estabelecimento.detalhe.preencherGrupo(function() {
                // Preenche combo de favorecido
                $.estabelecimento.detalhe.preencherFavorecido(function() {
                    // Chama o callback
                    if (callback)
                        callback();
                });
            });
        },

        // --------------------------------------------------------------------
        // Preenche o combo de grupos
        // --------------------------------------------------------------------
        preencherGrupo: function(callback) {

            executarServico(
                "ListarEmpresaGrupoRequest",
                {},
                function(data) {

                    var combo = $("#cmbGrupo");
                    // Limpo o combo (exceto o primeiro item)
                    combo.children().each(
                          function(index) {
                              if (index != 0)
                                  $(this).remove();
                          }
                    );

                    // Para cada item, adiciono no combo
                    for (var i = 0; i < data.Resultado.length; i++) {
                        // Se tem descricao utiliza, se nao, usa o mnemonico
                        var descricao = data.Resultado[i].NomeEmpresaGrupo;
                        // Adiciona o item no bomo
                        combo.append("<option value='" + data.Resultado[i].CodigoEmpresaGrupo + "'>"
                                         + descricao + "</option>");
                    }

                    // Chama callback
                    if (callback)
                        callback();
                }
            );
        },

        // --------------------------------------------------------------------
        // Preenche o combo de favorecidos
        // --------------------------------------------------------------------
        preencherFavorecido: function(callback) {

            executarServico(
                "ListarFavorecidoRequest",
                { FiltroAtivo: true },
                function(data) {

                    var combo = $("#cmbFavorecido");
                    // Limpo o combo (exceto o primeiro item)
                    combo.children().each(
                          function(index) {
                              if (index != 0)
                                  $(this).remove();
                          }
                    );

                    // Para cada item, adiciono no combo
                    for (var i = 0; i < data.Resultado.length; i++) {
                        // Se tem descricao utiliza, se nao, usa o mnemonico
                        var descricao = data.Resultado[i].NomeFavorecido;
                        // Adiciona o item no bomo
                        combo.append("<option value='" + data.Resultado[i].CodigoFavorecido + "'>"
                                         + descricao + "</option>");
                    }

                    // Chama callback
                    if (callback)
                        callback();
                }
            );
        },

        // --------------------------------------------------------------------------------
        // carregarSubGrupos: Carrega combo de subgrupos de acordo com o grupo selecionado
        // --------------------------------------------------------------------------------
        carregarSubGrupos: function(codigoEmpresaGrupo, callback) {

            // Limpa o combo (exceto o primeiro item)
            $("#cmbSubgrupo").children().each(
                function(index) {
                    if (index != 0)
                        $(this).remove();
                }
            );

            // Pede lista de subgrupos caso haja um grupo
            if (codigoEmpresaGrupo) {
                executarServico(
                    "ListarEmpresaSubGrupoRequest",
                    { FiltroCodigoEmpresaGrupo: codigoEmpresaGrupo },
                    function(data) {

                        // Varre lista de subgrupos
                        for (var i = 0; i < data.Resultado.length; i++) {
                            // Adiciona o item no combo
                            $("#cmbSubgrupo").append("<option value='" + data.Resultado[i].CodigoEmpresaSubGrupo + "'>"
				                    + data.Resultado[i].NomeEmpresaSubGrupo + "</option>");
                        }

                        // chama callback
                        if (callback)
                            callback();
                    }
                );

            } else {
                // chama callback
                if (callback)
                    callback();
            }
        },

        // ----------------------------------------------------------------------
        // abrir: funcoes executadas na abertura da tela
        // ----------------------------------------------------------------------
        abrir: function(codigoEstabelecimento, callback) {

            if (!codigoEstabelecimento)
                $("#txtCNPJ").attr("disabled", false);
            else
                $("#txtCNPJ").attr("disabled", true);
                
            // Garante que o html está carregado
            $.estabelecimento.detalhe.load(function() {

                if (codigoEstabelecimento) {
                    // Limpa campos do detalhe do estabelecimento
                    $("#site_configuracao_estabelecimento_detalhe").Forms("clearData");

                    // Carrega dados do estabelecimento existente
                    $.estabelecimento.detalhe.carregar(codigoEstabelecimento, function() {
                        // Abre dialog de detalhe
                        $("#site_configuracao_estabelecimento_detalhe").dialog("open");
                    });

                } else {
                    // Desabilito o modo de edição
                    $.estabelecimento.detalhe.variaveis.estabelecimentoInfo = null;

                    // Limpa campos do detalhe do estabelecimento
                    $("#site_configuracao_estabelecimento_detalhe").Forms("clearData");

                    // Abre dialog de detalhe
                    $("#site_configuracao_estabelecimento_detalhe").dialog("open");
                }
            });
        },

        // ----------------------------------------------------------------------
        // carregar: Carrega dados referentes ao estabelecimento
        // ----------------------------------------------------------------------
        carregar: function(codigoEstabelecimento, callback) {

            // Busco o estabelecimento
            executarServico(
                "ReceberEstabelecimentoRequest",
                { CodigoEstabelecimento: codigoEstabelecimento },
                function(data) {

                    // Habilito o modo de edição
                    $.estabelecimento.detalhe.variaveis.estabelecimentoInfo = data.EstabelecimentoInfo;

                    // Carrega os subgrupos
                    $.estabelecimento.detalhe.carregarSubGrupos(data.EstabelecimentoInfo.CodigoEmpresaGrupo, function() {

                        // Preenche os campos do detalhe
                        $("#site_configuracao_estabelecimento_detalhe").Forms("setData", { dataObject: data.EstabelecimentoInfo });

                        // Chama callback
                        if (callback)
                            callback();
                    });
                }
            );
        },

        // ----------------------------------------------------------------------
        // salvar: Trata inserção ou alteração de um Estabelecimento
        // ----------------------------------------------------------------------
        salvar: function(callback) {

            // Pede validação necessária
            $.estabelecimento.detalhe.validar(function() {

                // Cria novo objeto
                var estabelecimentoInfo = {};

                // Envia informações ao novo objeto
                $("#site_configuracao_estabelecimento_detalhe").Forms("getData", { dataObject: estabelecimentoInfo });

                executarServico(
                    "ListarEstabelecimentoRequest",
                    { FiltroCodigoCnpj: estabelecimentoInfo.CNPJ },
                    function(data) {

                        if (data && data.Resultado[0]) {

                            // Se não estiver em modo de edição...
                            if (!$.estabelecimento.detalhe.variaveis.estabelecimentoInfo
                                     || $.estabelecimento.detalhe.variaveis.estabelecimentoInfo.CodigoEstabelecimento != data.Resultado[0].CodigoEstabelecimento) {
                                popup({
                                    titulo: "Atenção",
                                    icone: "atencao",
                                    mensagem: "CNPJ já existe! Deseja alterar o estabelecimento?",
                                    callbackSim: function() {

                                        // Atribui os valores no objeto já existente
                                        estabelecimentoInfo = data.Resultado[0];
                                        $("#site_configuracao_estabelecimento_detalhe").Forms("getData", { dataObject: estabelecimentoInfo });

                                        executarServico(
                                            "SalvarEstabelecimentoRequest",
                                            { EstabelecimentoInfo: estabelecimentoInfo },
                                            function(data) {
                                                // Chama callback
                                                if (callback)
                                                    callback();
                                            }
                                        );
                                    },
                                    callbackNao: function() { }
                                });

                            } else {
                                // Atribui os valores no objeto já existente
                                estabelecimentoInfo = $.estabelecimento.detalhe.variaveis.estabelecimentoInfo;
                                $("#site_configuracao_estabelecimento_detalhe").Forms("getData", { dataObject: estabelecimentoInfo });

                                executarServico(
                                    "SalvarEstabelecimentoRequest",
                                    { EstabelecimentoInfo: estabelecimentoInfo },
                                    function(data) {
                                        // Chama callback
                                        if (callback)
                                            callback();
                                    }
                                );
                            }

                        } else {
                            // Salva o novo estabelecimento
                            executarServico(
                                "SalvarEstabelecimentoRequest",
                                { EstabelecimentoInfo: estabelecimentoInfo },
                                function(data) {
                                    // Chama callback
                                    if (callback)
                                        callback();
                                }
                            );
                        }
                    }
                );
            });
        },

        // ----------------------------------------------------------------------
        // remover: Trata remoção de um estabelecimento
        // ----------------------------------------------------------------------
        remover: function(codigoEstabelecimento, callback) {

            if (codigoEstabelecimento) {

                // Salva objeto com o item removido
                executarServico(
                    "RemoverEstabelecimentoRequest",
                    { CodigoEstabelecimento: codigoEstabelecimento },
                    function(ret) {

                        // Chama o callback
                        if (callback)
                            callback(ret);
                    });
            }
            else {
                // Pede para selecionar uma linha
                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Favor selecionar uma linha!",
                    callbackOK: function() { }
                });
            }
        },

        // ----------------------------------------------------------------------
        // validar: Faz validações
        // ----------------------------------------------------------------------
        validar: function(callbackOk, callbackErro) {

            // Inicializa criticas
            var criticas = new Array();

            // Pede a validação para o forms
            criticas = $("#site_configuracao_estabelecimento_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });

            // Se tem criticas, mostra o formulario
            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                callbackErro();

            } else {
                callbackOk();
            }
        }
    };

    // Pede a inicializacao
    $.estabelecimento.detalhe.load();
});