﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.processo)
        $.processo = {};

    // Funcoes do namespace processo.lista
    $.processo.lista = {

        // --------------------------------------------------------------------
        //  criar: criar novo processo
        // --------------------------------------------------------------------
        criar: function() {

            // Mostra o dialog de novo processo
            $("#site_processo_lista_novo").Forms("clearData");
            $("#site_processo_lista_novo").dialog("open");

        },

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function(chamarListar) {

            // Insere o subtítulo
            $("#subtitulo").html("Consulta > Lista de Processos");

            // Layout dos botões
            $("#site_processo_lista button").button();

            // Inicializa forms
            $("#site_processo_lista").Forms("initializeFields");
            //1463 evitando de sobrepor o filtro data inicial -- antes estava no 1065
            var date = new Date();
            date.setDate(date.getDate() - 1);
            $('#txtFiltroDataInicial').datepicker("setDate", date);

            // Botão filtrar
            $("#site_processo_lista .cmdFiltrar").click(function() {
                $.processo.lista.listar();
            });

            // Botão editar
            $("#site_processo_lista .cmdEditarProcesso").click(function() {

                // Pega o id do Processo
                var idProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");

                if (idProcesso) {
                    idProcesso = idProcesso.substring(idProcesso.lastIndexOf("_") + 1);

                    // Pede o detalhe
                    $.processo.detalhe.abrir(idProcesso);

                } else {
                    // Mostra popup de erro
                    popup({
                        titulo: "Mostrar Detalhe",
                        icone: "erro",
                        mensagem: "Selecione a linha que deseja ver o detalhe.",
                        callbackOK: function() {
                        }
                    });
                }
            });

            // Botão novo
            $("#site_processo_lista .cmdNovo").click(function() {
                $.processo.lista.criar();
            });

            // Botão log
            $("#site_processo_lista .cmdLog").click(function() {
                var codigoOrigem = $("#tbProcesso").jqGrid("getGridParam", "selrow");
                $.log.lista.abrir({ FiltroTipoOrigem: "ProcessoInfo", FiltroCodigoOrigem: codigoOrigem });
            });

            /*SCF-1518*/
            // Botão GerarInterchange
            $("#site_processo_lista .cmdGerarInterchange").click(function() {
            $.processo.lista.gerarInterchange();
            });


            // Botão Bloqueios
            $("#site_processo_lista .cmdBloqueios").click(function() {
            var codigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");

                if (codigoProcesso) {
            codigoProcesso = codigoProcesso.substring(codigoProcesso.lastIndexOf("_") + 1);
            $.bloqueio.lista.abrir(codigoProcesso);
            }

            });

            /*            // Automatizar
            $("#site_processo_lista .cmdLog").click(function() {
            var codigoOrigem = $("#tbProcesso").jqGrid("getGridParam", "selrow");
            $.log.lista.iniciarRobo({ FiltroTipoOrigem: "ProcessoInfo", FiltroCodigoOrigem: codigoOrigem });
            });
            */
            // Botão cancelar processo

            var cmdCancelarProcesso = function() {

                var codigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
                if (codigoProcesso) {
                    // pede confirmação de cancelamento
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Tem certeza que deseja cancelar este processo?",
                        callbackSim: function() {

                            executarServico(
                                "CancelarProcessoRequest",
                                { CodigoProcesso: codigoProcesso },
                                function(data) {

                                    // Mostra popup de sucesso
                                    popup({
                                        titulo: "Processo Cancelado",
                                        icone: "sucesso",
                                        mensagem: "Processo " + data.CodigoProcesso + " cancelado com sucesso.",
                                        callbackOK: function() { }
                                    });
                                });

                        },
                        callbackNao: function() { }
                    });
                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }

            };

            // Botão continuar processo
            $("#site_processo_lista .cmdContinuarProcesso").click(function() {

                var codigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
                if (codigoProcesso) {
                    // pede confirmação para continuar
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Tem certeza que deseja continuar este processo?",
                        callbackSim: function() {

                            executarServico(
                                "ExecutarProcessoRequest",
                                { CodigoProcesso: codigoProcesso, ExecutarAssincrono: true, ValidaProcessoEmAndamento: true, Descricao: "Processo Continuado" },
                                function(data) { //o campo Descricao foi criado para atender o Jira 968 - Marcos Matsuoka

                                    if (data.StatusProcesso == "Finalizado") {

                                        popup({
                                            titulo: "Processo Finalizado",
                                            icone: "atencao",
                                            mensagem: "Processo já está finalizado.",
                                            callbackOK: function() { }
                                        });
                                    } else if (data.StatusEstagio != "AguardandoValidacao" && data.StatusEstagio != "Erro") {
                                        popup({
                                            titulo: "Processo Em Andamento",
                                            icone: "atencao",
                                            mensagem: "Processo já está em andamento.",
                                            callbackOK: function() { }
                                        });
                                    } else {
                                        // Mostra popup de sucesso
                                        popup({
                                            titulo: "Processo Continuado",
                                            icone: "sucesso",
                                            mensagem: "Processo " + data.CodigoProcesso + " continuado com sucesso.",
                                            callbackOK: function() { }
                                        });
                                    }

                                });

                        },
                        callbackNao: function() { }
                    });
                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // Botão liberar processo
            $("#site_processo_lista .cmdLiberarProcesso").click(function() {

                var codigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
                if (codigoProcesso) {
                    // pede confirmação para continuar
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Tem certeza que deseja continuar este processo?",
                        callbackSim: function() {

                            executarServico(
                                "ExecutarProcessoRequest",
                                { CodigoProcesso: codigoProcesso, ExecutarAssincrono: true, ValidaProcessoEmAndamento: false,
                                    Descricao: "Processo Liberado"
                                }, //o campo Descricao foi criado para atender o Jira  968 - Marcos Matsuoka
                                function(data) {

                                    // Mostra popup de sucesso
                                    popup({
                                        titulo: "Processo Continuado",
                                        icone: "sucesso",
                                        mensagem: "Processo " + data.CodigoProcesso + " continuado com sucesso.",
                                        callbackOK: function() { }
                                    });
                                });

                        },
                        callbackNao: function() { }
                    });
                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // Botão reprocessar agenda
            $("#site_processo_lista .cmdReprocessarAgenda").click(function() {
                var codigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
                if (codigoProcesso) {
                    // pede confirmação para continuar
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Tem certeza que deseja reprocessar a agenda?",
                        callbackSim: function() {

                            executarServico(
                                "AgendarPagamentoExtratoRequest",
                                { CodigoProcesso: codigoProcesso },
                                function(data) {

                                    // Mostra popup de sucesso
                                    popup({
                                        titulo: "Agenda Reprocessada",
                                        icone: "sucesso",
                                        mensagem: "Reprocessamento da agenda realizado com sucesso.",
                                        callbackOK: function() { }
                                    });
                                });
                        },
                        callbackNao: function() { }
                    });
                }
            });

            // Botão reprocessar agenda
            var cmdEstornarExtrato = function() {
                var codigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
                if (codigoProcesso) {
                    // pede confirmação para continuar
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Tem certeza que deseja estornar o extrato?",
                        callbackSim: function() {

                            executarServico(
                                "EstornarLoteExtratoRequest",
                                { CodigoProcesso: codigoProcesso },
                                function(data) {

                                    // Mostra popup de sucesso
                                    popup({
                                        titulo: "Estorno Processado",
                                        icone: "sucesso",
                                        mensagem: "Estorno de extrato realizado com sucesso.",
                                        callbackOK: function() { }
                                    });
                                });
                        },
                        callbackNao: function() { }
                    });
                }
            };

            $("#cmdCancelar").click(function() {
                if ($("#cmdCancelar.cmdCancelarProcesso").length > 0) {
                    cmdCancelarProcesso();
                } else if ($("#cmdCancelar.cmdEstornarExtrato")) {
                    cmdEstornarExtrato();
                }
                else {
                    popup({
                        titulo: "Lista de Processsos",
                        icone: "erro",
                        mensagem: "Selecione uma linha.",
                        callbackOK: function() {
                        }
                    });
                }
            });


            // Inicializa Lista de Lotes
            $("#tbProcesso").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                colNames: ["Processo", "Tipo", "Dt. Início", "Nome Arquivo", "Estágio", "Status Estágio", "Status Processo", "Status Bloqueio", "EnviadoMatera"],
                colModel: [
                              { name: "CodigoProcesso", index: "CodigoProcesso", width: 55, sorttype: "integer", align: "center", key: true },
                              { name: "TipoProcesso", index: "TipoProcesso", width: 50, align: "left" },
                              { name: "DataInclusao", index: "DataInclusao", width: 50, align: "center" },
                              { name: "NomeArquivo", index: "NomeArquivo", width: 100, align: "left", key: true },
                              { name: "TipoEstagioAtual", index: "TipoEstagioAtual", width: 100, align: "left" },
                              { name: "StatusEstagio", index: "StatusEstagio", width: 50, align: "left" },
                              { name: "StatusProcesso", index: "StatusProcesso", width: 50, align: "left" },
                              { name: "StatusBloqueio", index: "StatusBloqueio", width: 50, align: "left" },
                              { name: "EnviadoMatera", index: "EnviadoMatera", hidden: true }
                            ],
                sortorder: "asc",
                viewrecords: true,
                ondblClickRow: function(rowid, status) {
                    return false;
                },
                onSelectRow: function(id) {
                    var objProcesso = $("#tbProcesso").jqGrid('getRowData', id);

                    // habilita o botão continuar processo
                    $("#site_processo_lista #cmdContinuarProcesso").attr("class", "cmdCancelarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_processo_lista #cmdContinuarProcesso").removeAttr("disabled");

                    // Preenche o resultado
                    /*if (objProcesso.StatusProcesso == "Finalizado" && (objProcesso.TipoProcesso == "ProcessoExtratoInfo" || objProcesso.TipoProcesso == "ProcessoCessaoInfo")) {
                    if (objProcesso.EnviadoMatera == true) {
                    $("#site_processo_lista #cmdCancelar").html("<span class='ui-button-text'>Estornar</span>");
                    $("#site_processo_lista #cmdCancelar").attr("class", "cmdEstornarExtrato ui-button ui-widget ui-state-default ui-corner-all ui-button-disabled ui-state-disabled ui-button-text-only");
                    $("#site_processo_lista #cmdCancelar").attr("disabled", "disabled");
                    } else {
                    $("#site_processo_lista #cmdCancelar").removeAttr("disabled");
                    $("#site_processo_lista #cmdCancelar").html("<span class='ui-button-text'>Estornar</span>");
                    $("#site_processo_lista #cmdCancelar").attr("class", "cmdEstornarExtrato ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    }
                    } else */
                    if (objProcesso.StatusProcesso == "Estornado") {
                        // habilita o botao cancelar
                        $("#site_processo_lista #cmdCancelar").removeAttr("disabled");
                        $("#site_processo_lista #cmdCancelar").html("<span class='ui-button-text'>Cancelar</span>");
                        $("#site_processo_lista #cmdCancelar").attr("class", "cmdCancelarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");

                        // desabilita o botao continuar processo
                        $("#site_processo_lista #cmdContinuarProcesso").attr("class", "cmdContinuarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-disabled ui-state-disabled ui-button-text-only");
                        $("#site_processo_lista #cmdContinuarProcesso").attr("disabled", "disabled");
                    } else if ((objProcesso.TipoProcesso == "ProcessoAtacadaoInfo" && objProcesso.StatusEstagio != "Erro") || (objProcesso.TipoProcesso == "ProcessoExtratoInfo" && objProcesso.StatusEstagio != "AguardandoValidacao" && objProcesso.StatusEstagio != "Erro") || objProcesso.StatusProcesso == "Cancelado") {
                        $("#site_processo_lista #cmdCancelar").html("<span class='ui-button-text'>Cancelar</span>");
                        $("#site_processo_lista #cmdCancelar").attr("class", "cmdCancelarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-disabled ui-state-disabled ui-button-text-only");
                        $("#site_processo_lista #cmdCancelar").attr("disabled", "disabled");
                    } else if ((objProcesso.TipoProcesso == "ProcessoExtratoInfo" && objProcesso.StatusEstagio == "AguardandoValidacao" && objProcesso.StatusEstagio == "Erro") || objProcesso.StatusEstagio == "Erro") {
                        $("#site_processo_lista #cmdCancelar").removeAttr("disabled");
                        $("#site_processo_lista #cmdCancelar").html("<span class='ui-button-text'>Cancelar</span>");
                        $("#site_processo_lista #cmdCancelar").attr("class", "cmdCancelarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    } else if (objProcesso.TipoProcesso == "ProcessoRelatorioConciliacaoContabilExcelInfo" || objProcesso.TipoProcesso == "ProcessoRelatorioConsolidadoExcelInfo" || objProcesso.TipoProcesso == "ProcessoRelatorioTransacaoExcelInfo" || objProcesso.TipoProcesso == "ProcessoRelatorioTransacaoInconsistenteExcelInfo" || objProcesso.TipoProcesso == "ProcessoRelatorioVencimentosExcelInfo" || objProcesso.TipoProcesso == "ProcessoRelatorioVendasExcelInfo") {
                        $("#site_processo_lista #cmdCancelar").html("<span class='ui-button-text'>Cancelar</span>");
                        $("#site_processo_lista #cmdCancelar").attr("class", "cmdCancelarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-disabled ui-state-disabled ui-button-text-only");
                        $("#site_processo_lista #cmdCancelar").attr("disabled", "disabled");
                    } else if (objProcesso.StatusProcesso == "Estornado") {
                        $("#site_processo_lista #cmdCancelar").html("<span class='ui-button-text'>Cancelar</span>");
                        $("#site_processo_lista #cmdCancelar").attr("class", "cmdCancelarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-disabled ui-state-disabled ui-button-text-only");
                        $("#site_processo_lista #cmdCancelar").attr("disabled", "disabled");

                        $("#site_processo_lista #cmdContinuarProcesso").attr("class", "cmdContinuarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-disabled ui-state-disabled ui-button-text-only");
                        $("#site_processo_lista #cmdContinuarProcesso").attr("disabled", "disabled");
                    } else {
                        $("#site_processo_lista #cmdCancelar").removeAttr("disabled");
                        $("#site_processo_lista #cmdCancelar").html("<span class='ui-button-text'>Cancelar</span>");
                        $("#site_processo_lista #cmdCancelar").attr("class", "cmdCancelarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    }
                }

            });

            // Inicializa o dialog de novo processo
            $("#site_processo_lista_novo").dialog({
                autoOpen: false,
                height: 200,
                width: 300,
                modal: true,
                buttons: {
                    "Criar": function() {
                        $(this).dialog("close");
                        $.processo.detalhe.abrir();
                        return false;
                    },
                    "Cancelar": function() {
                        $(this).dialog("close");
                        return false;
                    }
                },
                title: "Criar Novo Processo"
            });

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbProcesso").setGridWidth($(window).width() - margemEsquerda);
                $("#tbProcesso").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");

            //SCF1138 - Marcos Matsuoka
            if (chamarListar) {
                chamarListar();
            }

        },

        // --------------------------------------------------------------------
        //  listar: pede a lista de processos
        // --------------------------------------------------------------------
        listar: function() {

            // Limpa a lista
            $("#tbProcesso").jqGrid("clearGridData");

            // Monta o filtro
            var request = {};
            $("#site_processo_lista").Forms("getData", { dataObject: request });

            // Pede a lista de processos
            executarServico(
                "ListarProcessoRequest",
                request,
                function(data) {

                    // Preenche o resultado
                    for (var i = 0; i < data.Resultado.length; i++)
                        $("#tbProcesso").jqGrid(
                            "addRowData",
                            data.Resultado[i].CodigoProcesso,
                            data.Resultado[i]);
                });

        }
    };

    //SCF1138 - Marcos Matsuoka
    function chamarListar() {
        $.processo.lista.listar();
    };

    // Dispara a carga
    //SCF1138 - Marcos Matsuoka (incluindo o callback 'chamarlListar')
    $.processo.lista.load(chamarListar);

});