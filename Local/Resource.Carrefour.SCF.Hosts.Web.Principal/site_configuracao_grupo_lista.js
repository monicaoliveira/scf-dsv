﻿$(document).ready(function() {

    // Namespace
    if (!$.grupo) 
        $.grupo = {};
    
    // Funcoes do namespace 
    $.grupo.lista = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Grupos");
            
            // Estilo dos botões
            $("#site_configuracao_grupo_lista button").button();

            // Inicializa forms
            $("#site_configuracao_grupo_lista").Forms("initializeFields");
            
            // Inicializa grid de lista 
            $("#tbEmpresaGrupo").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 320,
                colNames: ["", "Nome", "Descrição", "Enviar Negativo"],
                colModel: [
                    { name: "CodigoEmpresaGrupo", index: "CodigoEmpresaGrupo",sorttype:"integer", hidden: true, key: true},
                    { name: "NomeEmpresaGrupo", index: "NomeEmpresaGrupo", sorttype: "text" },
                    { name: "DescricaoEmpresaGrupo", index: "DescricaoEmpresaGrupo", sorttype: "text" },
                    { name: "EnviarNegativoEmpresaGrupo", index: "EnviarNegativoEmpresaGrupo" }
                ],
                ondblClickRow: function(rowid, iRow, iCol, e) {

                    // Pega o código do Grupo
                    var codigoEmpresaGrupo = $("#tbEmpresaGrupo").getGridParam("selrow");

                    if (codigoEmpresaGrupo) {

                        // Abre dialog de detalhe do Grupo
                        $.grupo.detalhe.abrir(codigoEmpresaGrupo);
                    }

                    else {

                        // Pede para selecionar a linha
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Favor selecionar uma linha!",
                            callbackOK: function() { }
                        });
                    }
                }
            });

            // BOTÃO NOVO GRUPO
            $("#cmdNovoEmpresaGrupo").click(function() {

                // Abre dialog de detalhe do Grupo
                $.grupo.detalhe.abrir();

            });

            // BOTÃO EDITAR GRUPO
            $("#cmdEditarEmpresaGrupo").click(function() {

                // Pega o código do Grupo
                var codigoEmpresaGrupo = $("#tbEmpresaGrupo").getGridParam("selrow");

                if (codigoEmpresaGrupo) {

                    // Abre dialog de detalhe do Grupo
                    $.grupo.detalhe.abrir(codigoEmpresaGrupo);
                }
                else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // BOTÃO REMOVER GRUPO
            $("#cmdRemoverEmpresaGrupo").click(function() {

                // Pega o código do Grupo
                var codigoEmpresaGrupo = $("#tbEmpresaGrupo").getGridParam("selrow");

                if (codigoEmpresaGrupo) {

                    // pede confirmação de exclusão
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Tem certeza que deseja remover este grupo?",
                        callbackSim: function() {

                            // Pede para remover
                            $.grupo.detalhe.remover(codigoEmpresaGrupo, function() {

                                // Mostra popup de sucesso
                                popup({
                                    titulo: "Remover Grupo",
                                    icone: "sucesso",
                                    mensagem: "Grupo removido com sucesso!",
                                    callbackOK: function() { }
                                });
                            });
                        },
                        callbackNao: function() { }
                    });
                }
                else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }

            });

            // BOTÃO ATUALIZAR LISTA
            $("#cmdAtualizarListaEmpresaGrupo").click(function() {

                // Atualiza lista de grupos
                $.grupo.lista.listarGrupo();
            });

            // PEDE LISTA DE GRUPOS
            $.grupo.lista.listarGrupo();
            
            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {     
                $("#tbEmpresaGrupo").setGridWidth($(window).width() - margemEsquerda); 
                $("#tbEmpresaGrupo").setGridHeight($(window).height() - margemRodape); 
            }).trigger("resize"); 
        },
        
        //------------------------------------------------------------------------
        // listarGrupo: Pede listas de Grupos ao serviço
        //------------------------------------------------------------------------
        listarGrupo: function(callback) {

            // Cria request
        var request = {};
        
        request.MaxLinhas = 100;

            // Pede lista de grupos
            executarServico(
            "ListarEmpresaGrupoRequest",
            request,
            function(data) {

                // Limpa grid
                $("#tbEmpresaGrupo").clearGridData();

                for (var i = 0; i < data.Resultado.length; i++)
                    $("#tbEmpresaGrupo").jqGrid("addRowData", data.Resultado[i].CodigoEmpresaGrupo, data.Resultado[i]);

                // Chama callback
                if (callback)
                    callback();
            });
        }
    };

    // Pede a inicializacao
    $.grupo.lista.load();
});