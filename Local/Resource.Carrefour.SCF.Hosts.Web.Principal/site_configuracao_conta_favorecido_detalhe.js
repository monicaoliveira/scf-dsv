﻿$(document).ready(function() {

    //==============================================================================================================
    // Namespace
    //==============================================================================================================
    if (!$.contaFavorecido)
        $.contaFavorecido = {};
    //==============================================================================================================
    // Funcoes do namespace
    //==============================================================================================================
    $.contaFavorecido.detalhe = {
        variaveis: {},
        //==============================================================================================================
        // load: funcoes de inicializacao da tela
        //==============================================================================================================
        load: function(callback) {
            //==============================================================================================================
            // Verifica se o html esta carregado
            //==============================================================================================================
            if ($("#site_configuracao_conta_favorecido_detalhe").length == 0) {
                //==============================================================================================================
                // Pede o html e insere na área temporaria
                //==============================================================================================================
                $("#areaTemporaria").load("site_configuracao_conta_favorecido_detalhe.aspx #site_configuracao_conta_favorecido_detalhe", function() {
                    $("#site_configuracao_conta_favorecido_detalhe").appendTo("#areaComum");
                    $.contaFavorecido.detalhe.load2(function() {
                        if (callback)
                            callback();
                    });
                });
            }
            else {
                if (callback)
                    callback();
            }
        },
        //==============================================================================================================
        //  load2: funcoes de inicializacao da tela
        //==============================================================================================================
        load2: function() {
            $("#site_configuracao_conta_favorecido_detalhe").Forms("initializeFields");
            $("#site_configuracao_conta_favorecido_detalhe button").button();
            $("#site_configuracao_conta_favorecido_detalhe").dialog(
			{ autoOpen: false,
			    height: 245, //1384 // SCF1701 - ID 27
			    width: 330,
			    modal: true,
			    title: "Detalhe da conta",
			    buttons:
				{
				    "Salvar": function() {
				        $.contaFavorecido.detalhe.salvar(function() {
				            popup
							({ titulo: "Salvar Conta",
							    icone: "sucesso",
							    mensagem: "Conta salva com sucesso!",
							    callbackOK: function() {	//$.favorecido.detalhe.listarContas2(); //1380/1381
							        $("#site_configuracao_conta_favorecido_detalhe").dialog("close");
							        if ($("#chkAssociar").attr("checked") == true)
							        // SCF1701 - ID 27
							            $.favorecido.detalhe.Fechar();
							        else
							            $.favorecido.detalhe.listarContas();
							    }
							});
				        });
				    },
				    "Fechar": function() {
				        $("#site_configuracao_conta_favorecido_detalhe").dialog("close");
				    }
				}
			});
            //Melhroias 2017
            $("#chkAssociar").click(function() {
                $.contaFavorecido.detalhe.validaCheckbox()
            });
            $("#lblAssociar").click(function() {
                var chk = !$("#chkAssociar").attr("checked");
                $("#chkAssociar").attr("checked", chk);
                $.contaFavorecido.detalhe.validaCheckbox()
            });
        },
        validaCheckbox: function(){
            if ($("#chkAssociar").attr("checked") == true)
                $("#cmbConta").show();
            else
                $("#cmbConta").hide();
        },
        //==============================================================================================================
        // abrir: funcoes executadas na abertura da tela
        //==============================================================================================================
        abrir: function(codigoContaFavorecido, callback) {
            //Melhroias 2017
            $("#cmbConta").hide();
            $("#chkAssociar").attr("checked", false);
            executarServico("ListarContaFavorecidoRequest",
                { FiltroCodigoFavorecido: $.favorecido.detalhe.variaveis.codigoFavorecido
                },
                function(data) {
                    $.contaFavorecido.detalhe.variaveis.listaContas = data;
                    if (data.Resultado.length == 0 || $.favorecido.detalhe.variaveis.novoFavorecido == true) {
                        $("#associar").hide();
                    }
                    else {
                        $("#associar").show();
                        // Limpa o combo (exceto o primeiro item)
                        $("#cmbConta").children().each(
                            function(index) {
                                if (index != 0)
                                    $(this).remove();
                            }
                        );

                        // Varre lista de subgrupos
                        for (var i = 0; i < data.Resultado.length; i++) {
                            if (codigoContaFavorecido != data.Resultado[i].CodigoContaFavorecido) {
                                var opt = '<option value="' + data.Resultado[i].CodigoContaFavorecido + '">' + data.Resultado[i].CodigoContaFavorecido
                                + ' - ' + data.Resultado[i].Banco + '-' + data.Resultado[i].Agencia_CCI + '/' + data.Resultado[i].Conta_CCI
                                + '</option>';
                                $("#cmbConta").append(opt);
                            }
                        }
                    }
                });

            $.contaFavorecido.detalhe.load(function() {
                if (codigoContaFavorecido) {
                    $.contaFavorecido.detalhe.variaveis.codigoContaFavorecido = codigoContaFavorecido;
                    $("#site_configuracao_conta_favorecido_detalhe").Forms("clearData");
                    $.contaFavorecido.detalhe.carregar(codigoContaFavorecido, function() {
                        $("#site_configuracao_conta_favorecido_detalhe").dialog("open");
                    });
                }
                else {
                    $.contaFavorecido.detalhe.variaveis.codigoContaFavorecido = null;
                    $("#site_configuracao_conta_favorecido_detalhe").Forms("clearData");
                    $("#site_configuracao_conta_favorecido_detalhe").dialog("open");
                }
            });
        },
        //==============================================================================================================
        // carregar: Carrega dados referentes ao estabelecimento
        //==============================================================================================================
        carregar: function(codigoContaFavorecido, callback) {
            executarServico("ReceberContaFavorecidoRequest", { CodigoContaFavorecido: codigoContaFavorecido }, function(data) {
                $.contaFavorecido.detalhe.variaveis.contaFavorecidoInfo = data.ContaFavorecidoInfo;
                $("#site_configuracao_conta_favorecido_detalhe").Forms("setData", { dataObject: data.ContaFavorecidoInfo });
                if (callback)
                    callback();
            });
        },
        //==============================================================================================================
        // salvar: Trata inserção ou alteração de um Estabelecimento
        //==============================================================================================================
        salvar: function(callback) {
            $.contaFavorecido.detalhe.validar(function() {
                var contaFavorecidoInfo = null;
                contaFavorecidoInfo = {};
                var salva = false;
                $("#site_configuracao_conta_favorecido_detalhe").Forms("getData", { dataObject: contaFavorecidoInfo });
                if (contaFavorecidoInfo.Agencia == "" ||
						contaFavorecidoInfo.Agencia_CCI == "" ||
						contaFavorecidoInfo.Banco == "" ||
						contaFavorecidoInfo.Conta == "" ||
						contaFavorecidoInfo.Conta_CCI == ""
					) {
                    popup({
                        titulo: "SALVAR",
                        icone: "erro",
                        mensagem: "SALVAR CONTA: Favor preencher todos os campos.",
                        callbackOK: function() { }
                    });
                }
                else {
                    if ($("#chkAssociar").attr("checked") == true && $("#cmbConta").find('option:selected').val() == "?") {
                        popup({
                            titulo: "SALVAR",
                            icone: "erro",
                            mensagem: "SALVAR CONTA: Favor selecionar uma conta para realizar a substituição de associações e transações.",
                            callbackOK: function() { }
                        });
                    }
                    else {
                        data = $.contaFavorecido.detalhe.variaveis.listaContas;
                        var contRepetidas = 0;
                        if (data.Resultado.length > 0) {
                            for (var i = 0; i < data.Resultado.length; i++) {
                                if (data.Resultado[i].CodigoContaFavorecido == $.contaFavorecido.detalhe.variaveis.codigoContaFavorecido)
                                    continue;
                                if (data.Resultado[i].Banco == contaFavorecidoInfo.Banco) {
                                    if (data.Resultado[i].Agencia_CCI == contaFavorecidoInfo.Agencia_CCI &&
                                    data.Resultado[i].Conta_CCI == contaFavorecidoInfo.Conta_CCI) {
                                        contRepetidas++;
                                    }
                                }
                            }
                        }
                        //==============================================================================================================
                        if (contRepetidas == 0) {
                            salva = true;
                        }
                        else {
                            popup
						    ({
						        titulo: "Atenção",
						        icone: "erro",
						        mensagem: "ASSOCIAR: Uma agência e conta (DE) não pode ser cadastrada para duas contas (PARA).",
						        callbackOK: function() { return false; }
						    });
                            return false;
                        }
                        //==============================================================================================================
                        if (salva) {
                            if ($.contaFavorecido.detalhe.variaveis.codigoContaFavorecido) {
                                contaFavorecidoInfo.CodigoContaFavorecido = $.contaFavorecido.detalhe.variaveis.contaFavorecidoInfo.CodigoContaFavorecido;
                                contaFavorecidoInfo.CodigoFavorecido = $.favorecido.detalhe.variaveis.codigoFavorecido;
                            }
                            else {
                                contaFavorecidoInfo.CodigoFavorecido = $.favorecido.detalhe.variaveis.codigoFavorecido;
                            }
                            executarServico("SalvarContaFavorecidoRequest", { ContaFavorecidoInfo: contaFavorecidoInfo }, function(data) {
                                if (data.DescricaoResposta) {
                                    popup({
                                        titulo: "Erro",
                                        icone: "erro",
                                        mensagem: "Dados já cadastrados",
                                        callbackOK: function() { }
                                    });
                                }
                                else {
                                    //SCF1701 - ID 27
                                    contaFavorecidoInfo = data.ContaFavorecidoInfo;
                                    CodigoContaFavorecidoAnterior = $("#cmbConta").find('option:selected').val();
                                    if ($("#chkAssociar").attr("checked") == true) {
                                        executarServico("SubstituirContaFavorecidoRequest", { CodigoFavorecido: contaFavorecidoInfo.CodigoFavorecido, CodigoContaFavorecidoNovo: contaFavorecidoInfo.CodigoContaFavorecido, CodigoContaFavorecido: CodigoContaFavorecidoAnterior },
                                            function(data) {
                                                popup({
                                                    titulo: "Salvar Conta",
                                                    icone: "sucesso",
                                                    mensagem: "Processo número " + data.CodigoProcesso + " criado para a Substituição das Contas.",
                                                    callbackOK: function() {
                                                        if (callback)
                                                            callback();
                                                    }
                                                });
                                            });
                                    }
                                }
                                if (callback)
                                    callback();
                            });
                        }
                        //==============================================================================================================
                    }
                }
            }, function() { return true; })
        },
        //==============================================================================================================
        // remover: Trata remoção de uma conta
        //==============================================================================================================
        remover: function(codigoContaFavorecido, callback) {
            if (codigoContaFavorecido) {
                executarServico("RemoverContaFavorecidoRequest", { CodigoContaFavorecido: codigoContaFavorecido }, function(data) {
                    if (callback)
                        callback();
                });
            }
            else {
                popup({
                    titulo: "Atenção",
                    icone: "erro",
                    mensagem: "Favor selecionar uma linha!",
                    callbackOK: function() { }
                });
            }
        },
        //==============================================================================================================
        validar: function(callbackOk, callbackErro) {
            //==============================================================================================================
            var criticas = new Array();
            criticas = $("#site_configuracao_conta_favorecido_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });
            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                callbackErro();

            } else
                callbackOk();
        }
    };
    //==============================================================================================================
    $.contaFavorecido.detalhe.load();
});