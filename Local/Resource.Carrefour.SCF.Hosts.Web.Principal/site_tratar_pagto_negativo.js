﻿$(document).ready(function() {

    // Namespace 
    $.tratarnegativo = {};
    
    // Funcoes do namespace 
    $.tratarnegativo.lista = {

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Processos > Tratar Negativos");

            // Estilo dos botões
            $("#site_tratar_pagto_negativo button").button();

            // Inicializa forms
            $("#site_tratar_pagto_negativo").Forms("initializeFields");
            
            // Inicializa lista pagtos negativos
            $("#tbTratarPagtoNegativo").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 320,
                colNames: ["Número Pagamento", "Data Pagamento", "Favorecido", "Banco", "Agencia", "Conta", "Valor", "Data Liquidacao", ""],
                colModel: [
                    { name: "CodigoPagamento", index: 0, hidden: true, editable: false, width: 25 },
                    { name: "DataPagamento", index: 2, editable: false, classes: "grid[property[DataPagamento];datatype[date]]", width: 25 },
                    { name: "NomeFavorecido", index: 3, editable: false, width: 25 },
                    { name: "Banco", index: 4, editable: false, width: 25 },
                    { name: "Agencia", index: 5, editable: false, width: 25 },
                    { name: "Conta", index: 6, editable: false, width: 25 },
                    { name: "ValorPagamento", index: 7, editable: false, width: 25 },
                    { name: "DataLiquidacaoManual", index: 8, editable: true, editoptions: { dataInit: function(e) { $(e).datepicker(); } }, width: 25 }, 
                    { name: "Acao", index: 9, align: "center", editable: false, width: 25 }
                ],
                sortname: "Codigo"
                
            });

            // Inicializa edicao do grid
            $.grid.inicializar({
            grid: $("#tbTratarPagtoNegativo"),
                modeloLinha: { Ativo: true },
                colunaId: "Codigo",
                callbackEditar: $.tratarnegativo.lista.salvar,
                callbackRestaurar: $.tratarnegativo.lista.restaurar,
                permiteApenasAlterar: true           
            });

            // Pede lista de pagtos nao uteis
            $.tratarnegativo.lista.listarPagtoNegativo(true);
            $("#site_tratar_pagto_negativo").find("select").css("width", "100%");

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbTratarPagtoNegativo").setGridWidth($(window).width() - margemEsquerda);
                $("#tbTratarPagtoNegativo").setGridHeight($(window).height() - margemRodape); 
            }).trigger("resize"); 

        },
        
        // --------------------------------------------------------------------
        //  listarPagtoNegativo: lista os pagamentos negativos
        // --------------------------------------------------------------------
        listarPagtoNegativo: function(listaAtivos) {

            $("#tbTratarPagtoNegativo").clearGridData();

            // Executa Serviço
            executarServico(
                "ListarPagamentoRequest",
                { ListarPagamentoNegativo: "S" },
                function(data) {
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var tratarNegativo = data.Resultado[i];
                        $("#tbTratarPagtoNegativo").jqGrid("addRowData", "tbTratarPagtoNegativo_" + tratarNegativo.CodigoPagamento, tratarNegativo);
                        $.grid.esconderBotoes("tbTratarPagtoNegativo_" + tratarNegativo.CodigoPagamento, !listaAtivos);
                    }
            });
        },
        
        // --------------------------------------------------------------------
        //  salvar: atualizar o pagamento
        // --------------------------------------------------------------------
        salvar: function(param) {
            
            // Cria request
            var request = {};

            // Pede validação necessária
            $.tratarnegativo.lista.validar($("#tbTratarPagtoNegativo"), param, function() {


                // apresentar mensagem
                // Mensagem de sucesso
                popup({
                    titulo: "Liquidar Pagamento Negativo",
                    icone: "ok",
                    mensagem: "Confirma que o pagamento deve ser liquidado? ",
                    callbackSim: function() {
                   
                            // Envia parametros para serviço
                            request.codigoPagamento = param.valores.CodigoPagamento;
                            request.dataLiquidacao = param.valores.DataLiquidacaoManual;

                            executarServico(
                                        "LiquidarPagamentoNegativoRequest",
                                        request,
                                        function(data) {
                                            param.callbackOK({

                                        });
                                    });
                            },
                    callbackNao: function() { }
                    });
                        
                
            },
            function() {
            });
        },
        
        // --------------------------------------------------------------------
        //  validar: valida campos necessários do cadastro 
        // --------------------------------------------------------------------
        validar: function(lGrid, lParam, CallbackOK, CallbackErro) {
            
            // Cria criticas
            var criticas = new Array();
            
            // Realiza as validações
            criticas = $.grid.validate({ pGrid: lGrid, param: lParam, returnErrors: true, errors: criticas });

            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                CallbackErro();
            }
            else
                CallbackOK();
        }
    };

    // Pede a inicializacao
    $.tratarnegativo.lista.load();

});