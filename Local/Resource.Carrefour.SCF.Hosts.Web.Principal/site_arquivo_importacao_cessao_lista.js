﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.arquivo) $.arquivo = {};
    $.arquivo.importacaoCessao = {};
    
    // Funcoes do namespace processo.lista
    $.arquivo.importacaoCessao.lista = {
        
        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {
        
            // Insere o subtítulo
            $("#subtitulo").html("Histórico de Importação de Arquivos de Cessão");
            
            // Layout dos botões
            $("#site_arquivo_importacao_cessao_lista button").button();
        
            // Inicializa Lista 
            $("#tbArquivoImportacao").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                colNames: ["Código do Lote", "Data Importação", "Nome Arquivo", "Tamanho", "Tempo Importação", "Processo", "Status"],
                colModel: [
                    { name: "CodigoLote", index: 0, width: 55, align: "center" },
                    { name: "DataImportacao", index: 1, width: 50, align: "center" },
                    { name: "NomeArquivo", index: 2, width: 50, align: "left" },
                    { name: "Tamanho", index: 3, width: 50, align: "center" },
                    { name: "TempoImportacao", index: 3, width: 50, align: "center" },
                    { name: "Processo", index: 3, width: 50, align: "left" },
                    { name: "Status", index: 3, width: 50, align: "center" }
                ],
                height: 150,
                width: 1000,
                sortorder: "asc",
                viewrecords: true,
                ondblClickRow: function(rowid, status) {
                    return false;
                }
            });
        },
        
        // --------------------------------------------------------------------
        //  carregarDadosProtipo: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        carregarDadosProtipo: function() {
        
            // Cria linhas de exemplo para a lista
            $("#tbArquivoImportacao").jqGrid("addRowData", 165, {
                CodigoLote: "165",
                DataImportacao: "05/05/2011 04:44:12",
                NomeArquivo: "CCI_20110505.txt",
                Tamanho: "34 Mb",
                TempoImportacao: "3m 23s",
                Processo: "Importação Lote CCI",
                Status: "Importado"
            });
            $("#tbArquivoImportacao").jqGrid("addRowData", 166, {
                CodigoLote: "166",
                DataImportacao: "06/05/2011 04:44:12",
                NomeArquivo: "CCI_20110506.txt",
                Tamanho: "33,2 Mb",
                TempoImportacao: "5m 11s",
                Processo: "Importação Lote CCI",
                Status: "Cancelado"
            });
            $("#tbArquivoImportacao").jqGrid("addRowData", 167, {
                CodigoLote: "167",
                DataImportacao: "07/05/2011 06:34:53",
                NomeArquivo: "CCI_20110507.txt",
                Tamanho: "31,2 Mb",
                TempoImportacao: "4m 57s",
                Processo: "Importação Lote CCI",
                Status: "Arquivo Inválido"
            });
        
        }
        
    };

    // Dispara a carga
    $.arquivo.importacaoCessao.lista.load();

    // Carrega informacoes para o prototipo
    $.arquivo.importacaoCessao.lista.carregarDadosProtipo();

});