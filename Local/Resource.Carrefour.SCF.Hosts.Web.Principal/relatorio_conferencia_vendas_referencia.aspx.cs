﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using System.Text;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class relatorio_conferencia_vendas_referencia : System.Web.UI.Page
    {
        ListarRelatorioConferenciaVendasResponse resposta = new ListarRelatorioConferenciaVendasResponse();
        ItemAgrupamentoData item = new ItemAgrupamentoData();
        List<RelatorioConferenciaVendasInfo> listaInteira = new List<RelatorioConferenciaVendasInfo>();

        public bool alterarDataProcessamento = false;

        public Decimal TotalQuantidadeVendas { get; set; }
        public Decimal TotalBrutoVendas { get; set; }
        public Decimal TotalComissao { get; set; }
        public Decimal TotalVendas { get; set; }
        public Decimal TotalCedido { get; set; }
        public Decimal TotalCediveisNaoNegociado { get; set; }
        public Decimal TotalNaoCediveis { get; set; }
        public Decimal TotalPendente { get; set; }
        public Decimal ExibeReferencia { get; set; }
        public Decimal ExibeData { get; set; }
        public Decimal QuebraData { get; set; }

        class ItemAgrupamentoData : TotalGeral
        {
            public List<RelatorioConferenciaVendasInfo> Vendas { get; set; }
        }

        class ItemAgrupamentoReferenciaData : TotalGeral
        {
            public string Referencia { get; set; }
        }

        class TotalGeral
        {
            public string ReferenciaAgrupado { get; set; } //ClockWork
            public string DataVenda { get; set; }
            public string TotalQuantidadeVendas { get; set; }
            public string TotalBrutoVendas { get; set; }
            public string TotalComissao { get; set; }
            public string TotalVendas { get; set; }
            public string TotalCedido { get; set; }
            public string TotalCediveisNaoNegociado { get; set; }
            public string TotalNaoCediveis { get; set; }
            public string TotalPendente { get; set; }            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Lembrar:
            //DataVendaRepeater.ItemDataBound += new RepeaterItemEventHandler(DataVendaRepeater_ItemDataBound);
            ReferenciaRepeater.ItemDataBound += new RepeaterItemEventHandler(ReferenciaRepeater_ItemDataBound);

            resposta =
                    Mensageria.Processar<ListarRelatorioConferenciaVendasResponse>(
                        new ListarRelatorioConferenciaVendasRequest()
                        {
                            //CodigoSessao = (string)this.Session["CodigoSessao"],
                            FiltroEmpresaGrupo = this.Request["CodigoEmpresaGrupo"] != "?" ? this.Request["CodigoEmpresaGrupo"] : null,
                            FiltroEmpresaSubgrupo = this.Request["CodigoEmpresaSubgrupo"] != "?" ?  this.Request["CodigoEmpresaSubgrupo"] : null,
                            FiltroEstabelecimento = this.Request["CodigoEstabelecimento"] != "?" ? this.Request["CodigoEstabelecimento"] : null,
                            FiltroFavorecido = this.Request["CodigoFavorecido"] != "?" ? this.Request["CodigoFavorecido"] : null,
                            FiltroReferencia = this.Request["CodigoReferencia"] != "?" ? this.Request["CodigoReferencia"] : null, //Clock Work
                            FiltroDataInicioVendas = this.Request["DataInicioVenda"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioVenda"]) : null,
                            FiltroDataFimVendas =this.Request["DataFimVenda"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimVenda"]) : null,
                            FiltroDataMovimentoInicio = this.Request["DataInicioMovimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioMovimento"]) : null,
                            FiltroDataMovimentoFim =this.Request["DataFimMovimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimMovimento"]) : null,
                            // JIRA SCF-836
                            FiltroConsolidaGrupoSubGrupoProduto = Convert.ToBoolean(this.Request["ConsolidaGrupoSubGrupoProduto"]),
                            FiltroConsolidaDataVenda = Convert.ToBoolean(this.Request["ConsolidaDataVenda"]),
                            FiltroConsolidaDataProcessamento = Convert.ToBoolean(this.Request["ConsolidaDataProcessamento"]),
                            FiltroConsolidaBancoAgenciaConta = Convert.ToBoolean(this.Request["ConsolidaBancoAgenciaConta"]),
                            FiltroConsolidaEstabelecimento = Convert.ToBoolean(this.Request["ConsolidaEstabelecimento"]),
                            FiltroConsolidaReferencia = Convert.ToBoolean(this.Request["ConsolidaReferencia"]),//ClockWork
                            FiltrotxtDataRetornoCCIInicio = this.Request["DataInicioRetornoCCI"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioRetornoCCI"]) : null,
                            FiltrotxtDataRetornoCCIFim = this.Request["DataFimRetornoCCI"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimRetornoCCI"]) : null,
                            //ECOMMERCE - Fernando Bove - 20160105
                            FiltroFinanceiroContabil = this.Request["TipoFinanceiroContabil"] != null ? this.Request["TipoFinanceiroContabil"] : null,
                            FiltroTipoRegistro = this.Request["CodigoTipoRegistro"] != null ? this.Request["CodigoTipoRegistro"].ToString() : null,
                            TipoArquivo = this.Request["TipoArquivo"] != null ? this.Request["TipoArquivo"].ToString() : null //Marcos Matsuoka - Jira SCF 1085
                        });

            //SCF1109 - Marcos Matsuoka - Garantir o preenchimento do filtro - inicio
            this.lblFiltroGrupo.Text = this.Request["Grupo"] != null ? this.Request["Grupo"] : "Todos";
            this.lblFiltroSubgrupo.Text = this.Request["Subgrupo"] != null ? this.Request["Subgrupo"] : "Todos";
            this.lblFiltroFavorecido.Text = this.Request["Favorecido"] != null ? this.Request["Favorecido"] : "Todos";
            this.lblFiltroEstabelecimento.Text = this.Request["Estabelecimento"] != null ? this.Request["Estabelecimento"] : "Todos";
            this.lblFiltroReferencia.Text = this.Request["Referencia"] != null ? this.Request["Referencia"] : "Todos"; //ClockWork
            this.lblFiltroDataInicio.Text = this.Request["DataInicioVenda"] != null ? this.Request["DataInicioVenda"] : "Qualquer";
            this.lblFiltroDataFim.Text = this.Request["DataFimVenda"] != null ? this.Request["DataFimVenda"] : "Qualquer";
            this.lblFiltroDataMovimentoInicio.Text = this.Request["DataInicioMovimento"] != null ? this.Request["DataInicioMovimento"] : "Qualquer";
            this.lblFiltroDataMovimentoFim.Text = this.Request["DataFimMovimento"] != null ? this.Request["DataFimMovimento"] : "Qualquer";
            this.lblFiltroRetornoCCIInicio.Text = this.Request["DataInicioRetornoCCI"] != null ? this.Request["DataInicioRetornoCCI"] : "Qualquer";
            this.lblFiltroRetornoCCIFim.Text = this.Request["DataFimRetornoCCI"] != null ? this.Request["DataFimRetornoCCI"] : "Qualquer";
            this.lblFiltroTipoRegistro.Text = this.Request["CodigoTipoRegistro"].ToString();
            this.lblDataHora.Text = String.Format("{0:dd/MM/yyyy - HH:mm}", DateTime.Now);

            // ECOMMERCE - Fernando Bove - 20160106
            this.lblFiltroFinanceiroContabil.Text = this.Request["TipoFinanceiroContabil"] != null ? this.Request["TipoFinanceiroContabil"] == "C" ? "Contábil" : "Financeiro" : "Todos";


            // consolidacao
            StringBuilder sb = new StringBuilder();
            sb.Append("Grupo + SubGrupo + Produto");

            //Marcos Matsuoka - Jira SCF 1046
            //if (Convert.ToBoolean(this.Request["ConsolidaDataVenda"]) == true)
            //    sb.Append(" + Data da Venda");
            if (Convert.ToBoolean(this.Request["ConsolidaDataVenda"]) == true)
            {
                sb.Append(" + Data da Venda");
                this.lblTipoConsolidado.Text = (" por venda");
            };

            //Marcos Matsuoka - Jira SCF 1046
            //if (Convert.ToBoolean(this.Request["ConsolidaDataProcessamento"]) == true)
            //    sb.Append(" + Data de Processamento");
            if (Convert.ToBoolean(this.Request["ConsolidaDataProcessamento"]) == true)
            {
                sb.Append(" + Data de Processamento");
                this.lblTipoConsolidado.Text = (" por processamento");
            };


            if (Convert.ToBoolean(this.Request["ConsolidaBancoAgenciaConta"]) == true)
                sb.Append(" + Banco + Agência + C/C");
            if (Convert.ToBoolean(this.Request["ConsolidaEstabelecimento"]) == true)
                sb.Append(" + Estabelecimento");
            if (Convert.ToBoolean(this.Request["ConsolidaReferencia"]) == true) //ClockWork
                sb.Append(" + Referência");


            this.lblFiltroConsolidacao.Text = sb.ToString();

            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;

            if (resposta.Resultado.Count == 0)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Sem dados para imprimir pelo filtro informado.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Sem dados para imprimir pelo filtro informado.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
            else if (resposta.Resultado.Count > 1000 && (this.Request["TipoArquivo"] != "PDF")) //Marcos Matsuoka - Jira SCF 1085
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
            //SCF1109 - Marcos Matsuoka - Garantir o preenchimento do filtro - fim


            // Lucas - Jira SCF-976
            if (Convert.ToBoolean(this.Request["ConsolidaDataProcessamento"]))
            {
                this.alterarDataProcessamento = true;
            }

            // Agrupa por REFERENCIA
            var itensPorReferencia =
                from y in resposta.Resultado
                orderby y.DataVenda, y.Estabelecimento.CodigoEstabelecimento, y.CodigoTransacao
                group y by new { y.Referencia.Valor, y.Referencia.Descricao  } into g
                select new ItemAgrupamentoReferenciaData()
                {
                    TotalVendas = g.Sum(p => p.ValorLiquido).ToString("N2"),
                    TotalNaoCediveis = g.Sum(p => p.TotalNaoCedido).ToString("N2"),
                    TotalCedido = g.Sum(p => p.TotalCedido).ToString("N2"),
                    TotalCediveisNaoNegociado = g.Sum(p => p.TotalCedivelNaoNegociado).ToString("N2"),
                    Referencia = g.Key.Valor != null ? g.Key.Valor.ToString() : string.Empty,
                    TotalPendente = (g.Sum(p => p.TotalPendente)).ToString("N2"),
                    TotalBrutoVendas = (g.Sum(p => p.TotalBruto)).ToString("N2"),
                    TotalComissao = (g.Sum(p => p.TotalComissao)).ToString("N2"),
                    TotalQuantidadeVendas = (g.Sum(p => p.Quantidade)).ToString("N0"),
                    ReferenciaAgrupado = g.Key.Valor != null ? g.Key.Valor.ToString() + " - " + g.Key.Descricao : string.Empty
                };
            this.ExibeReferencia = itensPorReferencia.Count();

            ReferenciaRepeater.DataSource = itensPorReferencia;
            ReferenciaRepeater.DataBind();

            //foreach (RelatorioConferenciaVendasInfo venda in listaInteira)
            //{
            //    TotalVendas += Convert.ToDecimal(venda.ValorLiquido);
            //    TotalCedido += Convert.ToDecimal(venda.TotalCedido);
            //    TotalNaoCediveis += Convert.ToDecimal(venda.TotalNaoCedido);
            //    TotalPendente += Convert.ToDecimal(venda.TotalPendente);
            //    TotalCediveisNaoNegociado += Convert.ToDecimal(venda.TotalCedivelNaoNegociado);
            //    TotalBrutoVendas += Convert.ToDecimal(venda.TotalBruto);
            //    TotalComissao += Convert.ToDecimal(venda.TotalComissao);
            //    TotalQuantidadeVendas += Convert.ToDecimal(venda.Quantidade);
            //}

            //// Seto os totais gerais
            //this.lblTotalCedido.Text = TotalCedido.ToString("N2");
            //this.lblTotalCedidoNaoNegociado.Text = TotalCediveisNaoNegociado.ToString("N2");
            //this.lblTotalNaoCedido.Text = TotalNaoCediveis.ToString("N2");
            //this.lblTotalPendente.Text = TotalPendente.ToString("N2");
            //this.lblTotalVendas.Text = TotalVendas.ToString("N2");
            //this.lblTotalBrutoVendas.Text = TotalBrutoVendas.ToString("N2");
            //this.lblTotalComissao.Text = TotalComissao.ToString("N2");
            //this.lblTotalQuantidadeVendas.Text = TotalQuantidadeVendas.ToString("N0");
        }

        void ReferenciaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lbl = e.Item.FindControl("lblTotalCedido") as Label;
                if (lbl != null)
                    lbl.Text = TotalCedido.ToString("N2");

                lbl = e.Item.FindControl("lblTotalCedidoNaoNegociado") as Label;
                if (lbl != null)
                    lbl.Text = TotalCediveisNaoNegociado.ToString("N2");

                lbl = e.Item.FindControl("lblTotalNaoCedido") as Label;
                if (lbl != null)
                    lbl.Text = TotalNaoCediveis.ToString("N2");

                lbl = e.Item.FindControl("lblTotalPendente") as Label;
                if (lbl != null)
                    lbl.Text = TotalPendente.ToString("N2");

                lbl = e.Item.FindControl("lblTotalVendas") as Label;
                if (lbl != null)
                    lbl.Text = TotalVendas.ToString("N2");

                lbl = e.Item.FindControl("lblTotalBrutoVendas") as Label;
                if (lbl != null)
                    lbl.Text = TotalBrutoVendas.ToString("N2");

                lbl = e.Item.FindControl("lblTotalComissao") as Label;
                if (lbl != null)
                    lbl.Text = TotalComissao.ToString("N2");

                lbl = e.Item.FindControl("lblTotalQuantidadeVendas") as Label;
                if (lbl != null)
                    lbl.Text = TotalQuantidadeVendas.ToString("N0");
            }
            else
            {
                ItemAgrupamentoReferenciaData dados = ((ItemAgrupamentoReferenciaData)(e.Item.DataItem));
                {
                    TotalBrutoVendas += Convert.ToDecimal(dados.TotalBrutoVendas);
                    TotalVendas += Convert.ToDecimal(dados.TotalVendas);
                    TotalCedido += Convert.ToDecimal(dados.TotalCedido);
                    TotalNaoCediveis += Convert.ToDecimal(dados.TotalNaoCediveis);
                    TotalPendente += Convert.ToDecimal(dados.TotalPendente);
                    TotalCediveisNaoNegociado += Convert.ToDecimal(dados.TotalCediveisNaoNegociado);
                    TotalComissao += Convert.ToDecimal(dados.TotalComissao);
                    TotalQuantidadeVendas += Convert.ToDecimal(dados.TotalQuantidadeVendas);
                }

                Label lbl = e.Item.FindControl("lblReferenciaRepeater") as Label;
                if (lbl != null)
                {
                    if (e.Item.ItemIndex > 0)
                        lbl.Style.Add("page-break-before", "always");
                }

                Repeater DataVendaRepeater = (Repeater)e.Item.FindControl("DataVendaRepeater");
                DataVendaRepeater.ItemDataBound += new RepeaterItemEventHandler(DataVendaRepeater_ItemDataBound);

                var itensPorData =
                    from y in resposta.Resultado
                    where y.Referencia.Valor == ((ItemAgrupamentoReferenciaData)e.Item.DataItem).Referencia
                    orderby y.DataVenda, y.Estabelecimento.CodigoEstabelecimento, y.CodigoTransacao
                    group y by new { y.DataVenda } into g
                    select new ItemAgrupamentoData()
                    {
                        TotalVendas = g.Sum(p => p.ValorLiquido).ToString("N2"),
                        TotalNaoCediveis = g.Sum(p => p.TotalNaoCedido).ToString("N2"),
                        TotalCedido = g.Sum(p => p.TotalCedido).ToString("N2"),
                        TotalCediveisNaoNegociado = g.Sum(p => p.TotalCedivelNaoNegociado).ToString("N2"),
                        DataVenda = g.Key.DataVenda != null ? g.Key.DataVenda.Value.ToString("dd/MM/yyyy") : string.Empty,
                        Vendas = g.Select(p => p).ToList<RelatorioConferenciaVendasInfo>(),
                        TotalPendente = (g.Sum(p => p.TotalPendente)).ToString("N2"),
                        TotalBrutoVendas = (g.Sum(p => p.TotalBruto)).ToString("N2"),
                        TotalComissao = (g.Sum(p => p.TotalComissao)).ToString("N2"),
                        TotalQuantidadeVendas = (g.Sum(p => p.Quantidade)).ToString("N0")
                    };
                this.ExibeData = itensPorData.Count();

                DataVendaRepeater.DataSource = itensPorData;
                DataVendaRepeater.DataBind();

                this.ExibeReferencia -= 1;
                this.QuebraData = 0;
            }
        }

        void DataVendaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (this.alterarDataProcessamento)
            {
                Label lbl = e.Item.FindControl("lblDataVendaRep") as Label;
                if (lbl != null)
                {
                    lbl.Text = "Data Processamento:";
                }
            }

            Repeater EstabelecimentoRepeater = (Repeater)e.Item.FindControl("EstabelecimentoRepeater");
            EstabelecimentoRepeater.ItemDataBound += new RepeaterItemEventHandler(EstabelecimentoRepeater_ItemDataBound);

            //foreach (RelatorioConferenciaVendasInfo venda in ((ItemAgrupamentoData)e.Item.DataItem).Vendas)
            //{
            //    listaInteira.Add(venda);
            //}

            EstabelecimentoRepeater.DataSource = ((ItemAgrupamentoData)e.Item.DataItem).Vendas;
            EstabelecimentoRepeater.DataBind();

            this.ExibeData -= 1;
        }

        void EstabelecimentoRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        /// <summary>
        /// Função utilizada para adicionar o fim de tabela até a penúltima
        /// e retirar na última devido ao Total Geral
        /// </summary>
        /// <returns></returns>
        public string Exibir(int quebra)
        {
            string retorno = "";

            if (quebra == 1)
            {
                this.QuebraData += 1;
                if (this.QuebraData > 1)
                    retorno = "<table style='border: solid 1px #4682B4; width: 100%; repeat-header: yes;'><thead>";
            }
            else if (quebra == 2 && this.ExibeReferencia != 1)
            {
                retorno = "</tbody></table>";
            }
            else if (quebra == 3 && this.ExibeData != 1)
            {
                retorno = "</tbody></table>";
            }

            return retorno;
        }
    }
}
