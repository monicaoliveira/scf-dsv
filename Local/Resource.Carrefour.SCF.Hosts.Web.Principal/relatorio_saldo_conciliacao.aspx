﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorio_saldo_conciliacao.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.relatorio_saldo_conciliacao" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>Relatório de Saldo de Conciliação Contábil</title>
    <link href="relatorio_transacoes.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <form id="form1" runat="server">
        <header><table style="border:solid 1px #4682B4;" width="100%">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td>
                                <table width="100%" class="conteudo">
                                    <tr>
                                        <td class="espacoLeft">
                                            <h1>SALDO CONCILIAÇÃO CONTÁBIL - Conta Corrente</h1> 
                                        </td>
                                        <td>
                                            <label class="negrito pequeno">Data/Hora: </label> <asp:Label class="pequeno" runat="server" ID="lblDataHora"></asp:Label>
                                        </td>                                        
                                    </tr>
                                </table> 
                            </td>                                                                       
                        </tr>                        
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <label class="labelNegrito">Filtro:</label>
                                            <label class="labelNegrito">Período conciliação:</label>                              
                                            <asp:Label class="labelNegrito" runat="server" ID="lblFiltroDataConciliacaoDe"></asp:Label> - 
                                            <asp:Label class="labelNegrito" runat="server" ID="lblFiltroDataConciliacaoAte"></asp:Label>
                                        </td>  
                                    </tr>
                                </table> 
                            </td>                                                                       
                        </tr>                                                                                                                                                                       
                    </table> 
                </td>  
            </tr>
        </table></header>
        <table class="espacoTop conteudo">
            <tr runat="server" id="trMensagem" visible="false">
                <td colspan="10" class="grupo" style="border-top:solid 3px #4682B4;"><asp:Label class="pequeno" runat="server" ID="lblMensagem"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="10" class="espacoLeft bordaBottomFina bordaTopGrossa">
                    <h1>SALDO ANTERIOR&nbsp;&nbsp;&nbsp;<asp:Literal runat="server" ID="litSaldoAnterior"></asp:Literal></h1>
                </td>
            </tr>
            <tr>
                <td  class="espacoColuna">
                    <label class="labelNegrito">CONCILIAÇÃO<br />Status </label> 
                </td>
                <td  class="espacoColuna">
                    <label class="labelNegrito">CONCILIAÇÃO<br />Saldo Acumilado</label> 
                </td>
                <td  class="espacoColuna">
                    <label class="labelNegrito">CONCILIAÇÃO<br />Valor</label> 
                </td>
                <td  class="espacoColuna">
                    <label class="labelNegrito">CONCILIAÇÃO<br />Data</label> 
                </td>
                <td  class="espacoColuna">
                    <label class="labelNegrito">TRANSAÇÃO<br />Data</label> 
                </td>
                <td class="espacoColuna">
                    <label class="labelNegrito">TRANSAÇÃO<br />NSU HOST</label>
                </td>
                <td class="espacoColuna">
                    <label class="labelNegrito">TRANSAÇÃO<br />Código Autorização</label> 
                </td>
                <td  class="espacoColuna">
                    <label class="labelNegrito">TRANSAÇÃO<br />Valor</label> 
                </td>
                <td  class="espacoColuna">
                    <label class="labelNegrito">SITEF<br />/Rede</label> 
                </td>
                <td  class="espacoColuna">
                    <label class="labelNegrito"><br />Processo</label> 
                </td>
            </tr>
            <asp:Repeater runat="server" ID="rptResultado">
                <ItemTemplate>
                    <tr class="<%# Eval("StatusConciliacao").ToString() == "NaoConciliado" ? "labelNegrito" : "" %>">
                        <td>
                            <%# Eval("StatusConciliacao").ToString() %>
                        </td>
                        <td>
                            <%# (Convert.ToDecimal(Eval("SaldoAcumulado")) < 0 ) ? "(" +(Convert.ToDecimal(Eval("SaldoAcumulado")) * -1).ToString("n") + ")" : Convert.ToDecimal(Eval("SaldoAcumulado")).ToString("n") %>
                        </td>
                        <td>
                            <%# (Convert.ToDecimal(Eval("ValorConciliacao")) < 0) ? "(" + (Convert.ToDecimal(Eval("ValorConciliacao")) * -1).ToString("n") + ")" : Convert.ToDecimal(Eval("ValorConciliacao")).ToString("n") %>
                        </td>
                        <td>
                            <%# Eval("DataConciliacao") == null ? "": Convert.ToDateTime(Eval("DataConciliacao")).ToString("dd/MM/yyyy")%>
                        </td>
                        <td>
                            <%# Convert.ToDateTime(Eval("DataTransacao")).ToString("dd/MM/yyyy")%>
                        </td>
                        <td>
                            <%# Eval("NSUHost").ToString()%>
                        </td>
                        <td>
                            <%# Eval("CodigoAutorizacao").ToString()%>
                        </td>
                        <td>
                            <%# Eval("ValorTransacao").ToString()%>
                        </td>
                        <td>
                            <%# Eval("SitefRede").ToString()%>
                        </td>
                        <td>
                            <%# Eval("CodigoProcesso").ToString()%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td colspan="10" class="espacoLeft bordaBottomGrossa bordaTopFina">
                    <h1>SALDO FINAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Literal runat="server" ID="litSaldoFinal"></asp:Literal></h1>
                </td>
            </tr>            
        </table>
    </form>
</body>
</html>
