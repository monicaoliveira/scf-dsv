﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorio_cheques.aspx.cs" 
EnableSessionState="false" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.relatorio_cheques" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Relatório de Cheques Devolvidos</title>
    <link href="relatorio_cheques.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--1465-->
    <form id="form1" runat="server">
    <header>
        <table>
            <tr>
                <td colspan="8">
                    <h1>RELATÓRIO DE CHEQUES DEVOLVIDOS<asp:Label runat="server" ID="lblTitulo"></asp:Label></h1>  
                </td>
                <td colspan="2" align="right">
                    <label class="negrito pequeno">Data/Hora: </label> 
                    <asp:Label class="pequeno" runat="server" ID="lblDataHora"></asp:Label>
                </td>                                                                       
            </tr>                        
            <tr>
                <td colspan="10"><br />
                    <label class="negrito medio">Filtros:</label>
                </td>
            </tr>                        
            <tr>
                <td colspan="2" valign="top">
                    <label class="negrito medio">Tipo de Registro: AP </label> 
                </td>
                <td colspan="2" valign="top">
                    <label class="negrito medio">Meio de Pagamento: 4-Devolucao Cheque </label> 
                </td>
                                
                <td colspan="2" valign="top">
                    <label class="negrito medio">No Protocolo: </label> 
                    <asp:Label runat="server" ID="lblFiltroNumeroProtocolo"></asp:Label>
                </td>
                <td colspan="4" valign="top">
                    <label class="negrito medio">Período Estorno de: </label> 
                    <asp:Label runat="server" ID="lblFiltroDataEstornoDe"></asp:Label>
                    <label class="negrito medio">até: </label> 
                    <asp:Label runat="server" ID="lblFiltroDataEstornoAte"></asp:Label>
                </td>                
            </tr>                        
            <tr>
                <td colspan="2" valign="top">
                    <label class="negrito medio">Conta Cliente: </label> 
                    <asp:Label runat="server" ID="lblFiltroContaCliente"></asp:Label>
                </td>
                <td colspan="2" valign="top">
                    <label class="negrito medio">Tipo Estorno: </label> 
                    <asp:Label runat="server" ID="lblFiltroTipoEstorno"></asp:Label>
                </td>            
                <td colspan="2" valign="top">
                    <label class="negrito medio">Estabelecimento: </label> 
                    <asp:Label runat="server" ID="lblFiltroEstabelecimento"></asp:Label>
                </td>                
                <td colspan="4" valign="top">
                    <label class="negrito medio">Período Processamento de: </label> 
                    <asp:Label runat="server" ID="lblFiltroDataProcessamentoDe"></asp:Label>
                    <label class="negrito medio">até: </label> 
                    <asp:Label runat="server" ID="lblFiltroDataProcessamentoAte"></asp:Label>
                </td>        
            </tr>                        
            <tr>
                <td colspan="2" valign="top">
                    <label class="negrito medio">Status Envio CCI: </label> 
                    <asp:Label runat="server" ID="lblFiltroEnvioCci"></asp:Label>
                </td>                
                <td colspan="4" valign="top">
                    <label class="negrito medio">Status Protocolo: </label> 
                    <asp:Label runat="server" ID="lblFiltroStatusProtocolo"></asp:Label>
                </td>                                
                <td colspan="4" valign="top">
                    <label class="negrito medio">Período Registro Protocolo de: </label> 
                    <asp:Label runat="server" ID="lblFiltroDataRegProtocoloDe"></asp:Label>
                    <label class="negrito medio">até: </label> 
                    <asp:Label runat="server" ID="lblFiltroDataRegProtocoloAte"></asp:Label>
                </td>                             
            </tr>          
            <tr runat="server" id="trmensagem" visible=false>
                <td colspan="10" class="grupo" style="width:100%; border-top:solid 3px #4682b4;">
                    <asp:label class="pequeno" runat="server" id="lblmensagem"></asp:label>
                </td>
            </tr>
            <tr><td colspan="10">&nbsp;</td></tr>            
        </table> 
    </header>
    <asp:Repeater runat="server" ID="ChequesRepeater">
        <HeaderTemplate>
            <table>
                <thead>
                    <tr>
                        <th class="negrito medio centro  fixoValor">Data <br />Processamento</th>
                        <th class="negrito medio centro fixoTexto">Conta <br />Cliente</th>
                        <th class="negrito medio centro">NSUHOST</th>
                        <th class="negrito medio centro">NSUHOST <br /> Original</th>
                        <th class="negrito medio esquerda">Estabelecimento</th>
                        <th class="negrito medio direita fixoValor">Valor <br />Repasse</th>
                        <th class="negrito medio centro  fixoValor">Data <br />Protocolo</th>
                        <th class="negrito medio centro  fixoValor">Data <br />Estorno</th>
                        <th class="negrito medio centro">Numero <br />Protocolo</th>
                        <th class="negrito medio centro">Tipo <br />Estorno</th>
                        <th class="negrito medio centro">PSL</th>
                    </tr>
                    <tr><td colspan="11">&nbsp;</td></tr>
                </thead>
                <tbody>
        </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                            <td class="pequeno  centro"><%# Convert.ToDateTime(Eval("DataProcessamento")).ToString("dd/MM/yyyy")%></td>
                            <td class="pequeno  centro"><%# Eval("ContaCliente")%></td>
                            <td class="pequeno  centro"><%# Eval("NsuHost")%></td>
                            <td class="pequeno  centro"><%# Eval("NsuHostOriginal")%></td>
                            <td class="pequeno  esquerda"><%# Eval("Estabelecimento")%></td>
                            <td class="pequeno  direita"><%# Convert.ToDecimal(Eval("Valor")).ToString("N2")%></td>
                            <td class="pequeno  centro"><%# (Eval("DataRegProtocolo")) != null ? Convert.ToDateTime(Eval("DataRegProtocolo")).ToString("dd/MM/yyyy"): "" %></td>
                            <td class="pequeno  centro"><%# (Eval("DataEstorno")) != null ? Convert.ToDateTime(Eval("DataEstorno")).ToString("dd/MM/yyyy") : "" %></td>
                            <td class="pequeno  centro"><%# Eval("NumeroProtocolo")%></td>
                            <td class="pequeno  centro"><%# Eval("TipoEstorno")%></td>
                            <td class="pequeno  centro"><%# Eval("StatusProtocolo")%></td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                    <tr><td colspan="11">&nbsp;</td></tr>
                            <tr>
                            <td colspan="5" class="medio negrito direita">Total Geral...:</td>
                            <td class="medio negrito direita"><asp:Label runat="server" ID="lblTotalValorCheques"></asp:Label></td>                   
                            <td colspan="2" class="medio negrito direita">Quantidade...:</td>
                            <td class="medio negrito esquerda"><asp:Label runat="server" ID="lblTotalQtdeCheques"></asp:Label></td>
                            </tr>
                </tbody>                            
            </table>                
                    </FooterTemplate>
    </asp:Repeater>
    </form>
</body>
</html>
