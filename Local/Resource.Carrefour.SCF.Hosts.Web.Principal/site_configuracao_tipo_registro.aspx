﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_tipo_registro.js"></script>
    <link href="site_configuracao_tipo_registro.css" rel="stylesheet" type="text/css" />
    <title>SCF - Configurações por Tipo Registro</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--          Lista de tipo de registro -->
    <!-- ------------------------------------ -->
    <div id="site_configuracao_tipo_registro_lista" class="lista">
        <table id="tbConfigTipoRegistro"></table>
        <!-- SCF1333 -->
        <!--<div class="botoes"> -->
            <!--<button id="cmdEnvioCampoCCI">Envio de campo ao CCI</button> -->
        <!--</div> -->
        <div><label>(*) A configuração Dias Repasse só poderá ser feita para o Grupo Atacadão.</label></div>
    </div>
    

</asp:Content>
