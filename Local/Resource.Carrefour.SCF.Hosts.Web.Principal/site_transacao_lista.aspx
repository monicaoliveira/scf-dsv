﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_transacao_lista.css" rel="stylesheet" type="text/css" />
    <script src="site_transacao_lista.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div id="site_transacao_lista">
        
        <!-- Filtro -->
        <div class="filtro formulario">
            <table>
                <tr>
                    <td>
                        <label>Data Inclusão:</label>
                    </td>
                    <td>
                        <label>de&nbsp;</label>
                        <input type="text" class="txtFiltroDataInclusaoMaior" />
                        <label>até&nbsp;</label>
                        <input type="text" class="txtFiltroDataInclusaoMenor" />
                    </td>
                    
                    <td>
                        <label>Retorno de Cessão:</label>
                    </td>
                    <td>
                        <select class="cmbFiltroStatusRetornoCessao">
                            <option value="?">(selecione)</option>
                        </select>
                    </td>
                    <td>
                        <label>
                            <input type="checkbox" class="chkRetornoCessao" checked="checked" />
                            Problemas no Retorno da Cessão
                        </label>
                    </td>
                    <td>
                        <button class="cmdFiltrar">Filtrar</button>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Lista -->
        <div class="lista">
            <table id="tbTransacao"></table>
        </div>

    </div>
    
</asp:Content>
