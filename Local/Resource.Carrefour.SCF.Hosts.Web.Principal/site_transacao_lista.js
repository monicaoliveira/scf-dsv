﻿$(document).ready(function() {

    // Namespace de transacao
    if (!$.transacao)
        $.transacao = {};

    // Funcoes do namespace transacao.lista
    $.transacao.lista = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Consulta > Retorno de Cessão");

            // Layout dos botões
            $("#site_transacao_lista button").button();

            // Inicializa forms
            $("#site_transacao_lista").Forms("initializeFields");
            
            // Campos de data
            $("#site_transacao_lista .txtFiltroDataInclusaoMenor").datepicker();
            $("#site_transacao_lista .txtFiltroDataInclusaoMaior").datepicker();

            // Inicializa Lista de transacaos
            $("#tbTransacao").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                sortable: true,
                scroll: true,
                colNames: ["Código Transacao", "Data Inclusão", "Processo", "Repasse", "Repasse Calculado", "Tipo", "NsuHost", "StatusRetorno"],
                colModel: [
                  { name: "CodigoTransacao", index: 0, sorttype: "integer", key: true, width: 50, align: "center" },
                  { name: "DataInclusao", index: 1, width: 60, align: "center" },
                  { name: "CodigoProcesso", index: 1, width: 60, align: "center" },
                  { name: "DataRepasse", index: 1, width: 60, align: "center" },
                  { name: "DataRepasseCalculada", index: 1, width: 60, align: "center" },
                  { name: "TipoTransacao", index: 2, width: 60, align: "center" },
                  { name: "NsuHost", index: 2, width: 60, align: "center" },
                  { name: "StatusRetornoCessao", index: 3, width: 50, align: "center" }
                ],
                height: 150,
                width: 900,
                sortname: "Codigotransacao",
                sortorder: "asc",
                viewrecords: true
            });

            // Botão filtrar
            $("#site_transacao_lista .cmdFiltrar").click(function() {
                $.transacao.lista.listar();
            });

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbTransacao").setGridWidth($(window).width() - margemEsquerda);
                $("#tbTransacao").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");

            // Carrega listas de enumeradores
            carregarListas("TransacaoStatusRetornoCessaoEnum", function() {
                // Preenche combo de categoria
                preencherCombo($("#site_transacao_lista .cmbFiltroStatusRetornoCessao"), "TransacaoStatusRetornoCessaoEnum");
            });
        },

        // --------------------------------------------------------------------
        //  listar: pede a lista de transacaos
        // --------------------------------------------------------------------
        listar: function() {

            // Limpa a lista
            $("#tbTransacao").jqGrid("clearGridData");

            // Monta o filtro
            var request = { MaxLinhas: 200 };
            
            // Filtro de problemas no retorno da cessao
            var retornoCessao = $("#site_transacao_lista .chkRetornoCessao").attr("checked");
            if (retornoCessao)
                request.FiltroRetornoCessao = true;
            
            // Data de inclusao
            var dataInclusaoMaior = $("#site_transacao_lista .txtFiltroDataInclusaoMaior").val();
            if (dataInclusaoMaior)
                request.FiltroDataInclusaoMaior = dataInclusaoMaior;
            var dataInclusaoMenor = $("#site_transacao_lista .txtFiltroDataInclusaoMenor").val();
            if (dataInclusaoMenor)
                request.FiltroDataInclusaoMenor = dataInclusaoMenor;
                
            // Status de Retorno de Cessao
            var filtroRetornoCessao = $("#site_transacao_lista .cmbFiltroStatusRetornoCessao").val();
            if (filtroRetornoCessao != "?")
                request.FiltroStatusRetornoCessao = filtroRetornoCessao;

            // Pede a lista de transacaos
            executarServico(
                "ListarTransacaoRequest",
                request,
                function(data) {

                    // Preenche o resultado
                    for (var i = 0; i < data.Resultado.length; i++)
                        $("#tbTransacao").jqGrid("addRowData", data.Resultado[i].CodigoTransacao, data.Resultado[i]);
                });
        }

    };

    // Inicializa a tela
    $.transacao.lista.load();
    
});