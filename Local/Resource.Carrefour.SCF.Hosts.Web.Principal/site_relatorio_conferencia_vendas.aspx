﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="site_relatorio_conferencia_vendas.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_relatorio_conferencia_vendas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="site_relatorio_conferencia_vendas.js"></script>
    <link href="site_relatorio_conferencia_vendas.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">    
    <div id="site_relatorio_conferencia_vendas">
        <div id="filtros_conferencia_vendas">        
            <div id="filtroDetalhe">
                <label class="tituloFiltro">Detalhes</label>                
                <div class="borda">
                    <ul>                                           
                        <li >
                            <input type="radio" id="rdoTipoContabil" name="tiporelatorio" class="forms[property[FiltroFinanceiroContabil]]" value="C" />&nbsp;Contábil&nbsp;&nbsp;&nbsp;
                            <input type="radio" id="rdoTipoFinanceiro" name="tiporelatorio" class="forms[property[FiltroFinanceiroContabil]]" value="F" />&nbsp;Financeiro
                        </li>

                        <li class="label_consolidacao">
                            <label>Consolidação</label>
                        </li>
                    </ul>
                    <ul>
                        <li class="label_detalhe1">
                            <label>Grupo:</label>
                        </li>

                        <li class="campo_detalhe1">
                            <select id="cmbGrupo" class="forms[property[FiltroEmpresaGrupo]]">
                                <option value="?">Todos</option>
                                <asp:Repeater runat="server" ID="rptCmbGrupo">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoEmpresaGrupo") %>"><%# Eval("NomeEmpresaGrupo")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </li>    

                        <li class="label_detalhe2">
                            <label>Subgrupo:</label>
                        </li>

                        <li class="campo_detalhe2">
                            <select id="cmbSubgrupo" class="forms[property[FiltroEmpresaSubgrupo]]">
                                <option value="?">Todos</option>
                                <asp:Repeater runat="server" ID="rptCmbSubgrupo">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoEmpresaSubGrupo") %>"><%# Eval("NomeEmpresaSubgrupo")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </li>

                        <li class="label_detalhe3">
                            <label>Tipo de registro:</label>
                        </li>

                        <li class="campo_detalhe3">
                            <select id="cmbTipoRegistro" class="forms[property[FiltroTipoRegistro]]">
                                <option value="CV-AV">CV-AV</option>
                                <option value="CV">CV</option>
                                <option value="AV">AV</option>
                                <option value="AJ">AJ</option> <%--SCF1158--%>
                            </select>
                        </li>
                        <li class="chk_consolidacao">
                            <input type="checkbox" id="chkConsolidacaoGrupoSubGrupoProduto" checked="checked" disabled class="forms[property[FiltroConsolidaGrupoSubGrupoProduto]]" />&nbsp;Grupo + SubGrupo + Produto
                        </li>
                    </ul>
                    <ul>
                        <li class="label_detalhe_estabelecimento">
                            <label>Estabelecimento:</label>
                        </li>

                        <li class="campo_detalhe_estabelecimento">
                            <select id="cmbEstabelecimento"  class="forms[property[FiltroEstabelecimento]]">
                                <option value="?">Todos</option>
                                <asp:Repeater runat="server" ID="rptCmbEstabelecimento">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoEstabelecimento") %>"><%# Eval("RazaoSocial")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </li>
                        
                        <li class="chk_consolidacao1">
                            <input type="checkbox" id="chkConsolidacaoDataVenda" checked="checked" class="forms[property[FiltroConsolidaDataVenda]]" />&nbsp;Data da venda &nbsp; &nbsp; &nbsp; &nbsp;
                            <input type="checkbox" id="chkConsolidacaoDataProcessamento" class="forms[property[FiltroConsolidaDataProcessamento]]" />&nbsp;Data de processamento
                        </li>

                    </ul>
                    <ul>
                        <li class="label_detalhe_estabelecimento">
                            <label>Favorecido:</label>
                        </li>
                        <li class="campo_detalhe_favorecido">
                            <select id="cmbFavorecido" class="forms[property[FiltroFavorecido]]">
                                <option value="?">Todos</option>
                                <asp:Repeater runat="server" ID="rptCmbFavorecido">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoFavorecido") %>"><%# Eval("NomeFavorecido")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </li>
                        <li class="chk_consolidacao2">
                            <input type="checkbox" id="chkConsolidacaoBancoAgenciaConta" class="forms[property[FiltroConsolidaBancoAgenciaConta]]" />&nbsp;Banco + Agência + C/C
                        </li>
                    </ul>
                    <ul>
                         <li class="label_detalhe_referencia">
                            <label>Referência:</label>
                        </li>
                        <li >
                            <select id="cmbReferencia" multiple="multiple" style="width:100px;" class="ui-multiselect ui-state-focus ui-state-active"></select>
                        </li>
                        <li class="chk_consolidacao3">
                            <input type="checkbox" id="chkConsolidacaoEstabelecimento" class="forms[property[FiltroConsolidaEstabelecimento]]" />&nbsp;&nbsp;Estabelecimento&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" id="chkConsolidacaoReferencia" class="forms[property[FiltroConsolidaReferencia]]" />&nbsp;Referência
                        </li>
                    </ul>                    
                    
                </div>
            </div>
            <div class="vertical_line">
                &nbsp;
            </div>     
            <div id="filtroPeriodo">
                <label class="tituloFiltro">Períodos</label>
                
                <div class="borda">
                    <ul>
                        <li class="label_Data">
                            <label>Data da Venda:</label>
                        </li>                        
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>De:</label>
                                </li>
                                <li class="campo_data">
                                    <input type="text" id="txtPeriodoVendaDe" class="forms[property[FiltroDataInicioVendas];dataType[date];maskType[date]]" />
                                </li> 
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>                                
                                <li class="campo_data">
                                    <input type="text" id="txtPeriodoVendaAte" class="forms[property[FiltroDataFimVendas];dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>

                        <li class="label_Data1">
                            <label>Data de Retorno CCI:</label>
                        </li>                        
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>De:</label>
                                </li>
                                <li class="campo_data">
                                    <input type="text" id="txtDataRetornoCCIDe" class="forms[property[FiltroDataInicioRetornoCCI];dataType[date];maskType[date]]" />
                                </li> 
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>                                
                                <li class="campo_data">
                                    <input type="text" id="txtDataRetornoCCIFim" class="forms[property[FiltroDataFimRetornoCCI];dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>
                    </ul>                                                                                                 
                    <ul>
                        <li class="label_Data">
                            <label>Data do Processamento:</label>
                        </li>                        
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>De:</label>
                                </li>
                                <li class="campo_data">
                                    <input type="text" id="txtDataMovimentoDe" class="forms[property[FiltroDataMovimentoInicio];dataType[date];maskType[date]]" />
                                </li> 
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>                                
                                <li class="campo_data">
                                    <input type="text" id="txtDataMovimentoAte" class="forms[property[FiltroDataMovimentoFim];dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>
                    </ul>                                                                                                 
                </div>
            </div>     
            <div class="teste">
                <div style="float:left; text-align:left">
                    <button id="btnVisualizarHtml">Visualizar</button>
                </div>
                <div style=" text-align:right">            
                    <button id="btnGerarRelatorioExcel">Gerar Excel</button>
                    <button id="btnGerarRelatorioPdf">Gerar PDF</button>
                </div>                                
            </div>                             
        </div>        
        <div id="resultado_relatorio"></div>
    </div>
</asp:Content>