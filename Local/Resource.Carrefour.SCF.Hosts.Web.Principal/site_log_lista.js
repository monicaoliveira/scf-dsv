﻿$(document).ready(function() 
{   // ----------------------------------------------------------------------
    if (!$.log)
        $.log = {};
    // ----------------------------------------------------------------------
    $.log.lista = 
    {   variaveis: {}
    ,   vChamada: false    //1469
    // ----------------------------------------------------------------------
    // INICIA TELA CHAMADA PELO MANUTENÇÃO DE PROCESSO
    // ----------------------------------------------------------------------             
    ,   abrir: function(param, callback) 
        {   $.log.lista.load(function() 
            {   $("#site_log_lista").dialog
                (
                    {   autoOpen: false
                    ,   height: 500
                    ,   width: 950
                    ,   modal: true
                    ,   title: "LOG"
                    ,   buttons: 
                        {   "Fechar": function() 
                            {   $("#site_log_lista").dialog("close");
                                //1469$.log.lista.listar({ SetTimeOut: false });
                            }
                        }
                    ,   resize: function() 
                        {
                            $("#tbLog").setGridWidth($("#site_log_lista").width() - 10);
                            $("#tbLog").setGridHeight($("#site_log_lista").height() - 140);
                        }
                    }
                );
                // ----------------------------------------------------------------------
                // Carrega Parametro da tela Manutenção de Processo
                // ----------------------------------------------------------------------
                $.log.lista.paramLista = param;
                $.log.lista.listar({ SetTimeOut: true });
                // ----------------------------------------------------------------------
                // Trata formulario
                // ----------------------------------------------------------------------
                $("#tbLog").clearGridData();
                $("#site_log_lista").Forms("clearData");
                $("#site_log_lista").dialog("open");
                
                $("#painelProcesso .CodigoProcesso").text($.log.lista.paramLista.FiltroCodigoOrigem); //1469
                // ----------------------------------------------------------------------
                // Faz o primeiro resize
                // ----------------------------------------------------------------------
                $("#tbLog").setGridWidth($("#site_log_lista").width() - 10);
                $("#tbLog").setGridHeight($("#site_log_lista").height() - 140);
                // ----------------------------------------------------------------------
                // Se tem, chama o callback
                // ----------------------------------------------------------------------
                if (callback)
                    callback();
            });
        }
        
    // ----------------------------------------------------------------------
    // CONFERE DADOS DA TELA PARA EFETUA A CARGA DO GRID
    // ----------------------------------------------------------------------             
    ,   load: function(callback) 
        {   // ----------------------------------------------------------------------             
            // Verifica se o html esta carregado
            // ----------------------------------------------------------------------             
            if ($("#site_log_lista").length == 0) 
            {   
                // ----------------------------------------------------------------------             
                // Pede o html e insere na área temporaria
                // ----------------------------------------------------------------------             
                $("#areaTemporaria").load("site_log_lista.aspx #site_log_lista", function() 
                {
                    // ----------------------------------------------------------------------             
                    // Copia o elemento para a área comum
                    // ----------------------------------------------------------------------             
                    $("#site_log_lista").appendTo("#areaComum");

                    // ----------------------------------------------------------------------             
                    // Completa o load
                    // ----------------------------------------------------------------------             
                    $.log.lista.load2(function() 
                    {   if (callback)
                            callback();
                    });
                });

            } else 
            {
                if (callback)
                    callback();
            }
        }
    // ----------------------------------------------------------------------
    // EFETUA A CARGA DO GRID
    // ----------------------------------------------------------------------             
    ,   load2: function(callback) 
        {
            // ----------------------------------------------------------------------
            // Inicializa forms
            // ----------------------------------------------------------------------
            $("#site_log_lista").Forms("initializeFields");
            // ----------------------------------------------------------------------
            // Insere o subtítulo
            // ----------------------------------------------------------------------
            //$("#subtitulo").html("Administrativo > Logs"); //1353
            if (!$.log.lista.vChamada) //1469
            {   $("#site_log_lista. #painelProcesso").hide(); //1469
                $("#subtitulo").html("ADMINISTRATIVO [Logs]"); 
            }                
            else
            {
                $("#site_log_lista. #painelLogFiltro").hide(); //1469
            }
            // ----------------------------------------------------------------------
            // Layout dos botões
            // ----------------------------------------------------------------------
            $("#site_log_lista button").button();
            if (window.location.href.indexOf("site_log_lista") <= 0) 
            {
                //$(".cmdLogFilho").hide(); //1469
                //$(".cmdGerarExcel").hide(); //1469
                $(".lblTipoOrigemLog").hide();
                $(".filtroTipoOrigemLog").hide();
            };

// ----------------------------------------------------------------------
// 1469 BOTAO INIBIDO
// ----------------------------------------------------------------------
//            $(".cmdGerarExcel").click(function() {
//                // Pega o id do log
//                var idLog = $("#tbLog").jqGrid("getGridParam", "selrow");
//                var descPai = $("#tbLog").jqGrid("getRowData", idLog);
//                if (idLog) {
//                    request = {};
//                    request.Request = {};
//                    request.Request.FiltroCodigoOrigem = descPai.CodigoLog;
//                    executarServico("GerarRelatorioLogExcelRequest",  request, function(data) 
//                    {
//                        popup({
//                            titulo: "Relatório Excel",
//                            icone: "sucesso",
//                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
//                            callbackOK: function() { }
//                        });
//                    });
//                }
//                else {
//                    // Mostra popup de erro
//                    popup({
//                        titulo: "Gerar Excel",
//                        icone: "erro",
//                        mensagem: "Selecione a linha que deseja gerar o Excel.",
//                        callbackOK: function() {
//                        }
//                    });
//                }
//            });
// ----------------------------------------------------------------------
// 1469 BOTAO INIBIDO
// ----------------------------------------------------------------------
//            // Visualiza o log filho
//            $("#site_log_lista .cmdLogFilho").click(function() {
//                // Pega o id do log
//                var idLog = $("#tbLog").jqGrid("getGridParam", "selrow");
//                var descPai = $("#tbLog").jqGrid("getRowData", idLog);
//                if (idLog) {
//                    //idLog = idLog.substring(idLog.lastIndexOf("_") + 1);
//                    var descricao = "LISTA DE LOG – " + descPai.DataLog + " – " + descPai.Descricao
//                    // Pede o detalhe
//                    $.log.relatorio.abrir({ FiltroCodigoOrigem: descPai.CodigoLog, Descricao: descricao });
//                }
//                else {
//                    // Mostra popup de erro
//                    popup({
//                        titulo: "Mostrar Detalhe",
//                        icone: "erro",
//                        mensagem: "Selecione a linha que deseja ver o detalhe.",
//                        callbackOK: function() {
//                        }
//                    });
//                }
//            });
// ----------------------------------------------------------------------

            // ----------------------------------------------------------------------
            // Trata detalhe do log
            // ----------------------------------------------------------------------
            $("#site_log_lista .cmdDetalhe").click(function() 
            {
                // ----------------------------------------------------------------------
                // Pega o id do log
                // ----------------------------------------------------------------------
                var idLog = $("#tbLog").jqGrid("getGridParam", "selrow");

                if (idLog) 
                {   idLog = idLog.substring(idLog.lastIndexOf("_") + 1);

                    // ----------------------------------------------------------------------
                    // Pede o detalhe
                    // ----------------------------------------------------------------------
                    $.log.detalhe.abrir(idLog);
                }
                else 
                {
                    // ----------------------------------------------------------------------
                    // Mostra popup de erro
                    // ----------------------------------------------------------------------
                    popup(
                    {   titulo: "Mostrar Detalhe"
                    ,   icone: "erro"
                    ,   mensagem: "Selecione a linha que deseja ver o detalhe."
                    ,   callbackOK: function() { }
                    });
                }
            });

            // ----------------------------------------------------------------------
            // Trata botão pesquisar
            // ----------------------------------------------------------------------
            $("#site_log_lista .cmdPesquisar").click(function() 
            {
                $.log.lista.listar({ SetTimeOut: false });
            });

            $("#site_log_lista .cmdAtualizar").click(function() {
                $.log.lista.listar({ SetTimeOut: false });
            });
            // ----------------------------------------------------------------------
            // Carrega combo de filtro de tipos de log
            // ----------------------------------------------------------------------
            $.log.lista.carregarDescricoesLog(function() 
            {   $("#tbLog").jqGrid(
                {
                    datatype: "clientSide"
                ,   mtype: "GET"
                ,   scroll: true
                ,   colNames: ["Data",  "Descrição", "Cd_Processo","Usuário", "Tp_Log", "Tp_Origem", "Cd_Log"]
                ,   colModel: 
                    [
                      { name: "DataLog",        index: "DataLog",       editable: false, width: 120 }
                    , { name: "Descricao",      index: "Descricao",     editable: false, width: 650 }
                    , { name: "CodigoOrigem",   index: "CodigoOrigem",  editable: false, width: 100, hidden: $.log.lista.vChamada }
                    , { name: "CodigoUsuario",  index: "CodigoUsuario", editable: false, width: 80 , hidden: $.log.lista.vChamada }
                    , { name: "TipoLog",        index: "TipoLog",       editable: false, width: 200, hidden: $.log.lista.vChamada }
                    , { name: "TipoOrigem",     index: "TipoOrigem",    editable: false, width: 100, hidden: $.log.lista.vChamada }
                    , { name: "CodigoLog",      index: "CodigoLog",     editable: false, key: true, width: 80, hidden: true }
                    ]
                ,   shrinkToFit: $.log.lista.vChamada
                ,   sortname: "CodigoLog"
                ,   sortorder: "desc"
                ,   viewrecords: true
                });
// ----------------------------------------------------------------------
//1469
//                // Preenche o combo
//                var itens = $("body").data("descricoesLog");
//                for (var i = 0; i < itens.length; i++)
//                    $("#painelLogFiltro .filtroTipoLog").append("<option value='" + itens[i].NomeTipo + "'>" + itens[i].Descricao + "</option>");
// ----------------------------------------------------------------------
                if (callback)
                    callback();
            });
        }
    // ----------------------------------------------------------------------
    // listar: pede a lista de logs de acordo com o filtro
    // ----------------------------------------------------------------------
    ,   listar: function(param, callback) 
        {
            // ----------------------------------------------------------------------
            // Prepara o request
            // ----------------------------------------------------------------------
            var request = {};
            // ----------------------------------------------------------------------
            // 1469 Prepara flags
            // ----------------------------------------------------------------------
            var vDadosOk = true;
            var vTemProcesso = false;
            // ----------------------------------------------------------------------
            // 1469 FILTRO - Chamada da MANUTENÇÃO DE PROCESSO carrega o CODIGO ORIGEM e o TIPO PROCESSO
            // ----------------------------------------------------------------------
            if ($.log.lista.vChamada)
            {
                var filtroCodigoOrigem = $.log.lista.paramLista.FiltroCodigoOrigem;
                var filtroTipoOrigem = $.log.lista.paramLista.FiltroTipoOrigem;
            }
            else
            {
            // ----------------------------------------------------------------------
            // 1469 FILTRO - Digitado na tela de LOG
            // ----------------------------------------------------------------------
            var filtroCodigoUsuario = $("#painelLogFiltro .filtroUsuario").val();
            var filtroTipoLog       = $("#painelLogFiltro .filtroTipoLog").val();
            var filtroTipoOrigem    = $("#painelLogFiltro .filtroTipoOrigemLog").val(); 
            var filtroCodigoOrigem  = $("#painelLogFiltro .filtroCodigoOrigemLog").val();
            var filtroDataInicio    = $("#painelLogFiltro .filtroDataInicio").val();
            var filtroDataFim       = $("#painelLogFiltro .filtroDataFim").val();
            }
            // ----------------------------------------------------------------------
            // 1469 Trata filtro
            // ----------------------------------------------------------------------
            if  (filtroCodigoUsuario && filtroCodigoUsuario != "")
            {
                request.FiltroCodigoUsuario = filtroCodigoUsuario;
            }
            if  (filtroTipoLog && filtroTipoLog != "")
            {
                request.FiltroTipoLog = filtroTipoLog;
            }
            if  (filtroTipoOrigem && filtroTipoOrigem != "")
            {
                request.FiltroTipoOrigem = filtroTipoOrigem;
            }                
            if  (filtroCodigoOrigem && filtroCodigoOrigem != "")
            {
                request.FiltroCodigoOrigem = filtroCodigoOrigem;
                vTemProcesso = true;
            }                
            if  (filtroDataInicio && filtroDataInicio != "")
            {
                request.FiltroDataInicio = filtroDataInicio;
            }                
            else
            {
                if (!vTemProcesso)
                {   vDadosOk = false;
                    popup(
                    {
                            titulo: "FILTRO INVALIDO"
                        ,   icone: "erro"
                        ,   mensagem: "Data Inicial invalida, favor informar"
                        ,   callbackOK: function() { }
                    });                
                }
            }
            if  (filtroDataFim && filtroDataFim != "")
            {
                request.FiltroDataFim = filtroDataFim;
            }                
            else
            {   if (!vTemProcesso)
                {   vDadosOk = false;
                    popup({
                        titulo: "FILTRO INVALIDO",
                        icone: "erro",
                        mensagem: "Data Final invalida, favor informar",
                        callbackOK: function() { }
                    });                
                }
            }
            // ----------------------------------------------------------------------
            // 1469 Chama PR_LOG_L
            // ----------------------------------------------------------------------
            if (vDadosOk)
            {
                executarServico("ListarLogRequest",request,function(data) 
                {
                    $("#tbLog").jqGrid("clearGridData");
                    for (var i = 0; i < data.Resultado.length; i++) 
                    {
                        $("#tbLog").jqGrid("addRowData","tbLog_" + data.Resultado[i].CodigoLog,data.Resultado[i]);
                    }
                });
            }
        }

    // ----------------------------------------------------------------------
    // carregarDescricoesLog: carrega a lista de descricoes de log
    // ----------------------------------------------------------------------
    ,   carregarDescricoesLog: function(callback) 
        {   if (!$("body").data("descricoesLog")) 
            {   executarServico("ListarDescricaoLogRequest",{},function(data) 
                {
                    $("body").data("descricoesLog", data.Resultado);

                    if (callback)
                        callback();
                });
            } else 
            {   if (callback)
                    callback();
            }
        }
    };
    // ----------------------------------------------------------------------        
    if (window.location.href.indexOf("site_log_lista") > 0) 
    {   
        $.log.lista.vChamada = false;//1469
        $.log.lista.load2(function() 
        {
            $(window).bind("resize", function() 
            {
                $("#tbLog").setGridWidth($(window).width() - 60);
                $("#tbLog").setGridHeight($(window).height() - 300);
            }).trigger("resize");
        });
    }
    // ----------------------------------------------------------------------       
    if (window.location.href.indexOf("site_manutencao_processo") > 0 //1469
        || window.location.href.indexOf("site_processo_lista") > 0) //1469      
    {
        $.log.lista.vChamada = true;
    }        
});