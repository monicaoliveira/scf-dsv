﻿<%@ Page Title="Administrativo - Cadastro de Perfis" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="adm_perfil_cadastro.js"></script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    <div id="perfil_lista">
            <div class="lista">
                <table id="tbPerfilLista"></table>
                <div class="botoes">
                    <button id="cmdNovoPerfil">Novo</button>
                    <button id="cmdEditarPerfil">Editar</button>
                    <button id="cmdRemoverPerfil">Remover</button>
                    <button id="cmdAtualizarLista">Atualizar Lista</button>
                </div>
            </div>
    </div>
    <div id="perfil_edicao">
        <ul>
            <li><a href="#perfil_detalhe">Detalhe</a></li>
            <li><a href="#perfil_permissoes">Permissões</a></li>
        </ul>
        <div id="perfil_detalhe">
            <form class="cmxform" action="#">
                <fieldset>
                    <legend>Detalhe do Perfil</legend> <!-- 1477 - Ajuste na tela e inclusao do codigo -->
                    <br/>
                    <li>
                        <label style="text-align: left; width:100px" >PERFIL - Codigo</label>
                        <input type="text" id="codigoPerfil" value="" readonly />
                    </li>
                    <li>
                        <label style="text-align: left; width:100px">PERFIL - Nome</label>
                        <input type="text" id="nomePerfil" value="" />
                    </li>
                </fieldset>                
            </form>
        </div>
        <div id="perfil_permissoes">
            <table id="tbPerfilPermissaoLista"></table>
            <button id="cmdAdicionarPermissao">Adicionar</button>
            <button id="cmdRemoverPermissao">Remover</button>
        </div>
    </div>
    <div id="permissao_lista"> 
        <div class="lista"> <!-- 1477 - INCLUSAO DE BORDA -->
            <table id="tbPermissaoLista"></table>
            <br /> <!-- 1477 - INCLUSAO DE LINHA -->
            <br />
            <label for="cmbTipoPermissao">Tipo da Permissão:</label>
            <select id="cmbTipoPermissao">
                <option value="0">Permitir</option>
                <option value="1">Negar</option>
            </select>
        </div>
    </div>
    <div id="confirmacaoExclusao">
        <span>Confirma a exclusão deste item?</span>
    </div>
    <div id="mensagemAlerta">
        <span id="textoAlerta"></span>
    </div>
</asp:Content>