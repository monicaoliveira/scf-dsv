﻿$(document).ready(function() {

    // Cria namespace 
    if (!$.bloqueio)
        $.bloqueio = {};

    // Funcoes do namespace 
    $.bloqueio.lista = {

        variaveis: {},

        // Abre um dialog com a lista de logs solicitada
        abrir: function(codigoProcesso) 
        {
            // Garante que a tela está carregada
            $.bloqueio.lista.load2(function() 
            {

                // Limpa campos do detalhe do log
                $("#tbBloqueio").clearGridData();
                $("#site_bloqueio_lista").Forms("clearData");

                // Mostra o dialog
                $("#site_bloqueio_lista").dialog("open");

                // Guarda o codigo e busca
                if (codigoProcesso)
                    $("#site_bloqueio_lista #txtFiltroCodigoProcesso").val(codigoProcesso);
                    
                $.bloqueio.lista.listar2(codigoProcesso);

                // Faz o primeiro resize
                $("#tbBloqueio").setGridWidth($("#site_bloqueio_lista").width() - 10);
                $("#tbBloqueio").setGridHeight($("#site_bloqueio_lista").height() - 140);

            });
            $("#txtFiltroCodigoProcesso").focus(); //1308

        },

        // ----------------------------------------------------------------------
        // load: garante que o HTML da esteja carregado e inicializado
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        load2: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_bloqueio_lista").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_bloqueio_lista.aspx #site_bloqueio_lista", function() {

                    // Copia o elemento para a área comum
                    $("#site_bloqueio_lista").appendTo("#areaComum");

                    // Inicializa o dialog
                    $("#site_bloqueio_lista").dialog({
                        autoOpen: false,
                        height: 420,
                        width: 950,
                        modal: true,
                        title: "Lista de Bloqueios",
                        buttons: {
                            "Fechar": function() {
                                // Fecha o dialog
                                $("#site_bloqueio_lista").dialog("close");
                            }
                        },
                        resize: function() {
                            $("#tbBloqueio").setGridWidth($("#site_log_lista").width() - 10);
                            $("#tbBloqueio").setGridHeight($("#site_log_lista").height() - 140);
                        }
                    });

                    // Completa o load
                    $.bloqueio.lista.load(callback);
                });

            } else {
                // Chama o callback
                if (callback)
                    callback();
            }

        },

        // -----------------------------------------------------------------------------
        // load: funcoes de inicializacao da tela
        // -----------------------------------------------------------------------------
        load: function(callback) {
            
            // Carrega listas de enumeradores
            carregarListas("ArquivoItemBloqueioStatusEnum", function() 
            {   preencherCombo($("#cmbFiltroStatusArquivoItem"), "ArquivoItemBloqueioStatusEnum");
            });

            // Insere o subtítulo
            $("#subtitulo").html("CONSULTA [Bloqueio de Processos]"); //SCF1138 - Marcos Matsuoka - corrigir o nome do subtitulo

            // Estilo dos botões
            $("#site_bloqueio_lista button").button();
            
            // Inicializa forms
            $("#site_bloqueio_lista").Forms("initializeFields");

            // Inicializa grid de bloqueio 
            $("#tbBloqueio").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 320,
                shrinkToFit: false, //1308
                scroll: true,
                colNames: ["Codigo Item", "Tipo Registro", "Codigo Arquivo", "Codigo Processo", "Tipo Arquivo", "Data Inclusao Arquivo", "Status Item"],
                colModel: [
                    { name: "CodigoArquivoItem", index: "CodigoArquivoItem", key: true, sorttype: "integer" },
                    { name: "TipoArquivoItem", index: "TipoArquivoItem" },
                    { name: "CodigoArquivo", index: "CodigoArquivo" },
                    { name: "CodigoProcesso", index: "CodigoProcesso" },
                    { name: "TipoArquivo", index: "TipoArquivo" },
                    { name: "DataInclusao", index: "DataInclusao" }, //1308
                    { name: "StatusArquivoItem", index: "StatusArquivoItem" }
                ],
                sortable: true,
                multiselect: true,
                ondblClickRow: function(rowid, iRow, iCol, e) {

                    // Pega a linha selecionada
                    var idArquivoItem = $("#tbBloqueio").jqGrid("getGridParam", "selrow");
                    var arquivoItem = $('#tbBloqueio').jqGrid('getRowData', idArquivoItem);

                    if (arquivoItem.CodigoArquivoItem) {
                        idArquivoItem = idArquivoItem.substring(idArquivoItem.lastIndexOf("_") + 1);
                        // Pede o detalhe
                        $.bloqueio.detalhe.abrir(arquivoItem);
                    }
                    else {
                        // Pede para selecionar a linha
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Favor selecionar uma linha!",
                            callbackOK: function() { }
                        });
                    }
                }

            });

            // Botão filtrar
            $("#cmdFiltrar").click(function() {
                $.bloqueio.lista.listar();
            });

            // Botão atualizar lista
            $("#cmdAtualizarLista").click(function() {
                $.bloqueio.lista.listar();
            });

            // Botão bloquear marcados
            $("#cmdBloquearMarcados").click(function() {
            
                // Atualiza os itens para BLOQUEADO
                $.bloqueio.lista.atualizarMarcados(true, function() {
                
                    // Mostra popup de sucesso
                    popup({
                        titulo: "Bloquear itens de arquivo",
                        icone: "sucesso",
                        mensagem: "Itens bloqueados com sucesso!",
                        callbackOK: function() {
                            // Lista todos os bloqueios
                            $.bloqueio.lista.listar();
                        }
                    });
                });
            });

            // Botão debloquear marcados
            $("#cmdDesbloquearMarcados").click(function() {
                // Atualiza os itens para DESBLOQUEADO
                $.bloqueio.lista.atualizarMarcados(false, function() {
                    // Mostra popup de sucesso
                    popup({
                        titulo: "Desbloquear itens de arquivo",
                        icone: "sucesso",
                        mensagem: "Itens desbloqueados com sucesso!",
                        callbackOK: function() {
                            // Lista todos os bloqueios
                            $.bloqueio.lista.listar();
                        }
                    });
                });
            });

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbBloqueio").setGridWidth($(window).width() - margemEsquerda);
                $("#tbBloqueio").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");


            // Botão linhas
            $("#cmdDetalharBloqueio").click(function() {

                // Pega a linha selecionada
                var idArquivoItem = $("#tbBloqueio").jqGrid("getGridParam", "selrow");
                var arquivoItem = $('#tbBloqueio').jqGrid('getRowData', idArquivoItem);

                if (arquivoItem.CodigoArquivoItem) {
                    idArquivoItem = idArquivoItem.substring(idArquivoItem.lastIndexOf("_") + 1);
                    // Pede o detalhe
                    $.bloqueio.detalhe.abrir(arquivoItem);
                }
                else {
                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            if (callback)
                callback();
        },



        // -----------------------------------------------------------------------------
        // listar: Lista os bloqueios
        // -----------------------------------------------------------------------------
        listar: function(callback) 
        {        
            //1308 INICIO
            var vok    = "S";
            var dtinia = $("#cmbFiltroDataInicial").val();
            var dtfima = $("#cmbFiltroDataFinal").val();        
            var dtinib = "";
            var dtfimb = "";
            if (dtinia != "")
            {            
                dtinib = parseInt(dtinia.split("/")[2].toString() + dtinia.split("/")[1].toString() + dtinia.split("/")[0].toString());
            }
               
            if (dtfima != "")
            {
                dtfimb = parseInt(dtfima.split("/")[2].toString() + dtfima.split("/")[1].toString() + dtfima.split("/")[0].toString());
            }
            
            if (dtfimb)
            {
                if (dtfimb < dtinib) 
                {
                    vok = "N"
                    popup(
                    {   titulo: "ERRO"
                    ,   icone: "erro"
                    ,   mensagem: "Periodo final menor que inicial, no filtro data de inclusao",
                        callbackOK: function() {$("#cmbFiltroDataFinal").focus();}
                    });
                }
            }
            
            if (vok = "S")
            {
            //1308 FIM
                // Cria request
                var request = {};
        
                // Preencho o request
                $("#site_bloqueio_lista #filtro").Forms("getData", { dataObject: request });

                request.FiltroIgnorarConteudo = true;
                request.FiltroSomenteBloqueados = true;
                request.FiltroMaxLinhas = 100;


                // Pede lista de bloqueios
                executarServico(
                "ListarArquivoItemRequest",
                request,
                function(data) {

                    // Limpa grid
                    $("#tbBloqueio").resetSelection();
                    $("#tbBloqueio").clearGridData();
                    for (var i = 0; i < data.Resultado.length; i++)
                        $("#tbBloqueio").jqGrid("addRowData", data.Resultado[i].CodigoArquivoItem, data.Resultado[i]);

                    // Chama callback
                    if (callback)
                        callback();
                });
            }//1308
        },


        // -----------------------------------------------------------------------------
        // listar: Lista os bloqueios
        // -----------------------------------------------------------------------------
        listar2: function(codigoProcesso, callback) {

            // Cria request
            var request = {};

            // Preencho o request
            $("#site_bloqueio_lista #filtro").Forms("getData", { dataObject: request });

            request.FiltroCodigoProcesso = codigoProcesso;
            request.FiltroIgnorarConteudo = true;
            request.FiltroSomenteBloqueados = true;
            request.FiltroMaxLinhas = 100;

            // Pede lista de bloqueios
            executarServico(
            "ListarArquivoItemRequest",
            request,
            function(data) {

                // Limpa grid
                $("#tbBloqueio").resetSelection();
                $("#tbBloqueio").clearGridData();
                for (var i = 0; i < data.Resultado.length; i++)
                    $("#tbBloqueio").jqGrid("addRowData", data.Resultado[i].CodigoArquivoItem, data.Resultado[i]);

                // Chama callback
                if (callback)
                    callback();
            });
        },

        // -----------------------------------------------------------------------------
        // atualizarMarcados: Atualiza os itens marcados (bloqueando ou desbloqueando)
        // -----------------------------------------------------------------------------
        atualizarMarcados: function(bloquear, callback) {

            // Obtenho os itens selecionados
            var ids = $("#tbBloqueio").getGridParam("selarrrow");

            // Verifico se algum item foi selecionado
            if (!ids) {
                // Mostra popup de aviso
                popup({
                    titulo: "Atenção!",
                    icone: "atencao",
                    mensagem: "Nenhum item selecionado!",
                    callbackOK: function() { }
                });

            } else {
                // Monto a mensagem de request
                var request = new Object();
                if (bloquear) {
                    request.CodigosArquivoItemBloqueados = ids;
                } else {
                    request.CodigosArquivoItemDesbloqueados = ids;
                }

                // Chamo o servico
                executarServico(
                    "SalvarArquivoItemRequest",
                    request,
                    function(data) {
                        // Chama callback
                        if (callback)
                            callback();
                    }
                );
            }
        }
    };

    // Pede a inicializacao
    $.bloqueio.lista.load();
});