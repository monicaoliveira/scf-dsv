﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_tratar_pagto_negativo.js"></script>
    <link href="site_tratar_pagto_negativo.css" rel="stylesheet" type="text/css" />
    <title>SCF - Tratamento de Pagamento Negativo</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--          Lista de Dias Não Úteis         -->
    <!-- ------------------------------------ -->
    <div id="site_tratar_pagto_negativo" class="lista">
        <table id="tbTratarPagtoNegativo"></table>
    </div>

</asp:Content>
