﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Contratos.Comum;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Web;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class site_relatorio_transacoes : PaginaBase
    {               
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Preenche combos dos filtros
                PreencherCombos();
            }
        }


        //===========================================
        // PREENCHIMENTO DO COMBOS
        //===========================================
        private void PreencherCombos()
        {
            
            //-------------------------------------------------------------------------------------------
            // CARREGA COMBO DE ORIGENS
            //-------------------------------------------------------------------------------------------

            //// Recebe lista de origens
            //ListarArquivoResponse itens =
            //    Mensageria.Processar<ListarArquivoResponse>(
            //        new ListarArquivoRequest()
            //        {
            //            CodigoSessao = Session["CodigoSessao"].ToString()
            //        });
            
            Dictionary<string, string> tiposArquivo;
            tiposArquivo = new Dictionary<string,string>();

            tiposArquivo.Add("ArquivoCCI", "ArquivoCCI");
            tiposArquivo.Add("ArquivoCSU", "ArquivoCSU");
            tiposArquivo.Add("ArquivoSitef", "ArquivoSitef");
            tiposArquivo.Add("ArquivoTSYS", "ArquivoTSYS");

            this.rptCmbOrigem.DataSource = tiposArquivo;
            this.rptCmbOrigem.DataBind();

            //-------------------------------------------------------------------------------------------
            // CARREGA COMBO DE GRUPOS (EMPRESA)
            //-------------------------------------------------------------------------------------------
            MensagemResponseBase retorno = null;
            try
            {
                if (Session["CodigoSessao"] != null)
                {
                    // Recebe lista de grupos
                    ListarEmpresaGrupoResponse grupos =
                        Mensageria.Processar<ListarEmpresaGrupoResponse>(
                            new ListarEmpresaGrupoRequest()
                            {
                                CodigoSessao = Session["CodigoSessao"].ToString()
                            });

                    // Preenche combo
                    rptCmbEmpresaGrupo.DataSource = grupos.Resultado;
                    rptCmbEmpresaGrupo.DataBind();

                    //-------------------------------------------------------------------------------------------
                    // CARREGA COMBO DE SUBGRUPOS (EMPRESA)
                    //-------------------------------------------------------------------------------------------

                    // Recebe lista de subgrupos
                    ListarEmpresaSubGrupoResponse subgrupos =
                        Mensageria.Processar<ListarEmpresaSubGrupoResponse>(
                            new ListarEmpresaSubGrupoRequest()
                            {
                                CodigoSessao = Session["CodigoSessao"].ToString()
                            });

                    // Preenche combo
                    rptCmbEmpresaSubGrupo.DataSource = subgrupos.Resultado;
                    rptCmbEmpresaSubGrupo.DataBind();

                    //-------------------------------------------------------------------------------------------
                    // CARREGA COMBO DE ESTABELECIMENTOS
                    //-------------------------------------------------------------------------------------------

                    // Recebe lista de estabelecimentos
                    ListarConfiguracaoTipoRegistroResponse configuracaoTipoRegistroLista =
                        Mensageria.Processar<ListarConfiguracaoTipoRegistroResponse>(
                            new ListarConfiguracaoTipoRegistroRequest()
                            {
                                CodigoSessao = Session["CodigoSessao"].ToString()
                            });

                    // Preenche combo
                    rptCmbTipoRegistro.DataSource = configuracaoTipoRegistroLista.Resultado;
                    rptCmbTipoRegistro.DataBind();

                    //-------------------------------------------------------------------------------------------
                    // CARREGA COMBO DE ESTABELECIMENTOS
                    //-------------------------------------------------------------------------------------------

                    // Recebe lista de estabelecimentos
                    ListarEstabelecimentoResponse estabelecimentos =
                        Mensageria.Processar<ListarEstabelecimentoResponse>(
                            new ListarEstabelecimentoRequest()
                            {
                                CodigoSessao = Session["CodigoSessao"].ToString()
                            });

                    // Preenche combo
                    rptCmbEstabelecimento.DataSource = estabelecimentos.Resultado;
                    rptCmbEstabelecimento.DataBind();

                    //-------------------------------------------------------------------------------------------
                    // CARREGA COMBO DE FAVORECIDOS
                    //-------------------------------------------------------------------------------------------

                    // Recebe lista de bancos favorecidos
                    ListarFavorecidoResponse favorecidos =
                        Mensageria.Processar<ListarFavorecidoResponse>(
                            new ListarFavorecidoRequest()
                            {
                                CodigoSessao = Session["CodigoSessao"].ToString()
                            });

                    // Preenche combo
                    rptCmbFavorecido.DataSource = favorecidos.Resultado;
                    rptCmbFavorecido.DataBind();

                    //-------------------------------------------------------------------------------------------
                    // CARREGA COMBO DE PRODUTOS
                    //-------------------------------------------------------------------------------------------

                    // Recebe lista de produtos
                    ListarProdutoResponse produtos =
                        Mensageria.Processar<ListarProdutoResponse>(
                            new ListarProdutoRequest()
                            {
                                CodigoSessao = Session["CodigoSessao"].ToString()
                            });

                    // Preenche combo
                    rptCmbProduto.DataSource = produtos.Resultado;
                    rptCmbProduto.DataBind();

                    //-------------------------------------------------------------------------------------------
                    // CARREGA COMBO DE PLANOS
                    //-------------------------------------------------------------------------------------------

                    // Recebe lista de planos
                    ListarPlanoResponse planos =
                        Mensageria.Processar<ListarPlanoResponse>(
                            new ListarPlanoRequest()
                            {
                                CodigoSessao = Session["CodigoSessao"].ToString()
                            });

                    // Preenche combo
                    rptCmbPlano.DataSource = planos.Resultado;
                    rptCmbPlano.DataBind();


                    //-------------------------------------------------------------------------------------------
                    // CARREGA COMBO DE MEIOS DE CAPTURA
                    //-------------------------------------------------------------------------------------------
                    ReceberListaResponse meioCaptura =
                        Mensageria.Processar<ReceberListaResponse>(new ReceberListaRequest { MnemonicoLista = "MeioCaptura" });

                    // Preenche combo
                    rptCmbMeioCaptura.DataSource = meioCaptura.ListaInfo.Itens;
                    rptCmbMeioCaptura.DataBind();
                }
                else
                {
                    retorno = new MensagemErroResponse()
                    {
                        StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                        DescricaoResposta = "Sessão Inválida"
                    };
                }
            }
            catch (Exception ex)
            {
                retorno = new MensagemErroResponse()
                {
                    StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                    DescricaoResposta = ex.ToString()
                };
            }
        }

        
    }
}
