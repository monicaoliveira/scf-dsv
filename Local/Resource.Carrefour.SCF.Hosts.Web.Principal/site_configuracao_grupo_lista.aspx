﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_grupo_lista.js"></script>
    <link href="site_configuracao_grupo_lista.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="site_configuracao_grupo_detalhe.js"></script>
    <link href="site_configuracao_grupo_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Cadastro de Grupos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--           Lista de Grupos          -->
    <!-- ------------------------------------ -->
    <div id="site_configuracao_grupo_lista">
            <div class="lista">
                <div class="botoes">
                    <table id="tbEmpresaGrupo"></table>
                    <button id="cmdNovoEmpresaGrupo">Novo</button>
                    <button id="cmdEditarEmpresaGrupo">Editar</button>
                    <button id="cmdRemoverEmpresaGrupo">Remover</button>
                    <button id="cmdAtualizarListaEmpresaGrupo">AtualizarLista</button>
                </div>
             </div>
    </div>

</asp:Content>
