﻿/// <reference path="../../lib/jquery/dev/jquery-1.4.1-vsdoc.js" />
/// <reference path="comum_servico.aspx.js" />

var permissoesCarregadas = false;
var perfisCarregados = false;
var gruposCarregados = false;
var bNovoGrupo = false;

$(document).ready(function() 
{
    //===============================================================================================
    // DIV grupo_lista - A PRIMEIRA TELA 
    //===============================================================================================
    $("#subtitulo").html("ADMINISTRATIVO [Cadastro de Grupos de Usuários]");
    $("button").button();
    $("input[type='button']").button();
    $("#cmdNovoGrupo").click(novoGrupo);
    //cmdEditarGrupo
    //cmdRemoverGrupo
    $("#cmdAtualizarLista").click(atualizarLista);
    
    //===============================================================================================
    // DIV grupo_edicao - A SEGUNDA TELA
    //===============================================================================================
    $("#grupo_edicao").tabs();
    //grupo_detalhe Detalhe
    //grupo_perfis Perfis
    //grupo_permissoes Permissões
    
    $("#cmdAdicionarPermissao").click(adicionarPermissao);
    //cmdRemoverPermissao
    $("#cmdAdicionarPerfil").click(adicionarPerfil);
    //cmdRemoverPerfil
    
    //===============================================================================================
    // DIV grupo_lista - A PRIMEIRA TELA - Grid contendo todos os grupos cadastrados
    //===============================================================================================
    $("#tbGrupoLista").jqGrid(  //1477 INCLUSAO DO CODIGO DO GRUPO NA LISTA
    {   datatype: "clientSide"
    ,   mtype: "GET"
    ,   sortname: "CodigoUsuarioGrupo"
    ,   sortorder: "asc"
    ,   sortable: true
    ,   viewrecords: true
    ,   caption: ""
    ,   colNames: ["GRUPO", "NOME"]
    ,   colModel:
        [   { name: "CodigoUsuarioGrupo", index: "CodigoUsuarioGrupo", align: "center", key: true, width: 50 }
        ,   { name: "NomeUsuarioGrupo", index: "NomeUsuarioGrupo", align: "left", width: 500, sortable: false }
        ]
    ,   ondblClickRow: function(rowid, status) 
        {   selecionarGrupo(rowid);
            return false;
        }
    });
    
    //===============================================================================================
    // DISTRIBUIÇÃO DAS COLUNAS NO GRID
    //===============================================================================================
    $(window).bind("resize", function() 
    {   $("#tbGrupoLista").setGridWidth($(window).width() - margemEsquerda - 30); 
        $("#tbGrupoLista").setGridHeight($(window).height() - margemRodape - 50); 
    }).trigger("resize"); 
    
    //===============================================================================================
    // DIV grupo_lista - A PRIMEIRA TELA - Botao ATUALIZAR
    //===============================================================================================
    atualizarLista();

    //===============================================================================================
    // DIV grupo_lista - A PRIMEIRA TELA - Botao EDITAR
    //===============================================================================================
    $("#cmdEditarGrupo").click(function() 
    {   bNovoGrupo = false;
        var codigoUsuarioGrupo = $("#tbGrupoLista").getGridParam("selrow");
        if (codigoUsuarioGrupo)
            selecionarGrupo(codigoUsuarioGrupo);
        else {
            $("#textoAlerta").html("Selecione um Grupo.");
            $("#mensagemAlerta").dialog(
            {   autoOpen: true
            ,   resizable: false
            ,   height: 150
            ,   modal: true
            ,   title: "Atenção"
            ,   buttons: 
                {   "Ok": function() 
                    {   $(this).dialog("close");
                        return false;
                    }
                }
            });
        }
        return false;
    });
    //===============================================================================================
    // DIV grupo_lista - A PRIMEIRA TELA - Botao REMOVER
    //===============================================================================================
    $("#cmdRemoverGrupo").click(function() 
    {   var codigoUsuarioGrupo = $("#tbGrupoLista").getGridParam("selrow");
        if (codigoUsuarioGrupo) 
        {   popup(
            {   titulo: "Atenção"
            ,   icone: "atencao"
            ,   mensagem: "Tem certeza que deseja remover este grupo?"
            ,   callbackSim: function() 
                {   removerGrupo(codigoUsuarioGrupo, function() 
                    {       popup({
                                titulo: "Remover Grupo",
                                icone: "sucesso",
                                mensagem: "Grupo removido com sucesso!",
                                callbackOK: function() { }
                            });
                    });
                }
            ,   callbackNao: function() { }
            });
        }
        else 
        {   popup({
                titulo: "Atenção",
                icone: "erro",
                mensagem: "Favor selecionar uma linha!",
                callbackOK: function() { }
            });
        }
        return false;
    });
    
    //===============================================================================================
    // DIV grupo_edicao - A SEGUNDA TELA - Popup com abas DETALHE, PERFIS e PERMISSOES
    //===============================================================================================
    $("#grupo_edicao").dialog(
    {   autoOpen: false
    //1477 ,   title: "GRUPO [" + $("#grupo_detalhe #nomeUsuarioGrupo").val() + "] - Editar"
    ,   height: 320
    ,   width: 700
    ,   modal: true
    ,   buttons: 
        {   'Salvar': function() 
            {   salvarGrupo();
                $(this).dialog("close");
                return false;
            }
        ,   'Cancelar': function() 
            {   $(this).dialog("close");
                return false;
            }
        }

    });
    

    //===============================================================================================
    // DIV grupo_edicao - A SEGUNDA TELA - Popup Perfis do grupo
    //===============================================================================================
    $("#tbGrupoPerfilLista").jqGrid(
    {   datatype: "clientSide"
    ,   mtype: "GET"
    ,   viewrecords: true
    ,   caption: ""
    ,   colNames: ["CD", "Perfil"]
    ,   colModel: 
        [   { name: "CodigoPerfil", index: "CodigoPerfil", width: 100, align: "center", key: true }
        ,   { name: "NomePerfil", index: "NomePerfil", width: 500 }
        ]
    });
    //===============================================================================================
    // DIV grupo_edicao - A SEGUNDA TELA - Popup Permissoes do grupo
    //===============================================================================================
    $("#tbGrupoPermissaoLista").jqGrid( //1477 ajuste na tela
    {   datatype: "clientSide"
    ,   mtype: "GET"
    ,   viewrecords: true
    ,   caption: ""
    ,   colNames: ["CD", "Permissão", "Status"]
    ,   colModel: 
        [   { name: "CodigoPermissao", index: "CodigoPermissao", width: 100, align: "center", key: true }
        ,   { name: "NomePermissao", index: "NomePermissao", width: 400 }
        ,   { name: "Status", index: "Status", width: 120, align: "right", edittype: "select", formatter: "select", editoptions: { value: "0:Permitido;1:Negado"} }
        ]
    });
    //===============================================================================================
    // DIV grupo_edicao - ABA PERMISSAO GRUPO - Botao REMOVER PERMISSAO
    //===============================================================================================
    $("#cmdRemoverPermissao").click(function() 
    {   var codigoPermissao = $("#tbGrupoPermissaoLista").getGridParam("selrow");

        if (codigoPermissao)
            $("#tbGrupoPermissaoLista").delRowData($("#tbGrupoPermissaoLista").getGridParam("selrow"));
        else {
            $("#textoAlerta").html("Selecione uma Permissão.");
            $("#mensagemAlerta").dialog(
            {   autoOpen: true
            ,   title: "Atenção"
            ,   resizable: false
            ,   height: 150
            ,   modal: true
            ,   buttons: 
                {   "Ok": function() 
                    {   $(this).dialog("close");
                        return false;
                    }
                }
            });
        }
        return false;
    });
    //===============================================================================================
    // DIV grupo_edicao - ABA PERFIL GRUPO - Botao REMOVER PERFIL
    //===============================================================================================
    $("#cmdRemoverPerfil").click(function() 
    {   var codigoPefil = $("#tbGrupoPerfilLista").getGridParam("selrow");
        if (codigoPefil)
            $("#tbGrupoPerfilLista").delRowData($("#tbGrupoPerfilLista").getGridParam("selrow"));
        else 
        {   $("#textoAlerta").html("Selecione um Perfil.");
            $("#mensagemAlerta").dialog(
            {   autoOpen: true
            ,   title: "Atenção"
            ,   resizable: false
            ,   height: 150
            ,   modal: true
            ,   buttons: 
                {   "Ok": function() 
                    {   $(this).dialog("close");
                        return false;
                    }
                }

            });
        }
        return false;
    });

    //===============================================================================================
    // DIV confirmacaoExclusao
    //===============================================================================================
    $("#confirmacaoExclusao").dialog(
    {   autoOpen: false
    ,   resizable: false
    ,   height: 150
    ,   modal: true
    ,   buttons: 
        {   "Sim": function() 
            {  $(this).dialog("close");
                return false;
            }
        ,   "Não": function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });
    //===============================================================================================
    // GRID PERMISSOES
    //===============================================================================================
    $("#permissao_lista").dialog({   
        title: "GRUPO - Adicionar Permissão" // 1477 AJUSTE TELA
    ,   autoOpen: false
    ,   height: 350
    ,   width: 500
    ,   modal: true
    ,   close: function() {}
    ,   buttons: 
        {   'Incluir': function() 
            {   var rowid = $("#tbPermissaoLista").getGridParam("selrow");
                var rowdata = $("#tbPermissaoLista").getRowData(rowid);
                rowdata.Status = $("#cmbTipoPermissao").val();
                $("#tbGrupoPermissaoLista").addRowData(rowid,rowdata,"last","after");
                $(this).dialog("close");
                return false;
            }
        ,   'Cancelar': function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });
    //===============================================================================================
    // TABELA PERMISSÕES
    //===============================================================================================
    $("#tbPermissaoLista").jqGrid( //1477 INCLUSAO DO CODIGO NO GRID
    {   datatype: "clientSide"
    ,   height: 200 //1477
    ,   width: 450 //1477
    ,   sortable: false //1477
    ,   shrinkToFit: false //1477
    ,   mtype: "GET"
    //,   viewrecords: true //1477
    ,   caption: ""
    ,   colNames: ["Cd", "Permissao"]
    ,   colModel: 
        [   { name: "CodigoPermissao", index: "CodigoPermissao", width: 100, sortable: false, align: "center", key: true }
        ,   { name: "NomePermissao", index: "NomePermissao", sortable: false, width: 300 }
        ]
    });

    //===============================================================================================
    // GRID PERFIS
    //===============================================================================================
    $("#perfil_lista").dialog({
        title: "GRUPO - Adicionar Perfil"       // 1477 AJUSTE TELA
    ,   autoOpen: false 
    ,   height: 350
    ,   width: 500
    ,   modal: true
    ,   close: function() {}
    ,   buttons: 
        {   'Incluir': function() 
            {   var rowid = $("#tbPerfilLista").getGridParam("selrow");
                var rowdata = $("#tbPerfilLista").getRowData(rowid);
                $("#tbGrupoPerfilLista").addRowData(rowid,rowdata,"last","after");
                $(this).dialog("close");
                return false;
            }
        ,   'Cancelar': function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });
    //===============================================================================================
    // TABELA PERFIS
    //===============================================================================================
    $("#tbPerfilLista").jqGrid(
    {   datatype: "clientSide"
    ,   height: 200 //1477
    ,   width: 450 //1477
    ,   shrinkToFit: false //1477    
    ,   mtype: "GET"
    ,   viewrecords: true
    ,   caption: ""    
    ,   colNames: ["CD", "Perfil"]
    ,   colModel: 
        [   { name: "CodigoPerfil", index: "CodigoPerfil", width: 100, align: "center", key: true }
        ,   { name: "NomePerfil", index: "NomePerfil", width: 300 }
        ]

    });





});
//===============================================================================================
// DIV grupo_lista - A PRIMEIRA TELA - Botao cmdAtualizarLista
//===============================================================================================
function atualizarLista() 
{   executarServico("ListarUsuarioGruposRequest", {}, "atualizarListaCallback"); //PR_GRUPO_ACESSO_L
}
//===============================================================================================
function atualizarListaCallback(data) 
{   $("#tbGrupoLista").clearGridData();
    for (var i = 0; i < data.UsuarioGrupos.length; i++)
        $("#tbGrupoLista").addRowData(data.UsuarioGrupos[i].CodigoUsuarioGrupo, data.UsuarioGrupos[i], "last", "after");
}

//===============================================================================================
// DIV grupo_lista - A PRIMEIRA TELA - Botao cmdNovoGrupo
//===============================================================================================
function novoGrupo() 
{
    bNovoGrupo = true;
    $("#grupo_detalhe #codigoUsuarioGrupo").val("");
    $("#grupo_detalhe #nomeUsuarioGrupo").val("");
    $("#tbGrupoPerfilLista").clearGridData(); //1477
    $("#tbGrupoPermissaoLista").clearGridData();
    $("#grupo_edicao").tabs("select",0); //1477
    $("#grupo_edicao").dialog({title: "GRUPO - Inclusao"});  //1477
    $("#grupo_edicao").dialog("open");
}
//===============================================================================================
// DIV grupo_lista - A PRIMEIRA TELA - Botao cmdRemoverGrupo
//===============================================================================================
function removerGrupo(codigoUsuarioGrupo, callback) 
{   if (codigoUsuarioGrupo) 
    {   executarServico("RemoverUsuarioGrupoRequest",{ CodigoUsuarioGrupo: codigoUsuarioGrupo },function(data) 
        {       alert(data.StatusResposta);
                alert(data.DescricaoResposta );
        
                if (data.StatusResposta == "OK") 
                {
                    atualizarLista();
                }
                else
                {   popup(
                    {   titulo: "REMOVER"
                    ,   icone: "erro"
                    ,   mensagem: data.DescricaoResposta 
                    ,   callbackOK: function() { }
                    });
                }
                if (callback)
                    callback();
        });
    }
    else 
    {   popup(
        {   titulo: "Atenção"
        ,   icone: "erro"
        ,   mensagem: "Favor selecionar uma linha!"
        ,   callbackOK: function() { }
        });
    }
}
//===============================================================================================
// DIV grupo_lista - A PRIMEIRA TELA - CLICK na linha tbGrupoLista OU Botao cmdEditarGrupo
//===============================================================================================
function selecionarGrupo(codigoUsuarioGrupo) 
{   executarServico("ReceberUsuarioGrupoRequest",{CodigoUsuarioGrupo: codigoUsuarioGrupo,PreencherColecoesCompletas: true}, "selecionarGrupoCallback");
}
//===============================================================================================
function selecionarGrupoCallback(data) 
{   $("#grupo_detalhe #codigoUsuarioGrupo").val(data.UsuarioGrupo.CodigoUsuarioGrupo);
    $("#grupo_detalhe #codigoUsuarioGrupo").attr("disabled", "disabled");
    $("#grupo_detalhe #nomeUsuarioGrupo").val(data.UsuarioGrupo.NomeUsuarioGrupo);
    //===============================================================================================
    $("#tbGrupoPermissaoLista").clearGridData();
    for (var i = 0; i < data.UsuarioGrupo.Permissoes.length; i++) 
    {   var permissao = data.UsuarioGrupo.Permissoes[i];
        var permissaoInfo = 
        {   CodigoPermissao: permissao.CodigoPermissao,
            NomePermissao: permissao.PermissaoInfo.NomePermissao,
            Status: permissao.Status
        };
        if (permissaoInfo.Status == "Negado")
            permissaoInfo.Status = "1";
        if (permissaoInfo.Status == "Permitido")
            permissaoInfo.Status = "0";    
        $("#tbGrupoPermissaoLista").addRowData(data.UsuarioGrupo.Permissoes[i].CodigoPermissao, permissaoInfo, "last", "after");
    }
    //===============================================================================================
    $("#tbGrupoPerfilLista").clearGridData();
    for (var i = 0; i < data.UsuarioGrupo.Perfis2.length; i++)
        $("#tbGrupoPerfilLista").addRowData(data.UsuarioGrupo.Perfis2[i].CodigoPerfil, data.UsuarioGrupo.Perfis2[i], "last", "after");
    //===============================================================================================
    $("#grupo_edicao").tabs("select",0); //1477
    $("#grupo_edicao").dialog( //1477
        {title: 
            "GRUPO [" 
            + $("#grupo_detalhe #codigoUsuarioGrupo").val() 
            + " - "
            + $("#grupo_detalhe #nomeUsuarioGrupo").val() 
            + "] - Editar"});
    $("#grupo_edicao").dialog("open");
}
//===============================================================================================
// DIV grupo_edicao - A SEGUNDA TELA - Botao Salvar
//===============================================================================================
function salvarGrupo() 
{   var grupo = new Object();
    grupo.CodigoUsuarioGrupo = $("#grupo_detalhe #codigoUsuarioGrupo").val();
    grupo.NomeUsuarioGrupo = $("#grupo_detalhe #nomeUsuarioGrupo").val();
    grupo.Permissoes = new Array();
    var permissoesIds = $("#tbGrupoPermissaoLista").getDataIDs();
    for (var i = 0; i < permissoesIds.length; i++) 
    {   grupo.Permissoes[i] = new Object();
        grupo.Permissoes[i].CodigoPermissao = permissoesIds[i];
        grupo.Permissoes[i].Status = $("#tbGrupoPermissaoLista").getCell(permissoesIds[i], "Status");
    }
    grupo.Perfis = new Array();
    var perfisIds = $("#tbGrupoPerfilLista").getDataIDs();
    for (var i = 0; i < perfisIds.length; i++)
        grupo.Perfis[i] = perfisIds[i];

    executarServico("SalvarUsuarioGrupoRequest",{UsuarioGrupo: grupo,Novo: bNovoGrupo},"salvarGrupoCallback");
}
//===============================================================================================
function salvarGrupoCallback(data) 
{   atualizarLista();
}
//===============================================================================================
// DIV grupo_edicao - A SEGUNDA TELA - Botao cmdAdicionarPermissao
//===============================================================================================
function adicionarPermissao() 
{   if (!permissoesCarregadas) 
    {   carregarPermissoes();
        permissoesCarregadas = true;
    }
    $("#permissao_lista").dialog("open");
}
//===============================================================================================
function carregarPermissoes() 
{   executarServico("ListarPermissoesRequest", {}, "carregarPermissoesCallback");
}
//===============================================================================================
function carregarPermissoesCallback(data) 
{   $("#tbPermissaoLista").clearGridData();
    for (var i = 0; i < data.Permissoes.length; i++)
        $("#tbPermissaoLista").addRowData
        (   data.Permissoes[i].PermissaoInfo.CodigoPermissao
        ,   data.Permissoes[i].PermissaoInfo
        ,   "last"
        ,   "after"
        );
}
//===============================================================================================
// DIV grupo_edicao - A SEGUNDA TELA - Botao cmdAdicionarPerfil
//===============================================================================================
 function adicionarPerfil() 
 {  if (!perfisCarregados) 
    {   carregarPerfis();
        perfisCarregados = true;
    }
    $("#perfil_lista").dialog("open");
}
//===============================================================================================
function carregarPerfis() 
{   executarServico("ListarPerfisRequest", {}, "carregarPerfisCallback");
}
//===============================================================================================
function carregarPerfisCallback(data) 
{   $("#tbPerfilLista").clearGridData();
    for (var i = 0; i < data.Perfis.length; i++)
        $("#tbPerfilLista").addRowData(data.Perfis[i].CodigoPerfil, data.Perfis[i], "last", "after");
}
