﻿$(document).ready(function() {

    // Namespace
    if (!$.plano)
        $.plano = {};

    // Funcoes do namespace 
    $.plano.detalhe = {

        variaveis: {},

        // ----------------------------------------------------------------------
        // load: funcoes de inicializacao da tela
        // ----------------------------------------------------------------------
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_configuracao_plano_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_configuracao_plano_detalhe.aspx #site_configuracao_plano_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_configuracao_plano_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.plano.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();
                    });
                });

            } else {

                // Chama o callback
                if (callback)
                    callback();
            }
        },

        // --------------------------------------------------------------------
        //  load2: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load2: function() {

            // Inicializa forms
            $("#site_configuracao_plano_detalhe").Forms("initializeFields");

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Planos");

            // Estilo dos botões
            $("#site_configuracao_plano_detalhe button").button();

            // Inicializa o dialog do detalhe
            $("#site_configuracao_plano_detalhe").dialog({
                autoOpen: false,
                height: 190,
                width: 630,
                modal: true,
                title: "Detalhe do Plano",
                buttons: {
                    "Salvar": function() {

                        // Pede para salvar
                        $.plano.detalhe.salvar(function() {

                            // Mostra popup de sucesso
                            popup({
                                titulo: "Salvar Plano",
                                icone: "sucesso",
                                mensagem: "Plano salvo com sucesso!",
                                callbackOK: function() {

                                    // Fecha dialog de detalhe
                                    $("#site_configuracao_plano_detalhe").dialog("close");
                                    $.plano.lista.listarPlano(null, true);
                                }
                            });
                        });
                    },
                    "Fechar": function() {

                        // Fecha o dialog
                        $("#site_configuracao_plano_detalhe").dialog("close");
                    }
                }
            });
        },

        // ----------------------------------------------------------------------
        // abrir: funcoes executadas na abertura da tela
        // ----------------------------------------------------------------------
        abrir: function(codigoPlano, callback) {

            // Garante que o html está carregado
            $.plano.detalhe.load(function() {

                if (codigoPlano) {

                    // Guarda código do plano
                    $.plano.detalhe.variaveis.codigoPlano = codigoPlano;

                    // Limpa campos do detalhe do plano
                    $("#site_configuracao_plano_detalhe").Forms("clearData");

                    // Carrega dados do plano existente
                    $.plano.detalhe.carregar(codigoPlano, function() {

                        // Abre dialog de detalhe
                        $("#site_configuracao_plano_detalhe").dialog("open");
                    });
                }

                else {

                    // Limpa o objeto
                    $.plano.detalhe.variaveis.codigoPlano = null;

                    // Limpa campos do detalhe do plano
                    $("#site_configuracao_plano_detalhe").Forms("clearData");

                    // Abre dialog de detalhe
                    $("#site_configuracao_plano_detalhe").dialog("open");
                }
            });
        },

        // ----------------------------------------------------------------------
        // carregar: Carrega dados referentes ao plano
        // ----------------------------------------------------------------------
        carregar: function(codigoPlano, callback) {

            executarServico(
                "ReceberPlanoRequest",
                { CodigoPlano: codigoPlano },
                function(data) {

                    // Guarda dados do plano recebido
                    $.plano.detalhe.variaveis.planoInfo = data.PlanoInfo;

                    // Preenche os campos do detalhe
                    $("#site_configuracao_plano_detalhe").Forms("setData", { dataObject: data.PlanoInfo });

                    if (data.PlanoInfo.Comissao == 0) {
                        $("#txtComissao").val("0");
                    }

                    // Chama callback
                    if (callback)
                        callback();
                });
        },

        // ----------------------------------------------------------------------
        // salvar: Trata inserção ou alteração de um Plano
        // ----------------------------------------------------------------------
        salvar: function(callback) {

            // Pede validação necessária
            $.plano.detalhe.validar(function() {

                // Inicializo os campos HIDDEN
                if ($("#hidCodigoPlano").val() == "") {
                    $("#hidCodigoPlano").val("-");
                }
                if ($("#hidAtivo").val() == "") {
                    $("#hidAtivo").val("true");
                }

                // Cria novo objeto
                $.plano.detalhe.variaveis.planoInfo = {};

                // Envia informações ao novo objeto
                $("#site_configuracao_plano_detalhe").Forms("getData", { dataObject: $.plano.detalhe.variaveis.planoInfo });

                executarServico(
                    "ListarPlanoRequest",
                    { FiltroCodigoPlanoTSYS: $.plano.detalhe.variaveis.planoInfo.CodigoPlanoTSYS },
                    function(data) {
                        // Se encontrou um outro codigo TSYS igual, verifica se não é o mesmo plano (alteração)
                        if (data.Resultado[0] && data.Resultado[0].CodigoPlano != $.plano.detalhe.variaveis.planoInfo.CodigoPlano) {
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "Código TSYS já existe! Deseja alterar o plano?",
                                callbackSim: function() {
                                    // Guarda dados do plano recebido
                                    $("#site_configuracao_plano_detalhe").Forms("getData", { dataObject: $.plano.detalhe.variaveis.planoInfo });
                                    $.plano.detalhe.variaveis.planoInfo.CodigoPlano = data.Resultado[0].CodigoPlano;
                                    executarServico(
                                        "SalvarPlanoRequest",
                                        { PlanoInfo: $.plano.detalhe.variaveis.planoInfo },
                                        function(data) {
                                            // Chama callback
                                            if (callback)
                                                callback();
                                        });
                                },
                                callbackNao: function() { }
                            });

                        } else {
                            // Cria novo objeto
                            planoInfo = {};

                            // Envia informações ao novo objeto
                            $("#site_configuracao_plano_detalhe").Forms("getData", { dataObject: planoInfo });

                            executarServico(
                                "SalvarPlanoRequest",
                                { PlanoInfo: planoInfo },
                                function(data) {
                                    // Chama callback
                                    if (callback)
                                        callback();
                                });
                        }
                    }
                );
            },
            // callbackErro
            function() {
                return true;
            });
        },

        // ----------------------------------------------------------------------
        // remover: Trata remoção de um plano
        // ----------------------------------------------------------------------
        remover: function(codigoPlano, callback) {

            if (codigoPlano) {

                // Salva objeto com o item removido
                executarServico(
                    "RemoverPlanoRequest",
                    { CodigoPlano: codigoPlano },
                    function(data) {

                        // Chama o callback
                        if (callback)
                            callback(data);
                    });
            }

            else {

                // Pede para selecionar uma linha
                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Favor selecionar uma linha!",
                    callbackOK: function() { }
                });
            }
        },

        validar: function(callbackOk, callbackErro) {

            // Inicializa criticas
            var criticas = new Array();

            // caso a comissao esteja nulo, seta como zero
            if ($("#txtComissao").val() == "") {
                $("#txtComissao").val("0");
            }
            
            // Pede a validação para o forms
            criticas = $("#site_configuracao_plano_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });

            // Se tem criticas, mostra o formulario
            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                callbackErro();

            } else
                callbackOk();
        }

    };

    // Pede a inicializacao
    $.plano.detalhe.load();
});