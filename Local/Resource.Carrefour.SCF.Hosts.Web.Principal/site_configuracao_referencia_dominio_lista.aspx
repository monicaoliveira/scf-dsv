﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_referencia_dominio_lista.js"></script>
    <script type="text/javascript" src="site_configuracao_referencia_dominio_detalhe.js"></script>
    <link href="site_configuracao_referencia_dominio_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_configuracao_referencia_dominio_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Cadastro de Referencias de Domínios</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
<!-- 1309 REFAZENDO TELA-->
    <div id="site_configuracao_referencia_dominio_lista" class="lista">
        <table id="tbLinkDominio"></table> <!-- 1309<table id="tbReferenciaDominio"></table>-->
        <div class="botoes">
            <button id="cmdNovo">Novo</button>
            <button id="cmdConsultar">Consultar</button> 
            <button id="cmdRemover">Remover</button>
            <button id="cmdAtualizarLista">Atualizar Lista</button>
        </div>
    </div>

</asp:Content>
