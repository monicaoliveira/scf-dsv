﻿$(document).ready(function() {

    // Namespace
    if (!$.grupo)
        $.grupo = {};

    // Funcoes do namespace 
    $.grupo.detalhe = {

        variaveis: {},

        // ----------------------------------------------------------------------
        // load: funcoes de inicializacao da tela
        // ----------------------------------------------------------------------
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_configuracao_grupo_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_configuracao_grupo_detalhe.aspx #site_configuracao_grupo_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_configuracao_grupo_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.grupo.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();
                    });
                });

            } else {

                // Chama o callback
                if (callback)
                    callback();
            }
        },

        // --------------------------------------------------------------------
        //  load2: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load2: function() {

            // Estilo dos botões
            $("#site_configuracao_grupo_detalhe button").button();

            // Inicializa forms
            $("#site_configuracao_grupo_detalhe").Forms("initializeFields");

            // Inicializa o dialog do detalhe
            $("#site_configuracao_grupo_detalhe").dialog({
                autoOpen: false,
                height: 200,
                width: 500,
                modal: true,
                title: "Detalhe do Grupo",
                buttons: {
                    "Salvar": function() {

                        // Pede para salvar
                        $.grupo.detalhe.salvar(function() {

                            // Mostra popup de sucesso
                            popup({
                                titulo: "Salvar Grupo",
                                icone: "sucesso",
                                mensagem: "Grupo salvo com sucesso!",
                                callbackOK: function() {
                                    $.grupo.lista.listarGrupo();
                                    // Fecha dialog de detalhe
                                    $("#site_configuracao_grupo_detalhe").dialog("close");
                                }
                            });
                        });
                    },
                    "Fechar": function() {

                        // Fecha o dialog
                        $("#site_configuracao_grupo_detalhe").dialog("close");
                    }
                }
            });
        },

        // ----------------------------------------------------------------------
        // abrir: funcoes executadas na abertura da tela
        // ----------------------------------------------------------------------
        abrir: function(codigoEmpresaGrupo, callback) {

            // Garante que o html está carregado
            $.grupo.detalhe.load(function() {

                if (codigoEmpresaGrupo) {

                    // Guarda código do EmpresaGrupo
                    $.grupo.detalhe.variaveis.codigoEmpresaGrupo = codigoEmpresaGrupo;

                    // Limpa campos
                    $("#site_configuracao_grupo_detalhe").Forms("clearData");

                    // Carrega dados do Grupo existente
                    $.grupo.detalhe.carregar(codigoEmpresaGrupo, function() {

                        // Abre dialog de detalhe
                        $("#site_configuracao_grupo_detalhe").dialog("open");
                    });
                }

                else {

                    // Limpa código do EmpresaGrupo
                    $.grupo.detalhe.variaveis.codigoEmpresaGrupo = null;

                    // Limpa campos
                    $("#site_configuracao_grupo_detalhe").Forms("clearData");

                    // Abre dialog de detalhe
                    $("#site_configuracao_grupo_detalhe").dialog("open");
                }
            });
        },

        // ----------------------------------------------------------------------
        // carregar: Carrega dados referentes ao Grupo
        // ----------------------------------------------------------------------
        carregar: function(codigoEmpresaGrupo, callback) {

            executarServico(
                "ReceberEmpresaGrupoRequest",
                { CodigoEmpresaGrupo: codigoEmpresaGrupo },
                function(data) {

                    // Guarda dados do Grupo recebido
                    $.grupo.detalhe.variaveis.empresaGrupoInfo = data.EmpresaGrupoInfo;

                    // Preenche os campos do detalhe
                    $("#site_configuracao_grupo_detalhe").Forms("setData", { dataObject: data.EmpresaGrupoInfo });

                    // Chama callback
                    if (callback)
                        callback();
                });
        },

        // ----------------------------------------------------------------------
        // salvar: Trata inserção ou alteração de um Grupo
        // ----------------------------------------------------------------------
        salvar: function(callback) {

            $.grupo.detalhe.validar(function() {
                var empresaGrupoInfo = null;

                if ($.grupo.detalhe.variaveis.codigoEmpresaGrupo) {

                    // Recebe EmpresaGrupoInfo
                    empresaGrupoInfo = $.grupo.detalhe.variaveis.empresaGrupoInfo;

                    // Envia informações ao objeto existente
                    $("#site_configuracao_grupo_detalhe").Forms("getData", { dataObject: empresaGrupoInfo });

                    // Limpa temp
                    $.grupo.detalhe.variaveis.codigoEmpresaGrupo = null;

                } else {

                    // Cria novo objeto
                    empresaGrupoInfo = {};

                    // Envia informações ao novo objeto
                    $("#site_configuracao_grupo_detalhe").Forms("getData", { dataObject: empresaGrupoInfo });
                }


                executarServico(
                    "SalvarEmpresaGrupoRequest",
                    { EmpresaGrupoInfo: empresaGrupoInfo },
                    function(data) {

                        // Chama callback
                        if (callback)
                            callback();
                    });
            }, function() { return true; });
        },

        // --------------------------------------------------------------------
        //  validar: valida campos necessários do cadastro de planos
        // --------------------------------------------------------------------
        validar: function(callbackOk, callbackErro) {



            // Inicializa criticas
            var criticas = new Array();

            // Pede a validação para o forms
            criticas = $("#site_configuracao_grupo_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });

            // Se tem criticas, mostra o formulario
            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                callbackErro();

            } else
                callbackOk();
        },

        // ----------------------------------------------------------------------
        // remover: Trata remoção de um Grupo
        // ----------------------------------------------------------------------
        remover: function(codigoEmpresaGrupo, callback) {

            if (codigoEmpresaGrupo) {

                // Salva objeto com o item removido
                executarServico(
                    "RemoverEmpresaGrupoRequest",
                    { CodigoEmpresaGrupo: codigoEmpresaGrupo },
                    function(data) {
                        $.grupo.lista.listarGrupo();
                        // Chama o callback
                        if (callback)
                            callback();
                    });
            }

            else {

                // Pede para selecionar uma linha
                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Favor selecionar uma linha!",
                    callbackOK: function() { }
                });
            }
        }
    };

    // Pede a inicializacao
    $.grupo.detalhe.load();
});