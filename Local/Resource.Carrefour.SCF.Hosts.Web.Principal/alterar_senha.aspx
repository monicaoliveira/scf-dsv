﻿<%@ Page Title="Carrefour Soluções Financeiras - Alterar Senha de Usuário" Language="C#"  AutoEventWireup="true" CodeBehind="alterar_senha.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.alterarSenha" %>
<%-- 1415 - Retirado MasterPage MasterPageFile="~/template_default.Master"--%>
<%-- 1415 - Retirado MasterPage 

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="alterar_senha.js"></script>
</asp:Content> --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
	    <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=EDGE,chrome=1" />

        <!-- ESTILOS jQuery-->
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.7.custom.css" rel="stylesheet" />	

        <!-- ESTILOS pagina -->
        <link rel="stylesheet" media="screen" href="template_default.css" />
        <link rel="stylesheet" media="screen" href="login.css" />
        
        <!-- SCRIPTS jQuery-->
        <script type="text/javascript" src="js/lib/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="js/lib/jquery-ui-1.8.11.custom.min.js"></script>
		<script type="text/javascript" src="js/lib/jquery.json-2.2.js"></script>
		<script type="text/javascript" src="js/lib/jquery.resource.forms.js"></script>

        <!-- SCRIPTS pagina-->
        <script type="text/javascript" language="javascript" src="alterar_senha.js"></script> 
        <script type="text/javascript" language="javascript" src="comum_servico.js"></script> 

        <!-- ICONE FAVORITO -->
        <link rel="shortcut icon" href="./img/favicon.ico" />
    </head>
    <body>
        <div id="principal">
            <div id="main">
                <div id="cabecalho" >
                    <img alt="logo" id="logoLogin" src="img/lateralHeader.png"  />                               
<%--                    <div id="opcoesGlobais">
                        <span>Versão: </span><asp:Label ID="lblVersao" runat="server"></asp:Label>
                        <span>  [Ambiente: </span><asp:Label ID="lblAmbiente" runat="server" Text="" /><span>]</span><!-- INFORMANDO O AMBIENTE -->
                    </div>
--%>                </div>
                <div id="conteudo">
                    <div id="usuario_alterar_senha"  class="cmxform">
                        <asp:Label runat="server" ID="lblSenhaExpirada" ForeColor="Red"></asp:Label>
                        <br /><br /><br />
                        <fieldset>
                            <legend>Alterar Senha</legend>
                            <ol>
                                <li>
                                    <label>Senha Atual:</label>
                                    <input type="password" id="senhaAtual" value="" />
                                </li>
                                <li>
                                    <label>Nova Senha:</label>
                                    <input type="password" id="novaSenha" value="" />
                                </li>         
                                <li>
                                    <label>Confirmação da Nova Senha:</label>
                                    <input type="password" id="confirmacaoSenha" value="" />
                                </li>                              
                            </ol>
                        </fieldset>
        <%-- 
        //========================================================================================
        // 1415 PARAMETROS DE SEGURANÇA -  DiasExpiracaoSenha - TELA PEDINDO SENHA NOVA NA VALIDAÇÃO DE SENHA EXPIRADA ESTA COM OS BOTÕES SOBREPONDO OS CAMPOS
        //========================================================================================
        <div id="usuario_alterar" style="position:absolute; left:470px; top:290px;">
        --%>  
                        <div id="usuario_alterar" style="text-align: center;">
                            <button id="cmdAlterarSenha">Alterar</button>
                            <button id="cmdCancelarAlteracao">Cancelar</button>
                        </div>
                     </div>
<%-- 1415 - Retirado MasterPage 
</asp:Content>
 --%>
                </div>
                <div id="rodape_login">Desenvolvido por Resource IT Solutions - Versão 1.1.0.24</div>
            </div>
        </div>
     
        <!-- 1415 Adcionado Popups da masterpage-->   
        <!-- Popup carregando -->
        <div id="loadingDialog" style="display: none; text-align: center">
            <img src="img/carregando.gif" style="padding-top: 15px;" />
        </div>
        <!-- Popup genérico -->
        <div id="popupSimNao" style="display: none;">
	        <table border="0" cellpadding="0" cellspacing="0" width="100%">
		        <tr>
			        <td width="10%">
				        <img class="icone" src="#" />
			        </td>
			        <td width="1%"></td>
			        <td width="89%">
				        <span id="popupSimNao_mensagem" style="font-size: 12px; vertical-align: top;"></span>
			        </td>
		        </tr>
	        </table>
        </div>
    </body>
</html>
