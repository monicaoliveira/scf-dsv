﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.processo)
        $.processo = {};

    // Funcoes do namespace processo.lista
    $.processo.extrato = {

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Processos > Processo Extrato");

            // Layout dos botões
            $("#site_processo_extrato button").button();

            // Processamento
            $("#cmdProcessarExtrato").click(function(){
//=================================================================================================================== //
//1304 - inibindo
//=================================================================================================================== //
/*
                // Monta o request
                var request = {
                    ExecutarAssincrono: true,
                    Config: {
                        CaminhoArquivoTSYS: $("#txtArquivoTSYSDiretorio").val() != "" && $("#txtArquivoTSYSArquivo").val() != "" ? validarCamposDiretorio($("#txtArquivoTSYSDiretorio").val()) + $("#txtArquivoTSYSArquivo").val() : null,
                        CaminhoArquivoTSYSAtacadao: $("#txtArquivoTSYSDiretorioAtacadao").val() != "" && $("#txtArquivoTSYSArquivoAtacadao").val() != "" ? validarCamposDiretorio($("#txtArquivoTSYSDiretorioAtacadao").val()) + $("#txtArquivoTSYSArquivoAtacadao").val() : null,
                        CaminhoArquivoTSYSGaleria: $("#txtArquivoTSYSDiretorioGaleria").val() != "" && $("#txtArquivoTSYSArquivoGaleria").val() != "" ? validarCamposDiretorio($("#txtArquivoTSYSDiretorioGaleria").val()) + $("#txtArquivoTSYSArquivoGaleria").val() : null,
                        CaminhoArquivoCCI: $("#txtArquivoCCIDiretorio").val() != "" ? validarCamposDiretorio($("#txtArquivoCCIDiretorio").val()) : null,
                        CaminhoArquivoATA: $("#txtArquivoATADiretorio").val() != "" ? validarCamposDiretorio($("#txtArquivoATADiretorio").val()) : null,
                        CaminhoArquivoGAL: $("#txtArquivoGALDiretorio").val() != "" ? validarCamposDiretorio($("#txtArquivoGALDiretorio").val()) : null
                    }
                };

                // Pede a execucao do processo do extrato
                executarServico("ExecutarProcessoExtratoRequest", request, function(data) {

                    // Mostra popup de sucesso
                    popup({
                        titulo: "Processo Extrato",
                        icone: "sucesso",
                        mensagem: "Processo " + data.CodigoProcesso + " iniciado.",
                        callbackOK: function() { }
                    });
                });                    
            });                
*/                
//=================================================================================================================== //
//1304 - inibindo
//=================================================================================================================== //

//=================================================================================================================== //
//1304 - EXECUTANDO CARREFOUR
//=================================================================================================================== //

                if ( $("#txtArquivoTSYSDiretorio").val() != "" && $("#txtArquivoTSYSArquivo").val() != ""  &&
                     $("#txtArquivoTSYSDiretorio").val() != null && $("#txtArquivoTSYSArquivo").val() != null ) {
                    // Monta o request
                    var request = {
                        ExecutarAssincrono: true,
                        Config: {
                            CaminhoArquivoTSYS: $("#txtArquivoTSYSDiretorio").val() != "" && $("#txtArquivoTSYSArquivo").val() != "" ? validarCamposDiretorio($("#txtArquivoTSYSDiretorio").val()) + $("#txtArquivoTSYSArquivo").val() : null,
                            CaminhoArquivoTSYSAtacadao: null,
                            CaminhoArquivoTSYSGaleria: null,
                            CaminhoArquivoCCI: $("#txtArquivoCCIDiretorio").val() != "" ? validarCamposDiretorio($("#txtArquivoCCIDiretorio").val()) : null,
                            CaminhoArquivoATA: null,                        
                            CaminhoArquivoGAL: null
                        }
                    };

                    // Pede a execucao do processo do extrato
                    executarServico("ExecutarProcessoExtratoRequest", request, function(data) {      
                        // Mostra popup de sucesso
                        popup({
                            titulo: "Processo Extrato Carrefour",
                            icone: "sucesso",
                            mensagem: "Processo " + data.CodigoProcesso + " iniciado.",
                            callbackOK: function() { }
                        });
                    });                                        
                };                
//=================================================================================================================== //
//1304 - EXECUTANDO ATACADAO
//=================================================================================================================== //
                if ( $("#txtArquivoTSYSDiretorioAtacadao").val() != "" && $("#txtArquivoTSYSArquivoAtacadao").val() != ""  &&
                     $("#txtArquivoTSYSDiretorioAtacadao").val() != null && $("#txtArquivoTSYSArquivoAtacadao").val() != null ) {
                    // Monta o request
                    var request = {
                        ExecutarAssincrono: true,
                        Config: {
                            CaminhoArquivoTSYS: null,
                            CaminhoArquivoTSYSAtacadao: $("#txtArquivoTSYSDiretorioAtacadao").val() != "" && $("#txtArquivoTSYSArquivoAtacadao").val() != "" ? validarCamposDiretorio($("#txtArquivoTSYSDiretorioAtacadao").val()) + $("#txtArquivoTSYSArquivoAtacadao").val() : null,
                            CaminhoArquivoTSYSGaleria: null,
                            CaminhoArquivoCCI: null,
                            CaminhoArquivoATA: $("#txtArquivoATADiretorio").val() != "" ? validarCamposDiretorio($("#txtArquivoATADiretorio").val()) : null,
                            CaminhoArquivoGAL: null
                        }
                    };

                    // Pede a execucao do processo do extrato
                    executarServico("ExecutarProcessoExtratoRequest", request, function(data) {                
                        // Mostra popup de sucesso
                        popup({
                            titulo: "Processo Extrato Atacadao",
                            icone: "sucesso",
                            mensagem: "Processo " + data.CodigoProcesso + " iniciado.",
                            callbackOK: function() { }
                        });
                    });                                        
                };                
//=================================================================================================================== //
//1304 - EXECUTANDO GALERIA
//=================================================================================================================== //
                if ( $("#txtArquivoTSYSDiretorioGaleria").val() != "" && $("#txtArquivoTSYSArquivoGaleria").val() != ""  &&
                     $("#txtArquivoTSYSDiretorioGaleria").val() != null && $("#txtArquivoTSYSArquivoGaleria").val() != null ) {

                    // Monta o request
                    var request = {
                        ExecutarAssincrono: true,
                        Config: {
                            CaminhoArquivoTSYS: null,
                            CaminhoArquivoTSYSAtacadao: null,
                            CaminhoArquivoTSYSGaleria: $("#txtArquivoTSYSDiretorioGaleria").val() != "" && $("#txtArquivoTSYSArquivoGaleria").val() != "" ? validarCamposDiretorio($("#txtArquivoTSYSDiretorioGaleria").val()) + $("#txtArquivoTSYSArquivoGaleria").val() : null,
                            CaminhoArquivoCCI: null,
                            CaminhoArquivoATA: null,
                            CaminhoArquivoGAL: $("#txtArquivoGALDiretorio").val() != "" ? validarCamposDiretorio($("#txtArquivoGALDiretorio").val()) : null
                        }
                    };
                    
                    // Pede a execucao do processo do extrato
                    executarServico("ExecutarProcessoExtratoRequest", request, function(data) {        
                        // Mostra popup de sucesso
                        popup({
                            titulo: "Processo Extrato Galeria",
                            icone: "sucesso",
                            mensagem: "Processo " + data.CodigoProcesso + " iniciado.",
                            callbackOK: function() { }
                        });
                    });                                        
                };                
            });
            
            executarServico(
                "ReceberConfiguracaoGeralRequest", {},
                function(data) {

                    // 19/09/2011 Campidelli -> Não usamos mais este campo
                    $("#txtArquivoCCIArquivo").attr("disabled", true);

                    if (data.ConfiguracaoGeralInfo) {

                        if (data.ConfiguracaoGeralInfo.EnviarCessaoAutomatico == false) {
                            $("#txtArquivoCCIDiretorio").attr("disabled", true);
                        }
                        else {
                            $("#txtArquivoCCIDiretorio").attr("disabled", false);
                        }

                        if (data.ConfiguracaoGeralInfo.EnviarCessaoAutomaticoAtacadao == false) {
                            $("#txtArquivoATADiretorio").attr("disabled", true);
                        }
                        else {
                            $("#txtArquivoATADiretorio").attr("disabled", false);
                        }

                        if (data.ConfiguracaoGeralInfo.EnviarCessaoAutomaticoGaleria == false) {
                            $("#txtArquivoGALDiretorio").attr("disabled", true);
                        }
                        else {
                            $("#txtArquivoGALDiretorio").attr("disabled", false);
                        }

                        $("#txtArquivoTSYSDiretorio").val(data.ConfiguracaoGeralInfo.DiretorioImportacaoExtratoTSYSCarrefour);
                        $("#txtArquivoTSYSArquivo").val("TSYS_CARREFOUR.TXT");
                        $("#txtArquivoTSYSDiretorioAtacadao").val(data.ConfiguracaoGeralInfo.DiretorioImportacaoExtratoTSYSAtacadao);
                        $("#txtArquivoTSYSArquivoAtacadao").val("TSYS_ATACADAO.TXT");
                        $("#txtArquivoTSYSDiretorioGaleria").val(data.ConfiguracaoGeralInfo.DiretorioImportacaoExtratoTSYSGaleria);
                        $("#txtArquivoTSYSArquivoGaleria").val("TSYS_GALERIA.TXT");
                        $("#txtArquivoCCIDiretorio").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoCCI);
                        $("#txtArquivoATADiretorio").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoATA);
                        $("#txtArquivoGALDiretorio").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoGAL);
                        //$("#txtArquivoCCIArquivo").val("ExportacaoCCI.TXT");
                    }
                });
        }
    };

    // Dispara a carga
    $.processo.extrato.load();

    // Validar Campos de Diretorio (Valida string vazia e verifica "\" no final da string)
    function validarCamposDiretorio(campo) {

        var resultado = campo;
        if (resultado != "") {
            var contador = campo.length;
            while (campo.substring(contador - 1, contador) == "\\") {
                resultado = campo.substring(0, contador - 1);
                contador--;
            }
            resultado += "\\";
        } else {
            resultado = null;
        }
        return resultado;
    }


});