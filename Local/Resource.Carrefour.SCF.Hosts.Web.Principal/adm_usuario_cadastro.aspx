﻿<%@ Page Title="Administrativo - Cadastro de Usuários" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="adm_usuario_cadastro.js"></script>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
      
    <div id="usuario_lista">                
        <div class="lista">
          <table id="tbUsuarioLista"></table>
           <div class="botoes">
                <button id="cmdNovoUsuario">Novo</button>
                <button id="cmdEditarUsuario">Editar</button>
                <button id="cmdRemoverUsuario">Remover</button>
                <button id="cmdAtualizarLista">Atualizar Lista</button>
                <button id="cmdResetarSenha">Senha Reset</button>
            </div>
         </div>
    </div>
    
    <div id="usuario_edicao">
        <ul>
            <li><a href="#usuario_detalhe">Detalhe</a></li>
            <li><a href="#usuario_grupos">Grupos</a></li>
            <li><a href="#usuario_perfis">Perfis</a></li>
            <li><a href="#usuario_permissoes">Permissões</a></li>
        </ul>
        <div id="usuario_detalhe">
            <table cellpadding="5px">
               <tr>
                    <td>
                        <label>Login:</label>
                    </td>
                    <td>                        
                        <input type="text" id="txtLogin" value="" class="forms[property[Login]]" maxlength="9" /> <%-- 1436 na base é tamanho de 10 mas aceita até 9--%>
                    </td>                
<%-- 1436 INICIO --%> 
                    <td colspan=2>                        
                        <input type="checkbox" id="Checkbox1" value="" class="forms[property[Bloqueado]]" />
                        <label>Usuário bloqueado</label>                        
                    </td>                      
<%-- 1436 FIM --%> 
               </tr>
                <tr>                   
                    <td>
                        <label>Nome:</label>
                    </td>
                    <td colspan=3>
                        <input type="text" id="txtnome" value="" style="width: 100%" class="forms[property[Nome]]" maxlength="100" />
                    </td>
<%-- 1436 movi para linha do LOGIN 
                    <td>                        
                        <input type="checkbox" id="cbBloqueado" value="" class="forms[property[Bloqueado]]" />
                        <label>Usuário bloqueado</label>
                    </td>          
--%>                                                    
                </tr>
                <tr>                
                    <td>
                        <label>CPF:</label>
                    </td>
                    <td>                        
                        <input type="text" id="txtCpf" value="" class="forms[property[Cpf]]" maxlength="20" />
                    </td>                                                                                          
                </tr>
                <tr>
                    <td>
                        <label>Matrícula:</label>
                    </td>
                    <td>                        
                        <input type="text" id="txtMatricula" value="" class="forms[property[Matricula]]" maxlength="20" />
                    </td>                                                                             
               </tr>
               <tr>
                    <td>
                        <label>Bloqueado de: </label>
                    </td>
                    <td>                        
                        <input type="text" id="txtBloqueadoDe" value="" class="forms[property[DataBloqueadoInicio];dataType[date];maskType[date]]" /> 
                    </td>
                    <td style="text-align: right">                                              
                        <label>até:</label>
                    </td>
                    <td>                        
                        <input type="text" id="txtBloqueadoAte" value="" class="forms[property[DataBloqueadoFim];dataType[date];maskType[date]]" />
                    </td>                
               </tr>               
               <%-- 1436 inicio--%>
               <tr>
                    <td>                        
                        <label>Senha:</label>
                    </td>
                    <td>                        
                        <input type="password" id="txtSenha" value="" class="forms[property[Senha]]" /> <%-- 1414 retirando limitacao de 8 digitos --%>
                    </td>   
                    <td>
                        <label>Expira em:</label>
                    </td>
                    <td>                        
                        <input type="text" id="txtdataexpiracao" disabled value="" class="forms[property[DataExpiracaoSenha];maskType[date]]" /> 
                    </td>                       
               </tr>
               <tr>
                    <td colspan=4>                        
                        <label>ATENÇÃO: Alteração de SENHA exigirá troca no 1o acesso</label>
                    </td>
               </tr>
               <%-- 1436 fim--%>           

            </table>            
        </div>
        
        <div id="usuario_grupos">
            <table id="tbUsuarioGrupoLista"></table>
            <button id="cmdAdicionarGrupo">Adicionar</button>
            <button id="cmdRemoverGrupo">Remover</button>
        </div>
        
        <div id="usuario_perfis">
            <table id="tbUsuarioPerfilLista"></table>
            <button id="cmdAdicionarPerfil">Adicionar</button>
            <button id="cmdRemoverPerfil">Remover</button>
        </div>
        
        <div id="usuario_permissoes">
            <table id="tbUsuarioPermissaoLista"></table>
            <button id="cmdAdicionarPermissao">Adicionar</button>
            <button id="cmdRemoverPermissao">Remover</button>
        </div>     
    </div>
    
    <div id="permissao_lista">
        <div class="Lista">                     <!-- 1477 BORDA NO GRID -->
            <table id="tbPermissaoLista"></table>
            </br>                               <!-- 1477 PULANDO LINHAS -->
            </br>
            <label for="cmbTipoPermissao">Tipo da Permissão:</label>
            <select id="cmbTipoPermissao">
                <option value="0">Permitir</option>
                <option value="1">Negar</option>
            </select>
        </div>
    </div>
    
    <div id="perfil_lista">
        <div class="Lista">                     <!-- 1477 BORDA NO GRID -->
            <table id="tbPerfilLista"></table>
        </div>            
    </div>
    
    <div id="grupo_lista">
        <div class="Lista">                     <!-- 1477 BORDA NO GRID -->
            <table id="tbGrupoLista"></table>
        </div>            
    </div>
    
    <div id="confirmacaoExclusao">
        <span>Confirma a exclusão deste item?</span>
    </div>
    
    <div id="mensagemAlerta">
        <span id="textoAlerta"></span>
    </div>

    <div id="resetar_senha" class="cmxform">
        <fieldset>
            <ol>
                <li>
                    <label style=" font-size:large">RESET - Nova senha</label>
                </li>  
                <li>
                    <label>Exigirá troca de senha no 1o acesso do usuário</label>
                </li>  
                <li>
                    <input type="password" id="nova_senha" value=""/>
                </li>  
            </ol>
        </fieldset>
    </div>   
</asp:Content>
