﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorio_vencimento.aspx.cs"
    Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.relatorio_vencimento" EnableSessionState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Relatório de Vencimentos Bancários</title>
    <link href="relatorio_vencimento.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Arial,Times New Roman; font-size: 12px;">
    <form id="frm" runat="server">
        <header>
            <table style="border:solid 1px #4682B4; width:100%;">
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="4">
                                        <h1 style="font-size:14px;font-weight:bold;">CONFERÊNCIA DE VENCIMENTOS BANCÁRIOS - Consolidado</h1> 
                                    </td>
                                    <td style="text-align:right">
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Data/Hora: </label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblDataHora"></asp:Label>
                                    </td>                                        
                                </tr>
                            </table> 
                        </td>                                                                       
                    </tr>                        
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="5">
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Filtros</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Período transação:</label>                              
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataTransacaoInicio"></asp:Label> - 
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataTransacaoFim"></asp:Label>
                                    </td>  
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Período vencimento:</label>                              
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataVencimentoInicio"></asp:Label> - 
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataVencimentoFim"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Período processamento:</label>                              
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataMovimentoInicio"></asp:Label> - 
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataMovimentoFim"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Período agendamento:</label>                              
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataAgendamentoDe"></asp:Label> - 
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataAgendamentoAte"></asp:Label>
                                    </td>
                                    <td> <%--SCF1170--%>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito medio">Período Retorno CCI:</label>                              
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroRetornoCCIInicio"></asp:Label> - 
                                        <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroRetornoCCIFim"></asp:Label>                                    
                                    </td>                                        
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Grupo:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroGrupo"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">SubGrupo:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroSubGrupo"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Favorecido:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroFavorecido"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Produto:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroProduto"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Referência:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroReferencia"></asp:Label>
                                    </td>                                    
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Tipo de Registro:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblTipoRegistro"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Consolidar:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblConsolidar"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Status Cessão:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblStatusCessao"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Financeiro/Contábil:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFinanceiroContabil"></asp:Label>
                                    </td>
                                    <td>
                                        <label style="font-weight:bold; font-size:10px;" class="negrito pequeno">Retornar Transação Paga:</label> <asp:Label style="font-size:10px;" class="negrito pequeno" runat="server" ID="lblTransacaoPaga"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <label style="font-weight:bold; font-size:12px;" class="negrito medio-grande">Posição da Carteira em:</label> <asp:Label style="font-size:12px;" class="medio-grande" runat="server" ID="lblPosicaoCarteira"></asp:Label>
                                    </td>
                                </tr>
                            </table> 
                        </td>                                                                       
                    </tr>                                                                                                                                                                       
                </table> 
        </header>
    <table style="border: solid 1px #4682B4; width: 100%;">
        <tr runat="server" id="trMensagem" visible="false">
            <td colspan="8" class="grupo" style="padding-bottom: 10px;">
                <asp:Label Style="font-size: 10px;" class="pequeno" runat="server" ID="lblMensagem">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Repeater runat="server" ID="GrupoRepeater">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblGrupoRepeater"></asp:Label>
                        <table style="width: 100%;">
                            <tr>
                                <td colspan="8" class="grupo">
                                    <label style="font-weight: bold; font-size: 12px;" class="negrito medio">
                                        Grupo:
                                    </label>
                                    <label style="font-size: 10px;" class="pequeno">
                                        <%# Eval("AgrupamentoGrupo.CodigoGrupo")%>
                                        -
                                        <%# Eval("AgrupamentoGrupo.DescricaoGrupo")%></label>
                                </td>
                            </tr>
                            <%--ClockWork - Rogério--%>
                            <asp:Repeater runat="server" ID="ReferenciaRepeater">
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="8" style="height:10px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="border-bottom: solid 2px #4682B4">
                                        </td>
                                    </tr>
                                    <tr id="divQuebraReferencia" runat="server" visible="false" style="height: 16px">
                                        <td colspan="8">
                                            <label style="font-weight: bold; font-size: 11px;" class="negrito medio">
                                                Referência:
                                            </label>
                                            <label style="font-size: 10px;" class="pequeno">
                                                <%# Eval("AgrupamentoReferencia.CodigoReferencia") %>
                                                -
                                                <%# Eval("AgrupamentoReferencia.DescricaoReferencia") %>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr id="divQuebraLinhaReferencia" runat="server" visible="false">
                                        <td colspan="8" style="border-bottom: solid 1px #4682B4">
                                        </td>
                                    </tr>
                                    <asp:Repeater runat="server" ID="DataRepeater">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                        Data Agendamento:
                                                    </label>
                                                    <label style="font-size: 10px;" class="pequeno">
                                                        <%# Eval("AgrupamentoData.DataAgendamento")%>
                                                    </label>
                                                </td>
                                                <td colspan="7">
                                                    <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                        Data Vencimento:
                                                    </label>
                                                    <label style="font-size: 10px;" class="pequeno">
                                                        <%# Eval("AgrupamentoData.DataVencimento")%>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" style="border-bottom: solid 1px #4682B4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8">
                                                    <table style="width: 100%;">
                                                        <!-- Repeater Estabelecimento -->
                                                        <asp:Repeater runat="server" ID="BancoRepeater">
                                                            <ItemTemplate>
                                                                <tr style="font-size: 10px;" class="pequeno">
                                                                    <td>
                                                                        <label style="font-weight: bold;" class="negrito pequeno">
                                                                            Favorecido</label>
                                                                    </td>
                                                                    <td>
                                                                        <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                                            Produto</label>
                                                                    </td>
                                                                    <td>
                                                                        <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                                            Banco</label>
                                                                    </td>
                                                                    <td>
                                                                        <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                                            Agência</label>
                                                                    </td>
                                                                    <td>
                                                                        <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                                            Conta</label>
                                                                    </td>
                                                                    <td align="right">
                                                                        <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                                            Valor Líquido</label>
                                                                    </td>
                                                                    <td align="right">
                                                                        <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                                            Total Conta</label>
                                                                    </td>
                                                                    <td style="font-weight: bold; font-size: 10px;" align="right">
                                                                        <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                                                            Total Favorecido</label>
                                                                    </td>
                                                                </tr>
                                                                <!-- Repeater Estabelecimento -->
                                                                <asp:Repeater runat="server" ID="EstabelecimentoRepeater">
                                                                    <ItemTemplate>
                                                                        <tr style="font-size: 10px;" class="pequeno">
                                                                            <td style="width: 550px;">
                                                                                <asp:Label ID="lblEstabelecimentoCNPJ" runat="server" Text='<%# Eval("Estabelecimento.CNPJ")%>'></asp:Label>
                                                                                -
                                                                                <asp:Label ID="lblEstabelecimentoNome" runat="server" Text='<%# Eval("Estabelecimento.RazaoSocial")%>'></asp:Label>
                                                                            </td>
                                                                            <%--                                                        <td>
																		<asp:Label id="lblCodigoPagamento" runat="server" Text='<%# Convert.ToDecimal(Eval("CodigoPagamento")).ToString("N0")%>'></asp:Label> 
																	</td>                                                                                                                                             
			--%>
                                                                            <td style="width: 100px;">
                                                                                <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Eval("Produto.DescricaoProduto")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 60px;">
                                                                                <asp:Label ID="lblEstabelecimentoBanco" runat="server" Text='<%# Eval("Estabelecimento.Banco")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 80px;">
                                                                                <asp:Label ID="lblEstabelecimentoAgencia" runat="server" Text='<%# Eval("Estabelecimento.Agencia")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 100px;">
                                                                                <asp:Label ID="lblEstabelecimentoConta" runat="server" Text='<%# Eval("Estabelecimento.Conta")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 160px; text-align: right;">
                                                                                <asp:Label ID="lblEstabelecimentoValor" runat="server" Text='<%# Convert.ToDecimal(Eval("ValorLiquido")).ToString("N2")%>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 160px; text-align: right;">
                                                                                <asp:Label ID="lblTotalConta" runat="server" />
                                                                            </td>
                                                                            <td style="width: 160px; text-align: right;">
                                                                                <asp:Label ID="lblTotalFavorecido" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td colspan="7" style="text-align: right;">
                                                                        <label style="font-weight: bold; font-size: 10px; padding-top: 15px; margin-top: 15px;"
                                                                            class="negrito pequeno total">
                                                                            Total Data Agenda/Vencimento:
                                                                        </label>
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <label style="font-weight: bold; font-size: 10px; padding-top: 15px; margin-top: 15px;"
                                                                            class="negrito pequeno total">
                                                                            <%# Convert.ToDecimal(Eval("Total")).ToString("N2") %>
                                                                        </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="8" style="border-bottom: solid 1px #4682B4">
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr id="divQuebraReferenciaTotal" runat="server" visible="false">
                                        <td colspan="7" style="text-align: right;">
                                            <label style="font-weight: bold; font-size: 11px; padding-top: 15px; margin-top: 15px;"
                                                class="negrito medio total">
                                                Total Referência
                                                <%# Eval("AgrupamentoReferencia.CodigoReferencia") %>:
                                            </label>
                                        </td>
                                        <td style="text-align: right;">
                                            <label style="font-weight: bold; font-size: 11px; padding-top: 15px; margin-top: 15px;"
                                                class="negrito medio total">
                                                <%# Convert.ToDecimal(Eval("Total")).ToString("N2") %>
                                            </label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr>
                                <td colspan="7" style="text-align: right;"><br />
                                    <label style="font-weight: bold; font-size: 11px; padding-top: 15px; margin-top: 15px;"
                                        class="negrito grande total">
                                        Total Grupo:
                                        <%# Eval("AgrupamentoGrupo.CodigoGrupo") %>:
                                    </label>
                                </td>
                                <td style="text-align: right;"><br />
                                    <label style="font-weight: bold; font-size: 11px; padding-top: 15px; margin-top: 15px;"
                                        class="negrito grande total">
                                        <%# Convert.ToDecimal(Eval("Total")).ToString("N2")%>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
