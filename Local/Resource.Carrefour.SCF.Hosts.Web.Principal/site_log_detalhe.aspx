<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_log_detalhe.js"></script>
    <link href="site_log_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Log Detalhe</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- Conte�do da p�gina -->
    <div id="site_log_detalhe">
    
        <!-- Abas -->
        <ul>
            <li><a href="#logAbaDetalhe">Detalhe</a></li>
            <li><a href="#logAbaPropriedades">Todas as Propriedades</a></li>
        </ul>
    
        <!-- �rea de detalhe -->
        <div id="logAbaDetalhe" class="detalhe">
            <table>
                <tr><td>
                        <span>Codigo:</span>
                    </td><td>
                        <input type="text" class="forms[property[CodigoLog]]" readonly/>
                    </td><td>
                        <span>Tipo:</span>
                    </td><td>
                        <input type="text" class="forms[property[TipoLog]]" readonly />
                </td></tr><tr><td>
                        <span>Usuario:</span>
                    </td><td>
                        <input type="text" class="forms[property[CodigoUsuario]]" readonly />
                    </td><td>
                        <span>Data:</span>
                    </td><td>
                        <input type="text" class="forms[property[DataLog]]" readonly />
                </td></tr><tr><td>
                        <span>Descricao:</span>
                    </td><td colspan="3">
                        <textarea type="text" rows="10" class="forms[property[Descricao]]" style="width: 348px; " 
                            readonly></textarea>
                </td></tr>
            </table>
        </div>
    
        <!-- Container de propriedades -->
        <div id="logAbaPropriedades"></div>
    
    </div>

</asp:Content>
