﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="~/site_configuracao_geral.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_configuracao_geral" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_geral.js"></script>
    

    <link href="site_configuracao_geral.css" rel="stylesheet" type="text/css" />
    <title>SCF - Configurações Gerais</title>
    <style type="text/css">
        .style1
        {
            width: 199px;
        }
    </style>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    
    <div id="site_configuracao_geral">
    
        <!-- Abas do configuracao geral -->
        <div id="configuracao_geral_tabs">
            <ul>
                <!-- Abas -->  
                <li><a href="#tabProcessoAtacadao">Processo Atacadao</a></li>
                <li><a href="#tabProcessoExtrato">Processo Extrato</a></li>
                <li><a href="#tabExpurgo">Processo Expurgo</a></li>
                <li><a href="#tabRelatorio">Relatório</a></li>
            </ul>
            
            <!-- PROCESSO ATACADAO --> 
            <div id="tabProcessoAtacadao">
                <table width="100%">
                    <tr>
                        <td  colspan="4">PROCESSO ATACADAO</td>                
                    </tr>
                    <tr>
                        <td colspan="4"><span class="subtitulo">Diretórios de Importação</span></td>                
                    </tr>
                    <tr>
                        <td class="style1"><label>Importacao SITEF:</label></td>
                        <td colspan= "3"><input type="text" id="txtDiretorioImportacaoSITEF"  class="forms[property[DiretorioImportacaoAtacadaoSITEF]] diretorio" minlength="3" maxlength="231" /></td>
                    </tr>
                    <tr>
                        <td class="style1"><label>Importacao TSYS:</label></td>
                        <td colspan= "3"><input type="text" id="txtDiretorioImportacaoAtacadaoTSYS"  class="forms[property[DiretorioImportacaoAtacadaoTSYS]] diretorio" minlength="3" maxlength="231" /></td>
                    </tr>
                    <tr>
                        <td class="style1"><label>Importacao MA:</label></td>
                        <td colspan= "3"><input type="text" id="txtDiretorioImportacaoMA"  class="forms[property[DiretorioImportacaoAtacadaoMA]] diretorio" minlength="3" maxlength="231" /></td>
                    </tr>
                    <tr>
                        <td class="style1"><span class="subtitulo">Diretórios de Exportação</span></td>                
                    </tr>
                    <!--<tr>
                        <td class="style1"><label>Arquivo Matera Financeiro:</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoFinanceiro"  class="forms[property[DiretorioExportacaoMateraFinanceiro]] diretorio" minlength="3" maxlength="231" /></td>
                    </tr>
                    -->
                    <tr>
                        <td class="style1"><label>Arquivo Matera Contabil:</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoContabil" class="forms[property[DiretorioExportacaoMateraContabil]] diretorio"  minlength="3" maxlength="231"  /></td>
                    </tr>
                    <tr>
                        <td class="style1"><label>Resultado Conciliação:</label></td>
                        <td><input type="text" id="txtResultadoConciliacao" class="forms[property[DiretorioExportacaoAtacadaoConciliacao]] diretorio" minlength="3" maxlength="231" /></td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <label>CSU Sem Atacadao:</label>
                        </td>
                        <td>
                            <input type="text" id="txtDiretorioExportacaoCSUSem"  class="forms[property[DiretorioExportacaoAtacadaoCSUSemAtacadao]] diretorio" minlength="3" maxlength="231" />
                        </td>
                    </tr>
                </table>
                
                <table id="tableLeituraProcesso">    
                    <tr>
                        
                        <td>
                            <label id="labelLeituraProcesso">Leitura do Processo Atacadao: </label>
                        </td>
                        
                        <td>
                            <label class="hora" >de:</label>
                        </td>
                        <td><input type="text" id="txtHoraLeituraAtacadao_De" class="forms[property[HoraLeituraDeAtacadao]]" /></td>
                        <td >
                            <label class="hora" >até:</label>
                        </td>
                        <td><input type="text" id="txtHoraLeituraAtacadao_Ate" class="forms[property[HoraLeituraAteAtacadao]]" />
                        </td>
                        
                    </tr>
                 </table>
             </div>
            
             <!-- PROCESSO EXTRATO -->
             <div id="tabProcessoExtrato" >
                 <table>
                    <tr>    
<%--                        <td><!-- SCF 1473 - ID 18 -->
                            <label>Homologação TSYS:</label>
                        </td>
                        <td>
                            <select id="cmbHomologacaoTSYS" class="forms[property[HomologacaoTSYS];enableForPermissions[PermissaoAdministrador]]">
                                <option value="true">Sim</option>
                                <option value="false">Não</option>
                            </select>
                        </td>--%>
                        <td class="label">
                            <label id="labelCaptura" >Captura automática dos arquivos no horário: </label>
                        </td>
                        <td>
                            <label class="hora" >de:</label>
                        </td>
                        <td>
                            <input type="text" id="txtHoraLeituraExtrato_De" maxlength="5" class="forms[property[HoraLeituraDeTSYS]]"  />
                        </td>
                        <td >
                            <label class="hora" >até:</label>
                        </td>
                        <td>
                            <input type="text" id="txtHoraLeituraExtrato_Ate" maxlength="5" class="forms[property[HoraLeituraDeCCI]] " />
                        </td>
                    </tr>
                 </table>
                 <table>
                    <tr>
                        <td colspan="4"><span class="subtitulo">DIRETÓRIOS PROCESSO EXTRATO</span></td>                
                    </tr>
                    
                    <tr>
                        <td><span class="subtitulo">Importação TSYS</span></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Diretório Grupo Carrefour:</label>
                        </td>
                        <td>
                            <input type="text" id="txtDiretorioImportacaoExtratoTSYSCarrefour"  class="forms[property[DiretorioImportacaoExtratoTSYSCarrefour]] diretorio" minlength="3" maxlength="231" />
                        </td>
                        <td align="left">
                            <label style="white-space:nowrap;">Atualizar NSU Host já processado?</label>
                        </td>
                        <td>
                            <select id="cmdReprocessarNSUprocessado" class="forms[property[ReprocessarNSUProcessado]]">
                                <%--<option value="?">(selecione)</option>--%>
                                <option value="true">Sim</option>
                                <option value="false" selected>Não</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Diretório Grupo Atacadao:</label>
                        </td>
                        <td colspan="3">
                            <input type="text" id="txtDiretorioImportacaoExtratoAtacadao" class="forms[property[DiretorioImportacaoExtratoTSYSAtacadao]] diretorio" minlength="3" maxlength="231" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Diretório Grupo Galeria:</label>
                        </td>
                        <td colspan="3">
                            <input type="text" id="txtDiretorioImportacaoExtratoGaleria"  class="forms[property[DiretorioImportacaoExtratoTSYSGaleria]] diretorio" minlength="3" maxlength="231" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><span class="subtitulo">Exportação</span></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Diretório Grupo Carrefour:</label>
                        </td>
                        <td>
                            <input type="text" id="txtDiretorioExportacaoCCI" class="forms[property[DiretorioExportacaoCCI]] diretorio" minlength="3" maxlength="231"/>
                        </td>
                        <td align="left">
                            <label style="white-space:nowrap;">Gerar Arquivo Automaticamente?</label>
                        </td>
                        <td>
                            <select id="cmbEnviarCessao" class="forms[property[EnviarCessaoAutomatico]]">
                                <%--<option value="?">(selecione)</option>--%>
                                <option value="true" selected>Sim</option>
                                <option value="false">Não</option>
                            </select>
                        </td>
                        <td align="left" style="padding-left:7px;">
                            <label style="white-space:nowrap;">Layout</label>
                        </td>
                        <td>
                            <select id="layoutExportacaoCci" class="forms[property[LayoutExportacaoCCI]]">
                                <asp:Repeater runat="server" ID="rptCmbLayoutCCI">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoLayout") %>"><%# Eval("NomeLayout")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Diretório Grupo Atacadão:</label>
                        </td>
                        <td>
                            <input type="text" id="txtDiretorioExportacaoATA" class="forms[property[DiretorioExportacaoATA]] diretorio" minlength="3" maxlength="231"/>
                        </td>
                        <td align="left">
                            <label style="white-space:nowrap;">Gerar Arquivo Automaticamente?</label>
                        </td>
                        <td>
                            <select id="cmbEnviarCessaoAta" class="forms[property[EnviarCessaoAutomaticoAtacadao]]">
                                <%--<option value="?">(selecione)</option>--%>
                                <option value="true" selected>Sim</option>
                                <option value="false">Não</option>
                            </select>
                        </td>
                        <td align="left" style="padding-left:7px;">
                            <label style="white-space:nowrap;">Layout</label>
                        </td>
                        <td>
                            <select id="layoutExportacaoAta" class="forms[property[LayoutExportacaoATA]]">
                                <asp:Repeater runat="server" ID="rptCmbLayoutATA">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoLayout") %>"><%# Eval("NomeLayout")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Diretório Grupo Galeria:</label>
                        </td>
                        <td>
                            <input type="text" id="txtDiretorioExportacaoGAL" class="forms[property[DiretorioExportacaoGAL]] diretorio" minlength="3" maxlength="231"/>
                        </td>
                        <td align="left">
                            <label style="white-space:nowrap;">Gerar Arquivo Automaticamente?</label>
                        </td>
                        <td>
                            <select id="cmbEnviarCessaoGal" class="forms[property[EnviarCessaoAutomaticoGaleria]]">
                                <%--<option value="?">(selecione)</option>--%>
                                <option value="true" selected>Sim</option>
                                <option value="false">Não</option>
                            </select>
                        </td>
                        <td align="left" style="padding-left:7px;">
                            <label style="white-space:nowrap;">Layout</label>
                        </td>
                        <td>
                            <select id="layoutExportacaoGal" class="forms[property[LayoutExportacaoGAL]]">
                                <asp:Repeater runat="server" ID="rptCmbLayoutGAL">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoLayout") %>"><%# Eval("NomeLayout")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><span class="subtitulo">DIRETÓRIOS PROCESSO CESSÃO</span></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Diretório Importação Cessão:</label>
                        </td>
                        <td>
                            <input type="text" id="txtDiretorioImportacaoCessao" class="forms[property[DiretorioImportacaoCessao]] diretorio" minlength="3" maxlength="231"/>
                        </td>
                        <td align="left">
                            <label style="white-space:nowrap;">Atualizar NSU Host já cessionado?</label>
                        </td>
                        <td>
                            <select id="cmdReprocessarNSUcessionado" class="forms[property[ReprocessarNSUCessionado]]">
                                <%--<option value="?">(selecione)</option>--%>
                                <option value="true">Sim</option>
                                <option value="false" selected>Não</option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4"><span class="subtitulo">PROCESSO PAGAMENTO</span></td>
                    </tr>
                    <tr>
                        <td><label>Prazo de Pagamento:</label></td>
                        <td colspan="3" >
                            <input type="text" id="txtPrazoPagamento" class="forms[property[PrazoPagamento];dataType[integer];required]" maxlength="4" />
                            <label id="labelDiasVencidos">Dias Vencidos:</label>
                            <input type="text" id="txtDiasVencidos" class="forms[property[QuantidadeDiasVencidos];dataType[integer];required]" maxlength="4"/>
                        </td>
                    
                    </tr>
                    <!--
                    <tr>
                        <td>
                            <label>Diretorio de Exportacao Matera:</label>
                        </td>
                        <td>
                            <input type="text" id="txtDiretorioExportacaoMateraExtrato" class="forms[property[DiretorioExportacaoMateraExtrato]] diretorio" minlength="3" maxlength="231"/>
                        </td>
                        <td align="left">
                            <label>Gerar Matera Automaticamente?</label>
                        </td>
                        <td>
                            <select id="cmbEnviarMatera" class="forms[property[EnviarMateraFinanceiroAutomatico]]">
                                <%--<option value="?">(selecione)</option>--%>
                                <option value="true">Sim</option>
                                <option value="false">Não</option>
                            </select>
                        </td>
                        
                    </tr>
                    -->
                </table>
                
                
            </div>
            <!-- PROCESSO EXPURGO -->
            <div id="tabExpurgo" >                        
                        
                 <table>
                    <tr>
                        <td colspan="3"><span class="subtitulo">CONFIGURAÇÃO - PROCESSO EXPURGO TB_ARQUIVO_ITEM Datas previstas se a execução fosse hoje</span></td>                
                    </tr>
                    
                    <tr>
                        <td>
                            <label>Intervalo de execução de expurgo (dias):</label>
                        </td>
                        <td>
                            <input onblur="calcular();" type="text" id="diasAguardar" class="forms[property[DiasAguardarExpurgo];dataType[integer];required] " minlength="1" maxlength="3" />
                        </td>  
                        <td>
                            <label>Próxima execução:</label>
                            <input readonly="readonly" style="text-align: right" type="text" id="proxExecucao" class="forms[property[ProxExecucao]]" tabindex="-1"/>
                        </td>                                             
                    </tr>
                    <tr>
                        <td>
                            <label>Quantidade de dias a permanecer na base (dias):</label>
                        </td>
                        <td colspan="1">
                            <input onblur="calcular();" type="text" id="diasIgnorar" class="forms[property[DiasIgnorarExpurgo];dataType[integer];required] " minlength="1" maxlength="3" />
                        </td>
                        <td>
                            <label>Período que permanecerá na base: </label>
                            <label>De:</label><input readonly="readonly" style="text-align: right;" type="text" id="dePeriodo"  class="forms[property[DePeriodo]]" tabindex="-1"/>
                            <label>Até:</label><input readonly="readonly" style="text-align: right" type="text" id="atePeriodo" class="forms[property[AtePeriodo]]" tabindex="-1"/>
                        </td>                          
                    </tr>                                                           
                </table>                                
            </div>
            
            <!-- RELATORIOS --> 
            <div id="tabRelatorio">
                <table width="100%">
                    <tr>
                        <td colspan="2"><span class="subtitulo">Diretórios de Exportação</span></td>                
                    </tr>
                    <tr>
                        <td width="20%"><label>Relatório Transação:</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoRelatorioTransacao"  class="forms[property[DiretorioExportacaoRelatorioTransacao]] diretorio" minlength="3" maxlength="247" /></td>
                    </tr>
                    <tr>
                        <td width="20%"><label>Relatório Consolidado:</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoRelatorioConsolidado"  class="forms[property[DiretorioExportacaoRelatorioConsolidado]] diretorio" minlength="3" maxlength="247" /></td>
                    </tr>
                    <tr>
                        <td width="20%"><label>Relatório Venda:</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoRelatorioVenda"  class="forms[property[DiretorioExportacaoRelatorioVenda]] diretorio" minlength="3" maxlength="247" /></td>
                    </tr>
                    <tr>
                        <td width="20%"><label>Relatório Vencimento:</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoRelatorioVencimento"  class="forms[property[DiretorioExportacaoRelatorioVencimento]] diretorio" minlength="3" maxlength="247" /></td>
                    </tr>                                
                    <tr>
                        <td width="20%"><label>Relatório Saldo Conciliação:</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoRelatorioSaldoConciliacao"  class="forms[property[DiretorioExportacaoRelatorioSaldoConciliacao]] diretorio" minlength="3" maxlength="247" /></td>
                    </tr> 
                    <tr>
                        <td width="20%"><label>Relatório Recebimento:</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoRelatorioRecebimento"  class="forms[property[DiretorioExportacaoRelatorioRecebimento]] diretorio" minlength="3" maxlength="247" /></td>
                    </tr> 
                    <tr> <!--1465 -->
                        <td width="20%"><label>Relatório Cheques Devolvidos:</label></td>
                        <td><input type="text" id="Text1"  class="forms[property[DiretorioExportacaoRelatorioCheques]] diretorio" minlength="3" maxlength="247" /></td>
                    </tr>                    
                    <tr>
                        <td width="20%"><label>Arquivo de Pagamentos (Matera Financeiro):</label></td>
                        <td><input type="text" id="txtDiretorioExportacaoFinanceiro"  class="forms[property[DiretorioExportacaoMateraFinanceiro]] diretorio" minlength="3" maxlength="247" /></td>
                    </tr>
                                                  
                </table>               
             </div>            
        </div>        
        
        <div id="botaoSalvar">
            <button id="cmdSalvarConfiguracoes">Salvar</button>
        </div>
    </div>

</asp:Content>
