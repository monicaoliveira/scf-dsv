﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="site_arquivo_lista.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_arquivo_lista" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_arquivo_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_arquivo_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_arquivo_item_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_arquivo_lista.js" type="text/javascript"></script>
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_item_lista.js" type="text/javascript"></script>
    <script src="site_critica_lista.js" type="text/javascript"></script>
    <script src="site_critica_detalhe.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div id="site_arquivo_lista">
        
        <!-- Filtro -->
        <div class="filtro formulario"> 
            <table>
                <tr>
                    <td>
                        <label>Data Inclusão</label>
                    </td>
                    <td>
                        <input type="text" id="txtFiltroDataInclusao" class="forms[property[FiltroDataInclusao];dataType[date];maskType[date]]" />
                    </td>
                    
                    <td>
                        <label>Sts Arq</label>
                    </td>
                    <td>
                        <select id="cmbFiltroStatusArquivo" class="forms[property[FiltroStatusArquivo]]">
                            <option value="?">(selecione)</option>
                        </select>
                    </td>
                    
                    <td>
                        <button class="cmdFiltrar">Filtrar</button>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Lista -->
        <div class="lista">
            <table id="tbArquivo"></table>
            <div class="botoes">
            <%--<button class="cmdGerarArquivo">Gerar Arquivo</button>--%>
            <button class="cmdGerarCSUSemAtacadao">Gerar CSU Sem Atacadão</button>
            <button class="cmdGerarCCI">Exportar Arquivo</button>
            <%--<button class="cmdImportarArquivo">Importar Arquivo</button>--%>
            <%--<button class="cmdListarCriticas">Críticas</button>--%>
            <%--<button class="cmdDetalhe">Detalhe</button>--%>
            <%-- <button class="cmdSimularRetorno">Simular Retorno</button> --%> <%-- 1353 --%>
            <%--<button class="cmdValidar">Validar</button>--%>
            </div>
        </div>
        
        <div id="site_arquivo_info">
            <div id="divTipoArquivo">
                <label>Tipo Arquivo:</label>
                <select id="cmbTipoArquivo">
                    <option value="ArquivoCCI">ArquivoCCI</option>
                    <option value="ArquivoCSU">ArquivoCSU</option>
                    <option value="ArquivoSitef">ArquivoSitef</option>
                    <option value="ArquivoTSYS">ArquivoTSYS</option> 
                </select>
            </div>
            <label>Informe o diretorio a ser exportado: (apenas o diretorio )</label>
            <br />
            <input type="text" id="txtCaminhoArquivo" class="forms[property[CaminhoArquivo];required]" style="width: 360px;" />
        </div>
    </div>
    
</asp:Content>
