﻿$(document).ready(function() {

    // Define o namespace critica
    if (!$.bloqueio)
        $.bloqueio = {};

    // Define as funcoes do namespace critica.detalhe
    $.bloqueio.detalhe = {

        // ----------------------------------------------------------------------
        // load: garante que o HTML da esteja carregado e inicializado
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_bloqueio_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_bloqueio_detalhe.aspx #site_bloqueio_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_bloqueio_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.bloqueio.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();

                    });

                });

            } else {
                // Chama o callback
                if (callback)
                    callback();
            }

        },

        // ----------------------------------------------------------------------
        // Complementa a rotina de inicializacao especificamente na inicialização
        //   dos componentes da tela
        //
        //  callback:       função a ser chamada ao final da execução
        // ----------------------------------------------------------------------
        load2: function(callback) {

          
            // Inicializa o dialog
            $("#site_bloqueio_detalhe").dialog({
                autoOpen: false,
                height: 320,
                width: 460,
                modal: true,
                title: "Detalhe de Linha Arquivo",
                buttons: {
                    "Fechar": function() {
                        // Fecha o dialog
                        $("#site_bloqueio_detalhe").dialog("close");
                    }
                }
            });

            // Se tem callback, executa
            if (callback)
                callback();

        },

        // ----------------------------------------------------------------------
        // Abre o dialog de detalhe da critica carregando a critica informado,
        //   ou permitindo um novo cadastro (caso não seja informado um código existente)
        //
        //  codigoCritica:      código da critica a carregar. caso seja nulo,
        //                      abre a tela para inclusão de uma nova critica
        //  callback:           função a ser chamada ao final da execução
        // ----------------------------------------------------------------------
        abrir: function(arquivoItem, callback) {

            // Garante que o html está carregado
            $.bloqueio.detalhe.load(function() {

                // Carrega informações do critica
                $.bloqueio.detalhe.carregarDados(arquivoItem, function() {


                    // Mostra o dialog
                    $("#site_bloqueio_detalhe").dialog("open");

                    // Chama o callback
                    if (callback)
                        callback();

                });

            });

        },

        // ----------------------------------------------------------------------
        // Carrega as informações da tela
        //
        //  callback:       Função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        carregarDados: function(arquivoItem, callback) {

            var request = {};
            
           request.CodigoArquivo = arquivoItem.CodigoArquivo;
           request.CodigoArquivoItem = arquivoItem.CodigoArquivoItem;
           request.TipoLinha = arquivoItem.TipoArquivoItem;
           request.FiltrarComCritica = false;
           request.FiltrarSemCritica = true;
           request.IncluirValores = true;

            // Pede informações do critica
            executarServico(
                "ListarArquivoItemPorTipoLinhaRequest",
                request,
                function(data) {

                    // Preenche detalhe
                    $("#site_bloqueio_detalhe").Forms("setData", { dataObject: data.Resultado });

                    // Monta a lista de propriedades retornadas
                     var propriedades = "";
                    for (var i = 0; i < data.Cabecalho.length; i++) 
                        propriedades +=
                            "<label class='tituloArquivoLinhaPropriedade'>" + data.Cabecalho[i] + 
                            "</label></br /><label style='width: 250px;'>" + data.Resultado[0].Valores[i] + 
                            "</label><br /><br />";
                    $("#bloqueioPropriedades").html(propriedades);

                    // Chama o callback
                    if (callback)
                        callback();

                });

        }

    }

});