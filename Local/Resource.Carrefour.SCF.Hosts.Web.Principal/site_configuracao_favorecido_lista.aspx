﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_favorecido_lista.js"></script>
    <link href="site_configuracao_favorecido_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_configuracao_favorecido_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_configuracao_favorecido_detalhe.js" type="text/javascript"></script>
    <script src="site_configuracao_favorecido_lista.js" type="text/javascript"></script> 
    <script src="site_configuracao_conta_favorecido_detalhe.js" type="text/javascript"></script>  
                   
    <title>SCF - Cadastro de Favorecidos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">    
   
    <!-- ------------------------------------ -->
    <!--          Lista de Favorecidos        -->
    <!-- ------------------------------------ -->
    <div id="site_configuracao_favorecido_lista">
      <!-- Filtro -->
        <div class="filtro" class="lista formulario" style="margin-top:-200;">
            <table>
                <tr>
                    <td>
                        <label>CNPJ</label>
                    </td><td>
                        <input type="text" id="txtFiltroCNPJ" maxlength="15" class="forms[property[CnpjFavorecido]]" />
                    </td>
                    <td>
                        <button id="cmdFiltrar">Filtrar</button>
                    </td>
                </tr>
			    <tr>
				    <td>
					    <label id="avisoCarregar" style="color:red;">*Aguarde, efetuando consultado do favorecido...</label>
				    </td>
			    </tr>                
            </table>
         </div>
         
         <!--Lista-->
        <div id="lista" class="lista">
            <table id="tbFavorecido"></table>
            <div class="botoes">
                <button id="cmdNovoFavorecido">Novo</button>
                <button id="cmdEditarFavorecido">Editar</button>
                <button id="cmdRemoverFavorecido">Remover</button>
            </div>
        </div>        
    </div>

</asp:Content>
