﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" CodeBehind="site_relatorio_transacoes.aspx.cs" AutoEventWireup="true" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_relatorio_transacoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_relatorio_transacoes.js"></script>
    <link href="site_relatorio_transacoes.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="site_relatorio_transacoes">
        <div id="filtroDetalhe">
            <label class="tituloFiltro">Detalhes</label>
            
            <div class="borda">
                <ul>
                    <li class="label_detalhe1">
                        <label>Origem:</label>
                    </li>
                    
                    <li class="campo_detalhe1">
                        <select id="cmbOrigem" class="forms[property[FiltroOrigem]]">
                            <option value="?">Todos</option>
                            <asp:Repeater ID="rptCmbOrigem" runat="server">
                                <ItemTemplate>
                                    <option value="<%# Eval("Key") %>" ><%# Eval("Value") %></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>
                    
                    <li class="label_detalhe2">
                        <label>Meio de Captura:</label>
                    </li>
                    
                    <li class="campo_detalhe2">
                        <select id="cmbMeioCaptura" class="forms[property[FiltroMeioCaptura]]">
                            <option value="?">Todos</option>
                            <asp:Repeater runat="server" ID="rptCmbMeioCaptura">
                                <ItemTemplate>
                                    <option value="<%# Eval("Valor") %>"><%# Eval("Descricao") %></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>

                    <li class="label_detalhe3">
                        <label>Produto:</label>
                    </li>
                    
                    <li class="campo_detalhe3">
                        <select id="cmbProduto" class="forms[property[FiltroProduto]]">
                            <option value="?">Todos</option>
                            <asp:Repeater runat="server" ID="rptCmbProduto">
                                <ItemTemplate>
                                    <option value="<%# Eval("CodigoProduto") %>"><%# Eval("NomeProduto") %></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>
                    
                    <li class="label_detalhe3">
                        <label>Referência:</label>
                    </li>  

                    <li id="campo_detalhe3">
                        <select id="cmbReferencia" multiple="multiple" style="width:150px; height:15px; " class="ui-multiselect ui-state-focus ui-state-active forms[property[FiltroReferencia]]"></select>
                    </li>                                      


                </ul>
                
                <ul>
                    <li class="label_detalhe1">
                        <label>Grupo:</label>
                    </li>
                    
                    <li class="campo_detalhe1">
                        <select id="cmbEmpresaGrupo" class="forms[property[FiltroEmpresaGrupo]]">
                            <option value="?">Todos</option>
                            <asp:Repeater runat="server" ID="rptCmbEmpresaGrupo">
                                <ItemTemplate>
                                    <option value="<%# Eval("CodigoEmpresaGrupo") %>"><%# Eval("NomeEmpresaGrupo")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>

                    <li class="label_detalhe2">
                        <label>Subgrupo:</label>
                    </li>
                    
                    <li class="campo_detalhe2">
                        <select id="cmbEmpresaSubGrupo" class="forms[property[FiltroEmpresaSubGrupo]]">
                            <option value="?">Todos</option>
                            <asp:Repeater runat="server" ID="rptCmbEmpresaSubGrupo">
                                <ItemTemplate>
                                    <option value="<%# Eval("CodigoEmpresaSubGrupo") %>"><%# Eval("NomeEmpresaSubGrupo")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>

                    <li class="label_detalhe3">
                        <label>Tipo de Registro:</label>
                    </li>
                    
                    <li class="campo_detalhe3">
                        <select id="cmbTipoRegistro" class="forms[property[FiltroTipoRegistro]]">
                            <option value="?">Todos</option>
                            <asp:Repeater runat="server" ID="rptCmbTipoRegistro">
                                <ItemTemplate>
                                    <option value="<%# Eval("CodigoConfiguracaoTipoRegistro") %>"><%# Eval("TipoRegistro")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>
<!-- 1438 inicio -->
                    <li class="label_detalhe3">
                        <label>Conta Cliente:</label>
                    </li>
                    
                    <li class="campo_detalhe3">
                        <input type="text" id="txtContaCliente"  style="width:142px; height:15px;" class="forms[property[FiltroContaCliente];dataType[text]]"/>
                    </li>
<!-- 1438 fim -->                    
                </ul>
                
                <ul> 
                    <li class="label_detalhe1">
                        <label>Estabelecimento:</label>
                    </li>
                    
                    <li class="campo_detalhe1">
                        <select id="cmbEstabelecimento" class="forms[property[FiltroEstabelecimento]]">
                            <option value="?">Todos</option>
                            <asp:Repeater runat="server" ID="rptCmbEstabelecimento">
                                <ItemTemplate>
                                    <option value="<%# Eval("CodigoEstabelecimento") %>"><%# Eval("RazaoSocial")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>

                    <li class="label_detalhe2">
                        <label>Favorecido:</label>
                    </li>
                    
                    <li class="campo_detalhe2">
                        <select id="cmbFavorecido" class="forms[property[FiltroFavorecido]]">
                            <option value="?">Todos</option>
                            <asp:Repeater runat="server" ID="rptCmbFavorecido">
                                <ItemTemplate>
                                    <option value="<%# Eval("CodigoFavorecido") %>"><%# Eval("NomeFavorecido")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>

                    <li class="label_detalhe3">
                        <label>Plano:</label>
                    </li>
                    
                    <li class="campo_detalhe3">
                        <select id="cmbPlano" class="forms[property[FiltroPlano]]">
                            <option value="?">Todos</option>
                            <asp:Repeater runat="server" ID="rptCmbPlano">
                                <ItemTemplate>
                                    <option value="<%# Eval("CodigoPlano") %>"><%# Eval("CodigoPlanoTSYS") + " - " + Eval("DescricaoPlano")%></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>
                    
                    
                </ul>
                
                <ul>
                    <li class="label_detalhe1">
                        <label>Número do Pagamento:</label>
                    </li>
                    
                    <li class="campo_detalhe1">
                        <input type="text" id="txtNumeroPagamento"  style="width: 167px" class="forms[property[FiltroNumeroPagamento];dataType[integer]]"/>
                    </li>
                    
                    <li class="label_detalhe2">
                        <label>NSU Host:</label>
                    </li>
                    
                    <li class="campo_detalhe2">
                        <input type="text" id="txtNSUHost" maxlength="12" style="width:168px" class="forms[property[FiltroNSUHost]]"/>
                    </li>
                    
                    <li class="label_detalhe3">
                        <label>Financ./Contábil:</label>
                    </li>
                    <li class="campo_detalhe3">
                            <select id="cmbFinanceiroContabil" class="forms[property[FiltroFinanceiroContabil]]">
                                <option value="C">Contábil</option>
                                <option value="F">Financeiro</option>
                                <option value="?">Todos</option>
                            </select>
                    </li>
                    

                    <%--<li class="label_detalhe2">
                        <label>Tipo de Transação:</label>
                    </li>
                    <li class="campo_detalhe2">
                        <select id="cmbTipoTransacao" class="forms[property[]]">
                            <option value="?">TODOS</option>
                            <asp:Repeater runat="server" ID="rptCmbTipoTransacao">
                                <ItemTemplate>
                                    <option value="<%# Eval("CodigoTransacao") %>"><%# Eval("TipoTransacao") %></option>
                                </ItemTemplate>
                            </asp:Repeater>
                        </select>
                    </li>--%>
                </ul>
            </div>
        </div>
        
        <div id="filtroPeriodo">
            <label class="tituloFiltro">Períodos</label>
            
            <div class="borda">
                <ul>
                    <li class="label_Data">
                        <label>Data da Transação:</label>
                    </li>
                    
                    <li>
                        <ul>
                            <li class="label_data">
                                <label>De:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataTransacaoDe" class="forms[property[FiltroDataTransacaoDe];dataType[date];maskType[date]]" />
                            </li>
                            
                            <li class="label_data">
                                <label>Até:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataTransacaoAte" class="forms[property[FiltroDataTransacaoAte];dataType[date];maskType[date]]" />
                            </li>
                        </ul>
                    </li>
                    <li class="label_Data2">
                        <label>Data do Vencimento:</label>
                    </li>                    
                    <li>
                        <ul>
                            <li class="label_data">
                                <label>De:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataVencimentoDe" class="forms[property[FiltroDataVencimentoDe];dataType[date];maskType[date]]" />
                            </li>
                            
                            <li class="label_data">
                                <label>Até:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataVencimentoAte" class="forms[property[FiltroDataVencimentoAte];dataType[date];maskType[date]]" />
                            </li>
                        </ul>
                    </li>
                </ul>                
                <ul>                    
                </ul>                
                <ul>
                    <li class="label_Data">
                        <label>Data do Agendamento:</label>
                    </li>                    
                    <li>
                        <ul>
                            <li class="label_data">
                                <label>De:</label>
                            </li>                            
                            <li class="campo_data">
                                <input type="text" id="txtDataAgendamentoDe" class="forms[property[FiltroDataAgendamentoDe];dataType[date];maskType[date]]" />
                            </li>                            
                            <li class="label_data">
                                <label>Até:</label>
                            </li>                            
                            <li class="campo_data">
                                <input type="text" id="txtDataAgendamentoAte" class="forms[property[FiltroDataAgendamentoAte];dataType[date];maskType[date]]" />
                            </li>
                        </ul>
                    </li>
                    <li class="label_Data2">
                        <label>Data da Liquidação:</label>
                    </li>
                    
                    <li>
                        <ul>
                            <li class="label_data">
                                <label>De:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataLiquidacaoDe" class="forms[property[FiltroDataLiquidacaoDe];dataType[date];maskType[date]]" />
                            </li>
                            
                            <li class="label_data">
                                <label>Até:</label>
                            </li>                            
                            <li class="campo_data">
                                <input type="text" id="txtDataLiquidacaoAte" class="forms[property[FiltroDataLiquidacaoAte];dataType[date];maskType[date]]" />
                            </li>
                        </ul>
                    </li>
                </ul>                
                <ul>                    
                </ul>                
                <ul>
                    <li  class="label_Data">
                        <label>Data do Envio para Matera:</label>
                    </li>
                    
                    <li>
                        <ul>
                            <li class="label_data">
                                <label>De:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataEnvioMateraDe" class="forms[property[FiltroDataEnvioMateraDe];dataType[date];maskType[date]]" disabled="disabled" />
                            </li>
                            
                            <li class="label_data">
                                <label>Até:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataEnvioMateraAte" class="forms[property[FiltroDataEnvioMateraAte];dataType[date];maskType[date]]" disabled="disabled" />
                            </li>
                        </ul>
                    </li>
                    <li class="label_Data2">
                        <label>Data de Processamento:</label>
                    </li>
                    
                    <li>
                        <ul id="site_divergencia_interchange" >
                            <li class="label_data">
                                <label>De:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataProcessamentoDe" class="forms[property[FiltroDataMovimentoDe];dataType[date];maskType[date]]" />
                            </li>
                            
                            <li class="label_data">
                                <label>Até:</label>
                            </li>                            
                            <li class="campo_data">
                                <input type="text" id="txtDataProcessamentoAte" class="forms[property[FiltroDataMovimentoAte];dataType[date];maskType[date]]" />
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li  class="label_Data">
                        <label>Data de Retorno CCI:</label>
                    </li>
                    
                    <li>
                        <ul>
                            <li class="label_data">
                                <label>De:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataRetornoCessaoDe" class="forms[property[FiltroDataRetornoCessaoDe];dataType[date];maskType[date]]" />
                            </li>
                            
                            <li class="label_data">
                                <label>Até:</label>
                            </li>
                            
                            <li class="campo_data">
                                <input type="text" id="txtDataRetornoCessaoAte" class="forms[property[FiltroDataRetornoCessaoAte];dataType[date];maskType[date]]" />
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        
        <div id="filtroStatus">
            <label class="tituloFiltro">Status</label>
            
            <div class="borda">
                <ul>
                    <li class="label_status1">
                        <label>Status Retorno:</label>
                    </li>
                    
                    <li class="campo_status1">
                        <select id="cmbStatusRetorno" class="forms[property[FiltroStatusRetorno]]">
                            <option value="?">Todos</option>
                            <option value="1">Retornado OK</option>
                            <option value="0">Transação não Retornada</option>
                            <option value="3">Produto Não Cedível</option>
                            <option value="4">Transação já Liquidada</option>
                            <option value="2">Retorno Não Identificado</option>
                        </select>
                    </li>
                    
                    <li class="label_status2">
                        <label>Status Pagamento:</label>
                    </li>
                
                    <li class="campo_status2">
                        <select id="cmbStatusPagamento" class="forms[property[FiltroStatusPagamento]]">
                            <option value="?">Todos</option>
                            <option value="0">Pendente</option>
                            <option value="1">Efetuado</option>
                            <option value="9">Cancelado</option>
                        </select>
                    </li>

                    <li class="label_statusTran">
                        <label>Status Transação:</label>
                    </li>
                    <li class="campo_statusTran">
                        <select id="cmbStatusTransacao" multiple="multiple" style="width:100px;" class="ui-multiselect ui-state-focus ui-state-active"></select>
                    </li>
                </ul>
                
                <ul>
                    <li class="label_status1">
                        <label>Status Conciliação:</label>
                    </li>
                    
                    <li class="campo_status1">
                        <select id="cmbStatusConciliacao" class="forms[property[FiltroStatusConciliacao]]">
                            <option value="?">Todos</option>
                            <option value="1">Conciliado</option>
                            <option value="0">Não Conciliado</option>
                        </select>
                    </li>
                    
                    <li class="label_status2">
                        <label>Status Validação:</label>
                    </li>
                    
                    <li class="campo_status2">
                        <select id="cmbStatusValidacao" class="forms[property[FiltroStatusValidacao]]" disabled="disabled">
                            <option value="?">Todos</option>
                            <option value="1">Correta</option>
                            <option value="2">Bloqueada</option>
                            <option value="3">Excluida</option>
                            <option value="4">Inconsistente</option>
                            <option value="5">Inconsistente liberada</option>                                  
                        </select>
                    </li>

                </ul>
                
            </div>
        </div>
        <div class="teste">
            <input type="hidden" id="ObjetoJson" value="" class="ObjetoJson" runat="server" />
            <button id="btnGerarInterchange" title="O botão considera intervalo do filtro DATA DE PROCESSAMENTO">Divergencia Interchange</button>
            <button id="btnVisualizarRelatorio">Visualizar</button>
            <!--<button id="btnVisualizarRelatorioInconsistente">Visualizar Inconsistente</button> -->
            <!--<button id="btnGerarRelatorioPDF">Gerar Relatório PDF</button> -->
            <button id="btnGerarRelatorioExcel">Gerar Excel</button>            
        </div>
    </div>
</asp:Content>