﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    /// <summary>
    /// Configuracoes web
    /// </summary>
    public class WebConfig
    {
        /// <summary>
        /// Indica se deve utilizar os serviços locais, ou seja, 
        /// inicializar o framework na web
        /// </summary>
        public bool UtilizarServicosLocais { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public WebConfig()
        {
            this.UtilizarServicosLocais = true;
        }
    }
}
