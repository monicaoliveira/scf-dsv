﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.processo)
        $.processo = {};
    
    // Funcoes do namespace processo.lista
    $.processo.cessao = {
        
        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {
        
            // Insere o subtítulo
            $("#subtitulo").html("Processos > Processo Cessão");
            
            // Layout dos botões
            $("#site_processo_cessao button").button();
            
            // Processamento
            $("#cmdProcessarCessao").click(function() {
            
                // Monta o request
                var request = {
                    ExecutarAssincrono: true,
                    Config: {
                        CaminhoArquivoRetornoCessao: $("#txtArquivoRetCessaoDiretorio").val() != "" ? validarCamposDiretorio($("#txtArquivoRetCessaoDiretorio").val())+ $("#txtArquivoRetCessao").val() : null
                    }
                };
                
                // Pede a execucao do processo do cessao
                executarServico("ExecutarProcessoCessaoRequest", request, function(data) {

                    // Mostra popup de sucesso
                    popup({
                        titulo: "Processo Cessão",
                        icone: "sucesso",
                        mensagem: "Processo " + data.CodigoProcesso + " iniciado.",
                        callbackOK: function() {}
                    });
                });
            });
            
            /*
            // Pede informações do config
            executarServico("ReceberProcessoCessaoAutomaticoConfigRequest", {}, function(data) {
                        $("#txtArquivoRetCessaoDiretorio").val(data.Config.CaminhoArquivoRetornoCessao);
                });
            */

            executarServico(
                "ReceberConfiguracaoGeralRequest", {},
                function(data) {
                    if (data.ConfiguracaoGeralInfo) {
                        $("#txtArquivoRetCessaoDiretorio").val(data.ConfiguracaoGeralInfo.DiretorioImportacaoCessao);
                        $("#txtArquivoRetCessao").val("retornoCessao.txt");
                    }
                });
        }
     
    };

    // Dispara a carga
    $.processo.cessao.load();

    // Validar Campos de Diretorio (Valida string vazia e verifica "\" no final da string)
    function validarCamposDiretorio(campo) {

        var resultado = campo;
        if (resultado != "") {
            var contador = campo.length;
            while (campo.substring(contador - 1, contador) == "\\") {
                resultado = campo.substring(0, contador - 1);
                contador--;
            }
            resultado += "\\";
        } else {
            resultado = null;
        }
        return resultado;
    }

});