﻿$(document).ready(function() {

    // Namespace
    if (!$.subgrupo)
        $.subgrupo = {};

    // Funcoes do namespace 
    $.subgrupo.detalhe = {

        variaveis: {},

        //=================================================================================
        // load: funcoes de inicializacao da tela
        //=================================================================================
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_subgrupo_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_configuracao_subgrupo_detalhe.aspx #site_subgrupo_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_subgrupo_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.subgrupo.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();
                    });
                });

            } else {

                // Chama o callback
                if (callback)
                    callback();
            }
        },

        //=================================================================================
        // load2: funcoes de inicializacao da tela
        //=================================================================================
        load2: function(callback) {

            //------------------------------------------------------------------
            // Estilo dos botões
            //-------------------------------------------------------------------
            $("#site_subgrupo_detalhe button").button();

            //-------------------------------------------------------------------
            // Inicializa forms
            //-------------------------------------------------------------------
            $("#site_subgrupo_detalhe").Forms("initializeFields");

            //-------------------------------------------------------------------
            // Inicializa dialog de detalhe
            //-------------------------------------------------------------------
            $("#site_subgrupo_detalhe").dialog({
                autoOpen: false,
                height: 150,
                width: 500,
                modal: true,
                title: "Detalhe do Subgrupo",
                buttons: {
                    "Salvar": function() {

                        // Pede para salvar
                        $.subgrupo.detalhe.salvar(function() {

                            // Mostra popup de sucesso
                            popup({
                                titulo: "Salvar Subgrupo",
                                icone: "sucesso",
                                mensagem: "Subgrupo salvo com sucesso!",
                                callbackOK: function() {
                                    $.subgrupo.lista.listar();
                                    // Fecha dialog de detalhe
                                    $("#site_subgrupo_detalhe").dialog("close");
                                }
                            });
                        });
                    },
                    "Fechar": function() {

                        // Fecha o dialog
                        $("#site_subgrupo_detalhe").dialog("close");
                    }
                }
            });

            // Chama o callback
            if (callback)
                callback();
        },

        //=================================================================================
        // abrir: funcoes executadas na abertura da tela
        //=================================================================================
        abrir: function(codigoEmpresaSubGrupo, callback) {

            // Garante que o html está carregado
            $.subgrupo.detalhe.load(function() {

                if (codigoEmpresaSubGrupo) {

                    // Guarda código do subgrupo
                    $.subgrupo.detalhe.variaveis.codigoEmpresaSubGrupo = codigoEmpresaSubGrupo;

                    // Limpa campos
                    $("#site_subgrupo_detalhe").Forms("clearData");

                    // Carrega dados do subgrupo existente
                    $.subgrupo.detalhe.carregar(codigoEmpresaSubGrupo, function() {

                        // Abre dialog
                        $("#site_subgrupo_detalhe").dialog("open");
                    });

                } else {

                    // Guarda código do subgrupo
                    $.subgrupo.detalhe.variaveis.codigoEmpresaSubGrupo = null;

                    // Limpa campos
                    $("#site_subgrupo_detalhe").Forms("clearData");

                    // Abre dialog de detalhe
                    $("#site_subgrupo_detalhe").dialog("open");
                };

                // Chama o callback
                if (callback)
                    callback();
            });
        },

        //=================================================================================
        // carregar: Carrega dados referentes ao subgrupo
        //=================================================================================
        carregar: function(codigoEmpresaSubGrupo, callback) {

            executarServico(
            "ReceberEmpresaSubGrupoRequest",
            { CodigoEmpresaSubGrupo: codigoEmpresaSubGrupo },
            function(data) {

                // Guarda dados do subgrupo recebido
                $.subgrupo.detalhe.variaveis.empresaSubgrupoInfo = data.EmpresaSubGrupoInfo;

                // Preenche os campos do detalhe
                $("#site_subgrupo_detalhe").Forms("setData", { dataObject: data.EmpresaSubGrupoInfo });

                // Chama callback
                if (callback)
                    callback();
            });
        },

        //=================================================================================
        // salvar: Trata inserção ou alteração de um subgrupo
        //=================================================================================
        salvar: function(callback) {
            $.subgrupo.detalhe.validar(function() {
                if ($.subgrupo.detalhe.variaveis.codigoEmpresaSubGrupo) {

                    // Recebe empresaSubGrupoInfo
                    var empresaSubGrupoInfo = $.subgrupo.detalhe.variaveis.empresaSubgrupoInfo;

                    // Envia informações ao objeto existente
                    $("#site_subgrupo_detalhe").Forms("getData", { dataObject: empresaSubGrupoInfo });
                    
                    // Limpa temp
                    $.subgrupo.detalhe.variaveis.codigoEmpresaSubGrupo = null;

                } else {

                    // Cria novo objeto
                    empresaSubGrupoInfo = {};

                    // Envia informações ao novo objeto
                    $("#site_subgrupo_detalhe").Forms("getData", { dataObject: empresaSubGrupoInfo });
                }

                executarServico(
                "SalvarEmpresaSubGrupoRequest",
                { EmpresaSubGrupoInfo: empresaSubGrupoInfo },
                function(data) {

                    // Chama os callbacks
                    if (callback)
                        callback();
                });
            }, $.subgrupo.detalhe.retornarTrue);
        },

        // --------------------------------------------------------------------
        //  validar: valida campos necessários do cadastro de planos
        // --------------------------------------------------------------------
        validar: function(callbackOk, callbackErro) {



            // Inicializa criticas
            var criticas = new Array();

            // Pede a validação para o forms
            criticas = $("#site_subgrupo_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });

            // Se tem criticas, mostra o formulario
            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                if (callbackErro)
                    callbackErro();

            } else if (callbackOk)
                callbackOk();
        },
        
        //=================================================================================
        // remover: Trata remoção de um subgrupo
        //=================================================================================
        remover: function(codigoEmpresaSubGrupo, callback) {

            if (codigoEmpresaSubGrupo) {

                // Pede para remover subgrupo
                executarServico(
                "RemoverEmpresaSubGrupoRequest",
                { CodigoEmpresaSubGrupo: codigoEmpresaSubGrupo },
                function(data) {

                    // Chama o callback
                    if (callback)
                        callback();
                });
            }

            else {

                // Pede para selecionar uma linha
                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Favor selecionar uma linha!",
                    callbackOK: function() { }
                });
            }
        }
    };
});