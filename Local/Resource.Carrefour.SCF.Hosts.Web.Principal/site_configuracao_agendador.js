﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.agendador) $.agendador = {};

    // Funcoes do namespace processo.lista
    $.agendador = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("ADMINISTRATIVO [Captura Arquivos]");

            // Layout dos botões
            $("#site_configuracao_agendador button").button();

            // Inicializa forms
            $("#site_configuracao_agendador").Forms("initializeFields");

            // Iniciar servico
            $("#cmdIniciar").click(function() {
                $.agendador.iniciarServico(
                    function() {
                        $.agendador.carregar()
                    })
            });

            // Parar servico
            $("#cmdParar").click(function() {
                $.agendador.pararServico(
                    function() {
                        $.agendador.carregar()
                    })
            });

            // BOTÃO SALVAR
            $("#cmdAtualizar").click(function() { $.agendador.carregar() });



//            // Iniciar servico
//            $("#cmdExpurgoAgora").click(function() {
//                // Monta o request
//                var request = {
//                    ExecutarAssincrono: true,
//                    ExecutarAgora: true
//                };

//                // Pede a execucao do processo do extrato
//                executarServico("ExecutarProcessoExpurgoRequest", request, function(data) {

//                    // Mostra popup de sucesso
//                    popup({
//                        titulo: "Processo Expurgo",
//                        icone: "sucesso",
//                        mensagem: "Processo " + data.CodigoProcesso + " iniciado.",
//                        callbackOK: function() { }
//                    });

//                });
//            });

//            // Iniciar servico
//            $("#cmdIniciarExpurgo").click(function() {
//                $.agendador.iniciarServicoExpurgo(
//                    function() {
//                        $.agendador.carregar()
//                    })
//            });

//            // Parar servico
//            $("#cmdPararExpurgo").click(function() {
//                $.agendador.pararServicoExpurgo(
//                    function() {
//                        $.agendador.carregar()
//                    })
//            });

//            // BOTÃO SALVAR
//            $("#cmdAtualizarExpurgo").click(function() { $.agendador.carregar() });


            // Limpa campos
            $("#site_configuracao_agendador").Forms("clearData");

            // Carrega as configurações existentes
            $.agendador.carregar();
        },

        // ----------------------------------------------------------------------
        // carregar: Carrega as configurações realizadas anteriormente
        // ----------------------------------------------------------------------
        carregar: function(callback) {

            executarServico(
                "ReceberStatusServicoAgendadorRequest",
                {},
                function(data) {

                    // Guarda configuração recebida
                    $.agendador.variaveis.servicoAgendadorInfo = data.ServicoAgendadorInfo;

                    // Preenche os campos da tela
                    $("#site_configuracao_agendador").Forms("setData", { dataObject: data.ServicoAgendadorInfo });

//                    executarServico(
//                        "ReceberStatusServicoExpurgoRequest",
//                        {},
//                        function(data) {

//                            // Guarda configuração recebida
//                            $.agendador.variaveis.servicoAgendadorInfo = data.ServicoAgendadorInfo;

//                            // Preenche os campos da tela
//                            $("#expurgo_detalhe").Forms("setData", { dataObject: data.ServicoExpurgoInfo });

//                            // Chama callback
//                            if (callback)
//                                callback();
//                        });


                    // Chama callback
                    if (callback)
                        callback();
                });
        },


        iniciarServico: function(callback) {
            //SCF1130 - Marcos Matsuoka - Logar ao clicar na função INICIAR
            // Monta o request
            var request = {
                CodigoUsuario: $("#ctl00_usuario").text(),
                Descricao: "Iniciando captura de arquivos – botão INICIAR clicado",
                DataLog: new Date
            };
            // Pede a execucao do processo do atacadao
            executarServico("SalvarLogRequest", request,
                function(data) {
                    if (data) {
                        // Pede a execucao do processo do atacadao
                        executarServico("IniciarServicoAgendadorRequest", {},
                            function(data) {

                                // Chama callback
                                if (callback)
                                    callback();
                            });
                    }
                });            

            // Chama callback
            if (callback)
                callback();


        },

//        iniciarServicoExpurgo: function(callback) {

//            // Pede a execucao do processo do atacadao
//            executarServico("IniciarServicoExpurgoRequest", {},
//                function(data) {

//                    // Chama callback
//                    if (callback)
//                        callback();
//                });

//            // Chama callback
//            if (callback)
//                callback();


//        },

//        pararServicoExpurgo: function(callback) {

//            // Pede a execucao do processo do atacadao
//            executarServico("PararServicoExpurgoRequest", {},
//                function(data) {

//                    // Chama callback
//                    if (callback)
//                        callback();
//                });
//        },

        pararServico: function(callback) {

            //SCF1130 - Marcos Matsuoka - Logar ao clicar na função PARAR
            // Monta o request
            var request = {
                CodigoUsuario: $("#ctl00_usuario").text(),
                Descricao: "Iniciando captura de arquivos – botão PARAR clicado",
                DataLog: new Date
            };
            // Pede a execucao do processo do atacadao
            executarServico("SalvarLogRequest", request,
                function(data) {
                    if (data) {
                        // Pede a execucao do processo do atacadao
                        executarServico("PararServicoAgendadorRequest", {},
                            function(data) {

                                // Chama callback
                                if (callback)
                                    callback();
                            });
                    }
                });            

            // Chama callback
            if (callback)
                callback();


        },




    }

    // Dispara a inicialização
    $.agendador.load();

});