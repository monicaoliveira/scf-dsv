﻿$(document).ready(function() {

    // Define o namespace dominio
    if (!$.log)
        $.log = {};

    // Define as funcoes do namespace log.lista
    $.log.relatorio = {

        // Define a area de variaveis
        variaveis: {},

        // Abre um dialog com a lista de logs solicitada
        abrir: function(param, callback) {

            // Garante que a tela está carregada
            $.log.relatorio.load(function() {

                // Inicializa o dialog
                $("#site_log_lista_relatorio").dialog({
                    autoOpen: false,
                    height: 420,
                    width: 950,
                    modal: true,
                    title: "Lista de log",
                    buttons: {
                        "Fechar": function() {

                            // Fecha o dialog
                            $("#site_log_lista_relatorio").dialog("close");
                            $.log.relatorio.listar({ SetTimeOut: false });
                        }
                    },
                    resize: function() {
                        $("#tbLogRelatorio").setGridWidth($("#site_log_lista_relatorio").width() - 10);
                        $("#tbLogRelatorio").setGridHeight($("#site_log_lista_relatorio").height() - 60);
                    }
                });

                // Limpa campos do detalhe do log
                $("#tbLogRelatorio").clearGridData();
                $("#site_log_lista_relatorio").Forms("clearData");

                // Carrega a lista
                $.log.relatorio.paramLista = param;
                $.log.relatorio.listar({ SetTimeOut: true });

                // Mostra o dialog
                $("#site_log_lista_relatorio").dialog("open");

                // Monta o header
                $("#painelLogLabel .labelRelatorio").text(param.Descricao);

                // Faz o primeiro resize
                $("#tbLogRelatorio").setGridWidth($("#site_log_lista_relatorio").width() - 10);
                $("#tbLogRelatorio").setGridHeight($("#site_log_lista_relatorio").height() - 60);

                // Se tem, chama o callback
                if (callback)
                    callback();
            });
        },

        // ----------------------------------------------------------------------
        // load: garante que o HTML da esteja carregado e inicializado
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_log_lista_relatorio").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_log_lista_relatorio.aspx #site_log_lista_relatorio", function() {

                    // Copia o elemento para a área comum
                    $("#site_log_lista_relatorio").appendTo("#areaComum");

                    // Completa o load
                    $.log.relatorio.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();
                    });
                });

            } else {
                // Chama o callback
                if (callback)
                    callback();
            }
        },

        // ----------------------------------------------------------------------
        // load2: garante que o HTML da esteja carregado e inicializado
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        load2: function(callback) {

            // Inicializa forms
            $("#site_log_lista_relatorio").Forms("initializeFields");

            // Insere o subtítulo
            $("#subtitulo").html("Administrativo > Logs");

            // Layout dos botões
            $("#site_log_lista_relatorio button").button();

            //            // Trata detalhe do log
            //            $("#site_log_lista .cmdDetalhe").click(function() {

            //                // Pega o id do log
            //                var idLog = $("#tbLog").jqGrid("getGridParam", "selrow");

            //                if (idLog) {
            //                    idLog = idLog.substring(idLog.lastIndexOf("_") + 1);

            //                    // Pede o detalhe
            //                    $.log.detalhe.abrir(idLog);
            //                }
            //                else {
            //                    // Mostra popup de erro
            //                    popup({
            //                        titulo: "Mostrar Detalhe",
            //                        icone: "erro",
            //                        mensagem: "Selecione a linha que deseja ver o detalhe.",
            //                        callbackOK: function() {
            //                        }
            //                    });
            //                }
            //            });

            //            // Trata botão pesquisar
            //            $("#site_log_lista .cmdPesquisar").click(function() {
            //                $.log.lista.listar({ SetTimeOut: false });
            //            });

            // Carrega combo de filtro de tipos de log
            $.log.relatorio.carregarDescricoesLog(function() {

                // Inicializa tabela de log
                $("#tbLogRelatorio").jqGrid({
                    datatype: "clientSide",
                    mtype: "GET",
                    scroll: true,
                    colNames: ["CodigoLog", "Data", "Descrição", "Usuário", "Tipo", "Código Origem"],
                    colModel: [
                      { name: "CodigoLog", index: "CodigoLog", editable: false, key: true, hidden: true },
                      { name: "DataLog", index: "DataLog", editable: false },
                      { name: "Descricao", index: "Descricao", editable: false, width: 400 },
                      { name: "CodigoUsuario", index: "CodigoUsuario", editable: false },
                      { name: "TipoLog", index: "TipoLog", editable: false, formatter: "select", edittype: "select", editoptions: { value: $.log.lista.montarListaDescricoesLog() }, hidden: true },
                      { name: "CodigoOrigem", index: "CodigoOrigem", editable: false, hidden: true }
                    ],
                    sortname: "Usuario",
                    sortorder: "asc",
                    viewrecords: true
                });

                // Preenche o combo
                var itens = $("body").data("descricoesLog");
                for (var i = 0; i < itens.length; i++)
                    $("#painelLogFiltroRelatorio .filtroTipoLogRelatorio").append("<option value='" + itens[i].NomeTipo + "'>" + itens[i].Descricao + "</option>");

                // Chama o callback
                if (callback)
                    callback();
            });
        },

        // ----------------------------------------------------------------------
        // listar: pede a lista de logs de acordo com o filtro
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        listar: function(param, callback) {

            // Prepara o request
            var request = {};

            // Coloca as origens
            if ($.log.relatorio.paramLista && $.log.relatorio.paramLista.FiltroTipoOrigem)
                request.FiltroTipoOrigem = $.log.relatorio.paramLista.FiltroTipoOrigem;
            if ($.log.relatorio.paramLista && $.log.relatorio.paramLista.FiltroCodigoOrigem)
                request.FiltroCodigoOrigem = $.log.relatorio.paramLista.FiltroCodigoOrigem;

            // Monta o filtro
            //            var filtroCodigoUsuario = $("#painelLogFiltroRelatorio .filtroUsuarioRelatorio").val();
            //            if (filtroCodigoUsuario && filtroCodigoUsuario != "")
            //                request.FiltroCodigoUsuario = filtroCodigoUsuario;

            //            var filtroTipoLog = $("#painelLogFiltroRelatorio .filtroTipoLogRelatorio").val();
            //            if (filtroTipoLog != "?")
            //                request.FiltroTipoLog = filtroTipoLog;

            //            var filtroDataInicio = $("#painelLogFiltroRelatorio .filtroDataInicioRelatorio").val();
            //            if (filtroDataInicio != "")
            //                request.FiltroDataInicio = filtroDataInicio;

            //            var filtroDataFim = $("#painelLogFiltroRelatorio .filtroDataFimRelatorio").val();
            //            if (filtroDataFim != "")
            //                request.FiltroDataFim = filtroDataFim;

            // Pede a lista de logs
            executarServico(
                "ListarLogRequest",
                request,
                function(data) {

                    // Limpa o grid
                    $("#tbLogRelatorio").jqGrid("clearGridData");

                    // Adiciona as linhas de log
                    for (var i = 0; i < data.Resultado.length; i++) {
                        $("#tbLogRelatorio").jqGrid(
                            "addRowData",
                            "tbLog_" + data.Resultado[i].CodigoLog,
                            data.Resultado[i]);
                    }
                });
        },

        // ----------------------------------------------------------------------
        // montarListaDescricoesLog: monta a lista de descricoes de log para utilizar no grid
        // ----------------------------------------------------------------------
        montarListaDescricoesLog: function() {

            // Pega a lista
            var lista = $("body").data("descricoesLog");

            // String de retorno
            var retorno = "";

            // Para cada item, adiciona
            for (var i = 0; i < lista.length; i++) {

                // Se tem descricao utiliza, se nao, usa o mnemonico
                var descricao = lista[i].Descricao;
                if (descricao == null)
                    descricao = lista[i].NomeTipo;

                // Adiciona o item
                retorno += lista[i].NomeTipo + ":" + descricao + ";";
            }

            // Ajusta a string
            if (retorno != "")
                retorno = retorno.substr(0, retorno.length - 1);

            // Retorna
            return retorno;
        },

        // ----------------------------------------------------------------------
        // carregarDescricoesLog: carrega a lista de descricoes de log
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        carregarDescricoesLog: function(callback) {

            // Verifica se já não está carregado
            if (!$("body").data("descricoesLog")) {

                // Pede a lista
                executarServico(
                    "ListarDescricaoLogRequest",
                    {},
                    function(data) {

                        // Guarda a lista
                        $("body").data("descricoesLog", data.Resultado);

                        // Chama o callback
                        if (callback)
                            callback();
                    });

            } else {

                // Chama o callback
                if (callback)
                    callback();
            }
        }
    };

    // Dispara o load, caso tenha sido chamado da pagina de lista de log
    if (window.location.href.indexOf("site_log_lista_relatorio") > 0) {

        // Carrega
        $.log.lista.load2(function() {

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbLogRelatorio").setGridWidth($(window).width() - 60);
                $("#tbLogRelatorio").setGridHeight($(window).height() - 300);
            }).trigger("resize");
        });
    }
});