<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_processo_pagamento.js"></script>
    <link href="site_processo_pagamento.css" rel="stylesheet" type="text/css" />
    <title>SCF - Processo Extrato</title>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- 1447 - analise -  Conte�do da P�gina -->
    <div id="site_processo_pagamento">

        <!-- Disparo manual -->
        <div class="detalhe">
            
            <span class="header"><label id="labelDiretorio">Diretorio</label></span>
            <br />
            
            <label>Exportacao Matera:</label>
            <input type="text" class="inputleft" id="txtDiretorioFinanceiro" />
            <br />
            
            
            <span>Esta exportacao gera o arquivo Matera somente do processo Extrato.</span>
         
            <br />
            <br />           
            
            <button id="cmdProcessarPagamento">Iniciar Processamento</button>
            <br />
                
        </div>
       
    </div>

</asp:Content>
