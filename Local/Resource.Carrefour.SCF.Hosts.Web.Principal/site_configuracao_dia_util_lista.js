﻿$(document).ready(function() {

    // Namespace 
    $.diautil = {};

    // Funcoes do namespace 
    $.diautil.lista = {

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Dias Não Úteis");

            // Estilo dos botões
            $("#site_configuracao_dia_util_lista button").button();

            // Inicializa forms
            $("#site_configuracao_dia_util_lista").Forms("initializeFields");

            // Inicializa lista de dias não úteis
            $("#tbDiaNaoUtil").jqGrid({                              
                mtype: "GET",               
                width: 320,   
                datatype: "local",
                height: 'auto',
                autowidth: true,
                forceFit: true,                    
                colNames: ["Código", "Data", "Descrição", "Classificação", ""],
                colModel: [
                    { name: "CodigoDiaNaoUtil", index: "CodigoDiaNaoUtil", jsonmap: "CodigoDiaNaoUtil", hidden: true, sorttype: "integer", editable: true, key: true },
                    { name: "DataDiaNaoUtil", index: "DataDiaNaoUtil", jsonmap: "DataDiaNaoUtil", sorttype: "date", editable: true, datefmt: "dd/mm/yyyy", editrules: { date: true }, editoptions: { dataInit: function(e) { $(e).datepicker(); } }, formatter: formatterDate },
                    { name: "DescricaoDiaNaoUtil", index: "DescricaoDiaNaoUtil", jsonmap: "DescricaoDiaNaoUtil", editable: true, editoptions: { maxlength: 50} },
                    { name: "ClassificacaoDiaNaoUtil", index: "ClassificacaoDiaNaoUtil", jsonmap: "ClassificacaoDiaNaoUtil", editable: true, editoptions: { maxlength: 50} },
                    { name: "Acao", index: "Acao", align: "center", editable: false, width: 25 }
                ],
                    sortname: "Data",
                    scroll: true,                   
                    viewrecords: true               
            });

            // Inicializa edicao do grid
            $.grid.inicializar({
                grid: $("#tbDiaNaoUtil"),
                modeloLinha: { Ativo: true },
                colunaId: "CodigoDiaNaoUtil",
                callbackNovo: $.diautil.lista.salvar,
                callbackEditar: $.diautil.lista.salvar,
                callbackRemover: $.diautil.lista.remover,
                callbackRestaurar: $.diautil.lista.restaurar
            });

            // Pede lista de dias não uteis
            $.diautil.lista.listarDiaNaoUtil(true);
            $("#site_configuracao_dia_util_lista").find("select").css("width", "100%");

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbDiaNaoUtil").setGridWidth($(window).width() - margemEsquerda);
                $("#tbDiaNaoUtil").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");

        },

        // --------------------------------------------------------------------
        //  listarDiaNaoUtil: lista os dias não úteis
        // --------------------------------------------------------------------
        listarDiaNaoUtil: function(listaAtivos) {

            // Recupera a grid de resultado
            var grid = $("#tbDiaNaoUtil");
            
            $("#tbDiaNaoUtil").clearGridData();
                      
            // Executa Serviço
            executarServico(
                "ListarDiaNaoUtilRequest",
                { FiltroAtivo: listaAtivos },
                function(data) {
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var diaNaoUtil = data.Resultado[i];
                        //$("#tbDiaNaoUtil").jqGrid("addRowData", "tbDiaNaoUtil_" + diaNaoUtil.CodigoDiaNaoUtil, diaNaoUtil);
                        $.grid.esconderBotoes("tbDiaNaoUtil_" + diaNaoUtil.CodigoDiaNaoUtil, !listaAtivos);
                    }                               
                    $('#tbDiaNaoUtil').setGridParam({ data: data.Resultado, rowNum: data.Resultado.length }).trigger('reloadGrid');                      
               });                                       
        },

        // --------------------------------------------------------------------
        //  salvar: salva um novo dia não útil
        // --------------------------------------------------------------------
        salvar: function(param) {

            // Cria request
            var request = {};

            // Pede validação necessária
            $.diautil.lista.validar($("#tbDiaNaoUtil"), param, function() {

                // Envia parametros para serviço
                request.DiaNaoUtilInfo = param.valores;

                executarServico(
                "SalvarDiaNaoUtilRequest",
                request,
                function(data) {
                    param.callbackOK({
                        rowid: "tbDiaNaoUtil_" + data.DiaNaoUtilInfo.CodigoDiaNaoUtil,
                        valores: data.DiaNaoUtilInfo
                    });
                });
            },
            function() {
            });
        },

        // --------------------------------------------------------------------
        //  remover: remove um dia não útil
        // --------------------------------------------------------------------
        remover: function(param) {
            
            // Pede para inativar um dia nao util
            executarServico(
                "RemoverDiaNaoUtilRequest",
                { CodigoDiaNaoUtil: param.rowid },
                function(data) {
                    param.callbackOK(
                        $.diautil.lista.listarDiaNaoUtil(true)
                    );
                    
                });
        },

        // --------------------------------------------------------------------
        //  restaurar: restaura um dia não útil removido
        // --------------------------------------------------------------------
        restaurar: function(param) {

            // Pede para reativar um dia util
            executarServico(
                "RemoverDiaNaoUtilRequest",
                { CodigoDiaNaoUtil: param.rowid, Ativar: true },
                function(data) {
                    param.callbackOK();
                });
        },

        // --------------------------------------------------------------------
        //  validar: valida campos necessários do cadastro de dias não úteis
        // --------------------------------------------------------------------
        validar: function(lGrid, lParam, CallbackOK, CallbackErro) {

            // Cria criticas
            var criticas = new Array();

            // Realiza as validações
            criticas = $.grid.validate({ pGrid: lGrid, param: lParam, returnErrors: true, errors: criticas });

            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                CallbackErro();
            }
            else
                CallbackOK();
        }
    };

    // Pede a inicializacao
    $.diautil.lista.load();

});