﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_processo_lista.js"></script>
    <script type="text/javascript" src="site_processo_detalhe.js"></script>
    <script type="text/javascript" src="site_log_lista.js"></script>
    <script type="text/javascript" src="site_log_detalhe.js"></script>
    <link href="site_processo_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_bloqueio_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_processo_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_bloqueio_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_log_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_log_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_arquivo_detalhe.css" rel="stylesheet" type="text/css" />
    <%--<script src="site_arquivo_lista.js" type="text/javascript"></script>//SCF1138 - Marcos Matsuoka - corrigir o nome do subtitulo. assumia o titulo deste js--%> 
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <%--<script src="site_bloqueio_lista.js" type="text/javascript"></script>//SCF1138 - Marcos Matsuoka - corrigir o nome do subtitulo. assumia o titulo deste js--%>
    <script src="site_bloqueio_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_item_lista.js" type="text/javascript"></script>
    <script src="site_critica_lista.js" type="text/javascript"></script>
    <script src="site_critica_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_alteracao_massiva.js" type="text/javascript"></script>
    <title>SCF - Processos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--          Lista de Processos          -->
    <!-- ------------------------------------ -->
    <div id="site_processo_lista" >
        
        <!-- Filtro -->
        <div class="filtro formulario" >
            <table>
                <tr>
                    <td>
                        <label>Data Inicial</label>
                    </td>
                    <td>
                        <input type="text" id="txtFiltroDataInicial" class="forms[property[FiltroDataInclusaoInicial];dataType[date];maskType[date]]"/>
                    </td>
                    <td>
                        <label>Data Final</label>
                    </td>
                    <td>
                        <input type="text" id="txtFiltroDataFinal" class="forms[property[FiltroDataInclusaoFinal];dataType[date];maskType[date]]"/>
                    </td>
                     <td>
                        <label>Tipo de Processo</label>
                    </td>
                    <td>
                        <select class="tipoprocesso forms[property[FiltroTipoProcesso]]">
                            <option value="?">(selecione)</option>           
                            <option value="ProcessoAtacadaoInfo">Atacadão</option>
                            <option value="ProcessoExtratoInfo">Extrato</option>
                            <option value="ProcessoCessaoInfo">Retorno de Cessão</option>
                            <!-- <option value="ProcessoPagamentoInfo">Pagamento</option> -->
                            <option value="ProcessoRelatorioVencimentosExcelInfo">Relatório Vencimento Excel</option>
                            <option value="ProcessoRelatorioVencimentosPDFInfo">Relatório Vencimento PDF</option>
                            <option value="ProcessoRelatorioVendasExcelInfo">Relatório de Vendas Excel</option>
                            <option value="ProcessoRelatorioVendasPDFInfo">Relatório de Vendas PDF</option>
                            <option value="ProcessoRelatorioTransacaoExcelInfo">Relatório de Transações Excel</option>
                            <option value="ProcessoRelatorioTransacaoPDFInfo">Relatório de Transações PDF</option>
                            <option value="ProcessoRelatorioTransacaoInconsistenteExcelInfo">Relatório de Transações Inconsistentes Excel</option>
                            <option value="ProcessoRelatorioTransacaoInconsistentePDFInfo">Relatório de Transações Inconsistentes PDF</option>
                            <option value="ProcessoRelatorioConsolidadoExcelInfo">Relatório Consolidado Excel</option>
                            <option value="ProcessoRelatorioConsolidadoPDFInfo">Relatório Consolidado PDF</option>
                            <option value="ProcessoRelatorioConciliacaoContabilExcelInfo">Relatório de Conciliação Contábil Excel</option>
                            <option value="ProcessoRelatorioConciliacaoContabilPDFInfo">Relatório de Conciliação Contábil PDF</option>
                        </select>
                    </td>
                    <td>
                        <label>Status</label>
                    </td>
                    
                    <td>
                        <select id="cmbFiltroStatusProcesso" class="forms[property[FiltroStatusProcesso]]">
                            <option value="?">(selecione)</option>
                            <option value="0">Em Andamento</option>
                            <option value="1">Finalizado</option>
                            <option value="9">Cancelado</option>
                        </select>
                    </td>
                    <td>
                        <button class="cmdFiltrar">Filtrar</button>
                    </td>
                </tr>
                   <tr>
                   
                    <td>
                        <label>Status Bloqueios</label>
                    </td>
                    <td>
                         <select id="cmbFiltroBloqueio" class="forms[property[FiltroStatusBloqueio]]">
                            <option value="?">(selecione)</option>
                            <option value="1">Tem Bloqueios</option>
                            <option value="0">Sem Bloqueios</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Lista -->
        <div class="lista">
            <table id="tbProcesso"></table>
<%-- SCF1138 - Marcos Matsuoka - inibir botões
            <div style="float:left;">
                <button class="cmdLiberarProcesso">Liberar Processo</button>
            </div>--%>
            
            <div class="botoes">     
                
<%-- SCF1138 - Marcos Matsuoka - inibir botões
                <button class="cmdEditarProcesso">Detalhar Processo</button>--%>                
<%--                <button id="cmdCancelar">Cancelar</button>
                <button class="cmdContinuarProcesso" id="cmdContinuarProcesso">Continuar Processo</button>
                <button class="cmdReprocessarAgenda">Reprocessar Agenda</button>
                <button class="cmdBloqueios">Bloqueios</button>--%>
                <button class="cmdLog">Visualizar Log</button>
            </div>
        </div>   
    </div>

    <!-- -------------------------------------------- -->
    <!--             Dialog de Novo Processo          -->
    <!-- -------------------------------------------- -->
    <div id="site_processo_lista_novo">
    
        <label>Tipo do Processo:</label>
        <select>
            <option value="?">(selecione)</option>           
            <option value="ProcessoAtacadaoInfo">Atacadão</option>
            <option value="ProcessoExtratoInfo">Extrato</option>
            <option value="ProcessoCessaoInfo">Retorno de Cessão</option>
            <!--<option value="ProcessoPagamentoInfo">Pagamento</option>-->
        </select>
    
    </div>

</asp:Content>
