﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_produto_lista.js"></script>
    <script type="text/javascript" src="site_configuracao_produto_detalhe.js"></script>
    <link href="site_configuracao_produto_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_configuracao_produto_lista.css" rel="stylesheet" type="text/css" />
    
    <title>SCF - Cadastro de Produtos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--           Lista de Produtos          -->
    <!-- ------------------------------------ -->
    <div id="site_configuracao_produto_lista" class="lista">
        <table id="tbProduto"></table>
        <div class="botoes">
            <button id="cmdNovoProduto">Novo</button>
            <button id="cmdEditarProduto">Editar</button>
            <button id="cmdRemoverProduto">Remover</button>
            <button id="cmdAtualizarListaProduto">Atualizar Lista</button>
        </div>
    </div>

</asp:Content>
