﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class site_configuracao_geral : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MensagemResponseBase retorno = null;
            try
            {
                if (Session["CodigoSessao"] != null)
                {
                    ListarLayoutResponse layouts =
                            Mensageria.Processar<ListarLayoutResponse>(
                                new ListarLayoutRequest()
                                {
                                    CodigoSessao = Session["CodigoSessao"].ToString()
                                });
                    // Preenche combo Layout CCI
                    rptCmbLayoutCCI.DataSource = layouts.Resultado;
                    rptCmbLayoutCCI.DataBind();
                    //Preenche combo layout ATA
                    rptCmbLayoutATA.DataSource = layouts.Resultado;
                    rptCmbLayoutATA.DataBind();
                    //Preenche combo layout GAL
                    rptCmbLayoutGAL.DataSource = layouts.Resultado;
                    rptCmbLayoutGAL.DataBind();
                }
                else
                {
                    retorno =
                        new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                            DescricaoResposta = "Sessão Inválida"
                        };
                }
            }
            catch (Exception ex)
            {
                // Cria mensagem generica informando o erro
                retorno =
                    new MensagemErroResponse()
                    {
                        StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                        DescricaoResposta = ex.ToString()
                    };
            }
        }
    }
}