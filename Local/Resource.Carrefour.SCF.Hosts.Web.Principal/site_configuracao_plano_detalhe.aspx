﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_configuracao_plano_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_configuracao_plano_detalhe.js" type="text/javascript"></script>
    <title>SCF - Cadastro de Planos</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
     <div id="site_configuracao_plano_detalhe" style="margin: 15px;">

        <input type="hidden" id="hidCodigoPlano" class="forms[property[CodigoPlano]]" />
        <input type="hidden" id="hidAtivo" class="forms[property[Ativo]]" />
        <table>
            <tr>
                <td style="width: 100px;">
                    <label id="lblCodigoPlanoTSYS">Código TSYS:</label>
                </td>
                <td colspan="3">
                    <input type="text" id="txtCodigoPlanoTSYS" class="forms[property[CodigoPlanoTSYS]; required]" style="width: 70px;" maxlength="6"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label id="lblQuantidadeParcelas">Qtde. Parcelas:</label>
                </td>
                <td>
                    <input type="text" id="txtQuantidadeParcelas" class="forms[property[QuantidadeParcelas];dataType[integer];required]" style="width: 70px;" maxlength="5"/>
                </td>
                <td>
                    <label id="lblNomePlano">Nome:</label>
                </td>
                <td>
                    <input type="text" id="txtNomePlano" class="forms[property[NomePlano];required]" style="width: 270px;" maxlength="40"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label id="lblComissao">Comissão:</label>
                </td>
                <td style="width: 110px;">
                    <input type="text" id="txtComissao" class="forms[property[Comissao];dataType[decimal];required]" style="width: 70px;" maxlength="6"/>
                    <label id="lblComissaoPercentual">%</label>
                </td>
                <td>
                    <label id="lblDescricaoPlano">Descrição:</label>
                </td>
                <td>
                    <input type="text" id="txtDescricaoPlano" class="forms[property[DescricaoPlano]]" style="width: 270px;" maxlength="40"/>
                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>
