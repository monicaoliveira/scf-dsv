﻿$(document).ready(function() 
{   if (!$.arquivo)
        $.arquivo = {};

    $.arquivo.alteracaoMassiva = 
    {   variaveis: {}
    //=================================================================================================================
    ,   parametros: 
        {   codigoArquivo: null
        ,   codigoProcesso: null
        ,   tipoLinha: null
        ,   tipoCritica: null
        ,   nomeCampo: null
        }
    //=================================================================================================================        
    ,   funcaoResize: function() 
        {
            for (var i = 0; i < $.arquivo.alteracaoMassiva.variaveis.Tabelas.length; i++) {
                $($.arquivo.alteracaoMassiva.variaveis.Tabelas[i]).setGridWidth($("#site_arquivo_alteracao_massiva").width() - 15);
            }
        }
    //=================================================================================================================        
    ,   abrir: function(parametros, callback) 
        {
            $.arquivo.alteracaoMassiva.parametros = parametros; // 1282 $.arquivo.alteracaoMassiva.variaveis.parametros = parametros;
            //=================================================================================================================        
            // 1282 
            // Esta instrução sobrepunha os dados vindos dos parametros da tela anterior
            // Deixando os filtros anteriores, que tinha dados, carregados
            // parametros = $.extend($.arquivo.alteracaoMassiva.parametros, parametros); 
            //=================================================================================================================        
            $.arquivo.alteracaoMassiva.load(function() 
            {   $("#tb_criticas").jqGrid("clearGridData"); //1282 $("#tbCriticasAgrupadas").empty();

                $("#site_arquivo_alteracao_massiva").dialog(
                {   autoOpen: false
                ,   height: 500
                ,   width: 1200
                ,   modal: true
                ,   title: "ALTERACAO MASSIVA"
                ,   buttons: 
                    {
                        "Salvar": function() 
                        {   popup(
                            {   titulo: "SALVAR"
                            ,   icone: "atencao"
                            ,   mensagem: "Deseja realizar a atualização massiva?"
                            ,   callbackOK: function() 
                                {   var param = $.arquivo.alteracaoMassiva.parametros;
                                    var objLinhas = $("#tb_criticas").getUserData();
                                    var request = { };
                                    
                                    request.NovosValores = [];
                                    request.NomesCampos = [];
                                    request.TiposLinhas = [];
                                    request.TiposCriticas = [];
                                    request.DescricoesCriticas = [];

                                    request.CodigoArquivo = param.codigoArquivo;
                                    request.CodigoProcesso = param.codigoProcesso;
                                    request.TipoLinha = param.tipoLinha;
                                    request.TipoCritica = param.tipoCritica;
                                    
                                    request.NomesCampos = $.arquivo.alteracaoMassiva.variaveis.NomesCampos;
                                    request.TiposLinhas = $.arquivo.alteracaoMassiva.variaveis.TiposLinhas;
                                    request.TiposCriticas = $.arquivo.alteracaoMassiva.variaveis.TiposCriticas;
                                    request.DescricoesCriticas = $.arquivo.alteracaoMassiva.variaveis.DescricoesCriticas;
                                                                       
                                    $("#tb_criticas input").each(function(index, element)
                                    {   
                                          if (($(this).attr("id").indexOf("NovoValor")) >= 0)
                                            request.NovosValores.push($(this).val());
                                    });

                                    executarServico
                                    (   "AtualizarEmMassaRequest"
                                    ,   request
                                    ,   function(data) 
                                        {   popup(
                                            {   titulo: "Atualização Massiva"
                                            ,   icone: "sucesso"
                                            ,   mensagem: "Atualização Massiva executada com Sucesso"
                                            ,   callbackOK: function() 
                                                { $("#site_arquivo_alteracao_massiva").dialog("close") 
                                                }
                                            });
                                    });
                                }
                            ,   callbackCancelar: function() { }
                            });

                        }
                    ,   "Fechar": function() 
                        {
                            $("#site_arquivo_alteracao_massiva").dialog("close");
                        }
                    }
                ,   resize: $.arquivo.alteracaoMassiva.funcaoResize
                });

                $("#site_arquivo_alteracao_massiva").dialog("open");
                $.arquivo.alteracaoMassiva.listar(parametros, function() {
                    $.arquivo.alteracaoMassiva.funcaoResize();
                });

                if (callback)
                    callback();
            });
        }
    //=================================================================================================================        
    ,   load: function(callback) 
        {   if ($("#site_arquivo_alteracao_massiva").length == 0) 
            {   $("#areaTemporaria").load("site_arquivo_alteracao_massiva.aspx #site_arquivo_alteracao_massiva", function() 
                {   $("#site_arquivo_alteracao_massiva").appendTo("#areaComum");
                    $.arquivo.alteracaoMassiva.load2(function() 
                    {   if (callback)
                            callback();
                    });
                });
            } else 
            {   if (callback)
                    callback();
            }
        }
    //=================================================================================================================        
    ,   load2: function(callback) 
        {
            if (callback)
                callback();
        }
    //=================================================================================================================
    ,   listar: function(parametros, callback) 
        {   $.arquivo.alteracaoMassiva.variaveis.Tabelas = [];
            $.arquivo.alteracaoMassiva.variaveis.NomesCampos = [];
            $.arquivo.alteracaoMassiva.variaveis.TiposLinhas = []; // 1282
            $.arquivo.alteracaoMassiva.variaveis.NovosValores = []; // 1282
            $.arquivo.alteracaoMassiva.variaveis.DescricoesCriticas = []; // 1282
            $.arquivo.alteracaoMassiva.variaveis.TiposCriticas = []; // 1282
                                  
            var vTemDados = false; //1282
            executarServico
            (   "ListarCriticaRequest"
            ,   {   FiltroCodigoProcesso: parametros.codigoProcesso
                ,   FiltroCodigoArquivo: parametros.codigoArquivo
                ,   FiltroTipoLinha: parametros.tipoLinha
                ,   RetornarAgrupado: true
                ,   FiltroNomeCampo: parametros.nomeCampo
                ,   FiltroTipoCritica: parametros.tipoCritica
                ,   MaxLinhas: 0 //1282
                }
            ,   function(data) 
                {
                    $("#tb_criticas").jqGrid(
                    {   datatype: "clientSide"
                    ,   mtype: "GET"
                    ,   height: 350 //1282
                    ,   width: 1120
                    ,   shrinkToFit: false //1282
                    ,   colNames: 
                        [   "Qtd"
                        ,   "Descricao Critica"
                        ,   "Tp Linha"
                        ,   "Novo Valor"
                        ,   "Tipo Critica"
                        ,   "Nome Campo"
                        ]
                    ,   colModel: 
                        [ 
                          { name: "QtdRegistros", index: "QtdRegistros", width: 50, align: "center", editable: false }
                        , { name: "Descricao", index: "Descricao", width: 500, align: "left", editable: false }
                        , { name: "TipoLinha", index: "TipoLinha", width: 50, align: "center", editable: false }
                        , { name: "NovoValor", value: "", index: "NovoValor", width: 200, align: "left", edittype: 'text',  editable: true}
                        , { name: "TipoCritica", index: "TipoCritica", width: 100, align: "left", edittype: 'text',  editable: false}
                        , { name: "NomeCampo", index: "NomeCampo", width: 180, align: "left", edittype: 'text',  editable: false}
                        ]
                    ,   sortorder: "asc"
                    ,   viewrecords: true
                    });

                    for (var i = 0; i < data.ResultadoAgrupado.length; i++) 
                    {
                        if (data.ResultadoAgrupado[i].TipoCritica == "CriticaSCFDominio") //1282
                        {
                            vTemDados = true; //1282
                            $.arquivo.alteracaoMassiva.variaveis.NomesCampos.push(data.ResultadoAgrupado[i].NomeCampo);
                            $.arquivo.alteracaoMassiva.variaveis.DescricoesCriticas.push(data.ResultadoAgrupado[i].Descricao);
                            $.arquivo.alteracaoMassiva.variaveis.TiposLinhas.push(data.ResultadoAgrupado[i].TipoLinha); //1282
                            $.arquivo.alteracaoMassiva.variaveis.TiposCriticas.push(data.ResultadoAgrupado[i].TipoCritica); //1282
                            $("#tb_criticas").jqGrid("addRowData", "tb_criticas_" + data.ResultadoAgrupado[i].Descricao, data.ResultadoAgrupado[i]); //1282
                            $("#tb_criticas").jqGrid("editRow", "tb_criticas_" + data.ResultadoAgrupado[i].Descricao, true);
                        }                        
                    }
                    if (vTemDados == false)     //1282
                    {   popup(
                        {   titulo: "Atualização Massiva"
                        ,   icone: "erro"
                        ,   mensagem: "Não existem criticas que possam ser alteradas na Atualização Massiva"
                        ,   callbackOK: function() 
                            { $("#site_arquivo_alteracao_massiva").dialog("close") 
                            }
                        });
                    }
                    if (callback)
                        callback();
                }
            );

        }
    }
});