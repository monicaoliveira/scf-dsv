﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/template_default.Master"
    CodeBehind="site_configuracao_taxa_recebimento_detalhe.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_configuracao_taxa_recebimento_detalhe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="site_configuracao_taxa_recebimento_detalhe.js"></script>

    <link type="text/css" rel="stylesheet" href="site_configuracao_taxa_recebimento_detalhe.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="site_configuracao_taxa_recebimento_detalhe">
        <ul>
            <li>
                <label>
                    Grupo:</label>
            </li>
            <li>
                <select id="cmbGrupoDetalhe" class="forms[property[CodigoGrupo];required]">
                    <option value="?" selected="selected">Todos</option>
                </select>
            </li>
        </ul>
        <ul>
            <li>
                <label>
                    Referência:</label>
            </li>
            <li>
                <select id="cmbReferenciaDetalhe" class="forms[property[CodigoReferencia];required]">
                    <option value="?" selected="selected">Todas</option>
                </select>
            </li>
        </ul>
        <ul>
            <li>
                <label>
                    Meio de Pagamento:</label>
            </li>
            <li>
                <select id="cmbMeioPagamentoDetalhe" class="forms[property[CodigoMeioPagamento];required]">
                    <option value="?" selected="selected">Todos</option>
                </select>
            </li>
        </ul>
        <ul>
            <li>
                <label>
                    Válido De:</label>
            </li>
            <li>
                <input type="text" id="txtDataInicio" class="forms[property[DataInicio];dataType[date];maskType[date];required] DataInicio" />
            </li>
        </ul>
        <ul>
            <li>
                <label>
                    Válido Até:</label>
            </li>
            <li>
                <input type="text" id="txtDataFim" class="forms[property[DataFim];dataType[date];maskType[date];required] DataFim" />
            </li>
        </ul>
        <ul>
            <li>
                <label>
                    Valor (R$):</label></li>
            <li>
                <input type="text" id="txtValorRecebimento" class="forms[property[ValorRecebimento]]"
                    maxlength="7" style="text-align: right" /></li></ul>
        <ul>
            <li>
                <label>
                    Percentual (%):</label></li>
            <li>
                <input type="text" id="txtTaxaRecebimento" class="forms[property[PercentualRecebimento]]"
                    maxlength="5" style="text-align: right" /></li></ul>
    </div>
</asp:Content>
