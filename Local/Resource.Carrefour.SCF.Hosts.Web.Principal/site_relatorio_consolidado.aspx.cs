﻿using System;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class site_relatorio_consolidado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["acao"] == null && !IsPostBack)
            {
                //-------------------------------------------------------------------------------------------
                // CARREGA COMBO DE ESTABELECIMENTOS
                //-------------------------------------------------------------------------------------------
                MensagemResponseBase retorno = null;
                try
                {
                    if (Session["CodigoSessao"] != null)
                    {
                        // Recebe lista de estabelecimentos
                        ListarEstabelecimentoResponse estabelecimentos =
                            Mensageria.Processar<ListarEstabelecimentoResponse>(
                                new ListarEstabelecimentoRequest()
                                {
                                    CodigoSessao = Session["CodigoSessao"].ToString()
                                });

                        // Preenche combo
                        rptCmbEstabelecimento.DataSource = estabelecimentos.Resultado;
                        rptCmbEstabelecimento.DataBind();
                    }
                    else
                    {
                        retorno = new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                            DescricaoResposta = "Sessão Inválida"
                        };
                    }
                }
                catch (Exception ex)
                {
                    // Cria mensagem generica informando o erro
                    retorno =
                        new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                            DescricaoResposta = ex.ToString()
                        };
                }
            }
            else if (Request["acao"] == "pdf")
            {
                string urlToConvert = GetUrlRelatorio();
                string downloadName = "RelatorioVencimento";
                byte[] downloadBytes = null;

                downloadName += ".pdf";
                downloadBytes = PdfHelper.GetDownloadBytes(urlToConvert, downloadName);

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition",
                    "attachment; filename=" + downloadName + "; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        protected string GetUrlRelatorio()
        {
            /* **************************************
             * PARA TESTAR O PLUGIN GERADOR DE PDF 
             * É NECESSÁRIO ALTERAR O VALOR DA PORTA 
             * DE ACORDO COM A SUA MÁQUINA
             * **************************************/

            string url = "http://localhost:55143/relatorio_vencimento.aspx";

            string parametros = "";
            parametros += this.Request["CodigoFavorecido"] != "?" ? this.Request["CodigoFavorecido"] + "&" : null;
            parametros += this.Request["CodigoEmpresaGrupo"] != "?" ? this.Request["CodigoEmpresaGrupo"] + "&" : null;
            parametros += this.Request["CodigoProduto"] != "?" ? this.Request["CodigoProduto"] + "&" : null;
            parametros += this.Request["DataInicioTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioTransacao"]) : null;
            parametros += this.Request["DataFimTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimTransacao"]) : null;
            parametros += this.Request["DataInicioVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioVencimento"]) : null;
            parametros += this.Request["DataFimVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimVencimento"]) : null;
            
            if (parametros != "")
                url += "?" + parametros.Substring(0, parametros.Length - 1);

            return url;
        }
    }
}
