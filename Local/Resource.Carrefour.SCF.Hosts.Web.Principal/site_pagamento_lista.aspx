﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="site_pagamento_lista.js"></script>
    <link href="site_pagamento_lista.css" rel="stylesheet" type="text/css" />
    <title>SCF - Lista de Pagamentos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
       <div id="site_pagamento_filtro">
       
            <!-- Filtro -->
            <div class="filtro formulario">
                <table>
                    <tr>
                        <td>
                            <label class="label_detalhe2">Dt Geração De:</label>
                            <input size=8 type="text"  id="txtDtGeracaoDe"  class="forms[property[FiltroDataGeracaoDe];dataType[date];maskType[date]]"/>
                        </td>
                        <td>
                            <label class="label_detalhe2">Até:</label>
                            <input size=8 type="text" id="cmbDtGeracaoAte" class="forms[property[FiltroDataGeracaoAte];dataType[date];maskType[date]]"/>
                        </td>
                        <td>
                            <label class="label_detalhe2">Dt Envio De:</label>
                            <input size=8 type="text"  id="Text1"  class="forms[property[FiltroDataEnvioDe];dataType[date];maskType[date]]"/>
                        </td>
                        <td>    
                            <label class="label_detalhe2">Até:</label>
                            <input size=8 type="text" id="Text2" class="forms[property[FiltroDataEnvioAte];dataType[date];maskType[date]]"/>
                        </td>
                        <td>
                            <label class="label_detalhe2">Dt Pagamento De:</label>
                            <input size=8 type="text"  id="Text3"  class="forms[property[FiltroDataPagamentoInicio];dataType[date];maskType[date]]"/>
                        </td>
                        <td>    
                            <label class="label_detalhe2">Até:</label>
                            <input size=8 type="text" id="Text4" class="forms[property[FiltroDataPagamentoFim];dataType[date];maskType[date]]"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="label_detalhe2">Status:</label>
                            <select class="forms[property[FiltroStatusPagamento]]">
                                <option value="?">(todos)</option>
                                <option value="0">Pendente</option>                                
                                <option value="1">Enviado</option>
                                <option value="3">Em exclusão</option>
                            </select>
                        </td>
                        <td>
                            <label class="label_detalhe2">Banco:</label>
                            <input size=8 type="text" id="txtBanco" class="forms[property[FiltroBanco];dataType[integer]]" maxlength="3"/>
                        </td>
                        <td>
                            <label class="label_detalhe2">Agência:</label>
                            <input size=8 type="text" id="txtAgencia" class="forms[property[FiltroAgencia];dataType[integer]]" maxlength="6"/>
                        </td>
                        <td>
                            <label class="label_detalhe2">Conta:</label>
                            <input size=8 type="text" id="txtConta" class="forms[property[FiltroConta];dataType[integer]]" maxlength="12"/>
                        </td>                                  
                         <td>
                                <button class="cmdFiltrar">Filtrar</button>
                         </td> 
                    </tr>
                </table>
                
                <!-- ------------------------------------ -->
                <!--            Lista de Pagamentos           -->
                <!-- ------------------------------------ -->
                <div id="site_pagamento_lista" class="lista">
                    <table id="tbPagamento"></table>
                </div>

                
                <!-- ------------------------------------ -->
                <!--            Area de Geracao do arquivo         -->
                <!-- ------------------------------------ -->
                <br />
                
                <div style="border:#4f4f4f; border-width:1px; border-style:solid; " >
                    <table style="padding:10px">
                        <tr>
                            <td colspan="2"  class="label_detalhe3">
                                <label style="font-size:14px; font-weight:bold">Exportação do arquivo Matera Financeiro</label>
                            </td>                                  
                             
                        </tr>
                        <tr>
                            <td class="label_detalhe3">
                                <label >Qtde de pagamentos pendente de envio:</label>
                                <label id="lblQtdePagtoPendente"></label>
                                
                            </td>                                  
                             <td>
                                    <button class="btnExportarArquivoPagamentos">Gerar Arquivo Matera Financeiro</button>
                             </td> 
                        </tr>
                        <tr>
                            <td colspan = "2" >
                                <label style=" font-stretch:semi-condensed">* serão gerados todos os pagamentos Pendentes, independente do filtro utilizado na área acima.</label>
                                
                            </td>                                  
                            
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    
</asp:Content>
