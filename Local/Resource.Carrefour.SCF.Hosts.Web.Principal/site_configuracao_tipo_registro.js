﻿$(document).ready(function() {

    // Namespace 
    $.configuracaoTipoRegistro = {};

    // Funcoes do namespace
    $.configuracaoTipoRegistro.lista = {

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Tipos de Registro");

            // Estilo dos botões
            $("#site_configuracao_tipo_registro_lista button").button();

            // Inicializa forms
            $("#site_configuracao_tipo_registro_lista").Forms("initializeFields");

            // --------------------------------------------------------------------
            // BOTÃO ENVIO CAMPOS CCI
            // --------------------------------------------------------------------
            //1333            $("#cmdEnvioCampoCCI").click(function() {

            //                // Chamar página de edição de campos para envio CCI
            //                location.href='site_configuracao_envio_campos_para_cci.aspx';
            //            });

            // Inicializa lista de configuracoes
            // 1333               
            $("#tbConfigTipoRegistro").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                colNames: ["ID", "Grupo", "Tipo Registro", "Enviar CCI", "Gerar Agenda", "Dias Repasse", "Acao"],
                colModel: [
                    { name: "CodigoConfiguracaoTipoRegistro", index: 0, editable: false, width: 50 },
                    { name: "TipoArquivo", index: 2, classes: "grid[property[Arquivo];required]", editable: false, width: 600, edittype: "select", formatter: "select", editoptions: { value: "ArquivoTSYSCCI:Grupo Carrefour;ArquivoTSYSATA:Grupo Atacadao;ArquivoTSYSGAL:Grupo Galeria", dataInit: function(elem) { $(elem).width(200); } } },
                    { name: "TipoRegistro", index: 3, classes: "grid[property[TipoRegistro];required]", editable: false, width: 100, editoptions: { maxlength: 2, size: 5} },
                    { name: "EnviarCCI", index: 4, editable: true, width: 100, edittype: "select", formatter: "select", editoptions: { value: "true:Sim;false:Não", dataInit: function(elem) { $(elem).width(100); } } },
                    { name: "GerarAgenda", index: 4, editable: true, width: 100, edittype: "select", formatter: "select", editoptions: { value: "true:Sim;false:Não", dataInit: function(elem) { $(elem).width(100); } } },
                    { name: "DiasRepasse", index: 4, classes: "grid[property[DataRepasse]]", editable: true, width: 100, editoptions: { maxlength: 2, size: 5} },
                    { name: "Acao", index: 5, editable: false, width: 100, align: "center" }
                ],
                sortname: "NomeArquivo",
                shrinkToFit: false //1353
            });

            // Inicializa edicao do grid
            $.grid.inicializar({
                grid: $("#tbConfigTipoRegistro"),
                modeloLinha: { Ativo: true },
                colunaId: "CodigoConfiguracaoTipoRegistro",
                callbackRestaurar: $.configuracaoTipoRegistro.lista.restaurar,
                callbackEditar: $.configuracaoTipoRegistro.lista.salvar,
                permiteApenasAlterar: true
            });

            // Pede lista de configuracoes
            $.configuracaoTipoRegistro.lista.listarConfiguracaoTipoRegistro();
            $("#site_configuracao_tipo_registro_lista").find("select").css("width", "20%");

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbConfigTipoRegistro").setGridWidth($(window).width() - margemEsquerda);
                $("#tbConfigTipoRegistro").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");

        },

        // --------------------------------------------------------------------
        //  listarConfiguracaoTipoRegistro: lista os configuracoes
        // --------------------------------------------------------------------
        listarConfiguracaoTipoRegistro: function() {

            $("#tbConfigTipoRegistro").clearGridData();

            // Executa Serviço
            //1361 executarServico("ListarConfiguracaoTipoRegistroRequest",{FiltroAtivo: true}, - Não existe este filtro na procedure PR_CONFIG_TIPO_REGISTRO_L
            executarServico("ListarConfiguracaoTipoRegistroRequest", { FiltroTipoArquivo: "GRUPO" }, //1361 inclusao do filtro GRUPO para receber todos os tipos de arquivo nesta tela
            function(data) {
                for (var i = 0; i < data.Resultado.length; i++) {
                    var configuracaoTipoRegistro = data.Resultado[i];

                    if (configuracaoTipoRegistro.TipoArquivo != 'ArquivoTSYSATA')
                        configuracaoTipoRegistro.DiasRepasse = "";
                    $("#tbConfigTipoRegistro").jqGrid("addRowData", "tbConfigTipoRegistro_" + configuracaoTipoRegistro.CodigoConfiguracaoTipoRegistro, configuracaoTipoRegistro);
                }
            });
        },

        // --------------------------------------------------------------------
        //  salvar: salva uma nova configuracao
        // --------------------------------------------------------------------
        salvar: function(param) {

            // Cria request
            var request = {};

            // Pede validação necessária
            $.configuracaoTipoRegistro.lista.validar($("#tbConfigTipoRegistro"), param, function() {

                // Envia parametros para serviço
                request.ConfiguracaoConfiguracaoTipoRegistroInfo = param.valores;

                // O dias de repasse pode ser em branco e quando a pessoa digitava um numero e 
                // depois apaga, o valor na variavel param.valores permanece o anterior.
                // por isso é preciso pegar o valor do dias de repasse de outra maneira]
                var diasRepasse = $("#" + param.id + "_DiasRepasse").val();
                request.ConfiguracaoConfiguracaoTipoRegistroInfo.DiasRepasse = diasRepasse;

                executarServico(
                "SalvarConfiguracaoTipoRegistroRequest",
                request,
                function(data) {
                    param.callbackOK({
                        rowid: "tbConfigTipoRegistro_" + data.ConfiguracaoTipoRegistroInfo.CodigoConfiguracaoTipoRegistro,
                        valores: data.tbConfigTipoRegistro
                    });
                });
            },
            function() {

            });
        },

        // --------------------------------------------------------------------
        //  salvarNovo: salva uma nova configuracao
        // --------------------------------------------------------------------
        salvarNovo: function(param) {

            // apresentar mensagem
            popup({
                titulo: "Salvar Configurações",
                icone: "erro",
                mensagem: "Não é permitido inserir novas configurações",
                callbackOK: function() { }
            });


        },

        // --------------------------------------------------------------------
        //  remover: remove uma configuracao do tipo de registro 
        // --------------------------------------------------------------------
        remover: function(param) {

            // Pede para inativar uma configuracao de tipo de registro
            executarServico(
                "RemoverConfiguracaoTipoRegistroRequest",
                { CodigoConfiguracaoTipoRegistro: param.rowid },
                function(data) {
                    param.callbackOK();
                });
        },

        // --------------------------------------------------------------------
        //  restaurar: restaura uma configuracao do tipo de registro 
        // --------------------------------------------------------------------
        restaurar: function(param) {

            // Pede para reativar uma configuracao de tipo de registro
            executarServico(
                "RemoverConfiguracaoTipoRegistroRequest",
                { CodigoConfiguracaoTipoRegistro: param.rowid, Ativar: true },
                function(data) {
                    param.callbackOK();
                });
        },

        // --------------------------------------------------------------------
        //  validar: valida campos necessários do cadastro de configuracao de tipo de registro
        // --------------------------------------------------------------------
        validar: function(lGrid, lParam, CallbackOK, CallbackErro) {

            // Cria criticas
            var criticas = new Array();

            // Realiza as validações
            criticas = $.grid.validate({ pGrid: lGrid, param: lParam, returnErrors: true, errors: criticas });
            var diasRepasse = $("#" + lParam.id + "_DiasRepasse").val();
            if (diasRepasse != "") {
                if (lParam.valores.TipoArquivo != 'ArquivoTSYSATA') {
                    criticas.push({
                        message: "Não pode haver qtde de dias de repasse para esse grupo.", element: $(this)
                    });
                } else if (!parseInt(diasRepasse)) {
                    criticas.push({
                        message: "Valor de Repasse tem que ser númerico.", element: $(this)
                    });
                }
            }

            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                CallbackErro();
            }
            else
                CallbackOK();
        }

    };

    // Pede a inicializacao
    $.configuracaoTipoRegistro.lista.load();

});