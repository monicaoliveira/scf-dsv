﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adm_auxilio_2.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.adm_auxilio_2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
		<link type="text/css" href="css/custom-theme/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
        <script type="text/javascript" src="js/lib/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="js/lib/jquery-ui-1.8.11.custom.min.js"></script>
		<script type="text/javascript" src="js/lib/jquery.json-2.2.js"></script>
		<script type="text/javascript" src="js/lib/jquery.glob.js"></script>
		<script type="text/javascript" src="js/lib/jquery.glob.pt-br.min.js"></script>
        <script type="text/javascript" src="js/lib/i18n/grid.locale-pt-br.js"></script>
        <script type="text/javascript" src="js/lib/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="comum_servico.js"></script>
        <script type="text/javascript" src="template_default.js"></script>
        <script type="text/javascript" src="adm_auxilio_2.js"></script>
        <link href="adm_auxilio_2.css" rel="stylesheet" type="text/css" />
        <title></title>
    </head>
    <body>
        <div id="auxilio2">
        
            <label>Tipo Config: </label>
            <input type="text" id="txtTipoConfig" style="width: 500px;" />
            <input type="button" id="cmdExecutar" value="Executar" />
            <br /><br /><br />
            <code id="txtConfig" style="width: 80%"></code>
        
        </div>
        <div id="janelaErro" style="display: none;">
            <div id="sessaoInvalida" style="display: none;">
                <span>Sessão expirada. Faça o <a href="login.aspx">login</a> novamente.</span>
            </div>
            <div id="erroNegocio" style="display: none;">
                <span>Ocorreram erros de validação.</span>
            </div>
            <div id="erroPrograma" style="display: none;">
                <span>Ocorreram erros inesperados na execução.</span><br />
                <textarea id="txtErroPrograma" style="width: 100%; height: 250px;"></textarea>
            </div>
            <div id="acessoNaoPermitido" style="display: none;">
                <span>A requisição solicitada não pode ser executada com o perfil atual.</span>
            </div>
        </div>
    </body>
</html>
