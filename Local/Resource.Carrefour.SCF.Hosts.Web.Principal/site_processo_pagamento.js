﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.processo)
        $.processo = {};

    $.processo.prazoPagamento = 0;

    // Funcoes do namespace processo.lista
    $.processo.pagamento = {

        variaveis: function() { },

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Processos > Processo Pagamento");

            // Layout dos botões
            $("#site_processo_pagamento button").button();

            // Corrige caminho se a opção for Atacadão
//            $("#rdbOrigemAtacadao").change(function() {
//                $("#txtDiretorioFinanceiro").val($.processo.pagamento.variaveis.DiretorioExportacaoMateraAtacadao);
//            });

            // Corrige caminho se a opção for Extrato
//            $("#rdbOrigemExtrato").change(function() {
//                $("#txtDiretorioFinanceiro").val($.processo.pagamento.variaveis.DiretorioExportacaoMateraExtrato);
//            });

            // Recebe configurações
            executarServico(
                "ReceberConfiguracaoGeralRequest", {},
                function(data) {

                    if (data.ConfiguracaoGeralInfo) {

                        $("#rdbOrigemExtrato").attr("checked", true);
                        $("#txtArquivoFinanceiro").val("MATERA.TXT");
                        $("#txtDiretorioFinanceiro").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoMateraExtrato);
                        $.processo.prazoPagamento = data.ConfiguracaoGeralInfo.PrazoPagamento;
                        $.processo.pagamento.variaveis.DiretorioExportacaoMateraExtrato = data.ConfiguracaoGeralInfo.DiretorioExportacaoMateraExtrato;
                        $.processo.pagamento.variaveis.DiretorioExportacaoMateraAtacadao = data.ConfiguracaoGeralInfo.DiretorioExportacaoMateraFinanceiro;
                    }
                });


            // Processamento
            $("#cmdProcessarPagamento").click(function() {
                var nomeArquivo;
                var diretorio;
                var processoExtrato = false;
                var processoAtacadao = false;
                var caminho = null;

                if ($("#txtArquivoFinanceiro").val() != "")
                    nomeArquivo = $("#txtArquivoFinanceiro").val();

                if ($("#txtDiretorioFinanceiro").val() != "")
                    diretorio = $("#txtDiretorioFinanceiro").val();

                if (nomeArquivo != "" && diretorio != "")
                    caminho = diretorio + nomeArquivo;

                if ($("#rdbOrigemExtrato:checked").length > 0)
                    processoExtrato = true;

                if ($("#rdbOrigemAtacadao:checked").length > 0)
                    processoAtacadao = true;

                // Monta o request
                var request = {
                    ExecutarAssincrono: true,
                    Config: {
                        CaminhoArquivoMateraFinanceiro: $("#txtDiretorioFinanceiro").val() != "" ? $("#txtDiretorioFinanceiro").val() : null,
                        PrazoPagamento: $.processo.prazoPagamento,
                        ProcessarExtrato: true,
                        ProcessarAtacadao: false
                    }
                };
                // 1447 - ANALISE
                // Pede a execucao do processo do pagamento
                executarServico("ExecutarProcessoPagamentoRequest", request, function(data) {

                    // Mostra popup de sucesso
                    popup({
                        titulo: "Processo Pagamento",
                        icone: "sucesso",
                        mensagem: "Processo " + data.CodigoProcesso + " iniciado.",
                        callbackOK: function() { }
                    });
                });
            });
        }
    };

    // Dispara a carga
    $.processo.pagamento.load();

});