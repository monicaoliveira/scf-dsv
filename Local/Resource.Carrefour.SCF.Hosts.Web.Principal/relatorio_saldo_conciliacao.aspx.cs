﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class relatorio_saldo_conciliacao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.lblFiltroDataConciliacaoDe.Text = !string.IsNullOrEmpty(this.Request["DataInicioConciliacao"]) ? this.Request["DataInicioConciliacao"] : "Qualquer";
                this.lblFiltroDataConciliacaoAte.Text = !string.IsNullOrEmpty(this.Request["DataFimConciliacao"]) ? this.Request["DataFimConciliacao"] : "Qualquer";
                this.lblDataHora.Text = DateTime.Now.ToString("dd/MM/yyyy - HH:mm");

                if (!string.IsNullOrEmpty(this.Request["DataInicioConciliacao"]) && !string.IsNullOrEmpty(this.Request["DataInicioConciliacao"]))
                    this.PopularRelatorio();
            }
        }

        protected void PopularRelatorio()
        {
            string codSessao = null;
            if (Session["CodigoSessao"] != null)
            {
                codSessao = Session["CodigoSessao"].ToString();
            }
            else
            {
                codSessao = "Sem Sessão";
            }


            ListarRelatorioConciliacaoContabilRequest req = new ListarRelatorioConciliacaoContabilRequest();
            req.FiltroDataInicioConciliacao = Convert.ToDateTime(this.Request["DataInicioConciliacao"]);
            req.FiltroDataFimConciliacao = Convert.ToDateTime(this.Request["DataFimConciliacao"]);
            req.CodigoSessao = codSessao;

            ListarRelatorioConciliacaoContabilResponse res =
                Mensageria.Processar<ListarRelatorioConciliacaoContabilResponse>(req);

            this.litSaldoAnterior.Text = res.SaldoAnterior < 0 ? string.Concat("(", res.SaldoAnterior.ToString("n"), ")") : res.SaldoAnterior.ToString("n");
            this.litSaldoFinal.Text = res.SaldoFinal < 0 ? string.Concat("(", res.SaldoFinal.ToString("n"), ")") : res.SaldoFinal.ToString("n");

            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (res.Resultado.Count == 0)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Sem dados para imprimir pelo filtro informado.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Sem dados para imprimir pelo filtro informado.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
            else if (res.Resultado.Count > 1000)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }

            this.rptResultado.DataSource = res.Resultado;
            this.rptResultado.DataBind();

        }
    }
}
