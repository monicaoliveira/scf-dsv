﻿$(document).ready(function() {

    // Namespace 
    $.configuracaoCamposEnvioCCI = {};
    
    // Funcoes do namespace
    $.configuracaoCamposEnvioCCI.lista = {

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Tipos de Registro > Parametrização de campos para CCI");

            // Estilo dos botões
            $("#site_configuracao_envio_campos_para_cci_lista button").button();

            // Inicializa forms
            $("#site_configuracao_envio_campos_para_cci_lista").Forms("initializeFields");
            
            // Inicializa lista de configuracoes
            $("#tbCamposEnvioCci").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 320,
                colNames: ["Código", "Campos", "Enviar a CCI?", ""],
                colModel: [
                    { name: "CodigoCampoEnvioCCI", index: 0, hidden: true, editable: true },
                    { name: "NomeCampoEnvioCCI", index: 3, classes: "grid[property[EnvioCamposCci];required]",  editable: false, editoptions: { maxlength: 10} },
                    { name: "EnviarCCI", index: 4, editable: true, edittype: "select", formatter: "select", editoptions: { value: "true:Sim;false:Não" } } ,
                    { name: "Acao", index: 5, align: "center", editable: false, width: 25 }
                ],
                sortname: "Campos"
            });

            // Inicializa edicao do grid
            $.grid.inicializar({
            grid: $("#tbCamposEnvioCci"),
                modeloLinha: { Ativo: true },
                colunaId: "CodigoCampoEnvioCCI",
                callbackRestaurar: $.configuracaoCamposEnvioCCI.lista.restaurar,
                callbackEditar: $.configuracaoCamposEnvioCCI.lista.salvar,
                permiteApenasAlterar: true  
            });

            // Pede lista de configuracoes
            $.configuracaoCamposEnvioCCI.lista.listarCamposEnvioCCI(true);
            $("#site_configuracao_tipo_registro_lista").find("select").css("width", "100%");

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {     
                $("#tbCamposEnvioCci").setGridWidth($(window).width() - margemEsquerda); 
                $("#tbCamposEnvioCci").setGridHeight($(window).height() - margemRodape); 
            }).trigger("resize"); 

        },
        
        // --------------------------------------------------------------------
        //  listarCamposEnvioCCI: lista os configuracoes
        // --------------------------------------------------------------------
        listarCamposEnvioCCI: function(listaAtivos) {

            $("#tbCamposEnvioCci").clearGridData();

            // Executa Serviço
            executarServico(
                "ListarConfiguracaoEnvioCamposCCIRequest",
                { FiltroEnviarCCI: listaAtivos },
                function(data) {
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var camposEnvioCCI = data.Resultado[i];
                        $("#tbCamposEnvioCci").jqGrid("addRowData", "tbCamposEnvioCci_" + camposEnvioCCI.CodigoCampoEnvioCCI, camposEnvioCCI);
                        $.grid.esconderBotoes("tbCamposEnvioCci_" + camposEnvioCCI.CodigoCampoEnvioCCI, !listaAtivos);
                    }
            });
        },
        
        // --------------------------------------------------------------------
        //  salvar: salva uma nova configuracao
        // --------------------------------------------------------------------
        salvar: function(param) {
            
            // Cria request
            var request = {};

            // Pede validação necessária
            $.configuracaoCamposEnvioCCI.lista.validar($("#tbCamposEnvioCci"), param, function() {

                // Envia parametros para serviço
                request.ConfiguracaoEnvioCamposCCIInfo = param.valores;

                executarServico(
                "SalvarConfiguracaoEnvioCamposCCIRequest",
                request,
                function(data) {
                    param.callbackOK({
                    rowid: "tbCamposEnvioCci_" + data.ConfiguracaoEnvioCamposCCIInfo.CodigoCampoEnvioCCI,
                        valores: data.tbCamposEnvioCci
                    });
                });
            },
            function() {
                
            });
        },

        // --------------------------------------------------------------------
        //  salvarNovo: salva uma nova configuracao
        // --------------------------------------------------------------------
        salvarNovo: function(param) {

            // apresentar mensagem
            popup({
                titulo: "Salvar Campos Envio CCI",
                icone: "erro",
                mensagem: "Não é permitido inserir novos campos",
                callbackOK: function() { }
            });

            
        },
        
        // --------------------------------------------------------------------
        //  remover: remove uma configuracao do tipo de registro 
        // --------------------------------------------------------------------
        remover: function(param) {
            
            // Pede para inativar um campo para envio ao CCI
            executarServico(
                "RemoverConfiguracaoEnvioCamposCCIRequest",
                { CodigoCampoEnvioCCI: param.rowid },
                function(data) {
                    param.callbackOK();
            });
        },

        // --------------------------------------------------------------------
        //  restaurar: restaura uma configuracao do tipo de registro 
        // --------------------------------------------------------------------
        restaurar: function(param) {
            
            // Pede para reativar um campo para envio ao CCI
            executarServico(
                "RemoverConfiguracaoEnvioCamposCCIRequest",
                { CodigoCampoEnvioCCI: param.rowid, Ativar: true },
                function(data) {
                    param.callbackOK();
            });
        },

        // --------------------------------------------------------------------
        //  validar: valida campos necessários do cadastro de configuracao de tipo de registro
        // --------------------------------------------------------------------
        validar: function(lGrid, lParam, CallbackOK, CallbackErro) {
            
            // Cria criticas
            var criticas = new Array();
            
            // Realiza as validações
            criticas = $.grid.validate({ pGrid: lGrid, param: lParam, returnErrors: true, errors: criticas });

            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                CallbackErro();
            }
            else
                CallbackOK();
        }
    };

    // Pede a inicializacao
    $.configuracaoCamposEnvioCCI.lista.load();

});