﻿<%@ Page Title="Administrativo - Cadastro de Grupos" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="adm_grupo_cadastro.js"></script>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
<!-- ====================================================================================== -->
<!-- A PRIMEIRA TELA - Grid contendo todos os grupos cadastrados -->
<!-- ====================================================================================== -->
    <div id="grupo_lista">
        <div class="lista">
            <table id="tbGrupoLista"></table>
            <div = class="botoes">
                <button id="cmdNovoGrupo">Novo</button>
                <button id="cmdEditarGrupo">Editar</button>
                <button id="cmdRemoverGrupo">Remover</button>
                <button id="cmdAtualizarLista">Atualizar Lista</button>
            </div>
        </div>
    </div>    
<!-- ====================================================================================== -->
<!-- A SEGUNDA TELA - Popup com abas DETALHE, PERFIS e PERMISSOES -->
<!-- ====================================================================================== -->
    <div id="grupo_edicao">
        <ul>
            <li><a href="#grupo_detalhe">Detalhe</a></li>
            <li><a href="#grupo_perfis">Perfis</a></li>
            <li><a href="#grupo_permissoes">Permissões</a></li>
        </ul>
        
        <div id="grupo_detalhe" class="cmxform">
            <fieldset>
                <legend>Detalhe do Grupo de Usuários</legend> <!-- 1477 - Ajuste na tela e inclusao do codigo -->
                <br/>
                <li>
                    <label style="text-align: left; width:100px" >GRUPO - Codigo</label>
                    <input type="text" id="codigoUsuarioGrupo" value="" readonly />
                </li>
                <li>
                    <label style="text-align: left; width:100px">GRUPO - Nome</label>
                    <input type="text" id="nomeUsuarioGrupo" value="" />
                </li>
            </fieldset>
        </div>
        
        <div id="grupo_perfis">
                <table id="tbGrupoPerfilLista"></table>
                <button id="cmdAdicionarPerfil">Adicionar</button>
                <button id="cmdRemoverPerfil">Remover</button>
        </div>
        
        <div id="grupo_permissoes">
            <table id="tbGrupoPermissaoLista"></table>
            <button id="cmdAdicionarPermissao">Adicionar</button>
            <button id="cmdRemoverPermissao">Remover</button>
        </div>
    </div>
    
<!-- ====================================================================================== -->
<!-- GRID DE PERMISSÕES -->
<!-- ====================================================================================== -->
    <div id="permissao_lista"> 
        <div class="lista">                                         <!-- 1477 INCLUINDO BORDA NO GRID -->
            <table id="tbPermissaoLista"></table> 
            <br/>                                                   <!-- 1477 pulando linha -->
            <br/>                                                   <!-- 1477 pulando linha -->
            <label for="cmbTipoPermissao">Tipo da Permissão:</label>
            <select id="cmbTipoPermissao">
                <option value="0">Permitir</option>
                <option value="1">Negar</option>
            </select>
        </div>            
    </div>
<!-- ====================================================================================== -->
<!-- GRID DE PERFIS -->
<!-- ====================================================================================== -->
    <div id="perfil_lista">
        <div class="lista">                                         <!-- 1477 INCLUINDO BORDA NO GRID -->
            <table id="tbPerfilLista"></table>
        </div>
    </div>
<!-- ====================================================================================== -->
<!-- CONFIRMAR EXCLUSAO -->
<!-- ====================================================================================== -->
    <div id="confirmacaoExclusao">
        <span>Confirma a exclusão deste item?</span>
    </div>
<!-- ====================================================================================== -->
<!-- ALERTA -->
<!-- ====================================================================================== -->
    <div id="mensagemAlerta">
        <span id="textoAlerta"></span>
    </div>
</asp:Content>