﻿$(document).ready(function() 
{   if (!$.configuracao) $.configuracao = {};
    $.configuracao.geral = 
    {   variaveis: {}
    ,   load: function() 
        {
            $("#subtitulo").html("Configurações > Parâmetros de Sistema");
            $("#site_configuracao_geral button").button();
            $("#configuracao_geral_tabs").tabs();
            $("#site_configuracao_geral").Forms("initializeFields");
            carregarPermissoes(function() {$("#site_configuracao_geral").Forms("applyPermissions");});
//========================================================================================================================
// 1402 - movendo para ca - LIMPA TODOS OS CAMPOS e CARREGA DADOS DA TELA
//========================================================================================================================
            $("#site_configuracao_geral").Forms("clearData");
            $.configuracao.geral.carregar();            
//========================================================================================================================
// ABA - Processo Extrato
//========================================================================================================================
            $("#txtHoraLeituraExtrato_De").timepicker({showSecond: false,timeFormat: 'hh:mm'});
            $("#txtHoraLeituraExtrato_Ate").timepicker({showSecond: false,timeFormat: 'hh:mm'});
            $("#txtHoraLeituraExtrato_Ate").mask("99:99");
            $("#txtHoraLeituraExtrato_De").mask("99:99");
//========================================================================================================================
// ABA - Processo Atacadao
//========================================================================================================================
            $("#txtHoraLeituraAtacadao_De").mask("99:99");
            $("#txtHoraLeituraAtacadao_Ate").mask("99:99");
            $("#txtHoraLeituraAtacadao_De").timepicker({showSecond: false,timeFormat: 'hh:mm'});
            $("#txtHoraLeituraAtacadao_Ate").timepicker({showSecond: false,timeFormat: 'hh:mm'});
            $("#cmdSalvarConfiguracoes").click(function() 
            {   $.configuracao.geral.salvar(function() 
                {   popup(
                    {   titulo: "Salvar Configurações"
                    ,   icone: "sucesso"
//1441              ,   mensagem: "Configurações salvas com sucesso! Reinicie Captura Automatica de Arquivos para que as alterações tenham efeito"
                    ,   mensagem: "Configurações salvas com sucesso! A próxima captura automatica de arquivos já ira considerar esta alteração"
                    ,   callbackOK: function() { }
                    });
                });
//1402                $.configuracao.geral.carregar();       
            });
//========================================================================================================================
// 1402 - movendo - LIMPA TODOS OS CAMPOS e CARREGA DADOS DA TELA
//========================================================================================================================
//            $("#site_configuracao_geral").Forms("clearData");
//            $.configuracao.geral.carregar();            
        }
//========================================================================================================================
// funcao
//========================================================================================================================
    ,   carregar: function(callback) 
        {   executarServico("ReceberConfiguracaoGeralRequest",{},function(data) {$.configuracao.geral.variaveis.configuracaoGeralInfo = data.ConfiguracaoGeralInfo;
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10) { dd = '0' + dd }
                if (mm < 10) { mm = '0' + mm }
                var today = dd + '/' + mm + '/' + yyyy;

                var pastdays = new Date();
                pastdays.setDate(pastdays.getDate() - data.ConfiguracaoGeralInfo.DiasIgnorarExpurgo);

                var dd1 = pastdays.getDate();
                var mm1 = pastdays.getMonth() + 1;
                var yyyy1 = pastdays.getFullYear();
                if (dd1 < 10) { dd1 = '0' + dd1 }
                if (mm1 < 10) { mm1 = '0' + mm1 }
                var pastdays = dd1 + '/' + mm1 + '/' + yyyy1;
                data.ConfiguracaoGeralInfo.DePeriodo = pastdays;
                data.ConfiguracaoGeralInfo.AtePeriodo = today;
                
                var nextdays = new Date();
                nextdays.setDate(nextdays.getDate() + data.ConfiguracaoGeralInfo.DiasAguardarExpurgo);
                var dd2 = nextdays.getDate();
                var mm2 = nextdays.getMonth() + 1;
                var yyyy2 = nextdays.getFullYear();
                if (dd2 < 10) { dd2 = '0' + dd2 }
                if (mm2 < 10) { mm2 = '0' + mm2 }
                var nextdays = dd2 + '/' + mm2 + '/' + yyyy2;
                data.ConfiguracaoGeralInfo.ProxExecucao = nextdays;
                
                $("#site_configuracao_geral").Forms("setData", { dataObject: data.ConfiguracaoGeralInfo });

                if (data.ConfiguracaoGeralInfo) 
                {   if (data.ConfiguracaoGeralInfo.EnviarCessaoAutomatico)
                        $("#cmbEnviarCessao").val("true");
                    else
                        $("#cmbEnviarCessao").val("false");

                    if (data.ConfiguracaoGeralInfo.EnviarCessaoAutomaticoAtacadao)
                        $("#cmbEnviarCessaoAta").val("true");
                    else
                        $("#cmbEnviarCessaoAta").val("false");

                    if (data.ConfiguracaoGeralInfo.EnviarCessaoAutomaticoGaleria)
                        $("#cmbEnviarCessaoGal").val("true");
                    else
                        $("#cmbEnviarCessaoGal").val("false");
                    //  melhorias-2017 - nao será mais possível efetuar essa parametrizacao. A geracao de pagamentos será feita através do relatorio de vencimentos.  
//                    if (data.ConfiguracaoGeralInfo.EnviarMateraFinanceiroAutomatico)
//                        $("#cmbEnviarMatera").val("true");
//                    else
//                        $("#cmbEnviarMatera").val("false");
//SCF 1473 - ID 18
//                    if (data.ConfiguracaoGeralInfo.HomologacaoTSYS)
//                        $("#cmbHomologacaoTSYS").val("true");
//                    else
//                        $("#cmbHomologacaoTSYS").val("false");

                    if (data.ConfiguracaoGeralInfo.ReprocessarNSUProcessado)
                        $("#cmdReprocessarNSUprocessado").val("true");
                    else
                        $("#cmdReprocessarNSUprocessado").val("false");

                    if (data.ConfiguracaoGeralInfo.ReprocessarNSUCessionado)
                        $("#cmdReprocessarNSUcessionado").val("true");
                    else
                        $("#cmdReprocessarNSUcessionado").val("false");
                        
                    if (!data.ConfiguracaoGeralInfo.HoraLeituraDeTSYS && data.ConfiguracaoGeralInfo.HoraLeituraDeTSYS == "")
                        $("#txtHoraLeituraExtrato_De").val("00:00");

                    if (!data.ConfiguracaoGeralInfo.HoraLeituraDeCCI && data.ConfiguracaoGeralInfo.HoraLeituraDeCCI == "")
                        $("#txtHoraLeituraExtrato_Ate").val("23:59");

                    if (!data.ConfiguracaoGeralInfo.HoraLeituraDeAtacadao && data.ConfiguracaoGeralInfo.HoraLeituraDeAtacadao == "")
                        $("#txtHoraLeituraAtacadao_De").val("00:00");

                    if (!data.ConfiguracaoGeralInfo.HoraLeituraAteAtacadao && data.ConfiguracaoGeralInfo.HoraLeituraAteAtacadao == "")
                        $("#txtHoraLeituraAtacadao_Ate").val("23:59");
                } 
                else 
                {   $("#txtHoraLeituraExtrato_De").val("00:00");
                    $("#txtHoraLeituraExtrato_Ate").val("23:59");
                    $("#txtHoraLeituraAtacadao_De").val("00:00");
                    $("#txtHoraLeituraAtacadao_Ate").val("23:59");

                }
                if (callback)
                    callback();
            });
        }        
//========================================================================================================================
// botao cmdSalvarConfiguracoes
//========================================================================================================================
    ,   salvar: function(callback) 
        {   $.configuracao.geral.validar(function() 
            {   configuracaoGeralInfo = $.configuracao.geral.variaveis.configuracaoGeralInfo; // 1430 estava {} e por isto nao tinha o STATUS_AGENDADOR gravando 0 indevidamente
                $("#site_configuracao_geral").Forms("getData", { dataObject: configuracaoGeralInfo });
                executarServico("SalvarConfiguracaoGeralRequest",{ ConfiguracaoGeralInfo: configuracaoGeralInfo },function(data) 
                {       if (callback)
                            callback();
                });
            }
            ,   function() { });
        }
//========================================================================================================================
// botao cmdSalvarConfiguracoes funcao
//========================================================================================================================
    ,   validar: function(callbackOk, callbackErro) 
        {   var criticas = new Array();
            criticas = $("#site_configuracao_geral").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });
            var inputs = $("input[minlength]");
            var opcoes = $.grid.formClassDefaultOptions;
            var dAguardar = parseInt(document.getElementById('diasAguardar').value, 10);
            var dIgnorar = parseInt(document.getElementById('diasIgnorar').value, 10);
            
            if ((dAguardar == null || dAguardar == "" || dAguardar == 0) ||
                 (dIgnorar == null || dIgnorar == "" || dIgnorar == 0)) {
                var criticas = [];
                criticas.push({ message: opcoes.errorDouble, element: $(this) });
            }
                        
            for (var i = 0; i < inputs.length; i++) {
                var minlength = inputs[i].attributes.getNamedItem("minlength").value;
                var length = inputs[i].value.length;
                if ((minlength > length) && (length != 0)) {
                    criticas.push({ element: "",
                        label: "",
                        labelValeu: inputs[i].id.substring(3, inputs[i].id.length),
                        message: inputs[i].id.substring(3, inputs[i].id.length) + ": Preenchimento mínimo de " + minlength + " caracter(s)"
                    });
                }
            }
            if (criticas[0]) 
            {
                $.Forms.showValidationForm({ errors: criticas });
                callbackErro();

            } 
            else
                callbackOk();
        }
    };
//========================================================================================================================
// LOAD DA TELA
//========================================================================================================================
    $.configuracao.geral.load();

});
//========================================================================================================================
// funcao usada no ASPX para informar a data do PROCESSO EXPURGO
//========================================================================================================================
function calcular() 
{   var valor1 = parseInt(document.getElementById('diasIgnorar').value, 10);

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    var today = dd + '/' + mm + '/' + yyyy;

    var pastdays = new Date();
    pastdays.setDate(pastdays.getDate() - valor1);

    var dd1 = pastdays.getDate();
    var mm1 = pastdays.getMonth() + 1;
    var yyyy1 = pastdays.getFullYear();
    if (dd1 < 10) { dd1 = '0' + dd1 }
    if (mm1 < 10) { mm1 = '0' + mm1 }
    var pastdays = dd1 + '/' + mm1 + '/' + yyyy1;

    document.getElementById('dePeriodo').value = pastdays;
    document.getElementById('atePeriodo').value = today;

    var valor2 = parseInt(document.getElementById('diasAguardar').value, 10);
    var nextdays = new Date();
    nextdays.setDate(nextdays.getDate() + valor2);
    var dd2 = nextdays.getDate();
    var mm2 = nextdays.getMonth() + 1;
    var yyyy2 = nextdays.getFullYear();
    if (dd2 < 10) { dd2 = '0' + dd2 }
    if (mm2 < 10) { mm2 = '0' + mm2 }
    var nextdays = dd2 + '/' + mm2 + '/' + yyyy2;

    document.getElementById('proxExecucao').value = nextdays
}