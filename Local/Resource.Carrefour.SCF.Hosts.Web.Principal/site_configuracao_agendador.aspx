﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="site_configuracao_agendador.js"></script>
    <link href="site_configuracao_agendador.css" rel="stylesheet" type="text/css" />
    <title>SCF - Agendador </title>
    <style type="text/css">
        legend
        {
            font-weight:bold;	
        }
        
        .style1
        {
            width: 25%;
        }
    </style>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    
    <div id="site_configuracao_agendador" style="margin: 30px;">
    
        <fieldset>
            <legend>Serviço de captura de arquivo</legend>
            <table width="100%">               
                <tr>
                    <td class="style1"><label>Status do serviço de captura:</label></td>
                    <td ><input type="text" class="forms[property[StatusServico]] " disabled /></td>
                </tr>
                <tr>
                    <td class="style1"><label>Proxima Execução:</label></td>
                    <td ><input type="text"  class="forms[property[DataProximaExecucao]] "  disabled/></td>
                </tr>
                <tr>
                    <td class="style1"><label>Último processo do Atacadao:</label></td>
                    <td ><input type="text"  class="forms[property[CodigoProcessoAtacadao]] "  disabled/></td>
                </tr>
                <tr>
                    <td class="style1"><label>Observação Atacadao:</label></td>
                    <td><input type="text"  class="forms[property[ObservacaoAtacadao]]"   disabled/></td>
                </tr>
                <tr>
                    <td class="style1"><label>Observação Extrato:</label></td>
                    <td><input type="text"  class="forms[property[ObservacaoExtrato]]"   disabled/></td>
                </tr>
                <tr>
                    <td class="style1"><label>Observação Retorno Cessao:</label></td>
                    <td><input type="text"  class="forms[property[ObservacaoRetornoCessao]]" disabled/></td>
                </tr>
                <tr>
                    <td class="style1"><label>Observação Geral:</label></td>
                    <td><input type="text"  class="forms[property[ObservacaoGeral]]"  disabled/></td>
                </tr>

                <tr>
                    <td class="style1">
                        <button id="cmdAtualizar">Atualizar</button>
                        <button id="cmdIniciar">Iniciar</button>
                        <button id="cmdParar">Parar</button>
                    </td>
                </tr>
            </table> 
        </fieldset>
        <br />
<%--        <fieldset>
            <legend>Serviço Expurgo</legend>
            <div id="expurgo_detalhe">
                <table width="100%">                   
                    <tr>
                        <td class="style1"><label>Status do Serviço de Expurgo:</label></td>
                        <td ><input type="text" class="forms[property[StatusServico]] " disabled /></td>
                    </tr>
                    <tr>
                        <td class="style1"><label>Proxima Execução:</label></td>
                        <td ><input type="text"  class="forms[property[DataProximaExecucao]] " disabled /></td>
                    </tr>                             
                    <tr>
                        <td class="style1"><label>Última Execução:</label></td>
                        <td ><input type="text"  class="forms[property[DataUltimaExecucao]] " disabled/></td>
                    </tr>   --%>                            
                    <%--<tr>
                        <td class="style1"><label>Quantidade de Dias a Aguardar:</label></td>
                        <td><input type="text"  class="forms[property[ObservacaoExtrato]]"  /></td>
                    </tr>
                        <tr>
                        <td class="style1"><label>Quantidade de Dias a Ignorar:</label></td>
                        <td><input type="text"  class="forms[property[ObservacaoRetornoCessao]]" disabled/></td>
                    </tr>--%>
                    <%--<tr>
                        <td class="style1"><label>Observação Geral:</label></td>
                        <td><input type="text"  class="forms[property[ObservacaoGeral]]" disabled/></td>
                    </tr>
                    <tr>
                        <td class="style1"><label>Observação Expurgo:</label></td>
                        <td><input type="text"  class="forms[property[ObservacaoExpurgo]]" disabled/></td>
                    </tr>
                    <tr>
                        <td class="style1" colspan="3">
                            <button id="cmdAtualizarExpurgo">Atualizar</button>
                            <button id="cmdIniciarExpurgo">Iniciar</button>
                            <button id="cmdPararExpurgo">Parar</button>
                            <button id="cmdExpurgoAgora">Executar Expurgo Agora</button>
                            
                        </td>
                    </tr>
                </table>    
            </div>
        </fieldset>    --%>    
        
    </div>

</asp:Content>
