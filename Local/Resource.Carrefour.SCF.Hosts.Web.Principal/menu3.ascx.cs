﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Contratos.Interface.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Text;
using Resource.Framework.Contratos.Interface.Dados;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class menu3 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        protected override void Render(HtmlTextWriter writer)
        {
            MensagemResponseBase retorno = null;

            try
            {
                if (Session["codigoSessao"] != null)
                {
                    ReceberArvoreComandosInterfaceResponse resposta =
                        Mensageria.Processar<ReceberArvoreComandosInterfaceResponse>(
                            new ReceberArvoreComandosInterfaceRequest()
                            {
                                CodigoSessao = (string)Session["codigoSessao"],
                                CodigoGrupoComandoInterface = "default"
                            });

                    StringBuilder menus = new StringBuilder();
                    menus.Append("<table><tr><td>");
                    menus.Append(@"<ul class=""menu"">");
                    StringBuilder subMenus = new StringBuilder();
                    int percorridos = 0;
                    foreach (ComandoInterfaceInfo comandoInterface in resposta.ComandosInterfaceRaiz)
                    {
                        // Possui icone?
                        string icone = "";
                        if (comandoInterface.Tag2 != null && comandoInterface.Tag2 != "")
                        {
                            icone = @"<img id=""iconHomeLaranja"" src=""" + comandoInterface.Tag2 + @""" />";
                        }

                        // Verifica se menu ou submenu
                        if (comandoInterface.Tag != null && comandoInterface.Tag != "")
                        {
                            string id = comandoInterface.Tag;
                            if (++percorridos == resposta.ComandosInterfaceRaiz.Count)
                                menus.Append(@"<li><a href=""" + id + @""" class=""last"">" + icone + comandoInterface.Nome + "</a></li>");
                            else
                                menus.Append(@"<li><a href=""" + id + @""">" + icone + comandoInterface.Nome + "</a></li>");
                        }
                        else
                        {
                            string id = "#" + comandoInterface.CodigoComandoInterface;
                            if (++percorridos == resposta.ComandosInterfaceRaiz.Count)
                                menus.Append(@"<li><a href=""#"" name=""" + id + @""" class=""last temFilhos"">" + icone + comandoInterface.Nome + "</a></li>");
                            else
                                menus.Append(@"<li><a href=""#"" name=""" + id + @""" class=""temFilhos"">" + icone + comandoInterface.Nome + "</a></li>");


                            subMenus.Append(@"<ul class=""submenu sub_escondido"" id=""" + comandoInterface.CodigoComandoInterface + @""">");

                            int filhosPercorridos = 0;
                            foreach (ComandoInterfaceInfo submenu in comandoInterface.Filhos)
                            {
                                if (++filhosPercorridos == comandoInterface.Filhos.Count)
                                    subMenus.Append(@"<li><a href=""" + submenu.Tag + @""" class=""last"">" + submenu.Nome + "</a></li>");
                                else
                                    subMenus.Append(@"<li><a href=""" + submenu.Tag + @""">" + submenu.Nome + "</a></li>");
                            }
                            subMenus.Append("</ul>");

                        }
                    }
                    menus.Append("</ul>");
                    menus.Append("</td></tr>");

                    StringBuilder tudo = new StringBuilder();
                    tudo.Append(menus);
                    tudo.Append(@"<tr style=""height: 27px;""><td>");
                    tudo.Append(subMenus);
                    tudo.Append("</td></tr></table>");

                    writer.Write(tudo.ToString());
                }
                else
                {
                    // Cria mensagem generica informando o erro
                    retorno =
                        new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                            DescricaoResposta = "Sessão Inválida"
                        };
                }
            }
            catch (Exception ex)
            {
                // Cria mensagem generica informando o erro
                retorno =
                    new MensagemErroResponse()
                    {
                        StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                        DescricaoResposta = ex.ToString()
                    };
            }
        }

    }
}