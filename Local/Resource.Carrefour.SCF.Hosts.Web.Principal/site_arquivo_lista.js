﻿$(document).ready(function() {

    // Namespace de arquivo
    if (!$.arquivo)
        $.arquivo = {};

    // Funcoes do namespace arquivo.lista
    $.arquivo.lista = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Consulta > Lista de Arquivos");

            // Layout dos botões
            $("#site_arquivo_lista button").button();
            
            // desabilita o botao de exportacao para CCI
            //$("#site_arquivo_lista .cmdGerarCCI").button({ disabled: true });

            // Inicializa forms
            $("#site_arquivo_lista").Forms("initializeFields");

            // Inicializa Lista de Arquivos
            $("#tbArquivo").jqGrid(
            {
                datatype: "clientSide"
            ,   mtype: "GET"
            ,   sortable: true
            ,   scroll: true
            //,   height: 150 //1353
            //,   width: 900 //1353
            ,   sortname: "CodigoArquivo"
            ,   sortorder: "desc" //1353
            ,   shrinkToFit: false            //1353
            ,   viewrecords: true
            ,   colNames: 
                [
                    "ID Arq"
                ,   "Sts Arq"
                ,   "Data Inclusão"
                ,   "ID Proc"
                ,   "Sts Processo"
                ,   "Grupo"
                ,   "Tipo Arquivo"
                ,   "Nome Arquivo"
                ]
            ,   colModel: 
                [ { name: "CodigoArquivo",  index: "CodigoArquivo",     sorttype: "integer", key: true, width: 50, align: "center" }
                , { name: "StatusArquivo",  index: "StatusArquivo",     width: 100 }
                , { name: "DataInclusao",   index: "DataInclusao",      width: 150 }
                , { name: "CodigoProcesso", index: "CodigoProcesso",    width: 50,  align: "center" }
                , { name: "StatusProcesso", index: "StatusProcesso",    width: 100 }
                , { name: "SiglaGrupo",     index: "SiglaGrupo",        width: 50,  align: "center" }
                , { name: "TipoArquivo",    index: "TipoArquivo",       width: 150 }
                , { name: "NomeArquivo",    index: "NomeArquivo",       width: 350 }
                ]
            });

            // Inicializa o dialog de detalhe
            $("#site_arquivo_info").dialog({
                autoOpen: false,
                height: 200,
                width: 400,
                modal: true,
                title: "Detalhe do Arquivo",
                buttons: {
                    "Continuar": function() {

                        // Limpa input do caminho de arquivo
                        //$("#txtCaminhoArquivo").val("");

                        // Fecha o dialog
                        $("#site_arquivo_info").dialog("close");

                        // Se tem o callback, chama
                        if ($("#site_arquivo_lista").data("callbackCaminhoArquivo"))
                            $("#site_arquivo_lista").data("callbackCaminhoArquivo")();

                    },
                    "Fechar": function() {

                        // Limpa input do caminho de arquivo
                        $("#txtCaminhoArquivo").val("");

                        // Fecha o dialog
                        $("#site_arquivo_info").dialog("close");
                    }
                }
            });

            $("#tbArquivo").click(function(rowid) {
                if (    (
                            $('#tbArquivo').jqGrid('getCell', $('#tbArquivo').jqGrid('getGridParam', 'selrow'), 'StatusProcesso') == "Finalizado"   ||
                            $('#tbArquivo').jqGrid('getCell', $('#tbArquivo').jqGrid('getGridParam', 'selrow'), 'StatusProcesso') == "Cessionado" //1353
                        )
                    &&
                    $('#tbArquivo').jqGrid('getCell', $('#tbArquivo').jqGrid('getGridParam', 'selrow'), 'TipoArquivo') == "ArquivoTSYS"

                ) 
                {
                    $("#site_arquivo_lista .cmdGerarCCI").button("option", "disabled", false);
//1353                    $("#site_arquivo_lista .cmdSimularRetorno").button("option", "disabled", false);
                }
                else 
                {
                    $("#site_arquivo_lista .cmdGerarCCI").button("option", "disabled", true);
//1353                    $("#site_arquivo_lista .cmdSimularRetorno").button("option", "disabled", true);
                }

                if (
                    $('#tbArquivo').jqGrid('getCell', $('#tbArquivo').jqGrid('getGridParam', 'selrow'), 'TipoArquivo') == "ArquivoCSU"

                ) {
                    $("#site_arquivo_lista .cmdGerarCSUSemAtacadao").button("option", "disabled", false);
                }
                else {
                    $("#site_arquivo_lista .cmdGerarCSUSemAtacadao").button("option", "disabled", true);
                }
            });

            // BOTÃO FILTRAR
            $("#site_arquivo_lista .cmdFiltrar").click(function() {
                $.arquivo.lista.listar();
            });


            // BOTÃO GERAR ARQUIVO
            $("#site_arquivo_lista .cmdGerarArquivo").click(function() {

                // Verifica se tem arquivo selecionado
                var codigoArquivo = $("#tbArquivo").jqGrid("getGridParam", "selrow");
                if (codigoArquivo) {

                    // Pede a exportacao do arquivo
                    $.arquivo.lista.exportarArquivo(codigoArquivo);

                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            //            // Botão detalhe de arquivo
            //            $("#site_arquivo_lista .cmdDetalhe").click(function() {

            //                // Verifica se tem arquivo selecionado
            //                var codigoArquivo = $("#tbArquivo").jqGrid("getGridParam", "selrow");
            //                var arquivo = $('#tbArquivo').jqGrid('getRowData',codigoArquivo);
            //                if (arquivo.CodigoArquivo) {
            //                
            //                    codigoArquivo = codigoArquivo.substring(codigoArquivo.lastIndexOf("_") + 1);
            //                      
            //                    // Pede a exportacao do arquivo
            //                    $.arquivo.detalhe.abrir(arquivo.CodigoArquivo, arquivo.CodigoProcesso);

            //                } else {

            //                    // Pede para selecionar a linha
            //                    popup({
            //                        titulo: "Atenção",
            //                        icone: "atencao",
            //                        mensagem: "Favor selecionar um arquivo",
            //                        callbackOK: function() { }
            //                    });
            //                }
            //            });

            // Botão Gerar Arquivo CSU sem Atacadão
            $("#site_arquivo_lista .cmdGerarCSUSemAtacadao").click(function() {

                // Verifica se tem arquivo selecionado
                var codigoArquivo = $("#tbArquivo").jqGrid("getGridParam", "selrow");
                if (codigoArquivo) {

                    // Preencher campo com a configuração de Parâmetros (Menu: Configurções->Parâmetros)
                    executarServico(
                        "ReceberConfiguracaoGeralRequest", {},
                        function(data) {

                            if (data.ConfiguracaoGeralInfo) {

                                $("#txtCaminhoArquivo").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoAtacadaoCSUSemAtacadao);
                            }
                        });

                    // Pede a exportacao do arquivo
                    $.arquivo.lista.exportarArquivoCSUSemAtacadao(codigoArquivo);

                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // Botão Gerar Arquivo para CCI
            $("#site_arquivo_lista .cmdGerarCCI").click(function() {

                // Verifica se tem arquivo selecionado
                var codigoArquivo = $("#tbArquivo").jqGrid("getGridParam", "selrow");
                var siglaGrupo = $('#tbArquivo').jqGrid('getCell', $('#tbArquivo').jqGrid('getGridParam', 'selrow'), 'SiglaGrupo');

                if (codigoArquivo) {

                    // Preencher campo com a configuração de Parâmetros (Menu: Configurções->Parâmetros)
                    executarServico(
                        "ReceberConfiguracaoGeralRequest", {},
                        function(data) {

                            if (data.ConfiguracaoGeralInfo) {
                                if (siglaGrupo == "ATA") {
                                    $("#txtCaminhoArquivo").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoATA);
                                } else if (siglaGrupo == "GAL") {
                                    $("#txtCaminhoArquivo").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoGAL);
                                } else {
                                    $("#txtCaminhoArquivo").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoCCI);
                                }
                            }
                        });

                    // Pede a exportacao do arquivo
                    $.arquivo.lista.exportarArquivoCCI(codigoArquivo);

                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            //1353
            /*
            // Botão Gerar Arquivo para Retorno 
            $("#site_arquivo_lista .cmdSimularRetorno").click(function() {

                // Verifica se tem arquivo selecionado
                var codigoArquivo = $("#tbArquivo").jqGrid("getGridParam", "selrow");
                if (codigoArquivo) {

                    // Pede a exportacao do arquivo
                    $.arquivo.lista.exportarSimularRetorno(codigoArquivo);

                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });
            */
            
            //            // Botão Validar
            //            $("#site_arquivo_lista .cmdValidar").click(function() {

            //                // Verifica se tem arquivo selecionado
            //                var codigoArquivo = $("#tbArquivo").jqGrid("getGridParam", "selrow");
            //                if (codigoArquivo) {

            //                    // pede confirmação para continuar
            //                    popup({
            //                        titulo: "Atenção",
            //                        icone: "atencao",
            //                        mensagem: "Tem certeza que deseja validar?",
            //                        callbackSim: function() {

            //                            // Pede a validação do arquivo
            //                            executarServico(
            //                                        "ValidarArquivoRequest",
            //                                        { CodigoArquivo: codigoArquivo },
            //                                        function(data) {

            //                                            // Informa validação efetuada
            //                                            popup({
            //                                                titulo: "Validação efetuada",
            //                                                icone: "sucesso",
            //                                                mensagem: "Validação do arquivo efetuada",
            //                                                callbackOK: function() { }
            //                                            });
            //                                        });
            //                        },
            //                        callbackNao: function() { }
            //                    });
            //                } else {

            //                    // Pede para selecionar a linha
            //                    popup({
            //                        titulo: "Atenção",
            //                        icone: "atencao",
            //                        mensagem: "Favor selecionar uma linha!",
            //                        callbackOK: function() { }
            //                    });
            //                }
            //            });

            //            // Botão Importar Arquivo
            //            $("#site_arquivo_lista .cmdImportarArquivo").click(function() {

            //                // Dispara a importacao de arquivo
            //                $.arquivo.lista.importarArquivo();

            //            });

            //            // Botão Crítica
            //            $("#site_arquivo_lista .cmdListarCriticas").click(function() {
            //                var codigoArquivo = $("#tbArquivo").jqGrid("getGridParam", "selrow");
            //                $.critica.lista.abrir({ FiltroCodigoArquivo: codigoArquivo });
            //            });

            // Limpa lista
            $("#tbArquivo").clearGridData();

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbArquivo").setGridWidth($(window).width() - margemEsquerda);
                $("#tbArquivo").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");

            // Carrega listas de enumeradores
            carregarListas("ArquivoStatusEnum", function() {

                // Preenche combo de categoria
                preencherCombo($("#cmbFiltroStatusArquivo"), "ArquivoStatusEnum");
            });
        },

        // --------------------------------------------------------------------
        //  listar: pede a lista de arquivos
        // --------------------------------------------------------------------
        listar: function() {

            // Limpa a lista
            $("#tbArquivo").jqGrid("clearGridData");

            // Monta o filtro
            var request = {};
            $("#site_arquivo_lista .filtro").Forms("getData", { dataObject: request });

            if (request.FiltroDataInclusao == "")
                request.FiltroDataInclusao = null;

            if (request.FiltroStatusArquivo == "" || request.FiltroStatusArquivo == "?")
                request.FiltroStatusArquivo = null;

            request.FiltroArquivosPendentes = true;
            
            // Pede a lista de arquivos
            executarServico(
                "ListarArquivoRequest",
                request,
                function(data) {

                    // Preenche o resultado
                    for (var i = 0; i < data.Resultado.length; i++)
                        $("#tbArquivo").jqGrid("addRowData", data.Resultado[i].CodigoArquivo, data.Resultado[i]);
                });
        },

        // --------------------------------------------------------------------
        //  exportarArquivo: pede a exportacao do arquivo
        // --------------------------------------------------------------------
        exportarArquivo: function(codigoArquivo, callback) {

            // O callback do dialog de caminho será a rotina de exportação
            $("#site_arquivo_lista").data("callbackCaminhoArquivo", function() {

                // Faz a exportacao
                executarServico(
                    "ExportarArquivoRequest",
                    {
                        CodigoArquivo: codigoArquivo,
                        CaminhoArquivo: $("#txtCaminhoArquivo").val()
                    },
                    function(data) {

                        // Mensagem de sucesso
                        popup({
                            titulo: "Gerar Arquivo",
                            icone: "sucesso",
                            mensagem: "Arquivo gerado com sucesso!",
                            callbackOK: function() {

                                // Se houver, chama callback
                                if (callback)
                                    callback();

                            }
                        });
                    });

            });

            // Mostra o dialog do caminho sem o combo de tipo de arquivo
            $("#divTipoArquivo").hide();
            $("#site_arquivo_info").dialog("open");
        },


        // --------------------------------------------------------------------
        //  exportarArquivoCSUSemAtacadao: pede a exportacao do arquivo CSU sem Atacadao
        // --------------------------------------------------------------------
        exportarArquivoCSUSemAtacadao: function(codigoArquivo, callback) {

            // O callback do dialog de caminho será a rotina de exportação
            $("#site_arquivo_lista").data("callbackCaminhoArquivo", function() {

                // Faz a exportacao
                executarServico(
                    "ExportarCSUSemAtacadaoRequest",
                    {
                        CodigoArquivo: codigoArquivo,
                        CaminhoArquivo: $("#txtCaminhoArquivo").val()
                    },
                    function(data) {

                        // Mensagem de sucesso
                        popup({
                            titulo: "Gerar Arquivo",
                            icone: "sucesso",
                            mensagem: "Arquivo gerado com sucesso!",
                            callbackOK: function() {

                                // Se houver, chama callback
                                if (callback)
                                    callback();

                            }
                        });
                    });

            });

            // Mostra o dialog do caminho sem o combo de tipo de arquivo
            $("#divTipoArquivo").hide();
            $("#site_arquivo_info").dialog("open");
        },


        // --------------------------------------------------------------------
        // Exportar arquivo para a CCi
        // --------------------------------------------------------------------
        exportarArquivoCCI: function(codigoArquivo, callback) {

            // O callback do dialog de caminho será a rotina de exportação
            $("#site_arquivo_lista").data("callbackCaminhoArquivo", function() {


                // Monta o request
                var request = {
                    ExecutarAssincrono: true,
                    CodigoArquivoTSYS: codigoArquivo,
                    CaminhoGeracaoArquivo: $("#txtCaminhoArquivo").val()
                };

                // Pede a execucao do processo do extrato
                executarServico("ExecutarProcessoCCIManualRequest", request, function(data) {

                    // Mostra popup de sucesso
                    popup({
                        titulo: "Processo CCI Manual",
                        icone: "sucesso",
                        mensagem: "Processo " + data.CodigoProcesso + " iniciado.",
                        callbackOK: function() { }
                    });

                });

            });

            // Mostra o dialog do caminho sem o combo de tipo de arquivo
            $("#divTipoArquivo").hide();
            $("#site_arquivo_info").dialog("open");
        },

        // --------------------------------------------------------------------
        // Simular retorno da CCI
        // --------------------------------------------------------------------
        exportarSimularRetorno: function(codigoArquivo, callback) {

            // O callback do dialog de caminho será a rotina de exportação
            $("#site_arquivo_lista").data("callbackCaminhoArquivo", function() {

                // Faz a exportacao
                executarServico(
                    "SimularArquivoRetornoCessaoRequest",
                    {
                        CodigoArquivo: codigoArquivo,
                        CaminhoArquivo: $("#txtCaminhoArquivo").val()
                    },
                    function(data) {

                        // Mensagem de sucesso
                        popup({
                            titulo: "Gerar Arquivo",
                            icone: "sucesso",
                            mensagem: "Arquivo gerado com sucesso!",
                            callbackOK: function() {

                                // Se houver, chama callback
                                if (callback)
                                    callback();

                            }
                        });
                    });

            });

            // Mostra o dialog do caminho sem o combo de tipo de arquivo
            $("#divTipoArquivo").hide();
            $("#site_arquivo_info").dialog("open");
        },

        // --------------------------------------------------------------------
        //  getCellValue: recupera o valor de uma celula
        // --------------------------------------------------------------------
        getCellValue: function(content) {
            var k1 = content.indexOf(' value=', 0);
            var k2 = content.indexOf(' name=', k1);
            var val = '';
            if (k1 > 0) {
                val = content.substr(k1 + 7, k2 - k1 - 6);
            }
            return val;
        },

        // --------------------------------------------------------------------
        //  importarArquivo: importa um arquivo
        // --------------------------------------------------------------------
        importarArquivo: function(callback) {

            // O callback do dialog de caminho será a rotina de exportação
            $("#site_arquivo_lista").data("callbackCaminhoArquivo", function() {

                // Faz a exportacao
                executarServico(
                    "ImportarArquivoRequest",
                    {
                        TipoArquivo: $("#cmbTipoArquivo").val(),
                        CaminhoArquivo: $("#txtCaminhoArquivo").val()
                    },
                    function(data) {

                        // Mensagem de sucesso
                        popup({
                            titulo: "Importação de Arquivo",
                            icone: "sucesso",
                            mensagem: "Arquivo importado com Sucesso! Código do Arquivo: " + data.CodigoArquivo,
                            callbackOK: function() {

                                // Se houver, chama callback
                                if (callback)
                                    callback();

                            }
                        });
                    });

            });

            // Mostra o dialog do caminho com o combo de tipo de arquivo
            $("#divTipoArquivo").show();
            $("#site_arquivo_info").dialog("open");
        }

    };

    // Inicializa a tela
    $.arquivo.lista.load();

});