﻿$(document).ready(function() 
{   if (!$.protocolo)
        $.protocolo = {};
    var vlista = "";
    $.protocolo.filtro = 
    {   variaveis: {}
    ,   load: function(callback) 
    {       $("#subtitulo").html("CONFIGURAÇÕES [Cheque Devolvido]"); //963
            $("#site_configuracao_cadastro_protocolo_cheque_devolvido button").button();
            $("#site_configuracao_cadastro_protocolo_cheque_devolvido").Forms("initializeFields");
            //=================================================================================
            // BOTAO: FILTRAR
            //=================================================================================
            $("#site_configuracao_cadastro_protocolo_cheque_devolvido .cmdFiltrar").click(function() 
            {   $.protocolo.filtro.listar();
            });
            //=================================================================================
            // BOTAO: VISUALIZAR
            //=================================================================================
            $("#site_configuracao_cadastro_protocolo_cheque_devolvido .cmdVisualizar").click(function() //1465
            {   $.protocolo.filtro.recuperarFiltros("link", function(request) 
                {   if (request)
                        $.protocolo.filtro.abrirRelatorio(request)
                });
            });            
            //=================================================================================
            // BOTAO: EXCEL
            //=================================================================================
            $("#site_configuracao_cadastro_protocolo_cheque_devolvido .cmdGerarExcel").click(function() //1465
            {   $.protocolo.filtro.recuperarFiltros(null, function(request) 
                {   if (request) 
                    {   request.Request.TipoRelatorio = "Excel";
                        $.protocolo.filtro.criarProcesso(request);
                    }
                });
            });
            //=================================================================================
            // BOTAO: PDF
            //=================================================================================
            $("#site_configuracao_cadastro_protocolo_cheque_devolvido .cmdGerarPDF").click(function() //1465
            {   $.protocolo.filtro.recuperarFiltros("link", function(request) 
                {   if (request) 
                    {   request.Request.TipoRelatorio = "PDF";
                        $.protocolo.filtro.criarProcesso(request);
                    }
                });
            });
            //=================================================================================
            // GRID: tbProtocolo
            //=================================================================================
            $("#tbProtocolo").jqGrid(
            {   datatype: "clientSide"
            ,   mtype: "GET"
            ,   shrinkToFit: false
            ,   sortname: "ContaCliente"
            ,   sortorder: "asc"
            ,   viewrecords: true
            ,   rowNum: 100000
            ,   colNames: 
                [   "Dt.Proc."
                ,   "Cta Cliente"
                ,   "NSUHOST"
                ,   "NSUHOST Orig."
                ,   "Estabelecimento"
                ,   "Valor Repasse"
                ,   "Dt.Reg.Procotolo"
                ,   "Dt.Estorno"
                ,   "No Protocolo"
                ,   "Tp Estorno" //963
                ,   "PSL"
                ,   "Acao"
                ,   ""
                ]
            ,   colModel: 
                [   { name: "DataProcessamento", sortable: true, width: 80, index: "DataProcessamento", datefmt: "dd/mm/yyyy", sorttype: "integer", align: "center", key: true, editable: false, editrules: { date: true }, editoptions: { dataInit: function(e) { $(e).datepicker(); } }, formatter: formatterDate }
                ,   { name: "ContaCliente", sortable: true, width: 100, index: "ContaCliente", align: "center", editable: false }
                ,   { name: "NsuHost", width: 95, index: "NsuHost", align: "center", editable: false }
                ,   { name: "NsuHostOriginal", width: 100, index: "NsuHostOriginal", align: "center", editable: false }
                ,   { name: "Estabelecimento", width: 250, index: "Estabelecimento", align: "left", editable: false }
                ,   { name: "Valor", width: 100, index: "Valor", align: "right", editable: false, sorttype: "number", formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 2, prefix: "R$ ", defaultValue: "0"}}
                ,   { name: "DataRegProtocolo", width: 95, index: "DataRegProtocolo", classes: "grid[property[DataRegProtocolo];required]", editable: true, editoptions: { maxlength: 8 }, datefmt: "dd/mm/yyyy", align: "center", editoptions: { dataInit: function(e) { $(e).datepicker(); } }, formatter: formatterDate }
                ,   { name: "DataEstorno", width: 95, index: "DataEstorno", classes: "grid[property[DataEstorno];required]", editable: true, editoptions: { maxlength: 8 }, datefmt: "dd/mm/yyyy", align: "center", editoptions: { dataInit: function(e) { $(e).datepicker(); } }, formatter: formatterDate }
                ,   { name: "NumeroProtocolo", width: 100, index: "NumeroProtocolo", classes: "grid[property[NumeroProtocolo];required]", editable: true, editoptions: { maxlength: 14 }, align: "center" }
                ,   { name: "TipoEstorno"
                    ,   width: 80
                    ,   editable: true
                    ,   formatter: "select" //963 este comando é essencial para que o VALOR seja atribuído ao CAMPO. Senão existir o TEXTO é atribudo ao CAMPO
                    ,   edittype: "select"
                    ,   editrules: { required: true }
                    ,   editoptions: 
                        {   value: getFiltroTipoEstorno()
                        }
                    }
                ,   { name: "StatusProtocolo", width: 50, index: "StatusProtocolo", align: "center", sortable: true }
                ,   { name: "Acao", width: 56, index: "Acao", align: "center", editable: false, sortable: true }
                ,   { name: "CodigoTransacao", index: "CodigoTransacao", hidden: true }
                ]
            });
            //=================================================================================
            // Trata o redimensionamento da tela
            //=================================================================================
            $(window).bind("resize", function() 
            {   $("#tbProtocolo").setGridWidth($(window).width() - margemEsquerda);
                $("#tbProtocolo").setGridHeight($(window).height() - margemRodape - 20);
            }).trigger("resize");
            //=================================================================================
            if (callback) 
            {   callback();
            }

        }
        //=================================================================================
        //  CARREGANDO OS FILTROS
        //=================================================================================
        ,   recuperarFiltros: function(tipoRetorno, callback) 
        {   var link = "";
            request = {};
            request.Request = {};
            $("#site_configuracao_cadastro_protocolo_cheque_devolvido").Forms("getData", { dataObject: request.Request });
            
            link = link + "FiltroEnvioCci=" + request.Request.FiltroEnvioCci + "&";
            link = link + "FiltroNumeroProtocolo=" + ( request.Request.FiltroNumeroProtocolo == "" ? "null" : request.Request.FiltroNumeroProtocolo )+ "&";
            link = link + "FiltroEstabelecimento=" + request.Request.FiltroEstabelecimento + "&";
            link = link + "FiltroStatusProtocolo=" + request.Request.FiltroStatusProtocolo + "&";            
            link = link + "FiltroContaCliente=" + ( request.Request.FiltroContaCliente == "" ? "null" : request.Request.FiltroContaCliente ) + "&";                        
            link = link + "FiltroTipoEstorno=" + request.Request.FiltroTipoEstorno + "&";
            link = link + "FiltroDataEstornoDe=" + request.Request.FiltroDataEstornoDe + "&";
            link = link + "FiltroDataEstornoAte=" + request.Request.FiltroDataEstornoAte + "&";
            link = link + "FiltroDataProcessamentoDe=" + request.Request.FiltroDataProcessamentoDe + "&";
            link = link + "FiltroDataProcessamentoAte=" + request.Request.FiltroDataProcessamentoAte + "&";
            link = link + "FiltroDataRegProtocoloDe=" + request.Request.FiltroDataRegProtocoloDe + "&";
            link = link + "FiltroDataRegProtocoloAte=" + request.Request.FiltroDataRegProtocoloAte + "&";
            
            if (link != "") 
            {   link = "?" + link.substr(0, link.length - 1);
            }
            request.Link = link;
            
            if (callback) 
            {   callback(request);
            }
        }        
        //=================================================================================
        //  CARREGANDO O GRID
        //=================================================================================
        ,   listar: function() 
        {   $("#tbProtocolo").jqGrid("clearGridData");
            var request = {};
            $("#site_configuracao_cadastro_protocolo_cheque_devolvido").Forms("getData", { dataObject: request });
            executarServico("ListarProtocoloChequeRequest",request,function(data) 
            {       var acao =  "<input type='image' src='img/pen.png' style='width: 16px; height: 16px;' title='Editar' class='cmdEditar' />&nbsp;";
                    acao +=     "<input type='image' src='img/save_1.png' style='width: 16px; height: 16px;' title='Salvar' class='cmdSalvar' />&nbsp;";
                    acao +=     "<input type='image' src='img/button_white_remove.png' style='width: 16px; height: 16px;' title='Cancelar' class='cmdCancelar' />&nbsp;";
                    for (var i = 0; i < data.Resultado.length; i++) 
                    {   if (data.Resultado[i].StatusProtocolo == null || data.Resultado[i].StatusProtocolo == "") 
                        {   if ((data.Resultado[i].DataRegProtocolo     == null || data.Resultado[i].DataRegProtocolo   == "") &&
                                (data.Resultado[i].DataEstorno          == null || data.Resultado[i].DataEstorno        == "") &&
                                (data.Resultado[i].NumeroProtocolo      == null || data.Resultado[i].NumeroProtocolo    == "")) 
                            {
                                 data.Resultado[i].StatusProtocolo = "N";
                            }
                            else 
                            {
                                data.Resultado[i].StatusProtocolo = "S";
                            }
                        } 
                        var item = data.Resultado[i];
                        var objLinha = 
                        {
                            CodigoTransacao: data.Resultado[i].CodigoTransacao
                        ,   DataProcessamento: data.Resultado[i].DataProcessamento
                        ,   ContaCliente: data.Resultado[i].ContaCliente
                        ,   NsuHost: data.Resultado[i].NsuHost
                        ,   NsuHostOriginal: data.Resultado[i].NsuHostOriginal
                        ,   Estabelecimento: data.Resultado[i].Estabelecimento
                        ,   Valor: data.Resultado[i].Valor != null ? data.Resultado[i].Valor.replace("," , ".") : 0
                        ,   DataRegProtocolo: data.Resultado[i].DataRegProtocolo
                        ,   DataEstorno: data.Resultado[i].DataEstorno
                        ,   NumeroProtocolo: data.Resultado[i].NumeroProtocolo
                        ,   StatusProtocolo: data.Resultado[i].StatusProtocolo
                        ,   TipoEstorno: data.Resultado[i].TipoEstorno //963
                        ,   Acao: acao
                        };
                        
                        $("#tbProtocolo").jqGrid("addRowData", "protocolo_" + data.Resultado[i].CodigoTransacao, objLinha);
                        $("#protocolo_" + data.Resultado[i].CodigoTransacao + " .cmdEditar").click($.protocolo.filtro.editarLinha);
                        $("#protocolo_" + data.Resultado[i].CodigoTransacao + " .cmdSalvar").click($.protocolo.filtro.salvarLinha).hide();
                        $("#protocolo_" + data.Resultado[i].CodigoTransacao + " .cmdCancelar").click($.protocolo.filtro.cancelarLinha).hide();
                    }
                });
        }        
        //=================================================================================
        //  TRATANDO A LINHA DO GRID - Ícone EDITAR lapis
        //=================================================================================
        ,   editarLinha: function() 
        {   var tr = $(this).closest("tr");
            $("#tbProtocolo").jqGrid("editRow", tr.attr("id"), false, function(rowid) 
            {   $("input", tr).each(function(index, element) 
                {   $(element).attr("original", $(element).val());
                    $(element).change(function() 
                    {   if ($(this).val() != $(this).attr("original"))
                            $(this).addClass("alterado");
                        else
                            $(this).removeClass("alterado");
                    });
                });
            });
            $(".cmdEditar", tr).hide();
            $(".cmdSalvar", tr).show();
            $(".cmdCancelar", tr).show();
        }
        //=================================================================================
        //  TRATANDO A LINHA DO GRID - Icone SALVAR disquete
        //=================================================================================
        ,   salvarLinha: function() 
        {   var tr = $(this).closest("tr");
            $("#tbProtocolo").jqGrid("saveRow", $(tr).attr("id"), null, "clientArray");
            var objLinha = $("#tbProtocolo").jqGrid("getRowData", tr.attr("id"));
            var colNames = $("#tbProtocolo").jqGrid("getGridParam", "colNames");
            delete objLinha.acao;
            var request = 
            {   CodigoTransacao: objLinha.CodigoTransacao
            ,   DataRegProtocolo: objLinha.DataRegProtocolo
            ,   Dataestorno: objLinha.DataEstorno
            ,   NumeroProtocolo: objLinha.NumeroProtocolo
            ,   TipoEstorno: objLinha.TipoEstorno // 963
            };
            executarServico("SalvarProtocoloChequeRequest", request, function(data) 
            {   
                $(".cmdEditar", tr).show();
                $(".cmdSalvar", tr).hide();
                $(".cmdCancelar", tr).hide();
                popup(
                    {   titulo: "SALVAR LINHA DO GRID"
                    ,   icone: "sucesso"
                    ,   mensagem: "Protocolo do NSUHOST [" + objLinha.NsuHost + "] atualizado com sucesso!"
                    ,   callbackOK: function() { $.protocolo.filtro.listar(); }
                    }
                );
            });
        }
        //=================================================================================
        //  TRATANDO A LINHA DO GRID - Ícone CANCELAR bolinha vermelha com traco branco no meio
        //=================================================================================
        ,   cancelarLinha: function() 
        {   var tr = $(this).closest("tr");
            $("#tbProtocolo").jqGrid("restoreRow", tr.attr("id"));
            $(".cmdEditar", tr).show();
            $(".cmdSalvar", tr).hide();
            $(".cmdCancelar", tr).hide();
        }
        //=================================================================================
        //  BOTAO - VISUALIZAR - ação de exibir HTML do relatorio
        //=================================================================================
        ,   abrirRelatorio: function(request) //1465
        {   var link = "relatorio_cheques.aspx";
            link += request.Link;
            link = link + "&TipoArquivo=HTML";
            var url = window.location.href
            var arr = url.split("/");
            var result = arr[0] + "//" + arr[2]
            if (arr[3] == "scf") 
            {   arr[3] = "scf_html";
                result = result + "/" + arr[3];
            } else if (arr[3] == "scf_projetos") 
            {   arr[3] = "scf_html2";
                result = result + "/" + arr[3];
            }
            link = result + "/" + link;
            window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');
        }
        //=================================================================================
        //  BOTAO - EXCEL ou PDF - ação de criar processo e gerar o relatorio
        //=================================================================================
        ,   criarProcesso: function(request) //1465
        {   if (request.Request.TipoRelatorio == "Excel")
            {   executarServico
                (   "GerarRelatorioChequesExcelRequest"
                ,   request
                ,   function(data) 
                    {   popup
                        ({  titulo: "Relatório Excel"
                        ,   icone: "sucesso"
                        ,   mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório."
                        ,   callbackOK: function() { }
                        });
                    }
                );
            }                    
            else 
            {   var link = "relatorio_cheques.aspx";
                link += request.Link;
                link = link + "&TipoArquivo=PDF";
                var url = window.location.href
                var arr = url.split("/");
                var result = arr[0] + "//" + arr[2]
                if (arr[3] == "scf") 
                {   arr[3] = "scf_relatorios";
                    result = result + "/" + arr[3];
                }
                link = result + "/" + link;
                request.Request.Link = link;
                request.Request.TipoArquivo = "PDF";
                request.Link = link;
                request.TipoArquivo = "PDF";
                executarServico
                (   "GerarRelatorioChequesPDFRequest"
                ,   request
                ,   function(data)
                    {   popup
                        ({  titulo: "Relatório PDF"
                        ,   icone: "sucesso"
                        ,   mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório."
                        ,   callbackOK: function() { }
                        });
                    }
                );
            }
        }
    };
    function getFiltroTipoEstorno() 
    {
        vlista = "";
        $("#cmbFiltroTipoEstorno").children().each(function(index) 
        {       if (index != 0)
                    vlista += $("#cmbFiltroTipoEstorno").children()[index].value + ":" + $("#cmbFiltroTipoEstorno").children()[index].text + ";" ;
        });
        vlista = vlista.substring(0,vlista.length-1);
        return vlista;
    }

    function callback() 
    {   $.protocolo.filtro.listar();
    }
    $.protocolo.filtro.load();
});
