﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Reflection;
using Resource.Carrefour.SCF.Contratos.Principal.Permissoes;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Db.Oracle; // INFORMANDO O AMBIENTE

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class login : System.Web.UI.Page
    {
        // private static OracleDbLibConfig _config = null; // warning 25/02/2019

        protected void Page_Load(object sender, EventArgs e)
        {
            LogArquivo logArquivo = 
                new LogArquivo("web", "rotina", "login:Page_Load");
            try
            {
                // 10/2018 SegurancaAuditoria Projeto
                //if (!Page.IsPostBack)
                //{   _config = GerenciadorConfig.ReceberConfig<OracleDbLibConfig>();// INFORMANDO O AMBIENTE
                //    lblAmbiente.Text = _config.ConnectionStringName.ToString();// INFORMANDO O AMBIENTE

                //    txtSenha.Text = "123";
                //    lblVersao.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major.ToString() + "." +
                //                     System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString() + "." +
                //                     System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build.ToString() + "." +
                //                     System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Revision.ToString();                    
                //}
            }
            catch (Exception ex)
            {
                LogArquivo.LogErro(ex);
            }
            finally
            {
                logArquivo.Dispose();
            }

            this.txtUsuario.Focus();
        }

        protected void cmdLogin_Click(object sender, EventArgs e)
        {
            // Log
            LogArquivo logArquivo = 
                new LogArquivo("web", "rotina", "login:cmdLogin_Click");

            // Bloco de controle
            try
            {
                AutenticarUsuarioResponse response =
                    (AutenticarUsuarioResponse)
                        Mensageria.Processar(
                            new AutenticarUsuarioRequest()
                            {
                                CodigoUsuario = txtUsuario.Text,
                                Senha = txtSenha.Text,
                                RepassarExcessao = false
                            });

                if (response.StatusResposta == MensagemResponseStatusEnum.OK)
                {
                    // recebe os dados do usuario
                    UsuarioInfo usuarioInfo =
                        Mensageria.Processar<ReceberUsuarioResponse>(
                            new ReceberUsuarioRequest()
                            {
                                CodigoUsuario = txtUsuario.Text
                            }).Usuario;


                    if (usuarioInfo != null)
                    {
                        Usuario usuario = new Usuario(usuarioInfo);
                        
                        Session["CodigoSessao"] = response.Sessao.CodigoSessao;
                        Session["usuario"] = txtUsuario.Text.ToLower();
                        Session["usuarioNome"] = usuario.UsuarioInfo.Nome;
                        Session["CodigoUsuario"] = response.Sessao.CodigoUsuario;
                        // Response.Redirect(GerenciadorConfig.ReceberConfig<WebConfig>().PaginaInicial);
                        if (response.Sessao.SenhaExpirada)
                            Response.Redirect("alterar_senha.aspx?senhaExpirada=1");

                        //SCF1140 - Marcos Matsuoka (priorizando a tela manutenção para ADMIN e a tela lista de processos caso não tenha permissão para manutenção)
                        if (usuario.PermissoesAssociadas.ConsultarPermissao(typeof(PermissaoMenuManutencaoProcesso))) 
                        {
                            Response.Redirect("site_manutencao_processo.aspx");
                        } else if 
                            ((usuario.PermissoesAssociadas.ConsultarPermissao(typeof(PermissaoMenuListaProcesso))))
                        {
                            Response.Redirect("site_processo_lista.aspx");
                        } else { 
                            Response.Redirect("Default.aspx");
                        }
                    }
                }
                else
                {
                    //// 10/2018 SegurancaAuditoria Projeto
                    //response.DescricaoResposta = " '' " + response.DescricaoResposta + " '' "   ;
                    usuarioInvalido.InnerText = "Usuário/Senha inválido!";
                    //usuarioInvalido.InnerText = response.Erro + " - " + response.DescricaoResposta;

                    this.txtUsuario.Focus();
                }
            }
            catch (Exception ex)
            {
                LogArquivo.LogErro(ex);
            }
            finally
            {
                logArquivo.Dispose();
            }
        }
    }
}
