﻿$(document).ready(function() {
    if (!$.favorecido)
        $.favorecido = {};

    // ==============================================================================================================================================        
    $.favorecido.lista =
    { variaveis: {}
        // ==============================================================================================================================================
        //  load: funcoes de inicializacao da tela
        // ==============================================================================================================================================
    , load: function() {
        $("#avisoCarregar").hide();                 //1353
        $("#subtitulo").html("[CONFIGURACOES] Cadastro Favorecidos");
        $("#site_configuracao_favorecido_lista button").button();
        $("#site_configuracao_favorecido_lista").Forms("initializeFields");
        // --------------------------------------------------------------------
        // HABILITA APENAS O BOTAO NOVO e FILTRAR 
        // --------------------------------------------------------------------            
        $("#site_configuracao_favorecido_lista #cmdNovoFavorecido").attr("class", "cmdNovoFavorecido ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
        $("#site_configuracao_favorecido_lista #cmdNovoFavorecido").removeAttr("disabled");

        $("#site_configuracao_favorecido_lista #cmdFiltrar").attr("class", "cmdFiltrar ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
        $("#site_configuracao_favorecido_lista #cmdFiltrar").removeAttr("disabled");
        // --------------------------------------------------------------------
        // DESABILITA EDITAR e REMOVOER
        // --------------------------------------------------------------------
        $("#site_configuracao_favorecido_lista #cmdEditarFavorecido").attr("class", "cmdEditarFavorecido ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
        $("#site_configuracao_favorecido_lista #cmdEditarFavorecido").attr("disabled", "true");

        $("#site_configuracao_favorecido_lista #cmdRemoverFavorecido").attr("class", "cmdRemoverFavorecido ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
        $("#site_configuracao_favorecido_lista #cmdRemoverFavorecido").attr("disabled", "true");

        // ==============================================================================================================================================
        // BOTÃO NOVO FAVORECIDO
        // ==============================================================================================================================================
        $("#cmdNovoFavorecido").click(function() {
            $.favorecido.detalhe.variaveis.codigoFavorecido = null; // Reseta código.
            $.favorecido.detalhe.variaveis.editar = false; //Crio variavel global de edicao
            $("#txtCnpjFavorecido").attr("disabled", false);    // Habilita input de cnpj
            $("#avisoSalvar").show(); // Exibe a msg de alerta para o usuário salvar um favorecido antes de relacionar contas
            //1353 $.favorecido.detalhe.limparTelaDetalhes();  // Limpa a tela e os grids
            //1353 $.favorecido.detalhe.desabilitaBotoes();// Desabilita os botoes da tela de detalhes
            $.favorecido.detalhe.abrir();   // Abre dialog de detalhe
        });

        // ==============================================================================================================================================
        // BOTÃO FILTRAR
        // ==============================================================================================================================================
        $("#cmdFiltrar").click(function() {
            $.favorecido.lista.listar2();
        });

        // ==============================================================================================================================================
        // BOTÃO EDITAR FAVORECIDO
        // ==============================================================================================================================================
        $("#cmdEditarFavorecido").click(function() {
            var codigoFavorecido = $("#tbFavorecido").getGridParam("selrow");

            $.favorecido.detalhe.variaveis.codigoFavorecido = null;
            $.favorecido.detalhe.variaveis.editar = true;
            $("#txtCnpjFavorecido").attr("disabled", true);
            $("#avisoSalvar").hide();
            //1353 $.favorecido.detalhe.habilitaBotoes();
            //1353 $.favorecido.detalhe.variaveis.codigoFavorecido = $("#tbFavorecido").getGridParam("selrow");
            if (codigoFavorecido) {
                //1353
                $("#avisoCarregar").show();
                $.favorecido.detalhe.variaveis.codigoFavorecido = codigoFavorecido;
                $.favorecido.detalhe.listarProdutosSCF();
                $.favorecido.detalhe.listarReferenciasSCF();
                $.favorecido.detalhe.listarAssociacoes();
                $.favorecido.detalhe.listarContas();
                $.favorecido.detalhe.abrir(codigoFavorecido);
                //$.favorecido.detalhe.listarContas2(); //1380/1381                    
                //1353    
                //1353  $.favorecido.detalhe.abrir($.favorecido.detalhe.variaveis.codigoFavorecido);
            }
            else {
                popup(
                    {
                        titulo: "EDITAR FAVORECIDO",
                        icone: "erro",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
            }
        });

        // ==============================================================================================================================================
        // BOTÃO REMOVER FAVORECIDO
        // ==============================================================================================================================================
        $("#cmdRemoverFavorecido").click(function() {
            var codigoFavorecido = $("#tbFavorecido").getGridParam("selrow");
            var NomeFavorecido = $("#tbFavorecido").jqGrid('getCell', codigoFavorecido, 'NomeFavorecido');
            if (codigoFavorecido) {
                popup(
                    { titulo: "REMOVER FAVORECIDO"
                        , icone: "atencao"
                        , mensagem: "Confirma remover o favorecido [" + NomeFavorecido + "] ?"
                        , callbackSim: function() {
                            $.favorecido.detalhe.remover(codigoFavorecido, false, function(data) {
                                if (data.Criticas != null && data.Criticas.length > 0 && data.Criticas[0].Descricao.indexOf("estabelecimentos") >= 0) {
                                    popup(
                                    {
                                        titulo: "REMOVER FAVORECIDO"
                                    , icone: "atencao"
                                    , height: 200
                                    , mensagem: "Favorecido vinculado a Estabelecimento(s). Se confirmar a exclusão do Favorecido, o(s) Estabelecimento(s) ficará(ão) sem Favorecido gerando inconsistência na integração do arquivo Reconcilation com esse(s) estabelecimento(s). Confirma exclusão?"
                                    , callbackSim: function() {
                                        $.favorecido.detalhe.remover(codigoFavorecido, true, function(data) {
                                            popup(
                                                {
                                                    titulo: "REMOVER FAVORECIDO"
                                                , icone: "sucesso"
                                                , mensagem: "Favorecido removido com sucesso!"
                                                , callbackOK: function() { $.favorecido.lista.listar(); }
                                                });
                                        });
                                    }
                                    , callbackNao: function() { }
                                    });
                                }
                                else if (data.Criticas != null && data.Criticas.length > 0 && data.Criticas[0].Descricao.indexOf("transacões") >= 0) {
                                    popup(
                                    {
                                        titulo: "REMOVER FAVORECIDO"
                                    , icone: "erro"
                                    , mensagem: "O favorecido selecionado possui transacões / cessões vinculadas."
                                    , callbackOK: function() { $.favorecido.lista.listar(); }
                                    });

                                }
                                else if (data.Criticas != null && data.Criticas.length > 0 && data.Criticas[0].Descricao.indexOf("pagamentos") >= 0) {
                                    popup({
                                        titulo: "REMOVER FAVORECIDO",
                                        icone: "erro",
                                        mensagem: "O favorecido selecionado possui pagamentos vinculados.",
                                        callbackOK: function() { }
                                    });
                                }
                                else {
                                    $.favorecido.lista.listar();
                                }
                            });
                        }
                    , callbackNao: function() { }
                    });
            }
            else {
                popup({
                    titulo: "REMOVER FAVORECIDO"
                    , icone: "erro"
                    , mensagem: "Favor selecionar uma linha!"
                    , callbackOK: function() { }
                });
            }
        });

        // ==============================================================================================================================================
        // DADOS DO FAVORECIDO - CODIGO, CNPJ, RAZAO SOCIAL E SE É ORIGINAL
        // ==============================================================================================================================================
        $("#tbFavorecido").jqGrid(
            { datatype: "clientSide"
            , mtype: "GET"
            , height: 160
            , width: 320
            , rowNum: 100000
            , colNames: ["ID", "CNPJ", "Nome", "Original?", "Bloqueado?"]
            , colModel:
                [{ name: "CodigoFavorecido", index: "CodigoFavorecido", hidden: false, align: "right", key: true, width: 50 }
                , { name: "CnpjFavorecido", index: "CnpjFavorecido", width: 200, align: "right" }
                , { name: "NomeFavorecido", index: "NomeFavorecido", width: 800, align: "left" }
                , { name: "FavorecidoOriginal", index: "FavorecidoOriginal", width: 150, formatter: 'select', editoptions: { value: "false:Não;true:Sim" }, align: "center" }
                , { name: "FavorecidoBloqueado", index: "FavorecidoBloqueado", hidden: true, formatter: 'select', editoptions: { value: "false:Não;true:Sim"}}]
            , sortorder: "asc"
			, shrinkToFit: false
			, viewrecords: true
            , ondblClickRow: function(rowid, iRow, iCol, e) {
                $("#txtCnpjFavorecido").attr("disabled", true);
                $("#avisoSalvar").hide();
                $.favorecido.detalhe.variaveis.editar = true;
                var codigoFavorecido = rowid;
                if (codigoFavorecido) {
                    //SCF1701
                    var objProcesso = $("#tbFavorecido").jqGrid('getRowData', rowid);
                    if (objProcesso.FavorecidoBloqueado == "true") {
                        popup(
                        { titulo: "CADASTRO FAVORECIDO"
                        , icone: "erro"
                        , mensagem: "Favorecido Bloqueado para Substituição de Contas"
                        , callbackOK: function() { }
                        });
                    }
                    else {
                        //1353 $.favorecido.detalhe.variaveis.codigoFavorecido = codigoFavorecido;
                        //1353 $.favorecido.detalhe.listarProdutos();
                        //1353
                        $("#avisoCarregar").show();
                        $.favorecido.detalhe.variaveis.codigoFavorecido = codigoFavorecido;
                        $.favorecido.detalhe.listarProdutosSCF();
                        $.favorecido.detalhe.listarReferenciasSCF();
                        $.favorecido.detalhe.listarAssociacoes();
                        $.favorecido.detalhe.listarContas();
                        $.favorecido.detalhe.abrir(codigoFavorecido);
                        //  $.favorecido.detalhe.listarContas2(); //1380/1381                            
                        //1353  
                    }
                }
                else {
                    popup(
                        { titulo: "CADASTRO FAVORECIDO"
                        , icone: "erro"
                        , mensagem: "Selecione um Favorecido"
                        , callbackOK: function() { }
                        });
                }
            }
            , onSelectRow: function(id) {
                var objProcesso = $("#tbFavorecido").jqGrid('getRowData', id);
                if (objProcesso.FavorecidoBloqueado == "true") {
                    $("#site_configuracao_favorecido_lista #cmdEditarFavorecido").attr("class", "cmdEditarFavorecido ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_configuracao_favorecido_lista #cmdEditarFavorecido").attr("disabled", "true");

                    $("#site_configuracao_favorecido_lista #cmdRemoverFavorecido").attr("class", "cmdRemoverFavorecido ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_configuracao_favorecido_lista #cmdRemoverFavorecido").attr("disabled", "true");
                } else if (objProcesso) {
                    $("#site_configuracao_favorecido_lista #cmdFiltrar").attr("class", "cmdFiltrar ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_configuracao_favorecido_lista #cmdFiltrar").removeAttr("disabled");

                    $("#site_configuracao_favorecido_lista #cmdEditarFavorecido").attr("class", "cmdEditarFavorecido ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_configuracao_favorecido_lista #cmdEditarFavorecido").removeAttr("disabled");

                    $("#site_configuracao_favorecido_lista #cmdRemoverFavorecido").attr("class", "cmdRemoverFavorecido ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_configuracao_favorecido_lista #cmdRemoverFavorecido").removeAttr("disabled");
                }
            }
            });

        // --------------------------------------------------------------------
        // Pede lista de favorecidos
        // --------------------------------------------------------------------
        $.favorecido.lista.listar();

        // --------------------------------------------------------------------
        // Trata o redimensionamento da tela
        // --------------------------------------------------------------------
        $(window).bind("resize", function() {
            $("#tbFavorecido").setGridWidth($(window).width() - margemEsquerda);
            $("#tbFavorecido").setGridHeight($(window).height() - margemRodape);
        }).trigger("resize");
    }

        // ==============================================================================================================================================
        //  listar: CARREGA A tbFavorecido CNPJ, RAZAO SOCIAL E SE É ORIGINAL
        // ==============================================================================================================================================
    , listar: function(callback) {
        $("#avisoCarregar").hide(); //1353
        executarServico("ListarFavorecidoRequest", {}, function(data) {
            $("#tbFavorecido").clearGridData();
            for (var i = 0; i < data.Resultado.length; i++)
                $("#tbFavorecido").jqGrid("addRowData", data.Resultado[i].CodigoFavorecido, data.Resultado[i]);
            if (callback)
                callback();
        });
    }

        // ==============================================================================================================================================
        //  listar: EXECUTA O COMANDO cmdFiltrar
        // ==============================================================================================================================================
    , listar2: function(callback) {
        $("#avisoCarregar").hide(); //1353
        var cnpj = $("#txtFiltroCNPJ").val();
        if (cnpj == "")
            $.favorecido.lista.filtra();
        else {
            if (cnpj.length < 15) {
                popup(
                    {
                        titulo: "FILTRO CNPJ"
                    , icone: "erro"
                    , mensagem: "O campo CNPJ precisa ester em branco ou ter 15 caracteres para executar o filtro!"
                    , callbackOK: function() { }
                    });
            }
            else
                $.favorecido.lista.filtra();
        }
        if (callback)
            callback();
    }

        // =================================================================================
        //  filtra: executa o filtro do "listar2"
        // =================================================================================
    , filtra: function(callback) {
        $("#tbFavorecido").jqGrid("clearGridData");
        var request = {};
        var cnpj = $("#txtFiltroCNPJ").val();
        if (cnpj != "")
            request.FiltroCNPJFavorecido = cnpj;

        request.MaxLinhas = 100;

        executarServico("ListarFavorecidoRequest", request, function(data) {
            $("#tbFavorecido").clearGridData();
            for (var i = 0; i < data.Resultado.length; i++)
                $("#tbFavorecido").jqGrid("addRowData", data.Resultado[i].CodigoFavorecido, data.Resultado[i]);
            if (callback)
                callback();
        });
    }
    };
    $.favorecido.lista.load();
});