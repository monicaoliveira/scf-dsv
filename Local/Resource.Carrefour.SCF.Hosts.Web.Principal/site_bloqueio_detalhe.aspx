﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_bloqueio.css" rel="stylesheet" type="text/css" />
    <link href="site_bloqueio_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_bloqueio_detalhe.js" type="text/javascript"></script>
    <title>SCF - Detalhe Linha Arquivo</title>
</asp:Content>
 
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">   
        
        <div id="site_bloqueio_detalhe" class="cabecalho">
        
            <div id="bloqueioPropriedades"></div>  
        </div>
         
</asp:Content>
