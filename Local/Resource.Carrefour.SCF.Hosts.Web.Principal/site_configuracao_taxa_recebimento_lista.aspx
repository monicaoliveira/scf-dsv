﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/template_default.Master" CodeBehind="site_configuracao_taxa_recebimento_lista.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_configuracao_taxa_recebimento_lista"%>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_taxa_recebimento_lista.js"></script>
    <script type="text/javascript" src="site_configuracao_taxa_recebimento_detalhe.js"></script>
    <link type="text/css" rel="stylesheet" href="site_configuracao_taxa_recebimento_lista.css" />
    <link type="text/css" rel="stylesheet" href="site_configuracao_taxa_recebimento_detalhe.css" />
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    <div id="site_configuracao_taxa_recebimento_lista" class="lista">
        <table>
            <tr>
                <td><b>Grupo</b></td>
                <td><b>Referência</b></td>
                <td><b>Meio de Pagamento</b></td>
                <td rowspan="2"><button id="cmdFiltrar">Filtrar</button></td>
            </tr>
            <tr>
                <td>
                    <select id="cmbGrupo">
                        <option value="?" selected="selected"> Todos</option>
                    </select>
                </td>
                <td>
                    <select id="cmbReferencia">
                        <option value="?" selected="selected"> Todas</option>
                    </select>
                </td>
                <td>
                    <select id="cmbMeioPagamento">
                        <option value="?" selected="selected"> Todos</option>
                    </select>
                </td>
            </tr>
        </table><br />
        <table id="tbTaxaRecebimento"></table>
        <div class="botoes">
            <button id="cmdNovo">Novo</button>
            <button id="cmdEditar">Editar</button>
            <button id="cmdRemover">Remover</button>
        </div>
    </div>
</asp:Content>
