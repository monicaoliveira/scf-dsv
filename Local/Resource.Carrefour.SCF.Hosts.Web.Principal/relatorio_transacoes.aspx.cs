﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Web;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public class ObjetoJsonRelTransacoes
    {
        public string CodigoArquivo { get; set; }
        public string TipoArquivo { get; set; }
        public string CodigoEmpresaGrupo { get; set; }
        public string NomeEmpresaGrupo { get; set; }
        public string CodigoEmpresaSubGrupo { get; set; }
        public string NomeEmpresaSubGrupo { get; set; }
        public string CodigoEstabelecimento { get; set; }
        public string RazaoSocial { get; set; }
        public string CodigoFavorecido { get; set; }
        public string NomeFavorecido { get; set; }
        public string CodigoProduto { get; set; }
        public string NomeProduto { get; set; }
        public string CodigoPlano { get; set; }
        public string NomePlano { get; set; }
        public string CodigoMeioCaptura { get; set; }
        public string MeioCaptura { get; set; }
        public string CodigoTipoRegistro { get; set; }
        public string TipoRegistro { get; set; }
        public string CodigoTipoTransacao { get; set; }
        public string TipoTransacao { get; set; }
        public string NumeroPagamento { get; set; }
        public string ContaCliente { get; set; } //1438
        public string DataProcessamentoDe { get; set; }
        public string DataProcessamentoAte { get; set; }
        public string DataMovimentoDe { get; set; }
        public string DataMovimentoAte { get; set; }
        public string DataTransacaoDe { get; set; }
        public string DataTransacaoAte { get; set; }
        public string DataVencimentoDe { get; set; }
        public string DataVencimentoAte { get; set; }
        public string DataAgendamentoDe { get; set; }
        public string DataAgendamentoAte { get; set; }
        public string DataEnvioMateraDe { get; set; }
        public string DataEnvioMateraAte { get; set; }
        public string DataLiquidacaoDe { get; set; }
        public string DataLiquidacaoAte { get; set; }
        public string CodigoStatusRetorno { get; set; }
        public string DescricaoStatusRetorno { get; set; }
        public string CodigoStatusPagamento { get; set; }
        public string DescricaoStatusPagamento { get; set; }
        public string CodigoStatusConciliacao { get; set; }
        public string DescricaoStatusConciliacao { get; set; }
        public string CodigoStatusValidacao { get; set; }
        public string DescricaoStatusValidacao { get; set; }
        
        // NR-033
        public string CodigoStatusCancelamentoOnLine { get; set; }
        public string DescricaoStatusTransacaoCancelamentoOnline { get; set; }

        // NR-033
        public string DataRetornoCessaoDe { get; set; }
        public string DataRetornoCessaoAte { get; set; }

        public bool RelatorioDeInconsistente { get; set; }

        //ECOMMERCE - Fernando Bove - 20160105
        public string FiltroFinanceiroContabil { get; set; }

        //ECOMMERCE - Fernando Bove - 20160106
        public string FiltroNSUHost { get; set; }

        // ClockWork - Marcos Matsuoka
        public string FiltroReferenciaCodigo { get; set; }
        public string FiltroReferenciaDescricao { get; set; }
    }

    
    public partial class relatorio_transacoes : PaginaBase
    {
        
        //===============================================================================
        // Total Geral
        //===============================================================================
        public decimal gTotalGeral = 0;


        //===============================================================================
        // Agrupamento por Data de Transação
        //===============================================================================
        class ItemRelatorioTransacaoData
        {
            public DateTime? DataTransacao { get; set; }
            public decimal TotalDataTransacao { get; set; }
            public List<RelatorioTransacaoInfo> Transacoes { get; set; }
        }
        

        //===============================================================================
        // Page Load
        //===============================================================================
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                // Se é um relatório normal, tem que pedir para o browser informar os filtros
                // Se é um relatório de inconsistentes, o filtro é apenas o código do arquivo e este está
                //      sendo passado via GET. Neste caso já dá para preencher o relatório

                if (Request["tipoRelatorio"] != "inconsistencias")
                {
                    if (!this.ClientScript.IsStartupScriptRegistered("InicializarRelatorio"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "InicializarRelatorio", "$(document).ready(relatorio_transacoes_load());", true);
                    }
                }
                else
                {
                    PreencherRelatorio(new ObjetoJsonRelTransacoes() { 
                        CodigoArquivo = Request["codigoArquivo"],
                        RelatorioDeInconsistente = true
                    });
                }
            }
            else
            {
                if(!String.IsNullOrEmpty(Request.Form["ObjetoJson"]))
                {
                    ObjetoJsonRelTransacoes param = 
                        JsonLib.Desserializar<ObjetoJsonRelTransacoes>(Request.Form["ObjetoJson"]);
                    if (param != null)
                    {
                        // Preenche o relatório
                        PreencherRelatorio(param);
                    }
                }
            }
        }


        //===============================================================================
        // Preenche relatório
        //===============================================================================
        void PreencherRelatorio(ObjetoJsonRelTransacoes param)
        {
            //---------------------------------------------------------------------
            // Preenche dados dos filtros
            //---------------------------------------------------------------------
            this.lblFiltroOrigem.Text = param.TipoArquivo != null ? param.TipoArquivo : "TODOS";
            this.lblFiltroGrupo.Text = param.NomeEmpresaGrupo != null ? param.NomeEmpresaGrupo : "TODOS";
            this.lblFiltroSubgrupo.Text = param.NomeEmpresaSubGrupo != null ? param.NomeEmpresaSubGrupo : "TODOS";
            this.lblFiltroEstabelecimento.Text = param.RazaoSocial != null ? param.RazaoSocial : "TODOS";
            this.lblFiltroFavorecido.Text = param.NomeFavorecido != null ? param.NomeFavorecido : "TODOS";
            this.lblFiltroProduto.Text = param.NomeProduto != null ? param.NomeProduto : "TODOS";
            this.lblFiltroPlano.Text = param.NomePlano != null ? param.NomePlano : "TODOS";
            this.lblFiltroMeioCaptura.Text = param.MeioCaptura != null ? param.MeioCaptura : "TODOS";
            this.lblFiltroTipoRegistro.Text = param.TipoRegistro != null ? param.TipoRegistro : "TODOS";
            //this.lblFiltroTipoTransacao.Text = param.TipoTransacao != null ? param.TipoTransacao : "TODOS";
            this.lblFiltroNumeroPagamento.Text = param.NumeroPagamento != null ? param.NumeroPagamento : "QUALQUER";
            this.lblFiltroContaCliente.Text = param.ContaCliente != null ? param.ContaCliente : "QUALQUER"; //1438
            this.lblFiltroDataProcessamentoDe.Text = param.DataMovimentoDe != null ? param.DataMovimentoDe : "QUALQUER";
            this.lblFiltroDataProcessamentoAte.Text = param.DataMovimentoAte != null ? param.DataMovimentoAte : "QUALQUER";
            this.lblFiltroDataTransacaoDe.Text = param.DataTransacaoDe != null ? param.DataTransacaoDe : "QUALQUER";
            this.lblFiltroDataTransacaoAte.Text = param.DataTransacaoAte != null ? param.DataTransacaoAte : "QUALQUER";
            this.lblFiltroDataVencimentoDe.Text = param.DataVencimentoDe != null ? param.DataVencimentoDe : "QUALQUER";
            this.lblFiltroDataVencimentoAte.Text = param.DataVencimentoAte != null ? param.DataVencimentoAte : "QUALQUER";
            this.lblFiltroDataAgendamentoDe.Text = param.DataAgendamentoDe != null ? param.DataAgendamentoDe : "QUALQUER";
            this.lblFiltroDataAgendamentoAte.Text = param.DataAgendamentoAte != null ? param.DataAgendamentoAte : "QUALQUER";
            this.lblFiltroDataEnvioMateraDe.Text = param.DataEnvioMateraDe != null ? param.DataEnvioMateraDe : "QUALQUER";
            this.lblFiltroDataEnvioMateraAte.Text = param.DataEnvioMateraAte != null ? param.DataEnvioMateraAte : "QUALQUER";
            this.lblFiltroDataLiquidacaoDe.Text = param.DataLiquidacaoDe != null ? param.DataLiquidacaoDe : "QUALQUER";
            this.lblFiltroDataLiquidacaoAte.Text = param.DataLiquidacaoAte != null ? param.DataLiquidacaoAte : "QUALQUER";
            this.lblFiltroStatusRetorno.Text = param.DescricaoStatusRetorno != null ? param.DescricaoStatusRetorno : "TODOS";
            this.lblFiltroStatusPagamento.Text = param.DescricaoStatusPagamento != null ? param.DescricaoStatusPagamento : "TODOS";
            this.lblFiltroStatusConciliacao.Text = param.DescricaoStatusConciliacao != null ? param.DescricaoStatusConciliacao : "TODOS";
            //this.lblFiltroStatusValidacao.Text = param.DescricaoStatusValidacao != null ? param.DescricaoStatusValidacao : "TODOS";
            this.lblFiltroStatusImportacao.Text = param.DescricaoStatusTransacaoCancelamentoOnline != null ? param.DescricaoStatusTransacaoCancelamentoOnline : "TODOS";
            this.lblDataHora.Text = String.Format("{0:dd/MM/yyyy - HH:mm}", DateTime.Now);
            this.lblPagina.Text = "";
            this.lblDataRetornoCCIDe.Text = param.DataRetornoCessaoDe != null ? param.DataRetornoCessaoDe : "QUALQUER";
            this.lblDataRetornoCCIAte.Text = param.DataRetornoCessaoAte != null ? param.DataRetornoCessaoAte : "QUALQUER";
            // ECOMMERCE - Fernando Bove - 20160105
            this.lblFinanceiroContabil.Text = param.FiltroFinanceiroContabil != null ? param.FiltroFinanceiroContabil == "C" ? "CONTÁBIL" : "FINANCEIRO" : "TODOS";
            // ECOMMERCE - Fernando Bove - 20160106
            this.lblNSUHost.Text = param.FiltroNSUHost != null ? param.FiltroNSUHost : "TODOS";
            // ClockWork - Marcos Matsuoka
            this.lblReferencia.Text = param.FiltroReferenciaDescricao != null ? param.FiltroReferenciaDescricao : "TODOS";

            //---------------------------------------------------------------------
            // Preenche dados do relatório
            //---------------------------------------------------------------------

            // Prepara a coleção de itens a serem impressos no relatorio
            List<RelatorioTransacaoInfo> resultado = null;

            string CodigoSessaoTratado = null;
            if (Session["CodigoSessao"] != null)
            {
                CodigoSessaoTratado = this.Session["CodigoSessao"].ToString();
            }
            else
            {
                CodigoSessaoTratado = "Sem Sessao";
            }


            // Verifica se é relatório de transações ou de inconsistentes
            if (!param.RelatorioDeInconsistente)
            {
                // Recebe lista de transações
                resultado = Mensageria.Processar<ListarRelatorioTransacaoResponse>(
                    new ListarRelatorioTransacaoRequest()
                    {
                        CodigoSessao = CodigoSessaoTratado,
                        FiltroOrigem = param.TipoArquivo,
                        FiltroEmpresaGrupo = param.CodigoEmpresaGrupo,
                        FiltroEmpresaSubgrupo = param.CodigoEmpresaSubGrupo,
                        FiltroEstabelecimento = param.CodigoEstabelecimento,
                        FiltroFavorecido = param.CodigoFavorecido,
                        FiltroProduto = param.CodigoProduto,
                        FiltroPlano = param.CodigoPlano,
                        FiltroMeioCaptura = param.CodigoMeioCaptura,
                        FiltroTipoRegistro = param.TipoRegistro,
                        //FiltroTipoTransacao = param.TipoRegistro,
                        FiltroNumeroPagamento = param.NumeroPagamento,
                        FiltroContaCliente = param.ContaCliente,//1438
                        //FiltroDataProcessamentoDe = param.DataTransacaoDe != null ? (DateTime?)Convert.ToDateTime(param.DataTransacaoDe) : null,
                        //FiltroDataProcessamentoAte = param.DataProcessamentoAte != null ? (DateTime?)Convert.ToDateTime(param.DataProcessamentoAte) : null,
                        FiltroDataMovimentoDe = param.DataMovimentoDe != null ? (DateTime?)Convert.ToDateTime(param.DataMovimentoDe) : null,
                        FiltroDataMovimentoAte = param.DataMovimentoAte != null ? (DateTime?)Convert.ToDateTime(param.DataMovimentoAte) : null,
                        FiltroDataTransacaoDe = param.DataTransacaoDe != null ? (DateTime?)Convert.ToDateTime(param.DataTransacaoDe) : null,
                        FiltroDataTransacaoAte = param.DataTransacaoAte != null ? (DateTime?)Convert.ToDateTime(param.DataTransacaoAte) : null,
                        FiltroDataVencimentoDe = param.DataVencimentoDe != null ? (DateTime?)Convert.ToDateTime(param.DataVencimentoDe) : null,
                        FiltroDataVencimentoAte = param.DataVencimentoAte != null ? (DateTime?)Convert.ToDateTime(param.DataVencimentoAte) : null,
                        FiltroDataAgendamentoDe = param.DataAgendamentoDe != null ? (DateTime?)Convert.ToDateTime(param.DataAgendamentoDe) : null,
                        FiltroDataAgendamentoAte = param.DataAgendamentoAte != null ? (DateTime?)Convert.ToDateTime(param.DataAgendamentoAte) : null,
                        FiltroDataEnvioMateraDe = param.DataEnvioMateraDe != null ? (DateTime?)Convert.ToDateTime(param.DataEnvioMateraDe) : null,
                        FiltroDataEnvioMateraAte = param.DataEnvioMateraAte != null ? (DateTime?)Convert.ToDateTime(param.DataEnvioMateraAte) : null,
                        FiltroDataLiquidacaoDe = param.DataLiquidacaoDe != null ? (DateTime?)Convert.ToDateTime(param.DataLiquidacaoDe) : null,
                        FiltroDataLiquidacaoAte = param.DataLiquidacaoAte != null ? (DateTime?)Convert.ToDateTime(param.DataLiquidacaoAte) : null,
                        FiltroStatusRetorno = param.CodigoStatusRetorno,
                        FiltroStatusPagamento = param.CodigoStatusPagamento,
                        FiltroStatusConciliacao = param.CodigoStatusConciliacao,
                        FiltroStatusValidacao = param.CodigoStatusValidacao,
                        FiltroStatusCancelamentoOnLine = param.CodigoStatusCancelamentoOnLine,
                        FiltroDataRetornoCessaoDe = param.DataRetornoCessaoDe != null ? (DateTime?)Convert.ToDateTime(param.DataRetornoCessaoDe) : null,
                        FiltroDataRetornoCessaoAte = param.DataRetornoCessaoAte != null ? (DateTime?)Convert.ToDateTime(param.DataRetornoCessaoAte) : null,
                        // ECOMMERCE - Fernando Bove - 20160105
                        FiltroFinanceiroContabil = param.FiltroFinanceiroContabil != null ? param.FiltroFinanceiroContabil : null,
                        // ECOMMERCE - Fernando Bove - 20160106
                        FiltroNSUHost = param.FiltroNSUHost != null ? param.FiltroNSUHost : null,
                        // ClockWork - Marcos Matsuoka
                        FiltroReferencia = param.FiltroReferenciaCodigo != null ? param.FiltroReferenciaCodigo : null,
                        MaxLinhas = 1000
                    }).Resultado;
            }
            else
            {
                resultado = Mensageria.Processar<ListarRelatorioTransacaoInconsistenteResponse>(
                    new ListarRelatorioTransacaoInconsistenteRequest()
                    {
                        CodigoSessao = CodigoSessaoTratado,
                        FiltroDataTransacaoDe = param.DataTransacaoDe != null ? (DateTime?)Convert.ToDateTime(param.DataTransacaoDe) : null,
                        FiltroDataTransacaoAte = param.DataTransacaoAte != null ? (DateTime?)Convert.ToDateTime(param.DataTransacaoAte) : null,
                        FiltroDataVencimentoDe = param.DataVencimentoDe != null ? (DateTime?)Convert.ToDateTime(param.DataVencimentoDe) : null,
                        FiltroDataVencimentoAte = param.DataVencimentoAte != null ? (DateTime?)Convert.ToDateTime(param.DataVencimentoAte) : null,
                        FiltroStatusValidacao = param.CodigoStatusValidacao,
                        CodigoArquivo = param.CodigoArquivo
                    }).Resultado;
            }

            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (resultado.Count == 0)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Sem dados para imprimir pelo filtro informado.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Sem dados para imprimir pelo filtro informado.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
            else if (resultado.Count > 1000)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }

            // Agrupa por data de transação
            var orderGroups =
                from p in resultado
                group p by p.DataTransacao != null ? (new DateTime?(p.DataTransacao.Value.Date)) : null into g
                select new ItemRelatorioTransacaoData()
                {
                    DataTransacao = g.Key,
                    TotalDataTransacao = g.Sum(s => (Convert.ToInt32(s.ValorParcelaLiquido) != 0 && s.TipoRegistro != "CP") ? s.ValorParcelaLiquido : (s.TipoRegistro == "CP") ? s.MeioPagamentoValor : s.ValorLiquido),
                    Transacoes = g.Select(p => p).ToList()
                };

            rptDataTransacao.ItemDataBound +=
                new RepeaterItemEventHandler(rptDataResultado_ItemDataBound);

            // Preenche repeater data
            this.rptDataTransacao.DataSource = orderGroups;
            this.rptDataTransacao.DataBind();
        }


        //===============================================================================
        // Preenche Repeater Resultado
        //===============================================================================
        void rptDataResultado_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Encontra o repeater
            Repeater rptResultado = (Repeater)e.Item.FindControl("rptResultado");

            // Verifica o tipo
            ItemRelatorioTransacaoData Item = (ItemRelatorioTransacaoData)e.Item.DataItem;

            // Preenche repeater resultado
            rptResultado.DataSource = Item.Transacoes;
            rptResultado.DataBind();

            // Soma valor no total geral
            gTotalGeral += Item.TotalDataTransacao;

            this.lblTotalGeral.Text = gTotalGeral.ToString("N2");
        }

        protected void rptResultado_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (((RelatorioTransacaoInfo)e.Item.DataItem).StatusTransacao == TransacaoStatusEnum.CanceladoOnline.ToString())
                ((Label)e.Item.FindControl("lblStatusTransacao")).Text = "*";
        }
    }
}
