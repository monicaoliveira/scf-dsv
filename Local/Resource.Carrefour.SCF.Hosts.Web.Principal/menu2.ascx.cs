﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Resource.Framework.Contratos.Interface;
using Resource.Framework.Contratos.Interface.Dados;
using Resource.Framework.Contratos.Interface.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class menu2 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void Render(HtmlTextWriter writer)
        {
            MensagemResponseBase retorno = null;

            try
            {

                if (Session["codigoSessao"] != null)
                {
                    ReceberArvoreComandosInterfaceResponse resposta =
                        Mensageria.Processar<ReceberArvoreComandosInterfaceResponse>(
                            new ReceberArvoreComandosInterfaceRequest()
                            {
                                CodigoSessao = (string)Session["codigoSessao"],
                                CodigoGrupoComandoInterface = "default"
                            });

                    StringBuilder menus = new StringBuilder();
                    menus.Append(@"<ul class=""menu"">");
                    StringBuilder subMenus = new StringBuilder();
                    subMenus.Append(@"<ul class=""submenu"" id=""sub_default""></ul>");
                    foreach (ComandoInterfaceInfo comandoInterface in resposta.ComandosInterfaceRaiz)
                    {
                        // Verifica se menu ou submenu
                        if (comandoInterface.Tag != null && comandoInterface.Tag != "")
                        {
                            string id = comandoInterface.Tag;
                            menus.Append(@"<li><a href=""" + id + @""">" + comandoInterface.Nome + "</a></li>");
                        }
                        else
                        {
                            string id = "#" + comandoInterface.CodigoComandoInterface;
                            menus.Append(@"<li><a href=""" + id + @""">" + comandoInterface.Nome + "</a></li>");

                            subMenus.Append(@"<ul class=""submenu sub_escondido"" id=""" + comandoInterface.CodigoComandoInterface + @""">");
                            foreach (ComandoInterfaceInfo submenu in comandoInterface.Filhos)
                                subMenus.Append(@"<li><a href=""" + submenu.Tag + @""">" + submenu.Nome + "</a></li>");
                            subMenus.Append("</ul>");
                        }
                    }
                    menus.Append("</ul>");

                    writer.Write(menus.ToString() + subMenus.ToString());
                }
                else
                {
                    // Cria mensagem generica informando o erro
                    retorno =
                        new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                            DescricaoResposta = "Sessão Inválida"
                        };
                }
            }
            catch (Exception ex)
            {
                // Cria mensagem generica informando o erro
                retorno =
                    new MensagemErroResponse()
                    {
                        StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                        DescricaoResposta = ex.ToString()
                    };
            }
        }
       
    }
}