﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using System.Web.UI.HtmlControls;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class relatorio_consolidado : System.Web.UI.Page
    {
        ListarRelatorioConsolidadoResponse resposta = new ListarRelatorioConsolidadoResponse();

        class ItemAgrupamentoFavorecido
        {
            public string CodigoReferencia { get; set; }
            public string DescricaoReferencia { get; set; }
            public string DescricaoFavorecido { get; set; }
            public string CodigoFavorecido { get; set; }
            public Decimal ValorTotalCV { get; set; }
            public Decimal ValorTotalAV { get; set; }
            public Decimal ValorTotalAJ { get; set; }
            public Decimal ValorTotalCP { get; set; }
            public Decimal ValorTotalAP { get; set; }
            public Decimal ValorTotalComissaoCV { get; set; }
            public Decimal ValorTotalComissaoAV { get; set; }
            public Decimal ValorTotalComissaoAJ { get; set; }
            public Decimal ValorTotalLiquido { get; set; }
            public Decimal ValorTotalTotal { get; set; }
            public int QtdeTotalCV { get; set; }
            public int QtdeTotalCP { get; set; }
            public int QtdeTotalAV { get; set; }
            public int QtdeTotalAP { get; set; }
            public int QtdeTotalAJ { get; set; }
        }

        class ItemAgrupamentoData : ItemAgrupamentoFavorecido
        {
            public string DescricaoData { get; set; }
            public Decimal ValorTotalDiaCV { get; set; }
            public Decimal ValorTotalDiaAV { get; set; }
            public Decimal ValorTotalDiaAJ { get; set; }
            public Decimal ValorTotalDiaCP { get; set; }
            public Decimal ValorTotalDiaAP { get; set; }
            public Decimal ValorTotalComissaoDiaCV { get; set; }
            public Decimal ValorTotalComissaoDiaAV { get; set; }
            public Decimal ValorTotalComissaoDiaAJ { get; set; }
            public Decimal ValorTotalDiaLiquido { get; set; }
            public Decimal ValorTotalDiaTotal { get; set; }
            public int QtdeTotalDiaCV { get; set; }
            public int QtdeTotalDiaCP { get; set; }
            public int QtdeTotalDiaAV { get; set; }
            public int QtdeTotalDiaAP { get; set; }
            public int QtdeTotalDiaAJ { get; set; }
        }

        class ItemAgrupamentoReferencia : ItemAgrupamentoFavorecido
        {
            public Decimal ValorTotalReferenciaCV { get; set; }
            public Decimal ValorTotalReferenciaAV { get; set; }
            public Decimal ValorTotalReferenciaAJ { get; set; }
            public Decimal ValorTotalReferenciaCP { get; set; }
            public Decimal ValorTotalReferenciaAP { get; set; }
            public Decimal ValorTotalComissaoReferenciaCV { get; set; }
            public Decimal ValorTotalComissaoReferenciaAV { get; set; }
            public Decimal ValorTotalComissaoReferenciaAJ { get; set; }
            public Decimal ValorTotalReferenciaLiquido { get; set; }
            public Decimal ValorTotalReferenciaTotal { get; set; }
            public int QtdeTotalReferenciaCV { get; set; }
            public int QtdeTotalReferenciaCP { get; set; }
            public int QtdeTotalReferenciaAV { get; set; }
            public int QtdeTotalReferenciaAP { get; set; }
            public int QtdeTotalReferenciaAJ { get; set; }
        }

        class ItemRelatorioConsolidadoFavorecido
        {
            public ItemRelatorioConsolidadoFavorecido()
            {
                this.AgrupamentoFavorecido = new ItemAgrupamentoFavorecido();
                this.Itens = new List<RelatorioConsolidadoInfo>();
            }
            public ItemAgrupamentoFavorecido AgrupamentoFavorecido { get; set; }
            public List<RelatorioConsolidadoInfo> Itens { get; set; }
            public string Total { get; set; }
        }

        class ItemRelatorioConsolidadoFavorecidoReferencia
        {
            public ItemRelatorioConsolidadoFavorecidoReferencia()
            {
                this.AgrupamentoReferencia = new ItemAgrupamentoReferencia();
                this.Itens = new List<RelatorioConsolidadoInfo>();
            }
            public ItemAgrupamentoReferencia AgrupamentoReferencia { get; set; }
            public List<RelatorioConsolidadoInfo> Itens { get; set; }
            public string Total { get; set; }
        }

        class ItemRelatorioConsolidadoData
        {
            public ItemRelatorioConsolidadoData()
            {
                this.AgrupamentoData = new ItemAgrupamentoData();
                this.Itens = new List<RelatorioConsolidadoInfo>();
            }
            public ItemAgrupamentoData AgrupamentoData { get; set; }
            public List<RelatorioConsolidadoInfo> Itens { get; set; }
            public string Total { get; set; }
        }

        public Decimal Exibe { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            FavorecidoRepeater.ItemDataBound += new RepeaterItemEventHandler(FavorecidoRepeater_ItemDataBound);

            string codLog = null;
            string codSessao = null;

            if (Session["CodigoSessao"] != null)
            {
                codSessao = (string)this.Session["CodigoSessao"];
            }
            else
            {
                codSessao = "Sem Sessão";
            }

            //// Pede o detalhe da pessoa
            resposta =
                    Mensageria.Processar<ListarRelatorioConsolidadoResponse>(
                        new ListarRelatorioConsolidadoRequest()
                        {
                            CodigoSessao = codSessao,
                            FiltroTipoConsolidado = this.Request["TipoRelatorio"],
                            FiltroQuebraFavorecido = Convert.ToBoolean(this.Request["SubTotalFavorecido"]),
                            FiltroProduto = this.Request["CodigoProduto"],
                            FiltroFavorecidoOriginal = this.Request["CodigoFavorecidoOriginal"],
                            FiltroCodigoTipoRegistro = this.Request["CodigoTipoRegistro"],
                            FiltroDescricaoTipoRegistro = this.Request["DescricaoTipoRegistro"],
                            FiltroEstabelecimento = this.Request["CodigoEstabelecimento"],
                            FiltroMeioPagamento = this.Request["CodigoMeioPagamento"],
                            MeioPagamento = this.Request["MeioPagamento"],
                            FiltroLayoutVenda = this.Request["LayoutRelatorioVenda"],
                            FiltroLayoutRecebimento = this.Request["LayoutRelatorioRecebimento"],
                            FiltroQtdeVenda = Convert.ToBoolean(this.Request["QtdeTransacaoVenda"]),
                            FiltroQtdeRecebimento = Convert.ToBoolean(this.Request["QtdeTransacaoRecebimento"]),
                            FiltroDataAgendamentoDe = this.Request["DataAgendamentoDe"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataAgendamentoDe"]) : null,
                            FiltroDataAgendamentoAte = this.Request["DataAgendamentoAte"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataAgendamentoAte"]) : null,
                            FiltroDataInicioMovimento = this.Request["DataInicioMovimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioMovimento"]) : null,
                            FiltroDataFimMovimento = this.Request["DataFimMovimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimMovimento"]) : null,
                            FiltroCodigoStatusCCI = this.Request["CodigoStatusCCI"],
                            CodLog = codLog,
                            // ECOMMERCE - Fernando Bove - 20160106
                            FiltroFinanceiroContabil = this.Request["TipoFinanceiroContabil"],
                            FiltroReferencia = this.Request["CodigoReferencia"],
                            FiltroQuebraReferencia = Convert.ToBoolean(this.Request["SubTotalReferencia"]),
                            RetornarRelatorio = true,
                            TipoArquivo = this.Request["TipoArquivo"] != null ? this.Request["TipoArquivo"].ToString() : null //Marcos Matsuoka - Jira SCF 1085
                        });


            //Na homologação do Jira 1056 vimos que a instrução abaixo (for) deixa esses campos > Marcos Matsuoka
            //  positivo no relatório. Alguns campos no BD está negativo.                                                         >
            for (int i = 0; i < resposta.Resultado.Count; i++)
            {
                resposta.Resultado[i].ValorCP = (resposta.Resultado[i].ValorCP * -1);
                resposta.Resultado[i].ValorAP = (resposta.Resultado[i].ValorAP * -1);
                resposta.Resultado[i].ValorAJ = (resposta.Resultado[i].ValorAJ * -1);
                resposta.Resultado[i].ValorTotal = (resposta.Resultado[i].ValorTotal * -1);
            }

            var favorecidos =
                    from y in resposta.Resultado
                    orderby y.NomeFavorecido
                    group y by new { y.NomeFavorecido } into g
                    select new ItemRelatorioConsolidadoFavorecido()
                    {
                        Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                    };
            this.Exibe = favorecidos.Count();

            if (this.Request["TipoRelatorio"] == "V")
            {
                decimal totalValorBrutoCV = resposta.Resultado.Sum(c => c.ValorBrutoCV);
                this.lblTotalValorBrutoCV.Text = totalValorBrutoCV.ToString("n");
                decimal totalValorBrutoAV = resposta.Resultado.Sum(c => c.ValorBrutoAV);
                this.lblTotalValorBrutoAV.Text = totalValorBrutoAV.ToString("n");
                decimal totalValorBrutoAJ = resposta.Resultado.Sum(c => c.ValorBrutoAJ);
                this.lblTotalValorBrutoAJ.Text = totalValorBrutoAJ.ToString("n");
                decimal totalValorComissaoCV = resposta.Resultado.Sum(c => c.ValorComissaoCV);
                this.lblTotalValorComissaoCV.Text = totalValorComissaoCV.ToString("n");
                decimal totalValorComissaoAV = resposta.Resultado.Sum(c => c.ValorComissaoAV);
                this.lblTotalValorComissaoAV.Text = totalValorComissaoAV.ToString("n");
                decimal totalValorComissaoAJ = resposta.Resultado.Sum(c => c.ValorComissaoAJ);
                this.lblTotalValorComissaoAJ.Text = totalValorComissaoAJ.ToString("n");
                decimal totalLiquido = resposta.Resultado.Sum(c => c.TotalLiquido);
                this.lblTotalLiquido.Text = totalLiquido.ToString("n");

                if (Convert.ToBoolean(this.Request["QtdeTransacaoVenda"]))
                {
                    int totalQtdeCV = resposta.Resultado.Sum(c => c.QtdeCV);
                    this.lblTotalQtdeCV.Text = totalQtdeCV.ToString();
                    decimal totalQtdeAV = resposta.Resultado.Sum(c => c.QtdeAV);
                    this.lblTotalQtdeAV.Text = totalQtdeAV.ToString();
                    decimal totalQtdeAJ = resposta.Resultado.Sum(c => c.QtdeAJ);
                    this.lblTotalQtdeAJ.Text = totalQtdeAJ.ToString();
                }
                this.divVendasTotal.Visible = true;

            }
            else
            {
                favorecidos =
                    from y in resposta.Resultado
                    orderby y.MeioPagamento
                    group y by new { y.NomeFavorecido } into g
                    select new ItemRelatorioConsolidadoFavorecido()
                    {
                        Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                    };

                decimal totalValorCP = resposta.Resultado.Sum(c => c.ValorCP);
                this.lblTotalValorCP.Text = totalValorCP.ToString("n");
                decimal totalValorAP = resposta.Resultado.Sum(c => c.ValorAP);
                this.lblTotalValorAP.Text = totalValorAP.ToString("n");
                decimal totalValorAJ = resposta.Resultado.Sum(c => c.ValorAJ);
                this.lblTotalValorAJ.Text = totalValorAJ.ToString("n");
                decimal totalValor = resposta.Resultado.Sum(c => c.ValorTotal);
                this.lblTotalValor.Text = totalValor.ToString("n");

                if (Convert.ToBoolean(this.Request["QtdeTransacaoRecebimento"]))
                {
                    int totalQtdeCP = resposta.Resultado.Sum(c => c.QtdeCP);
                    this.lblTotalQtdeCP.Text = totalQtdeCP.ToString();
                    decimal totalQtdeAP = resposta.Resultado.Sum(c => c.QtdeAP);
                    this.lblTotalQtdeAP.Text = totalQtdeAP.ToString();
                    decimal totalQtdeAJ = resposta.Resultado.Sum(c => c.QtdeAJ);
                    this.lblTotalQtdeAJR.Text = totalQtdeAJ.ToString();
                }
                this.divRecebimentoTotal.Visible = true;
            }

            if (Convert.ToBoolean(this.Request["SubTotalFavorecido"]) && !Convert.ToBoolean(this.Request["SubTotalReferencia"]))
            {
                if (this.Request["TipoRelatorio"] == "V")
                {
                    // Agrupa por Favorecido
                    favorecidos =
                        from y in resposta.Resultado
                        orderby y.NomeFavorecido, y.NomeProduto
                        group y by new { y.NomeFavorecido } into g
                        select new ItemRelatorioConsolidadoFavorecido()
                        {
                            AgrupamentoFavorecido = new ItemAgrupamentoFavorecido
                            {
                                DescricaoFavorecido = g.Key.NomeFavorecido,
                                ValorTotalCV = g.Sum(p => p.ValorBrutoCV),
                                ValorTotalAV = g.Sum(p => p.ValorBrutoAV),
                                ValorTotalAJ = g.Sum(p => p.ValorBrutoAJ),
                                ValorTotalComissaoCV = g.Sum(p => p.ValorComissaoCV),
                                ValorTotalComissaoAV = g.Sum(p => p.ValorComissaoAV),
                                ValorTotalComissaoAJ = g.Sum(p => p.ValorComissaoAJ),
                                ValorTotalLiquido = g.Sum(p => p.TotalLiquido),
                                QtdeTotalCV = g.Sum(p => p.QtdeCV),
                                QtdeTotalAV = g.Sum(p => p.QtdeAV),
                                QtdeTotalAJ = g.Sum(p => p.QtdeAJ)
                            },
                            Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                        };
                }
                else
                {
                    // Agrupa por Favorecido
                    favorecidos =
                        from y in resposta.Resultado
                        orderby y.NomeFavorecido, y.MeioPagamento
                        group y by new { y.NomeFavorecido } into g
                        select new ItemRelatorioConsolidadoFavorecido()
                        {
                            AgrupamentoFavorecido = new ItemAgrupamentoFavorecido
                            {
                                DescricaoFavorecido = g.Key.NomeFavorecido,
                                ValorTotalCP = g.Sum(p => p.ValorCP),
                                ValorTotalAP = g.Sum(p => p.ValorAP),
                                ValorTotalAJ = g.Sum(p => p.ValorAJ),
                                ValorTotalTotal = g.Sum(p => p.ValorTotal),
                                QtdeTotalCP = g.Sum(p => p.QtdeCP),
                                QtdeTotalAP = g.Sum(p => p.QtdeAP),
                                QtdeTotalAJ = g.Sum(p => p.QtdeAJ)
                            },
                            Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                        };
                }
            }
            if (Convert.ToBoolean(this.Request["SubTotalReferencia"]) && !Convert.ToBoolean(this.Request["SubTotalFavorecido"]))
            {
                if (this.Request["TipoRelatorio"] == "V")
                {
                    favorecidos =
                        from y in resposta.Resultado
                        orderby y.NomeFavorecido, y.NomeProduto
                        group y by new { y.CodigoReferencia, y.NomeReferencia } into g
                        select new ItemRelatorioConsolidadoFavorecido()
                        {
                            AgrupamentoFavorecido = new ItemAgrupamentoFavorecido
                            {
                                DescricaoReferencia = g.Key.CodigoReferencia + " - " + g.Key.NomeReferencia,
                                ValorTotalCV = g.Sum(p => p.ValorBrutoCV),
                                ValorTotalAV = g.Sum(p => p.ValorBrutoAV),
                                ValorTotalAJ = g.Sum(p => p.ValorBrutoAJ),
                                ValorTotalComissaoCV = g.Sum(p => p.ValorComissaoCV),
                                ValorTotalComissaoAV = g.Sum(p => p.ValorComissaoAV),
                                ValorTotalComissaoAJ = g.Sum(p => p.ValorComissaoAJ),
                                ValorTotalLiquido = g.Sum(p => p.TotalLiquido),
                                QtdeTotalCV = g.Sum(p => p.QtdeCV),
                                QtdeTotalAV = g.Sum(p => p.QtdeAV),
                                QtdeTotalAJ = g.Sum(p => p.QtdeAJ)
                            },
                            Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                        };
                }
                else
                {
                    // Agrupa por Favorecido
                    favorecidos =
                        from y in resposta.Resultado
                        orderby y.NomeFavorecido, y.MeioPagamento
                        group y by new { y.CodigoReferencia, y.NomeReferencia } into g
                        select new ItemRelatorioConsolidadoFavorecido()
                        {
                            AgrupamentoFavorecido = new ItemAgrupamentoFavorecido
                            {
                                DescricaoReferencia = g.Key.CodigoReferencia + " - " + g.Key.NomeReferencia,
                                ValorTotalCP = g.Sum(p => p.ValorCP),
                                ValorTotalAP = g.Sum(p => p.ValorAP),
                                ValorTotalAJ = g.Sum(p => p.ValorAJ),
                                ValorTotalTotal = g.Sum(p => p.ValorTotal),
                                QtdeTotalCP = g.Sum(p => p.QtdeCP),
                                QtdeTotalAP = g.Sum(p => p.QtdeAP),
                                QtdeTotalAJ = g.Sum(p => p.QtdeAJ)
                            },
                            Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                        };
                }
            }
            if (Convert.ToBoolean(this.Request["SubTotalReferencia"]) && Convert.ToBoolean(this.Request["SubTotalFavorecido"]))
            {
                if (this.Request["TipoRelatorio"] == "V")
                {
                    favorecidos =
                        from y in resposta.Resultado
                        orderby y.NomeFavorecido, y.CodigoReferencia
                        group y by new { y.NomeFavorecido, y.CodigoReferencia, y.NomeReferencia } into g
                        select new ItemRelatorioConsolidadoFavorecido()
                        {
                            AgrupamentoFavorecido = new ItemAgrupamentoFavorecido
                            {
                                CodigoReferencia = g.Key.CodigoReferencia,
                                DescricaoReferencia = g.Key.CodigoReferencia + " - " + g.Key.NomeReferencia,
                                DescricaoFavorecido = g.Key.NomeFavorecido,
                                ValorTotalCV = g.Sum(p => p.ValorBrutoCV),
                                ValorTotalAV = g.Sum(p => p.ValorBrutoAV),
                                ValorTotalAJ = g.Sum(p => p.ValorBrutoAJ),
                                ValorTotalComissaoCV = g.Sum(p => p.ValorComissaoCV),
                                ValorTotalComissaoAV = g.Sum(p => p.ValorComissaoAV),
                                ValorTotalComissaoAJ = g.Sum(p => p.ValorComissaoAJ),
                                ValorTotalLiquido = g.Sum(p => p.TotalLiquido),
                                QtdeTotalCV = g.Sum(p => p.QtdeCV),
                                QtdeTotalAV = g.Sum(p => p.QtdeAV),
                                QtdeTotalAJ = g.Sum(p => p.QtdeAJ)
                            },
                            Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                        };
                }
                else
                {
                    // Agrupa por Favorecido
                    favorecidos =
                        from y in resposta.Resultado
                        orderby y.NomeFavorecido, y.MeioPagamento
                        group y by new { y.NomeFavorecido, y.CodigoReferencia, y.NomeReferencia } into g
                        select new ItemRelatorioConsolidadoFavorecido()
                        {
                            AgrupamentoFavorecido = new ItemAgrupamentoFavorecido
                            {
                                DescricaoFavorecido = g.Key.NomeFavorecido,
                                DescricaoReferencia = g.Key.CodigoReferencia + " - " + g.Key.NomeReferencia,
                                ValorTotalCP = g.Sum(p => p.ValorCP),
                                ValorTotalAP = g.Sum(p => p.ValorAP),
                                ValorTotalAJ = g.Sum(p => p.ValorAJ),
                                ValorTotalTotal = g.Sum(p => p.ValorTotal),
                                QtdeTotalCP = g.Sum(p => p.QtdeCP),
                                QtdeTotalAP = g.Sum(p => p.QtdeAP),
                                QtdeTotalAJ = g.Sum(p => p.QtdeAJ)
                            },
                            Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                        };
                }
            }

            string msg = "Filtro Período Dt Agendamento informado, valores não apresentados para Atacadão";

            if (this.Request["DataAgendamentoDe"] != null || this.Request["DataAgendamentoAte"] != null)
            {
                String reg = "";
                if (resposta.ListaConfigTipoRegistro.Count > 0)
                {
                    foreach (ConfiguracaoTipoRegistroInfo config in resposta.ListaConfigTipoRegistro)
                        reg = reg + config.TipoRegistro + ", ";

                    reg = reg.Trim();
                    reg = reg.Substring(0, reg.Length - 1);

                    if (resposta.ListaConfigTipoRegistro.Count > 0)
                    {
                        this.lblMensagem.Text = (
                            msg + " " +
                            "e Tipo Registro [" + reg + "].");
                        this.trMensagem.Visible = true;
                    }
                }
                else
                {
                    this.lblMensagem.Text = msg + ".";
                    this.trMensagem.Visible = true;
                }
            }

            this.lblFiltroFavorecido.Text = this.Request["FavorecidoOriginal"] != null ? this.Request["FavorecidoOriginal"] : "Todos";
            this.lblFiltroEstabelecimento.Text = this.Request["Estabelecimento"] != null ? this.Request["Estabelecimento"] : "Todos";

            this.lblFiltroFinanceiroContabil.Text = this.Request["TipoFinanceiroContabil"] != null ? (this.Request["TipoFinanceiroContabil"] == "C" ? "Contábil" : "Financeiro") : "Todos";

            this.lblFiltroPMP.Text = this.Request["TipoRelatorio"] != "R" ? "Produto" : "Meio Pagto";
            this.lblFiltroProdutoMeioPagamento.Text = this.Request["TipoRelatorio"] != "R" ? this.Request["Produto"] != null ? this.Request["Produto"] : "Todos" : this.Request["MeioPagamento"] != null ? this.Request["MeioPagamento"] : "Todos";

            this.lblFiltroTipoRegistro.Text = this.Request["DescricaoTipoRegistro"] != null ? this.Request["DescricaoTipoRegistro"] : "Todos";
            this.lblFiltroQtde.Text = this.Request["TipoRelatorio"] != "R" ? Convert.ToBoolean(this.Request["QtdeTransacaoVenda"]) != false ? "Sim" : "Não" : Convert.ToBoolean(this.Request["QtdeTransacaoRecebimento"]) != false ? "Sim" : "Não";
            this.lblFiltroStatusCCI.Text = this.Request["DescricaoStatusCCI"] != null ? this.Request["DescricaoStatusCCI"] : "Todos";
            //
            this.lblFiltroDataAgendamentoDe.Text = this.Request["DataAgendamentoDe"] != null ? this.Request["DataAgendamentoDe"] : "Qualquer";
            this.lblFiltroDataAgendamentoAte.Text = this.Request["DataAgendamentoAte"] != null ? this.Request["DataAgendamentoAte"] : "Qualquer";
            this.lblFiltroDataProcessamentoInicio.Text = this.Request["DataInicioMovimento"] != null ? this.Request["DataInicioMovimento"] : "Qualquer";
            this.lblFiltroDataProcessamentoFim.Text = this.Request["DataFimMovimento"] != null ? this.Request["DataFimMovimento"] : "Qualquer";
            //
            this.lblDataHora.Text = String.Format("{0:dd/MM/yyyy - HH:mm}", DateTime.Now);
            this.lblTipoRelatorio.Text = this.Request["TipoRelatorio"] != "R" ? "VENDAS" : "RECEBIMENTOS";
            this.lblSubTotalFavorecido.Text = Convert.ToBoolean(this.Request["SubTotalFavorecido"]) != false ? "por Favorecido" : "";
            this.lblConsolidarReferencia.Text = Convert.ToBoolean(this.Request["SubTotalReferencia"]) != false ? "Sim" : "Não";
            this.lblReferencia.Text = this.Request["Referencia"] != null ? this.Request["Referencia"] : "Todos";
            this.lblLayout.Text =
                this.Request["TipoRelatorio"] != "R" ?
                    this.Request["LayoutRelatorioVenda"] == "T" ?
                        "Totalizado por Produto" :
                        this.Request["LayoutRelatorioVenda"] == "A" ?
                            "Analítico por Data" :
                            "Sintético por Data" :
                    this.Request["LayoutRelatorioRecebimento"] == "T" ?
                        "Totalizado por Meio Pagamento" :
                        this.Request["LayoutRelatorioRecebimento"] == "C" ?
                            "Totalizador envio CCI" :
                            this.Request["LayoutRelatorioRecebimento"] == "A" ?
                            "Analítico por Data" :
                            "Sintético por Data";

            FavorecidoRepeater.DataSource = favorecidos;
            FavorecidoRepeater.DataBind();

            // Marcos Matsuoka - Jira SCF 1109 (Esse If estava logo após os filtros 'logo após resposta =')
            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;

            if (resposta.Resultado.Count == 0)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Sem dados para imprimir pelo filtro informado.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Sem dados para imprimir pelo filtro informado.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
            else if (resposta.Resultado.Count > 1000 && (this.Request["TipoArquivo"] != "PDF")) //Marcos Matsuoka - Jira SCF 1085 (adicionado tratamento para PDF)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
        }

        void FavorecidoRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater DataRepeater = (Repeater)e.Item.FindControl("DataRepeater");
            DataRepeater.ItemDataBound += new RepeaterItemEventHandler(DataRepeater_ItemDataBound);
            ItemRelatorioConsolidadoFavorecido data = (ItemRelatorioConsolidadoFavorecido)e.Item.DataItem;

            // Agrupa por datas
            var datas =
                from y in data.Itens
                group y by new { y.NomeFavorecido, DataProcessamento = y.DataProcessamento != null ? ((DateTime)y.DataProcessamento.Value).ToString("dd/MM/yyyy") : string.Empty } into g
                select new ItemRelatorioConsolidadoData()
                {
                    Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                };

            if (Convert.ToBoolean(this.Request["SubTotalFavorecido"]) && !Convert.ToBoolean(this.Request["SubTotalReferencia"]))
            {
                HtmlTableRow divQuebraFavorecido = e.Item.FindControl("divQuebraFavorecido") as HtmlTableRow;
                divQuebraFavorecido.Visible = true;
            }

            if (Convert.ToBoolean(this.Request["SubTotalReferencia"]) && !Convert.ToBoolean(this.Request["SubTotalFavorecido"]))
            {
                HtmlTableRow divQuebraReferencia = e.Item.FindControl("divQuebraReferencia") as HtmlTableRow;
                divQuebraReferencia.Visible = true;
            }

            if (Convert.ToBoolean(this.Request["SubTotalFavorecido"]) && Convert.ToBoolean(this.Request["SubTotalReferencia"]))
            {
                HtmlTableRow divQuebraFavorecido = e.Item.FindControl("divQuebraFavorecido") as HtmlTableRow;
                divQuebraFavorecido.Visible = true;
                HtmlTableRow divQuebraReferencia = e.Item.FindControl("divQuebraReferencia") as HtmlTableRow;
                divQuebraReferencia.Visible = true;
            }

            if (this.Request["TipoRelatorio"] == "V")
            {
                if (this.Request["LayoutRelatorioVenda"] == "T")
                {
                    HtmlTableCell divVendas1 = e.Item.FindControl("divVendas1") as HtmlTableCell;
                    HtmlTableCell divVendas2 = e.Item.FindControl("divVendas2") as HtmlTableCell;
                    HtmlTableCell divVendas3 = e.Item.FindControl("divVendas3") as HtmlTableCell;
                    HtmlTableCell divVendas4 = e.Item.FindControl("divVendas4") as HtmlTableCell;
                    HtmlTableCell divVendas5 = e.Item.FindControl("divVendas5") as HtmlTableCell;
                    HtmlTableCell divVendas6 = e.Item.FindControl("divVendas6") as HtmlTableCell;
                    HtmlTableCell divVendas7 = e.Item.FindControl("divVendas7") as HtmlTableCell;
                    HtmlTableCell divVendas8 = e.Item.FindControl("divVendas8") as HtmlTableCell;
                    divVendas1.Visible = true;
                    divVendas2.Visible = true;
                    divVendas3.Visible = true;
                    divVendas4.Visible = true;
                    divVendas5.Visible = true;
                    divVendas6.Visible = true;
                    divVendas7.Visible = true;
                    divVendas8.Visible = true;
                }

                if (Convert.ToBoolean(this.Request["SubTotalFavorecido"]) || Convert.ToBoolean(this.Request["SubTotalReferencia"]))
                {
                    HtmlTableCell divVendasEvalTotal = e.Item.FindControl("divVendasEvalTotal") as HtmlTableCell;
                    HtmlTableCell divVendasEvalTotal1 = e.Item.FindControl("divVendasEvalTotal1") as HtmlTableCell;
                    HtmlTableCell divVendasEvalTotal2 = e.Item.FindControl("divVendasEvalTotal2") as HtmlTableCell;
                    HtmlTableCell divVendasEvalTotal3 = e.Item.FindControl("divVendasEvalTotal3") as HtmlTableCell;
                    HtmlTableCell divVendasEvalTotal4 = e.Item.FindControl("divVendasEvalTotal4") as HtmlTableCell;
                    HtmlTableCell divVendasEvalTotal5 = e.Item.FindControl("divVendasEvalTotal5") as HtmlTableCell;
                    HtmlTableCell divVendasEvalTotal6 = e.Item.FindControl("divVendasEvalTotal6") as HtmlTableCell;
                    HtmlTableCell divVendasEvalTotal10 = e.Item.FindControl("divVendasEvalTotal10") as HtmlTableCell;
                    divVendasEvalTotal.Visible = true;
                    divVendasEvalTotal1.Visible = true;
                    divVendasEvalTotal2.Visible = true;
                    divVendasEvalTotal3.Visible = true;
                    divVendasEvalTotal4.Visible = true;
                    divVendasEvalTotal5.Visible = true;
                    divVendasEvalTotal6.Visible = true;
                    divVendasEvalTotal10.Visible = true;
                }

                if (Convert.ToBoolean(this.Request["QtdeTransacaoVenda"]))
                {
                    if (this.Request["LayoutRelatorioVenda"] == "T")
                    {
                        HtmlTableCell divVendasQtde = e.Item.FindControl("divVendasQtde") as HtmlTableCell;
                        divVendasQtde.Visible = true;
                        HtmlTableCell divVendasQtde1 = e.Item.FindControl("divVendasQtde1") as HtmlTableCell;
                        divVendasQtde1.Visible = true;
                        HtmlTableCell divVendasQtde2 = e.Item.FindControl("divVendasQtde2") as HtmlTableCell;
                        divVendasQtde2.Visible = true;
                    }

                    if (Convert.ToBoolean(this.Request["SubTotalFavorecido"]))
                    {
                        HtmlTableCell divVendasEvalTotal7 = e.Item.FindControl("divVendasEvalTotal7") as HtmlTableCell;
                        HtmlTableCell divVendasEvalTotal8 = e.Item.FindControl("divVendasEvalTotal8") as HtmlTableCell;
                        HtmlTableCell divVendasEvalTotal9 = e.Item.FindControl("divVendasEvalTotal9") as HtmlTableCell;
                        divVendasEvalTotal7.Visible = true;
                        divVendasEvalTotal8.Visible = true;
                        divVendasEvalTotal9.Visible = true;
                    }
                }

                // Agrupa por datas
                datas =
                    from y in data.Itens
                    orderby y.NomeFavorecido, y.DataProcessamento, y.NomeProduto
                    group y by new { y.NomeFavorecido, DataProcessamento = y.DataProcessamento != null ? ((DateTime)y.DataProcessamento.Value).ToString("dd/MM/yyyy") : string.Empty } into g
                    select new ItemRelatorioConsolidadoData()
                    {
                        AgrupamentoData = new ItemAgrupamentoData
                        {
                            DescricaoData = g.Key.DataProcessamento,
                            ValorTotalDiaCV = g.Sum(p => p.ValorBrutoCV),
                            ValorTotalDiaAV = g.Sum(p => p.ValorBrutoAV),
                            ValorTotalDiaAJ = g.Sum(p => p.ValorBrutoAJ),
                            ValorTotalComissaoDiaCV = g.Sum(p => p.ValorComissaoCV),
                            ValorTotalComissaoDiaAV = g.Sum(p => p.ValorComissaoAV),
                            ValorTotalComissaoDiaAJ = g.Sum(p => p.ValorComissaoAJ),
                            ValorTotalDiaLiquido = g.Sum(p => p.TotalLiquido),
                            QtdeTotalDiaCV = g.Sum(p => p.QtdeCV),
                            QtdeTotalDiaAV = g.Sum(p => p.QtdeAV),
                            QtdeTotalDiaAJ = g.Sum(p => p.QtdeAJ)
                        },
                        Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                    };
            }
            else
            {
                if (this.Request["LayoutRelatorioRecebimento"] == "C" || this.Request["LayoutRelatorioRecebimento"] == "T")
                {
                    HtmlTableCell divRecebimento = e.Item.FindControl("divRecebimento") as HtmlTableCell;
                    divRecebimento.Visible = true;
                    HtmlTableCell divRecebimento1 = e.Item.FindControl("divRecebimento1") as HtmlTableCell;
                    divRecebimento1.Visible = true;
                    HtmlTableCell divRecebimento2 = e.Item.FindControl("divRecebimento2") as HtmlTableCell;
                    divRecebimento2.Visible = true;
                    HtmlTableCell divRecebimento3 = e.Item.FindControl("divRecebimento3") as HtmlTableCell;
                    divRecebimento3.Visible = true;
                    HtmlTableCell divRecebimento4 = e.Item.FindControl("divRecebimento4") as HtmlTableCell;
                    divRecebimento4.Visible = true;
                    HtmlTableCell divRecebimento5 = e.Item.FindControl("divRecebimento5") as HtmlTableCell;
                    divRecebimento5.Visible = true;
                    HtmlTableCell divRecebimento6 = e.Item.FindControl("divRecebimento6") as HtmlTableCell;
                    divRecebimento6.Visible = true;
                    HtmlTableCell divRecebimento7 = e.Item.FindControl("divRecebimento7") as HtmlTableCell;
                    divRecebimento7.Visible = true;
                }


                if (Convert.ToBoolean(this.Request["SubTotalFavorecido"]) || Convert.ToBoolean(this.Request["SubTotalReferencia"]))
                {
                    //Na homologação do Jira 1056 encontramos erro na geração do relatorio com os filtros > -  Marcos Matsuoka
                    //  recebimento e subtotal/quebra por favorecido onde alguns campos abaixo não        > -  'inserção de campos'
                    //  estavam sendo usados ocasionando em um relatório sem esses dados.                 >     
                    HtmlTableCell divRecebimentoEvalTotal = e.Item.FindControl("divRecebimentoEvalTotal") as HtmlTableCell;
                    divRecebimentoEvalTotal.Visible = true;
                    HtmlTableCell divRecebimentoEvalTotal1 = e.Item.FindControl("divRecebimentoEvalTotal1") as HtmlTableCell;
                    divRecebimentoEvalTotal1.Visible = true;
                    HtmlTableCell divRecebimentoEvalTotal2 = e.Item.FindControl("divRecebimentoEvalTotal2") as HtmlTableCell;
                    divRecebimentoEvalTotal2.Visible = true;
                    HtmlTableCell divRecebimentoEvalTotal3 = e.Item.FindControl("divRecebimentoEvalTotal3") as HtmlTableCell;
                    divRecebimentoEvalTotal3.Visible = true;
                    HtmlTableCell divRecebimentoEvalTotal4 = e.Item.FindControl("divRecebimentoEvalTotal4") as HtmlTableCell;
                    divRecebimentoEvalTotal4.Visible = true;
                    HtmlTableCell divRecebimentoEvalTotal5 = e.Item.FindControl("divRecebimentoEvalTotal5") as HtmlTableCell;
                    divRecebimentoEvalTotal5.Visible = true;
                    HtmlTableCell divRecebimentoEvalTotal6 = e.Item.FindControl("divRecebimentoEvalTotal6") as HtmlTableCell;
                    divRecebimentoEvalTotal6.Visible = true;
                    HtmlTableCell divRecebimentoEvalTotal7 = e.Item.FindControl("divRecebimentoEvalTotal7") as HtmlTableCell;
                    divRecebimentoEvalTotal7.Visible = true;
                }

                if (this.Request["LayoutRelatorioRecebimento"] != "C")
                {
                    HtmlGenericControl lblMeioPagamentoHeader = e.Item.FindControl("lblMeioPagamentoHeader") as HtmlGenericControl;
                    lblMeioPagamentoHeader.Visible = true;
                }

                if (Convert.ToBoolean(this.Request["QtdeTransacaoRecebimento"]))
                {
                    if (this.Request["LayoutRelatorioRecebimento"] == "C" || this.Request["LayoutRelatorioRecebimento"] == "T")
                    {
                        HtmlTableCell divVendasQtde = e.Item.FindControl("divRecebimento4") as HtmlTableCell;
                        divVendasQtde.Visible = true;
                        HtmlTableCell divVendasQtde1 = e.Item.FindControl("divRecebimento5") as HtmlTableCell;
                        divVendasQtde1.Visible = true;
                        HtmlTableCell divVendasQtde2 = e.Item.FindControl("divRecebimento7") as HtmlTableCell;
                        divVendasQtde2.Visible = true;
                    }
                }

                // Agrupa por datas
                datas =
                    from y in data.Itens
                    orderby y.NomeFavorecido, y.DataProcessamento, y.MeioPagamento
                    group y by new { y.NomeFavorecido, DataProcessamento = y.DataProcessamento != null ? ((DateTime)y.DataProcessamento.Value).ToString("dd/MM/yyyy") : string.Empty } into g
                    select new ItemRelatorioConsolidadoData()
                    {
                        AgrupamentoData = new ItemAgrupamentoData
                        {
                            DescricaoData = g.Key.DataProcessamento,
                            ValorTotalDiaCP = g.Sum(p => p.ValorCP),
                            ValorTotalDiaAP = g.Sum(p => p.ValorAP),
                            ValorTotalDiaAJ = g.Sum(p => p.ValorAJ),
                            ValorTotalDiaTotal = g.Sum(p => p.ValorTotal),
                            QtdeTotalDiaCP = g.Sum(p => p.QtdeCP),
                            QtdeTotalDiaAP = g.Sum(p => p.QtdeAP),
                            QtdeTotalDiaAJ = g.Sum(p => p.QtdeAJ)
                        },
                        Itens = g.Select(p => p).ToList<RelatorioConsolidadoInfo>()
                    };
            }

            DataRepeater.DataSource = datas;
            DataRepeater.DataBind();
            this.Exibe -= 1;
        }

        void DataRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater ProdutoRepeater = (Repeater)e.Item.FindControl("ProdutoRepeater");
            ProdutoRepeater.ItemDataBound += new RepeaterItemEventHandler(ProdutoRepeater_ItemDataBound);
            ItemRelatorioConsolidadoData produtos = (ItemRelatorioConsolidadoData)e.Item.DataItem;

            if (this.Request["LayoutRelatorioVenda"] == "A" || this.Request["LayoutRelatorioVenda"] == "S" ||
                this.Request["LayoutRelatorioRecebimento"] == "A" || this.Request["LayoutRelatorioRecebimento"] == "S")
            {
                HtmlTableRow divDataProcessamento = e.Item.FindControl("divDataProcessamento") as HtmlTableRow;
                divDataProcessamento.Visible = true;

                if (this.Request["TipoRelatorio"] == "V")
                {
                    HtmlTableCell divVendasData = e.Item.FindControl("divVendasData") as HtmlTableCell;
                    divVendasData.Visible = true;
                    HtmlTableCell divVendasData2 = e.Item.FindControl("divVendasData2") as HtmlTableCell;
                    divVendasData2.Visible = true;
                    HtmlTableCell divVendasData3 = e.Item.FindControl("divVendasData3") as HtmlTableCell;
                    divVendasData3.Visible = true;
                    HtmlTableCell divVendasData4 = e.Item.FindControl("divVendasData4") as HtmlTableCell;
                    divVendasData4.Visible = true;
                    HtmlTableCell divVendasData5 = e.Item.FindControl("divVendasData5") as HtmlTableCell;
                    divVendasData5.Visible = true;
                    HtmlTableCell divVendasData6 = e.Item.FindControl("divVendasData6") as HtmlTableCell;
                    divVendasData6.Visible = true;
                    HtmlTableCell divVendasData7 = e.Item.FindControl("divVendasData7") as HtmlTableCell;
                    divVendasData7.Visible = true;

                    HtmlTableCell divVendasData8 = e.Item.FindControl("divVendasData8") as HtmlTableCell;
                    divVendasData8.Visible = true;

                    HtmlTableRow divVendasEvalTotalDia = e.Item.FindControl("divVendasEvalTotalDia") as HtmlTableRow;
                    divVendasEvalTotalDia.Visible = true;
                    HtmlTableRow divVendasEvalTotalDia1 = e.Item.FindControl("divVendasEvalTotalDia1") as HtmlTableRow;
                    divVendasEvalTotalDia1.Visible = true;

                    if (Convert.ToBoolean(this.Request["QtdeTransacaoVenda"]))
                    {
                        HtmlTableCell divVendasQtdeData = e.Item.FindControl("divVendasQtdeData") as HtmlTableCell;
                        divVendasQtdeData.Visible = true;
                        HtmlTableCell divVendasQtdeData1 = e.Item.FindControl("divVendasQtdeData1") as HtmlTableCell;
                        divVendasQtdeData1.Visible = true;
                        HtmlTableCell divVendasQtdeData2 = e.Item.FindControl("divVendasQtdeData2") as HtmlTableCell;
                        divVendasQtdeData2.Visible = true;

                        HtmlTableCell divVendasEvalQtdeTotalDia = e.Item.FindControl("divVendasEvalQtdeTotalDia") as HtmlTableCell;
                        divVendasEvalQtdeTotalDia.Visible = true;
                        HtmlTableCell divVendasEvalQtdeTotalDia1 = e.Item.FindControl("divVendasEvalQtdeTotalDia1") as HtmlTableCell;
                        divVendasEvalQtdeTotalDia1.Visible = true;
                        HtmlTableCell divVendasEvalQtdeTotalDia2 = e.Item.FindControl("divVendasEvalQtdeTotalDia2") as HtmlTableCell;
                        divVendasEvalQtdeTotalDia2.Visible = true;
                    }
                }
                else
                {
                    HtmlTableCell divRecebimentoData = e.Item.FindControl("divRecebimentoData") as HtmlTableCell;
                    divRecebimentoData.Visible = true;
                    HtmlTableCell divRecebimentoData1 = e.Item.FindControl("divRecebimentoData1") as HtmlTableCell;
                    divRecebimentoData1.Visible = true;
                    HtmlTableCell divRecebimentoData2 = e.Item.FindControl("divRecebimentoData2") as HtmlTableCell;
                    divRecebimentoData2.Visible = true;
                    HtmlTableCell divRecebimentoData3 = e.Item.FindControl("divRecebimentoData3") as HtmlTableCell;
                    divRecebimentoData3.Visible = true;
                    HtmlTableCell divRecebimentoData4 = e.Item.FindControl("divRecebimentoData4") as HtmlTableCell;
                    divRecebimentoData4.Visible = true;
                    HtmlTableCell divRecebimentoData5 = e.Item.FindControl("divRecebimentoData5") as HtmlTableCell;
                    divRecebimentoData5.Visible = true;
                    HtmlTableCell divRecebimentoData6 = e.Item.FindControl("divRecebimentoData6") as HtmlTableCell;
                    divRecebimentoData6.Visible = true;
                    HtmlTableCell divRecebimentoData7 = e.Item.FindControl("divRecebimentoData7") as HtmlTableCell;
                    divRecebimentoData7.Visible = true;

                    HtmlTableRow divRecebimentoEvalTotalDia = e.Item.FindControl("divRecebimentoEvalTotalDia") as HtmlTableRow;
                    divRecebimentoEvalTotalDia.Visible = true;

                    HtmlTableRow divRecebimentoEvalTotalDia1 = e.Item.FindControl("divRecebimentoEvalTotalDia1") as HtmlTableRow;
                    divRecebimentoEvalTotalDia1.Visible = true;

                    //Na homologação do Jira 1056 encontramos o erro onde o campo de vendas aparece como Null ocorrendo erro. -  Marcos Matsuoka
                    //HtmlTableRow divVendasEvalQtdeTotalDia1 = e.Item.FindControl("divVendasEvalQtdeTotalDia1") as HtmlTableRow;
                    //divVendasEvalQtdeTotalDia1.Visible = true;



                    HtmlTableCell divRecebimentoEvalQtdeTotalDia = e.Item.FindControl("divRecebimentoEvalQtdeTotalDia") as HtmlTableCell;
                    HtmlTableCell divRecebimentoEvalQtdeTotalDia1 = e.Item.FindControl("divRecebimentoEvalQtdeTotalDia1") as HtmlTableCell;
                    HtmlTableCell divRecebimentoEvalQtdeTotalDia2 = e.Item.FindControl("divRecebimentoEvalQtdeTotalDia2") as HtmlTableCell;

                    if (Convert.ToBoolean(this.Request["QtdeTransacaoRecebimento"]))
                    {
                        divRecebimentoData4.Visible = true;
                        divRecebimentoData5.Visible = true;
                        divRecebimentoData7.Visible = true;

                        divRecebimentoEvalQtdeTotalDia.Visible = true;
                        divRecebimentoEvalQtdeTotalDia1.Visible = true;
                        divRecebimentoEvalQtdeTotalDia2.Visible = true;
                    }
                    else
                    {
                        divRecebimentoData4.Visible = false;
                        divRecebimentoData5.Visible = false;
                        divRecebimentoData7.Visible = false;

                        divRecebimentoEvalQtdeTotalDia.Visible = false;
                        divRecebimentoEvalQtdeTotalDia1.Visible = false;
                        divRecebimentoEvalQtdeTotalDia2.Visible = false;
                    }
                }
            }

            ProdutoRepeater.DataSource = produtos.Itens;
            ProdutoRepeater.DataBind();
        }

        void ProdutoRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (this.Request["TipoRelatorio"] == "V")
            {
                HtmlTableCell divVendasEval = e.Item.FindControl("divVendasEval") as HtmlTableCell;
                HtmlTableCell divVendasEval2 = e.Item.FindControl("divVendasEval2") as HtmlTableCell;
                HtmlTableCell divVendasEval3 = e.Item.FindControl("divVendasEval3") as HtmlTableCell;
                HtmlTableCell divVendasEval4 = e.Item.FindControl("divVendasEval4") as HtmlTableCell;
                HtmlTableCell divVendasEval5 = e.Item.FindControl("divVendasEval5") as HtmlTableCell;
                HtmlTableCell divVendasEval6 = e.Item.FindControl("divVendasEval6") as HtmlTableCell;
                HtmlTableCell divVendasEval7 = e.Item.FindControl("divVendasEval7") as HtmlTableCell;
                HtmlTableCell divVendasEval8 = e.Item.FindControl("divVendasEval8") as HtmlTableCell;
                HtmlTableCell divVendasEval9 = e.Item.FindControl("divVendasEval9") as HtmlTableCell;
                HtmlTableCell divVendasEval10 = e.Item.FindControl("divVendasEval10") as HtmlTableCell;
                HtmlTableCell divVendasEval11 = e.Item.FindControl("divVendasEval11") as HtmlTableCell;
                divVendasEval.Visible = true;
                divVendasEval2.Visible = true;
                divVendasEval3.Visible = true;
                divVendasEval4.Visible = true;
                divVendasEval5.Visible = true;
                divVendasEval6.Visible = true;
                divVendasEval7.Visible = true;
                divVendasEval8.Visible = true;
                divVendasEval9.Visible = true;
                divVendasEval10.Visible = true;
                divVendasEval11.Visible = true;

                if (Convert.ToBoolean(this.Request["QtdeTransacaoVenda"]))
                {
                    divVendasEval8.Visible = true;
                    divVendasEval9.Visible = true;
                    divVendasEval10.Visible = true;
                }
                else
                {
                    divVendasEval8.Visible = false;
                    divVendasEval9.Visible = false;
                    divVendasEval10.Visible = false;
                }
            }
            else
            {
                HtmlTableCell divRecebimentoEval = e.Item.FindControl("divRecebimentoEval") as HtmlTableCell;
                HtmlTableCell divRecebimentoEval2 = e.Item.FindControl("divRecebimentoEval2") as HtmlTableCell;
                HtmlTableCell divRecebimentoEval3 = e.Item.FindControl("divRecebimentoEval3") as HtmlTableCell;
                HtmlTableCell divRecebimentoEval4 = e.Item.FindControl("divRecebimentoEval4") as HtmlTableCell;
                HtmlTableCell divRecebimentoEval5 = e.Item.FindControl("divRecebimentoEval5") as HtmlTableCell;
                HtmlTableCell divRecebimentoEval6 = e.Item.FindControl("divRecebimentoEval6") as HtmlTableCell;
                HtmlTableCell divRecebimentoEval7 = e.Item.FindControl("divRecebimentoEval7") as HtmlTableCell;
                HtmlTableCell divRecebimentoEval8 = e.Item.FindControl("divRecebimentoEval8") as HtmlTableCell;

                divRecebimentoEval.Visible = true;
                divRecebimentoEval2.Visible = true;
                divRecebimentoEval3.Visible = true;
                divRecebimentoEval4.Visible = true;
                divRecebimentoEval5.Visible = true;
                divRecebimentoEval6.Visible = true;
                divRecebimentoEval7.Visible = true;
                divRecebimentoEval8.Visible = true;

                if (Convert.ToBoolean(this.Request["QtdeTransacaoRecebimento"]))
                {
                    divRecebimentoEval5.Visible = true;
                    divRecebimentoEval6.Visible = true;
                    divRecebimentoEval8.Visible = true;
                }
                else
                {
                    divRecebimentoEval5.Visible = false;
                    divRecebimentoEval6.Visible = false;
                    divRecebimentoEval8.Visible = false;
                }
            }
        }
        /// <summary>
        /// Função utilizada para adicionar o fim de tabela até a penúltima
        /// e retirar na última devido ao Total Geral
        /// </summary>
        /// <returns></returns>
        public string Exibir()
        {
            if (this.Exibe == 1)
                return "";
            else
                return "</tbody></table>";
        }
    }
}