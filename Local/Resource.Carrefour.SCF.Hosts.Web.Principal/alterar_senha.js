﻿$(document).ready(function() {alterar_senha_load();});
// =============================================================================================
// LOAD
// =============================================================================================
function alterar_senha_load(callback) 
{
    $("#senhaAtual").focus();
    $("button").button();
    $("#subtitulo").html("LOGIN [Senha Expirada]"); // 1415 Insere o subtítulo

    // Inicializa popup de carregamento
    $("#loadingDialog").dialog({
        autoOpen: false,
        resizable: false,
        width: 160,
        height: 80,
        modal: true,
        title: "Carregando...",
        open: function(event, ui) {
            //hideVisibleComboboxes(document, this);
        },
        close: function(event, ui) {
            //showHiddenComboboxes(document, this);
        }
    });
    $("#popupValidacao").dialog({
        autoOpen: false,
        resizable: true,
        width: 470,
        height: 290,
        modal: true,
        title: "Validacao",
        buttons: {
            "Sair": function() {
                $(this).dialog("close");
                hideLoadingDialog();
                return false;
            }
        },
        open: function(event, ui) {
            hideVisibleComboboxes(document, this);
        },
        close: function(event, ui) {
            showHiddenComboboxes(document, this);
        }
    });
    // =============================================================================================
    // BOTAO CANCELAR
    // =============================================================================================
    $("#cmdCancelarAlteracao").click(function() {
        window.location = "login.aspx";
    }); 
    // =============================================================================================
    // BOTAO ALTERAR
    // =============================================================================================
    $("#cmdAlterarSenha").click(function()    
    {   var senha_atual = $("#senhaAtual").val();
        var nova_senha = $("#novaSenha").val();
        var confirmacao = $("#confirmacaoSenha").val();
        var v_OK = "S"; //1415
        // =============================================================================================
        // 1415 consistindo - inicio
        // =============================================================================================
        if ((senha_atual == null || senha_atual == "") && v_OK == "S")
        {   popup(
            {   titulo: "ALTERAR SENHA - Erro"
            ,   mensagem: "SENHA ATUAL: Em branco, favor informar."
            ,   icone: "erro"
            ,   callbackOK: function() {alterar_senha_limpar();}
            });               
            v_OK = "N";
        }
        if ((nova_senha == null || nova_senha == "") && v_OK == "S")      //1415
        {   popup(
            {   titulo: "ALTERAR SENHA - Erro"
            ,   mensagem: "NOVA SENHA: Em branco, favor informar."
            ,   icone: "erro"
            ,   callbackOK: function() {alterar_senha_limpar();}
            });               
            v_OK = "N";
        }        
        if ((confirmacao == null || confirmacao == "") && v_OK == "S")     //1415
        {   popup(
            {   titulo: "ALTERAR SENHA - Erro"
            ,   mensagem: "CONFIRMAÇÃO SENHA: Em branco, favor informar."
            ,   icone: "erro"
            ,   callbackOK: function() {alterar_senha_limpar();}
            });               
            v_OK = "N";
        }          
        if (senha_atual == nova_senha && v_OK == "S") 
        {
            popup(
            {   titulo: "ALTERAR SENHA - Erro"
            ,   mensagem: "NOVA SENHA: Esta igual a SENHA ATUAL"
            ,   icone: "erro"
            ,   callbackOK: function() {alterar_senha_limpar();
                }
            });
            v_OK = "N";
        }        
        if (nova_senha != confirmacao && v_OK == "S") 
        {   popup(
            {   titulo: "ALTERAR SENHA - Erro"
            ,   mensagem: "SENHA DE CONFIRMAÇÃO: Não confere com a NOVA SENHA"
            ,   icone: "erro"
            ,   callbackOK: function() {alterar_senha_limpar();}
            });               
            v_OK = "N";
        }        
        // =============================================================================================
        // 1415 consistindo - fim
        // =============================================================================================
        // 1415 TRATAR RETORNO DA ALTERACAO DE SENHA
        //{   executarServico("AlterarSenhaRequest",{ SenhaAtual: senha_atual,NovaSenha: nova_senha}, "alterar_senha_callback");
        // =============================================================================================
        var v_Descricao = "";        
        if (v_OK == "S") 
        {   executarServico("AlterarSenhaRequest", {SenhaAtual: senha_atual,NovaSenha: nova_senha}, function(data)
            {   if (data && data.Criticas.length > 0) 
                {   for (var i = 0; i < data.Criticas.length; i++) 
                    {   v_Descricao = v_Descricao + (i + 1) + "-" + data.Criticas[i].Descricao + "<br \>";
                    }
                    popup(
                    {   titulo: "ALTERAR SENHA - Erro"
                    ,   mensagem: v_Descricao
                    ,   icone: "erro"
                    ,   callbackOK: function() {alterar_senha_limpar();}
                    });                
                }    
                else
                {
                    popup(
                    {   titulo: "ALTERAR SENHA - SUCESSO"
                    ,   mensagem: "OK !!!! A sua senha foi alterada com sucesso."
                    ,   icone: "sucesso"
                    ,   callbackOK: function() {alterar_senha_limpar();window.location = "login.aspx"}
                    });                
                }
            }); 
        }
        // =============================================================================================
        // 1415 TRATAR RETORNO DA ALTERACAO DE SENHA - FIM
        // =============================================================================================
    });
    //Associando o ENTER ao botão Alterar Senha
    $(document).keypress(function(e) {
        if (e.which == 13) $('#cmdAlterarSenha').click();
    });
}
// =============================================================================================
// LIMPAR DADOS DA TELA
// =============================================================================================
function alterar_senha_limpar() 
{
    $("#senhaAtual").val("");
    $("#novaSenha").val("");
    $("#confirmacaoSenha").val("");
    $("#senhaAtual").focus();
}
// 1415  NAO TRATAVA RETORNO DA ALTERACAO DE SENHA
//function alterar_senha_callback(resposta) {
//    popup(
//    {   titulo: "Alterar Senha"
//    ,   mensagem: "A sua senha foi alterada com sucesso."
//    ,   icone: "sucesso"
//    ,   callbackOK: function() {alterar_senha_limpar();window.location = "login.aspx"}
//    });
//}
function showLoadingDialog() {
    // Abro o dialog
    $("#loadingDialog").dialog("open");
}

function hideLoadingDialog() {
    // Abro o dialog
    $("#loadingDialog").dialog("close");
}
function popup(_opcoes) {

    var opcoes = {};

    $.extend(opcoes, popupOpcoesPadrao(), _opcoes);

    $("#popupSimNao_mensagem").html(opcoes.mensagem);

    var opcoesDialog = {
        autoOpen: true,
        modal: true,
        closeOnEscape: false,
        resizable: opcoes.resizable,
        width: opcoes.width,
        height: opcoes.height,
        title: opcoes.titulo,
        buttons: {},
        open: function(event, ui) {
            //			hideVisibleComboboxes(document, this);
        },
        close: function(event, ui) {
            //			showHiddenComboboxes(document, this);
        },
        focus: function(event, ui) {
            //			hideLoadingDialog();
        }
    };

    if (opcoes.icone == "ajuda" || opcoes.icone == "atencao" || opcoes.icone == "erro"
				|| opcoes.icone == "info" || opcoes.icone == "sucesso")
        $("#popupSimNao img.icone")
			.attr("src", "img/icone_" + opcoes.icone + ".png")
			.show();
    else
        $("#popupSimNao img").hide();

    if (opcoes.callbackSim)
        opcoesDialog.buttons["Sim"] = function() { popupCallback(opcoes.callbackSim); };
    if (opcoes.callbackNao)
        opcoesDialog.buttons["Não"] = function() { popupCallback(opcoes.callbackNao); };
    if (opcoes.callbackOK)
        opcoesDialog.buttons["OK"] = function() { popupCallback(opcoes.callbackOK); };
    if (opcoes.callbackCancelar)
        opcoesDialog.buttons["Cancel"] = function() { popupCallback(opcoes.callbackCancelar); };

    $("#popupSimNao").dialog(opcoesDialog);

}
function popupOpcoesPadrao() {
    return {
        resizable: false,
        width: 400,
        height: 150,
        icone: "nenhum"
    };
}
function popupCallback(callback) {
    $("#popupSimNao").dialog("close");
    callback();
}
