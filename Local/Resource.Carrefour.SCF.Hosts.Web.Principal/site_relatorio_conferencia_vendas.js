﻿$(document).ready(function() {

    // Namespace
    if (!$.relatorio)
        $.relatorio = {};

    // Funcoes do namespace 
    $.relatorio = {

        variaveis: {},

        //=================================================================================
        // load: funcoes de inicializacao da tela
        //=================================================================================
        load: function(callback) {

            // Insere o subtítulo
            $("#subtitulo").html("Relatórios > Relatório de Conferência de Vendas");

            //------------------------------------------------------------------
            // Estilo dos botões
            //-------------------------------------------------------------------
            $("#site_relatorio_conferencia_vendas button").button();

            //-------------------------------------------------------------------
            // Inicializa forms
            //-------------------------------------------------------------------
            $("#site_relatorio_conferencia_vendas").Forms("initializeFields");

            // Lucas - Jira 976
            $("#chkConsolidacaoDataVenda").click(function() {
                if ($(this).attr("checked") == true)
                    $("#chkConsolidacaoDataProcessamento").attr("checked", false);
            });

            // Lucas - Jira 976
            $("#chkConsolidacaoDataProcessamento").click(function() {
                if ($(this).attr("checked") == true)
                    $("#chkConsolidacaoDataVenda").attr("checked", false);
            });

            // ECOMMERCE - Fernando Bove - 20160107
            // seta o valor default do campo tipo Contábil
            $("#rdoTipoContabil").attr("checked", true);

            //-------------------------------------------------------------------
            // BOTÃO VISUALIZAR HTML
            //-------------------------------------------------------------------
            $("#btnVisualizarHtml").click(function() {
                //$.relatorio.receberNumeroDeLinhas(
                //function() { 
                $.relatorio.gerarRelatorioHtml();
                //});
            });

            $("#btnGerarRelatorioExcel").click(function() {
                var request = {};

                request.Request = {};

                if ($("#cmbGrupo").find('option:selected').val() != "?") {
                    request.Request.FiltroEmpresaGrupo = $("#cmbGrupo").val();
                }
                if ($("#cmbSubgrupo").find('option:selected').val() != "?") {
                    request.Request.FiltroEmpresaSubgrupo = $("#cmbSubgrupo").val();
                }
                if ($("#cmbFavorecido").find('option:selected').val() != "?") {
                    request.Request.FiltroFavorecido = $("#cmbFavorecido").val();
                }
                if ($("#cmbEstabelecimento").val() != "?") {
                    request.Request.FiltroEstabelecimento = $("#cmbEstabelecimento").val();
                }
                //ClockWork combo Referencia
                if ($("#cmbReferencia").find('option:selected').val() != null) {
                    request.Request.FiltroReferencia = $("#cmbReferencia").val().toString();                
                }
                if ($("#txtPeriodoVendaDe").val() != "") {
                    request.Request.FiltroDataInicioVendas = $("#txtPeriodoVendaDe").val();
                }
                if ($("#txtPeriodoVendaAte").val() != "") {
                    request.Request.FiltroDataFimVendas = $("#txtPeriodoVendaAte").val();
                }
                if ($("#txtDataMovimentoDe").val() != "") {
                    request.Request.FiltroDataMovimentoInicio = $("#txtDataMovimentoDe").val();
                }
                if ($("#txtDataMovimentoAte").val() != "") {
                    request.Request.FiltroDataMovimentoFim = $("#txtDataMovimentoAte").val();
                }

                // data de retorno de CCI - JIRA SCF-836
                if ($("#txtDataRetornoCCIDe").val() != "") {
                    request.Request.FiltrotxtDataRetornoCCIInicio = $("#txtDataRetornoCCIDe").val();
                }
                if ($("#txtDataRetornoCCIFim").val() != "") {
                    request.Request.FiltrotxtDataRetornoCCIFim = $("#txtDataRetornoCCIFim").val();
                }

                //ECOMMERCE - Fernando Bove - 20160105: Inicio
                if ($("#rdoTipoContabil").attr("checked")) {
                    request.Request.FiltroFinanceiroContabil = $("#rdoTipoContabil").val();
                }
                if ($("#rdoTipoFinanceiro").attr("checked")) {
                    request.Request.FiltroFinanceiroContabil = $("#rdoTipoFinanceiro").val();
                }
                //ECOMMERCE - Fernando Bove - 20160105: Fim

                // checkbox
                request.Request.FiltroConsolidaGrupoSubGrupoProduto = $("#chkConsolidacaoGrupoSubGrupoProduto").attr("checked");
                request.Request.FiltroConsolidaDataVenda = $("#chkConsolidacaoDataVenda").attr("checked");
                request.Request.FiltroConsolidaDataProcessamento = $("#chkConsolidacaoDataProcessamento").attr("checked");
                request.Request.FiltroConsolidaBancoAgenciaConta = $("#chkConsolidacaoBancoAgenciaConta").attr("checked");
                request.Request.FiltroConsolidaEstabelecimento = $("#chkConsolidacaoEstabelecimento").attr("checked");
                //ClockWork ConsolidaRefrencia
                request.Request.FiltroConsolidaReferencia = $("#chkConsolidacaoReferencia").attr("checked");
                

                if ($("#cmbTipoRegistro").find('option:selected').val() != "?") {
                    request.Request.FiltroTipoRegistro = $("#cmbTipoRegistro").val();
                }



                executarServico(
                    "GerarRelatorioVendaExcelRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Relatório Excel",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
                            callbackOK: function() { }
                        });
                    });


            });


            $("#btnGerarRelatorioPdf").click(function() {

                var request = {};

                request.Request = {};
                request.Request.TipoRelatorio = "Excel";

                if ($("#chkConsolidacaoReferencia").attr("checked"))
                    var link = "relatorio_conferencia_vendas_referencia.aspx"; //ClockWork
                else
                    var link = "relatorio_conferencia_vendas.aspx";
                
                link += $.relatorio.gerarUrl();
                var url = window.location.href;

                var arr = url.split("/");
                var result = arr[0] + "//" + arr[2]

                //Jira SCF 1046 - Marcos Matsuoka
                if (arr[3] == "scf") {
                    arr[3] = "scf_relatorios";
                    result = result + "/" + arr[3];
                } else if (arr[3] == "scf_projetos") {
                    arr[3] = "scf_relatorios2";
                    result = result + "/" + arr[3];
                }

                var link = link + "&TipoArquivo=PDF"; //Marcos Matsuoka - Jira SCF 1085

                link = result + "/" + link;

                //window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');
                request.Request.Link = link;
                request.Request.TipoArquivo = "PDF";
                request.Link = link;
                request.TipoArquivo = "PDF";

                executarServico(
                    "GerarRelatorioVendasPDFRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Relatório PDF",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório PDF.",
                            callbackOK: function() { }
                        });
                    });
                //window.open("site_relatorio_vencimento.aspx" + parametros + "&acao=pdf", "_self");
            });

            //-------------------------------------------------------------------
            // Preenche combo de subgrupos
            //-------------------------------------------------------------------
            $.relatorio.carregarSubGrupos(function() {

                //-------------------------------------------------------------------
                // Preenche combo de estabelecimentos
                //-------------------------------------------------------------------
                //                $.relatorio.carregarEstabelecimentos(function() {

                //                    // Chama callback
                //                    if (callback)
                //                        callback();
                //                });
            });

            $.relatorio.carregarReferencias(function() {
            });


            //-------------------------------------------------------------------
            // Evento change do combo de grupo 
            //-------------------------------------------------------------------
            $("#cmbGrupo").change(function() {

                // Carrega combo de subgrupos correspondente ao grupo
                $.relatorio.carregarSubGrupos(function() {

                    // Chama callback
                    if (callback)
                        callback();
                });
            });

            //-------------------------------------------------------------------
            // Evento change do combo de estabelecimentos
            //-------------------------------------------------------------------
            $("#cmbSubgrupo").change(function() {

                //                // Carrega combo de estabelecimentos correspondente ao subgrupo
                //                $.relatorio.carregarEstabelecimentos(function() {

                //                    // Chama callback
                //                    if (callback)
                //                        callback();
                //                });

            });
        },

        //=================================================================================
        // carregarSubGrupos: carrega combo de subgrupos de acordo com o grupo 
        //                selecionado, caso tenha
        //=================================================================================
        carregarSubGrupos: function(callback) {

            // Inicializa filtro
            var codigoEmpresaGrupo = null;

            // Pega código do grupo, se tiver
            if ($("#cmbGrupo").val() != "?")
                codigoEmpresaGrupo = $("#cmbGrupo").val();

            // Pede lista de subgrupos
            executarServico(
                "ListarEmpresaSubGrupoRequest",
                { FiltroCodigoEmpresaGrupo: codigoEmpresaGrupo },
                function(data) {


                    // Guarda resultado
                    $.relatorio.variaveis.subgrupos = data.Resultado;

                    // Limpa o combo (exceto o primeiro item)
                    $("#cmbSubgrupo").children().each(
		                function(index) {
		                    if (index != 0)
		                        $(this).remove();
		                }
	                );

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {

                        // Adiciona o item no combo
                        $("#cmbSubgrupo").append("<option value='" + data.Resultado[i].CodigoEmpresaSubGrupo + "'>"
				                + data.Resultado[i].NomeEmpresaSubGrupo + "</option>");
                    }

                    // chama callback
                    if (callback)
                        callback();
                });
        },

        //=================================================================================
        // carregarReferencias: carrega combo de referencia 
        //=================================================================================
        carregarReferencias: function(callback) {

            // Limpa o combo (exceto o primeiro item)
            $("#cmbReferencia").children().each(
                function(index) {
                    if (index != 0)
                        $(this).remove();
                }
            );

            var el = $("#cmbReferencia").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: ""
            });

            executarServico(
                "ListarListaItemRequest",
                { NomeLista: 'Referencia' },
                function(data) {

                    // Varre lista de referencia
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(el);
                    }

                    el.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
           );
        },


        //=================================================================================
        // gerarRelatorioPDF: gera o relatório em PDF de transações conforme filtros
        //=================================================================================
        gerarRelatorioHtml: function(callback) {

            if ($("#chkConsolidacaoReferencia").attr("checked"))
                var link = "relatorio_conferencia_vendas_referencia.aspx"; //ClockWork
            else
                var link = "relatorio_conferencia_vendas.aspx";
            link += $.relatorio.gerarUrl();

            //Jira SCF 749 - Marcos Matsuoka
            var url = window.location.href;

            var arr = url.split("/");
            var result = arr[0] + "//" + arr[2]

            if (arr[3] == "scf") {
                arr[3] = "scf_html";
                result = result + "/" + arr[3];
            } else if (arr[3] == "scf_projetos") {
                arr[3] = "scf_html2";
                result = result + "/" + arr[3];
            }

            var link = link + "&TipoArquivo=HTML"; //Marcos Matsuoka - Jira SCF 1085

            link = result + "/" + link;
            //

            window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');

            // Chama callback
            if (callback)
                callback();
        },

        gerarUrl: function(callback) {

            var link = "";

            if ($("#cmbGrupo").find('option:selected').val() != "?") {
                var link = link + "CodigoEmpresaGrupo=" + $("#cmbGrupo").val() + "&" + "Grupo=" + $("#cmbGrupo option:selected").text() + "&"; ;
            }
            if ($("#cmbSubgrupo").find('option:selected').val() != "?") {
                var link = link + "CodigoEmpresaSubgrupo=" + $("#cmbSubgrupo").val() + "&" + "Subgrupo=" + $("#cmbSubgrupo option:selected").text() + "&"; ;
            }
            if ($("#cmbFavorecido").find('option:selected').val() != "?") {
                var link = link + "CodigoFavorecido=" + $("#cmbFavorecido").val() + "&" + "Favorecido=" + $("#cmbFavorecido option:selected").text() + "&"; ;
            }
            if ($("#cmbEstabelecimento").val() != "?") {
                var link = link + "CodigoEstabelecimento=" + $("#cmbEstabelecimento").val() + "&" + "Estabelecimento=" + $("#cmbEstabelecimento option:selected").text() + "&"; ;
            }
            //ClockWork - Combo de Referencia
            if ($("#cmbReferencia").find('option:selected').val() != null) {
                var link = link + "CodigoReferencia=" + $("#cmbReferencia").val().toString() + "&" + "Referencia=" + $("#cmbReferencia option:selected").text() + "&"; ;
            }

            if ($("#txtPeriodoVendaDe").val() != "") {
                var link = link + "DataInicioVenda=" + $("#txtPeriodoVendaDe").val() + "&";
            }
            if ($("#txtPeriodoVendaAte").val() != "") {
                var link = link + "DataFimVenda=" + $("#txtPeriodoVendaAte").val() + "&";
            }
            if ($("#txtDataMovimentoDe").val() != "") {
                var link = link + "DataInicioMovimento=" + $("#txtDataMovimentoDe").val() + "&";
            }
            if ($("#txtDataMovimentoAte").val() != "") {
                var link = link + "DataFimMovimento=" + $("#txtDataMovimentoAte").val() + "&";
            }

            // data de retorno de CCI - JIRA SCF-836
            if ($("#txtDataRetornoCCIDe").val() != "") {
                var link = link + "DataInicioRetornoCCI=" + $("#txtDataRetornoCCIDe").val() + "&";
            }
            if ($("#txtDataRetornoCCIFim").val() != "") {
                var link = link + "DataFimRetornoCCI=" + $("#txtDataRetornoCCIFim").val() + "&";
            }

            //ECOMMERCE - Fernando Bove - 20160105: Inicio
            if ($("#rdoTipoContabil").attr("checked")) {
                var link = link + "TipoFinanceiroContabil=C&";
            }
            if ($("#rdoTipoFinanceiro").attr("checked")) {
                var link = link + "TipoFinanceiroContabil=F&";
            }
            //ECOMMERCE - Fernando Bove - 20160105: Fim

            // checkbox
            var link = link + "ConsolidaGrupoSubGrupoProduto=" + $("#chkConsolidacaoGrupoSubGrupoProduto").attr("checked") + "&";
            var link = link + "ConsolidaDataVenda=" + $("#chkConsolidacaoDataVenda").attr("checked") + "&";
            var link = link + "ConsolidaDataProcessamento=" + $("#chkConsolidacaoDataProcessamento").attr("checked") + "&";
            var link = link + "ConsolidaBancoAgenciaConta=" + $("#chkConsolidacaoBancoAgenciaConta").attr("checked") + "&";
            var link = link + "ConsolidaEstabelecimento=" + $("#chkConsolidacaoEstabelecimento").attr("checked") + "&";
            //ClockWork - Consolida por Referencia
            var link = link + "ConsolidaReferencia=" + $("#chkConsolidacaoReferencia").attr("checked") + "&";

            if ($("#cmbTipoRegistro").find('option:selected').val() != "?") {
                var link = link + "CodigoTipoRegistro=" + $("#cmbTipoRegistro").val() + "&" + "TipoRegistro=" + $("#cmbTipoRegistro option:selected").text() + "&";
            }

            if (link != "") {
                link = "?" + link.substr(0, link.length - 1);
            }

            // Chama callback
            if (callback)
                callback();

            return link;
        },

        //=================================================================================
        // receberNumero: carrega combo de estabelecimentos de acordo com
        //                          o grupo e/ou subgrupo selecionados, caso tenha
        //=================================================================================
        receberNumeroDeLinhas: function(callback) {

            var request = {};

            // Envia informações ao objeto existente
            $("#filtros_conferencia_vendas").Forms("getData", { dataObject: request });

            request.VerificarQuantidadeLinhas = true;

            // Verifica quantidade de linhas
            executarServico(
                "ListarRelatorioConferenciaVendasRequest",
                request,
                function(data) {

                    if (data.QuantidadeLinhas > 1000) {

                        // Pergunta se o usuario deseja continuar..
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Não foi possível visualizar o relatório devido a quantidade excessiva de linhas (" + data.QuantidadeLinhas + "). Por favor selecione a opção para exportação de relatório para Excel.",
                            callbackOK: function() {
                                return true;
                            }
                        });
                    } else {


                        if (data.QuantidadeLinhas == 0) {
                            // Pergunta se o usuario deseja continuar..
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "O relatório a ser gerado não possui linhas. Deseja continuar a operação?",
                                callbackSim: function() {
                                    if (callback)
                                        callback();
                                },
                                callbackNao: function() {
                                    return true;
                                }
                            });
                        } else {

                            if (callback)
                                callback();
                        }
                    }
                });
        }
    }

    // Chama load da página
    $.relatorio.load();
});