﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true"
    CodeBehind="site_relatorio_consolidado.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_relatorio_consolidado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="site_relatorio_consolidado.js"></script>

    <link href="site_relatorio_consolidado.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="site_relatorio_consolidado" style="width: 100%">
        <div id="filtros_relatorio_consolidado">
            <table width="100%">
                <tr>
                    <td width='50%' valign='top'>
                        <fieldset style="border: 1px solid silver" id="filtroDetalhe">
                            <legend style="position: relative; left: 10px" class="tituloFiltro">Tipo de Relat&oacute;rio</legend>
                            <table align="center" width="100%">
                                <tr>
                                    <td>
                                        <input type="radio" id="rdoTipoVenda" name="tipoconsolid" class="forms[property[FiltroTipoConsolidado]]"
                                            value="V" /><b><label>Relat&oacute;rio Venda:</label></b>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoVendaTotalizado" name="layoutVenda"
                                                class="forms[property[FiltroLayoutVenda]]" value="T" />&nbsp;Totalizado por
                                            Produto
                                        </p>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoVendaAnalitico" name="layoutVenda"
                                                class="forms[property[FiltroLayoutVenda]]" value="A" />&nbsp;Anal&iacute;tico
                                            por Data&nbsp;&nbsp;&nbsp;
                                        </p>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoVendaSintetico" name="layoutVenda"
                                                class="forms[property[FiltroLayoutVenda]]" value="S" />&nbsp;Sint&eacute;tico
                                            por Data
                                        </p>
                                        <p>
                                            <br />
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chkVendaQtde" name="chkVendaQtde"
                                                class="forms[property[FiltroQtdeVenda]]" />&nbsp;Visualizar Qtde Transação
                                        </p>
                                    </td>
                                    <td>
                                        <input type="radio" id="rdoTipoRecebimento" name="tipoconsolid" class="forms[property[FiltroTipoConsolidado]]"
                                            value="R" /><b><label>Relat&oacute;rio Recebimento:</label></b>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoRecebimentoTotalizado" name="layoutRecebimento"
                                                class="forms[property[FiltroLayoutRecebimento]]" value="T" />&nbsp;Totalizado
                                            por Meio Pagamento
                                        </p>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoRecebimentoAnalitico" name="layoutRecebimento"
                                                class="forms[property[FiltroLayoutRecebimento]]" value="A" />&nbsp;Anal&iacute;tico
                                            por Data&nbsp;&nbsp;&nbsp;
                                        </p>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoRecebimentoSintetico" name="layoutRecebimento"
                                                class="forms[property[FiltroLayoutRecebimento]]" value="S" />&nbsp;Sint&eacute;tico
                                            por Data
                                        </p>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoRecebimentoCCI" name="layoutRecebimento"
                                                class="forms[property[FiltroLayoutRecebimento]]" value="C" />&nbsp;Totalizador
                                            envio CCI
                                        </p>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chkRecebimentoQtde" name="chkRecebimentoQtde"
                                                class="forms[property[FiltroQtdeRecebimento]]" />&nbsp;Visualizar Qtde Meio
                                            Pagamento
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br />
                                        <p>
                                            <b>Totais</b></p>
                                        <input type="checkbox" id="chkSubTotal" name="chkSubTotal" class="forms[property[FiltroQuebraFavorecido]]" />&nbsp;SubTotal/Quebra
                                        por Favorecido&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" id="chkSubTotalReferencia" name="chkSubTotalReferencia" class="forms[property[FiltroQuebraReferenciaFavorecido]]" />&nbsp;SubTotal/Quebra
                                        por Referência&nbsp;&nbsp;<br />
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td width='50%' valign='top'>
                        <fieldset style="border: 1px solid silver" id="filtroCombos">
                            <legend style="position: relative; left: 10px" class="tituloFiltro">Filtros</legend>
                            <table width="100%">
                                <tr style="margin-bottom: 3px;">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="margin-left: 3px">
                                        <input type="radio" id="rdoTipoContabil" name="tiporelatorio" class="forms[property[FiltroFinanceiroContabil]]"
                                            value="C" />&nbsp;Contábil &nbsp;&nbsp;&nbsp;
                                        <input type="radio" id="rdoTipoFinanceiro" name="tiporelatorio" class="forms[property[FiltroFinanceiroContabil]]"
                                            value="F" />&nbsp;Financeiro
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_referencia">
                                        <label>
                                            Referência:</label>
                                    </td>
                                    <td id="referenciaContainer">
                                        <select id="cmbReferencia" multiple="multiple" style="width: 300px;" class="ui-multiselect ui-state-focus ui-state-active forms[property[FiltroReferencia]]">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_detalhe2">
                                        <label>
                                            Tipo de Registro:
                                        </label>
                                    </td>
                                    <td id="campo_tipo_registro">
                                        <select id="cmbTipoRegistro" multiple="multiple" style="width: 300px;" class="ui-multiselect ui-state-focus ui-state-active forms[property[FiltroTipoRegistro]]">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_produto">
                                        <label>
                                            Produto:</label>
                                    </td>
                                    <td id="produtoContainer" class="campo_produto">
                                        <select id="cmbProduto" multiple="multiple" style="width: 300px;" class="ui-multiselect ui-state-focus ui-state-active forms[property[FiltroProduto]]">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_detalhe2">
                                        <label>
                                            Meio de Pagamento:
                                        </label>
                                    </td>
                                    <td>
                                        <select id="cmbMeioPagamento" multiple="multiple" style="width: 300px;" class="ui-multiselect ui-state-focus ui-state-active forms[property[FiltroMeioPagamento]]">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            Favorecido Original:
                                        </label>
                                    </td>
                                    <td>
                                        <select id="cmbFavorecidoOriginal" multiple="multiple" style="width: 300px;" class="ui-multiselect ui-state-focus ui-state-active forms[property[FiltroFavorecidoOriginal]]">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 24px;">
                                        <label>
                                            Estabelecimento:&nbsp;</label>
                                    </td>
                                    <td class="campo_estabelecimento">
                                        <select id="cmbEstabelecimento" style="height: 23px; width: 295px;">
                                            <option value="?">Todos</option>
                                            <asp:Repeater runat="server" ID="rptCmbEstabelecimento">
                                                <ItemTemplate>
                                                    <option class="forms[property[FiltroEstabelecimento]]" value="<%# Eval("CodigoEstabelecimento") %>">
                                                        <%# Eval("RazaoSocial")%></option>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 24px;">
                                        <label>
                                            Status Transação:</label>
                                    </td>
                                    <td class="cmb_StatusCCI">
                                        <select id="cmbStatusCCI"  multiple="multiple" style="width:300px;" class="ui-multiselect ui-state-focus ui-state-active"></select>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" width='100%' valign='top'>
                        <fieldset style="border: 1px solid silver" id="filtroPeriodo">
                            <legend style="position: relative; left: 10px" class="tituloFiltro">Per&iacute;odos</legend>
                            <div class="borda">
                                <ul style="margin-bottom: 2px; position: relative; left: 2px">
                                    <li style="position: relative; left: 2px; width: 135px">
                                        <label>
                                            Data de Agendamento:</label>
                                    </li>
                                    <li>
                                        <ul>
                                            <li>
                                                <label>
                                                    De:</label>
                                            </li>
                                            <li>
                                                <input type="text" id="txtDataAgendamentoDe" class="forms[property[FiltroDataAgendamentoDe];dataType[date];maskType[date]]" />
                                            </li>
                                            <li>
                                                <label>
                                                    Até:</label>
                                            </li>
                                            <li>
                                                <input type="text" id="txtDataAgendamentoAte" class="forms[property[FiltroDataAgendamentoAte];dataType[date];maskType[date]]" />
                                            </li>
                                        </ul>
                                    </li>
                                    <li style="position: relative; left: 2px; width: 135px">
                                        <label>
                                            Data de Processamento:</label>
                                    </li>
                                    <li>
                                        <ul>
                                            <li>
                                                <label>
                                                    De:</label>
                                            </li>
                                            <li>
                                                <input type="text" id="txtDataProcessamentoDe" class="forms[property[FiltroDataMovimentoDe];dataType[date];maskType[date]]" />
                                            </li>
                                            <li>
                                                <label>
                                                    Até:</label>
                                            </li>
                                            <li style="margin-bottom: 3px;">
                                                <input type="text" id="txtDataProcessamentoAte" class="forms[property[FiltroDataMovimentoAte];dataType[date];maskType[date]]" />
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </fieldset>
                    </td>
                    <td width='50%' valign='top'>
                    </td>
                </tr>
            </table>
            <div class="teste">
                <div style="float: left; text-align: left">
                    <button style="font-family: Arial, Helvetica, sans-serif" id="btnVisualizarHtml">
                        Visualizar</button>
                </div>
                <div style="text-align: right">
                    <button style="font-family: Arial, Helvetica, sans-serif" id="btnGerarRelatorioExcel">
                        Gerar Excel</button>
                    <button style="font-family: Arial, Helvetica, sans-serif" id="btnGerarPdf">
                        Gerar PDF</button>
                </div>
            </div>
        </div>
        <div id="resultado_relatorio">
        </div>
    </div>
</asp:Content>
