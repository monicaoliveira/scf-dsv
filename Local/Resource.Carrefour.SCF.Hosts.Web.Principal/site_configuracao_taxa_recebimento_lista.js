﻿$(document).ready(function() {

    // Cria namespace 
    if (!$.taxa)
        $.taxa = {};

    // Funcoes do namespace 
    $.taxa.lista = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Taxa de Recebimento");

            // Estilo dos botões
            $("#site_configuracao_taxa_recebimento_lista button").button();

            // Inicializa forms
            $("#site_configuracao_taxa_recebimento_lista").Forms("initializeFields");

            // Inicializa grid 
            $("#tbTaxaRecebimento").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
				shrinkToFit: false,
                colNames: ["ID", "Grupo", "Referência", "Meio de Pagamento", "Válido de", "Válido até", "Valor (R$)", "Percental (%)"],
                colModel: [
                    { name: "CodigoTaxaRecebimento", index: "CodigoTaxaRecebimento", key: true, align: 'center', sorttype: "number", width: 50 },
                    { name: "NomeGrupo", index: "NomeGrupo", sorttype: "text", width: 120 },
                    { name: "DescricaoReferencia", index: "DescricaoReferencia", sorttype: "text", width: 120 },
                    { name: "DescricaoMeioPagamento", index: "DescricaoMeioPagamento", sorttype: "text", width: 200 },
                    { name: "DataInicio", index: "DataInicio", align: 'center', width: 100 },
                    { name: "DataFim", index: "DataFim", align: 'center', width: 100 },
                    { name: "ValorRecebimento", index: "ValorRecebimento", align: 'right', sorttype: "number", width: 100,
                        formatter: 'number',
                        formatoptions: { prefix: 'R$ ', suffix: '', decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 2, defaultValue: '' }
                    },
                    { name: "PercentualRecebimento", index: "PercentualRecebimento", align: 'right', sorttype: "number", width: 100,
                        formatter: 'number',
                        formatoptions: { prefix: '', suffix: '%', decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 2, defaultValue: '' }
                    }
                ],
                ondblClickRow: function(rowid, iRow, iCol, e) {

                    // Pega a chave de leitura
                    var codigoTaxaRecebimento = $("#tbTaxaRecebimento").getGridParam("selrow");

                    if (codigoTaxaRecebimento) {
                        $.taxa.detalhe.abrir(codigoTaxaRecebimento);
                    }

                    else {
                        popup({
                            titulo: "Editar Taxa de Recebimento",
                            icone: "atencao",
                            mensagem: "Favor selecionar uma linha!",
                            callbackOK: function() { }
                        });
                    }
                }
            });

            //-------------------------------------------------------------------
            // Preenche combo de Grupo
            //-------------------------------------------------------------------
            $.taxa.lista.carregarGrupo(function(callback) {
                //-------------------------------------------------------------------
                // Preenche combo de meio de pagamento
                //-------------------------------------------------------------------
                $.taxa.lista.carregarMeioPagamento(function(callback) {
                    //-------------------------------------------------------------------
                    // Preenche combo de referencia
                    //-------------------------------------------------------------------
                    $.taxa.lista.carregarReferencia(function(callback) {
                        // Chama callback
                        if (callback)
                            callback();
                    });
                });
            });

            // BOTÃO FILTRAR
            $("#cmdFiltrar").click(function() {
                $.taxa.lista.listarTaxa(function() { });
            });

            // BOTÃO NOVO 
            $("#cmdNovo").click(function() {
                $.taxa.detalhe.abrir(null);
            });

            // BOTÃO EDITAR PRODUTO
            $("#cmdEditar").click(function() {
                var codigoTaxaRecebimento = $("#tbTaxaRecebimento").getGridParam("selrow");

                if (codigoTaxaRecebimento) {
                    $.taxa.detalhe.abrir(codigoTaxaRecebimento);
                }
                else {
                    popup({
                        titulo: "Editar Taxa de Recebimento",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // BOTÃO REMOVER 
            $("#cmdRemover").click(function() {
                var codigoTaxaRecebimento = $("#tbTaxaRecebimento").getGridParam("selrow");

                if (codigoTaxaRecebimento) {
                    executarServico(
                        "ReceberTaxaRecebimentoRequest",
                        {
                            CodigoTaxaRecebimento: codigoTaxaRecebimento
                        },
                    function(data) {
                        if (data.TaxaRecebimentoInfo) {

                            popup({
                                titulo: "Remover Taxa de Recebimento",
                                icone: "atencao",
                                mensagem: "Tem certeza que deseja remover esta Taxa de Recebimento?",
                                callbackSim: function() {

                                    $.taxa.detalhe.remover(
                                        codigoTaxaRecebimento,
                                        function() {
                                            popup({
                                                titulo: "Remover Taxa de Recebimento",
                                                icone: "sucesso",
                                                mensagem: "Taxa de Recebimento removido com sucesso!",
                                                callbackOK: function() {
                                                    $.taxa.lista.listarTaxa(function() { });
                                                }
                                            });
                                        });
                                },
                                callbackNao: function() {

                                }
                            });
                        }
                        else {
                            popup({
                                titulo: "Remover Taxa de Recebimento",
                                icone: "atencao",
                                mensagem: "Taxa de Recebimento já foi excluido!",
                                callbackOK: function() {
                                    $.taxa.lista.listarTaxa(function() { });
                                }
                            });
                        }

                    });
                }
                else {
                    popup({
                        titulo: "Remover Taxa de Recebimento",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() {

                        }
                    });
                }
            });

            $.taxa.lista.listarTaxa(function() { });

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbTaxaRecebimento").setGridWidth($(window).width() - margemEsquerda);
                $("#tbTaxaRecebimento").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");
        },

        //------------------------------------------------------------------------
        // listarTaxa: Pede listas de Taxas de Recebimento
        //------------------------------------------------------------------------
        listarTaxa: function(callback) {

            var request = {};

            request.FiltroMaxLinhas = 100;

            if ($("#cmbGrupo").val() != "?")
                request.FiltroCodigoGrupo = $("#cmbGrupo").val();

            if ($("#cmbReferencia").val() != "?")
                request.FiltroCodigoReferencia = $("#cmbReferencia").val();

            if ($("#cmbMeioPagamento").val() != "?")
                request.FiltroCodigoMeioPagamento = $("#cmbMeioPagamento").val();

            executarServico(
                "ListarTaxaRecebimentoRequest",
                request,
                function(data) {
                    // Limpa grid
                    $("#tbTaxaRecebimento").clearGridData();

                    for (var i = 0; i < data.Resultado.length; i++) {
                        if (data.Resultado[i].ValorRecebimento == "0")
                            data.Resultado[i].ValorRecebimento = "";
                        else
                            data.Resultado[i].ValorRecebimento = parseFloat(data.Resultado[i].ValorRecebimento.replace(",", ".")).toFixed(2);

                        if (data.Resultado[i].PercentualRecebimento == "0")
                            data.Resultado[i].PercentualRecebimento = "";
                        else
                            data.Resultado[i].PercentualRecebimento = parseFloat(data.Resultado[i].PercentualRecebimento.replace(",", ".")).toFixed(2);

                        $("#tbTaxaRecebimento").jqGrid("addRowData", data.Resultado[i].CodigoTaxaRecebimento, data.Resultado[i]);
                    }

                    // Chama callback
                    if (callback)
                        callback();
                });
        },
        //=================================================================================
        // carregarGrupo: carrega combo de Grupo
        //=================================================================================
        carregarGrupo: function(callback) {

            executarServico(
                "ListarEmpresaGrupoRequest",
                {},
                function(data) {
                    $("#cmbGrupo").children().each(
                        function(index) {
                            $(this).remove();
                        }
                    );

                    elG = $("#cmbGrupo");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' Todos'
                    });
                    opt.appendTo(elG);

                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].CodigoEmpresaGrupo,
                            text: ' ' + data.Resultado[i].NomeEmpresaGrupo
                        });
                        opt.appendTo(elG);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },
        //=================================================================================
        // carregarMeioPagamento: carrega combo de meio de pagamento
        //=================================================================================
        carregarMeioPagamento: function(callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    CodigoLista: 9
                },
                function(data) {
                    $("#cmbMeioPagamento").children().each(
                        function(index) {
                            $(this).remove();
                        }
                    );

                    elMP = $("#cmbMeioPagamento");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' Todos'
                    });
                    opt.appendTo(elMP);

                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elMP);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //=================================================================================
        // carregarReferencia: carrega combo de Referencia
        //=================================================================================
        carregarReferencia: function(callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    NomeLista: "Referencia"
                },
                function(data) {
                    $("#cmbReferencia").children().each(
                        function(index) {
                            $(this).remove();
                        }
                    );

                    elRef = $("#cmbReferencia");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' Todas'
                    });
                    opt.appendTo(elRef);

                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elRef);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        }
    };

    $.taxa.lista.load();
});