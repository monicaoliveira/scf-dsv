﻿$(document).ready(function() {

    // Cria namespace 
    if (!$.produto)
        $.produto = {};

    // Funcoes do namespace 
    $.produto.detalhe = {

        variaveis: {},

        // ----------------------------------------------------------------------
        // load: funcoes de inicializacao da tela
        // ----------------------------------------------------------------------
        load: function(callback) {

            $("#produto_tabDetalhe").Forms("initializeFields");

            // Verifica se o html esta carregado
            if ($("#site_configuracao_produto_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_configuracao_produto_detalhe.aspx #site_configuracao_produto_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_configuracao_produto_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.produto.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();
                    });
                });

            } else {

                // Chama o callback
                if (callback)
                    callback();
            }
        },

        load2: function(callback) {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Produto");

            // Estilo dos botões
            $("#site_configuracao_produto_detalhe button").button();

            // Inicializa tabs
            $("#site_configuracao_produto_tabs").tabs();

            // Inicializa Forms
            $("#site_configuracao_produto_detalhe").Forms("initializeFields");

            // Inicializa o dialog do detalhe
            $("#site_configuracao_produto_detalhe").dialog({
                autoOpen: false,
                height: 460,
                width: 680,
                modal: true,
                title: "Detalhe do Produto",
                buttons: {
                    "Salvar": function() {

                        // Pede para salvar
                        $.produto.detalhe.salvar(function() {

                            // Mostra popup de sucesso
                            popup({
                                titulo: "Salvar Produto",
                                icone: "sucesso",
                                mensagem: "Produto salvo com sucesso!",
                                callbackOK: function() {
                                    $.produto.lista.listarProduto();
                                    // Fecha dialog de detalhe
                                    $("#site_configuracao_produto_detalhe").dialog("close");

                                }
                            });
                        });
                    },
                    "Fechar": function() {

                        // Fecha o dialog
                        $("#site_configuracao_produto_detalhe").dialog("close");
                    }
                }
            });

            $("#tb_PlanosDisponiveis").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 105,
                width: 580,
                colNames: ["CodigoPlano", "PlanoTSYS", "Nome", "Descrição"],
                colModel: [
                    { name: "CodigoPlano", index: "CodigoPlano", key: true, hidden: true },
                    { name: "CodigoPlanoTSYS", index: "PlanoTSYS", width: 40 },
                    { name: "NomePlano", index: "NomePlano", width: 100 },
                    { name: "DescricaoPlano", index: "DescricaoPlano" }
                ],
                sortable: true
            });

            $("#tb_PlanosSelecionados").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 105,
                width: 580,
                colNames: ["CodigoPlano", "PlanoTSYS", "Nome", "Descrição"],
                colModel: [
                    { name: "CodigoPlano", index: "CodigoPlano", key: true, hidden: true },
                    { name: "CodigoPlanoTSYS", index: "PlanoTSYS", width: 40 },
                    { name: "NomePlano", index: "NomePlano", width: 100 },
                    { name: "DescricaoPlano", index: "DescricaoPlano" }
                ],
                sortable: true
            });

            $("#btnSelecionarTodosPlanos").click(function() {
                $.produto.detalhe.selecionarTodosPlanos();
            });

            $("#btnSelecionarPlano").click(function() {
                // Pega o código do produto
                var codigoPlano = $("#tb_PlanosDisponiveis").getGridParam("selrow");

                if (codigoPlano) {
                    $.produto.detalhe.selecionarPlano(codigoPlano);

                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            $("#btnRemoverTodosPlanos").click(function() {
                $.produto.detalhe.removerTodosPlanos();
            });

            $("#btnRemoverPlano").click(function() {
                var codigoPlano = $("#tb_PlanosSelecionados").getGridParam("selrow");

                if (codigoPlano) {
                    $.produto.detalhe.removerPlano(codigoPlano);

                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // Se tem callback, executo-o
            if (callback)
                callback();
        },

        // ----------------------------------------------------------------------
        // abrir: funcoes executadas na abertura da tela
        // ----------------------------------------------------------------------
        abrir: function(codigoProduto, callback, callbackSalvar) {

            // Seleciona a primeira aba
            $("#site_configuracao_produto_tabs").tabs("select", 0);

            // Guarda o callbackSalvar
            $("#site_configuracao_produto_detalhe").data("callbackSalvar", callbackSalvar);

            // Garante que o html está carregado
            $.produto.detalhe.load(function() {

                $.produto.detalhe.listarPlanos(function() {

                    if (codigoProduto) {

                        // Guarda código do produto
                        $.produto.detalhe.variaveis.codigoProduto = codigoProduto;

                        // Limpa campos
                        $("#site_configuracao_produto_detalhe").Forms("clearData");

                        // Carrega dados do produto existente
                        $.produto.detalhe.carregar(codigoProduto, function() {

                            // Abre dialog
                            $("#site_configuracao_produto_detalhe").dialog("open");
                        });

                    } else {

                        // Limpa variaveis internas
                        $.produto.detalhe.variaveis.codigoProduto = null;

                        // Limpa campos
                        $("#site_configuracao_produto_detalhe").Forms("clearData");

                        $("#tb_PlanosDisponiveis").clearGridData();
                        $("#tb_PlanosSelecionados").clearGridData();

                        // Abre dialog de detalhe
                        $("#site_configuracao_produto_detalhe").dialog("open");
                    };

                    // Chama o callback
                    if (callback) {
                        callback();
                    } else if (callbackSalvar) {
                        callbackSalvar();
                    }

                });

            });
        },

        // ----------------------------------------------------------------------
        // carregar: Carrega dados referentes ao produto
        // ----------------------------------------------------------------------
        carregar: function(codigoProduto, callback) {

            executarServico(
                "ReceberProdutoRequest",
                { CodigoProduto: codigoProduto },
                function(data) {

                    // Guarda dados do produto recebido
                    $.produto.detalhe.variaveis.produtoInfo = data.ProdutoInfo;

                    // Gambiarra para não vir vazio os inputs quando for zero
                    data.ProdutoInfo.PrazoPagamento = data.ProdutoInfo.PrazoPagamento + "";
                    data.ProdutoInfo.DiasVencidos = data.ProdutoInfo.DiasVencidos + "";
                    data.ProdutoInfo.DiasVencidosAJ = data.ProdutoInfo.DiasVencidosAJ + "";                                        

                    // Preenche os campos do detalhe
                    $("#site_configuracao_produto_detalhe").Forms("setData", { dataObject: data.ProdutoInfo });

                    if (data.ProdutoInfo.CodigoPlanos != null) {
                        for (var i = 0; i < data.ProdutoInfo.CodigoPlanos.length; i++) {
                            $.produto.detalhe.selecionarPlano(data.ProdutoInfo.CodigoPlanos[i]);
                        }
                    }

                    // Preenche combo cedivel
                    if (data.ProdutoInfo.Cedivel)
                        $("#cmbCedivel").val("true");
                    else
                        $("#cmbCedivel").val("false");


                    // Chama callback
                    if (callback)
                        callback();
                });
        },

        // ----------------------------------------------------------------------
        // salvar: Trata inserção ou alteração de um produto
        // ----------------------------------------------------------------------
        salvar: function(callback) {

            $.produto.detalhe.validar(function() {
                if ($.produto.detalhe.variaveis.codigoProduto) {

                    // Recebe produtoInfo
                    var produtoInfo = $.produto.detalhe.variaveis.produtoInfo

                    // Envia informações ao objeto existente
                    $("#site_configuracao_produto_detalhe").Forms("getData", { dataObject: produtoInfo });

                    // Limpa temp
                    $.produto.detalhe.variaveis.codigoProduto = null;

                } else {

                    // Cria novo objeto
                    produtoInfo = {};

                    // Envia informações ao novo objeto
                    $("#site_configuracao_produto_detalhe").Forms("getData", { dataObject: produtoInfo });
                }

                produtoInfo.CodigoPlanos = new Array();

                var idsPlanosSelecionados = $("#tb_PlanosSelecionados").getDataIDs();

                for (var i = 0; i < idsPlanosSelecionados.length; i++) {
                    produtoInfo.CodigoPlanos.push(idsPlanosSelecionados[i]);
                }

                executarServico(
                        "SalvarProdutoRequest",
                        { ProdutoInfo: produtoInfo },
                        function(data) {

                            // Chama os callbacks
                            if (callback)
                                callback();

                            if ($("#site_configuracao_produto_detalhe").data("callbackSalvar"))
                                $("#site_configuracao_produto_detalhe").data("callbackSalvar")(data.ProdutoInfo);
                        })
            }, $.produto.detalhe.retornarTrue);


        },

        retornarTrue: function() {
            return true;
        },

        // --------------------------------------------------------------------
        //  validar: valida campos necessários do cadastro de planos
        // --------------------------------------------------------------------
        validar: function(callbackOk, callbackErro) {



            // Inicializa criticas
            var criticas = new Array();

            // Pede a validação para o forms
            criticas = $("#site_configuracao_produto_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });

            // Se tem criticas, mostra o formulario
            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                callbackErro();

            } else
                callbackOk();
        },

        // ----------------------------------------------------------------------
        // remover: Trata remoção de um produto
        // ----------------------------------------------------------------------
        remover: function(codigoProduto, callback) {

            if (codigoProduto) {

                // Salva objeto com o item removido
                executarServico(
                "RemoverProdutoRequest",
                { CodigoProduto: codigoProduto },
                function(data) {

                    var criticas = data.Criticas;

                    if (criticas[0]) {

                        // Exibe a critica
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: criticas[0].Descricao,
                            callbackOK: function() {
                            }
                        });

                    }
                    else {
                        // Chama o callback
                        if (callback)
                            callback();
                    };
                });
            }

            else {

                // Pede para selecionar uma linha
                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Favor selecionar uma linha!",
                    callbackOK: function() { }
                });
            }
        },

        listarPlanos: function(callback) {

            // Cria request
            var request = {};

            // Pede lista de produtos
            executarServico(
            "ListarPlanoRequest",
            request,
            function(data) {

                // Limpa grid
                $("#tb_PlanosDisponiveis").clearGridData();
                $("#tb_PlanosSelecionados").clearGridData();
                for (var i = 0; i < data.Resultado.length; i++)
                    $("#tb_PlanosDisponiveis").jqGrid("addRowData", data.Resultado[i].CodigoPlano, data.Resultado[i]);

                // Chama callback
                if (callback)
                    callback();
            });
        },

        selecionarTodosPlanos: function(callback) {
            var ids = $("#tb_PlanosDisponiveis").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                $.produto.detalhe.selecionarPlano(ids[i]);
            }
        },

        selecionarPlano: function(codigoPlano, callback) {
            $("#tb_PlanosSelecionados").jqGrid('addRowData', codigoPlano, $("#tb_PlanosDisponiveis").jqGrid('getRowData', codigoPlano));
            $("#tb_PlanosDisponiveis").jqGrid('delRowData', codigoPlano);
        },

        removerTodosPlanos: function(callback) {
            var ids = $("#tb_PlanosSelecionados").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                $.produto.detalhe.removerPlano(ids[i]);
            }
        },

        removerPlano: function(codigoPlano, callback) {
            $("#tb_PlanosDisponiveis").jqGrid('addRowData', codigoPlano, $("#tb_PlanosSelecionados").jqGrid('getRowData', codigoPlano));
            $("#tb_PlanosSelecionados").jqGrid('delRowData', codigoPlano);
        }
    };

});

