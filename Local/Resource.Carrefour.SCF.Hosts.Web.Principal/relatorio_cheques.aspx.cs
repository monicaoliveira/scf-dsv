﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos;
using System.Text;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class relatorio_cheques : System.Web.UI.Page //1465
    {
        ListarProtocoloChequeResponse resposta = new ListarProtocoloChequeResponse();
        
        public Decimal TotalQtdeCheques { get; set; }
        public Decimal TotalValorCheques{ get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            ChequesRepeater.ItemDataBound += new RepeaterItemEventHandler(ChequesRepeater_ItemDataBound);
            resposta =
                    Mensageria.Processar<ListarProtocoloChequeResponse>(
                        new ListarProtocoloChequeRequest()
                        {	FiltroDataEstornoDe 			=   this.Request["FiltroDataEstornoDe"] != "null" ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["FiltroDataEstornoDe"]) : null
                        ,   FiltroDataEstornoAte = this.Request["FiltroDataEstornoAte"] != "null" ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["FiltroDataEstornoAte"]) : null
                        ,   FiltroDataProcessamentoDe = this.Request["FiltroDataProcessamentoDe"] != "null" ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["FiltroDataProcessamentoDe"]) : null
                        ,   FiltroDataProcessamentoAte = this.Request["FiltroDataProcessamentoAte"] != "null" ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["FiltroDataProcessamentoAte"]) : null
                        ,   FiltroDataRegProtocoloDe = this.Request["FiltroDataRegProtocoloDe"] != "null" ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["FiltroDataRegProtocoloDe"]) : null
                        ,   FiltroDataRegProtocoloAte = this.Request["FiltroDataRegProtocoloAte"] != "null" ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["FiltroDataRegProtocoloAte"]) : null
                        ,   FiltroEnvioCci = this.Request["FiltroEnvioCci"] != "null" ? this.Request["FiltroEnvioCci"] : null
                        ,   FiltroNumeroProtocolo = this.Request["FiltroNumeroProtocolo"] != "null" ? this.Request["FiltroNumeroProtocolo"] : null
                        ,   FiltroEstabelecimento = this.Request["FiltroEstabelecimento"] != "null" ? this.Request["FiltroEstabelecimento"] : null
                        ,   FiltroStatusProtocolo = this.Request["FiltroStatusProtocolo"] != "null" ? this.Request["FiltroStatusProtocolo"] : null
                        ,   FiltroContaCliente = this.Request["FiltroContaCliente"] != "null" ? this.Request["FiltroContaCliente"] : null
                        ,   FiltroTipoEstorno = this.Request["FiltroTipoEstorno"] != "null" ? this.Request["FiltroTipoEstorno"] : null
                        ,   TipoArquivo = this.Request["TipoArquivo"] != "null" ? this.Request["TipoArquivo"].ToString() : null 
                        });
            //-------------------------------------------------------------------------------------------
            // Tratando estabelecimento
            //-------------------------------------------------------------------------------------------
            string codigoEstabelecimento = this.Request["FiltroEstabelecimento"];
            if (codigoEstabelecimento == "null")
                this.lblFiltroEstabelecimento.Text = "Todos";
            else
            {   ReceberEstabelecimentoResponse reg = Mensageria.Processar<ReceberEstabelecimentoResponse>
                (   new ReceberEstabelecimentoRequest() 
                    {   CodigoEstabelecimento = codigoEstabelecimento
                    }
                );
                if (reg.StatusResposta == Resource.Framework.Library.Servicos.Mensagens.MensagemResponseStatusEnum.OK)
                    this.lblFiltroEstabelecimento.Text = reg.EstabelecimentoInfo.CNPJ + "-" + reg.EstabelecimentoInfo.RazaoSocial;
                else
                    this.lblFiltroEstabelecimento.Text = codigoEstabelecimento + " não cadastrado";
            }
            //-------------------------------------------------------------------------------------------
            // Tratando tipo estorno
            //-------------------------------------------------------------------------------------------
            string codigotipoEstorno = this.Request["FiltroTipoEstorno"];
            if (codigotipoEstorno == "null")
                this.lblFiltroTipoEstorno.Text = "Todos";
            else
            {
                this.lblFiltroEstabelecimento.Text = codigotipoEstorno + " não cadastrado";
                ListarTipoEstornoResponse ListaTipoEstorno = Mensageria.Processar<ListarTipoEstornoResponse>(new ListarTipoEstornoRequest() { FiltroTipoEstorno=codigotipoEstorno });
                foreach (ListaItemInfo itemtipoEstorno in ListaTipoEstorno.Resultado)
                {
                    if (codigotipoEstorno ==itemtipoEstorno.Valor)
                    {
                        this.lblFiltroEstabelecimento.Text = itemtipoEstorno.Valor + "-" + itemtipoEstorno.Descricao;
                        break;
                    }
                }
            }
            //-------------------------------------------------------------------------------------------
            // Tratando Status Protocolo
            //-------------------------------------------------------------------------------------------
            string codigostatusProtocolo = this.Request["FiltroStatusProtocolo"] ;
            if (codigostatusProtocolo == "null")
                this.lblFiltroStatusProtocolo.Text = "Todos";
            else
            {
                this.lblFiltroStatusProtocolo.Text = "Status Invalido: " + codigostatusProtocolo;
                if (codigostatusProtocolo == "S")
                    this.lblFiltroStatusProtocolo.Text = "Concluídos";
                if (codigostatusProtocolo == "N")
                    this.lblFiltroStatusProtocolo.Text = "Pendentes";
            }
            //-------------------------------------------------------------------------------------------
            // Tratando Status Envio CCI
            //-------------------------------------------------------------------------------------------
            string codigoenvioCCI = this.Request["FiltroStatusProtocolo"];
            if (codigoenvioCCI == "null")
                this.lblFiltroEnvioCci.Text = "Todos";
            else
            {
                if (codigoenvioCCI == "1")
                    this.lblFiltroEnvioCci.Text = "Enviadas";
                else
                    if (codigoenvioCCI == "9")
                        this.lblFiltroEnvioCci.Text = "Não Enviadas";
                    else
                        this.lblFiltroEnvioCci.Text = "Todas";
            }
            //-------------------------------------------------------------------------------------------
            // demais filtros
            //-------------------------------------------------------------------------------------------
            this.lblFiltroDataEstornoDe.Text = this.Request["FiltroDataEstornoDe"] != "null" ? this.Request["FiltroDataEstornoDe"] : "Qualquer";
            this.lblFiltroDataEstornoAte.Text = this.Request["FiltroDataEstornoAte"] != "null" ? this.Request["FiltroDataEstornoAte"] : "Qualquer";
            this.lblFiltroDataProcessamentoDe.Text = this.Request["FiltroDataProcessamentoDe"] != "null" ? this.Request["FiltroDataProcessamentoDe"] : "Qualquer";
            this.lblFiltroDataProcessamentoAte.Text = this.Request["FiltroDataProcessamentoAte"] != "null" ? this.Request["FiltroDataProcessamentoAte"] : "Qualquer";
            this.lblFiltroDataRegProtocoloDe.Text = this.Request["FiltroDataRegProtocoloDe"] != "null" ? this.Request["FiltroDataRegProtocoloDe"] : "Qualquer";
            this.lblFiltroDataRegProtocoloAte.Text = this.Request["FiltroDataRegProtocoloAte"] != "null" ? this.Request["FiltroDataRegProtocoloAte"] : "Qualquer";
            this.lblFiltroNumeroProtocolo.Text = this.Request["FiltroNumeroProtocolo"] != "null" ? this.Request["FiltroNumeroProtocolo"] : "Todos";
            this.lblFiltroContaCliente.Text = this.Request["FiltroContaCliente"] != "null" ? this.Request["FiltroContaCliente"] : "Todas";
            
            this.lblDataHora.Text = String.Format("{0:dd/MM/yyyy - HH:mm}", DateTime.Now);
            this.lblTitulo.Text = "";

            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (resposta.Resultado.Count == 0)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblmensagem.Text = ("Sem dados para imprimir pelo filtro informado.");
                    this.trmensagem.Visible = true;
                    String cstext = "alert('Sem dados para imprimir pelo filtro informado.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
            else if (resposta.Resultado.Count > 1000 && (this.Request["TipoArquivo"] != "PDF"))
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblmensagem.Text = ("Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.");
                    this.trmensagem.Visible = true;
                    String cstext = "alert('Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
            
            ChequesRepeater.DataSource = TratarCampos(resposta.Resultado);
            ChequesRepeater.DataBind();
        }

        void ChequesRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label lbl;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ProcessoProtocoloChequeInfo dados = (ProcessoProtocoloChequeInfo)e.Item.DataItem;
                TotalQtdeCheques += 1;
                TotalValorCheques += dados.Valor;
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                lbl = e.Item.FindControl("lblTotalQtdeCheques") as Label;
                if (lbl != null)
                    lbl.Text = TotalQtdeCheques.ToString("N0");

                lbl = e.Item.FindControl("lblTotalValorCheques") as Label;
                if (lbl != null)
                    lbl.Text = TotalValorCheques.ToString("N2");
            }
        }
        List<ProcessoProtocoloChequeInfo> TratarCampos(List<ProcessoProtocoloChequeInfo> resultado)
        {
            List<ProcessoProtocoloChequeInfo> retorno = new List<ProcessoProtocoloChequeInfo>();

            foreach (ProcessoProtocoloChequeInfo dados in resultado)
            {
                //=========================================================================================
                // Tratando tipo estorno
                //=========================================================================================
                string codigotipoEstorno = dados.TipoEstorno;
                string descricaotipoEstorno = "";
                ListarTipoEstornoResponse ListaTipoEstorno = Mensageria.Processar<ListarTipoEstornoResponse>(new ListarTipoEstornoRequest() { FiltroTipoEstorno = codigotipoEstorno });
                foreach (ListaItemInfo itemtipoEstorno in ListaTipoEstorno.Resultado)
                {
                    if (codigotipoEstorno == itemtipoEstorno.Valor)
                    {
                        descricaotipoEstorno = itemtipoEstorno.Valor + "-" + itemtipoEstorno.Descricao;
                        break;
                    }
                }
                dados.TipoEstorno = descricaotipoEstorno;
                //=========================================================================================
                // Tratando tipo estorno
                //=========================================================================================
                string codigostatusProtocolo = dados.StatusProtocolo;
                string descricaoStatusProtocolo = "";
                if (codigostatusProtocolo == "S")
                    descricaoStatusProtocolo = "Concluídos";
                else
                    if (codigostatusProtocolo == "N")
                        descricaoStatusProtocolo = "Pendentes";
                dados.StatusProtocolo = descricaoStatusProtocolo;

                retorno.Add(dados);
                //=========================================================================================
            }
            return retorno;
        }
    }

}
