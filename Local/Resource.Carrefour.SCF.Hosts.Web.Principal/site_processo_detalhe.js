﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.processo)
        $.processo = {};

    // Funcoes do namespace processo.detalhe
    $.processo.detalhe = {

        // --------------------------------------------------------------------
        //  load: garante que a tela está carregada
        // --------------------------------------------------------------------
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_processo_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_processo_detalhe.aspx #site_processo_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_processo_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.processo.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();
                    });
                });

            } else {

                // Chama o callback
                if (callback)
                    callback();
            }
        },

        // --------------------------------------------------------------------
        //  load2: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load2: function(callback) {

            // Layout dos botões
            $("#site_processo_detalhe button").button();

            // Inicializa Forms
            $("#site_processo_detalhe").Forms("initializeFields");

            // Tabs
            $("#site_processo_detalhe .abas").tabs();

            // Botão log
            $("#site_processo_detalhe_aba_log").click(function() {
                var codigoOrigem = $("#tbProcesso").jqGrid("getGridParam", "selrow");
                $.log.lista.abrir({ FiltroTipoOrigem: "ProcessoInfo", FiltroCodigoOrigem: codigoOrigem });
            });

            // Inicializa o dialog
            $("#site_processo_detalhe").dialog({
                autoOpen: true,
                height: 550,
                width: 900,
                modal: true,
                buttons: {

                    "Fechar": function() {
                        $(this).dialog("close");
                        return false;
                    }
                },
                title: "Detalhe do Processo"
            });

            // Callback
            if (callback)
                callback();


            $("#tbArquivos").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                sortable: true,
                scroll: true,
                colNames: ["Código Arquivo", "Nome Arquivo", "Tipo Arquivo", "Data Inclusão", "Código Processo", "Status"],
                colModel: [
                  { name: "CodigoArquivo", index: "CodigoArquivo", sorttype: "integer", key: true, width: 50, align: "center" },
                  { name: "NomeArquivo", index: "NomeArquivo", width: 190 },
                  { name: "TipoArquivo", index: "TipoArquivo", width: 60 },
                  { name: "DataInclusao", index: "DataInclusao", width: 60, align: "center" },
                  { name: "CodigoProcesso", index: "CodigoProcesso", width: 50, align: "center" },
                  { name: "StatusArquivo", index: "StatusArquivo", width: 50 }
                ],
                height: 150,
                width: 870,
                sortname: "CodigoArquivo",
                sortorder: "asc",
                viewrecords: true
            });

            // Botão detalhe de arquivo
            $("#site_processo_detalhe .cmdDetalheArquivoProcesso").click(function() {

                // Verifica se tem arquivo selecionado
                var codigoArquivo = $("#tbArquivos").jqGrid("getGridParam", "selrow");
                if (codigoArquivo) {

                    // Bloqueia tela anterior e botões
                    $("#site_processo_detalhe").attr("disabled", true);
                    $("#site_processo_detalhe .cmdDetalheArquivoProcesso").attr("disabled", true).addClass("ui-state-disabled");
                    //$(".ui-dialog-buttonpane button:contains('Fechar')").attr("disabled", true).addClass("ui-state-disabled");

                    // Pede a exportacao do arquivo
                    $.arquivo.detalhe.abrir(codigoArquivo);

                } else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar um arquivo.",
                        callbackOK: function() { }
                    });
                }
            });
            
            // 1463 Botão Inconsistencias Detalhadas
            $("#site_processo_detalhe .cmdInconsDetal").click(function() 
            {   var codigoArquivo = $("#tbArquivos").jqGrid("getGridParam", "selrow");
                if (codigoArquivo) 
                {   executarServico
                    (   "GerarRelatorioTransacaoInconsistenteExcelRequest"
                        ,   { CodigoArquivo: codigoArquivo }
                        ,   function(data) 
                            {   if (data.StatusResposta = "OK") 
                                {   popup(
                                    {   titulo: "Relatório Inconsistências"
                                    ,   icone: "sucesso"
                                    ,   mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório."
                                    ,   callbackOK: function() { }
                                    });
                                }
                            }
                    );
                } else 
                {   popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar um arquivo.",
                        callbackOK: function() { }
                    });
                }
            });
            // 1463 Botão Inconsistencias Detalhadas
            $("#site_processo_detalhe .cmdInconsAgrup").click(function() 
            {   var codigoArquivo = $("#tbArquivos").jqGrid("getGridParam", "selrow");
                if (codigoArquivo) 
                {   executarServico
                    (   "GerarRelatorioInconsistenteAgrupadoExcelRequest"
                        ,   { CodigoArquivo: codigoArquivo }
                        ,   function(data) 
                            {   if (data.StatusResposta = "OK") 
                                {   popup(
                                    {   titulo: "Relatório Inconsistências Agrupadas"
                                    ,   icone: "sucesso"
                                    ,   mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório."
                                    ,   callbackOK: function() { }
                                    });
                                }
                            }
                    );
                } else 
                {   popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar um arquivo.",
                        callbackOK: function() { }
                    });
                }
            });
            
        },

        // --------------------------------------------------------------------
        //  listar: listar os arquivos de acordo com o processo
        // --------------------------------------------------------------------
        listar: function(callback) {

            // Cria request
            var request = {};

            var codigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");

            // Pede lista de arquivos
            executarServico(
            "ListarArquivoRequest",
            { FiltroCodigoProcesso: codigoProcesso },
            function(data) {

                // Limpa grid
                $("#tbArquivos").clearGridData();
                for (var i = 0; i < data.Resultado.length; i++)
                    $("#tbArquivos").jqGrid("addRowData", data.Resultado[i].CodigoArquivo, data.Resultado[i]);

                // Chama callback
                if (callback)
                    callback();
            });
        },
        // --------------------------------------------------------------------
        //  abrir: mostra o detalhe do processo
        // --------------------------------------------------------------------
        abrir: function(codigoProcesso, callback) {

            // Garante que a tela está carregada
            $.processo.detalhe.load(function() {

                // Limpa campos do detalhe do processo
                $("#tbArquivos").clearGridData();
                $("#site_processo_detalhe").Forms("clearData");
                $("#site_processo_detalhe_aba_detalhe").Forms("clearData");
                $("#site_processo_detalhe_aba_arquivos").Forms("clearData");
            
                // Carrega informações do log
                $.processo.detalhe.carregarDados(codigoProcesso, function() {

                    // Carrega a primeira aba
                    $("#site_processo_detalhe_aba_detalhe").tabs("select", 0);

                    // Mostra o dialog
                    $("#site_processo_detalhe").dialog("open");

                    //Lista Arquivos
                    $.processo.detalhe.listar();

                    // Chama o callback
                    if (callback)
                        callback();
                });
            });
        },


        // ----------------------------------------------------------------------
        // Carrega as informações da tela
        //
        //  callback:       Função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        carregarDados: function(codigoProcesso, callback) {

            // Pede informações do processo
            executarServico(
                "ReceberProcessoRequest",
                { CodigoProcesso: codigoProcesso },
                function(data) {

                    // Preenche detalhe
                    $("#site_processo_detalhe_aba_detalhe").Forms("setData", { dataObject: data.ProcessoInfo });

                    // Chama o callback
                    if (callback)
                        callback();
                });
        }
    }
});