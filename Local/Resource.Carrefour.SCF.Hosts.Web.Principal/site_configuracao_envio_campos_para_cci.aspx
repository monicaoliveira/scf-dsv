﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_envio_campos_para_cci.js"></script>
    <link href="site_configuracao_envio_campos_para_cci.css" rel="stylesheet" type="text/css" />
    <title>SCF - Configurações Envio Campos CCI</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--    Lista de Envio de campos para CCI -->
    <!-- ------------------------------------ -->
    <div id="site_configuracao_envio_campos_para_cci_lista" class="lista">
        <table id="tbCamposEnvioCci"></table>
        
    </div>

</asp:Content>
