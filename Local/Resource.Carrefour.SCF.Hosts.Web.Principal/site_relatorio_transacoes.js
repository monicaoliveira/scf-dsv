﻿$(document).ready(function() {

    // Namespace
    if (!$.relatorio)
        $.relatorio = {};

    // Funcoes do namespace 
    $.relatorio.filtro = {

        variaveis: {},

        //=================================================================================
        // load: funcoes de inicializacao da tela
        //=================================================================================
        load: function(callback) {

            // Insere o subtítulo
            $("#subtitulo").html("Relatórios > Relatório de Transações");

            //------------------------------------------------------------------
            // Estilo dos botões
            //-------------------------------------------------------------------
            $("#site_relatorio_transacoes button").button();

            //-------------------------------------------------------------------
            // Inicializa forms
            //-------------------------------------------------------------------
            $("#site_relatorio_transacoes").Forms("initializeFields");

            $.relatorio.filtro.limparReferencia();

            //-------------------------------------------------------------------
            // BOTÃO GERAR INTERCHANGE SCF-1518
            //-------------------------------------------------------------------
            $("#btnGerarInterchange").click(function() {
                $.relatorio.filtro.gerarInterchange();
            });

            //-------------------------------------------------------------------
            // BOTÃO VISUALIZAR RELATÓRIO 
            //-------------------------------------------------------------------
            $("#btnVisualizarRelatorio").click(function() {

                //$.relatorio.filtro.receberNumeroDeLinhas(                function() { 
                $.relatorio.filtro.visualizarRelatorio();
                //});

                return false;
            });

            //-------------------------------------------------------------------
            // BOTÃO VISUALIZAR RELATÓRIO INCONSISTENTE
            //-------------------------------------------------------------------
            $("#btnVisualizarRelatorioInconsistente").click(function() {

                // $.relatorio.filtro.receberNumeroDeLinhas(function() { 
                $.relatorio.filtro.visualizarRelatorioInconsistente();
                //});

                return false;
            });

            //-------------------------------------------------------------------
            // BOTÃO GERAR RELATÓRIO PDF
            //-------------------------------------------------------------------
            $("#btnGerarRelatorioPDF").click(function() {
                $.relatorio.filtro.gerarRelatorioPDF();
            });

            //-------------------------------------------------------------------
            // BOTÃO GERAR RELATÓRIO EXCEL
            //-------------------------------------------------------------------
            $("#btnGerarRelatorioExcel").click(function() {
                if ($("#txtDataProcessamentoDe").val() == "" &&
                    $("#txtDataProcessamentoAte").val() == "" &&
                    $("#txtDataTransacaoDe").val() == "" &&
                    $("#txtDataTransacaoAte").val() == "" &&
                    $("#txtDataVencimentoDe").val() == "" &&
                    $("#txtDataVencimentoAte").val() == "" &&
                    $("#txtDataAgendamentoDe").val() == "" &&
                    $("#txtDataAgendamentoAte").val() == "" &&
                    $("#txtDataEnvioMateraDe").val() == "" &&
                    $("#txtDataEnvioMateraAte").val() == "" &&
                    $("#txtDataLiquidacaoDe").val() == "" &&
                    $("#txtDataLiquidacaoAte").val() == "" &&
                    $("#txtDataRetornoCessaoDe").val() == "" &&
                    $("#txtDataRetornoCessaoAte").val() == "" &&
                    $("#txtNSUHost").val() == "") {

                    popup({
                        titulo: "Relatório Excel",
                        icone: "alerta",
                        mensagem: "Selecione pelo menos uma data!",
                        callbackOK: function() { }
                    });
                }
                else {

                    // Pede relatório
                    $.relatorio.filtro.gerarRelatorioExcel();
                }
            });

            //-------------------------------------------------------------------
            // Evento change do combo de grupo 
            //-------------------------------------------------------------------
            $("#cmbEmpresaGrupo").change(function() {

                // Carrega combo de subgrupos correspondente ao grupo
                $.relatorio.filtro.carregarSubGrupos(function() {

                    // Carrega combo de estabelecimentos correspondente ao grupo
                    $.relatorio.filtro.carregarEstabelecimentos(function() {

                        // Chama callback
                        if (callback)
                            callback();
                    });
                });
            });

            //-------------------------------------------------------------------
            // Evento change do combo de produtos
            //-------------------------------------------------------------------
            $("#cmbProduto").change(function() {

                // Carrega combo de planos correspondente ao produto
                $.relatorio.filtro.carregarPlanos(function() {

                    // Chama callback
                    if (callback)
                        callback();
                });
            });

            //-------------------------------------------------------------------
            // Evento change do combo de estabelecimentos
            //-------------------------------------------------------------------
            $("#cmbEmpresaSubGrupo").change(function() {

                // Carrega combo de estabelecimentos correspondente ao subgrupo
                $.relatorio.filtro.carregarEstabelecimentos(function() {

                    // Chama callback
                    if (callback)
                        callback();
                });
            });

            //-------------------------------------------------------------------
            // Preenche combo de referencia
            //-------------------------------------------------------------------
            $.relatorio.filtro.carregarReferencia(function(callback) {
                // Chama callback
                if (callback)
                    callback();
            });

            // Carrega listas de enumeradores
            carregarListas("TransacaoStatusEnum", function() {
                // Preenche combo de Status Transacao
                var el = $("#cmbStatusTransacao").multiselect(
                {
                    checkAllText: "Selecionar todos",
                    uncheckAllText: "Remover seleção",
                    selectedText: "# selecionados",
                    noneSelectedText: ""
                });

                preencherComboComValor($("#cmbStatusTransacao"), "TransacaoStatusEnum");

                $('#cmbStatusTransacao option[value="1"]').attr({ selected: "selected" });
                el.multiselect('refresh');
            });
        },


        //=================================================================================
        // limparReferencia: Limpa os filtros antes da construcao
        //=================================================================================
        limparReferencia: function(callback) {

            // Limpa o combo
            $("#cmbReferencia").children().each(
                function(index) {
                    $(this).remove();
                }
            );

            elRef = $("#cmbReferencia").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: "",
                minWidth: 120
            });

            $("button.ui-multiselec").css("width", "114px");

            if (callback)
                callback();
        },

        //=================================================================================
        // carregarReferencia: carrega combo de referencia
        //=================================================================================
        carregarReferencia: function(callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    NomeLista: "Referencia"
                },
                function(data) {

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elRef);
                    }

                    // Ordena a lista pelo nome do produto
                    $("#cmbReferencia").html($("option", $("#cmbReferencia")).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
                    }));

                    elRef.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //=================================================================================
        // carregarSubGrupos: carrega combo de subgrupos de acordo com o grupo 
        //                selecionado, caso tenha
        //=================================================================================
        carregarSubGrupos: function(callback) {

            // Inicializa filtro
            var codigoEmpresaGrupo = null;

            // Pega código do grupo, se tiver
            if ($("#cmbEmpresaGrupo").val() != "?")
                codigoEmpresaGrupo = $("#cmbEmpresaGrupo").val();

            // Pede lista de subgrupos
            executarServico(
                "ListarEmpresaSubGrupoRequest",
                { FiltroCodigoEmpresaGrupo: codigoEmpresaGrupo },
                function(data) {

                    // Guarda resultado
                    $.relatorio.filtro.variaveis.subgrupos = data.Resultado;

                    // Limpa o combo (exceto o primeiro item)
                    $("#cmbEmpresaSubGrupo").children().each(
		                function(index) {
		                    if (index != 0)
		                        $(this).remove();
		                }
	                );

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {

                        // Adiciona o item no combo
                        $("#cmbEmpresaSubGrupo").append("<option value='" + data.Resultado[i].CodigoEmpresaSubGrupo + "'>"
				                + data.Resultado[i].NomeEmpresaSubGrupo + "</option>");
                    }

                    // chama callback
                    if (callback)
                        callback();
                });
        },

        //=================================================================================
        // carregarPlanos: carrega combo de planos de acordo com o produto 
        //                selecionado, caso tenha
        //=================================================================================
        carregarPlanos: function(callback) {

            // Inicializa filtro
            var codigoProduto = null;

            // Pega código do produto, se tiver
            if ($("#cmbProduto").val() != "?")
                codigoProduto = $("#cmbProduto").val();

            // Pede lista de planos
            executarServico(
                "ListarPlanoRequest",
                { FiltroCodigoProduto: codigoProduto },
                function(data) {

                    // Guarda resultado
                    $.relatorio.filtro.variaveis.planos = data.Resultado;

                    // Limpa o combo (exceto o primeiro item)
                    $("#cmbPlano").children().each(
		                function(index) {
		                    if (index != 0)
		                        $(this).remove();
		                }
	                );

                    // Varre lista de planos
                    for (var i = 0; i < data.Resultado.length; i++) {

                        // Adiciona o item no combo
                        $("#cmbPlano").append("<option value='" + data.Resultado[i].CodigoPlano + "'>"
				                + data.Resultado[i].CodigoPlanoTSYS + " - " + data.Resultado[i].DescricaoPlano + "</option>");
                    }
                });
        },

        //=================================================================================
        // carregarEstabelecimentos: carrega combo de estabelecimentos de acordo com
        //                          o grupo e/ou subgrupo selecionados, caso tenha
        //=================================================================================
        carregarEstabelecimentos: function(callback) {

            // Inicializa filtro
            var codigoEmpresaSubGrupo = null;
            var codigoEmpresaGrupo = null;

            // Pega código do subgrupo, se tiver
            if ($("#cmbEmpresaSubGrupo").val() != "?")
                codigoEmpresaSubGrupo = $("#cmbEmpresaSubGrupo").val();

            // Pega código do grupo, se tiver
            if ($("#cmbEmpresaGrupo").val() != "?")
                codigoEmpresaGrupo = $("#cmbEmpresaGrupo").val();

            // Pede lista de estabelecimentos
            executarServico(
                "ListarEstabelecimentoRequest",
                {
                    FiltroCodigoEmpresaSubGrupo: codigoEmpresaSubGrupo,
                    FiltroCodigoEmpresaGrupo: codigoEmpresaGrupo
                },
                function(data) {

                    // Guarda resultado
                    $.relatorio.filtro.variaveis.estabelecimentos = data.Resultado;

                    // Limpa o combo (exceto o primeiro item)
                    $("#cmbEstabelecimento").children().each(
		                function(index) {
		                    if (index != 0)
		                        $(this).remove();
		                }
	                );

                    // Varre lista de estabelecimentos
                    for (var i = 0; i < data.Resultado.length; i++) {

                        // Adiciona o item no combo
                        $("#cmbEstabelecimento").append("<option value='" + data.Resultado[i].CodigoEstabelecimento + "'>"
				                + data.Resultado[i].RazaoSocial + "</option>");
                    }
                });
        },

        //=================================================================================
        // visualizarRelatório: permite a visualização do resultado do relatório
        //=================================================================================
        visualizarRelatorio: function(callback) {

            var request = $.relatorio.filtro.preencherRequest();
            request.RelatorioDeInconsistente = false;

            //----------------------------------------------
            // Monta e abre visualização do relatório
            //----------------------------------------------
            $(".ObjetoJson").val($.toJSON(request));
            window.open('relatorio_transacoes.aspx', 'page', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1024, height=600');

            // Retorno
            return false;
        },        /*SCF-1518*/
        // =================================================================================
        //  gerarInterchange: cria um arquivo csv com Interchange Divergencia
        // =================================================================================
        gerarInterchange: function() {
            var request = {};
            var erro = 'N';

            $("#site_divergencia_interchange").Forms("getData", { dataObject: request });

            if (request.FiltroDataMovimentoDe == null) {
                popup({
                    titulo: "Interchange Divergencia",
                    icone: "erro",
                    mensagem: "Favor informar a data inicial do filtro data de processamento",
                    callbackOK: function() {
                        $("#txtDataProcessamentoDe").focus();
                    }
                });
                erro = 'S';
                
            } else {
                var date;

                date = new Date(request.FiltroDataMovimentoDe.substring(6, 10), request.FiltroDataMovimentoDe.substring(3, 5) - 1, request.FiltroDataMovimentoDe.substring(0, 2));
                if (date > new Date()) {
                    popup({
                        titulo: "Interchange Divergencia",
                        icone: "erro",
                        mensagem: "Favor informar data inicial menor igual à hoje no filtro data de processamento",
                        callbackOK: function() {
                            $("#txtDataProcessamentoDe").focus();
                        }
                    });
                    erro = 'S';
                }
            }

            if (erro == 'N') {
                var dateFim;
                if (request.FiltroDataMovimentoAte == null) {
                    popup({
                        titulo: "Interchange Divergencia",
                        icone: "erro",
                        mensagem: "Favor informar a data final do filtro data de processamento",
                        callbackOK: function() {
                            $("#txtDataProcessamentoAte").focus();
                        }
                    });
                    erro = 'S';
                } else {

                    dateFim = new Date(request.FiltroDataMovimentoAte.substring(6, 10), request.FiltroDataMovimentoAte.substring(3, 5) - 1, request.FiltroDataMovimentoAte.substring(0, 2));
                    if (dateFim > new Date()) {
                        popup({
                            titulo: "Interchange Divergencia",
                            icone: "erro",
                            mensagem: "Favor informar data final menor igual à hoje no filtro data de processamento",
                            callbackOK: function() {
                                $("#txtDataProcessamentoAte").focus();
                            }
                        });
                        erro = 'S';
                    } else if (date > dateFim) {
                        popup({
                            titulo: "Interchange Divergencia",
                            icone: "erro",
                            mensagem: "Favor informar data inicial menor igual à data final de processamento",
                            callbackOK: function() {
                                $("#txtDataProcessamentoDe").focus();
                            }
                        });
                        erro = 'S';
                    }
                }
            }

            if (erro == 'N') {
                // Faz a chamada do servico
                executarServico("GerarRelatorioInterchangeExcelRequest", request, function(data) {
                    popup({
                        titulo: "Interchange Divergencia",
                        icone: "sucesso",
                        mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
                        callbackOK: function() { }
                    });
                });
            }
        },


        //===================================================================================
        // visualizarRelatórioInconsistente: permite a visualização do resultado do relatório
        //===================================================================================
        //        visualizarRelatorioInconsistente: function(callback) {

        //            var request = $.relatorio.filtro.preencherRequest();
        //            
        //            // Flag para relatório inconsistentes
        //            request.RelatorioDeInconsistente = true;

        //            //----------------------------------------------
        //            // Monta e abre visualização do relatório
        //            //----------------------------------------------
        //            $(".ObjetoJson").val($.toJSON(request));
        //            window.open('relatorio_transacoes.aspx', 'page', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1024, height=600');

        //            // Retorno
        //            return false;
        //        },

        //=================================================================================
        // gerarRelatorioExcel: gera o relatório em Excel de transações conforme filtros
        //=================================================================================
        gerarRelatorioExcel: function() {

            //----------------------------------------------
            // Preenche request
            //----------------------------------------------
            var request = $.relatorio.filtro.preencherRequest();

            // Faz a chamada do servico
            executarServico("GerarRelatorioTransacaoExcelRequest", { Param: request }, function(data) {
                popup({
                    titulo: "Relatório Excel",
                    icone: "sucesso",
                    mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
                    callbackOK: function() { }
                });
            });

        },

        //=================================================================================
        // gerarRelatorioPDF: gera o relatório em PDF de transações conforme filtros
        //=================================================================================
        gerarRelatorioPDF: function(callback) {

            // Chama callback
            if (callback)
                callback();
        },

        //=================================================================================
        // preencherRequest: preenche request para enviar como parametro ao serviço
        //=================================================================================
        preencherRequest: function() {
            var request = {};

            // Origem
            if ($("#cmbOrigem").find('option:selected').val() != "?") {
                request.CodigoArquivo = $("#cmbOrigem").val();
                request.TipoArquivo = $("#cmbOrigem option:selected").text();
            }

            // Grupo
            if ($("#cmbEmpresaGrupo").find('option:selected').val() != "?") {
                request.CodigoEmpresaGrupo = $("#cmbEmpresaGrupo").val();
                request.NomeEmpresaGrupo = $("#cmbEmpresaGrupo option:selected").text();
            }

            // Subgrupo
            if ($("#cmbEmpresaSubGrupo").find('option:selected').val() != "?") {
                request.CodigoEmpresaSubGrupo = $("#cmbEmpresaSubGrupo").val();
                request.NomeEmpresaSubGrupo = $("#cmbEmpresaSubGrupo option:selected").text();
            }

            // Estabelecimento            
            if ($("#cmbEstabelecimento").find('option:selected').val() != "?") {
                request.CodigoEstabelecimento = $("#cmbEstabelecimento").val();
                request.RazaoSocial = $("#cmbEstabelecimento option:selected").text();
            }

            // Favorecido            
            if ($("#cmbFavorecido").find('option:selected').val() != "?") {
                request.CodigoFavorecido = $("#cmbFavorecido").val();
                request.NomeFavorecido = $("#cmbFavorecido option:selected").text();
            }

            // Produto            
            if ($("#cmbProduto").find('option:selected').val() != "?") {
                request.CodigoProduto = $("#cmbProduto").val();
                request.NomeProduto = $("#cmbProduto option:selected").text();
            }

            // Plano            
            if ($("#cmbPlano").find('option:selected').val() != "?") {
                request.CodigoPlano = $("#cmbPlano").val();
                request.NomePlano = $("#cmbPlano option:selected").text();
            }

            // Meio Captura
            if ($("#cmbMeioCaptura").find('option:selected').val() != "?") {
                request.CodigoMeioCaptura = $("#cmbMeioCaptura").val();
                request.MeioCaptura = $("#cmbMeioCaptura option:selected").text();
            }

            // Tipo Registro
            if ($("#cmbTipoRegistro").find('option:selected').val() != "?") {
                request.CodigoTipoRegistro = $("#cmbTipoRegistro").val();
                request.TipoRegistro = $("#cmbTipoRegistro option:selected").text();
            }

            // Tipo Transação
            if ($("#cmbTipoTransacao").find('option:selected').val() != "") {
                request.CodigoTipoTransacao = $("#cmbTipoTransacao").val();
                request.TipoTransacao = $("#cmbTipoTransacao option:selected").text();
            }

            // Número Pagamento            
            if ($("#txtNumeroPagamento").val() != "") {
                request.NumeroPagamento = $("#txtNumeroPagamento").val();
            }
            request.ContaCliente = $("#txtContaCliente").val(); // 1438 CONTA CLIENTE

            // Data Processamento (Data Movimento do registro L0)
            if ($("#txtDataProcessamentoDe").val() != "") {
                request.DataMovimentoDe = $("#txtDataProcessamentoDe").val();
            }

            if ($("#txtDataProcessamentoAte").val() != "") {
                request.DataMovimentoAte = $("#txtDataProcessamentoAte").val();
            }

            // Data Transação            
            if ($("#txtDataTransacaoDe").val() != "") {
                request.DataTransacaoDe = $("#txtDataTransacaoDe").val();
            }

            if ($("#txtDataTransacaoAte").val() != "") {
                request.DataTransacaoAte = $("#txtDataTransacaoAte").val();
            }

            // Data Vencimento            
            if ($("#txtDataVencimentoDe").val() != "") {
                request.DataVencimentoDe = $("#txtDataVencimentoDe").val();
            }

            if ($("#txtDataVencimentoAte").val() != "") {
                request.DataVencimentoAte = $("#txtDataVencimentoAte").val();
            }

            // Data Agendamento            
            if ($("#txtDataAgendamentoDe").val() != "") {
                request.DataAgendamentoDe = $("#txtDataAgendamentoDe").val();
            }

            if ($("#txtDataAgendamentoAte").val() != "") {
                request.DataAgendamentoAte = $("#txtDataAgendamentoAte").val();
            }

            // Data Envio Matera            
            if ($("#txtDataEnvioMateraDe").val() != "") {
                request.DataEnvioMateraDe = $("#txtDataEnvioMateraDe").val();
            }

            if ($("#txtDataEnvioMateraAte").val() != "") {
                request.DataEnvioMateraAte = $("#txtDataEnvioMateraAte").val();
            }

            // Data Liquidação            
            if ($("#txtDataLiquidacaoDe").val() != "") {
                request.DataLiquidacaoDe = $("#txtDataLiquidacaoDe").val();
            }

            if ($("#txtDataLiquidacaoAte").val() != "") {
                request.DataLiquidacaoAte = $("#txtDataLiquidacaoAte").val();
            }

            // Status Retorno            
            if ($("#cmbStatusRetorno").find('option:selected').val() != "?") {
                request.CodigoStatusRetorno = $("#cmbStatusRetorno").val();
                request.DescricaoStatusRetorno = $("#cmbStatusRetorno option:selected").text();
            }

            // Status Pagamento            
            if ($("#cmbStatusPagamento").find('option:selected').val() != "?") {
                request.CodigoStatusPagamento = $("#cmbStatusPagamento").val();
                request.DescricaoStatusPagamento = $("#cmbStatusPagamento option:selected").text();
            }

            // Status Conciliação            
            if ($("#cmbStatusConciliacao").find('option:selected').val() != "?") {
                request.CodigoStatusConciliacao = $("#cmbStatusConciliacao").val();
                request.DescricaoStatusConciliacao = $("#cmbStatusConciliacao option:selected").text();
            }

            // Status Validação            
            if ($("#cmbStatusValidacao").find('option:selected').val() != "?") {
                request.CodigoStatusValidacao = $("#cmbStatusValidacao").val();
                request.DescricaoStatusValidacao = $("#cmbStatusValidacao option:selected option:selected").text();
            }

            // Status Transação Cancelamento Online - NR-033
            if ($("#cmbStatusTransacao").find('option:selected').val() != null) {
                request.CodigoStatusCancelamentoOnLine = $("#cmbStatusTransacao").val().toString();
                request.DescricaoStatusTransacaoCancelamentoOnline = retornarTextoCombo($("#cmbStatusTransacao option:selected"));
            }

            // Data de retorno de cessão - NR-033
            if ($("#txtDataRetornoCessaoDe").val() != "") {
                request.DataRetornoCessaoDe = $("#txtDataRetornoCessaoDe").val();
            }

            if ($("#txtDataRetornoCessaoAte").val() != "") {
                request.DataRetornoCessaoAte = $("#txtDataRetornoCessaoAte").val();
            }

            // ECOMMERCE - Fernando Bove - 20160105
            // Tipo Financeiro Contábil
            if ($("#cmbFinanceiroContabil").find('option:selected').val() != "?") {
                request.FiltroFinanceiroContabil = $("#cmbFinanceiroContabil").val();
            }

            // ECOMMERCE - Fernando Bove - 20160106
            // NSU Host Transação
            if ($("#txtNSUHost").val() != "") {
                request.FiltroNSUHost = $("#txtNSUHost").val();
            }

            // Campo Referencia - ClockWork
            if ($("#cmbReferencia").find('option:selected').val() != null) {
                request.FiltroReferenciaCodigo = $("#cmbReferencia").val().toString();
                request.FiltroReferenciaDescricao = $("#cmbReferencia option:selected").text();
            }

            // Retorna request
            return request;
        },

        //=================================================================================
        // receberNumero: carrega combo de estabelecimentos de acordo com
        //                          o grupo e/ou subgrupo selecionados, caso tenha
        //=================================================================================
        receberNumeroDeLinhas: function(callback) {

            var request = {};

            // Envia informações ao objeto existente
            $("#site_relatorio_transacoes").Forms("getData", { dataObject: request });
            if (request.FiltroNumeroPagamento == "") {
                request.FiltroNumeroPagamento = null;
            }
            // Tipo Registro
            if ($("#cmbTipoRegistro").find('option:selected').val() != "?") {
                request.FiltroTipoRegistro = $("#cmbTipoRegistro option:selected").text();
            }
            request.FiltroContaCliente = $("#txtContaCliente").val(); //1438
            request.VerificarQuantidadeLinhas = true;

            // Verifica quantidade de linhas
            executarServico(
                "ListarRelatorioTransacaoRequest",
                request,
                function(data) {

                    if (data.QuantidadeLinhas > 1000) {

                        // Pergunta se o usuario deseja continuar..
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Não foi possível visualizar o relatório devido a quantidade excessiva de linhas (" + data.QuantidadeLinhas + "). Por favor selecione a opção para exportação de relatório para Excel.",
                            callbackOK: function() {
                                return true;
                            }
                        });
                    } else {

                        if (data.QuantidadeLinhas == 0) {
                            // Pergunta se o usuario deseja continuar..
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "O relatório que será gerado não possui linhas. Deseja continuar a operação?",
                                callbackSim: function() {
                                    if (callback)
                                        callback();
                                },
                                callbackNao: function() {
                                    return true;
                                }
                            });
                        } else {

                            if (callback)
                                callback();
                        }
                    }
                });
        }
    }

    // Chama load da página
    $.relatorio.filtro.load();
});