﻿$(document).ready(function() {

    // Namespace
    if (!$.subgrupo)
        $.subgrupo = {};

    // Funcoes do namespace 
    $.subgrupo.lista = {

        variaveis: {},

        //=================================================================================
        // load: funcoes de inicializacao da tela
        //=================================================================================
        load: function(callback) {

            // Seta o subtítulo da tela
            $("#subtitulo").html("Configurações > Cadastro de Subgrupos");

            //------------------------------------------------------------------
            // Estilo dos botões
            //-------------------------------------------------------------------
            $("#site_subgrupo_lista button").button();

            //-------------------------------------------------------------------
            // Inicializa forms
            //-------------------------------------------------------------------
            $("#site_subgrupo_lista").Forms("initializeFields");

            //-------------------------------------------------------------------
            // Inicializa lista de subgrupos
            //-------------------------------------------------------------------
            $("#tbEmpresaSubGrupo").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                //height: 160,
                //width: 320,
                colNames: ["Código Subgrupo", "Código Grupo", "Nome", "Descrição"],
                colModel: [
                    { name: "CodigoEmpresaSubGrupo", index: "CodigoEmpresaSubGrupo", hidden: true, key : true },
                    { name: "CodigoEmpresaGrupo", index: "CodigoEmpresaGrupo", hidden: true, key: true },
                    { name: "NomeEmpresaSubGrupo", index: "NomeEmpresaSubGrupo", key: true},
                    { name: "DescricaoEmpresaSubGrupo", index: "DescricaoEmpresaSubGrupo",key: true }
                ],
                ondblClickRow: function(rowid, iRow, iCol, e) {

                    // Pega o código do subgrupo
                    var codigoEmpresaSubGrupo = $("#tbEmpresaSubGrupo").getGridParam("selrow");

                    if (codigoEmpresaSubGrupo) {

                        // Abre dialog de detalhe do subgrupo
                        $.subgrupo.detalhe.abrir(codigoEmpresaSubGrupo);
                    }

                    else {

                        // Pede para selecionar a linha
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Favor selecionar uma linha!",
                            callbackOK: function() { }
                        });
                    }
                }
            });

            //-------------------------------------------------------------------
            // BOTÃO INCLUIR SUBGRUPO
            //-------------------------------------------------------------------
            $("#btnIncluirSubGrupo").click(function() {

                // Abre dialog de detalhe do produto
                $.subgrupo.detalhe.abrir();
            });

            //-------------------------------------------------------------------
            // BOTÃO EDITAR SUBGRUPO
            //-------------------------------------------------------------------
            $("#btnEditarSubGrupo").click(function() {

                // Pega o código do subgrupo
                var codigoEmpresaSubGrupo = $("#tbEmpresaSubGrupo").getGridParam("selrow");

                if (codigoEmpresaSubGrupo) {

                    // Abre dialog de detalhe do subgrupo
                    $.subgrupo.detalhe.abrir(codigoEmpresaSubGrupo);
                }
                else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            //-------------------------------------------------------------------
            // BOTÃO REMOVER SUBGRUPO
            //-------------------------------------------------------------------
            $("#btnRemoverSubGrupo").click(function() {

                // Pega o código do subgrupo
                var codigoEmpresaSubGrupo = $("#tbEmpresaSubGrupo").getGridParam("selrow");

                if (codigoEmpresaSubGrupo) {

                    // pede confirmação de exclusão
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Tem certeza que deseja remover este subgrupo?",
                        callbackSim: function() {

                            // Pede para remover
                            $.subgrupo.detalhe.remover(codigoEmpresaSubGrupo, function() {

                                // Mostra popup de sucesso
                                popup({
                                    titulo: "Remover Subgrupo",
                                    icone: "sucesso",
                                    mensagem: "Subgrupo removido com sucesso!",
                                    callbackOK: function() {
                                        $.subgrupo.lista.listar();
                                    }
                                });
                            });
                        },
                        callbackNao: function() { }
                    });
                }
                else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            //-------------------------------------------------------------------
            // BOTÃO ATUALIZA LISTA SUBGRUPO
            //-------------------------------------------------------------------
            $("#btnAtualizarLista").click(function() {

                // Atualiza lista de subgrupos
                $.subgrupo.lista.listar();
            });

            //-------------------------------------------------------------------
            // PEDE LISTA DE SUBGRUPOS
            //-------------------------------------------------------------------
            $.subgrupo.lista.listar();

            //-------------------------------------------------------------------
            // Trata o redimensionamento da tela
            //-------------------------------------------------------------------
            $(window).bind("resize", function() {
                $("#tbEmpresaSubGrupo").setGridWidth($(window).width() - margemEsquerda);
                $("#tbEmpresaSubGrupo").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");
        },

        //=================================================================================
        // listar: Pede listas de subgrupos ao serviço
        //=================================================================================
        listar: function(callback) {

            // Pede lista de subgrupos
            executarServico(
            "ListarEmpresaSubGrupoRequest",
            {},
            function(data) {

                // Limpa grid
                $("#tbEmpresaSubGrupo").clearGridData();

                for (var i = 0; i < data.Resultado.length; i++) {
                    $("#tbEmpresaSubGrupo").jqGrid("addRowData",
                        data.Resultado[i].CodigoEmpresaSubGrupo, data.Resultado[i]);
                }

                // Chama callback
                if (callback)
                    callback();
            });
        }
    };

    // Pede a inicializacao
    $.subgrupo.lista.load();
});