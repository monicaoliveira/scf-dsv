﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="site_configuracao_subgrupo_detalhe.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_configuracao_subgrupo_detalhe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="site_configuracao_subgrupo_detalhe.js" type="text/javascript"></script>
    <link href="site_configuracao_subgrupo_detalhe.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="site_subgrupo_detalhe">
        <ul>   
            <li class="labelDetalhe1">
                <label>Nome:</label>
            </li>
            
            <li class="campoDetalhe1">
                <input type="text" id="txtNomeEmpresaSubGrupo" class="forms[property[NomeEmpresaSubGrupo];required]" maxlength="20" />
            </li>
        </ul>
        
        <ul> 
            <li class="labelDetalhe1">
                <label>Descrição:</label>
            </li>
            
            <li class="campoDetalhe1">
                <input type="text" id="txtDescricaoEmpresaSubGrupo" class="forms[property[DescricaoEmpresaSubGrupo]]" maxlength="40" />
            </li>
        </ul>
        
        <ul>
            <li class="labelDetalhe1">
                <label>Grupo:</label>
            </li>
            
            <li class="campoDetalhe1">
                <select id="cmbCodigoEmpresaGrupo" class="forms[property[CodigoEmpresaGrupo];required]">
                    <option value="?">(selecione)</option>
                    <asp:Repeater ID="rptCmbCodigoEmpresaGrupo" runat="server">
                        <ItemTemplate>
                            <option value="<%# Eval("CodigoEmpresaGrupo") %>" ><%# Eval("NomeEmpresaGrupo")%></option>
                        </ItemTemplate>
                    </asp:Repeater>
                </select>
            </li>
        </ul>
    </div>
</asp:Content>
