﻿// =====================================================================================
// 1309 Refazendo tela. Ocorria erro de CALLBACK no COMUMSERVICO.JS
// =====================================================================================
$(document).ready(function() 
{   if (!$.referencia)
        $.referencia = {};
    $.referencia.lista = 
    {   variaveis: {}
    //==================================================================================
    // CARREGA GRID Pgm PR_LISTA_ITEM_REFERENCIA_L
    //==================================================================================
    ,   listar: function() 
        {   $("#tbLinkDominio").jqGrid("clearGridData");
            executarServico
            (   "ListarListaItemReferenciaRequest"
            ,   {}
            ,   function(data) 
                {   for (var i = 0; i < data.Resultado.length; i++)
                        $("#tbLinkDominio").jqGrid("addRowData", data.Resultado[i].CodigoListaItemReferencia, data.Resultado[i]);
                }
            );
        }
    //==========================================================================
    // 1309 REMOVER estava na tela de DETALHE
    //==========================================================================
    ,   remover: function(Grid)
        {   if (Grid.CodigoListaItemReferencia)
            {   popup(
                {   titulo: "REMOVER"
                ,   icone: "atencao"
                ,   mensagem:
                "<b>CONFIRMA?</b>" +
                "<br><i><b>" + Grid.Dominio + "</b></i>" +
                "<br><b>DOMINIO</b> [" + Grid.CodigoListaItemSituacao + "] <b>'" + Grid.DominioListaValor + "</b>'" +
                "<br><b>ASSOCIADO COM</b> [" + Grid.NomeReferencia + "] <b>'" + Grid.ValorReferencia + "</b>'"
                ,   callbackSim: function() 
                    {   executarServico
                        (   "RemoverListaItemReferenciaRequest" 
                        ,   { CodigoListaItemReferencia: Grid.CodigoListaItemReferencia }
                        ,   function(data) 
                            {   popup(
                                {   titulo: "REMOVIDO"
                                ,   icone: "info"
                                ,   mensagem:
                                "<b>REMOVIDO COM SUCESSO!</b>" +
                                "<br><br><i><b>" + Grid.Dominio + "</b></i>" +
                                "<br><b>DOMINIO</b> [" + Grid.CodigoListaItemSituacao + "] <b>'" + Grid.DominioListaValor + "</b>'" +
                                "<br><b>ASSOCIADO COM</b> [" + Grid.NomeReferencia + "] <b>'" + Grid.ValorReferencia + "</b>'"
                                ,   callbackOK: function() {$.referencia.lista.listar();}
                                });                            
                            }
                        );
                    }
                ,   callbackNao: function() {}
                });
            } else 
            {   popup(
                {   titulo: "REMOVER"
                ,   icone: "erro"
                ,   mensagem: "Não foi possivel carregar os dados do [" + Grid.CodigoListaItemReferencia + "], Click em Atualizar Tela e tente Novamente"
                ,   callbackOK: function() {}
                });
            }
        }            
    //==========================================================================
    // 1309 CONSULTAR estava na tela de DETALHE
    //==========================================================================
    ,   consultar: function(Grid)
        {   if (Grid.CodigoListaItemReferencia)
            {   popup(
                {   titulo: "CONSULTAR"
                ,   icone: "info"
                ,   mensagem:
                "<i><b>" + Grid.Dominio + "</b></i>" +
                "<br><br><b>DOMINIO</b> [" + Grid.CodigoListaItemSituacao + "] <b>'" + Grid.DominioListaValor + "</b>'" +
                "<br><b>ASSOCIADO COM</b> [" + Grid.NomeReferencia + "] <b>'" + Grid.ValorReferencia + "</b>'"
                ,   callbackOK: function() {}
                });
            } else 
            {   popup(
                {   titulo: "CONSULTA"
                ,   icone: "erro"
                ,   mensagem: "Não foi possivel carregar os dados do " + codigoListaItemReferencia + " Tente Novamente"
                ,   callbackOK: function() {}
                });
            }
        }            
    //==================================================================================
    // GRID PRINCIPAL
    //==================================================================================
    ,   load: function() 
    {       $("#subtitulo").html("CONFIGURACOES [Link de Dominios]");
            $("#site_configuracao_referencia_dominio_lista button").button();
            $("#site_configuracao_referencia_dominio_lista").Forms("initializeFields");
            $("#tbLinkDominio").jqGrid(   
            {   datatype: "clientSide"
            ,   mtype: "GET"
            ,   sortable: true
            ,   scroll: true
            ,   shrinkToFit: false 
            ,   colNames: ["CodigoListaItemReferencia", "Dominio", "Valor Domínio", "Descricao Domínio", "Link Dominio", "Valor Link"]
            ,   colModel: 
                [
                  { name: "CodigoListaItemReferencia"   , index: "CodigoListaItemReferencia", width: 100, hidden: true, key: true }
                , { name: "CodigoListaItemSituacao"     , index: "CodigoListaItemSituacao"  , width: 100 }
                , { name: "DominioListaValor"           , index: "DominioListaValor"        , width: 150 }
                , { name: "Dominio"                     , index: "Dominio"                  , width: 550 }
                , { name: "NomeReferencia"              , index: "NomeReferencia"           , width: 150 }
                , { name: "ValorReferencia"             , index: "ValorReferencia"          , width: 200 }
                ]
            ,   ondblClickRow: function(rowid, iRow, iCol, e) 
                {   var codigoListaItemReferencia = $("#tbLinkDominio").getGridParam("selrow");
                    var Grid = $("#tbLinkDominio").getRowData(codigoListaItemReferencia);
                    if (codigoListaItemReferencia)
                    {   $.referencia.lista.consultar(Grid);
                    }                    
                }
            });

            //==================================================================================
            // BOTAO NOVO
            //==================================================================================
            $("#site_configuracao_referencia_dominio_lista #cmdNovo").click(function() 
            {   $.referencia.detalhe.abrir(null ); 
            });

            //==================================================================================
            // BOTAO CONSULTAR
            //==================================================================================
            $("#site_configuracao_referencia_dominio_lista #cmdConsultar").click(function() 
            {   var codigoListaItemReferencia = $("#tbLinkDominio").getGridParam("selrow");
                var Grid = $("#tbLinkDominio").getRowData(codigoListaItemReferencia);
                if (codigoListaItemReferencia)
                {   $.referencia.lista.consultar(Grid);
                } else
                {   popup(
                    {   titulo: "CONSULTAR"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar uma linha!"
                    ,   callbackOK: function() { }
                    });
                }
            });
            //==================================================================================
            // BOTAO REMOVER PR_LISTA_ITEM_REFERENCIA_B
            //==================================================================================
            $("#site_configuracao_referencia_dominio_lista #cmdRemover").click(function() 
            {   var codigoListaItemReferencia = $("#tbLinkDominio").getGridParam("selrow");
                var Grid = $("#tbLinkDominio").getRowData(codigoListaItemReferencia);
                if (codigoListaItemReferencia)
                {   $.referencia.lista.remover(Grid);
                } else
                {   popup(
                    {   titulo: "REMOVER"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar uma linha!"
                    ,   callbackOK: function() { }
                    });
                }
            });
            
            //==================================================================================
            // BOTAO ATUALIZAR LISTA
            //==================================================================================
            $("#site_configuracao_referencia_dominio_lista #cmdAtualizarLista").click(function() 
            {   $.referencia.lista.listar();
            });

            //==================================================================================
            // pede o carregamento da lista
            //==================================================================================
            $.referencia.lista.listar();

            //==================================================================================
            // Trata o redimensionamento da tela
            //==================================================================================
            $(window).bind("resize", function() 
            {
                $("#tbLinkDominio").setGridWidth($(window).width() - margemEsquerda);
                $("#tbLinkDominio").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");
        }
    };        
    //==================================================================================
    // Pede a inicializacao
    //==================================================================================
    $.referencia.lista.load();
});