﻿<%@ Page Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true"  %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="adm_configuracao_seguranca.js"></script>
    <link href="adm_configuracao_seguranca.css" rel="stylesheet" type="text/css" />
    <title>SCF - Configurações de Segurança </title>
    <style type="text/css">
        .style2
        {
            width: 983px;
        }
        .style3
        {
            width: 1196px;
        }
        .style4
        {
            width: 471px;
        }
        .style5
        {
            width: 361px;
        }
    </style>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    <div id="adm_configuracao_seguranca" class="status">
        <tr>
            <td align="right">
                <label style=" font-size:large">Configurando Dias para Expiração de Senha e Tamanho da Senha</label>
            </td>
        </tr>    
        <table cellspacing="5" cellpadding="3" style="margin: 30px; border: solid 1px #4682B4">
            <tr>
                <td align="right" class="style5">
                    <label>Quantidade dias para expirar senha do operador:</label>
                </td>
                <td class="style2">
                    <input type="text" id="txtQtdDiasExpirarSenha" class="forms[property[DiasExpiracaoSenha]]" style="width:40px;" />
                    <label style=" font-weight:bold">( configura a data de expiração da senha de cada usuário = data hoje + este parametro )</label>
                </td>
                <%-- 1416 MOVENDO 
                <td>
                    <input type="checkbox" id="chkExigirNumerosNaSenha" class="forms[property[ExigirNumerosNaSenha]]" />
                    <label for="chkExigirNumerosNaSenha" id="lblExigirNumerosNaSenha">Exigir números na formação da senha</label>
                </td>
                --%>
            </tr>
            <tr>
                <td align="right" class="style5">
                    <label>Tamanho minimo da senha:</label>
                </td>
                <td class="style2">
                    <input type="text" id="txtTamanhoMinSenha" class="forms[property[TamanhoMinimoSenha]]" style="width:40px;"/>
                    <label style=" font-weight:bold">( configura o tamanho mínimo da senha digitada não permitindo ser menor do que este parametro )</label>
                </td>
                <%-- 1416 MOVENDO 
                <td>
                    <input type="checkbox" id="chkExigirLetrasNaSenha" class="forms[property[ExigirLetrasNaSenha]]" />
                    <label for="chkExigirLetrasNaSenha" id="lblExigirLetrasNaSenha">Exigir letras na formação da senha</label>
                </td>
                --%>
            </tr>
        </table>
        <tr>
            <td align="right">
                <label style=" font-size:large">Configurando Exigências no preenchimento da senha e Validações</label>
            </td>
        </tr>
        <table cellspacing="5" cellpadding="3" style="margin: 30px; border: solid 1px #4682B4">    
            <%-- 1416 MOVENDO - Parametro EXIGIR numeros na senha--%>
            <tr>
                <td align="right" class="style4">
                    <label style=" font-weight:bold">Indica que exigirá NÚMEROS na senha do usuário</label>
                </td>
                <td class="style3">
                    <input type="checkbox" id="Checkbox1" class="forms[property[ExigirNumerosNaSenha]]" />
                    <label for="chkExigirNumerosNaSenha" id="Label1">Exigir números na formação da senha?</label>
                </td>
            </tr>
            <%-- 1416 MOVENDO - Parametro EXIGIR letras na letra--%>
            <tr>
                <td align="right" class="style4">
                    <label style=" font-weight:bold">Indica que exigirá LETRAS na senha do usuário</label>
                </td>
                <td class="style3">
                    <input type="checkbox" id="Checkbox2" class="forms[property[ExigirLetrasNaSenha]]" />
                    <label for="chkExigirLetrasNaSenha" id="Label2">Exigir letras na formação da senha?</label>
                </td>
            </tr>
            <%-- 1416 DISPONIBILIZANDO - Parametro VALIDAR SENHA --%>
            <tr>
                <td align="right" class="style4">
                    <label style=" font-weight:bold">Indica que no LOGIN exigirá a digitação da SENHA do usuário</label>
                </td>
                <td class="style3">
                    <input type="checkbox" id="Checkbox3" class="forms[property[ValidarSenhas]]" />
                    <label for="chkValidarSenhas" id="Label3">Efetuar validação senha?</label>
                </td>
            </tr>
            <%-- 1416 DISPONIBILIZANDO - Parametro VALIDAR SENHA EXPIRADA --%>
            <tr>
                <td align="right" class="style4">
                    <label style=" font-weight:bold">Indica que no LOGIN irá conferir se o usuário está com senha expirada</label>
                </td>
                <td class="style3">
                    <input type="checkbox" id="Checkbox4" class="forms[property[ValidarSenhaExpirada]]" />
                    <label for="chkValidarSenhaExpirada" id="Label4">Efetuar validação senha expirada?</label>
                </td>
            </tr>
            <%-- 1416 INIBINDO - Não tem instrução para campo BLOQUEAR ULTIMAS SENHAS flag
            <tr>
                <td align="right">
                    <label>Qtd de tentativas p/ bloquear operador:</label>
                </td>
                <td>
                    <input type="text" id="txtQtdTentativaBloqOper" class="forms[property[QuantidadeTentativasBloqueio]]" style="width:40px;"/>
                </td>
            </tr>
            --%>
            <%-- 1416 INIBINDO - Não tem instrução para campo BLOQUEAR ULTIMAS SENHAS qtd
            <tr>
                <td align="right">
                    <input type="checkbox" id="chkBloqueioUltimasSenhas" class="forms[property[BloqueioUltimasSenhas]]" />
                    <label for="chkBloqueioUltimasSenhas" id="lblBloqueioUltimasSenhas">Bloquear ultimas senhas:</label>
                </td>
                <td>
                    <input type="text" id="txtBloqueioSenhas" class="forms[property[QuantidadeBloqueioUltimasSenhas]]" style="width:40px;" />
                </td>
            </tr>
            --%>
        </table>
    </div>

    <div id="adm_configuracao_seguranca_btn" style="margin: 30px;>   
        <table cellspacing="5" cellpadding="3">
            <tr>
                <td colspan="3" class="style1">
                    <%-- 1416 INIBINDO - Não tem instrução para este botão 
                    <button id="cmdVoltar">Voltar</button> 
                    --%>
                    <button id="cmdSalvar">Salvar</button>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
