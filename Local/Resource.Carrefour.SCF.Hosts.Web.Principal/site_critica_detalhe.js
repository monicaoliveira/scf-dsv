﻿$(document).ready(function() {

    // Define o namespace critica
    if (!$.critica)
        $.critica = {};

    // Define as funcoes do namespace critica.detalhe
    $.critica.detalhe = {

        // ----------------------------------------------------------------------
        // load: garante que o HTML da esteja carregado e inicializado
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_critica_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_critica_detalhe.aspx #site_critica_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_critica_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.critica.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();

                    });

                });

            } else {
                // Chama o callback
                if (callback)
                    callback();
            }

        },

        // ----------------------------------------------------------------------
        // Complementa a rotina de inicializacao especificamente na inicialização
        //   dos componentes da tela
        //
        //  callback:       função a ser chamada ao final da execução
        // ----------------------------------------------------------------------
        load2: function(callback) {

            // Tabs
            $("#site_critica_detalhe").tabs();
            
            // Inicializa o dialog
            $("#site_critica_detalhe").dialog({
                autoOpen: false,
                height: 320,
                width: 460,
                modal: true,
                title: "Detalhe de critica",
                buttons: {
                    "Fechar": function() {
                        // Fecha o dialog
                        $("#site_critica_detalhe").dialog("close");
                    }
                }
            });

            // Se tem callback, executa
            if (callback)
                callback();

        },

        // ----------------------------------------------------------------------
        // Abre o dialog de detalhe da critica carregando a critica informado,
        //   ou permitindo um novo cadastro (caso não seja informado um código existente)
        //
        //  codigoCritica:      código da critica a carregar. caso seja nulo,
        //                      abre a tela para inclusão de uma nova critica
        //  callback:           função a ser chamada ao final da execução
        // ----------------------------------------------------------------------
        abrir: function(codigoCritica, callback) {

            // Garante que o html está carregado
            $.critica.detalhe.load(function() {

                // Carrega informações do critica
                $.critica.detalhe.carregarDados(codigoCritica, function() {

                    // Carrega a primeira aba
                    $("#site_critica_detalhe").tabs("select", 0);

                    // Mostra o dialog
                    $("#site_critica_detalhe").dialog("open");

                    // Chama o callback
                    if (callback)
                        callback();

                });

            });

        },

        // ----------------------------------------------------------------------
        // Carrega as informações da tela
        //
        //  callback:       Função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        carregarDados: function(codigoCritica, callback) {

            // Pede informações do critica
            executarServico(
                "ReceberCriticaRequest",
                { CodigoCritica: codigoCritica },
                function(data) {

                    // Preenche detalhe
                    $("#site_critica_detalhe").Forms("setData", { dataObject: data.CriticaInfo });

                    // Monta a lista de propriedades retornadas no critica
                    var propriedades = "";
                    for (var propriedade in data.CriticaInfo) 
                        propriedades +=
                            "<label class='tituloCriticaPropriedade'>" + propriedade + 
                            "</label><label style='width: 250px;'>" + data.CriticaInfo[propriedade] + 
                            "</label><br /><br />";
                    $("#criticaAbaPropriedades").html(propriedades);

                    // Chama o callback
                    if (callback)
                        callback();

                });

        }

    }

});



