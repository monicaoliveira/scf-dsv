<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="site_configuracao_favorecido_detalhe.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_configuracao_favorecido_detalhe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_configuracao_favorecido_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_configuracao_favorecido_detalhe.js" type="text/javascript"></script>
    <script src="site_configuracao_conta_favorecido_detalhe.js" type="text/javascript"></script>    
    <title>SCF - Cadastro de Favorecidos</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="site_configuracao_favorecido_detalhe" style="margin: 30px;">
<!--================================================================================================================================================ --> 
<!-- INICIO - Favorecido Cadastro --> 
<!--================================================================================================================================================ --> 
            <table width="100%">
	            <tr>
		            <td align=right>
			            <label style="font-weight:bold">CNPJ: </label>
		            </td>
				    <td>
					    <input disabled="true" type="text" id="txtCnpjFavorecido" class="forms[property[CnpjFavorecido]; required]" maxlength="15" />
				    </td>  
		            <td align=left> <!-- 1403 alterou, estava <td align=right> -->
			            <label style="font-weight:bold">Favorecido Original? (S/N)</label>
					    <input type="checkbox" id="chkFavorecidoOriginal" class="forms[property[FavorecidoOriginal]]" />
				    </td>                                       
			    </tr>
			    <tr>
		            <td align=right>
			            <label style="font-weight:bold">Razao Social: </label><br />
		            </td>
				    <td colspan="2">
					    <input type="text" style="width:494px;" id="txtNomeFavorecido" class="forms[property[NomeFavorecido];required]" maxlength="40" />
				    </td>                                        
			    </tr>
			    <tr>
				    <td colspan="3">
					    <label id="avisoSalvar" style="color:red;">*Salve o favorecido para associar contas!</label>
				    </td>
			    </tr>
	        </table> <br />
<!--================================================================================================================================================ --> 
<!-- FIM - Favorecido Cadastro --> 
<!--================================================================================================================================================ --> 

<!--================================================================================================================================================ --> 
<!-- INICIO - Abas do configuracao geral -->
<!--================================================================================================================================================ --> 
        <div id="configuracao_favorecido_detalhe_tabs">
            <ul>
                <!-- Abas -->  
                <li><a href="#tabCadContas"><b>Cadastro Conta</b></a></li>
                <li><a href="#tabAssContas"><b>Conta x Produto x Referencia</b></a></li>
            </ul>
<!--================================================================================================================================================ --> 
<!-- INICIO - aba CADASTRO CONTAS --> 
<!--================================================================================================================================================ --> 
            <div id="tabCadContas">
<!--================================================================================================================================================ --> 
<!-- INICIO - Favorecido Conta Cadastro --> 
<!--================================================================================================================================================ --> 
                <table width="50%"> <!-- 1403 alterou, estava 100% - -->
		            <tr>
			            <td>
				            <label style="font-weight:bold">Lista de Contas do Favorecido</label><br />
				            <table id="tbContasFavorecido"></table>
			            </td>
						<td colspan="2" style="vertical-align:top">						
						<br />
								<button class="botoes" style="width:100px" id="cmdNovaConta">Novo</button><br />
								<button class="botoes" style="width:100px" id="cmdEditarConta">Editar</button><br />
								<button class="botoes" style="width:100px" id="cmdRemoverConta">Remover</button><br />
						</td>
		            </tr>
				</table>
<!--================================================================================================================================================ --> 
<!-- FIM - Favorecido Conta Cadastro --> 
<!--================================================================================================================================================ --> 
			</div>               
<!--================================================================================================================================================ --> 
<!-- INICIO - aba CADASTRO CONTAS --> 
<!--================================================================================================================================================ --> 

<!--================================================================================================================================================ --> 
<!-- INICIO - aba CONTA X PRODUTO X REFERENCIA --> 
<!--================================================================================================================================================ --> 
            <div id="tabAssContas">
<!--================================================================================================================================================ --> 
<!-- Lista de contas --> 
<!--================================================================================================================================================ --> 
				<table id="gridContas" width="100%">
		            <tr>
			            <td>
				            <label style="font-weight:bold">Selecione uma Conta do Favorecido</label><br />
				            <table id="tbContasFavorecido2"></table>
			            </td>
<!--================================================================================================================================================ --> 
<!-- Lista de produtos --> 
<!--================================================================================================================================================ --> 
			            <td>
				            <label style="font-weight:bold">Selecione um Produto</label><br />
				            <table id="tbProdutosSCF"></table>
			            </td>
<!--================================================================================================================================================ --> 
<!-- Lista de referencia de cartao --> 
<!--================================================================================================================================================ --> 
			            <td>
				            <label style="font-weight:bold">Selecione Referencia</label><br /> <!-- 1403 alterou, estava Selecione um Referencia de Cartao-->
				            <table id="tbReferenciasSCF"></table>
			            </td>
			            <td style="vertical-align:top">
			                <br><button class="botoes" style="width:100px" id="btnAsssociar">Associar</button><br/>
			            </td>
		            </tr>
<!--================================================================================================================================================ --> 
<!-- Associações existentes --> 
<!--================================================================================================================================================ --> 
		            <tr>
			            <td colspan="3"><br />
				            <label style="font-weight:bold">Conta x Produto x Referencia</label><br />
				            <table id="tbAssociacaoFavorecido"></table>
			            </td>
			            <td style="vertical-align:top">
			                <br><button class="botoes" style="width:100px" id="btnDesassociar">Desassociar</button><br />                                                
			            </td>
		            </tr>
				</table>
<!--================================================================================================================================================ --> 
<!-- FIM - aba CONTA X PRODUTO X REFERENCIA --> 
<!--================================================================================================================================================ --> 
			</div>  
        </div>  			
    </div>
<!--================================================================================================================================================ --> 
<!-- FIM - Abas do configuracao geral -->
<!--================================================================================================================================================ --> 
</asp:Content>
