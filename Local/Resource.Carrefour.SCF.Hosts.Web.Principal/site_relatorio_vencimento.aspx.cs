﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using System.Drawing;
using Resource.Framework.Contratos.Comum;
using System.Text;
using System.IO;
using System.Data;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using System.Text.RegularExpressions;
using Resource.Framework.Library.Servicos.Mensagens;


namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class site_relatorio_vencimento : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request["acao"] == null && !IsPostBack)
            {

                //-------------------------------------------------------------------------------------------
                // CARREGA COMBO DE GRUPOS (EMPRESA)
                //-------------------------------------------------------------------------------------------
                MensagemResponseBase retorno = null;
                try
                {
                    if (Session["CodigoSessao"] != null)
                    {
                        // Recebe lista de grupos
                        ListarEmpresaGrupoResponse grupos =
                            Mensageria.Processar<ListarEmpresaGrupoResponse>(
                                new ListarEmpresaGrupoRequest()
                                {
                                    CodigoSessao = Session["CodigoSessao"].ToString()
                                });

                        // Preenche combo
                        rptCmbEmpresaGrupo.DataSource = grupos.Resultado;
                        rptCmbEmpresaGrupo.DataBind();

                        //-------------------------------------------------------------------------------------------
                        // CARREGA COMBO DE FAVORECIDOS
                        //-------------------------------------------------------------------------------------------

                        // Recebe lista de bancos
                        ListarFavorecidoResponse favorecidos =
                            Mensageria.Processar<ListarFavorecidoResponse>(
                                new ListarFavorecidoRequest()
                                {
                                    CodigoSessao = Session["CodigoSessao"].ToString()
                                });

                        // Preenche combo
                        rptCmbFavorecido.DataSource = favorecidos.Resultado;
                        rptCmbFavorecido.DataBind();
                    }
                    else
                    {
                        retorno = new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                            DescricaoResposta = "Sessão Inválida"
                        };
                    }
                }
                catch (Exception ex)
                {
                    // Cria mensagem generica informando o erro
                    retorno =
                        new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                            DescricaoResposta = ex.ToString()
                        };
                }
                //-------------------------------------------------------------------------------------------
                // CARREGA COMBO DE PRODUTOS
                //-------------------------------------------------------------------------------------------
                
                //// Recebe lista de produtos
                //ListarProdutoResponse produtos = 
                //    Mensageria.Processar<ListarProdutoResponse>(
                //        new ListarProdutoRequest()
                //        {
                //            CodigoSessao = Session["CodigoSessao"].ToString()
                //        });

                //// Preenche combo
                //rptCmbProduto.DataSource = produtos.Resultado;
                //rptCmbProduto.DataBind();

            }
            else if (Request["acao"] == "xls") 
            {

                //GerarRelatorioVencimentoExcelResponse resposta =
                //    Mensageria.Processar<GerarRelatorioVencimentoExcelResponse>(
                //        new GerarRelatorioVencimentoExcelRequest()
                //        {
                //            Request = new ListarRelatorioVencimentoRequest()
                //            {
                //                CodigoSessao = (string)this.Session["CodigoSessao"],
                //                FiltroBanco = this.Request["CodigoFavorecido"],
                //                FiltroFavorecido = this.Request["CodigoFavorecido"],
                //                FiltroEmpresaGrupo = this.Request["CodigoEmpresaGrupo"],
                //                FiltroProduto = this.Request["CodigoProduto"],
                //                FiltroDataInicioTransacao = this.Request["DataInicioTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioTransacao"]) : null,
                //                FiltroDataFimTransacao = this.Request["DataFimTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimTransacao"]) : null,
                //                FiltroDataInicioVencimento = this.Request["DataInicioVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioVencimento"]) : null,
                //                FiltroDataFimVencimento = this.Request["DataFimVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimVencimento"]) : null
                //            }
                //        });
                
            }
            else if (Request["acao"] == "pdf")
            {
                string urlToConvert = GetUrlRelatorio();
                string downloadName = "RelatorioVencimento";
                byte[] downloadBytes = null;

                downloadName += ".pdf";
                downloadBytes = PdfHelper.GetDownloadBytes(urlToConvert, downloadName);

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition",
                    "attachment; filename=" + downloadName + "; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();

            }
        }


        protected string GetUrlRelatorio()
        {
            /* **************************************
             * PARA TESTAR O PLUGIN GERADOR DE PDF 
             * É NECESSÁRIO ALTERAR O VALOR DA PORTA 
             * DE ACORDO COM A SUA MÁQUINA
             * **************************************/

            string url = "http://localhost:55143/relatorio_vencimento.aspx";

            string parametros = "";
            parametros += this.Request["CodigoFavorecido"] != "?" ? this.Request["CodigoFavorecido"] + "&" : null;
            parametros += this.Request["CodigoEmpresaGrupo"] != "?" ? this.Request["CodigoEmpresaGrupo"] + "&" : null;
            parametros += this.Request["CodigoProduto"] != "?" ? this.Request["CodigoProduto"] + "&" : null;
            parametros += this.Request["DataInicioTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioTransacao"]) : null;
            parametros += this.Request["DataFimTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimTransacao"]) : null;
            parametros += this.Request["DataInicioVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioVencimento"]) : null;
            parametros += this.Request["DataFimVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimVencimento"]) : null;
            
            if (parametros != "")
                url += "?" + parametros.Substring(0, parametros.Length - 1);

            return url;
        }
    }
}
