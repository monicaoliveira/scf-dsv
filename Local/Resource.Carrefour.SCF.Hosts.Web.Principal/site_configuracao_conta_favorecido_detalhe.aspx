﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="site_configuracao_conta_favorecido_detalhe.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_configuracao_conta_favorecido_detalhe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_configuracao_conta_favorecido_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_configuracao_conta_favorecido_detalhe.js" type="text/javascript"></script>    
    <title>SCF - Cadastro de Conta do favorecido</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
     <div id="site_configuracao_conta_favorecido_detalhe" style="margin: 15px;">         
        
        <table>
            <tr>
                <td style="width: 80px; padding-left: 5px;">
                    <label id="lblBanco">Banco:</label>
                </td>            
                <td>
                    <input type="text" id="txtBanco" class="forms[property[Banco];required]" style="width: 70px;" maxlength="3"/>
                </td>
             </tr>
             <tr>   
                <td style="width: 70px; padding-left: 5px;">
                    <label id="Label1">Agência DE:</label> <%-- 1384--%>
                </td>
                <td>
                    <input type="text" id="txtAgenciaCCI" class="forms[property[Agencia_CCI];required]" style="width: 70px;" maxlength="6"/>
                </td>
              </tr>
             <tr>
                <td style="width: 75px; padding-left: 5px;">
                    <label id="Label2">Conta DE:</label> <%-- 1384--%>
                </td>
                <td>
                    <input type="text" id="txtContaCCI" 
                        class="forms[property[Conta_CCI];required]" style="width: 108px;" 
                        maxlength="11"/>
                </td>
            </tr>
             <tr>   
                <td style="width: 70px; padding-left: 5px;">
                    <label id="lblAgencia">Agência PARA:</label> <%-- 1384--%>
                </td>
                <td>
                    <input type="text" id="txtAgencia" class="forms[property[Agencia];required]" style="width: 70px;" maxlength="6"/>
                </td>
              </tr>
             <tr>
                <td style="width: 75px; padding-left: 5px;">
                    <label id="lblConta">Conta PARA:</label> <%-- 1384--%>
                </td>
                <td>
                    <input type="text" id="txtConta" 
                        class="forms[property[Conta];required]" style="width: 108px;" 
                        maxlength="11"/>
                </td>
            </tr>
            <tr><%-- SCF1701 - ID 27--%>
            <td><br /></td>
            </tr>
            <tr><%-- SCF1701 - ID 27--%>
                <td colspan="2" id="associar" title="Selecione caso deseje que as associações e transações de outra conta sejam substituidas por esta">
                    <input type="checkbox" id="chkAssociar" /><label id="lblAssociar" style="font-weight:bold">Substituir as associações de outra conta?</label>
                </td>
            </tr>   
            <tr><%-- SCF1701 - ID 27--%>
                <td colspan="2">
                    <select id="cmbConta" >
                        <option value="?" selected>Selecione</option>
                    </select>
                </td>
            </tr>
        </table>        
    </div>
</asp:Content>
