﻿<%--
//====================================================================================================================
// SCF963 - TELA CADASTRO DE PROTOCOLO DE CHEQUES
//====================================================================================================================
--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" CodeBehind="site_configuracao_cadastro_protocolo_cheque_devolvido.aspx.cs" AutoEventWireup="true" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_configuracao_cadastro_protocolo_cheque_devolvido" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_cadastro_protocolo_cheque_devolvido.js"></script>
    <link href="site_configuracao_cadastro_protocolo_cheque_devolvido.css" rel="stylesheet" type="text/css" />
    <title>SCF - Cheque Devolvido (site_configuracao_cadastro_protocolo_cheque_devolvido)</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<%--
//====================================================================================================================
// DIV site_configuracao_cadastro_protocolo_cheque_devolvido 
//====================================================================================================================
--%>
    <div id="site_configuracao_cadastro_protocolo_cheque_devolvido">
<%--
//====================================================================================================================
// TABLE
//====================================================================================================================
--%>
    <table>
<%--
//====================================================================================================================
//PRIMEIRA LINHA
//====================================================================================================================
--%>                
        <tr>
            <td>
                <label>Tipo de registro:</label>
            </td>
            <td>
                <input 
                    type        ="text" 
                    maxlength   =12 
                    size        =10 
                    value       =" AP"  
                    style       ="text-transform:uppercase" 
                    onkeypress  ="return SoLetras(event)" 
                    id          ="cmbTipoRegistro" 
                    class       ="forms[property[FiltroTipoRegistro]]" 
                    readonly
                    onclick     ="return cmbTipoRegistro_onclick()" />
            </td>
            <td class="label_detalhe1">
                <label>Meio Pagamento:</label>
            </td>                    
            <td>
                <input 
                    style="
                        border-color: #B5B5B5;
                        top: 0px; 
                        width: 140px";
                    type        ="text"  
                    value       =" 4–Devolução Cheque" 
                    id          ="cmbMeioPagamento" 
                    class       ="forms[property[FiltroMeioPagamento]]" 
                    readonly/>
            </td>
            <td class="label_detalhe2">
                <label>Data Estorno</label>
            </td>
            <td class="label_detalhe3">
                <label>De:</label>
            </td>
            <td class="label_detalhe3">
                <input 
                    size    =8 
                    type    ="text" 
                    id      ="cmbDtEstornoDe" 
                    class   ="forms[property[FiltroDataEstornoDe];dataType[date];maskType[date]]"
                />
            </td>
            <td class="label_detalhe3">
                <label>Até:</label>
            </td>
            <td class="label_detalhe3">
                <input 
                    size    =8 
                    type    ="text" 
                    id      ="cmbDtEstornoAte" 
                    class   ="forms[property[FiltroDataEstornoAte];dataType[date];maskType[date]]"
                />
            </td>
            <td class="label_detalhe3">
                <label>Status Envio CCI:</label>
            </td>
            <td>
                 <select 
                    style=  
                            "width:125px; 
                            border-color: #B5B5B5" 
                    id="cmbFiltroEnvioCci" 
                    class="forms[property[FiltroEnvioCci]]">
                    <option value="?">Todos</option>
                    <option value="1">Enviadas</option>
                    <option value="9">Não Enviadas</option>
                </select>
            </td>
        </tr>
<%--
//====================================================================================================================
//SEGUNDA LINHA
//====================================================================================================================
--%>                
        <tr>
            <td>
                <label>No Protocolo:</label>
            </td>
            <td class="label_detalhe6">
                <input 
                    style       ="border-color: #B5B5B5" 
                    type        ="text"  
                    maxlength   =14 
                    size        =10 
                    value       ="" 
                    onkeypress  ="return SomenteNumero(event)" 
                    id          ="cmbNumeroProtocolo" 
                    class       ="forms[property[FiltroNumeroProtocolo]]"
                />
            </td>
            <td class="label_detalhe1">
                 <label>Estabelecimento:</label>  
            </td>
            <td class="label_detalhe5">
                <select 
                    style=
                        "border-color: #B5B5B5; 
                        top: 0px; 
                        width: 140px;"; 
                    type        ="text"  
                    id          ="cmbEstabelecimento"                             
                    class       ="forms[property[FiltroEstabelecimento]]">
                    
                <option value="?">Todos</option>
                <asp:Repeater 
                    runat       ="server" 
                    ID          ="rptCmbEstabelecimento">
                    <ItemTemplate>
                            <option value="<%# Eval("CodigoEstabelecimento") %>"><%# Eval("RazaoSocial")%></option>
                    </ItemTemplate>
                </asp:Repeater>
                </select>  
            </td>
            <td class="label_detalhe2">
                <label>Data Registro Protocolo SCF</label>
            </td>
            <td class="label_detalhe3">
                <label>De:</label>
            </td>
            <td class="label_detalhe3">
                <input size=8 type="text" id="txtDtRegProtocoloDe" class="forms[property[FiltroDataRegProtocoloDe];dataType[date];maskType[date]]"/>
            </td>
            <td class="label_detalhe3">
                <label>Até:</label>
            </td>
            <td class="label_detalhe3">
                <input size=8 type="text" id="txtDtRegProtocoloAte" class="forms[property[FiltroDataRegProtocoloAte];dataType[date];maskType[date]]"/>
            </td>   
            
            <td class="label_detalhe3">
                <label>Status Protocolo:</label>
            </td>
            <td>
                 <select style="width:125px; border-color: #B5B5B5;" id="cmbFiltroStatusProtocolo" class="forms[property[FiltroStatusProtocolo]]">
                    <option value="?">Todos</option>
                    <option value="S">Concluídos</option>
                    <option value="N">Pendentes</option>
                </select>
            </td>                                     
        </tr>
<%--
//====================================================================================================================
//TERCEIRA LINHA
//====================================================================================================================
--%>                
        <tr>
            <td>
                <label>Conta Cliente:</label>
            </td>
            <td class="label_detalhe6">
                <input type="text"  maxlength=20 size=10 value='' onkeypress='return SomenteNumero(event)' id="cmbContaCliente" class="forms[property[FiltroContaCliente]]" />
            </td>
            <td class="label_detalhe1">
                <label>Tipo de Estorno:</label>
            </td>
            <td class="label_detalhe1">
                <select 
                    style=
                        "border-color: #B5B5B5; 
                        top: 0px; 
                        width: 140px;"; 
                    type        ="text"  
                    id          ="cmbFiltroTipoEstorno"
                    class       ="forms[property[FiltroTipoEstorno]]">
                <option 
                    value="?">Todos
                </option>
                <asp:Repeater 
                    runat="server" 
                    ID="rptCmbTipoEstorno">
                    <ItemTemplate>
                        <option value="<%# Eval("Valor") %>"><%# Eval("Descricao")%></option>
                    </ItemTemplate>
                </asp:Repeater>
                </select>  
            </td>                    
            
            <td class="label_detalhe2">
                <label>Data Processamento</label>
            </td>
            <td class="label_detalhe3">
                <label>De:</label>
            </td>
            <td class="label_detalhe3">
                <input size=8 type="text" id="txtDataProcDe" class="forms[property[FiltroDataProcessamentoDe];dataType[date];maskType[date]]"/>
            </td>
            <td class="label_detalhe3">
                <label>Até:</label>
            </td>
            <td class="label_detalhe3">
                <input size=8 type="text" id="txtDataProcAte" class="forms[property[FiltroDataProcessamentoAte];dataType[date];maskType[date]]"/>
            </td>   
            
            <td colspan=2 class="cmdFiltrar" style="text-align:right"> 
                <button >Filtrar</button>
            </td>                                      
                                
        </tr>
    </table>
<%--
//====================================================================================================================
//GRID
//====================================================================================================================
--%>                
        <div class="lista">
            <table id="tbProtocolo"></table>
        </div>  
<%--
//====================================================================================================================
//IMPRIMIR
//====================================================================================================================
--%>                
        <div style="float: left; text-align: left">
            <button class="cmdVisualizar" >Visualizar</button> <!--1465-->
        </div>
        <div style="text-align: right">
            <button class="cmdGerarExcel" >Gerar Excel</button> <!--1465-->
            <button class="cmdGerarPDF" >Gerar PDF</button> <!--1465-->
        </div>       
    </div>       
<%--
//====================================================================================================================
//TRATANDO SOMENTE NUMEROS E LETRAS
//====================================================================================================================
--%>                
    <script language='JavaScript'>
        function SomenteNumero(e) 
        {
            var tecla = (window.event) ? event.keyCode : e.which;
            if ((tecla >= 48 && tecla <= 57)) return true;
            else 
            {
                if (tecla == 8 || tecla == 0) return true;
                else return false;
            }
        }
        function SoLetras(obj) 
        {
            tecla = event.keyCode;
            if (tecla >= 33 && tecla <= 64 || tecla >= 91 && tecla <= 93 || tecla >= 123 && tecla <= 159 || tecla >= 162 && tecla <= 191) {
                return false;
            } else 
            {
                return true;
            }
        }
    </script>
</asp:Content>