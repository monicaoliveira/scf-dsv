var template_default_site_login_carregado = false;
var template_default_permissoes_carregarCallback = null;
var template_default_permissoes_carregadas = null;

$(document).ready(function() {
    // Manter o titulo da aplicação sempre
    setTimeout(function(self) {
        $("title").text("Carrefour Soluções Financeiras");
    }, 300, this);

    // Inicializa popup de validação
    $("#popupValidacao").dialog({
        autoOpen: false,
        resizable: true,
        width: 470,
        height: 290,
        modal: true,
        title: "Validacao",
        buttons: {
            "Sair": function() {
                $(this).dialog("close");
                hideLoadingDialog();
                return false;
            }
        },
        open: function(event, ui) {
            hideVisibleComboboxes(document, this);
        },
        close: function(event, ui) {
            showHiddenComboboxes(document, this);
        }
    });


    // Inicializa lista de validação
    $("#popupValidacao_lista")
	    .jqGrid({
	        datatype: "clientSide",
	        mtype: "GET",
	        colNames: ["Aba", "Erro"],
	        colModel: [
              { name: "aba", index: 0, width: 200, hidden: true },
              { name: "erro", index: 1, width: 500 }
            ],
	        sortname: "aba",
	        height: 200,
	        width: 450,
	        sortorder: "asc",
	        viewrecords: true,
	        caption: ""
	    });

    // Inicializa popup de carregamento
    $("#loadingDialog").dialog({
        autoOpen: false,
        resizable: false,
        width: 160,
        height: 80,
        modal: true,
        title: "Carregando...",
        open: function(event, ui) {
            //hideVisibleComboboxes(document, this);
        },
        close: function(event, ui) {
            //showHiddenComboboxes(document, this);
        }
    });

});


function popup(_opcoes) {

    var opcoes = {};

    $.extend(opcoes, popupOpcoesPadrao(), _opcoes);

    $("#popupSimNao_mensagem").html(opcoes.mensagem);

    var opcoesDialog = {
        autoOpen: true,
        modal: true,
        closeOnEscape: false,
        resizable: opcoes.resizable,
        width: opcoes.width,
        height: opcoes.height,
        title: opcoes.titulo,
        buttons: {},
        open: function(event, ui) {
            //			hideVisibleComboboxes(document, this);
        },
        close: function(event, ui) {
            //			showHiddenComboboxes(document, this);
        },
        focus: function(event, ui) {
            //			hideLoadingDialog();
        }
    };

    if (opcoes.icone == "ajuda" || opcoes.icone == "atencao" || opcoes.icone == "erro"
				|| opcoes.icone == "info" || opcoes.icone == "sucesso")
        $("#popupSimNao img.icone")
			.attr("src", "img/icone_" + opcoes.icone + ".png")
			.show();
    else
        $("#popupSimNao img").hide();

    if (opcoes.callbackSim)
        opcoesDialog.buttons["Sim"] = function() { popupCallback(opcoes.callbackSim); };
    if (opcoes.callbackNao)
        opcoesDialog.buttons["Não"] = function() { popupCallback(opcoes.callbackNao); };
    if (opcoes.callbackOK)
        opcoesDialog.buttons["OK"] = function() { popupCallback(opcoes.callbackOK); };
    if (opcoes.callbackCancelar)
        opcoesDialog.buttons["Cancel"] = function() { popupCallback(opcoes.callbackCancelar); };

    $("#popupSimNao").dialog(opcoesDialog);

}

function popupOpcoesPadrao() {
    return {
        resizable: false,
        width: 400,
        height: 150,
        icone: "nenhum"
    };
}

function popupCallback(callback) {
    $("#popupSimNao").dialog("close");
    callback();
}

/* --------------------------------------------------------------------------------------------------
Funcões para manipulação de elementos de enumeradores
-------------------------------------------------------------------------------------------------- */

// Solicita a carga dos enumeradores informados
function carregarListas(listas, callback) {

    // Monta lista de enumeradores
    var enumeradores = null;
    if (typeof (listas) == "object") {
        enumeradores = listas;
    } else {
        enumeradores = [listas];
    }

    // Pega eventuais listas carregadas
    var listas = $("body").data("listas");

    // Se tem listas carregadas...
    if (listas) {
        // Varre as listas já carregadas retirando do array se existirem
        for (var lista in listas) {
            // Varre o array verificando se existe
            for (var j = 0; j < enumeradores.length; j++) {
                if (enumeradores[j] == lista) {
                    enumeradores.splice(j, 1);
                    break;
                }
            }
        }

    }

    // Continua apenas se tem listas a carregar
    if (enumeradores.length > 0) {

        // Guarda lista dos enumeradores que estão sendo solicitados
        $("body").data("listasEnumeradores", enumeradores);

        // Guarda o callback
        $("body").data("carregarListasCallback", callback);

        // Pede a lista dos enumeradores informados
        executarServico(
            "GerarEnumMetadadoRequest",
            { EnumeradoresString: enumeradores },
            "carregarListasCallback");

    } else {

        // Chama o callback
        if (callback)
            callback();

    }

}

function carregarListasCallback(data) {

    // Pega eventual array de listas existente
    var listas = $("body").data("listas");
    if (!listas)
        listas = new Object();

    // Varre as listas retornadas incluindo no objeto
    for (var i = 0; i < data.Listas.length; i++) {
        listas[data.Listas[i].Mnemonico] = data.Listas[i];
    }

    // Salva o objeto
    $("body").data("listas", listas);

    // Se houver, chama o callback
    if ($("body").data("carregarListasCallback"))
        $("body").data("carregarListasCallback")();

}

// Solicita a carga das listas informadas
function carregarListas2(listas, callback) {

    // Monta lista de enumeradores
    var listas2 = null;
    if (typeof (listas) == "object") {
        listas2 = listas;
    } else {
        listas2 = [listas];
    }

    // Pega eventuais listas carregadas
    var listas = $("body").data("listas");

    // Se tem listas carregadas...
    if (listas) {
        // Varre as listas já carregadas retirando do array se existirem
        for (var lista in listas) {
            // Varre o array verificando se existe
            for (var j = 0; j < listas2.length; j++) {
                if (listas2[j] == lista) {
                    listas2.splice(j, 1);
                    break;
                }
            }
        }

    }

    // Continua apenas se tem listas a carregar
    if (listas2.length > 0) {

        // Guarda lista dos enumeradores que estão sendo solicitados
        $("body").data("listas2Listas", listas2);

        // Guarda o callback
        $("body").data("carregarListas2Callback2", callback);

        // Pede a lista dos enumeradores informados
        executarServico(
            "ReceberListasRequest",
            { MnemonicosLista: listas2 },
            "carregarListas2Callback");

    } else {

        // Chama o callback
        if (callback)
            callback();

    }

}

function carregarListas2Callback(data) {

    // Pega eventual array de listas existente
    var listas = $("body").data("listas");
    if (!listas)
        listas = new Object();

    // Varre as listas retornadas incluindo no objeto
    for (var i = 0; i < data.Listas.length; i++) {
        if (data.Listas[i])
            listas[data.Listas[i].Mnemonico] = data.Listas[i];
    }

    // Salva o objeto
    $("body").data("listas", listas);

    // Se houver, chama o callback
    if ($("body").data("carregarListas2Callback2"))
        $("body").data("carregarListas2Callback2")();

}
function retornarTextoCombo(combo) {
    var textoRetorno = "";
    combo.each(function() {
        var $this = $(this);
        if ($this.length) {
            textoRetorno += ", " + $this.text();
        }
    });
    return textoRetorno.substring(2);
}

function preencherCombo(combo, nomeLista) {

    // Pega a lista
    var lista = $("body").data("listas")[nomeLista];

    // Limpa o combo (exceto o primeiro item)
    combo.children().each(
		function(index) {
		    if (index != 0)
		        $(this).remove();
		}
	);

    // Para cada item, adiciona no combo
		if (lista) {
		    for (var i = 0; i < lista.Itens.length; i++) {

		        // Se tem descricao utiliza, se nao, usa o mnemonico
		        var descricao = lista.Itens[i].Descricao;
		        if (descricao == null)
		            descricao = lista.Itens[i].Mnemonico;

		        // Adiciona o item no bomo
		        combo.append("<option value='" + lista.Itens[i].Mnemonico + "'>"
				+ descricao + "</option>");
		    }
		}
}

function preencherComboComValor(combo, nomeLista) {

    // Pega a lista
    var lista = $("body").data("listas")[nomeLista];

    // Limpa o combo (exceto o primeiro item)
    combo.children().each(
		function(index) {
		    if (index != 0)
		        $(this).remove();
		}
	);

    // Para cada item, adiciona no combo
    if (lista) {
        for (var i = 0; i < lista.Itens.length; i++) {

            // Se tem descricao utiliza, se nao, usa o mnemonico
            var descricao = lista.Itens[i].Descricao;
            if (descricao == null)
                descricao = lista.Itens[i].Mnemonico;

            // Adiciona o item no bomo
            combo.append("<option value='" + lista.Itens[i].Valor + "'>"
				+ descricao + "</option>");
        }
    }
}

function preencherCombo2(combo, nomeLista) {

    // Pega a lista
    var lista = $("body").data("listas")[nomeLista];

    // Limpa o combo (exceto o primeiro item)
    combo.children().each(
		function(index) {
		    if (index != 0)
		        $(this).remove();
		}
	);

    // Para cada item, adiciona no combo
    for (var i = 0; i < lista.Itens.length; i++) {

        // Se tem descricao utiliza, se nao, usa o mnemonico
        var descricao = lista.Itens[i].Descricao;
        if (descricao == null)
            descricao = lista.Itens[i].Mnemonico;

        // Adiciona o item no bomo
        combo.append("<option value='" + lista.Itens[i].CodigoListaItem + "'>"
				+ descricao + "</option>");
    }
}

function retornarGridSelect(nomeLista) {

    // Pega a lista
    var lista = $("body").data("listas")[nomeLista];

    // String de retorno
    var retorno = "";

    // Para cada item, adiciona no combo
    for (var i = 0; i < lista.Itens.length; i++) {

        // Se tem descricao utiliza, se nao, usa o mnemonico
        var descricao = lista.Itens[i].Descricao;
        if (descricao == null)
            descricao = lista.Itens[i].Mnemonico;

        // Adiciona o item
        retorno += lista.Itens[i].Mnemonico + ":" + descricao + ";";

    }

    // Ajusta a string
    if (retorno != "")
        retorno = retorno.substr(0, retorno.length - 1);

    // Retorna
    return retorno;

}

function retornarGridSelect2(nomeLista) {

    // Pega a lista
    var lista = $("body").data("listas")[nomeLista];

    // String de retorno
    var retorno = "";

    // Para cada item, adiciona no combo
    for (var i = 0; i < lista.Itens.length; i++) {

        // Se tem descricao utiliza, se nao, usa o mnemonico
        var descricao = lista.Itens[i].Descricao;
        if (descricao == null)
            descricao = lista.Itens[i].Mnemonico;

        // Adiciona o item
        retorno += lista.Itens[i].CodigoListaItem + ":" + descricao + ";";

    }

    // Ajusta a string
    if (retorno != "")
        retorno = retorno.substr(0, retorno.length - 1);

    // Retorna
    return retorno;

}

// função para verificar números nos texts dos JqGrids
function verificarNumeros(e) {

    // Se for IE
    if (window.event) {
        var tecla = window.event.keyCode;
    }
    // Se for outro browser  
    else if (e.charCode) {
        var tecla = e.charCode;
    }

    // Seleciona apenas as teclas que forem números, backspace, setas ou delete              
    if (!((tecla >= "47") && (tecla <= "58") || (tecla == "0") || (tecla == null))) {
        return false;
    } else
        return true;
}

function retornarGridSelectComum(lista, propriedadeCodigo, propriedadeDescricao) {

    // String de retorno
    var retorno = "";

    // Para cada item, adiciona no combo
    for (var i = 0; i < lista.length; i++) {

        // Se tem descricao utiliza, se nao, usa o mnemonico
        var descricao = lista[i][propriedadeDescricao];

        // Adiciona o item
        retorno += lista[i][propriedadeCodigo] + ":" + lista[i][propriedadeDescricao] + ";";

    }

    // Ajusta a string
    if (retorno != "")
        retorno = retorno.substr(0, retorno.length - 1);

    // Retorna
    return retorno;

}

// Validação de documentos.
function validarObjeto(validacoes) {
    //    validacoes = 
    //    [
    //      {
    //        Aba : "",
    //        Objeto: {},
    //        Message: "",
    //        TipoObeto : "",
    //        Obrigatorio: true,
    //        Valor: ""
    //    },
    //    {
    //        Aba : "",
    //        Propriedade: {},
    //        Message: "",
    //        TipoObeto : "",
    //        Obrigatorio: true,
    //        Valor: ""
    //    }
    //];

    var erros = [];

    for (var objValidacao in validacoes) {
        // Verifica se tem validacao
        if (validacoes[objValidacao]) {
            if (validacoes[objValidacao].Obrigatorio) {
                if (validacoes[objValidacao].Valor == "") {
                    erros.push(
                    {
                        tab: objeto[propriedade].Aba,
                        labelValue: objeto[propriedade].Label,
                        message: objeto[propriedade].Mensagem
                    });
                }
            }
        }
    }
    return erros;
}

function showLoadingDialog() {
    // Abro o dialog
    $("#loadingDialog").dialog("open");
}

function hideLoadingDialog() {
    // Abro o dialog
    $("#loadingDialog").dialog("close");
}

function hideVisibleComboboxes(parentObject, callingObject) {
    $('select:visible', parentObject).not($('select:visible', callingObject)).data('hideCallingObject', callingObject).attr('wasVisible', 'x').hide();

    if ($('#loadingDialog:visible').length == 0 && callingObject.id.substring(0, 5) == 'site_') {
    }
}

function showHiddenComboboxes(parentObject, callingObject) {
    $('select[wasVisible]:not(:visible)', parentObject).each(function() {
        if ($(this).data('hideCallingObject') == callingObject) {
            $(this).show().removeAttr('wasVisible').removeData('hideCallingObject');
        }
    });
}

function stringToDate(dataString) {
    dataString += " 00:00:00";
    var dia = $.trim(dataString.split("/")[0]);
    var mes = $.trim(dataString.split("/")[1]);
    var ano = $.trim(dataString.split(" ")[0].split("/")[2]);
    var hora = $.trim(dataString.split(":")[0].substr(10));
    var minuto = $.trim(dataString.split(":")[1]);
    var segundo = $.trim(dataString.split(":")[2]);

    var data = new Date(ano, mes - 1, dia, hora, minuto, segundo);

    return data;
}

function carregarPermissoes(callback) {
    template_default_permissoes_carregarCallback = callback;
    // Carrega as permissoes do usuario logado (caso nao existam)
    if (!template_default_permissoes_carregadas) {
        executarServico(
			"ListarPermissoesAssociadasRequest", {},
			"carregarPermissoesCallback");
    } else if (template_default_permissoes_carregarCallback) {
        template_default_permissoes_carregarCallback();
    }
}

function carregarPermissoesCallback(data) {
    // Possui os valores?
    if (data.PermissoesAssociadas) {
        template_default_permissoes_carregadas = data.PermissoesAssociadas;
    }
    // Chamo o callback
    if (template_default_permissoes_carregarCallback) {
        template_default_permissoes_carregarCallback();
    }
}

function LogCampoPreview() {
    /* CONFIG */

    xOffset = 10;
    yOffset = 30;

    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result

    /* END CONFIG */
    $("label.previewLog").hover(function(e) {
        this.t = this.attributes.getNamedItem("log").value;
        this.title = "";
        var c = this.t;
        $("body").append("<div style='z-index:999999;' id='previewLog'>" + c + "</div>");
        $("#previewLog")
			    .css("top", (e.pageY - xOffset) + "px")
			    .css("left", (e.pageX + yOffset) + "px")
			    .fadeIn("fast");
    },
            function() {
                this.title = this.t;
                $("#previewLog").remove();
            });

    $("a.previewLog").mousemove(
	        function(e) {
	            $("#previewLog")
			    .css("top", (e.pageY - xOffset) + "px")
			    .css("left", (e.pageX + yOffset) + "px");
	        });
}

/* --------------------------------------------------------------------------------------------------
Funcao para tratar apresentação de Data
-------------------------------------------------------------------------------------------------- */
function formatterBoolean(valor) {
    if (valor == undefined) {
        return '';
    }
    if (valor == true) {
        return 'S';
    } else {
        return 'N';
    }
}

/* --------------------------------------------------------------------------------------------------
Funcao para tratar apresentação de boolean 
-------------------------------------------------------------------------------------------------- */
function formatterDate(valorData) {
    if (valorData == undefined) {
        return '';
    }
    var pFormatado = valorData.substring(0, 10);
    return pFormatado;
}

/* --------------------------------------------------------------------------------------------------
Funcao para tratar valores monetarios
-------------------------------------------------------------------------------------------------- */
function float2moeda(num) {

    x = 0;

    if (num < 0) {
        num = Math.abs(num);
        x = 1;
    }

    if (isNaN(num)) num = "0";
    cents = Math.floor((num * 100 + 0.5) % 100);

    num = Math.floor((num * 100 + 0.5) / 100).toString();

    if (cents < 10) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.'
               + num.substring(num.length - (4 * i + 3));

    ret = num + ',' + cents;

    if (x == 1) ret = ' - ' + ret; return ret;

}