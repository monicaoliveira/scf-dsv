﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorio_vencimento_flat.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.relatorio_vencimento_flat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="relatorio_vencimento_flat" runat="server">
        <div>
            <asp:DataGrid ID="dgRelatorioVencimento" runat="server"
                AlternatingItemStyle-BackColor="LightGray"
                HeaderStyle-BackColor="Gray"
                HeaderStyle-BorderStyle="Solid"
                HeaderStyle-BorderColor="White"
                HeaderStyle-ForeColor="White"
                AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateColumn HeaderText="Estabelecimento" >
                        <ItemTemplate>
                            <asp:Label ID="lbl1" runat="server" Text='<%# Eval("Estabelecimento.CNPJ") %>' /> 
                            - 
                            <asp:Label id="lbl7" runat="server" Text='<%# Eval("Estabelecimento.RazaoSocial")%>'></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Produto" >
                        <ItemTemplate>
                            <asp:Label ID="lbl2" runat="server" Text='<%# Eval("Produto.DescricaoProduto")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Banco" >
                        <ItemTemplate>
                            <asp:Label ID="lbl3" runat="server" Text='<%# Eval("Estabelecimento.Banco")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Agencia" >
                        <ItemTemplate>
                            <asp:Label ID="lbl4" runat="server" Text='<%# Eval("Estabelecimento.Agencia")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Conta" >
                        <ItemTemplate>
                            <asp:Label ID="lbl5" runat="server" Text='<%# Eval("Estabelecimento.Conta")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Valor Liquido" >
                        <ItemTemplate>
                            <asp:Label ID="lbl6" runat="server" Text='<%# Convert.ToDecimal(Eval("ValorLiquido")).ToString("N2")%>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </form>
</body>
</html>
