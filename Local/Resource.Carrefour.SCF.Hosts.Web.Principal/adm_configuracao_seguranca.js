﻿$(document).ready(function() 
{   if (!$.config)
        $.config = {};
    $("#subtitulo").html("ADMINISTRATIVO [Parâmetro de Segurança]");// 1416 Insere o subtítulo
    $.config.seguranca = 
    {   load: function() 
        {
            $("#cmdSalvar").click(function() 
            {
                if ($("#txtQtdDiasExpirarSenha").val() == "" || $("#txtTamanhoMinSenha").val() == "")  // 1416 if ($("#txtQtdDiasExpirarSenha").val() == "" || $("#txtTamanhoMinSenha").val() == "" ||  $("#txtQtdTentativaBloqOper").val() == "" || $("#txtBloqueioSenhas").val() == "") 
                {   popup(
                    {   titulo: "ERRO NO PREENCHIMENTO"
                    ,   icone: "erro"
                    ,   mensagem: "Obrigatório o preenchimento de todos campos !"
                    ,   callbackOK: function() { }
                    });
                }
                else 
                {   $.config.seguranca.salvarServicoSegurancaConfig();
                }
            });
            //1416$("#adm_configuracao_seguranca").Forms("initializeFields");
            $.config.seguranca.receberServicoSegurancaConfig();
        }
    ,   receberServicoSegurancaConfig: function() 
        {   executarServico
            (   "ReceberServicoSegurancaConfigRequest"
            ,   {}
            ,   function(data) 
                {   $("#adm_configuracao_seguranca").Forms("initializeFields");
                    $("#adm_configuracao_seguranca").Forms("setData", { dataObject: data.ServicoSegurancaConfig }); 
                }
            );
        }            
    ,   salvarServicoSegurancaConfig: function() 
        {   var servicoSegurancaConfig = {};
            $("#adm_configuracao_seguranca").Forms("getData", { dataObject: servicoSegurancaConfig });
            if (servicoSegurancaConfig && servicoSegurancaConfig.DiasExpiracaoSenha == "") 
            {   servicoSegurancaConfig.DiasExpiracaoSenha = "0";
            }
            if (servicoSegurancaConfig && servicoSegurancaConfig.TamanhoMinimoSenha == "") 
            {   servicoSegurancaConfig.TamanhoMinimoSenha = "0";
            }
            executarServico
            (   "SalvarServicoSegurancaConfigRequest"
            ,   { ServicoSegurancaConfig: servicoSegurancaConfig }
            ,   $.config.seguranca.salvarParametrosCallback
            );
        }
    ,   salvarParametrosCallback: function(data) 
        {   var strCriticas = "";
            if (data && data.Criticas.length > 0) 
            {   for (var i = 0; i < data.Criticas.length; i++) 
                {   strCriticas = strCriticas + (i + 1) + " - " + data.Criticas[i].Descricao + "<br />";
                }
                popup(
                {   titulo: "ERRO SALVAR"
                ,   icone: "erro"
                ,   mensagem: "Ocorreu o erro abaixo na tentativa de salvar os parâmetros: <br /><br />" + strCriticas
                ,   callbackOK: function() {}
                });
            } else {
                popup(
                {   titulo: "SUCESSO"
                ,   icone: "sucesso"
                ,   mensagem: "Parâmetros salvo com Sucesso!"
                ,   callbackOK: function() {}
                });
            }
        }
    };
    $.config.seguranca.load();
});