﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_log_lista.js"></script>
    <script type="text/javascript" src="site_log_lista_relatorio.js"></script>
    <script type="text/javascript" src="site_log_detalhe.js"></script>
    <link href="site_log_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_log_lista_relatorio.css" rel="stylesheet" type="text/css" />
    <link href="site_log_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Log</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- Conteúdo da Página -->
    <div id="site_log_lista">
    
<%--  1469 INICIO --%>
        <!-- Área do Processo -->
        <div class="processo" id="painelProcesso">
            <table style="width:100%">
                <tr>
                    <td style="width:80%">
                        <label style=" font-size:large">Lista do Log do Processo </label>
                        <label style=" font-size:large" class="CodigoProcesso"></label>
                    </td>                    
                    <td style="width:20%" align="right">
                        <button class="cmdAtualizar">Atualizar</button>
                    </td>
                </tr>
            </table>                 
        </div>  
<%--  1469 FIM --%>

        <!-- Área do filtro -->
        <div class="filtro formulario" id="painelLogFiltro">
            <table >
                <tr>
                    <td>
                        <label>Usuário:</label>
                    </td>
                    <td>
                        <input type="text" value="" class="filtroUsuario" maxlength="100"/>
                    </td>
<%--  1469 INICIO --%>
                    <td>
                        <label>Tp Log:</label>
                    </td>
                    <td>
                        <input type="text" value="" class="filtroTipoLog" maxlength="30"/>
                    </td>                    
                    <td>
                        <label>Tp Origem:</label>
                    </td>
                    <td>
                        <input type="text" value="" class="filtroTipoOrigemLog" maxlength="30"/>
                    </td>                          
                    <td>
                        <label>Processo:</label>
                    </td>
                    <td>
                        <input type="number" value="" class="filtroCodigoOrigemLog" maxlength="12"/>
                    </td>                     
<%--  1469
ESTE CAMPO É CARREGADO INCORRETAMENTE, SEM CONFERIR AS OPÇÕES QUE EXISTEM NO BANCO DE DADOS
EXEMPLO:PR_RELATORIO_TRANSACAO_L TEM NO CAMPO TIPO DE LOG MAS NÃO É EXIBIDO NO FILTRO     
ACIMA ESTAS ESTES MESMOS CAMPOS PARA SEREM DIGITADOS E PROCURADOS COM O CONTEM            
                    <td>
                        <label>Tipo de Log:</label>
                    </td>
                    <td >
                        <select class="filtroTipoLog">
                            <option value="?">(selecione)</option>
                        </select>
                    </td>--%>
<%--    1469
ESTE CAMPO CONTINHA OPÇÕES INEXISTENTES NA TB_LOG
ACIMA ESTAS ESTES MESMOS CAMPOS PARA SEREM DIGITADOS E PROCURADOS COM O CONTEM
                    <td>
                        <label class="lblTipoOrigemLog">Tipo Origem de Log:</label>
                    </td>
                    <td >
                        <select class="filtroTipoOrigemLog">
                            <option value="?">(selecione)</option>
                            <option value="RelatorioConsolidado">Relatório Consolidado</option>
                            <option value="RelatorioVencimento">Relatório Vencimento</option>
                        </select>
                    </td>--%>
<%--  1469 FIM --%>                    
                    <td>
                        <label>De:</label>
                    </td>
                    <td>
                        <input type="text" class="forms[property[filtroDataInicio];dataType[date];maskType[date]] filtroDataInicio"/>
                    </td>
                    <td>
                        <label>Até:</label>
                    </td>
                    <td>
                        <input type="text" class="forms[property[filtroDataFim];dataType[date];maskType[date]] filtroDataFim"/>
                    </td>
                    <td align="right">
                        <button class="cmdPesquisar">Pesquisar</button>
                    </td>
                </tr>
            </table>                 
        </div>  
        <!-- Resultado -->
        <div id="tb_site_log_lista" class="lista">
        
            <table id="tbLog"></table>
            <div class="botoes">
<%--1469 - 
ESTES BOTÕES ERAM PARA IDENTIFICAR O LOG WEB E APP PARA TEMPO
MAS NÃO EXISTEM MAIS ESTES LOGS
                <button class="cmdGerarExcel">Gerar Excel Log Filho</button>
                <button class="cmdLogFilho">Visualizar Logs Filhos</button>
--%>                
                <button class="cmdDetalhe">Mostrar Detalhe</button>
            </div>
        </div>
       
    </div>

</asp:Content>
