﻿$(document).ready(function() {

    // Cria namespace 
    $.dominiogenerico = {};

    // Cria namespace 
    $.dominiogenerico.lista = {};

    // Funcoes do namespace 
    $.dominiogenerico.lista.configuracao = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function(callback) {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Configuração de Domínios Genéricos");

            // Inicializa grid de lista 
            $("#tbLista").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 320,
                colNames: ["CodigoLista", "Lista", "Mnemonico", ""],
                colModel: [
                    { name: "CodigoLista", index: "CodigoLista", editable: true, hidden: true, key: true },
                    { name: "Descricao", index: "Descricao", editable: true, editoptions: { maxlength: 20} },
                    { name: "Mnemonico", index: "Mnemonico", editable: false, hidden: true, editoptions: { maxlength: 20} },
                    { name: "ColunaConfiguravelHeader", index: "ColunaConfiguravelHeader", editable: false, hidden: true }
                ],
                onSelectRow: $.dominiogenerico.lista.configuracao.listarListaItem

            });

            // Inicializa grid de lista item
            $("#tbListaItem").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 640,
                colNames: ["CodigoListaConfiguracao", "CodigoListaItem", "Descrição", "Valor", "Exporta CCI", "", ""],
                colModel: [
                    { name: "CodigoListaConfiguracao", width: 50, index: "CodigoListaConfiguracao", hidden: true, key: true },
                    { name: "CodigoListaItem", width: 50, index: "CodigoListaItem", hidden: true },
                    { name: "NomeListaItem", index: "NomeListaItem", width: 550, editable: false, classes: "grid[property[NomeListaItem];required]" }, 
                    { name: "Valor", index: "Valor", width: 60, editable: false, classes: "grid[property[Valor]]" },
                    { name: "Configuracao", width: 70, index: "Configuracao", classes: "grid[property[Configuracao];required]", editable: true, key: true, editoptions: { maxlength: 1, style: "text-transform: uppercase"} }, //SCF1139 >>> editoptions: { maxlength: 1, style: "text-transform: uppercase"}
                    { name: "Ativo", index: "Ativo", width: 50, classes: "grid[property[Ativo];required]", hidden: true },
                    { name: "Acao", index: "Acao", align: "center", width: 50, editable: false }

                ],
                sortable: true
            });
            

            carregarListas2(["VersaoLayout"], function() {

                // Inicializa edicao do grid
                $.grid.inicializar({
                    grid: $("#tbListaItem"),
                    modeloLinha: { Ativo: true },
                    colunaId: "CodigoListaConfiguracao",
                    callbackNovo: $.dominiogenerico.lista.configuracao.salvarLista,
                    callbackEditar: $.dominiogenerico.lista.configuracao.salvarLista,
                    permiteApenasAlterar: true,
                    callbackRemover: $.dominiogenerico.lista.configuracao.removerItemLista,
                    callbackRestaurar: $.dominiogenerico.lista.configuracao.ativarListaItem
                });

                // Pede lista genérica -- melhorias-2017 - sera carregado depois de escolher o grupo
                //$.dominiogenerico.lista.configuracao.listarListas(callback);
                
                // melhorias-2017 - Combo de Grupo
                $.dominiogenerico.lista.configuracao.preencherGrupo();

                // Trata o redimensionamento da tela
                $(window).bind("resize", function() {
                    //$("#tbLista").setGridHeight($(window).height() - margemRodape + 20);
                    $("#tbLista").setGridHeight($(window).height() - margemRodape);
                    $("#tbListaItem").setGridWidth($(window).width() - margemEsquerda - 380);
                    $("#tbListaItem").setGridHeight($(window).height() - margemRodape);
                }).trigger("resize");
            });
        },

        //------------------------------------------------------------------------
        // listarListas: Pede listas dominios ao serviço
        //------------------------------------------------------------------------
        listarListas: function(callback) {

            // Guarda o callback
            $.dominiogenerico.lista.configuracao.variaveis.listarListasCallback = callback;

            // Cria request
            var request = {};

            //ECOMMERCE - Fernando Bove - 20151223: Colocando filtro para trazer apenas MeioPagamento
            request.FiltroCodigoLista = "9";
            request.FiltroCodigoEmpresaGrupo = $("#cmbEmpresaGrupo").val() // melhorias-2017
            request.RetornarItensExcluidos = true;

            // Pede lista de parametros
            executarServico(
            "ListarListaConfiguracaoRequest",
            request,
            "$.dominiogenerico.lista.configuracao.listarListasFixas");
        },
        
        // --------------------------------------------------------------------
        // Preenche o combo de grupos
        // --------------------------------------------------------------------
        preencherGrupo: function(callback) {

            executarServico(
                "ListarEmpresaGrupoRequest",
                {},
                function(data) {

                    var combo = $("#cmbEmpresaGrupo");
                    // Limpo o combo (exceto o primeiro item)
                    combo.children().each(
                          function(index) {
                                if (index != 0)
                                  $(this).remove();
                          }
                    );

                    // Para cada item, adiciono no combo
                    for (var i = 0; i < data.Resultado.length; i++) {
                        // Se tem descricao utiliza, se nao, usa o mnemonico
                        var descricao = data.Resultado[i].NomeEmpresaGrupo;
                        // Adiciona o item no bomo
                        combo.append("<option value='" + data.Resultado[i].CodigoEmpresaGrupo + "'>"
                                         + descricao + "</option>");
                    }

                    // Chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //------------------------------------------------------------------------------------------
        // listarListasB: Pede listas dominios ao serviço - Codigo Ajuste (SCF1139) - Marcos Matsuoka
        //------------------------------------------------------------------------------------------
        listarListasB: function(callback) {

            // Guarda o callback
            $.dominiogenerico.lista.configuracao.variaveis.listarListasCallback = callback;

            // Cria request
            var request = {};

            request.FiltroCodigoLista = "8";
            request.FiltroCodigoEmpresaGrupo = $("#cmbEmpresaGrupo").val() // melhorias-2017
            request.RetornarItensExcluidos = true;

            // Pede lista de parametros
            executarServico(
            "ListarListaConfiguracaoRequest",
            request,
            "$.dominiogenerico.lista.configuracao.listarListasFixas");
        },

        // --------------------------------------------------------------------
        //  listarListasFixas: Pede listas fixas ao serviço
        // --------------------------------------------------------------------
        listarListasFixas: function(callback) {

            // Guarda o callback
            $.dominiogenerico.lista.configuracao.variaveis.listaListar = callback.Resultado;

            // Cria request
            var request = {};

            // Pede lista de parametros
            executarServico(
            "ListarListasFixasRequest",
            request,
            "$.dominiogenerico.lista.configuracao.listarListasConfiguracao");
        },

        listarListasConfiguracao: function(callback) {

            // Guarda resultado
            $.dominiogenerico.lista.configuracao.variaveis.listaExcluir = callback.Resultado;
            var listar = $.dominiogenerico.lista.configuracao.variaveis.listaListar;

            for (var j = 0; j < $.dominiogenerico.lista.configuracao.variaveis.listaExcluir.length; j++) {
                for (var i = 0; i < listar.length; i++) {
                    if ($.dominiogenerico.lista.configuracao.variaveis.listaExcluir[j] == listar[i].Descricao) {
                        listar.splice(i, 1);
                    }
                }
            }

            for (var i = 0; i < listar.length; i++) {
                $("#tbLista").jqGrid(
                "addRowData",
                "tbLista_" + listar[i].CodigoLista,
                listar[i]);
            }

            // Se houver, chama o callback
            if ($.dominiogenerico.lista.configuracao.variaveis.listarListasCallback)
                $.dominiogenerico.lista.configuracao.variaveis.listarListasCallback();
        },

        // --------------------------------------------------------------------
        //  listarListaItem: Pede lista item ao serviço
        // --------------------------------------------------------------------
        listarListaItem: function(rowId) {

            //Limpa a os campos de inserção
            $("#novo_tbListaItem_Valor").val("");
            $("#novo_tbListaItem_Descricao").val("");

            // Cria request
            var request = {};

            $.dominiogenerico.lista.configuracao.variaveis.linhaIdSelecionada = rowId;

            // Envia parametros
            $.dominiogenerico.lista.configuracao.variaveis.Lista = $("#tbLista").getRowData(rowId);
            $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens = [];

            request.FiltroCodigoLista = $.dominiogenerico.lista.configuracao.variaveis.Lista.CodigoLista;
            request.FiltroCodigoEmpresaGrupo = $("#cmbEmpresaGrupo").val();
            request.RetornarItensExcluidos = true;

            $("#tbListaItem").jqGrid("setLabel", 4, $.dominiogenerico.lista.configuracao.variaveis.Lista.ColunaConfiguravelHeader);

            // Pede lista ao serviço
            executarServico(
            "ListarListaItemConfiguracaoRequest",
            request,
            "$.dominiogenerico.lista.configuracao.listarListaItemCallBack");
        },

        listarListaItemCallBack: function(data) {

            // Guarda resultado
            $.dominiogenerico.lista.configuracao.variaveis.listaSelecionada = data.Resultado;
            // Limpa grid
            $("#tbListaItem").clearGridData();

            // Verifica status
            var exibirexcluidos;
            if (!exibirexcluidos) {

                // Mostra os ativos
                for (var i = 0; i < data.Resultado.length; i++) {
                    $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens.push(data.Resultado[i]);
                    if (data.Resultado[i].Ativo == true) {
                        $("#tbListaItem").jqGrid(
                            "addRowData",
                             "tbListaItem_" + data.Resultado[i].CodigoListaItem,
                            data.Resultado[i]
                        );
                    }
                }
            }

            else {

                // Mostra os excluidos
                for (var i = 0; i < data.Resultado.length; i++) {
                    if (data.ListaInfo.Itens[i].Ativo == false) {
                        $("#tbListaItem").jqGrid(
                            "addRowData",
                             "tbListaItem_" + data.Resultado[i].CodigoListaItem,
                            data.ListaInfo.Itens[i]
                            );
                        $.grid.esconderBotoes(data.Resultado[i].CodigoListaItem, true);
                    }
                }
            }
        },

        // ----------------------------------------------------------------------
        // validar: Faz a validação necessária dos campos
        // ----------------------------------------------------------------------
        validar: function(lGrid, lParam, CallbackOK, CallbackErro, pCriticas) {
            var criticas = new Array();

            // Realiza as validações
            criticas = $.grid.validate({ pGrid: lGrid, param: lParam, returnErrors: true, errors: criticas });

            // Se existir críticas passadas via parametro, incluir à lista
            if (pCriticas) {
                for (i = 0; i < pCriticas.length; i++) {
                    if (pCriticas[i])
                        criticas.push(pCriticas[i]);
                }
            }

            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                CallbackErro();
            }
            else
                CallbackOK();
        },

        // ----------------------------------------------------------------------
        // salvarLista: Trata inserção de novo Dominio
        // ----------------------------------------------------------------------
        salvarLista: function(param) {

            var request = {};
            var ListaItemInfo = {};
            var rowid = null;

            // Verifica se a lista foi selecionada
            if ($("#tbLista").getGridParam("selrow") == null) {
                var criticas = new Array();
                criticas.push({ message: "Nenhuma lista selecionada." });
                rowid = $("#tbLista").getGridParam("selrow");
            }

            //SCF1139 - Marcos Matsuoka - valida campos da tela de Configuração de Domínios Genéricos - inicio
            // Extrai valores
            var selecionado = $("#tbListaItem").jqGrid('getGridParam', 'selrow');
            var objSelecionado = $("#tbListaItem").getRowData(selecionado);
            var config = objSelecionado.Configuracao;
            var valorAtual = $(config).val();
            
            var configuracao = param.valores.Configuracao; 
            var opcoes = $.grid.formClassDefaultOptions;

            if (configuracao != "S" && configuracao != "N" && configuracao != "s" && configuracao != "n") {
                var criticas = [];
                criticas.push({
                    message: opcoes.errorCampoConfiguracao, element: $(this)
                });
                $.Forms.showValidationForm({ errors: criticas });

            } else {
                if (configuracao == "s" || configuracao == "n") {
                    if (configuracao == "s") {
                        param.valores.Configuracao = "S";
                    } else {
                        param.valores.Configuracao = "N";
                    }
                }
            }
            //SCF1139 - Marcos Matsuoka - valida campos da tela de Configuração de Domínios Genéricos - fim


            // Pede validação necessária
            $.dominiogenerico.lista.configuracao.validar(
                $("#tbListaItem"),
                param,
                function() {

                    var request = {};

                    // Pega codigo da lista
                    param.valores.CodigoLista = $.dominiogenerico.lista.configuracao.variaveis.listaSelecionada.CodigoLista;

                    // Verifica se existe o item
                    if (param.valores.CodigoListaItem && param.valores.CodigoListaItem != "") {

                        // Item existe
                        for (var i = 0; i < $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens.length; i++) {
                            if (param.valores.CodigoListaItem == $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens[i].CodigoListaItem)
                                $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens[i] = param.valores;
                        }
                    }
                    else {

                        // Novo item
                        $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens.push(param.valores);
                    }


                    // Envia parametros
                    request = $.dominiogenerico.lista.configuracao.variaveis.Lista;
                    //request.CodigoEmpresaGrupo = $("#cmbEmpresaGrupo").val() // melhorias-2017

                    // Pede para salvar
                    executarServico(
                        "SalvarListaItemConfiguracaoRequest",
                        request,
                        function(data) {

                            var criticas = data.Criticas;

                            if (criticas[0]) {

                                $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens.splice($.dominiogenerico.lista.variaveis.Lista.Itens.length - 1, 1);
                                param.valores.CodigoListaItem = "";

                                // Pede para selecionar uma linha
                                popup({
                                    titulo: "Atenção",
                                    icone: "atencao",
                                    mensagem: criticas[0].Descricao,
                                    callbackOK: function() {

                                    }
                                });

                            }
                            else {
                                param.callbackOK({});
                                $.dominiogenerico.lista.configuracao.listarListaItem($.dominiogenerico.lista.configuracao.variaveis.linhaIdSelecionada);


                            };
                        }
                    );
                }, function() { }, criticas);
        },

        // ----------------------------------------------------------------------
        // removerItemLista: Trata remoção de novo Dominio
        // ----------------------------------------------------------------------
        removerItemLista: function(param) {

            var request = {};

            // Remove da lista
            for (var i = 0; i < $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens.length; i++) {
                if (param.rowid == $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens[i].CodigoListaItem)
                    $.dominiogenerico.lista.configuracao.variaveis.Lista.Itens.splice(i, 1);
            }

            // Envia parametros
            request.ListaInfo = $.dominiogenerico.lista.configuracao.variaveis.Lista;

            // Salva objeto com o item removido
            executarServico(
                "SalvarListaItemConfiguracaoRequest",
                request,
                function(data) {

                    var criticas = data.Criticas;

                    if (criticas[0]) {

                        // Exibe a critica
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: criticas[0].Descricao,
                            callbackOK: function() {
                                $.dominiogenerico.lista.configuracao.listarListaItem($.dominiogenerico.lista.configuracao.variaveis.linhaIdSelecionada);
                            }
                        });

                    }
                    else {

                        param.callbackOK({});
                        $.dominiogenerico.lista.configuracao.listarListaItem($.dominiogenerico.lista.configuracao.variaveis.linhaIdSelecionada);

                    };
                }
            );
            //            // Salva objeto com o item removido
            //            executarServico(
            //                "SalvarListaRequest",
            //                request,
            //                function(data) {
            //                    param.callbackOK({
            //                });
            //                $.dominiogenerico.lista.listarListaItem($.dominiogenerico.lista.variaveis.linhaIdSelecionada);

            //            }
            //            );

        },

        // ----------------------------------------------------------------------
        // inativarListaItem: Inativa um ítem da lista
        // ----------------------------------------------------------------------
        inativarListaItem: function(param) {
            $.dominiogenerico.lista.configuracao.ativarInativarItemLista(param, false);
        },

        // ----------------------------------------------------------------------
        // ativarListaItem: Ativa novamente um ítem da lista
        // ----------------------------------------------------------------------
        ativarListaItem: function(param) {
            $.dominiogenerico.lista.configuracao.ativarInativarItemLista(param, true);
        },

        // ----------------------------------------------------------------------
        // ativarInativarItemLista: Ativa ou inativa um ítem da lista
        // ----------------------------------------------------------------------
        ativarInativarItemLista: function(param, ativo) {

            var request = {};
            request.Ativar = ativo;
            request.CodigoListaItem = param.rowid;

            executarServico(
                "RemoverListaConfiguracaoRequest",
                request,
                function() {
                    param.callbackOK();
                });

            return false;
        }
        
        
    }
    
    // -------------------------------------------------------------------
    // Evento change do combo de grupo 
    // -------------------------------------------------------------------
    $("#cmbEmpresaGrupo").change(function() {

        $("#tbLista").clearGridData();
        $("#tbListaItem").clearGridData();
        
        // Pega código do grupo, se tiver
        var codigoEmpresaGrupo = $("#cmbEmpresaGrupo").val();
        if (codigoEmpresaGrupo != "?") {
            // Carrega combo de subgrupos correspondente ao grupo
            $.dominiogenerico.lista.configuracao.listarListas(function() {
        $.dominiogenerico.lista.configuracao.listarListasB();
    });
        }

    });
    
    // Pede load da página
    $.dominiogenerico.lista.configuracao.load(function() {
        $.dominiogenerico.lista.configuracao.listarListasB();
    });

    // Carrega dados do protótipo
    //    $.dominiogenerico.lista.carregarDadosPrototipo();


});