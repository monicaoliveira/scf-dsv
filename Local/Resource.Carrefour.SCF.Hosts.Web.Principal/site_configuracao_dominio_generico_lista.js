﻿$(document).ready(function() {

    // Cria namespace 
    $.dominiogenerico = {};

    // Funcoes do namespace 
    $.dominiogenerico.lista = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Domínios Genéricos");

            // Inicializa grid de lista 
            $("#tbLista").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 320,
                colNames: ["CodigoLista", "Lista", "Mnemonico"],
                colModel: [
                    { name: "CodigoLista", index: "CodigoLista", editable: true, hidden: true, key: true },
                    { name: "Descricao", index: "Descricao", editable: true, editoptions: { maxlength: 20} },
                    { name: "Mnemonico", index: "Mnemonico", editable: false, hidden: true, editoptions: { maxlength: 20} }
                ],
                onSelectRow: $.dominiogenerico.lista.listarListaItem

            });

            // Inicializa grid de lista item
            $("#tbListaItem").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 160,
                width: 640,
                colNames: ["CodigoListaItem", "Valor", "Descrição", "", ""],
                colModel: [
                    { name: "CodigoListaItem", index: "CodigoListaItem", hidden: true, key: true },
                    { name: "Valor", index: "Valor", classes: "grid[property[Valor]]", editable: true, editoptions: { maxlength: 50} },
                    { name: "Descricao", index: "Descricao", classes: "grid[property[Descricao];required]", editable: true, editoptions: { maxlength: 50} },
                    { name: "Ativo", index: "Ativo", hidden: true },
                    { name: "Acao", index: "Acao", align: "center", width: 50, editable: false }
                ],
                sortable: true
            });

            

            // Inicializa listas
            /*
            carregarListas2(["VersaoLayout", "IdentificacaoAutorizada", "IdentificacaoMoeda", "IdentificacaoTEF",
            "TipoProduto", "MeioCaptura", "CupomFiscal", "ModalidadeVenda", "MeioPagamento",
            "CodigoAjuste", "CodigoAnulacao", "FormaMeioPagamento"], function() {
            */
            carregarListas2(["VersaoLayout"], function() {

                // Inicializa edicao do grid
                $.grid.inicializar({
                    grid: $("#tbListaItem"),
                    modeloLinha: { Ativo: true },
                    colunaId: "CodigoListaItem",
                    callbackNovo: $.dominiogenerico.lista.salvarLista,
                    callbackEditar: $.dominiogenerico.lista.salvarLista,
                    callbackRemover: $.dominiogenerico.lista.removerItemLista,
                    callbackRestaurar: $.dominiogenerico.lista.ativarListaItem
                });

                // Pede lista genérica
                $.dominiogenerico.lista.listarListas();

                // Trata o redimensionamento da tela
                $(window).bind("resize", function() {
                    $("#tbLista").setGridHeight($(window).height() - margemRodape + 23);
                    $("#tbListaItem").setGridWidth($(window).width() - margemEsquerda - 380);
                    $("#tbListaItem").setGridHeight($(window).height() - margemRodape);
                }).trigger("resize");
            });
        },

        //------------------------------------------------------------------------
        // listarListas: Pede listas dominios ao serviço
        //------------------------------------------------------------------------
        listarListas: function(callback) {

            // Guarda o callback
            $.dominiogenerico.lista.variaveis.listarListasCallback = callback;

            // Cria request
            var request = {};

            // Pede lista de parametros
            executarServico(
            "ListarListaRequest",
            request,
            "$.dominiogenerico.lista.listarListasFixas");
        },

        // --------------------------------------------------------------------
        //  listarListasFixas: Pede listas fixas ao serviço
        // --------------------------------------------------------------------
        listarListasFixas: function(callback) {

            // Guarda o callback
            $.dominiogenerico.lista.variaveis.listaListar = callback.Resultado;

            // Cria request
            var request = {};

            // Pede lista de parametros
            executarServico(
            "ListarListasFixasRequest",
            request,
            "$.dominiogenerico.lista.listarListasFixasCallBack");
        },

        listarListasFixasCallBack: function(callback) {

            // Guarda resultado
            $.dominiogenerico.lista.variaveis.listaExcluir = callback.Resultado;
            var listar = $.dominiogenerico.lista.variaveis.listaListar;

            for (var j = 0; j < $.dominiogenerico.lista.variaveis.listaExcluir.length; j++) {
                for (var i = 0; i < listar.length; i++) {
                    if ($.dominiogenerico.lista.variaveis.listaExcluir[j] == listar[i].Descricao) {
                        listar.splice(i, 1);
                    }
                }
            }

            for (var i = 0; i < listar.length; i++) {
                $("#tbLista").jqGrid(
                "addRowData",
                "tbLista_" + listar[i].CodigoLista,
                listar[i]);
            }

            // Se houver, chama o callback
            if ($.dominiogenerico.lista.variaveis.listarListasCallback)
                $.dominiogenerico.lista.variaveis.listarListasCallback();
        },

        // --------------------------------------------------------------------
        //  listarListaItem: Pede lista item ao serviço
        // --------------------------------------------------------------------
        listarListaItem: function(rowId) {

            //Limpa a os campos de inserção
            $("#novo_tbListaItem_Valor").val("");
            $("#novo_tbListaItem_Descricao").val("");

            // Cria request
            var request = {};

            $.dominiogenerico.lista.variaveis.linhaIdSelecionada = rowId;

            // Envia parametros
            $.dominiogenerico.lista.variaveis.Lista = $("#tbLista").getRowData(rowId);
            $.dominiogenerico.lista.variaveis.Lista.Itens = [];

            request.CodigoLista = $.dominiogenerico.lista.variaveis.Lista.CodigoLista;
            request.RetornarItensExcluidos = true;

            // Pede lista ao serviço
            executarServico(
            "ReceberListaRequest",
            request,
            "$.dominiogenerico.lista.listarListaItemCallBack");
        },

        listarListaItemCallBack: function(data) {

            // Guarda resultado
            $.dominiogenerico.lista.variaveis.listaSelecionada = data.ListaInfo;
            // Limpa grid
            $("#tbListaItem").clearGridData();

            // Verifica status
            var exibirexcluidos;
            if (!exibirexcluidos) {

                // Mostra os ativos
                for (var i = 0; i < data.ListaInfo.Itens.length; i++) {
                    $.dominiogenerico.lista.variaveis.Lista.Itens.push(data.ListaInfo.Itens[i]);
                    if (data.ListaInfo.Itens[i].Ativo == true) {
                        $("#tbListaItem").jqGrid(
                            "addRowData",
                             "tbListaItem_" + data.ListaInfo.Itens[i].CodigoListaItem,
                            data.ListaInfo.Itens[i]
                        );
                    }
                }
            }

            else {

                // Mostra os excluidos
                for (var i = 0; i < data.ListaInfo.Itens.length; i++) {
                    if (data.ListaInfo.Itens[i].Ativo == false) {
                        $("#tbListaItem").jqGrid(
                            "addRowData",
                             "tbListaItem_" + data.ListaInfo.Itens[i].CodigoListaItem,
                            data.ListaInfo.Itens[i]
                            );
                        $.grid.esconderBotoes(data.ListaInfo.Itens[i].CodigoListaItem, true);
                    }
                }
            }
        },

        // ----------------------------------------------------------------------
        // validar: Faz a validação necessária dos campos
        // ----------------------------------------------------------------------
        validar: function(lGrid, lParam, CallbackOK, CallbackErro, pCriticas) {
            var criticas = new Array();

            // Realiza as validações
            criticas = $.grid.validate({ pGrid: lGrid, param: lParam, returnErrors: true, errors: criticas });

            // Se existir críticas passadas via parametro, incluir à lista
            if (pCriticas) {
                for (i = 0; i < pCriticas.length; i++) {
                    if (pCriticas[i])
                        criticas.push(pCriticas[i]);
                }
            }

            if (criticas[0]) {
                $.Forms.showValidationForm({ errors: criticas });
                CallbackErro();
            }
            else
                CallbackOK();
        },

        // ----------------------------------------------------------------------
        // salvarLista: Trata inserção de novo Dominio
        // ----------------------------------------------------------------------
        salvarLista: function(param) {

            var request = {};
            var ListaItemInfo = {};
            var rowid = null;

            // Verifica se a lista foi selecionada
            if ($("#tbLista").getGridParam("selrow") == null) {
                var criticas = new Array();
                criticas.push({ message: "Nenhuma lista selecionada." });
                rowid = $("#tbLista").getGridParam("selrow");
            }

            // Pede validação necessária
            $.dominiogenerico.lista.validar(
                $("#tbListaItem"),
                param,
                function() {

                    var request = {};

                    // Pega codigo da lista
                    param.valores.CodigoLista = $.dominiogenerico.lista.variaveis.listaSelecionada.CodigoLista;

                    // Verifica se existe o item
                    if (param.valores.CodigoListaItem && param.valores.CodigoListaItem != "") {

                        // Item existe
                        for (var i = 0; i < $.dominiogenerico.lista.variaveis.Lista.Itens.length; i++) {
                            if (param.valores.CodigoListaItem == $.dominiogenerico.lista.variaveis.Lista.Itens[i].CodigoListaItem)
                                $.dominiogenerico.lista.variaveis.Lista.Itens[i] = param.valores;
                        }
                    }
                    else {

                        // Novo item
                        $.dominiogenerico.lista.variaveis.Lista.Itens.push(param.valores);
                    }


                    // Envia parametros
                    request.ListaInfo = $.dominiogenerico.lista.variaveis.Lista;

                    // Pede para salvar
                    executarServico(
                        "SalvarListaRequest",
                        request,
                        function(data) {

                            var criticas = data.Criticas;

                            if (criticas[0]) {

                                $.dominiogenerico.lista.variaveis.Lista.Itens.splice($.dominiogenerico.lista.variaveis.Lista.Itens.length - 1, 1);
                                param.valores.CodigoListaItem = "";

                                // Pede para selecionar uma linha
                                popup({
                                    titulo: "Atenção",
                                    icone: "atencao",
                                    mensagem: criticas[0].Descricao,
                                    callbackOK: function() {

                                    }
                                });

                            }
                            else {
                                param.callbackOK({});
                                $.dominiogenerico.lista.listarListaItem($.dominiogenerico.lista.variaveis.linhaIdSelecionada);


                            };
                        }
                    );
                }, function() { }, criticas);
        },

        // ----------------------------------------------------------------------
        // removerItemLista: Trata remoção de novo Dominio
        // ----------------------------------------------------------------------
        removerItemLista: function(param) {

            var request = {};

            // Remove da lista
            for (var i = 0; i < $.dominiogenerico.lista.variaveis.Lista.Itens.length; i++) {
                if (param.rowid == $.dominiogenerico.lista.variaveis.Lista.Itens[i].CodigoListaItem)
                    $.dominiogenerico.lista.variaveis.Lista.Itens.splice(i, 1);
            }

            // Envia parametros
            request.ListaInfo = $.dominiogenerico.lista.variaveis.Lista;

            // Salva objeto com o item removido
            executarServico(
                "SalvarListaRequest",
                request,
                function(data) {

                    var criticas = data.Criticas;

                    if (criticas[0]) {

                        // Exibe a critica
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: criticas[0].Descricao,
                            callbackOK: function() {
                                $.dominiogenerico.lista.listarListaItem($.dominiogenerico.lista.variaveis.linhaIdSelecionada);
                            }
                        });

                    }
                    else {

                        param.callbackOK({});
                        $.dominiogenerico.lista.listarListaItem($.dominiogenerico.lista.variaveis.linhaIdSelecionada);

                    };
                }
            );
            //            // Salva objeto com o item removido
            //            executarServico(
            //                "SalvarListaRequest",
            //                request,
            //                function(data) {
            //                    param.callbackOK({
            //                });
            //                $.dominiogenerico.lista.listarListaItem($.dominiogenerico.lista.variaveis.linhaIdSelecionada);

            //            }
            //            );

        },

        // ----------------------------------------------------------------------
        // inativarListaItem: Inativa um ítem da lista
        // ----------------------------------------------------------------------
        inativarListaItem: function(param) {
            $.dominiogenerico.lista.ativarInativarItemLista(param, false);
        },

        // ----------------------------------------------------------------------
        // ativarListaItem: Ativa novamente um ítem da lista
        // ----------------------------------------------------------------------
        ativarListaItem: function(param) {
            $.dominiogenerico.lista.ativarInativarItemLista(param, true);
        },

        // ----------------------------------------------------------------------
        // ativarInativarItemLista: Ativa ou inativa um ítem da lista
        // ----------------------------------------------------------------------
        ativarInativarItemLista: function(param, ativo) {

            var request = {};
            request.Ativar = ativo;
            request.CodigoListaItem = param.rowid;

            executarServico(
                "RemoverListaItemRequest",
                request,
                function() {
                    param.callbackOK();
                });

            return false;
        }
    };

    // Pede load da página
    $.dominiogenerico.lista.load();

    // Carrega dados do protótipo
    //    $.dominiogenerico.lista.carregarDadosPrototipo();

});