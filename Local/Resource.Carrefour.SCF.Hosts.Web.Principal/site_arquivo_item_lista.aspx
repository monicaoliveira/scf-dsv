﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_arquivo_item_lista.js"></script>
    <link href="site_arquivo_item_lista.css" rel="stylesheet" type="text/css" />
    <title>SCF - ArquivoItem</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--         Lista de ArquivoItem         -->
    <!-- ------------------------------------ -->
    <div id="site_arquivo_item_lista" class="lista">
        
        <!-- Filtro -->
        <div class="filtro">
            <table>
                <tr>
                    <td>
                        <label>Arquivo:</label>
                    </td><td colspan="2">
                        <input type="text" class="txtCodigoArquivo" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Tipo do Item:</label>
                    </td><td>
                        <input type="text" class="txtTipoArquivoItem" />
                    </td><td>
                        <button class="cmdPesquisar">Pesquisar</button>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Lista -->
        <div class="lista">
            <div style="padding: 10px;"></div>
            <table id="tbArquivoItem"></table>
            <div style="padding: 10px;"></div>
            <button class="cmdCriticas">Críticas</button>
        </div>
    </div>

</asp:Content>
