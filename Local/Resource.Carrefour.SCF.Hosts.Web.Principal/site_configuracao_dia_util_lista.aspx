﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_dia_util_lista.js"></script>
    <link href="site_configuracao_dia_util_lista.css" rel="stylesheet" type="text/css" />
    <title>SCF - Configurações de Dias Não Úteis</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--          Lista de Dias Não Úteis         -->
    <!-- ------------------------------------ -->
    <div id="site_configuracao_dia_util_lista" class="lista">
        <table id="tbDiaNaoUtil"></table>
    </div>

</asp:Content>
