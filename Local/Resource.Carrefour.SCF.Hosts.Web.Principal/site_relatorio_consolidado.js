﻿$(document).ready(function() {

    //Global utilizada para fazer o Log de tempo dos relaórios
    myVar = {};

    // Namespace
    if (!$.relatorio)
        $.relatorio = {};

    // Funcoes do namespace 
    $.relatorio = {

        variaveis: {},

        //=================================================================================
        // load: funcoes de inicializacao da tela
        //=================================================================================
        load: function(callback) {

            // Insere o subtítulo
            $("#subtitulo").html("Relatórios > Relatório Consolidado");

            //------------------------------------------------------------------
            // Estilo dos botões
            //-------------------------------------------------------------------
            $("#site_relatorio_consolidado button").button();

            //-------------------------------------------------------------------
            // Inicializa forms
            //-------------------------------------------------------------------
            $("#site_relatorio_consolidado").Forms("initializeFields");

            //-------------------------------------------------------------------
            // BOTÃO VISUALIZAR HTML
            //-------------------------------------------------------------------
            $("#btnVisualizarHtml").click(function() {
                $.relatorio.gerarRelatorioHtml();
            });

            $.relatorio.limparFavorecidos();
            $.relatorio.limparProdutos();
            $.relatorio.limparTipoRegistro();
            $.relatorio.limparMeioPagamento();
            $.relatorio.limparReferencia();

            // seta o valor default do campo Status da Transação - NR-033
            // Carrega listas de enumeradores
            carregarListas("TransacaoStatusEnum", function() {
                // Preenche combo de Status da Transação
                var el = $("#cmbStatusCCI").multiselect(
                    {
                        checkAllText: "Selecionar todos",
                        uncheckAllText: "Remover seleção",
                        selectedText: "# selecionados",
                        noneSelectedText: ""
                    });

                preencherComboComValor($("#cmbStatusCCI"), "TransacaoStatusEnum");

                $('#cmbStatusCCI option[value="1"]').attr({ selected: "selected" });
                el.multiselect('refresh');
            });

            // seta o valor default do campo tipo Venda/Recebimento
            $("#rdoTipoVenda").attr("checked", true);

            $("#rdoTipoVenda").click(function() {
                //$("#chkSubTotal").attr("checked", false);
                $.relatorio.limparTipoRegistro();
                $.relatorio.carregarTipoRegistro();
                $.relatorio.habilitarBotoes();
            });

            $("#rdoTipoRecebimento").click(function() {
                $.relatorio.limparTipoRegistro();
                $.relatorio.carregarTipoRegistro();
                $.relatorio.habilitarBotoes();
            });

            var desabilitaQuantidade = function() {
                if ($("#rdoTipoVenda").attr("checked")) {
                    if ($("#txtDataAgendamentoDe").val() || $("#txtDataAgendamentoAte").val()) {
                        $("#chkVendaQtde").removeAttr('checked');
                        $("#chkVendaQtde").attr('disabled', 'disabled');
                    } else {
                        $("#chkVendaQtde").removeAttr('disabled');
                    }
                }
            };

            $('#txtDataAgendamentoDe').datepicker("option", "onSelect", desabilitaQuantidade());

            $("#txtDataAgendamentoDe").change(function() {
                desabilitaQuantidade();
            });

            $("#txtDataAgendamentoAte").change(function() {
                desabilitaQuantidade();
            });

            $('#txtDataAgendamentoAte').datepicker("option", "onSelect", desabilitaQuantidade());


            // seta o valor default do layout relatorio venda
            $("#rdoVendaTotalizado").attr("checked", true);

            // seta o valor default do layout relatorio recebimento
            $("#rdoRecebimentoTotalizado").attr("checked", true);

            // seta o valor default do layout relatorio recebimento
            $("#chkRecebimentoQtde").attr("checked", true);

            // ECOMMERCE - Fernando Bove - 20160107
            // seta o valor default do campo tipo Contábil
            $("#rdoTipoContabil").attr("checked", true);

            $.relatorio.habilitarBotoes();

            $("#btnGerarRelatorioExcel").click(function() {

                //                if ($("#rdoRecebimentoAnalitico").attr("checked") || $("#rdoRecebimentoSintetico").attr("checked") ||
                //                    $("#rdoVendaAnalitico").attr("checked") || $("#rdoVendaSintetico").attr("checked")) {
                //                    popup({
                //                        titulo: "Atenção",
                //                        icone: "atencao",
                //                        mensagem: "Função não disponível, em desenvolvimento, aguarde....",
                //                        callbackOK: function() {
                //                            return true;
                //                        }
                //                    });
                //                }
                //                else {

                request = {};

                request.Request = {};

                if ($("#rdoTipoVenda").attr("checked")) {
                    request.Request.FiltroTipoConsolidado = $("#rdoTipoVenda").val();
                }
                else
                    request.Request.FiltroTipoConsolidado = $("#rdoTipoRecebimento").val();

                //ECOMMERCE - Fernando Bove - 20160105: Inicio
                if ($("#rdoTipoContabil").attr("checked")) {
                    request.Request.FiltroFinanceiroContabil = $("#rdoTipoContabil").val();
                }
                if ($("#rdoTipoFinanceiro").attr("checked")) {
                    request.Request.FiltroFinanceiroContabil = $("#rdoTipoFinanceiro").val();
                }
                //ECOMMERCE - Fernando Bove - 20160105: Fim

                request.Request.FiltroQuebraFavorecido = $("#chkSubTotal").attr("checked");

                if ($("#cmbProduto").find('option:selected').val() != null) {
                    request.Request.FiltroProduto = $("#cmbProduto").val().toString();
                }
                if ($("#cmbFavorecidoOriginal").find('option:selected').val() != null) {
                    request.Request.FiltroFavorecidoOriginal = $("#cmbFavorecidoOriginal").val().toString();
                }
                if ($("#cmbTipoRegistro").find('option:selected').val() != null) {
                    request.Request.FiltroCodigoTipoRegistro = $("#cmbTipoRegistro").val().toString();
                    request.Request.FiltroDescricaoTipoRegistro = $("#cmbTipoRegistro option:selected").text();
                }
                if ($("#cmbEstabelecimento").val() != "?") {
                    request.Request.FiltroEstabelecimento = $("#cmbEstabelecimento").val();
                    request.Request.FiltroDescricaoEstabelecimento = $("#cmbEstabelecimento option:selected").text();
                }
                if ($("#cmbMeioPagamento").find('option:selected').val() != null) {
                    var cboMeioPagamento = $.trim($("#cmbMeioPagamento").val().toString()).split(" ");
                    var FiltroMeioPagamento = "", MeioPagamento = "";
                    for (var i = 0; i < cboMeioPagamento.length; i++) {
                        if (i % 2 != 0)
                            MeioPagamento = MeioPagamento + cboMeioPagamento[i] + ',';
                        else
                            FiltroMeioPagamento = FiltroMeioPagamento + cboMeioPagamento[i];
                    }
                    MeioPagamento = MeioPagamento.substring(0, MeioPagamento.length - 1);
                    request.Request.MeioPagamento = MeioPagamento;
                    request.Request.FiltroMeioPagamento = FiltroMeioPagamento;
                }

                if ($("#rdoVendaTotalizado").attr("checked"))
                    request.Request.FiltroLayoutVenda = $("#rdoVendaTotalizado").val();
                else if ($("#rdoVendaAnalitico").attr("checked"))
                    request.Request.FiltroLayoutVenda = $("#rdoVendaAnalitico").val();
                else
                    request.Request.FiltroLayoutVenda = $("#rdoVendaSintetico").val();

                if ($("#rdoRecebimentoTotalizado").attr("checked"))
                    request.Request.FiltroLayoutRecebimento = $("#rdoRecebimentoTotalizado").val();
                else if ($("#rdoRecebimentoAnalitico").attr("checked"))
                    request.Request.FiltroLayoutRecebimento = $("#rdoRecebimentoAnalitico").val();
                else if ($("#rdoRecebimentoSintetico").attr("checked"))
                    request.Request.FiltroLayoutRecebimento = $("#rdoRecebimentoSintetico").val();
                else
                    request.Request.FiltroLayoutRecebimento = $("#rdoRecebimentoCCI").val();

                request.Request.FiltroQtdeVenda = $("#chkVendaQtde").attr("checked");
                request.Request.FiltroQtdeRecebimento = $("#chkRecebimentoQtde").attr("checked");

                if ($("#txtDataAgendamentoDe").val() != "") {
                    request.Request.FiltroDataAgendamentoDe = $("#txtDataAgendamentoDe").val();
                }
                if ($("#txtDataAgendamentoAte").val() != "") {
                    request.Request.FiltroDataAgendamentoAte = $("#txtDataAgendamentoAte").val();
                }

                if ($("#txtDataProcessamentoDe").val() != "") {
                    request.Request.FiltroDataInicioMovimento = $("#txtDataProcessamentoDe").val();
                }
                if ($("#txtDataProcessamentoAte").val() != "") {
                    request.Request.FiltroDataFimMovimento = $("#txtDataProcessamentoAte").val();
                }

                if ($("#cmbStatusCCI").find('option:selected').val() != "?") {
                    request.Request.FiltroCodigoStatusCCI = $("#cmbStatusCCI").val().toString();
                    request.Request.FiltroDescricaoStatusCCI = retornarTextoCombo($("#cmbStatusCCI option:selected"));
                }

                if ($("#cmbReferencia").find('option:selected').val() != null) {
                    var cboReferencia = $.trim($("#cmbReferencia").val().toString()).split(" ");
                    var FiltroReferencia = "", Referencia = "";
                    for (var i = 0; i < cboReferencia.length; i++) {
                        if (i % 2 != 0)
                            Referencia = Referencia + cboReferencia[i] + ',';
                        else
                            FiltroReferencia = FiltroReferencia + cboReferencia[i];
                    }
                    Referencia = Referencia.substring(0, Referencia.length - 1);
                    request.Request.Referencia = Referencia;
                    request.Request.FiltroReferencia = FiltroReferencia;
                }

                request.Request.FiltroQuebraReferencia = $("#chkSubTotalReferencia").attr("checked");

                executarServico(
                    "GerarRelatorioConsolidadoExcelRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Relatório Excel",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
                            callbackOK: function() { }
                        });
                    });
                //}
            });

            //------------------------------------------------------------------
            //- BOTÃO PDF
            //------------------------------------------------------------------
            $("#btnGerarPdf").click(function() {

                var request = {};

                request.Request = {};
                request.Request.TipoRelatorio = "PDF";

                var link = "relatorio_consolidado.aspx";
                link += $.relatorio.gerarUrl();
                var url = window.location.href;

                var arr = url.split("/");
                var result = arr[0] + "//" + arr[2]

                //Jira SCF 1046 - Marcos Matsuoka
                if (arr[3] == "scf") {
                    arr[3] = "scf_relatorios";
                    result = result + "/" + arr[3];
                } else if (arr[3] == "scf_projetos") {
                    arr[3] = "scf_relatorios2";
                    result = result + "/" + arr[3];
                }

                var link = link + "&TipoArquivo=PDF"; //Marcos Matsuoka - Jira SCF 1085

                link = result + "/" + link;

                //window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');
                request.Request.Link = link;
                request.Request.TipoArquivo = "PDF";
                request.Link = link;
                request.TipoArquivo = "PDF";

                executarServico(
                    "GerarRelatorioConsolidadoPDFRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Relatório PDF",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório PDF.",
                            callbackOK: function() { }
                        });
                    });
                //window.open("relatorio_consolidado.aspx" + parametros + "&acao=pdf", "_self");
            });


            //-------------------------------------------------------------------
            // Preenche combo de favorecidos
            //-------------------------------------------------------------------
            $.relatorio.carregarFavorecidos(function(callback) {
                //-------------------------------------------------------------------
                // Preenche combo de produtos
                //-------------------------------------------------------------------
                $.relatorio.carregarProdutos(function(callback) {
                    //-------------------------------------------------------------------
                    // Preenche combo de tipo de registro
                    //-------------------------------------------------------------------
                    $.relatorio.carregarTipoRegistro(function(callback) {
                        //-------------------------------------------------------------------
                        // Preenche combo de meio de pagamento
                        //-------------------------------------------------------------------
                        $.relatorio.carregarMeioPagamento(function (callback) {
                            //-------------------------------------------------------------------
                            // Preenche combo de referencia
                            //-------------------------------------------------------------------
                            $.relatorio.carregarReferencia(function (callback) {
                                // Chama callback
                                if (callback)
                                    callback();
                            });
                        });
                    });
                });
            });
        },

        //=================================================================================
        // limparFavorecidos: Limpa os filtros antes da construcao
        //=================================================================================
        limparFavorecidos: function(callback) {

            // Limpa o combo
            $("#cmbFavorecidoOriginal").children().each(
                function(index) {
                    $(this).remove();
                }

            // Chama callback
            );

            elFO = $("#cmbFavorecidoOriginal").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: "",
                minWidth: 120 //Jira SCF1064 - Marcos Matsuoka
            });

            if (callback)
                callback();
        },

        //=================================================================================
        // limparProdutos: Limpa os filtros antes da construcao
        //=================================================================================
        limparProdutos: function (callback) {

            // Limpa o combo
            $("#cmbProduto").children().each(
                function (index) {
                    $(this).remove();
                }
            );

            elP = $("#cmbProduto").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: "",
                minWidth: 120 //Jira SCF1064 - Marcos Matsuoka
            });

            $("button.ui-multiselec").css("width", "114px"); //Jira SCF1064 - Marcos Matsuoka

            if (callback)
                callback();
        },

        //=================================================================================
        // limparReferencia: Limpa os filtros antes da construcao
        //=================================================================================
        limparReferencia: function (callback) {

            // Limpa o combo
            $("#cmbReferencia").children().each(
                function (index) {
                    $(this).remove();
                }
            );

            elRef = $("#cmbReferencia").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: "",
                minWidth: 180 //Jira SCF1064 - Marcos Matsuoka
            });

            $("button.ui-multiselec").css("width", "114px"); //Jira SCF1064 - Marcos Matsuoka

            if (callback)
                callback();
        },

        //=================================================================================
        // limparTipoRegistro: Limpa os filtros antes da construcao
        //=================================================================================
        limparTipoRegistro: function(callback) {

            // Limpa o combo
            $("#cmbTipoRegistro").children().each(
                function(index) {
                    $(this).remove();
                }
            );

            $("#txtDataAgendamentoDe").val("");
            $("#txtDataAgendamentoAte").val("");
            $("#txtDataProcessamentoDe").val("");
            $("#txtDataProcessamentoAte").val("");

            elTR = $("#cmbTipoRegistro").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: "",
                minWidth: 120 //Jira SCF1064 - Marcos Matsuoka
            });

            if (callback)
                callback();
        },

        //=================================================================================
        // limparMeioPagamento: Limpa os filtros antes da construcao
        //=================================================================================
        limparMeioPagamento: function(callback) {

            // Limpa o combo
            $("#cmbMeioPagamento").children().each(
                function(index) {
                    $(this).remove();
                }
            );

            elMP = $("#cmbMeioPagamento").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: "",
                minWidth: 120 //Jira SCF1064 - Marcos Matsuoka
            });

            if (callback)
                callback();
        },

        //=================================================================================
        // carregarFavorecidos: carrega combo de favorecidos
        //=================================================================================
        carregarFavorecidos: function(callback) {

            executarServico(
                "ListarFavorecidoRequest",
                {},
                function(data) {

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {
                        if (data.Resultado[i].FavorecidoOriginal == "1") {
                            var opt = $('<option />', {
                                value: data.Resultado[i].CodigoFavorecido,
                                text: ' ' + data.Resultado[i].NomeFavorecido
                            });
                            opt.appendTo(elFO);
                        }
                    }

                    // Ordena a lista pelo nome do favorecido
                    $("#cmbFavorecidoOriginal").html($("option", $("#cmbFavorecidoOriginal")).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
                    }));

                    elFO.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //=================================================================================
        // carregarProdutos: carrega combo de produtos
        //=================================================================================
        carregarProdutos: function(callback) {

            executarServico(
                "ListarProdutoRequest",
                {},
                function(data) {

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {
                        if (data.Resultado[i].NomeProduto != "RECTO") {
                            var opt = $('<option />', {
                                value: data.Resultado[i].CodigoProduto,
                                text: ' ' + data.Resultado[i].NomeProduto
                            });
                            opt.appendTo(elP);
                        }
                    }

                    // Ordena a lista pelo nome do produto
                    $("#cmbProduto").html($("option", $("#cmbProduto")).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
                    }));

                    elP.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //=================================================================================
        // carregarProdutos: carrega combo de tipo de registro
        //=================================================================================
        carregarTipoRegistro: function(callback) {

            executarServico(
                "ListarConfiguracaoTipoRegistroRequest",
                {},
                function(data) {

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {
                        if ($("#rdoTipoVenda").attr("checked")) {
                            if (data.Resultado[i].TipoRegistro == "AJ"
                                || data.Resultado[i].TipoRegistro == "AV"
                                || data.Resultado[i].TipoRegistro == "CV") {
                                var opt = $('<option />', {
                                    value: data.Resultado[i].CodigoConfiguracaoTipoRegistro,
                                    text: ' ' + data.Resultado[i].TipoRegistro
                                });
                                opt.appendTo(elTR);
                            }
                        }
                        else {
                            if (data.Resultado[i].TipoRegistro == "AJ"
                                || data.Resultado[i].TipoRegistro == "AP"
                                || data.Resultado[i].TipoRegistro == "CP") {
                                var opt = $('<option />', {
                                    value: data.Resultado[i].CodigoConfiguracaoTipoRegistro,
                                    text: ' ' + data.Resultado[i].TipoRegistro
                                });
                                opt.appendTo(elTR);
                            }
                        }
                    }

                    // Ordena a lista pelo nome do produto
                    $("#cmbTipoRegistro").html($("option", $("#cmbTipoRegistro")).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
                    }));

                    elTR.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },
        //=================================================================================
        // carregarMeioPagamento: carrega combo de meio de pagamento
        //=================================================================================
        carregarMeioPagamento: function(callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    CodigoLista: 9
                },
                function(data) {

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor + ' ' + data.Resultado[i].CodigoListaItem + ' ',
                            text: ' ' + data.Resultado[i].Valor + ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elMP);
                    }

                    // Ordena a lista pelo nome do produto
                    $("#cmbMeioPagamento").html($("option", $("#cmbMeioPagamento")).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
                    }));

                    elMP.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //=================================================================================
        // carregarReferencia: carrega combo de meio de pagamento
        //=================================================================================
        carregarReferencia: function (callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    NomeLista: "Referencia"
                },
                function (data) {

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elRef);
                    }

                    // Ordena a lista pelo nome do produto
                    $("#cmbReferencia").html($("option", $("#cmbReferencia")).sort(function (a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
                    }));

                    elRef.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //=================================================================================
        // Habilita os botões de acordo com o tipo
        //=================================================================================
        habilitarBotoes: function(callback) {

            $("input[name='layoutRecebimento']:radio").attr("disabled", $("#rdoTipoVenda").attr("checked"));
            $("#chkRecebimentoQtde").attr("disabled", $("#rdoTipoVenda").attr("checked"));
            $("input[name='layoutVenda']:radio").attr("disabled", $("#rdoTipoRecebimento").attr("checked"));
            $("#chkVendaQtde").attr("disabled", $("#rdoTipoRecebimento").attr("checked"));

            if ($("#rdoTipoVenda").attr("checked")) {
                $("#cmbProduto").multiselect('enable');
                $("#cmbMeioPagamento").multiselect("uncheckAll");
                $("#cmbMeioPagamento").multiselect('disable');
            }
            else {
                $("#cmbProduto").multiselect("uncheckAll");
                $("#cmbProduto").multiselect('disable');
                $("#cmbMeioPagamento").multiselect('enable');
            }

            // Chama callback
            if (callback)
                callback();
        },

        //=================================================================================
        // gerarRelatorioPDF: gera o relatório em PDF de transações conforme filtros
        //=================================================================================
        gerarRelatorioHtml: function(callback) {

            // Verificar quantidade de linhas
            //$.relatorio.receberNumeroDeLinhas(function() {

            var link = "relatorio_consolidado.aspx";
            link += $.relatorio.gerarUrl();

            //Jira SCF 749 - Marcos Matsuoka
            var url = window.location.href;

            var arr = url.split("/");
            var result = arr[0] + "//" + arr[2]

            if (arr[3] == "scf") {
                arr[3] = "scf_html";
                result = result + "/" + arr[3];
            } else if (arr[3] == "scf_projetos") {
                arr[3] = "scf_html2";
                result = result + "/" + arr[3];
            }

            var link = link + "&TipoArquivo=HTML"; //Marcos Matsuoka - Jira SCF 1085

            link = result + "/" + link;
            //

            window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');

            // Chama callback
            if (callback)
                callback();

            //});
        },

        //=================================================================================
        // gerarUrl: gera o relatório em PDF de transações conforme filtros
        //=================================================================================
        gerarUrl: function(callback) {

            var link = "";

            if ($("#rdoTipoVenda").attr("checked")) {
                link = link + "TipoRelatorio=V&";
                if ($("#rdoVendaTotalizado").attr("checked"))
                    link = link + "LayoutRelatorioVenda=" + $("#rdoVendaTotalizado").val() + "&";
                else if ($("#rdoVendaAnalitico").attr("checked"))
                    link = link + "LayoutRelatorioVenda=" + $("#rdoVendaAnalitico").val() + "&";
                else
                    link = link + "LayoutRelatorioVenda=" + $("#rdoVendaSintetico").val() + "&";
                if ($("#chkVendaQtde").attr("checked"))
                    link = link + "QtdeTransacaoVenda=" + $("#chkVendaQtde").attr("checked") + "&";
            }
            else {
                link = link + "TipoRelatorio=R&";
                if ($("#rdoRecebimentoTotalizado").attr("checked"))
                    link = link + "LayoutRelatorioRecebimento=" + $("#rdoRecebimentoTotalizado").val() + "&";
                else if ($("#rdoRecebimentoAnalitico").attr("checked"))
                    link = link + "LayoutRelatorioRecebimento=" + $("#rdoRecebimentoAnalitico").val() + "&";
                else if ($("#rdoRecebimentoSintetico").attr("checked"))
                    link = link + "LayoutRelatorioRecebimento=" + $("#rdoRecebimentoSintetico").val() + "&";
                else
                    link = link + "LayoutRelatorioRecebimento=" + $("#rdoRecebimentoCCI").val() + "&";
                if ($("#chkRecebimentoQtde").attr("checked"))
                    link = link + "QtdeTransacaoRecebimento=" + $("#chkRecebimentoQtde").attr("checked") + "&";
            }

            //ECOMMERCE - Fernando Bove - 20160105: Inicio
            if ($("#rdoTipoContabil").attr("checked"))
                link = link + "TipoFinanceiroContabil=C&";

            if ($("#rdoTipoFinanceiro").attr("checked"))
                link = link + "TipoFinanceiroContabil=F&";
            //ECOMMERCE - Fernando Bove - 20160105: Fim

            if ($("#chkSubTotal").attr("checked"))
                link = link + "SubTotalFavorecido=" + $("#chkSubTotal").attr("checked") + "&";

            if ($("#chkSubTotalReferencia").attr("checked"))
                link = link + "SubTotalReferencia=" + $("#chkSubTotalReferencia").attr("checked") + "&";

            if ($("#cmbProduto").find('option:selected').val() != null)
                link = link + "CodigoProduto=" + $("#cmbProduto").val().toString() + "&" + "Produto=" + $("#cmbProduto option:selected").text() + "&";

            if ($("#cmbFavorecidoOriginal").find('option:selected').val() != null)
                link = link + "CodigoFavorecidoOriginal=" + $("#cmbFavorecidoOriginal").val().toString() + "&" + "FavorecidoOriginal=" + $("#cmbFavorecidoOriginal option:selected").text() + "&";

            if ($("#cmbTipoRegistro").find('option:selected').val() != null)
                link = link + "CodigoTipoRegistro=" + $("#cmbTipoRegistro").val().toString() + "&" + "DescricaoTipoRegistro=" + $("#cmbTipoRegistro option:selected").text() + "&";

            if ($("#cmbEstabelecimento").val() != "?")
                link = link + "CodigoEstabelecimento=" + $("#cmbEstabelecimento").val() + "&" + "Estabelecimento=" + $("#cmbEstabelecimento option:selected").text() + "&";

            if ($("#cmbMeioPagamento").find('option:selected').val() != null) {
                var cboMeioPagamento = $.trim($("#cmbMeioPagamento").val().toString()).split(" ");
                var CodigoMeioPagamento = "";
                for (var i = 0; i < cboMeioPagamento.length; i++) {
                    if (i % 2 == 0)
                        CodigoMeioPagamento = CodigoMeioPagamento + cboMeioPagamento[i];
                }
                link = link + "CodigoMeioPagamento=" + CodigoMeioPagamento + "&" + "MeioPagamento=" + $("#cmbMeioPagamento option:selected").text() + "&";
            }

            if ($("#cmbReferencia").find('option:selected').val() != null) {
                var cboReferencia = $.trim($("#cmbReferencia").val().toString()).split(" ");
                var CodigoReferencia = "";
                for (var i = 0; i < cboReferencia.length; i++) {
                    if (i % 2 == 0)
                        CodigoReferencia = CodigoReferencia + cboReferencia[i];
                }
                link = link + "CodigoReferencia=" + CodigoReferencia + "&" + "Referencia=" + $("#cmbReferencia option:selected").text() + "&";
            }

            if ($("#txtDataAgendamentoDe").val() != "")
                link = link + "DataAgendamentoDe=" + $("#txtDataAgendamentoDe").val() + "&";

            if ($("#txtDataAgendamentoAte").val() != "")
                link = link + "DataAgendamentoAte=" + $("#txtDataAgendamentoAte").val() + "&";

            if ($("#txtDataProcessamentoDe").val() != "")
                link = link + "DataInicioMovimento=" + $("#txtDataProcessamentoDe").val() + "&";

            if ($("#txtDataProcessamentoAte").val() != "")
                link = link + "DataFimMovimento=" + $("#txtDataProcessamentoAte").val() + "&";

            if ($("#cmbStatusCCI").find('option:selected').val() != "?")
                var link = link + "CodigoStatusCCI=" + $("#cmbStatusCCI").val().toString() +"&" + "DescricaoStatusCCI=" + retornarTextoCombo($("#cmbStatusCCI option:selected")) + "&";

            if (myVar.codLog != "") {
                var link = link + "codLog=" + myVar.codLog + "&";
            }

            if (link != "") {
                link = "?" + link.substr(0, link.length - 1);
            }

            // Chama callback
            if (callback)
                callback();

            return link;
        },

        //=================================================================================
        // receberNumeroDeLinhas: carrega combo de estabelecimentos de acordo com
        //                          o grupo e/ou subgrupo selecionados, caso tenha
        //=================================================================================
        receberNumeroDeLinhas: function(callback) {

            var request = {};

            // Envia informações ao objeto existente
            $("#filtros_relatorio_consolidado").Forms("getData", { dataObject: request });

            if ($("#rdoTipoVenda").attr("checked"))
                request.FiltroTipoConsolidado = $("#rdoTipoVenda").val();
            else
                request.FiltroTipoConsolidado = $("#rdoTipoRecebimento").val();

            //ECOMMERCE - Fernando Bove - 20160105: Inicio
            if ($("#rdoTipoContabil").attr("checked")) {
                request.Request.FiltroFinanceiroContabil = $("#rdoTipoContabil").val();
            }
            if ($("#rdoTipoFinanceiro").attr("checked")) {
                request.Request.FiltroFinanceiroContabil = $("#rdoTipoFinanceiro").val();
            }
            //ECOMMERCE - Fernando Bove - 20160105: Fim

            if ($("#rdoVendaTotalizado").attr("checked"))
                request.FiltroLayoutVenda = $("#rdoVendaTotalizado").val();
            else if ($("#rdoVendaAnalitico").attr("checked"))
                request.FiltroLayoutVenda = $("#rdoVendaAnalitico").val();
            else
                request.FiltroLayoutVenda = $("#rdoVendaSintetico").val();

            if ($("#rdoRecebimentoTotalizado").attr("checked"))
                request.FiltroLayoutRecebimento = $("#rdoRecebimentoTotalizado").val();
            else if ($("#rdoRecebimentoAnalitico").attr("checked"))
                request.FiltroLayoutRecebimento = $("#rdoRecebimentoAnalitico").val();
            else if ($("#rdoRecebimentoSintetico").attr("checked"))
                request.FiltroLayoutRecebimento = $("#rdoRecebimentoSintetico").val();
            else
                request.FiltroLayoutRecebimento = $("#rdoRecebimentoCCI").val();

            if ($("#txtDataAgendamentoDe").val() != "")
                request.FiltroDataAgendamentoDe = $("#txtDataAgendamentoDe").val();

            if ($("#txtDataAgendamentoAte").val() != "")
                request.FiltroDataAgendamentoAte = $("#txtDataAgendamentoAte").val();

            if ($("#txtDataProcessamentoDe").val() != "")
                request.FiltroDataInicioMovimento = $("#txtDataProcessamentoDe").val();

            if ($("#txtDataProcessamentoAte").val() != "")
                request.FiltroDataFimMovimento = $("#txtDataProcessamentoAte").val();

            if ($("#cmbProduto").find('option:selected').val() != null)
                request.FiltroProduto = $("#cmbProduto").val().toString();
            if ($("#cmbFavorecidoOriginal").find('option:selected').val() != null)
                request.FiltroFavorecidoOriginal = $("#cmbFavorecidoOriginal").val().toString();
            if ($("#cmbMeioPagamento").find('option:selected').val() != null) {
                var cboMeioPagamento = $.trim($("#cmbMeioPagamento").val().toString()).split(" ");
                var CodigoMeioPagamento = "";
                for (var i = 0; i < cboMeioPagamento.length; i++) {
                    if (i % 2 == 0)
                        CodigoMeioPagamento = CodigoMeioPagamento + cboMeioPagamento[i];
                }
                request.FiltroMeioPagamento = CodigoMeioPagamento;
            }

            request.RetornarNumeroLinhas = 'S';
            request.RetornarRelatorio = false;

            request.VerificarQuantidadeLinhas = true;

            request.TipoRelatorio = "Online";

            // Verifica quantidade de linhas
            executarServico(
                "ListarRelatorioConsolidadoRequest",
                request,
                function(data) {

                    if (data.QuantidadeLinhas > 1000) {

                        // Pergunta se o usuario deseja continuar..
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Não foi possível visualizar o relatório devido a quantidade excessiva de linhas (" + data.QuantidadeLinhas + "). Por favor selecione a opção para exportação de relatório para Excel.",
                            callbackOK: function() {
                                return true;
                            }
                        });
                    } else {

                        if (data.QuantidadeLinhas == 0) {
                            // Pergunta se o usuario deseja continuar..
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "O relatório que será gerado não possui linhas. Deseja continuar a operação?",
                                callbackSim: function() {
                                    if (data.CodLog != "")
                                        myVar.codLog = data.CodLog;

                                    if (callback)
                                        callback();
                                },
                                callbackNao: function() {
                                    return true;
                                }
                            });
                        } else {

                            if (data.CodLog != "")
                                myVar.codLog = data.CodLog;

                            if (callback)
                                callback();
                        }
                    }
                });
        }
    }

    // Chama load da página
    $.relatorio.load();
});