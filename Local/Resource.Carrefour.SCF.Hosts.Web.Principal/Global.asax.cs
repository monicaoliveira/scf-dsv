﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Sistemas.Principal;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Sistemas.Interface;
using Resource.Framework.Sistemas.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public class Global : System.Web.HttpApplication
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_EndRequest()  // BCAR_00049.002 
        {
            WebConfig config = GerenciadorConfig.ReceberConfig<WebConfig>();
            if (config == null)
                config = new WebConfig();

            if (!config.UtilizarServicosLocais)
                Response.Headers.Remove("Server");
        }
        
        protected void Application_Start(object sender, EventArgs e)
        {
            
            log4net.Config.BasicConfigurator.Configure();
            log.Info("Inicializando Aplicação Web");
            
            WebConfig config = GerenciadorConfig.ReceberConfig<WebConfig>();
            if (config == null)
                config = new WebConfig();

            GerenciadorConfig.SetarConfig(
                new LocalizadorTiposConfig()
                {
                    ListaFixa =
                        new List<LocalizadorTipoInfo>()
                        {
                            new LocalizadorTipoInfo()
                            {
                                IncluirNamespace = 
                                    "Resource.Framework.Contratos.Comum.Dados, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Framework.Contratos.Comum.Permissoes, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Framework.Contratos.Comum.Mensagens, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados.Log, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Mensagens, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Integracao.Dados, Resource.Carrefour.SCF.Contratos.Integracao;" +
                                    "Resource.Carrefour.SCF.Contratos.Integracao.Mensagens, Resource.Carrefour.SCF.Contratos.Integracao;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador, Resource.Carrefour.SCF.Contratos.Principal;" +                                    
                                    "Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador, Resource.Carrefour.SCF.Contratos.Principal;" +                                    
                                    "Resource.Carrefour.SCF.Contratos.Principal.Permissoes, Resource.Carrefour.SCF.Contratos.Principal",
                            },
                            new LocalizadorTipoInfo()
                            {
                                IncluirTipo =
                                        "Resource.Framework.Contratos.Comum.ServicoSegurancaConfig, Resource.Framework.Contratos.Comum;" +
                                        "Resource.Framework.Contratos.Comum.ConfigHelperConfig, Resource.Framework.Contratos.Comum"
                            }
                        }
                });

            if (config.UtilizarServicosLocais)
            {
                ContainerServicoHost containerServicoHost = ContainerServicoHost.GetInstance();
                containerServicoHost.Iniciar(
                    new ContainerServicoHostConfig()
                    {
                        ServicoHostConfig =
                            new ServicoHostConfig()
                            {
                                TipoVarreduraTipos = ServicoHostTipoVarreduraEnum.DefaultIncluir,
                                IncluirTipos =
                                    new List<Type>() 
                                { 
                                    typeof(ServicoSeguranca),
                                    typeof(ServicoPersistencia),
                                    typeof(ServicoInterface),
                                    typeof(ServicoMetadadoComum),
                                    typeof(ServicoConsolidadorDeSaldo)
                                    
                                },
                                IncluirAssemblies =
                                    new List<Assembly>()
                                {
                                    typeof(ServicoPrincipal).Assembly,
                                    typeof(ServicoIntegracao).Assembly
                                }
                            }
                    });

                Mensageria.Processar(new InicializarSegurancaRequest());

                // Inicializa as listas genéricas. Tenta pegar a url... só funciona no IIS6
                string url = null;
                try { url = this.Context.Request.Url.AbsoluteUri; }
                catch (Exception) { } // catch (Exception ex) { } // warning 25/02/2019

                ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });// lucas.rodrigues - Verificar o status que ficou o serviço antes da aplicação parar
                // ==========================================================================================
                // 1430 - inclusao do parametro OrigemChamada - inicio
                // ==========================================================================================
                if (resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador == ServicoAgendadorStatusEnum.Iniciado)
                {
                    IniciarServicoAgendadorRequest venvio = new IniciarServicoAgendadorRequest();
                    venvio.OrigemChamada = "Global";
                    Mensageria.Processar(venvio); //1430 Mensageria.Processar(new IniciarServicoAgendadorRequest());
                }
                // ==========================================================================================
                // 1430 - inclusao do parametro OrigemChamada - fim
                // ==========================================================================================
                //if (resConfig.ConfiguracaoGeralInfo.StatusServicoExpurgo == ServicoExpurgoStatusEnum.Iniciado)
                //    Mensageria.Processar(new IniciarServicoExpurgoRequest());
                // ==========================================================================================
                // 1070 - Confere processos que foram parados           
                // ==========================================================================================
                ServicoExecutarRegrasInicializacao executarRegrasInicializacao = new ServicoExecutarRegrasInicializacao();
                if (resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador == ServicoAgendadorStatusEnum.Iniciado)
                {
                    executarRegrasInicializacao.LiberarTransacoesEmAndamento();
                }
                else
                {
                    executarRegrasInicializacao.LogarTransacoesEmAndamento();
                }
            }
            else
            {
                
                ContainerServicoHost containerServicoHost = ContainerServicoHost.GetInstance();
                containerServicoHost.Iniciar(
                    new ContainerServicoHostConfig()
                    {
                        ServicoHostConfig =
                            new ServicoHostConfig()
                            {
                                TipoVarreduraTipos = ServicoHostTipoVarreduraEnum.DefaultIncluir,
                                IncluirTipos =
                                    new List<Type>() 
                                {                                     
                                    typeof(ServicoInterface)
                                }
                            }
                    });
                Mensageria.Inicializar(new MensageriaConfig() { UtilizarWCF = true }); // Pede para a mensageria inicializar wcf
            }
            Mensageria.Processar(new SalvarPermissoesRequest());//Garante que as permissões estejam no banco de dados

        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }
        
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
            if (Session["CodigoSessao"] != null)
            {
                RemoverSessaoRequest remSessao = new RemoverSessaoRequest()
                {
                    CodigoSessao = Session["CodigoSessao"].ToString(),
                    CodigoSessaoARemover = Session["CodigoSessao"].ToString()
                };

                Mensageria.Processar(remSessao);
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}