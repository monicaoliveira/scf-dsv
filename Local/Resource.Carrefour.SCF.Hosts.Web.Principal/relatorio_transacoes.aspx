﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorio_transacoes.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.relatorio_transacoes" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >


<head runat="server">
    <!-- JQUERY -->
    <script type="text/javascript" src="js/lib/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="js/lib/jquery-ui-1.8.11.custom.min.js"></script>
    <script type="text/javascript" src="js/lib/jquery.json-2.2.js"></script>
    <script type="text/javascript" src="js/lib/jquery.resource.forms.js"></script>
    <script type="text/javascript" src="js/lib/jquery.resource.tree.js"></script>
    <script type="text/javascript" src="js/lib/jquery.resource.grid.js"></script>
    <script type="text/javascript" src="js/lib/jquery.treeview.js"></script>
    <script type="text/javascript" src="js/lib/jquery.glob.js"></script>
    <script type="text/javascript" src="js/lib/jquery.glob.pt-br.min.js"></script>
    <script type="text/javascript" src="js/lib/splitter.js"></script>
    <script type="text/javascript" src="js/lib/jquery.maskedinput-1.2.2.min.js"></script>
    <script type="text/javascript" src="js/lib/jquery.maskMoney.0.2.js"></script>
    <script type="text/javascript" src="js/lib/jquery.ui.datepicker-pt-BR.js"></script>
    <script type="text/javascript" src="js/lib/ajaxFileUpload.js"></script>
	
    <!-- JGRID -->
    <script src="js/lib/i18n/grid.locale-pt-br.js" type="text/javascript"></script>
    <script src="js/lib/jquery.jqGrid.min.js" type="text/javascript"></script>

    <!-- ESTILOS -->
    <link type="text/css" href="css/custom-theme/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
    <link type="text/css" href="css/jquery.treeview.custom.css" rel="stylesheet" />	
    <link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />
    <link rel="stylesheet" media="screen" href="menu3.css" />
    <link rel="stylesheet" media="screen" href="template_default.css" />

    <!-- BIBLIOTECAS -->
    <script type="text/javascript" src="comum_servico.js"></script>
    <script type="text/javascript" src="template_default.js"></script>
    <script type="text/javascript" src="menu3.js"></script>
        
    <link href="relatorio_transacoes.css" rel="stylesheet" type="text/css" />
    <script src="relatorio_transacoes.js" type="text/javascript"></script>
    <title>Relatórios de Transações</title>
</head>

 <body class="conteudo">
<form id="frmRelTransacoes" runat="server" action="relatorio_transacoes.aspx">   
        <div id="dialogLoading" style="position:absolute; border:solid 2px black; display: block; height: auto; text-align:center; vertical-align:middle; margin-left:42%;  outline: 0 none; top: 198px; width: 160px; z-index: 1002;">
            <div style="background-image: url('img/bar.png'); color: White; font-weight: bold; height: 15px; margin: 0 0 2px; padding: 3px 0 2px 4px;">
                <span style="font-size: 12px; color: White; font-weight: bold; float: left; margin: 0.1em 16px 0.1em 0;" >Carregando...</span>
            </div>
            <div style="background-color:White; height: 49px; min-height: 0; text-align: center; width: auto; height: 50px;">
                <img alt="" style="padding-top: 15px;" src="img/carregando.gif" />
            </div>
        </div>        
    
        <div id="relatorio_transacoes">
            <input type="hidden" id="ObjetoJson" runat="server" value="" />
            
            <table style="border:solid 1px #4682B4; width:100%;">
            <tr>
            <td>            
            <%--Cabeçalho--%>
            <header>
            <table style="" class="conteudo" width="100%">    
                <tr>
                    <td class="espacoLeft" colspan="8">
                        <h1>TRANSAÇÕES - Analítico</h1> 
                    </td>
                    <td class="" style="width:60px;">
                        <label class="espacoColuna labelNegrito">Data/Hora</label>
                        <asp:Label runat="server" ID="lblDataHora" CssClass="espacoCampo"></asp:Label>                        
                        <asp:Label runat="server" ID="lblPagina" CssClass="espacoCampo"></asp:Label>
                    </td>                                              
                </tr>
                <tr>
                     <td class="" style="padding-left:8px; width:6%;"><label class="labelNegrito">Filtros</label></td>   
                                   
                    <td class="" style="width:60px;">
                        <label class="labelNegrito">Origem</label>
                    </td>                    
                    <td class="" style="width:12%;">
                        <asp:Label runat="server" ID="lblFiltroOrigem" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td class="" style="width:60px;">
                        <label class="labelNegrito">Tipo de Registro</label> 
                    </td>
                    <td class="" style="width:12%;">
                        <asp:Label runat="server" ID="lblFiltroTipoRegistro" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td class="" style="width:60px;">
                        <label class="labelNegrito">Favorecido</label>
                    </td>                    
                    <td class="" style="width:12%;">
                        <asp:Label runat="server" ID="lblFiltroFavorecido" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td class="" style="width:60px;">
                        <label class="labelNegrito">Produto</label>
                    </td>                    
                    <td class="" style="width:12%;">
                        <asp:Label runat="server" ID="lblFiltroProduto" CssClass="espacoCampo"></asp:Label>
                    </td>                                        
                </tr>
                
                <tr>  
                    <td class=""></td>
                                       
                    <td  class="espacoColuna">
                        <label class="labelNegrito">Grupo</label>
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroGrupo" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td class="espacoColuna">
                        <label class="labelNegrito">Subgrupo</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroSubgrupo" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td class="espacoColuna">
                        <label class="labelNegrito">Estabelecimento</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroEstabelecimento" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td class="espacoColuna">
                        <label class="labelNegrito">Meio de Captura</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroMeioCaptura" CssClass="espacoCampo"></asp:Label>
                    </td>
                </tr>
                
                <tr> 
                     <td class=""></td>
                     
                    <td class="">
                        <label class="labelNegrito">Status Conciliacao</label>
                    </td>
                    <td class="" >
                        <asp:Label runat="server" ID="lblFiltroStatusConciliacao" CssClass="espacoCampo"></asp:Label>
                    </td>  
                    
                    <td  class="">
                        <label class="labelNegrito">Status Retorno</label>
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroStatusRetorno" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td  class="">
                        <label class="labelNegrito">Status Pagamento</label>
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroStatusPagamento" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td  class="">
                        <label class="labelNegrito">Status Transação</label>
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroStatusImportacao" CssClass="espacoCampo"></asp:Label>
                    </td>
                </tr>
                 <tr>
                     <td class=""></td>
                     
                    <td class="espacoColuna">
                        <label class="labelNegrito">Data Transação</label>
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroDataTransacaoDe" CssClass="espacoCampo espacoRight"></asp:Label> - 
                        <asp:Label runat="server" ID="lblFiltroDataTransacaoAte" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td class="espacoColuna">
                        <label class="labelNegrito">Data Vencimento</label>
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroDataVencimentoDe" CssClass="espacoCampo espacoRight"></asp:Label> - 
                        <asp:Label runat="server" ID="lblFiltroDataVencimentoAte" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td  class="espacoColuna">
                        <label class="labelNegrito">Data Agendamento</label>
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroDataAgendamentoDe" CssClass="espacoCampo espacoRight"></asp:Label> - 
                        <asp:Label runat="server" ID="lblFiltroDataAgendamentoAte" CssClass="espacoCampo"></asp:Label>
                    </td>
                    <td class="espacoColuna">
                        <label class="labelNegrito">Número Pagamento</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroNumeroPagamento" CssClass="espacoCampo"></asp:Label>
                    </td>
<!-- 1438 inicio -->                    
                    <td class="espacoColuna">
                        <label class="labelNegrito">Conta Cliente</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroContaCliente" CssClass="espacoCampo"></asp:Label>
                    </td>
<!-- 1438 fim -->                                                            
                </tr>
                
                <tr>
                    <td class=""></td>
                    
                    <td class="espacoColuna">
                        <label class="labelNegrito">Data Envio Matera</label> 
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroDataEnvioMateraDe" CssClass="espacoCampo espacoRight"></asp:Label> - 
                        <asp:Label runat="server" ID="lblFiltroDataEnvioMateraAte" CssClass="espacoCampo"></asp:Label>
                    </td>
                    <td class="espacoColuna">
                        <label class="labelNegrito">Data Liquidação</label>
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroDataLiquidacaoDe" CssClass="espacoCampo espacoRight"></asp:Label> - 
                        <asp:Label runat="server" ID="lblFiltroDataLiquidacaoAte" CssClass="espacoCampo"></asp:Label>
                    </td>
                    
                    <td  class="espacoColuna">
                        <label class="labelNegrito">Data Processamento</label> 
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroDataProcessamentoDe" CssClass="espacoCampo espacoRight"></asp:Label> - 
                        <asp:Label runat="server" ID="lblFiltroDataProcessamentoAte" CssClass="espacoCampo"></asp:Label>
                    </td>
                    <td  class="espacoColuna">
                        <label class="labelNegrito">Plano</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblFiltroPlano" CssClass="espacoCampo"></asp:Label>
                    </td>     
                </tr> 
                <tr>  
                    <td class=""></td>             
                    <td class="espacoColuna">
                        <label class="labelNegrito">Data Retorno CCI</label> 
                    </td>
                    <td class="">
                        <asp:Label runat="server" ID="lblDataRetornoCCIDe" CssClass="espacoCampo espacoRight"></asp:Label> - 
                        <asp:Label runat="server" ID="lblDataRetornoCCIAte" CssClass="espacoCampo"></asp:Label>
                    </td>
                    <td  class="espacoColuna">
                        <label class="labelNegrito">Financ./Contábil</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblFinanceiroContabil" CssClass="espacoCampo"></asp:Label>
                    </td> 
                    <td  class="espacoColuna">
                        <label class="labelNegrito">NSU Host</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblNSUHost" CssClass="espacoCampo"></asp:Label>
                    </td> 
                </tr> 
                
                <tr>  
                    <td class=""></td>             
                    <td  class="espacoColuna">
                        <label class="labelNegrito">Referencia</label>
                    </td>                    
                    <td class="">
                        <asp:Label runat="server" ID="lblReferencia" CssClass="espacoCampo"></asp:Label>
                    </td>                     
                    
                </tr> 
            </table>
            </header>
            <%--<table style="border:solid 1px #4682B4; padding:10px; width:100%; ">
               
            </table>--%>
            
            <%--Resultado--%>
            <table class="espacoTop conteudo">
                <tr runat="server" id="trMensagem" visible="false">
                    <td colspan="14" class="grupo" style="border-top:solid 3px #4682B4;"><asp:Label class="pequeno" runat="server" ID="lblMensagem"></asp:Label></td>
                </tr>      	        	        	        
                <%--Repeater Data Transação--%>
                <asp:Repeater runat="server" ID="rptDataTransacao" >
                    <ItemTemplate>
                        
                        <%--Data Transação--%>
                        <tr><td colspan="14" class="bordaTopGrossa"></td></tr>
                        <tr>
                            <td class="resultado1">
                                <label class="labelNegrito">Data Transação:</label>
                            </td>
                            <td colspan="2" class="resultado2">
                                <label class="espacoCampo"><%# ((DateTime)Eval("DataTransacao")).ToString("dd/MM/yyyy") %></label>
                            </td>
                        </tr>
                        <tr><td colspan="15" class="bordaTopFina"></td></tr>
            
                        <%--Titulos do Resultado--%>
                        <tr>
                            <td class="resultado14 ">
                                <label class="labelNegrito">Tipo Registro</label> 
                            </td> <!--1438 -->             
                                          
                            <td class="resultado1 ">
                                <label class="labelNegrito">Origem</label> 
                            </td>
                            
                            <td class="resultado2 ">
                                <label class="labelNegrito">Grupo</label> 
                            </td>
                                                                   
                            <td class="resultado3 ">
                                <label class="labelNegrito">SubGrupo</label> 
                            </td>
                            
                            <td class="resultado4 ">
                                <label class="labelNegrito">Estabelecimento</label> 
                            </td>
                            
                            <td class="resultado5 ">
                                <label class="labelNegrito">Produto</label> 
                            </td>
                            
                            <td class="resultado6 ">
                                <label class="labelNegrito">Plano</label> 
                            </td>
                            
                            <td class="resultado13 ">
                                <label class="labelNegrito">Parcela</label> 
                            </td>

                            <td class="resultado7 ">
                                <label class="labelNegrito">Captura</label> 
                            </td>
                            
                            <td class="resultado8 ">
                                <label class="labelNegrito">Nro Pagamento</label> 
                            </td>
                            
                            <td class="resultado9 ">
                                <label class="labelNegrito">NSU Host</label> 
                            </td>
                            
                            <td class="resultado10 ">
                                <label class="labelNegrito">Vencimento</label> 
                            </td>
                            
                            <td class="resultado11 ">
                                <label class="labelNegrito">Favorecido</label> 
                            </td>
                            
                            <td class="resultado12 ">
                                <label class="labelNegrito" style="text-align:right">Valor</label> 
                            </td>
                        </tr>	
                                
                        <%--Resultado--%>
                        <asp:Repeater runat="server" ID="rptResultado" OnItemDataBound="rptResultado_ItemDataBound" >
                            <ItemTemplate>  
                            
                                <%--Resultado da Data de Transação--%>
                                <tr>

                                    <td class="resultado14 ">
                                        <%# Eval("TipoRegistro")%>
                                    </td> <!--1438-->
                                    
                                    <td class="resultado1">
                                        <%# Eval("Origem") %>
                                    </td>
                                    
                                    <td class="resultado2">                                                     
                                        <%# Eval("NomeEmpresaGrupo")%>
                                    </td>   
                                     
                                    <td class="resultado3">
                                        <%# Eval("NomeEmpresaSubGrupo")%>
                                    </td>
                                    
                                    <td class="resultado4">
                                        <%# Eval("NomeEstabelecimento")%>
                                    </td>
                                    
                                    <td class="resultado5">
                                        <%# Eval("NomeProduto")%>
                                    </td>
                                    
                                    <td class="resultado6">
                                        <%# Eval("CodigoPlano")%>
                                    </td>

                                    <td class="resultado13">
                                        <%# Eval("NumeroParcela")%>
                                    </td>
                                    
                                    <td class="resultado7">
                                        <%# Eval("MeioCaptura")%>
                                    </td>
                                    
                                    <td class="resultado8">
                                        <%# Eval("NumeroPagamento")%>
                                    </td>
                                    
                                    <td class="resultado9">
                                        <%# Eval("NsuHost")%>
                                    </td>
                                    
                                    <td class="resultado10">
                                        <%# Eval("DataRepasse") != null ? ((DateTime)Eval("DataRepasse")).ToString("dd/MM/yyyy") : ""%>
                                    </td>
                                    
                                    <td class="resultado11">
                                        <%# Eval("NomeFavorecido")%>
                                     </td>
                                    
                                    <td class="resultado12">
                                        <%# (Convert.ToInt32(Eval("ValorParcelaLiquido")) != 0 && Eval("TipoRegistro").ToString() != "CP") ? ((decimal)Eval("ValorParcelaLiquido")).ToString("N2") : (Eval("TipoRegistro").ToString() == "CP") ? ((decimal)Eval("MeioPagamentoValor")).ToString("N2") : ((decimal)Eval("ValorLiquido")).ToString("N2")%>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStatusTransacao" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        
                        <%--Total da Transação--%>
                        <tr>
                            <td colspan="13" class="right espacoTop">
                                <label class="labelNegrito espacoRight">Total</label>
                            </td> 
                            <td class="right espacoRight espacoTop">
                                <label class="espacoRight"><%# ((decimal)Eval("TotalDataTransacao")).ToString("N2")%> </label>
                            </td>
                        </tr>
                   </ItemTemplate>
                </asp:Repeater>	
                                
                <tr><td colspan="14" class="espacoTop bordaTopGrossa"></td></tr>
                
                <%--Total Geral--%>
                <tr>
                    <td colspan="13" class="right">
                        <label class="labelNegrito espacoRight">Total Geral</label>
                    </td>
                    <td class="right espacoRight">
                        <asp:label runat="server" class="espacoRight" ID="lblTotalGeral"></asp:label>
                    </td>
                </tr>
                
                <tr><td colspan="14" class="espacoBotton bordaBottomGrossa"></td></tr>
            </table>
            </td>
            </tr>
            </table>
            
            <table>
            <tr>
                <td>
                    * são os registros que foram cancelados on line
                </td>
            </tr>
            </table>
            <br />
        </div>        
    </form>
    <script language="javascript" type="text/javascript">
        // Esconde o div de carregamento
        document.getElementById("dialogLoading").style.display = "none";
    </script>     
    </body>  
</html>
