﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.critica)
        $.critica = {};
    
    // Funcoes do namespace critica.lista
    $.critica.lista = {
        
        // Abre um dialog com a lista de criticas solicitada
        abrir: function(param, callback) {
        
            // Garante que a tela está carregada
            $.critica.lista.load(function() {

                // Inicializa o dialog
                $("#site_critica_lista").dialog({
                    autoOpen: false,
                    height: 420,
                    width: 850,
                    modal: true,
                    title: "Lista de Críticas",
                    buttons: {
                        "Fechar": function() {
                            // Fecha o dialog
                            $("#site_critica_lista").dialog("close");
                        }
                    },
                    resize: function() {
                        $("#tbCritica").setGridWidth($("#site_critica_lista").width() - 10);
                        $("#tbCritica").setGridHeight($("#site_critica_lista").height() - 140);
                    }
                });

                // Carrega a lista
                $.critica.lista.paramLista = param;
                $.critica.lista.listar();

                // Mostra o dialog
                $("#site_critica_lista").dialog("open");

                // Faz o primeiro resize
                $("#tbCritica").setGridWidth($("#site_critica_lista").width() - 10);
                $("#tbCritica").setGridHeight($("#site_critica_lista").height() - 140);

                // Se tem, chama o callback
                if (callback)
                    callback();

            });

        },

        // ----------------------------------------------------------------------
        // load: garante que o HTML da esteja carregado e inicializado
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        load: function(callback) {

            // Verifica se o html esta carregado
            if ($("#site_critica_lista").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_critica_lista.aspx #site_critica_lista", function() {

                    // Copia o elemento para a área comum
                    $("#site_critica_lista").appendTo("#areaComum");

                    // Completa o load
                    $.critica.lista.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();

                    });

                });

            } else {
                // Chama o callback
                if (callback)
                    callback();
            }

        },

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load2: function(callback) {
        
            // Insere o subtítulo
            $("#subtitulo").html("Lista de Críticas");
            
            // Layout dos botões
            $("#site_critica_lista button").button();
        
            // Botão pesquisar
            $("#site_critica_lista .cmdPesquisar").click(function() {
                $.critica.lista.listar();
            });
        
            // Botão detalhe
            $("#site_critica_lista .cmdDetalhe").click(function() {

                // Pega o id da critica
                var idCritica = $("#tbCritica").jqGrid("getGridParam", "selrow");

                if (idCritica) {
                    idCritica = idCritica.substring(idCritica.lastIndexOf("_") + 1);
                    // Pede o detalhe
                    $.critica.detalhe.abrir(idCritica);
                }
                else {
                    // Mostra popup de erro
                    popup({
                        titulo: "Mostrar Detalhe",
                        icone: "erro",
                        mensagem: "Selecione a linha que deseja ver o detalhe.",
                        callbackOK: function() {
                        }
                    });
                }
                
            });

            // Carrega combo de filtro de tipos de critica
            $.critica.lista.carregarDescricoesCritica(function() {

                // Inicializa Lista de Criticas
                $("#tbCritica").jqGrid({
                    datatype: "clientSide",
                    mtype: "GET",
                    colNames: ["Código", "Tipo", "Data", "Descrição", "Processo", "Arquivo", "ArquivoItem"],
                    colModel: [
                                  { name: "CodigoCritica", index: 0, width: 55, align: "center" },
                                  { name: "TipoCritica", index: 1, width: 60, editable: false, formatter: "select", edittype: "select", editoptions: { value: $.critica.lista.montarListaDescricoesCritica()} },
                                  { name: "DataCritica", index: 2, width: 80, align: "center" },
                                  { name: "DescricaoCritica", index: 3, width: 100, align: "left" },
                                  { name: "CodigoProcesso", index: 4, width: 40, align: "center" },
                                  { name: "CodigoArquivo", index: 5, width: 40, align: "center" },
                                  { name: "CodigoArquivoItem", index: 6, width: 40, align: "center" }
                                ],
                    height: 150,
                    width: 900,
                    sortorder: "asc",
                    viewrecords: true,
                    ondblClickRow: function(rowid, status) {
                        return false;
                    }
                });

                // Preenche o combo
                var itens = $("body").data("descricoesCritica");
                for (var i = 0; i < itens.length; i++)
                    $("#painelCriticaFiltro .cmbTipoCritica").append("<option value='" + itens[i].NomeTipo + "'>" + itens[i].Descricao + "</option>");

                // Chama o callback
                if (callback)
                    callback();

            });

        },
        
        // --------------------------------------------------------------------
        //  listar: pede a lista de criticas
        // --------------------------------------------------------------------
        listar: function() {
        
            // Limpa a lista
            $("#tbCritica").jqGrid("clearGridData");
            
            // Monta o filtro
            var request = {};
            
            // Coloca os parametros fixos
            if ($.critica.lista.paramLista && $.critica.lista.paramLista.FiltroCodigoProcesso)
                request.FiltroCodigoProcesso = $.critica.lista.paramLista.FiltroCodigoProcesso;
            if ($.critica.lista.paramLista && $.critica.lista.paramLista.FiltroCodigoArquivo)
                request.FiltroCodigoArquivo = $.critica.lista.paramLista.FiltroCodigoArquivo;
            if ($.critica.lista.paramLista && $.critica.lista.paramLista.FiltroCodigoArquivoItem)
                request.FiltroCodigoArquivoItem = $.critica.lista.paramLista.FiltroCodigoArquivoItem;
            if ($.critica.lista.paramLista && $.critica.lista.paramLista.FiltroTipoLinha)
                request.FiltroTipoLinha = $.critica.lista.paramLista.FiltroTipoLinha;
            if ($.critica.lista.paramLista && $.critica.lista.paramLista.FiltroNomeCampo)
                request.FiltroNomeCampo = $.critica.lista.paramLista.FiltroNomeCampo;

            // Tipo de critica
            var tipoCritica = $("#site_critica_lista .cmbTipoCritica").val();
            if ($.critica.lista.paramLista && $.critica.lista.paramLista.FiltroTipoCritica)
                request.FiltroTipoCritica = $.critica.lista.paramLista.FiltroTipoCritica;
            else if (tipoCritica != "?")
                request.FiltroTipoCritica = tipoCritica;

            // Data inicial
            var dataInicial = $("#site_critica_lista .txtFiltroDataInicial").val();
            if (dataInicial != "")
                request.FiltroDataCriticaInicial = dataInicial;
            
            // Data final
            var dataFinal = $("#site_critica_lista .txtFiltroDataFinal").val();
            if (dataFinal != "")
                request.FiltroDataCriticaFinal = dataFinal;
                
            // Pede a lista de criticas
            executarServico("ListarCriticaRequest", request, function(data) 
            {       for (var i = 0; i < data.Resultado.length; i++)
                        $("#tbCritica").jqGrid(
                            "addRowData", 
                            "critica_" + data.Resultado[i].CodigoCritica, 
                            data.Resultado[i]);
                });
        
        },
        
        // ----------------------------------------------------------------------
        // montarListaDescricoesCritica: monta a lista de descricoes de critica para utilizar no grid
        // ----------------------------------------------------------------------
        montarListaDescricoesCritica: function() {

            // Pega a lista
            var lista = $("body").data("descricoesCritica");

            // String de retorno
            var retorno = "";

            // Para cada item, adiciona
            for (var i = 0; i < lista.length; i++) {

                // Se tem descricao utiliza, se nao, usa o mnemonico
                var descricao = lista[i].Descricao;
                if (descricao == null)
                    descricao = lista[i].NomeTipo;

                // Adiciona o item
                retorno += lista[i].NomeTipo + ":" + descricao + ";";

            }

            // Ajusta a string
            if (retorno != "")
                retorno = retorno.substr(0, retorno.length - 1);

            // Retorna
            return retorno;

        },

        // ----------------------------------------------------------------------
        // carregarDescricoesCritica: carrega a lista de descricoes de critica
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        carregarDescricoesCritica: function(callback) {

            // Verifica se já não está carregado
            if (!$("body").data("descricoesCritica")) {

                // Pede a lista
                executarServico(
                    "ListarDescricaoCriticaRequest",
                    {},
                    function(data) {

                        // Guarda a lista
                        $("body").data("descricoesCritica", data.Resultado);

                        // Chama o callback
                        if (callback)
                            callback();

                    });

            } else {

                // Chama o callback
                if (callback)
                    callback();

            }

        }

    };

    // Dispara o load, caso tenha sido chamado da pagina de lista de criticas
    if (window.location.href.indexOf("site_critica_lista") > 0) {

        // Carrega
        $.critica.lista.load2(function() {
        
            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbCritica").setGridWidth($(window).width() - margemEsquerda);
                $("#tbCritica").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");

        });
        
    }

});