﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_produto_lista.js"></script>
    <link href="site_configuracao_produto_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Cadastro de Produtos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <div id="site_configuracao_produto_detalhe" >
        
        <!-- Abas do detalhe do produto -->
        <div id="site_configuracao_produto_tabs">
            
            <!-- Abas -->
            <ul>
                <li ><a href="#produto_tabDetalhe">Detalhe</a></li>
                <li><a href="#produto_tabPlanos">Planos</a></li>
            </ul>
            
            <!-- DETALHE -->
            <div id="produto_tabDetalhe">
            
                <table class="formProduto">
                    <tr>
                        <td>
                            <label>Nome do Produto:</label>
                        </td>
                        <td>
                            <input type="text"  id="txtNomeProduto" maxlength="40" class="forms[property[NomeProduto];required]"  />
                        </td>
                    </tr>
                    
                    <tr >
                        <td>
                            <label >Descrição do Produto:</label>
                        </td>
                        <td rowspan="2">
                            <textarea rows="2" cols="38" id="txtDescricaoProduto" class="forms[property[DescricaoProduto]]" maxlength="40" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cedível:</label>
                        </td>
                        <td>
                            <select id="cmbCedivel" class="forms[property[Cedivel];required]" >
                                <option value="?">(selecione)</option>
                                <option value="true">Sim</option>
                                <option value="false">Não</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Produto TSYS:</label>
                        </td>
                        <td>
                            <input type="text"  id="codigoProdutoTSYS" maxlength="6" class="forms[property[CodigoProdutoTSYS];required]"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Prazo Pagamento Qtd. Dias:</label>
                        </td>
                        <td>
                            <input type="text" id="przPagamentoQtd" style="width: 30px;" maxlength="3" class="forms[property[PrazoPagamento];dataType[integer];required]"  />
                        </td>
                    </tr>      
                    <tr>
                        <td>
                            <label>Qtd. Dias Vencidos (CV/AV/CP/AP):</label>
                        </td>
                        <td>
                            <input type="text" id="qtdDiasVencido" style="width: 30px;" maxlength="3" class="forms[property[DiasVencidos];dataType[integer];required]"  />
                        </td>
                    </tr>     
                    <tr>
                        <td>
                            <label>Qtd. Dias Vencidos (AJ):</label>
                        </td>
                        <td>
                            <input type="text" id="qtdDiasVencidoAJ" style="width: 30px;" maxlength="3" class="forms[property[DiasVencidosAJ];dataType[integer];required]"  />
                        </td>
                    </tr>                                                        
                </table>
            </div>
            
            <!-- PLANOS -->
            <div id="produto_tabPlanos">
                <table>
                    <tr>
                        <td>
                            <table id="tb_PlanosDisponiveis"></table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <button id="btnSelecionarTodosPlanos" class="selectbutton all_down"><%--&gt;&gt;--%></button>
                            &nbsp;
                            <button id="btnSelecionarPlano" class="selectbutton down"><%--&gt;--%></button>
                            &nbsp;
                            <button id="btnRemoverPlano" class="selectbutton up"><%--&lt;--%></button>
                            &nbsp;
                            <button id="btnRemoverTodosPlanos" class="selectbutton all_up"><%--&lt;&lt;--%></button>
                        </td>
                     </tr>
                     <tr>   
                        <td>
                            <table id="tb_PlanosSelecionados"></table>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
</div>

</asp:Content>
