﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_processo_atacadao.js"></script>
    <link href="site_processo_atacadao.css" rel="stylesheet" type="text/css" />
    <title>SCF - Processo Atacadão</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- Conteúdo da Página -->
    <div id="site_processo_atacadao">

        <!-- Disparo manual -->
        <div class="detalhe">
            
            <span id="cabecalho_coluna_1">Arquivo</span>
            <span id="cabecalho_coluna_2">Diretório</span>
            <span id="cabecalho_coluna_3">Nome do Arquivo</span>
            <br/>
            
            <label>TSYS:</label>
            <input type="text" class="inputleft" id="txtArquivoTSYS" />
            <input type="text" class="inputright" id="txtArquivoTSYSArquivo" />
            <br />
            
            <label>CSU:</label>
            <input type="text" class="inputleft" id="txtArquivoCSU" />
            <input type="text" class="inputright" id="txtArquivoCSUArquivo" />
            <br />

            <label>Sitef:</label>
            <input type="text" class="inputleft" id="txtArquivoSitef" />
            <input type="text" class="inputright" id="txtArquivoSitefArquivo" />
            <br />

            <label>Matera Financ.:</label>
            <input type="text" class="inputleft" id="txtArquivoMatera" />&nbsp;
            <br />
            
            <label>Conciliação:</label>
            <input type="text" class="inputleft" id="txtArquivoConciliacao" />&nbsp;
            <br />

            <label>Matera Cont.:</label>
            <input type="text" class="inputleft" id="txtArquivoMateraContabil" />&nbsp;
            <br />
            
            <label>CSU sem Atacadão:</label>
            <input type="text" class="inputleft" id="txtArquivoCSUSemAtacadao" />&nbsp;
            <br />
            <br />
            
            <button id="cmdProcessarAtacadao">Iniciar Processamento</button>
            <br />
            
                   
        </div>
       
    </div>

</asp:Content>
