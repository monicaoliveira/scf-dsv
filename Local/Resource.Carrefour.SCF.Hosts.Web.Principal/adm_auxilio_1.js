﻿$(document).ready(function() {

    // Funcoes do namespace processo.lista
    $.auxilio1 = {
    
        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {
        
            // Botao executar
            $("#cmdExecutar").click(function() {
            
                // Chama o servico
                executarServico(
                    "ExecutarAuxilioQueryRequest", 
                    {
                        Query: $("#txtQuery").val()
                    }, 
                    function(data) {

                        // Verifica se não deu erro
                        if (data.StatusResposta == "OK") {
                        
                            // Prepara o resultado da tabela
                            var tabela = "";
                            
                            // Monta o cabecalho
                            tabela += "<tr>";
                            for (var i = 0; i < data.Cabecalho.length; i++)
                                tabela += "<td>" + data.Cabecalho[i] + "</td>";
                            tabela += "</tr>";
                            
                            // Indica se deve informar que a tabela foi preenchida parcialmente
                            var informarParcial = false;

                            // Monta as linhas
                            for (var j = 0; j < data.Resultado.length; j++) {
                                tabela += "<tr>";
                                var linha = data.Resultado[j];
                                if (linha) {
                                    linha = linha.split(";");
                                    for (var i = 0; i < linha.length; i++) {
                                        tabela += "<td>" + linha[i] + "</td>";
                                    }
                                } else {
                                    informarParcial = true;
                                }
                                tabela += "</tr>";
                            }
                        
                            // Mostra na tabela
                            $("#tbResultado").html(tabela);
                            
                            // Deve informar?
                            if (informarParcial)
                                alert("A tabela foi preenchida parcialmente");
                            
                        } else {
                        
                            // Informa erro
                            $("#tbResultado").html("<tr><td>" + data.Erro + "</td></tr>");
                        
                        }

                    });
            
            });
        
        }
        
    };
    
    // Dispara a carga
    $.auxilio1.load();

});