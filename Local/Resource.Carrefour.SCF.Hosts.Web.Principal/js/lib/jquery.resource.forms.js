(function($) {
    $.fn.Forms = function(method) {

        // Method calling logic
        if ($.Forms[method]) {
            return $.Forms[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return $.Forms.initializeFields.apply(this, arguments);
        } else {
            $.error('Método ' + method + ' não existe em Forms.');
        }

    };

    $.Forms = {

        // -----------------------------------
        // Lista de opções default
        // -----------------------------------
        defaultSettings: {
            abc: "abc",
            xyz: "xyz"
        },

        // -----------------------------------
        // Inicialização do formulário
        // -----------------------------------
        initialize: function(settings) {

        },

        // -----------------------------------
        // Opções default para o valor forms da classe
        // -----------------------------------
        formClassDefaultOptions: {
            required: false,
            optional: false,
            dataType: "string",
            property: null,
            defaultValue: null,
            label: null,
            errorDate: "Data inválida",
            errorInteger: "Número inválido",
            errorDecimal: "Número decimal inválido",
            errorDouble: "O número deve ser diferente de zero!",
            errorMissing: "Campo obrigatório",
            allowNegative: false,
            maskType: null,
            customMask: null,
            placeHolder: " ",
            datePicker: true,
            toUpperCase: false,
            enableForPermissions: null,
            disableForPermissions: null,
            showForPermissions: null,
            hideForPermissions: null
        },

        // -----------------------------------
        // Faz a extração das opções de forms de uma classe
        // -----------------------------------
        getFormClassOptions: function(classAttr) {

            // Faz a separação das opções desejadas
            var posForms = classAttr.indexOf("forms");                 // Acha o inicio da palavra forms
            var posFimForms = classAttr.indexOf("] ", posForms); // Acha o possível fim de forms
            var classValue = "";                                            // Extrai apenas esta parte
            if (posFimForms == -1)
                classValue = classAttr.substring(posForms, classAttr.length - 1);
            else
                classValue = classAttr.substring(posForms, posFimForms - posForms);
            classValue = classValue.replace("forms[", "");             // Retira o início
            classValue = classValue.split(";");                             // Separa as opções

            // Monta objeto com as opções
            var opcoes = {};
            for (var i = 0; i < classValue.length; i++) {
                var opcaoTemp = classValue[i];
                var nome = "";
                var valor = true;
                var posColchete = opcaoTemp.indexOf("[");
                if (posColchete == -1) {
                    nome = opcaoTemp;
                } else {
                    nome = opcaoTemp.substring(0, posColchete);
                    valor = opcaoTemp.substring(posColchete + 1, opcaoTemp.length - 1);
                }
                eval("opcoes." + nome + " = valor;");
            }

            // Junta com os valores default e especificos do campo, caso exista
            var opcoesRetornar = {};
            $.extend(opcoesRetornar, $.Forms.formClassDefaultOptions, opcoes);

            // Retorna 
            return opcoesRetornar;
        },

        // -----------------------------------
        // Opções default para o método showValidation
        // -----------------------------------
        validateDefaultOptions: {
            showForm: false,
            returnErrors: false,
            markLabels: false,
            errorClass: "labelErro",
            errors: {}
        },

        // -----------------------------------
        // Solicita a validação
        // -----------------------------------
        validate: function(options) {

            // Junta as opções
            options = $.extend($.Forms.validateDefaultOptions, options);

            // Prepara a coleção de críticas
            var criticas = new Array();

            // Executa para todos 
            $("[class*='forms']", this).each(function() {

                // Pega opções
                var opcoes =
                     $.Forms.getFormClassOptions(
                           $(this).attr("class"));

                // Verifica se já há mensagem de erro para este campo
                if (options.errors[opcoes.property]) {
                    criticas.push({ property: opcoes.property,
                        message: options.errors[opcoes.property], element: $(this)
                    });
                }
                else {
                    // Extrai o valor
                    var valorCampo = $.Forms.getControlValue($(this));

                    // Valida tipo do campo
                    if (valorCampo != "" && valorCampo != null) {
                        if (opcoes.dataType == "date" && $.parseDate(valorCampo, $.Forms.maskOptions.date) == null) {
                            criticas.push({ property: opcoes.property,
                                message: opcoes.errorDate, element: $(this)
                            });
                        }
                        else if (opcoes.dataType == "dateTime" && $.parseDate(valorCampo, $.Forms.maskOptions.dateTime) == null) {
                            criticas.push({ property: opcoes.property,
                                message: opcoes.errorDate, element: $(this)
                            });
                        }
                        else if (opcoes.dataType == "decimal" && isNaN($.parseFloat(valorCampo))) {
                            criticas.push({ property: opcoes.property,
                                message: opcoes.errorDecimal, element: $(this)
                            });
                        }
                        else if (opcoes.dataType == "integer" && isNaN($.parseInt(valorCampo))) {
                            criticas.push({ property: opcoes.property,
                                message: opcoes.errorInteger, element: $(this)
                            });

                        } else if (opcoes.dataType == "integer"
                                                           && valorCampo.indexOf($.culture.numberFormat["."]) != -1) {
                            criticas.push({ property: opcoes.property,
                                message: opcoes.errorInteger, element: $(this)
                            });
                        }
                    }

                    // Valida opcional ou requerido
                    if (opcoes.required == true && (valorCampo == null || valorCampo == "")) {
                        criticas.push({ property: opcoes.property, message: opcoes.errorMissing, element: $(this) });
                    }
                    
                }

            });

            // Conta quantidade de criticas e adiciona os labels
            var qtdeCriticas = 0;
            for (var c in criticas) {
                criticas[c].label = $.Forms.getControlLabel(criticas[c].element);
                if (criticas[c].label && criticas[c].label.html() != null)
                    criticas[c].labelValue = criticas[c].label.html().replace(":", "");
                else
                    criticas[c].labelValue = criticas[c].property;
                qtdeCriticas++;
            }

            // Pinta os labels?
            if (options.markLabels) {
                $("." + options.errorClass, this).removeClass(options.errorClass);
                for (var c in criticas) {
                    var critica = criticas[c];
                    if (critica.label && !critica.label.hasClass(options.errorClass))
                        critica.label.addClass(options.errorClass);
                }
            }

            // Mostra erros?
            if (options.showForm == true && qtdeCriticas != 0) {
                $.Forms.showValidationForm({ errors: criticas });
            }

            // Retorna
            if (options.returnErrors)
                return criticas;   // Retorna criticas
            else
                return qtdeCriticas == 0; // Retorna se o form está valido
        },

        // -----------------------------------
        // Retorna o label associado ao controle
        // -----------------------------------
        getControlLabel: function(element, opcoes) {

            // Se nao passou opcoes, pega
            opcoes =
                        $.Forms.getFormClassOptions(
                             element.attr("class"));

            // Informou o id do label?
            if (opcoes.label)
                return $("#" + opcoes.label);
            else
                return element.prev("label");

        },

        // -----------------------------------
        // Opções default para o método showValidation
        // -----------------------------------
        showValidationDefaltOptions: {
            showForm: true,
            markLabels: false,
            errorClass: "labelErro",
            errors: {}
        },

        // -----------------------------------
        // Mostra o dialog de validação
        // -----------------------------------
        showValidation: function(options) {

        },

        // -----------------------------------
        // Opções default para o método setData
        // -----------------------------------
        setDataDefaultOptions: {
            dataObject: null
        },

        // -----------------------------------
        // Insere informações nos controles
        // -----------------------------------
        setData: function(options) {

            // Pega o objeto
            var dataObject = {};
            if (options.dataObject)
                dataObject = options.dataObject;

            // Executa para todos 
            $("[class*='forms']", this).each(function() {

                // Pega opcoes
                var opcoes = $.Forms.getFormClassOptions($(this).attr("class"));

                // Pega o valor
                var valor = null;

                if (opcoes.property) {
                    eval("valor = dataObject." + opcoes.property + ";");

                    // Seta o valor
                    if (opcoes.property && opcoes.dataType == "date") {
                        $.Forms.setControlValue(
                            $(this), valor ? fJsonDate(valor, $.Forms.maskOptions.date) : null);
                    } else if (opcoes.property && opcoes.dataType == "dateTime") {
                        $.Forms.setControlValue($(this), valor ? fJsonDate(valor, $.Forms.maskOptions.dateTime) : null);
                    } else if (opcoes.property && opcoes.dataType == "decimal") {
                        if (valor) {
                            $.Forms.setControlValue($(this), $.format(valor, "N", $.culture));
                        }
                    } else {
                        $.Forms.setControlValue($(this), valor);
                    }
                }

            });

            // Retorna
            return dataObject;

        },

        // -----------------------------------
        // Acha o tipo do controle
        // -----------------------------------
        getControlType: function(elemento) {
            switch (elemento[0].tagName.toUpperCase()) {
                case "SELECT":
                    return "select";
                    break;
                case "INPUT":
                    switch (elemento.attr("type").toUpperCase()) {
                        case "BUTTON":
                            return "button";
                            break;
                        case "RADIO":
                            return "radio";
                            break;
                        case "CHECKBOX":
                            return "checkbox";
                            break;
                        default:
                            return "text";
                    }
                    break;
                case "TEXTAREA":
                    return "textarea";
                    break;
                case "LABEL":
                    return "label";
                    break;
            }
            return null;
        },

        // -----------------------------------
        // Seta o valor do controle
        // -----------------------------------
        setControlValue: function(elemento, value) {

            // Verifica o tipo do controle
            var tipoControle = $.Forms.getControlType(elemento);

            // Seta o valor
            switch (tipoControle) {
                case "text":
                    if (value)
                        elemento.val(value);
                    else
                        elemento.val("");
                    break;
                case "select":
                    if (value == null || value == "")
                        value = "?";
                    try { elemento.val(value); } catch (ex) { };
                    break;
                case "radio":
                    var nomeControle = elemento.attr("name");
                    $("input[name='" + nomeControle + "']", elemento.parent()).val(new Array(value + ''));
                    break;
                case "checkbox":
                    if (value && (value == true || value == "true"))
                        elemento.attr("checked", "checked");
                    else
                        elemento.removeAttr("checked");
                    break;
                case "textarea":
                    if (value)
                        elemento.val(value);
                    else
                        elemento.val("");
                    break;
                case "label":
                    elemento.text((value) ? value : "");
            }

        },

        // -----------------------------------
        // Retorna o valor do controle
        // -----------------------------------
        getControlValue: function(elemento) {

            // Verifica o tipo do controle
            var tipoControle = $.Forms.getControlType(elemento);

            // Extrai o valor
            var valorCampo = null;
            switch (tipoControle) {
                case "text":
                    valorCampo = elemento.val();
                    break;
                case "select":
                    valorCampo = elemento.val();
                    if (valorCampo == "?")
                        valorCampo = null;
                    break;
                case "radio":
                    var nomeControle = elemento.attr("name");
                    valorCampo = $("input[name='" + nomeControle + "']:radio:checked",
                                         elemento.parent()).val();
                    break;
                case "checkbox":
                    var checkValue = elemento.attr("checked");
                    if (checkValue && (checkValue == true || checkValue == "true"))
                        valorCampo = true;
                    else
                        valorCampo = false;
                    break;
                case "textarea":
                    valorCampo = elemento.val();
                    break;
            }

            // Retorna
            return valorCampo;
        },

        // -----------------------------------
        // Opções default para o método getData
        // -----------------------------------
        getDataDefaultOptions: {
            dataObject: null
        },

        // -----------------------------------
        // Lê informações dos controles
        // -----------------------------------
        getData: function(options) {

            // Pega o objeto
            var dataObject = {};
            if (options && options.dataObject)
                dataObject = options.dataObject;

            // Executa para todos 
            $("[class*='forms']", this).each(function() {

                // Pega opcoes
                var opcoes = $.Forms.getFormClassOptions($(this).attr("class"));

                // Pega o valor 
                var valorCampo = $.Forms.getControlValue($(this));

                // Se for data ou número e estiver vazio, passa nulo
                if (valorCampo == "" && (opcoes.dataType == "decimal" || opcoes.dataType == "integer"))
                    valorCampo = null;
                else if (valorCampo == "" && opcoes.dataType == "date")
                    valorCampo = null;
                else if (valorCampo == "" && opcoes.dataType == "dateTime")
                    valorCampo = null;
                else if (valorCampo != "" && opcoes.dataType == "decimal")
                    valorCampo = $.parseFloat(valorCampo);
                else if (valorCampo != "" && opcoes.dataType == "integer")
                    valorCampo = $.parseInt(valorCampo);
                else if (valorCampo != "" && opcoes.dataType == "date")
                    valorCampo = valorCampo;
                else if (valorCampo != "" && opcoes.dataType == "dateTime")
                    valorCampo = valorCampo;

                // Suba data sempre no formato dateTime
                if (valorCampo && (opcoes.dataType == "date" || opcoes.dataType == "dateTime"))
                    valorCampo = fJsonDate(valorCampo, $.Forms.maskOptions.dateTime);

                // Se for um CPF ou CNPJ, removo os pontos, traços e barras
                if (opcoes.maskType == "cnpj" || opcoes.maskType == "cpf") {
                    valorCampo = valorCampo.replace(/[^0-9]/g, "");
                }

                // Atribui o valor
                if (opcoes.property)
                    eval("dataObject." + opcoes.property + " = valorCampo;");

            });

            // Retorna
            return dataObject;
        },

        // -----------------------------------
        // Apaga os valores dos campos
        // -----------------------------------
        clearData: function() {

            // Executa para todos 
            $("[class*='forms']", this).each(function() {

                // Pega opcoes
                var opcoes =
                             $.Forms.getFormClassOptions(
                                   $(this).attr("class"));

                // Resseta o valor dos campos
                if (!opcoes.defaultValue)
                    $.Forms.setControlValue($(this), "");
                else
                    $.Forms.setControlValue($(this), opcoes.defaultValue);

            });

            // Retorna
            return this;
        },

        // Mostra dialog com os erros
        showValidationForm: function(options) {
            if (options.errors[0]) {
                $("#popupValidacao_lista").clearGridData();
                for (var c in options.errors) {
                    $("#popupValidacao_lista")
                                   .addRowData("",
                                               { aba: options.errors[c].tab ? options.errors[c].tab : "",
                                                   erro: options.errors[c].labelValue ? options.errors[c].labelValue + ": " + options.errors[c].message : "" + options.errors[c].message
                                               },
                                               "last",
                                               "after");
                }
                $("#popupValidacao").dialog("open");
            }
        },

        // -----------------------------------
        // Opções default para a mascara
        // -----------------------------------
        maskOptions: {
            decimalSeparator: ",",
            decimalDigits: 2,
            thousandSeparator: ".",
            useThousandSeparator: true,
            allowNegative: false,
            date: "dd/MM/yyyy",
            dateTime: "dd/MM/yyyy HH:mm:ss",
            time: "HH:mm:ss",
            cnpj: "99.999.999/9999-99",
            cpf: "999.999.999-99",
            cep: "(99) 9999-9999",
            phone: "99999-999"
        },

        // -----------------------------
        // SUFIXO PARA AS DATAS COM HORA
        // -----------------------------
        dateTimeSuffix: "",

        // -----------------------------------------
        // Inicializa os campos de input e text area
        // -----------------------------------------
        initializeFields: function() {
            // Executa para todos 
            $("[class*='forms']", this).each(function() {

                // Verifica o tipo do controle
                var tipoControle = $.Forms.getControlType($(this));
                // Pega opcoes
                var opcoes = $.Forms.getFormClassOptions($(this).attr("class"));

                // Devo transformar em maiúsculas?
                if (opcoes.toUpperCase && (tipoControle == "text" || tipoControle == "textarea")) {
                    $(this).css("text-transform", "uppercase");
                    $(this).blur(function() {
                        $(this).val($(this).val().toUpperCase());
                    });
                }

                // Trato o tamanho máximo digitado para textareas
                if (tipoControle == "textarea") {

                    $(this).keypress(function(event) {
                        var max = $(this).attr("maxlength");
                        var key;
                        var ie = (typeof window.ActiveXObject != "undefined");

                        (ie) ? key = event.keyCode : key = event.which; /* Se for IE (keyCode), caso contrario, (wich) */

                        if (key >= 33 || key == 13 || key == 32) {
                            /* Se ja estiver no limite de caracteres return null (preventDefault)*/
                            var tam = $(this).val().length;
                            (tam >= max) ? event.preventDefault() : null;
                        }
                        window.status = $(this).val().length;
                    });

                    $(this).bind('paste', function() {
                        var max = $(this).attr("maxlength");
                        var that = this;
                        setTimeout(function() {
                            var tam = that.value.length;
                            if (tam > max) {
                                that.value = that.value.substring(0, max);
                            }
                        }, 0);
                    });

                    /* Substring no evento keyup */
                    $(this).keyup(function(event) {
                        var max = $(this).attr("maxlength");
                        var tam = $(this).val().length;
                        if (tam > max) {
                            $(this).val($(this).val().substring(0, max));
                        }
                    });
                }

                // Aplico eventuais máscaras
                if (opcoes.maskType && ((tipoControle == "text" || tipoControle == "textarea"))) {
                    // Possui mascara customzada?
                    if (opcoes.maskType == "custom" && opcoes.customMask) {
                        $(this).mask(opcoes.customMask, { placeholder: opcoes.placeHolder });
                    } else {
                        // Converto a máscara. Nossas máscaras são numéricas, todos os caracteres
                        // alfa-numéricos são substituidos  por 9 (numérico)
                        var maskPattern = $.Forms.maskOptions[opcoes.maskType];
                        if (!maskPattern) {
                            alert("Unknown mask type!");
                            return false;
                        }
                        maskPattern = maskPattern.replace(new RegExp("[^.,:/()--  ]", "g"), "9");
                        // Aplico a máscara
                        $(this).mask(maskPattern, { placeholder: opcoes.placeHolder });
                    }
                }

                // Se for INTEGER, só aceito números (alinho a direita)
                if (opcoes.dataType == "integer") {
                    $(this).css("text-align", "right").keydown(function(e) {
                        var key = e.charCode || e.keyCode || 0;
                        // allow backspace, tab, delete, arrows, numbers, and keypad numbers ONLY
                        return (
                                  key == 8 || key == 9 || key == 46
                                  || (key >= 37 && key <= 40)
                                  || (key >= 48 && key <= 57)
                                  || (key >= 96 && key <= 105));
                    });

                }

                // Decimais
                var decimalOptions = {
                    decimal: $.Forms.maskOptions.decimalSeparator,
                    precision: $.Forms.maskOptions.decimalDigits,
                    thousands: $.Forms.maskOptions.useThousandSeparator
                                                     ? $.Forms.maskOptions.thousandSeparator : "",
                    showSymbol: false
                };
                // Se for um DECIMAL (alinho a direita)
                if (opcoes.dataType == "decimal") {
                    $(this).css("text-align", "right").maskMoney(decimalOptions);
                }

                // Se for integer ou decimal, adiciono um padding para o cursor aparecer piscando
                if (opcoes.dataType == "integer" || opcoes.dataType == "decimal") {
                    $(this).css("padding-right", "1px");
                }

                // Deve possuir um date picker?
                opcoes.datePicker = opcoes.datePicker === true || opcoes.datePicker === "true";
                if ((opcoes.dataType == "date" || opcoes.dataType == "dateTime") && opcoes.datePicker) {

                    // Se o valor do campo for valido e maior que o formato de data, guardo o
                    // sufixo (somente se a mascara do campo for maior)
                    var defaultDateMaskLength = $.Forms.maskOptions.date.length;

                    $(this).datepicker(
                        {
                            buttonImage: "img/icone_calendario.gif",
                            showOn: 'button',
                            buttonImageOnly: true,
                            beforeShow: function(dateText, inst) {
                                if ($(inst.input).val()
                                                     && $(inst.input).val().length > defaultDateMaskLength) {
                                    $.Forms.dateTimeSuffix = $(inst.input).val()
                                                     .substring(defaultDateMaskLength, $(inst.input).val().length);

                                } else if (opcoes.maskType) {
                                    $.Forms.dateTimeSuffix = "";
                                    var dateMask = "";
                                    if (opcoes.maskType == "custom" && opcoes.customMask) {
                                        dateMask = opcoes.customMask;
                                    } else {
                                        dateMask = $.Forms.maskOptions[opcoes.maskType];
                                    }
                                    if (dateMask.length > defaultDateMaskLength) {
                                        dateMask = dateMask.substring(defaultDateMaskLength, dateMask.length);
                                        // Formato a string substituindo os 9 por 0 (zero)
                                        dateMask = dateMask.replace(new RegExp("[^.,:/()--  ]", "g"), "9");
                                        $.Forms.dateTimeSuffix = dateMask.replace(new RegExp("[9]", "g"), "0");
                                    }
                                } else {
                                    $.Forms.dateTimeSuffix = Date;
                                }
                            },
                            onSelect: function(dateText, inst) {
                                $(inst.input).val($(inst.input).val() + $.Forms.dateTimeSuffix);
                            }
                        }
                            );
                    } 
                    //1463
                    //Jira SCF1065 - Marcos Matsuoka
//                    else {
//                            var date = new Date();
//                            date.setDate(date.getDate() - 1);
//                            $('#txtFiltroDataInicial').datepicker("setDate", date);
//                    }
//                    //Fim
//                    
            });

            // Acerto o posicionamento da imagem dos calendários e
            // removo as propriedades ALT e TITLE
            $(".ui-datepicker-trigger").each(function() {
                $(this).css("vertical-align", "text-bottom")
                             .removeAttr("alt").removeAttr("title");
            });
            // Mãozinha do cursor do mouse
            $(".ui-datepicker-trigger").mouseover(function() {
                $(this).css("cursor", "pointer");
            });
        },

        // --------------------------------------------------
        // Aplica regras de bloqueio e vizualização de campos
        // --------------------------------------------------
        applyPermissions: function() {

            // Executa para todos campos (incluindo botões)
            $("[class*='forms']", this).each(function() {

                // Obtenho as permissoes do usuario
                var permissoes = template_default_permissoes_carregadas;
                // Obtenho as opções
                var opcoes = $.Forms.getFormClassOptions($(this).attr("class"));
                // Verifica o tipo do controle
                var tipoControle = $.Forms.getControlType($(this));

                // As permissoes podem ser E ou OU, por exemplo:
                // "PERMISSAO_1 / PERMISSAO_2, PERMISSAO_3"
                // Indica que o usuario deve ter a permissao 1 ou 2 E a pemrmissão 3, ok?

                // MOSTRAR?
                var mostrar = true;
                if (opcoes.showForPermissions) {
                    // Possui as permissões?
                    mostrar = $.Forms.hasPermissions(opcoes.showForPermissions, permissoes);
                }

                // HABILITAR?
                var habilitar = true;
                if (opcoes.enableForPermissions) {
                    // Possui as permissões?
                    habilitar = $.Forms.hasPermissions(opcoes.enableForPermissions, permissoes);
                }

                // DESABILITAR?
                if (opcoes.disableForPermissions) {
                    // Possui as permissões?
                    habilitar = !$.Forms.hasPermissions(opcoes.disableForPermissions, permissoes);
                }

                // ESCONDER?
                if (opcoes.hideForPermissions) {
                    // Possui as permissões?
                    mostrar = !$.Forms.hasPermissions(opcoes.hideForPermissions, permissoes);
                }

                // Elemento(s)
                var elemento = $(this);
                // Se for um radio, devo pegar todos os RADIOs com o mesmo nome
                if (tipoControle == "radio") {
                    var nomeControle = elemento.attr("name");
                    elemento = $("input[name='" + nomeControle + "']:radio");
                }

                // Aplico as ações processadas
                if (mostrar) {
                    elemento.show();
                    if (habilitar) {
                        // Se for um botão, trato de um outro jeito
                        if (tipoControle == "button") {
                            elemento.button("enable");
                        } else {
                            elemento.removeAttr("disabled");
                        }
                    } else {
                        if (tipoControle == "button") {
                            elemento.button("disable");
                        } else {
                            elemento.attr("disabled", "disabled");
                        }
                    }
                } else {
                    elemento.hide();
                }
            });
        },

        hasPermissions: function(permissions, permissionsToCheck) {

            // Possui as permissoes ?
            var possuiPermissoes = true;

            // Transformo em array
            var permissionsAND = permissions.split(",");
            // Possui alguma permissao?
            if (!permissionsAND || permissionsAND.length == 0) {
                possuiPermissoes = false;
            } else {
                // As permissões estão divididas por E, vamos subdividi-las por OU
                var permissionsANDandOR = new Array();
                for (var i = 0; i < permissionsAND.length; i++) {
                    permissionsANDandOR.push($.trim(permissionsAND[i]).split("/"));
                }
                // Verifico cada permissao
                for (var i = 0; i < permissionsANDandOR.length && possuiPermissoes; i++) {
                    possuiPermissoes = false;
                    for (var j = 0; j < permissionsANDandOR[i].length; j++) {
                        // Se um dos itens corresponder então possuiPermissoes = TRUE
                        if ($.inArray($.trim(permissionsANDandOR[i][j]), permissionsToCheck) != -1) {
                            possuiPermissoes = true;
                            break;
                        }
                    }
                }
            }
            return possuiPermissoes;
        }
    };
})(jQuery);