﻿(function($) {
    $.fn.tree = function(method) {

        // Method calling logic
        if ($.tree[method]) {
            return $.tree[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return $.tree.initialize.apply(this, arguments);
        } else {
            $.error('Método ' + method + ' não existe em Tree.');
        }

    };

    $.tree = {

        // -----------------------------------
        // Inicialização da arvore
        // -----------------------------------
        initialize: function(callback) {

            // Deixa no div o espaço para adicionar os nós
            $(this).html("<ul></ul>");
            // Salva callback na raiz
            $(this).find("ul:first").data("treeCallback", callback);

            // Retorna
            return $(this);

        },

        // -----------------------------------
        // Default Settings de addNode
        // -----------------------------------
        addNodeDefaultOptions: {
            parent: null,
            id: null,
            text: null,
            open: false
        },

        // -----------------------------------
        // Adiciona um nó
        // -----------------------------------
        addNode: function(options) {

            // Junta as opções
            options = $.extend($.Forms.addNodeDefaultOptions, options);

            // Acha o pai
            var nodePai = null;
            if (!options.parent) {
                nodePai = $("ul:first", this);
            } else {
                nodePai = $("#" + options.parent + " ul:first", this);
            }

            // Adiciona
            nodePai.append("<li id='" + options.id + "'><span>" + options.text + "</span><ul></ul></li>");

            // Pego o <span> do nó pai e aplico a classe FOLDER
            nodePai.parent().find("span:first").removeClass("file").addClass("folder");
            if (options.open == true)
                nodePai.addClass("open")
            else
                nodePai.addClass("close")

            
            // Pega o elemento
            var node = $("#" + options.id, this);

            // Pego o <span> deste nó filho e aplico a classe FILE
            node.find("span:first").addClass("file");
            if (options.open == true)
                node.addClass("open")
            else
                node.addClass("close")

            // Retorna
            return $(this);

        },
        removeNode: function(options) {

            // Junta as opções
            options = $.extend($.Forms.addNodeDefaultOptions, options);

            // Acha o pai
            var nodePai = null;
            if (!options.parent) {
                nodePai = $("ul:first", this);
            } else {
                nodePai = $("#" + options.parent + " ul:first", this);
            }

            // Pega o elemento
            var node = $("#" + options.id, this);

            // Pego o <span> deste nó filho e aplico a classe FILE
            node.remove();
            
            return true;

        },

        // -----------------------------------
        // Finaliza
        // -----------------------------------
        finalize: function() {

            // Remove os UL de itens que não possuem filhos
            $(this).find("span[class='file']").each(function() {
                $(this).next("ul:first").remove();
            });
            // Aplico o componente TREEVIEW
            $(this).find("ul:first").addClass("filetree").treeview( {collapsed: true} );            // Processo o onMouseOver
            $(this).find(".filetree li[id*='Raiz'] li span").mouseover(
              function() {
                  $(this).addClass("hover");
              }
            );
            // Processo o onMouseOut
            $(this).find(".filetree li[id*='Raiz'] li span").mouseout(
              function() {
                  $(this).removeClass("hover");
              }
            );
            // Processo o click
            $(this).find(".filetree li[id*='Raiz'] li span").click(
              function() {
                  // Removo a seleção de outros itens
                  var raiz = $(this).closest(".filetree");
                  raiz.find(".selectedNode").removeClass("selectedNode");
                  // Adiciono um <span> somente para o TEXTO e a classe de seleção
                  var texto = $(this).text();
                  $(this).html("<span class='selectedNode'>" + texto + "</span>");
                  // Dispara callback (caso exista)
                  if (raiz.data("treeCallback"))
                      raiz.data("treeCallback")();
              }
            )
            // Retorna
            return $(this);
        },
        
       

        // -----------------------------------
        // Retorna o id do nó selecionado
        // -----------------------------------
        getSelectedNodeId: function() {

            // Retorna o id do item selecionado
            var element = $(this).find(".selectedNode");
            if (element) {
                return element.closest("li[id]").attr("id");
            }
            return null;
        }
    };
})(jQuery);

