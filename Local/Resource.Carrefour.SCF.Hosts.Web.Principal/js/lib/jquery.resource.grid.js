﻿$(document).ready(function() {

    // Define o namespace do grid
    $.grid = {

        // ----------------------------------------------------------------------
        // inicializarParam: parametros de inicializacao. Apenas para referencia
        // ----------------------------------------------------------------------
        inicializarParam: {
            grid: null,
            modeloLinha: null,
            callbackNovo: null,
            callbackEditar: null,
            callbackRemover: null,
            callbackPrepararEdicao: null,
            callbackRestaurar: null,
            permiteApenasAlterar: null,
            permissaoNovo: null,
            permissaoEditar: null,
            permissaoRemover: null
        },

        variaveis: {},

        // ----------------------------------------------------------------------
        // inicializar: faz a inicialização do grid
        // ----------------------------------------------------------------------
        inicializar: function(param) {

            $.grid.variaveis.permiteApenasAlterar = param.permiteApenasAlterar;

            // Inicializa
            var id = param.grid.attr("id");
            var idNovaLinha = "novo_" + id;
            var btnIncluir = param.permissaoNovo ? "<input type='image' src='img/button_white_add.png' style='width: 16px; height: 16px;' Value='Adicionar' title='Adicionar' class='grid[showForPermissions[" + param.permissaoNovo + "]]' />&nbsp;" : "<input type='image' src='img/button_white_add.png' style='width: 16px; height: 16px;' Value='Adicionar' title='Adicionar' />&nbsp;";
            var btnLimpar = param.permissaoNovo ? "<input type='image' src='img/edit_clear.png' style='width: 16px; height: 16px;' title='Limpar' class='grid[showForPermissions[" + param.permissaoNovo + "]]' />" : "<input type='image' src='img/edit_clear.png' style='width: 16px; height: 16px;' title='Limpar' />";

            if ($.grid.variaveis.permiteApenasAlterar != true) {

                // Adiciona nova linha e coloca em edição
                param.grid.jqGrid("addRowData", idNovaLinha,
                { Acao: btnIncluir + btnLimpar });
                param.grid.jqGrid("editRow", idNovaLinha, true);
            }

            // Coloca a nova linha junto com o cabecalho
            $("#gview_" + id + " .ui-jqgrid-htable thead").append($("#" + idNovaLinha));

            // Trata os botões de novo item
            var acoes = $(".ui-jqgrid-hdiv .ui-jqgrid-htable .jqgrow td:last input", param.grid.closest(".ui-jqgrid-view"));

            // --------------------------------------------------
            // Evento de Novo Item
            // --------------------------------------------------
            $(acoes[0]).click(function() {

                // Colhe os valores da linha
                var novaLinha = $.extend({}, param.modeloLinha);
                $.grid.montarLinha($("#novo_" + id), novaLinha);

                // Dispara callback de nova linha
                // Poderá retornar ok, com um código de linha, ou erro (de negócio), indicando que não deve fazer nada
                if (param.callbackNovo) {
                    param.callbackNovo({ valores: novaLinha, callbackOK: function(param) {

                        // Verifica se retornou valores
                        if (!param.valores)
                            param.valores = novaLinha;

                        // Salva a nova linha
                        $("#" + id).jqGrid("addRowData", id + "_" + param.rowid, param.valores);

                        // Pega referencia da linha criada
                        var linha = $("#" + id + "_" + param.rowid);

                        // Resseta a linha de adição
                        $.grid.limparLinha($("#" + idNovaLinha));

                    }
                    });
                } else {

                    // Gera um id para a nova linha
                    var rowid = id + "_" + new Date().getTime();

                    // Salva a nova linha
                    $("#" + id).jqGrid("addRowData", rowid, novaLinha);

                    // Pega referencia da linha criada
                    var linha = $("#" + rowid);

                    // Resseta a linha de adição
                    $.grid.limparLinha($("#" + idNovaLinha));

                }
            });

            // --------------------------------------------------
            // Evento Limpar Valores de Novo Item
            // --------------------------------------------------
            $(acoes[1]).click(function() {

                // Limpa a linha
                $.grid.limparLinha($(this).closest("tr"));

            });

            // --------------------------------------------------
            // Complemento de Novo Item
            // --------------------------------------------------
            param.grid.jqGrid("setGridParam", {
                afterInsertRow: function(rowid, rowdata, rowelem) {

                    if (rowid.toString().indexOf('_') < 0) {
                        rowidAux = this.id + "_" + rowid;
                        $("#" + rowid).attr("id", rowidAux);
                        rowid = rowidAux;
                    }
                    //this.id = rowid;
                    // Monta acoes de edição
                    var btnEditar = param.permissaoEditar ? "<input id='Editar_" + rowid + "' type='image' src='img/pen.png' style='width: 16px; height: 16px;' title='Editar'  class='grid[showForPermissions[" + param.permissaoEditar + "]]' />&nbsp;" : "<input id='Editar_" + rowid + "' type='image' src='img/pen.png' style='width: 16px; height: 16px;' title='Editar' />&nbsp;";
                    var btnRemover = param.permissaoRemover ? "<input id='Remover_" + rowid + "' type='image' src='img/trashcan_empty_alt.png' style='width: 16px; height: 16px;' title='Remover'  class='grid[showForPermissions[" + param.permissaoRemover + "]]' />" : "<input id='Remover_" + rowid + "' type='image' src='img/trashcan_empty_alt.png' style='width: 16px; height: 16px;' title='Remover' />";

                    // Item criado para quando for permitido apenas alterar
                    var btnRemoverVazio = "<input id='Remover_" + rowid + "' type='image' src='img/trashcan_empty_alt.png' style='width: 16px; height: 16px; display: none;' title='Remover' />";

                    var btnSalvar = param.permissaoEditar ? "<input id='Salvar_" + rowid + "' type='image' src='img/save_1.png' style='width: 16px; height: 16px; display: none;' title='Salvar'  class='grid[showForPermissions[" + param.permissaoEditar + "]]'/>&nbsp;" : "<input id='Salvar_" + rowid + "' type='image' src='img/save_1.png' style='width: 16px; height: 16px; display: none;' title='Salvar' />";
                    var btnRestaurar = param.permissaoEditar ? "<input id='Restaurar_" + rowid + "' type='image' src='img/icone_restaurar.png' style='width: 16px; height: 16px; display: none;' title='Restaurar'  class='grid[showForPermissions[" + param.permissaoEditar + "]]'/>" : "<input id='Restaurar_" + rowid + "' type='image' src='img/icone_restaurar.png' style='width: 16px; height: 16px; display: none;' title='Restaurar' />";
                    var btnCancelar = param.permissaoEditar ? "<input id='Cancelar_" + rowid + "' type='image' src='img/button_white_remove.png' style='width: 16px; height: 16px; display: none;' title='Cancelar' class='grid[showForPermissions[" + param.permissaoEditar + "]]' />" : "<input id='Cancelar_" + rowid + "' type='image' src='img/button_white_remove.png' style='width: 16px; height: 16px; display: none;' title='Cancelar' />";

                    if ($.grid.variaveis.permiteApenasAlterar != true) {
                        var botoesAcoes = btnEditar + btnRemover + btnSalvar + btnRestaurar + btnCancelar;
                    } else {
                        var botoesAcoes = btnEditar + btnRemoverVazio + btnSalvar + btnRestaurar + btnCancelar;
                    }
                    $("#" + rowid + " td:last").html(botoesAcoes);

                    //rowdata.Acao = botoesAcoes;

                    // Salva o array de dados na propria linha
                    $("#" + rowid).data("valores", rowdata);

                    // Captura os eventos de edicao
                    var acoes = $("#" + rowid + " td:last input");

                    // --------------------------------------------------
                    // Evento Editar
                    // --------------------------------------------------
                    $(acoes[0]).click(function() {

                        // Acha o id da linha
                        var linha = $($(this).closest("tr"));
                        var idLinha = linha.attr("id");

                        // Acha o elemento do grid
                        var grid = $(this).closest("table");

                        // Pede para editar
                        $(grid).jqGrid("editRow", idLinha, false, param.callbackPrepararEdicao);

                        // Mostra o botão de salvar e esconde o resto
                        $(acoes[0]).hide(); // EDITAR
                        $(acoes[1]).hide(); // REMOVER
                        $(acoes[2]).show(); // SALVAR
                        $(acoes[3]).hide(); // RESTAURAR
                        $(acoes[4]).show(); // CANCELAR
                    });

                    // --------------------------------------------------
                    // Evento Remover
                    // --------------------------------------------------
                    $(acoes[1]).click(function() {

                        // Acha a linha e o id 
                        var linha = $($(this).closest("tr"));
                        var idLinha = linha.attr("id");

                        // Acha o grid
                        var grid = $(this).closest("table");

                        // Pega os valores da linha
                        var valores = linha.data("valores");

                        // Pede confirmacao
                        popup({
                            mensagem: "Deseja excluir este ítem?",
                            titulo: "Excluir Ítem",
                            icone: "atencao",
                            callbackSim: function() {

                                // Se houver, chama o callback para remover
                                if (param.callbackRemover) {
                                    param.callbackRemover({
                                        rowid: valores[param.colunaId],
                                        callbackOK: function() {
                                            $(grid).jqGrid("delRowData", idLinha);
                                            $.grid.applyPermissions();
                                        }
                                    });
                                } else {
                                    $(grid).jqGrid("delRowData", idLinha);
                                }

                            },
                            callbackNao: function() { }
                        });

                    });

                    // --------------------------------------------------
                    // Evento Salvar
                    // --------------------------------------------------
                    $(acoes[2]).click(function() {

                        // Acha a linha e o id 
                        var linha = $($(this).closest("tr"));
                        var idLinha = linha.attr("id");

                        // Acha o grid
                        var grid = $(this).closest("table");

                        // Colhe os valores da linha
                        var valores = linha.data("valores");
                        $.grid.montarLinha($("#" + idLinha), valores);

                        // Chama o callback de edição
                        if (param.callbackEditar) {
                            param.callbackEditar({ id: idLinha, valores: valores, tr: linha, callbackOK: function(param) {

                                // Verifica se retornou os valores
                                if (!param.valores) {
                                    param.valores = valores;
                                }

                                // Salva o valor da linha
                                linha.data("valores", param.valores);

                                // Efetiva valores no grid
                                $(grid).jqGrid("saveRow", idLinha, false, "clientArray");
                                $(grid).jqGrid("setRowData", idLinha, param.valores);

                                // Esconde o botão de salvar e mostra o resto
                                $(acoes[0]).show();
                                // Deve mostrar o botao remover?
                                if ($.grid.variaveis.permiteApenasAlterar != true) {
                                    $(acoes[1]).show();
                                } else {
                                    $(acoes[1]).hide();
                                }
                                $(acoes[2]).hide();
                                $(acoes[3]).hide();
                                $(acoes[4]).hide();
                            }
                            });
                        } else {

                            // Salva o valor da linha
                            linha.data("valores", valores);

                            // Efetiva valores no grid
                            $(grid).jqGrid("saveRow", idLinha, false, "clientArray");
                            $(grid).jqGrid("setRowData", idLinha, param.valores);

                            // Esconde o botão de salvar/restaurar e mostra o resto
                            $(acoes[0]).show();
                            // Deve mostrar o botao remover?
                            if ($.grid.variaveis.permiteApenasAlterar != true) {
                                $(acoes[1]).show();
                            } else {
                                $(acoes[1]).hide();
                            }
                            $(acoes[2]).hide();
                            $(acoes[3]).hide();
                            $(acoes[4]).hide();
                        }

                    });
                    //------------------------------------
                    // Evento Cancelar
                    //------------------------------------
                    $(acoes[4]).click(function() {
                        // Acha a linha e o id 
                        var linha = $($(this).closest("tr"));
                        var idLinha = linha.attr("id");

                        // Acha o grid
                        var grid = $(this).closest("table");

                        // Pega os valores da linha
                        var valores = linha.data("valores");

                        // Se houver, chama o callback para remover
                        if (param.callbackCancelar) {
                            param.callbackCancelar({
                                rowid: idLinha, //valores[param.colunaId],
                                callbackOK: function() {

                                    // Acha o grid
                                    //var grid = $(this).closest("table");
                                    $(grid).jqGrid("restoreRow", idLinha, function() {

                                        // Esconde o botão de salvar e mostra o resto
                                        $(acoes[0]).show();
                                        // Deve mostrar o botao remover?
                                        if ($.grid.variaveis.permiteApenasAlterar != true) {
                                            $(acoes[1]).show();
                                        } else {
                                            $(acoes[1]).hide();
                                        }
                                        $(acoes[2]).hide();
                                        $(acoes[3]).hide();
                                        $(acoes[4]).hide();
                                    });
                                }
                            });
                        } else {

                            // Acha o grid
                            //var grid = $(this).closest("table");
                            $(grid).jqGrid("restoreRow", idLinha, function() {

                                // Esconde o botão de salvar e mostra o resto
                                $(acoes[0]).show();
                                // Deve mostrar o botao remover?
                                if ($.grid.variaveis.permiteApenasAlterar != true) {
                                    $(acoes[1]).show();
                                } else {
                                    $(acoes[1]).hide();
                                }
                                $(acoes[2]).hide();
                                $(acoes[3]).hide();
                                $(acoes[4]).hide();
                            });
                        }
                    });

                    // --------------------------------------------------
                    // Evento Restaurar
                    // callbackRestaurar
                    // --------------------------------------------------
                    $(acoes[3]).click(function() {

                        // Acha a linha e o id 
                        var linha = $($(this).closest("tr"));
                        var idLinha = linha.attr("id");

                        // Acha o grid
                        var grid = $(this).closest("table");

                        // Pega os valores da linha
                        var valores = linha.data("valores");

                        // Pede confirmacao
                        popup({
                            mensagem: "Deseja incluir este ítem novamente na lista?",
                            titulo: "Restaurar Item",
                            icone: "atencao",
                            callbackSim: function() {

                                // Se houver, chama o callback para remover
                                if (param.callbackRestaurar) {
                                    param.callbackRestaurar({
                                        rowid: valores[param.colunaId],
                                        callbackOK: function() {
                                            $(grid).jqGrid("delRowData", idLinha);
                                        }
                                    });
                                } else {
                                    $(grid).jqGrid("delRowData", idLinha);
                                }
                            },
                            callbackNao: function() { }
                        });

                    });
                }
            });

            // Pede a lista de itens
            if (param.callbackListar) {
                param.callbackListar({
                    callbackOK: function(param2) {
                        // Varre os elementos adicionando no grid
                        for (var i = 0; i < param2.resultado.length; i++) {
                            param.grid.jqGrid("addRowData", id + "_" + param2.resultado[i][param.colunaID], param2.resultado[i]);
                        }
                    }
                });
            }
        },

        // ----------------------------------------------------------------------
        // limparLinha: limpa os valores dos campos da linha
        // ----------------------------------------------------------------------
        limparLinha: function(tr) {

            // Acha a linha e o id 
            var linha = $(tr);
            var idLinha = linha.attr("id");

            // Varre os campos limpando os valores
            var campos = $("td", linha);
            for (var i = 0; i < campos.length; i++) {
                var campo = $(campos[i]);
                var input = $(campo.children().first());
                if ($(input).attr("type") == "text") {
                    $(input).val("");
                }
                if (input.attr("type") == "select-one") {
                    $(input).val("?");
                    $(input).val("");
                }
                if (input.attr("type") == "checkbox") {
                    $(input).attr("checked", false);
                }
            }

        },

        // ----------------------------------------------------------------------
        // montarLinha: preenche o objeto com os valores da linha
        // ----------------------------------------------------------------------
        montarLinha: function(tr, obj) {
            var campos = $("td", tr);
            for (var i = 0; i < campos.length; i++) {
                var campo = $(campos[i]);
                var input = campo.children().first();
                if (input.attr("type") != "button") {
                    var nomeCampo = campo.attr("aria-describedby");
                    nomeCampo = nomeCampo.substr(nomeCampo.lastIndexOf("_") + 1);
                    if (input.attr("type") == "checkbox" || input.attr("type") == "radio") {
                        obj[nomeCampo] = input.attr("checked");
                    } else {
                        if (input.val())
                            obj[nomeCampo] = input.val();
                    }
                }
            }
        },

        //Esconde os botões do grid.
        esconderBotoes: function(rowid, esconder) {

            if (esconder) {
                var acoes = $("#" + rowid + " td:last input");
                $(acoes[0]).hide();
                $(acoes[1]).hide();
                $(acoes[2]).hide();
                $(acoes[3]).show();
                $(acoes[4]).hide();
            }
        },

        // -----------------------------------
        // Opções default para o método showValidation
        // -----------------------------------
        validateDefaultOptions: {
            pGrid: null,
            param: null,
            returnErrors: false,
            errors: {}
        },

        validate: function(options) {
            options = $.extend($.grid.validateDefaultOptions, options);
            // Prepara a coleção de críticas
            var criticas = new Array();

            var colModel = options.pGrid.jqGrid('getGridParam', 'colModel');

            for (var i = 0; i < colModel.length; i++) {

                if (colModel[i].classes) {
                    var opcoes = $.grid.getFormClassOptions(colModel[i].classes);

                    // Verifica se já há mensagem de erro para este campo
                    if (options.errors[opcoes.property]) {
                        criticas.push({ property: opcoes.property,
                            message: options.errors[opcoes.property], element: $(this)
                        });
                    }
                    else {
                        // Extrai o valor
                        var valorCampo = options.param.valores[colModel[i].name];
                        var dataValida = true;

                        // Valida tipo do campo
                        if (valorCampo != "" && valorCampo != null) {
                            if (opcoes.dataType == "date" || opcoes.dataType == "dateTime") {

                                if (opcoes.dataType == "date" && $.parseDate(valorCampo, $.Forms.maskOptions.date) == null) {
                                    criticas.push({ property: opcoes.property,
                                        message: opcoes.errorDate, element: $(this)
                                    });
                                    dataValida = false;
                                }
                                else if (opcoes.dataType == "dateTime" && $.parseDate(valorCampo, $.Forms.maskOptions.dateTime) == null) {
                                    criticas.push({ property: opcoes.property,
                                        message: opcoes.errorDate, element: $(this)
                                    });
                                    dataValida = false;
                                }
                            }
                            else if (opcoes.dataType == "decimal" && isNaN($.parseFloat(valorCampo))) {
                                criticas.push({ property: opcoes.property,
                                    message: opcoes.errorDecimal, element: $(this)
                                });
                            }
                            else if (opcoes.dataType == "integer" && isNaN($.parseInt(valorCampo))) {
                                criticas.push({ property: opcoes.property,
                                    message: opcoes.errorInteger, element: $(this)
                                });
                            } else if (opcoes.dataType == "integer"
                                                           && valorCampo.indexOf($.culture.numberFormat["."]) != -1) {
                                criticas.push({ property: opcoes.property,
                                    message: opcoes.errorInteger, element: $(this)
                                });
                            }
                        }

                        // Valida opcional ou requerido
                        if (opcoes.required == true && (valorCampo == null || valorCampo == "" || valorCampo == "?")) {
                            criticas.push({ property: opcoes.property, message: opcoes.errorMissing, element: $(this) });
                        }

                        // Valida se a data informada é menor ou igual à data atual
                        if (opcoes.verificaDataAtual && dataValida) {
                            var dataComparar = $.parseDate(valorCampo, opcoes.dataType == "date" ? $.Forms.maskOptions.date : $.Forms.maskOptions.dateTime);
                            var today = new Date();
                            if (dataComparar > today) {
                                criticas.push({
                                    property: opcoes.property,
                                    message: opcoes.errorDateOutOfRange,
                                    element: $(this)
                                });
                            }
                        }
                    }
                }
            }
            // Conta quantidade de criticas e adiciona os labels
            var qtdeCriticas = 0;
            for (var c in criticas) {
                criticas[c].labelValue = criticas[c].property;
                qtdeCriticas++;
            }


            // Retorna
            if (options.returnErrors)
                return criticas;   // Retorna criticas
            else
                return qtdeCriticas == 0; // Retorna se o form está valido
        },

        // -----------------------------------
        // Opções default para o valor forms da classe
        // -----------------------------------
        formClassDefaultOptions: {
            required: false,
            optional: false,
            dataType: "string",
            property: null,
            defaultValue: null,
            label: null,
            errorCampoConfiguracao: "O valor deve ser 'S' de Sim ou 'N' de Não!",
            errorDate: "Data inválida",
            errorInteger: "Número Inválido",
            errorDecimal: "Número Decimal Inválido",
            errorMissing: "Campo Obrigatório",
            allowNegative: false,
            maskType: null,
            customMask: null,
            placeHolder: " ",
            datePicker: true,
            toUpperCase: false,
            enableForPermissions: null,
            disableForPermissions: null,
            showForPermissions: null,
            hideForPermissions: null,
            errorDouble: "O número deve ser diferente de zero!", //SCF1253 - Marcos Matsuoka
            errorDateOutOfRange: "A data informada deve ser menor ou igual a data de hoje."
        },

        // -----------------------------------
        // Faz a extração das opções de forms de uma classe
        // -----------------------------------
        getFormClassOptions: function(classAttr) {

            // Junta com os valores default e especificos do campo, caso exista
            var opcoesRetornar = {};
            // Faz a separação das opções desejadas
            var posForms = classAttr.indexOf("grid");                 // Acha o inicio da palavra forms
            // Se não encontrou a chave grid, retorna vazio.
            if (posForms == -1)
                return opcoesRetornar;
            var posFimForms = classAttr.indexOf("] ", posForms); // Acha o possível fim de forms
            var classValue = "";                                            // Extrai apenas esta parte
            if (posFimForms == -1)
                classValue = classAttr.substring(posForms, classAttr.length - 1);
            else
                classValue = classAttr.substring(posForms, posFimForms - posForms);
            classValue = classValue.replace("grid[", "");             // Retira o início
            classValue = classValue.split(";");                             // Separa as opções

            // Monta objeto com as opções
            var opcoes = {};
            for (var i = 0; i < classValue.length; i++) {
                var opcaoTemp = classValue[i];
                var nome = "";
                var valor = true;
                var posColchete = opcaoTemp.indexOf("[");
                if (posColchete == -1) {
                    nome = opcaoTemp;
                } else {
                    nome = opcaoTemp.substring(0, posColchete);
                    valor = opcaoTemp.substring(posColchete + 1, opcaoTemp.length - 1);
                }
                if (nome && nome != "")
                    eval("opcoes." + nome + " = valor;");
            }


            $.extend(opcoesRetornar, $.grid.formClassDefaultOptions, opcoes);

            // Retorna 
            return opcoesRetornar;
        },

        // -----------------------------------
        // Acha o tipo do controle
        // -----------------------------------
        getControlType: function(elemento) {
            switch (elemento[0].tagName.toUpperCase()) {
                case "SELECT":
                    return "select";
                    break;
                case "INPUT":
                    switch (elemento.attr("type").toUpperCase()) {
                        case "BUTTON":
                            return "button";
                            break;
                        case "RADIO":
                            return "radio";
                            break;
                        case "CHECKBOX":
                            return "checkbox";
                            break;
                        default:
                            return "text";
                    }
                    break;
                case "TEXTAREA":
                    return "textarea";
                    break;
                case "LABEL":
                    return "label";
                    break;
            }
            return null;
        },


        // --------------------------------------------------
        // Aplica regras de bloqueio e vizualização de campos
        // --------------------------------------------------
        applyPermissions: function(grid) {

            // Executa para todos campos (incluindo botões)
            $("[class*='grid']").find('input').each(function() {

                // Obtenho as permissoes do usuario
                var permissoes = template_default_permissoes_carregadas;
                // Obtenho as opções
                var opcoes = $.grid.getFormClassOptions($(this).attr("class"));
                // Verifica o tipo do controle
                var tipoControle = $.grid.getControlType($(this));

                // As permissoes podem ser E ou OU, por exemplo:
                // "PERMISSAO_1 / PERMISSAO_2, PERMISSAO_3"
                // Indica que o usuario deve ter a permissao 1 ou 2 E a pemrmissão 3, ok?

                if (permissoes) {

                    // Se for Admin, sempre mostra ou habilita.
                    if ($.inArray("PermissaoAdministrador", permissoes) >= 0) {

                        mostrar = true;
                        habilitar = true;

                    } else {
                        // MOSTRAR?
                        var mostrar = true;
                        if (opcoes.showForPermissions) {
                            // Possui as permissões?
                            mostrar = $.grid.hasPermissions(opcoes.showForPermissions, permissoes);
                        }

                        // HABILITAR?
                        var habilitar = true;
                        if (opcoes.enableForPermissions) {
                            // Possui as permissões?
                            habilitar = $.grid.hasPermissions(opcoes.enableForPermissions, permissoes);
                        }

                        // DESABILITAR?
                        if (opcoes.disableForPermissions) {
                            // Possui as permissões?
                            habilitar = !$.grid.hasPermissions(opcoes.disableForPermissions, permissoes);
                        }

                        // ESCONDER?
                        if (opcoes.hideForPermissions) {
                            // Possui as permissões?
                            mostrar = !$.grid.hasPermissions(opcoes.hideForPermissions, permissoes);
                        }
                    }


                    // Elemento(s)
                    var elemento = $(this);
                    // Se for um radio, devo pegar todos os RADIOs com o mesmo nome
                    if (tipoControle == "radio") {
                        var nomeControle = elemento.attr("name");
                        elemento = $("input[name='" + nomeControle + "']:radio");
                    }

                    // Aplico as ações processadas
                    if (mostrar) {
                        if (!$(elemento).is(':hidden'))
                            elemento.show();

                        if (habilitar) {
                            // Se for um botão, trato de um outro jeito
                            if (tipoControle == "button") {
                                elemento.button("enable");
                            } else {
                                elemento.removeAttr("disabled");
                            }
                        } else {
                            if (tipoControle == "button") {
                                elemento.button("disable");
                            } else {
                                elemento.attr("disabled", "disabled");
                            }
                        }
                    } else {
                        elemento.hide();
                    }
                }
            });
        },

        hasPermissions: function(permissions, permissionsToCheck) {

            // Possui as permissoes ?
            var possuiPermissoes = true;

            // Transformo em array
            var permissionsAND = permissions.split(",");
            // Possui alguma permissao?
            if (!permissionsAND || permissionsAND.length == 0) {
                possuiPermissoes = false;
            } else {
                // As permissões estão divididas por E, vamos subdividi-las por OU
                var permissionsANDandOR = new Array();
                for (var i = 0; i < permissionsAND.length; i++) {
                    permissionsANDandOR.push($.trim(permissionsAND[i]).split("/"));
                }
                // Verifico cada permissao
                for (var i = 0; i < permissionsANDandOR.length && possuiPermissoes; i++) {
                    possuiPermissoes = false;
                    for (var j = 0; j < permissionsANDandOR[i].length; j++) {
                        // Se um dos itens corresponder então possuiPermissoes = TRUE
                        if ($.inArray($.trim(permissionsANDandOR[i][j]), permissionsToCheck) != -1) {
                            possuiPermissoes = true;
                            break;
                        }
                    }
                }
            }
            return possuiPermissoes;
        }
    };
});