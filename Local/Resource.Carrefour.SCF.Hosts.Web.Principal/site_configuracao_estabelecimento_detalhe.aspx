﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_configuracao_estabelecimento_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_configuracao_estabelecimento_detalhe.js" type="text/javascript"></script>
    <title>SCF - Cadastro de Estabelecimentos</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
     <div id="site_configuracao_estabelecimento_detalhe" style="margin: 15px;">
        
        <table>
            <tr>
                <td style="width: 80px;">
                    <label id="lblCNPJ">CNPJ:</label>
                </td>
                <td>
                    <input type="text" id="txtCNPJ" class="forms[property[CNPJ];dataType[integer];required]" style="width: 140px;" maxlength="15" />
                </td>
                
                <td style="padding-left: 5px;">
                    <label id="lblCodigoCSU">Código CSU:</label>
                </td>
                <td>
                    <input type="text" id="txtEstabelecimentoCSU" class="forms[property[EstabelecimentoCSU]]" style="width: 80px; " maxlength="10"/>
                </td>
                
                <td style="padding-left: 5px;">
                    <label id="Label1">Código Sitef:</label>
                </td>
                <td>
                    <input type="text" id="txtEstabelecimentoSitef" class="forms[property[EstabelecimentoSitef]]" style="width: 80px;" maxlength="10"/>
                </td>
            </tr>
        </table>
        
        <table>
            <tr>
                <td style="width: 80px;">
                    <label id="lblRazaoSocial">Razão Social:</label>
                </td>
                <td>
                    <input type="text" id="txtRazaoSocial" class="forms[property[RazaoSocial];required]" style="width: 478px;" maxlength="200"/>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td style="width: 80px;">
                    <label>Grupo:</label>
                </td>
                <td>
                    <select id="cmbGrupo" class="forms[property[CodigoEmpresaGrupo];required]" style="width: 204px;">
                        <option value="?">(selecione)</option>
                    </select>                        
                </td>
                <td style="padding-left: 5px;">
                     <label>Subgrupo:</label>
                </td>
                <td>
                     <select id="cmbSubgrupo" class="forms[property[CodigoEmpresaSubGrupo];required]" style="width: 204px;">
                        <option value="?">(selecione)</option>
                    </select>
                </td>
                <td>
                    

                </td>                
            </tr>
        </table>

        <table>
            <tr>
                <td style="width: 80px;">
                    <label>Favorecido:</label>
                </td>
                <td>
                    <select id="cmbFavorecido" class="forms[property[CodigoFavorecido]]" style="width: 480px;">
                        <option value="?">(selecione)</option>
                    </select>
                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>
