﻿<%@ Page Title="Carrefour Soluções Financeiras" Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
	    <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=EDGE,chrome=1" />

        <!-- ESTILOS -->
        <link rel="stylesheet" media="screen" href="template_default.css" />
        <link rel="stylesheet" media="screen" href="login.css" />
        <!-- ESTILOS -->
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.7.custom.css" rel="stylesheet" />	
		<link type="text/css" href="css/jquery.treeview.custom.css" rel="stylesheet" />	
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />
        <link rel="stylesheet" media="screen" href="menu.principal.css" />
        <link rel="stylesheet" media="screen" href="menu.layout.horizontal.css" />
        <!-- ICONE FAVORITO -->
        <link rel="shortcut icon" href="./img/favicon.ico" />
    </head>
    <body>
        <form id="frm" runat="server">
        <div id="main">
            <div id="cabecalho" >
                <img alt="logo" id="logoLogin" src="img/lateralHeader.png"  />                               
                <%--// 10/2018 SegurancaAuditoria Projeto 
                <div id="opcoesGlobais">
                    <span>Versão: </span><asp:Label ID="lblVersao" runat="server"></asp:Label>
                    <span>  [Ambiente: </span><asp:Label ID="lblAmbiente" runat="server" Text="" /><span>]</span><!-- INFORMANDO O AMBIENTE -->
                </div>--%>
            </div>
            <div id="conteudo">
                <br /><br /><br />
                <table style="width:100%">
                    <tr>
                        <td align="right"><span>Usuário:</span></td>
                        <td><asp:TextBox ID="txtUsuario" runat="server" Text="Admin" maxlength="9"/></td> <%--1436 limitando ao tamanho da base de dados que é 10 mas na tela tem que ser 9 --%>
                    </tr>
                    <tr>
                        <td align="right"><span>Senha:</span></td>
                        <td><asp:TextBox ID="txtSenha" runat="server" Text="123" TextMode="Password" focus/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td  align="left">
                            <asp:Button ID="cmdLogin" runat="server" Text="Login" onclick="cmdLogin_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><br />
                            <span id="usuarioInvalido" runat="server" style="font-size: medium"></span>
                        </td>
                    </tr>
                </table>
                
            </div>
            <div id="rodape_login">Desenvolvido por Resource IT Solutions - Versão 1.1.0.24</div>
        </div>
        </form>
    </body>
</html>
