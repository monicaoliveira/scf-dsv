﻿/// <reference path="../../lib/jquery/dev/jquery-1.4.1-vsdoc.js" />
/// <reference path="servico.aspx.js" />

$(document).ready(function() {
    $("#sub_default").addClass("sub_espacado");
    $("ul.menu li a").click(function() {
        $("#sub_default").addClass("sub_espacado");
        $(".submenu").addClass("sub_escondido");
        if ($(this).attr("name") && $(this).attr("href").charAt(0) == "#") {
            $($(this).attr("name")).removeClass("sub_escondido");
            $("#sub_default").removeClass("sub_espacado");
        }
        else {
            $("#sub_default").removeClass("sub_escondido");
        }
    });

});