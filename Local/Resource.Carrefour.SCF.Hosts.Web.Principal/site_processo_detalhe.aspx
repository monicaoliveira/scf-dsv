<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_processo_detalhe.js"></script>
    <link href="site_processo_detalhe.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="site_log_lista.js"></script>
    <script type="text/javascript" src="site_log_detalhe.js"></script>
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_item_lista.js" type="text/javascript"></script>
    <script src="site_critica_lista.js" type="text/javascript"></script>
    <script src="site_critica_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_lista.js" type="text/javascript"></script>
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <link href="site_log_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_log_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_arquivo_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_arquivo_item_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_detalhe.css" rel="stylesheet" type="text/css" />
    
    <title>SCF - Detalhe de Processo</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--    Dialog de Detalhe de Processo     -->
    <!-- ------------------------------------ -->
    <div id="site_processo_detalhe">
    
        <!-- Cabe�alho -->
        <div id="site_processo_detalhe_aba_detalhe" class="cabecalho">
            
            <label>Processo:</label>
            <input type="text" class="forms[property[CodigoProcesso]]" readonly />
            
            <span class="separador" />
            
            <label>Tipo do Processo:</label>
            <input type="text" class="forms[property[TipoProcesso]]" readonly />
            <br />

            <label>Estagio:</label>
            <input type="text"  class="forms[property[TipoEstagioAtualString]]" readonly />

            <span class="separador" />
            
            <label>Data de Inicio:</label>
            <input type="text" class="forms[property[DataInclusao]]" readonly />
            <br />
            
            <label>Status Estagio:</label>
            <input type="text" class="forms[property[StatusEstagio]]" readonly />
            <br />
            
             <label>Status Processo:</label>
            <input type="text" class="forms[property[StatusProcesso]]" readonly />
            <br />
            
        </div>
        
          <!-- Abas -->
        <div class="abas">
            <ul>
                <li><a href="#site_processo_detalhe_aba_arquivos">Lista Arquivos</a></li>
            </ul>
            
            <div id="site_processo_detalhe_aba_arquivos" style="padding:2px;">
                <table id="tbArquivos"></table>
                <button class="cmdDetalheArquivoProcesso" style="margin-top:10px; margin-left:2px">Detalhar</button> 
                <button class="cmdInconsDetal" style="margin-top:10px; margin-left:2px">Gerar Excel Inconsistencias</button> <!--1463-->
                <button class="cmdInconsAgrup" style="margin-top:10px; margin-left:2px">Gerar Excel Inconsistencias Agrupadas</button> <!--1463-->
            </div>
        </div>
    </div>
</asp:Content>
