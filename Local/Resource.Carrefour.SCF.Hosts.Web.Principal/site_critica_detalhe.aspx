﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_critica_detalhe.js"></script>
    <link href="site_critica_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Crítica Detalhe</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- Conteúdo da página -->
    <div id="site_critica_detalhe">
    
        <!-- Abas -->
        <ul>
            <li><a href="#criticaAbaDetalhe">Detalhe</a></li>
            <li><a href="#criticaAbaPropriedades">Todas as Propriedades</a></li>
        </ul>
    
        <!-- Área de detalhe -->
        <div id="criticaAbaDetalhe" class="detalhe">
            <table>
                <tr><td>
                        <span>Código:</span>
                    </td><td>
                        <input type="text" class="forms[property[CodigoCritica]]" readonly/>
                    </td><td>
                        <span>Tipo:</span>
                    </td><td>
                        <input type="text" class="forms[property[TipoCritica]]" readonly />
                </td></tr><tr><td>
                        <span>Descrição:</span>
                    </td><td colspan="3">
                        <textarea type="text" rows="10" class="forms[property[Descricao]]" style="width: 348px; " 
                            readonly></textarea>
                </td></tr>
            </table>
        </div>
    
        <!-- Container de propriedades -->
        <div id="criticaAbaPropriedades"></div>
    
    </div>

</asp:Content>
