﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.processo)
        $.processo = {};

    // Funcoes do namespace processo.lista
    $.processo.atacadao = {

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Processos > Processo Atacadão");

            // Layout dos botões
            $("#site_processo_atacadao button").button();

            // Processamento
            $("#cmdProcessarAtacadao").click(function() {

                // Monta o request
                var request = {
                    ExecutarAssincrono: true,
                    Config: {
                        CaminhoArquivoCSU: $("#txtArquivoCSU").val() != "" ? validarCamposDiretorio($("#txtArquivoCSU").val()) + $("#txtArquivoCSUArquivo").val() : null,
                        CaminhoArquivoTSYS: $("#txtArquivoTSYS").val() != "" ? validarCamposDiretorio($("#txtArquivoTSYS").val()) + $("#txtArquivoTSYSArquivo").val() : null,
                        CaminhoArquivoSitef: $("#txtArquivoSitef").val() != "" ? validarCamposDiretorio($("#txtArquivoSitef").val()) + $("#txtArquivoSitefArquivo").val() : null,
                        CaminhoArquivoMateraFinanceiro: $("#txtArquivoMatera").val() != "" ? validarCamposDiretorio($("#txtArquivoMatera").val()) : null,
                        CaminhoArquivoResultadoConciliacao: $("#txtArquivoConciliacao").val() != "" ? validarCamposDiretorio($("#txtArquivoConciliacao").val()) : null,
                        CaminhoArquivoCSUSemAtacadao: $("#txtArquivoCSUSemAtacadao").val() != "" ? validarCamposDiretorio($("#txtArquivoCSUSemAtacadao").val()) : null,
                        CaminhoArquivoMateraContabil: $("#txtArquivoMateraContabil").val() != "" ? validarCamposDiretorio($("#txtArquivoMateraContabil").val()) : null
                    }
                };

                //SCF1213 - Marcos Matsuoka - inicio
                if  ( ($("#txtArquivoTSYS").val()        == ""  || $("#txtArquivoTSYS").val() == null)            &&
                      ($("#txtArquivoTSYSArquivo").val() == ""  || $("#txtArquivoTSYSArquivo").val() == null)     &&
                     
                      ($("#txtArquivoSitef").val()        == "" || $("#txtArquivoSitef").val() == null)           &&
                      ($("#txtArquivoSitefArquivo").val() == "" || $("#txtArquivoSitefArquivo").val() == null)    &&
                    
                      ($("#txtArquivoCSUArquivo").val()  == ""  || $("#txtArquivoCSUArquivo").val() == null)      &&
                      ($("#txtArquivoCSU").val()         == ""  || $("#txtArquivoCSU").val() == null)
					) 
					{
                    // Pede para preencher pelo menos um campo
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor preencher pelo menos uma IMPORTAÇÃO - Drietorio e Nome do Arquivo",
                        callbackOK: function() { }
                    });
				}	else if // informou o DIRETORIO ou ARQUIVO tsys
                    ( 	(		$("#txtArquivoTSYS").val()        == ""  &&
						        $("#txtArquivoTSYSArquivo").val() != ""  
						)   ||
						(       $("#txtArquivoTSYS").val()        != ""  &&
						        $("#txtArquivoTSYSArquivo").val() == ""  
						)																							
					)  
                    {
                        popup
						({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Arquivo TSYS - Preencher o Diretorio e o Nome do Arquivo",							
                            callbackOK: function() { }
						});
				}	else if // informou o DIRETORIO ou ARQUIVO csu
                    ( 	(       $("#txtArquivoCSU").val()        == ""  &&
						        $("#txtArquivoCSUArquivo").val() != ""  
						)   ||
						(       $("#txtArquivoCSU").val()        != ""  &&
						        $("#txtArquivoCSUArquivo").val() == ""   
						)																							
					)  
                    {
                        popup
						({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Arquivo CSU - Preencher o Diretorio e o Nome do Arquivo",							
                            callbackOK: function() { }
						});					
				}	else if // informou o DIRETORIO ou ARQUIVO csu
                    ( 	(       $("#txtArquivoSitef").val()        == ""  &&
						        $("#txtArquivoSitefArquivo").val() != ""  
						)   ||
						(       $("#txtArquivoSitef").val()        != ""  &&
						        $("#txtArquivoSitefArquivo").val() == ""
						)																							
					)  
                    {
                        popup
						({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Arquivo SITEF - Preencher o Diretorio e o Nome do Arquivo",
                            callbackOK: function() { }
						});											
					
				}	else { 
//==================================================================================================================================
// Pede a execucao do processo do atacadao
//==================================================================================================================================
                    executarServico("ExecutarProcessoAtacadaoRequest", request, function(data) {
                        popup({
                            titulo: "Processo Atacadão",
                            icone: "sucesso",
                            mensagem: "Processo Atacadao " + data.CodigoProcesso + " iniciado.",
                            callbackOK: function() { }
                        });

                    });
                }
            });
            //SCF1213 - Marcos Matsuoka - fim

            executarServico(
                "ReceberConfiguracaoGeralRequest", {},
                function(data) {
                    $("#txtArquivoCSU").val(data.ConfiguracaoGeralInfo.DiretorioImportacaoAtacadaoMA);
                    $("#txtArquivoCSUArquivo").val("CSU.TXT");
                    $("#txtArquivoTSYS").val(data.ConfiguracaoGeralInfo.DiretorioImportacaoAtacadaoTSYS);
                    $("#txtArquivoTSYSArquivo").val("TSYS.TXT");
                    $("#txtArquivoSitef").val(data.ConfiguracaoGeralInfo.DiretorioImportacaoAtacadaoSITEF);
                    $("#txtArquivoSitefArquivo").val("SITEF.TXT");
                    $("#txtArquivoMatera").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoMateraFinanceiro);
                    $("#txtArquivoConciliacao").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoAtacadaoConciliacao);
                    $("#txtArquivoCSUSemAtacadao").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoAtacadaoCSUSemAtacadao);
                    $("#txtArquivoMateraContabil").val(data.ConfiguracaoGeralInfo.DiretorioExportacaoMateraContabil);

                });

            /*
            // Pede informações do config
            executarServico("ReceberProcessoAtacadaoAutomaticoConfigRequest", {}, function(data) {
            var ArquivoCSU = data.Config.CaminhoArquivoCSU;


                $("#txtArquivoCSU").val(separarDiretorio(data.Config.CaminhoArquivoCSU).diretorio);
            $("#txtArquivoCSUArquivo").val(separarDiretorio(data.Config.CaminhoArquivoCSU).nomeArquivo);
            $("#txtArquivoTSYS").val(separarDiretorio(data.Config.CaminhoArquivoTSYS).diretorio);
            $("#txtArquivoTSYSArquivo").val(separarDiretorio(data.Config.CaminhoArquivoTSYS).nomeArquivo);
            $("#txtArquivoSitef").val(separarDiretorio(data.Config.CaminhoArquivoSitef).diretorio);
            $("#txtArquivoSitefArquivo").val(separarDiretorio(data.Config.CaminhoArquivoSitef).nomeArquivo);
            $("#txtArquivoMatera").val(data.Config.CaminhoArquivoMateraFinanceiro);
            $("#txtArquivoConciliacao").val(data.Config.CaminhoArquivoResultadoConciliacao);
            $("#txtArquivoCSUSemAtacadao").val(data.Config.CaminhoArquivoCSUSemAtacadao);
            $("#txtArquivoMateraContabil").val(data.Config.CaminhoArquivoMateraContabil);
                
            });
            */
        }

    };

    // Dispara a carga
    $.processo.atacadao.load();

    // Separa o Caminho dos Arquivo em duas partes (Diretorio e Nome do Arquivo)
    function separarDiretorio(Arquivo) {
        var IndexUltimaBarra;
        for (var i = Arquivo.length; i >= 0; i--) {
            if (Arquivo.charAt(i) == "\\") {
                IndexUltimaBarra = i;
                break;
            }
        }
        var CaminhoArquivo = {
            diretorio: Arquivo.substring(0, IndexUltimaBarra + 1),
            nomeArquivo: Arquivo.substring(IndexUltimaBarra + 1)
        }
        return CaminhoArquivo;
    }

    // Validar Campos de Diretorio (Valida string vazia e verifica "\" no final da string)
    function validarCamposDiretorio(campo) {

        var resultado = campo;
        if (resultado != "") {
            var contador = campo.length;
            while (campo.substring(contador - 1, contador) == "\\") {
                resultado = campo.substring(0, contador - 1);
                contador--;
            }
            resultado += "\\";
        } else {
            resultado = null;
        }
        return resultado;
    }
});