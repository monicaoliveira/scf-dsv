﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Web;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Data.OracleClient;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;





namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class comum_servico : PaginaBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override object OnExecutarAcao(string acao, object parametros)
        {
            MensagemResponseBase retorno = null;

            // Recebe parametros
            string paramRequestTag = Request.Form["RequestTag"] != null ? Request.Form["RequestTag"] : Request["RequestTag"];
            string paramTipoMensagem = Request.Form["TipoMensagem"] != null ? Request.Form["TipoMensagem"] : Request["TipoMensagem"];

            switch (acao)
            {
                case "executar":

                    // Bloco de controle
                    try
                    {
                        // Verifica se tem a sessao
                        if (Session["CodigoSessao"] != null || paramTipoMensagem.Contains("Auxilio"))
                        {
                            // Faz a chamada para o serviço de mensageria. Ele irá repassar para o serviço 
                            // que irá tratar esta mensagem
                            ((MensagemRequestBase)parametros).RepassarExcessao = false;
                            retorno = Mensageria.Processar((MensagemRequestBase)parametros);
                            retorno.ResponseTag = paramRequestTag;
                        }
                        else
                        {
                            // Cria mensagem generica informando o erro
                            retorno =
                                new MensagemErroResponse()
                                {
                                    StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                                    DescricaoResposta = "Sessão Inválida"
                                };
                        }
                    }
                    catch (Exception ex)
                    {
                        // Cria mensagem generica informando o erro
                        retorno =
                            new MensagemErroResponse()
                            {
                                StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                                DescricaoResposta = ex.ToString()
                            };
                    }

                    break;
            }

            // Retorna
            return retorno;
        }
    }
}
