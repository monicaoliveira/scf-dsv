﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="site_relatorio_vencimento.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_relatorio_vencimento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="site_relatorio_vencimento.js"></script>
    <link href="site_relatorio_vencimento.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    
    <div id="site_relatorio_vencimento">
        <div id="filtros_relatorio_vencimento">
        
            <div id="filtroDetalhe">
                <label class="tituloFiltro">Detalhes</label>
                               
                <div class="borda">
                    
                    <ul>                                           
                        <li >
                            <input type="radio" id="rdoTipoContabil" name="tiporelatorio" class="forms[property[FiltroFinanceiroContabil]]" value="C" />&nbsp;Contábil
                        </li>
                        <li>        
                            <input type="radio" id="rdoTipoFinanceiro" name="tiporelatorio" class="forms[property[FiltroFinanceiroContabil]]" value="F" />&nbsp;Financeiro
                        </li>
                        <li></li>
                        <li class="label_retorarpago">
                            <input type="checkbox" id="chkRetornaTransacaoPaga" class="forms[property[FiltroFiltroRetornarTransacaoPaga]]" />Retornar Transações Pagas
                        </li>
                        <li class="label_consolidacao">
                            <label>Consolidar / Totalizar:</label>
                        </li>                          
                      
                    </ul>
                    <ul>                                   
                        <li class="label_detalhe1">
                            <label>Grupo:</label>
                        </li>
                        
                        <li class="campo_detalhe1">
                            <select id="cmbEmpresaGrupo" class="forms[property[FiltroEmpresaGrupo]]">
                                <option value="?">Todos</option>
                                <asp:Repeater runat="server" ID="rptCmbEmpresaGrupo">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoEmpresaGrupo") %>"><%# Eval("NomeEmpresaGrupo")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </li>

                        <li class="label_detalhe2">
                            <label>SubGrupo:</label>
                        </li>
                        
                        <li class="campo_detalhe2">
                            <select id="cmbEmpresaSubGrupo" class="forms[property[FiltroEmpresaSubGrupo]]">
                                <option value="?">Todos</option>
                                <asp:Repeater runat="server" ID="Repeater1">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoEmpresaSubGrupo") %>"><%# Eval("NomeEmpresaSubGrupo")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </li>

                        <li class="label_detalhe3">
                            <label>Produto:</label>
                        </li>
                        
                        <li class="">
                            <select id="cmbProduto" multiple="multiple" style="width:100px;" class="ui-multiselect ui-state-focus ui-state-active"></select>
                        </li>

                        <li class="chk_consolidacao">
                            <input type="checkbox" id="consolidacao" name="chkConsolidacaoAgendamentoVencimento" />&nbsp;Dt Agendamento + Dt. Vencimento
                        </li>

                    </ul>
                    <ul>
                        <li class="label_detalhe_favorecido">
                            <label>Favorecido:</label>
                        </li>
                        
                        <li class="campo_detalhe_favorecido">
                            <select id="cmbFavorecido" class="forms[property[FiltroFavorecido]]">
                                <option value="?">Todos</option>
                                <asp:Repeater runat="server" ID="rptCmbFavorecido">
                                    <ItemTemplate>
                                        <option value="<%# Eval("CodigoFavorecido") %>"><%# Eval("NomeFavorecido")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </li> 
                        
                        

                        <li class="label_detalhe3">
                            <label>Tipo de registro:</label>
                        </li>
                        
                        <li class="campo_detalhe3">
                            <select id="cmbTipoRegistro" class="forms[property[FiltroTipoRegistro]]">
                                <option value="CV-AV">CV-AV</option>
                                <option value="CV">CV</option>
                                <option value="AV">AV</option>
                                <option value="AJ">AJ</option> <%--SCF1158--%>
                            </select>
                        </li>
                        <li class="chk_consolidacao1">
                            <input type="checkbox" id="consolidacao1" name="chkConsolidacaoMaiorAgendamento" />&nbsp;Maior Dt. Agendamento
                        </li> 
                        </ul> 
                    <ul >                        
                        <li class="label_detalhe_status_cessao">
                            <label>Status Cessão:</label>
                        </li>
                        
                        <li class="campo_sessao">
                            <select id="cmbStatusCessao" class="forms[property[FiltroStatusCessao]]">
                                <option value="?">Todas</option>
                                <option value="S">Cessionadas</option>
                                <option value="N">Não cessionadas</option>
                            </select>
                        </li>

                        <li class="label_detalhe4">
                            <label>Referência:</label>
                        </li>
                        
                        <li >
                            <select id="cmbReferencia" multiple="multiple" style="width:100px;" class="ui-multiselect ui-state-focus ui-state-active"></select>
                        </li>                      
                        
                        <li class="label_detalhe3">
                            <label>Data Posição em:</label>
                        </li>
                        
                        <li class="campo_detalhe3">
                            <input type="text" id="txtDataPosicao" class="forms[property[FiltroDataPosicao];dataType[date];maskType[date]]" />
                        </li>                        
 
                        <%--checkbox consolida por referencia--%>
                        <li class="chk_consolidacao2" >
                            <input type="checkbox"  id="consolidacao2" name="chkReferencia" />&nbsp;Referência
                            <%--<input type="checkbox" style="display:none"  id="Checkbox1" name="chkReferencia" />&nbsp;--%>
                        </li>  
 
                    </ul>
                </div>
            </div>
            
            <div id="filtroPeriodo">
                <label class="tituloFiltro">Períodos</label>
                
                <div class="borda">
                    <ul>
                        <li class="label_Data">
                            <label>Data da Transação:</label>
                        </li>
                        
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>De:</label>
                                </li>
                                
                                <li class="campo_data">
                                    <input type="text" id="txtPeriodoTransacaoDe" class="forms[property[FiltroDataInicioTransacao];dataType[date];maskType[date]]" />
                                </li>
                                
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>
                                
                                <li class="campo_data">
                                    <input type="text" id="txtPeriodoTransacaoAte" class="forms[property[FiltroDataFimTransacao];dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>
                        
                        <li class="label_Data2">
                            <label>Data de Processamento:</label>
                        </li>
                        
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>De:</label>
                                </li>
                                <li class="campo_data">
                                    <input type="text" id="txtDataProcessamentoDe" class="forms[property[FiltroDataMovimentoDe];dataType[date];maskType[date]]" />
                                </li>
                                
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>                            
                                <li class="campo_data">
                                    <input type="text" id="txtDataProcessamentoAte" class="forms[property[FiltroDataMovimentoAte];dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>
                        
                    </ul>
                    
                    <ul>
                        <li class="label_Data">
                            <label>Data do Vencimento:</label>
                        </li>
                        
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>De:</label>
                                </li>
                                
                                <li class="campo_data">
                                    <input type="text" id="txtPeriodoVencimentoDe" class="forms[property[FiltroDataInicioVencimento];dataType[date];maskType[date]]" />
                                </li>
                                
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>
                                
                                <li class="campo_data">
                                    <input type="text" id="txtPeriodoVencimentoAte" class="forms[property[FiltroDataFimVencimento];dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>
                        
                        <li class="label_Data2 label_Data3">
                            <label>Data de Agendamento:</label>
                        </li>
                        
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>De:</label>
                                </li>
                                <li class="campo_data">
                                    <input type="text" id="txtDataAgendamentoDe" class="forms[property[FiltroDataAgendamentoDe];dataType[date];maskType[date]]" />
                                </li>                                
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>                                                            
                                <li class="campo_data">
                                    <input type="text" id="txtDataAgendamentoAte" class="forms[property[FiltroDataAgendamentoAte];dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>
                    </ul>        
                    <ul>
                                            
                        <%--SCF1170--%>
                        <li class="label_Data">
                            <label>Data de Retorno CCI:</label>
                        </li> 
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>De:</label>
                                </li>
                                <li class="campo_data">
                                    <input type="text" id="txtDataRetornoCCIDe" class="forms[property[FiltroDataInicioRetornoCCI];dataType[date];maskType[date]]" />
                                </li> 
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>                                
                                <li class="campo_data">
                                    <input type="text" id="txtDataRetornoCCIFim" class="forms[property[FiltroDataFimRetornoCCI];dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>
                    </ul>                                                                          
                </div>
            </div>            
            
            <div class="teste">
                <div style="float:left; text-align:left">
                    <button id="btnVisualizarHtml">Visualizar</button>
                </div>
                <div style=" text-align:right">
                    <%--SCF-1137
                    <button id="btnRelatorioFlat">Relatório de Teste</button>                --%>   
                    <button id="btnGerarPagamento">Gerar Pagamentos</button>                 
                    <button id="btnGerarRelatorioExcel">Gerar Excel</button>
                    <button id="btnGerarRelatorioPdf">Gerar PDF</button>
                </div>                                
            </div>
        </div>
        
        <div id="resultado_relatorio"></div>
    </div>
</asp:Content>