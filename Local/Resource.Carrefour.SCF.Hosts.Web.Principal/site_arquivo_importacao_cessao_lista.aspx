﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_arquivo_importacao_cessao_lista.js"></script>
    <link href="site_arquivo_importacao_cessao_lista.css" rel="stylesheet" type="text/css" />
    <title>SCF - Histórico de Importação de Arquivos de Cessão</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ----------------------------------------- -->
    <!--  Lista Arquivos de Importacao de Extrato  -->
    <!-- ----------------------------------------- -->
    <div id="site_arquivo_importacao_extrato_lista" style="margin: 30px;">
        
        <!-- Filtro -->
        <div class="filtro">
            <table>
                <tr>
                    <td>
                        <label>Data Inicial:</label>
                    </td><td>
                        <input type="text" />
                    </td><td>&nbsp;</td><td>
                        <label>Data Final:</label>
                    </td><td>
                        <input type="text" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Tipo de Processo:</label>
                    </td><td>
                        <select>
                            <option>(selecione)</option>
                            <option>Importação TSYS</option>
                            <option>Importação TSYS - Atacadão</option>
                            <option>Importação CSU</option>
                            <option>Importação Sitef</option>
                            <option>Importação Cessão</option>
                            <option>Exportação CCI</option>
                            <option>Exportação Pagamento Matera</option>
                            <option>Conciliar Transações CSU x Sitef</option>
                        </select>
                    </td><td>&nbsp;</td><td>
                        <label>Status:</label>
                    </td><td>
                        <select>
                            <option>(selecione)</option>
                            <option>Importado</option>
                            <option>Cancelado</option>
                            <option>Inválido</option>
                        </select>
                    </td><td style="padding-left: 20px;">
                        <button class="cmdFiltrar">Filtrar</button>
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Lista -->
        <div class="lista">
            <div style="padding: 10px;"></div>
            <table id="tbArquivoImportacao"></table>
        </div>
    </div>

</asp:Content>
