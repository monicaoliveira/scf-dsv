﻿$(document).ready(function() 
{   if (!$.arquivo)
        $.arquivo = {};

    $.arquivo.detalhe = 
    {   variaveis: {}
    //=================================================================================================        
    ,   abrir: function(codigoArquivo, codigoProcesso, callback) 
        {   $.arquivo.detalhe.variaveis.CodigoProcesso = codigoProcesso;
            $(".cmdAlterarEmMassa").show(); 
            $(".cmdLinhas").show();
            //=================================================================================================
            $.arquivo.detalhe.load(function() 
            {   $("#site_arquivo_detalhe").Forms("clearData");
                //=================================================================================================
                $("#site_arquivo_detalhe").dialog(
                {   autoOpen: false
                ,   height: 500
                ,   width: 730
                ,   modal: true
                ,   title: "Detalhe de Arquivo"
                ,   resize: $.arquivo.detalhe.resizeDialog
                ,   buttons: 
                    {   "Fechar": function() 
                        {   $("#site_processo_detalhe").attr("disabled", false);
                            $("#site_processo_detalhe .cmdDetalheArquivoProcesso").attr("disabled", false).removeClass("ui-state-disabled");
                            $(".ui-dialog-buttonpane button:contains('Fechar')").attr("disabled", false).removeClass("ui-state-disabled");
                            $("#site_arquivo_detalhe").dialog("close");
                        }
                    }
                });
                //=================================================================================================
                executarServico
                (   "ReceberArquivoRequest"
                ,   {   CodigoArquivo: codigoArquivo
                    ,   RetornarDetalhe: true 
                    }
                ,   function(data) 
                    {   $("#site_arquivo_detalhe .resumoArquivo").Forms("setData", { dataObject: data.ArquivoInfo });
                        var tipoArquivo = $("#txtTipoArquivo").val();
                        var CodigoProcesso = $("#txtCodigoProcesso").val();
                        $.arquivo.detalhe.variaveis.CodigoProcesso = CodigoProcesso;
                        if (tipoArquivo == "ArquivoRetornoCessao") 
                        {
                            $(".cmdAlterarEmMassa").hide();
                            $(".cmdLinhas").hide();
                        };
                    }
                );
                //=================================================================================================
                $.arquivo.detalhe.carregarArvore(codigoArquivo, function() 
                {   $("#site_arquivo_detalhe .tvwCriticas").treeview();
                    $("#site_arquivo_detalhe").dialog("open");
                    $.arquivo.detalhe.resizeDialog();
                    if (callback)
                        callback();
                });
            });
        }
    //=================================================================================================        
    ,   resizeDialog: function() 
        {
            $("#site_arquivo_detalhe div.abas").width($("#site_arquivo_detalhe").width() - $("#site_arquivo_detalhe div.treeview").width() - 30);
            $("#site_arquivo_detalhe div.abas").height($("#site_arquivo_detalhe").height() - 200);
            $("#site_arquivo_detalhe div.treeview").height($("#site_arquivo_detalhe").height() - 200);
        }
    //=================================================================================================        
    ,   load: function(callback) 
        {   if ($("#site_arquivo_detalhe").length == 0) 
            {   $("#areaTemporaria").load("site_arquivo_detalhe.aspx #site_arquivo_detalhe", function() 
                {   $("#site_arquivo_detalhe").appendTo("#areaComum");
                    $.arquivo.detalhe.load2(function() 
                    {   if (callback)
                            callback();
                    });
                });
            } else 
            {   if (callback)
                    callback();
            }
        }
    //=================================================================================================        
    ,   load2: function(callback) 
        {   $("#site_arquivo_detalhe button").button();
            $("#site_arquivo_detalhe .abas").tabs();
            $("#site_arquivo_detalhe .resumoArquivo input").attr("disabled", "disabled");
            //=================================================================================================
            $("#site_arquivo_detalhe_aba_resumo .cmdLinhas").click(function() 
            {   var linhaSelecionada = $("#site_arquivo_detalhe .tvwCriticas li.selected");
                if (linhaSelecionada.length > 0) 
                {   var filtro = {};
                    filtro.CodigoArquivo = linhaSelecionada.attr("codigoArquivo") != "" ? linhaSelecionada.attr("codigoArquivo") : null;
                    filtro.TipoLinha = linhaSelecionada.attr("tipoLinha") != "" ? linhaSelecionada.attr("tipoLinha") : null;
                    filtro.TipoCritica = linhaSelecionada.attr("tipoCritica") != "" ? linhaSelecionada.attr("tipoCritica") : null;
                    filtro.NomeCampo = linhaSelecionada.attr("nomeCampo") != "" ? linhaSelecionada.attr("nomeCampo") : null;
                    filtro.FiltrarComCritica = true;
                    var nivel = linhaSelecionada.attr("nivel") != "" ? linhaSelecionada.attr("nivel") : null;
                    if (nivel == "AgrupamentoPorArquivo" || nivel == "Total") 
                    {   $.arquivoItem.lista.limparLista();
                        return false;
                    }

                    $.arquivoItem.lista.abrir(filtro);
                }
                else 
                {   popup(
                    {   titulo: "EDITAR LINHA"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar um nível na árvore do Resumo de Críticas!"
                    ,   callbackOK: function() { }
                    });
                }
            });
            //=================================================================================================        
            $("#site_arquivo_detalhe_aba_resumo .cmdCriticas").click(function() 
            {   var linhaSelecionada = $("#site_arquivo_detalhe .tvwCriticas li.selected");

                if (linhaSelecionada.length > 0) 
                {   var filtro = {};
                    filtro.FiltroCodigoArquivo = linhaSelecionada.attr("codigoArquivo") != "" ? linhaSelecionada.attr("codigoArquivo") : null;
                    filtro.FiltroTipoLinha = linhaSelecionada.attr("tipoLinha") != "" ? linhaSelecionada.attr("tipoLinha") : null;
                    filtro.FiltroTipoCritica = linhaSelecionada.attr("tipoCritica") != "" ? linhaSelecionada.attr("tipoCritica") : null;
                    filtro.FiltroNomeCampo = linhaSelecionada.attr("nomeCampo") != "" ? linhaSelecionada.attr("nomeCampo") : null;
                    var nivel = linhaSelecionada.attr("nivel") != "" ? linhaSelecionada.attr("nivel") : null;

                    if (nivel == "Total") 
                    {   $.arquivoItem.lista.limparLista();
                        return false;
                    }

                    $.critica.lista.abrir(filtro);
                }
                else 
                {   popup(
                    {   titulo: "CRITICAS"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar um nível na árvore do Resumo de Críticas!",
                        callbackOK: function() { }
                    });
                }
            });
            //=================================================================================================        
            $("#site_arquivo_detalhe_aba_resumo .cmdBloquear").click(function() 
            {   var linhaSelecionada = $("#site_arquivo_detalhe .tvwCriticas li.selected");
                if (linhaSelecionada.length > 0) 
                {   var request = {};
                    request.FiltroCodigoArquivo = linhaSelecionada.attr("codigoArquivo") != "" ? linhaSelecionada.attr("codigoArquivo") : null;
                    request.FiltroTipoLinha = linhaSelecionada.attr("tipoLinha") != "" ? linhaSelecionada.attr("tipoLinha") : null;
                    request.FiltroTipoCritica = linhaSelecionada.attr("tipoCritica") != "" ? linhaSelecionada.attr("tipoCritica") : null;
                    request.FiltroNomeCampo = linhaSelecionada.attr("nomeCampo") != "" ? linhaSelecionada.attr("nomeCampo") : null;
                    request.StatusArquivoItem = "Bloqueado";
                    
                    executarServico("AlterarStatusArquivoItemRequest", request, function(data) 
                    {   popup(
                        {   titulo: "BLOQUEAR"
                        ,   icone: "sucesso"
                        ,   mensagem: "Bloqueio efetuado com sucesso. " + data.QuantidadeRegistrosAlterados.toString() + " registros alterados."
                        ,   callbackOK: function() { }
                        });

                    });
                }
                else 
                {   popup(
                    {   titulo: "BLOQUEAR"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar um nível na árvore do Resumo de Críticas!"
                    ,   callbackOK: function() { }
                    });
                }
            });
            //=================================================================================================        
            $("#site_arquivo_detalhe_aba_resumo .cmdDesbloquear").click(function() 
            {   var linhaSelecionada = $("#site_arquivo_detalhe .tvwCriticas li.selected");

                if (linhaSelecionada.length > 0) 
                {   var request = {};
                    request.FiltroCodigoArquivo = linhaSelecionada.attr("codigoArquivo") != "" ? linhaSelecionada.attr("codigoArquivo") : null;
                    request.FiltroTipoLinha = linhaSelecionada.attr("tipoLinha") != "" ? linhaSelecionada.attr("tipoLinha") : null;
                    request.FiltroTipoCritica = linhaSelecionada.attr("tipoCritica") != "" ? linhaSelecionada.attr("tipoCritica") : null;
                    request.FiltroNomeCampo = linhaSelecionada.attr("nomeCampo") != "" ? linhaSelecionada.attr("nomeCampo") : null;
                    request.FiltroStatusArquivoItem = "Bloqueado";
                    request.StatusArquivoItem = "PendenteDesbloqueio";

                    executarServico("AlterarStatusArquivoItemRequest", request, function(data) 
                    {   popup(
                        {   titulo: "DESBLOQUEAR"
                        ,   icone: "sucesso"
                        ,   mensagem: "Desbloqueio efetuado com sucesso. " + data.QuantidadeRegistrosAlterados.toString() + " registros alterados."
                        ,   callbackOK: function() { }
                        });
                    });
                }
                else 
                {   popup(
                    {   titulo: "DESBLOQUEAR"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar um nível na árvore do Resumo de Críticas!"
                    ,   callbackOK: function() { }
                    });
                }
            });
            //=================================================================================================        
            $("#site_arquivo_detalhe_aba_resumo .cmdExcluir").click(function() 
            {   var linhaSelecionada = $("#site_arquivo_detalhe .tvwCriticas li.selected");
                if (linhaSelecionada.length > 0) 
                {   var request = {};
                    request.FiltroCodigoArquivo = linhaSelecionada.attr("codigoArquivo") != "" ? linhaSelecionada.attr("codigoArquivo") : null;
                    request.FiltroTipoLinha = linhaSelecionada.attr("tipoLinha") != "" ? linhaSelecionada.attr("tipoLinha") : null;
                    request.FiltroTipoCritica = linhaSelecionada.attr("tipoCritica") != "" ? linhaSelecionada.attr("tipoCritica") : null;
                    request.FiltroNomeCampo = linhaSelecionada.attr("nomeCampo") != "" ? linhaSelecionada.attr("nomeCampo") : null;
                    request.StatusArquivoItem = "Excluido";
                    
                    executarServico("AlterarStatusArquivoItemRequest", request, function(data) 
                    {   popup(
                        {   titulo: "EXCLUIR"
                        ,   icone: "sucesso"
                        ,   mensagem: "Exclusão efetuada com sucesso. " + data.QuantidadeRegistrosAlterados.toString() + " registros alterados."
                        ,   callbackOK: function() { }
                        });
                    });
                }
                else 
                {   popup(
                    {   titulo: "EXCLUIR"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar um nível na árvore do Resumo de Críticas!"
                    ,   callbackOK: function() { }
                    });
                }
            });
            //=================================================================================================        
            $("#site_arquivo_detalhe_aba_resumo .cmdAlterarEmMassa").click(function() 
            {   var linhaSelecionada = $("#site_arquivo_detalhe .tvwCriticas li.selected");
                if (linhaSelecionada.length > 0) 
                {   var parametros = {};
                    parametros.codigoArquivo = linhaSelecionada.attr("codigoArquivo") != "" ? linhaSelecionada.attr("codigoArquivo") : null;
                    parametros.tipoLinha = linhaSelecionada.attr("tipoLinha") != "" ? linhaSelecionada.attr("tipoLinha") : null;
                    parametros.tipoCritica = linhaSelecionada.attr("tipoCritica") != "" ? linhaSelecionada.attr("tipoCritica") : null;
                    parametros.nomeCampo = linhaSelecionada.attr("nomeCampo") != "" ? linhaSelecionada.attr("nomeCampo") : null;
                    parametros.codigoProcesso = $.arquivo.detalhe.variaveis.CodigoProcesso ? $.arquivo.detalhe.variaveis.CodigoProcesso : "";
                    
                    if (parametros.tipoCritica != 'CriticaSCFDominio' && parametros.tipoCritica != undefined) //1282
                    {   popup(
                        {   titulo: "ALTERAÇÃO EM MASSA"
                        ,   icone: "erro"
                        ,   mensagem: "Somente Criticas de Dominio podem ser alteradas na Atualização Massiva"
                        ,   callbackOK: function() { }
                        });
                    }
                    else
                        $.arquivo.alteracaoMassiva.abrir(parametros, null);
                }
                else 
                {   popup(
                    {   titulo: "ALTERAÇÃO EM MASSA"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar um nível na árvore do Resumo de Críticas!"
                    ,   callbackOK: function() { }
                    });
                }

            });
            if (callback)
                callback();
        }
    //=================================================================================================        
    ,   carregarArvore: function(codigoArquivo, callback) 
        {   executarServico
            (   "MontarCriticaAgrupamentoRequest"
            ,   { CodigoArquivo: codigoArquivo, RetornarArvore: true }
            ,   function(data) 
                {   var montaItens = function(itens) 
                    {   var html = "";
                        for (var i = 0; i < itens.length; i++) 
                        {   var item = itens[i];
                            html += "<li" + (item.NivelNumero < 3 ? " class='closed'" : "");
                            html += " nivel='" + item.Nivel + "'";
                            if (item.CodigoArquivo) html += " codigoArquivo='" + item.CodigoArquivo + "'";
                            if (item.TipoLinha) html += " tipoLinha='" + item.TipoLinha + "'";
                            if (item.TipoCritica) html += " tipoCritica='" + item.TipoCritica + "'";
                            if (item.NomeCampo) html += " nomeCampo='" + item.NomeCampo + "'";
                            html += ">";

                            html += "<span class='folder'>" + item.Descricao + "</span>";

                            if (item.Filhos.length > 0) 
                            {   html += "<ul>" + montaItens(item.Filhos) + "</ul>";
                            }
                        }
                        return html;
                    }

                    if (data.Resultado.length > 0)
                        $("#site_arquivo_detalhe .tvwCriticas").html(montaItens(data.Resultado[0].Filhos));
                    else 
                        $("#site_arquivo_detalhe .tvwCriticas").html("");

                    $("#site_arquivo_detalhe .tvwCriticas li").click(function(event) 
                    {   if (event.target != $("span:first", this)[0])
                            return;
                        $("#site_arquivo_detalhe .tvwCriticas li.selected").toggleClass("selected");
                        $(this).toggleClass("selected");
                        var request = {};
                        request.CodigoArquivo = $(this).attr("codigoArquivo") != "" ? $(this).attr("codigoArquivo") : null;
                        request.TipoLinha = $(this).attr("tipoLinha") != "" ? $(this).attr("tipoLinha") : null;
                        request.TipoCritica = $(this).attr("tipoCritica") != "" ? $(this).attr("tipoCritica") : null;
                        request.NomeCampo = $(this).attr("nomeCampo") != "" ? $(this).attr("nomeCampo") : null;
                        var nivel = $(this).attr("nivel") != "" ? $(this).attr("nivel") : null;

                        executarServico("ReceberCriticaAgrupamentoDetalheRequest", request, function(data) 
                        {
                            $("#site_arquivo_detalhe_aba_resumo").Forms("setData", { dataObject: data.CriticaAgrupamentoDetalheInfo });
                        });

                        return false;
                    });

                    if (callback)
                        callback();
                });
        }
    };
    //=================================================================================================        
    if (window.location.href.indexOf("site_arquivo_detalhe") > 0) 
    {   $.arquivo.detalhe.abrir(1);
    }
});