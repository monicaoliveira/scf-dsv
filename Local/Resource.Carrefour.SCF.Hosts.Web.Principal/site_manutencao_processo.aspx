<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {

    }
</script>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="site_manutencao_processo.js"></script>
    <script type="text/javascript" src="site_processo_detalhe.js"></script>
    <script type="text/javascript" src="site_log_lista.js"></script>
    <script type="text/javascript" src="site_log_detalhe.js"></script>
    <link href="site_processo_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_bloqueio_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_processo_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_bloqueio_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_log_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_log_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_arquivo_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <script src="site_bloqueio_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_item_lista.js" type="text/javascript"></script>
    <script src="site_critica_lista.js" type="text/javascript"></script>
    <script src="site_critica_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_alteracao_massiva.js" type="text/javascript"></script>
    <title>SCF - Processos</title>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    <div id="site_manutencao_processo" >
        <div class="filtro formulario" >
            <table>
                <tr>
                    <td>
                        <label>Dt.Inicial</label>
                    </td>
                    <td>
                        <input type="text" id="txtFiltroDataInicial" class="forms[property[FiltroDataInclusaoInicial];dataType[date];maskType[date]]"/>
                    </td>
                    <td>
                        <label>Dt.Final</label>
                    </td>
                    <td>
                        <input type="text" id="txtFiltroDataFinal" class="forms[property[FiltroDataInclusaoFinal];dataType[date];maskType[date]]"/>
                    </td>
                     <td>
                        <label>Tip.Processo</label>
                    </td>
                    <td>
                        <select class="tipoprocesso forms[property[FiltroTipoProcesso]]">
                            <option value="?">(selecione)</option>           
                            <option value="Processo">PROCESSO</option>
                            <option value="ProcessoExtratoInfo">PROCESSO - Extrato</option>
                            <option value="ProcessoCessaoInfo">PROCESSO - Retorno de Cessão</option>
                            <option value="ProcessoGerarPagamentoInfo">PROCESSO - Pagamento</option>
                            <option value="ProcessoAtacadaoInfo">PROCESSO - Atacadão</option>
                            <option value="ProcessoRelatorio">RELATORIO</option>
                            <option value="ProcessoRelatorioVencimentosExcelInfo">RELATORIO - Vencimento Excel</option>
                            <option value="ProcessoRelatorioVencimentosPDFInfo">RELATORIO - Vencimento PDF</option>
                            <option value="ProcessoRelatorioVendasExcelInfo">RELATORIO - Vendas Excel</option>
                            <option value="ProcessoRelatorioVendasPDFInfo">RELATORIO -  Vendas PDF</option>
                            <option value="ProcessoRelatorioTransacaoExcelInfo">RELATORIO - Transações Excel</option>
                            <option value="ProcessoRelatorioTransacaoPDFInfo">RELATORIO - Transações PDF</option>
                            <option value="ProcessoRelatorioTransacaoInconsistenteExcelInfo">RELATORIO - Inconsistentes Excel</option>
                            <option value="ProcessoRelatorioTransacaoInconsistentePDFInfo">RELATORIO - Inconsistentes PDF</option>
                            <option value="ProcessoRelatorioInconsistenteAgrupadoExcelInfo">RELATORIO - Inconsistentes Agrupados Excel</option>
							<option value="ProcessoRelatorioInterchangeExcelInfo">RELATORIO - Interchange Excel</option>
                            <option value="ProcessoRelatorioConsolidadoExcelInfo">RELATORIO - Consolidado Excel</option>
                            <option value="ProcessoRelatorioConsolidadoPDFInfo">RELATORIO - Consolidado PDF</option>
                            <option value="ProcessoRelatorioConciliacaoContabilExcelInfo">RELATORIO - Saldo Contábil Excel</option>
                            <option value="ProcessoRelatorioConciliacaoContabilPDFInfo">RELATORIO - Saldo Contábil PDF</option>
                        </select>
                    </td>
                    <td>
                        <label>Status</label>
                    </td>
                    
                    <td>
                        <select id="cmbFiltroStatusProcesso" class="forms[property[FiltroStatusProcesso]]">
                            <option value="?">(selecione)</option>
                            <option value="0">Em Andamento</option>
                            <option value="1">Finalizado</option>
                            <option value="9">Cancelado</option>
                            <option value="3">Cessionado</option> <!--1337-->
                            <option value="2">Estornado</option> <!--1337-->  
                        </select>
                    </td>
                    <td>
                        <label>Sts.Bloqueios</label>
                    </td>
                    <td>
                         <select id="Select1" class="forms[property[FiltroStatusBloqueio]]">
                            <option value="?">(selecione)</option>
                            <option value="1">Tem Bloqueios</option>
                            <option value="0">Sem Bloqueios</option>
                        </select>
                    </td>                    
                    <td>
                        <button class="cmdFiltrar">Filtrar</button>
                    </td>
                </tr>
                   <tr>
<%--  1337                  <td>
                        <label>Status Bloqueios</label>
                    </td>
                    <td>
                         <select id="cmbFiltroBloqueio" class="forms[property[FiltroStatusBloqueio]]">
                            <option value="?">(selecione)</option>
                            <option value="1">Tem Bloqueios</option>
                            <option value="0">Sem Bloqueios</option>
                        </select>
                    </td>--%>
                </tr>
            </table>
        </div>
        <div class="lista">
            <table id="tbProcesso"></table>
        </div>
        <div class="botoes"> 
            <table width="100%">
                <tr>
                    <td>
                        <button style="float:left"  class="cmdEstornarExtrato" id="cmdEstornarExtrato">Estorno Extrato</button> <%--SCF1213--%>         
                        <button style="float:left"  class="cmdEstornarCessao" id="cmdEstornarCessao">Estorno Cessao</button> <%--SCF1213--%> 
                    </td>
                    <td>
                        <button style="float:left" class="cmdContinuarProcesso" id="cmdContinuarProcesso">Continuar</button> 
                        <button style="float:left" class="cmdCancelar" id="cmdCancelar">Cancelar</button>
                        <!-- SCF1388 Mover o botao liberar
                        <button style="float:left"  class="cmdLiberarProcesso" id="cmdLiberarProcesso">Liberar</button> <%--SCF1333--%>
                        -->
                    </td>
                    <td>
                        <button style="float:right" class="cmdEditarProcesso" id="cmdEditarProcesso">Detalhar</button>
                        <button style="float:right" class="cmdLog" id="cmdLog">Visualizar Log</button>
                    </td>
                </tr>
                <!-- SCF1388 Mover o botao liberar-->
                <tr>
                    <td>
                        <button style="float:left"  class="cmdLiberarProcesso" id="cmdLiberarProcesso">Liberar</button> <%--SCF1333--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </asp:Content>
