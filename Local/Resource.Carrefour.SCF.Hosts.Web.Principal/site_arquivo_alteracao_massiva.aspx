﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/template_default.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_arquivo_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_arquivo_item_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_arquivo_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_item_lista.js" type="text/javascript"></script>
    <script src="site_critica_lista.js" type="text/javascript"></script>
    <script src="site_critica_detalhe.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="site_arquivo_alteracao_massiva">
    
        <%--  1282 INICIO --%>
        <div class="processo" id="painelProcesso">
            <table >
                <tr>
                    <td>
                        <label style=" font-size:large">Alteração Massiva somente de críticas de DOMINIO que podem ser alteradas</label>
                    </td>
                </tr>
            </table>                 
        </div>  
        <br>
        <%--  1282 FIM --%>
    
        <%--  1282 <div id="tbCriticasAgrupadas"></div> FIM --%>
        <div class="lista">        
            <table id="tb_criticas"></table>
        </div>            
    </div>
</asp:Content>