﻿//====================================================================================================================================
// site_manutencao_processo.cs
//====================================================================================================================================
$(document).ready(function() {
    if (!$.processo)
        $.processo = {};
    //====================================================================================================================================
    // 00 - Funcoes do namespace processo.lista (01 - criar, 02 - load, 03 - listar)
    //====================================================================================================================================
    $.processo.lista =
    // --------------------------------------------------------------------
    //  01 - criar: criar novo processo
    // --------------------------------------------------------------------
	{criar: function() {
	    // Mostra o dialog de novo processo
	    $("#site_manutencao_processo_novo").Forms("clearData");
	    $("#site_manutencao_processo_novo").dialog("open");
	}
	// --------------------------------------------------------------------
	//  02 - load: funcoes de inicializacao da tela
	// --------------------------------------------------------------------
	, load: function(callback) {
	    // --------------------------------------------------------------------
	    // Insere o subtítulo
	    // --------------------------------------------------------------------
	    $("#subtitulo").html("CONSULTA [Manutenção de Processos]");
	    // --------------------------------------------------------------------
	    // Layout dos botões
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo button").button();
	    // --------------------------------------------------------------------
	    // Inicializa forms
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo").Forms("initializeFields");
	    //1463 evitando de sobrepor o filtro data inicial -- antes estava no 1065
        var date = new Date();
        date.setDate(date.getDate() - 1);
        $('#txtFiltroDataInicial').datepicker("setDate", date);
	    //====================================================================================================================================
	    // BOTAO
	    //====================================================================================================================================
	    // --------------------------------------------------------------------
	    // Botão filtrar
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdFiltrar").click(function() {
	        $.processo.lista.listar();
	    });
	    // --------------------------------------------------------------------
	    // 01 - Botão Estornar Extrato - SCF1213
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdEstornarExtrato").click(function() {
	        var CodigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
	        var Confirmar = "N";
	        // --------------------------------------------------------------------
	        // Se tem codigo processo, conferir status
	        // --------------------------------------------------------------------
	        if (CodigoProcesso) {
	            // --------------------------------------------------------------------
	            var StatusProcesso = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'StatusProcesso');
	            var TipoProcesso = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'TipoProcesso');
	            var ProcessoEstornarext = "ProcessoExtratoInfo";
	            var ProcessoEstornarata = "ProcessoAtacadaoInfo"; //1410
	            Confirmar = "S";
	            // --------------------------------------------------------------------
	            if (TipoProcesso != ProcessoEstornarext && TipoProcesso != ProcessoEstornarata) 
	            {
                    Confirmar = "N";
                    popup({
                        titulo: "BOTÃO ESTORNAR EXTRATO"
					        , icone: "erro"
					        , mensagem: "ESTORNAR EXTRATO: " + CodigoProcesso + ". Não é EXTRATO."
					        , callbackOK: function() { }
                    });
	            }
	            else if (StatusProcesso == "Cessionado") {
	                Confirmar = "N";
	                popup({
	                    titulo: "BOTÃO ESTORNAR EXTRATO"
							, icone: "erro"
							, mensagem: "ESTORNAR EXTRATO: " + CodigoProcesso + ". Não é possivel pois já foi cessionado."
							, callbackOK: function() { }
	                });
	            }
	            else if (StatusProcesso != "Finalizado") {
	                Confirmar = "N";
	                popup({
	                    titulo: "BOTÃO ESTORNAR EXTRATO"
							, icone: "erro"
							, mensagem: "ESTORNAR EXTRATO: " + CodigoProcesso + ". Não é possivel pois não está finalizado."
							, callbackOK: function() { }
	                });
	            }
	            // --------------------------------------------------------------------
	            // pede confirmação
	            // --------------------------------------------------------------------
	            if (Confirmar == "S") {
	                popup({
	                    titulo: "BOTÃO ESTORNAR EXTRATO"
							, icone: "atencao"
							, mensagem: "ESTORNAR EXTRATO: " + CodigoProcesso + ". CONFIRMA?"
							, callbackSim: function() {
							    executarServico("EstornarLoteExtratoRequest", { CodigoProcesso: CodigoProcesso },
								function(data) {
								    popup({
								        titulo: "BOTÃO ESTONRAR EXTRATO"
										, icone: "sucesso"
										, mensagem: "ESTONRAR EXTRATO: " + CodigoProcesso + " EFETUADO." + data.DescricaoResposta
										, callbackOK: function() { callback(); }
								    });
								});
							},
	                    callbackNao: function() { }
	                });
	            }
	        }
	        // --------------------------------------------------------------------
	        // Se NAO tem codigo processo, pede para selecionar
	        // --------------------------------------------------------------------
	        else {
	            popup({
	                titulo: "BOTÃO ESTORNAR EXTRATO"
						, icone: "erro"
						, mensagem: "ESTONRAR EXTRATO: Favor selecionar uma linha!"
						, callbackOK: function() { }
	            });
	        }
	    });
	    // --------------------------------------------------------------------
	    // 02 - Botão Estornar Cessao - SCF1213
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdEstornarCessao").click(function() {
	        var CodigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
	        var Confirmar = "N";
	        // --------------------------------------------------------------------
	        // Se tem codigo processo, conferir status
	        // --------------------------------------------------------------------
	        if (CodigoProcesso) {
	            // --------------------------------------------------------------------
	            var StatusProcesso = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'StatusProcesso');
	            var TipoProcesso = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'TipoProcesso');
	            var ProcessoEstornar = "ProcessoCessaoInfo";
	            Confirmar = "S";
	            // --------------------------------------------------------------------
	            if (TipoProcesso != ProcessoEstornar) {
	                Confirmar = "N";
	                popup({
	                    titulo: "BOTÃO ESTORNAR CESSAO"
							, icone: "erro"
							, mensagem: "ESTORNAR CESSAO: " + CodigoProcesso + ". Não é CESSAO."
							, callbackOK: function() { }
	                });
	            }
	            else if (StatusProcesso != "Finalizado") {
	                Confirmar = "N";
	                popup({
	                    titulo: "BOTÃO ESTORNAR CESSAO"
							, icone: "erro"
							, mensagem: "ESTORNAR CESSAO: " + CodigoProcesso + ". Não é possivel pois não está finalizado."
							, callbackOK: function() { }
	                });
	            }
	            // --------------------------------------------------------------------
	            // pede confirmação
	            // --------------------------------------------------------------------
	            if (Confirmar == "S") {
	                popup({
	                    titulo: "BOTÃO ESTORNAR CESSAO"
							, icone: "atencao"
							, mensagem: "ESTORNAR CESSAO: " + CodigoProcesso + ". CONFIRMA?"
							, callbackSim: function() {
							    executarServico("EstornarLoteExtratoRequest", { CodigoProcesso: CodigoProcesso },
								function(data) {
								    popup({
								        titulo: "BOTÃO ESTONRAR CESSAO"
										, icone: "sucesso"
										, mensagem: "ESTORNAR CESSAO: " + CodigoProcesso + " EFETUADO." + data.DescricaoResposta
										, callbackOK: function() { callback(); }
								    });
								});
							},
	                    callbackNao: function() { }
	                });
	            }
	        }
	        // --------------------------------------------------------------------
	        // Se NAO tem codigo processo, pede para selecionar
	        // --------------------------------------------------------------------
	        else {
	            popup
				    ({ titulo: "BOTÃO ESTORNAR CESSAO"
						, icone: "erro"
						, mensagem: "ESTONRAR CESSAO: Favor selecionar uma linha!"
						, callbackOK: function() { }
				    });
	        }
	    });
	    // --------------------------------------------------------------------
	    // 03 - Botão detalhar
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdEditarProcesso").click(function() {
	        var CodigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
	        if (CodigoProcesso) {
	            CodigoProcesso = CodigoProcesso.substring(CodigoProcesso.lastIndexOf("_") + 1);
	            $.processo.detalhe.abrir(CodigoProcesso);
	        }
	        else {
	            popup({
	                titulo: "BOTÃO DETALHAR"
                        , icone: "erro"
                        , mensagem: "DETALHAR: Favor selecionar uma linha'"
                        , callbackOK: function() {
                        }
	            });
	        }
	    });

	    // --------------------------------------------------------------------
	    // 04 - Botão log
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdLog").click(function() {
	        var CodigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
	        // --------------------------------------------------------------------
	        // se tem processo selecionado abre o LOG
	        // --------------------------------------------------------------------
	        if (CodigoProcesso) {
	            $.log.lista.abrir({ FiltroTipoOrigem: "ProcessoInfo", FiltroCodigoOrigem: CodigoProcesso });
	        }
	        // --------------------------------------------------------------------
	        // se nao tem processo selecionado, pede para selecionar
	        // --------------------------------------------------------------------
	        else {
	            popup({
	                titulo: "BOTÃO LOG"
                        , icone: "erro"
                        , mensagem: "LOG: Favor selecionar uma linha"
                        , callbackOK: function() { }
	            });
	        }
	    });
	    // --------------------------------------------------------------------
	    // 05 -Botão Cancelar
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdCancelar").click(function() {
	        var CodigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
	        if (CodigoProcesso) {
	            // --------------------------------------------------------------------
	            var StatusProcesso = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'StatusProcesso');
	            Confirmar = "S";
	            // --------------------------------------------------------------------
	            if (StatusProcesso != "EmAndamento") {
	                Confirmar = "N";
	                popup({
	                    titulo: "BOTÃO CANCELAR"
							, icone: "erro"
							, mensagem: "CANCELAR: " + CodigoProcesso + ". Não está em andamento."
							, callbackOK: function() { }
	                });
	            }
	            // --------------------------------------------------------------------
	            // pede confirmação
	            // --------------------------------------------------------------------
	            if (Confirmar == "S") {
	                popup({
	                    titulo: "BOTÃO CANCELAR"
						    , icone: "atencao"
						    , mensagem: "CANCELAR PROCESSO: " + CodigoProcesso + ". Confirma?"
						    , callbackSim: function() {
						        executarServico("CancelarProcessoRequest", { CodigoProcesso: CodigoProcesso }, function(data) {
						            popup({
						                titulo: "Processo Cancelado"
                                            , icone: "sucesso"
                                            , mensagem: "PROCESSO " + CodigoProcesso + " CANCELADO."
                                            , callbackOK: function() { callback(); }
						            });
						        });
						    }
						    , callbackNao: function() { }
	                });
	            }
	        }
	        // --------------------------------------------------------------------
	        // se nao tem processo selecionado, pede para selecionar
	        // --------------------------------------------------------------------
	        else {
	            popup({
	                titulo: "BOTÃO CANCELAR"
                        , icone: "erro"
						, mensagem: "CANCELAR PROCESSO: Favor selecionar uma linha!"
						, callbackOK: function() { }
	            });
	        }
	    });
	    // --------------------------------------------------------------------
	    // 06 - Botão continuar processo
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdContinuarProcesso").click(function() {
	        var CodigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
	        var Confirmar = "N";
	        // --------------------------------------------------------------------
	        // Se tem codigo processo, conferir status
	        // --------------------------------------------------------------------
	        if (CodigoProcesso) {
	            // --------------------------------------------------------------------
	            var StatusProcesso = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'StatusProcesso');
	            var StatusEstagio = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'StatusEstagio');
	            Confirmar = "S";
	            // --------------------------------------------------------------------
	            if (StatusProcesso == "Finalizado") {
	                Confirmar = "N";
	                popup({
	                    titulo: "BOTÃO CONTINUAR"
							, icone: "erro"
							, mensagem: "CONTINUAR: " + CodigoProcesso + ". Não é possivel pois já esta finalizado."
							, callbackOK: function() { }
	                });
	            }
	            //1337 INICIO
	            else if (StatusProcesso == "Cessionado") {
	                Confirmar = "N";
	                popup({
	                    titulo: "BOTÃO CONTINUAR"
							, icone: "erro"
							, mensagem: "CONTINUAR: " + CodigoProcesso + ". Não é possivel pois já foi cessionado."
							, callbackOK: function() { }
	                });
	            }
	            //1337 FIM                                                                          
	            else if (StatusEstagio != "AguardandoValidacao" && StatusEstagio != "Erro") {
	                Confirmar = "N";
	                popup({
	                    titulo: "BOTÃO CONTINUAR"
							, icone: "erro"
							, mensagem: "CONTINUAR: " + CodigoProcesso + ". Não é possivel pois está em andamento."
							, callbackOK: function() { }
	                });
	            }
	            // --------------------------------------------------------------------
	            // pede confirmação
	            // --------------------------------------------------------------------
	            if (Confirmar == "S") {
	                popup({
	                    titulo: "BOTÃO CONTINUAR"
							, icone: "atencao"
							, mensagem: "CONTINUAR: " + CodigoProcesso + ". CONFIRMA?"
							, callbackSim: function() {
							    executarServico("ExecutarProcessoRequest", { CodigoProcesso: CodigoProcesso, ExecutarAssincrono: true, ValidaProcessoEmAndamento: true, Descricao: "Processo Continuado" }, //o campo Descricao foi criado para atender o Jira  968 - Marcos Matsuoka
								function(data) {
								    popup({
								        titulo: "BOTÃO CONTINUAR"
										, icone: "sucesso"
										, mensagem: "CONTINUAR: " + CodigoProcesso + " EFETUADO."
										, callbackOK: function() { callback(); }
								    });
								});
							},
	                    callbackNao: function() { }
	                });
	            }
	        }
	        // --------------------------------------------------------------------
	        // Se NAO tem codigo processo, pede para selecionar
	        // --------------------------------------------------------------------
	        else {
	            popup({
	                titulo: "BOTÃO CONTINUAR"
						, icone: "erro"
						, mensagem: "CONTINUAR: Favor selecionar uma linha!"
						, callbackOK: function() { }
	            });
	        }
	    });
	    // --------------------------------------------------------------------
	    // 07 - Botão liberar processo
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdLiberarProcesso").click(function() {
	        var CodigoProcesso = $("#tbProcesso").jqGrid("getGridParam", "selrow");
	        // --------------------------------------------------------------------
	        // Se tem codigo processo, pede confirmação
	        // --------------------------------------------------------------------
	        if (CodigoProcesso)
	        // --------------------------------------------------------------------
	            var StatusProcesso = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'StatusProcesso');
	        var StatusEstagio = $("#tbProcesso").jqGrid('getCell', CodigoProcesso, 'StatusEstagio');
	        Confirmar = "S";
	        // --------------------------------------------------------------------
	        if (StatusProcesso != "EmAndamento") {
	            Confirmar = "N";
	            popup({
	                titulo: "BOTÃO CONTINUAR"
							, icone: "erro"
							, mensagem: "CONTINUAR: " + CodigoProcesso + ". Não é possivel liberar pois não está em andamento."
							, callbackOK: function() { }
	            });
	        }
	        // --------------------------------------------------------------------
	        if (Confirmar == "S") {
	            popup
					    ({
					        titulo: "BOTÃO LIBERAR"
						    , icone: "atencao"
						    , mensagem: "LIBERAR: " + CodigoProcesso + ". CONFIRMA?"
						    , callbackSim: function() {
						        executarServico("ExecutarProcessoRequest", { CodigoProcesso: CodigoProcesso, ExecutarAssincrono: true, ValidaProcessoEmAndamento: false, Descricao: "Processo Liberado" }, //o campo Descricao foi criado para atender o Jira  968 - Marcos Matsuoka
							    function(data) {
							        popup({
							            titulo: "Processo Liberado"
										    , icone: "sucesso"
										    , mensagem: "LIBERAR: " + CodigoProcesso + ". EFETUADO."
										    , callbackOK: function() { callback(); }
							        });
							    });
						    },
					        callbackNao: function() { }
					    });
	        }
	        // --------------------------------------------------------------------
	        // Se nao tem codigo processo, pede para selecionar
	        // --------------------------------------------------------------------
	        else {
	            popup
					({
					    titulo: "BOTÃO LIBERAR"
						, icone: "erro"
						, mensagem: "LIBERAR: Favor selecionar uma linha!"
						, callbackOK: function() { }
					});
	        }
	    });
	    // --------------------------------------------------------------------
	    // Botão novo
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo .cmdNovo").click(function() {
	        $.processo.lista.criar();
	    });
	    //====================================================================================================================================
	    // GRID
	    //====================================================================================================================================
	    // --------------------------------------------------------------------
	    // Inicializa Lista de Lotes
	    // --------------------------------------------------------------------
	    $("#tbProcesso").jqGrid
			({
			    datatype: "clientSide"
				, mtype: "GET"
				, colNames: ["Processo", "Tipo", "Início", "Sts.Processo", "Arquivo", "Estágio", "Sts.Estágio", "Bloqueio", "EnviadoMatera"]
				, colModel: [
                              { name: "CodigoProcesso", index: "CodigoProcesso", width: 100, sorttype: "integer", align: "center", key: true },
                              { name: "TipoProcesso", index: "TipoProcesso", width: 150, align: "left" },
                              { name: "DataInclusao", index: "DataInclusao", width: 150, align: "center" },
                              { name: "StatusProcesso", index: "StatusProcesso", width: 100, align: "left" },
                              { name: "NomeArquivo", index: "NomeArquivo", width: 300, align: "left", key: true },
                              { name: "TipoEstagioAtual", index: "TipoEstagioAtual", width: 200, align: "left" },
                              { name: "StatusEstagio", index: "StatusEstagio", width: 100, align: "left" },
                              { name: "StatusBloqueio", index: "StatusBloqueio", width: 100, align: "left" },
                              { name: "EnviadoMatera", index: "EnviadoMatera", hidden: true }
                            ]
				, sortorder: "asc"
				, shrinkToFit: false
				, viewrecords: true
				, ondblClickRow: function(rowid, status) 
				{
				    return false;
				}
                , onSelectRow: function(id) 
                {
                    var objProcesso = $("#tbProcesso").jqGrid('getRowData', id);
                    // --------------------------------------------------------------------
                    // habilita apenas o LOG
                    // --------------------------------------------------------------------
                    //=======================================================================================================
                    //1388
                    //=======================================================================================================
                    //6)	VISUALIZAR LOG (cmdLog) 
                    //INFORMAÇÃO DA FUNCIONALIDADE DO BOTÃO:
                    //Exibir uma lista de log do processamento efetuado informando data,hora e descrição do log.
                    //Janela exibida: Grid da LISTA DE LOG
                    //Tela chamada: $.log.lista.abrir
                    //Parametros:
                    //ProcessoInfo 
                    //Codigo do Processo 
                    //Ficará habilitado quando: Sempre
                    //=======================================================================================================
                    $("#site_manutencao_processo #cmdLog").attr("class", "cmdLog ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_manutencao_processo #cmdLog").removeAttr("disabled");
                    // --------------------------------------------------------------------
                    // desabilita OS OUTROS botões 
                    // --------------------------------------------------------------------
                    $("#site_manutencao_processo #cmdEditarProcesso").attr("class", "cmdEditarProcesso ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_manutencao_processo #cmdEditarProcesso").attr("disabled", "true");

                    $("#site_manutencao_processo #cmdCancelar").attr("class", "cmdCancelar ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_manutencao_processo #cmdCancelar").attr("disabled", "true");

                    $("#site_manutencao_processo #cmdContinuarProcesso").attr("class", "cmdContinuarProcesso ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_manutencao_processo #cmdContinuarProcesso").attr("disabled", "true");

                    $("#site_manutencao_processo #cmdLiberarProcesso").attr("class", "cmdLiberarProcesso ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_manutencao_processo #cmdLiberarProcesso").attr("disabled", "true");

                    $("#site_manutencao_processo #cmdEstornarExtrato").attr("class", "cmdEstornarExtrato ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_manutencao_processo #cmdEstornarExtrato").attr("disabled", "true");

                    $("#site_manutencao_processo #cmdEstornarCessao").attr("class", "cmdEstornarCessao ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_manutencao_processo #cmdEstornarCessao").attr("disabled", "true");
                    
                    // --------------------------------------------------------------------
                    // Libera alguns botoes dependendo da linha selecionada
                    // --------------------------------------------------------------------
                    //==================================================================================================
                    //1388
                    //==================================================================================================
                    //1)ESTORNO DE EXTRATO (cmdEstornarExtrato)
                    //INFORMAÇÃO DA FUNCIONALIDADE DO BOTÃO:
                    //Efetuar a limpeza do extrato no SCF deletando fisicamente dados de importação, logs e transações. Permance apenas dados do arquivo e do processo com o status ESTORNADO.
                    //Programa acionado: PR_ESTORNO_EXTRATO_S
                    //Serviço chamado: EstornarLoteExtratoRequest
                    //Ficará habilitado quando: 
                    //===>Quando a coluna “STS.PROCESSO” estiver Finalizado E quando a coluna “TIPO” for ProcessoAtacadaoInfo Ou ProcessoExtratoInfo.
                    //==================================================================================================
                    if (objProcesso.StatusProcesso == "Finalizado" && (objProcesso.TipoProcesso == "ProcessoAtacadaoInfo" || objProcesso.TipoProcesso == "ProcessoExtratoInfo"))
                    {
                        $("#site_manutencao_processo #cmdEstornarExtrato").attr("class", "cmdEstornarExtrato ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                        $("#site_manutencao_processo #cmdEstornarExtrato").removeAttr("disabled");
                    }
                    //==================================================================================================
                    //1388
                    //==================================================================================================
                    //2)	ESTORNO DE CESSÃO (cmdEstornarCessao)
                    //INFORMAÇÃO DA FUNCIONALIDADE DO BOTÃO:
                    //Efetuar a limpeza da cessão no SCF eliminando os dados de data de retorno de cessão, status de cessão e processo de cessão nas transações.
                    //Programa acionado: PR_ESTORNO_RETORNO_CESSAO_S
                    //Serviço chamado: EstornarLoteExtratoRequest
                    //Ficará habilitado quando: 
                    //===>Quando a coluna “STS.PROCESSO” estiver Finalizado.
                    //===>E quando a coluna “TIPO” for ProcessoCessaoInfo.
                    //==================================================================================================
                    if (objProcesso.StatusProcesso == "Finalizado" && objProcesso.TipoProcesso == "ProcessoCessaoInfo" ) 
                    {
                        $("#site_manutencao_processo #cmdEstornarCessao").attr("class", "cmdEstornarCessao ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                        $("#site_manutencao_processo #cmdEstornarCessao").removeAttr("disabled");
                    }
                    //=======================================================================================================
                    //1388
                    //=======================================================================================================
                    //3)	CONTINUAR (cmdContinuarProcesso)
                    //INFORMAÇÃO DA FUNCIONALIDADE DO BOTÃO:
                    //Efetuar a retomada dos processos que estiverem em aguardando validação.
                    //Programa acionado: PR_PROCESSO_L 
                    //Serviço chamado: ExecutarProcessoRequest (PROCESSODBLIB.CS)
                    //Parametros: 
                    //Codigo do Processo 
                    //Fase do processo:ValidaProcessoEmAndamento
                    //Ficará habilitado quando: 
                    //Quando a coluna “STS.PROCESSO” estiver EmAndamento 
                    //===>E quando a coluna “TIPO” for ProcessoAtacadaoInfo Ou ProcessoExtratoInfo Ou ProcessoCessaoInfo 
                    //===>E quando a coluna “STS.ESTAGIO” estiver AguardandoValidação.
                    //=======================================================================================================
                    if (objProcesso.StatusProcesso == "EmAndamento" && objProcesso.StatusEstagio == "AguardandoValidacao" && (objProcesso.TipoProcesso == "ProcessoAtacadaoInfo" || objProcesso.TipoProcesso == "ProcessoExtratoInfo" || objProcesso.TipoProcesso == "ProcessoCessaoInfo") )                             
                    {
                        $("#site_manutencao_processo #cmdContinuarProcesso").attr("class", "cmdContinuarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                        $("#site_manutencao_processo #cmdContinuarProcesso").removeAttr("disabled");
                    }                    
                    //=======================================================================================================
                    //1388
                    //=======================================================================================================
                    //4)	CANCELAR (cmdCancelar)
                    //INFORMAÇÃO DA FUNCIONALIDADE DO BOTÃO:
                    //Efetuar o cancelamento de um processo extrato que está em aguardando validação Ou com erro.
                    //Programa acionado: GerenciadorProcesso.CS
                    //Serviço chamado: CancelarProcessoRequest (executa assincrono)
                    //Parametros: 
                    //Codigo do Processo 
                    //Ficará habilitado quando: 
                    //==> Quando a coluna “STS.ESTÁGIO” estiver Erro.
                    //Ou quando a coluna “STS.PROCESSO” estiver EmAndamento 
                    //E quando a coluna “TIPO” for ProcessoAtacadaoInfo Ou ProcessoExtratoInfo Ou ProcessoCessaoInfo 
                    //E quando o “STS.ESTÁGIO” estiver AguardandoValidação.
                    //=======================================================================================================                       
                    if (objProcesso.StatusEstagio == "Erro") 
                    {
                        $("#site_manutencao_processo #cmdCancelar").attr("class", "cmdCancelar ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                        $("#site_manutencao_processo #cmdCancelar").removeAttr("disabled");
                    }                    
                    //=======================================================================================================
                    //1388
                    //=======================================================================================================
                    //4)	CANCELAR (cmdCancelar)
                    //INFORMAÇÃO DA FUNCIONALIDADE DO BOTÃO:
                    //Efetuar o cancelamento de um processo extrato que está em aguardando validação Ou com erro.
                    //Programa acionado: GerenciadorProcesso.CS
                    //Serviço chamado: CancelarProcessoRequest (executa assincrono)
                    //Parametros: 
                    //Codigo do Processo 
                    //Ficará habilitado quando: 
                    //Quando a coluna “STS.ESTÁGIO” estiver Erro.
                    //===>Ou quando a coluna “STS.PROCESSO” estiver EmAndamento 
                    //===>E quando a coluna “TIPO” for ProcessoAtacadaoInfo Ou ProcessoExtratoInfo Ou ProcessoCessaoInfo 
                    //===>E quando o “STS.ESTÁGIO” estiver AguardandoValidação.
                    //=======================================================================================================  
                    if (objProcesso.StatusProcesso == "EmAndamento" && objProcesso.StatusEstagio == "AguardandoValidacao" && (objProcesso.TipoProcesso == "ProcessoAtacadaoInfo" || objProcesso.TipoProcesso == "ProcessoExtratoInfo" || objProcesso.TipoProcesso == "ProcessoCessaoInfo")) 
                    {
                        $("#site_manutencao_processo #cmdCancelar").attr("class", "cmdCancelar ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                        $("#site_manutencao_processo #cmdCancelar").removeAttr("disabled");
                    }                    
                    //=======================================================================================================
                    //1388
                    //=======================================================================================================  
                    //5)	LIBERAR (cmdLiberarProcesso)
                    //INFORMAÇÃO DA FUNCIONALIDADE DO BOTÃO:
                    //Efetuar a liberação um processo extrato que está sem log de execução e a muito tempo aparentemente parado.
                    //Programa acionado: PR_PROCESSO_L 
                    //Serviço chamado: ExecutarProcessoRequest (PROCESSODBLIB.CS)
                    //Parametros: 
                    //Codigo do Processo 
                    //Fase do processo:ValidaProcessoEmAndamento
                    //Ficará habilitado quando: 
                    //===>Quando a coluna “STS.PROCESSO” estiver EmAndamento.
                    //===>E quando a coluna “TIPO” for ProcessoAtacadaoInfo Ou ProcessoExtratoInfo Ou ProcessoCessaoInfo.
                    //=======================================================================================================  
                    if (objProcesso.StatusProcesso == "EmAndamento" && (objProcesso.TipoProcesso == "ProcessoAtacadaoInfo" || objProcesso.TipoProcesso == "ProcessoExtratoInfo" || objProcesso.TipoProcesso == "ProcessoCessaoInfo")) 
                    {
                        $("#site_manutencao_processo #cmdLiberarProcesso").attr("class", "cmdLiberarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                        $("#site_manutencao_processo #cmdLiberarProcesso").removeAttr("disabled");
                    }                    
                    //=======================================================================================================
                    //1388
                    //=======================================================================================================
                    //7)	DETALHAR (cmdEditarProcesso)
                    //INFORMAÇÃO DA FUNCIONALIDADE DO BOTÃO:
                    //Exibir uma lista dos arquivos do processo, informando codigo do arquivo, nome do arquivo, tipo do arquivo, data da inclusao, codigo do processo e status do arquivo.
                    //Nesta tela tem a funcionalidade do botão DETALHE, que permitirá conferir linhas inconsistentes do arquivo selecionado e efetuar procedimentos de exclusão, bloqueio e desbloqueio destas linhas.
                    //Janela exibida: Grid da DETALHE DO PROCESSO
                    //Tela chamada: $.processo.detalhe.abrir
                    //Parametros:
                    //Codigo do Processo 
                    //Ficará habilitado quando: 
                    //Quando a coluna “TIPO” for ProcessoAtacadaoInfo Ou ProcessoExtratoInfo Ou ProcessoCessaoInfo.
                    //=======================================================================================================
                    if (objProcesso.TipoProcesso == "ProcessoAtacadaoInfo" || objProcesso.TipoProcesso == "ProcessoExtratoInfo" ||  objProcesso.TipoProcesso == "ProcessoCessaoInfo") 
                    {
                        $("#site_manutencao_processo #cmdEditarProcesso").attr("class", "cmdEditarProcesso ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                        $("#site_manutencao_processo #cmdEditarProcesso").removeAttr("disabled");
                    }                    
                }
			});
	    // --------------------------------------------------------------------
	    // Inicializa o dialog de novo processo
	    // --------------------------------------------------------------------
	    $("#site_manutencao_processo_novo").dialog
			({
			    autoOpen: false
				, height: 200
				, width: 300
				, modal: true
				, buttons:
					{ "Criar": function() {
					    $(this).dialog("close");
					    $.processo.detalhe.abrir();
					    return false;
					}
					, "Cancelar": function() {
					    $(this).dialog("close");
					    return false;
					}
					}
				, title: "Criar Novo Processo"
			});
	    // --------------------------------------------------------------------
	    // Trata o redimensionamento da tela
	    // --------------------------------------------------------------------
	    $(window).bind("resize", function() {
	        $("#tbProcesso").setGridWidth($(window).width() - margemEsquerda);
	        $("#tbProcesso").setGridHeight($(window).height() - margemRodape - 7); //SCF1269 - Marcos Matsuoka (- 7)
	    }).trigger("resize");
	    if (callback) {
	        callback();
	    }
	}

	// --------------------------------------------------------------------
	//  03 - listar: pede a lista de processos
	// --------------------------------------------------------------------
	, listar: function() {
	    // --------------------------------------------------------------------
	    // Limpa a lista
	    // --------------------------------------------------------------------
	    $("#tbProcesso").jqGrid("clearGridData");
	    // --------------------------------------------------------------------
	    // Monta o filtro
	    // --------------------------------------------------------------------
	    var request = {};
	    $("#site_manutencao_processo").Forms("getData", { dataObject: request });
	    // --------------------------------------------------------------------
	    // Pede a lista de processos
	    // --------------------------------------------------------------------
	    executarServico("ListarProcessoRequest", request,
			function(data) 
			{
			    for (var i = 0; i < data.Resultado.length; i++)
			        $("#tbProcesso").jqGrid
						("addRowData"
							, data.Resultado[i].CodigoProcesso
							, data.Resultado[i]
						);
			});
	}
};
    //====================================================================================================================================
    // 00 - fim
    //====================================================================================================================================
    function callback() {
        $.processo.lista.listar();
    }
    // --------------------------------------------------------------------
    // Dispara a carga
    // --------------------------------------------------------------------
    $.processo.lista.load(callback);
});
