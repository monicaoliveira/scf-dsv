﻿$(document).ready(function() {

    // Namespace de processo
    if (!$.arquivo) $.arquivo = {};
    $.arquivo.exportacaoExtrato = {};
    
    // Funcoes do namespace processo.lista
    $.arquivo.exportacaoExtrato.lista = {
        
        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {
        
            // Insere o subtítulo
            $("#subtitulo").html("Histórico de Exportação de Arquivos de Extrato");
            
            // Layout dos botões
            $("#site_arquivo_exportacao_extrato_lista button").button();
        
            // Inicializa Lista 
            $("#tbArquivoExportado").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                colNames: ["Código do Lote", "Data Exportação", "Nome Arquivo", "Tamanho", "Tempo Exportação", "Processo", "Status"],
                colModel: [
                    { name: "CodigoLote", index: 0, width: 55, align: "center" },
                    { name: "DataExportacao", index: 1, width: 50, align: "center" },
                    { name: "NomeArquivo", index: 2, width: 50, align: "left" },
                    { name: "Tamanho", index: 3, width: 50, align: "center" },
                    { name: "TempoExportacao", index: 3, width: 50, align: "center" },
                    { name: "Processo", index: 3, width: 50, align: "left" },
                    { name: "Status", index: 3, width: 50, align: "center" }
                ],
                height: 150,
                width: 1000,
                sortorder: "asc",
                viewrecords: true,
                ondblClickRow: function(rowid, status) {
                    return false;
                }
            });
        },
        
        // --------------------------------------------------------------------
        //  carregarDadosProtipo: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        carregarDadosProtipo: function() {
        
            // Cria linhas de exemplo para a lista
            $("#tbArquivoExportado").jqGrid("addRowData", 165, {
                CodigoLote: "165",
                DataExportacao: "05/05/2011 04:44:12",
                NomeArquivo: "CCI_20110505.txt",
                Tamanho: "34 Mb",
                TempoExportacao: "3m 23s",
                Processo: "Importação Lote TSYS",
                Status: "Atual"
            });
            $("#tbArquivoExportado").jqGrid("addRowData", 166, {
                CodigoLote: "166",
                DataExportacao: "06/05/2011 04:44:12",
                NomeArquivo: "CCI_20110506.txt",
                Tamanho: "33,2 Mb",
                TempoExportacao: "5m 11s",
                Processo: "Importação Lote TSYS",
                Status: "Cancelado"
            });
            $("#tbArquivoExportado").jqGrid("addRowData", 167, {
                CodigoLote: "167",
                DataExportacao: "07/05/2011 06:34:53",
                NomeArquivo: "CCI_20110507.txt",
                Tamanho: "31,2 Mb",
                TempoExportacao: "4m 57s",
                Processo: "Importação Lote Atacadão",
                Status: "Atual"
            });
        
        }
        
    };

    // Dispara a carga
    $.arquivo.exportacaoExtrato.lista.load();

    // Carrega informacoes para o prototipo
    $.arquivo.exportacaoExtrato.lista.carregarDadosProtipo();

});