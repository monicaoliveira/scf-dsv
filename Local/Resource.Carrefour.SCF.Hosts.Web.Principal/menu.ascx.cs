﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Resource.Framework.Contratos.Interface;
using Resource.Framework.Contratos.Interface.Dados;
using Resource.Framework.Contratos.Interface.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class menu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void Render(HtmlTextWriter writer)
        {
            MensagemResponseBase retorno = null;
            //Bloco de controle
            try
            {


                if (Session["codigoSessao"] != null)
                {
                    ReceberArvoreComandosInterfaceResponse resposta =
                        Mensageria.Processar<ReceberArvoreComandosInterfaceResponse>(
                            new ReceberArvoreComandosInterfaceRequest()
                            {
                                CodigoSessao = (string)Session["codigoSessao"],
                                CodigoGrupoComandoInterface = "default"
                            });

                    StringBuilder resultado = new StringBuilder();
                    resultado.Append(@"<ul class=""menu"">");
                    foreach (ComandoInterfaceInfo comandoInterface in resposta.ComandosInterfaceRaiz)
                        resultado.Append(montarComandoInterface(comandoInterface));
                    resultado.Append("</ul>");

                    writer.Write(resultado.ToString());
                }
                else
                {
                    //Cria mensagem generica informando o erro
                    retorno =
                        new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                            DescricaoResposta = "Sessão Inválida"
                        };
                }

            }
            catch (Exception ex)
            {
                // Cria mensagem generica informando o erro
                retorno =
                    new MensagemErroResponse()
                    {
                        StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                        DescricaoResposta = ex.ToString()
                    };
            }
        }

        private string montarComandoInterface(ComandoInterfaceInfo comandoInterface)
        {
            // Inicializa
            StringBuilder resultado = new StringBuilder();

            // Inicia o elemento
            resultado.Append("<li>");

            // Se tem filhos, insere
            if (comandoInterface.Filhos.Count > 0)
            {
                resultado.Append("<ul>");
                foreach (ComandoInterfaceInfo comandoInterfaceFilho in comandoInterface.Filhos)
                    resultado.Append(montarComandoInterface(comandoInterfaceFilho));
                resultado.Append("</ul>");
            }

            // Se tem link, insere
            if (comandoInterface.Tag != null && comandoInterface.Tag != "")
                resultado.Append(@"<a href=""" + comandoInterface.Tag + @""">" + comandoInterface.Nome + "</a>");
            else
                resultado.Append(@"<a href=""#"">" + comandoInterface.Nome + "</a>");

            // Finaliza o elemento
            resultado.Append("</li>");

            // Retorna
            return resultado.ToString();
        }
    }
}