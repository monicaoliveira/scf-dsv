﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.IO;
using System.Net;
using System.Web.UI.HtmlControls;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class relatorio_vencimento : System.Web.UI.Page
    {
        ListarRelatorioVencimentoResponse resposta = new ListarRelatorioVencimentoResponse();
        double valorAcumuladoConta = 0, valorAcumuladoFavorecido = 0;
        string banco, agencia, conta, favorecido;

        class ItemAgrupamentoGrupo
        {
            public string DescricaoGrupo { get; set; }
            public string CodigoGrupo { get; set; }
            public DateTime Data { get; set; }
        }

        //ClockWork - Rogério
        class ItemAgrupamentoReferencia : ItemAgrupamentoGrupo
        {
            public ItemAgrupamentoReferencia() : base()
            {
                this.Itens = new List<RelatorioVencimentoInfo>();
            }
            public string Total { get; set; }
            public string DescricaoReferencia { get; set; }
            public string CodigoReferencia { get; set; }
            public List<RelatorioVencimentoInfo> Itens { get; set; }
        }

        class ItemAgrupamentoGrupoData : ItemAgrupamentoGrupo
        {
            public ItemAgrupamentoGrupoData() : base()
            {
                this.Itens = new List<RelatorioVencimentoInfo>();
            }
            public string Total { get; set; }
            public string DataVencimento { get; set; }
            public string DataAgendamento { get; set; }
            public List<RelatorioVencimentoInfo> Itens { get; set; }
        }

        //ClockWork - Rogério
        class ItemAgrupamentoReferenciaData : ItemAgrupamentoReferencia
        {
            public ItemAgrupamentoReferenciaData() : base()
            {
                this.Itens = new List<RelatorioVencimentoInfo>();
            }
            public string Total { get; set; }
            public string DataVencimento { get; set; }
            public string DataAgendamento { get; set; }
            public List<RelatorioVencimentoInfo> Itens { get; set; }
        }

        class ItemAgrupamentoDataBancos : ItemAgrupamentoGrupoData
        {
            public ItemAgrupamentoDataBancos() : base()
            {
                this.Itens = new List<RelatorioVencimentoInfo>();
            }
            public List<RelatorioVencimentoInfo> Itens { get; set; }
        }

        //ItemRelatorioS ******************************************

        class ItemRelatorioVencimentosGrupo
        {
            public ItemRelatorioVencimentosGrupo()
            {
                this.AgrupamentoGrupo = new ItemAgrupamentoGrupo();
                this.Datas = new List<RelatorioVencimentoInfo>();
            }
            public ItemAgrupamentoGrupo AgrupamentoGrupo { get; set; }
            public List<RelatorioVencimentoInfo> Datas { get; set; }
            public string Total { get; set; }
        }

        class ItemRelatorioVencimentosReferencia
        {
            public ItemRelatorioVencimentosReferencia()
            {
                this.AgrupamentoReferencia = new ItemAgrupamentoReferenciaData();
                this.Datas = new List<RelatorioVencimentoInfo>();
            }
            public ItemAgrupamentoReferencia AgrupamentoReferencia { get; set; }
            public List<RelatorioVencimentoInfo> Datas { get; set; }
            public string Total { get; set; }
        }

        class ItemRelatorioVencimentoData
        {
            public ItemRelatorioVencimentoData()
            {
                this.AgrupamentoData = new ItemAgrupamentoDataBancos();
                this.Datas = new List<RelatorioVencimentoInfo>();
            }
            public string Total { get; set; }
            public ItemAgrupamentoDataBancos AgrupamentoData { get; set; }
            public List<RelatorioVencimentoInfo> Datas { get; set; }
        }

        public class ItemRelatorioVencimentosBanco
        {
            public string Banco { get; set; }
            public string Agencia { get; set; }
            public string Conta { get; set; }
            //public IGrouping<string, RelatorioVencimentoInfo> ItensRelatorio { get; set; }
            public List<RelatorioVencimentoInfo> ItensRelatorio { get; set; }
            public string Total { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string codOrigem = "";
            string consolidados= null;

            bool gerarPagamentos = Convert.ToBoolean(this.Request["GerarPagamentos"]);

            try
            {
                GrupoRepeater.ItemDataBound += new RepeaterItemEventHandler(GrupoRepeater_ItemDataBound);
                codOrigem = (string)this.Request["codLog"] != null ? (string)this.Request["codLog"] : null;
                //LogInfo log = new LogInfo() { Descricao = "Relatório de Vencimento - Iniciando a geração do relatório vencimento." };
                
                string codLog = "";

                // melhorias2017
                string codigoProcesso = (string)this.Request["codProcesso"] != null ? (string)this.Request["codProcesso"] : null;

                //codLog = Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "WEB - INI - Relatório Vencimento - Iniciando a geração do relatório vencimento.",
                //        TipoOrigem = "RelatorioVencimento",
                //        CodigoOrigem = codOrigem
                //    }).retorno.CodigoLog;

                Type cstype = this.GetType();
                ClientScriptManager cs = Page.ClientScript;

                // Se escolheu gerar pagamentos, precisa seguir as regras
                if (gerarPagamentos)
                {
                    // Tem que ter escolhido o tipo Financeiro
                    if (this.Request["TipoFinanceiroContabil"] != "F")
                    {
                        if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                        {
                            this.lblMensagem.Text = ("Para gerar pagamentos, é preciso ter escolhido o Tipo Financeiro.");
                            this.trMensagem.Visible = true;
                            String cstext = "alert('Para gerar pagamentos, é preciso ter escolhido o Tipo Financeiro.');";
                            cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                            return;
                        }

                    }

                    if (Convert.ToBoolean(this.Request["ConsolidacaoReferencia"]) == false)
                    {
                        if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                        {
                            this.lblMensagem.Text = ("Para gerar pagamentos, é preciso ter escolhido a Consolidação por referencia.");
                            this.trMensagem.Visible = true;
                            String cstext = "alert('Para gerar pagamentos, é preciso ter escolhido o Tipo Financeiro.');";
                            cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                            return;
                        }

                    }
                }
                

                // Pede o detalhe da pessoa
                resposta =
                        Mensageria.Processar<ListarRelatorioVencimentoResponse>(
                            new ListarRelatorioVencimentoRequest()
                            {
                                //CodigoSessao = (string)this.Session["CodigoSessao"],
                                FiltroFavorecido = this.Request["CodigoFavorecido"],
                                FiltroEmpresaGrupo = this.Request["CodigoEmpresaGrupo"],
                                FiltroEmpresaSubGrupo = this.Request["CodigoEmpresaSubGrupo"],
                                FiltroProduto = this.Request["CodigoProduto"],
                                FiltroTipoRegistro = this.Request["TipoRegistro"],
                                FiltroStatusRetornoCessao = this.Request["CodigoStatusCessao"],
                                FiltroConsolidaAgendamentoVencimento = Convert.ToBoolean(this.Request["ConsolidaAgendamentoVencimento"]),
                                FiltroConsolidaMaiorData = Convert.ToBoolean(this.Request["ConsolidacaoMaiorAgendamento"]),
                                FiltroConsolidaReferencia  = Convert.ToBoolean(this.Request["ConsolidacaoReferencia"]),
                                FiltroReferencia = this.Request["Valor"],
                                FiltroDataInicioTransacao = this.Request["DataInicioTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioTransacao"]) : null,
                                FiltroDataFimTransacao = this.Request["DataFimTransacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimTransacao"]) : null,
                                FiltroDataInicioVencimento = this.Request["DataInicioVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioVencimento"]) : null,
                                FiltroDataFimVencimento = this.Request["DataFimVencimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimVencimento"]) : null,
                                FiltroDataInicioMovimento = this.Request["DataInicioMovimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioMovimento"]) : null,
                                FiltroDataFimMovimento = this.Request["DataFimMovimento"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimMovimento"]) : null,
                                FiltroDataAgendamentoDe = this.Request["DataAgendamentoDe"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataAgendamentoDe"]) : null,
                                FiltroDataAgendamentoAte = this.Request["DataAgendamentoAte"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataAgendamentoAte"]) : null,
                                //SCF1170
                                FiltrotxtDataRetornoCCIInicio = this.Request["DataInicioRetornoCCI"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioRetornoCCI"]) : null,
                                FiltrotxtDataRetornoCCIFim = this.Request["DataFimRetornoCCI"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimRetornoCCI"]) : null,
                                //SCF1170
                                FiltroDataPosicao = this.Request["DataPosicao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataPosicao"]) : null,
                                // ECOMMERCE - Fernando Bove - 20160106
                                FiltroFinanceiroContabil = this.Request["TipoFinanceiroContabil"],
                                // CodLog = codOrigem,
                                RetornarRelatorio = true,
                                TipoArquivo = this.Request["TipoArquivo"] != null ? this.Request["TipoArquivo"].ToString() : null, //Marcos Matsuoka - Jira SCF 1085,
                                // 2017 - melhorias 
                                GerarPagamentos = Convert.ToBoolean(this.Request["GerarPagamentos"]),
                                CodigoProcesso = codigoProcesso,
                                FiltroRetornarTransacaoPaga = Convert.ToBoolean(this.Request["RetornarTransacaoPaga"])

                            });

               

                //// Faz o log
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "WEB - INF - Relatório Vencimento - Dados recuperados. Montando agrupamento.",
                //        TipoOrigem = "WEB",
                //        CodigoOrigem = codOrigem
                //    });

                // Agrupa por grupos
                var grupos =
                    from y in resposta.Resultado
                    //orderby y.DataVencimento
                    orderby y.DataAgendamento

                    group y by new
                    {
                        y.CodigoEmpresaGrupo,
                        y.DescricaoEmpresaGrupo
                    } into g
                    select new ItemRelatorioVencimentosGrupo()
                    {
                        AgrupamentoGrupo = new ItemAgrupamentoGrupo()
                        {
                            DescricaoGrupo = g.Key.DescricaoEmpresaGrupo,
                            CodigoGrupo = g.Key.CodigoEmpresaGrupo
                        },
                        Total = g.Sum(p => p.ValorLiquido).ToString("n"),
                        Datas = g.Select(p => p).ToList<RelatorioVencimentoInfo>()
                    };

                this.lblFiltroGrupo.Text = this.Request["EmpresaGrupo"] != null ? this.Request["EmpresaGrupo"] : "Todos";
                this.lblFiltroSubGrupo.Text = this.Request["EmpresaSubGrupo"] != null ? this.Request["EmpresaSubGrupo"] : "Todos";
                this.lblFiltroFavorecido.Text = this.Request["Favorecido"] != null ? this.Request["Favorecido"] : "Todos";
                this.lblFiltroProduto.Text = this.Request["Produto"] != null ? this.Request["Produto"] : "Todos";
                this.lblTipoRegistro.Text = this.Request["TipoRegistro"];
                
                this.lblStatusCessao.Text = this.Request["StatusCessao"] != null ? this.Request["StatusCessao"] : "Todos";
                this.lblFiltroDataTransacaoInicio.Text = this.Request["DataInicioTransacao"] != null ? this.Request["DataInicioTransacao"] : "Qualquer";
                this.lblFiltroDataTransacaoFim.Text = this.Request["DataFimTransacao"] != null ? this.Request["DataFimTransacao"] : "Qualquer";
                this.lblFiltroDataVencimentoInicio.Text = this.Request["DataInicioVencimento"] != null ? this.Request["DataInicioVencimento"] : "Qualquer";
                this.lblFiltroDataVencimentoFim.Text = this.Request["DataFimVencimento"] != null ? this.Request["DataFimVencimento"] : "Qualquer";
                this.lblFiltroDataMovimentoInicio.Text = this.Request["DataInicioMovimento"] != null ? this.Request["DataInicioMovimento"] : "Qualquer";
                this.lblFiltroDataMovimentoFim.Text = this.Request["DataFimMovimento"] != null ? this.Request["DataFimMovimento"] : "Qualquer";
                this.lblFiltroDataAgendamentoDe.Text = this.Request["DataAgendamentoDe"] != null ? this.Request["DataAgendamentoDe"] : "Qualquer";
                this.lblFiltroDataAgendamentoAte.Text = this.Request["DataAgendamentoAte"] != null ? this.Request["DataAgendamentoAte"] : "Qualquer";
                this.lblPosicaoCarteira.Text = this.Request["DataPosicao"] != null ? this.Request["DataPosicao"] : String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                //SCF1170
                this.lblFiltroRetornoCCIInicio.Text = this.Request["DataInicioRetornoCCI"] != null ? this.Request["DataInicioRetornoCCI"] : "Qualquer";
                this.lblFiltroRetornoCCIFim.Text = this.Request["DataFimRetornoCCI"] != null ? this.Request["DataFimRetornoCCI"] : "Qualquer";
                //SCF1170
                //if (resposta.Resultado.Count > 0)
                //    this.lblPosicaoCarteira.Text = resposta.Resultado[0].DataPosicaoCarteira != null ? String.Format("{0:dd/MM/yyyy}", resposta.Resultado[0].DataPosicaoCarteira) : "";

                this.lblDataHora.Text = String.Format("{0:dd/MM/yyyy - HH:mm}", DateTime.Now);

                // ECOMMERCE - Fernando Bove - 20160106
                this.lblFinanceiroContabil.Text = this.Request["TipoFinanceiroContabil"] != null ? this.Request["TipoFinanceiroContabil"] == "C" ? "Contábil" : "Financeiro" : "Todos";

                //Edgar Oliveira
                this.lblTransacaoPaga.Text = Convert.ToBoolean(this.Request["RetornarTransacaoPaga"]) == true ? "Sim" : "Não";
                
                //ClockWork - Rogério Alves 
                this.lblFiltroReferencia.Text = this.Request["Referencia"] != null ? this.Request["Referencia"] : "Todos";

                if (Convert.ToBoolean(this.Request["ConsolidacaoReferencia"]) == true) consolidados = "Referência";
                if (Convert.ToBoolean(this.Request["ConsolidaAgendamentoVencimento"]) == true)
                    consolidados = "Dt Agend + Dt. Venc" + (consolidados != null ? " + " + consolidados : "");
                else if (Convert.ToBoolean(this.Request["ConsolidacaoMaiorAgendamento"]) == true)
                    consolidados = "Maior Dt. Agend" + (consolidados != null ? " + " + consolidados : "");
                this.lblConsolidar.Text = consolidados != null ? consolidados : "";

                GrupoRepeater.DataSource = grupos;
                GrupoRepeater.DataBind();

                // Marcos Matsuoka - Jira SCF 1109 (Esse If estava logo após os filtros 'logo após resposta =')
                //Type cstype = this.GetType();
                //ClientScriptManager cs = Page.ClientScript;

                if (resposta.Resultado.Count == 0 && gerarPagamentos == false)
                {
                    if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                    {
                        this.lblMensagem.Text = ("Sem dados para imprimir pelo filtro informado.");
                        this.trMensagem.Visible = true;
                        String cstext = "alert('Sem dados para imprimir pelo filtro informado.');";
                        cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                        return;
                    }
                }
                else if (resposta.Resultado.Count > 1000 && (this.Request["TipoArquivo"] != "PDF")) //Marcos Matsuoka - Jira SCF 1085
                {
                    if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                    {
                        this.lblMensagem.Text = ("Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.");
                        this.trMensagem.Visible = true;
                        String cstext = "alert('Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.');";
                        cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                        return;
                    }
                }

                // totaliza as colunas
                CalculaTotais(codLog);
                //// Faz o log
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "WEB - FIM - Relatório Vencimento - Fim da geração do relatório vencimento.",
                //        TipoOrigem = "WEB",
                //        CodigoOrigem = codOrigem
                //    });

            }
            catch (WebException ex)
            {
                string pageContent = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd().ToString();
                this.lblMensagem.Text = pageContent;
                this.trMensagem.Visible = true;
            }
            catch (Exception ex)
            {
                string pageContent = ex.StackTrace;
                this.lblMensagem.Text = pageContent;
                this.trMensagem.Visible = true;
            }
        }

        void GrupoRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label lbl = e.Item.FindControl("lblGrupoRepeater") as Label;
            if (lbl != null)
            {
                if (e.Item.ItemIndex > 0)
                    lbl.Style.Add("page-break-before", "always");
            }

            Repeater ReferenciaRepeater = (Repeater)e.Item.FindControl("ReferenciaRepeater");
            ReferenciaRepeater.ItemDataBound += new RepeaterItemEventHandler(ReferenciaRepeater_ItemDataBound);
            ItemRelatorioVencimentosGrupo data = (ItemRelatorioVencimentosGrupo)e.Item.DataItem;

            // Agrupa por referencia
            var datas =
                from y in data.Datas
                group y by new { CodigoReferencia = y.Referencia.Valor != null ? y.Referencia.Valor : string.Empty, DescricaoReferencia = y.Referencia.Descricao != null ? y.Referencia.Descricao : string.Empty } into g
                select new ItemRelatorioVencimentosReferencia()
                {
                    AgrupamentoReferencia = new ItemAgrupamentoReferenciaData()
                    {
                        CodigoReferencia = g.Key.CodigoReferencia != null ? g.Key.CodigoReferencia : null,
                        DescricaoReferencia = g.Key.DescricaoReferencia != null ? g.Key.DescricaoReferencia : null
                    },
                    Datas = g.Select(p => p).ToList<RelatorioVencimentoInfo>(),
                    Total = g.Sum(p => p.ValorLiquido).ToString("n")
                };

            ReferenciaRepeater.DataSource = datas;
            ReferenciaRepeater.DataBind();
        }

        void ReferenciaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater DataRepeater = (Repeater)e.Item.FindControl("DataRepeater");
            DataRepeater.ItemDataBound += new RepeaterItemEventHandler(DataRepeater_ItemDataBound);
            ItemRelatorioVencimentosReferencia data = (ItemRelatorioVencimentosReferencia)e.Item.DataItem;

            // Agrupa por datas
            var datas =
                from y in data.Datas
                group y by new { DataAgendamento = y.DataAgendamento != null ? ((DateTime)y.DataAgendamento.Value).ToString("dd/MM/yyyy") : string.Empty, DataVencimento = y.DataVencimento != null ? ((DateTime)y.DataVencimento.Value).ToString("dd/MM/yyyy") : string.Empty } into g
                select new ItemRelatorioVencimentoData()
                {
                    AgrupamentoData = new ItemAgrupamentoDataBancos()
                    {
                        DataVencimento = g.Key.DataVencimento != null ? g.Key.DataVencimento : null,
                        DataAgendamento = g.Key.DataAgendamento
                    },
                    Datas = g.Select(p => p).ToList<RelatorioVencimentoInfo>(),
                    Total = g.Sum(p => p.ValorLiquido).ToString("n")
                };

            if (Convert.ToBoolean(this.Request["ConsolidacaoReferencia"]))
            {
                HtmlTableRow divQuebraReferencia = e.Item.FindControl("divQuebraReferencia") as HtmlTableRow;
                divQuebraReferencia.Visible = true;

                HtmlTableRow divQuebraReferenciaTotal = e.Item.FindControl("divQuebraReferenciaTotal") as HtmlTableRow;
                divQuebraReferenciaTotal.Visible = true;

                HtmlTableRow divQuebraLinhaReferencia = e.Item.FindControl("divQuebraLinhaReferencia") as HtmlTableRow;
                divQuebraLinhaReferencia.Visible = true;
            }                

            DataRepeater.DataSource = datas;
            DataRepeater.DataBind();
        }


        void DataRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater BancoRepeater = (Repeater)e.Item.FindControl("BancoRepeater");
            BancoRepeater.ItemDataBound += new RepeaterItemEventHandler(BancoRepeater_ItemDataBound);
            ItemRelatorioVencimentoData data = (ItemRelatorioVencimentoData)e.Item.DataItem;

            // Agrupa por datas
            var bancos =
                from y in data.Datas
                group y by new { y.Estabelecimento.Banco/*, y.Estabelecimento.Agencia, y.Estabelecimento.Conta*/ } into g
                select new ItemRelatorioVencimentosBanco()
                {
                    Banco = g.Key.Banco,
                    //Agencia = g.Key.Agencia,
                    //Conta = g.Key.Conta,
                    Total = g.Sum(p => p.ValorLiquido).ToString("n"),
                    ItensRelatorio = g.Select(p => p).ToList<RelatorioVencimentoInfo>()
                };

            BancoRepeater.DataSource = bancos;
            BancoRepeater.DataBind();            
        }

        void BancoRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater estabelecimentoRepeater = (Repeater)e.Item.FindControl("EstabelecimentoRepeater");
            estabelecimentoRepeater.DataSource = ((ItemRelatorioVencimentosBanco)e.Item.DataItem).ItensRelatorio;
            estabelecimentoRepeater.DataBind();
        }

        /// varre as linhas para colocar os totais de conta e favorecido
        private void CalculaTotais(string codLog)
        {
            //// Faz o log
            //Mensageria.Processar<SalvarLogResponse>(
            //    new SalvarLogRequest()
            //    {
            //        CodigoUsuario = "",
            //        Descricao = "WEB - INI - Relatório Vencimento - Iniciando o cálculo de totais por conta e estabelecimento.",
            //        TipoOrigem = "WEB",
            //        CodigoOrigem = codLog
            //    });

            // incializa as variaveis
            banco = "";
            agencia = "";
            conta = "";
            favorecido = "";

            foreach (RepeaterItem grupoItem in GrupoRepeater.Items)
            {
                //varre o repeater de referencia
                Repeater referenciaRepeater = (Repeater)grupoItem.FindControl("ReferenciaRepeater");
                foreach (RepeaterItem referenciaItem in referenciaRepeater.Items)
                {
                    // varre o repeater de data
                    Repeater dataRepeater = (Repeater) referenciaItem.FindControl("DataRepeater");
                    foreach (RepeaterItem dataItem in dataRepeater.Items)
                    {
                        // varre o repeater de banco
                        Repeater bancoRepeater = (Repeater)dataItem.FindControl("BancoRepeater");
                        foreach (RepeaterItem bancoItem in bancoRepeater.Items)
                        {
                            // varre o repeater de estabelecimento
                            Repeater estabelecimentoRepeater = (Repeater)bancoItem.FindControl("EstabelecimentoRepeater");
                            foreach (RepeaterItem estabelecimentoItem in estabelecimentoRepeater.Items)
                            {
                                // caso nao seja o primeiro item
                                if (estabelecimentoItem.ItemIndex > 0)
                                {
                                    // alterou banco/agencia ou conta. Inclui a somatoria
                                    if ((banco != "" && banco != ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoBanco")).Text)
                                        || (agencia != "" && agencia != ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoAgencia")).Text)
                                        || (conta != "" && conta != ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoConta")).Text))
                                    {
                                        // recupera a linha anterior
                                        RepeaterItem item = estabelecimentoRepeater.Items[estabelecimentoItem.ItemIndex - 1];

                                        // caso haja valor acumulado, insere, senão insere o proprio valor liquido
                                        if (valorAcumuladoConta == 0)
                                            ((Label)item.FindControl("lblTotalConta")).Text = ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoValor")).Text;
                                        else
                                            ((Label)item.FindControl("lblTotalConta")).Text = valorAcumuladoConta.ToString("N2");

                                        // zera a variavel
                                        valorAcumuladoConta = 0;
                                    }

                                    // alterou favorecido
                                    if (favorecido != "" && favorecido != ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoCNPJ")).Text)
                                    {
                                        // recupera a linha anterior
                                        RepeaterItem item = estabelecimentoRepeater.Items[estabelecimentoItem.ItemIndex - 1];

                                        // caso haja valor acumulado, insere, senão insere o proprio valor liquido
                                        if (valorAcumuladoFavorecido == 0)
                                        {

                                            ((Label)item.FindControl("lblTotalFavorecido")).Text = ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoValor")).Text;
                                        }
                                        else
                                            ((Label)item.FindControl("lblTotalFavorecido")).Text = valorAcumuladoFavorecido.ToString("N2");

                                        // zera a variavel
                                        valorAcumuladoFavorecido = 0;
                                    }
                                }
                                // insere os valores nas variáveis
                                banco = ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoBanco")).Text;
                                agencia = ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoAgencia")).Text;
                                conta = ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoConta")).Text;
                                favorecido = ((Label)estabelecimentoItem.FindControl("lblEstabelecimentoCNPJ")).Text;

                                // acumula os valores
                                valorAcumuladoConta = valorAcumuladoConta + Convert.ToDouble(((Label)estabelecimentoItem.FindControl("lblEstabelecimentoValor")).Text);
                                valorAcumuladoFavorecido = valorAcumuladoFavorecido + Convert.ToDouble(((Label)estabelecimentoItem.FindControl("lblEstabelecimentoValor")).Text);

                                // caso seja a última linha, coloca os totalizadores.
                                if (estabelecimentoItem.ItemIndex == estabelecimentoRepeater.Items.Count - 1)
                                {
                                    if (valorAcumuladoConta != 0)
                                    {
                                        ((Label)estabelecimentoItem.FindControl("lblTotalFavorecido")).Text = valorAcumuladoFavorecido.ToString("N2");
                                        ((Label)estabelecimentoItem.FindControl("lblTotalConta")).Text = valorAcumuladoConta.ToString("N2");
                                    }
                                }
                            } //EstabelecimentoRepeater

                            valorAcumuladoConta = valorAcumuladoFavorecido = 0;
                        } //bancoRepeater
                    } //dataRepeater
                } //referenciaRepeater
            } //grupoRepeater
             
            //// Faz o log
            //Mensageria.Processar<SalvarLogResponse>(
            //    new SalvarLogRequest()
            //    {
            //        CodigoUsuario = "",
            //        Descricao = "WEB - FIM - Relatório Vencimento - Cáculo de totais finalizado.",
            //        TipoOrigem = "WEB",
            //        CodigoOrigem = codLog
            //    });
        }
    }
}
