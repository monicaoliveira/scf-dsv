﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Resource.Framework.Contratos.Comum;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class site_relatorio_saldo_conciliacao : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["acao"] == "xls")
            {
                /****************************************************************
                TODO: Montar filtro do relatório de saldo de conciliação contábil
                ****************************************************************/

                this.Response.Clear();
                this.Response.AddHeader("Content-Disposition",
                    "attachment; filename=relatorio_saldo_conciliacao.csv; size=6");

                StringBuilder resultado = new StringBuilder();
                /******************************************************************************
                TODO: Montar resultado do relatório de saldo de conciliação contábil para Excel
                ******************************************************************************/

                this.Response.Write(resultado.ToString());
                this.Response.Flush();
                this.Response.End();
            }
            else if (Request["acao"] == "pdf")
            {
                string urlToConvert = GetUrlRelatorio();
                string downloadName = "RelatorioSaldoConciliacao";
                byte[] downloadBytes = null;

                downloadName += ".pdf";
                downloadBytes = PdfHelper.GetDownloadBytes(urlToConvert, downloadName);

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition",
                    "attachment; filename=" + downloadName + "; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();

            }
        }

        protected string GetUrlRelatorio()
        {
            /* **************************************
             * PARA TESTAR O PLUGIN GERADOR DE PDF 
             * É NECESSÁRIO ALTERAR O VALOR DA PORTA 
             * DE ACORDO COM A SUA MÁQUINA
             * **************************************/

            string url = "http://localhost/relatorio_saldo_conciliacao.aspx";

            string parametros = "";
            parametros += this.Request["DataInicioConciliacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataInicioConciliacao"]) + "&" : null;
            parametros += this.Request["DataFimConciliacao"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["DataFimConciliacao"]) + "&" : null;

            if (parametros != "")
                url += "?" + parametros.Substring(0, parametros.Length - 1);

            return url;
        }

    }
}
