﻿$(document).ready(function() {

    // Funcoes do namespace processo.lista
    $.auxilio2 = {
   
        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {
        
            // Botao executar
            $("#cmdExecutar").click(function() {
            
                // Chama o servico
                executarServico(
                    "ExecutarAuxilioConfigRequest", 
                    {
                        TipoConfig: $("#txtTipoConfig").val()
                    }, 
                    function(data) {
                    
                        // Insere o valor do config no textArea
                        $("#txtConfig").append(data.ConfigXML);
                    
                    });
            
            });
        }
   
    };
    
    // Dispara a carga
    $.auxilio2.load();

});    