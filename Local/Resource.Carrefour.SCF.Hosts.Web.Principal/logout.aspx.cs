﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Campidelli 24/05/2012
            this.Session.Abandon();
            Response.Redirect("login.aspx");
        }
    }
}
