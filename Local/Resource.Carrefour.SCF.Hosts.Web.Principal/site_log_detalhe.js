﻿$(document).ready(function() {

    // Define o namespace log
    if (!$.log)
        $.log = {};

    // Define as funcoes do namespace log.detalhe
    $.log.detalhe = {

        // ----------------------------------------------------------------------
        // load: garante que o HTML da esteja carregado e inicializado
        //
        //  callback:       função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        load: function(callback) {

            // Insere o subtítulo
            //$("#subtitulo").html("Administrativo > Logs"); //1353
            
            // Verifica se o html esta carregado
            if ($("#site_log_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_log_detalhe.aspx #site_log_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_log_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.log.detalhe.load2(function() {

                        // Chama o callback
                        if (callback)
                            callback();

                    });

                });

            } else {
                // Chama o callback
                if (callback)
                    callback();
            }

        },

        // ----------------------------------------------------------------------
        // Complementa a rotina de inicializacao especificamente na inicialização
        //   dos componentes da tela
        //
        //  callback:       função a ser chamada ao final da execução
        // ----------------------------------------------------------------------
        load2: function(callback) {

            // Tabs
            $("#site_log_detalhe").tabs();
            
            // Inicializa o dialog
            $("#site_log_detalhe").dialog({
                autoOpen: false,
                height: 320,
                width: 460,
                modal: true,
                title: "Detalhe de log",
                buttons: {
                    "Fechar": function() {
                        // Fecha o dialog
                        $("#site_log_detalhe").dialog("close");
                    }
                }
            });

            // Inicializa tabela de condicoes
            $("#site_log_detalhe_condicoes").jqGrid({
                datatype: "clientSide",
                colNames: ["Propriedade", "Tipo", "Valor"],
                colModel: [
                    { name: "Propriedade", index: 0, width: 55 },
                    { name: "TipoCondicao", index: 1, width: 50, align: "right" },
                    { name: "Valor", index: 2, width: 80 }
                ],
                height: 150,
                width: 400,
                caption: "Condições"
            });

            //Inicializa tabela de propriedades
            $("#site_log_detalhe_propriedades").jqGrid(
            {
                datatype: "clientSide",
                colNames: ["Propriedade", "Valor Antigo", "Valor Novo"],
                colModel: [
                    { name: "NomePropriedade", index: 0, width: 55 },
                    { name: "ValorAnterior", index: 1, width: 50 },
                    { name: "ValorNovo", index: 2, width: 80 }
                ],
                height: 150,
                width: 400,
                caption: "Propriedades"
            });

            // Se tem callback, executa
            if (callback)
                callback();

        },

        // ----------------------------------------------------------------------
        // Abre o dialog de detalhe da log carregando a log informado,
        //   ou permitindo um novo cadastro (caso não seja informado um código existente)
        //
        //  codigolog:     código da log a carregar. caso seja nulo,
        //                      abre a tela para inclusão de uma nova log
        //  callback:           função a ser chamada ao final da execução
        // ----------------------------------------------------------------------
        abrir: function(codigolog, callback) {

            // Garante que o html está carregado
            $.log.detalhe.load(function() {

                // Carrega informações do log
                $.log.detalhe.carregarDados(codigolog, function() {

                    // Carrega a primeira aba
                    $("#site_log_detalhe").tabs("select", 0);

                    // Mostra o dialog
                    $("#site_log_detalhe").dialog("open");

                    // Chama o callback
                    if (callback)
                        callback();

                });

            });

        },

        // ----------------------------------------------------------------------
        // Carrega as informações da tela
        //
        //  callback:       Função a ser chamada no final da execução
        // ----------------------------------------------------------------------
        carregarDados: function(codigolog, callback) {

            // Pede informações do log
            executarServico(
                "ReceberLogRequest",
                { Codigolog: codigolog },
                function(data) {

                    // Preenche detalhe
                    $("#site_log_detalhe").Forms("setData", { dataObject: data.LogInfo });

                    // Esconde todas as visoes especificas
                    $("#site_log_detalhe .visoes > div").hide();

                    // Mostra a visao necessaria
                    $("#site_log_detalhe .visoes ." + data.LogInfo.TipoLog).show();

                    // Monta a lista de propriedades retornadas no log
                    var propriedades = "";
                    for (var propriedade in data.LogInfo) 
                        propriedades +=
                            "<label class='tituloLogPropriedade'>" + propriedade + 
                            "</label><label style='width: 250px;'>" + data.LogInfo[propriedade] + 
                            "</label><br /><br />";
                    $("#logAbaPropriedades").html(propriedades);

                    // Chama o callback
                    if (callback)
                        callback();

                });

        }

    }

});



