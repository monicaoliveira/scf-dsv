﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_processo_cessao.js"></script>
    <link href="site_processo_cessao.css" rel="stylesheet" type="text/css" />
    <title>SCF - Processo Atacadão</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- Conteúdo da Página -->
    <div id="site_processo_cessao">

        <!-- Disparo manual -->
        <div class="detalhe">
            
            <span class="header"><label id="labelDiretorio">Diretório</label></span>
            <span class="header"><label id="labelNomeArquivo">Nome do Arquivo</label></span>
            <br />
            
            <label>Importação Cessao:</label>
            <input type="text" id="txtArquivoRetCessaoDiretorio" class="inputLeft" />
            <input type="text" id="txtArquivoRetCessao" class="inputRight" />
            <br />
            <br />
             
            <button id="cmdProcessarCessao">Iniciar Processamento</button>
            <br />
                
        </div>
       
    </div>

</asp:Content>
