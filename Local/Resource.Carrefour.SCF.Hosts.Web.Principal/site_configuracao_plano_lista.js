﻿$(document).ready(function() {

    // Cria namespace 
    if (!$.plano)
        $.plano = {};

    // Funcoes do namespace 
    $.plano.lista = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Configurações > Cadastro de Planos");

            // Estilo dos botões
            $("#site_configuracao_plano_lista button").button();

            // Inicializa forms
            $("#site_configuracao_plano_lista").Forms("initializeFields");

            // Botão Importar Produto/Plano
            $("#cmdImportarProdutoPlano").click(function() {
                // Limpa campos
                $("#importacao").Forms("clearData");
                // Atribui um valor padrão
                $("#txtCaminhoArquivo").val("");
                // Abre dialog de importação de produtos/planos
                $("#importacao").dialog("open");
            });

            // Inicializa o dialog de importação produtos/planos
            $("#importacao").dialog({
                autoOpen: false,
                height: 150,
                width: 500,
                modal: true,
                title: "Importação de planos",
                buttons: {
                    "Importar": function() {

                        // Pede para importar
                        $.plano.lista.importar(function(data) {

                            // Contém críticas?
                            var mensagem = "Planos importados com sucesso!";
                            var icone = "sucesso";
                            var largura = 400;
                            var altura = 150;
                            var redimensionar = true;
                            if (data && data.Criticas[0]) {
                                // Monta mensagem
                                largura = 580;
                                altura = 150 + (data.Criticas.length * 10);
                                redimensionar = true;
                                icone = "atencao";
                                mensagem = "Planos importados com sucesso!<br/><br/>";
                                mensagem += "ATENÇÃO! Ocorreram as seguintes críticas:<br/>";
                                for (var i = 0; i < data.Criticas.length; i++) {
                                    mensagem += "<br/>" + data.Criticas[i].Descricao;
                                }
                            }

                            // Mostra popup de sucesso
                            popup({
                                titulo: "Importar Planos",
                                icone: icone,
                                mensagem: mensagem,
                                resizable: redimensionar,
                                width: largura,
                                height: altura,
                                callbackOK: function() {
                                    // Atualizo a lista
                                    $.plano.lista.listarPlano(function() {
                                        // Fecha dialog de importação de planos
                                        $("#importacao").dialog("close");
                                    });
                                }
                            });
                        });
                    },
                    "Fechar": function() {

                        // Fecha o dialog 
                        $("#importacao").dialog("close");
                    }
                }
            });

            // Inicializa grid de plano 
            $("#tbPlano").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                scroll: true,
                height: 160,
                width: 320,
                colNames: ["Código", "Código TSYS", "Nome", "Descrição", "Quantidade de Parcelas", "Comissão"],
                colModel: [
                    { name: "CodigoPlano", index: "CodigoPlano", hidden: true, key: true, sorttype: "integer" },
                    { name: "CodigoPlanoTSYS", index: "CodigoPlanoTSYS", width: 40, firstsortorder: "desc" },
                    { name: "NomePlano", index: "NomePlano", width: 100 },
                    { name: "DescricaoPlano", index: "DescricaoPlano", width: 100 },
                    { name: "QuantidadeParcelas", index: "QuantidadeParcelas", width: 40, align: "right" },
                    { name: "Comissao", index: "Comissao", width: 40, align: "right", formatter: "number" }
                ],
                sortable: true,
                ondblClickRow: function(rowid, iRow, iCol, e) {

                    // Pega o código do plano
                    var codigoPlano = $("#tbPlano").getGridParam("selrow");

                    if (codigoPlano) {

                        // Abre dialog de detalhe do plano
                        $.plano.detalhe.abrir(codigoPlano, null, function() {
                            $.plano.lista.listarPlano(null, true);
                        });
                    }

                    else {

                        // Pede para selecionar a linha
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Favor selecionar uma linha!",
                            callbackOK: function() { }
                        });
                    }
                }
            });

            // BOTÃO NOVO PLANO
            $("#cmdNovoPlano").click(function() {
                // Abre dialog de detalhe do plano
                $.plano.detalhe.abrir(null, null, function() {
                    $.plano.lista.listarPlano(null, true);
                });
            });

            // BOTÃO EDITAR PLANO
            $("#cmdEditarPlano").click(function() {

                // Pega o código do plano
                var codigoPlano = $("#tbPlano").getGridParam("selrow");

                if (codigoPlano) {

                    // Abre dialog de detalhe do plano
                    $.plano.detalhe.abrir(codigoPlano, null, function() {
                        $.plano.lista.listarPlano(null, true);
                    });
                }
                else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // BOTÃO REMOVER PLANO
            $("#cmdRemoverPlano").click(function() {

                // Pega o código do plano
                var codigoPlano = $("#tbPlano").getGridParam("selrow");

                executarServico(
                "ReceberPlanoRequest",
                { CodigoPlano: codigoPlano },
                function(data) {
                    if (data.PlanoInfo) {

                        if (codigoPlano) {

                            // pede confirmação de exclusão
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "Tem certeza que deseja remover este plano?",
                                callbackSim: function() {

                                    // Pede para remover
                                    $.plano.detalhe.remover(codigoPlano, function(data) {

                                        if (data.DescricaoResposta) {
                                         
                                            // Mostra popup de mensagem de erro
                                            popup({
                                                titulo: "Remover Plano",
                                                icone: "erro",
                                                mensagem: data.DescricaoResposta,
                                                callbackOK: function() {
                                                    
                                                }
                                            });

                                        } else {

                                            // Mostra popup de sucesso
                                            popup({
                                                titulo: "Remover Plano",
                                                icone: "sucesso",
                                                mensagem: "Plano removido com sucesso!",
                                                callbackOK: function() {
                                                    $.plano.lista.listarPlano(null, true);
                                                }
                                            });
                                        }
                                    });
                                },
                                callbackNao: function() {

                                }
                            });
                        }
                        else {

                            // Pede para selecionar a linha
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "Favor selecionar uma linha!",
                                callbackOK: function() {

                                }
                            });
                        }
                    } else {
                        //Avisa o usuario que o plano já foi excluido.
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Plano já foi excluido!",
                            callbackOK: function() {
                                $.plano.lista.listarPlano(null, true);
                            }
                        });
                    }

                });
            });


            // BOTÃO ATUALIZAR LISTA
            $("#cmdAtualizarListaPlano").click(function() {

                // Atualiza lista de planos
                $.plano.lista.listarPlano(null, true);
            });

            // PEDE LISTA DE PLANOS
            $.plano.lista.listarPlano(null, true);

            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbPlano").setGridWidth($(window).width() - margemEsquerda);
                $("#tbPlano").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");
        },

        //------------------------------------------------------------------------
        // listarPlano: Pede listas de planos ao serviço
        //------------------------------------------------------------------------
        listarPlano: function(callback, listaAtivos) {

            // Cria request
            var request = { FiltroAtivo: listaAtivos };

            // Pede lista de planos
            executarServico(
            "ListarPlanoRequest",
            request,
            function(data) {

                // Limpa grid
                $("#tbPlano").clearGridData();
                for (var i = 0; i < data.Resultado.length; i++)
                    $("#tbPlano").jqGrid("addRowData", data.Resultado[i].CodigoPlano, data.Resultado[i]);

                // Chama callback
                if (callback)
                    callback();
            });
        },

        // --------------------------------------------------------------------
        //  importar: Importa produtos/planos do arquivo txt
        // --------------------------------------------------------------------
        importar: function(callback) {

            // Cria request
            var request = {};
            var caminhoArquivo = $("#txtCaminhoArquivo").val();

            if (caminhoArquivo && caminhoArquivo != "") {
                request.CaminhoArquivo = caminhoArquivo;
                // Solicita servico importacao
                executarServico(
                "ImportarProdutoPlanoRequest",
                request,
                function(data) {

                    // Chama callback
                    if (callback)
                        callback(data);
                });

            } else {
                //Avisa o usuario que o plano já foi excluido.
                popup({
                    titulo: "Atenção",
                    icone: "atencao",
                    mensagem: "Selecione um arquivo de importação válido!",
                    callbackOK: function() { }
                });
            }
        }
    };

    // Pede a inicializacao
    $.plano.lista.load();
});