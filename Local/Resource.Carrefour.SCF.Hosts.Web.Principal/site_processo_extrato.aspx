﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_processo_extrato.js"></script>
    <link href="site_processo_extrato.css" rel="stylesheet" type="text/css" />
    <title>SCF - Processo Extrato</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- Conteúdo da Página -->
    <div id="site_processo_extrato">

        <!-- Disparo manual -->
        <div class="detalhe">
            
            
            <span class="header"><label id="labelDiretorio">Diretório</label></span>
            <span class="header"><label id="labelNomeArquivo">Nome do Arquivo</label></span>
            <br />
            
            <label>Importação Grupo Carrefour:</label>
            <input type="text" class="inputleft" id="txtArquivoTSYSDiretorio" />
            <input type="text" class="inputright" id="txtArquivoTSYSArquivo" />
            <br />
            
            
            
            <label>Importação Grupo Atacadão:</label>
            <input type="text" class="inputleft" id="txtArquivoTSYSDiretorioAtacadao" />
            <input type="text" class="inputright" id="txtArquivoTSYSArquivoAtacadao" />
            <br />
            
            <label>Importação Grupo Galeria:</label>
            <input type="text" class="inputleft" id="txtArquivoTSYSDiretorioGaleria" />
            <input type="text" class="inputright" id="txtArquivoTSYSArquivoGaleria" />
            <br />
            
            <label>Exportação CCI:</label>
            <input type="text" class="inputleft" id="txtArquivoCCIDiretorio" />
            <br />
            
            <label>Exportação ATA:</label>
            <input type="text" class="inputleft" id="txtArquivoATADiretorio" />
            <br />
            
            <label>Exportação GAL:</label>
            <input type="text" class="inputleft" id="txtArquivoGALDiretorio" />
            <br />
            <br />
            
            <button id="cmdProcessarExtrato">Iniciar Processamento</button>
            <br />
                
        </div>
       
    </div>

</asp:Content>
