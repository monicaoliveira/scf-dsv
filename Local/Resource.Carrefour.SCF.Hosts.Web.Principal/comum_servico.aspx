﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="comum_servico.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.comum_servico" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Serviços</title>

        <!-- BIBLIOTECAS DO JQUERY -->
        <% if (HttpContext.Current.IsDebuggingEnabled) { %>
            <script type="text/javascript" src="js/lib/jquery-1.4.4.js"></script>
        <% } else { %>
            <script type="text/javascript" src="js/lib/jquery-1.4.4.min.js"></script>
        <% } %>

        <!-- BIBLIOTECAS DA PAGINA -->
        <script type="text/javascript" src="comum_servico.js"></script>
    </head>
    <body>
        <span>Página de serviços</span>
    </body>
</html>
