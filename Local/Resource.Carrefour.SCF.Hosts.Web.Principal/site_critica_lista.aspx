﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_critica_lista.js"></script>
    <script type="text/javascript" src="site_critica_detalhe.js"></script>
    <link href="site_critica_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_critica_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Críticas</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- Conteúdo da Página -->
    <div id="site_critica_lista" class="divFormulario">

        <!-- Área do filtro -->
        <div id="painelCriticaFiltro">
            <table width="605px">
                <tr>
                    <td>
                        <label>Tipo de Crítica:</label>
                    </td>
                    <td colspan="2">
                        <select class="cmbTipoCritica">
                            <option value="?">(selecione)</option>
                        </select>
                    </td>
                    <td align="right">
                        <button class="cmdPesquisar">Pesquisar</button>
                    </td>
                 </tr>
            </table>                 
        </div>  

        <!-- Resultado -->
        <div style="margin-top:10px" id="tb_site_critica_lista">
            <table>
                <tr>
                    <td>
                        <table id="tbCritica"></table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <button class="cmdDetalhe">Mostrar Detalhe</button>
                    </td>
                </tr>
            </table>
        </div>
       
    </div>

</asp:Content>
