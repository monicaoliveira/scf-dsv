﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" CodeBehind="site_relatorio_saldo_conciliacao.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_relatorio_saldo_conciliacao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_relatorio_saldo_conciliacao.js"></script>
    <link href="site_relatorio_saldo_conciliacao.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">    
    <div id="site_relatorio_saldo_conciliacao">
        <div id="filtros_saldo_conciliacao">        
            <div id="filtroPeriodo">
                <label class="tituloFiltro">Períodos</label>

                <div class="borda">
                    <ul>
                        <li class="label_data_titulo">
                            <label>Data da Transacao:</label>
                        </li>                        
                        <li>
                            <ul>
                                <li class="label_data">
                                    <label>Dê:</label>
                                </li>
                                <li class="campo_data">
                                    <input type="text" id="txtPeriodoConciliacaoDe" class="forms[property[FiltroDataInicioConciliacao];required;dataType[date];maskType[date]]" />
                                </li> 
                                <li class="label_data">
                                    <label>Até:</label>
                                </li>                                
                                <li class="campo_data">
                                    <input type="text" id="txtPeriodoConciliacaoAte" class="forms[property[FiltroDataFimConciliacao];required;dataType[date];maskType[date]]" />
                                </li>
                            </ul>
                        </li>
                    </ul>                                                                                                 
                </div>
            </div>            
            <div class="teste">                
                <button id="btnVisualizarHtml">Visualizar</button>
                <button id="btnGerarRelatorioExcel">Gerar Excel</button>                
            </div>
        </div>        
        <div id="resultado_relatorio"></div>
    </div>
</asp:Content>