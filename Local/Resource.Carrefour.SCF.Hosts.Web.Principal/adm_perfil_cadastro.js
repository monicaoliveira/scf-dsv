﻿/// <reference path="../../lib/jquery/dev/jquery-1.4.1-vsdoc.js" />
/// <reference path="comum_servico.aspx.js" />

var permissoesCarregadas = false;
var perfisCarregados = false;
var gruposCarregados = false;
var bNovoPerfil = false;

$(document).ready(function() {

    // Seta o subtítulo da tela
    $("#subtitulo").html("ADMINISTRATIVO [Cadastro de Perfis]");

    //$("button").addClass("botao marginBotao");
    //$("input[type='button']").addClass("botao marginBotao");
    $("button").button();

    $("input[type='button']").button();

    $("#perfil_edicao").tabs();
    $("#cmdAtualizarLista").click(atualizarLista);
    $("#cmdAdicionarPermissao").click(adicionarPermissao);

    $("#cmdNovoPerfil").click(novoPerfil);

    $("#cmdEditarPerfil").click(function() {
        var codigoPerfil = $("#tbPerfilLista").getGridParam("selrow");
        bNovoPerfil = false;
        if (codigoPerfil)
            selecionarPerfil(codigoPerfil);
        else {
            $("#textoAlerta").html("Selecione um Perfil.");
            $("#mensagemAlerta").dialog({
                autoOpen: true,
                resizable: false,
                height: 150,
                modal: true,
                buttons: {
                    "Ok": function() {
                        $(this).dialog("close");
                        return false;
                    }
                },
                title: "Atenção"
            });
        }
        return false;

    });
    $("#cmdRemoverPerfil").click(function() {
        var codigoPerfil = $("#tbPerfilLista").getGridParam("selrow");

        if (codigoPerfil) {

            // pede confirmação de exclusão
            popup({
                titulo: "Atenção",
                icone: "atencao",
                mensagem: "Tem certeza que deseja remover este perfil?",
                callbackSim: function() {

                    // Pede para remover
                    removerPerfil(codigoPerfil, function() {

                        // Mostra popup de sucesso
                        popup({
                            titulo: "Remover Perfil",
                            icone: "sucesso",
                            mensagem: "Perfil removido com sucesso!",
                            callbackOK: function() { }
                        });
                    });
                },
                callbackNao: function() { }
            });
        }
        else {

            // Pede para selecionar a linha
            popup({
                titulo: "Atenção",
                icone: "atencao",
                mensagem: "Favor selecionar uma linha!",
                callbackOK: function() { }
            });
        }
        return false;
    });

    $("#cmdRemoverPermissao").click(function() {

        var codigoPerfilPermissao = $("#tbPerfilPermissaoLista").getGridParam("selrow")

        if (codigoPerfilPermissao)
            $("#tbPerfilPermissaoLista").delRowData($("#tbPerfilPermissaoLista").getGridParam("selrow"));
        else {
            $("#textoAlerta").html("Selecione uma Permissao.");
            $("#mensagemAlerta").dialog({
                autoOpen: true,
                resizable: false,
                height: 150,
                modal: true,
                buttons: {
                    "Ok": function() {
                        $(this).dialog("close");
                        return false;
                    }
                },
                title: "Atenção"
            });
        }
        return false;
    });

    $("#confirmacaoExclusao").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Sim": function() {
                $(this).dialog("close");
                return false;
            },
            "Não": function() {
                $(this).dialog("close");
                return false;
            }
        }
    });

    $("#perfil_edicao").dialog(
    {   autoOpen: false
    //1477 ,   title: "Editar Perfil"
    ,   height: 320
    ,   width: 700
    ,   modal: true
    ,   buttons: 
        {   'Salvar': function() 
            {   salvarPerfil();
                $(this).dialog("close");
                return false;
            }
        ,   'Cancelar': function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });

    $("#permissao_lista").dialog( //1477 AJUSTE NA TELA
    {   autoOpen: false
    ,   close: function() {}
    ,   title: "PERFIL - Adicionar Permissão" 
    ,   height: 350
    ,   width: 500
    ,   modal: true
    ,   buttons: 
        {   'Incluir': function() 
            {   var rowid = $("#tbPermissaoLista").getGridParam("selrow");
                if (rowid) 
                {   var rowdata = $("#tbPermissaoLista").getRowData(rowid);
                    rowdata.Status = $("#cmbTipoPermissao").val();
                    $("#tbPerfilPermissaoLista").addRowData(rowid,rowdata,"last","after");
                    $(this).dialog("close");
                    return false;
                }
                else 
                {   popup(
                    {   titulo: "Atenção"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar uma linha!"
                    ,   callbackOK: function() { }
                    });
                }
            }
        ,   'Cancelar': function() 
            {   $(this).dialog("close");
                return false;
            }
        }
    });

    $("#tbPerfilLista").jqGrid(//1477 INCLUSAO DO CODIGO DO GRUPO NA LISTA
    {       datatype: "clientSide"
    ,       viewrecords: true
    ,       caption: ""
    ,       mtype: "GET"
    ,       colNames: ["PERFIL", "NOME"]
    ,       colModel: [
                          { name: "CodigoPerfil", index: "CodigoPerfil", align: "center", key: true, width: 50 },
                          { name: "NomePerfil", index: "NomePerfil", align: "left", width: 500, sortable: false  }
                      ]
    ,       ondblClickRow: function(rowid, status) 
            {
                selecionarPerfil(rowid);
                return false;
            }
    });

    $("#tbPermissaoLista").jqGrid(//1477 INCLUSAO DO CODIGO NO GRID
    {       datatype: "clientSide"
    ,       height: 200 //1477
    ,       width: 450 //1477
    ,       sortable: false //1477
    ,       shrinkToFit: false //1477
    ,       viewrecords: true
    ,       caption: ""
    ,       mtype: "GET"
    ,       colNames: ["CD", "Permissão"]
    ,       colModel: 
            [   { name: "CodigoPermissao", index: "CodigoPermissao",  width: 100, sortable: false, align: "center", key: true }
            ,   { name: "NomePermissao", index: "NomePermissao", sortable: false, width: 300 }
            ]
        });

    $("#tbPerfilPermissaoLista").jqGrid(//1477 ajuste na tela
    {       datatype: "clientSide"
    ,       viewrecords: true
    ,       caption: ""
    ,       mtype: "GET"
    ,       colNames: ["CD", "Permissão", "Status"]
    ,       colModel: 
            [   { name: "CodigoPermissao", index: "CodigoPermissao", width: 100, align: "center", key: true }
            ,   { name: "NomePermissao", index: "NomePermissao", width: 400 }
            ,   { name: "Status", index: "Status", width: 120, align: "right", edittype: "select", formatter: "select", editoptions: { value: "0:Permitido;1:Negado"} }
                        ]
        });

    $(window).bind("resize", function() {
        $("#tbPerfilLista").setGridWidth($(window).width() - margemEsquerda);
        $("#tbPerfilLista").setGridHeight($(window).height() - margemRodape);
    }).trigger("resize");

    atualizarLista();

});

function atualizarLista() {
    executarServico("ListarPerfisRequest", {}, "atualizarListaCallback");
}

function atualizarListaCallback(data) {
    $("#tbPerfilLista").clearGridData();
    for (var i = 0; i < data.Perfis.length; i++)
        $("#tbPerfilLista").addRowData(data.Perfis[i].CodigoPerfil, data.Perfis[i], "last", "after");       
}

function novoPerfil() {
    bNovoPerfil = true;
    //$("#perfil_detalhe #codigoPerfil").removeAttr("disabled");
    $("#perfil_detalhe #codigoPerfil").val("");
    $("#perfil_detalhe #nomePerfil").val("");

    $("#tbPerfilPermissaoLista").clearGridData();
    $("#perfil_edicao").tabs("select",0); //1477
    $("#perfil_edicao").dialog({title: "PERFIL - Inclusao"});  //1477
    $("#perfil_edicao").dialog("open");
}

function selecionarPerfil(codigoPerfil) {
    executarServico(
        "ReceberPerfilRequest",
        {
            CodigoPerfil: codigoPerfil,
            PreencherColecoesCompletas: true
        },
        "selecionarPerfilCallback");
}

function selecionarPerfilCallback(data) {
    $("#perfil_detalhe #codigoPerfil").val(data.Perfil.CodigoPerfil);
    $("#perfil_detalhe #nomePerfil").val(data.Perfil.NomePerfil);    
    $("#perfil_detalhe #codigoPerfil").attr("disabled", "disabled");

    $("#tbPerfilPermissaoLista").clearGridData();
    for (var i = 0; i < data.Perfil.Permissoes.length; i++) 
    {
        var permissao = data.Perfil.Permissoes[i];
        if (permissao.Status == "Permitido")
            permissao.Status = 0;
        
        if (permissao.Status == "Negado")
            permissao.Status = 1;
             
        var permissaoInfo = {
            CodigoPermissao: permissao.CodigoPermissao,
            NomePermissao: permissao.PermissaoInfo.NomePermissao,
            Status: permissao.Status
        };
        $("#tbPerfilPermissaoLista").addRowData(data.Perfil.Permissoes[i].CodigoPermissao, permissaoInfo, "last", "after");
    }
    $("#perfil_edicao").tabs("select",0); //1477
    $("#perfil_edicao").dialog( //1477
        {title: 
            "PERFIL [" 
            + $("#perfil_detalhe #codigoPerfil").val() 
            + " - "
            + $("#perfil_detalhe #nomePerfil").val() 
            + "] - Editar"});

    $("#perfil_edicao").dialog("open");
}

function salvarPerfil() {
    var perfil = new Object();
    perfil.CodigoPerfil = $("#perfil_detalhe #codigoPerfil").val();
    perfil.NomePerfil = $("#perfil_detalhe #nomePerfil").val();

    perfil.Permissoes = new Array();
    var permissoesIds = $("#tbPerfilPermissaoLista").getDataIDs();
    for (var i = 0; i < permissoesIds.length; i++) {
        perfil.Permissoes[i] = new Object();
        perfil.Permissoes[i].CodigoPermissao = permissoesIds[i];
        perfil.Permissoes[i].Status = $("#tbPerfilPermissaoLista").getCell(permissoesIds[i], "Status");
    }

    executarServico(
        "SalvarPerfilRequest",
        {
            Perfil: perfil,
            Novo: bNovoPerfil
        },
        "salvarPerfilCallback");
}

function salvarPerfilCallback(data) {
    atualizarLista();
}

function adicionarPermissao() {
    if (!permissoesCarregadas) {
        carregarPermissoes();
        permissoesCarregadas = true;
    }
    $("#permissao_lista").dialog("open");
}

function carregarPermissoes() {
    executarServico("ListarPermissoesRequest", {}, "carregarPermissoesCallback");
}

function carregarPermissoesCallback(data) {
    $("#tbPermissaoLista").clearGridData();
    for (var i = 0; i < data.Permissoes.length; i++)
        $("#tbPermissaoLista").addRowData(data.Permissoes[i].PermissaoInfo.CodigoPermissao, data.Permissoes[i].PermissaoInfo, "last", "after");
}

function removerPerfil(codigoPerfil, callback) {

    if (codigoPerfil) {

        // Salva objeto com o item removido
        executarServico(
                    "RemoverPerfilRequest",
                    { CodigoPerfil: codigoPerfil },
                    function(data) {
                        atualizarLista();
                        // Chama o callback
                        if (callback)
                            callback();
                    });
    }
    else {

        // Pede para selecionar uma linha
        popup({
            titulo: "Atenção",
            icone: "atencao",
            mensagem: "Favor selecionar uma linha!",
            callbackOK: function() { }
        });
    }
}

