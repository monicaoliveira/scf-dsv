﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using System.Text;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class relatorio_recebimento : System.Web.UI.Page
    {
        ListarRelatorioRecebimentoResponse resposta = new ListarRelatorioRecebimentoResponse();
        
        public Decimal TotalQtdeRecebimento { get; set; }
        public Decimal TotalValorRecebimento{ get; set; }
        public Decimal TotalValorTaxaRecebimento { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            RecebimentosRepeater.ItemDataBound += new RepeaterItemEventHandler(RecebimentosRepeater_ItemDataBound);
            DateTime? dataRepasseInicio = this.Request["FiltroDataRepasseInicio"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["FiltroDataRepasseInicio"]) : null;
            DateTime? dataRepasseFim = this.Request["FiltroDataRepasseFim"] != null ? (Nullable<DateTime>)Convert.ToDateTime(this.Request["FiltroDataRepasseFim"]) : null;

            resposta =
                    Mensageria.Processar<ListarRelatorioRecebimentoResponse>(
                        new ListarRelatorioRecebimentoRequest()
                        {
                            FiltroGrupo = this.Request["FiltroGrupo"] != "?" ? this.Request["FiltroGrupo"] : null,
                            FiltroReferencia = this.Request["FiltroReferencia"] != "?" ? this.Request["FiltroReferencia"] : null,
                            FiltroMeioPagamento = this.Request["FiltroMeioPagamento"] != "?" ? this.Request["FiltroMeioPagamento"] : null,
                            FiltroDataRepasseInicio = dataRepasseInicio,
                            FiltroDataRepasseFim = dataRepasseFim,
                            TipoArquivo = this.Request["TipoArquivo"] != null ? this.Request["TipoArquivo"].ToString() : null 
                        });

            this.lblFiltroGrupo.Text = this.Request["NomeGrupo"] != null ? this.Request["NomeGrupo"] : "Todos";
            this.lblFiltroReferencia.Text = this.Request["DescricaoReferencia"] != null ? this.Request["DescricaoReferencia"] : "Todas";
            this.lblFiltroMeioPagamento.Text = this.Request["DescricaoMeioPagamento"] != null ? this.Request["DescricaoMeioPagamento"] : "Todos";
            this.lblFiltroDataInicio.Text = dataRepasseInicio != null ? dataRepasseInicio.Value.ToString("dd/MM/yyyy") : "Qualquer";
            this.lblFiltroDataFim.Text = dataRepasseFim != null ? dataRepasseFim.Value.ToString("dd/MM/yyyy") : "Qualquer";
            this.lblDataHora.Text = String.Format("{0:dd/MM/yyyy - HH:mm}", DateTime.Now);

            this.lblTitulo.Text = "";
            if (this.lblFiltroMeioPagamento.Text.ToUpper() == " DINHEIRO")
                this.lblTitulo.Text = " - MECIR";

            Type cstype = this.GetType();
            ClientScriptManager cs = Page.ClientScript;
            if (resposta.Resultado.Count == 0)
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Sem dados para imprimir pelo filtro informado.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Sem dados para imprimir pelo filtro informado.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }
            else if (resposta.Resultado.Count > 1000 && (this.Request["TipoArquivo"] != "PDF"))
            {
                if (!cs.IsStartupScriptRegistered(cstype, "PopupScript"))
                {
                    this.lblMensagem.Text = ("Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.");
                    this.trMensagem.Visible = true;
                    String cstext = "alert('Quantidade de linhas deste relatório impede a impressão por HTML, favor gerar em excel ou PDF.');";
                    cs.RegisterStartupScript(cstype, "PopupScript", cstext, true);
                    return;
                }
            }

            RecebimentosRepeater.DataSource = resposta.Resultado;
            RecebimentosRepeater.DataBind();
        }

        void RecebimentosRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label lbl;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            { 
                RelatorioRecebimentoInfo dados = (RelatorioRecebimentoInfo)e.Item.DataItem;
                TotalQtdeRecebimento += dados.QtdeRecebimento;
                TotalValorRecebimento += dados.ValorRecebimento;
                TotalValorTaxaRecebimento += dados.ValorTaxaRecebimento;
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                lbl = e.Item.FindControl("lblTotalQtdeRecebimento") as Label;
                if (lbl != null)
                    lbl.Text = TotalQtdeRecebimento.ToString("N0");

                lbl = e.Item.FindControl("lblTotalValorRecebimento") as Label;
                if (lbl != null)
                    lbl.Text = TotalValorRecebimento.ToString("N2");

                lbl = e.Item.FindControl("lblTotalValorTaxaRecebimento") as Label;
                if (lbl != null)
                    lbl.Text = TotalValorTaxaRecebimento.ToString("N2");
            }
        }
        public string FormataTaxa(string valor, string tipo){
            if (tipo == "%")
                return Convert.ToDecimal(valor).ToString("N2") + tipo;
            else
                return tipo + Convert.ToDecimal(valor).ToString("N2");
        }
    }
}
