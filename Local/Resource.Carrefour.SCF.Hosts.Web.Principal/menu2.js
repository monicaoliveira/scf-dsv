﻿/// <reference path="../../lib/jquery/dev/jquery-1.4.1-vsdoc.js" />
/// <reference path="servico.aspx.js" />

$(document).ready(function()
{
    $("ul.menu li a").hover(function()
    {
        $(".submenu").addClass("sub_escondido");
        if ($(this).attr("href") && $(this).attr("href").charAt(0) == "#")
        {
            $($(this).attr("href")).removeClass("sub_escondido");
        }
        else
        {
            $("#sub_default").removeClass("sub_escondido");
        }
    });

});