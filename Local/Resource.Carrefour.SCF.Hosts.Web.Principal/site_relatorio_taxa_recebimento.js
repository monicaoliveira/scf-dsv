﻿$(document).ready(function() {

    // Cria namespace 
    if (!$.taxa)
        $.taxa = {};

    // Funcoes do namespace 
    $.taxa.relatorio = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Insere o subtítulo
            $("#subtitulo").html("Relatórios > Recebimento");

            // Estilo dos botões
            $("#site_relatorio_taxa_recebimento button").button();

            // Inicializa forms
            $("#site_relatorio_taxa_recebimento").Forms("initializeFields");

            var hoje = new Date();
            inicioMes = new Date(hoje.getFullYear(), hoje.getMonth(), 1);

            // Inicializando com 1º dia do mes e data atual
            $('#txtDataRepasseInicio').datepicker('setDate', inicioMes);
            $('#txtDataRepasseFim').datepicker('setDate', hoje);

            //-------------------------------------------------------------------
            // Preenche combo de Grupo
            //-------------------------------------------------------------------
            $.taxa.relatorio.carregarGrupo(function(callback) {
                //-------------------------------------------------------------------
                // Preenche combo de meio de pagamento
                //-------------------------------------------------------------------
                $.taxa.relatorio.carregarMeioPagamento(function(callback) {
                    //-------------------------------------------------------------------
                    // Preenche combo de referencia
                    //-------------------------------------------------------------------
                    $.taxa.relatorio.carregarReferencia(function(callback) {
                        // Chama callback
                        if (callback)
                            callback();
                    });
                });
            });

            // BOTÃO Visualizar
            $("#cmdVisualizar").click(function() {
                $.taxa.relatorio.recuperarFiltros("link", function(request) {
                    if (request)
                        $.taxa.relatorio.abrirRelatorio(request)
                });
            });

            // BOTÃO Excel 
            $("#cmdExcel").click(function() {
                $.taxa.relatorio.recuperarFiltros(null, function(request) {
                    if (request) {
                        request.Request.TipoRelatorio = "Excel";
                        $.taxa.relatorio.criarProcesso(request);
                    }
                });
            });

            // BOTÃO PDF
            $("#cmdPDF").click(function() {
                $.taxa.relatorio.recuperarFiltros("link", function(request) {
                    if (request) {
                        request.Request.TipoRelatorio = "PDF";
                        $.taxa.relatorio.criarProcesso(request);
                    }
                });
            });
        },

        abrirRelatorio: function(request) {
            var link = "relatorio_recebimento.aspx";
            link += request.Link;
            link = link + "&TipoArquivo=HTML";

            var url = window.location.href
            var arr = url.split("/");
            var result = arr[0] + "//" + arr[2]

            if (arr[3] == "scf") {
                arr[3] = "scf_html";
                result = result + "/" + arr[3];
            } else if (arr[3] == "scf_projetos") {
                arr[3] = "scf_html2";
                result = result + "/" + arr[3];
            }

            link = result + "/" + link;
            window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');
        },

        criarProcesso: function(request) {
            if (request.Request.TipoRelatorio == "Excel")
                executarServico(
                    "GerarRelatorioRecebimentoExcelRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Relatório Excel",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
                            callbackOK: function() { }
                        });
                    });
            else {
                var link = "relatorio_recebimento.aspx";
                link += request.Link;
                link = link + "&TipoArquivo=PDF";

                var url = window.location.href
                var arr = url.split("/");
                var result = arr[0] + "//" + arr[2]

                if (arr[3] == "scf") {
                    arr[3] = "scf_relatorios";
                    result = result + "/" + arr[3];
                }

                link = result + "/" + link;

                request.Request.Link = link;
                request.Request.TipoArquivo = "PDF";
                request.Link = link;
                request.TipoArquivo = "PDF";
                executarServico(
                    "GerarRelatorioRecebimentoPDFRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Relatório PDF",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
                            callbackOK: function() { }
                        });
                    });
            }
        },

        recuperarFiltros: function(tipoRetorno, callback) {
            var parts ;
            var data;
            var link = "";
            request = {};

            request.Request = {};
            request.Request.NomeGrupo = "Todos";
            request.Request.DescricaoReferencia = "Todas";
            request.Request.DescricaoMeioPagamento = "Todos";

            if ($("#cmbGrupo").find('option:selected').val() != "?") {
                link = link + "FiltroGrupo=" + $("#cmbGrupo").val() + "&" + "NomeGrupo=" + $("#cmbGrupo option:selected").text() + "&";

                request.Request.FiltroGrupo = $("#cmbGrupo").val();
                request.Request.NomeGrupo = $("#cmbGrupo option:selected").text();
            }
            if ($("#cmbReferencia").find('option:selected').val() != "?") {
                link = link + "FiltroReferencia=" + $("#cmbReferencia").val() + "&" + "DescricaoReferencia=" + $("#cmbReferencia option:selected").text() + "&";

                request.Request.FiltroReferencia = $("#cmbReferencia").val();
                request.Request.DescricaoReferencia = $("#cmbReferencia option:selected").text();
            }
            if ($("#cmbMeioPagamento").find('option:selected').val() != "?") {
                link = link + "FiltroMeioPagamento=" + $("#cmbMeioPagamento").val() + "&" + "DescricaoMeioPagamento=" + $("#cmbMeioPagamento option:selected").text() + "&";

                request.Request.FiltroMeioPagamento = $("#cmbMeioPagamento").val();
                request.Request.DescricaoMeioPagamento = $("#cmbMeioPagamento option:selected").text();
            }
            if ($("#txtDataRepasseInicio").val() != "") {
                parts = $("#txtDataRepasseInicio").val().split('/');
                data = parts[2] + "-" + parts[1] + "-" + parts[0];
                
                link = link + "FiltroDataRepasseInicio=" + data + "&";

                request.Request.FiltroDataRepasseInicio = data;
            }
            if ($("#txtDataRepasseFim").val() != "") {
                parts = $("#txtDataRepasseFim").val().split('/');
                data = parts[2] + "-" + parts[1] + "-" + parts[0];

                link = link + "FiltroDataRepasseFim=" + data + "&";

                request.Request.FiltroDataRepasseFim = data;
            }

            if (link != "") {
                link = "?" + link.substr(0, link.length - 1);
            }

            if (tipoRetorno)
                request.Link = link;

            var parts = $("#txtDataRepasseInicio").val().split('/');
            var dataInicio = new Date(parts[2], parts[1] - 1, parts[0]);

            parts = $("#txtDataRepasseFim").val().split('/');
            var dataFim = new Date(parts[2], parts[1] - 1, parts[0]);

            if (dataInicio != "" && dataFim != "" && dataInicio > dataFim)
                popup({
                    titulo: "Relatório Taxa de Recebimento",
                    icone: "erro",
                    mensagem: "Data Agendamento Até deve ser maior ou igual à Data Agendamento de!",
                    callbackOK: function() {
                        if (callback)
                            callback(null);
                    }
                });
            else
                if (callback) {
                callback(request);
            }

        },
        //=================================================================================
        // carregarGrupo: carrega combo de Grupo
        //=================================================================================
        carregarGrupo: function(callback) {

            executarServico(
                "ListarEmpresaGrupoRequest",
                {},
                function(data) {
                    $("#cmbGrupo").children().each(
                        function(index) {
                            $(this).remove();
                        }
                    );

                    elG = $("#cmbGrupo");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' Todos'
                    });
                    opt.appendTo(elG);

                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].CodigoEmpresaGrupo,
                            text: ' ' + data.Resultado[i].NomeEmpresaGrupo
                        });
                        opt.appendTo(elG);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },
        //=================================================================================
        // carregarMeioPagamento: carrega combo de meio de pagamento
        //=================================================================================
        carregarMeioPagamento: function(callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    CodigoLista: 9
                },
                function(data) {
                    $("#cmbMeioPagamento").children().each(
                        function(index) {
                            $(this).remove();
                        }
                    );

                    elMP = $("#cmbMeioPagamento");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' Todos'
                    });
                    opt.appendTo(elMP);

                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elMP);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //=================================================================================
        // carregarReferencia: carrega combo de Referencia
        //=================================================================================
        carregarReferencia: function(callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    NomeLista: "Referencia"
                },
                function(data) {
                    $("#cmbReferencia").children().each(
                        function(index) {
                            $(this).remove();
                        }
                    );

                    elRef = $("#cmbReferencia");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' Todas'
                    });
                    opt.appendTo(elRef);

                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elRef);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        }
    };

    $.taxa.relatorio.load();
});