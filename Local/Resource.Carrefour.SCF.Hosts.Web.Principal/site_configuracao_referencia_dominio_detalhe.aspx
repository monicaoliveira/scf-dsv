﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="site_configuracao_referencia_dominio_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_configuracao_referencia_dominio_detalhe.js" type="text/javascript"></script>
    <title>SCF - Cadastro de Referências de Domínio</title>
<%--1309
    <script language="javascript" type="text/javascript">
       function cmbNomeReferencia_onclick() {}
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="site_configuracao_referencia_dominio_detalhe" style="margin: 15px;">
        <table>
            <tr>
                <td style="width:120px;">
                    <h4>Dominio Principal</h4>
                </td>
                <td>
                    <select id="cmbSituacao" class="forms[property[CodigoListaItemSituacao];required]">
                        <option value="?">(selecione)</option>
                    </select>
                </td>
<%--1309
             </tr>
            <tr>
--%>            
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td><h4>Domínio</h4></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="tbListaItem" />
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td style="width:120px;">
                    <h4>Link Dominio</h4>
                </td>
                <td>
                    <select id="cmbNomeReferencia" class="forms[property[NomeReferencia];required]" <!--onclick="return cmbNomeReferencia_onclick()"-->>
                        <option value="?">(selecione)</option>
                    </select>
                </td>
<%--1309
             </tr>
            <tr>
--%>            

                <td colspan="2">&nbsp;</td>
            </tr>
             <tr>
                    <td><h4>Valor Link</h4></td>
             </tr>
             <tr>                
                <td colspan="2">
                    <div id="dvTbMeioPagamento">
                        <table id="tbValorReferenciaMeioPagamento" />
                    </div>
                    <div id="dvTbValorReferencia">
                        <table id="tbValorReferenciaProduto" />
                    </div>
                    <div id="dvTbTipoAjuste">
                        <table id="tbTipoAjuste" />
                    </div>
                    <%--// ClockWork - Resource - Ago/16 - Margarida Vitolo--%>
                    <div id="dvTbReferencia">
                        <table id="tbReferencia" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>