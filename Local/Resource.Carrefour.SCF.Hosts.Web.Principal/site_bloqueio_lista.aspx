﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_bloqueio_lista.js"></script>
    <script src="site_bloqueio_detalhe.js" type="text/javascript"></script>
    <script src="site_arquivo_item_lista.js" type="text/javascript"></script>
    <script src="site_critica_lista.js" type="text/javascript"></script>
    <script src="site_critica_detalhe.js" type="text/javascript"></script>
    <link href="site_bloqueio_detalhe.css" rel="stylesheet" type="text/css" />
    <link href="site_bloqueio_lista.css" rel="stylesheet" type="text/css" />
    <title>SCF - Lista de Bloqueios</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">    

    <div id="site_bloqueio_lista">

        <div id="filtro" class="filtro">
            <table>
                <tr>
                    <td>
                        <label>Código do processo:</label>
                    </td>
                    <td>
                        <input size=12 type="text" id="txtFiltroCodigoProcesso" class="forms[property[FiltroCodigoProcesso]]" />
                    </td>
                    <td>
                        <label>Status:</label>
                    </td>
                    <td>
                        <select id="cmbFiltroStatusArquivoItem" class="forms[property[FiltroStatusArquivoItem]]">
                        <option value="?">(selecione)</option>
                        </select>
                    </td>
                    <%-- 1308 --%>
                    <td>
                        <label>Data Inclusão</label>
                    </td>
                    <td>
                        <label>De:</label>
                    </td>
                    <td>
                        <input size=8 type="text" id="cmbFiltroDataInicial" class="forms[property[FiltroDataInclusaoInicial];dataType[date];maskType[date]]"/>
                    </td>
                    <td>
                        <label>Até:</label>
                    </td>
                    <td>
                        <input size=8  type="text" id="cmbFiltroDataFinal" class="forms[property[FiltroDataInclusaoFinal];dataType[date];maskType[date]]"/>
                    </td>
                    <%-- 1308 --%>
                    <td >
                        <button id="cmdFiltrar">Filtrar</button>
                    </td>
                </tr>
            </table>
        </div>

        <div id="lista" class="lista">
            <table id="tbBloqueio"></table>
            <div class="botoes">
            <button id="cmdAtualizarLista">Atualizar lista</button>
            <button id="cmdBloquearMarcados">Bloquear marcados</button>
            <button id="cmdDesbloquearMarcados">Desboquear marcados</button>
            <button id="cmdDetalharBloqueio">Detalhar</button>
            </div>
        </div>

    </div>
    
</asp:Content>
