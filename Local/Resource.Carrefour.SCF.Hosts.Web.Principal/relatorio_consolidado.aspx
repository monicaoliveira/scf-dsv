﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorio_consolidado.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.relatorio_consolidado" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title>Relatório de Consolidado Bancários</title>
    <link href="relatorio_consolidado.css" rel="stylesheet" type="text/css" />
</head>

<body style="font-family: Arial,Times New Roman; font-size: 12px">
    <form id="frm" runat="server">
        <header><table style="border: solid 1px #4682B4; width: 100%;">
            <tr>
                <td colspan="8">
                    <h1 style="font-size: 14px; font-weight: bold">RELATÓRIO DE
                        <asp:Label runat="server" ID="lblTipoRelatorio"></asp:Label>
                        POR PROCESSAMENTO - Consolidado
                        <asp:Label runat="server" ID="lblSubTotalFavorecido"></asp:Label></h1>
                    <h4>
                        <asp:Label runat="server" ID="lblLayout"></asp:Label></h4>
                </td>
                <td colspan="4" align="right">
                    <label style="font-weight: bold; font-size: 10px;">Data/Hora: </label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblDataHora"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="12">
                    <label style="font-weight: bold; font-size: 10px;">Filtros:</label>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Favorecido:</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroFavorecido"></asp:Label>
                </td>
                <td colspan="3" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Estabelecimento:</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroEstabelecimento"></asp:Label>
                </td>
                <td colspan="2" style="max-width: 15%">
                    <asp:Label Style="font-weight: bold; font-size: 10px;" runat="server" ID="lblFiltroPMP"></asp:Label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroProdutoMeioPagamento"></asp:Label>
                </td>
                <td colspan="2" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Tipo Registro:</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroTipoRegistro"></asp:Label>
                </td>
                <td colspan="2" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Financeiro/Contábil:</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroFinanceiroContabil"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Qtde:</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroQtde"></asp:Label>
                </td>
                <td colspan="3" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Status envio CCI:</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroStatusCCI"></asp:Label>
                </td>
                <td colspan="2" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Data Agendamento:</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroDataAgendamentoDe"></asp:Label>
                    -
                <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroDataAgendamentoAte"></asp:Label>
                </td>
                <td colspan="4" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Data Processamento:</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroDataProcessamentoInicio"></asp:Label>
                    -
                <asp:Label Style="font-size: 10px;" runat="server" ID="lblFiltroDataProcessamentoFim"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <label style="font-weight: bold; font-size: 10px;">Consolidar Referência: </label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblConsolidarReferencia"></asp:Label>
                </td>
                <td colspan="9" style="max-width: 15%">
                    <label style="font-weight: bold; font-size: 10px;">Referência</label>
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblReferencia"></asp:Label>
                </td>
            </tr>
        </table></header>
        <table class="tbValores" width="100%" style="border: solid 1px #4682B4;">
            <tr runat="server" id="trMensagem" visible="false">
                <td colspan="12" style="border-top: solid 3px #4682B4;">
                    <asp:Label Style="font-size: 10px;" runat="server" ID="lblMensagem"></asp:Label></td>
            </tr>
        <!-- Repeater Favorecido -->
        <asp:Repeater runat="server" ID="FavorecidoRepeater">
            <ItemTemplate>
                    <tr>
                        <td colspan="12" style="height: 5px; border-top: solid 2px #4682B4;"></td>
                    </tr>

                    <tr id="divQuebraFavorecido" runat="server" visible="false">
                        <td colspan="12" style="padding-bottom: 5px; border-bottom: solid 1px #4682B4;">
                            <label style="font-size: 11px; font-weight: bold;">Favorecido: </label>
                            <label style="font-size: 10px;"><%# Eval("AgrupamentoFavorecido.DescricaoFavorecido")%></label>
                        </td>
                    </tr>
                    <tr id="divQuebraReferencia" runat="server" visible="false">
                        <td colspan="12" style="padding-bottom: 5px; border-bottom: solid 1px #4682B4;">
                            <label style="font-size: 11px; font-weight: bold;">Referencia: </label>
                            <label style="font-size: 10px;"><%# Eval("AgrupamentoFavorecido.DescricaoReferencia")%></label>
                        </td>
                    </tr>
                    <tr style="font-size: 10px;">
                        <td id="divVendas1" runat="server" visible="false" style="width: 10%">
                            <label style="font-weight: bold; font-size: 10px;">Produto</label>
                        </td>
                        <td colspan="1" id="divVendas2" runat="server" visible="false" align="right">
                            <label style="font-weight: bold; font-size: 10px;">Vlr. Bruto CV</label>
                        </td>
                        <td colspan="1" id="divVendas3" runat="server" visible="false" align="right">
                            <label style="font-weight: bold; font-size: 10px;">Vlr. Bruto AV</label>
                        </td>
                        <td colspan="1" id="divVendas4" runat="server" visible="false" align="right">
                            <label style="font-weight: bold; font-size: 10px;">Vlr. Bruto AJ</label>
                        </td>
                        <td colspan="1" id="divVendas5" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Vlr. Comissão CV</label>
                        </td>
                        <td id="divVendas6" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Vlr. Comissão AV</label>
                        </td>
                        <td id="divVendas8" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Vlr. Comissão AJ</label>
                        </td>
                        <td colspan="1" id="divVendas7" runat="server" visible="false" align="right">
                            <label style="font-weight: bold; font-size: 10px;">Total Líquido</label>
                        </td>
                        <td id="divVendasQtde" runat="server" visible="false" align="right">
                            <label style="font-weight: bold; font-size: 10px;">Qtde CV</label>
                        </td>
                        <td id="divVendasQtde1" runat="server" visible="false" align="right">
                            <label style="font-weight: bold; font-size: 10px;">Qtde AV</label>
                        </td>
                        <td id="divVendasQtde2" runat="server" visible="false" align="right">
                            <label style="font-weight: bold; font-size: 10px;">Qtde AJ</label>
                        </td>
                        <td id="divRecebimento" runat="server" visible="false" colspan="4">
                            <label id="lblMeioPagamentoHeader" runat="server" style="font-weight: bold; font-size: 10px;" visible="false">Meio Pagamento</label>
                        </td>
                        <td id="divRecebimento1" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Valor CP</label>
                        </td>
                        <td id="divRecebimento2" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Valor AP</label>
                        </td>
                        <td id="divRecebimento6" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Valor AJ</label>
                        </td>
                        <td id="divRecebimento3" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Valor Total</label>
                        </td>
                        <td id="divRecebimento4" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Qtde MP(CP)</label>
                        </td>
                        <td id="divRecebimento5" runat="server" visible="false" align="right">
                            <label style="font-weight: bold; font-size: 10px;">Qtde MP(AP)</label>
                        </td>
                        <td id="divRecebimento7" runat="server" visible="false" align="right" >
                            <label style="font-weight: bold; font-size: 10px;">Qtde MP(AJ)</label>
                        </td>
                    </tr>
                    <!-- Repeater Data -->
                    <asp:Repeater runat="server" ID="DataRepeater">
                        <ItemTemplate>

                            <tr id="divDataProcessamento" runat="server" visible="false">
                                <td colspan="12">
                                    <label style="font-weight: bold; font-size: 10px;">Data Processamento: </label>
                                    <label style="font-size: 10px;"><%# Eval("AgrupamentoData.DescricaoData")%> </label>
                                </td>
                            </tr>

                            <tr style="font-size: 10px;">
                                <td id="divVendasData" runat="server" visible="false" style="width: 10%">
                                    <label style="font-weight: bold; font-size: 10px;">Produto</label>
                                </td>
                                <td id="divVendasData2" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Vlr. Bruto CV</label>
                                </td>
                                <td id="divVendasData3" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Vlr. Bruto AV</label>
                                </td>
                                <td id="divVendasData4" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Vlr. Bruto AJ</label>
                                </td>
                                <td id="divVendasData5" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Vlr. Comissão CV</label>
                                </td>
                                <td id="divVendasData6" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Vlr. Comissão AV</label>
                                </td>
                                <td id="divVendasData8" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Vlr. Comissão AJ</label>
                                </td>
                                <td id="divVendasData7" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Total Líquido</label>
                                </td>
                                <td id="divVendasQtdeData" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Qtde CV</label>
                                </td>
                                <td id="divVendasQtdeData1" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Qtde AV</label>
                                </td>
                                <td id="divVendasQtdeData2" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Qtde AJ</label>
                                </td>
                                <td id="divRecebimentoData" runat="server" visible="false" colspan="4">
                                    <label style="font-weight: bold; font-size: 10px;">Meio Pagamento</label>
                                </td>
                                <td id="divRecebimentoData1" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Valor CP</label>
                                </td>
                                <td id="divRecebimentoData2" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Valor AP</label>
                                </td>
                                <td id="divRecebimentoData6" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Valor AJ</label>
                                </td>
                                <td id="divRecebimentoData3" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Valor Total</label>
                                </td>
                                <td id="divRecebimentoData4" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Qtde MP(CP)</label>
                                </td>
                                <td id="divRecebimentoData5" runat="server" visible="false" align="right">
                                    <label style="font-weight: bold; font-size: 10px;">Qtde MP(AP)</label>
                                </td>
                                <td id="divRecebimentoData7" runat="server" visible="false" align="right" >
                                    <label style="font-weight: bold; font-size: 10px;">Qtde MP(AJ)</label>
                                </td>
                            </tr>
                            <!-- Repeater Produto -->
                            <asp:Repeater runat="server" ID="ProdutoRepeater">
                                <ItemTemplate>
                                    <tr style="font-size: 10px;">
                                        <td id="divVendasEval" runat="server" visible="false" style="width: 10%">
                                            <%# Eval("NomeProduto")%>
                                        </td>
                                        <td id="divVendasEval2" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorBrutoCV")).ToString("N2")%>
                                        </td>
                                        <td id="divVendasEval3" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorBrutoAV")).ToString("N2")%>
                                        </td>
                                        <td colspan="1" id="divVendasEval4" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorBrutoAJ")).ToString("N2")%>
                                        </td>
                                        <td id="divVendasEval5" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorComissaoCV")).ToString("N2")%>
                                        </td>
                                        <td id="divVendasEval6" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorComissaoAV")).ToString("N2")%>
                                        </td>
                                        <td id="divVendasEval11" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorComissaoAJ")).ToString("N2")%>
                                        </td>
                                        <td id="divVendasEval7" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("TotalLiquido")).ToString("N2")%>
                                        </td>
                                        <td id="divVendasEval8" runat="server" visible="false" align="right" >
                                            <%# Eval("QtdeCV")%>
                                        </td>
                                        <td id="divVendasEval9" runat="server" visible="false" align="right" >
                                            <%# Eval("QtdeAV")%>
                                        </td>
                                        <td id="divVendasEval10" runat="server" visible="false" align="right" >
                                            <%# Eval("QtdeAJ")%>
                                        </td>
                                        <td id="divRecebimentoEval" runat="server" visible="false" colspan="4">
                                            <%# Eval("MeioPagamento")%>
                                        </td>
                                        <td id="divRecebimentoEval2" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorCP")).ToString("N2")%>
                                        </td>
                                        <td id="divRecebimentoEval3" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorAP")).ToString("N2")%>
                                        </td>
                                        <td id="divRecebimentoEval7" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorAJ")).ToString("N2")%>
                                        </td>
                                        <td id="divRecebimentoEval4" runat="server" visible="false" align="right" >
                                            <%# Convert.ToDecimal(Eval("ValorTotal")).ToString("N2")%>
                                        </td>
                                        <td id="divRecebimentoEval5" runat="server" visible="false" align="right" >
                                            <%# Eval("QtdeCP")%>
                                        </td>
                                        <td id="divRecebimentoEval6" runat="server" visible="false" align="right">
                                            <%# Eval("QtdeAP")%>
                                        </td>
                                        <td id="divRecebimentoEval8" runat="server" visible="false" align="right" >
                                            <%# Eval("QtdeAJ")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <!-- Fim Repeater Produto-->
                            <tr id="divVendasEvalTotalDia" runat="server" visible="false" style="font-size: 10px;">
                                <td style="width: 10%">
                                    <label style="font-weight: bold; font-size: 10px;">Total Dia: </label>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalDiaCV")).ToString("N2")%>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalDiaAV")).ToString("N2")%>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalDiaAJ")).ToString("N2")%>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalComissaoDiaCV")).ToString("N2")%>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalComissaoDiaAV")).ToString("N2")%>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalComissaoDiaAJ")).ToString("N2")%>
                                </td>

                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalDiaLiquido")).ToString("N2")%>
                                </td>
                                <td id="divVendasEvalQtdeTotalDia" runat="server" visible="false" align="right" style="font-weight: bold; ">
                                    <%# Eval("AgrupamentoData.QtdeTotalDiaCV")%>
                                </td>
                                <td id="divVendasEvalQtdeTotalDia1" runat="server" visible="false" align="right" style="font-weight: bold; ">
                                    <%# Eval("AgrupamentoData.QtdeTotalDiaAV")%>
                                </td>
                                <td id="divVendasEvalQtdeTotalDia2" runat="server" visible="false" align="right" style="font-weight: bold; ">
                                    <%# Eval("AgrupamentoData.QtdeTotalDiaAJ")%>
                                </td>
                            </tr>
                            <tr id="divVendasEvalTotalDia1" runat="server" visible="false">
                                <td colspan="12" style="height: 5px;"></td>
                            </tr>

                            <tr id="divRecebimentoEvalTotalDia" runat="server" visible="false" style="font-size: 10px;">
                                <td colspan="4">
                                    <label style="font-weight: bold; font-size: 10px;">Total Dia: </label>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalDiaCP")).ToString("N2")%>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalDiaAP")).ToString("N2")%>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalDiaAJ")).ToString("N2")%>
                                </td>
                                <td align="right" style="font-weight: bold; ">
                                    <%# Convert.ToDecimal(Eval("AgrupamentoData.ValorTotalDiaTotal")).ToString("N2")%>
                                </td>
                                <td id="divRecebimentoEvalQtdeTotalDia" runat="server" visible="false" align="right" style="font-weight: bold; ">
                                    <%# Eval("AgrupamentoData.QtdeTotalDiaCP")%>
                                </td>
                                <td id="divRecebimentoEvalQtdeTotalDia1" runat="server" visible="false" align="right" style="font-weight: bold; " >
                                    <%# Eval("AgrupamentoData.QtdeTotalDiaAP")%>
                                </td>
                                <td id="divRecebimentoEvalQtdeTotalDia2" runat="server" visible="false" align="right" style="font-weight: bold; " >
                                    <%# Eval("AgrupamentoData.QtdeTotalDiaAJ")%>
                                </td>
                            </tr>

                            <tr id="divRecebimentoEvalTotalDia1" runat="server" visible="false">
                                <td colspan="12" style="border-bottom: solid 5px #4682B4;"></td>
                            </tr>

                        </ItemTemplate>
                    </asp:Repeater>
                    <!-- Fim Repeater Data-->
                    <tr style="font-size: 10px;">
                        <td id="divVendasEvalTotal" runat="server" visible="false" style="padding-bottom: 5px; width: 10%">
                            <label style="font-weight: bold; font-size: 10px;">Total: </label>
                        </td>
                        <td id="divVendasEvalTotal1" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalCV")).ToString("N2")%>
                        </td>
                        <td id="divVendasEvalTotal2" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalAV")).ToString("N2")%>
                        </td>
                        <td id="divVendasEvalTotal3" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalAJ")).ToString("N2")%>
                        </td>
                        <td id="divVendasEvalTotal4" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalComissaoCV")).ToString("N2")%>
                        </td>
                        <td id="divVendasEvalTotal5" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalComissaoAV")).ToString("N2")%>
                        </td>
                        <td id="divVendasEvalTotal10" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalComissaoAJ")).ToString("N2")%>
                        </td>
                        <td id="divVendasEvalTotal6" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalLiquido")).ToString("N2")%>
                        </td>
                        <td id="divVendasEvalTotal7" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Eval("AgrupamentoFavorecido.QtdeTotalCV")%>
                        </td>
                        <td id="divVendasEvalTotal8" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Eval("AgrupamentoFavorecido.QtdeTotalAV")%>
                        </td>
                        <td id="divVendasEvalTotal9" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Eval("AgrupamentoFavorecido.QtdeTotalAJ")%>
                        </td>
                        <td id="divRecebimentoEvalTotal" runat="server" visible="false" style="padding-bottom: 5px; " colspan="4">
                            <label style="font-weight: bold; font-size: 10px;">Total: </label>
                        </td>
                        <td id="divRecebimentoEvalTotal1" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalCP")).ToString("N2")%>
                        </td>
                        <td id="divRecebimentoEvalTotal2" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalAP")).ToString("N2")%>
                        </td>
                        <td id="divRecebimentoEvalTotal6" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalAJ")).ToString("N2")%>
                        </td>
                        <td id="divRecebimentoEvalTotal3" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Convert.ToDecimal(Eval("AgrupamentoFavorecido.ValorTotalTotal")).ToString("N2")%>
                        </td>
                        <td id="divRecebimentoEvalTotal4" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Eval("AgrupamentoFavorecido.QtdeTotalCP")%>
                        </td>
                        <td id="divRecebimentoEvalTotal5" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; ">
                            <%# Eval("AgrupamentoFavorecido.QtdeTotalAP")%>
                        </td>
                        <td id="divRecebimentoEvalTotal7" runat="server" visible="false" align="right" style="padding-bottom: 5px; font-weight: bold; " >
                            <%# Eval("AgrupamentoFavorecido.QtdeTotalAJ")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <!-- Fim Repeater Favorecido-->
            <tr>
                <td colspan="12" style="height: 5px; border-bottom: solid 2px #4682B4"></td>
            </tr>
            <tr id="divVendasTotal" runat="server" visible="false" style="width: 10%; font-size: 10px;font-weight: bold;">
                <td style="width: 10%">
                    <label style="font-weight: bold;">Total Geral: </label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorBrutoCV" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorBrutoAV" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorBrutoAJ" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorComissaoCV" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorComissaoAV" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorComissaoAJ" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalLiquido" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalQtdeCV" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalQtdeAV" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalQtdeAJ" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="divRecebimentoTotal" runat="server" visible="false" style="font-size: 10px;">
                <td align="left" colspan="4">
                    <label style="font-weight: bold;">Total Geral: </label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorCP" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorAP" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValorAJ" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalValor" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalQtdeCP" runat="server"></asp:Label>
                </td>
                <td align="right" >
                    <asp:Label ID="lblTotalQtdeAP" runat="server"></asp:Label>
                </td>
                <td align="right"  >
                    <asp:Label ID="lblTotalQtdeAJR" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
