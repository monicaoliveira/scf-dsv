﻿$(document).ready(function() {

    //Global utilizada para fazer o Log de tempo dos relaórios
    myVar = {};

    // Namespace
    if (!$.relatorio)
        $.relatorio = {};

    // Funcoes do namespace 
    $.relatorio = {

        variaveis: {},

        //=================================================================================
        // load: funcoes de inicializacao da tela
        //=================================================================================
        load: function(callback) {

            // Insere o subtítulo
            $("#subtitulo").html("Relatórios > Relatório de Vencimento");

            //------------------------------------------------------------------
            // Estilo dos botões
            //-------------------------------------------------------------------
            $("#site_relatorio_vencimento button").button();

            //-------------------------------------------------------------------
            // Inicializa forms
            //-------------------------------------------------------------------
            $("#site_relatorio_vencimento").Forms("initializeFields");

            //-------------------------------------------------------------------
            // Inicializa Data Posição com dia atual
            //-------------------------------------------------------------------
            $('#txtDataPosicao').datepicker('setDate', '+0');

            // ECOMMERCE - Fernando Bove - 20160107
            // seta o valor default do campo tipo Contábil
            $("#rdoTipoContabil").attr("checked", true);

            //-------------------------------------------------------------------
            // BOTÃO VISUALIZAR HTML
            //-------------------------------------------------------------------
            $("#btnVisualizarHtml").click(function() {
                $.relatorio.gerarRelatorioHtml();
            });

            //-------------------------------------------------------------------
            // BOTÃO GERAR PAGAMENTOS
            //-------------------------------------------------------------------
            $("#btnGerarPagamento").click(function() {
                $.relatorio.gerarPagamentos();
            });

            //-------------------------------------------------------------------
            // BOTÃO RELATORIO DE TESTE
            //-------------------------------------------------------------------
            //            //SCF-1137
            //            $("#btnRelatorioFlat").click(function() {
            //                $.relatorio.gerarRelatorioFlat();
            //            });

            $("#btnGerarRelatorioExcel").click(function() {

                request = {};

                request.Request = {};

                if ($("#cmbEmpresaGrupo").find('option:selected').val() != "?") {
                    request.Request.FiltroEmpresaGrupo = $("#cmbEmpresaGrupo").val();
                }
                if ($("#cmbEmpresaSubGrupo").find('option:selected').val() != "?") {
                    request.Request.FiltroEmpresaSubGrupo = $("#cmbEmpresaSubGrupo").val();
                }
                if ($("#cmbFavorecido").find('option:selected').val() != "?") {
                    request.Request.FiltroFavorecido = $("#cmbFavorecido").val();
                }
                if ($("#cmbProduto").find('option:selected').val() != null) {
                    request.Request.FiltroProduto = $("#cmbProduto").val().toString();
                }
                if ($("#cmbReferencia").find('option:selected').val() != null) {
                    request.Request.FiltroReferencia = $("#cmbReferencia").val().toString();
                }
                if ($("#cmbTipoRegistro").find('option:selected').val() != "?") {
                    request.Request.FiltroTipoRegistro = $("#cmbTipoRegistro").val();
                }
                if ($("#cmbStatusCessao").find('option:selected').val() != "?") {
                    request.Request.FiltroStatusRetornoCessao = $("#cmbStatusCessao").val();
                }

                //ECOMMERCE - Fernando Bove - 20160105: Inicio
                if ($("#rdoTipoContabil").attr("checked")) {
                    request.Request.FiltroFinanceiroContabil = $("#rdoTipoContabil").val();
                }
                if ($("#rdoTipoFinanceiro").attr("checked")) {
                    request.Request.FiltroFinanceiroContabil = $("#rdoTipoFinanceiro").val();
                }
                //ECOMMERCE - Fernando Bove - 20160105: Fim

                //Edgar Oliveira
                request.Request.FiltroRetornarTransacaoPaga = $("#chkRetornaTransacaoPaga").attr("checked");

                // consolidações
                request.Request.FiltroConsolidaAgendamentoVencimento = $("#consolidacao").attr("checked");
                request.Request.FiltroConsolidaMaiorData = $("#consolidacao1").attr("checked");
                request.Request.FiltroConsolidaReferencia = $("#consolidacao2").attr("checked");

                if ($("#txtDataPosicao").val() != "") {
                    request.Request.FiltroDataPosicao = $("#txtDataPosicao").val();
                } else {
                    $('#txtDataPosicao').datepicker('setDate', '+0');
                    request.Request.FiltroDataPosicao = $("#txtDataPosicao").val();
                    //Jira 982 - Marcos Matsuoka - De:request.FiltroDataPosicao Para:request.Request.FiltroDataPosicao
                }
                if ($("#txtPeriodoTransacaoDe").val() != "") {
                    request.Request.FiltroDataInicioTransacao = $("#txtPeriodoTransacaoDe").val();
                }
                if ($("#txtPeriodoTransacaoAte").val() != "") {
                    request.Request.FiltroDataFimTransacao = $("#txtPeriodoTransacaoAte").val();
                }
                if ($("#txtPeriodoVencimentoDe").val() != "") {
                    request.Request.FiltroDataInicioVencimento = $("#txtPeriodoVencimentoDe").val();
                }
                if ($("#txtPeriodoVencimentoAte").val() != "") {
                    request.Request.FiltroDataFimVencimento = $("#txtPeriodoVencimentoAte").val();
                }

                if ($("#txtDataProcessamentoDe").val() != "") {
                    request.Request.FiltroDataInicioMovimento = $("#txtDataProcessamentoDe").val();
                }
                if ($("#txtDataProcessamentoAte").val() != "") {
                    request.Request.FiltroDataFimMovimento = $("#txtDataProcessamentoAte").val();
                }

                if ($("#txtDataAgendamentoDe").val() != "") {
                    request.Request.FiltroDataAgendamentoDe = $("#txtDataAgendamentoDe").val();
                }
                if ($("#txtDataAgendamentoAte").val() != "") {
                    request.Request.FiltroDataAgendamentoAte = $("#txtDataAgendamentoAte").val();
                }

                // SCF1170 - data de retorno de CCI
                if ($("#txtDataRetornoCCIDe").val() != "") {
                    request.Request.FiltrotxtDataRetornoCCIInicio = $("#txtDataRetornoCCIDe").val();
                }
                if ($("#txtDataRetornoCCIFim").val() != "") {
                    request.Request.FiltrotxtDataRetornoCCIFim = $("#txtDataRetornoCCIFim").val();
                }
                request.Request.TipoRelatorio = "Excel";

                executarServico(
                    "GerarRelatorioVencimentoExcelRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Relatório Excel",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório.",
                            callbackOK: function() { }
                        });
                    });
            });

            $("#btnGerarRelatorioPdf").click(function() {

                var request = {};

                request.Request = {};
                request.Request.TipoRelatorio = "Excel";

                var link = "relatorio_vencimento.aspx";
                link += $.relatorio.gerarUrl();
                var url = window.location.href

                var arr = url.split("/");
                var result = arr[0] + "//" + arr[2]

                //Jira SCF 1046 - Marcos Matsuoka
                if (arr[3] == "scf") {
                    arr[3] = "scf_relatorios";
                    result = result + "/" + arr[3];
                }

                var link = link + "&TipoArquivo=PDF"; //Marcos Matsuoka - Jira SCF 1085

                link = result + "/" + link;

                //window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');
                request.Request.Link = link;
                request.Request.TipoArquivo = "PDF";
                request.Link = link;
                request.TipoArquivo = "PDF";

                executarServico(
                    "GerarRelatorioVencimentoPDFRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Relatório PDF",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração do relatório PDF.",
                            callbackOK: function() { }
                        });
                    });
                //window.open("site_relatorio_vencimento.aspx" + parametros + "&acao=pdf", "_self");
            });

            // não permite que os dois checkbox estejam selecionados
            $("#consolidacao").click(function() {
                if ($(this).attr("checked") == true)
                    $("#consolidacao1").attr("checked", false);
            });
            $("#consolidacao1").click(function() {
                if ($(this).attr("checked") == true)
                    $("#consolidacao").attr("checked", false);
            });

            // -------------------------------------------------------------------
            // Evento change do combo de grupo 
            // -------------------------------------------------------------------
            $("#cmbEmpresaGrupo").change(function() {

                // Pega código do grupo, se tiver
                var codigoEmpresaGrupo = $("#cmbEmpresaGrupo").val();
                if (codigoEmpresaGrupo != "?") {
                    // Carrega combo de subgrupos correspondente ao grupo
                    $.relatorio.carregarSubGrupos(codigoEmpresaGrupo);
                } else {
                    // Limpa o combo (exceto o primeiro item)
                    $("#cmbEmpresaSubGrupo").children().each(
                        function(index) {
                            if (index != 0)
                                $(this).remove();
                        }
                    );
                }
            });

            //-------------------------------------------------------------------
            // Preenche combo de planos
            //-------------------------------------------------------------------
            $.relatorio.carregarPlanos(function(callback) {

                //-------------------------------------------------------------------
                // Preenche combo de estabelecimentos
                //-------------------------------------------------------------------
                $.relatorio.carregarEstabelecimentos(function(callback) {

                    //-------------------------------------------------------------------
                    // Preenche combo de produtos
                    //-------------------------------------------------------------------
                    $.relatorio.carregarProdutos(function(callback) {

                        //-------------------------------------------------------------------
                        // Preenche combo de referencia
                        //-------------------------------------------------------------------
                        $.relatorio.carregarReferencias(function(callback) {

                            // Chama callback
                            if (callback)
                                callback();
                        });
                    });
                });
            });

            //$("#cmbProduto").multiselect();
        },

        //=================================================================================
        // carregarSubGrupos: carrega combo de subgrupos de acordo com o grupo 
        //                selecionado, caso tenha
        //=================================================================================
        carregarSubGrupos: function(codigoEmpresaGrupo, callback) {

            // Limpa o combo (exceto o primeiro item)
            $("#cmbEmpresaSubGrupo").children().each(
                function(index) {
                    if (index != 0)
                        $(this).remove();
                }
            );

            // Pede lista de subgrupos caso haja um grupo
            if (codigoEmpresaGrupo) {
                executarServico(
                    "ListarEmpresaSubGrupoRequest",
                    { FiltroCodigoEmpresaGrupo: codigoEmpresaGrupo },
                    function(data) {

                        // Varre lista de subgrupos
                        for (var i = 0; i < data.Resultado.length; i++) {
                            // Adiciona o item no combo
                            $("#cmbEmpresaSubGrupo").append("<option value='" + data.Resultado[i].CodigoEmpresaSubGrupo + "'>"
				                    + data.Resultado[i].NomeEmpresaSubGrupo + "</option>");
                        }

                        // chama callback
                        if (callback)
                            callback();
                    }
                );

            } else {
                // chama callback
                if (callback)
                    callback();
            }
        },

        //=================================================================================
        // carregarPlanos: carrega combo de planos de acordo com o produto 
        //                selecionado, caso tenha
        //=================================================================================
        carregarPlanos: function(callback) {

            // Chama callback
            if (callback)
                callback();
        },

        //=================================================================================
        // carregarProdutos: carrega combo de planos de acordo com o produto 
        //                selecionado, caso tenha
        //=================================================================================
        carregarProdutos: function(callback) {

            // Limpa o combo (exceto o primeiro item)
            $("#cmbProduto").children().each(
                function(index) {
                    if (index != 0)
                        $(this).remove();
                }
            );

            var el = $("#cmbProduto").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: ""
            });

            executarServico(
                "ListarProdutoRequest",
                {},
                function(data) {

                    // Varre lista de subgrupos
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].CodigoProduto,
                            text: ' ' + data.Resultado[i].NomeProduto
                        });
                        opt.appendTo(el);
                    }

                    el.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },


        //=================================================================================
        // carregarReferencias: carrega combo de referencia 
        //=================================================================================
        carregarReferencias: function(callback) {

            // Limpa o combo (exceto o primeiro item)
            $("#cmbReferencia").children().each(
                function(index) {
                    if (index != 0)
                        $(this).remove();
                }
            );

            var el = $("#cmbReferencia").multiselect(
            {
                checkAllText: "Selecionar todos",
                uncheckAllText: "Remover seleção",
                selectedText: "# selecionados",
                noneSelectedText: ""
            });

            executarServico(
                "ListarListaItemRequest",
                { NomeLista: 'Referencia' },
                function(data) {

                    // Varre lista de referencia
                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(el);
                    }

                    el.multiselect('refresh');

                    // chama callback
                    if (callback)
                        callback();
                }
           );
        },


        //=================================================================================
        // carregarEstabelecimentos: carrega combo de estabelecimentos de acordo com
        //                          o grupo e/ou subgrupo selecionados, caso tenha
        //=================================================================================
        carregarEstabelecimentos: function(callback) {

            // Chama callback
            if (callback)
                callback();
        },

        //=================================================================================
        // gerarRelatorioPDF: gera o relatório em PDF de transações conforme filtros
        //=================================================================================
        gerarRelatorioHtml: function(callback) {

            // Verificar quantidade de linhas
            //$.relatorio.receberNumeroDeLinhas(function() {

            var link = "relatorio_vencimento.aspx";
            link += $.relatorio.gerarUrl();

            //Jira SCF 749 - Marcos Matsuoka
            var url = window.location.href;

            var arr = url.split("/");
            var result = arr[0] + "//" + arr[2]

            if (arr[3] == "scf") {
                arr[3] = "scf_html";
                result = result + "/" + arr[3];
            } else if (arr[3] == "scf_projetos") {
                arr[3] = "scf_html2";
                result = result + "/" + arr[3];
            }

            var link = link + "&TipoArquivo=HTML"; //Marcos Matsuoka - Jira SCF 1085

            link = result + "/" + link;
            //

            window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');

            // Chama callback
            if (callback)
                callback();

            //});

        },

        //=================================================================================
        // gerarPagamentos: Chama a procedure do relatorio, informando que precisa gerar os pagamentos
        //=================================================================================
        gerarPagamentos: function(callback) {

            // gera o request, apenas para log
            // para o processamento, o que vale é o link gerado
            request = {};

            request.Request = {};

            if ($("#cmbEmpresaGrupo").find('option:selected').val() != "?") {
                request.Request.FiltroEmpresaGrupo = $("#cmbEmpresaGrupo").val();
            }
            if ($("#cmbEmpresaSubGrupo").find('option:selected').val() != "?") {
                request.Request.FiltroEmpresaSubGrupo = $("#cmbEmpresaSubGrupo").val();
            }
            if ($("#cmbFavorecido").find('option:selected').val() != "?") {
                request.Request.FiltroFavorecido = $("#cmbFavorecido").val();
            }
            if ($("#cmbProduto").find('option:selected').val() != null) {
                request.Request.FiltroProduto = $("#cmbProduto").val().toString();
            }
            if ($("#cmbReferencia").find('option:selected').val() != null) {
                request.Request.FiltroReferencia = $("#cmbReferencia").val().toString();
            }
            if ($("#cmbTipoRegistro").find('option:selected').val() != "?") {
                request.Request.FiltroTipoRegistro = $("#cmbTipoRegistro").val();
            }
            if ($("#cmbStatusCessao").find('option:selected').val() != "?") {
                request.Request.FiltroStatusRetornoCessao = $("#cmbStatusCessao").val();
            }
            if ($("#rdoTipoContabil").attr("checked")) {
                request.Request.FiltroFinanceiroContabil = $("#rdoTipoContabil").val();
            }
            if ($("#rdoTipoFinanceiro").attr("checked")) {
                request.Request.FiltroFinanceiroContabil = $("#rdoTipoFinanceiro").val();
            }
            request.Request.FiltroConsolidaAgendamentoVencimento = $("#consolidacao").attr("checked");
            request.Request.FiltroConsolidaMaiorData = $("#consolidacao1").attr("checked");
            request.Request.FiltroConsolidaReferencia = $("#consolidacao2").attr("checked");

            //Edgar Oliveira
            request.Request.FiltroFiltroRetornarTransacaoPaga = $("#chkRetornaTransacaoPaga").attr("checked");

            if ($("#txtDataPosicao").val() != "") {
                request.Request.FiltroDataPosicao = $("#txtDataPosicao").val();
            } else {
                $('#txtDataPosicao').datepicker('setDate', '+0');
                request.Request.FiltroDataPosicao = $("#txtDataPosicao").val();
            }
            if ($("#txtPeriodoTransacaoDe").val() != "") {
                request.Request.FiltroDataInicioTransacao = $("#txtPeriodoTransacaoDe").val();
            }
            if ($("#txtPeriodoTransacaoAte").val() != "") {
                request.Request.FiltroDataFimTransacao = $("#txtPeriodoTransacaoAte").val();
            }
            if ($("#txtPeriodoVencimentoDe").val() != "") {
                request.Request.FiltroDataInicioVencimento = $("#txtPeriodoVencimentoDe").val();
            }
            if ($("#txtPeriodoVencimentoAte").val() != "") {
                request.Request.FiltroDataFimVencimento = $("#txtPeriodoVencimentoAte").val();
            }

            if ($("#txtDataProcessamentoDe").val() != "") {
                request.Request.FiltroDataInicioMovimento = $("#txtDataProcessamentoDe").val();
            }
            if ($("#txtDataProcessamentoAte").val() != "") {
                request.Request.FiltroDataFimMovimento = $("#txtDataProcessamentoAte").val();
            }

            if ($("#txtDataAgendamentoDe").val() != "") {
                request.Request.FiltroDataAgendamentoDe = $("#txtDataAgendamentoDe").val();
            }
            if ($("#txtDataAgendamentoAte").val() != "") {
                request.Request.FiltroDataAgendamentoAte = $("#txtDataAgendamentoAte").val();
            }
            if ($("#txtDataRetornoCCIDe").val() != "") {
                request.Request.FiltrotxtDataRetornoCCIInicio = $("#txtDataRetornoCCIDe").val();
            }
            if ($("#txtDataRetornoCCIFim").val() != "") {
                request.Request.FiltrotxtDataRetornoCCIFim = $("#txtDataRetornoCCIFim").val();
            }


            var tipoFinanceiro = false;
            var consolidReferencia = false;
            var validado = false;
            // Valida se pode gerar o arquivo com o filtro informado
            if ($("#rdoTipoFinanceiro").attr("checked")) {
                tipoFinanceiro = $("#rdoTipoFinanceiro").val();
            }
            if ($("#consolidacao2").attr("checked")) {
                consolidReferencia = $("#consolidacao2").val();
            }

            if (!tipoFinanceiro) {

                popup({
                    titulo: "Gerar Pagamentos",
                    icone: "erro",
                    mensagem: "Para gerar pagamentos, é preciso selecionar a opção Tipo Financeiro.",
                    callbackOK: function() { }
                });

            }
            else {
                if (!consolidReferencia) {

                    popup({
                        titulo: "Gerar Pagamentos",
                        icone: "erro",
                        mensagem: "Para gerar pagamentos, é preciso selecionar a opção Consolidacao por Referencia.",
                        callbackOK: function() { }
                    });
                }
                else
                    validado = true;


            }



            if (validado) {

                // VERSAO QUE FAZ O PROCESSAMENTO ATRAVÉS DA GERACAO DE UM PROCESSO ASSINCRONO
                //var request = {};
                //request.Request = {};
                request.Request.TipoRelatorio = "Pagamento";

                // gera o link
                var link = "relatorio_vencimento.aspx";
                link += $.relatorio.gerarUrl();
                var url = window.location.href

                var arr = url.split("/");
                var result = arr[0] + "//" + arr[2]

                if (arr[3] == "scf") {
                    arr[3] = "scf_relatorios";
                    result = result + "/" + arr[3];
                }

                var link = link + "&TipoArquivo=Pagamento";
                link = link + "&GerarPagamentos=true"

                link = result + "/" + link;
                request.Request.Link = link;
                request.Request.TipoArquivo = "Pagamento";

                request.Link = link;
                request.TipoArquivo = "Pagamento";

                // Chama a execução do servico
                executarServico(
                    "GerarPagamentobyRelatorioVenctoRequest",
                    request,
                    function(data) {

                        popup({
                            titulo: "Gerar Pagamentos",
                            icone: "sucesso",
                            mensagem: "Processo número " + data.CodigoProcesso + " criado para a geração dos pagamentos.",
                            callbackOK: function() { }
                        });
                    });
            }

        },

        //        //SCF-1137
        //        gerarRelatorioFlat: function(callback) {

        //            var link = "relatorio_vencimento_flat.aspx";

        //            link += $.relatorio.gerarUrl();

        //            // Verificar quantidade de linhas
        //            window.open(link, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=1,resizable=yes,width=950,height=600');

        //            // chama o callback
        //            if (callback)
        //                callback();
        //        },

        //=================================================================================
        // gerarUrl: gera o relatório em PDF de transações conforme filtros
        //=================================================================================
        gerarUrl: function(callback) {

            var link = "";

            if ($("#cmbEmpresaGrupo").find('option:selected').val() != "?") {
                var link = link + "CodigoEmpresaGrupo=" + $("#cmbEmpresaGrupo").val() + "&" + "EmpresaGrupo=" + $("#cmbEmpresaGrupo option:selected").text() + "&";
            }
            if ($("#cmbEmpresaSubGrupo").find('option:selected').val() != "?") {
                var link = link + "CodigoEmpresaSubGrupo=" + $("#cmbEmpresaSubGrupo").val() + "&" + "EmpresaSubGrupo=" + $("#cmbEmpresaSubGrupo option:selected").text() + "&";
            }
            if ($("#cmbFavorecido").find('option:selected').val() != "?") {
                var link = link + "CodigoFavorecido=" + $("#cmbFavorecido").val() + "&" + "Favorecido=" + $("#cmbFavorecido option:selected").text() + "&";
            }
            if ($("#cmbProduto").find('option:selected').val() != null) {
                var link = link + "CodigoProduto=" + $("#cmbProduto").val().toString() + "&" + "Produto=" + $("#cmbProduto option:selected").text() + "&";
            }
            if ($("#cmbReferencia").find('option:selected').val() != null) {
                var link = link + "Valor=" + $("#cmbReferencia").val().toString() + "&" + "Referencia=" + $("#cmbReferencia option:selected").text() + "&";
            }
            if ($("#cmbTipoRegistro").find('option:selected').val() != "?") {
                var link = link + "CodigoTipoRegistro=" + $("#cmbTipoRegistro").val() + "&" + "TipoRegistro=" + $("#cmbTipoRegistro option:selected").text() + "&";
            }
            if ($("#cmbStatusCessao").find('option:selected').val() != "?") {
                var link = link + "CodigoStatusCessao=" + $("#cmbStatusCessao").val() + "&" + "StatusCessao=" + $("#cmbStatusCessao option:selected").text() + "&";
            }

            //ECOMMERCE - Fernando Bove - 20160105: Inicio
            if ($("#rdoTipoContabil").attr("checked")) {
                var link = link + "TipoFinanceiroContabil=" + $("#rdoTipoContabil").val() + "&";
            }
            if ($("#rdoTipoFinanceiro").attr("checked")) {
                var link = link + "TipoFinanceiroContabil=" + $("#rdoTipoFinanceiro").val() + "&";
            }
            //ECOMMERCE - Fernando Bove - 20160105: Fim

            var link = link + "ConsolidaAgendamentoVencimento=" + $("#consolidacao").attr("checked") + "&";
            var link = link + "ConsolidacaoMaiorAgendamento=" + $("#consolidacao1").attr("checked") + "&";
            //ClockWork - Rogério (passa consolidação por referencia)
            var link = link + "ConsolidacaoReferencia=" + $("#consolidacao2").attr("checked") + "&";

            if ($("#txtDataPosicao").val() != "") {
                var link = link + "DataPosicao=" + $("#txtDataPosicao").val() + "&";
            } else {
                $('#txtDataPosicao').datepicker('setDate', '+0');
                var link = link + "DataPosicao=" + $("#txtDataPosicao").val() + "&";
            }
            if ($("#txtPeriodoTransacaoDe").val() != "") {
                var link = link + "DataInicioTransacao=" + $("#txtPeriodoTransacaoDe").val() + "&";
            }
            if ($("#txtPeriodoTransacaoAte").val() != "") {
                var link = link + "DataFimTransacao=" + $("#txtPeriodoTransacaoAte").val() + "&";
            }
            if ($("#txtPeriodoVencimentoDe").val() != "") {
                var link = link + "DataInicioVencimento=" + $("#txtPeriodoVencimentoDe").val() + "&";
            }
            if ($("#txtPeriodoVencimentoAte").val() != "") {
                var link = link + "DataFimVencimento=" + $("#txtPeriodoVencimentoAte").val() + "&";
            }
            if ($("#txtDataProcessamentoDe").val() != "") {
                var link = link + "DataInicioMovimento=" + $("#txtDataProcessamentoDe").val() + "&";
            }
            if ($("#txtDataProcessamentoAte").val() != "") {
                var link = link + "DataFimMovimento=" + $("#txtDataProcessamentoAte").val() + "&";
            }
            if ($("#txtDataAgendamentoDe").val() != "") {
                var link = link + "DataAgendamentoDe=" + $("#txtDataAgendamentoDe").val() + "&";
            }
            if ($("#txtDataAgendamentoAte").val() != "") {
                var link = link + "DataAgendamentoAte=" + $("#txtDataAgendamentoAte").val() + "&";
            }
            //SCF1170
            // data de retorno de CCI - JIRA SCF-836
            if ($("#txtDataRetornoCCIDe").val() != "") {
                var link = link + "DataInicioRetornoCCI=" + $("#txtDataRetornoCCIDe").val() + "&";
            }
            if ($("#txtDataRetornoCCIFim").val() != "") {
                var link = link + "DataFimRetornoCCI=" + $("#txtDataRetornoCCIFim").val() + "&";
            }
            //SCF1170

            //Edgar Oliveira
            var link = link + "RetornarTransacaoPaga=" + $("#chkRetornaTransacaoPaga").attr("checked") + "&";

            if (link != "") {
                link = "?" + link.substr(0, link.length - 1);
            }

            // Chama callback
            if (callback)
                callback();

            return link;
        },

        //=================================================================================
        // receberNumeroDeLinhas: carrega combo de estabelecimentos de acordo com
        //                          o grupo e/ou subgrupo selecionados, caso tenha
        //=================================================================================
        receberNumeroDeLinhas: function(callback) {

            var request = {};
            //request = {};

            //request.Request = {};

            if ($("#cmbEmpresaGrupo").find('option:selected').val() != "?") {
                request.FiltroEmpresaGrupo = $("#cmbEmpresaGrupo").val();
            }
            if ($("#cmbEmpresaSubGrupo").find('option:selected').val() != "?") {
                request.FiltroEmpresaSubGrupo = $("#cmbEmpresaSubGrupo").val();
            }
            if ($("#cmbFavorecido").find('option:selected').val() != "?") {
                request.FiltroFavorecido = $("#cmbFavorecido").val();
            }
            if ($("#cmbProduto").find('option:selected').val() != null) {
                request.FiltroProduto = $("#cmbProduto").val().toString();
            }
            if ($("#cmbReferencia").find('option:selected').val() != null) {
                request.FiltroReferencia = $("#cmbReferencia").val().toString();
            }
            if ($("#cmbTipoRegistro").find('option:selected').val() != "?") {
                request.FiltroTipoRegistro = $("#cmbTipoRegistro").val();
            }
            if ($("#cmbStatusCessao").find('option:selected').val() != "?") {
                request.FiltroStatusRetornoCessao = $("#cmbStatusCessao").val();
            }

            //ECOMMERCE - Fernando Bove - 20160105: Inicio
            if ($("#rdoTipoContabil").attr("checked")) {
                request.FiltroFinanceiroContabil = $("#rdoTipoContabil").val();
            }
            if ($("#rdoTipoFinanceiro").attr("checked")) {
                request.FiltroFinanceiroContabil = $("#rdoTipoFinanceiro").val();
            }
            //ECOMMERCE - Fernando Bove - 20160105: Fim

            request.FiltroConsolidaAgendamentoVencimento = $("#chkConsolida").attr("checked");
            request.FiltroConsolidaMaiorData = $("#consolidacao1").attr("checked");
            request.FiltroConsolidaReferencia = $("#consolidacao2").attr("checked");

            //Edgar Oliveira
            request.FiltroFiltroRetornarTransacaoPaga = $("#chkRetornaTransacaoPaga").attr("checked");

            if ($("#txtDataPosicao").val() != "") {
                request.FiltroDataPosicao = $("#txtDataPosicao").val();
            } else {
                $('#txtDataPosicao').datepicker('setDate', '+0');
                request.FiltroDataPosicao = $("#txtDataPosicao").val();
            }

            if ($("#txtPeriodoTransacaoDe").val() != "") {
                request.FiltroDataInicioTransacao = $("#txtPeriodoTransacaoDe").val();
            }
            if ($("#txtPeriodoTransacaoAte").val() != "") {
                request.FiltroDataFimTransacao = $("#txtPeriodoTransacaoAte").val();
            }
            if ($("#txtPeriodoVencimentoDe").val() != "") {
                request.FiltroDataInicioVencimento = $("#txtPeriodoVencimentoDe").val();
            }
            if ($("#txtPeriodoVencimentoAte").val() != "") {
                request.FiltroDataFimVencimento = $("#txtPeriodoVencimentoAte").val();
            }

            if ($("#txtDataProcessamentoDe").val() != "") {
                request.FiltroDataInicioMovimento = $("#txtDataProcessamentoDe").val();
            }
            if ($("#txtDataProcessamentoAte").val() != "") {
                request.FiltroDataFimMovimento = $("#txtDataProcessamentoAte").val();
            }

            if ($("#txtDataAgendamentoDe").val() != "") {
                request.FiltroDataAgendamentoDe = $("#txtDataAgendamentoDe").val();
            }
            if ($("#txtDataAgendamentoAte").val() != "") {
                request.FiltroDataAgendamentoAte = $("#txtDataAgendamentoAte").val();
            }

            // Envia informações ao objeto existente
            //$("#filtros_relatorio_vencimento").Forms("getData", { dataObject: request });

            request.RetornarNumeroLinhas = 'S';
            request.RetornarRelatorio = false;


            request.VerificarQuantidadeLinhas = true;

            request.TipoRelatorio = "Online";

            // Verifica quantidade de linhas
            executarServico(
                "ListarRelatorioVencimentoRequest",
                request,
                function(data) {

                    if (data.QuantidadeLinhas > 1000) {

                        // Pergunta se o usuario deseja continuar..
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Não foi possível visualizar o relatório devido a quantidade excessiva de linhas (" + data.QuantidadeLinhas + "). Por favor selecione a opção para exportação de relatório para Excel.",
                            callbackOK: function() {
                                return true;
                            }
                        });
                    } else {

                        if (data.QuantidadeLinhas == 0) {
                            // Pergunta se o usuario deseja continuar..
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "O relatório que será gerado não possui linhas. Deseja continuar a operação?",
                                callbackSim: function() {
                                    if (data.CodLog != "")
                                        myVar.codLog = data.CodLog;

                                    if (callback)
                                        callback();
                                },
                                callbackNao: function() {
                                    return true;
                                }
                            });
                        } else {

                            if (data.CodLog != "")
                                myVar.codLog = data.CodLog;

                            if (callback)
                                callback();
                        }
                    }
                });
        }
    }

    // Chama load da página
    $.relatorio.load();
});