﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/template_default.Master"
    CodeBehind="site_relatorio_taxa_recebimento.aspx.cs" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.site_relatorio_taxa_recebimento" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="site_relatorio_taxa_recebimento.js"></script>

    <link type="text/css" rel="stylesheet" href="site_relatorio_taxa_recebimento.css" />
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    <div id="site_relatorio_taxa_recebimento" class="lista">
        <fieldset style="border: 1px solid silver" id="filtroCombos">
            <legend style="position: relative; left: 10px" class="tituloFiltro">Filtros </legend>
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <b>Grupo</b>
                    </td>
                    <td colspan="2">
                        <b>Referência</b>
                    </td>
                    <td colspan="2">
                        <b>Meio de Pagamento</b>
                    </td>
                    <td >
                        <b>Período de Agendamemto</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <select id="cmbGrupo" style="width:100%">
                            <option value="?" selected="selected">Todos</option>
                        </select>
                    </td>
                    <td colspan="2">
                        <select id="cmbReferencia" style="width:100%">
                            <option value="?" selected="selected">Todas</option>
                        </select>
                    </td>
                    <td colspan="2">
                        <select id="cmbMeioPagamento" style="width:100%">
                            <option value="?" selected="selected">Todos</option>
                        </select>
                    </td>
                    <td >
                        <input id="txtDataRepasseInicio" size="8" class="forms[property[FiltroDataRepasseInicio];dataType[date];maskType[date]]" />
                        <input id="txtDataRepasseFim" size="8" class="forms[property[FiltroDataRepasseFim];dataType[date];maskType[date]]" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <div style="float: left; text-align: left">
            <button id="cmdVisualizar">
                Visualizar</button>
        </div>
        <div style="text-align: right">
            <button id="cmdExcel">
                Gerar Excel</button>
            <button id="cmdPDF">
                Gerar PDF</button>
        </div>
    </div>
</asp:Content>
