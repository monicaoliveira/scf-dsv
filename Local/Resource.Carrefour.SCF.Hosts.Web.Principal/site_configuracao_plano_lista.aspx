﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script src="site_configuracao_plano_lista.js" type="text/javascript"></script>
    <script src="site_configuracao_plano_detalhe.js" type="text/javascript"></script>

    <link href="site_configuracao_plano_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_configuracao_plano_detalhe.css" rel="stylesheet" type="text/css" />

    <title>SCF - Cadastro de Planos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--            Lista de Planos           -->
    <!-- ------------------------------------ -->
    <div id="site_configuracao_plano_lista">
        <div id="lista" class="lista">

            <table id="tbPlano"></table>
                <div class="botoes">
                    <button id="cmdImportarProdutoPlano">Importar</button>
                    <button id="cmdNovoPlano">Novo</button>
                    <button id="cmdEditarPlano">Editar</button>
                    <button id="cmdRemoverPlano">Remover</button>
                    <button id="cmdAtualizarListaPlano">Atualizar Lista</button>
                </div>
        </div>
        
        <div id="importacao">
            <label>Caminho do Arquivo:</label>
            <input type="text" id="txtCaminhoArquivo" style="width: 340px;" />
        </div>
        
    </div>

</asp:Content>
