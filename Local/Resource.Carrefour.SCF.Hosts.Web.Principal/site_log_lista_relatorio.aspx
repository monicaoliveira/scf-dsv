﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_log_lista_relatorio.js"></script>
    <script type="text/javascript" src="site_log_detalhe.js"></script>
    <link href="site_log_lista_relatorio.css" rel="stylesheet" type="text/css" />
    <link href="site_log_detalhe.css" rel="stylesheet" type="text/css" />
    <title>SCF - Log</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- Conteúdo da Página -->
    <div id="site_log_lista_relatorio">

        <!-- Área do filtro -->
        <div class="filtro formulario" id="painelLogLabel">
            <table >
                <tr>
                    <td>
                        <label class="labelRelatorio"></label>
                    </td>
                </tr>
            </table>
        </div>
        <!-- <div class="filtro formulario" id="painelLogFiltro">
            <table >
                <tr>
                    <td>
                        <input type="text" value="" class="filtroUsuario" maxlength="100"/>
                    </td>
                    <td>
                        <label>Tipo de Log:</label>
                    </td>
                    <td >
                        <select class="filtroTipoLog">
                            <option value="?">(selecione)</option>
                        </select>
                    </td>
                
                    <td>
                        <label>De:</label>
                    </td>
                    <td>
                        <input type="text" class="forms[property[filtroDataInicio];dataType[date];maskType[date]] filtroDataInicio"/>
                    </td>
                    <td>
                        <label>Até:</label>
                    </td>
                    <td>
                        <input type="text" class="forms[property[filtroDataFim];dataType[date];maskType[date]] filtroDataFim"/>
                    </td>
                    <td align="right">
                        <button class="cmdPesquisar">Pesquisar</button>
                    </td>
                </tr>
            </table>
        </div> -->

        <!-- Resultado -->
        <div id="tb_site_log_lista_relatorio" class="listaRelatorio">
            <table id="tbLogRelatorio"></table>
            <!-- <div class="botoes">
                <button class="cmdDetalhe">Mostrar Detalhe</button>
            </div> -->
        </div>
       
    </div>

</asp:Content>
