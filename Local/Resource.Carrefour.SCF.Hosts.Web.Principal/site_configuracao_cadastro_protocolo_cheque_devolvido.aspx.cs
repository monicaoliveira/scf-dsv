﻿//963
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Contratos.Comum;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Web;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class site_configuracao_cadastro_protocolo_cheque_devolvido : PaginaBase
    {   protected void Page_Load(object sender, EventArgs e)
        {   if (!Page.IsPostBack)
            {   PreencherCombos();
            }
        }
        //==============================================================================================================================================
        // PREENCHIMENTO DO COMBOS
        //==============================================================================================================================================
        private void PreencherCombos()
        {
            string CodigoSessaoTratado = null;

            if (Session["CodigoSessao"] != null)
            {
                CodigoSessaoTratado = Session["CodigoSessao"].ToString();
            }
            else
            {
                CodigoSessaoTratado = "Sem Sessão";
            }
            //-------------------------------------------------------------------------------------------
            // CARREGA COMBO DE ESTABELECIMENTOS
            //-------------------------------------------------------------------------------------------
            ListarEstabelecimentoResponse estabelecimentos = Mensageria.Processar<ListarEstabelecimentoResponse>( new ListarEstabelecimentoRequest(){CodigoSessao = CodigoSessaoTratado });
            rptCmbEstabelecimento.DataSource = estabelecimentos.Resultado;
            rptCmbEstabelecimento.DataBind();

            //-------------------------------------------------------------------------------------------
            // CARREGA COMBO DE TIPO DE ESTORNO
            //-------------------------------------------------------------------------------------------
            ListarTipoEstornoResponse estornos = Mensageria.Processar<ListarTipoEstornoResponse>(new ListarTipoEstornoRequest(){CodigoSessao = CodigoSessaoTratado });
            rptCmbTipoEstorno.DataSource = estornos.Resultado;
            rptCmbTipoEstorno.DataBind();
        }
    }
}
