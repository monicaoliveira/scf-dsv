﻿<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_dominio_configuracao_lista.js"></script>
    <link href="site_configuracao_dominio_configuracao_lista.css" rel="stylesheet" type="text/css" />
    <title>SCF - Configuração de Domínios Genéricos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">

    <!-- ------------------------------------ -->
    <!--     Lista de Domínios Genéricos      -->
    <!-- ------------------------------------ -->
    <div id="site_dominio_generico_lista" style="margin: 30px;">
    
        <div class="filtro formulario">
            <table>
            <tr>
                <td><label>Grupo:</label></td>
                <td>
                    <select id="cmbEmpresaGrupo" class="forms[property[CodigoEmpresaGrupo];required]" style="width: 204px;">
                        <option value="?">(selecione)</option>
                    </select>
                </td>
            </tr>
            </table>
        </div>
        
        <div >
           <table>
            <tr valign="top">
                <td>
                    <table id="tbLista"></table>
                </td>
                
                <td style="padding-left: 5px;">
                    <table id="tbListaItem"></table>
                </td>
            </tr>
            </table>
        </div>
        
        
        
    </div>

</asp:Content>
