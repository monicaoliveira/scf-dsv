﻿$(document).ready(function() {

    // Cria namespace 
    if (!$.taxa)
        $.taxa = {};

    // Funcoes do namespace 
    $.taxa.detalhe = {

        variaveis: {},

        // --------------------------------------------------------------------
        //  iniciar: verifica se tela já está carregada
        // --------------------------------------------------------------------
        iniciar: function() {
            // Verifica se o html esta carregado
            if ($("#site_configuracao_taxa_recebimento_detalhe").length == 0) {

                // Pede o html e insere na área temporaria
                $("#areaTemporaria").load("site_configuracao_taxa_recebimento_detalhe.aspx #site_configuracao_taxa_recebimento_detalhe", function() {

                    // Copia o elemento para a área comum
                    $("#site_configuracao_taxa_recebimento_detalhe").appendTo("#areaComum");

                    // Completa o load
                    $.taxa.detalhe.load(function() {

                        // Chama o callback
                        if (callback)
                            callback();
                    });
                });

            } else {

                // Chama o callback
                if (callback)
                    callback();
            }
        },
        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {

            // Estilo dos botões
            $("#site_configuracao_taxa_recebimento_detalhe button").button();

            // Inicializa forms
            $("#site_configuracao_taxa_recebimento_detalhe").Forms("initializeFields");

            //-------------------------------------------------------------------
            // Inicializa dialog de detalhe
            //-------------------------------------------------------------------
            $("#site_configuracao_taxa_recebimento_detalhe").dialog({
                autoOpen: false,
                height: 210,
                width: 380,
                modal: true,
                title: "Detalhe da Taxa de Recebimento",
                buttons: {
                    "Salvar": function() {

                        // Pede para salvar
                        $.taxa.detalhe.salvar(function() {

                            // Mostra popup de sucesso
                            popup({
                                titulo: "Salvar Taxa de Recebimento",
                                icone: "sucesso",
                                mensagem: "Taxa de Recebimento salva com sucesso!",
                                callbackOK: function() {
                                    $.taxa.lista.listarTaxa();
                                    // Fecha dialog de detalhe
                                    $("#site_configuracao_taxa_recebimento_detalhe").dialog("close");
                                }
                            });
                        });
                    },
                    "Fechar": function() {
                        // Fecha o dialog
                        $("#site_configuracao_taxa_recebimento_detalhe").dialog("close");
                    }
                }
            });

            $("#txtValorRecebimento").bind("change paste keyup", function() {
                $.taxa.detalhe.tratarValor("txtValorRecebimento");
            });
            $("#txtTaxaRecebimento").bind("change paste keyup", function() {
                $.taxa.detalhe.tratarValor("txtTaxaRecebimento");
            });

            //-------------------------------------------------------------------
            // Preenche combo de Grupo
            //-------------------------------------------------------------------
            $.taxa.detalhe.carregarGrupo(function(callback) {
                //-------------------------------------------------------------------
                // Preenche combo de meio de pagamento
                //-------------------------------------------------------------------
                $.taxa.detalhe.carregarMeioPagamento(function(callback) {
                    //-------------------------------------------------------------------
                    // Preenche combo de referencia
                    //-------------------------------------------------------------------
                    $.taxa.detalhe.carregarReferencia(function(callback) {
                        // Chama callback
                        if (callback)
                            callback();
                    });
                });
            });
        },
        //=================================================================================
        // abrir: funcoes executadas na abertura da tela
        //=================================================================================
        abrir: function(codigoTaxaRecebimento) {
            $("#txtValorRecebimento").removeAttr("readonly");
            $("#txtTaxaRecebimento").removeAttr("readonly");

            $.taxa.detalhe.variaveis.codigoTaxaRecebimento = codigoTaxaRecebimento;

            // Limpa campos
            $("#site_configuracao_taxa_recebimento_detalhe").Forms("clearData");

            if (codigoTaxaRecebimento) {
                // Carrega dados do taxa existente
                $.taxa.detalhe.carregar(codigoTaxaRecebimento, function() {

                    // Abre dialog
                    $("#site_configuracao_taxa_recebimento_detalhe").dialog("open");
                });

            } else {
                // Abre dialog 
                $("#site_configuracao_taxa_recebimento_detalhe").dialog("open");
            };
        },

        //=================================================================================
        // carregar: Carrega dados referentes ao taxa
        //=================================================================================
        carregar: function(codigoTaxaRecebimento, callback) {

            executarServico(
                "ReceberTaxaRecebimentoRequest",
                { CodigoTaxaRecebimento: codigoTaxaRecebimento },
                function(data) {

                    $.taxa.detalhe.variaveis.taxaRecebimentoInfo = data.TaxaRecebimentoInfo;

                    if ($.taxa.detalhe.variaveis.taxaRecebimentoInfo.ValorRecebimento == "0") {
                        $.taxa.detalhe.variaveis.taxaRecebimentoInfo.ValorRecebimento = "";
                        $.taxa.detalhe.variaveis.taxaRecebimentoInfo.PercentualRecebimento = parseFloat($.taxa.detalhe.variaveis.taxaRecebimentoInfo.PercentualRecebimento.replace(",", ".")).toFixed(2).replace(".", ",");
                        $("#txtValorRecebimento").attr("readonly", true);
                    }
                    else {
                        $.taxa.detalhe.variaveis.taxaRecebimentoInfo.PercentualRecebimento = "";
                        $.taxa.detalhe.variaveis.taxaRecebimentoInfo.ValorRecebimento = parseFloat($.taxa.detalhe.variaveis.taxaRecebimentoInfo.ValorRecebimento.replace(",", ".")).toFixed(2).replace(".", ",");
                        $("#txtTaxaRecebimento").attr("readonly", true);
                    }

                    // Preenche os campos do detalhe
                    $("#site_configuracao_taxa_recebimento_detalhe").Forms("setData", { dataObject: data.TaxaRecebimentoInfo });

                    // Chama callback
                    if (callback)
                        callback();
                });
        },

        //=================================================================================
        // salvar: Trata inserção ou alteração de um taxa
        //=================================================================================
        salvar: function(callback) {
            $.taxa.detalhe.validar(function() {
                if ($.taxa.detalhe.variaveis.codigoTaxaRecebimento) {

                    // Recebe taxaRecebimentoInfo
                    var taxaRecebimentoInfo = $.taxa.detalhe.variaveis.taxaRecebimentoInfo;

                    // Envia informações ao objeto existente
                    $("#site_configuracao_taxa_recebimento_detalhe").Forms("getData", { dataObject: taxaRecebimentoInfo });

                    // Limpa temp
                    $.taxa.detalhe.variaveis.codigoTaxaRecebimento = null;

                } else {

                    // Cria novo objeto
                    taxaRecebimentoInfo = {};

                    // Envia informações ao novo objeto
                    $("#site_configuracao_taxa_recebimento_detalhe").Forms("getData", { dataObject: taxaRecebimentoInfo });
                }
                taxaRecebimentoInfo.ValorRecebimento = taxaRecebimentoInfo.ValorRecebimento == "" ? 0 : parseFloat(taxaRecebimentoInfo.ValorRecebimento.replace(",", "."));
                taxaRecebimentoInfo.PercentualRecebimento = taxaRecebimentoInfo.PercentualRecebimento == "" ? 0 : parseFloat(taxaRecebimentoInfo.PercentualRecebimento.replace(",", "."));
                taxaRecebimentoInfo.DataInicio = taxaRecebimentoInfo.DataInicio.substr(0, 10);
                taxaRecebimentoInfo.DataFim = taxaRecebimentoInfo.DataFim.substr(0, 10);

                executarServico(
                    "SalvarTaxaRecebimentoRequest",
                    { TaxaRecebimentoInfo: taxaRecebimentoInfo },
                function(data) {

                    // Chama os callbacks
                    if (callback)
                        callback();
                });
            }, $.taxa.detalhe.retornarTrue);
        },
        // --------------------------------------------------------------------
        //  validar: valida campos necessários do cadastro de planos
        // --------------------------------------------------------------------
        validar: function(callbackOk, callbackErro) {

            // Inicializa criticas
            var criticas = new Array();
            var valorRecebimento = $("#txtValorRecebimento").val() != "" ? parseFloat($("#txtValorRecebimento").val().replace(",", ".")) : 0;
            var percentualRecebimento = $("#txtTaxaRecebimento").val() != "" ? parseFloat($("#txtTaxaRecebimento").val().replace(",", ".")) : 0;

            var parts = $("#txtDataInicio").val().split('/');
            //please put attention to the month (parts[1]), Javascript counts months from 0:
            // January - 0, February - 1, etc
            var dataInicio = new Date(parts[2], parts[1] - 1, parts[0]);

            parts = $("#txtDataFim").val().split('/');
            var dataFim = new Date(parts[2], parts[1] - 1, parts[0]);

            // Pede a validação para o forms
            criticas = $("#site_configuracao_taxa_recebimento_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });

            if (dataInicio != "" && dataFim != "")
                if (dataInicio > dataFim)
                criticas.push({
                    property: "DataFim",
                    labelValue: "DataFim",
                    message: "Válido Até deve ser maior ou igual à Válido de"
                });

            if (valorRecebimento != 0 && percentualRecebimento != 0)
                criticas.push({
                    property: "ValorRecebimento",
                    labelValue: "ValorRecebimento/TaxaRecebimento",
                    message: "Não é permitido inserir Valor e Percentual de Taxa,\n favor informa apenas um deles."
                });

            if (valorRecebimento == 0 && percentualRecebimento == 0)
                criticas.push({
                    property: "ValorRecebimento",
                    labelValue: "ValorRecebimento/TaxaRecebimento",
                    message: "Deve-se informar um número para Valor ou Percentual de Taxa."
                });

            $.taxa.detalhe.validarTaxaDuplicada(criticas, function() {
                // Se tem criticas, mostra o formulario
                if (criticas[0]) {
                    $.Forms.showValidationForm({ errors: criticas });
                    if (callbackErro)
                        callbackErro();

                } else if (callbackOk)
                    callbackOk();
            });
        },
        //------------------------------------------------------------------------
        // validarTaxaDuplicada: Busca por Taxas de Recebimento já cadastrada
        //------------------------------------------------------------------------
        validarTaxaDuplicada: function(criticas, callback) {

            if (criticas.length == 0) {
                var request = {};

                request.FiltroMaxLinhas = 100;
                request.FiltroCodigoGrupo = $("#cmbGrupoDetalhe").val();
                request.FiltroCodigoReferencia = $("#cmbReferenciaDetalhe").val();
                request.FiltroCodigoMeioPagamento = $("#cmbMeioPagamentoDetalhe").val();

                //please put attention to the month (parts[1]), Javascript counts months from 0:
                // January - 0, February - 1, etc
                var parts = $("#txtDataInicio").val().split('/');
                request.FiltroDataInicio = new Date(parts[2], parts[1] - 1, parts[0]);

                parts = $("#txtDataFim").val().split('/');
                request.FiltroDataFim = new Date(parts[2], parts[1] - 1, parts[0]);

                if ($.taxa.detalhe.variaveis.codigoTaxaRecebimento)
                    request.FiltroCodigoTaxaRecebimento = $.taxa.detalhe.variaveis.codigoTaxaRecebimento;

                executarServico(
                "ListarTaxaRecebimentoRequest",
                request,
                function(data) {
                    if (data.Resultado.length > 0)
                        criticas.push({
                            property: "TaxaRecebimento",
                            labelValue: "TaxaRecebimento",
                            message: "Já existe uma taxa cadastrada contendo esta vigência:"
                            + "\n<b>Grupo</b>: " + $("#cmbGrupoDetalhe option:selected").text()
                            + "\n<b>Referencia</b>: " + $("#cmbReferenciaDetalhe option:selected").text()
                            + "\n<b>Meio de Pagamento</b>: " + $("#cmbMeioPagamentoDetalhe option:selected").text()
                            + "\n<b>Vigência Inicio</b>: " + $("#txtDataInicio").val()
                            + "\n<b>Vigência Fim</b>: " + $("#txtDataFim").val()
                        });

                    // Chama os callbacks
                    if (callback)
                        callback();
                });
            } else {
                // Chama os callbacks
                if (callback)
                    callback();
            }
        },
        //=================================================================================
        // remover: Trata remoção de um taxa
        //=================================================================================
        remover: function(codigoTaxaRecebimento, callback) {

            if (codigoTaxaRecebimento) {

                // Pede para remover taxa
                executarServico(
                    "RemoverTaxaRecebimentoRequest",
                    { CodigoTaxaRecebimento: codigoTaxaRecebimento },
                function(data) {

                    // Chama o callback
                    if (callback)
                        callback();
                });
            }

            else {

                // Pede para selecionar uma linha
                popup({
                    titulo: "Remover Taxa de Recebimento",
                    icone: "atencao",
                    mensagem: "Favor selecionar uma linha!",
                    callbackOK: function() { }
                });
            }
        },
        //=================================================================================
        // carregarGrupo: carrega combo de Grupo
        //=================================================================================
        carregarGrupo: function(callback) {

            executarServico(
                "ListarEmpresaGrupoRequest",
                {},
                function(data) {
                    $("#cmbGrupoDetalhe").children().each(
                            function(index) {
                                $(this).remove();
                            }
                        );

                    elG = $("#cmbGrupoDetalhe");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' (selecione)'
                    });
                    opt.appendTo(elG);

                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].CodigoEmpresaGrupo,
                            text: ' ' + data.Resultado[i].NomeEmpresaGrupo
                        });
                        opt.appendTo(elG);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },
        //=================================================================================
        // carregarMeioPagamento: carrega combo de meio de pagamento
        //=================================================================================
        carregarMeioPagamento: function(callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    CodigoLista: 9
                },
                function(data) {
                    $("#cmbMeioPagamentoDetalhe").children().each(
                            function(index) {
                                $(this).remove();
                            }
                        );

                    elMP = $("#cmbMeioPagamentoDetalhe");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' (selecione)'
                    });
                    opt.appendTo(elMP);


                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elMP);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },

        //=================================================================================
        // carregarReferencia: carrega combo de Referencia
        //=================================================================================
        carregarReferencia: function(callback) {

            executarServico(
                "ListarListaItemRequest",
                {
                    NomeLista: "Referencia"
                },
                function(data) {
                    $("#cmbReferenciaDetalhe").children().each(
                        function(index) {
                            $(this).remove();
                        }
                    );

                    elRef = $("#cmbReferenciaDetalhe");
                    var opt = $('<option />', {
                        value: "?",
                        text: ' (selecione)'
                    });
                    opt.appendTo(elRef);

                    for (var i = 0; i < data.Resultado.length; i++) {
                        var opt = $('<option />', {
                            value: data.Resultado[i].Valor,
                            text: ' ' + data.Resultado[i].Descricao
                        });
                        opt.appendTo(elRef);
                    }

                    // chama callback
                    if (callback)
                        callback();
                }
            );
        },
        tratarValor: function(campo) {

            var campoOff = "";
            var valorTexto = $("#" + campo).val();
            var lista = valorTexto.split(",");
            var numero = parseInt(lista[0]);
            
            //            if (!valorTexto.match(/^-?\d*\,\d+$/)) {
            //                valorTexto = valorTexto.slice(0, -1);
            //            }

            if (lista[0].indexOf("00") > -1 ) {
                if (numero != undefined && numero == 0)
                    valorTexto = valorTexto.replace("00", "0");
            } else {
                valorTexto = valorTexto.replace(/[^0-9\,]/g, '');
            }
            if (lista[2] != null) {
                valorTexto = valorTexto.substring(0, valorTexto.lastIndexOf(","));
            }

            //var valor = parseFloat(valorTexto).toFixed(2);
            var tamanhoMaximo = $("#" + campo).attr("maxlength");

            if (campo == "txtValorRecebimento") {
                campoOff = "txtTaxaRecebimento";
            } else {
                campoOff = "txtValorRecebimento";
            }

            $("#" + campoOff).attr("readonly", true);

            if (valorTexto == "") {
                $("#" + campoOff).removeAttr("readonly");
            }

            if (valorTexto.length > tamanhoMaximo) {
                valorTexto = valorTexto.slice(0, tamanhoMaximo);
            }

            $("#" + campo).val(valorTexto);
        }
    };

    $.taxa.detalhe.iniciar();
});