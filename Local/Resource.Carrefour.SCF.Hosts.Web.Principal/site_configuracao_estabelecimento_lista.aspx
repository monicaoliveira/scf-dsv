<%@ Page Title="" Language="C#" MasterPageFile="~/template_default.Master" AutoEventWireup="true" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="site_configuracao_estabelecimento_lista.js"></script>
    <link href="site_configuracao_estabelecimento_lista.css" rel="stylesheet" type="text/css" />
    <link href="site_configuracao_estabelecimento_detalhe.css" rel="stylesheet" type="text/css" />
    <script src="site_configuracao_estabelecimento_detalhe.js" type="text/javascript"></script>

    <title>SCF - Cadastro de Estabelecimentos</title>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">       
    
        <!-- ------------------------------------ -->
        <!--       Lista de Estabelecimentos      -->
        <!-- ------------------------------------ -->
        <div id="site_configuracao_estabelecimento_lista">   
        <!-- ------------------------------------ -->
        <!--                Filtros               -->
        <!-- ------------------------------------ -->
        <div class="filtro" class="lista formulario">         
            <table>
                <tr>
                    <td>
                        <label>Cnpj:</label>
                    </td><td>
                        <input type="text" id="txtFiltroCnpj" class="forms[property[filtroCnpj];dataType[integer]]" maxlength="15" />
                    </td><td>&nbsp;</td><td>
                        <label>Razão social:</label>
                    </td><td>
                        <input type="text" id="txtFiltroRazaoSocial"  class="forms[property[filtroRazaoSocial]]" maxlength="200" />
                    </td>
               
                    <td>
                        <label>Código CSU:</label>
                    </td>
                    <td>
                        <input type="text" id="txtFiltroCodigoCsu" class="forms[property[filtroCodigoCsu]]" maxlength="10" />
                    </td><td>&nbsp;</td><td>
                        <label>Código SITEF:</label>
                    </td><td>
                        <input type="text" id="txtFiltroCodigoSitef" class="forms[property[filtroCodigoSitef]]" maxlength="10" />
                    </td><td style="padding-left: 20px;">
                        <button id="cmdFiltrar" class="cmdFiltrar">Filtrar</button>
                    </td>
                </tr>
            </table>
        </div>    
        <div id="lista" class="lista">
            <table id="tbEstabelecimento"></table>
            <div class="botoes">
                <button id="cmdImportarEstabelecimento">Importar</button>
                <button id="cmdNovoEstabelecimento">Novo</button>
                <button id="cmdEditarEstabelecimento">Editar</button>
                <button id="cmdRemoverEstabelecimento">Remover</button>
                <button id="cmdAtualizarListaEstabelecimento">Atualizar Lista</button>
            </div>
        </div>
        
        <div id="importacao">
            <label>Caminho do Arquivo:</label>
            <input type="text" id="txtCaminhoArquivo"  class="forms[property[CaminhoArquivo]]" style="width: 481px;"/>
        </div>
        <div id="dialogErroLayout">
            <table cellspacing="0" cellpadding="0"> 
                <tr>
                    <td width="10%" style="text-align: center;">
                        <img class="icone" src="img/icone_erro.png" />       
                    </td>                    
                    <td width="89%">
                        <span id="popupSimNao_mensagem_1" style="font-size: 12px;
                        vertical-align: top;">Ocorreu um erro durante a leitura do arquivo, verifique se o layout está correto, de acordo com a tabela abaixo.</span>
                    </td>                    
                </tr>
            </table>        
            <table class="bordasimples">
                <tr>
                    <td>
                       <label style="font-weight:bold">Seq</label>
                    </td>
                    <td>
                        <label style="font-weight:bold">Nome do Campo</label>
                    </td>
                    <td>
                        <label style="font-weight:bold">Posição</label>
                    </td>
                    <td>
                        <label style="font-weight:bold">Tam</label>
                    </td>                    
                    <td>
                        <label style="font-weight:bold">Inicio</label>
                    </td>     
                    <td>
                        <label style="font-weight:bold">Tipo</label>
                    </td>    
                    <td>
                        <label style="font-weight:bold">O Conteúdo deve atender as regras abaixo</label>
                    </td>                                                    
                </tr>                
                <tr>
                    <td>
                        <label>1</label>
                    </td>
                    <td>
                        <label>CNPJ Estabelecimento</label>
                    </td>
                    <td>
                        <label>1-15</label>
                    </td>
                    <td>
                        <label>15</label>
                    </td>  
                    <td>
                        <label>1</label>
                    </td>
                    <td>
                        <label>Numero</label>
                    </td>                                                            
                    <td>
                        <label>Sem Formatação</label>
                    </td>                 
                </tr>
                <tr> 
                    <td>
                        <label>2</label>
                    </td>
                    <td>
                        <label>Nome Estabelecimento</label>
                    </td>
                    <td>
                        <label>16-35</label>
                    </td>
                    <td>
                        <label>20</label>
                    </td>  
                    <td>
                        <label>16</label>
                    </td>
                    <td>
                        <label>String</label>
                    </td>                                                            
                    <td>
                        <label></label>
                    </td>        
                </tr>
                <tr>  
                    <td>
                        <label>3</label>
                    </td>
                    <td>
                        <label>CNPJ Favorecido</label>
                    </td>
                    <td>
                        <label>36-50</label>
                    </td>
                    <td>
                        <label>15</label>
                    </td>  
                    <td>
                        <label>36</label>
                    </td>
                    <td>
                        <label>Numero</label>
                    </td>                                                            
                    <td>
                        <label>Tem que ser igual ao cadastrado no SCF, se não existir mandar ZEROS</label>
                    </td>            
                </tr>
                <tr>       
                    <td>
                        <label>4</label>
                    </td>
                    <td>
                        <label>Nome do GRUPO</label>
                    </td>
                    <td>
                        <label>51-90</label>
                    </td>
                    <td>
                        <label>40</label>
                    </td>  
                    <td>
                        <label>51</label>
                    </td>
                    <td>
                        <label>String</label>
                    </td>                                                            
                    <td>
                        <label>Tem que ser igual ao cadastrado no SCF, se não existir mandar BRANCOS</label>
                    </td>        
                </tr>
                <tr>  
                    <td>
                        <label>5</label>
                    </td>
                    <td>
                        <label>Nome do SUBGRUPO</label>
                    </td>
                    <td>
                        <label>91-110</label>
                    </td>
                    <td>
                        <label>20</label>
                    </td>  
                    <td>
                        <label>91</label>
                    </td>
                    <td>
                        <label>String</label>
                    </td>                                                            
                    <td>
                        <label>Tem que ser igual ao cadastrado no SCF, se não existir mandar BRANCOS</label>
                    </td>      
                </tr>
                <tr>      
                    <td>
                        <label>6</label>
                    </td>
                    <td>
                        <label>Código LOJA CSU</label>
                    </td>
                    <td>
                        <label>111-120</label>
                    </td>
                    <td>
                        <label>10</label>
                    </td>  
                    <td>
                        <label>111</label>
                    </td>
                    <td>
                        <label>String</label>
                    </td>                                                            
                    <td>
                        <label>Se não existir mandar ZEROS</label>
                    </td>           
                </tr>
                <tr>    
                    <td>
                        <label>7</label>
                    </td>
                    <td>
                        <label>Código LOJA SITEF</label>
                    </td>
                    <td>
                        <label>121-130</label>
                    </td>
                    <td>
                        <label>10</label>
                    </td>  
                    <td>
                        <label>121</label>
                    </td>
                    <td>
                        <label>String</label>
                    </td>                                                            
                    <td>
                        <label>Se não existir mandar ZEROS</label>
                    </td>              
                </tr>                                                                                                                
            </table>
        </div>
    </div>

</asp:Content>
