﻿$(document).ready(function() {

    // Namespace 
    if (!$.pagamento)
        $.pagamento = {};

    // Funcoes do namespace 
    $.pagamento.lista = {

        // --------------------------------------------------------------------
        //  load: funcoes de inicializacao da tela
        // --------------------------------------------------------------------
        load: function() {
       
            // Insere o subtítulo
            $("#subtitulo").html("Consulta > Lista de Pagamentos");

            // Estilo dos botões
            $("#site_pagamento_filtro button").button();
            
            // Botão filtrar
            $("#site_pagamento_filtro .cmdFiltrar").click(function() {
    
                $.pagamento.lista.listarPagamento();
                // monica 06/09/2017 chamar de novo para carregar a quantidade
                requestListar = {};
                requestListar.RetornaApenasQuantidade = true;
                requestListar.FiltroStatusPagamento = 0; // apenas os pendentes 
                $.pagamento.lista.listarPagamento(requestListar);
            });

            // Inicializa forms
            $("#site_pagamento_filtro").Forms("initializeFields");

            // Inicializa grid de lista 
            $("#tbPagamento").jqGrid({
                datatype: "clientSide",
                mtype: "GET",
                height: 500,
                colNames: ["ID Pagamento",  "Favorecido", "Banco", "Agencia", "Conta", "Valor", "Data Geração", "Data Pagto",  "Data Envio", "Status",  "Acao" ],
                colModel: [
                    { name: "CodigoPagamento", index: "Codigo", editable: false, width: 20, key: true, sortable: false},
                    { name: "NomeFavorecido", index: "RazaoSocial", editable: false, sortable: false , width: 50 },
                    { name: "Banco", index: "Banco", editable: false, sortable: false , width: 10 },
                    { name: "Agencia", index: "Agencia", editable: false, sortable: false , width: 10 },
                    { name: "Conta", index: "Conta", editable: false, sortable: false , width: 20 },
                    { name: "ValorPagamento", index: "ValorPagamento", editable: false, sortable: false , width: 30, sorttype: "integer", formatter: float2moeda },
                    { name: "DataGeracao", index: "DataGeracao", editable: false, sortable: false , formatter: 'date' ,classes: "grid[property[DataGeracao];datatype[date]]", width: 40 ,sorttype:'date'},
                    { name: "DataPagamento", index: "DataPagamento", editable: false, sortable: false , formatter: 'date' ,classes: "grid[property[DataPagamento];datatype[date]]", width: 20 ,sorttype:'date'},
                    { name: "DataEnvio", index: "DataEnvio", editable: false, sortable: false , formatter: 'date' ,classes: "grid[property[DataEnvio];datatype[date]]", width: 40 ,sorttype:'date'},
                    { name: "Status", index: "Status", editable: false, width: 20, sortable: false  },
                    { name: "acao", width: 10, index: "acao", align: "center", editable: false, sortable: false }
                ],
                scroll: true
            });
            
            // carrega a qtde de pagamentos pendentes
            requestListar = {};
            requestListar.RetornaApenasQuantidade = true;
            requestListar.FiltroStatusPagamento = 0; // apenas os pendentes 
            $.pagamento.lista.listarPagamento(requestListar);
           
            // Trata o redimensionamento da tela
            $(window).bind("resize", function() {
                $("#tbPagamento").setGridWidth($(window).width() - margemEsquerda);
                $("#tbPagamento").setGridHeight($(window).height() - margemRodape-60);
            }).trigger("resize"); 
            
            
            $("#site_pagamento_filtro .btnExportarArquivoPagamentos").click(function() {
                $.pagamento.lista.gerarPagamento();
            }); 
            
            // setar a data
            //var date = new Date();
            //date.setDate(date.getDate() - 1);
            // $('#txtDtGeracaoDe').datepicker("setDate", date);
            // set combo com o valor pendente
            $("#cmbStatusPagamento").val("0");
            
            
        },
        
        removerLinha: function() 
        {   var tr = $(this).closest("tr");
            var objLinha = $("#tbPagamento").jqGrid("getRowData", tr.attr("id"));
            request = {}
            request.CodigoPagamento = objLinha.CodigoPagamento;
            $.pagamento.lista.excluirPagamento(request, tr);         // inclui o tr no parametro
            var acoes = $("#" + tr.attr("id") + " td:last input");
            // Esconde o botao de excluir
            $(acoes[0]).hide(); // remover
            // troca o texto do status
            $("#" + tr.attr("id") + " td[aria-describedby='tbPagamento_Status']").text("Em exclusão")
        },
        
        

        // --------------------------------------------------------------------
        //  listarPagamento: lista os pagamentos
        // --------------------------------------------------------------------
        listarPagamento: function(requestListar) {

            
            
            if (requestListar == null) 
            {
                requestListar = {};
                requestListar.RetornaApenasQuantidade = false;
            }

            // Monta o filtro
            var request = {};
            $("#site_pagamento_filtro").Forms("getData", { dataObject: request });
            
            if  (requestListar.RetornaApenasQuantidade == true)
            {
                request.RetornaApenasQuantidade = true;
                request.FiltroStatusPagamento = requestListar.FiltroStatusPagamento;
            }
            else
            {
                request.RetornaApenasQuantidade = false;
                // Limpa lista
                $("#tbPagamento").clearGridData();
            }
                
            // Executa Serviço
            executarServico(
                "ListarPagamentoRequest",
                request,
                function(data) {
                
                    if (request.RetornaApenasQuantidade != true)
                    {
                        for (var i = 0; i < data.Resultado.length; i++) {
                            
                            var acao =  "<input type='image' src='img/button_white_remove.png' style='width: 16px; height: 16px;' title='Excluir' class='cmdEditar' />&nbsp;";
                            
                            var pagamento = data.Resultado[i];
                            if (data.Resultado[i].Status == "Pendente"){
                                pagamento.acao = acao;
                            }
                            else
                                pagamento.acao = "";
                            pagamento.DataPagamento = pagamento.DataPagamento.substring(0,10);
                            $("#tbPagamento").jqGrid("addRowData", "tbPagamento_" + pagamento.CodigoPagamento, pagamento);
                            $("#tbPagamento_" + pagamento.CodigoPagamento + " .cmdEditar").click($.pagamento.lista.removerLinha);
                        }
                    }
                    else
                    {
                        $("#lblQtdePagtoPendente").text(data.Quantidade);
                        
                    }
                });
        },
        excluirPagamento: function(requestExcluir, tr) { // inclui o tr no parametro para tratar a volta do status quando NAO
            
            // pede confirmação de exclusão
            popup({
                titulo: "Atenção",
                icone: "atencao",
                mensagem: "Tem certeza que deseja remover este pagamento?",
                callbackSim: function() {

                    // Pede para remover
                    
                    // Executa Serviço
                    executarServico(
                        "ProcessarExclusaoPagamentosRequest",
                        request,
                        function(data) {
                        
                                    
                                // Mostra popup de sucesso
                                    popup({
                                        titulo: "Ok",
                                        icone: "sucesso",
                                        mensagem: "Processo número " + data.CodigoProcesso + " criado para a exclusão do Pagamento.",
                                        callbackOK: function() {
                                            
                                            //// recarregar a lista
                                            //$.pagamento.lista.listarPagamento(); 
                                            
                                            // recarrega a qtde de pagamentos pendentes
                                            requestListar = {};
                                            requestListar.RetornaApenasQuantidade = true;
                                            requestListar.FiltroStatusPagamento = 0; // apenas os pendentes 
                                            $.pagamento.lista.listarPagamento(requestListar);
                                            }
                                    });
                        });
                    
                    },
                    callbackNao: function() 
                    { 
                        var acoes = $("#" + tr.attr("id") + " td:last input");
                        $(acoes[0]).show(); // reexibir
                        $("#" + tr.attr("id") + " td[aria-describedby='tbPagamento_Status']").text("Pendente");
                    }
            });
        },
        
        gerarPagamento: function() {
            
            // pede confirmação da geracao
            popup({
                titulo: "Atenção",
                icone: "atencao",
                mensagem: "Tem certeza que deseja gerar o Arquivo de Pagamentos?  Essa ação não pode ser desfeita ",
                callbackSim: function() {

                    var request = {};
                
                    executarServico(
                        "ProcessarExportacaoPagamentosRequest",
                        request,
                        function(data) {

                            popup({
                                titulo: "Exportação de Arquivo de Pagamentos",
                                icone: "sucesso",
                                mensagem: "Processo número " + data.CodigoProcesso + " criado para a exportação do arquivo de Pagamentos.",
                                callbackOK: function() { }
                            });
                        });
                    
                    },
                    callbackNao: function() { }
            });
        }
    };
    
    
    

    // Pede a inicializacao
    $.pagamento.lista.load();
});
