﻿//====================================================================================================================================
// site_configuracao_favorecido_detelhe.js
//====================================================================================================================================
$(document).ready(function() {
    //====================================================================================================================================
    // Namespace
    //====================================================================================================================================
    if (!$.favorecido)
        $.favorecido = {};
    //====================================================================================================================================
    // Funcoes do namespace 
    //====================================================================================================================================
    $.favorecido.detalhe =
    { variaveis: {}
        //====================================================================================================================================
        // INICIAR TELA: funcoes de inicializacao da tela
        //====================================================================================================================================
    , iniciar: function(callback) {
        //====================================================================================================================================
        // Tamanho do Dialog
        //====================================================================================================================================
        //1353
        $("#site_configuracao_favorecido_detalhe").dialog(
            {
                autoOpen: false,
                height: 600, //1379 600                
                width: 1200, //1379 1000, ELIMINANDO BARRA DE ROLAGEM ok
                modal: true,
                title: "Cadastro de Favorecido"
            });
        //====================================================================================================================================
        // Inicializa tabs
        //====================================================================================================================================
        $("#configuracao_favorecido_detalhe_tabs").tabs();
        //====================================================================================================================================
        // Verifica se o html esta carregado
        //====================================================================================================================================
        if ($("#site_configuracao_favorecido_detalhe").length == 0) {
            //====================================================================================================================================
            // Pede o html e insere na área temporaria
            //====================================================================================================================================
            $("#areaTemporaria").load("site_configuracao_favorecido_detalhe.aspx #site_configuracao_favorecido_detalhe", function() {
                $("#site_configuracao_favorecido_detalhe").appendTo("#areaComum");
                $.favorecido.detalhe.load(function() {
                    if (callback)
                        callback();
                });
            });
        }
        else {
            if (callback)
                callback();
        }
    }
        //====================================================================================================================================
        //  load: funcoes de inicializacao da tela
        //====================================================================================================================================
    , load: function() {
        $("#site_configuracao_favorecido_detalhe").Forms("initializeFields");
        $("#site_configuracao_favorecido_detalhe button").button();
        //====================================================================================================================================
        // ABA 1 - BOTÃO NOVA CONTA
        //====================================================================================================================================
        $("#cmdNovaConta").click(function() {
            $.contaFavorecido.detalhe.abrir();
        });
        //====================================================================================================================================
        // ABA 1 - BOTÃO EDITAR CONTA
        //====================================================================================================================================
        $("#cmdEditarConta").click(function() {
            var codigoContaFavorecido = $("#tbContasFavorecido").getGridParam("selrow");
            if (codigoContaFavorecido) {
                $.contaFavorecido.detalhe.abrir(codigoContaFavorecido);
            }
            else {
                popup(
                    { titulo: "EDITAR CONTA"
                    , icone: "erro"
                    , mensagem: "Selecione a conta do favorecido"
                    , callbackOK: function() {   //1353
                        $.favorecido.detalhe.listarProdutosSCF();
                        $.favorecido.detalhe.listarReferenciasSCF();
                        $.favorecido.detalhe.listarAssociacoes();
                        $.favorecido.detalhe.listarContas();
                        //$.favorecido.detalhe.listarContas2(); //1380/1381
                        //1353         
                    }
                    });
            }
        });
        //====================================================================================================================================
        // ABA 2 - BOTÃO ASSOCIAR
        //====================================================================================================================================
        $("#btnAsssociar").click(function() {
            var codigoContaFavorecido = $("#tbContasFavorecido2").getGridParam("selrow");
            var codigoProduto = $("#tbProdutosSCF").getGridParam("selrow");
            var codigoReferencia = $("#tbReferenciasSCF").getGridParam("selrow");
            var vOK = 'S';

            //====================================================================================================================================
            // CONSISTINDO
            //====================================================================================================================================
            if (!codigoContaFavorecido) {
                vOK = 'N';
                popup(
                    { titulo: "ASSOCIAR"
                    , icone: "erro"
                    , mensagem: "Selecione uma conta do favorecido"
                    , callbackOK: function() { }
                    });
            }

            if (!codigoProduto) {
                vOK = 'N';
                popup(
                    { titulo: "ASSOCIAR"
                    , icone: "erro"
                    , mensagem: "Selecione um produto"
                    , callbackOK: function() { }
                    });
            }

            if (!codigoReferencia) {
                vOK = 'N';
                popup(
                    { titulo: "ASSOCIAR"
                    , icone: "erro"
                    , mensagem: "Selecione uma Referencia de Cartao"
                    , callbackOK: function() { }
                    });
            }
            //====================================================================================================================================
            // SE OK ASSOCIA
            //====================================================================================================================================                
            if (vOK == 'S') {
                $.favorecido.detalhe.salvarContaFavorecidoProduto();
            }
        });
        //====================================================================================================================================
        // ABA 2 - BOTÃO DESASSOCIAR
        //====================================================================================================================================            
        $("#btnDesassociar").click(function() {
            var codigoProduto = $("#tbAssociacaoFavorecido").getGridParam("selrow");
            if (codigoProduto) {
                $.favorecido.detalhe.removerContaFavorecidoProduto();
            }
            else {
                popup(
                    { titulo: "DESASSOCIAR"
                    , icone: "erro"
                    , mensagem: "Selecione a conta do favorecido"
                    , callbackOK: function() { }
                    });
            }
        });
        //====================================================================================================================================
        // ABA 1 - BOTÃO REMOVER CONTA DO FAVORECIDO
        //====================================================================================================================================
        $("#cmdRemoverConta").click(function() {
            var codigoContaFavorecido = $("#tbContasFavorecido").getGridParam("selrow");
            if (codigoContaFavorecido) {
                var qtdtr = $("#tbContasFavorecido").jqGrid('getCell', codigoContaFavorecido, 'Transacoes');
                if (qtdtr != 0) {
                    popup(
                        { titulo: "REMOVER CONTA"
                        , icone: "erro"
                        , mensagem: "Conta " || codigoContaFavorecido || " do Favorecido possui" || qtdtr || " transações, não pode ser removida."
                        , callbackOK: function() { }
                        });
                }
                else {
                    var contaAssociada = false;
                    //1353 if ($.favorecido.detalhe.variaveis.listaProdutos) 
                    if ($.favorecido.detalhe.variaveis.listaAssociacao) {
                        //1353 var listaProdutos   = $.favorecido.detalhe.variaveis.listaProdutos;
                        var listaAssociacao = $.favorecido.detalhe.variaveis.listaAssociacao;
                        var contaFavorecido = $("#tbContasFavorecido").getRowData(codigoContaFavorecido);
                        for (var i = 0; i < listaAssociacao.length; i++) {
                            if
                                (listaAssociacao[i].Agencia == contaFavorecido.Agencia &&
                                    listaAssociacao[i].Agencia_CCI == contaFavorecido.Agencia_CCI &&
                                    listaAssociacao[i].Banco == contaFavorecido.Banco &&
                                    listaAssociacao[i].Conta == contaFavorecido.Conta &&
                                    listaAssociacao[i].Conta_CCI == contaFavorecido.Conta_CCI &&
                                    listaAssociacao[i].NomeProduto == contaFavorecido.NomeProduto &&
                                    listaAssociacao[i].Referencia == contaFavorecido.Referencia
                                ) {
                                contaAssociada = true;
                                popup(
                                    { titulo: "REMOVER CONTA"
                                    , icone: "erro"
                                    , mensagem: "A conta " || codigoContaFavorecido || " do favorecido possui produto e referencia vinculado, desvincule antes de remover."
                                    , callbackOK: function() { return false; }
                                    });
                            }
                        }
                    }
                    if (contaAssociada == false) {
                        popup(
                            { titulo: "REMOVER CONTA"
                            , icone: "atencao"
                            , mensagem: "Confirma remoção da conta " || codigoContaFavorecido || " ?"
                            , callbackNao: function() { }      //1382                     
                            , callbackSim: function() {
                                $.contaFavorecido.detalhe.remover(codigoContaFavorecido,
                                    function() {
                                        popup(
                                        { titulo: "REMOVER CONTA"
                                        , icone: "sucesso"
                                        , mensagem: "Conta do favorecido removida com sucesso!"
                                        , callbackOK: function() {   //1353
                                            $.favorecido.detalhe.listarProdutosSCF();
                                            $.favorecido.detalhe.listarReferenciasSCF();
                                            $.favorecido.detalhe.listarAssociacoes();
                                            $.favorecido.detalhe.listarContas();
                                            //$.favorecido.detalhe.listarContas2(); //1380/1381
                                            //1353                                             
                                        }
                                            //1382 ,   callbackNao: function() { }
                                        });
                                    });
                            }
                            });
                    }
                }
            }
            else {
                popup
                    ({ titulo: "REMOVER CONTA"
                    , icone: "erro"
                    , mensagem: "Selecione a conta do favorecido"
                    , callbackOK: function() { }
                    });
            }
        });
        //====================================================================================================================================
        // PRINCIPAL - Botao SALVAR - Cadastro Favorecido
        //====================================================================================================================================
        $("#site_configuracao_favorecido_detalhe").dialog(
            { autoOpen: false
                //1403 inibiu, tela assume a configuracao inicial,   height: 600
                //1403 inibiu, tela assume a configuracao inicial,   width: 730 
            , modal: true
            , title: "FAVORECIDO - Conta"
            , buttons:
            { "Salvar e Fechar": function() {
                $.favorecido.detalhe.salvar(function() {
                    popup(
                            { titulo: "SALVAR E FECHAR"
                            , icone: "sucesso"
                            , mensagem: "Cadastro Favorecido salvo com sucesso!"
                            , callbackOK: function() {
                                $.favorecido.detalhe.variaveis.editar = true;

                                // SCF1701 - ID 27
                                $.favorecido.detalhe.Fechar();
                            }
                            });
                });
            }
            , "Salvar": function() {
                $("#txtCnpjFavorecido").attr("disabled", true);
                $.favorecido.detalhe.salvar(function() {
                    popup(
                            { titulo: "SALVAR FAVORECIDO"
                            , icone: "sucesso"
                            , mensagem: "Cadastro Favorecido salvo com sucesso!"
                            , callbackOK: function() {
                                // SCF1701 - ID 27
                                $.favorecido.detalhe.substituirTransacoes($.favorecido.detalhe.variaveis.codigoFavorecido);
                                $.favorecido.detalhe.habilitaBotoes(true);

                                //1353
                                $.favorecido.detalhe.listarProdutosSCF();
                                $.favorecido.detalhe.listarReferenciasSCF();
                                $.favorecido.detalhe.listarAssociacoes();
                                $.favorecido.detalhe.listarContas();
                                //$.favorecido.detalhe.listarContas2(); //1380/1381
                                //1353                                             
                                $("#avisoSalvar").hide();
                                return true;
                            }
                            });
                });
            }
            , "Fechar": function() {
                // SCF1701 - ID 27
                $.favorecido.detalhe.Fechar();
            }
            }
            });
        //====================================================================================================================================
        // ABA 1 - tbContasFavorecido - CADASTRO DE CONTAS
        //====================================================================================================================================
        $("#tbContasFavorecido").jqGrid(
            { datatype: "clientSide"
            , mtype: "GET"
            , height: 300
            , width: 705 //1379 700 ELIMINANDO BARRA DE ROLAGEM ok
            , sortable: true
            , colNames: ["ID", "Banco", "Agencia_DE", "Conta_DE", "Agencia_PARA", "Conta_PARA", "Transacoes"] //1384
            , colModel:
                [{ name: "CodigoContaFavorecido", index: "CodigoContaFavorecido", width: 100, align: "left", key: true }
                , { name: "Banco", index: "Banco", width: 70, align: "left" }
                , { name: "Agencia_CCI", index: "Agencia_CCI", width: 100 }
                , { name: "Conta_CCI", index: "Conta_CCI", width: 100 }
                , { name: "Agencia", index: "Agencia", width: 100 }
                , { name: "Conta", index: "Conta", width: 100 }
                , { name: "Transacoes", index: "Transacoes", width: 100, hidden: false, align: "left", key: true} //1327
                ]
            , sortorder: "asc"
			, shrinkToFit: false
			, viewrecords: true
			, ondblClickRow: function(rowid, iRow, iCol, e) {
			    var codigoContaFavorecido = $("#tbContasFavorecido").getGridParam("selrow");
			    if (codigoContaFavorecido) {
			    }
			    else {
			        popup(
                        { titulo: "Atenção"
                        , icone: "atencao"
                        , mensagem: "Favor selecionar uma linha!"
                        , callbackOK: function() { }
                        });
			    }
			}
            , onSelectRow: function(id) {     //1327
                var objProcesso = $("#tbContasFavorecido").jqGrid('getRowData', id);
                //====================================================================================================================================
                // DESABILITA O REMOVER
                //====================================================================================================================================
                $("#site_configuracao_favorecido_detalhe #cmdRemoverConta").attr("class", "cmdEditarProcesso ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                $("#site_configuracao_favorecido_detalhe #cmdRemoverConta").attr("disabled", "true");
                //====================================================================================================================================
                // HABILITA O REMOVER SE NÃO TEM TRANSACOES
                //====================================================================================================================================
                if (objProcesso.Transacoes == 0) {
                    $("#site_configuracao_favorecido_detalhe #cmdRemoverConta").attr("class", "cmdRemoverConta ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    $("#site_configuracao_favorecido_detalhe #cmdRemoverConta").removeAttr("disabled");
                }
            }
            });
        //====================================================================================================================================
        // ABA 2 - tbAssociacaoFavorecido - ASSOCIACAO - Conta x Produto x Referencia
        //====================================================================================================================================
        //1353 $("#tbProdutos").jqGrid
        $("#tbAssociacaoFavorecido").jqGrid
            ({ datatype: "clientSide"
            , mtype: "GET"
            , height: 160
            , width: 845 //1379 740 ELIMINANDO BARRA DE ROLAGEM
            , sortable: true
            , colNames: ["Seq", "ID Conta", "Banco", "Agencia_DE", "Conta_DE", "Agencia_PARA", "Conta_PARA", "Codigo", "Produto", "Referencia"]
            , colModel:
                [{ name: "ID", index: "ID", key: true, width: 50 }
                , { name: "CodigoContaFavorecido", index: "CodigoContaFavorecido", width: 80 }
                , { name: "Banco", index: "Banco", width: 80 }
                , { name: "Agencia_CCI", index: "Agencia_CCI", width: 80} //1379
                , { name: "Conta_CCI", index: "Conta_CCI", width: 110} //1379
                , { name: "Agencia", index: "Agencia", width: 80 }
                , { name: "Conta", index: "Conta", width: 110 }
                , { name: "CodigoProduto", index: "CodigoProduto", hidden: true }
                , { name: "NomeProduto", index: "NomeProduto", align: "left", width: 110 }
                , { name: "Referencia", index: "Referencia", align: "left", width: 80} //1327
                ]
            , sortorder: "asc"
			, shrinkToFit: false
			, viewrecords: true
            });
        //====================================================================================================================================
        // ABA 2 - tbContasFavorecido2 - CONTAS - Para selecionar
        //====================================================================================================================================
        $("#tbContasFavorecido2").jqGrid
            ({ datatype: "clientSide"
            , mtype: "GET"
            , height: 90
            , width: 605 //1379 300 ELIMINANDO BARRA DE ROLAGEM
            , sortable: true
                /*
                1379 e 1384         
                ,   colNames: ["ID", "Banco", "Agencia", "Conta"]
                ,   colModel: 
                [
                { name: "CodigoContaFavorecido", index: "CodigoContaFavorecido", align: "left", key: true , width: 50},
                { name: "Banco", index: "Banco", align: "left" , width: 50},
                { name: "Agencia", index: "Agencia" , width: 50},
                { name: "Conta", index: "Conta" , width: 110}
                ]
                */
            , colNames: ["ID", "Banco", "Agencia_DE", "Conta_DE", "Agencia_PARA", "Conta_PARA", "Transacoes"]
            , colModel:
            [{ name: "CodigoContaFavorecido", index: "CodigoContaFavorecido", width: 100, hidden: false, align: "left", key: true }
            , { name: "Banco", index: "Banco", width: 70, align: "left" }
            , { name: "Agencia_CCI", index: "Agencia_CCI", width: 100 }
            , { name: "Conta_CCI", index: "Conta_CCI", width: 100 }
            , { name: "Agencia", index: "Agencia", width: 100 }
            , { name: "Conta", index: "Conta", width: 100 }
            , { name: "Transacoes", index: "Transacoes", width: 100, hidden: true} // SCF1701 - ID 27
            ]
            , sortorder: "asc"
			, shrinkToFit: false
			, viewrecords: true
            });
        //====================================================================================================================================
        // ABA 2 - tbProdutosSCF - PRODUTOS - Para selecionar
        //====================================================================================================================================
        $("#tbProdutosSCF").jqGrid
            ({ datatype: "clientSide"
            , mtype: "GET"
            , height: 90
            , width: 190 //1379 200 ELIMINANDO BARRA DE ROLAGEM ok
            , sortable: true
            , colNames: ["ID", "Produto"]
            , colModel:
                [{ name: "CodigoProduto", index: "CodigoProduto", align: "left", key: true, width: 50 }
                , { name: "NomeProduto", index: "NomeProduto", align: "left", width: 110 }
                ]
            , sortorder: "asc"
			, shrinkToFit: false
			, viewrecords: true
            });
        //====================================================================================================================================
        // ABA 2 - tbReferenciasSCF - REFERENCIA - Para selecionar
        //====================================================================================================================================
        $("#tbReferenciasSCF").jqGrid
            ({ datatype: "clientSide"
            , mtype: "GET"
            , height: 90
            , width: 170 //1379 200 ELIMINANDO BARRA DE ROLAGEM
            , sortable: true
            , colNames: ["ID", "Referencia"]
            , colModel:
                [{ name: "Valor", index: "Valor", align: "left", key: true, width: 50 }
                , { name: "Descricao", index: "Descricao", align: "left", width: 110 }
                ]
            , sortorder: "asc"
			, shrinkToFit: false
			, viewrecords: true
            });
    }
        //====================================================================================================================================
        // abrir: funcoes executadas na abertura da tela
        //====================================================================================================================================
    , abrir: function(codigoFavorecido, callback) {
        // SCF1701 - ID 27
        $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo = [];
        $.favorecido.detalhe.habilitaBotoes(false);
        $("#configuracao_favorecido_detalhe_tabs").tabs('select', 0);
        if (codigoFavorecido)
            $.favorecido.detalhe.variaveis.novoFavorecido = false;
        else
            $.favorecido.detalhe.variaveis.novoFavorecido = true;

        $.favorecido.detalhe.iniciar(function() {
            if (codigoFavorecido) {
                $("#site_configuracao_favorecido_detalhe").Forms("clearData");
                $.favorecido.detalhe.carregar(codigoFavorecido, function() {
                    $("#site_configuracao_favorecido_detalhe").dialog("open");
                });
            }
            else {
                $.favorecido.detalhe.variaveis.codigoFavorecido = null;
                $("#tbContasFavorecido").clearGridData(); //1353
                $("#tbContasFavorecido2").clearGridData(); //1353
                $("#tbProdutosSCF").clearGridData(); //1353
                $("#tbReferenciasSCF").clearGridData(); //1353
                $("#tbAssociacaoFavorecido").clearGridData(); //1353
                $("#site_configuracao_favorecido_detalhe").Forms("clearData");
                $("#site_configuracao_favorecido_detalhe").dialog("open");
            }
        });
    }
        //====================================================================================================================================
        // PRINCIPAL - Dados do FAVORECIDO 
        //====================================================================================================================================
    , carregar: function(codigoFavorecido, callback) {
        $.favorecido.detalhe.habilitaBotoes(true);
        $.favorecido.detalhe.variaveis.codigoFavorecido = codigoFavorecido;

        executarServico("ReceberFavorecidoRequest", { CodigoFavorecido: codigoFavorecido },
            function(data) {
                $.favorecido.detalhe.variaveis.favorecidoInfo = data.FavorecidoInfo;
                $("#site_configuracao_favorecido_detalhe").Forms("setData", { dataObject: data.FavorecidoInfo });
                if (callback)
                    callback();
            });
    }
        //====================================================================================================================================
        // PRINCIPAL - Botao SALVAR
        //====================================================================================================================================
    , salvar: function(callback) {
        var favorecidoInfo = null;
        $.favorecido.detalhe.validar(function() {
            var favorecidoInfo = null;
            favorecidoInfo = {};
            $("#site_configuracao_favorecido_detalhe").Forms("getData", { dataObject: favorecidoInfo });
            executarServico("ListarFavorecidoRequest", { FiltroCnpjFavorecido: favorecidoInfo.CnpjFavorecido },
                function(data) {
                    if (data.Resultado[0] && ($.favorecido.detalhe.variaveis.editar != true)) {
                        $("#txtCnpjFavorecido").removeAttr("disabled"); //1422
                        popup(
                        { titulo: "SALVAR FAVORECIDO"
                        , icone: "erro"
                        , mensagem: "Este CNPJ ja está associado a um favorecido existente."
                        , callbackOK: function() {
                            return true;
                        }
                        });

                    }
                    else {
                        if ($.favorecido.detalhe.variaveis.codigoFavorecido) {
                            favorecidoInfo = $.favorecido.detalhe.variaveis;
                            $("#site_configuracao_favorecido_detalhe").Forms("getData", { dataObject: favorecidoInfo });
                        }
                        else {
                            favorecidoInfo = {};
                            $("#site_configuracao_favorecido_detalhe").Forms("getData", { dataObject: favorecidoInfo });
                        }

                        $.favorecido.detalhe.salvarDados(favorecidoInfo, callback);
                    }
                });
        });
    }
    , salvarDados: function(favorecidoInfo, callback) {
        if ($.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo.length != 0 && $.favorecido.detalhe.variaveis.novoFavorecido == false)
            favorecidoInfo.FavorecidoBloqueado = 1;
        else
            favorecidoInfo.FavorecidoBloqueado = 0;

        executarServico("SalvarFavorecidoRequest", { FavorecidoInfo: favorecidoInfo },
                            function(data) {
                                $.favorecido.lista.listar();
                                $.favorecido.detalhe.variaveis.codigoFavorecido = data.FavorecidoInfo.CodigoFavorecido;
                                $.favorecido.detalhe.variaveis.editar = true; //1383
                                $.favorecido.detalhe.variaveis.favorecidoInfo = data.FavorecidoInfo; //1403
                                if (callback)
                                    callback();
                            });
    }
        //====================================================================================================================================
        // PRINCPAL - Botao REMOVER CONTA
        //====================================================================================================================================
    , remover: function(codigoFavorecido, forcarRemocao, callback) {
        if (codigoFavorecido) {
            executarServico("RemoverFavorecidoRequest", { CodigoFavorecido: codigoFavorecido, ForcarRemocao: forcarRemocao },
                function(data) {
                    if (callback)
                        callback(data);
                });
        }
        else {
            popup(
                { titulo: "REMOVER CONTA FAVORECIDO"
                , icone: "erro"
                , mensagem: "Selecione uma conta"
                , callbackOK: function() { }
                });
        }
    }
        //====================================================================================================================================
        // PRINCIPAL - Botao SALVAR - Chama VALIDAR
        //====================================================================================================================================
    , validar: function(callbackOk, callbackErro) {
        var criticas = new Array();
        criticas = $("#site_configuracao_favorecido_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });
        if (criticas[0]) {
            $.Forms.showValidationForm({ errors: criticas });
            callbackErro();
        }
        else {
            if ($("#txtCnpjFavorecido").val().length != 15) {
                popup(
                    { titulo: "CNPJ DO FAVORECIDO"
                    , icone: "erro"
                    , mensagem: "O CNPJ deve ter 15 numeros."
                    , callbackOK: function() {
                        $("#txtCnpjFavorecido").attr("disabled", false);
                    }
                    });
            }
            else
                callbackOk();
        }
    }
        //====================================================================================================================================
        //  ABA 1 - tbContasFavorecido - listarContas
        //====================================================================================================================================
    , listarContas: function(callback) {
        if ($.favorecido.detalhe.variaveis.codigoFavorecido) {
            executarServico("ListarContaFavorecidoRequest", { FiltroCodigoFavorecido: $.favorecido.detalhe.variaveis.codigoFavorecido },
                function(data) {
                    $("#tbContasFavorecido").clearGridData();   //1376
                    $("#tbContasFavorecido2").clearGridData(); //1377
                    for (var i = 0; i < data.Resultado.length; i++) {
                        $("#tbContasFavorecido").jqGrid("addRowData", data.Resultado[i].CodigoContaFavorecido, data.Resultado[i]);
                        $("#tbContasFavorecido2").jqGrid("addRowData", data.Resultado[i].CodigoContaFavorecido, data.Resultado[i]);
                    }
                    if (callback)
                        callback();
                });
        }
    }
        //====================================================================================================================================
        //  ABA 2 - tbAssociacaoFavorecido - listarAssociacoes
        //====================================================================================================================================
    , listarAssociacoes: function(callback) {
        if ($.favorecido.detalhe.variaveis.codigoFavorecido) {
            executarServico("ListarContaFavorecidoProdutoRequest", { FiltroCodigoFavorecido: $.favorecido.detalhe.variaveis.codigoFavorecido },
                function(data) {
                    $("#tbAssociacaoFavorecido").clearGridData();
                    for (var i = 0; i < data.Resultado.length; i++) {
                        $("#tbAssociacaoFavorecido").jqGrid("addRowData", data.Resultado[i].ID, data.Resultado[i]);
                    }
                    if (callback)
                        callback();
                });
        }
    }
        //====================================================================================================================================
        //  ABA 2 - tbProdutosSCF - listarProdutosSCF
        //====================================================================================================================================
    , listarProdutosSCF: function(callback) {
        if ($.favorecido.detalhe.variaveis.codigoFavorecido) {
            executarServico("ListarProdutoRequest", {},
                function(data) {
                    $("#tbProdutosSCF").clearGridData();
                    for (var i = 0; i < data.Resultado.length; i++) {
                        $("#tbProdutosSCF").jqGrid("addRowData", data.Resultado[i].CodigoProduto, data.Resultado[i]);
                    }
                    if (callback)
                        callback();
                });
        }
    }
        //====================================================================================================================================
        //  ABA 2 - tbReferenciasSCF - listarReferenciasSCF
        //====================================================================================================================================
    , listarReferenciasSCF: function(callback) {
        if ($.favorecido.detalhe.variaveis.codigoFavorecido) {
            executarServico("ReceberListaRequest", { MnemonicoLista: "Referencia" },
                function(data) {
                    $("#tbReferenciasSCF").clearGridData();
                    $.favorecido.detalhe.variaveis.listarReferenciasSCF = data.ListaInfo.Itens;
                    for (var i = 0; i < data.ListaInfo.Itens.length; i++) {
                        $("#tbReferenciasSCF").jqGrid("addRowData", data.ListaInfo.Itens[i].Valor, data.ListaInfo.Itens[i]);
                    }
                    if (callback)
                        callback();
                });
        }
    }
        //====================================================================================================================================
        // ABA 2 - Botao ASSOCIAR - btnAsssociar
        //====================================================================================================================================
    , salvarContaFavorecidoProduto: function(callback) {
        var codigoContaFavorecido = $("#tbContasFavorecido2").getGridParam("selrow");
        var numeroTransacoes = $("#tbContasFavorecido2").jqGrid('getCell', codigoContaFavorecido, 'Transacoes');
        var codigoProduto = $("#tbProdutosSCF").getGridParam("selrow");
        var codigoReferencia = $("#tbReferenciasSCF").getGridParam("selrow");

        var contaFavorecidoProdutoInfo = null;
        contaFavorecidoProdutoInfo = {};
        contaFavorecidoProdutoInfo.CodigoProduto = codigoProduto;
        contaFavorecidoProdutoInfo.CodigoContaFavorecido = codigoContaFavorecido;
        contaFavorecidoProdutoInfo.Referencia = codigoReferencia;

        //====================================================================================================================================
        // FAVORECIDO ORIGINAL - Consiste se já existe o relacionamento DO PRODUTO e REFERENCIA para o FAVORECIDO em qualquer CONTA
        //====================================================================================================================================
        if ($.favorecido.detalhe.variaveis.favorecidoInfo.FavorecidoOriginal) {
            executarServico("ListarContaFavorecidoProdutoRequest"
                , { FiltroCodigoFavorecido: $.favorecido.detalhe.variaveis.codigoFavorecido
                    , FiltroCodigoProduto: contaFavorecidoProdutoInfo.CodigoProduto
                    , FiltroReferencia: contaFavorecidoProdutoInfo.Referencia
                },
                function(data) {
                    if (data.Resultado.length > 0) {
                        if (data.Resultado[0].CodigoContaFavorecido) {
                            popup(
                            { titulo: "ASSOCIAR"
                            , icone: "erro"
                            , mensagem: "Favorecido Original, já tem relacionalmento de conta neste Produto e Referencia",
                                callbackOK: function() { }
                            });
                        }
                    }
                    else {
                        // SCF1701 - ID 27
                        contaFavorecidoProdutoInfo.Transacoes = numeroTransacoes;
                        $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo.push(contaFavorecidoProdutoInfo);

                        executarServico("SalvarContaFavorecidoProdutoRequest"
                        , { ContaFavorecidoProdutoInfo: contaFavorecidoProdutoInfo
                        },
                        function(data) {
                            $.favorecido.detalhe.listarAssociacoes(callback);
                        });
                    }
                });
        }
        //====================================================================================================================================
        // FAVORECIDO NAO ORIGINAL - Pode inserir o registro
        //====================================================================================================================================
        else {
            // SCF1701 - ID 27
            contaFavorecidoProdutoInfo.Transacoes = numeroTransacoes;
            $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo.push(contaFavorecidoProdutoInfo);

            executarServico("SalvarContaFavorecidoProdutoRequest", { ContaFavorecidoProdutoInfo: contaFavorecidoProdutoInfo },
                function(data) {
                    $.favorecido.detalhe.listarAssociacoes(callback);
                });
        }
    }
        //====================================================================================================================================
        // ABA 2 - Botao DESASSOCIAR - btnDesassociar
        //====================================================================================================================================
    , removerContaFavorecidoProduto: function(callback) {
        var rowid = $("#tbAssociacaoFavorecido").getGridParam("selrow");
        var contaFavorecidoProduto = $("#tbAssociacaoFavorecido").getRowData(rowid);
        if (contaFavorecidoProduto) {
            $.favorecido.detalhe.removerContaFavorecidoProdutoNovos(contaFavorecidoProduto);

            executarServico("RemoverContaFavorecidoProdutoRequest", { CodigoContaFavorecido: contaFavorecidoProduto.CodigoContaFavorecido, CodigoProduto: contaFavorecidoProduto.CodigoProduto, Referencia: contaFavorecidoProduto.Referencia },
                function(data) {
                    $.favorecido.detalhe.listarAssociacoes(callback);
                });
        }
        else {
            popup(
                { titulo: "DESASSOCIAR"
                , icone: "erro"
                , mensagem: "Favor selecionar uma linha!"
                , callbackOK: function() { }
                });
        }
    }
        // SCF1701 - ID 27
    , removerContaFavorecidoProdutoNovos: function(contaFavorecidoProduto, callback) {
        for (var i = 0; i < $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo.length; i++) {
            var dados = $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo[i];
            if (dados.CodigoContaFavorecido == contaFavorecidoProduto.CodigoContaFavorecido
            && dados.CodigoProduto == contaFavorecidoProduto.CodigoProduto
            && dados.Referencia == contaFavorecidoProduto.Referencia) {
                $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo.splice(i, 1);
            }
        }
    }
        //SCF1701 - ID 27
    , substituirTransacoes: function(codigoFavorecido) {
        var contaFavorecidoProdutoInfo = $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo;
        var soma = [].reduce.call(contaFavorecidoProdutoInfo, function(somatorio, el) {
            return somatorio + parseInt(el.Transacoes, 10) || 0;
        }, 0);

        if (contaFavorecidoProdutoInfo.length != 0 && $.favorecido.detalhe.variaveis.novoFavorecido == false) {
            popup(
            { titulo: "SALVAR FAVORECIDO"
            , icone: "info"
            , mensagem: "Deseja atualizar as transações deste Favorecido com as novas associações?"
            , callbackSim: function() {
                // SCF1701 - ID 27
                executarServico("SubstituirContaFavorecidoProdutoRequest", {
                    CodigoFavorecido: codigoFavorecido,
                    ListaFavorecidoProduto: contaFavorecidoProdutoInfo
                },
                function(data) {
                    popup({
                        titulo: "Salvar Conta",
                        icone: "sucesso",
                        mensagem: "Processo número " + data.CodigoProcesso + " criado para a Substituição das das Trasanações do Favorecido.",
                        callbackOK: function() {
                            // SCF1701 - ID 27
                            $.favorecido.detalhe.Fechar();
                        }
                    });
                });
            }
            , callbackNao: function() {
                //Desbloqueando Favorecido
                var favorecidoInfo = $.favorecido.detalhe.variaveis;
                $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo = [];
                $.favorecido.detalhe.salvarDados(favorecidoInfo, null);
                $.favorecido.lista.listar();
            }
            });
        }
        $.favorecido.detalhe.variaveis.contaFavorecidoProdutoInfo = [];
    }
    , Fechar: function() {
        // SCF1701 - ID 27
        $.favorecido.detalhe.substituirTransacoes($.favorecido.detalhe.variaveis.codigoFavorecido);

        //1403
        //        $.favorecido.detalhe.variaveis.codigoFavorecido = null;
        $("#tbContasFavorecido").clearGridData();
        $("#tbContasFavorecido2").clearGridData();
        $("#tbProdutosSCF").clearGridData();
        $("#tbReferenciasSCF").clearGridData();
        $("#tbAssociacaoFavorecido").clearGridData();
        $("#site_configuracao_favorecido_detalhe").Forms("clearData");
        //1403
        $("#site_configuracao_favorecido_detalhe").dialog("close");
        $.favorecido.lista.listar(); //1353
    }
    , habilitaBotoes: function(habilitar) {
        if (habilitar == true) {
            //====================================================================================================================================
            // HABILITA O BOTÕES
            //====================================================================================================================================
            $("#site_configuracao_favorecido_detalhe #cmdNovaConta").removeAttr("disabled");
            $("#site_configuracao_favorecido_detalhe #cmdNovaConta").attr("class", "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
            $("#site_configuracao_favorecido_detalhe #cmdEditarConta").removeAttr("disabled");
            $("#site_configuracao_favorecido_detalhe #cmdEditarConta").attr("class", "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
            $("#site_configuracao_favorecido_detalhe #cmdRemoverConta").removeAttr("disabled");
            $("#site_configuracao_favorecido_detalhe #cmdRemoverConta").attr("class", "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
        }
        else {
            //====================================================================================================================================
            // DESABILITA O BOTÕES
            //====================================================================================================================================
            $("#site_configuracao_favorecido_detalhe #cmdNovaConta").attr("disabled", "true");
            $("#site_configuracao_favorecido_detalhe #cmdNovaConta").attr("class", "ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
            $("#site_configuracao_favorecido_detalhe #cmdEditarConta").attr("disabled", "true");
            $("#site_configuracao_favorecido_detalhe #cmdEditarConta").attr("class", "ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
            $("#site_configuracao_favorecido_detalhe #cmdRemoverConta").attr("disabled", "true");
            $("#site_configuracao_favorecido_detalhe #cmdRemoverConta").attr("class", "ui-state-disabled ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
        }
    }
    };
    //====================================================================================================================================
    // Pede a inicializacao
    //====================================================================================================================================
    $.favorecido.detalhe.iniciar();
});
//====================================================================================================================================
// site_configuracao_favorecido_detelhe.js
//====================================================================================================================================
