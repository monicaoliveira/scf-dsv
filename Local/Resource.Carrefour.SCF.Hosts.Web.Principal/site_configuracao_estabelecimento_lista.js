$(document).ready(function() {

    // Namespace
    if (!$.estabelecimento)
        $.estabelecimento = {};

    // Funcoes do namespace 
    $.estabelecimento.lista = {

        variaveis: {},

        // =================================================================================
        //  load: funcoes de inicializacao da tela
        // =================================================================================
        load: function() {

            // --------------------------------------------------------------------
            // Insere o subtítulo
            // --------------------------------------------------------------------
            $("#subtitulo").html("Configurações > Cadastro de Estabelecimentos");

            // Estilo dos botões
            $("#site_configuracao_estabelecimento_lista button").button();

            $("#site_configuracao_estabelecimento_lista").Forms("initializeFields");

            // --------------------------------------------------------------------
            // BOTÃO IMPORTAR ESTABELECIMENTO
            // --------------------------------------------------------------------
            $("#cmdImportarEstabelecimento").click(function() {

                // Limpa campos
                $("#importacao").Forms("clearData");

                // Preenche caminho do arquivo default
                $("#txtCaminhoArquivo").val("");

                // Abre dialog de importação de estabelecimentos
                $("#importacao").dialog("open");
            });

            $("#cmdFiltrar").click(function() {

                $.estabelecimento.lista.listar();
            });

            // --------------------------------------------------------------------
            // BOTÃO ATUALIZAR LISTA
            // --------------------------------------------------------------------
            $("#cmdAtualizarListaEstabelecimento").click(function() {

                // Atualiza lista de estabelecimentos
                $.estabelecimento.lista.listar();
            });

            // --------------------------------------------------------------------
            // BOTÃO NOVO ESTABELECIMENTO
            // --------------------------------------------------------------------
            $("#cmdNovoEstabelecimento").click(function() {

                //Crio variavel global de edicao
                $.estabelecimento.detalhe.variaveis.editar = false;

                // Abre dialog de detalhe
                $.estabelecimento.detalhe.abrir();
            });

            // --------------------------------------------------------------------
            // BOTÃO EDITAR ESTABELECIMENTO
            // --------------------------------------------------------------------
            $("#cmdEditarEstabelecimento").click(function() {

                //Crio variavel global de edicao
                $.estabelecimento.detalhe.variaveis.editar = true;

                // Pega o código do estabelecimento
                var codigoEstabelecimento = $("#tbEstabelecimento").getGridParam("selrow");
                if (codigoEstabelecimento) {

                    // Abre dialog de detalhe
                    $.estabelecimento.detalhe.abrir(codigoEstabelecimento);
                }
                else {

                    // Pede para selecionar a linha
                    popup({
                        titulo: "Atenção",
                        icone: "atencao",
                        mensagem: "Favor selecionar uma linha!",
                        callbackOK: function() { }
                    });
                }
            });

            // --------------------------------------------------------------------
            // BOTÃO REMOVER ESTABELECIMENTO
            // --------------------------------------------------------------------
            $("#cmdRemoverEstabelecimento").click(function() {

                // Pega o código do estabelecimento
                var codigoEstabelecimento = $("#tbEstabelecimento").getGridParam("selrow");

                executarServico(
                "ReceberEstabelecimentoRequest",
                { CodigoEstabelecimento: codigoEstabelecimento },
                function(data) {
                    if (data.EstabelecimentoInfo) {

                        if (codigoEstabelecimento) {

                            // Pede confirmação de exclusão
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "Tem certeza que deseja remover este Estabelecimento?",
                                callbackSim: function() {

                                    // Pede para remover
                                    $.estabelecimento.detalhe.remover(codigoEstabelecimento, function(ret) {

                                        if (ret.DescricaoResposta) {

                                            //$("#popupSimNao_mensagem").html("");
                                        
                                            // Mostra popup de sucesso
                                            popup({
                                                titulo: "Remover Estabelecimento",
                                                icone: "erro",
                                                mensagem: ret.DescricaoResposta,
                                                callbackOK: function() {

                                                }
                                            });
                                        } else {
                                            // Mostra popup de sucesso
                                            popup({
                                                titulo: "Remover Estabelecimento",
                                                icone: "sucesso",
                                                mensagem: "Estabelecido removido com sucesso!",
                                                callbackOK: function() { $.estabelecimento.lista.listar(); }
                                            });
                                        }
                                    });
                                },
                                callbackNao: function() { }
                            });
                        } else {

                            // Pede para selecionar a linha
                            popup({
                                titulo: "Atenção",
                                icone: "atencao",
                                mensagem: "Favor selecionar uma linha!",
                                callbackOK: function() {
                                }
                            });
                        }
                    } else {
                        //Avisa o usuario que o plano já foi excluido.
                        popup({
                            titulo: "Atenção",
                            icone: "atencao",
                            mensagem: "Estabelecimento já foi excluido!",
                            callbackOK: function() {
                                $.estabelecimento.lista.listar(null, true);
                            }
                        });
                    }

                });
            });


            // --------------------------------------------------------------------
            // Inicializa grid de lista 
            // 1351 inclusao do nome favorecido, grupo e subgrupo
            // --------------------------------------------------------------------
            $("#tbEstabelecimento").jqGrid(
            {       datatype: "clientSide"
                ,    mtype: "GET"
                ,    height: 160
                ,    width: 320
                ,    rowNum: 100000
                ,   colNames: ["Codigo", "CNPJ", "Razão Social", "Grupo", "SubGrupo", "Favorecido", "Cod.CSU", "Cod.Sitef"]
                ,   colModel: 
                    [
                    { name: "CodigoEstabelecimento", index: "CodigoEstabelecimento", sorttype: "integer", width: 50, align: "center", key: true },
                    { name: "CNPJ", index: "CNPJ", width: 150 },
                    { name: "RazaoSocial", index: "RazaoSocial", width: 400 },
                    { name: "NomeGrupo", index: "NomeGrupo", width: 150 },
                    { name: "NomeSubGrupo", index: "NomeSubGrupo", width: 150 },
                    { name: "NomeFavorecido", index: "NomeFavorecido", width: 250 },
                    { name: "EstabelecimentoCSU", index: "EstabelecimentoCSU",  width: 100, align: "center" },
                    { name: "EstabelecimentoSitef", index: "EstabelecimentoSitef", width: 100, align: "center" }                  ]
                , sortorder: "asc"
				, shrinkToFit: false
				, viewrecords: true
				, ondblClickRow: function(rowid, iRow, iCol, e) 
				    {
                    var codigoEstabelecimento = $("#tbEstabelecimento").getGridParam("selrow"); ;
                    if (codigoEstabelecimento) 
                        {   $.estabelecimento.detalhe.abrir(codigoEstabelecimento);
                        }
                    else 
                        {   popup(
                            {
                                titulo: "Atenção",
                                icone: "erro",
                                mensagem: "Favor selecionar uma linha!",
                                callbackOK: function() { }
                            });
                        }
                    }
                });

            // --------------------------------------------------------------------
            // Inicializa o dialog de erro de leitura
            // --------------------------------------------------------------------
            $("#dialogErroLayout").dialog({
                autoOpen: false,
                height: 230,
                width: 850,
                modal: true,
                title: "Erro Layout",
                buttons: {
                    "Fechar": function() {

                        // Fecha o dialog 
                        $("#dialogErroLayout").dialog("close");
                    }
                }
            });

            // --------------------------------------------------------------------
            // Inicializa o dialog de importação de estabelecimentos
            // --------------------------------------------------------------------
            $("#importacao").dialog({
                autoOpen: false,
                height: 130,
                width: 500,
                modal: true,
                title: "Importação de Estabelecimentos",
                buttons: {
                    "Importar": function() {

                        // Pede para importar
                        $.estabelecimento.lista.importar(function(data) {

                            // Contém críticas?
                            var mensagem = "Estabelecimentos importados com sucesso!";
                            var icone = "sucesso";
                            var largura = 400;
                            var altura = 150;
                            var redimensionar = true;
                            if (data && data.Criticas[0]) {
                                // Monta mensagem
                                largura = 580;
                                altura = 150 + (data.Criticas.length * 10);
                                redimensionar = true;
                                icone = "atencao";
                                mensagem = "Estabelecimentos importados com sucesso!<br/><br/>";
                                mensagem += "ATENÇÃO! Ocorreram as seguintes críticas:<br/>";
                                for (var i = 0; i < data.Criticas.length; i++) {
                                    mensagem += "<br/>" + data.Criticas[i].Descricao;
                                }
                            }


                            if (data.ErroLayout == false) {

                                // Mostra popup de sucesso
                                popup({
                                    titulo: "Importar Estabelecimentos",
                                    icone: icone,
                                    mensagem: mensagem,
                                    resizable: redimensionar,
                                    width: largura,
                                    height: altura,
                                    callbackOK: function() {
                                        // Atualizo a lista
                                        $.estabelecimento.lista.listar(function() {
                                            // Fecha dialog de importação de estabelecimentos
                                            $("#importacao").dialog("close");
                                            // Preenche os combos do detalhe
                                            $.estabelecimento.detalhe.carregarCombosGrupoFavorecido();
                                        });
                                    }
                                });
                            } else {
                                $("#dialogErroLayout").dialog("open");
                            }
                        });
                    },
                    "Fechar": function() {

                        // Fecha o dialog 
                        $("#importacao").dialog("close");
                    }
                }
            });

            // --------------------------------------------------------------------
            // Pede lista de estabelecimentos
            // --------------------------------------------------------------------
            $.estabelecimento.lista.listar();

            // --------------------------------------------------------------------
            // Trata o redimensionamento da tela
            // --------------------------------------------------------------------
            $(window).bind("resize", function() {
                $("#tbEstabelecimento").setGridWidth($(window).width() - margemEsquerda);
                $("#tbEstabelecimento").setGridHeight($(window).height() - margemRodape);
            }).trigger("resize");
        },

        // =================================================================================
        //  listar: preenche lista de estabelecimentos cadastrados
        // =================================================================================
        listar: function(callback) {

            // Monta o filtro
            var request = {};
            var cnpj = $("#txtFiltroCnpj").val();
            if (cnpj != "")
                request.FiltroCodigoCnpj = cnpj;
            var razaoSocial = $("#txtFiltroRazaoSocial").val();
            if (razaoSocial != "")
                request.FiltroRazaoSocial = razaoSocial;
            var codigoCsu = $("#txtFiltroCodigoCsu").val();
            if (codigoCsu != "")
                request.FiltroCodigoCsu = codigoCsu;
            var codigoSitef = $("#txtFiltroCodigoSitef").val();
            if (codigoSitef != "")
                request.FiltroCodigoSitef = codigoSitef;
                
            //1366request.FiltroMaxLinhas = 100;

            // --------------------------------------------------------------------
            // Pede lista ao serviço
            // --------------------------------------------------------------------
            executarServico( "ListarEstabelecimentoRequest", request, function(data) 
            {       $("#tbEstabelecimento").clearGridData();
                    for (var i = 0; i < data.Resultado.length; i++) 
                    {   $("#tbEstabelecimento").jqGrid("addRowData", data.Resultado[i].CodigoEstabelecimento, data.Resultado[i]);
                    }
                    if (callback)
                        callback();
            });
        },

        // =================================================================================
        //  importar: Importa estabelecimentos do arquivo txt
        // =================================================================================
        importar: function(callback) {

            // --------------------------------------------------------------------
            // Cria request
            // --------------------------------------------------------------------
            var request = {};

            // --------------------------------------------------------------------
            // Preenche request
            // --------------------------------------------------------------------
            $("#importacao").Forms("getData", { dataObject: request });

            // --------------------------------------------------------------------
            // Solicita servico importacao
            // --------------------------------------------------------------------
            executarServico(
                "ImportarEstabelecimentoRequest",
                request,
                function(data) {

                    // Chama callback
                    if (callback)
                        callback(data);
                });
        }
    };

    // Pede a inicializacao
    $.estabelecimento.lista.load();
});