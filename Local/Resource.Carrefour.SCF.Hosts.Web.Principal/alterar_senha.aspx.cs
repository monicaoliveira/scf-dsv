﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Framework.Library; // INFORMANDO O AMBIENTE
using Resource.Framework.Library.Db.Oracle; // INFORMANDO O AMBIENTE

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class alterarSenha : System.Web.UI.Page
    {
        //private static OracleDbLibConfig _config = null; // INFORMANDO O AMBIENTE
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{   _config = GerenciadorConfig.ReceberConfig<OracleDbLibConfig>();// INFORMANDO O AMBIENTE
            //    lblAmbiente.Text = _config.ConnectionStringName.ToString();// INFORMANDO O AMBIENTE

            //    lblVersao.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major.ToString() + "." +
            //                     System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString() + "." +
            //                     System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build.ToString() + "." +
            //                     System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Revision.ToString();
            //}
            if (this.Request["senhaExpirada"] == "1")
            {
                lblSenhaExpirada.Text = "Senha Expirada ou Resetada, favor alterar..."; //1432 alteração do texto
            }
        }
    }
}
