﻿function relatorio_transacoes_load()
{
    // Namespace
    if (!$.relatorio)
        $.relatorio = {};

    // Funcoes do namespace
    $.relatorio.transacao = {

        variaveis: {},

        //=================================================================================
        // load: funcoes de inicializacao da tela
        //=================================================================================
        load: function(callback) {
            
            //-------------------------------------------------------------------
            // Pega informações da tela dos filtros
            //-------------------------------------------------------------------
            if (window.opener.$(".ObjetoJson").val() != "") {
                $("#ObjetoJson").val(window.opener.$(".ObjetoJson").val());
                $("form").submit();
            }


//            // Executa a chamada ajax
//            $.ajax({
//                type: "POST",
//                url: "relatorio_transacoes.aspx/PreencherRelatorio",
//                dataType: "json",
//                data: {
//                    Acao: "executar",
//                    TipoMensagem: tipoMensagem,
//                    Mensagem: window.opener.$(".ObjetoJson").val()
//                },
//                cache: false,
//                success: executarRelatorioCallBack,
//                error: function(jqXHR, textStatus, errorThrown) {
//                    alert("Erro na chamada HTTP: " + textStatus.toString() + ";" + errorThrown.toString() + ";" + jqXHR.toString());
//                }
//            });            

            if (callback)
                callback();
        }
    }


    function executarRelatorioCallBack(data) {
        if (data.StatusResposta != "OK") {
        
        
        }

        // Escondo janela de "carregando"
        hideLoadingDialog();    
    }
    
    
    // Chama load da página
    $.relatorio.transacao.load(function() { hideLoadingDialog(); });
}