﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Hosts.Web.Principal
{
    public partial class site_configuracao_subgrupo_detalhe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack){
                MensagemResponseBase retorno = null;
                //-------------------------------------------------------------------------------------------
                // CARREGA COMBO DE GRUPOS (EMPRESA)
                //-------------------------------------------------------------------------------------------

                try
                {
                    if (Session["CodigoSessao"] != null)
                    {
                        // Recebe lista de grupos
                        ListarEmpresaGrupoResponse grupos =
                            Mensageria.Processar<ListarEmpresaGrupoResponse>(
                                new ListarEmpresaGrupoRequest()
                                {
                                    CodigoSessao = Session["CodigoSessao"].ToString()
                                });

                        // Preenche combo
                        rptCmbCodigoEmpresaGrupo.DataSource = grupos.Resultado;
                        rptCmbCodigoEmpresaGrupo.DataBind();
                    }
                    else
                    {
                        retorno = new MensagemErroResponse()
                        {
                            StatusResposta = MensagemResponseStatusEnum.SessaoInvalida,
                            DescricaoResposta = "Sessão Inválida"
                        };
                    }
                }
                catch (Exception ex)
                {
                    retorno = new MensagemErroResponse()
                    {
                        StatusResposta = MensagemResponseStatusEnum.ErroPrograma,
                        DescricaoResposta = ex.ToString()
                    };
                }
            }
        }
    }
}
