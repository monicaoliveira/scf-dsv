﻿// =====================================================================================
// 1309 Refazendo teLA. Ocorria erro de CALLBACK no COMUMSERVICO.JS
// =====================================================================================

$(document).ready(function() 
{   if (!$.referencia)
        $.referencia = {};
    $.referencia.detalhe = 
    {   variaveis: {}
    //==========================================================================
    // ABRIR - Chamado pela tela LISTA
    //==========================================================================
    ,   abrir: function(callback) 
        {   $('#cmbNomeReferencia option').each(function(i, option) {$(option).remove();})
        
            codigoReferencia = null;
            $.referencia.detalhe.variaveis = {};
            
            $.referencia.detalhe.limparGrids();
            $.referencia.detalhe.esconderGrids();

            $("#site_configuracao_referencia_dominio_detalhe").Forms("clearData");
            $("#cmbSituacao").removeAttr("disabled");
            $("#cmbNomeReferencia").removeAttr("disabled");

            //==========================================================================
            // oculta as grids
            //==========================================================================
            $("#dvTbMeioPagamento").hide();
            $("#dvTbValorReferencia").hide();
            $("#dvTbTipoAjuste").hide();
            $("#dvTbReferencia").hide(); 
            
            //==========================================================================
            // Abre dialog de detalhe
            //==========================================================================
            $(":button:contains('Salvar')").removeAttr("disabled");
            $(":button:contains('Salvar')").removeClass("ui-state-disabled");
            $("#site_configuracao_referencia_dominio_detalhe").dialog({title: "INCLUSAO - Detalhe do Link de Domínio"});
            $("#site_configuracao_referencia_dominio_detalhe").dialog("open");
        }
    //==========================================================================
    // PREPARA A TELA
    //==========================================================================
    ,   preparar: function()
        {   if ($("#site_configuracao_referencia_dominio_detalhe").length == 0) 
            {   $("#areaTemporaria").load("site_configuracao_referencia_dominio_detalhe.aspx #site_configuracao_referencia_dominio_detalhe", function() 
                {   $("#site_configuracao_referencia_dominio_detalhe").appendTo("#areaComum");
                    $("#site_configuracao_referencia_dominio_detalhe").Forms("clearData");
                    $.referencia.detalhe.load();
                });
            }
        }
    //==========================================================================
    // LIMPA GRIDS
    //==========================================================================
    ,   limparGrids: function()
        {   $("#tbListaItem").jqGrid("clearGridData");
            $("#tbValorReferenciaMeioPagamento").jqGrid("clearGridData");
            $("#tbValorReferenciaProduto").jqGrid("clearGridData");
            $("#tbTipoAjuste").jqGrid("clearGridData");
            $("#tbReferencia").jqGrid("clearGridData");
        }
    //==========================================================================
    // ESCONDE GRIDS
    //==========================================================================
    ,   esconderGrids: function()
        {   $("#dvTbMeioPagamento").hide();
            $("#dvTbValorReferencia").hide();
            $("#dvTbTipoAjuste").hide();
            $("#dvTbReferencia").hide(); 
        }
    //==========================================================================
    // CARREGA DADOS
    //==========================================================================
    ,   load: function() 
        {   $("#site_configuracao_referencia_dominio_detalhe").Forms("initializeFields");
            $("#site_configuracao_referencia_dominio_detalhe button").button();
           
            //==========================================================================
            // TELA PRINCIPAL - Detalhe INCLUSAO
            //==========================================================================
            // Inicializa o dialog do detalhe
            $("#site_configuracao_referencia_dominio_detalhe").dialog(
            {   autoOpen: false
            ,   height: 550
            ,   width: 640
            ,   modal: true
            ,   buttons: 
                {   "Salvar": function() 
                    {   $.referencia.detalhe.salvar(function() 
                        {   popup(
                            {   titulo: "SALVAR"
                            ,   icone: "sucesso"
                            ,   mensagem: "Sucesso, dados salvos!"
                            ,   callbackOK: function() 
                                {   $("#site_configuracao_referencia_dominio_detalhe").dialog("close");
                                    $.referencia.lista.listar();
                                }
                            });
                        });
                    }
                ,   "Fechar": function() 
                    {   $("#site_configuracao_referencia_dominio_detalhe").dialog("close");
                    }
                }
            });

            //==========================================================================
            // GRID DOMINIO PRINCIPAL - MONTAR
            //==========================================================================
            $("#tbListaItem").jqGrid(
            {   datatype: "clientSide"
            ,   mtype: "GET"
            ,   height: 200
            ,   width: 570
            ,   shrinkToFit: false //1309
            ,   colNames: ["CodigoListaItem", "Codigo", "Valor", "Descrição"]
            ,   colModel: 
                [   { name: "CodigoListaItem"   , index: "CodigoListaItem"  , width: 100 , key: true, hidden: true  }
                ,   { name: "CodigoLista"       , index: "CodigoLista"      , width: 100 , sortable: false}
                ,   { name: "Valor"             , index: "Valor"            , width: 100 , classes: "grid[property[Valor]]"     , sortable: false}
                ,   { name: "Descricao"         , index: "Descricao"        , width: 330 , classes: "grid[property[Descricao]]" , sortable: false}
                ]
            });

            //==========================================================================
            // GRID DOMINIO SECUNDARIO - Do dominio REFERENCIA x MEIO PAGAMENTO
            //==========================================================================
            $("#tbValorReferenciaMeioPagamento").jqGrid(
            {   datatype: "clientSide"
            ,   mtype: "GET"
            ,   height: 100 //1309
            ,   width: 570
            ,   sortable: true
            ,   shrinkToFit: false
            ,   colNames: ["CodigoListaItem", "Valor", "Descrição"]
            ,   colModel: 
                [   { name: "CodigoListaItem"   , index: "CodigoListaItem"  , width: 100    ,   key: true , hidden: true }
                ,   { name: "Valor"             , index: "Valor"            , width: 100    ,   classes: "grid[property[Valor]]"}
                ,   { name: "Descricao"         , index: "Descricao"        , width: 330    ,   classes: "grid[property[Descricao]]" }
                ]
            });

            //==========================================================================
            // GRID DOMINIO SECUNDARIO - Do dominio REFERENCIA x PRODUTO
            //==========================================================================
            $("#tbValorReferenciaProduto").jqGrid(
            {   datatype: "clientSide"
            ,   mtype: "GET"
            ,   height: 100
            ,   width: 570
            ,   sortable: true
            ,   shrinkToFit: false
            ,   colNames: ["CodigoProduto", "Nome", "Descrição"]
            ,   colModel: 
                [   { name: "CodigoProduto"     , index: "CodigoProduto"    , width: 100    , key: true , hidden: true }
                ,   { name: "NomeProduto"       , index: "NomeProduto"      , width: 100    ,   classes: "grid[property[NomeProduto]]" }
                ,   { name: "DescricaoProduto"  , index: "DescricaoProduto" , width: 330    ,   classes: "grid[property[DescricaoProduto]]" }
                ]
            });
            
            //==========================================================================
            // GRID DOMINIO SECUNDARIO - Do dominio TIPO AJUSTE
            //==========================================================================
            $("#tbTipoAjuste").jqGrid(
            {   datatype: "clientSide"
            ,   mtype: "GET"
            ,   height: 100
            ,   width: 570
            ,   sortable: true
            ,   shrinkToFit: false
            ,   colNames: ["CodigoListaItem", "Valor", "Descrição"]
            ,   colModel: 
                [   { name: "CodigoListaItem"   , index: "CodigoListaItem"  , width: 100    ,   key: true , hidden: true }
                ,   { name: "Valor"             , index: "Valor"            , width: 100    ,   classes: "grid[property[Valor]]", editable: true, editoptions: { maxlength: 50} }
                ,   { name: "Descricao"         , index: "Descricao"        , width: 330    ,   classes: "grid[property[Descricao];required]", editable: true, editoptions: { maxlength: 50} }
                ]
            });
            //==========================================================================
            // GRID DOMINIO SECUNDARIO - Do dominio REFERENCIA ClockWork - Resource - Ago/16 - Margarida Vitolo
            //==========================================================================
            $("#tbReferencia").jqGrid(
            {   datatype: "clientSide"
            ,   mtype: "GET"
            ,   height: 100
            ,   width: 570
            ,   sortable: true
            ,   shrinkToFit: false
            ,   colNames: ["CodigoListaItem", "Valor", "Descrição"]
            ,   colModel: 
                [   { name: "CodigoListaItem"   , index: "CodigoListaItem"  , width: 100    ,   key: true , hidden: true }
                ,   { name: "Valor"             , index: "Valor"            , width: 100    ,   classes: "grid[property[Valor]]", editable: true, editoptions: { maxlength: 50} }
                ,   { name: "Descricao"         , index: "Descricao"        , width: 430    ,   classes: "grid[property[Descricao];required]", editable: true, editoptions: { maxlength: 50} }
                ]
            });            

            //==========================================================================
            // DOMINIO PRINCIPAL - Fixos do ENUM [ carrgarListas E preencherCombo do template_default.js ]
            //==========================================================================
            carregarListas("ListaItemReferenciaSituacaoEnum", function() {preencherCombo($("#cmbSituacao"),"ListaItemReferenciaSituacaoEnum");});
            
            //==========================================================================
            // DOMINIO PRINCIPAL - Dados dos DOMINIOS PRINCIPAIS
            //==========================================================================
            $("#site_configuracao_referencia_dominio_detalhe #cmbSituacao").change
            (   function() 
                {   $.referencia.detalhe.preencheComboNome();
                }
            );

            //==========================================================================
            // DOMINIO SECUNDARIO - Combo cmbNomeReferencia Alterado
            //==========================================================================
            $("#site_configuracao_referencia_dominio_detalhe #cmbNomeReferencia").change
            (   function() 
                {   $.referencia.detalhe.listaValorReferencia();
                }
            );
            
        }
    //==========================================================================
    // listarListaItem
    //==========================================================================
    ,   listarListaItem: function(nomeLista, callback) 
        {   executarServico
            (   "ListarListaItemRequest"
            ,   { NomeLista: nomeLista }
            ,   function(data) 
                {   $("#tbListaItem").clearGridData();
                    for (var i = 0; i < data.Resultado.length; i++)
                            $("#tbListaItem").jqGrid("addRowData", data.Resultado[i].CodigoListaItem, data.Resultado[i]);
                    
                    if (callback)
                        callback();
                });
        }
        
    //==========================================================================
    // listaValorReferencia
    //==========================================================================
    ,   listaValorReferencia: function(callback) 
        {   $.referencia.detalhe.esconderGrids();

            //==========================================================================
            // 1309 - Carrega combos dos DOMINIOS SECUNDARIOS se for selecionado o DOMINIO PRINCIPAL
            //==========================================================================
            var itemSelecionadoComboNome = $("#cmbNomeReferencia option:selected").val();
            if (itemSelecionadoComboNome != "?") 
            {
            //==========================================================================
            // MEIO DE PAGAMENTO - ListarListaItemRequest, com NomeLista = MeioPagamento
            //==========================================================================
                if (itemSelecionadoComboNome == "MeioPagamento") 
                {   executarServico
                    (   "ListarListaItemRequest"
                    ,   { NomeLista: itemSelecionadoComboNome }
                    ,   function(data) 
                        {   $("#tbValorReferenciaMeioPagamento").clearGridData();
                            for (var i = 0; i < data.Resultado.length; i++)
                                $("#tbValorReferenciaMeioPagamento").jqGrid("addRowData", data.Resultado[i].CodigoListaItem, data.Resultado[i]);
                                
                            // ===========================================================================
                            // 1309 - exib grid do link
                            // ===========================================================================                            
                            $("#dvTbMeioPagamento").show();

                            // ===========================================================================
                            // Chama callback
                            // ===========================================================================
                            if (callback)
                                callback();
                        }
                    );
            //==========================================================================
            // PRODUTO - ListarProdutoRequest
            //==========================================================================
                } else if (itemSelecionadoComboNome == "Produto") 
                {   executarServico
                    (   "ListarProdutoRequest"
                    ,   {}
                    ,   function(data) 
                        {   $("#tbValorReferenciaProduto").clearGridData();

                            for (var i = 0; i < data.Resultado.length; i++)
                                $("#tbValorReferenciaProduto").jqGrid("addRowData", data.Resultado[i].CodigoProduto, data.Resultado[i]);
                                
                            // ===========================================================================
                            // 1309 - exib grid do link
                            // ===========================================================================                            
                            $("#dvTbValorReferencia").show();

                            // ===========================================================================
                            // Chama callback
                            // ===========================================================================
                            if (callback)
                                callback();
                        }
                    );
            //==========================================================================
            // TIPO DE AJUSTE - ListarListaItemRequest, com NomeLista = TipoAjuste
            //==========================================================================
                 } else if (itemSelecionadoComboNome == "TipoAjuste") 
                 {  executarServico
                    (   "ListarListaItemRequest"
                    ,   { NomeLista: itemSelecionadoComboNome }
                    ,   function(data) 
                        {   $("#tbTipoAjuste").clearGridData();
                            for (var i = 0; i < data.Resultado.length; i++)
                                $("#tbTipoAjuste").jqGrid("addRowData", data.Resultado[i].CodigoListaItem, data.Resultado[i]);
                                
                            // ===========================================================================
                            // 1309 - exib grid do link
                            // ===========================================================================                            
                            $("#dvTbTipoAjuste").show();

                            // ===========================================================================
                            // Chama callback
                            // ===========================================================================
                            if (callback)
                                callback();
                        }
                    );
            //==========================================================================
            // REFERENCIA - ListarListaItemRequest, com NomeLista = Referencia
            //==========================================================================
                } else if (itemSelecionadoComboNome == "Referencia") 
                {   executarServico
                    (   "ListarListaItemRequest"
                    ,   { NomeLista: itemSelecionadoComboNome }
                    ,   function(data) 
                        {   $("#tbReferencia").clearGridData();
                            for (var i = 0; i < data.Resultado.length; i++)
                                $("#tbReferencia").jqGrid("addRowData", data.Resultado[i].CodigoListaItem, data.Resultado[i]);

                            // ===========================================================================
                            // 1309 - exib grid do link
                            // ===========================================================================                            
                            $("#dvTbReferencia").show(); 

                            // ===========================================================================
                            // Chama callback
                            // ===========================================================================
                            if (callback)
                                callback();
                        }
                    );
                }
            }
        }
    //==========================================================================
    // Combo com o Dominio Principal
    //==========================================================================
    ,   preencheComboNome: function() 
        {   var itemSelecionadoComboSituacao = $("#cmbSituacao option:selected");

            // limpa o combo dominio principal
            $('#cmbNomeReferencia option').each(function(i, option) {
                $(option).remove();
            })

            // preenche a lista de link de domínios
            if (itemSelecionadoComboSituacao.val() != "?") 
            {   $.referencia.detalhe.listarListaItem(itemSelecionadoComboSituacao.val(), null);

                // insere as opcoes do combo de nome da referencia
                if (itemSelecionadoComboSituacao.text() == "CodigoAnulacao") 
                {   $("#cmbNomeReferencia").append("<option value='?'>(selecione)</option>");
                    $("#cmbNomeReferencia").append("<option value='MeioPagamento'>MeioPagamento</option>");
                    $("#cmbNomeReferencia").append("<option value='Produto'>Produto</option>");

                } else if (itemSelecionadoComboSituacao.text() == "CodigoAjuste") 
                {   $("#cmbNomeReferencia").append("<option value='?'>(selecione)</option>");
                    $("#cmbNomeReferencia").append("<option value='Produto'>Produto</option>");
                    $("#cmbNomeReferencia").append("<option value='TipoAjuste'>TipoAjuste</option>");

                } else if (itemSelecionadoComboSituacao.text() == "BIN") { // ClockWork - Resource - Ago/16 - Margarida Vitolo
                    $("#cmbNomeReferencia").append("<option value='?'>(selecione)</option>");
                    $("#cmbNomeReferencia").append("<option value='Referencia'>Referencia</option>");
                    
                }
                $.referencia.detalhe.listaValorReferencia(null);
            }
            else 
            {   $("#tbListaItem").clearGridData();
                $("#dvTbMeioPagamento").hide();
                $("#dvTbValorReferencia").hide();
                $("#dvTbTipoAjuste").hide();
                $("#dvTbReferencia").hide(); // ClockWork - Resource - Ago/16 - Margarida Vitolo
            }
        }
    //==========================================================================
    // salvar
    //==========================================================================
    ,   salvar: function(callback) 
        {   $.referencia.detalhe.validar(function()
            //==========================================================================
            // callbackOk ( PARAMETRO DO VALIDAR )
            //==========================================================================
            {   if ($.referencia.detalhe.variaveis.referenciaDominioInfo) // 1309 ($.referencia.detalhe.variaveis.codigoReferencia) 
                {   var listaReferenciaInfo = $.referencia.detalhe.variaveis.referenciaDominioInfo;
                    if (listaReferenciaInfo)
                        $("#site_configuracao_referencia_dominio_detalhe").Forms("getData", { dataObject: listaReferenciaInfo });
                } else 
                {   listaReferenciaInfo = {};
                    $("#site_configuracao_referencia_dominio_detalhe").Forms("getData", { dataObject: listaReferenciaInfo });
                }

                // seta os parametros que nao estao no forms
                listaReferenciaInfo.CodigoListaItem = $("#tbListaItem").getGridParam("selrow");

                var itemSelecionadoListaValor;
                if ($("#cmbNomeReferencia option:selected").val() == "MeioPagamento") 
                {   itemSelecionadoListaValor = $("#tbValorReferenciaMeioPagamento").getGridParam("selrow");
                    listaReferenciaInfo.ValorReferencia = $('#tbValorReferenciaMeioPagamento').jqGrid('getCell', itemSelecionadoListaValor, "Valor");

                } else if ($("#cmbNomeReferencia option:selected").val() == "Produto") 
                {   itemSelecionadoListaValor = $("#tbValorReferenciaProduto").getGridParam("selrow");
                    listaReferenciaInfo.ValorReferencia = $('#tbValorReferenciaProduto').jqGrid('getCell', itemSelecionadoListaValor, "NomeProduto");
                    
                } else if ($("#cmbNomeReferencia option:selected").val() == "TipoAjuste") 
                {   itemSelecionadoListaValor = $("#tbTipoAjuste").getGridParam("selrow");
                    listaReferenciaInfo.ValorReferencia = $('#tbTipoAjuste').jqGrid('getCell', itemSelecionadoListaValor, "Valor");
                    
                } else if ($("#cmbNomeReferencia option:selected").val() == "Referencia") 
                {   itemSelecionadoListaValor = $("#tbReferencia").getGridParam("selrow");
                    listaReferenciaInfo.ValorReferencia = $('#tbReferencia').jqGrid('getCell', itemSelecionadoListaValor, "Valor");
                }

                executarServico
                (   "SalvarListaItemReferenciaRequest"
                ,   { ListaItemReferenciaInfo: listaReferenciaInfo }
                ,   function(data) 
                    {   var criticas = data.Criticas;
                        if (criticas[0]) 
                        {   popup(
                            {   titulo: "SALVAR"
                            ,   icone: "erro"
                            ,   mensagem: criticas[0].Descricao
                            ,   callbackOK: function() {   }
                            });
                        } else 
                        {   if (callback)
                                callback();
                        }
                    }
                )
            //==========================================================================
            // callbackErro ( PARAMETRO DO VALIDAR )
            //==========================================================================
            ,       $.referencia.detalhe.retornarTrue()
            })
        }
    //==========================================================================
    // retornarTrue
    //==========================================================================
    ,   retornarTrue: function() {return true;}
    //==========================================================================
    // validar
    //==========================================================================
    ,   validar: function(callbackOk, callbackErro) 
        {   var codigoListaItem = $("#tbListaItem").getGridParam("selrow");
            var itemSelecionadoComboNome = $("#cmbNomeReferencia option:selected").val();
            var itemSelecionadoListaValor;

            if (itemSelecionadoComboNome == "MeioPagamento")
                itemSelecionadoListaValor = $("#tbValorReferenciaMeioPagamento").getGridParam("selrow");
            else if (itemSelecionadoComboNome == "Produto")
                itemSelecionadoListaValor = $("#tbValorReferenciaProduto").getGridParam("selrow");
            else if (itemSelecionadoComboNome == "TipoAjuste")
                itemSelecionadoListaValor = $("#tbTipoAjuste").getGridParam("selrow");
            else if (itemSelecionadoComboNome == "Referencia")
                itemSelecionadoListaValor = $("#tbReferencia").getGridParam("selrow");

            if (codigoListaItem) 
            {   if (itemSelecionadoListaValor) 
                {   var criticas = new Array();
                    criticas = $("#site_configuracao_referencia_dominio_detalhe").Forms("validate", { returnErrors: true, markLabels: true, errors: criticas });
                    if (criticas[0]) 
                    {   $.Forms.showValidationForm({ errors: criticas });
                        callbackErro();
                    } else
                        callbackOk();
                } else 
                {   popup(
                    {   titulo: "SALVAR"
                    ,   icone: "erro"
                    ,   mensagem: "Favor selecionar um VALOR do DOMINIO PRINCIPAL"
                    ,   callbackOK: function() { }
                    });
                }
            } else 
            {   popup(
                {   titulo: "SALVAR"
                ,   icone: "erro"
                ,   mensagem: "Favor selecionar uma VALOR do LINK DE DOMINIO"
                ,   callbackOK: function() { }
                });
            }
        }
    }

    //==========================================================================
    // pede a inicializacao da tela
    //==========================================================================
    $.referencia.detalhe.preparar();
});