﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorio_conferencia_vendas.aspx.cs"
    EnableSessionState="false" Inherits="Resource.Carrefour.SCF.Hosts.Web.Principal.relatorio_conferencia_vendas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Relatório de Vendas</title>
    <link href="relatorio_conferencia_vendas.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frm" runat="server">
    <header>
        <table style="border:solid 1px #4682B4;width:100%;">
            <tr>
                <td colspan="21">
                    <h1 style="font-size:14px; font-weight:bold;">RELATÓRIO DE VENDAS - Consolidado - <asp:Label style="font-size:14px; font-weight:bold;" runat="server" ID="lblTipoConsolidado"></asp:Label></h1>  
                </td>
                <td colspan="11" align="right">
                    <label style="font-weight:bold; font-size:11px;" class="negrito pequeno">Data/Hora: </label> 
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblDataHora"></asp:Label>
                </td>                                                                       
            </tr>                        
            <tr>
                <td colspan="32">
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Filtros:</label>
                </td>
            </tr>                        
            <tr>
                <td colspan="12" valign="top">
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Grupo:</label> 
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroGrupo"></asp:Label>
                    &nbsp; &nbsp; &nbsp; &nbsp;
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subgrupo:</label> 
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroSubgrupo"></asp:Label>
                    <br />
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Favorecido:</label> 
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroFavorecido"></asp:Label>
                    &nbsp; &nbsp; &nbsp; &nbsp;
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">&nbsp;&nbsp;Estabelecimento:</label> 
                    <asp:Label style="font-size:10px;"  class="pequeno" runat="server" ID="lblFiltroEstabelecimento"></asp:Label>
                    <br />
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Referência:</label> 
                    <asp:Label style="font-size:10px;"  class="pequeno" runat="server" ID="lblFiltroReferencia"></asp:Label>                    
                    <br />
                </td>  
                <td colspan="4" valign="top">
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Período transação:</label>                              
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataInicio"></asp:Label> - 
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataFim"></asp:Label>
                    <br />
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Período processamento:</label>                              
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataMovimentoInicio"></asp:Label> - 
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroDataMovimentoFim"></asp:Label>
                    <br />
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Período Retorno CCI:</label>                              
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroRetornoCCIInicio"></asp:Label> - 
                    <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroRetornoCCIFim"></asp:Label>
                </td>  
                <td colspan="16" valign="top">
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Tipo de Registro:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroTipoRegistro"></asp:Label>
                    <br />
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Consolidação:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroConsolidacao"></asp:Label>
                    <br />
                    <label style="font-weight:bold; font-size:11px;" class="negrito medio">Financeiro/Contábil:</label> <asp:Label style="font-size:10px;" class="pequeno" runat="server" ID="lblFiltroFinanceiroContabil"></asp:Label>                    
                </td>                  
            </tr>          
            <tr runat="server" id="trMensagem" visible="false">
                <td colspan="32" class="grupo" style="width:100%; border-top:solid 3px #4682B4;">
                    <asp:Label style="font-size:10px;" class="negrito medio" runat="server" ID="lblMensagem"></asp:Label>
                </td>
            </tr>
        </table>
    </header>
    <asp:Repeater runat="server" ID="DataVendaRepeater">
        <ItemTemplate>
            <table style="border: solid 1px #4682B4; width: 100%; repeat-header: yes;">
                <thead>
                    <tr>
                        <th colspan="32" style="width: 100%; height: 10px; border-bottom: solid 2px #4682B4;"
                            class="linhaGrossa">
                        </th>
                    </tr>
                    <tr>
                        <th colspan="32" class="grupo">
                            <asp:Label Style="font-weight: bold; font-size: 11px;" ID="lblDataVendaRep" runat="server"
                                class="negrito medio">Data Venda: </asp:Label>
                            <label style="font-size: 10px;" class="pequeno">
                                <%# Eval("DataVenda")%></label>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="32" style="border-bottom: solid 1px #4682B4;" class="linhaFina">
                        </th>
                    </tr>
                    <tr>
                        <th colspan="4">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Grupo/Subgrupo</label>
                        </th>
                        <th colspan="4">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Estabelecimento</label>
                        </th>
                        <th colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Produto</label>
                        </th>
                        <th colspan="2">
                            <label style="font-weight: bold; font-size: 10px; width: 40px;" class="negrito pequeno">
                                Banco</label>
                        </th>
                        <th colspan="2">
                            <label style="font-weight: bold; font-size: 10px; width: 40px;" class="negrito pequeno">
                                Agência</label>
                        </th>
                        <th colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Conta</label>
                        </th>
                        <th style="text-align: right; width: 5%" colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Total Qtde. Vendas</label>
                        </th>
                        <th style="text-align: right" colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Total Bruto Vendas</label>
                        </th>
                        <th style="text-align: right" colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Total Comissão</label>
                        </th>
                        <th style="text-align: right" colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Total Vendas</label>
                        </th>
                        <th style="text-align: right; width: 6%" colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Total Cedido</label>
                        </th>
                        <th style="text-align: right; width: 9%" colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Total Cedíveis Não Negociados</label>
                        </th>
                        <th style="text-align: right; width: 6%" colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Total Não Cedíveis</label>
                        </th>
                        <th style="text-align: right" colspan="2">
                            <label style="font-weight: bold; font-size: 10px;" class="negrito pequeno">
                                Pendente</label>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Repeater Estabelecimento -->
                    <asp:Repeater runat="server" ID="EstabelecimentoRepeater">
                        <ItemTemplate>
                            <tr style="font-size: 10px;" class="pequeno">
                                <td colspan="4">
                                    <%# Eval("NomeEmpresaGrupo")%>
                                    /
                                    <%# Eval("NomeEmpresaSubgrupo")%>
                                </td>
                                <td colspan="4">
                                    <%# Eval("Estabelecimento.RazaoSocial")%>
                                </td>
                                <td colspan="2">
                                    <%# Eval("Produto.DescricaoProduto")%>
                                </td>
                                <td colspan="2">
                                    <%# Eval("Estabelecimento.Banco")%>
                                </td>
                                <td colspan="2">
                                    <%# Eval("Estabelecimento.Agencia")%>
                                </td>
                                <td colspan="2">
                                    <%# Eval("Estabelecimento.Conta")%>
                                </td>
                                <td style="text-align: right; width: 2%" colspan="2">
                                    <%# Convert.ToDecimal(Eval("Quantidade")).ToString("N0")%>
                                </td>
                                <td style="text-align: right" colspan="2">
                                    <%# Convert.ToDecimal(Eval("TotalBruto")).ToString("N2")%>
                                </td>
                                <td style="text-align: right" colspan="2">
                                    <%# Convert.ToDecimal(Eval("TotalComissao")).ToString("N2")%>
                                </td>
                                <td style="text-align: right" colspan="2">
                                    <%# Convert.ToDecimal(Eval("ValorLiquido")).ToString("N2") %>
                                </td>
                                <td style="text-align: right" colspan="2">
                                    <%# Convert.ToDecimal(Eval("TotalCedido")).ToString("N2") %>
                                </td>
                                <td style="text-align: right" colspan="2">
                                    <%# Convert.ToDecimal(Eval("TotalCedivelNaoNegociado")).ToString("N2") %>
                                </td>
                                <td style="text-align: right" colspan="2">
                                    <%# Convert.ToDecimal(Eval("TotalNaoCedido")).ToString("N2") %>
                                </td>
                                <td style="text-align: right" colspan="2">
                                    <%# Convert.ToDecimal(Eval("TotalPendente")).ToString("N2")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="32" style="height: 15px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="15" style="text-align: right; font-size: 10px;" class="negrito pequeno">
                            Totais:
                        </td>
                        <td align="right" colspan="3">
                            <label style="padding-top: 15px; font-size: 10px;" class="pequeno total">
                                <%# Convert.ToDecimal(Eval("TotalQuantidadeVendas")).ToString("N0") %>
                            </label>
                        </td>
                        <td align="right" colspan="2">
                            <label style="padding-top: 15px; font-size: 10px;" class="pequeno total">
                                <%# Convert.ToDecimal(Eval("TotalBrutoVendas")).ToString("N2")%>
                            </label>
                        </td>
                        <td align="right" colspan="2">
                            <label style="padding-top: 15px; font-size: 10px;" class="pequeno total">
                                <%# Convert.ToDecimal(Eval("TotalComissao")).ToString("N2") %>
                            </label>
                        </td>
                        <td align="right" colspan="2">
                            <label style="padding-top: 15px; font-size: 10px;" class="pequeno total">
                                <%# Convert.ToDecimal(Eval("TotalVendas")).ToString("N2") %>
                            </label>
                        </td>
                        <td align="right" colspan="2">
                            <label style="padding-top: 15px; font-size: 10px;" class="pequeno total">
                                <%# Convert.ToDecimal(Eval("TotalCedido")).ToString("N2") %>
                            </label>
                        </td>
                        <td align="right" colspan="2">
                            <label style="padding-top: 15px; font-size: 10px;" class="pequeno total">
                                <%# Convert.ToDecimal(Eval("TotalCediveisNaoNegociado")).ToString("N2") %>
                            </label>
                        </td>
                        <td align="right" colspan="2">
                            <label style="padding-top: 15px; font-size: 10px;" class="pequeno total">
                                <%# Convert.ToDecimal(Eval("TotalNaoCediveis")).ToString("N2") %>
                            </label>
                        </td>
                        <td align="right" colspan="2">
                            <label style="padding-top: 15px; font-size: 10px;" class="pequeno total">
                                <%# Convert.ToDecimal(Eval("TotalPendente")).ToString("N2")%>
                            </label>
                        </td>
                    </tr>
                    <%# Exibir() %>
        </ItemTemplate>
        <FooterTemplate>
            <tr>
                <td colspan="32" style="height: 10px; border-bottom: solid 2px #4682B4;" class="linhaGrossa">
                </td>
            </tr>
            <tr>
                <td colspan="15" align="left">
                    <label style="padding-top: 15px; font-size: 11px; font-weight: bold;" class="negrito medio total">
                        TOTAL GERAL:
                    </label>
                </td>
                <td align="right" colspan="3">
                    <asp:Label Style="padding-top: 15px; font-size: 10px; font-weight: bold;" runat="server"
                        ID="lblTotalQuantidadeVendas" class="negrito medio total"></asp:Label>
                </td>
                <td align="right" colspan="2">
                    <asp:Label Style="padding-top: 15px; font-size: 10px; font-weight: bold;" runat="server"
                        ID="lblTotalBrutoVendas" class="negrito medio total"></asp:Label>
                </td>
                <td align="right" colspan="2">
                    <asp:Label Style="padding-top: 15px; font-size: 10px; font-weight: bold;" runat="server"
                        ID="lblTotalComissao" class="negrito medio total"></asp:Label>
                </td>
                <td align="right" colspan="2">
                    <asp:Label Style="padding-top: 15px; font-size: 10px; font-weight: bold;" runat="server"
                        ID="lblTotalVendas" class="negrito medio total"></asp:Label>
                </td>
                <td align="right" colspan="2">
                    <asp:Label Style="padding-top: 15px; font-size: 10px; font-weight: bold;" runat="server"
                        ID="lblTotalCedido" class="negrito medio total"></asp:Label>
                </td>
                <td align="right" colspan="2">
                    <asp:Label Style="padding-top: 15px; font-size: 10px; font-weight: bold;" runat="server"
                        ID="lblTotalCedidoNaoNegociado" class="negrito medio total"></asp:Label>
                </td>
                <td align="right" colspan="2">
                    <asp:Label Style="padding-top: 15px; font-size: 10px; font-weight: bold;" runat="server"
                        ID="lblTotalNaoCedido" class="negrito medio total"></asp:Label>
                </td>
                <td align="right" colspan="2">
                    <asp:Label Style="padding-top: 15px; font-size: 10px; font-weight: bold;" runat="server"
                        ID="lblTotalPendente" class="negrito medio total"></asp:Label>
                </td>
            </tr>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>
    </form>
</body>
</html>
