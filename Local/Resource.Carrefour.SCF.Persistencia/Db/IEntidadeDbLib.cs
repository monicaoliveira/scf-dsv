﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Interface para implementação pelas rotinas de persistencia em banco das entidades
    /// </summary>
    public interface IEntidadeDbLib<T> where T : ICodigoEntidade
    {
        /// <summary>
        /// Consulta
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parametros"></param>
        /// <returns></returns>
        ConsultarObjetosResponse<T> ConsultarObjetos(ConsultarObjetosRequest<T> request);

        /// <summary>
        /// Detalhe
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parametros"></param>
        /// <returns></returns>
        ReceberObjetoResponse<T> ReceberObjeto(ReceberObjetoRequest<T> request);

        /// <summary>
        /// Salvar
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parametros"></param>
        /// <returns></returns>
        SalvarObjetoResponse<T> SalvarObjeto(SalvarObjetoRequest<T> request);

        /// <summary>
        /// Remover
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parametros"></param>
        /// <returns></returns>
        RemoverObjetoResponse<T> RemoverObjeto(RemoverObjetoRequest<T> request);

        /// <summary>
        /// Monta um objeto através do seu datarow
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        T MontarObjeto(DataRow dr);
    }
}
