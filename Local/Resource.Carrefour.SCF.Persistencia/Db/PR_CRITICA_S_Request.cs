﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Request para executar procedure PR_CRITICA_S_Request
    /// </summary>
    public class PR_CRITICA_S_Request
    {
        public string CodigoCritica { get; set; }
        public string CodigoProcesso { get; set; }
        public string CodigoArquivo { get; set; }
        public string CodigoArquivoItem { get; set; }
        public DateTime? DataCritica { get; set; }
        public string TipoCritica { get; set; }
        public string DescricaoCritica { get; set; }
        public string ConteudoCritica { get; set; }
        public bool RetornarRegistro { get; set; }
        public string TipoLinha { get; set; }
        public string NomeCampo { get; set; }
    }
}
