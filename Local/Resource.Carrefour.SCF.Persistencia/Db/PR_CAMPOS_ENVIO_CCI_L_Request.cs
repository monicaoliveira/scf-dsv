﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Solicitação de lista de campos envio CCI
    /// </summary>
    public class PR_CAMPOS_ENVIO_CCI_L_Request
    {
        public int CodigoCampoEnvioCCI { get; set; }
        public string NomeCampo { get; set; }
        public string StatusEnvioCCI { get; set; }
    }
}

