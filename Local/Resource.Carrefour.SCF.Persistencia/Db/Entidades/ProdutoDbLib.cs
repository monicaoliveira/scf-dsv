﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Framework.Library.Db;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class ProdutoDbLib : IEntidadeDbLib<ProdutoInfo>
    {
        #region IEntidadeDbLib<ProdutoInfo> Members

        public ConsultarObjetosResponse<ProdutoInfo> ConsultarObjetos(ConsultarObjetosRequest<ProdutoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // NomeProduto
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeProduto");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_PRODUTO", condicaoInfo.Valores[0]);

                // MaxLinhas
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "MaxLinhas");
                if (condicaoInfo != null)
                    paramsProc.Add("maxLinhas", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PRODUTO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ProdutoInfo> resultado = new List<ProdutoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ProdutoInfo>()
                {
                    Resultado = resultado
                };
        }

        public ReceberObjetoResponse<ProdutoInfo> ReceberObjeto(ReceberObjetoRequest<ProdutoInfo> request)
        {
            // Prepara flags
            bool retornarDetalhe = false;
            
            // Verifica se é mensagem especifica
            if (request is ReceberProdutoDbRequest)
            {
                // Pega mensagem com o tipo correto
                ReceberProdutoDbRequest request2 = (ReceberProdutoDbRequest)request;

                // Seta flags
                retornarDetalhe = request2.RetornarDetalhe;
            }

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PRODUTO_B",
                        "pCODIGO_PRODUTO", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbPlanos = (DataTable)retorno["RETCURPLANOS"];

            // Retorna
            return
                new ReceberObjetoResponse<ProdutoInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0], tbPlanos, retornarDetalhe) : null
                };
        }

        public RemoverObjetoResponse<ProdutoInfo> RemoverObjeto(RemoverObjetoRequest<ProdutoInfo> request)
        {
            try
            {
                // Prepara flags
                bool removeAssociacoesPlanos = false;

                // Verifica se é mensagem especifica
                if (request is RemoverProdutoDbRequest)
                {
                    // Pega mensagem com o tipo correto
                    RemoverProdutoDbRequest request2 = (RemoverProdutoDbRequest)request;

                    // Seta flags
                    removeAssociacoesPlanos = request2.RemoverAssociacoesPlanos;
                }

                // Monta a execução da procedure
                OracleDbLib2.Default.ExecutarProcedure(
                    "PR_PRODUTO_R",
                    "pCODIGO_PRODUTO", request.CodigoObjeto,
                    "pREMOVER_ASSOC_PLANOS", removeAssociacoesPlanos ? 'S' : 'N');

                // Retorna
                return new RemoverObjetoResponse<ProdutoInfo>();
            }
            catch (DbLibException ex)
            {
                if (ex.InnerException != null && ex.InnerException.ToString().Contains("-02292"))
                {
                    // Cria a critica
                    CriticaInfo critica = new CriticaInfo();
                    critica.Descricao = "Produto com Planos vinculados não pode ser removido. Para remover o Produto, desvincule os planos.";


                    RemoverObjetoResponse<ProdutoInfo> resposta = new RemoverObjetoResponse<ProdutoInfo>();
                    resposta.Criticas.Add(critica);
                    return resposta;
                }
                else
                {
                    throw ex;
                }
            }
        }

        public SalvarObjetoResponse<ProdutoInfo> SalvarObjeto(SalvarObjetoRequest<ProdutoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoProduto;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Prepara flags
            bool retornarDetalhe = false;
            SalvarObjetoTipoSincronismoEnum tipoAtualizacaoPlanos = SalvarObjetoTipoSincronismoEnum.Sincronizar;
            
            // Verifica se é mensagem especifica
            if (request is SalvarProdutoDbRequest)
            {
                // Pega mensagem com o tipo correto
                SalvarProdutoDbRequest request2 = (SalvarProdutoDbRequest)request;

                // Seta flags
                retornarDetalhe = request2.RetornarDetalhe;
                tipoAtualizacaoPlanos = request2.TipoSincronismo;
            }

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_PRODUTO", pk);
            paramsProc.Add("pCODIGO_PRODUTO_TSYS", request.Objeto.CodigoProdutoTSYS);
            paramsProc.Add("pNOME_PRODUTO", request.Objeto.NomeProduto);
            paramsProc.Add("pDESCRICAO_PRODUTO", request.Objeto.DescricaoProduto);
            paramsProc.Add("pCEDIVEL", request.Objeto.Cedivel ? 'S' : 'N');
            paramsProc.Add("pPRAZO_PGMTO", request.Objeto.PrazoPagamento);
            paramsProc.Add("pDIAS_VENCIDOS", request.Objeto.DiasVencidos);
            paramsProc.Add("pDIAS_VENCIDOS_AJ", request.Objeto.DiasVencidosAJ);
            paramsProc.Add("retornarRegistro", 'S');

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PRODUTO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbPlanos = (DataTable)retorno["RETCURPLANOS"];

            // Monta o produto salvo
            ProdutoInfo produtoSalvo = this.MontarObjeto(tb.Rows[0], tbPlanos, retornarDetalhe);

            // Monta o DetalheView caso a flag seja verdadeira
            ProdutoDetalheView produtoSalvoDetalheView = null;
            if (retornarDetalhe)
            {
                produtoSalvoDetalheView = (ProdutoDetalheView)produtoSalvo;
            }

            // Salva planos de acordo com o método pedido
            switch (tipoAtualizacaoPlanos)
            {
                // Caso a sincronização seja necessária, o sistema entra numa rotina de 
                //  comparação, onde todos os planos novos são inseridos e os planos 
                //  inexistentes são removidos.
                case SalvarObjetoTipoSincronismoEnum.Sincronizar:
                    // Itera sobre os planos recebidos, todos os que não existirem no produto salvo são adicionados ao banco
                    foreach (string codigoPlano in request.Objeto.CodigoPlanos)
                    {
                        if (!produtoSalvo.CodigoPlanos.Contains(codigoPlano))
                        {
                            OracleDbLib2.Default.ExecutarProcedure("PR_PRODUTO_PLANO_S", 
                                "pCODIGO_PRODUTO", produtoSalvo.CodigoProduto, 
                                "pCODIGO_PLANO", codigoPlano);
                        }
                    }

                    // Itera sobre os planos atualmente em banco, todos os que não existirem produto recebido são removidos do banco
                    foreach (string codigoPlano in produtoSalvo.CodigoPlanos)
                    {
                        if (!request.Objeto.CodigoPlanos.Contains(codigoPlano))
                        {
                            OracleDbLib2.Default.ExecutarProcedure("PR_PRODUTO_PLANO_R", 
                                "pCODIGO_PRODUTO", produtoSalvo.CodigoProduto, 
                                "pCODIGO_PLANO", codigoPlano);
                        }
                    }

                    break;
                case SalvarObjetoTipoSincronismoEnum.Adicionar:
                    foreach (string codigoPlano in request.Objeto.CodigoPlanos)
                    {
                        OracleDbLib2.Default.ExecutarProcedure("PR_PRODUTO_PLANO_S", "pCODIGO_PRODUTO", produtoSalvo.CodigoProduto, "pCODIGO_PLANO", codigoPlano);
                        produtoSalvo.CodigoPlanos.Add(codigoPlano);
                    }
                    break;
                case SalvarObjetoTipoSincronismoEnum.Remover:
                    foreach (string codigoPlano in request.Objeto.CodigoPlanos)
                    {
                        OracleDbLib2.Default.ExecutarProcedure("PR_PRODUTO_PLANO_R", "pCODIGO_PRODUTO", produtoSalvo.CodigoProduto, "pCODIGO_PLANO", codigoPlano);
                        produtoSalvo.CodigoPlanos.Remove(codigoPlano);
                    }
                    break;
            }

            // Caso o DetalheView deva ser retornado, o objeto é recebido 
            //  novamente para garantir que todos os PlanosInfo estejam no DetalheView.
            if (retornarDetalhe)
            {
                produtoSalvo = this.ReceberObjeto(new ReceberProdutoDbRequest() {
                    RetornarDetalhe = true
                }).Objeto;
            }

            // Retorna
            return
                new SalvarObjetoResponse<ProdutoInfo>()
                {
                    Objeto = produtoSalvo
                };
        }

        #endregion

        #region MontarObjeto 

        public ProdutoInfo MontarObjeto(DataRow dr)
        {
            return this.MontarObjeto(dr, null, false);
        }

        public ProdutoInfo MontarObjeto(DataRow dr, DataTable tbPlano, bool retornarDetalhe)
        {
            // Cria a instancia do produto
            ProdutoInfo produtoInfo = 
                retornarDetalhe ? new ProdutoDetalheView() : new ProdutoInfo();
            ProdutoDetalheView produtoDetalheView =
                retornarDetalhe ? (ProdutoDetalheView)produtoInfo : null;
            
            // Cria o usuario
            produtoInfo.CodigoProduto = dr["CODIGO_PRODUTO"] != DBNull.Value ? dr["CODIGO_PRODUTO"].ToString() : null;
            produtoInfo.CodigoProdutoTSYS = dr["CODIGO_PRODUTO_TSYS"] != DBNull.Value ? dr["CODIGO_PRODUTO_TSYS"].ToString() : null;
            produtoInfo.NomeProduto = dr["NOME_PRODUTO"] != DBNull.Value ? dr["NOME_PRODUTO"].ToString() : null;
            produtoInfo.DescricaoProduto = dr["DESCRICAO_PRODUTO"] != DBNull.Value ? dr["DESCRICAO_PRODUTO"].ToString() : null;
            produtoInfo.Cedivel = dr["CEDIVEL"] != DBNull.Value ? dr["CEDIVEL"].ToString() == "S" : false;
            produtoInfo.PrazoPagamento = dr["PRAZO_PGMTO"] != DBNull.Value ? Int32.Parse(dr["PRAZO_PGMTO"].ToString()) : 0;
            produtoInfo.DiasVencidos = dr["DIAS_VENCIDOS"] != DBNull.Value ? Int32.Parse(dr["DIAS_VENCIDOS"].ToString()) : 0;
            produtoInfo.DiasVencidosAJ = dr["DIAS_VENCIDOS_AJ"] != DBNull.Value ? Int32.Parse(dr["DIAS_VENCIDOS_AJ"].ToString()) : 0;

            // Deve montar planos?
            if (tbPlano != null)
            {
                PlanoDbLib planoDbLib = new PlanoDbLib();
                foreach (DataRow drPlano in tbPlano.Rows)
                {
                    // Adiciona no info
                    produtoInfo.CodigoPlanos.Add(drPlano["CODIGO_PLANO"].ToString());
                    
                    // Se pediu detalhe, adiciona no detalhe
                    if (retornarDetalhe)
                        produtoDetalheView.Planos.Add(planoDbLib.MontarObjeto(drPlano));
                }
            }

            // Retorna
            return produtoInfo;
        }

        #endregion

    }
}
