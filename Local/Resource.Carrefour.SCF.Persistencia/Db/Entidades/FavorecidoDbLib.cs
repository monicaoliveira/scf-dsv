﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Classe de persistencia de favorecidos
    /// </summary>
    public class FavorecidoDbLib : IEntidadeDbLib<FavorecidoInfo>
    {
        #region IEntidadeDbLib<FavorecidoInfo> Members

        /// <summary>
        /// Consulta favorecidos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<FavorecidoInfo> ConsultarObjetos(ConsultarObjetosRequest<FavorecidoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Código favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_FAVORECIDO", condicaoInfo.Valores[0]);

                // CNPJ favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CnpjFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pCNPJ_FAVORECIDO", SCFUtils.CompletarCNPJ(condicaoInfo.Valores[0].ToString()));

                // MaxLinhas
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "MaxLinhas");
                if (condicaoInfo != null)
                    paramsProc.Add("maxLinhas", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_FAVORECIDO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<FavorecidoInfo> resultado = new List<FavorecidoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<FavorecidoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<FavorecidoInfo> ReceberObjeto(ReceberObjetoRequest<FavorecidoInfo> request)
        {
            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_FAVORECIDO_B", 
                        "pCODIGO_FAVORECIDO", request.CodigoObjeto,
                        "retornarContas", "S");

            // Pega as tabelas necessarias
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbContas = (DataTable)retorno["RETCURCONTAS"];

            // Retorna
            return
                new ReceberObjetoResponse<FavorecidoInfo>()
                {
                    CodigoSessao = request.CodigoSessao,
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0], tbContas) :
                        null
                };
        }

        /// <summary>
        /// Salva um favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<FavorecidoInfo> SalvarObjeto(SalvarObjetoRequest<FavorecidoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoFavorecido;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_FAVORECIDO", pk);
            paramsProc.Add("pCNPJ_FAVORECIDO", request.Objeto.CnpjFavorecido == null ? null : SCFUtils.CompletarCNPJ(request.Objeto.CnpjFavorecido));
            paramsProc.Add("pNOME_FAVORECIDO", request.Objeto.NomeFavorecido);
            // SCF-900 - 24/04/2014
            paramsProc.Add("pFAVORECIDO_ORIGINAL", request.Objeto.FavorecidoOriginal == true ? '1' : '0');
            // SCF 1701 - 18/08/2017
            paramsProc.Add("pFAVORECIDO_BLOQUEADO", request.Objeto.FavorecidoBloqueado == true ? 1 : 0);
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_FAVORECIDO_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<FavorecidoInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        /// <summary>
        /// Remove um favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<FavorecidoInfo> RemoverObjeto(RemoverObjetoRequest<FavorecidoInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_FAVORECIDO_R",
                "pCODIGO_FAVORECIDO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<FavorecidoInfo>();
        }

        /// <summary>
        /// Monsta um favorecido
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public FavorecidoInfo MontarObjeto(DataRow dr)
        {
            // Retorna
            return
                new FavorecidoInfo()
                {
                    Linha = Convert.ToInt32(dr["rowNumber"].ToString()),
                    CodigoFavorecido = dr["CODIGO_FAVORECIDO"].ToString(),
                    CnpjFavorecido = dr["CNPJ_FAVORECIDO"] != DBNull.Value ? (string)dr["CNPJ_FAVORECIDO"] : null,
                    NomeFavorecido = dr["NOME_FAVORECIDO"] != DBNull.Value ? (string)dr["NOME_FAVORECIDO"] : null,
                    // SCF-900 - 24/04/2014
                    FavorecidoOriginal = dr["FAVORECIDO_ORIGINAL"] != DBNull.Value ? dr["FAVORECIDO_ORIGINAL"].ToString() == "1" ? true : false : false,
                    // SCF-1701 - 18/08/2017
                    FavorecidoBloqueado = dr["FAVORECIDO_BLOQUEADO"] != DBNull.Value ? dr["FAVORECIDO_BLOQUEADO"].ToString() == "1" ? true : false : false
                };
        }

        /// <summary>
        /// Monsta um favorecido
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public FavorecidoInfo MontarObjeto(DataRow dr, DataTable tbContas)
        {
            // Retorna            
             FavorecidoDetalheInfo favorecido = new FavorecidoDetalheInfo()
                {
                    Linha = Convert.ToInt32(dr["rowNumber"].ToString()),
                    CodigoFavorecido = dr["CODIGO_FAVORECIDO"].ToString(),
                    CnpjFavorecido = dr["CNPJ_FAVORECIDO"] != DBNull.Value ? (string)dr["CNPJ_FAVORECIDO"] : null,
                    NomeFavorecido = dr["NOME_FAVORECIDO"] != DBNull.Value ? (string)dr["NOME_FAVORECIDO"] : null,
                    // SCF-900 - 24/04/2014
                    FavorecidoOriginal = dr["FAVORECIDO_ORIGINAL"] != DBNull.Value ? dr["FAVORECIDO_ORIGINAL"].ToString() == "1" ? true : false : false,
                    // SCF-1701 - 18/08/2017
                    FavorecidoBloqueado = dr["FAVORECIDO_BLOQUEADO"] != DBNull.Value ? dr["FAVORECIDO_BLOQUEADO"].ToString() == "1" ? true : false : false
                };

            // Monta itens
             if (tbContas != null)
                 foreach (DataRow drConta in tbContas.Rows)
                     favorecido.ContasFavorecido.Add(
                        new ContaFavorecidoInfo()
                        {
                            CodigoContaFavorecido = drConta["CODIGO_CONTA_FAVORECIDO"].ToString(),
                            CodigoFavorecido = drConta["CODIGO_FAVORECIDO"] != DBNull.Value ? drConta["CODIGO_FAVORECIDO"].ToString() : null,
                            Conta = drConta["CONTA"] != DBNull.Value ? drConta["CONTA"].ToString() : null,
                            Agencia = drConta["AGENCIA"] != DBNull.Value ? drConta["AGENCIA"].ToString() : null,
                            Banco = drConta["BANCO"] != DBNull.Value ? drConta["BANCO"].ToString() : null
                        });
             return favorecido;
        }

        #endregion
    }
}
