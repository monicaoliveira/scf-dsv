﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Db.Oracle;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class TaxaRecebimentoDb : IEntidadeDbLib<TaxaRecebimentoInfo>
    {
        #region Consultar (Listar)

        public ConsultarObjetosResponse<TaxaRecebimentoInfo> ConsultarObjetos(ConsultarObjetosRequest<TaxaRecebimentoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null && request.Condicoes.Count() > 0)
            {
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_GRUPO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoReferencia");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_REFERENCIA", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoMeioPagamento");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_MEIO_PAGAMENTO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInicio");
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataFim");
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoTaxaRecebimento");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_TAXA_RECEBIMENTO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_TAXA_RECEBIMENTO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<TaxaRecebimentoInfo> resultado = new List<TaxaRecebimentoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<TaxaRecebimentoInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<TaxaRecebimentoInfo> ReceberObjeto(ReceberObjetoRequest<TaxaRecebimentoInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_TAXA_RECEBIMENTO_B",
                        "pCODIGO_TAXA_RECEBIMENTO", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<TaxaRecebimentoInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<TaxaRecebimentoInfo> RemoverObjeto(RemoverObjetoRequest<TaxaRecebimentoInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_TAXA_RECEBIMENTO_R",
                "pCODIGO_TAXA_RECEBIMENTO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<TaxaRecebimentoInfo>();
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<TaxaRecebimentoInfo> SalvarObjeto(SalvarObjetoRequest<TaxaRecebimentoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoTaxaRecebimento;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_TAXA_RECEBIMENTO", pk);
            paramsProc.Add("pCODIGO_EMPRESA_GRUPO", request.Objeto.CodigoGrupo);
            paramsProc.Add("pCODIGO_REFERENCIA", request.Objeto.CodigoReferencia);
            paramsProc.Add("pCODIGO_MEIO_PAGAMENTO", request.Objeto.CodigoMeioPagamento);
            paramsProc.Add("pDATA_FIM", request.Objeto.DataFim);
            paramsProc.Add("pDATA_INICIO", request.Objeto.DataInicio);
            paramsProc.Add("pPERCENTUAL_RECEBIMENTO", request.Objeto.PercentualRecebimento);
            paramsProc.Add("pVALOR_RECEBIMENTO", request.Objeto.ValorRecebimento);
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_TAXA_RECEBIMENTO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            TaxaRecebimentoInfo TaxaRecebimentoSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<TaxaRecebimentoInfo>()
                {
                    Objeto = TaxaRecebimentoSalvo
                };
        }

        #endregion

        #region MontarObjeto

        public TaxaRecebimentoInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            TaxaRecebimentoInfo TaxaRecebimentoInfo =
                new TaxaRecebimentoInfo()
                {

                    CodigoTaxaRecebimento = dr["CODIGO_TAXA_RECEBIMENTO"] != DBNull.Value ? dr["CODIGO_TAXA_RECEBIMENTO"].ToString() : null,
                    CodigoGrupo = dr["CODIGO_EMPRESA_GRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_GRUPO"].ToString() : null,
                    CodigoReferencia= dr["CODIGO_REFERENCIA"] != DBNull.Value ? dr["CODIGO_REFERENCIA"].ToString() : null,
                    CodigoMeioPagamento = dr["CODIGO_MEIO_PAGAMENTO"] != DBNull.Value ? dr["CODIGO_MEIO_PAGAMENTO"].ToString() : null,
                    DataFim = dr["DATA_FIM"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_FIM"]) : null,
                    DataInicio = dr["DATA_INICIO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_INICIO"]) : null,
                    PercentualRecebimento = dr["PERCENTUAL_RECEBIMENTO"] != DBNull.Value ? Decimal.Parse(dr["PERCENTUAL_RECEBIMENTO"].ToString()) : Decimal.MinValue,
                    ValorRecebimento = dr["VALOR_RECEBIMENTO"] != DBNull.Value ? Decimal.Parse(dr["VALOR_RECEBIMENTO"].ToString()) : Decimal.MinValue,
                    NomeGrupo = dr["NOME_EMPRESA_GRUPO"] != DBNull.Value ? dr["NOME_EMPRESA_GRUPO"].ToString() : null,
                    DescricaoReferencia = dr["DESCRICAO_REFERENCIA"] != DBNull.Value ? dr["DESCRICAO_REFERENCIA"].ToString() : null,
                    DescricaoMeioPagamento = dr["DESCRICAO_MEIO_PAGAMENTO"] != DBNull.Value ? dr["DESCRICAO_MEIO_PAGAMENTO"].ToString() : null
                };

            // Retorna
            return TaxaRecebimentoInfo;
        #endregion
        }
    }
}
