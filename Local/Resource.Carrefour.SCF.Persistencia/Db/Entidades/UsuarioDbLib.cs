﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db.Oracle;

using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class UsuarioDbLib : IEntidadeDbLib<UsuarioInfo>
    {
        #region IEntidadeDbLib<UsuarioInfo> Members

        #region Consultar

        public ConsultarObjetosResponse<UsuarioInfo> ConsultarObjetos(ConsultarObjetosRequest<UsuarioInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes.Count != 0)
            {
                // NomeUsuario
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Nome");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_USUARIO", condicaoInfo.Valores[0]);

                // Numero maximo de resultados
                paramsProc.Add("maxLinhas", 100);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_USUARIO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<UsuarioInfo> resultado = new List<UsuarioInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr, null, null, null));

            // Retorna
            return
                new ConsultarObjetosResponse<UsuarioInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<UsuarioInfo> ReceberObjeto(ReceberObjetoRequest<UsuarioInfo> request)
        {
            // Inicializa
            bool preencherGrupos = true;
            bool preencherPerfis = true;
            bool preencherPermissoes = true;
            bool preencherGrupoPerfil = true;

            // Verifica se veio a mensagem especifica
            if (request is ReceberUsuarioDbRequest)
            {
                // Pega a mensagem com o tipo especifico
                ReceberUsuarioDbRequest request2 = (ReceberUsuarioDbRequest)request;

                // Pega os valores
                preencherGrupos = request2.PreencherGrupos;
                preencherPerfis = request2.PreencherPerfis;
                preencherPermissoes = request2.PreencherPermissoes;
            }

            Dictionary<string, object> retorno = new Dictionary<string, object>();

            try
            {
            // Faz a consulta no banco
            retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_USUARIO_B",
                        "pCODIGO_USUARIO"     , request.CodigoObjeto,
                        "retornarPermissoes"  , preencherGrupos ? "S" : "N",
                        "retornarPerfisAcesso", preencherPerfis ? "S" : "N",
                        "retornarGruposAcesso", preencherGrupos ? "S" : "N",
                        "retornarGrupoPerfilAcesso", preencherGrupoPerfil ? "S" : "N");
            }
            catch (Exception ) // catch (Exception ex) // warning 25/02/2019
            {                
                throw;
            }
            
            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbPermissoes   = (DataTable)retorno["RETCURPERMISSOES"];
            DataTable tbPerfisAcesso = (DataTable)retorno["RETCURPERFISACESSO"];
            DataTable tbGruposAcesso = (DataTable)retorno["RETCURGRUPOSACESSO"];
            DataTable tbGrupoPerfilAcesso = (DataTable)retorno["RETCURGRUPOPERFILACESSO"];


            // Retorna
            return
                new ReceberObjetoResponse<UsuarioInfo>()
                {
                    Objeto = 
                        tb.Rows.Count > 0 ? 
                        this.MontarObjeto(tb.Rows[0], tbPermissoes, tbPerfisAcesso, tbGruposAcesso) : 
                        null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<UsuarioInfo> RemoverObjeto(RemoverObjetoRequest<UsuarioInfo> request)
        {
            RemoverObjetoResponse<UsuarioInfo> resposta = new RemoverObjetoResponse<UsuarioInfo>();
            try
            {
                // Monta a execução da procedure
                OracleDbLib2.Default.ExecutarProcedure(
                    "PR_USUARIO_R",
                    "pCODIGO_USUARIO", request.CodigoObjeto);
            }
            catch (Exception)
            {
                resposta.RepassarExcessao = false;
                resposta.DescricaoResposta = "FAVOR DELETAR TODOS OS CADASTROS DE GRUPOS/PERFIS/PERMISSOES DESTE USUÁRIO ANTES DE REMOVÊ-LO";
                resposta.Erro = resposta.DescricaoResposta;
                resposta.StatusResposta = Resource.Framework.Library.Servicos.Mensagens.MensagemResponseStatusEnum.ErroPrograma; //1477 .ErroNegocio;
            }
            // Retorna
            return resposta;
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<UsuarioInfo> SalvarObjeto(SalvarObjetoRequest<UsuarioInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoUsuario;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Inicializa
            bool preencherGrupos = true;
            bool preencherPerfis = true;
            bool preencherPermissoes = true;

            // Verifica se veio a mensagem especifica
            if (request is SalvarUsuarioDbRequest)
            {
                // Pega a mensagem com o tipo especifico
                SalvarUsuarioDbRequest request2 = (SalvarUsuarioDbRequest)request;

                // Pega os valores
                preencherGrupos = request2.PreencherGrupos;
                preencherPerfis = request2.PreencherPerfis;
                preencherPermissoes = request2.PreencherPermissoes;
            }

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_USUARIO", pk);
            paramsProc.Add("pNOME_USUARIO", request.Objeto.Nome);
            if(request.Objeto.Senha != null)
                paramsProc.Add("pSENHA", request.Objeto.Senha);
            paramsProc.Add("pASSINATURA_ELETRONICA", request.Objeto.AssinaturaEletronica);
            paramsProc.Add("pEMAIL", request.Objeto.Email);
            paramsProc.Add("pDATA_EXPIRACAO_SENHA", request.Objeto.DataExpiracaoSenha);
            paramsProc.Add("pSTATUS", OracleDbLib2.EnumToDb<UsuarioStatusEnum>(request.Objeto.Status));
            paramsProc.Add("retornarRegistro", "S");
            paramsProc.Add("retornarPermissoes", "S");
            paramsProc.Add("retornarPerfisAcesso", "S");
            paramsProc.Add("retornarGruposAcesso", "S");

            // Adiciona parametros especificos de UsuarioSCFInfo
            if (request.Objeto is UsuarioSCFInfo)
            {
                // Pega o objeto com o tipo correto
                UsuarioSCFInfo usuarioSCFInfo = (UsuarioSCFInfo)request.Objeto;

                // Adiciona parametros especificos
                paramsProc.Add("pCPF", usuarioSCFInfo.Cpf);
                paramsProc.Add("pMATRICULA", usuarioSCFInfo.Matricula);
                paramsProc.Add("pBLOQUEADO", usuarioSCFInfo.Bloqueado == true ? 'S' : 'N' );
                paramsProc.Add("pDATA_BLOQUEADO_INICIO", usuarioSCFInfo.DataBloqueadoInicio);
                paramsProc.Add("pDATA_BLOQUEADO_FIM", usuarioSCFInfo.DataBloqueadoFim);
                paramsProc.Add("pLOGIN", usuarioSCFInfo.Login);
            }

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_USUARIO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbPermissoes = (DataTable)retorno["RETCURPERMISSOES"];
            DataTable tbPerfisAcesso = (DataTable)retorno["RETCURPERFISACESSO"];
            DataTable tbGruposAcesso = (DataTable)retorno["RETCURGRUPOSACESSO"];

            UsuarioInfo usuarioSalvo = this.MontarObjeto(tb.Rows[0], tbPermissoes, tbPerfisAcesso, tbGruposAcesso);
            
            // Salva permissões
            salvarPermissoes(request.Objeto, usuarioSalvo);

            // Salva grupos
            salvarGrupos(request.Objeto, usuarioSalvo);

            // Salva perfis
            salvarPerfis(request.Objeto, usuarioSalvo);

            // Retorna
            return
                new SalvarObjetoResponse<UsuarioInfo>()
                {
                    Objeto = usuarioSalvo
                };
        }

        #endregion

        #endregion

        #region MontarObjeto

        public UsuarioInfo MontarObjeto(DataRow dr)
        {
            return this.MontarObjeto(dr, null, null, null);
        }

        #endregion

        #region MontarObjeto Sobrescrito

        public UsuarioInfo MontarObjeto(DataRow dr, DataTable tbPermissoes, DataTable tbPerfisAcesso, DataTable tbGruposAcesso)
        {
            Nullable<DateTime> vDT_BLOQUEADO_FIM = Convert.IsDBNull(dr["DT_BLOQUEADO_FIM"]) ? null : (Nullable<DateTime>)dr["DT_BLOQUEADO_FIM"]; //1433 log
            if (vDT_BLOQUEADO_FIM != null)
            {
                vDT_BLOQUEADO_FIM = vDT_BLOQUEADO_FIM.Value.AddHours(23).AddMinutes(59).AddSeconds(59); // 1433 log
            }
            // Cria o usuario
            UsuarioSCFInfo usuarioInfo =
                new UsuarioSCFInfo()
                {
                    CodigoUsuario = dr["CODIGO_USUARIO"].ToString(),
                    Nome = dr["NOME_USUARIO"].ToString(),
                    AssinaturaEletronica = dr["ASSINATURA_ELETRONICA"].ToString(),
                    Status = OracleDbLib2.EnumToObject<UsuarioStatusEnum>(dr["STATUS"]),
                    Email = dr["EMAIL"].ToString(),
                    DataExpiracaoSenha = (DateTime)dr["DATA_EXPIRACAO_SENHA"],
                    Cpf = dr["CPF"] != DBNull.Value ? dr["CPF"].ToString() : null,
                    Login = dr["LOGIN"] != DBNull.Value ? dr["LOGIN"].ToString() : null,
                    Matricula = dr["MATRICULA"] != DBNull.Value ? dr["MATRICULA"].ToString() : null,
                    Bloqueado = dr["BLOQUEADO"] != DBNull.Value ? dr["BLOQUEADO"].ToString() == "S" ? true : false : false,
                    DataBloqueadoInicio = Convert.IsDBNull(dr["DT_BLOQUEADO_INICIO"]) ? null : (Nullable<DateTime>)dr["DT_BLOQUEADO_INICIO"],
                    DataBloqueadoFim = vDT_BLOQUEADO_FIM, //1433 log Convert.IsDBNull(dr["DT_BLOQUEADO_FIM"]) ? null : (Nullable<DateTime>)dr["DT_BLOQUEADO_FIM"], 
                    Senha = dr["SENHA"].ToString()
                };



            //if (dr["SENHA"] != DBNull.Value)
            //    usuarioInfo.Senha = CriptografiaHelper.ReceberMD5Hash(dr["SENHA"].ToString());

            // Se tem permissoes, preenche
            if (tbPermissoes != null)
                foreach (DataRow drPermissao in tbPermissoes.Rows)
                    usuarioInfo.Permissoes.Add(
                        new PermissaoAssociadaInfo() 
                        { 
                            CodigoPermissao = drPermissao["CODIGO_PERMISSAO"].ToString(),
                            Status = OracleDbLib2.EnumToObject<PermissaoAssociadaStatusEnum>(drPermissao["STATUS_PERMISSAO"])
                        });

            // Se tem grupos, preenche
            if (tbGruposAcesso != null)
                foreach (DataRow drGrupoAcesso in tbGruposAcesso.Rows)
                    usuarioInfo.Grupos.Add(drGrupoAcesso["CODIGO_GRUPO_ACESSO"].ToString());

            // Se tem perfis, preenche
            if (tbPerfisAcesso != null)
                foreach (DataRow drPerfilAcesso in tbPerfisAcesso.Rows)
                    usuarioInfo.Perfis.Add(drPerfilAcesso["CODIGO_PERFIL"].ToString());

            //if(tbGrupoPerfilAcesso != null)
            //    foreach

            // Retorna
            return usuarioInfo;
        }

        #endregion

        #region Rotinas Locais

        /// <summary>
        /// Salva a lista de permissoes
        /// </summary>
        /// <param name="clienteOriginal"></param>
        /// <param name="clienteSalvo"></param>
        private void salvarPermissoes(UsuarioInfo usuarioOriginal, UsuarioInfo usuarioSalvo)
        {
            // Inicializa
            UsuarioPermissaoDbLib usuarioPermissaoDbLib = new UsuarioPermissaoDbLib();

            // Pega lista de permissoes atuais
            List<PermissaoAssociadaInfo> permissoesAtuais =
                usuarioPermissaoDbLib.ConsultarObjetos(
                    new List<CondicaoInfo>() 
                    { 
                        new CondicaoInfo("CodigoUsuario", CondicaoTipoEnum.Igual, usuarioSalvo.CodigoUsuario)
                    });

            // Varre a lista de que foi pedido para salvar
            foreach (PermissaoAssociadaInfo permissaoAssociada in usuarioOriginal.Permissoes)
                usuarioPermissaoDbLib.SalvarObjeto(permissaoAssociada, usuarioSalvo.CodigoUsuario);

            // Verifica se existem permissoes a remover
            foreach (PermissaoAssociadaInfo permissaoAssociada in permissoesAtuais)
                if (usuarioOriginal.Permissoes.Find(p => p.CodigoPermissao == permissaoAssociada.CodigoPermissao) == null)
                    usuarioPermissaoDbLib.RemoverObjeto(usuarioSalvo.CodigoUsuario, permissaoAssociada.CodigoPermissao);

            // Atribui a coleção ao cliente salvo
            usuarioSalvo.Permissoes = usuarioOriginal.Permissoes;
        }

        /// <summary>
        /// Salva a lista de grupos
        /// </summary>
        /// <param name="clienteOriginal"></param>
        /// <param name="clienteSalvo"></param>
        private void salvarGrupos(UsuarioInfo usuarioOriginal, UsuarioInfo usuarioSalvo)
        {
            // Inicializa
            UsuarioGrupoAcessoDbLib usuarioGrupoAcessoDbLib = new UsuarioGrupoAcessoDbLib();

            // Pega lista de grupos atuais
            List<string> gruposAtuais =
                usuarioGrupoAcessoDbLib.ConsultarObjetosPorUsuario(usuarioSalvo.CodigoUsuario);

            // Varre a lista de que foi pedido para salvar
            foreach (string codigoUsuarioGrupo in usuarioOriginal.Grupos)
                usuarioGrupoAcessoDbLib.SalvarObjeto(codigoUsuarioGrupo, usuarioSalvo.CodigoUsuario);

            // Verifica se existem grupos a remover
            foreach (string codigoUsuarioGrupo in gruposAtuais)
                if (usuarioOriginal.Grupos.Find(g => g == codigoUsuarioGrupo) == null)
                    usuarioGrupoAcessoDbLib.RemoverObjeto(usuarioSalvo.CodigoUsuario, codigoUsuarioGrupo);

            // Atribui a coleção ao cliente salvo
            usuarioSalvo.Grupos = usuarioOriginal.Grupos;
        }

        /// <summary>
        /// Salva a lista de grupos
        /// </summary>
        /// <param name="clienteOriginal"></param>
        /// <param name="clienteSalvo"></param>
        private void salvarPerfis(UsuarioInfo usuarioOriginal, UsuarioInfo usuarioSalvo)
        {
            // Inicializa
            UsuarioUsuarioPerfilDbLib usuarioUsuarioPerfilDbLib = new UsuarioUsuarioPerfilDbLib();

            // Pega lista de grupos atuais
            List<string> perfisAtuais =
                usuarioUsuarioPerfilDbLib.ConsultarObjetosPorUsuario(usuarioSalvo.CodigoUsuario);

            // Varre a lista de que foi pedido para salvar
            foreach (string codigoPerfil in usuarioOriginal.Perfis)
                usuarioUsuarioPerfilDbLib.SalvarObjeto(codigoPerfil, usuarioSalvo.CodigoUsuario);

            // Verifica se existem grupos a remover
            foreach (string codigoPerfil in perfisAtuais)
                if (usuarioOriginal.Perfis.Find(p => p == codigoPerfil) == null)
                    usuarioUsuarioPerfilDbLib.RemoverObjeto(usuarioSalvo.CodigoUsuario, codigoPerfil);

            // Atribui a coleção ao cliente salvo
            usuarioSalvo.Perfis = usuarioOriginal.Perfis;
        }

        #endregion 
    }
}
