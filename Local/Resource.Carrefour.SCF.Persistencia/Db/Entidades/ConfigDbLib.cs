﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using System.IO;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Xml.Serialization;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistência para ConfigInfo
    /// </summary>
    public class ConfigDbLib : IEntidadeDbLib<ConfigInfo>
    {
        #region IEntidadeDbLib<ConfigInfo> Members

        public ConsultarObjetosResponse<ConfigInfo> ConsultarObjetos(ConsultarObjetosRequest<ConfigInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // TipoObjeto
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoObjeto");
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_OBJETO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CONFIG_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ConfigInfo> resultado = new List<ConfigInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ConfigInfo>()
                {
                    Resultado = resultado
                };
        }

        public ReceberObjetoResponse<ConfigInfo> ReceberObjeto(ReceberObjetoRequest<ConfigInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CONFIG_B", "pTIPO_OBJETO", request.CodigoObjeto)["RETCUR"];

            if (tb.Rows.Count > 0)
            {

                // Retorna
                return
                    new ReceberObjetoResponse<ConfigInfo>()
                    {
                        Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                    };
            }
            else
            {
                return
                    new ReceberObjetoResponse<ConfigInfo>();
            }
        }

        public SalvarObjetoResponse<ConfigInfo> SalvarObjeto(SalvarObjetoRequest<ConfigInfo> request)
        {
            // Serializa o objeto
            MemoryStream ms = new MemoryStream();
            new XmlSerializer(
                request.Objeto.ObjetoConfig.GetType()).Serialize(
                    ms, request.Objeto.ObjetoConfig);
            ms.Position = 0;
            StreamReader reader = new StreamReader(ms);
            string serializacaoObjeto = reader.ReadToEnd();
            reader.Close();

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pTIPO_OBJETO", request.Objeto.ObjetoConfig.GetType().Name);
            paramsProc.Add("pCONTEUDO_OBJETO", serializacaoObjeto);
            paramsProc.Add("retornarRegistro", "S");

            // Executa a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_CONFIG_S", paramsProc)["RETCUR"];

            if (tb.Rows.Count > 0)
            {
                // Retorna
                return
                    new SalvarObjetoResponse<ConfigInfo>()
                    {
                        Objeto = this.MontarObjeto(tb.Rows[0])
                    };
            }
            else
                return new SalvarObjetoResponse<ConfigInfo>();
        }

        public RemoverObjetoResponse<ConfigInfo> RemoverObjeto(RemoverObjetoRequest<ConfigInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_CONFIG_R",
                "pTIPO_LOG", request.CodigoObjeto);

            // Retorna
            return
                new RemoverObjetoResponse<ConfigInfo>()
                {
                    CodigoMensagemRequest = request.CodigoMensagem
                };
        }

        public ConfigInfo MontarObjeto(DataRow dr)
        {
            // Resolve o tipo
            Type tipoConfig = ResolutorTipos.Resolver((string)dr["TIPO_OBJETO"]);

            // Desserializa o objeto
            object config =
                new XmlSerializer(tipoConfig).Deserialize(
                    new MemoryStream(
                        ASCIIEncoding.UTF8.GetBytes((string)dr["CONTEUDO_OBJETO"])));

            // Cria o configInfo
            ConfigInfo configInfo =
                new ConfigInfo()
                {
                    ObjetoConfig = config
                };

            // Retorna
            return configInfo;
        }

        #endregion
    }
}
