﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Db.Oracle;
using Resource.Carrefour.SCF.Contratos.Principal;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Classe de persistencia de Estabelecimento
    /// </summary>
    public class BinReferenciaDbLib : IEntidadeDbLib<BinReferenciaInfo>
    {
        #region IEntidadeDbLib<BinReferenciaInfo> Members

        /// <summary>
        /// Consulta BinReferencia
        /// </summary>
        public ConsultarObjetosResponse<BinReferenciaInfo> ConsultarObjetos(ConsultarObjetosRequest<BinReferenciaInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LISTA_ITEM_BIN_REFERENCIA_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<BinReferenciaInfo> resultado = new List<BinReferenciaInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<BinReferenciaInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<BinReferenciaInfo> ReceberObjeto(ReceberObjetoRequest<BinReferenciaInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LISTA_ITEM_BIN_REFERENCIA_L", "pCODIGO_BIN", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<BinReferenciaInfo>()
                {
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        /// <summary>
        public SalvarObjetoResponse<BinReferenciaInfo> SalvarObjeto(SalvarObjetoRequest<BinReferenciaInfo> request)
        {
            return
                new SalvarObjetoResponse<BinReferenciaInfo>();
        }

        /// <summary>
        public RemoverObjetoResponse<BinReferenciaInfo> RemoverObjeto(RemoverObjetoRequest<BinReferenciaInfo> request)
        {
            // Retorna
            return new RemoverObjetoResponse<BinReferenciaInfo>();
        }

        /// <summary>
        /// Monta BinReferencia
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public BinReferenciaInfo MontarObjeto(DataRow dr)
        {
            // Retorna
            return
                new BinReferenciaInfo()
                {
                    CodigoBin = dr["BIN"].ToString(),
                    CodigoReferencia = dr["REFERENCIA"].ToString(),
                };
        }

        #endregion
    }
}
