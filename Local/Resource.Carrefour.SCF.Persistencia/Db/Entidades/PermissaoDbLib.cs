﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class PermissaoDbLib: IEntidadeDbLib<PermissaoInfo>
    {
        #region IEntidadeDbLib<PermissaoInfo> Members

        #region ConsultarObjeto

        public ConsultarObjetosResponse<PermissaoInfo> ConsultarObjetos(ConsultarObjetosRequest<PermissaoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // NomePermissao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomePermissao");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PERMISSAO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            System.Data.DataTable tb =
                (System.Data.DataTable)
                    Resource.Framework.Library.Db.Oracle.OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PERMISSAO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<PermissaoInfo> resultado = new List<PermissaoInfo>();
            foreach (System.Data.DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<PermissaoInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region ReceberObjeto

        public ReceberObjetoResponse<PermissaoInfo> ReceberObjeto(ReceberObjetoRequest<PermissaoInfo> request)
        {
            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    Resource.Framework.Library.Db.Oracle.OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PERMISSAO_B",
                        "pCODIGO_PERMISSAO", request.CodigoObjeto);

            // Pega as tabelas
            System.Data.DataTable tb = (System.Data.DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<PermissaoInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        #endregion

        #region SalvarObjeto

        public SalvarObjetoResponse<PermissaoInfo> SalvarObjeto(SalvarObjetoRequest<PermissaoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoPermissao;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_PERMISSAO", pk);
            paramsProc.Add("pNOME_PERMISSAO", request.Objeto.NomePermissao);
            paramsProc.Add("pDESCRICAO_PERMISSAO", request.Objeto.DescricaoPermissao);
            paramsProc.Add("retornarRegistro", 'S');

            // Execução a procedure
            Dictionary<string, object> retorno =
                    Resource.Framework.Library.Db.Oracle.OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PERMISSAO_S", paramsProc);

            // Pega as tabelas
            System.Data.DataTable tb = (System.Data.DataTable)retorno["RETCUR"];

            PermissaoInfo permissaoSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<PermissaoInfo>()
                {
                    Objeto = permissaoSalvo
                };
        }

        #endregion

        #region RemoverObjeto

        public RemoverObjetoResponse<PermissaoInfo> RemoverObjeto(RemoverObjetoRequest<PermissaoInfo> request)
        {
            // Monta a execução da procedure
            Resource.Framework.Library.Db.Oracle.OracleDbLib2.Default.ExecutarProcedure(
                "PR_PERMISSAO_R",
                "pCODIGO_PERMISSAO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<PermissaoInfo>();
        }

        #endregion

        #region MontarObjeto

        public PermissaoInfo MontarObjeto(System.Data.DataRow dr)
        {
            // Cria o usuario
            PermissaoInfo permissaoInfo =
                new PermissaoInfo()
                {
                    CodigoPermissao = dr["CODIGO_PERMISSAO"] != DBNull.Value ? dr["CODIGO_PERMISSAO"].ToString() : null,
                    NomePermissao = dr["NOME_PERMISSAO"] != DBNull.Value ? dr["NOME_PERMISSAO"].ToString() : null,
                    DescricaoPermissao = dr["DESCRICAO_PERMISSAO"] != DBNull.Value ? dr["DESCRICAO_PERMISSAO"].ToString() : null
                };

            // Retorna
            return permissaoInfo;
        }

        #endregion

        #endregion
    }
}
