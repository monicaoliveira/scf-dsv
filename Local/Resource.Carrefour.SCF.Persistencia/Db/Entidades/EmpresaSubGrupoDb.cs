﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library.Db.Oracle;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class EmpresaSubGrupoDb : IEntidadeDbLib<EmpresaSubGrupoInfo>
    {
        #region Consultar (Listar)

        public ConsultarObjetosResponse<EmpresaSubGrupoInfo> ConsultarObjetos(ConsultarObjetosRequest<EmpresaSubGrupoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Nome subgrupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeEmpresaSubGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_EMPRESA_SUBGRUPO", condicaoInfo.Valores[0]);

                // Codigo subgrupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoEmpresaSubGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_SUBGRUPO", condicaoInfo.Valores[0]);

                // Codigo grupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoEmpresaGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_GRUPO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_EMPRESA_SUBGRUPO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<EmpresaSubGrupoInfo> resultado = new List<EmpresaSubGrupoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<EmpresaSubGrupoInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<EmpresaSubGrupoInfo> ReceberObjeto(ReceberObjetoRequest<EmpresaSubGrupoInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_EMPRESA_SUBGRUPO_B",
                        "pCODIGO_EMPRESA_SUBGRUPO", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<EmpresaSubGrupoInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<EmpresaSubGrupoInfo> RemoverObjeto(RemoverObjetoRequest<EmpresaSubGrupoInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_EMPRESA_SUBGRUPO_R",
                "pCODIGO_EMPRESA_SUBGRUPO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<EmpresaSubGrupoInfo>();
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<EmpresaSubGrupoInfo> SalvarObjeto(SalvarObjetoRequest<EmpresaSubGrupoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoEmpresaSubGrupo;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_EMPRESA_SUBGRUPO", pk);
            paramsProc.Add("pNOME_EMPRESA_SUBGRUPO", request.Objeto.NomeEmpresaSubGrupo);
            paramsProc.Add("pDESCRICAO_EMPRESA_SUBGRUPO", request.Objeto.DescricaoEmpresaSubGrupo);
            paramsProc.Add("pCODIGO_EMPRESA_GRUPO", request.Objeto.CodigoEmpresaGrupo); 
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_EMPRESA_SUBGRUPO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            EmpresaSubGrupoInfo EmpresaSubGrupoSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<EmpresaSubGrupoInfo>()
                {
                    Objeto = EmpresaSubGrupoSalvo
                };
        }

        #endregion

        #region MontarObjeto

        public EmpresaSubGrupoInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            EmpresaSubGrupoInfo empresaSubGrupoInfo =
                new EmpresaSubGrupoInfo()
                {

                    CodigoEmpresaSubGrupo = dr["CODIGO_EMPRESA_SUBGRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_SUBGRUPO"].ToString() : null,
                    NomeEmpresaSubGrupo = dr["NOME_EMPRESA_SUBGRUPO"] != DBNull.Value ? dr["NOME_EMPRESA_SUBGRUPO"].ToString() : null,
                    DescricaoEmpresaSubGrupo = dr["DESCRICAO_EMPRESA_SUBGRUPO"] != DBNull.Value ? dr["DESCRICAO_EMPRESA_SUBGRUPO"].ToString() : null,
                    CodigoEmpresaGrupo = dr["CODIGO_EMPRESA_GRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_GRUPO"].ToString() : null
                };

            // Retorna
            return empresaSubGrupoInfo;
        #endregion
        }
    }
}
