﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class LogRelatorioVencimentoDbLib : IEntidadeDbLib<LogSCFRelatorioInfo>
    {
        #region IEntidadeDbLib<LogSCFRelatorioVencimentoInfo> Members

        public ConsultarObjetosResponse<LogSCFRelatorioInfo> ConsultarObjetos(ConsultarObjetosRequest<LogSCFRelatorioInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // TipoLog
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoLog" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_LOG", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroData"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LOG_MENOR", condicaoInfo.Valores[0]);

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroData"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LOG_MAIOR", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroData"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_LOG_MENOR", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_LOG_MAIOR", condicaoInfo.Valores[0]);
                }

                // CodigoUsuario
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoUsuario");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_USUARIO", condicaoInfo.Valores[0]);
            }

            // Coloca o número máximo de linhas
            paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LOG_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<LogSCFRelatorioInfo> resultado = new List<LogSCFRelatorioInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<LogSCFRelatorioInfo>()
                {
                    Resultado = resultado
                };
        }

        public ReceberObjetoResponse<LogSCFRelatorioInfo> ReceberObjeto(ReceberObjetoRequest<LogSCFRelatorioInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LOG_B", "pCODIGO_LOG", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<LogSCFRelatorioInfo>()
                {
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        public SalvarObjetoResponse<LogSCFRelatorioInfo> SalvarObjeto(SalvarObjetoRequest<LogSCFRelatorioInfo> request)
        {
            // Infere a pk
            string pk = request.Objeto.CodigoLog;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Serializa o objeto
            MemoryStream ms = new MemoryStream();
            new XmlSerializer(
                request.Objeto.GetType(), this.listarTiposExtras()).Serialize(ms, request.Objeto);
            ms.Position = 0;
            StreamReader reader = new StreamReader(ms);
            string serializacaoObjeto = reader.ReadToEnd();
            reader.Close();

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_LOG", pk);
            paramsProc.Add("pCODIGO_USUARIO", request.Objeto.CodigoUsuario);
            paramsProc.Add("pDESCRICAO_LOG", request.Objeto.Descricao.Length <= 4000 ? request.Objeto.Descricao : request.Objeto.Descricao.Substring(0, 4000));
            paramsProc.Add("pDATA_LOG", DateTime.Now);//SCF1175 paramsProc.Add("pDATA_LOG", request.Objeto.DataLog);
            paramsProc.Add("pTIPO_LOG", request.Objeto.GetType().Name);
            paramsProc.Add("pSERIALIZACAO_LOG", serializacaoObjeto);
            paramsProc.Add("pCODIGO_ORIGEM", request.Objeto.CodigoOrigem);
            paramsProc.Add("pTIPO_ORIGEM", request.Objeto.TipoOrigem);
            paramsProc.Add("retornarRegistro", "S");

            // Executa a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_LOG_S", paramsProc)["RETCUR"];

            // Log em arquivo
            LogArquivo.Objeto("log", request.Objeto);

            // Retorna
            return
                new SalvarObjetoResponse<LogSCFRelatorioInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        public RemoverObjetoResponse<LogSCFRelatorioInfo> RemoverObjeto(RemoverObjetoRequest<LogSCFRelatorioInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_LOG_R",
                "pCODIGO_LOG", request.CodigoObjeto);

            // Retorna
            return
                new RemoverObjetoResponse<LogSCFRelatorioInfo>()
                {
                    CodigoMensagemRequest = request.CodigoMensagem
                };
        }

        public LogSCFRelatorioInfo MontarObjeto(DataRow dr)
        {
            // Resolve o tipo
            Type tipoLog = ResolutorTipos.Resolver((string)dr["TIPO_LOG"]);

            // Desserializa o log
            LogSCFRelatorioInfo logInfo =
                (LogSCFRelatorioInfo)
                    new XmlSerializer(tipoLog, this.listarTiposExtras()).Deserialize(
                        new MemoryStream(
                            ASCIIEncoding.UTF8.GetBytes((string)dr["SERIALIZACAO_LOG"])));

            // Preenche com as informações do banco
            logInfo.CodigoLog = dr["CODIGO_LOG"].ToString();
            logInfo.CodigoUsuario = dr["CODIGO_USUARIO"] != DBNull.Value ? dr["CODIGO_USUARIO"].ToString() : null;
            logInfo.Descricao = dr["DESCRICAO_LOG"] != DBNull.Value ? dr["DESCRICAO_LOG"].ToString() : null;
            logInfo.DataLog = dr["DATA_LOG"] != DBNull.Value ? (DateTime)dr["DATA_LOG"] : DateTime.MinValue;

            // Retorna
            return logInfo;
        }

        /// <summary>
        /// Retorna lista de tipos extras para serializacao
        /// </summary>
        /// <returns></returns>
        private Type[] listarTiposExtras()
        {
            return
                new Type[] 
                { 
                }.ToArray();
        }

        #endregion
    }
}
