﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db.Oracle;

// using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class UsuarioPermissaoDbLib 
    {
        
        #region Consultar

        public List<PermissaoAssociadaInfo> ConsultarObjetos(List<CondicaoInfo> condicoes)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (condicoes != null)
            {
                // CodigoUsuario
                condicaoInfo = condicoes.Find(c => c.Propriedade == "CodigoUsuario");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_USUARIO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_USUARIO_PERMISSAO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<PermissaoAssociadaInfo> resultado = new List<PermissaoAssociadaInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return resultado;
                
        }

        #endregion

        #region Remover

        public bool RemoverObjeto(string codigoUsuario, string codigoPermissao)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_USUARIO_PERMISSAO_R",
                "pCODIGO_USUARIO", codigoUsuario,
                "pCODIGO_PERMISSAO", codigoPermissao);

            // Retorna
            return true;
        }

        #endregion

        #region Salvar

        public PermissaoAssociadaInfo SalvarObjeto(PermissaoAssociadaInfo permissaoAssociada, string codigoUsuario)
        {
            
            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_USUARIO", codigoUsuario);
            paramsProc.Add("pCODIGO_PERMISSAO", permissaoAssociada.CodigoPermissao);
            paramsProc.Add("pSTATUS_PERMISSAO", OracleDbLib2.EnumToDb<PermissaoAssociadaStatusEnum>(permissaoAssociada.Status));
            paramsProc.Add("retornarRegistro", "S");

            // Execução da procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_USUARIO_PERMISSAO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return this.MontarObjeto(tb.Rows[0]);
                
        }

        #endregion
                
        #region MontarObjeto 

        public PermissaoAssociadaInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            PermissaoAssociadaInfo permissaoAssociadaInfo = new 
                PermissaoAssociadaInfo() 
                        { 
                            CodigoPermissao = dr["CODIGO_PERMISSAO"].ToString(),
                            Status = OracleDbLib2.EnumToObject<PermissaoAssociadaStatusEnum>(dr["STATUS_PERMISSAO"])
                        };

            // Retorna
            return permissaoAssociadaInfo;
        }

        #endregion

        
    }
}
