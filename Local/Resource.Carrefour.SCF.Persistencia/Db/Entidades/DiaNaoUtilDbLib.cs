﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class DiaNaoUtilDbLib : IEntidadeDbLib<DiaNaoUtilInfo>
    {
        #region IEntidadeDbLib<DiaNaoUtilInfo> Members

        #region Consultar

        public ConsultarObjetosResponse<DiaNaoUtilInfo> ConsultarObjetos(ConsultarObjetosRequest<DiaNaoUtilInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // NomeDiaNaoUtil
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeDiaNaoUtil");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_DIA_NAO_UTIL", condicaoInfo.Valores[0]);

                // Ativo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Ativo");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_ATIVO", (bool)condicaoInfo.Valores[0] ? "S" : "N");

                // Data Maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataDiaNaoUtil" && c.TipoCondicao == CondicaoTipoEnum.Maior);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_MAIOR", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_DIA_NAO_UTIL_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<DiaNaoUtilInfo> resultado = new List<DiaNaoUtilInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<DiaNaoUtilInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<DiaNaoUtilInfo> ReceberObjeto(ReceberObjetoRequest<DiaNaoUtilInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_DIA_NAO_UTIL_B",
                        "pCODIGO_DIA_NAO_UTIL", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<DiaNaoUtilInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) :  null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<DiaNaoUtilInfo> RemoverObjeto(RemoverObjetoRequest<DiaNaoUtilInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_DIA_NAO_UTIL_R",
                "pCODIGO_DIA_NAO_UTIL", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<DiaNaoUtilInfo>();
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<DiaNaoUtilInfo> SalvarObjeto(SalvarObjetoRequest<DiaNaoUtilInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoDiaNaoUtil;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_DIA_NAO_UTIL", pk);
            paramsProc.Add("pCLASSIFICACAO_DIA_NAO_UTIL", request.Objeto.ClassificacaoDiaNaoUtil);
            paramsProc.Add("pDESCRICAO_DIA_NAO_UTIL", request.Objeto.DescricaoDiaNaoUtil);
            paramsProc.Add("pDATA_DIA_NAO_UTIL", request.Objeto.DataDiaNaoUtil);
            paramsProc.Add("pATIVO", request.Objeto.Ativo == true ? "S" : "N");
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_DIA_NAO_UTIL_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            DiaNaoUtilInfo produtoSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<DiaNaoUtilInfo>()
                {
                    Objeto = produtoSalvo
                };
        }

        #endregion

        #endregion

        #region MontarObjeto 

        public DiaNaoUtilInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            DiaNaoUtilInfo produtoInfo =
                new DiaNaoUtilInfo()
                {

                    CodigoDiaNaoUtil = dr["CODIGO_DIA_NAO_UTIL"] != DBNull.Value ? dr["CODIGO_DIA_NAO_UTIL"].ToString() : null,
                    Ativo = dr["ATIVO"] != null ? dr["ATIVO"].ToString() == "S" ? true : false : false,
                    ClassificacaoDiaNaoUtil = dr["CLASSIFICACAO_DIA_NAO_UTIL"] != DBNull.Value ? dr["CLASSIFICACAO_DIA_NAO_UTIL"].ToString() : null,
                    DescricaoDiaNaoUtil = dr["DESCRICAO_DIA_NAO_UTIL"] != DBNull.Value ? dr["DESCRICAO_DIA_NAO_UTIL"].ToString() : null,
                    DataDiaNaoUtil = dr["DATA_DIA_NAO_UTIL"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_DIA_NAO_UTIL"]) : null
                };

            // Retorna
            return produtoInfo;
        }

        #endregion
    }
}
