﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Classe de persistencia de favorecidos
    /// </summary>
    public class ContaFavorecidoProdutoDbLib : IEntidadeDbLib<ContaFavorecidoProdutoInfo>
    {
        #region IEntidadeDbLib<ContaFavorecidoProdutoInfo> Members

        /// <summary>
        /// Consulta favorecidos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<ContaFavorecidoProdutoInfo> ConsultarObjetos(ConsultarObjetosRequest<ContaFavorecidoProdutoInfo> request)
        {

            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Codigo da conta do Favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoProduto");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PRODUTO", condicaoInfo.Valores[0]);

                // Codigo da conta do Favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoContaFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_CONTA_FAVORECIDO", condicaoInfo.Valores[0]);

                // Codigo da conta do Favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_FAVORECIDO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CONTA_PRODUTO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ContaFavorecidoProdutoInfo> resultado = new List<ContaFavorecidoProdutoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ContaFavorecidoProdutoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<ContaFavorecidoProdutoInfo> ReceberObjeto(ReceberObjetoRequest<ContaFavorecidoProdutoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Salva um favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<ContaFavorecidoProdutoInfo> SalvarObjeto(SalvarObjetoRequest<ContaFavorecidoProdutoInfo> request)
        {

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_CONTA_FAVORECIDO", request.Objeto.CodigoContaFavorecido);
            paramsProc.Add("pCODIGO_PRODUTO", request.Objeto.CodigoProduto); 
            paramsProc.Add("retornarRegistro", "N");

            // Execução a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_CONTA_PRODUTO_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<ContaFavorecidoProdutoInfo>() { };
        }

        /// <summary>
        /// Remove um favorecido
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<ContaFavorecidoProdutoInfo> RemoverObjeto(RemoverObjetoRequest<ContaFavorecidoProdutoInfo> request)
        {
            RemoverContaFavorecidoDbRequest novoRequest = (RemoverContaFavorecidoDbRequest)request;

            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_CONTA_PRODUTO_R",
                "pCODIGO_CONTA_FAVORECIDO", novoRequest.CodigoContaFavorecido,
                "pCODIGO_PRODUTO", novoRequest.CodigoProduto);

            // Retorna
            return new RemoverObjetoResponse<ContaFavorecidoProdutoInfo>();
        }

        /// <summary>
        /// Monsta um favorecido
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ContaFavorecidoProdutoInfo MontarObjeto(DataRow dr)
        {
            // Retorna
            return
                new ContaFavorecidoProdutoInfo()
                {                    
                    ID = dr["ID"].ToString(),
                    CodigoProduto = dr["CODIGO_PRODUTO"].ToString(),
                    NomeProduto = dr["NOME_PRODUTO"] != DBNull.Value ? dr["NOME_PRODUTO"].ToString() : null,
                    Banco = dr["BANCO"] != DBNull.Value ? dr["BANCO"].ToString() : null,
                    Agencia = dr["AGENCIA"] != DBNull.Value ? dr["AGENCIA"].ToString() : null,
                    Conta = dr["CONTA"] != DBNull.Value ? dr["CONTA"].ToString() : null,
                    CodigoContaFavorecido = dr["CODIGO_CONTA_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_CONTA_FAVORECIDO"].ToString() : null,
                    Agencia_CCI = dr["AGENCIA_CCI"] != DBNull.Value ? dr["AGENCIA_CCI"].ToString() : null,
                    Conta_CCI = dr["CONTA_CCI"] != DBNull.Value ? dr["CONTA_CCI"].ToString() : null
                };
        }

        #endregion
    }
}
