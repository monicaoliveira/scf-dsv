﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Db.Oracle;
using Resource.Carrefour.SCF.Contratos.Principal;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Classe de persistencia de Estabelecimento
    /// </summary>
    public class EstabelecimentoDbLib : IEntidadeDbLib<EstabelecimentoInfo>
    {
        #region IEntidadeDbLib<EstabelecimentoInfo> Members

        /// <summary>
        /// Consulta estabelecimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<EstabelecimentoInfo> ConsultarObjetos(ConsultarObjetosRequest<EstabelecimentoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {

                // Filtro Codigo do Favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_FAVORECIDO", SCFUtils.CompletarCNPJ(condicaoInfo.Valores[0].ToString()));
                
                // CNPJ
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CnpjFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CNPJ_FAVORECIDO", SCFUtils.CompletarCNPJ(condicaoInfo.Valores[0].ToString()));
                    
                // CNPJ
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoCnpj");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CNPJ", SCFUtils.CompletarCNPJ(condicaoInfo.Valores[0].ToString()));

                // Razao Social
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "RazaoSocial");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_RAZAO_SOCIAL", condicaoInfo.Valores[0]);

                // Codigo CSU
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoCsu");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_CSU", condicaoInfo.Valores[0]);

                // Sitef
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoSitef");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_SITEF", condicaoInfo.Valores[0]);

                // SubGrupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoSubGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_SUBGRUPO", condicaoInfo.Valores[0]);

                // Grupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_GRUPO", condicaoInfo.Valores[0]);

                // MaxLinhas
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "MaxLinhas");
                if (condicaoInfo != null)
                    paramsProc.Add("maxLinhas", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_ESTABELECIMENTO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<EstabelecimentoInfo> resultado = new List<EstabelecimentoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<EstabelecimentoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<EstabelecimentoInfo> ReceberObjeto(ReceberObjetoRequest<EstabelecimentoInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_ESTABELECIMENTO_B", "pCODIGO_ESTABELECIMENTO", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<EstabelecimentoInfo>()
                {
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        /// <summary>
        /// Salva um estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<EstabelecimentoInfo> SalvarObjeto(SalvarObjetoRequest<EstabelecimentoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoEstabelecimento;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_ESTABELECIMENTO", pk);
            paramsProc.Add("pCNPJ", SCFUtils.CompletarCNPJ(request.Objeto.CNPJ));
            paramsProc.Add("pRAZAO_SOCIAL", request.Objeto.RazaoSocial);
            paramsProc.Add("pBANCO", request.Objeto.Banco);
            paramsProc.Add("pAGENCIA", request.Objeto.Agencia);
            paramsProc.Add("pCONTA", request.Objeto.Conta);
            paramsProc.Add("pESTABELECIMENTO_CSU", request.Objeto.EstabelecimentoCSU);
            paramsProc.Add("pESTABELECIMENTO_SITEF", request.Objeto.EstabelecimentoSitef);
            paramsProc.Add("pCODIGO_EMPRESA_SUBGRUPO", request.Objeto.CodigoEmpresaSubGrupo);
            paramsProc.Add("pCODIGO_FAVORECIDO", request.Objeto.CodigoFavorecido);
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_ESTABELECIMENTO_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<EstabelecimentoInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        /// <summary>
        /// Remove um estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<EstabelecimentoInfo> RemoverObjeto(RemoverObjetoRequest<EstabelecimentoInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_ESTABELECIMENTO_R",
                "pCODIGO_ESTABELECIMENTO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<EstabelecimentoInfo>();
        }

        /// <summary>
        /// Monsta um estabelecimento
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public EstabelecimentoInfo MontarObjeto(DataRow dr)
        {
            // Retorna
            return
                new EstabelecimentoInfo()
                {
                    CodigoEstabelecimento = dr["CODIGO_ESTABELECIMENTO"].ToString(),
                    CNPJ = dr["CNPJ"] != DBNull.Value ? SCFUtils.CompletarCNPJ(dr["CNPJ"].ToString()) : null,
                    RazaoSocial = dr["RAZAO_SOCIAL"] != DBNull.Value ? dr["RAZAO_SOCIAL"].ToString() : null,
                    Banco = dr["BANCO"] != DBNull.Value ? dr["BANCO"].ToString() : null,
                    Agencia = dr["AGENCIA"] != DBNull.Value ? dr["AGENCIA"].ToString() : null,
                    Conta = dr["CONTA"] != DBNull.Value ? dr["CONTA"].ToString() : null,
                    CodigoFavorecido = dr["CODIGO_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_FAVORECIDO"].ToString() : null,
                    EstabelecimentoCSU = dr["ESTABELECIMENTO_CSU"] != DBNull.Value ? dr["ESTABELECIMENTO_CSU"].ToString() : "",
                    EstabelecimentoSitef = dr["ESTABELECIMENTO_SITEF"] != DBNull.Value ? dr["ESTABELECIMENTO_SITEF"].ToString() : "",
                    CodigoEmpresaGrupo = dr["CODIGO_EMPRESA_GRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_GRUPO"].ToString() : "",
                    CodigoEmpresaSubGrupo = dr["CODIGO_EMPRESA_SUBGRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_SUBGRUPO"].ToString() : "",
                    NomeFavorecido = dr["NOME_FAVORECIDO"] != DBNull.Value ? dr["NOME_FAVORECIDO"].ToString() : "", //1351
                    NomeGrupo = dr["NOME_EMPRESA_GRUPO"] != DBNull.Value ? dr["NOME_EMPRESA_GRUPO"].ToString() : "", //1351
                    NomeSubGrupo = dr["NOME_EMPRESA_SUBGRUPO"] != DBNull.Value ? dr["NOME_EMPRESA_SUBGRUPO"].ToString() : "", //1351
                };
        }

        #endregion
    }
}
