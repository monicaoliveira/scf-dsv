﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class PlanoDbLib : IEntidadeDbLib<PlanoInfo>
    {
        #region IEntidadeDbLib<PlanoInfo> Members

        #region Consultar

        public ConsultarObjetosResponse<PlanoInfo> ConsultarObjetos(ConsultarObjetosRequest<PlanoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Ativo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Ativo");
                if (condicaoInfo != null)
                    paramsProc.Add("pATIVO", (bool) condicaoInfo.Valores[0] == true ? 'S' : 'N');

                // NomePlano
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomePlano");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_PLANO", condicaoInfo.Valores[0]);

                // DescricaoPlano
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DescricaoPlano");
                if (condicaoInfo != null)
                    paramsProc.Add("pDESCRICAO_PLANO", condicaoInfo.Valores[0]);

                // QuantidadeParcelas
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "QuantidadeParcelas");
                if (condicaoInfo != null)
                    paramsProc.Add("pQUANTIDADE_PARCELAS", condicaoInfo.Valores[0]);

                // Comissao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Comissao");
                if (condicaoInfo != null)
                    paramsProc.Add("pCOMISSAO", condicaoInfo.Valores[0]);

                // Codigo TSYS
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoPlanoTSYS");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PLANO_TSYS", condicaoInfo.Valores[0]);

                // Codigo Produto
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoProduto");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PRODUTO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PLANO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<PlanoInfo> resultado = new List<PlanoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<PlanoInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<PlanoInfo> ReceberObjeto(ReceberObjetoRequest<PlanoInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PLANO_B",
                        "pCODIGO_PLANO", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<PlanoInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) :  null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<PlanoInfo> RemoverObjeto(RemoverObjetoRequest<PlanoInfo> request)
        {
            RemoverObjetoResponse<PlanoInfo> resposta = new RemoverObjetoResponse<PlanoInfo>();
            resposta.RepassarExcessao = true;

            try
            {
                // Prepara flags
                bool removeAssociacoesPlanos = false;

                // Verifica se é mensagem especifica
                if (request is RemoverPlanoDbRequest)
                {
                    // Pega mensagem com o tipo correto
                    RemoverPlanoDbRequest request2 = (RemoverPlanoDbRequest)request;

                    // Seta flags
                    removeAssociacoesPlanos = request2.RemoverAssociacoesPlanos;
                }

                // Monta a execução da procedure
                OracleDbLib2.Default.ExecutarProcedure(
                    "PR_PLANO_R",
                    "pCODIGO_PLANO", request.CodigoObjeto,
                    "pREMOVER_ASSOC_PLANOS", removeAssociacoesPlanos ? 'S' : 'N');
            }
            catch (Exception ex)
            {
                // trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<PlanoInfo> SalvarObjeto(SalvarObjetoRequest<PlanoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoPlano;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_PLANO", pk);
            paramsProc.Add("pCODIGO_PLANO_TSYS", request.Objeto.CodigoPlanoTSYS);
            paramsProc.Add("pNOME_PLANO", request.Objeto.NomePlano);
            paramsProc.Add("pDESCRICAO_PLANO", request.Objeto.DescricaoPlano);
            paramsProc.Add("pQUANTIDADE_PARCELAS", request.Objeto.QuantidadeParcelas);
            paramsProc.Add("pCOMISSAO", request.Objeto.Comissao);
            paramsProc.Add("pATIVO", request.Objeto.Ativo ? 'S' : 'N');
            paramsProc.Add("retornarRegistro", 'S');

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PLANO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            PlanoInfo planoSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<PlanoInfo>()
                {
                    Objeto = planoSalvo
                };
        }

        #endregion

        #endregion

        #region MontarObjeto 

        public PlanoInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            PlanoInfo planoInfo =
                new PlanoInfo()
                {
                    CodigoPlano = dr["CODIGO_PLANO"] != DBNull.Value ? dr["CODIGO_PLANO"].ToString() : null,
                    CodigoPlanoTSYS = dr["CODIGO_PLANO_TSYS"] != DBNull.Value ? dr["CODIGO_PLANO_TSYS"].ToString() : null,
                    NomePlano = dr["NOME_PLANO"] != DBNull.Value ? dr["NOME_PLANO"].ToString() : null,
                    DescricaoPlano = dr["DESCRICAO_PLANO"] != DBNull.Value ? dr["DESCRICAO_PLANO"].ToString() : null,
                    QuantidadeParcelas = dr["QUANTIDADE_PARCELAS"] != DBNull.Value ? (int?) Convert.ToInt32(dr["QUANTIDADE_PARCELAS"].ToString()) : null,
                    Comissao = dr["COMISSAO"] != DBNull.Value ? new double?((float)dr["COMISSAO"]) : 0,
                    Ativo = dr["ATIVO"] != DBNull.Value ? dr["ATIVO"].ToString() == "S" : false
                };            

            // Retorna
            return planoInfo;
        }

        #endregion

    }
}
