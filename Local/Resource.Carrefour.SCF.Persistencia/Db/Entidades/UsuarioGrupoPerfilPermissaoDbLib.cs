﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db.Oracle;

//using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class UsuarioGrupoPerfilPermissaoDbLib
    {
        #region Consultar

        public List<string> ConsultarObjetosPorGrupo(string codigoGrupoUsuario)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_USUARIO", codigoGrupoUsuario);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PERMISSAO_GRUPO_PERFIL_L", paramsProc)["RETCUR"];

            //// Preenche a coleção resultado
            //List<UsuarioGrupoInfo> resultado = new List<UsuarioGrupoInfo>();
            //foreach (DataRow dr in tb.Rows)
            //    resultado.Add(this.MontarObjeto(dr));

            List<string> resultado = new List<string>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(dr["CODIGO_PERMISSAO"].ToString());

            // Retorna
            return resultado;



        }

        #endregion
    }
}
