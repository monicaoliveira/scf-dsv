﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia de Arquivo
    /// </summary>
    public class RelatorioVecimentoDbLib : IEntidadeDbLib<RelatorioVencimentoInfo>
    {
        #region IEntidadeDbLib<ArquivoInfo> Members

        /// <summary>
        /// Recebe arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<RelatorioVencimentoInfo> ReceberObjeto(ReceberObjetoRequest<RelatorioVencimentoInfo> request)
        {
            return null;
        }

        public SalvarObjetoResponse<RelatorioVencimentoInfo> SalvarObjeto(SalvarObjetoRequest<RelatorioVencimentoInfo> request)
        {
            return null;
        }

        public ConsultarObjetosResponse<RelatorioVencimentoInfo> ConsultarObjetos(ConsultarObjetosRequest<RelatorioVencimentoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;
            //1353 string codLog = "";

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Tipo do Processo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEmpresaGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_GRUPO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEmpresaSubGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_SUB_GRUPO", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroProduto" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PRODUTO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroTipoRegistro" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_REGISTRO", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroFavorecido" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_FAVORECIDO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidacaoAgendamentoVencimento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_AG_VC", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidacaoMaiorData" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_MAIOR", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroStatusRetornoCessao" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pSTATUS_RETORNO_CESSAO", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataTransacao"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_TRANSACAO", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataTransacao"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_TRANSACAO", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataTransacao"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_INICIO_TRANSACAO", condicaoInfo.Valores[0]);
                    paramsProc.Add("pDATA_FIM_TRANSACAO", condicaoInfo.Valores[0]);
                }

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataVencimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_VENCIMENTO", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataVencimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_VENCIMENTO", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataVencimento"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_INICIO_VENCIMENTO", condicaoInfo.Valores[0]);
                    paramsProc.Add("pDATA_FIM_VENCIMENTO", condicaoInfo.Valores[0]);
                }
            
                // Data Movimento - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMovimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_MOVIMENTO", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMovimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_MOVIMENTO", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMovimento"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_INICIO_MOVIMENTO", condicaoInfo.Valores[0]);
                    paramsProc.Add("pDATA_FIM_MOVIMENTO", condicaoInfo.Valores[0]);
                }

                // Data Agendamento - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataAgendamento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_AGENDAMENTO", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataAgendamento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_AGENDAMENTO", condicaoInfo.Valores[0]);

                // Data Posicao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataPosicao"
                    && (c.TipoCondicao == CondicaoTipoEnum.Igual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_POSICAO_CARTEIRA", condicaoInfo.Valores[0]);

                //1353 
                // FILTRO DESNECESSÁRIO, NÃO EXISTE ESTE CAMPO A SER FILTRADO NO RELATORIO VENCIMENTO [ codlog ]
                // retirado dos parametros da procedure
                // CodLog
                //condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodLog");
                //if (condicaoInfo != null && null != condicaoInfo.Valores[0])
                //{
                //    codLog = condicaoInfo.Valores[0].ToString();
                //    paramsProc.Add("pCOD_LOG", condicaoInfo.Valores[0]);
                //}

               

                // Tipo Financeiro Contabil
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroFinanceiroContabil");
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_FINANCEIRO_CONTABIL", condicaoInfo.Valores[0]);

                //SCF1170 - inicio
                // Data Retorno CCI - Fim
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltrotxtDataRetornoCCI"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_CCI", condicaoInfo.Valores[0]);

                // Data Retorno CCI - Inicio
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltrotxtDataRetornoCCI"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_CCI", condicaoInfo.Valores[0]);

                // Data Retorno CCI igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltrotxtDataRetornoCCI"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_FIM_CCI", condicaoInfo.Valores[0]);
                    paramsProc.Add("pDATA_INICIO_CCI", condicaoInfo.Valores[0]);
                }

                //Referencia selecionada
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroReferencia" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pREFERENCIA", condicaoInfo.Valores[0]); 

                //Consolidação de Referencia
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidacaoReferencia" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_REFERENCIA", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");
          
                //SCF1170 - inicio

                // codigo da origem - Essa informação será utilizada para gravar o log no processo de geracao de pagamentos
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoProcesso");
                if (condicaoInfo != null && null != condicaoInfo.Valores[0])
                {
                    paramsProc.Add("pCODIGO_PROCESSO", condicaoInfo.Valores[0]);
                }

                // Gerar Pagamentos -- 2017-Melhorias
                // Parametro que irá informar se é para processar o relatório ou gerar os pagamentos
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "GerarPagamentos" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null && Convert.ToBoolean(condicaoInfo.Valores[0]) == true)
                    paramsProc.Add("pGERAR_PAGAMENTOS", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // Retornar Transações Pagas 
                // Parametro que irá informar se é para retornar as transações já pagas no relatório 
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroRetornarTransacaoPaga" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null && Convert.ToBoolean(condicaoInfo.Valores[0]) == true)
                    paramsProc.Add("pRETORNAR_PAGOS", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");


            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_RELATORIO_VENCIMENTO_L2", paramsProc)["RETCUR"];
            
            // Se solicitou a geracao de pagamentos, retorna com a lista vazia
            condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "GerarPagamentos" && c.TipoCondicao == CondicaoTipoEnum.Igual);
            if (condicaoInfo != null && Convert.ToBoolean(condicaoInfo.Valores[0]) == true)
            {
                List<RelatorioVencimentoInfo> resultado2 = new List<RelatorioVencimentoInfo>();
                return
                    new ConsultarObjetosResponse<RelatorioVencimentoInfo>()
                    {
                        Resultado = resultado2
                    };
            }


            if (request is ListarRelatorioVencimentoDbRequest)
            {
                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //new SalvarLogRequest()
                //{
                //    CodigoUsuario = "",
                //    Descricao = "APP - INF - ListarRelatorioVencimento - Contagem do Objeto.",
                //    TipoOrigem = "APP",
                //    CodigoOrigem = codLog
                //});

                return
                    new ListarRelatorioVencimentoDbResponse()
                    {
                        QuantidadeLinhas = tb.Rows.Count
                    };
            }
            //1353
            //Mensageria.Processar<SalvarLogResponse>(
            //    new SalvarLogRequest()
            //    {
            //        CodigoUsuario = "",
            //        Descricao = "APP - INI - ListarRelatorioVencimento - Iniciando montagem do Objeto.",
            //        TipoOrigem = "APP",
            //        CodigoOrigem = codLog
            //    });

            // Preenche a coleção resultado
            List<RelatorioVencimentoInfo> resultado = new List<RelatorioVencimentoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));
            //1353
            //Mensageria.Processar<SalvarLogResponse>(
            //    new SalvarLogRequest()
            //    {
            //        CodigoUsuario = "",
            //        Descricao = "APP - FIM - ListarRelatorioVencimento - Fim da montagem do Objeto.",
            //        TipoOrigem = "APP",
            //        CodigoOrigem = codLog
            //    });

            // Retorna
            return
                new ConsultarObjetosResponse<RelatorioVencimentoInfo>()
                {
                    Resultado = resultado
                };

            //ListarRelatorioVencimentoDbResponse response = new ListarRelatorioVencimentoDbResponse();
                
            //var tbresultado = OracleDbLib.Default.ExecutarProcedure(
            //        "PR_RELATORIO_VENCIMENTO_B", paramsProc);

            //DataTable tb = null;
            //DataTable tbNumLinhas = null;

            //if (tbresultado.ContainsKey("RETCUR"))
            //{
            //    tb = (DataTable)tbresultado["RETCUR"];

            //    if (tb != null)
            //    {
            //        // Preenche a coleção resultado
            //        List<RelatorioVencimentoInfo> resultado = new List<RelatorioVencimentoInfo>();
            //        foreach (DataRow dr in tb.Rows)
            //            resultado.Add(this.MontarObjeto(dr));
            //        response.Resultado = resultado;
            //    }
            //}

            //if (tbresultado.ContainsKey("RETCURNUMEROLINHAS"))
            //{
            //    tbNumLinhas = (DataTable)tbresultado["RETCURNUMEROLINHAS"];
            //    if (tbNumLinhas != null)
            //    {
            //        if (tbNumLinhas.Rows.Count > 0)
            //            response.QuantidadeLinhas = Convert.ToInt32(tbNumLinhas.Rows[0]["NUMERO_LINHAS"]);
            //        else
            //            response.QuantidadeLinhas = 0;
            //    }
            //}
        }

        public RemoverObjetoResponse<RelatorioVencimentoInfo> RemoverObjeto(RemoverObjetoRequest<RelatorioVencimentoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Monta arquivo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public RelatorioVencimentoInfo MontarObjeto(System.Data.DataRow dr)
        {
            EstabelecimentoInfo estabelecimento = new EstabelecimentoInfo()
            {
                // pagamento
                Agencia = dr["AGENCIA"] != DBNull.Value ? dr["AGENCIA"].ToString() : null,
                Banco = dr["BANCO"] != DBNull.Value ? dr["BANCO"].ToString() : null,
                CNPJ = dr["CNPJ"] != DBNull.Value ? dr["CNPJ"].ToString() : null,
                Conta = dr["CONTA"] != DBNull.Value ? dr["CONTA"].ToString() : null,
                CodigoEstabelecimento = dr["CODIGO_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_FAVORECIDO"].ToString() : null,
                RazaoSocial = dr["NOME_FAVORECIDO"] != DBNull.Value ? dr["NOME_FAVORECIDO"].ToString() : null
            };

            ProdutoInfo produto = new ProdutoInfo()
            {                
                DescricaoProduto = dr["DESCRICAO_PRODUTO"] != DBNull.Value ? dr["DESCRICAO_PRODUTO"].ToString() : null
            };

            ListaItemInfo referencia = new ListaItemInfo()
            {
                //Valor = dr["REFERENCIA"] != DBNull.Value ? dr["REFERENCIA"].ToString() : null
                Valor = dr.Table.Columns.Contains("REFERENCIA") ? dr["REFERENCIA"].ToString() : null,
                Descricao = dr.Table.Columns.Contains("NOME_REFERENCIA") ? dr["NOME_REFERENCIA"].ToString() : null
            };


            DateTime? dataRepasse;

            if (dr.Table.Columns.Contains("DATA_REPASSE"))
                dataRepasse = dr["DATA_REPASSE"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_REPASSE"]) : null;
            else
                dataRepasse = null;

            //SCF1157 - inicio
            DateTime? dataRepasseCalculada;

            if (dr.Table.Columns.Contains("DATA_REPASSE_CALCULADA"))
                dataRepasseCalculada = dr["DATA_REPASSE_CALCULADA"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_REPASSE_CALCULADA"]) : null;
            else
                dataRepasseCalculada = null;
            //SCF1157 - fim

            DateTime? dataPosicao;
            if (dr["POSICAO_CARTEIRA"] != DBNull.Value)
            {
                try { dataPosicao = new DateTime?(Convert.ToDateTime(dr["POSICAO_CARTEIRA"])); }
                catch (Exception ) { }         // catch (Exception ex) { } // warning 25/02/2019
                finally { dataPosicao = null; }
            }
            else
                dataPosicao = null;

            RelatorioVencimentoInfo relatorioVencimentoInfo = new RelatorioVencimentoInfo()
            {
                CodigoEmpresaGrupo = dr["CODIGO_EMPRESA_GRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_GRUPO"].ToString() : null,
                DescricaoEmpresaGrupo = dr["DESCRICAO_EMPRESA_GRUPO"] != DBNull.Value ? dr["DESCRICAO_EMPRESA_GRUPO"].ToString() : null,
                Estabelecimento = estabelecimento,
                Produto = produto,
                Referencia = referencia,
                // nao usado
                //CodigoPagamento = dr["CODIGO_PAGAMENTO"] != DBNull.Value ? dr["CODIGO_PAGAMENTO"].ToString() : null,
                ValorLiquido = dr["VALOR_LIQUIDO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_LIQUIDO"]) : 0,
                // Fernando Orbite 16/04/2014 - SCF-901 - Inicio
                ValorBruto = dr["VALOR_BRUTO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_BRUTO"]) : 0,
                ValorComissao = dr["VALOR_COMISSAO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_COMISSAO"]) : 0,
                // Fernando Orbite 16/04/2014 - SCF-901 - Fim
                //DataVencimento = dr["DATA_REPASSE"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_REPASSE"]) : null,
                //DataAgendamento = dr["DATA_REPASSE_CALCULADA"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_REPASSE_CALCULADA"]) : null, //SCF1157
                DataAgendamento = dataRepasseCalculada, //SCF1157
                DataVencimento = dataRepasse,
                DataPosicaoCarteira = dataPosicao
            };

            // Retorna
            return relatorioVencimentoInfo;
        }

        #endregion
    }
}
