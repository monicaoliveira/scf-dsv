﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Framework.Library.Db.Oracle;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class ListaResumoDbLib : IEntidadeDbLib<ListaResumoInfo>
    {
        #region IEntidadeDbLib<ListaResumoInfo> Members

        public ConsultarObjetosResponse<ListaResumoInfo> ConsultarObjetos(ConsultarObjetosRequest<ListaResumoInfo> request)
        {
            // Faz a consulta
            DataTable tb =
                (DataTable) OracleDbLib2.Default.ExecutarProcedure("PR_LISTA_L")["RETCUR"];


            // Prepara resposta
            ConsultarObjetosResponse<ListaResumoInfo> resposta = 
                new ConsultarObjetosResponse<ListaResumoInfo>();

            // Monta os objetos
            foreach (DataRow dr in tb.Rows)
                resposta.Resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return resposta;
        }

        public ReceberObjetoResponse<ListaResumoInfo> ReceberObjeto(ReceberObjetoRequest<ListaResumoInfo> request)
        {
            throw new NotImplementedException();
        }

        public SalvarObjetoResponse<ListaResumoInfo> SalvarObjeto(SalvarObjetoRequest<ListaResumoInfo> request)
        {
            throw new NotImplementedException();
        }

        public RemoverObjetoResponse<ListaResumoInfo> RemoverObjeto(RemoverObjetoRequest<ListaResumoInfo> request)
        {
            throw new NotImplementedException();
        }

        public ListaResumoInfo MontarObjeto(DataRow dr)
        {
            // Retorna o objeto
            return 
                new ListaResumoInfo() 
                {
                    CodigoLista = dr["CODIGO_LISTA"].ToString(),
                    Descricao = dr["NOME_LISTA"] != DBNull.Value ? (string)dr["NOME_LISTA"] : null,
                    Mnemonico = dr["MNEMONICO_LISTA"] != DBNull.Value ? (string)dr["MNEMONICO_LISTA"] : null
                };
        }

        #endregion
    }
}
