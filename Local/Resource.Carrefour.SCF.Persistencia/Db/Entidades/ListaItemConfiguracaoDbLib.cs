﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class ListaItemConfiguracaoDbLib : IEntidadeDbLib<ListaConfiguracaoItemInfo>
    {
        #region IEntidadeDbLib<ListaConfiguracaoItemInfo> Members

        public ConsultarObjetosResponse<ListaConfiguracaoItemInfo> ConsultarObjetos(ConsultarObjetosRequest<ListaConfiguracaoItemInfo> request)
        {
            
            // Interpreta as condicoes
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // CodigoLista
                //1351 condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoLista");
                //1362 condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "pCODIGO_LISTA"); //1351

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoLista"); //1362
                if (condicaoInfo != null)
                    parametros.Add("pCODIGO_LISTA", condicaoInfo.Valores[0]);
                else
                {
                    condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "pCODIGO_LISTA");
                    if (condicaoInfo != null)
                        parametros.Add("pCODIGO_LISTA", condicaoInfo.Valores[0]);                    
                }

                
                // Grupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoEmpresaGrupo" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    parametros.Add("pCODIGO_EMPRESA_GRUPO", condicaoInfo.Valores[0]);
                
            }

            // Faz a consulta no banco
            DataTable tb =   (DataTable) OracleDbLib2.Default.ExecutarProcedure("PR_LISTA_ITEM_CONF_L", parametros)["RETCUR"];

            ConsultarObjetosResponse<ListaConfiguracaoItemInfo> resposta = new ConsultarObjetosResponse<ListaConfiguracaoItemInfo>();

            // Monta a lista
            foreach (DataRow dr in tb.Rows)
                resposta.Resultado.Add(MontarObjeto(dr));

            return resposta;
        }

        public ReceberObjetoResponse<ListaConfiguracaoItemInfo> ReceberObjeto(ReceberObjetoRequest<ListaConfiguracaoItemInfo> request)
        {
            // Retorna
            return
                new ReceberObjetoResponse<ListaConfiguracaoItemInfo>()
                {
                };
        }

        public SalvarObjetoResponse<ListaConfiguracaoItemInfo> SalvarObjeto(SalvarObjetoRequest<ListaConfiguracaoItemInfo> request)
        {
            SalvarObjetoResponse<ListaConfiguracaoItemInfo> resposta = new SalvarObjetoResponse<ListaConfiguracaoItemInfo>();
            resposta.PreparaResposta(request);

            // Insere os parametros
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("pCODIGO_LISTA_CONF", request.Objeto.CodigoListaItemConfiguracao);
            parametros.Add("pCONF_LISTA", request.Objeto.Configuracao);
            parametros.Add("pCODIGO_EMPRESA_GRUPO", request.Objeto.CodigoEmpresaGrupo);
            

            // Execução a procedure
            OracleDbLib2.Default.ExecutarProcedure("PR_LISTA_ITEM_CONF_S", parametros);

            return resposta;
        }

        public RemoverObjetoResponse<ListaConfiguracaoItemInfo> RemoverObjeto(RemoverObjetoRequest<ListaConfiguracaoItemInfo> request)
        {
            throw new NotImplementedException();
        }

        public ListaConfiguracaoItemInfo MontarObjeto(System.Data.DataRow dr)
        {
            return new ListaConfiguracaoItemInfo()
            {
                CodigoListaItemConfiguracao = dr["CODIGO_LISTA_CONF"] != DBNull.Value ? dr["CODIGO_LISTA_CONF"].ToString() : null,
                CodigoEmpresaGrupo = dr["CODIGO_EMPRESA_GRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_GRUPO"].ToString() : null,
                CodigoListaItem = dr["CODIGO_LISTA_ITEM"] != DBNull.Value ? dr["CODIGO_LISTA_ITEM"].ToString() : null,
                CodigoLista = dr["CODIGO_LISTA"] != DBNull.Value ? dr["CODIGO_LISTA"].ToString() : null,
                NomeListaItem = dr["NOME_LISTA_ITEM"] != DBNull.Value ? dr["NOME_LISTA_ITEM"].ToString() : null,
                Configuracao = dr["CONF_LISTA"] != DBNull.Value ? dr["CONF_LISTA"].ToString() : null,
                Valor = dr["VALOR"] != DBNull.Value ? dr["VALOR"].ToString() : null,
                Ativo = dr["ATIVO"] != DBNull.Value ? (string)dr["ATIVO"] == "S" ? true : false : false,
                ColunaConfiguravelHeader = dr["LISTA_CONF_HEADER"] != DBNull.Value ? dr["LISTA_CONF_HEADER"].ToString() : null
            };

        }

        #endregion
    }
}
