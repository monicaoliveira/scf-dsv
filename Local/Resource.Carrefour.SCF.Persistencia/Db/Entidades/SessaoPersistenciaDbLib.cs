﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using System.Runtime.Serialization.Formatters.Binary;
using Resource.Framework.Sistemas.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Permissoes;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistência para SessaoPersistenciaInfo
    /// </summary>
    public class SessaoPersistenciaDbLib : IEntidadeDbLib<SessaoPersistenciaInfo>
    {
        #region IEntidadeDbLib<SessaoPersistenciaInfo> Members

        public ConsultarObjetosResponse<SessaoPersistenciaInfo> ConsultarObjetos(ConsultarObjetosRequest<SessaoPersistenciaInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // TipoObjeto
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao");
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INCLUSAO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_SESSAO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<SessaoPersistenciaInfo> resultado = new List<SessaoPersistenciaInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<SessaoPersistenciaInfo>()
                {
                    Resultado = resultado
                };
        }

        public ReceberObjetoResponse<SessaoPersistenciaInfo> ReceberObjeto(ReceberObjetoRequest<SessaoPersistenciaInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_SESSAO_B", "pCODIGO_SESSAO", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<SessaoPersistenciaInfo>()
                {
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        public SalvarObjetoResponse<SessaoPersistenciaInfo> SalvarObjeto(SalvarObjetoRequest<SessaoPersistenciaInfo> request)
        {

            byte[] b;
            // Serializa o objeto
            using (MemoryStream ms = new MemoryStream())
            {

                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(ms, request.Objeto.Sessao);
                ms.Position = 0;
                b = ms.ToArray();
            }
            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_SESSAO", request.Objeto.CodigoSessao);
            paramsProc.Add("pCONTEUDO_SESSAO", b);
            paramsProc.Add("retornarRegistro", "S");

            // Executa a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_SESSAO_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<SessaoPersistenciaInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        public RemoverObjetoResponse<SessaoPersistenciaInfo> RemoverObjeto(RemoverObjetoRequest<SessaoPersistenciaInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_SESSAO_R",
                "pCODIGO_SESSAO", request.CodigoObjeto);

            // Retorna
            return
                new RemoverObjetoResponse<SessaoPersistenciaInfo>()
                {
                    CodigoMensagemRequest = request.CodigoMensagem
                };
        }

        public SessaoPersistenciaInfo MontarObjeto(DataRow dr)
        {
            object sessao = null;

            BinaryFormatter bin = new BinaryFormatter();
            using (MemoryStream bufMemoria = new System.IO.MemoryStream((byte[])dr["CONTEUDO_SESSAO"]))
            {
                sessao = bin.Deserialize(bufMemoria);
            }

            // Cria o SessaoPersistenciaInfo
            SessaoPersistenciaInfo SessaoPersistenciaInfo =
                new SessaoPersistenciaInfo()
                {
                    CodigoSessao = (string)dr["CODIGO_SESSAO"],
                    Sessao = (Sessao)sessao
                };

            // Retorna
            return SessaoPersistenciaInfo;
        }

        #endregion
    }
}
