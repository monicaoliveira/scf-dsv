﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Framework.Library.Db;
using Resource.Framework.Contratos.Comum;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class ListaItemReferenciaDb : IEntidadeDbLib<ListaItemReferenciaInfo>
    {
        #region IEntidadeDbLib<ListaItemReferenciaInfo> Members

        public ConsultarObjetosResponse<ListaItemReferenciaInfo> ConsultarObjetos(ConsultarObjetosRequest<ListaItemReferenciaInfo> request)
        {
                        // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // codigo lista item referencia
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoListaItemReferencia");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_LISTA_ITEM_REFERENCIA", condicaoInfo.Valores[0]);

                // codigo situacao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoListaItemSituacao");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_LISTA_ITEM_SITUACAO", condicaoInfo.Valores[0]);
                
                // codigo lista item
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoListaItem");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_LISTA_ITEM", condicaoInfo.Valores[0]);

                // nome da referencia
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeReferencia");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_REFERENCIA", condicaoInfo.Valores[0]);

            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LISTA_ITEM_REFERENCIA_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ListaItemReferenciaInfo> resultado = new List<ListaItemReferenciaInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ListaItemReferenciaInfo>()
                {
                    Resultado = resultado
                };

        }

        public ReceberObjetoResponse<ListaItemReferenciaInfo> ReceberObjeto(ReceberObjetoRequest<ListaItemReferenciaInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LISTA_ITEM_REFERENCIA_B",
                        "pCODIGO_LISTA_ITEM_REFERENCIA", request.CodigoObjeto)["RETCUR"];
                        

            // Retorna
            return
                new ReceberObjetoResponse<ListaItemReferenciaInfo>()
                {
                    CodigoSessao = request.CodigoSessao,
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) :
                        null
                };

        }

        public SalvarObjetoResponse<ListaItemReferenciaInfo> SalvarObjeto(SalvarObjetoRequest<ListaItemReferenciaInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoListaItemReferencia;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_LISTA_ITEM_REFERENCIA", pk);
            paramsProc.Add("pCODIGO_LISTA_ITEM", request.Objeto.CodigoListaItem);
            paramsProc.Add("pCODIGO_LISTA_ITEM_SITUACAO", request.Objeto.CodigoListaItemSituacao);
            paramsProc.Add("pNOME_REFERENCIA", request.Objeto.NomeReferencia);
            paramsProc.Add("pVALOR_REFERENCIA", request.Objeto.ValorReferencia);
            paramsProc.Add("retornarRegistro", "S");

            // Executa a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_LISTA_ITEM_REFERENCIA_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<ListaItemReferenciaInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        public RemoverObjetoResponse<ListaItemReferenciaInfo> RemoverObjeto(RemoverObjetoRequest<ListaItemReferenciaInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_LISTA_ITEM_REFERENCIA_R",
                "pCODIGO_LISTA_ITEM_REFERENCIA", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<ListaItemReferenciaInfo>();
        }

        public ListaItemReferenciaInfo MontarObjeto(DataRow dr)
        {
            ListaItemReferenciaInfo listaItemReferenciaInfo =
                new ListaItemReferenciaInfo();

            listaItemReferenciaInfo.CodigoListaItemReferencia = dr["CODIGO_LISTA_ITEM_REFERENCIA"] != DBNull.Value ? dr["CODIGO_LISTA_ITEM_REFERENCIA"].ToString() : null;
            listaItemReferenciaInfo.CodigoListaItem = dr["CODIGO_LISTA_ITEM"] != DBNull.Value ? dr["CODIGO_LISTA_ITEM"].ToString() : null;
            listaItemReferenciaInfo.CodigoListaItemSituacao = dr["CODIGO_LISTA_ITEM_SITUACAO"] != DBNull.Value ? dr["CODIGO_LISTA_ITEM_SITUACAO"].ToString() : "";
            listaItemReferenciaInfo.NomeReferencia = dr["NOME_REFERENCIA"] != DBNull.Value ? dr["NOME_REFERENCIA"].ToString() : null;
            listaItemReferenciaInfo.ValorReferencia = dr["VALOR_REFERENCIA"] != DBNull.Value ? dr["VALOR_REFERENCIA"].ToString() : null;
            listaItemReferenciaInfo.listaItemInfo = PersistenciaHelper.Receber<ListaItemInfo>(null, dr["CODIGO_LISTA_ITEM"].ToString());
            listaItemReferenciaInfo.DominioLista = PersistenciaHelper.Receber<ListaInfo>(null, listaItemReferenciaInfo.listaItemInfo.CodigoLista.ToString()).Descricao;
            listaItemReferenciaInfo.Dominio = listaItemReferenciaInfo.listaItemInfo.Descricao;
            listaItemReferenciaInfo.DominioListaValor = listaItemReferenciaInfo.listaItemInfo.Valor;
            listaItemReferenciaInfo.CodigoValorReferencia = dr["CODIGO_VALOR_REFERENCIA"] != DBNull.Value ? dr["CODIGO_VALOR_REFERENCIA"].ToString() : null;
            return listaItemReferenciaInfo;
        }

        #endregion
    }
}
