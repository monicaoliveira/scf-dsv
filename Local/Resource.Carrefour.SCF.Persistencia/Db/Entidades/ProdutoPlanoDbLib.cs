﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class ProdutoPlanoDbLib : IEntidadeDbLib<ProdutoPlanoInfo>
    {
        #region IEntidadeDbLib<ProdutoPlanoInfo> Members

        public ConsultarObjetosResponse<ProdutoPlanoInfo> ConsultarObjetos(ConsultarObjetosRequest<ProdutoPlanoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Codigo TSYS
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoPlano");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PLANO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PRODUTO_PLANO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ProdutoPlanoInfo> resultado = new List<ProdutoPlanoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ProdutoPlanoInfo>()
                {
                    Resultado = resultado
                };
        }

        public ReceberObjetoResponse<ProdutoPlanoInfo> ReceberObjeto(ReceberObjetoRequest<ProdutoPlanoInfo> request)
        {
            throw new NotImplementedException();
        }

        public SalvarObjetoResponse<ProdutoPlanoInfo> SalvarObjeto(SalvarObjetoRequest<ProdutoPlanoInfo> request)
        {
            throw new NotImplementedException();
        }

        public RemoverObjetoResponse<ProdutoPlanoInfo> RemoverObjeto(RemoverObjetoRequest<ProdutoPlanoInfo> request)
        {
            throw new NotImplementedException();
        }

        public ProdutoPlanoInfo MontarObjeto(DataRow dr)
        {
            ProdutoPlanoInfo produtoPlanoInfo =
                new ProdutoPlanoInfo()
                {
                    codigoProduto = dr["CODIGO_PRODUTO"] != DBNull.Value ? dr["CODIGO_PRODUTO"].ToString() : null,
                    codigoPlano = dr["CODIGO_PLANO"] != DBNull.Value ? dr["CODIGO_PLANO"].ToString() : null
                };

            // Retorna
            return produtoPlanoInfo;

        }

        #endregion

    }
}
