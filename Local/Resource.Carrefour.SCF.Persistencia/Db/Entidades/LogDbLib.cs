﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db;

using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;
using System.IO;
using System.Xml.Serialization;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Framework.Library.Db.Oracle;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia para LogInfo com entendimento de ParametrizacaoLogInfo
    /// </summary>
    public class LogDbLib : IEntidadeDbLib<LogInfo>
    {
        #region IEntidadeDbLib<LogInfo> Members

        public ConsultarObjetosResponse<LogInfo> ConsultarObjetos(ConsultarObjetosRequest<LogInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // TipoLog
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoLog" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_LOG", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroData"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LOG_MENOR", condicaoInfo.Valores[0] );

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroData"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LOG_MAIOR", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroData"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_LOG_MENOR", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_LOG_MAIOR", condicaoInfo.Valores[0]);
                }

                // CodigoUsuario
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoUsuario");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_USUARIO", condicaoInfo.Valores[0]);
            }

            // Coloca o número máximo de linhas
            paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LOG_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<LogInfo> resultado = new List<LogInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<LogInfo>()
                {
                    Resultado = resultado
                };
        }

        public ReceberObjetoResponse<LogInfo> ReceberObjeto(ReceberObjetoRequest<LogInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LOG_B", "pCODIGO_LOG", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<LogInfo>()
                {
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        public SalvarObjetoResponse<LogInfo> SalvarObjeto(SalvarObjetoRequest<LogInfo> request)
        {
            // Infere a pk
            string pk = request.Objeto.CodigoLog;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Serializa o objeto
            MemoryStream ms = new MemoryStream();
            new XmlSerializer(
                request.Objeto.GetType(), this.listarTiposExtras()).Serialize(ms, request.Objeto);
            ms.Position = 0;
            StreamReader reader = new StreamReader(ms);
            string serializacaoObjeto = reader.ReadToEnd();
            reader.Close();

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_LOG", pk);
            paramsProc.Add("pCODIGO_USUARIO", request.Objeto.CodigoUsuario);
            paramsProc.Add("pDESCRICAO_LOG", request.Objeto.Descricao.Length <= 4000 ? request.Objeto.Descricao : request.Objeto.Descricao.Substring(0, 4000));
            paramsProc.Add("pDATA_LOG", DateTime.Now); //datalog inalterada
            //datalog inalterada paramsProc.Add("pDATA_LOG", request.Objeto.DataLog == null ? DateTime.Now : request.Objeto.DataLog); //SCF1175 paramsProc.Add("pDATA_LOG", request.Objeto.DataLog);
            paramsProc.Add("pTIPO_LOG", request.Objeto.TipoLog == null ? request.Objeto.GetType().Name : request.Objeto.TipoLog); //1433 log paramsProc.Add("pTIPO_LOG",  request.Objeto.GetType().Name );
            paramsProc.Add("pSERIALIZACAO_LOG", serializacaoObjeto);
            paramsProc.Add("retornarRegistro", "S"); 

            // Verifica se o objeto contem o codigo origem
            if (request.Objeto is LogSCFInfo)
            {
                // Pega o objeto com o tipo correto
                LogSCFInfo objeto2 = (LogSCFInfo)request.Objeto;

                // Adiciona o parametro
                paramsProc.Add("pCODIGO_ORIGEM", objeto2.CodigoOrigem);
                paramsProc.Add("pTIPO_ORIGEM", objeto2.TipoOrigem);
            }

            // Executa a procedure
            DataTable tb =(DataTable)OracleDbLib2.Default.ExecutarProcedure("PR_LOG_S", paramsProc)["RETCUR"];

            // Log em arquivo
            LogArquivo.Objeto("log", request.Objeto);
            
            // Retorna
            return
                new SalvarObjetoResponse<LogInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        public RemoverObjetoResponse<LogInfo> RemoverObjeto(RemoverObjetoRequest<LogInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_LOG_R",
                "pCODIGO_LOG", request.CodigoObjeto);

            // Retorna
            return
                new RemoverObjetoResponse<LogInfo>()
                {
                    CodigoMensagemRequest = request.CodigoMensagem
                };
        }

        public LogInfo MontarObjeto(DataRow dr)
        {
            //==================================================================================================
            // SCF1329 - INICIO
            //==================================================================================================
            //==================================================================================================
            // Resolve o tipo
            //==================================================================================================
            //Type tipoLog = ResolutorTipos.Resolver((string)dr["TIPO_LOG"]);
            // Desserializa o log
                //LogInfo logInfo =
                //    (LogInfo)
                //        new XmlSerializer(tipoLog, this.listarTiposExtras()).Deserialize(
                //            new MemoryStream(
                //                ASCIIEncoding.UTF8.GetBytes((string)dr["SERIALIZACAO_LOG"])));
            //==================================================================================================
            // Preenche com as informações do banco
            //==================================================================================================
            LogInfo logInfo = new LogInfo();
            logInfo.CodigoLog = dr["CODIGO_LOG"].ToString();
            logInfo.CodigoUsuario = dr["CODIGO_USUARIO"] != DBNull.Value ? dr["CODIGO_USUARIO"].ToString() : null;
            logInfo.Descricao = dr["DESCRICAO_LOG"] != DBNull.Value ? dr["DESCRICAO_LOG"].ToString() : null;
            logInfo.DataLog = dr["DATA_LOG"] != DBNull.Value ? (DateTime)dr["DATA_LOG"] : DateTime.MinValue;
            //==================================================================================================
            // Retorna
            //==================================================================================================
            return logInfo;
        }

        /// <summary>
        /// Retorna lista de tipos extras para serializacao
        /// </summary>
        /// <returns></returns>
        private Type[] listarTiposExtras()
        {
            return
                new Type[] 
                { 
                }.ToArray();
        }

        #endregion
    }
}
