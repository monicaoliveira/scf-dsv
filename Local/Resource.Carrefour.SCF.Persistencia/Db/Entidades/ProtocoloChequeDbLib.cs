﻿//963
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library.Db;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class ProtocoloChequeDbLib : IEntidadeDbLib<ProcessoProtocoloChequeInfo> //1465
    {
        #region IEntidadeDbLib<ProcessoProtocoloChequeInfo> Members

        //===========================================================================================================================================
        public ConsultarObjetosResponse<ProcessoProtocoloChequeInfo> ConsultarObjetos(ConsultarObjetosRequest<ProcessoProtocoloChequeInfo> request)
        {   Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            if (request.Condicoes != null)
            {   condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataEstorno" && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DT_ESTORNO_ATE", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataEstorno" && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DT_ESTORNO_DE", condicaoInfo.Valores[0]);
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataEstorno"  && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DT_ESTORNO_ATE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DT_ESTORNO_DE", condicaoInfo.Valores[0]);
                }

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroNumeroProtocolo" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pNUMERO_PROTOCOLO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEnvioCci" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_ENVIO_CCI", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEstabelecimento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCOD_ESTABELECIMENTO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataRegProtocolo" && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DT_PROTOCOLO_ATE", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataRegProtocolo" && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DT_PROTOCOLO_DE", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataRegProtocolo" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DT_PROTOCOLO_ATE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DT_PROTOCOLO_DE", condicaoInfo.Valores[0]);
                }

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroStatusProtocolo" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_PROTOCOLO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroContaCliente" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CTA_CLIENTE", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroTipoEstorno" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TP_ESTORNO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataProcessamento" && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DT_PROC_ATE", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataProcessamento"  && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DT_PROC_DE", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataProcessamento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DT_PROC_ATE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DT_PROC_DE", condicaoInfo.Valores[0]);
                }
            }

            DataTable tb =(DataTable)OracleDbLib2.Default.ExecutarProcedure("PR_CHEQUE_DEVOLVIDO_L", paramsProc)["RETCUR"];
            List<ProcessoProtocoloChequeInfo> resultado = new List<ProcessoProtocoloChequeInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));
            return
                new ConsultarObjetosResponse<ProcessoProtocoloChequeInfo>()
                {   Resultado = resultado
                };
        }

        //===========================================================================================================================================
        public ReceberObjetoResponse<ProcessoProtocoloChequeInfo> ReceberObjeto(ReceberObjetoRequest<ProcessoProtocoloChequeInfo> request)
        {   DataTable tb =(DataTable)OracleDbLib2.Default.ExecutarProcedure("PR_CHEQUE_DEVOLVIDO_B","pCODIGO_TRANSACAO", request.CodigoObjeto)["RETCUR"];
            return
                new ReceberObjetoResponse<ProcessoProtocoloChequeInfo>()
                {   Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }
        //===========================================================================================================================================
        public SalvarObjetoResponse<ProcessoProtocoloChequeInfo> SalvarObjeto(SalvarObjetoRequest<ProcessoProtocoloChequeInfo> request)
        {   SalvarObjetoResponse<ProcessoProtocoloChequeInfo> resposta = new SalvarObjetoResponse<ProcessoProtocoloChequeInfo>();
            resposta.PreparaResposta(request);
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_TRANSACAO", request.Objeto.CodigoTransacao);
            paramsProc.Add("pDATA_PROTOCOLO", request.Objeto.DataRegProtocolo);
            paramsProc.Add("pDATA_ESTORNO", request.Objeto.DataEstorno);
            paramsProc.Add("pNUMERO_PROTOCOLO", request.Objeto.NumeroProtocolo);
            paramsProc.Add("pTIPO_ESTORNO", request.Objeto.TipoEstorno);
            OracleDbLib2.Default.ExecutarProcedure("PR_CHEQUE_DEVOLVIDO_S", paramsProc);
            return resposta;
        }
        //===========================================================================================================================================
        public RemoverObjetoResponse<ProcessoProtocoloChequeInfo> RemoverObjeto(RemoverObjetoRequest<ProcessoProtocoloChequeInfo> request)
        {
            return null;
        }

        //===========================================================================================================================================
        public ProcessoProtocoloChequeInfo MontarObjeto(System.Data.DataRow dr)
        {
            ProcessoProtocoloChequeInfo ProcessoProtocoloChequeInfo = new ProcessoProtocoloChequeInfo();

            ProcessoProtocoloChequeInfo.DataEstorno = dr["DATA_ESTORNO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_ESTORNO"]) : null;
            ProcessoProtocoloChequeInfo.NumeroProtocolo = dr["NUMERO_PROTOCOLO"] != DBNull.Value ? dr["NUMERO_PROTOCOLO"].ToString() : null;
            ProcessoProtocoloChequeInfo.NsuHost = dr["NSU_HOST"] != DBNull.Value ? dr["NSU_HOST"].ToString() : null;
            ProcessoProtocoloChequeInfo.NsuHostOriginal = dr["NSU_HOST_ORIGINAL"] != DBNull.Value ? dr["NSU_HOST_ORIGINAL"].ToString() : null;
            ProcessoProtocoloChequeInfo.EnvioCci = dr["STATUS_ENVIO_CCI"] != DBNull.Value ? (string)dr["STATUS_ENVIO_CCI"] : null;
            ProcessoProtocoloChequeInfo.Estabelecimento = dr["NOME_ESTABELECIMENTO"] != DBNull.Value ? (string)dr["NOME_ESTABELECIMENTO"] : null;
            ProcessoProtocoloChequeInfo.Valor = dr["VALOR_REPASSE"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_REPASSE"]) : 0; //1465 no JS substituimos o PONTO para VIRGULA
            ProcessoProtocoloChequeInfo.TipoTransacao = dr["TIPO_TRANSACAO"] != DBNull.Value ? (string)dr["TIPO_TRANSACAO"] : null;
            ProcessoProtocoloChequeInfo.DataRegProtocolo = dr["DATA_PROTOCOLO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_PROTOCOLO"]) : null;
            ProcessoProtocoloChequeInfo.StatusProtocolo = dr["STATUS_PROTOCOLO"] != DBNull.Value ? (string)dr["STATUS_PROTOCOLO"] : null;
            ProcessoProtocoloChequeInfo.ContaCliente = dr["NUMERO_CONTA_CLIENTE"] != DBNull.Value ? (string)dr["NUMERO_CONTA_CLIENTE"] : null;
            ProcessoProtocoloChequeInfo.TipoEstorno = dr["TIPO_ESTORNO"] != DBNull.Value ? (string)dr["TIPO_ESTORNO"] : null;
            ProcessoProtocoloChequeInfo.DataProcessamento = dr["DATA_MOVIMENTO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_MOVIMENTO"]) : null;
            ProcessoProtocoloChequeInfo.CodigoTransacao = dr["CODIGO_TRANSACAO"] != DBNull.Value ? dr["CODIGO_TRANSACAO"].ToString() : null;

            return ProcessoProtocoloChequeInfo;
        }
        #endregion
    }
}
