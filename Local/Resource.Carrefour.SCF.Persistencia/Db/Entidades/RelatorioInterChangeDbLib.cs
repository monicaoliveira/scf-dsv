﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia de Arquivo
    /// </summary>
    /// Jira: SCF - 1520
    public class RelatorioInterChangeDbLib : IEntidadeDbLib<RelatorioInterChangeInfo>
    {
        #region IEntidadeDbLib<RelatorioInterChangeInfo> Members

        /// <summary>
        /// Recebe Objeto
        /// </summary>
        /// <param name = "request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<RelatorioInterChangeInfo> ReceberObjeto(ReceberObjetoRequest<RelatorioInterChangeInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Consulta Objeto
        /// </summary>
        /// <param name = "request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<RelatorioInterChangeInfo> ConsultarObjetos(ConsultarObjetosRequest<RelatorioInterChangeInfo> request)
        {
            //Monta a lista de parametros
            Dictionary<string, Object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null && request.Condicoes.Count() > 0)
            {
                //Data de Inicio do Processamento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMovimentoDe");
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_MOVIMENTO_INICIO", condicaoInfo.Valores[0]);
                }

                //Data de Final do Processamento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMovimentoAte");
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_MOVIMENTO_FIM", condicaoInfo.Valores[0]);
                }
            }

            //Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                    "PR_TAXA_REL_INTERCHANGE_L", paramsProc)["RETCUR"]; // 18/06/2018 "PR_TAXA_RELATORIO_INTERCHANGE_L", paramsProc)["RETCUR"];

            List<RelatorioInterChangeInfo> resultado = new List<RelatorioInterChangeInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            //Retorna
            return
                new ConsultarObjetosResponse<RelatorioInterChangeInfo>()
                {
                    Resultado = resultado
                };
        }

        public SalvarObjetoResponse<RelatorioInterChangeInfo> SalvarObjeto(SalvarObjetoRequest<RelatorioInterChangeInfo> request)
        {
            return null;
        }

        public RemoverObjetoResponse<RelatorioInterChangeInfo> RemoverObjeto(RemoverObjetoRequest<RelatorioInterChangeInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Monta arquivo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public RelatorioInterChangeInfo MontarObjeto(DataRow dr)
        {
            EstabelecimentoInfo estabelecimento = new EstabelecimentoInfo()
            {
                CNPJ = dr["CNPJ"] != DBNull.Value ? dr["CNPJ"].ToString() : null
            };

            RelatorioInterChangeInfo relatorioInterchangeInfo = new RelatorioInterChangeInfo()
            {
                NomeGrupo = dr["NOME_EMPRESA_GRUPO"] != DBNull.Value ? dr["NOME_EMPRESA_GRUPO"].ToString() : null,
                Referencia = dr["NOME_LISTA_ITEM"] != DBNull.Value ? dr["NOME_LISTA_ITEM"].ToString() : null,
                DataProcessamento = dr["DATA_MOVIMENTO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_MOVIMENTO"]) : null,
                TipoTransacao = dr["TIPO_TRANSACAO"] != DBNull.Value ? dr["TIPO_TRANSACAO"].ToString() : null,
                CodigoProduto = dr["CODIGO_PRODUTO"] != DBNull.Value ? dr["CODIGO_PRODUTO"].ToString() : null,
                CodigoPlano = dr["CODIGO_PLANO_TSYS"] != DBNull.Value ? dr["CODIGO_PLANO_TSYS"].ToString() : null,
                NumeroAutorizacaoTransacao = dr["CODIGO_AUTORIZACAO"] != DBNull.Value ? dr["CODIGO_AUTORIZACAO"].ToString() : null,
                QuantidadeParcelas = dr["NUMERO_PARCELA_TOTAL"] != DBNull.Value ? dr["NUMERO_PARCELA_TOTAL"].ToString() : null,
                DataTransacao = dr["DATA_TRANSACAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_TRANSACAO"]) : null,
                Numero_NSU_HOST = dr["NSU_HOST"] != DBNull.Value ? dr["NSU_HOST"].ToString() : null,
                NumeroContaClienteTSYS = dr["NUMERO_CONTA_CLIENTE"] != DBNull.Value ? dr["NUMERO_CONTA_CLIENTE"].ToString() : null,
                ValorBrutoVenda = dr["VALOR_BRUTO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_BRUTO"]) : 0,
                ValorComissaoAplicado = dr["VALOR_DESCONTO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_DESCONTO"]) : 0,
                ValorLiquido = dr["VALOR_LIQUIDO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_LIQUIDO"]) : 0,
                TaxaTSYS = dr["TAXA_TSYS"] != DBNull.Value ? Convert.ToDecimal(dr["TAXA_TSYS"]) : 0,
                ValorComissaoSCF = dr["VALOR_COMISSAO_SCF"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_COMISSAO_SCF"]) : 0,
                TaxaInterchange = dr["PERCENTUAL_TAXA_INTERCHANGE"] != DBNull.Value ? Convert.ToDecimal(dr["PERCENTUAL_TAXA_INTERCHANGE"]) : (decimal?) null,
                VigenciaInical = dr["DATA_INICIO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_INICIO"]) : null,
                VigenciaFinal = dr["DATA_FIM"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_FIM"]) : null,
                Diferenca = dr["DIFERENCA"] != DBNull.Value ? Convert.ToDecimal(dr["DIFERENCA"]) : 0,
                Estabelecimento = estabelecimento
            };

            //Retorna 
            return relatorioInterchangeInfo;
        }

        #endregion
    }
}
