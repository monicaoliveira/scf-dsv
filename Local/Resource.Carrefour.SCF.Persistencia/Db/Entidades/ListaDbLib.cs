﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para Lista
    /// </summary>
    public class ListaDbLib : IEntidadeDbLib<ListaInfo>
    {
        #region IEntidadeDbLib<ListaInfo> Members

        /// <summary>
        /// Listar Listas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<ListaInfo> ConsultarObjetos(ConsultarObjetosRequest<ListaInfo> request)
        {
            // Interpreta as condicoes
            Dictionary<string, object> parametros = new Dictionary<string, object>();

            
            // Monta lista de parametros
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Nome da Lista
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeLista" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    parametros.Add("pNOME_LISTA", condicaoInfo.Valores[0]);
            }


            // Pede a lista
            DataTable tb =
                (DataTable) OracleDbLib2.Default.ExecutarProcedure(
                    "PR_LISTA_L", parametros)["RETCUR"];

            // Prepara a resposta
            ConsultarObjetosResponse<ListaInfo> resposta = new ConsultarObjetosResponse<ListaInfo>();

            // Monta a lista
            foreach (DataRow dr in tb.Rows)
                resposta.Resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Retornar detalhe de Lista
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<ListaInfo> ReceberObjeto(ReceberObjetoRequest<ListaInfo> request)
        {
            // Prepara parametros
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("retornarItens", "S");
            
            // É mensagem específica?
            if (request is ReceberListaDbRequest)
            {
                // Pega mensagem com o tipo correto
                ReceberListaDbRequest request2 = (ReceberListaDbRequest)request;

                // Informou código da lista?
                if (request2.CodigoLista != null)
                    parametros.Add("pCODIGO_LISTA", request2.CodigoLista);

                // Informou mnemonico?
                if (request2.MnemonicoLista != null)
                    parametros.Add("pMNEMONICO_LISTA", request2.MnemonicoLista);
            }
            else
            {
                // Adiciona o código
                parametros.Add("pCODIGO_LISTA", request.CodigoObjeto);
            }
            
            // Pede o detalhe
            Dictionary<string, object> retorno =
                OracleDbLib2.Default.ExecutarProcedure(
                    "PR_LISTA_B",
                    parametros);

            // Pega as tabelas necessarias
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbItens = (DataTable)retorno["RETCURITENS"];
            
            // Retorna
            return
                new ReceberObjetoResponse<ListaInfo>()
                {
                    CodigoSessao = request.CodigoSessao,
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0], tbItens) :
                        null
                };
        }

        /// <summary>
        /// Salvar Lista
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<ListaInfo> SalvarObjeto(SalvarObjetoRequest<ListaInfo> request)
        {
            try
            {

                // Infere a pk
                string pk = request.Objeto.CodigoLista;
                if (pk != null && pk.Contains("-"))
                    pk = null;

                // Salva a Lista
                Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LISTA_S",
                        "pCODIGO_LISTA", pk,
                        "pNOME_LISTA", request.Objeto.Descricao,
                        "pMNEMONICO_LISTA", request.Objeto.Mnemonico,
                        "retornarRegistro", "S",
                        "retornarItens", "S");

                // Pega as tabelas necessarias
                DataTable tb = (DataTable)retorno["RETCUR"];
                DataTable tbItens = (DataTable)retorno["RETCURITENS"];

                // Remove os itens necessarios
                List<DataRow> itensRemover = new List<DataRow>();
                foreach (DataRow drItem in tbItens.Rows)
                {
                    // Verifica se o item ainda consta na lista
                    if (request.Objeto.Itens.Find(i => i.CodigoListaItem == drItem["CODIGO_LISTA_ITEM"].ToString()) == null)
                    {
                        // Pede a remocao
                        OracleDbLib2.Default.ExecutarProcedure(
                            "PR_LISTA_ITEM_R",
                            "pCODIGO_LISTA_ITEM", drItem["CODIGO_LISTA_ITEM"]);

                        // Marca este item para exclusao
                        itensRemover.Add(drItem);
                    }
                }
                foreach (DataRow drItem in itensRemover)
                    tbItens.Rows.Remove(drItem);

                // Salva os itens informados
                foreach (ListaItemInfo listaItemInfo in request.Objeto.Itens)
                {
                    // Infere a pk
                    string pkItem = listaItemInfo.CodigoListaItem;
                    if (pkItem != null && pkItem.Contains("-"))
                        pkItem = null;

                    // Salva o item
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LISTA_ITEM_S",
                        "pCODIGO_LISTA_ITEM", pkItem,
                        "pCODIGO_LISTA", tb.Rows[0]["CODIGO_LISTA"],
                        "pNOME_LISTA_ITEM", listaItemInfo.Descricao,
                        "pMNEMONICO_LISTA_ITEM", listaItemInfo.Mnemonico,
                        "pATIVO", listaItemInfo.Ativo ? "S" : "N",
                        "pVALOR", listaItemInfo.Valor);
                }

                // Pede novamente o detalhe da lista
                ListaInfo listaInfo =
                    this.ReceberObjeto(
                        new ReceberObjetoRequest<ListaInfo>()
                        {
                            CodigoSessao = request.CodigoSessao,
                            CodigoObjeto = tb.Rows[0]["CODIGO_LISTA"].ToString()
                        }).Objeto;

                // Retorna
                return
                    new SalvarObjetoResponse<ListaInfo>()
                    {
                        CodigoSessao = request.CodigoSessao,
                        Objeto = listaInfo
                    };
            }
            catch (DbLibException ex)
            {
                if (ex.InnerException != null && ex.InnerException.ToString().Contains("-20009"))
                {
                    // Cria a critica
                    CriticaInfo critica = new CriticaInfo();
                    critica.Descricao = "Valor já existe no cadastro.";


                    SalvarObjetoResponse<ListaInfo> resposta = new SalvarObjetoResponse<ListaInfo>();
                    resposta.Criticas.Add(critica);
                    return resposta;
                }
                else if (ex.InnerException != null && ex.InnerException.ToString().Contains("-02292"))
                {
                    // Cria a critica
                    CriticaInfo critica = new CriticaInfo();
                    critica.Descricao = "Existe Referência de Domínio configurada para esse Domínio. Para removê-lo, excluir primeiro a Referência de Domínio vinculada a ele.";


                    SalvarObjetoResponse<ListaInfo> resposta = new SalvarObjetoResponse<ListaInfo>();
                    resposta.Criticas.Add(critica);
                    return resposta;
                }
                else
                {

                    throw ex;
                }
            }
        }

        /// <summary>
        /// Remover Lista
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<ListaInfo> RemoverObjeto(RemoverObjetoRequest<ListaInfo> request)
        {
            // Pede remocao
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_LISTA_R",
                "pCODIGO_LISTA", request.CodigoObjeto);

            // Retorna
            return
                new RemoverObjetoResponse<ListaInfo>()
                {
                    CodigoSessao = request.CodigoSessao
                };
        }

        /// <summary>
        /// Monta Lista
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ListaInfo MontarObjeto(DataRow dr)
        {
            return this.MontarObjeto(dr, null);
        }

        /// <summary>
        /// Monta Lista
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ListaInfo MontarObjeto(DataRow dr, DataTable tbItens)
        {
            // Cria o objeto
            ListaInfo listaInfo =
                new ListaInfo()
                {
                    CodigoLista = dr["CODIGO_LISTA"] != DBNull.Value ? dr["CODIGO_LISTA"].ToString() : null,
                    Descricao = dr["NOME_LISTA"] != DBNull.Value ? (string)dr["NOME_LISTA"] : null,
                    Mnemonico = dr["MNEMONICO_LISTA"] != DBNull.Value ? (string)dr["MNEMONICO_LISTA"] : null,
                    ColunaConfiguravelHeader = dr.Table.Columns.Contains("LISTA_CONF_HEADER") ? dr["LISTA_CONF_HEADER"] != DBNull.Value ? (string)dr["LISTA_CONF_HEADER"] : null : null
                };

            // Monta itens
            if (tbItens != null)
                foreach (DataRow drItem in tbItens.Rows)
                    listaInfo.Itens.Add(
                        new ListaItemInfo() 
                        {
                            CodigoListaItem = drItem["CODIGO_LISTA_ITEM"].ToString(),
                            CodigoLista = drItem["CODIGO_LISTA"].ToString(),
                            Descricao = drItem["NOME_LISTA_ITEM"] != DBNull.Value ? (string)drItem["NOME_LISTA_ITEM"] : null,
                            Mnemonico = drItem["MNEMONICO_LISTA_ITEM"] != DBNull.Value ? (string)drItem["MNEMONICO_LISTA_ITEM"] : null,
                            Ativo = drItem["ATIVO"] != DBNull.Value ? (string)drItem["ATIVO"] == "S" : true,
                            Valor = drItem["VALOR"] != DBNull.Value ? (string)drItem["VALOR"] : null,
                        });

            // Retorna o objeto
            return listaInfo;
        }

        #endregion
    }
}
