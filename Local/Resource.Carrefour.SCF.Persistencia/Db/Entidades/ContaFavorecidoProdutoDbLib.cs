using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

//=============================================================================================01
namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
	//=============================================================================================02
    public class ContaFavorecidoProdutoDbLib : IEntidadeDbLib<ContaFavorecidoProdutoInfo>
    {
		//=============================================================================================03
        #region IEntidadeDbLib<ContaFavorecidoProdutoInfo> Members
		//=============================================================================================04
        public ConsultarObjetosResponse<ContaFavorecidoProdutoInfo> ConsultarObjetos(ConsultarObjetosRequest<ContaFavorecidoProdutoInfo> request)
		//=============================================================================================
        {
			//=============================================================================================
            // Monta lista de parametros
			//=============================================================================================
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;
			//=============================================================================================
            // Apenas se tem coleção de condições criada
			//=============================================================================================
            if (request.Condicoes != null)
            {
				//=============================================================================================
                // PRODUTO
				//=============================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoProduto");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PRODUTO", condicaoInfo.Valores[0]);
				//=============================================================================================
                // CONTA FAVORECIDO
				//=============================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoContaFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_CONTA_FAVORECIDO", condicaoInfo.Valores[0]);
				//=============================================================================================
                // FAVORECIDO
				//=============================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_FAVORECIDO", condicaoInfo.Valores[0]);
				//=============================================================================================
                // REFERENCIA
				//=============================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroReferencia"); //1339
                if (condicaoInfo != null)
                    paramsProc.Add("pREFERENCIA", condicaoInfo.Valores[0]);				
            }
			//=============================================================================================
            // Monta a execução da procedure e executa
			//=============================================================================================
            DataTable tb = (DataTable) OracleDbLib2.Default.ExecutarProcedure("PR_CONTA_PRODUTO_L", paramsProc)["RETCUR"];
			//=============================================================================================
            // Preenche a coleção resultado
			//=============================================================================================
            List<ContaFavorecidoProdutoInfo> resultado = new List<ContaFavorecidoProdutoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));
			//=============================================================================================
            // Retorna
			//=============================================================================================
            return
                new ConsultarObjetosResponse<ContaFavorecidoProdutoInfo>()
                {
                    Resultado = resultado
                };
        }
		//=============================================================================================04
        public ReceberObjetoResponse<ContaFavorecidoProdutoInfo> ReceberObjeto(ReceberObjetoRequest<ContaFavorecidoProdutoInfo> request)
		//=============================================================================================05
        {
            return null;
        }
		//=============================================================================================05
        public SalvarObjetoResponse<ContaFavorecidoProdutoInfo> SalvarObjeto(SalvarObjetoRequest<ContaFavorecidoProdutoInfo> request)
		//=============================================================================================06
        {

			//=============================================================================================
            // Monta parametros
			//=============================================================================================
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_CONTA_FAVORECIDO", request.Objeto.CodigoContaFavorecido);
            paramsProc.Add("pCODIGO_PRODUTO", request.Objeto.CodigoProduto);
            paramsProc.Add("pREFERENCIA", request.Objeto.Referencia); //1339
            paramsProc.Add("retornarRegistro", "N");
			//=============================================================================================
            // Execução a procedure
			//=============================================================================================
            DataTable tb =(DataTable)OracleDbLib2.Default.ExecutarProcedure("PR_CONTA_PRODUTO_S", paramsProc)["RETCUR"];
			//=============================================================================================
            // Retorna
			//=============================================================================================
            return
                new SalvarObjetoResponse<ContaFavorecidoProdutoInfo>() { };
        }
		//=============================================================================================06
        public RemoverObjetoResponse<ContaFavorecidoProdutoInfo> RemoverObjeto(RemoverObjetoRequest<ContaFavorecidoProdutoInfo> request)
		//=============================================================================================07
        {
            RemoverContaFavorecidoDbRequest novoRequest = (RemoverContaFavorecidoDbRequest)request;
			//=============================================================================================
            // Monta a execução da procedure
			//=============================================================================================
            OracleDbLib2.Default.ExecutarProcedure
			(
                "PR_CONTA_PRODUTO_R"
			,	"pCODIGO_CONTA_FAVORECIDO"
			,	novoRequest.CodigoContaFavorecido
			,	"pCODIGO_PRODUTO"
			,	novoRequest.CodigoProduto
            ,   "pREFERENCIA"                   //1339
            ,   novoRequest.Referencia          //1339
			);

			//=============================================================================================
            // Retorna
			//=============================================================================================
            return new RemoverObjetoResponse<ContaFavorecidoProdutoInfo>();
        }
        //=============================================================================================06
        //SCF1701 - ID 27
        public SubstituirObjetoResponse<ContaFavorecidoProdutoInfo> SubstituirObjeto(SubstituirObjetoRequest<ContaFavorecidoProdutoInfo> request)
        //=============================================================================================07
        {
            Int32 quantidadeAlterada = 0;
            //=============================================================================================
            // Monta a execução da procedure
            //=============================================================================================
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_SUBSTITUI_CONTA", cn))
                {
                    //===============================================================================================
                    // Envia parametros
                    //===============================================================================================
                    cm.Parameters[":pCODIGO_CONTA_FAVORECIDO"].Value = request.CodigoContaFavorecido;
                    cm.Parameters[":pCODIGO_PRODUTO"].Value = request.CodigoProduto;
                    cm.Parameters[":pREFERENCIA"].Value = request.Referencia;
                    //===============================================================================================
                    // Executa a procedure
                    //===============================================================================================
                    cm.ExecuteNonQuery();
                    //===============================================================================================
                    // Recebe retornos
                    //===============================================================================================
                    OracleDecimal PRET_QTDE_ALTERADA = (OracleDecimal)cm.Parameters[":pQTDE_ALTERADA"].Value;
                    quantidadeAlterada = PRET_QTDE_ALTERADA.ToInt32();

                    //===============================================================================================
                    // Fechando conexao da procedure
                    //===============================================================================================
                    cm.Connection.Dispose();
                    cm.Dispose();
                }
            }
            //=============================================================================================
            // Retorna
            //=============================================================================================
            return new SubstituirObjetoResponse<ContaFavorecidoProdutoInfo>()
                {QuantidadeAlterada = quantidadeAlterada};
        }
        //=============================================================================================07
		public ContaFavorecidoProdutoInfo MontarObjeto(DataRow dr)
		//=============================================================================================08
        {
			//=============================================================================================
            // Retorna
			//=============================================================================================
            return
                new ContaFavorecidoProdutoInfo()
                {                    
					ID = dr["ID"].ToString()
				,	CodigoProduto = dr["CODIGO_PRODUTO"].ToString()
                ,   NomeProduto = dr["NOME_PRODUTO"] != DBNull.Value ? dr["NOME_PRODUTO"].ToString() : null
                ,   Banco = dr["BANCO"] != DBNull.Value ? dr["BANCO"].ToString() : null
                ,   Agencia = dr["AGENCIA"] != DBNull.Value ? dr["AGENCIA"].ToString() : null
                ,   Conta = dr["CONTA"] != DBNull.Value ? dr["CONTA"].ToString() : null
                ,   CodigoContaFavorecido = dr["CODIGO_CONTA_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_CONTA_FAVORECIDO"].ToString() : null
                ,   Agencia_CCI = dr["AGENCIA_CCI"] != DBNull.Value ? dr["AGENCIA_CCI"].ToString() : null
                ,   Conta_CCI = dr["CONTA_CCI"] != DBNull.Value ? dr["CONTA_CCI"].ToString() : null
				,   Referencia = dr["REFERENCIA"] != DBNull.Value ? dr["REFERENCIA"].ToString() : null //1339
                };
        }
		//=============================================================================================08
        #endregion
		//=============================================================================================03
    }
	//=============================================================================================02
}
//=============================================================================================01
