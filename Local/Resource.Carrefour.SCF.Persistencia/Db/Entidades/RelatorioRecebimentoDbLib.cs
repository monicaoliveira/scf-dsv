﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia de Arquivo
    /// </summary>
    public class RelatorioRecebimentoDbLib : IEntidadeDbLib<RelatorioRecebimentoInfo>
    {
        #region IEntidadeDbLib<ArquivoInfo> Members

        /// <summary>
        /// Recebe arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<RelatorioRecebimentoInfo> ReceberObjeto(ReceberObjetoRequest<RelatorioRecebimentoInfo> request)
        {
            return null;
        }

        public SalvarObjetoResponse<RelatorioRecebimentoInfo> SalvarObjeto(SalvarObjetoRequest<RelatorioRecebimentoInfo> request)
        {
            return null;
        }

        public ConsultarObjetosResponse<RelatorioRecebimentoInfo> ConsultarObjetos(ConsultarObjetosRequest<RelatorioRecebimentoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null && request.Condicoes.Count() > 0)
            {
                // Tipo do Processo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEmpresaGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_GRUPO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroReferencia");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_REFERENCIA", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroMeioPagamento");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_MEIO_PAGAMENTO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataRepasseInicio");
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataRepasseFim");
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_RELATORIO_RECEBIMENTO_L", paramsProc)["RETCUR"];
            
            List<RelatorioRecebimentoInfo> resultado = new List<RelatorioRecebimentoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<RelatorioRecebimentoInfo>()
                {
                    Resultado = resultado
                };

        }

        public RemoverObjetoResponse<RelatorioRecebimentoInfo> RemoverObjeto(RemoverObjetoRequest<RelatorioRecebimentoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Monta arquivo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public RelatorioRecebimentoInfo MontarObjeto(System.Data.DataRow dr)
        {
            EstabelecimentoInfo estabelecimento = new EstabelecimentoInfo()
            {
                CNPJ = dr["CNPJ"] != DBNull.Value ? dr["CNPJ"].ToString() : null,
                RazaoSocial = dr["RAZAO_SOCIAL"] != DBNull.Value ? dr["RAZAO_SOCIAL"].ToString() : null
            };

            RelatorioRecebimentoInfo relatorioRecebimentoInfo = new RelatorioRecebimentoInfo()
            {
                NomeGrupo = dr["NOME_EMPRESA_GRUPO"] != DBNull.Value ? dr["NOME_EMPRESA_GRUPO"].ToString() : null,
                DescricaoReferencia = dr["DESCRICAO_REFERENCIA"] != DBNull.Value ? dr["DESCRICAO_REFERENCIA"].ToString() : null,
                DescricaoMeioPagamento = dr["DESCRICAO_MEIO_PAGAMENTO"] != DBNull.Value ? dr["DESCRICAO_MEIO_PAGAMENTO"].ToString() : null,
                DataAgendamento = dr["DATA_REPASSE_CALCULADA"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_REPASSE_CALCULADA"]) : null,
                QtdeRecebimento = dr["QTDE_RECEBIMENTO"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_RECEBIMENTO"]) : 0,
                ValorRecebimento = dr["VALOR_RECEBIMENTO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_RECEBIMENTO"]) : 0,
                ValorTaxaRecebimento = dr["VALOR_TAXA_RECEBIMENTO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_TAXA_RECEBIMENTO"]) : 0,
                TaxaRecebimento = dr["TAXA_RECEBIMENTO"] != DBNull.Value ? Convert.ToDecimal(dr["TAXA_RECEBIMENTO"]) : 0,
                TipoTaxaRecebimento = dr["TIPO_TAXA_RECEBIMENTO"] != DBNull.Value ? dr["TIPO_TAXA_RECEBIMENTO"].ToString() : "",
                Estabelecimento = estabelecimento
            };

            // Retorna
            return relatorioRecebimentoInfo;
        }

        #endregion
    }
}
