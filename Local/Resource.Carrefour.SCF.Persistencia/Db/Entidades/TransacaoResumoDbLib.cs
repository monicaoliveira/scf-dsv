﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para TransacaoResumoDbLib
    /// </summary>
    public class TransacaoResumoDbLib : IEntidadeDbLib<TransacaoResumoInfo>
    {
        #region IEntidadeDbLib<TransacaoResumoInfo> Members

        /// <summary>
        /// Consulta de Criticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<TransacaoResumoInfo> ConsultarObjetos(ConsultarObjetosRequest<TransacaoResumoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Codigo do favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_FAVORECIDO", condicaoInfo.Valores[0]);

                // CodigoArquivo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoArquivo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_ARQUIVO", condicaoInfo.Valores[0]);

                // TipoTransacao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoTransacao");
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_TRANSACAO", condicaoInfo.Valores[0]);

                // NsuHost
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NsuHost");
                if (condicaoInfo != null)
                    paramsProc.Add("pNSU_HOST", condicaoInfo.Valores[0]);

                // Chave
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Chave");
                if (condicaoInfo != null)
                    paramsProc.Add("pCHAVE", condicaoInfo.Valores[0]);

                // Chave 2
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Chave2");
                if (condicaoInfo != null)
                    paramsProc.Add("pCHAVE2", condicaoInfo.Valores[0]);


                // DataInclusao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INCLUSAO", condicaoInfo.Valores[0]);
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" && c.TipoCondicao == CondicaoTipoEnum.Maior);
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INCLUSAO_MAIOR", condicaoInfo.Valores[0]);
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" && c.TipoCondicao == CondicaoTipoEnum.Menor);
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INCLUSAO_MENOR", condicaoInfo.Valores[0]);

                // CodigoArquivoRetornoCessao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusRetornoCessao");
                if (condicaoInfo != null)
                    paramsProc.Add("pSTATUS_RET_CESSAO", SqlDbLib.EnumToDb<TransacaoStatusRetornoCessaoEnum>(condicaoInfo.Valores[0]));

                // RetornoCessao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "RetornoCessao");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_RETORNO_CESSAO", ((bool)condicaoInfo.Valores[0]) ? "S" : "N");

                // CodigoProcesso
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoProcesso");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PROCESSO", condicaoInfo.Valores[0]);
            }

            // Número maximo de linhas a retornar
            paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_TRANSACAO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<TransacaoResumoInfo> resultado = new List<TransacaoResumoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<TransacaoResumoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<TransacaoResumoInfo> ReceberObjeto(ReceberObjetoRequest<TransacaoResumoInfo> request)
        {
            throw new NotImplementedException("ReceberObjeto de TransacaoResumoInfo não implementado");
        }

        /// <summary>
        /// Salva Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<TransacaoResumoInfo> SalvarObjeto(SalvarObjetoRequest<TransacaoResumoInfo> request)
        {
            throw new NotImplementedException("SalvarObjeto de TransacaoResumoInfo não implementado");
        }

        /// <summary>
        /// Remove um Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<TransacaoResumoInfo> RemoverObjeto(RemoverObjetoRequest<TransacaoResumoInfo> request)
        {
            throw new NotImplementedException("RemoverObjeto de TransacaoResumoInfo não implementado");
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public TransacaoResumoInfo MontarObjeto(System.Data.DataRow dr)
        {
            // Preenche com as informações do banco
            TransacaoResumoInfo TransacaoResumoInfo =
                new TransacaoResumoInfo()
                {
                    CodigoTransacao = dr["CODIGO_TRANSACAO"].ToString(),
                    Chave1 = dr["CHAVE"] != DBNull.Value ? (string)dr["CHAVE"] : null,
                    Chave2 = dr["CHAVE2"] != DBNull.Value ? (string)dr["CHAVE2"] : null,
                    CodigoArquivo1 = dr["CODIGO_ARQUIVO"] != DBNull.Value ? dr["CODIGO_ARQUIVO"].ToString() : null,
                    CodigoArquivo2 = dr["CODIGO_ARQUIVO2"] != DBNull.Value ? dr["CODIGO_ARQUIVO2"].ToString() : null,
                    CodigoArquivoItem1 = dr["CODIGO_ARQUIVO_ITEM_1"] != DBNull.Value ? dr["CODIGO_ARQUIVO_ITEM_1"].ToString() : null,
                    CodigoArquivoItem2 = dr["CODIGO_ARQUIVO_ITEM_2"] != DBNull.Value ? dr["CODIGO_ARQUIVO_ITEM_2"].ToString() : null,
                    CodigoArquivoRetornoCessao = dr["CODIGO_ARQUIVO_RET_CESSAO"] != DBNull.Value ? dr["CODIGO_ARQUIVO_RET_CESSAO"].ToString() : null,
                    CodigoContaFavorecido = dr["CODIGO_CONTA_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_CONTA_FAVORECIDO"].ToString() : null,
                    CodigoContaFavorecidoOriginal = dr["CODIGO_CONTA_FAVORECIDO_ORIG"] != DBNull.Value ? dr["CODIGO_CONTA_FAVORECIDO_ORIG"].ToString() : null,
                    CodigoEstabelecimento = dr["CODIGO_ESTABELECIMENTO"] != DBNull.Value ? dr["CODIGO_ESTABELECIMENTO"].ToString() : null,
                    CodigoFavorecido = dr["CODIGO_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_FAVORECIDO"].ToString() : null,
                    CodigoFavorecidoOriginal = dr["CODIGO_FAVORECIDO_ORIGINAL"] != DBNull.Value ? dr["CODIGO_FAVORECIDO_ORIGINAL"].ToString() : null,
                    CodigoPagamento = dr["CODIGO_PAGAMENTO"] != DBNull.Value ? dr["CODIGO_PAGAMENTO"].ToString() : null,
                    CodigoProcesso = dr["CODIGO_PROCESSO"] != DBNull.Value ? dr["CODIGO_PROCESSO"].ToString() : null,
                    CodigoProcessoRetornoCessao = dr["CODIGO_PROCESSO_RET_CESSAO"] != DBNull.Value ? dr["CODIGO_PROCESSO_RET_CESSAO"].ToString() : null,
                    CodigoProduto = dr["CODIGO_PRODUTO"] != DBNull.Value ? dr["CODIGO_PRODUTO"].ToString() : null,
                    DataInclusao = dr["DATA_INCLUSAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_INCLUSAO"]) : null,
                    DataRepasse = dr["DATA_REPASSE"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_REPASSE"]) : null,
                    DataRepasseCalculada = dr["DATA_REPASSE_CALCULADA"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_REPASSE_CALCULADA"]) : null,
                    DataTransacao = dr["DATA_TRANSACAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_TRANSACAO"]) : null,
                    NsuHost = dr["NSU_HOST"] != DBNull.Value ? (string)dr["NSU_HOST"] : null,
                    StatusConciliacao = dr["STATUS_CONCILIACAO"] != DBNull.Value ? SqlDbLib.EnumToObject<TransacaoStatusConciliacaoEnum>(dr["STATUS_CONCILIACAO"]) : TransacaoStatusConciliacaoEnum.NaoConciliado,
                    StatusRetornoCessao = dr["STATUS_RET_CESSAO"] != DBNull.Value ? SqlDbLib.EnumToObject<TransacaoStatusRetornoCessaoEnum>(dr["STATUS_RET_CESSAO"]) : TransacaoStatusRetornoCessaoEnum.NaoRetornado,
                    StatusTransacao = dr["STATUS_TRANSACAO"] != DBNull.Value ? SqlDbLib.EnumToObject<TransacaoStatusEnum>(dr["STATUS_TRANSACAO"]) : TransacaoStatusEnum.Importado,
                    StatusValidacao = dr["STATUS_VALIDACAO"] != DBNull.Value ? SqlDbLib.EnumToObject<TransacaoStatusValidacaoEnum>(dr["STATUS_VALIDACAO"]) : TransacaoStatusValidacaoEnum.PendenteValidacao,
                    TipoTransacao = dr["TIPO_TRANSACAO"] != DBNull.Value ? (string)dr["TIPO_TRANSACAO"] : null,
                    DataMovimento = dr["DATA_PROCESSAMENTO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_PROCESSAMENTO"]) : null
                };

            // Retorna
            return TransacaoResumoInfo;
        }

        #endregion
    }
}
