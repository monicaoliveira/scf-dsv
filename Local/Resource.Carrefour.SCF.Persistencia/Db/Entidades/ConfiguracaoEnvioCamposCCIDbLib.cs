﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class ConfiguracaoEnvioCamposCCIDbLib : IEntidadeDbLib<ConfiguracaoEnvioCamposCCIInfo>
    {
        #region IEntidadeDbLib<ConfiguracaoEnvioCamposCCIInfo> Members

        #region Consultar

        public ConsultarObjetosResponse<ConfiguracaoEnvioCamposCCIInfo> ConsultarObjetos(ConsultarObjetosRequest<ConfiguracaoEnvioCamposCCIInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // CodigoCampoEnvioCCI
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoCampoEnvioCCI");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_CAMPO_ENVIO_CCI", condicaoInfo.Valores[0]);

                // EnviarCCI
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "EnviarCCI");
                if (condicaoInfo != null)
                    if (condicaoInfo.Valores[0] != null && (bool)condicaoInfo.Valores[0] == true)
                        paramsProc.Add("pSTATUS_ENVIO", "S");
                    else
                        paramsProc.Add("pSTATUS_ENVIO", "N");
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CAMPOS_ENVIO_CCI_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ConfiguracaoEnvioCamposCCIInfo> resultado = new List<ConfiguracaoEnvioCamposCCIInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ConfiguracaoEnvioCamposCCIInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<ConfiguracaoEnvioCamposCCIInfo> ReceberObjeto(ReceberObjetoRequest<ConfiguracaoEnvioCamposCCIInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CAMPOS_ENVIO_CCI_L",
                        "pCODIGO_CAMPO_ENVIO_CCI", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<ConfiguracaoEnvioCamposCCIInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<ConfiguracaoEnvioCamposCCIInfo> RemoverObjeto(RemoverObjetoRequest<ConfiguracaoEnvioCamposCCIInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_CONFIG_TIPO_REGISTRO_R",
                "pCODIGO_CONFIG_TIPO_REGISTRO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<ConfiguracaoEnvioCamposCCIInfo>();
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<ConfiguracaoEnvioCamposCCIInfo> SalvarObjeto(SalvarObjetoRequest<ConfiguracaoEnvioCamposCCIInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoCampoEnvioCCI;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_CAMPO_ENVIO_CCI", pk);
            paramsProc.Add("pNOME_CAMPO", request.Objeto.NomeCampoEnvioCCI);
            paramsProc.Add("pSTATUS_ENVIO", request.Objeto.EnviarCCI == true ? "S" : "N");
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CAMPOS_ENVIO_CCI_S", paramsProc);

            //// Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            ConfiguracaoEnvioCamposCCIInfo configuracaoSistemaSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<ConfiguracaoEnvioCamposCCIInfo>()
                {
                    Objeto = configuracaoSistemaSalvo
                };
        }

        #endregion

        #endregion


        #region MontarObjeto 

        public ConfiguracaoEnvioCamposCCIInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            ConfiguracaoEnvioCamposCCIInfo configuracaoEnvioCamposCCIInfo =
                new ConfiguracaoEnvioCamposCCIInfo()
                {
                    CodigoCampoEnvioCCI = dr["CODIGO_CAMPO_ENVIO_CCI"] != DBNull.Value ? dr["CODIGO_CAMPO_ENVIO_CCI"].ToString() : null,
                    NomeCampoEnvioCCI = dr["NOME_CAMPO"].ToString(),
                    EnviarCCI = dr["STATUS_ENVIO_CCI"].ToString() == "S" ? true : false 
                };

            
            // Retorna
            return configuracaoEnvioCamposCCIInfo;
        }

        #endregion

    }
}
