﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para CriticaAgrupamentoDetalheDbLib
    /// </summary>
    public class CriticaAgrupamentoDetalheDbLib : IEntidadeDbLib<CriticaAgrupamentoDetalheInfo>
    {
        #region IEntidadeDbLib<CriticaSCFResumoInfo> Members

        /// <summary>
        /// Consulta de Detalhe de Agrupamento de Criticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<CriticaAgrupamentoDetalheInfo> ConsultarObjetos(ConsultarObjetosRequest<CriticaAgrupamentoDetalheInfo> request)
        {
            throw new NotImplementedException("ConsultarObjetos de CriticaAgrupamentoDetalheInfo não implementado");
        }

        /// <summary>
        /// Detalhe de Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<CriticaAgrupamentoDetalheInfo> ReceberObjeto(ReceberObjetoRequest<CriticaAgrupamentoDetalheInfo> request)
        {
            // Pega mensagem com o tipo especifico
            ReceberCriticaAgrupamentoDetalheDbRequest request2 = (ReceberCriticaAgrupamentoDetalheDbRequest)request;

            // Faz a consulta no banco
            Dictionary<string, object> retornoProc = 
                OracleDbLib2.Default.ExecutarProcedure(
                    "PR_CRITICA_AGRUPAMENTO_DET_B", 
                    "pCODIGO_ARQUIVO", request2.CodigoArquivo,
                    "pTIPO_LINHA", request2.TipoLinha,
                    "pTIPO_CRITICA", request2.TipoCritica,
                    "pNOME_CAMPO", request2.NomeCampo);

            // Pega as tabelas
            DataTable tb = (DataTable)retornoProc["RETCUR"];
            // 1463 DataTable tbStatus = (DataTable)retornoProc["RETCURSTATUS"];

            // Retorna
            return
                new ReceberObjetoResponse<CriticaAgrupamentoDetalheInfo>()
                {
                    //1463 Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0], tbStatus) : null
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        /// <summary>
        /// Salva Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<CriticaAgrupamentoDetalheInfo> SalvarObjeto(SalvarObjetoRequest<CriticaAgrupamentoDetalheInfo> request)
        {
            throw new NotImplementedException("SalvarObjeto de CriticaAgrupamentoDetalheInfo não implementado");
        }

        /// <summary>
        /// Remove um Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<CriticaAgrupamentoDetalheInfo> RemoverObjeto(RemoverObjetoRequest<CriticaAgrupamentoDetalheInfo> request)
        {
            throw new NotImplementedException("RemoverObjeto de CriticaAgrupamentoDetalheInfo não implementado");
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public CriticaAgrupamentoDetalheInfo MontarObjeto(System.Data.DataRow dr)
        {
            return this.MontarObjeto(dr, null);
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public CriticaAgrupamentoDetalheInfo MontarObjeto(System.Data.DataRow dr, DataTable tbStatus)
        {
            // Preenche com as informações do banco
            CriticaAgrupamentoDetalheInfo criticaAgrupamentoDetalheInfo =
                new CriticaAgrupamentoDetalheInfo()
                {
                    CodigoArquivo                           = dr["CODIGO_ARQUIVO"] != DBNull.Value ? dr["CODIGO_ARQUIVO"].ToString() : null,
                    TipoCritica                             = dr["TIPO_CRITICA"] != DBNull.Value ? (string)dr["TIPO_CRITICA"] : null,
                    TipoLinha                               = dr["TIPO_LINHA"] != DBNull.Value ? (string)dr["TIPO_LINHA"] : null,
                    NomeCampo                               = dr["NOME_CAMPO"] != DBNull.Value ? (string)dr["NOME_CAMPO"] : null,
                    QuantidadeCriticas                      = dr["QUANTIDADE_CRITICAS"] != DBNull.Value ? Convert.ToInt32(dr["QUANTIDADE_CRITICAS"]) : 0,
                    //1463
                    QuantidadeLinhas                        = dr["QUANTIDADE_LINHAS"] != DBNull.Value ? Convert.ToInt32(dr["QUANTIDADE_LINHAS"]) : 0,
                    QuantidadeLinhasValidas                 = dr["STATUS01"] != DBNull.Value ? Convert.ToInt32(dr["STATUS01"]) : 0,
                    QuantidadeLinhasInvalidas               = dr["STATUS02"] != DBNull.Value ? Convert.ToInt32(dr["STATUS02"]) : 0,
                    QuantidadeLinhasBloqueadas              = dr["STATUS03"] != DBNull.Value ? Convert.ToInt32(dr["STATUS03"]) : 0,
                    QuantidadeLinhasExcluidas               = dr["STATUS04"] != DBNull.Value ? Convert.ToInt32(dr["STATUS04"]) : 0,
                    QuantidadeLinhasPendentesDesbloqueio    = dr["STATUS05"] != DBNull.Value ? Convert.ToInt32(dr["STATUS05"]) : 0
                };

            //1463
            /*
            // Insere quantidade de linhas totais e por status
            if (tbStatus != null)
            {
                // Varre...
                foreach (DataRow drStatus in tbStatus.Rows)
                {
                    // Converte a quantidade
                    int quantidade = Convert.ToInt32(drStatus[1]);

                    // Total?
                    if (drStatus[0] == DBNull.Value)
                        criticaAgrupamentoDetalheInfo.QuantidadeLinhas = quantidade;
                    else
                    {
                        // Converte o status
                        ArquivoItemStatusEnum arquivoItemStatus = SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(drStatus[0]);

                        // Coloca na propriedade correta
                        switch (arquivoItemStatus)
                        {
                            case ArquivoItemStatusEnum.Valido:
                                criticaAgrupamentoDetalheInfo.QuantidadeLinhasValidas = quantidade;
                                break;
                            case ArquivoItemStatusEnum.PendenteValidacao:
                                criticaAgrupamentoDetalheInfo.QuantidadeLinhasPendentesValidacao = quantidade;
                                break;
                            case ArquivoItemStatusEnum.PendenteDesbloqueio:
                                criticaAgrupamentoDetalheInfo.QuantidadeLinhasPendentesDesbloqueio = quantidade;
                                break;
                            case ArquivoItemStatusEnum.Invalido:
                                criticaAgrupamentoDetalheInfo.QuantidadeLinhasInvalidas = quantidade;
                                break;
                            case ArquivoItemStatusEnum.Excluido:
                                criticaAgrupamentoDetalheInfo.QuantidadeLinhasExcluidas = quantidade;
                                break;
                            case ArquivoItemStatusEnum.Bloqueado:
                                criticaAgrupamentoDetalheInfo.QuantidadeLinhasBloqueadas = quantidade;
                                break;
                        }
                    }
                }
            
            }
            */
            // Retorna
            return criticaAgrupamentoDetalheInfo;
        }

        #endregion
    }
}
