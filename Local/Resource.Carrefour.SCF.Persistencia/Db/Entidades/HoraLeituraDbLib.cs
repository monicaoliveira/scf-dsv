﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class HoraLeituraDbLib : IEntidadeDbLib<HoraLeituraInfo>
    {
        #region IEntidadeDbLib<HoraLeituraInfo> Members

        #region Consultar

        public ConsultarObjetosResponse<HoraLeituraInfo> ConsultarObjetos(ConsultarObjetosRequest<HoraLeituraInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // NomeHoraLeitura
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeHoraLeitura");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_HORA_LEITURA", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_HORA_LEITURA_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<HoraLeituraInfo> resultado = new List<HoraLeituraInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<HoraLeituraInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<HoraLeituraInfo> ReceberObjeto(ReceberObjetoRequest<HoraLeituraInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_HORA_LEITURA_B",
                        "pCODIGO_HORA_LEITURA", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<HoraLeituraInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) :  null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<HoraLeituraInfo> RemoverObjeto(RemoverObjetoRequest<HoraLeituraInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_HORA_LEITURA_R",
                "pCODIGO_HORA_LEITURA", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<HoraLeituraInfo>();
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<HoraLeituraInfo> SalvarObjeto(SalvarObjetoRequest<HoraLeituraInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoHoraLeitura;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_HORA_LEITURA", pk);
            paramsProc.Add("pDE", request.Objeto.De);
            paramsProc.Add("pATE", request.Objeto.Ate);
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_HORA_LEITURA_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            HoraLeituraInfo horaLeituraSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<HoraLeituraInfo>()
                {
                    Objeto = horaLeituraSalvo
                };
        }

        #endregion

        #endregion


        #region MontarObjeto 

        public HoraLeituraInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            HoraLeituraInfo horaLeituraInfo =
                new HoraLeituraInfo()
                {
                    CodigoHoraLeitura = dr["CODIGO_HORA_LEITURA"] != DBNull.Value ? dr["CODIGO_HORA_LEITURA"].ToString() : null,
                    De = dr["DE"] != DBNull.Value ? new DateTime?((DateTime)dr["DE"]) : null,
                    Ate = dr["ATE"] != DBNull.Value ? new DateTime?((DateTime)dr["ATE"]) : null
                };

            // Retorna
            return horaLeituraInfo;
        }

        #endregion

    }
}
