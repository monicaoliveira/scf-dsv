﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db.Oracle;

//using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class UsuarioGrupoAcessoDbLib 
    {
        
        #region Consultar

        public List<string> ConsultarObjetosPorUsuario(string codigoUsuario)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_USUARIO", codigoUsuario.ToUpper());
            
            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_USUARIO_GRUPO_ACESSO_L", paramsProc)["RETCUR"];

            //// Preenche a coleção resultado
            //List<UsuarioGrupoInfo> resultado = new List<UsuarioGrupoInfo>();
            //foreach (DataRow dr in tb.Rows)
            //    resultado.Add(this.MontarObjeto(dr));

            List<string> resultado = new List<string>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(dr["CODIGO_GRUPO_ACESSO"].ToString());

            // Retorna
            return resultado;
                
        }

        #endregion

        #region Remover

        public bool RemoverObjeto(string codigoUsuario, string codigoGrupoAcesso)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_USUARIO_GRUPO_ACESSO_R",
                "pCODIGO_USUARIO", codigoUsuario,
                "pCODIGO_GRUPO_ACESSO", codigoGrupoAcesso);

            // Retorna
            return true;
        }

        #endregion

        #region Salvar

        public UsuarioGrupoInfo SalvarObjeto(string codigoUsuarioGrupo, string codigoUsuario)
        {
            
            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_USUARIO", codigoUsuario);
            paramsProc.Add("pCODIGO_GRUPO_ACESSO", codigoUsuarioGrupo);            
            paramsProc.Add("retornarRegistro", "S");

            // Execução da procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_USUARIO_GRUPO_ACESSO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return this.MontarObjeto(tb.Rows[0]);
                
        }

        #endregion
                
        #region MontarObjeto 

        public UsuarioGrupoInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            UsuarioGrupoInfo permissaoAssociadaInfo = new
                UsuarioGrupoInfo() 
                        { 
                            CodigoUsuarioGrupo = dr["CODIGO_GRUPO_ACESSO"].ToString()                            
                        };

            // Retorna
            return permissaoAssociadaInfo;
        }

        #endregion

        
    }
}
