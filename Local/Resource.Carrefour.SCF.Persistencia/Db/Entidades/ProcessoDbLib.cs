﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library.Db;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia de Processo
    /// </summary>
    public class ProcessoDbLib : IEntidadeDbLib<ProcessoInfo>
    {
        #region IEntidadeDbLib<ProcessoInfo> Members

        /// <summary>
        /// Consulta de processos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<ProcessoInfo> ConsultarObjetos(ConsultarObjetosRequest<ProcessoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Tipo do Processo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoProcesso");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_PROCESSO", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO", condicaoInfo.Valores[0]);

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusaoFinal" && c.TipoCondicao == CondicaoTipoEnum.Maior);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_MAIOR", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusaoInicial" && c.TipoCondicao == CondicaoTipoEnum.Menor);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_MENOR", condicaoInfo.Valores[0]);

                // Data Status - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataStatus" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_STATUS", condicaoInfo.Valores[0]);

                // Data Status - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataStatusFinal" && c.TipoCondicao == CondicaoTipoEnum.Maior);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_STATUS_MAIOR", condicaoInfo.Valores[0]);

                // Data Status - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataStatusInicial" && c.TipoCondicao == CondicaoTipoEnum.Menor);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_STATUS_MENOR", condicaoInfo.Valores[0]);

                // StatusProcesso
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusProcesso" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_PROCESSO", SqlDbLib.EnumToDb<ProcessoStatusEnum>(condicaoInfo.Valores[0]));

                // StatusProcesso diferente
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusProcessoDiferente" && c.TipoCondicao == CondicaoTipoEnum.Diferente);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_PROCESSO_DIF", SqlDbLib.EnumToDb<ProcessoStatusEnum>(condicaoInfo.Valores[0]));

                // StatusBloqueio
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusBloqueio");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_BLOQUEIO", SqlDbLib.EnumToDb<ProcessoStatusBloqueioEnum>(condicaoInfo.Valores[0]));

            }

            // Coloca o número máximo de linhas
            paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PROCESSO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ProcessoInfo> resultado = new List<ProcessoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ProcessoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<ProcessoInfo> ReceberObjeto(ReceberObjetoRequest<ProcessoInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PROCESSO_B", "pCODIGO_PROCESSO", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<ProcessoInfo>()
                {
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }
        /// <summary>
        /// Salva processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<ProcessoInfo> SalvarObjeto(SalvarObjetoRequest<ProcessoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoProcesso;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Serializa o objeto
            MemoryStream ms = new MemoryStream();
            new XmlSerializer(request.Objeto.GetType()).Serialize(ms, request.Objeto);
            ms.Position = 0;
            StreamReader reader = new StreamReader(ms);
            string serializacaoObjeto = reader.ReadToEnd();
            reader.Close();

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_PROCESSO", pk);
            paramsProc.Add("pTIPO_PROCESSO", request.Objeto.GetType().Name);
            paramsProc.Add("pTIPO_ESTAGIO_ATUAL", request.Objeto.TipoEstagioAtualString);
            paramsProc.Add("pSERIALIZACAO_PROCESSO", serializacaoObjeto);
            paramsProc.Add("pSTATUS_PROCESSO", SqlDbLib.EnumToDb<ProcessoStatusEnum>(request.Objeto.StatusProcesso));
            paramsProc.Add("pSTATUS_ESTAGIO", SqlDbLib.EnumToDb<EstagioStatusEnum>(request.Objeto.StatusEstagio));
            paramsProc.Add("pSTATUS_BLOQUEIO", SqlDbLib.EnumToDb<ProcessoStatusBloqueioEnum>(request.Objeto.StatusBloqueio));
            paramsProc.Add("pDATA_STATUS", request.Objeto.DataStatus);
            paramsProc.Add("pDATA_ESTAGIO", request.Objeto.DataEstagio);
            paramsProc.Add("pDATA_ATUALIZACAO", request.Objeto.DataAtualizacao);
            paramsProc.Add("retornarRegistro", "S");

            // Executa a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_PROCESSO_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<ProcessoInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        /// <summary>
        /// Remove um processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<ProcessoInfo> RemoverObjeto(RemoverObjetoRequest<ProcessoInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_PROCESSO_R",
                "pCODIGO_PROCESSO", request.CodigoObjeto);

            // Retorna
            return
                new RemoverObjetoResponse<ProcessoInfo>()
                {
                    CodigoMensagemRequest = request.CodigoMensagem
                };
        }

        /// <summary>
        /// Monta processo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ProcessoInfo MontarObjeto(System.Data.DataRow dr)
        {
            // Resolve o tipo
            Type tipoProcesso = ResolutorTipos.Resolver((string)dr["TIPO_PROCESSO"]);

            // Desserializa o log
            ProcessoInfo processoInfo =
                (ProcessoInfo)
                    new XmlSerializer(tipoProcesso).Deserialize(
                        new MemoryStream(
                            ASCIIEncoding.UTF8.GetBytes((string)dr["SERIALIZACAO_PROCESSO"])));

            // Preenche com as informações do banco
            processoInfo.TipoProcesso = dr["TIPO_PROCESSO"] != DBNull.Value ? dr["TIPO_PROCESSO"].ToString() : null;
            processoInfo.CodigoProcesso = dr["CODIGO_PROCESSO"].ToString();
            //processoInfo.NomeArquivo = dr["NOME_ARQUIVO"].ToString();
            processoInfo.NomeArquivo = (dr["NOME_ARQUIVO"] != null && dr["NOME_ARQUIVO"] != DBNull.Value) ? dr["NOME_ARQUIVO"].ToString() : null; 
            processoInfo.StatusProcesso = dr["STATUS_PROCESSO"] != DBNull.Value ? SqlDbLib.EnumToObject<ProcessoStatusEnum>(dr["STATUS_PROCESSO"].ToString()) : ProcessoStatusEnum.EmAndamento;
            processoInfo.StatusEstagio = dr["STATUS_ESTAGIO"] != DBNull.Value ? SqlDbLib.EnumToObject<EstagioStatusEnum>(dr["STATUS_ESTAGIO"].ToString()) : EstagioStatusEnum.Parado;
            processoInfo.StatusBloqueio = dr["STATUS_BLOQUEIO"] != DBNull.Value ? SqlDbLib.EnumToObject<ProcessoStatusBloqueioEnum>(dr["STATUS_BLOQUEIO"].ToString()) : ProcessoStatusBloqueioEnum.NaoHaBloqueios;
            processoInfo.DataInclusao = dr["DATA_INCLUSAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_INCLUSAO"]) : null;
            processoInfo.DataStatus = dr["DATA_STATUS"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_STATUS"]) : null;
            processoInfo.DataEstagio = dr["DATA_ESTAGIO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_ESTAGIO"]) : null;
            processoInfo.TipoEstagioAtualString = dr["TIPO_ESTAGIO_ATUAL"] != DBNull.Value ? (string)dr["TIPO_ESTAGIO_ATUAL"] : null;
            processoInfo.DataAtualizacao = dr["DATA_ATUALIZACAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_ATUALIZACAO"]) : null;
            
            // Retorna
            return processoInfo;
        }

        #endregion
    }
}
