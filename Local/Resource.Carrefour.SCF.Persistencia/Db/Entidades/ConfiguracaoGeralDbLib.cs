﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class ConfiguracaoGeralDbLib : IEntidadeDbLib<ConfiguracaoGeralInfo>
    {
        #region IEntidadeDbLib<ConfiguracaoGeralInfo> Members

        #region Consultar

        public ConsultarObjetosResponse<ConfiguracaoGeralInfo> ConsultarObjetos(ConsultarObjetosRequest<ConfiguracaoGeralInfo> request)
        {
            return null;
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<ConfiguracaoGeralInfo> ReceberObjeto(ReceberObjetoRequest<ConfiguracaoGeralInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure("PR_CONFIGURACAO_SISTEMA_B");

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<ConfiguracaoGeralInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) :  null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<ConfiguracaoGeralInfo> RemoverObjeto(RemoverObjetoRequest<ConfiguracaoGeralInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_CONFIGURACAO_SISTEMA_R",
                "pCODIGO_CONFIGURACAO_SISTEMA", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<ConfiguracaoGeralInfo>();
        }

        #endregion

        #region Salvar
        
        public SalvarObjetoResponse<ConfiguracaoGeralInfo> SalvarObjeto(SalvarObjetoRequest<ConfiguracaoGeralInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoConfiguracaoGeral;
            if (pk != null && pk.Contains("-"))
                pk = null;
            
            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_CONFIGURACAO_SISTEMA", pk);
            paramsProc.Add("pPRAZO_PAGAMENTO", request.Objeto.PrazoPagamento);
            paramsProc.Add("pDIAS_VENCIDO", request.Objeto.QuantidadeDiasVencidos);
            paramsProc.Add("pDIR_IMP_EXTRATO", request.Objeto.DiretorioImportacaoExtratoTSYSCarrefour);
            paramsProc.Add("pDIR_IMP_EXTRATO_ATACADAO", request.Objeto.DiretorioImportacaoExtratoTSYSAtacadao);
            paramsProc.Add("pDIR_IMP_EXTRATO_GALERIA", request.Objeto.DiretorioImportacaoExtratoTSYSGaleria);
            paramsProc.Add("pDIR_EXP_CESSAO", request.Objeto.DiretorioExportacaoCCI);
            paramsProc.Add("pDIR_IMP_RET_CESSAO", request.Objeto.DiretorioImportacaoCessao);
            paramsProc.Add("pDIR_EXP_MATERA_CONTABIL", request.Objeto.DiretorioExportacaoMateraContabil);
            paramsProc.Add("pDIR_EXP_MATERA_FINANCEIRO", request.Objeto.DiretorioExportacaoMateraFinanceiro);
            paramsProc.Add("pDIR_EXP_MATERA_EXTRATO", request.Objeto.DiretorioExportacaoMateraExtrato);
            paramsProc.Add("pFL_ENVIO_CESSAO_AUT", request.Objeto.EnviarCessaoAutomatico == true ? "S" : "N");
            // Esse campo nao será mais utilizado. A geracao do pagamento será feita através do relatório de vencimentos.
            //paramsProc.Add("pFL_ENVIO_MATERA_AUT", request.Objeto.EnviarMateraFinanceiroAutomatico == true ? "S" : "N");
            paramsProc.Add("pHORA_DE_LEITURA_EXTRATO", request.Objeto.HoraLeituraDeTSYS);
            paramsProc.Add("pHORA_ATE_LEITURA_RET_CESSAO", request.Objeto.HoraLeituraDeCCI);
            paramsProc.Add("pDIR_IMP_ATACADAO_MA", request.Objeto.DiretorioImportacaoAtacadaoMA);
            paramsProc.Add("pDIR_IMP_ATACADAO_SITEF", request.Objeto.DiretorioImportacaoAtacadaoSITEF);
            paramsProc.Add("pDIR_IMP_ATACADAO_TSYS", request.Objeto.DiretorioImportacaoAtacadaoTSYS);
            paramsProc.Add("pDIR_EXP_ATACADAO_CONC", request.Objeto.DiretorioExportacaoAtacadaoConciliacao);
            paramsProc.Add("pDIR_EXP_ATACADAO_CSUSEM", request.Objeto.DiretorioExportacaoAtacadaoCSUSemAtacadao);
            paramsProc.Add("pHORA_DE_LEITURA_ATACADAO", request.Objeto.HoraLeituraDeAtacadao);
            paramsProc.Add("pHORA_ATE_LEITURA_ATACADAO", request.Objeto.HoraLeituraAteAtacadao);
            paramsProc.Add("pDIR_EXP_REL_TRANSACAO", request.Objeto.DiretorioExportacaoRelatorioTransacao);
            paramsProc.Add("pDIR_EXP_REL_VENDA", request.Objeto.DiretorioExportacaoRelatorioVenda);
            paramsProc.Add("pDIR_EXP_REL_VENCIMENTO", request.Objeto.DiretorioExportacaoRelatorioVencimento);
            paramsProc.Add("pDIR_EXP_REL_CONSOLIDADO", request.Objeto.DiretorioExportacaoRelatorioConsolidado);
            paramsProc.Add("pDIR_EXP_REL_SALDO_CONSILIACAO", request.Objeto.DiretorioExportacaoRelatorioSaldoConciliacao);
            paramsProc.Add("pDIR_EXP_REL_RECEBIMENTO", request.Objeto.DiretorioExportacaoRelatorioRecebimento);
            paramsProc.Add("pDIR_EXP_REL_CHEQUES", request.Objeto.DiretorioExportacaoRelatorioCheques); //1465
            paramsProc.Add("pFL_HOMOLOGACAO_TSYS", request.Objeto.HomologacaoTSYS == true ? "S" : "N");
            paramsProc.Add("pFL_REPROCESSAR_NSU_PROCESSADO", request.Objeto.ReprocessarNSUProcessado == true ? "S" : "N");
            paramsProc.Add("pFL_REPROCESSAR_NSU_CESSIONADO", request.Objeto.ReprocessarNSUCessionado == true ? "S" : "N");
            paramsProc.Add("pSTATUS_AGENDADOR", request.Objeto.StatusServicoAgendador == ServicoAgendadorStatusEnum.Iniciado ? "1" : "0");
            paramsProc.Add("pSTATUS_EXPURGO", request.Objeto.StatusServicoExpurgo == ServicoExpurgoStatusEnum.Iniciado ? "1" : "0");
            paramsProc.Add("pDIAS_AGUARDAR_EXPURGO", request.Objeto.DiasAguardarExpurgo);
            paramsProc.Add("pDIAS_IGNORAR_EXPURGO", request.Objeto.DiasIgnorarExpurgo);
            paramsProc.Add("pDAT_ULTM_EXPURGO", request.Objeto.DataUltimoExpurgo);
            paramsProc.Add("pDIR_EXP_CESSAO_ATACADAO", request.Objeto.DiretorioExportacaoATA);
            paramsProc.Add("pDIR_EXP_CESSAO_GALERIA", request.Objeto.DiretorioExportacaoGAL);
            paramsProc.Add("pFL_ENVIO_CESSAO_AUT_ATACADAO", request.Objeto.EnviarCessaoAutomaticoAtacadao ==  true ? "S" : "N");
            paramsProc.Add("pFL_ENVIO_CESSAO_AUT_GALERIA", request.Objeto.EnviarCessaoAutomaticoGaleria == true ? "S" : "N");
            paramsProc.Add("pLAYOUT_CCI", request.Objeto.LayoutExportacaoCCI);
            paramsProc.Add("pLAYOUT_ATACADAO", request.Objeto.LayoutExportacaoATA);
            paramsProc.Add("pLAYOUT_GALERIA", request.Objeto.LayoutExportacaoGAL);
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                       "PR_CONFIGURACAO_SISTEMA_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            ConfiguracaoGeralInfo configuracaoSistemaSalvo = this.MontarObjeto(tb.Rows[0]);

            //ConfiguracaoGeralInfo configuracaoSistemaSalvo = this.MontarObjeto(dr);

            // Retorna
            return
                new SalvarObjetoResponse<ConfiguracaoGeralInfo>()
                {
                    Objeto = configuracaoSistemaSalvo
                };
        }

        #endregion

        #endregion

        #region MontarObjeto 

        public ConfiguracaoGeralInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            ConfiguracaoGeralInfo configuracaoGeralInfo =
                new ConfiguracaoGeralInfo()
                {

                    CodigoConfiguracaoGeral = dr["CODIGO_CONFIGURACAO_SISTEMA"] != DBNull.Value ? dr["CODIGO_CONFIGURACAO_SISTEMA"].ToString() : null,
                    PrazoPagamento = dr["PRAZO_PAGAMENTO"].ToString(),
                    QuantidadeDiasVencidos = dr["DIAS_VENCIDO"].ToString(),
                    DiretorioImportacaoExtratoTSYSCarrefour = dr["DIR_IMP_EXTRATO"].ToString(),
                    DiretorioImportacaoExtratoTSYSAtacadao = dr["DIR_IMP_EXTRATO_ATACADAO"].ToString(),
                    DiretorioImportacaoExtratoTSYSGaleria = dr["DIR_IMP_EXTRATO_GALERIA"].ToString(),
                    DiretorioExportacaoCCI = dr["DIR_EXP_CESSAO"].ToString(),
                    DiretorioExportacaoATA = dr["DIR_EXP_CESSAO_ATACADAO"].ToString(),
                    DiretorioExportacaoGAL = dr["DIR_EXP_CESSAO_GALERIA"].ToString(),
                    DiretorioImportacaoCessao = dr["DIR_IMP_RET_CESSAO"].ToString(),
                    DiretorioExportacaoMateraContabil= dr["DIR_EXP_MATERA_CONTABIL"].ToString(),
                    DiretorioExportacaoMateraFinanceiro = dr["DIR_EXP_MATERA_FINANCEIRO"].ToString(),
                    DiretorioExportacaoMateraExtrato = dr["DIR_EXP_MATERA_EXTRATO"].ToString(),
                    // Esse campo nao será mais utilizado. A geracao do pagamento será feita através do relatório de vencimentos.
                    //EnviarMateraFinanceiroAutomatico = dr["FL_ENVIO_MATERA_AUT"].ToString() == "S" ? true : false,
                    EnviarCessaoAutomatico = dr["FL_ENVIO_CESSAO_AUT"].ToString() == "S" ? true : false,
                    EnviarCessaoAutomaticoAtacadao = dr["FL_ENVIO_CESSAO_AUT_ATACADAO"].ToString() == "S" ? true : false,
                    EnviarCessaoAutomaticoGaleria = dr["FL_ENVIO_CESSAO_AUT_GALERIA"].ToString() == "S" ? true : false,
                    HoraLeituraDeTSYS   = dr["HORA_DE_LEITURA_EXTRATO"].ToString(),
                    HoraLeituraDeCCI  = dr["HORA_ATE_LEITURA_RET_CESSAO"].ToString() ,
                    DiretorioImportacaoAtacadaoMA = dr["DIR_IMP_ATACADAO_MA"].ToString(),
                    DiretorioImportacaoAtacadaoTSYS = dr["DIR_IMP_ATACADAO_TSYS"].ToString(),
                    DiretorioImportacaoAtacadaoSITEF = dr["DIR_IMP_ATACADAO_SITEF"].ToString(),
                    DiretorioExportacaoAtacadaoConciliacao = dr["DIR_EXP_ATACADAO_CONC"].ToString(),
                    DiretorioExportacaoAtacadaoCSUSemAtacadao = dr["DIR_EXP_ATACADAO_CSUSEM"].ToString(),
                    HoraLeituraDeAtacadao = dr["HORA_DE_LEITURA_ATACADAO"].ToString(),
                    HoraLeituraAteAtacadao = dr["HORA_ATE_LEITURA_ATACADAO"].ToString(),
                    DiretorioExportacaoRelatorioTransacao = dr["DIR_EXP_REL_TRANSACAO"].ToString(),
                    DiretorioExportacaoRelatorioVenda = dr["DIR_EXP_REL_VENDA"].ToString(),
                    DiretorioExportacaoRelatorioVencimento = dr["DIR_EXP_REL_VENCIMENTO"].ToString(),
                    DiretorioExportacaoRelatorioConsolidado = dr["DIR_EXP_REL_CONSOLIDADO"].ToString(),
                    DiretorioExportacaoRelatorioSaldoConciliacao = dr["DIR_EXP_REL_SALDO_CONSILIACAO"].ToString(),
                    DiretorioExportacaoRelatorioRecebimento = dr["DIR_EXP_REL_RECEBIMENTO"].ToString(),
                    DiretorioExportacaoRelatorioCheques = dr["DIR_EXP_REL_CHEQUES"].ToString(), //1465
                    StatusServicoAgendador = dr["STATUS_AGENDADOR"].ToString() == "1" ? ServicoAgendadorStatusEnum.Iniciado : ServicoAgendadorStatusEnum.Parado,
                    StatusServicoExpurgo = dr["STATUS_EXPURGO"].ToString() == "1" ? ServicoExpurgoStatusEnum.Iniciado : ServicoExpurgoStatusEnum.Parado,
                    DiasAguardarExpurgo = Int32.Parse(dr["DIAS_AGUARDAR_EXPURGO"].ToString()),
                    DiasIgnorarExpurgo = Int32.Parse(dr["DIAS_IGNORAR_EXPURGO"].ToString()),
                    DataUltimoExpurgo = dr["DAT_ULTM_EXPURGO"] != DBNull.Value ? new DateTime?((DateTime)dr["DAT_ULTM_EXPURGO"]) : null,
                    HomologacaoTSYS = dr["FL_HOMOLOGACAO_TSYS"].ToString() == "S" ? true : false,
                    ReprocessarNSUProcessado = dr["FL_REPROCESSAR_NSU_PROCESSADO"].ToString() == "S" ? true : false,
                    ReprocessarNSUCessionado = dr["FL_REPROCESSAR_NSU_CESSIONADO"].ToString() == "S" ? true : false,
                    LayoutExportacaoCCI = dr["LAYOUT_CCI"].ToString(),
                    LayoutExportacaoATA = dr["LAYOUT_ATACADAO"].ToString(),
                    LayoutExportacaoGAL = dr["LAYOUT_GALERIA"].ToString(),
                };
            
            // Retorna
            return configuracaoGeralInfo;
        }

        #endregion

    }
}
