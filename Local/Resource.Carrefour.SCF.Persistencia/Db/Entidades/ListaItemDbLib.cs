﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para ListaItem
    /// </summary>
    public class ListaItemDbLib : IEntidadeDbLib<ListaItemInfo>
    {
        #region IEntidadeDbLib<ListaItemInfo> Members

        /// <summary>
        /// Listar ListaItems
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<ListaItemInfo> ConsultarObjetos(ConsultarObjetosRequest<ListaItemInfo> request)
        {
            // Interpreta as condicoes
            Dictionary<string, object> parametros = new Dictionary<string, object>();

            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // CodigoLista
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoLista");
                if (condicaoInfo != null)
                    parametros.Add("pFILTRO_CODIGO_LISTA", condicaoInfo.Valores[0]);
                
                // NomeLista
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeLista");
                if (condicaoInfo != null)
                    parametros.Add("pNOME_LISTA", condicaoInfo.Valores[0]);

                // Mnemonico
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Valor");
                if (condicaoInfo != null)
                    parametros.Add("pVALOR", condicaoInfo.Valores[0]);
             }


            // Pede a lista
            DataTable tb =
                (DataTable) OracleDbLib2.Default.ExecutarProcedure(
                    "PR_LISTA_ITEM_L", parametros)["RETCUR"];

            // Prepara a resposta
            ConsultarObjetosResponse<ListaItemInfo> resposta = new ConsultarObjetosResponse<ListaItemInfo>();

            // Monta a lista
            foreach (DataRow dr in tb.Rows)
                resposta.Resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Retornar detalhe de ListaItem
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<ListaItemInfo> ReceberObjeto(ReceberObjetoRequest<ListaItemInfo> request)
        {
            // Pede o detalhe
            DataTable tb =
                (DataTable)OracleDbLib2.Default.ExecutarProcedure(
                    "PR_LISTA_ITEM_B",
                    "pCODIGO_LISTA_ITEM", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<ListaItemInfo>()
                {
                    CodigoSessao = request.CodigoSessao,
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) :
                        null
                };
        }

        /// <summary>
        /// Salvar ListaItem
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<ListaItemInfo> SalvarObjeto(SalvarObjetoRequest<ListaItemInfo> request)
        {
            // Infere a pk
            string pk = request.Objeto.CodigoListaItem;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Salva a ListaItem

            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(                 
                    "PR_LISTA_ITEM_S",
                    "CODIGO_LISTA_ITEM", pk,
                    "CODIGO_LISTA", request.Objeto.CodigoLista,
                    "NOME_LISTA_ITEM", request.Objeto.Descricao,
                    "MNEMONICO_LISTA_ITEM", request.Objeto.Mnemonico,
                    "ATIVO", request.Objeto.Ativo ? "S" : "N",
                    "VALOR", request.Objeto.Valor,
                    "retornarRegistro", "S")["RETCUR"];;


            // Pega as tabelas necessarias


            // Retorna
            return
                new SalvarObjetoResponse<ListaItemInfo>()
                {
                    CodigoSessao = request.CodigoSessao,
                    Objeto =
                        tb.Rows.Count > 0 ?
                            this.MontarObjeto(tb.Rows[0]) :
                            null
                };
        }

        /// <summary>
        /// Remover ListaItem
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<ListaItemInfo> RemoverObjeto(RemoverObjetoRequest<ListaItemInfo> request)
        {
            try
            {
                RemoverListaItemDbRequest req = (RemoverListaItemDbRequest)request;
                // Pede remocao
                OracleDbLib2.Default.ExecutarProcedure(
                    "PR_LISTA_ITEM_R",
                    "CODIGO_LISTA_ITEM", req.CodigoObjeto,
                    "ATIVO", req.Ativar ? 'S' : 'N'
                );

                // Retorna
                return
                    new RemoverObjetoResponse<ListaItemInfo>()
                    {
                        CodigoSessao = request.CodigoSessao
                    };
            }
            catch (DbLibException) // catch (DbLibException ex) // warning 25/02/2019
            {
                // Cria a critica
                CriticaInfo critica = new CriticaInfo();
                critica.Descricao = "Valor já existe no cadastro.";


                RemoverObjetoResponse<ListaItemInfo> resposta = new RemoverObjetoResponse<ListaItemInfo>();
                resposta.Criticas.Add(critica);
                return resposta;

            }
        }

        /// <summary>
        /// Monta ListaItem
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ListaItemInfo MontarObjeto(DataRow dr)
        {
            // Cria o objeto
            ListaItemInfo listaItemInfo =
                new ListaItemInfo();

            listaItemInfo.CodigoListaItem = dr["CODIGO_LISTA_ITEM"] != DBNull.Value ? dr["CODIGO_LISTA_ITEM"].ToString() : null;
            listaItemInfo.CodigoLista = dr["CODIGO_LISTA"] != DBNull.Value ? dr["CODIGO_LISTA"].ToString() : null;
            listaItemInfo.Descricao = dr["NOME_LISTA_ITEM"] != DBNull.Value ? (string)dr["NOME_LISTA_ITEM"] : null;
            listaItemInfo.Ativo = dr["ATIVO"] != DBNull.Value ? (string)dr["ATIVO"] == "S" ? true : false : false;
            listaItemInfo.Mnemonico = dr["MNEMONICO_LISTA_ITEM"] != DBNull.Value ? (string)dr["MNEMONICO_LISTA_ITEM"] : null;
            listaItemInfo.Valor = dr["VALOR"] != DBNull.Value ? (string)dr["VALOR"] : null;

            // Retorna o objeto
            return listaItemInfo;            
        }

        #endregion
    }
}
