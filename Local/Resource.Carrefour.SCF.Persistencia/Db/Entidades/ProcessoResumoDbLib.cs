﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library.Db;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia de ProcessoResumo
    /// </summary>
    public class ProcessoResumoDbLib : IEntidadeDbLib<ProcessoResumoInfo>
    {
        #region IEntidadeDbLib<ProcessoResumoInfo> Members

        /// <summary>
        /// Consulta de processos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<ProcessoResumoInfo> ConsultarObjetos(ConsultarObjetosRequest<ProcessoResumoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes.Count() > 0) //1493  //if (request.Condicoes != null)
            {
                // Tipo do Processo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoProcesso");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_PROCESSO", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" 
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_MENOR", condicaoInfo.Valores[0]);
                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" 
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_MAIOR", condicaoInfo.Valores[0]);
                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_MENOR", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_MAIOR", condicaoInfo.Valores[0]);
                }

                // Data Status - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataStatus" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null) 
                {
                    paramsProc.Add("pFILTRO_DATA_STATUS_MENOR", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_STATUS_MAIOR", condicaoInfo.Valores[0]);
                }                

                // Data Status - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataStatusFinal" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_STATUS_MAIOR", condicaoInfo.Valores[0]);
                }
                // Data Status - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataStatusInicial" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {                    
                    paramsProc.Add("pFILTRO_DATA_STATUS_MENOR", condicaoInfo.Valores[0]);
                }
                // StatusProcesso
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusProcesso" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_PROCESSO", SqlDbLib.EnumToDb<ProcessoStatusEnum>(condicaoInfo.Valores[0]));

                // StatusProcesso diferente
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusProcessoDiferente" && c.TipoCondicao == CondicaoTipoEnum.Diferente);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_PROCESSO_DIF", SqlDbLib.EnumToDb<ProcessoStatusEnum>(condicaoInfo.Valores[0]));

                // StatusBloqueio
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusBloqueio");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_BLOQUEIO", SqlDbLib.EnumToDb<ProcessoStatusBloqueioEnum>(condicaoInfo.Valores[0]));
            }
            else //1493 Apenas se NAO tem coleção de condições criada
                // Coloca o número máximo de linhas
                paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PROCESSO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ProcessoResumoInfo> resultado = new List<ProcessoResumoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ProcessoResumoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<ProcessoResumoInfo> ReceberObjeto(ReceberObjetoRequest<ProcessoResumoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Salva processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<ProcessoResumoInfo> SalvarObjeto(SalvarObjetoRequest<ProcessoResumoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Remove um processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<ProcessoResumoInfo> RemoverObjeto(RemoverObjetoRequest<ProcessoResumoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Monta processo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ProcessoResumoInfo MontarObjeto(System.Data.DataRow dr)
        {
            // Cria o resumo
            ProcessoResumoInfo processoResumoInfo = new ProcessoResumoInfo();

            // Preenche com as informações do banco
            processoResumoInfo.CodigoProcesso = dr["CODIGO_PROCESSO"].ToString();
            processoResumoInfo.TipoProcesso = dr["TIPO_PROCESSO"] != DBNull.Value ? dr["TIPO_PROCESSO"].ToString() : null;
            //processoResumoInfo.NomeArquivo = dr["NOME_ARQUIVO"].ToString();
            processoResumoInfo.NomeArquivo = dr["NOME_ARQUIVO"] != DBNull.Value ? dr["NOME_ARQUIVO"].ToString() : null;
            processoResumoInfo.StatusProcesso = dr["STATUS_PROCESSO"] != DBNull.Value ? SqlDbLib.EnumToObject<ProcessoStatusEnum>(dr["STATUS_PROCESSO"].ToString()) : ProcessoStatusEnum.EmAndamento;
            processoResumoInfo.StatusEstagio = dr["STATUS_ESTAGIO"] != DBNull.Value ? SqlDbLib.EnumToObject<EstagioStatusEnum>(dr["STATUS_ESTAGIO"].ToString()) : EstagioStatusEnum.Parado;
            processoResumoInfo.StatusBloqueio = dr["STATUS_BLOQUEIO"] != DBNull.Value ? SqlDbLib.EnumToObject<ProcessoStatusBloqueioEnum>(dr["STATUS_BLOQUEIO"].ToString()) : ProcessoStatusBloqueioEnum.NaoHaBloqueios;
            processoResumoInfo.DataInclusao = dr["DATA_INCLUSAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_INCLUSAO"]) : null;
            processoResumoInfo.DataStatus = dr["DATA_STATUS"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_STATUS"]) : null;
            processoResumoInfo.DataEstagio = dr["DATA_ESTAGIO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_ESTAGIO"]) : null;
            processoResumoInfo.TipoEstagioAtual = dr["TIPO_ESTAGIO_ATUAL"] != DBNull.Value ? (string)dr["TIPO_ESTAGIO_ATUAL"] : null;
            processoResumoInfo.EnviadoMatera = dr["STATUS_PAGAMENTO"] != DBNull.Value && dr["STATUS_PAGAMENTO"].ToString() == "2";

            // Retorna
            return processoResumoInfo;
        }

        #endregion
    }
}
