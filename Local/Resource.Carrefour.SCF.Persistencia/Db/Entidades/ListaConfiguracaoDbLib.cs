﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class ListaConfiguracaoDbLib : IEntidadeDbLib<ListaConfiguracaoInfo>
    {

        #region IEntidadeDbLib<ListaConfiguracao> Members

        public ConsultarObjetosResponse<ListaConfiguracaoInfo> ConsultarObjetos(ConsultarObjetosRequest<ListaConfiguracaoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // CodigoLista
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoLista");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_LISTA", condicaoInfo.Valores[0]);

                // Grupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoEmpresaGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_GRUPO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LISTA_CONF_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ListaConfiguracaoInfo> resultado = new List<ListaConfiguracaoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ListaConfiguracaoInfo>()
                {
                    Resultado = resultado
                };

        }

        public ReceberObjetoResponse<ListaConfiguracaoInfo> ReceberObjeto(ReceberObjetoRequest<ListaConfiguracaoInfo> request)
        {
            return new ReceberObjetoResponse<ListaConfiguracaoInfo>();
        }

        public SalvarObjetoResponse<ListaConfiguracaoInfo> SalvarObjeto(SalvarObjetoRequest<ListaConfiguracaoInfo> request)
        {
            return new SalvarObjetoResponse<ListaConfiguracaoInfo>();
        }

        public RemoverObjetoResponse<ListaConfiguracaoInfo> RemoverObjeto(RemoverObjetoRequest<ListaConfiguracaoInfo> request)
        {
            return new RemoverObjetoResponse<ListaConfiguracaoInfo>();
        }

        public ListaConfiguracaoInfo MontarObjeto(DataRow dr)
        {
            return new ListaConfiguracaoInfo()
            {
                CodigoLista = dr["CODIGO_LISTA"] != DBNull.Value ? dr["CODIGO_LISTA"].ToString() : null,
                Descricao = dr["NOME_LISTA"] != DBNull.Value ? dr["NOME_LISTA"].ToString() : null,
                Mnemonico = dr["MNEMONICO_LISTA"] != DBNull.Value ? dr["MNEMONICO_LISTA"].ToString() : null,
                ColunaConfiguravelHeader = dr["LISTA_CONF_HEADER"] != DBNull.Value ? dr["LISTA_CONF_HEADER"].ToString() : null
            };
        }

        #endregion
    }
}
