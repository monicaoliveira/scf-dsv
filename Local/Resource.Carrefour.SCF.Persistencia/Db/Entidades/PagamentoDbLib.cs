﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Classe de persistencia de Pagamento
    /// </summary>
    public class PagamentoDbLib : IEntidadeDbLib<PagamentoInfo>
    {
        #region IEntidadeDbLib<PagamentoInfo> Members

        /// <summary>
        /// Consulta pagamentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<PagamentoInfo> ConsultarObjetos(ConsultarObjetosRequest<PagamentoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Codigo do estabelecimento
                //condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoEstabelecimento");
                //if (condicaoInfo != null)
                //    paramsProc.Add("pFILTRO_CODIGO_ESTABELECIMENTO", condicaoInfo.Valores[0]);

                // Codigo do favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_FAVORECIDO", condicaoInfo.Valores[0]);

                // Banco
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Banco");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_BANCO", condicaoInfo.Valores[0]);

                // Agencia
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Agencia");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_AGENCIA", condicaoInfo.Valores[0]);

                // Conta
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Conta");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CONTA", condicaoInfo.Valores[0]);

                // Status Pagamento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroStatusPagamento");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_PAGAMENTO", condicaoInfo.Valores[0]);
            
                // Pagamento negativo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "ListarPagamentoNegativo");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_PAGAMENTO_NEGATIVO", condicaoInfo.Valores[0]);

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataPagamento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_PAGAMENTO", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataPagamento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_PAGAMENTO_FIM", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataPagamento"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_PAGAMENTO", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_PAGAMENTO_FIM", condicaoInfo.Valores[0]);
                }

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataInclusaoDe"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_DE", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataInclusaoAte"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_ATE", condicaoInfo.Valores[0]);

                // Data Envio - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataEnvioDe"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_ENVIO_DE", condicaoInfo.Valores[0]);

                // Data Envio - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataEnvioAte"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_ENVIO_ATE", condicaoInfo.Valores[0]);

                // Data Geracao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataGeracaoDe"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_GERACAO_DE", condicaoInfo.Valores[0]);

                // Data Envio - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataGeracaoAte"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_GERACAO_ATE", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "RetornarReferencias"
                    && (c.TipoCondicao == CondicaoTipoEnum.Igual));
                if (condicaoInfo != null )
                    if (Convert.ToBoolean(condicaoInfo.Valores[0]) == true)
                        paramsProc.Add("pRETORNAR_REFERENCIAS", "1" );


                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "RetornaApenasQuantidade"
                    && (c.TipoCondicao == CondicaoTipoEnum.Igual));
                if (condicaoInfo != null)
                    if (Convert.ToBoolean(condicaoInfo.Valores[0]) == true)
                        paramsProc.Add("pRETORNAR_QUANTIDADE", '1');

                /*
                //Filtro Data de Liquidacao
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataLiquidacao");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LIQUIDACAO", condicaoInfo.Valores[0]);
                */
                 
                // Numero maximo de resultados
                paramsProc.Add("maxLinhas", 100);

            
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PAGAMENTO_L", paramsProc)["RETCUR"]; //1447 apenas referencia de analise

            if (request is ListarPagamentoQtdeDbRequest)
            {

                return
                    new ListarPagamentoQtdeDbResponse()
                    {
                        QuantidadeLinhas = Convert.ToInt32(tb.Rows[0]["QTDE"])
                    };
            }

            // Preenche a coleção resultado
            List<PagamentoInfo> resultado = new List<PagamentoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<PagamentoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de pagamento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<PagamentoInfo> ReceberObjeto(ReceberObjetoRequest<PagamentoInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PAGAMENTO_B", "pCODIGO_PAGAMENTO", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<PagamentoInfo>()
                {
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        /// <summary>
        /// Salva pagamento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<PagamentoInfo> SalvarObjeto(SalvarObjetoRequest<PagamentoInfo> request)
        {
            // Prepara resposta
            SalvarObjetoResponse<PagamentoInfo> resposta = null;

            // Verifica se é mensagem específica para atualização de pagamentos
            if (request is AtualizarPagamentosDbRequest)
            {
                // Pega a mensagem com o tipo correto
                AtualizarPagamentosDbRequest request2 = (AtualizarPagamentosDbRequest)request;

                // Monta parametros
                Dictionary<string, object> paramsProc = new Dictionary<string, object>();
                paramsProc.Add("pFILTRO_CODIGO_PAGAMENTO", request2.FiltroCodigoPagamento);
                paramsProc.Add("pFILTRO_CODIGO_ARQUIVO", request2.FiltroCodigoArquivo);

                // Execução a procedure
                OracleDbLib2.Default.ExecutarProcedure("PR_PAGAMENTO_A", paramsProc);

                // Cria resposta
                resposta = new SalvarObjetoResponse<PagamentoInfo>();
            }
            else
            {

                // Verifica se é mensagem específica para liquidar de pagamentos negativos
                if (request is LiquidarPagamentoNegativoDbRequest)
                {
                    // Pega a mensagem com o tipo correto
                    LiquidarPagamentoNegativoDbRequest request2 = (LiquidarPagamentoNegativoDbRequest)request;

                    // Monta parametros
                    Dictionary<string, object> paramsProc = new Dictionary<string, object>();
                    paramsProc.Add("pCODIGO_PAGAMENTO", request2.CodigoPagamento);
                    paramsProc.Add("pDATA_LIQUIDACAO", request2.DataLiquidacao);

                    // Execução a procedure
                    OracleDbLib2.Default.ExecutarProcedure("PR_PAGAMENTO_LIQUIDAR_MANUAL", paramsProc);

                    // Cria resposta
                    resposta = new SalvarObjetoResponse<PagamentoInfo>();
                }
                else
                    // Verifica se é mensagem específica para liquidar de pagamentos negativos
                    if (request is AtualizarStatusPagamentoDbRequest)
                    {
                        // Pega a mensagem com o tipo correto
                        AtualizarStatusPagamentoDbRequest request2 = (AtualizarStatusPagamentoDbRequest)request;

                        // Monta parametros
                        Dictionary<string, object> paramsProc = new Dictionary<string, object>();
                        paramsProc.Add("pCODIGO_PAGAMENTO", request2.CodigoPagamento);
                        paramsProc.Add("pSTATUS_PAGAMENTO", SqlDbLib.EnumToDb<PagamentoStatusEnum>(request2.Status));
                        paramsProc.Add("pCODIGO_USUARIO_ENVIO", request2.CodigoUsuarioEnvio);
                        paramsProc.Add("pCODIGO_USUARIO_EXCLUSAO", request2.CodigoUsuarioExclusao); 

                        // Execução a procedure
                        OracleDbLib2.Default.ExecutarProcedure("PR_PAGAMENTO_ATUALIZAR_STATUS", paramsProc);

                        // Cria resposta
                        resposta = new SalvarObjetoResponse<PagamentoInfo>();
                    }
                    else
                    {
                        // Verifica se é novo ou alteração
                        string pk = request.Objeto.CodigoPagamento;
                        if (pk != null && pk.Contains("-"))
                            pk = null;

                        // Monta parametros
                        Dictionary<string, object> paramsProc = new Dictionary<string, object>();
                        //paramsProc.Add("pCODIGO_PAGAMENTO", pk);
                        paramsProc.Add("pDATA_PAGAMENTO", request.Objeto.DataPagamento);
                        paramsProc.Add("pVALOR_PAGAMENTO", request.Objeto.ValorPagamento);
                        paramsProc.Add("pSTATUS_PAGAMENTO", SqlDbLib.EnumToDb<PagamentoStatusEnum>(request.Objeto.Status));
                        paramsProc.Add("pBANCO", request.Objeto.Banco);
                        paramsProc.Add("pAGENCIA", request.Objeto.Agencia);
                        paramsProc.Add("pCONTA", request.Objeto.Conta);
                        paramsProc.Add("pCODIGO_FAVORECIDO", request.Objeto.CodigoFavorecido);
                        paramsProc.Add("retornarRegistro", "S");


                        // Execução a procedure
                        DataTable tb =
                            (DataTable)
                                OracleDbLib2.Default.ExecutarProcedure("PR_PAGAMENTO_S", paramsProc)["RETCUR"];

                        // Cria resposta
                        resposta =
                            new SalvarObjetoResponse<PagamentoInfo>()
                            {
                                Objeto = this.MontarObjeto(tb.Rows[0])
                            };
                    }
                }
            

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove pagamento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<PagamentoInfo> RemoverObjeto(RemoverObjetoRequest<PagamentoInfo> request)
        {
            // Monta a execução da procedure - Exclusao logica
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_PAGAMENTO_R2",
                "pCODIGO_PAGAMENTO", request.CodigoObjeto);

            
            // Retorna
            return new RemoverObjetoResponse<PagamentoInfo>();
        }

        /// <summary>
        /// Monta um pagamento
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public PagamentoInfo MontarObjeto(DataRow dr)
        {
            // Retorna
            return
                new PagamentoDetalheView()
                {
                    CodigoPagamento = dr["CODIGO_PAGAMENTO"].ToString(),
                    Banco = dr["BANCO"] != DBNull.Value ? (string)dr["BANCO"] : null,
                    Agencia = dr["AGENCIA"] != DBNull.Value ? (string)dr["AGENCIA"] : null,
                    Conta = dr["CONTA"] != DBNull.Value ? (string)dr["CONTA"] : null,
                    CodigoFavorecido = dr["CODIGO_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_FAVORECIDO"].ToString() : null,
                    DataPagamento = dr["DATA_PAGAMENTO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_PAGAMENTO"]) : null,
                    Status = dr["STATUS_PAGAMENTO"] != DBNull.Value ? SqlDbLib.EnumToObject<PagamentoStatusEnum>(dr["STATUS_PAGAMENTO"]) : PagamentoStatusEnum.Pendente,
                    ValorPagamento = dr["VALOR_PAGAMENTO"] != DBNull.Value ? new double?((double)dr["VALOR_PAGAMENTO"]) : null,
                    CNPJ = dr["CNPJ_FAVORECIDO"] != DBNull.Value ? (string)dr["CNPJ_FAVORECIDO"] : null,
                    NomeFavorecido = dr["NOME_FAVORECIDO"] != DBNull.Value ? (string)dr["NOME_FAVORECIDO"] : null,
                    DataLiquidacaoManual = dr["DATA_LIQUIDACAO_MANUAL"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_LIQUIDACAO_MANUAL"]) : null,
                    //CodigoEmpresaGrupo = dr["CODIGO_EMPRESA_GRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_GRUPO"].ToString() : null,
                    //DescricaoEmpresaGrupo = dr["DESCRICAO_EMPRESA_GRUPO"] != DBNull.Value ? dr["DESCRICAO_EMPRESA_GRUPO"].ToString() : null
                    DataEnvio = dr["DATA_ENVIO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_ENVIO"]) : null,
                    DataGeracao = dr["DATA_GERACAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_GERACAO"]) : null,
                    DataExclusao = dr["DATA_EXCLUSAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_EXCLUSAO"]) : null,
                    CodigoReferencia = dr["CODIGO_REFERENCIA"] != DBNull.Value ? (string)dr["CODIGO_REFERENCIA"] : null,
                    ValorReferencia = dr["VALOR_PAGAMENTO_REFERENCIA"] != DBNull.Value ? new double?((double)dr["VALOR_PAGAMENTO_REFERENCIA"]) : null,
                    CodigoTipoDocumentoMatera = dr["CODIGO_TIPO_DOC_MATERA"] != DBNull.Value ? (string)dr["CODIGO_TIPO_DOC_MATERA"] : null,
                    CodigoTipoProdutoServicoMatera = dr["CODIGO_TIPO_PROD_SERV_MATERA"] != DBNull.Value ? (string)dr["CODIGO_TIPO_PROD_SERV_MATERA"] : null,
                    CodigoContaContabil = dr["CODIGO_CONTA_CONTABIL"] != DBNull.Value ? (string)dr["CODIGO_CONTA_CONTABIL"] : null



                };
        }

        #endregion
    }
}
