﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db.Oracle;

//sing Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class UsuarioGrupoPerfilDbLib
    {

        #region Consultar

        public List<string> ConsultarObjetosPorGrupo(string codigoGrupoUsuario)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_GRUPO_ACESSO", codigoGrupoUsuario);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_GRUPO_ACESSO_PERFIL_L", paramsProc)["RETCUR"];

            //// Preenche a coleção resultado
            //List<UsuarioGrupoInfo> resultado = new List<UsuarioGrupoInfo>();
            //foreach (DataRow dr in tb.Rows)
            //    resultado.Add(this.MontarObjeto(dr));

            List<string> resultado = new List<string>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(dr["CODIGO_PERFIL"].ToString());

            // Retorna
            return resultado;

        }

        #endregion

        #region Remover

        public bool RemoverObjeto(string codigoGrupoUsuario, string codigoPerfil)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_GRUPO_ACESSO_PERFIL_R",
                "pCODIGO_GRUPO_ACESSO", codigoGrupoUsuario,
                "pCODIGO_PERFIL", codigoPerfil);

            // Retorna
            return true;
        }

        #endregion

        #region Salvar

        public PerfilInfo SalvarObjeto(string perfilAssociado, string codigoUsuarioGrupo)
        {

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_GRUPO_ACESSO", codigoUsuarioGrupo);
            paramsProc.Add("pCODIGO_PERFIL", perfilAssociado);
            paramsProc.Add("retornarRegistro", "S");

            // Execução da procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_GRUPO_ACESSO_PERFIL_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return this.MontarObjeto(tb.Rows[0]);

        }

        #endregion

        #region MontarObjeto

        public PerfilInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            PerfilInfo perfilAssociadoInfo = new
                PerfilInfo()
            {
                CodigoPerfil = dr["CODIGO_PERFIL"].ToString()
            };

            // Retorna
            return perfilAssociadoInfo;
        }

        #endregion


    }
}
