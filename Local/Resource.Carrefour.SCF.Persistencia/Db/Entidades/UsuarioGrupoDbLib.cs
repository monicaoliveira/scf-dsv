﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db.Oracle;

//using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class UsuarioGrupoDbLib : IEntidadeDbLib<UsuarioGrupoInfo>
    {
        #region IEntidadeDbLib<UsuarioGrupoInfo> Members

        #region ConsultarObjeto

        public ConsultarObjetosResponse<UsuarioGrupoInfo> ConsultarObjetos(ConsultarObjetosRequest<UsuarioGrupoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // NomeUsuario
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Nome");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_GRUPO_ACESSO", condicaoInfo.Valores[0]);

                // Numero maximo de resultados
                paramsProc.Add("maxLinhas", 100);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_GRUPO_ACESSO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<UsuarioGrupoInfo> resultado = new List<UsuarioGrupoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr, null, null));

            // Retorna
            return
                new ConsultarObjetosResponse<UsuarioGrupoInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region ReceberObjeto

        public ReceberObjetoResponse<UsuarioGrupoInfo> ReceberObjeto(ReceberObjetoRequest<UsuarioGrupoInfo> request)
        {
            // Inicializa
            bool preencherPerfis = true;
            bool preencherPermissoes = true;

            string pk = request.CodigoObjeto;
            if (pk != null && pk.Contains("-"))
                pk = null;
            else
                if (pk.Trim() == "" )
                    pk = null;


            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_GRUPO_ACESSO_B",
                        "pCODIGO_GRUPO_ACESSO", pk,
                        "retornarPermissoes", preencherPermissoes ? "S" : "N",
                        "retornarPerfisAcesso", preencherPerfis ? "S" : "N");

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbPermissoes = (DataTable)retorno["RETCURPERMISSOES"];
            DataTable tbPerfisAcesso = (DataTable)retorno["RETCURPERFISACESSO"];

            // Retorna
            return
                new ReceberObjetoResponse<UsuarioGrupoInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0], tbPermissoes, tbPerfisAcesso) :
                        null
                };
        }

        #endregion

        #region RemoverObjeto

        public RemoverObjetoResponse<UsuarioGrupoInfo> RemoverObjeto(RemoverObjetoRequest<UsuarioGrupoInfo> request)
        {
            ////1477 TRATANDO REMOVER
            //OracleDbLib2.Default.ExecutarProcedure("PR_GRUPO_ACESSO_R", "pCODIGO_GRUPO_ACESSO", request.CodigoObjeto);
            //return new RemoverObjetoResponse<UsuarioGrupoInfo>();
            RemoverObjetoResponse<UsuarioGrupoInfo> resposta = new RemoverObjetoResponse<UsuarioGrupoInfo>();
            try
            {

                OracleDbLib2.Default.ExecutarProcedure("PR_GRUPO_ACESSO_R", "pCODIGO_GRUPO_ACESSO", request.CodigoObjeto);
            }
            catch (Exception)
            {
                resposta.RepassarExcessao = false;
                resposta.DescricaoResposta = "FAVOR DELETAR TODOS OS CADASTROS DESTE GRUPO (USUARIOS/PERFIS/PERMISSOES) ANTES DE REMOVÊ-LO";
                resposta.Erro = resposta.DescricaoResposta;
                resposta.StatusResposta = Resource.Framework.Library.Servicos.Mensagens.MensagemResponseStatusEnum.ErroPrograma;
            }
            return resposta; 
        }

        #endregion

        #region SalvarObjeto

        public SalvarObjetoResponse<UsuarioGrupoInfo> SalvarObjeto(SalvarObjetoRequest<UsuarioGrupoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoUsuarioGrupo;
            if (pk != null && pk.Contains("-") || pk.Equals(string.Empty))
                pk = null;
            else
                if (pk.Trim() == "" )
                    pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_GRUPO_ACESSO", pk);
            paramsProc.Add("pNOME_GRUPO_ACESSO", request.Objeto.NomeUsuarioGrupo);
            paramsProc.Add("retornarRegistro", "S");
            paramsProc.Add("retornarPermissoes", "S");
            paramsProc.Add("retornarPerfisAcesso", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_GRUPO_ACESSO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbPermissoes = (DataTable)retorno["RETCURPERMISSOES"];
            DataTable tbPerfisAcesso = (DataTable)retorno["RETCURPERFISACESSO"];

            UsuarioGrupoInfo usuarioGrupoSalvo = this.MontarObjeto(tb.Rows[0], tbPermissoes, tbPerfisAcesso);

            // Salva permissões
            salvarPermissoes(request.Objeto, usuarioGrupoSalvo);

            // Salva perfis
            salvarPerfis(request.Objeto, usuarioGrupoSalvo);


            // Retorna
            return
                new SalvarObjetoResponse<UsuarioGrupoInfo>()
                {
                    Objeto = usuarioGrupoSalvo
                };
        }

        #endregion

        #endregion

        #region MontarObjeto

        public UsuarioGrupoInfo MontarObjeto(DataRow dr)
        {
            return this.MontarObjeto(dr, null, null);
        }

        #endregion

        #region MontarObjeto Sobrescrito

        public UsuarioGrupoInfo MontarObjeto(DataRow dr, DataTable tbPermissoes, DataTable tbPerfisAcesso)
        {
            // Cria o usuario
            UsuarioGrupoInfo usuarioGrupoInfo =
                new UsuarioGrupoInfo()
                {
                    CodigoUsuarioGrupo = dr["CODIGO_GRUPO_ACESSO"].ToString(),
                    NomeUsuarioGrupo = dr["NOME_GRUPO_ACESSO"].ToString()
                };


            // Se tem permissoes, preenche
            if (tbPermissoes != null)
                foreach (DataRow drPermissao in tbPermissoes.Rows)
                    usuarioGrupoInfo.Permissoes.Add(
                        new PermissaoAssociadaInfo()
                        {
                            CodigoPermissao = drPermissao["CODIGO_PERMISSAO"].ToString(),
                            Status = OracleDbLib2.EnumToObject<PermissaoAssociadaStatusEnum>(drPermissao["STATUS_PERMISSAO"])
                        });

            // Se tem perfis, preenche
            if (tbPerfisAcesso != null)
                foreach (DataRow drPerfilAcesso in tbPerfisAcesso.Rows)
                    usuarioGrupoInfo.Perfis.Add(drPerfilAcesso["CODIGO_PERFIL"].ToString());

            // Retorna
            return usuarioGrupoInfo;
        }

        #endregion

        #region Rotinas Locais

        /// <summary>
        /// Salva a lista de permissoes
        /// </summary>
        /// <param name="clienteOriginal"></param>
        /// <param name="clienteSalvo"></param>
        private void salvarPermissoes(UsuarioGrupoInfo usuarioGrupoOriginal, UsuarioGrupoInfo usuarioGrupoSalvo)
        {
            // Inicializa
            UsuarioGrupoPermissaoDbLib usuarioGrupoPermissaoDbLib = new UsuarioGrupoPermissaoDbLib();

            // Pega lista de permissoes atuais
            List<PermissaoAssociadaInfo> permissoesAtuais =
                usuarioGrupoPermissaoDbLib.ConsultarObjetos(
                    new List<CondicaoInfo>() 
                    { 
                        new CondicaoInfo("CodigoUsuarioGrupo", CondicaoTipoEnum.Igual, usuarioGrupoSalvo.CodigoUsuarioGrupo)
                    });

            // Varre a lista de que foi pedido para salvar
            foreach (PermissaoAssociadaInfo permissaoAssociada in usuarioGrupoOriginal.Permissoes)
                usuarioGrupoPermissaoDbLib.SalvarObjeto(permissaoAssociada, usuarioGrupoSalvo.CodigoUsuarioGrupo);

            // Verifica se existem permissoes a remover
            foreach (PermissaoAssociadaInfo permissaoAssociada in permissoesAtuais)
                if (usuarioGrupoOriginal.Permissoes.Find(p => p.CodigoPermissao == permissaoAssociada.CodigoPermissao) == null)
                    usuarioGrupoPermissaoDbLib.RemoverObjeto(usuarioGrupoSalvo.CodigoUsuarioGrupo, permissaoAssociada.CodigoPermissao);

            // Atribui a coleção ao cliente salvo
            usuarioGrupoSalvo.Permissoes = usuarioGrupoOriginal.Permissoes;
        }

        /// <summary>
        /// Salva a lista de perfis
        /// </summary>
        /// <param name="clienteOriginal"></param>
        /// <param name="clienteSalvo"></param>
        private void salvarPerfis(UsuarioGrupoInfo usuarioGrupoOriginal, UsuarioGrupoInfo usuarioGrupoSalvo)
        {
            // Inicializa
            UsuarioGrupoPerfilDbLib usuarioGrupoPerfilDbLib = new UsuarioGrupoPerfilDbLib();
            
            // Pega lista de grupos atuais
            List<string> perfisAtuais =
                usuarioGrupoPerfilDbLib.ConsultarObjetosPorGrupo(usuarioGrupoSalvo.CodigoUsuarioGrupo);

            // Varre a lista de que foi pedido para salvar
            foreach (string codigoPerfil in usuarioGrupoOriginal.Perfis)
                usuarioGrupoPerfilDbLib.SalvarObjeto(codigoPerfil, usuarioGrupoSalvo.CodigoUsuarioGrupo);

            // Verifica se existem grupos a remover
            foreach (string codigoPerfil in perfisAtuais)
                if (usuarioGrupoOriginal.Perfis.Find(p => p == codigoPerfil) == null)
                    usuarioGrupoPerfilDbLib.RemoverObjeto(usuarioGrupoSalvo.CodigoUsuarioGrupo, codigoPerfil);

            // Atribui a coleção ao cliente salvo
            usuarioGrupoSalvo.Perfis = usuarioGrupoOriginal.Perfis;
        }


        #endregion
    }
}
