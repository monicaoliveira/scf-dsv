﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para CriticaSCFResumoInfo
    /// </summary>
    public class CriticaSCFResumoDbLib : IEntidadeDbLib<CriticaSCFResumoInfo>
    {
        #region IEntidadeDbLib<CriticaSCFResumoInfo> Members

        /// <summary>
        /// Consulta de Criticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<CriticaSCFResumoInfo> ConsultarObjetos(ConsultarObjetosRequest<CriticaSCFResumoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Tipo do Critica
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoCritica");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_CRITICA", condicaoInfo.Valores[0]);

                // Codigo Arquivo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoArquivo");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_ARQUIVO", condicaoInfo.Valores[0]);

                // Codigo Arquivo Item
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoArquivoItem");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_ARQUIVO_ITEM", condicaoInfo.Valores[0]);

                // Codigo Processo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoProcesso");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_PROCESSO", condicaoInfo.Valores[0]);

                // Tipo Linha
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoLinha");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_LINHA", condicaoInfo.Valores[0]);

                // Nome Campo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeCampo");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_NOME_CAMPO", condicaoInfo.Valores[0]);

                // 1282 DESCRICAO CRITICA
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DescricaoCritica");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DESCRICAO_CRITICA", condicaoInfo.Valores[0]);

                // 1282 retorna agrupado
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "RetornaAgrupado");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_AGRUPADO", condicaoInfo.Valores[0]);
            }

            // Coloca o número máximo de linhas
            paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CRITICA_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<CriticaSCFResumoInfo> resultado = new List<CriticaSCFResumoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<CriticaSCFResumoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<CriticaSCFResumoInfo> ReceberObjeto(ReceberObjetoRequest<CriticaSCFResumoInfo> request)
        {
            throw new NotImplementedException("ReceberObjeto de CriticaSCFResumoDbLib não implementado");
        }

        /// <summary>
        /// Salva Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<CriticaSCFResumoInfo> SalvarObjeto(SalvarObjetoRequest<CriticaSCFResumoInfo> request)
        {
            throw new NotImplementedException("SalvarObjeto de CriticaSCFResumoDbLib não implementado");
        }

        /// <summary>
        /// Remove um Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<CriticaSCFResumoInfo> RemoverObjeto(RemoverObjetoRequest<CriticaSCFResumoInfo> request)
        {
            throw new NotImplementedException("RemoverObjeto de CriticaSCFResumoDbLib não implementado");
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public CriticaSCFResumoInfo MontarObjeto(System.Data.DataRow dr)
        {
            // Preenche com as informações do banco
            return new CriticaSCFResumoInfo() 
            { 
                CodigoCritica = dr["CODIGO_CRITICA"].ToString(),
                DataCritica = dr["DATA_CRITICA"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_CRITICA"]) : null,
                DescricaoCritica = dr["DESCRICAO_CRITICA"] != DBNull.Value ? (string)dr["DESCRICAO_CRITICA"] : null,
                TipoCritica = dr["TIPO_CRITICA"] != DBNull.Value ? (string)dr["TIPO_CRITICA"] : null,
                CodigoArquivo = dr["CODIGO_ARQUIVO"] != DBNull.Value ? dr["CODIGO_ARQUIVO"].ToString() : null,
                CodigoArquivoItem = dr["CODIGO_ARQUIVO_ITEM"] != DBNull.Value ? dr["CODIGO_ARQUIVO_ITEM"].ToString() : null,
                CodigoProcesso = dr["CODIGO_PROCESSO"] != DBNull.Value ? dr["CODIGO_PROCESSO"].ToString() : null,
                NomeCampo = dr["NOME_CAMPO"] != DBNull.Value ? dr["NOME_CAMPO"].ToString() : null,
                TipoLinha = dr["TIPO_LINHA"] != DBNull.Value ? dr["TIPO_LINHA"].ToString() : null
            };
        }

        #endregion
    }
}
