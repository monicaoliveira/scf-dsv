﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
//====================================================================================================================================00
namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    //====================================================================================================================================01    
    public class ContaFavorecidoDbLib : IEntidadeDbLib<ContaFavorecidoInfo>
    {
        //====================================================================================================================================02
        #region IEntidadeDbLib<ContaFavorecidoInfo> Members
        //====================================================================================================================================03        
        public ConsultarObjetosResponse<ContaFavorecidoInfo> ConsultarObjetos(ConsultarObjetosRequest<ContaFavorecidoInfo> request)
        {
            //================================================================================================================================
            // Monta lista de parametros
            //================================================================================================================================
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;
            //================================================================================================================================
            // Apenas se tem coleção de condições criada
            //================================================================================================================================
            if (request.Condicoes != null)
            {
                //================================================================================================================================
                // Código favorecido
                //================================================================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_FAVORECIDO", condicaoInfo.Valores[0]);
                //================================================================================================================================
                // Banco
                //================================================================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Banco");
                if (condicaoInfo != null)
                    paramsProc.Add("pBANCO", condicaoInfo.Valores[0]);
                //================================================================================================================================
                // Agencia
                //================================================================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Agencia");
                if (condicaoInfo != null)
                    paramsProc.Add("pAGENCIA", condicaoInfo.Valores[0]);
                //================================================================================================================================
                // Conta
                //================================================================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Conta");
                if (condicaoInfo != null)
                    paramsProc.Add("pCONTA", condicaoInfo.Valores[0]);
                //================================================================================================================================
                // Agencia CCI
                //================================================================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Agencia_CCI");
                if (condicaoInfo != null)
                    paramsProc.Add("pAGENCIA_CCI", condicaoInfo.Valores[0]);
                //================================================================================================================================
                // Conta CCI
                //================================================================================================================================
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Conta_CCI");
                if (condicaoInfo != null)
                    paramsProc.Add("pCONTA_CCI", condicaoInfo.Valores[0]);
            }
            //================================================================================================================================                
            // Monta a execução da procedure e executa
            //================================================================================================================================                
            DataTable tb = (DataTable)OracleDbLib2.Default.ExecutarProcedure("PR_CONTA_FAVORECIDO_L", paramsProc)["RETCUR"];
            //================================================================================================================================                
            // Preenche a coleção resultado
            //================================================================================================================================                
            List<ContaFavorecidoInfo> resultado = new List<ContaFavorecidoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));
            //================================================================================================================================                
            // Retorna
            //================================================================================================================================                
            return
                new ConsultarObjetosResponse<ContaFavorecidoInfo>()
                {
                    Resultado = resultado
                };
        }
        //====================================================================================================================================03

        //====================================================================================================================================4
        public ReceberObjetoResponse<ContaFavorecidoInfo> ReceberObjeto(ReceberObjetoRequest<ContaFavorecidoInfo> request)
        {
            //================================================================================================================================
            // Faz a consulta no banco
            //================================================================================================================================
            DataTable tb = (DataTable)  OracleDbLib2.Default.ExecutarProcedure("PR_CONTA_FAVORECIDO_B", "pCODIGO_CONTA_FAVORECIDO", request.CodigoObjeto)["RETCUR"];
            //================================================================================================================================
            // Retorna
            //================================================================================================================================
            return
                new ReceberObjetoResponse<ContaFavorecidoInfo>()
                {   Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }
        //====================================================================================================================================4
        //====================================================================================================================================5
        public SalvarObjetoResponse<ContaFavorecidoInfo> SalvarObjeto(SalvarObjetoRequest<ContaFavorecidoInfo> request)
        {
            //===================================================================================================                
            // Verifica se é novo ou alteração
            //===================================================================================================                
            string pk = request.Objeto.CodigoContaFavorecido;
            if (pk != null && pk.Contains("-"))
                pk = null;
            //===================================================================================================                
            // Monta parametros
            //===================================================================================================                
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_CONTA_FAVORECIDO", pk);
            paramsProc.Add("pCODIGO_FAVORECIDO", request.Objeto.CodigoFavorecido); 
            paramsProc.Add("pCONTA", request.Objeto.Conta);
            paramsProc.Add("pAGENCIA", request.Objeto.Agencia);
            paramsProc.Add("pBANCO", request.Objeto.Banco);
            paramsProc.Add("pAGENCIA_CCI", request.Objeto.Agencia_CCI); //978
            paramsProc.Add("pCONTA_CCI", request.Objeto.Conta_CCI); //978
            paramsProc.Add("retornarRegistro", "S");
            //===================================================================================================                
            // Execução a procedure
            //===================================================================================================                
            DataTable tb = (DataTable) OracleDbLib2.Default.ExecutarProcedure("PR_CONTA_FAVORECIDO_S", paramsProc)["RETCUR"];
            //===================================================================================================                
            // Retorna
            //===================================================================================================                
            return
                new SalvarObjetoResponse<ContaFavorecidoInfo>()
                {   Objeto = this.MontarObjeto(tb.Rows[0]) 
                };
        }
        //====================================================================================================================================5
        //====================================================================================================================================6
        public RemoverObjetoResponse<ContaFavorecidoInfo> RemoverObjeto(RemoverObjetoRequest<ContaFavorecidoInfo> request)
        {
            //================================================================================================================================
            // Monta a execução da procedure
            //================================================================================================================================
            OracleDbLib2.Default.ExecutarProcedure("PR_CONTA_FAVORECIDO_R","pCODIGO_CONTA_FAVORECIDO", request.CodigoObjeto);
            //================================================================================================================================
            // Retorna
            //================================================================================================================================
            return new RemoverObjetoResponse<ContaFavorecidoInfo>();
        }
        //====================================================================================================================================6
        //====================================================================================================================================6
        //SCF1701 - ID 27
        public SubstituirObjetoResponse<ContaFavorecidoInfo> SubstituirObjeto(SubstituirObjetoRequest<ContaFavorecidoInfo> request)
        {
            //================================================================================================================================
            // Monta a execução da procedure
            //================================================================================================================================
            OracleDbLib2.Default.ExecutarProcedure
                (
                    "PR_CONTA_FAVORECIDO_U", 
                    "pCODIGO_CONTA_FAVORECIDO", 
                    request.CodigoContaFavorecido, 
                    "pCODIGO_CONTA_FAVORECIDO_NOVO", 
                    request.CodigoContaFavorecidoNovo,
                    "pCODIGO_PROCESSO",
                    request.CodigoProcesso
                 );
            //================================================================================================================================
            // Retorna
            //================================================================================================================================
            return new SubstituirObjetoResponse<ContaFavorecidoInfo>();
        }
        //====================================================================================================================================6
        //====================================================================================================================================7
        public ContaFavorecidoInfo MontarObjeto(DataRow dr)
        {
            //================================================================================================================================
            // Retorna
            //================================================================================================================================
            return
                new ContaFavorecidoInfo()
                {                    
                    CodigoContaFavorecido = dr["CODIGO_CONTA_FAVORECIDO"].ToString()
                ,   CodigoFavorecido = dr["CODIGO_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_FAVORECIDO"].ToString() : null
                ,   Conta = dr["CONTA"] != DBNull.Value ? dr["CONTA"].ToString() : null
                ,   Agencia = dr["AGENCIA"] != DBNull.Value ? dr["AGENCIA"].ToString() : null
                ,   Banco = dr["BANCO"] != DBNull.Value ? dr["BANCO"].ToString() : null
                ,   Agencia_CCI = dr["AGENCIA_CCI"] != DBNull.Value ? dr["AGENCIA_CCI"].ToString() : null //978
                ,   Conta_CCI = dr["CONTA_CCI"] != DBNull.Value ? dr["CONTA_CCI"].ToString() : null //978
                ,   Transacoes  = dr["TRANSACOES"].ToString()   //1339
                };
        }
        //====================================================================================================================================7
        #endregion
        //====================================================================================================================================02
    }
    //====================================================================================================================================01    
}
//====================================================================================================================================00
