﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para CriticaInfo
    /// </summary>
    public class CriticaDbLib : IEntidadeDbLib<CriticaInfo>
    {
        #region IEntidadeDbLib<CriticaInfo> Members

        /// <summary>
        /// Consulta de Criticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<CriticaInfo> ConsultarObjetos(ConsultarObjetosRequest<CriticaInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Tipo do Critica
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoCritica");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_CRITICA", condicaoInfo.Valores[0]);
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoArquivoItem");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_ARQUIVO_ITEM", condicaoInfo.Valores[0]);
            }

            // Coloca o número máximo de linhas
            paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CRITICA_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<CriticaInfo> resultado = new List<CriticaInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<CriticaInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<CriticaInfo> ReceberObjeto(ReceberObjetoRequest<CriticaInfo> request)
        {
            // Faz a consulta no banco
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CRITICA_B", "pCODIGO_CRITICA", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<CriticaInfo>()
                {
                    Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0]) : null
                };
        }

        /// <summary>
        /// Salva Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<CriticaInfo> SalvarObjeto(SalvarObjetoRequest<CriticaInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoCritica;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Serializa o objeto
            MemoryStream ms = new MemoryStream();
            new XmlSerializer(request.Objeto.GetType()).Serialize(ms, request.Objeto);
            ms.Position = 0;
            StreamReader reader = new StreamReader(ms);
            string serializacaoObjeto = reader.ReadToEnd();
            reader.Close();

            // Propriedades que dependem do tipo do objeto
            string codigoProcesso = null;
            string codigoArquivo = null;
            string codigoArquivoItem = null;

            // Verifica se tem informacoes de arquivo, arquivoItem e processo
            if (request.Objeto.GetType().IsSubclassOf(typeof(CriticaSCFArquivoInfo)))
            {
                CriticaSCFArquivoInfo criticaSCFArquivo = (CriticaSCFArquivoInfo)request.Objeto;
                codigoProcesso = criticaSCFArquivo.CodigoProcesso;
                codigoArquivo = criticaSCFArquivo.CodigoArquivo;
            }
            if (request.Objeto.GetType().IsSubclassOf(typeof(CriticaSCFArquivoItemInfo)))
                codigoArquivoItem = ((CriticaSCFArquivoItemInfo)request.Objeto).CodigoArquivoItem;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_CRITICA", pk);
            paramsProc.Add("pTIPO_CRITICA", request.Objeto.GetType().Name);
            paramsProc.Add("pCONTEUDO_CRITICA", serializacaoObjeto);
            paramsProc.Add("pCODIGO_PROCESSO", codigoProcesso);
            paramsProc.Add("pCODIGO_ARQUIVO", codigoArquivo);
            paramsProc.Add("pCODIGO_ARQUIVO_ITEM", codigoArquivoItem);
            paramsProc.Add("pDATA_CRITICA", request.Objeto.DataCritica);
            paramsProc.Add("pDESCRICAO_CRITICA", request.Objeto.Descricao);
            paramsProc.Add("retornarRegistro", "S");

            // Executa a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_CRITICA_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<CriticaInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        /// <summary>
        /// Remove um Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<CriticaInfo> RemoverObjeto(RemoverObjetoRequest<CriticaInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_CRITICA_R",
                "pCODIGO_CRITICA", request.CodigoObjeto);

            // Retorna
            return
                new RemoverObjetoResponse<CriticaInfo>()
                {
                    CodigoMensagemRequest = request.CodigoMensagem
                };
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public CriticaInfo MontarObjeto(System.Data.DataRow dr)
        {
            // Resolve o tipo
            Type tipoCritica = ResolutorTipos.Resolver((string)dr["TIPO_CRITICA"]);

            // Desserializa o log
            CriticaInfo criticaInfo =
                (CriticaInfo)
                    new XmlSerializer(tipoCritica).Deserialize(
                        new MemoryStream(
                            ASCIIEncoding.UTF8.GetBytes((string)dr["CONTEUDO_CRITICA"])));

            // Preenche com as informações do banco
            criticaInfo.CodigoCritica = dr["CODIGO_CRITICA"].ToString();
            criticaInfo.DataCritica = (DateTime)dr["DATA_CRITICA"];
            criticaInfo.Descricao = dr["DESCRICAO_CRITICA"] != DBNull.Value ? (string)dr["DESCRICAO_CRITICA"] : null;

            // Retorna
            return criticaInfo;
        }

        #endregion
    }
}
