﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para CriticaAgrupamentoDbLib
    /// </summary>
    public class CriticaAgrupamentoDbLib : IEntidadeDbLib<CriticaAgrupamentoInfo>
    {
        #region IEntidadeDbLib<CriticaSCFResumoInfo> Members

        /// <summary>
        /// Consulta de Criticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<CriticaAgrupamentoInfo> ConsultarObjetos(ConsultarObjetosRequest<CriticaAgrupamentoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Tipo do Critica
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoProcesso");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PROCESSO", condicaoInfo.Valores[0]);

                // Codigo Arquivo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoArquivo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_ARQUIVO", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CRITICA_AGRUPAMENTO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<CriticaAgrupamentoInfo> resultado = new List<CriticaAgrupamentoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<CriticaAgrupamentoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<CriticaAgrupamentoInfo> ReceberObjeto(ReceberObjetoRequest<CriticaAgrupamentoInfo> request)
        {
            throw new NotImplementedException("ReceberObjeto de CriticaAgrupamentoInfo não implementado");
        }

        /// <summary>
        /// Salva Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<CriticaAgrupamentoInfo> SalvarObjeto(SalvarObjetoRequest<CriticaAgrupamentoInfo> request)
        {
            throw new NotImplementedException("SalvarObjeto de CriticaAgrupamentoInfo não implementado");
        }

        /// <summary>
        /// Remove um Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<CriticaAgrupamentoInfo> RemoverObjeto(RemoverObjetoRequest<CriticaAgrupamentoInfo> request)
        {
            throw new NotImplementedException("RemoverObjeto de CriticaAgrupamentoInfo não implementado");
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public CriticaAgrupamentoInfo MontarObjeto(System.Data.DataRow dr)
        {
            // Preenche com as informações do banco
            CriticaAgrupamentoInfo criticaAgrupamentoInfo = 
                new CriticaAgrupamentoInfo()
                {
                    CodigoArquivo = dr["CODIGO_ARQUIVO"] != DBNull.Value ? dr["CODIGO_ARQUIVO"].ToString() : null,
                    TipoCritica = dr["TIPO_CRITICA"] != DBNull.Value ? (string)dr["TIPO_CRITICA"] : null,
                    TipoLinha = dr["TIPO_LINHA"] != DBNull.Value ? (string)dr["TIPO_LINHA"] : null,
                    NomeCampo = dr["NOME_CAMPO"] != DBNull.Value ? (string)dr["NOME_CAMPO"] : null,
                    Quantidade = dr["QUANTIDADE"] != DBNull.Value ? Convert.ToInt32(dr["QUANTIDADE"]) : 0,
                    Nivel = SqlDbLib.EnumToObject<CriticaAgrupamentoNivelEnum>(dr["NIVEL"])
                };

            // Retorna
            return criticaAgrupamentoInfo;
        }

        #endregion
    }
}
