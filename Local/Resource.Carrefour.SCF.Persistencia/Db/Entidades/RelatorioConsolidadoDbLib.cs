﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia de Arquivo
    /// </summary>
    public class RelatorioConsolidadoDbLib : IEntidadeDbLib<RelatorioConsolidadoInfo>
    {
        #region IEntidadeDbLib<ArquivoInfo> Members

        /// <summary>
        /// Recebe arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<RelatorioConsolidadoInfo> ReceberObjeto(ReceberObjetoRequest<RelatorioConsolidadoInfo> request)
        {
            return null;
        }

        public SalvarObjetoResponse<RelatorioConsolidadoInfo> SalvarObjeto(SalvarObjetoRequest<RelatorioConsolidadoInfo> request)
        {
            return null;
        }

        public ConsultarObjetosResponse<RelatorioConsolidadoInfo> ConsultarObjetos(ConsultarObjetosRequest<RelatorioConsolidadoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;
            //1353 string codLog = "";

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Tipo do Relatorio - Venda / Recebimento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroTipoConsolidado");
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_CONSOLIDADO", condicaoInfo.Valores[0]);

                // Quebra por favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroQuebraFavorecido" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pSUB_TOTAL_FAVORECIDO", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // Tipo do Produto
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroProduto" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PRODUTO", condicaoInfo.Valores[0]);

                // Tipo do Favorecido original
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroFavorecidoOriginal" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFAVORECIDO_ORIGINAL", condicaoInfo.Valores[0]);

                // Tipo do Registro
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroTipoRegistro" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_TIPO_REGISTRO", condicaoInfo.Valores[0].ToString().TrimStart().Replace(" ","','"));

                // Tipo do Estabelecimento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEstabelecimento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_ESTABELECIMENTO", condicaoInfo.Valores[0]);

                // Tipo do Meio de Pagamento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroMeioPagamento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_MEIO_PAGAMENTO", condicaoInfo.Valores[0]);

                // Tipo do Layout de Venda
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroLayoutVenda" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pLAYOUT_VENDA", condicaoInfo.Valores[0]);

                // Tipo do Layout de Recebimento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroLayoutRecebimento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pLAYOUT_RECEBIMENTO", condicaoInfo.Valores[0]);

                // Quantidade de Venda
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroQtdeVenda" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pQTDE_TRANS_VENDA", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // Quantidade Recebimento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroQtdeRecebimento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pQTDE_PAG_RECEBIMENTO", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // Data Agendamento - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataAgendamentoDe"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_AGENDAMENTO", condicaoInfo.Valores[0]);

                // Data Agendamento - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataAgendamentoAte"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_AGENDAMENTO", condicaoInfo.Valores[0]);

                // Data Movimento - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataInicioMovimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_MOVIMENTO", condicaoInfo.Valores[0]);

                // Data Movimento - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataFimMovimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_MOVIMENTO", condicaoInfo.Valores[0]);

                // Codigo Status Cessao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroCodigoStatusCCI" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pSTATUS_RETORNO_CESSAO", condicaoInfo.Valores[0]);

                //1353 
                // FILTRO DESNECESSÁRIO, NÃO EXISTE ESTE CAMPO A SER FILTRADO NO RELATORIO CONSOLIDADO [ codlog ]
                // retirado dos parametros da procedure
                // CodLog
                //condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodLog");
                //if (condicaoInfo != null)
                //{
                //    codLog = condicaoInfo.Valores[0].ToString();
                //    paramsProc.Add("pCOD_LOG", condicaoInfo.Valores[0]);
                //}

                // ECOMMERCE - Fernando Bove - 20160105
                // tipo financeiro contábil
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroFinanceiroContabil" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_FINANCEIRO_CONTABIL", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroReferencia" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pREFERENCIA", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroQuebraReferencia" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_REFERENCIA", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_REL_TRANS_CONSOLIDADO_L1", paramsProc)["RETCUR"];

            if (request is ListarRelatorioConsolidadoDbRequest)
            {
                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //new SalvarLogRequest()
                //{
                //    CodigoUsuario = "",
                //    Descricao = "APP - INF - ListarRelatorioConsolidado - Contagem do Objeto.",
                //    TipoOrigem = "APP",
                //    CodigoOrigem = codLog
                //});

                return
                    new ListarRelatorioConsolidadoDbResponse()
                    {
                        QuantidadeLinhas = tb.Rows.Count
                    };
            }

            //1353
            //Mensageria.Processar<SalvarLogResponse>(
            //    new SalvarLogRequest()
            //    {
            //        CodigoUsuario = "",
            //        Descricao = "APP - INI - ListarRelatorioConsolidado - Iniciando montagem do Objeto.",
            //        TipoOrigem = "APP",
            //        CodigoOrigem = codLog
            //    });

            // Preenche a coleção resultado
            List<RelatorioConsolidadoInfo> resultado = new List<RelatorioConsolidadoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            //1353
            //Mensageria.Processar<SalvarLogResponse>(
            //    new SalvarLogRequest()
            //    {
            //        CodigoUsuario = "",
            //        Descricao = "APP - FIM - ListarRelatorioConsolidado - Fim da montagem do Objeto.",
            //        TipoOrigem = "APP",
            //        CodigoOrigem = codLog
            //    });

            // Retorna
            return
                new ConsultarObjetosResponse<RelatorioConsolidadoInfo>()
                {
                    Resultado = resultado
                };
        }

        public RemoverObjetoResponse<RelatorioConsolidadoInfo> RemoverObjeto(RemoverObjetoRequest<RelatorioConsolidadoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Monta arquivo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public RelatorioConsolidadoInfo MontarObjeto(System.Data.DataRow dr)
        {
            // Retorna
            return
                new RelatorioConsolidadoInfo()
                {
                    ValorBrutoCV = dr.Table.Columns.Contains("VALOR_BRUTO_CV") ? dr["VALOR_BRUTO_CV"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_BRUTO_CV"]) : 0 : 0,
                    ValorBrutoAV = dr.Table.Columns.Contains("VALOR_BRUTO_AV") ? dr["VALOR_BRUTO_AV"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_BRUTO_AV"]) : 0 : 0,
                    ValorBrutoAJ = dr.Table.Columns.Contains("VALOR_BRUTO_AJ") ? dr["VALOR_BRUTO_AJ"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_BRUTO_AJ"]) : 0 : 0,
                    ValorComissaoCV = dr.Table.Columns.Contains("VALOR_COMISSAO_CV") ? dr["VALOR_COMISSAO_CV"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_COMISSAO_CV"]) : 0 : 0,
                    ValorComissaoAJ = dr.Table.Columns.Contains("VALOR_COMISSAO_AJ") ? dr["VALOR_COMISSAO_AJ"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_COMISSAO_AJ"]) : 0 : 0,
                    ValorComissaoAV = dr.Table.Columns.Contains("VALOR_COMISSAO_AV") ? dr["VALOR_COMISSAO_AV"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_COMISSAO_AV"]) : 0 : 0,
                    TotalLiquido = dr.Table.Columns.Contains("TOTAL_LIQUIDO") ? dr["TOTAL_LIQUIDO"] != DBNull.Value ? Convert.ToDecimal(dr["TOTAL_LIQUIDO"]) : 0 : 0,
                    QtdeCV = dr.Table.Columns.Contains("QTDE_CV") ? dr["QTDE_CV"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_CV"]) : 0 : 0,
                    QtdeAV = dr.Table.Columns.Contains("QTDE_AV") ? dr["QTDE_AV"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_AV"]) : 0 : 0,
                    QtdeAJ = dr.Table.Columns.Contains("QTDE_AJ") ? dr["QTDE_AJ"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_AJ"]) : 0 : 0,
                    CNPJ = dr.Table.Columns.Contains("CNPJ") ? dr["CNPJ"] != DBNull.Value ? dr["CNPJ"].ToString() : null : null,
                    NomeFavorecido = dr.Table.Columns.Contains("NOME_FAVORECIDO") ? dr["NOME_FAVORECIDO"] != DBNull.Value ? dr["NOME_FAVORECIDO"].ToString() : null : null,
                    NomeProduto = dr.Table.Columns.Contains("NOME_PRODUTO") ? dr["NOME_PRODUTO"] != DBNull.Value ? dr["NOME_PRODUTO"].ToString() : null : null,
                    ValorCP = dr.Table.Columns.Contains("VALOR_CP") ? dr["VALOR_CP"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_CP"]) : 0 : 0,
                    ValorAP = dr.Table.Columns.Contains("VALOR_AP") ? dr["VALOR_AP"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_AP"]) : 0 : 0,
                    ValorAJ = dr.Table.Columns.Contains("VALOR_AJ") ? dr["VALOR_AJ"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_AJ"]) : 0 : 0,
                    ValorTotal = dr.Table.Columns.Contains("VALOR_TOTAL") ? dr["VALOR_TOTAL"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_TOTAL"]) : 0 : 0,
                    QtdeCP = dr.Table.Columns.Contains("QTDE_CP") ? dr["QTDE_CP"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_CP"]) : 0 : 0,
                    QtdeAP = dr.Table.Columns.Contains("QTDE_AP") ? dr["QTDE_AP"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_AP"]) : 0 : 0,
                    MeioPagamento = dr.Table.Columns.Contains("MEIO_PAGAMENTO") ? dr["MEIO_PAGAMENTO"] != DBNull.Value ? dr["MEIO_PAGAMENTO"].ToString() : null : null,
                    DataProcessamento = dr.Table.Columns.Contains("DATA_MOVIMENTO") ? dr["DATA_MOVIMENTO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_MOVIMENTO"]) : null : null,
                    CodigoReferencia = dr.Table.Columns.Contains("REFERENCIA") ? dr["REFERENCIA"] != DBNull.Value ? dr["REFERENCIA"].ToString() : null : null,
                    NomeReferencia = dr.Table.Columns.Contains("NOME_LISTA_ITEM") ? dr["NOME_LISTA_ITEM"] != DBNull.Value ? dr["NOME_LISTA_ITEM"].ToString() : null : null
                };
        }

        #endregion
    }
}
