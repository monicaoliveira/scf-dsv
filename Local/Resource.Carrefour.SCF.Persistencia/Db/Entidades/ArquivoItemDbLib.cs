﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db;
using Resource.Framework.Library.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para ArquivoItemInfo
    /// </summary>
    public class ArquivoItemDbLib : IEntidadeDbLib<ArquivoItemInfo>
    {
        #region IEntidadeDbLib<ArquivoItemInfo> Members

        /// <summary>
        /// Consulta de Criticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<ArquivoItemInfo> ConsultarObjetos(ConsultarObjetosRequest<ArquivoItemInfo> request)
        {
            throw new NotImplementedException("ConsultarObjetos de ArquivoItemDbLib não implementado");
        }

        /// <summary>
        /// Detalhe de Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<ArquivoItemInfo> ReceberObjeto(ReceberObjetoRequest<ArquivoItemInfo> request)
        {
            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_ARQUIVO_ITEM_B", 
                        "pCODIGO_ARQUIVO_ITEM", request.CodigoObjeto)["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<ArquivoItemInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        /// <summary>
        /// Salva Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<ArquivoItemInfo> SalvarObjeto(SalvarObjetoRequest<ArquivoItemInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoArquivoItem;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_ARQUIVO_ITEM", pk);
            paramsProc.Add("pCODIGO_ARQUIVO", request.Objeto.CodigoArquivo);
            paramsProc.Add("pCONTEUDO_ARQUIVO_ITEM", request.Objeto.ConteudoArquivoItem);
            paramsProc.Add("pTIPO_ARQUIVO_ITEM", request.Objeto.TipoArquivoItem);
            paramsProc.Add("pSTATUS_ARQUIVO_ITEM", SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(request.Objeto.StatusArquivoItem));
            paramsProc.Add("pREFERENCIA", request.Objeto.ReferenciaBinArquivoItem);
            paramsProc.Add("retornarRegistro", "S");

            // Executa a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_ARQUIVO_ITEM_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<ArquivoItemInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        /// <summary>
        /// Remove um Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<ArquivoItemInfo> RemoverObjeto(RemoverObjetoRequest<ArquivoItemInfo> request)
        {
            throw new NotImplementedException("RemoverObjeto de ArquivoItemDbLib não implementado");
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ArquivoItemInfo MontarObjeto(DataRow dr)
        {
            // Preenche com as informações do banco
            return 
                new ArquivoItemInfo()
                {
                    CodigoArquivo = dr["CODIGO_ARQUIVO"] != DBNull.Value ? dr["CODIGO_ARQUIVO"].ToString() : null,
                    CodigoArquivoItem = dr["CODIGO_ARQUIVO_ITEM"] != DBNull.Value ? dr["CODIGO_ARQUIVO_ITEM"].ToString() : null,
                    TipoArquivoItem = dr["TIPO_ARQUIVO_ITEM"] != DBNull.Value ? (string)dr["TIPO_ARQUIVO_ITEM"] : null,
                    ConteudoArquivoItem = dr["CONTEUDO_ARQUIVO_ITEM"] != DBNull.Value ? (string)dr["CONTEUDO_ARQUIVO_ITEM"] : null,
                    StatusArquivoItem = dr["STATUS_ARQUIVO_ITEM"] != DBNull.Value ? SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(dr["STATUS_ARQUIVO_ITEM"]) : ArquivoItemStatusEnum.PendenteValidacao
                };
        }

        #endregion
    }
}
