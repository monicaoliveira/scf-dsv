﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia do relatório de saldo de conciliação contábil
    /// </summary>
    public class RelatorioConciliacaoContabilDb : IEntidadeDbLib<RelatorioConciliacaoContabilInfo>
    {
        #region IEntidadeDbLib<RelatorioConciliacaoContabilInfo> Members

        /// <summary>
        /// Listar relatório de saldo de conciliação contábil
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<RelatorioConciliacaoContabilInfo> ConsultarObjetos(ConsultarObjetosRequest<RelatorioConciliacaoContabilInfo> request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Não implementado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<RelatorioConciliacaoContabilInfo> ReceberObjeto(ReceberObjetoRequest<RelatorioConciliacaoContabilInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Não implementado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<RelatorioConciliacaoContabilInfo> SalvarObjeto(SalvarObjetoRequest<RelatorioConciliacaoContabilInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Não implementado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<RelatorioConciliacaoContabilInfo> RemoverObjeto(RemoverObjetoRequest<RelatorioConciliacaoContabilInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Monta o objeto de retorno do relatório de saldo de conciliação contábil
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public RelatorioConciliacaoContabilInfo MontarObjeto(DataRow dr)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
