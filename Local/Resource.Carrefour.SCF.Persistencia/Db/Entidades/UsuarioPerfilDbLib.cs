﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    public class UsuarioPerfilDbLib : IEntidadeDbLib<PerfilInfo>
    {
        #region IEntidadeDbLib<PerfilInfo> Members

        #region ConsultarObjeto

        public ConsultarObjetosResponse<PerfilInfo> ConsultarObjetos(ConsultarObjetosRequest<PerfilInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // NomePerfil
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomePerfil");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PERFIL", condicaoInfo.Valores[0]);

                // Numero maximo de resultados
                paramsProc.Add("maxLinhas", 100);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PERFIL_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<PerfilInfo> resultado = new List<PerfilInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr, null));

            // Retorna
            return
                new ConsultarObjetosResponse<PerfilInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region ReceberObjeto

        public ReceberObjetoResponse<PerfilInfo> ReceberObjeto(ReceberObjetoRequest<PerfilInfo> request)
        {
            // Inicializa
            bool preencherPermissoes = true;

            string pk = request.CodigoObjeto;
            if ((pk != null && pk.Contains("-")) || pk.Equals(string.Empty))
                pk = null;

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PERFIL_B",
                        "pCODIGO_PERFIL", pk,
                        "retornarPermissoes", preencherPermissoes ? "S" : "N");

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbPermissoes = (DataTable)retorno["RETCURPERMISSOES"];

            // Retorna
            return
                new ReceberObjetoResponse<PerfilInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0], tbPermissoes) :
                        null
                };
        }

        #endregion

        #region RemoverObjeto

        public RemoverObjetoResponse<PerfilInfo> RemoverObjeto(RemoverObjetoRequest<PerfilInfo> request)
        {   ////1477 TRATANDO REMOVER
            //OracleDbLib2.Default.ExecutarProcedure("PR_PERFIL_R","pCODIGO_PERFIL", request.CodigoObjeto);
            //return new RemoverObjetoResponse<PerfilInfo>();
            RemoverObjetoResponse<PerfilInfo> resposta = new RemoverObjetoResponse<PerfilInfo>();
            try
            {

                OracleDbLib2.Default.ExecutarProcedure("PR_PERFIL_R", "pCODIGO_PERFIL", request.CodigoObjeto);
            }
            catch (Exception)
            {
                resposta.RepassarExcessao = false;
                resposta.DescricaoResposta = "FAVOR DELETAR TODOS OS CADASTROS DESTE PERFIL (USUARIOS/GRUPOS/PERMISSOES) ANTES DE REMOVÊ-LO";
                resposta.Erro = resposta.DescricaoResposta;
                resposta.StatusResposta = Resource.Framework.Library.Servicos.Mensagens.MensagemResponseStatusEnum.ErroPrograma;
            }
            return resposta; 
        }

        #endregion

        #region SalvarObjeto

        public SalvarObjetoResponse<PerfilInfo> SalvarObjeto(SalvarObjetoRequest<PerfilInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoPerfil;
            if (pk != null && pk.Contains("-"))
                pk = null;

            if (pk != null && pk.Trim() == "")
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_PERFIL", pk);
            paramsProc.Add("pNOME_PERFIL", request.Objeto.NomePerfil);
            paramsProc.Add("retornarRegistro", "S");
            paramsProc.Add("retornarPermissoes", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_PERFIL_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];
            DataTable tbPermissoes = (DataTable)retorno["RETCURPERMISSOES"];

            PerfilInfo perfilSalvo = this.MontarObjeto(tb.Rows[0], tbPermissoes);

            // Salva permissões
            salvarPermissoes(request.Objeto, perfilSalvo);

            // Retorna
            return
                new SalvarObjetoResponse<PerfilInfo>()
                {
                    Objeto = perfilSalvo
                };
        }

        #endregion

        #endregion

        #region MontarObjeto

        public PerfilInfo MontarObjeto(DataRow dr)
        {
            return this.MontarObjeto(dr, null);
        }

        #endregion

        #region MontarObjeto Sobrescrito

        public PerfilInfo MontarObjeto(DataRow dr, DataTable tbPermissoes)
        {
            // Cria o usuario
            PerfilInfo perfilInfo =
                new PerfilInfo()
                {
                    CodigoPerfil = dr["CODIGO_PERFIL"].ToString(),
                    NomePerfil= dr["NOME_PERFIL"].ToString()
                };


            // Se tem permissoes, preenche
            if (tbPermissoes != null)
                foreach (DataRow drPermissao in tbPermissoes.Rows)
                    perfilInfo.Permissoes.Add(
                        new PermissaoAssociadaInfo()
                        {
                            CodigoPermissao = drPermissao["CODIGO_PERMISSAO"].ToString(),
                            Status = OracleDbLib2.EnumToObject<PermissaoAssociadaStatusEnum>(drPermissao["STATUS_PERMISSAO"])
                        });

            // Retorna
            return perfilInfo;
        }

        #endregion

        #region Rotinas Locais

        /// <summary>
        /// Salva a lista de permissoes
        /// </summary>
        /// <param name="clienteOriginal"></param>
        /// <param name="clienteSalvo"></param>
        private void salvarPermissoes(PerfilInfo perfilOriginal, PerfilInfo perfilSalvo)
        {
            // Inicializa
            UsuarioPerfilPermissaoDbLib usuarioPerfilPermissaoDbLib = new UsuarioPerfilPermissaoDbLib();

            // Pega lista de permissoes atuais
            List<PermissaoAssociadaInfo> permissoesAtuais =
                usuarioPerfilPermissaoDbLib.ConsultarObjetos(
                    new List<CondicaoInfo>() 
                    { 
                        new CondicaoInfo("CodigoPerfil", CondicaoTipoEnum.Igual, perfilSalvo.CodigoPerfil)
                    });

            // Varre a lista de que foi pedido para salvar
            foreach (PermissaoAssociadaInfo permissaoAssociada in perfilOriginal.Permissoes)
                usuarioPerfilPermissaoDbLib.SalvarObjeto(permissaoAssociada, perfilSalvo.CodigoPerfil);

            // Verifica se existem permissoes a remover
            foreach (PermissaoAssociadaInfo permissaoAssociada in permissoesAtuais)
                if (perfilOriginal.Permissoes.Find(p => p.CodigoPermissao == permissaoAssociada.CodigoPermissao) == null)
                    usuarioPerfilPermissaoDbLib.RemoverObjeto(perfilSalvo.CodigoPerfil, permissaoAssociada.CodigoPermissao);

            // Atribui a coleção ao cliente salvo
            perfilSalvo.Permissoes = perfilOriginal.Permissoes;
        }


        #endregion
    }
}
