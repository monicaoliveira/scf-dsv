﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Db;

using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens.Db;
using Resource.Framework.Library.Servicos;
using System.IO;
using System.Xml.Serialization;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Framework.Library.Db.Oracle;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia para LogInfo com entendimento de ParametrizacaoLogInfo
    /// </summary>
    public class LogResumoDbLib : IEntidadeDbLib<LogResumoInfo>
    {
        #region IEntidadeDbLib<LogInfo> Members

        public ConsultarObjetosResponse<LogResumoInfo> ConsultarObjetos(ConsultarObjetosRequest<LogResumoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // TipoLog
                //1469 condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoLog" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoLog"); //1469
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_LOG", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMaior"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LOG_MAIOR", condicaoInfo.Valores[0]);

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMenor"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LOG_MENOR", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroData"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_LOG_MENOR", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_LOG_MAIOR", condicaoInfo.Valores[0]);
                }

                // CodigoUsuario
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoUsuario");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_USUARIO", condicaoInfo.Valores[0]);

                // CodigoOrigem
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoOrigem");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_ORIGEM", condicaoInfo.Valores[0]);

                // TipoOrigem
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoOrigem");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_ORIGEM", condicaoInfo.Valores[0]);
            }

            // Coloca o número máximo de linhas
            paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_LOG_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<LogResumoInfo> resultado = new List<LogResumoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<LogResumoInfo>()
                {
                    Resultado = resultado
                };
        }

        public ReceberObjetoResponse<LogResumoInfo> ReceberObjeto(ReceberObjetoRequest<LogResumoInfo> request)
        {
            return null;
        }

        public SalvarObjetoResponse<LogResumoInfo> SalvarObjeto(SalvarObjetoRequest<LogResumoInfo> request)
        {
            return null;
        }

        public RemoverObjetoResponse<LogResumoInfo> RemoverObjeto(RemoverObjetoRequest<LogResumoInfo> request)
        {
            return null;
        }

        public LogResumoInfo MontarObjeto(DataRow dr)
        {
            // Resolve o tipo
            Type tipoLog = ResolutorTipos.Resolver((string)dr["TIPO_LOG"]);

            // Desserializa o log
            LogResumoInfo logResumoInfo =
                new LogResumoInfo()
                {
                    CodigoLog = dr["CODIGO_LOG"].ToString(),
                    CodigoUsuario = dr["CODIGO_USUARIO"] != DBNull.Value ? dr["CODIGO_USUARIO"].ToString() : null,
                    Descricao = dr["DESCRICAO_LOG"] != DBNull.Value ? dr["DESCRICAO_LOG"].ToString() : null,
                    DataLog = dr["DATA_LOG"] != DBNull.Value ? (DateTime)dr["DATA_LOG"] : DateTime.MinValue,
                    DataBDLog = dr["DATA_BD_LOG"] != DBNull.Value ? (DateTime)dr["DATA_BD_LOG"] : DateTime.MinValue,
                    TipoLog = dr["TIPO_LOG"] != DBNull.Value ? (string)dr["TIPO_LOG"] : null,
                    CodigoOrigem = dr["CODIGO_ORIGEM"] != DBNull.Value ? dr["CODIGO_ORIGEM"].ToString() : null,
                    TipoOrigem = dr["TIPO_ORIGEM"] != DBNull.Value ? dr["TIPO_ORIGEM"].ToString() : null
                };

            // Retorna
            return logResumoInfo;
        }

        #endregion
    }
}
