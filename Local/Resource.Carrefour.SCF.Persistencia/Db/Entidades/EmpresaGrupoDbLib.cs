﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class EmpresaGrupoDbLib : IEntidadeDbLib<EmpresaGrupoInfo>
    {
        #region IEntidadeDbLib<EmpresaGrupoInfo> Members

        #region Consultar

        public ConsultarObjetosResponse<EmpresaGrupoInfo> ConsultarObjetos(ConsultarObjetosRequest<EmpresaGrupoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // NomeEmpresaGrupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeEmpresaGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_EMPRESA_GRUPO", condicaoInfo.Valores[0]);

                // MaxLinhas
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "MaxLinhas");
                if (condicaoInfo != null)
                    paramsProc.Add("maxLinhas", condicaoInfo.Valores[0]);
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_EMPRESA_GRUPO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<EmpresaGrupoInfo> resultado = new List<EmpresaGrupoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<EmpresaGrupoInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<EmpresaGrupoInfo> ReceberObjeto(ReceberObjetoRequest<EmpresaGrupoInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_EMPRESA_GRUPO_B",
                        "pCODIGO_EMPRESA_GRUPO", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<EmpresaGrupoInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) :  null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<EmpresaGrupoInfo> RemoverObjeto(RemoverObjetoRequest<EmpresaGrupoInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_EMPRESA_GRUPO_R",
                "pCODIGO_EMPRESA_GRUPO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<EmpresaGrupoInfo>();
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<EmpresaGrupoInfo> SalvarObjeto(SalvarObjetoRequest<EmpresaGrupoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoEmpresaGrupo;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_EMPRESA_GRUPO", pk);
            paramsProc.Add("pNOME_EMPRESA_GRUPO", request.Objeto.NomeEmpresaGrupo);
            paramsProc.Add("pDESCRICAO_EMPRESA_GRUPO", request.Objeto.DescricaoEmpresaGrupo);
            paramsProc.Add("pENVIAR_NEGATIVO_EMPRESA_GRUPO", request.Objeto.EnviarNegativoEmpresaGrupo);
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_EMPRESA_GRUPO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            EmpresaGrupoInfo empresaGrupoSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<EmpresaGrupoInfo>()
                {
                    Objeto = empresaGrupoSalvo
                };
        }

        #endregion

        #endregion


        #region MontarObjeto 

        public EmpresaGrupoInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            EmpresaGrupoInfo empresaGrupoInfo =
                new EmpresaGrupoInfo()
                {

                    CodigoEmpresaGrupo = dr["CODIGO_EMPRESA_GRUPO"] != DBNull.Value ? dr["CODIGO_EMPRESA_GRUPO"].ToString() : null,
                    NomeEmpresaGrupo = dr["NOME_EMPRESA_GRUPO"] != DBNull.Value ? dr["NOME_EMPRESA_GRUPO"].ToString() : null,
                    DescricaoEmpresaGrupo = dr["DESCRICAO_EMPRESA_GRUPO"] != DBNull.Value ? dr["DESCRICAO_EMPRESA_GRUPO"].ToString() : null,
                    EnviarNegativoEmpresaGrupo = dr["ENVIAR_NEGATIVO_EMPRESA_GRUPO"] != DBNull.Value ? dr["ENVIAR_NEGATIVO_EMPRESA_GRUPO"].ToString() : null
                };

            // Retorna
            return empresaGrupoInfo;
        }

        #endregion

    }
}
