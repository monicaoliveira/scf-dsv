﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using System.IO;
using System.Xml.Serialization;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// DbLib para ArquivoItemResumoInfo
    /// </summary>
    public class ArquivoItemResumoDbLib : IEntidadeDbLib<ArquivoItemResumoInfo>
    {
        #region IEntidadeDbLib<ArquivoItemResumoInfo> Members

        /// <summary>
        /// Consulta de Criticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<ArquivoItemResumoInfo> ConsultarObjetos(ConsultarObjetosRequest<ArquivoItemResumoInfo> request)
        {
            // Parametros especificos
            bool incluirConteudo = false;
            bool filtrarApenasBloqueados = false;
            bool filtrarSemCritica = false;
            bool filtrarComCritica = false;
            bool filtrarSobAnalise = false;

            // Verifica se enviou mensagem especifica
            if (request is ListarArquivoItemDbRequest)
            {
                ListarArquivoItemDbRequest request2 = (ListarArquivoItemDbRequest)request;
                incluirConteudo = request2.IncluirConteudo;
                filtrarApenasBloqueados = request2.FiltrarApenasBloqueados;
                filtrarComCritica = request2.FiltrarComCritica;
                filtrarSemCritica = request2.FiltrarSemCritica;
                filtrarSobAnalise = request2.FiltrarApenasSobAnalise;
            }
            
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusaoInicial"); //1308
                if (condicaoInfo != null)
                    paramsProc.Add("pDTINCINICIAL", condicaoInfo.Valores[0]);

                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusaoFinal"); //1308
                if (condicaoInfo != null)
                    paramsProc.Add("pDTINCFINAL", condicaoInfo.Valores[0]);

                // Codigo Arquivo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoArquivo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_ARQUIVO", condicaoInfo.Valores[0]);

                // Codigo Arquivo Item
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoArquivoItem");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_ARQUIVO_ITEM", condicaoInfo.Valores[0]);

                // Tipo Arquivo Item
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoArquivoItem");
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_ARQUIVO_ITEM", condicaoInfo.Valores[0]);

                // Tipo Critica
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoCritica");
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_CRITICA", condicaoInfo.Valores[0]);

                // Nome Campo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NomeCampo");
                if (condicaoInfo != null)
                    paramsProc.Add("pNOME_CAMPO", condicaoInfo.Valores[0]);

                // Status Arquivo Item
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusArquivoItem");
                if (condicaoInfo != null)
                    paramsProc.Add("pSTATUS_ARQUIVO_ITEM", SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(condicaoInfo.Valores[0]));

                // Status Arquivo Item
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusArquivoItem2");
                if (condicaoInfo != null)
                    paramsProc.Add("pSTATUS_ARQUIVO_ITEM_2", SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(condicaoInfo.Valores[0]));

                // Codigo Processo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoProcesso");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_PROCESSO", condicaoInfo.Valores[0]);
            }

            // Indica se deve trazer o conteúdo da linha
            paramsProc.Add("pIGNORAR_CONTEUDO", incluirConteudo ? "S" : "N");

            // Indica se deve trazer o conteúdo da linha
            paramsProc.Add("pFILTRARAPENASBLOQUEADOS", filtrarApenasBloqueados ? "S" : "N");

            // Indica se deve trazer o conteudo com critica
            paramsProc.Add("pFILTRARCOMCRITICA", filtrarComCritica ? "S" : "N");

            // Indica se deve trazer o conteudo com critica
            paramsProc.Add("pFILTRARAPENASSOBANALISE", filtrarSobAnalise ? "S" : "N");

            // Indica se deve retornar apenas itens que nao tem critica
            paramsProc.Add("pFILTRARSEMCRITICA", filtrarSemCritica ? "S" : "N");

            // MaxLinhas
            condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "MaxLinhas");
            if (condicaoInfo != null)
                paramsProc.Add("maxLinhas", condicaoInfo.Valores[0]);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_ARQUIVO_ITEM_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ArquivoItemResumoInfo> resultado = new List<ArquivoItemResumoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr, incluirConteudo));

            // Retorna
            return
                new ConsultarObjetosResponse<ArquivoItemResumoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Detalhe de Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<ArquivoItemResumoInfo> ReceberObjeto(ReceberObjetoRequest<ArquivoItemResumoInfo> request)
        {
            throw new NotImplementedException("ReceberObjeto de ArquivoItemResumoDbLib não implementado");
        }

        /// <summary>
        /// Salva Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<ArquivoItemResumoInfo> SalvarObjeto(SalvarObjetoRequest<ArquivoItemResumoInfo> request)
        {
            // Verifica se é uma instancia do método de bloqueio
            if (request is SalvarBloqueioArquivoItemDbRequest)
            {
                // Converto
                SalvarBloqueioArquivoItemDbRequest bloqueioRequest = (SalvarBloqueioArquivoItemDbRequest) request;

                // Trato os bloqueios
                if (bloqueioRequest.CodigosArquivoItemBloqueados != null && bloqueioRequest.CodigosArquivoItemBloqueados.Count > 0)
                {
                    foreach (string codigoArquivoItem in bloqueioRequest.CodigosArquivoItemBloqueados)
                    {
                        // Monta parâmetros
                        Dictionary<string, object> paramsProc = new Dictionary<string, object>();
                        paramsProc.Add("pCODIGO_ARQUIVO_ITEM", codigoArquivoItem);
                        paramsProc.Add("pSTATUS_ARQUIVO_ITEM",
                            SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(ArquivoItemStatusEnum.Bloqueado));

                        // Executa a procedure
                        OracleDbLib2.Default.ExecutarProcedure("PR_ARQUIVO_ITEM_S", paramsProc);

                        
                    }
                }

                // Trato os desbloqueios
                if (bloqueioRequest.CodigosArquivoItemDesbloqueados != null && bloqueioRequest.CodigosArquivoItemDesbloqueados.Count > 0)
                {
                    foreach (string codigoArquivoItem in bloqueioRequest.CodigosArquivoItemDesbloqueados)
                    {
                        // Monta parâmetros
                        Dictionary<string, object> paramsProc = new Dictionary<string, object>();
                        paramsProc.Add("pCODIGO_ARQUIVO_ITEM", codigoArquivoItem);
                        paramsProc.Add("pSTATUS_ARQUIVO_ITEM",
                            SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(ArquivoItemStatusEnum.PendenteDesbloqueio));

                        // Executa a procedure
                        DataTable tb =
                            (DataTable)
                                OracleDbLib2.Default.ExecutarProcedure("PR_ARQUIVO_ITEM_S", paramsProc)["RETCUR"];

                        
                    }
                }

                // Retorna
                return new SalvarObjetoResponse<ArquivoItemResumoInfo>();
            }

            throw new NotImplementedException("RemoverObjeto de ArquivoItemResumoDbLib não implementado");
        }

        /// <summary>
        /// Remove um Critica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<ArquivoItemResumoInfo> RemoverObjeto(RemoverObjetoRequest<ArquivoItemResumoInfo> request)
        {
            throw new NotImplementedException("RemoverObjeto de ArquivoItemResumoDbLib não implementado");
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ArquivoItemResumoInfo MontarObjeto(DataRow dr)
        {
            // Repassa a chamada
            return this.MontarObjeto(dr, false);
        }

        /// <summary>
        /// Monta Critica
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ArquivoItemResumoInfo MontarObjeto(DataRow dr, bool incluirConteudo)
        {
            string dtfim = dr["DATA_INCLUSAO"] != DBNull.Value ? dr["DATA_INCLUSAO"].ToString() : null;    //1308
            DateTime dtfimc = Convert.ToDateTime(dtfim); //1308
            
            // Preenche com as informações do banco
            return new ArquivoItemResumoInfo()
            {
                
                CodigoArquivo       = dr["CODIGO_ARQUIVO"] != DBNull.Value ? dr["CODIGO_ARQUIVO"].ToString() : null,
                ConteudoArquivoItem = dr["CONTEUDO_ARQUIVO_ITEM"] != DBNull.Value ? (string)dr["CONTEUDO_ARQUIVO_ITEM"] : null,
                CodigoArquivoItem   = dr["CODIGO_ARQUIVO_ITEM"] != DBNull.Value ? dr["CODIGO_ARQUIVO_ITEM"].ToString() : null,
                TipoArquivoItem     = dr["TIPO_ARQUIVO_ITEM"] != DBNull.Value ? (string)dr["TIPO_ARQUIVO_ITEM"] : null,
                StatusArquivoItem   = dr["STATUS_ARQUIVO_ITEM"] != DBNull.Value ? SqlDbLib.EnumToObject<ArquivoItemStatusEnum>(dr["STATUS_ARQUIVO_ITEM"]) : ArquivoItemStatusEnum.PendenteValidacao,
                TipoArquivo         = dr["TIPO_ARQUIVO"] != DBNull.Value ? (string)dr["TIPO_ARQUIVO"] : null,
                CodigoProcesso      = dr["CODIGO_PROCESSO"] != DBNull.Value ? dr["CODIGO_PROCESSO"].ToString() : null, //1353
                DataInclusao        = dtfimc //1308
            };
        }

        #endregion
    }
}
