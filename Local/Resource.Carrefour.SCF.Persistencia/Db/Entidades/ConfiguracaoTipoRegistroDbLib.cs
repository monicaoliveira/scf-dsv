﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum.Dados;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    class ConfiguracaoTipoRegistroDbLib : IEntidadeDbLib<ConfiguracaoTipoRegistroInfo>
    {
        #region IEntidadeDbLib<ConfiguracaoTipoRegistroInfo> Members

        #region Consultar

        public ConsultarObjetosResponse<ConfiguracaoTipoRegistroInfo> ConsultarObjetos(ConsultarObjetosRequest<ConfiguracaoTipoRegistroInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // TipoArquivo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoArquivo");
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_ARQUIVO", condicaoInfo.Valores[0]);

                // EnviarCCI
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "EnviarCCI");
                if (condicaoInfo != null)
                    if (condicaoInfo.Valores[0] != null && (bool)condicaoInfo.Valores[0] == true)
                        paramsProc.Add("pFL_ENVIAR_CCI", "S");
                    else
                        paramsProc.Add("pFL_ENVIAR_CCI", "N");
            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CONFIG_TIPO_REGISTRO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ConfiguracaoTipoRegistroInfo> resultado = new List<ConfiguracaoTipoRegistroInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ConfiguracaoTipoRegistroInfo>()
                {
                    Resultado = resultado
                };
        }

        #endregion

        #region Receber

        public ReceberObjetoResponse<ConfiguracaoTipoRegistroInfo> ReceberObjeto(ReceberObjetoRequest<ConfiguracaoTipoRegistroInfo> request)
        {

            // Faz a consulta no banco
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CONFIG_TIPO_REGISTRO_B",
                        "pCODIGO_CONFIG_TIPO_REGISTRO", request.CodigoObjeto);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            // Retorna
            return
                new ReceberObjetoResponse<ConfiguracaoTipoRegistroInfo>()
                {
                    Objeto =
                        tb.Rows.Count > 0 ?
                        this.MontarObjeto(tb.Rows[0]) :  null
                };
        }

        #endregion

        #region Remover

        public RemoverObjetoResponse<ConfiguracaoTipoRegistroInfo> RemoverObjeto(RemoverObjetoRequest<ConfiguracaoTipoRegistroInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_CONFIG_TIPO_REGISTRO_R",
                "pCODIGO_CONFIG_TIPO_REGISTRO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<ConfiguracaoTipoRegistroInfo>();
        }

        #endregion

        #region Salvar

        public SalvarObjetoResponse<ConfiguracaoTipoRegistroInfo> SalvarObjeto(SalvarObjetoRequest<ConfiguracaoTipoRegistroInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoConfiguracaoTipoRegistro;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_CONFIG_TIPO_REGISTRO", pk);
            paramsProc.Add("pTIPO_ARQUIVO", request.Objeto.TipoArquivo);
            paramsProc.Add("pTIPO_REGISTRO", request.Objeto.TipoRegistro);
            paramsProc.Add("pFL_ENVIAR_CCI", request.Objeto.EnviarCCI == true ? "S" : "N"); // paramsProc.Add("pFL_ENVIAR_CCI", request.Objeto.EnviarCCI != null ? (request.Objeto.EnviarCCI == true ? "S" : "N") : "N"); // warning 25/02/2019
            paramsProc.Add("pFL_GERAR_AGENDA", request.Objeto.GerarAgenda == true ? "S" : "N"); // paramsProc.Add("pFL_GERAR_AGENDA", request.Objeto.GerarAgenda != null ? (request.Objeto.GerarAgenda == true ? "S" : "N") : "N"); // warning 25/02/2019
            paramsProc.Add("pDIAS_REPASSE", request.Objeto.DiasRepasse == "" ? null : request.Objeto.DiasRepasse);
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            Dictionary<string, object> retorno =
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_CONFIG_TIPO_REGISTRO_S", paramsProc);

            // Pega as tabelas
            DataTable tb = (DataTable)retorno["RETCUR"];

            ConfiguracaoTipoRegistroInfo configuracaoSistemaSalvo = this.MontarObjeto(tb.Rows[0]);

            // Retorna
            return
                new SalvarObjetoResponse<ConfiguracaoTipoRegistroInfo>()
                {
                    Objeto = configuracaoSistemaSalvo
                };
        }

        #endregion

        #endregion


        #region MontarObjeto 

        public ConfiguracaoTipoRegistroInfo MontarObjeto(DataRow dr)
        {
            // Cria o usuario
            ConfiguracaoTipoRegistroInfo configuracaoTipoRegistroInfo =
                new ConfiguracaoTipoRegistroInfo()
                {
                    CodigoConfiguracaoTipoRegistro = dr["CODIGO_CONFIG_TIPO_REGISTRO"] != DBNull.Value ? dr["CODIGO_CONFIG_TIPO_REGISTRO"].ToString() : null,
                    TipoArquivo = dr["TIPO_ARQUIVO"].ToString(),
                    TipoRegistro = dr["TIPO_REGISTRO"].ToString(),
                    EnviarCCI = dr["FL_ENVIAR_CCI"].ToString() == "S" ? true : false ,
                    GerarAgenda = dr["FL_GERAR_AGENDA"].ToString() == "S" ? true : false ,
                    DiasRepasse = dr["DIAS_REPASSE"].ToString() 
                };

            
            // Retorna
            return configuracaoTipoRegistroInfo;
        }

        #endregion

    }
}
