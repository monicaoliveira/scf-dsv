﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia de Arquivo
    /// </summary>
    public class RelatorioConferenciaVendasDbLib : IEntidadeDbLib<RelatorioConferenciaVendasInfo>
    {
        #region IEntidadeDbLib<ArquivoInfo> Members

        /// <summary>
        /// Recebe arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<RelatorioConferenciaVendasInfo> ReceberObjeto(ReceberObjetoRequest<RelatorioConferenciaVendasInfo> request)
        {
            return null;
        }

        public SalvarObjetoResponse<RelatorioConferenciaVendasInfo> SalvarObjeto(SalvarObjetoRequest<RelatorioConferenciaVendasInfo> request)
        {
            return null;
        }

        public ConsultarObjetosResponse<RelatorioConferenciaVendasInfo> ConsultarObjetos(ConsultarObjetosRequest<RelatorioConferenciaVendasInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Grupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEmpresaGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_GRUPO", condicaoInfo.Valores[0]);


                // Subgrupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEmpresaSubgrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_EMPRESA_SUBGRUPO", condicaoInfo.Valores[0]);

                // Favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroFavorecido" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_BANCO_FAVORECIDO", condicaoInfo.Valores[0]);

                // Estabelecimento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroEstabelecimento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCODIGO_ESTABELECIMENTO", condicaoInfo.Valores[0]);

                // Data da venda - Fim
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataVendas"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_VENDA", condicaoInfo.Valores[0]);

                // Data da Venda - Inicio
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataVendas"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_VENDA", condicaoInfo.Valores[0]);

                // Data Venda igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataVendas"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_FIM_VENDA", condicaoInfo.Valores[0]);
                    paramsProc.Add("pDATA_INICIO_VENDA", condicaoInfo.Valores[0]);
                }


                // Data Movimento - Fim
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMovimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_MOVIMENTO", condicaoInfo.Valores[0]);

                // Data Movimento - Inicio
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMovimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_MOVIMENTO", condicaoInfo.Valores[0]);

                // Data Movimento igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroDataMovimento"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_FIM_MOVIMENTO", condicaoInfo.Valores[0]);
                    paramsProc.Add("pDATA_INICIO_MOVIMENTO", condicaoInfo.Valores[0]);
                }


                /*****************************************************/
                //  SCF-836 - Melhoria no relatório de vendas - Inicio
                /*****************************************************/

                // Data Retorno CCI - Fim
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltrotxtDataRetornoCCI"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_FIM_CCI", condicaoInfo.Valores[0]);

                // Data Retorno CCI - Inicio
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltrotxtDataRetornoCCI"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pDATA_INICIO_CCI", condicaoInfo.Valores[0]);

                // Data Retorno CCI igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltrotxtDataRetornoCCI"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pDATA_FIM_CCI", condicaoInfo.Valores[0]);
                    paramsProc.Add("pDATA_INICIO_CCI", condicaoInfo.Valores[0]);
                }

                // consolidacoes
                // grupo, subgrupo, produto
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidaGrupoSubGrupoProduto" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_GRUPO", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // data da venda
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidaDataVenda" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_DATA_VENDA", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // data da processamento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidaDataProcessamento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_DATA_PROCESSAMENTO", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // banco, agencia e conta
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidaBancoAgenciaConta" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_BANCO", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // estabelecimento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidaEstabelecimento" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_ESTABELECIMENTO", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");

                // tipo de registro
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroTipoRegistro" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_REGISTRO", condicaoInfo.Valores[0]);

                // ECOMMERCE - Fernando Bove - 20160105
                // tipo financeiro contábil
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroFinanceiroContabil" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pTIPO_FINANCEIRO_CONTABIL", condicaoInfo.Valores[0]);

                //Referencia selecionada
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroReferencia" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pREFERENCIA", condicaoInfo.Valores[0]);

                //Consolidação de Referencia
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroConsolidaReferencia" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pCONSOLIDA_REFERENCIA", Convert.ToBoolean(condicaoInfo.Valores[0]) == true ? "S" : "N");


                /*****************************************************/
                //  SCF-836 - Melhoria no relatório de vendas - Fim
                /*****************************************************/

            }

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_RELATORIO_VENDAS_L1", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<RelatorioConferenciaVendasInfo> resultado = new List<RelatorioConferenciaVendasInfo>();

            if (request is ListarRelatorioVendasDbRequest)
            {
                return
                    new ListarRelatorioVendasDbResponse()
                    {
                        QuantidadeLinhas = tb.Rows.Count
                    };
            }

            foreach (DataRow dr in tb.Rows)
            {
                resultado.Add(this.MontarObjeto(dr));
            }
            // Retorna
            return
                new ConsultarObjetosResponse<RelatorioConferenciaVendasInfo>()
                {
                    Resultado = resultado
                };
        }

        public RemoverObjetoResponse<RelatorioConferenciaVendasInfo> RemoverObjeto(RemoverObjetoRequest<RelatorioConferenciaVendasInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Monta arquivo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public RelatorioConferenciaVendasInfo MontarObjeto(System.Data.DataRow dr)
        {
            EstabelecimentoInfo estabelecimento = new EstabelecimentoInfo()
            {
                Agencia = dr.Table.Columns.Contains("AGENCIA") ? dr["AGENCIA"].ToString() : null,
                RazaoSocial = dr.Table.Columns.Contains("RAZAO_SOCIAL") ? dr["RAZAO_SOCIAL"].ToString() : null,
                Banco = dr.Table.Columns.Contains("BANCO") ? dr["BANCO"].ToString() : null,
                //CNPJ = dr["CNPJ"] != DBNull.Value ? dr["CNPJ"].ToString() : null,
                Conta = dr.Table.Columns.Contains("CONTA") ? dr["CONTA"].ToString() : null,
                //CodigoEstabelecimento = dr["CODIGO_ESTABELECIMENTO"] != DBNull.Value ? dr["CODIGO_ESTABELECIMENTO"].ToString() : null
            };

            ProdutoInfo produto = new ProdutoInfo()
            {
                DescricaoProduto = dr.Table.Columns.Contains("NOME_PRODUTO") ? dr["NOME_PRODUTO"].ToString() : null
            };

            ListaItemInfo referencia = new ListaItemInfo()
            {
                Valor = dr.Table.Columns.Contains("REFERENCIA") ? dr["REFERENCIA"].ToString() : null,
                Descricao = dr.Table.Columns.Contains("NOME_LISTA_ITEM") ? dr["NOME_LISTA_ITEM"].ToString() : null
            };

            // Retorna
            return
                new RelatorioConferenciaVendasInfo()
                {      
                    // CodigoTransacao = dr["CODIGO_TRANSACAO"] != DBNull.Value ? dr["CODIGO_TRANSACAO"].ToString() : null,
                   //StatusRetorno = dr["STATUS_RET_CESSAO"] != DBNull.Value ? dr["STATUS_RET_CESSAO"].ToString() : null,
                    NomeEmpresaGrupo = dr.Table.Columns.Contains("NOME_EMPRESA_GRUPO") ? dr["NOME_EMPRESA_GRUPO"].ToString() : null,                    
                    NomeEmpresaSubgrupo = dr.Table.Columns.Contains("NOME_EMPRESA_SUBGRUPO") ? dr["NOME_EMPRESA_SUBGRUPO"].ToString() : null,
                    //NomeFavorecido = dr.Table.Columns.Contains("NOME_FAVORECIDO") ? dr["NOME_FAVORECIDO"].ToString() : null,
                    Produto = produto,
                    Estabelecimento = estabelecimento,
                    Referencia = referencia,
                    TotalCedivelNaoNegociado = dr["TOTAL_CEDIVEL_NAO_NEGOCIADO"] != DBNull.Value ? Convert.ToDecimal(dr["TOTAL_CEDIVEL_NAO_NEGOCIADO"]) : 0,
                    TotalCedido = dr["TOTAL_CEDIDO"] != DBNull.Value ? Convert.ToDecimal(dr["TOTAL_CEDIDO"]) : 0,
                    TotalNaoCedido = dr["TOTAL_NAO_CEDIDO"] != DBNull.Value ? Convert.ToDecimal(dr["TOTAL_NAO_CEDIDO"]) : 0,
                    ValorLiquido = dr["VALOR_LIQUIDO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_LIQUIDO"]) : 0,
                    DataVenda = dr.Table.Columns.Contains("DATA_TRANSACAO") ? new DateTime?((DateTime)dr["DATA_TRANSACAO"]) : 
                        (dr.Table.Columns.Contains("DATA_MOVIMENTO") ? new DateTime?((DateTime)dr["DATA_MOVIMENTO"]) : null),
                    TotalBruto = dr["TOTAL_BRUTO"] != DBNull.Value ? Convert.ToDecimal(dr["TOTAL_BRUTO"]) : 0,
                    TotalComissao = dr["TOTAL_COMISSAO"] != DBNull.Value ? Convert.ToDecimal(dr["TOTAL_COMISSAO"]) : 0,
                    Quantidade = dr["QUANTIDADE"] != DBNull.Value ? Convert.ToInt32(dr["QUANTIDADE"]) : 0
                };
        }

        #endregion
    }
}
