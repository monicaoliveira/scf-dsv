﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db;
using Resource.Carrefour.SCF.Contratos.Principal;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia do relatório de transações
    /// </summary>
    public class RelatorioTransacaoDbLib : IEntidadeDbLib<RelatorioTransacaoInfo>
    {
        #region IEntidadeDbLib<RelatorioTransacaoInfo> Members

        /// <summary>
        /// Recebe relatorio de transações
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<RelatorioTransacaoInfo> ReceberObjeto(ReceberObjetoRequest<RelatorioTransacaoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Salva relatorio de transações
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<RelatorioTransacaoInfo> SalvarObjeto(SalvarObjetoRequest<RelatorioTransacaoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Lista relatório de transações
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<RelatorioTransacaoInfo> ConsultarObjetos(ConsultarObjetosRequest<RelatorioTransacaoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Filtro por origem
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "Origem");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_ORIGEM", condicaoInfo.Valores[0]);

                // Filtro por grupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoEmpresaGrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_EMPRESA_GRUPO", condicaoInfo.Valores[0]);

                // Filtro por subgrupo
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoEmpresaSubgrupo");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_EMPRESA_SUBGRUPO", condicaoInfo.Valores[0]);

                // Filtro por estabelecimento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoEstabelecimento");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_ESTABELECIMENTO", condicaoInfo.Valores[0]);

                // Filtro por favorecido
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoFavorecido");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_FAVORECIDO", condicaoInfo.Valores[0]);

                // Filtro por produto
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoProduto");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_PRODUTO", condicaoInfo.Valores[0]);

                // Filtro por plano
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoPlano");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_PLANO", condicaoInfo.Valores[0]);

                // Filtro por meio de captura
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "MeioCaptura");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_MEIO_CAPTURA", condicaoInfo.Valores[0]);

                // Filtro por tipo de registro
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoTransacao");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_REGISTRO", condicaoInfo.Valores[0]);

                // Filtro por tipo de transação
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "TipoTransacao");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_TRANSACAO", condicaoInfo.Valores[0]);

                // Filtro por número de pagamento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "NumeroPagamento");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_NUMERO_PAGAMENTO", condicaoInfo.Valores[0]);

                // Filtro por conta do cliente 1438
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "ContaCliente");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CONTA_CLIENTE", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataProcessamento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_PROCESSAMENTO_DE", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataProcessamento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_PROCESSAMENTO_ATE", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataProcessamento"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_PROCESSAMENTO_DE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_PROCESSAMENTO_ATE", condicaoInfo.Valores[0]);
                }

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataTransacao"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_TRANSACAO_DE", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataTransacao"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_TRANSACAO_ATE", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataTransacao"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_TRANSACAO_DE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_TRANSACAO_ATE", condicaoInfo.Valores[0]);
                }

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataVencimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_VENCIMENTO_DE", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataVencimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_VENCIMENTO_ATE", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataVencimento"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_VENCIMENTO_DE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_VENCIMENTO_ATE", condicaoInfo.Valores[0]);
                }

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataAgendamento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_AGENDAMENTO_DE", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataAgendamento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_AGENDAMENTO_ATE", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataAgendamento"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_AGENDAMENTO_DE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_AGENDAMENTO_ATE", condicaoInfo.Valores[0]);
                }

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataEnvioMatera"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_ENVIO_MATERA_DE", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataEnvioMatera"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_ENVIO_MATERA_ATE", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataEnvioMatera"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_ENVIO_MATERA_DE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_ENVIO_MATERA_ATE", condicaoInfo.Valores[0]);
                }

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataLiquidacao"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LIQUIDACAO_DE", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataLiquidacao"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_LIQUIDACAO_ATE", condicaoInfo.Valores[0]);

                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataLiquidacao"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_LIQUIDACAO_DE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_LIQUIDACAO_ATE", condicaoInfo.Valores[0]);
                }

                // Filtro por status de retorno do CCI
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusRetorno");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_RETORNO", condicaoInfo.Valores[0]);

                // Filtro por status do pagamento
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusPagamento");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_PAGAMENTO", condicaoInfo.Valores[0]);

                // Filtro por status da conciliação
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusConciliacao");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_CONCILIACAO", condicaoInfo.Valores[0]);

                // Filtro por status de validação
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusValidacao");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_VALIDACAO", condicaoInfo.Valores[0]);

                // Data Movimento - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataMovimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_MOVIMENTO_DE", condicaoInfo.Valores[0]);

                // Data Movimento - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataMovimento"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_MOVIMENTO_ATE", condicaoInfo.Valores[0]);

                // Data Movimento - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataMovimento"
                    && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                {
                    paramsProc.Add("pFILTRO_DATA_MOVIMENTO_DE", condicaoInfo.Valores[0]);
                    paramsProc.Add("pFILTRO_DATA_MOVIMENTO_ATE", condicaoInfo.Valores[0]);
                }

                // Filtro por status de cancelamento on line
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusCancelamentoOnLine");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_CANC_ON_LINE", condicaoInfo.Valores[0]);

                // Data Retorno Cessao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataRetornoCessaoDe"
                    && (c.TipoCondicao == CondicaoTipoEnum.Maior || c.TipoCondicao == CondicaoTipoEnum.MaiorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_RET_CESSAO_DE", condicaoInfo.Valores[0]);

                // Data Retorno Cessao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataRetornoCessaoAte"
                    && (c.TipoCondicao == CondicaoTipoEnum.Menor || c.TipoCondicao == CondicaoTipoEnum.MenorIgual));
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_RET_CESSAO_ATE", condicaoInfo.Valores[0]);

                // ECOMMRECE - Fernando Bove - 20160104
                // Filtro por tipo financeiro contábil
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroFinanceiroContabil");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_TIPO_FINANC_CTBIL", condicaoInfo.Valores[0]);

                // ECOMMRECE - Fernando Bove - 20160104
                // Filtro por NSU Host da transação
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroNSUHost");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_NSU_HOST", condicaoInfo.Valores[0]);

                // Filtro por Referencia
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "FiltroReferencia");
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_REFERENCIA", condicaoInfo.Valores[0]);

                // *************************************************

                // Filtro por status de validação
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "maxLinhas");
                if (condicaoInfo != null)
                    paramsProc.Add("maxLinhas", condicaoInfo.Valores[0]);
            }
            Dictionary<string, object> retorno =
                OracleDbLib2.Default.ExecutarProcedure(
                        "PR_RELATORIO_TRANSACAO_L", paramsProc);
            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)retorno["RETCUR"];

            DataTable TB2 = (DataTable)retorno["RETCUR_QUERY"];

            

            if (request is ListarRelatorioTransacaoLinhasDbRequest)
            {
                return
                    new ListarRelatorioTransacaoLinhasDbResponse()
                    {
                        QuantidadeLinhas = tb.Rows.Count
                    };           
            }

            // Preenche a coleção resultado
            List<RelatorioTransacaoInfo> resultado = new List<RelatorioTransacaoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<RelatorioTransacaoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Remove relatório de transações
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<RelatorioTransacaoInfo> RemoverObjeto(RemoverObjetoRequest<RelatorioTransacaoInfo> request)
        {
            return null;
        }

        /// <summary>
        /// Monta relatório de transações
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public RelatorioTransacaoInfo MontarObjeto(DataRow dr)
        {
            // Monta relatório
            RelatorioTransacaoInfo relInfo = new RelatorioTransacaoInfo();

            ListaItemInfo referencia = new ListaItemInfo()
            {
                //Valor = dr["REFERENCIA"] != DBNull.Value ? dr["REFERENCIA"].ToString() : null
                Valor = dr.Table.Columns.Contains("REFERENCIA") ? dr["REFERENCIA"].ToString() : null,
                Descricao = dr.Table.Columns.Contains("NOME_REFERENCIA") ? dr["NOME_REFERENCIA"].ToString() : null
            };

            relInfo.Agencia = dr["AGENCIA"] != DBNull.Value ? dr["AGENCIA"].ToString() : null;
            relInfo.Banco = dr["BANCO"] != DBNull.Value ? dr["BANCO"].ToString() : null;
            relInfo.CodigoAjuste = dr["CODIGO_AJUSTE"] != DBNull.Value ? dr["CODIGO_AJUSTE"].ToString() : null;
            relInfo.CodigoAnulacao = dr["CODIGO_ANULACAO"] != DBNull.Value ? dr["CODIGO_ANULACAO"].ToString() : null;
            relInfo.CodigoAutorizacao = dr["CODIGO_AUTORIZACAO"] != DBNull.Value ? dr["CODIGO_AUTORIZACAO"].ToString() : null;
            relInfo.CodigoAutorizacaoOriginal = dr["CODIGO_AUTORIZACAO_ORIGINAL"] != DBNull.Value ? dr["CODIGO_AUTORIZACAO_ORIGINAL"].ToString() : null;
            relInfo.CodigoEmpresaGrupo = dr["CODIGO_GRUPO"] != DBNull.Value ? dr["CODIGO_GRUPO"].ToString() : null;
            relInfo.CodigoEmpresaSubgrupo = dr["CODIGO_SUBGRUPO"] != DBNull.Value ? dr["CODIGO_SUBGRUPO"].ToString() : null;
            relInfo.CodigoEstabelecimento = dr["CODIGO_ESTABELECIMENTO"] != DBNull.Value ? dr["CODIGO_ESTABELECIMENTO"].ToString() : null;
            relInfo.CnpjEstabelecimento = dr["CNPJ"] != DBNull.Value ? SCFUtils.CompletarCNPJ(dr["CNPJ"].ToString()) : null;
            relInfo.CodigoFavorecido = dr["CODIGO_FAVORECIDO"] != DBNull.Value ? dr["CODIGO_FAVORECIDO"].ToString() : null;
            relInfo.CodigoPlano = dr["CODIGO_PLANO"] != DBNull.Value ? dr["CODIGO_PLANO"].ToString() : null;
            relInfo.CodigoProduto = dr["CODIGO_PRODUTO"] != DBNull.Value ? dr["CODIGO_PRODUTO"].ToString() : null;
            relInfo.Conta = dr["CONTA"] != DBNull.Value ? dr["CONTA"].ToString() : null;
            relInfo.CupomFiscal = dr["CUPOM_FISCAL"] != DBNull.Value ? dr["CUPOM_FISCAL"].ToString() : null;
            relInfo.DataAgendamento = dr["DATA_AGENDAMENTO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_AGENDAMENTO"]) : null;
            relInfo.DataEnvioCCI = dr["DATA_ENVIO_CCI"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_ENVIO_CCI"]) : null;
            relInfo.DataEnvioMatera = dr["DATA_ENVIO_MATERA"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_ENVIO_MATERA"]) : null;
            relInfo.DataGeracaoAgenda = dr["DATA_GERACAO_AGENDA"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_GERACAO_AGENDA"]) : null;
            relInfo.DataLiquidacao = dr["DATA_LIQUIDACAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_LIQUIDACAO"]) : null;
            //relInfo.DataInclusaoSCF = dr["DATA_INCLUSAO_SCF"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_INCLUSAO_SCF"]) : null;
            relInfo.DataRepasse = dr["DATA_REPASSE"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_REPASSE"]) : null;
            relInfo.DataRetornoCCI = dr["DATA_RETORNO_CCI"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_RETORNO_CCI"]) : null;
            relInfo.DataTransacao = dr["DATA_TRANSACAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_TRANSACAO"]) : null;
            relInfo.DataTransacaoOriginal = dr["DATA_TRANSACAO_ORIGINAL"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_TRANSACAO_ORIGINAL"]) : null;
            relInfo.DataVencimento = dr["DATA_VENCIMENTO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_VENCIMENTO"]) : null;
            relInfo.MeioCaptura = dr["MEIO_CAPTURA"] != DBNull.Value ? dr["MEIO_CAPTURA"].ToString() : null;
            relInfo.MeioPagamento = dr["MEIO_PAGAMENTO"] != DBNull.Value ? dr["MEIO_PAGAMENTO"].ToString() : null;
            relInfo.FormaMeioPagamento = dr["FORMA_MEIO_PAGAMENTO"] != DBNull.Value ? dr["FORMA_MEIO_PAGAMENTO"].ToString() : null;
            relInfo.MeioPagamentoSequencia = dr["MEIO_PAGAMENTO_SEQ"] != DBNull.Value ? dr["MEIO_PAGAMENTO_SEQ"].ToString() : null;
            relInfo.MeioPagamentoValor = dr["MEIO_PAGAMENTO_VALOR"] != DBNull.Value ? Convert.ToDecimal(dr["MEIO_PAGAMENTO_VALOR"]) : 0;
            relInfo.Modalidade = dr["MODALIDADE"] != DBNull.Value ? dr["MODALIDADE"].ToString() : null;
            relInfo.MotivoAjuste = dr["MOTIVO_AJUSTE"] != DBNull.Value ? dr["MOTIVO_AJUSTE"].ToString() : null;
            relInfo.MotivoAnulacao = dr["MOTIVO_ANULACAO"] != DBNull.Value ? dr["MOTIVO_ANULACAO"].ToString() : null;
            relInfo.NomeEmpresaGrupo = dr["NOME_GRUPO"] != DBNull.Value ? dr["NOME_GRUPO"].ToString() : null;
            relInfo.NomeEmpresaSubgrupo = dr["NOME_SUBGRUPO"] != DBNull.Value ? dr["NOME_SUBGRUPO"].ToString() : null;
            relInfo.NomeEstabelecimento = dr["NOME_ESTABELECIMENTO"] != DBNull.Value ? dr["NOME_ESTABELECIMENTO"].ToString() : null;
            relInfo.NomeFavorecido = dr["NOME_FAVORECIDO"] != DBNull.Value ? dr["NOME_FAVORECIDO"].ToString() : null;
            relInfo.NomePlano = dr["NOME_PLANO"] != DBNull.Value ? dr["NOME_PLANO"].ToString() : null;
            relInfo.NomeProduto = dr["NOME_PRODUTO"] != DBNull.Value ? dr["NOME_PRODUTO"].ToString() : null;
            relInfo.NsuHost = dr["NSU_HOST"] != DBNull.Value ? dr["NSU_HOST"].ToString() : null;
            relInfo.NsuHostOriginal = dr["NSU_HOST_ORIGINAL"] != DBNull.Value ? dr["NSU_HOST_ORIGINAL"].ToString() : null;
            relInfo.NsuTef = dr["NSU_TEF"] != DBNull.Value ? dr["NSU_TEF"].ToString() : null;
            relInfo.NsuTefOriginal = dr["NSU_TEF_ORIGINAL"] != DBNull.Value ? dr["NSU_TEF_ORIGINAL"].ToString() : null;
            relInfo.NumeroCartao = dr["NUMERO_CARTAO"] != DBNull.Value ? dr["NUMERO_CARTAO"].ToString() : null;
            relInfo.NumeroContaCliente = dr["NUMERO_CONTA_CLIENTE"] != DBNull.Value ? dr["NUMERO_CONTA_CLIENTE"].ToString() : null;
            relInfo.NumeroPagamento = dr["NUMERO_PAGAMENTO"] != DBNull.Value ? dr["NUMERO_PAGAMENTO"].ToString() : null;
            relInfo.NumeroParcela = dr["NUMERO_PARCELA"] != DBNull.Value ? dr["NUMERO_PARCELA"].ToString() : null;
            relInfo.NumerParcelaTotal = dr["NUMERO_PARCELA_TOTAL"] != DBNull.Value ? dr["NUMERO_PARCELA_TOTAL"].ToString() : null;
            relInfo.Origem = dr["ORIGEM"] == DBNull.Value ? (dr["ORIGEM2"] != DBNull.Value ? dr["ORIGEM2"].ToString() : "") : dr["ORIGEM"].ToString();
            relInfo.QuantidadeMeioPagamento = dr["QUANTIDADE_MEIO_PAGAMENTO"] != DBNull.Value ? dr["QUANTIDADE_MEIO_PAGAMENTO"].ToString() : null;
            relInfo.StatusConciliacao = dr["STATUS_CONCILIACAO"] != DBNull.Value ? ((Nullable<TransacaoStatusConciliacaoEnum>)SqlDbLib.EnumToObject<TransacaoStatusConciliacaoEnum>(dr["STATUS_CONCILIACAO"])) : null;
            relInfo.StatusImportacao = dr["STATUS_IMPORTACAO"] != DBNull.Value ? ((Nullable<ProcessoStatusEnum>)SqlDbLib.EnumToObject<ProcessoStatusEnum>(dr["STATUS_IMPORTACAO"])) : null;
            relInfo.StatusPagamento = dr["STATUS_PAGAMENTO"] != DBNull.Value ? ((Nullable<PagamentoStatusEnum>)SqlDbLib.EnumToObject<PagamentoStatusEnum>(dr["STATUS_PAGAMENTO"])) : null;
            relInfo.StatusRetorno = dr["STATUS_RETORNO"] != DBNull.Value ? ((Nullable<TransacaoStatusRetornoCessaoEnum>)SqlDbLib.EnumToObject<TransacaoStatusRetornoCessaoEnum>(dr["STATUS_RETORNO"])) : null;
            relInfo.StatusTransacao = dr["STATUS_TRANSACAO"] != DBNull.Value ? ((TransacaoStatusEnum)int.Parse(dr["STATUS_TRANSACAO"].ToString())).ToString() : null;
            relInfo.StatusValidacao = dr["STATUS_VALIDACAO"] != DBNull.Value ? dr["STATUS_VALIDACAO"].ToString() : null;
            relInfo.TipoAjuste = dr["TIPO_AJUSTE"] != DBNull.Value ? dr["TIPO_AJUSTE"].ToString() : null;
            relInfo.TipoLancamento = dr["TIPO_LANCAMENTO"] != DBNull.Value ? dr["TIPO_LANCAMENTO"].ToString() : null;
            relInfo.TipoProduto = dr["TIPO_PRODUTO"] != DBNull.Value ? dr["TIPO_PRODUTO"].ToString() : null;
            relInfo.TipoRegistro = dr["TIPO_REGISTRO"] != DBNull.Value ? dr["TIPO_REGISTRO"].ToString() : null;
            relInfo.TipoTransacao = dr["TIPO_TRANSACAO"] != DBNull.Value ? dr["TIPO_TRANSACAO"].ToString() : null;
            relInfo.UsuarioEnvioCCI = dr["USUARIO_ENVIO_CCI"] != DBNull.Value ? dr["USUARIO_ENVIO_CCI"].ToString() : null;
            relInfo.UsuarioEnvioMatera = dr["USUARIO_ENVIO_MATERA"] != DBNull.Value ? dr["USUARIO_ENVIO_MATERA"].ToString() : null;
            relInfo.UsuarioRetornoCCI = dr["USUARIO_RETORNO_CCI"] != DBNull.Value ? dr["USUARIO_RETORNO_CCI"].ToString() : null;
            relInfo.ValorBruto = dr["VALOR_BRUTO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_BRUTO"]) : 0;
            relInfo.ValorDesconto = dr["VALOR_DESCONTO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_DESCONTO"]) : 0;
            relInfo.ValorLiquido = dr["VALOR_LIQUIDO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_LIQUIDO"]) : 0;
            relInfo.ValorParcela = dr["VALOR_PARCELA"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_PARCELA"]) : 0;
            relInfo.ValorParcelaDesconto = dr["VALOR_PARCELA_DESCONTO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_PARCELA_DESCONTO"]) : 0;
            relInfo.ValorParcelaLiquido = dr["VALOR_PARCELA_LIQUIDO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_PARCELA_LIQUIDO"]) : 0;
            relInfo.CodigoProcesso = dr["CODIGO_PROCESSO"] != DBNull.Value ? dr["CODIGO_PROCESSO"].ToString() : null;
            relInfo.DataProcessamento = dr["DATA_PROCESSAMENTO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_PROCESSAMENTO"]) : null;
            //relInfo.TipoFinanceiroContabil = dr["TIPO_FINANCEIRO_CONTABIL"] != DBNull.Value ? dr["TIPO_FINANCEIRO_CONTABIL"].ToString() : null;
            //relInfo.NSUHostReversao = dr["NSU_HOST_REVERSAO"] != DBNull.Value ? dr["NSU_HOST_REVERSAO"].ToString() : null;
            //ClockWork - Marcos Matsuoka - inicio
            relInfo.Referencia = referencia;
            //ClockWork - Marcos Matsuoka - fim

            return relInfo;
        }

        #endregion
    }
}
