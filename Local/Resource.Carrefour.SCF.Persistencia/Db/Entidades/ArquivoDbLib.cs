﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Data;
using Resource.Framework.Library.Db.Oracle;
using Resource.Framework.Library;
using System.Xml.Serialization;
using System.IO;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;

namespace Resource.Carrefour.SCF.Persistencia.Db.Entidades
{
    /// <summary>
    /// Persistencia de Arquivo
    /// </summary>
    public class ArquivoDbLib : IEntidadeDbLib<ArquivoInfo>
    {
        #region IEntidadeDbLib<ArquivoInfo> Members

        /// <summary>
        /// Consulta de arquivos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsultarObjetosResponse<ArquivoInfo> ConsultarObjetos(ConsultarObjetosRequest<ArquivoInfo> request)
        {
            // Monta lista de parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            CondicaoInfo condicaoInfo = null;

            // Apenas se tem coleção de condições criada
            if (request.Condicoes != null)
            {
                // Data Inclusao - igual
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO", condicaoInfo.Valores[0]);

                // Data Inclusao - maior
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" && c.TipoCondicao == CondicaoTipoEnum.Maior);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_MAIOR", condicaoInfo.Valores[0]);

                // Data Inclusao - menor
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "DataInclusao" && c.TipoCondicao == CondicaoTipoEnum.Menor);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_DATA_INCLUSAO_MENOR", condicaoInfo.Valores[0]);

                // StatusProcesso
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "StatusArquivo" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_STATUS_ARQUIVO", SqlDbLib.EnumToDb<ArquivoStatusEnum>(condicaoInfo.Valores[0]));

                // codigoProcesso
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "CodigoProcesso" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CODIGO_PROCESSO", condicaoInfo.Valores[0]);

                // CHAVE ARQUIVIO
                condicaoInfo = request.Condicoes.Find(c => c.Propriedade == "ChaveArquivo" && c.TipoCondicao == CondicaoTipoEnum.Igual);
                if (condicaoInfo != null)
                    paramsProc.Add("pFILTRO_CHAVE_ARQUIVO", condicaoInfo.Valores[0]);
            }

            // Coloca o número máximo de linhas
            paramsProc.Add("maxLinhas", request.MaxLinhas);

            // Monta a execução da procedure e executa
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure(
                        "PR_ARQUIVO_L", paramsProc)["RETCUR"];

            // Preenche a coleção resultado
            List<ArquivoInfo> resultado = new List<ArquivoInfo>();
            foreach (DataRow dr in tb.Rows)
                resultado.Add(this.MontarObjeto(dr));

            // Retorna
            return
                new ConsultarObjetosResponse<ArquivoInfo>()
                {
                    Resultado = resultado
                };
        }

        /// <summary>
        /// Recebe arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberObjetoResponse<ArquivoInfo> ReceberObjeto(ReceberObjetoRequest<ArquivoInfo> request)
        {
            // Flags específicas
            bool retornarDetalhe = false;

            // Verifica se é mensagem específica
            if (request is ReceberArquivoDbRequest)
            {
                // Recebe flags específicas
                retornarDetalhe = ((ReceberArquivoDbRequest)request).RetornarDetalhe;
            }

            // Faz a consulta no banco
            DataTable tb = 
                (DataTable)OracleDbLib2.Default.ExecutarProcedure(
                    "PR_ARQUIVO_B", 
                    "pCODIGO_ARQUIVO", request.CodigoObjeto,
                    "retornarDetalhe", retornarDetalhe ? "S" : "N")["RETCUR"];

            // Retorna
            return new ReceberObjetoResponse<ArquivoInfo>()
            {
                Objeto = tb.Rows.Count > 0 ? this.MontarObjeto(tb.Rows[0], retornarDetalhe) : null
            };
        }

        /// <summary>
        /// Salva arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarObjetoResponse<ArquivoInfo> SalvarObjeto(SalvarObjetoRequest<ArquivoInfo> request)
        {
            // Verifica se é novo ou alteração
            string pk = request.Objeto.CodigoArquivo;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Monta parametros
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();
            paramsProc.Add("pCODIGO_ARQUIVO", pk);
            paramsProc.Add("pTIPO_ARQUIVO", request.Objeto.TipoArquivo);
            paramsProc.Add("pNOME_ARQUIVO", request.Objeto.NomeArquivo);
            paramsProc.Add("pLAYOUT_ARQUIVO", request.Objeto.LayoutArquivo);
            paramsProc.Add("pCODIGO_EMPRESA_GRUPO", request.Objeto.CodigoEmpresaGrupo);
            paramsProc.Add("pSTATUS_ARQUIVO", SqlDbLib.EnumToDb<ArquivoStatusEnum>(request.Objeto.StatusArquivo));
            
            if (request.Objeto.CodigoProcesso != "")
                paramsProc.Add("pCODIGO_PROCESSO", request.Objeto.CodigoProcesso);
            paramsProc.Add("retornarRegistro", "S");

            // Execução a procedure
            DataTable tb =
                (DataTable)
                    OracleDbLib2.Default.ExecutarProcedure("PR_ARQUIVO_S", paramsProc)["RETCUR"];

            // Retorna
            return
                new SalvarObjetoResponse<ArquivoInfo>()
                {
                    Objeto = this.MontarObjeto(tb.Rows[0])
                };
        }

        /// <summary>
        /// Remove Arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverObjetoResponse<ArquivoInfo> RemoverObjeto(RemoverObjetoRequest<ArquivoInfo> request)
        {
            // Monta a execução da procedure
            OracleDbLib2.Default.ExecutarProcedure(
                "PR_ARQUIVO_R",
                "pCODIGO_ARQUIVO", request.CodigoObjeto);

            // Retorna
            return new RemoverObjetoResponse<ArquivoInfo>();
        }

        /// <summary>
        /// Monta arquivo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ArquivoInfo MontarObjeto(System.Data.DataRow dr)
        {
            return this.MontarObjeto(dr, false);
        }

        /// <summary>
        /// Monta arquivo
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public ArquivoInfo MontarObjeto(System.Data.DataRow dr, bool retornarDetalhe)
        {
            // Cria a instancia
            ArquivoInfo arquivoInfo = new ArquivoDetalheView();
            ArquivoDetalheView arquivoInfo2 = (ArquivoDetalheView)arquivoInfo;
            
            // Monta informações do Info
            arquivoInfo.CodigoArquivo = dr["CODIGO_ARQUIVO"].ToString();
            arquivoInfo.NomeArquivo = dr["NOME_ARQUIVO"] != DBNull.Value ? (string)dr["NOME_ARQUIVO"] : null;
            arquivoInfo.DataInclusao = dr["DATA_INCLUSAO"] != DBNull.Value ? new DateTime?((DateTime)dr["DATA_INCLUSAO"]) : null;
            arquivoInfo.TipoArquivo = dr["TIPO_ARQUIVO"] != DBNull.Value ? (string)dr["TIPO_ARQUIVO"] : null;
            arquivoInfo.LayoutArquivo = dr["LAYOUT_ARQUIVO"] != DBNull.Value ? (string)dr["LAYOUT_ARQUIVO"] : null;
            arquivoInfo.StatusArquivo = dr["STATUS_ARQUIVO"] != DBNull.Value ? OracleDbLib2.EnumToObject<ArquivoStatusEnum>(dr["STATUS_ARQUIVO"]) : ArquivoStatusEnum.Cancelado;
            arquivoInfo.CodigoProcesso = dr["CODIGO_PROCESSO"].ToString();

            arquivoInfo2.StatusProcesso = dr["STATUS_PROCESSO"] != DBNull.Value ? OracleDbLib2.EnumToObject<ProcessoStatusEnum>(dr["STATUS_PROCESSO"]) : ProcessoStatusEnum.Cancelado;

            arquivoInfo.ChaveArquivo = dr["CHAVE_ARQUIVO"].ToString();
            arquivoInfo.CodigoEmpresaGrupo = dr["CODIGO_EMPRESA_GRUPO"].ToString() == "" ? null : dr["CODIGO_EMPRESA_GRUPO"].ToString();
            arquivoInfo.SiglaGrupo = dr["SIGLA_GRUPO"].ToString();
            // Se necessário, monta informações do detalhe
            if (retornarDetalhe)
            {
                arquivoInfo2.QuantidadeCriticas = dr["QUANTIDADE_CRITICAS"] != DBNull.Value ? Convert.ToInt32(dr["QUANTIDADE_CRITICAS"]) : 0;
                arquivoInfo2.QuantidadeLinhas = dr["QUANTIDADE_LINHAS"] != DBNull.Value ? Convert.ToInt32(dr["QUANTIDADE_LINHAS"]) : 0;
            }
            
            // Retorna
            return arquivoInfo;
        }

        #endregion
    }
}
