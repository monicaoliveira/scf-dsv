﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Request para executar procedure PR_TRANSACAO_L
    /// </summary>
    public class PR_TRANSACAO_L_Request
    {
        public string CodigoArquivo { get; set; }
        public string TipoTransacao { get; set; }
        public DateTime? DataInclusao { get; set; }
        public string CodigoArquivoRetornoCessao { get; set; }
        public TransacaoStatusRetornoCessaoEnum StatusRetornoCessao { get; set; }
        public string Chave { get; set; }
        public string ChaveLike { get; set; }
        public string Chave2 { get; set; }
        public string CodigoEstabelecimento { get; set; }
        public string NSU_HOST { get; set; }


    }
}
