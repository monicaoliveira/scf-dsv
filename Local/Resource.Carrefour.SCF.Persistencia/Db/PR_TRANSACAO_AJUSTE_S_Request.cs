﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Request para executar procedure PR_TRANSACAO_AJUSTE_S_Request
    /// </summary>
    public class PR_TRANSACAO_AJUSTE_S_Request
    {
        public string CodigoTransacao { get; set; }
        public int CodigoArquivoItem1 { get; set; }
        public int CodigoArquivoItem2 { get; set; }
        public int CodigoEstabelecimento { get; set; }
        public string NsuHost { get; set; }
        public DateTime DataTransacao { get; set; }
        public int CodigoArquivo { get; set; }
        public int CodigoArquivo2 { get; set; }
        public string Chave { get; set; }
        public string Chave2 { get; set; }
        public string Chave3 { get; set; }
        public string CodigoPagamento { get; set; }
        public string CodigoProcesso { get; set; }
        public string NsuHostOriginal { get; set; } 
        public DateTime DataTransacaoOriginal { get; set; }
        public string TipoLancamento { get; set; }
        public DateTime DataRepasse { get; set; }
        public string TipoAjuste { get; set; }
        public string CodigoAjuste { get; set; }
        public string MotivoAjuste { get; set; }
        public double ValorAjuste { get; set; }
        public double ValorDesconto { get; set; }
        public double ValorLiquido { get; set; }
        public string Banco { get; set; }
        public string Agencia { get; set; } 
        public string Conta { get; set; }
        // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Início
        public string NumeroContaCliente { get; set; }
        // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Fim
        //SCF1148 - inicio
        public string NumeroCartao { get; set; }
        //SCF1148 - fim
        public int? CodigoProduto { get; set; }
        public string CodigoFavorecidoOriginal { get; set; }
        public string CodigoFavorecido { get; set; }
        public string CodigoContaFavorecidoOriginal { get; set; }
        public string CodigoContaFavorecido { get; set; }
        public bool RetornarRegistro { get; set; }
        public DateTime? DataRepasseCalculada { get; set; }
        public double ValorRepasse { get; set; }
        public Nullable<TransacaoStatusEnum> StatusTransacao { get; set; }
        public TransacaoStatusRetornoCessaoEnum StatusRetornoCessao { get; set; }
        public DateTime DataMovimento { get; set; }
        public string NumeroLinhaArquivoTsys { get; set; }
        // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Início
        public string TipoFinanceiroContabil { get; set; }
        // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Fim
        public string BINRefTransacao { get; set; } //ROGERIO
        
        public bool AtualizaTransacao { get; set; }
    }
}
