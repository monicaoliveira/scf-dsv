﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Request para executar procedure PR_TRANSACAO_AJUSTE_S_Request
    /// </summary>
    public class PR_TRANSACAO_VENDA_S_Request
    {
        public string CodigoTransacao { get; set; }
        public double? CodigoArquivoItem1 { get; set; }
        public double? CodigoArquivoItem2 { get; set; }
        public int? CodigoEstabelecimento { get; set; }
        public string NsuHost { get; set; }
        public DateTime? DataTransacao { get; set; }
        public int? CodigoArquivo { get; set; }
        public int? CodigoArquivo2 { get; set; }
        public string Chave { get; set; }
        public string Chave2 { get; set; }
        public string Chave3 { get; set; }
        public string CodigoPagamento { get; set; }
        public string CodigoProcesso { get; set; }
        public string NsuTEF { get; set; }
        public string CodigoAutorizacao { get; set; }
        public DateTime? DataRepasse { get; set; }
        public DateTime? DataRepasseCalculada { get; set; }
        public string TipoProduto { get; set; }
        public string MeioCaptura { get; set; }
        public double? ValorVenda { get; set; }
        public double? ValorDesconto { get; set; }
        public double? ValorLiquido { get; set; }
        public string NumeroCartao { get; set; }
        public int? NumeroParcela { get; set; }
        public int? NumeroParcelaTotal { get; set; }
        public double? ValorParcela { get; set; }
        public double? ValorParcelaDesconto { get; set; }
        public double? ValorParcelaLiquido { get; set; }
        public string Banco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string NumeroContaCliente { get; set; }
        public int? CodigoProduto { get; set; }
        public int? CodigoPlano { get; set; }
        public string CupomFiscal { get; set; }
        public string Modalidade { get; set; }
        public string TipoLancamento { get; set; }
        public string CodigoFavorecidoOriginal { get; set; }
        public string CodigoFavorecido { get; set; }
        public string CodigoContaFavorecidoOriginal { get; set; }
        public string CodigoContaFavorecido { get; set; }
        public bool RetornarRegistro { get; set; }
        public double? ValorComissao { get; set; }
        public string BancoOriginal { get; set; }
        public string AgenciaOriginal { get; set; }
        public string ContaOriginal { get; set; }
        public string CodigoProcessoRetornoCessao { get; set; }
        public TransacaoStatusRetornoCessaoEnum StatusRetornoCessao { get; set; }
        public string CodigoArquivoRetornoCessao { get; set; }
        public double ValorRepasse { get; set; }
        public Nullable<TransacaoStatusEnum> StatusTransacao { get; set; }
        public DateTime DataMovimento { get; set; }
        public string NumeroLinhaArquivoTsys { get; set; }
        public bool AtualizaTransacao { get; set; }
        // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Início
        public DateTime DataPreAutorizacao { get; set; }
        // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Fim
        // Fernando Bove - envia o indicador de financeiro/contabil, NSU Host Original e Reversao - Ecommerce - Início
        public string TipoFinanceiroContabil { get; set; }
        public string BINRefTransacao { get; set; } //ROGERIO
        public string NsuHostOriginal { get; set; }
        public string NsuHostReversao { get; set; }
        // Fernando Bove - envia o indicador de financeiro/contabil, NSU Host Original e Reversao - Ecommerce - Fim
    }
}
