﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Db.Oracle;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Resource.Framework.Library.Servicos.Dados;
using System.IO;
using System.Xml.Serialization;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Db;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Framework.Library;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Fornece acesso direto às procedures
    /// </summary>
    public static class Procedures
    {
        /// <summary>
        /// Retorna uma conexao
        /// </summary>
        /// <returns></returns>
        public static OracleConnection ReceberConexao()
        {
            return OracleDbLib2.ReceberConexao();
        }

        /// <summary>
        /// Executa a query solicitada
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static DataTable ExecutarQuery(string query)
        {
            OracleConnection cn = Procedures.ReceberConexao();
            OracleCommand cm = new OracleCommand(query, cn);
            OracleDataAdapter da = new OracleDataAdapter(cm);
            DataTable tb = new DataTable();
            da.Fill(tb);
            cn.Close();
            return tb;
        }

        #region PR_ARQUIVO_S

        /// <summary>
        /// Salva linha de arquivo
        /// </summary>
        public static DataRow PR_ARQUIVO_S(OracleConnection cn, string codigoArquivo, string tipoArquivo, string nomeArquivo, string statusArquivo, bool retornarRegistro, string codigoProcesso, string chaveArquivo, string layoutArquivo, string codigoEmpresaGrupo)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_S", cn);

            // Atribui os valores
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = codigoArquivo;
            cm.Parameters[":pTIPO_ARQUIVO"].Value = tipoArquivo;
            cm.Parameters[":pNOME_ARQUIVO"].Value = nomeArquivo;
            cm.Parameters[":pSTATUS_ARQUIVO"].Value = statusArquivo;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = codigoProcesso;
            cm.Parameters[":pLAYOUT_ARQUIVO"].Value = layoutArquivo;
            cm.Parameters[":pCODIGO_EMPRESA_GRUPO"].Value = codigoEmpresaGrupo;

            if (chaveArquivo != null)
                cm.Parameters[":pCHAVE_ARQUIVO"].Value = chaveArquivo;

            if (codigoEmpresaGrupo != null)
                cm.Parameters[":pCODIGO_EMPRESA_GRUPO"].Value = codigoEmpresaGrupo;

            cm.Parameters[":retornarRegistro"].Value = retornarRegistro ? "S" : "N";

            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataRow dr = null;

            // Se pediu para retornar o registro...
            if (retornarRegistro)
            {
                // Verifica se tem retorno
                OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;
                if (!orc.IsNull)
                {
                    // Pega o reader
                    OracleDataReader dataReader = orc.GetDataReader();

                    // Transforma em dataTable
                    DataTable tb = new DataTable();
                    tb.Load(dataReader);

                    // Fecha o cursor
                    dataReader.Close();

                    // Retorna no parametro
                    if (tb.Rows.Count > 0)
                        dr = tb.Rows[0];
                }
            }

            // Finaliza
            cm.Connection.Dispose();
            cm.Dispose();

            // Retorna
            return dr;
        }


        /// <summary>
        /// Salva linha de arquivo
        /// </summary>
        public static DataRow PR_ARQUIVO_S(OracleConnection cn, string codigoArquivo, string tipoArquivo, string nomeArquivo, ArquivoStatusEnum statusArquivo, bool retornarRegistro, string codigoProcesso, string chaveArquivo, string layoutArquivo, string codigoEmpresaGrupo)
        {
            string status = SqlDbLib.EnumToDb<ArquivoStatusEnum>(statusArquivo).ToString();
            return PR_ARQUIVO_S(cn, codigoArquivo, tipoArquivo, nomeArquivo, status, retornarRegistro, codigoProcesso, chaveArquivo, layoutArquivo, codigoEmpresaGrupo);
        
       
        }

        #endregion

        #region PR_ARQUIVO_ITEM_S

        /// <summary>
        /// Pede alteração massiva de status de arquivoItem
        /// </summary>
        /// <returns>Quantidade de itens alterados</returns>
        public static int PR_ARQUIVO_ITEM_STATUS_S(string filtroCodigoArquivo, string filtroCodigoArquivoItem, string filtroTipoArquivoItem, string filtroTipoCritica, string filtroNomeCampo, ArquivoItemStatusEnum? filtroArquivoItemStatus, ArquivoItemStatusEnum statusArquivoItem)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_ITEM_STATUS_S");

            // Atribui os parametros
            cm.Parameters[":pFILTRO_CODIGO_ARQUIVO"].Value = filtroCodigoArquivo;
            cm.Parameters[":pFILTRO_CODIGO_ARQUIVO_ITEM"].Value = filtroCodigoArquivoItem;
            cm.Parameters[":pFILTRO_TIPO_ARQUIVO_ITEM"].Value = filtroTipoArquivoItem;
            cm.Parameters[":pFILTRO_TIPO_CRITICA"].Value = filtroTipoCritica;
            cm.Parameters[":pFILTRO_NOME_CAMPO"].Value = filtroNomeCampo;
            cm.Parameters[":pFILTRO_STATUS_ARQUIVO_ITEM"].Value = filtroArquivoItemStatus != null ? SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(filtroArquivoItemStatus.Value) : null;
            cm.Parameters[":pSTATUS_ARQUIVO_ITEM"].Value = SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(statusArquivoItem);

            // Executa
            cm.ExecuteNonQuery();

            // Pega o retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;
            OracleDataReader reader = orc.GetDataReader();
            int quantidade = 0;
            if (reader.Read())
                quantidade = Convert.ToInt32(reader.GetDecimal(0));

            // Retorna a quantidade de registros alterada
            return quantidade;
        }

        #endregion

        #region PR_CRITICA_R

        /// <summary>
        /// Exclusão de críticas
        /// </summary>
        /// <returns>Quantidade de itens alterados</returns>
        public static void PR_CRITICA_R(string codigoCritica, string codigoArquivo)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_CRITICA_R");

            // Atribui os parametros
            cm.Parameters[":pCODIGO_CRITICA"].Value = codigoCritica;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = codigoArquivo;
            //cm.Parameters[":pREMOVE_BLOQUEADOS"].Value = removeBloqueados ? "S" : "N";

            // Executa
            cm.ExecuteNonQuery();

            // Retorna a quantidade de registros alterada
            return;
        }

        #endregion

        //1408
        #region PR_LOG_R
        public static void PR_LOG_R(int codigoLog)
        {   OracleCommand cm =  (OracleCommand) OracleDbLib2.Default.GetProcedureCommand("PR_LOG_R");
            if (codigoLog == 0)
            {
                cm.Parameters[":pCODIGO_LOG"].Value = null;
            }
            else
            {
                cm.Parameters[":pCODIGO_LOG"].Value = codigoLog;
            }
            cm.ExecuteNonQuery();
            return;
        }
        #endregion

        #region PR_ARQUIVO_ITEM_S

        /// <summary>
        /// Faz a preparacao do command para a procedure PR_ARQUIVO_ITEM
        /// </summary>
        public static OracleCommand PR_ARQUIVO_ITEM_S_preparar(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_ITEM_S");

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Faz a preparacao do command para a procedure PR_ARQUIVO_ITEM
        /// </summary>
        public static OracleCommand PR_ARQUIVO_ITEM_S_preparar()
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_ITEM_S");

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Salva linha de arquivo
        /// </summary>
        //monica
        public static DataTable PR_ARQUIVO_ITEM_S(OracleCommand cm, string codigoArquivoItem, long codigoArquivo, string conteudoArquivoItem, string tipoArquivoItem, bool retornarRegistro, ArquivoItemStatusEnum statusArquivoItem, string chave, string referencia)
        {
            // Atribui os valores
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM"].Value = codigoArquivoItem;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = codigoArquivo;
            cm.Parameters[":pCONTEUDO_ARQUIVO_ITEM"].Value = conteudoArquivoItem;
            if(!String.IsNullOrEmpty(tipoArquivoItem)) 
                cm.Parameters[":pTIPO_ARQUIVO_ITEM"].Value = tipoArquivoItem.Equals("RP") ? "AV" : tipoArquivoItem;
            cm.Parameters[":pSTATUS_ARQUIVO_ITEM"].Value = SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(statusArquivoItem);
            cm.Parameters[":pCHAVE"].Value = chave;
            cm.Parameters[":pREFERENCIA"].Value = referencia;
            cm.Parameters[":retornarRegistro"].Value = retornarRegistro ? "S" : "N";

            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataTable tb = null;

            // Se pediu para retornar o registro...
            if (retornarRegistro)
            {
            }

            // Retorna
            return tb;
        }

        #region PR_ARQUIVO_ITEM_R

        /// <summary>
        /// Deletar registros da TB_ARQUIVO_ITEM
        /// </summary>
        /// <returns>Quantidade de itens alterados</returns>
        //SCF1131 - Marcos Matsuoka
        public static void PR_ARQUIVO_ITEM_R(string codigoArquivo)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_ITEM_R");

            // Atribui os parametros
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = codigoArquivo;
            
            // Executa
            cm.ExecuteNonQuery();

            // Retorna a quantidade de registros alterada
            return;
        }

        #endregion

        #endregion

        #region PR_ARQUIVO_ITEM_L

        /// <summary>
        /// Lista os itens de arquivo
        /// </summary>
        public static OracleRefCursor PR_ARQUIVO_ITEM_L(OracleConnection cn, string codigoArquivo, string tipoArquivoItem, int maxLinhas, bool filtrarSobAnalise)
        {
            return PR_ARQUIVO_ITEM_L(cn, codigoArquivo, tipoArquivoItem, false, false, maxLinhas, 0, 0, true, filtrarSobAnalise);
        }

        /// <summary>
        /// Lista os itens de arquivo
        /// </summary>
        public static OracleRefCursor PR_ARQUIVO_ITEM_L(OracleConnection cn, string codigoArquivo, string tipoArquivoItem, bool pFiltrarSemCritica, bool pFiltrarNaoBloqueados, int maxLinhas, int limiteMinimo, int limiteMaximo, bool pfiltrarStatusCancelado, bool filtrarSobAnalise)
        {
            return
                PR_ARQUIVO_ITEM_L(
                    cn,
                    new PR_ARQUIVO_ITEM_L_Request()
                    {
                        CodigoArquivo = codigoArquivo,
                        TipoArquivoItem = tipoArquivoItem,
                        FiltrarSemCritica = pFiltrarSemCritica,
                        FiltrarNaoBloqueados = pFiltrarNaoBloqueados,
                        FiltrarStatusCancelamentoOnline = pfiltrarStatusCancelado,
                        MaxLinhas = maxLinhas,
                        LimiteMinimo = limiteMinimo,
                        LimiteMaximo = limiteMaximo,
                        FiltrarSobAnalise = filtrarSobAnalise
                    });
        }

        /// <summary>
        /// Lista os itens de arquivo
        /// </summary>
        public static OracleRefCursor PR_ARQUIVO_ITEM_L(OracleConnection cn, PR_ARQUIVO_ITEM_L_Request request)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_ITEM_L", cn);

            // Atribui os valores
            cm.Parameters[":pCHAVE"].Value = request.Chave;
            cm.Parameters[":pCHAVE_INICIO"].Value = request.ChaveInicio;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM"].Value = request.CodigoArquivoItem;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso;
            cm.Parameters[":pFILTRARAPENASBLOQUEADOS"].Value = request.FiltrarApenasBloqueados ? "S" : "N";
            cm.Parameters[":pFILTRARCOMCRITICA"].Value = request.FiltrarComCritica ? "S" : "N";
            cm.Parameters[":pFILTRARNAOBLOQUEADOS"].Value = request.FiltrarNaoBloqueados ? "S" : "N";
            cm.Parameters[":pFILTRARSEMCRITICA"].Value = request.FiltrarSemCritica ? "S" : "N";
            cm.Parameters[":pFILTRARAPENASSOBANALISE"].Value = request.FiltrarSobAnalise ? "S" : "N";
            cm.Parameters[":ctSTATUS_CANCELADO"].Value = request.FiltrarStatusCancelamentoOnline ? "S" : "N";
            cm.Parameters[":pNOME_CAMPO"].Value = request.NomeCampo;
            cm.Parameters[":pSTATUS_ARQUIVO_ITEM"].Value = request.StatusArquivoItem1.HasValue ? SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(request.StatusArquivoItem1.Value) : null;
            cm.Parameters[":pSTATUS_ARQUIVO_ITEM2"].Value = request.StatusArquivoItem2.HasValue ? SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(request.StatusArquivoItem2.Value) : null;
            cm.Parameters[":pTIPO_ARQUIVO_ITEM"].Value = request.TipoArquivoItem;
            cm.Parameters[":pTIPO_CRITICA"].Value = request.TipoCritica;
            cm.Parameters[":ctSTATUS_BLOQUEADO"].Value = SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(ArquivoItemStatusEnum.Bloqueado);
            cm.Parameters[":ctSTATUS_EXCLUIDO"].Value = SqlDbLib.EnumToDb<ArquivoItemStatusEnum>(ArquivoItemStatusEnum.Excluido);
            cm.Parameters[":maxLinhas"].Value = request.MaxLinhas;
            cm.Parameters[":LIMITE_MINIMO"].Value = request.LimiteMinimo;
            cm.Parameters[":LIMITE_MAXIMO"].Value = request.LimiteMaximo;

            //1308
            string dtini = request.DataInclusaoInicial.HasValue ? request.DataInclusaoInicial.ToString() : null;
            string dtfim = request.DataInclusaoFinal.HasValue ? request.DataInclusaoFinal.ToString() : null;

            cm.Parameters[":pDTINCINICIAL"].Value = dtini; //== null ? null : Convert.ToDateTime(dtini);
            cm.Parameters[":pDTINCFINAL"].Value = dtfim; // == null ?  null : Convert.ToDateTime(dtfim);
            //1308
            
            // Executa
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }

        #endregion

        #region PR_REL_SALDO_CONCILIACAO_L

        /// <summary>
        /// Relatório de saldo conciliado
        /// </summary>                
        public static void PR_REL_SALDO_CONCILIACAO_L(OracleConnection cn, PR_REL_SALDO_CONCILIACAO_L_Request request, out OracleRefCursor retcur, out OracleRefCursor retCurSaldo)
        {
            // Log
            LogArquivo logArquivo =
                new LogArquivo("db", "rotina", "PR_REL_SALDO_CONCILIACAO_L");
            LogArquivo.Objeto("request", request);

            // Inicializa
            retcur = null;
            retCurSaldo = null;

            // Bloco de controle
            try
            {
                // Pega o comando
                OracleCommand cm =
                    (OracleCommand)
                        OracleDbLib2.Default.GetProcedureCommand("PR_REL_SALDO_CONCILIACAO_L", cn);
                Dictionary<string, object> paramsProc = new Dictionary<string, object>();

                cm.Parameters[":pFILTRO_DATA_CON_INICIO"].Value = request.FiltroDataConciliacaoInicio;
                cm.Parameters[":pFILTRO_DATA_CON_FIM"].Value = request.FiltroDataConciliacaoFim;

                // Executa
                cm.ExecuteNonQuery();

                // Verifica se tem retorno
                retcur = (OracleRefCursor)cm.Parameters[":retcur"].Value;
                retCurSaldo = (OracleRefCursor)cm.Parameters[":retcurSaldo"].Value;
            }
            catch (Exception ex)
            {
                LogArquivo.LogErro(ex);
            }
            finally
            {
                logArquivo.Dispose();
            }
        }

        #endregion

        #region PR_RELATORIO_TRANSACAO_L

        /// <summary>
        /// Lista os itens de arquivo
        /// </summary>                
        public static OracleRefCursor PR_RELATORIO_TRANSACAO_L(OracleConnection cn, PR_RELATORIO_TRANSACAO_L_Request request)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_RELATORIO_TRANSACAO_L", cn);
            Dictionary<string, object> paramsProc = new Dictionary<string, object>();

            // Atribui os valores
            if (request.FiltroProduto != null)
                cm.Parameters[":pFILTRO_PRODUTO"].Value = request.FiltroProduto;
            if (request.FiltroFavorecido != null)
                cm.Parameters[":pFILTRO_FAVORECIDO"].Value = request.FiltroFavorecido;
            if (request.FiltroEstabelecimento != null)
                cm.Parameters[":pFILTRO_ESTABELECIMENTO"].Value = request.FiltroEstabelecimento;
            if (request.FiltroEmpresaSubgrupo != null)
                cm.Parameters[":pFILTRO_EMPRESA_SUBGRUPO"].Value = request.FiltroEmpresaSubgrupo;
            if (request.FiltroEmpresaGrupo != null)
                cm.Parameters[":pFILTRO_EMPRESA_GRUPO"].Value = request.FiltroEmpresaGrupo;
            if (request.FiltroOrigem != null)
                cm.Parameters[":pFILTRO_ORIGEM"].Value = request.FiltroOrigem;
            if (request.FiltroDataProcessamentoAte != null)
                cm.Parameters[":pFILTRO_DATA_PROCESSAMENTO_ATE"].Value = request.FiltroDataProcessamentoAte;
            if (request.FiltroDataProcessamentoDe != null)
                cm.Parameters[":pFILTRO_DATA_PROCESSAMENTO_DE"].Value = request.FiltroDataProcessamentoDe;

            if (request.FiltroDataMovimentoAte != null)
                cm.Parameters[":pFILTRO_DATA_MOVIMENTO_ATE"].Value = request.FiltroDataMovimentoAte;
            if (request.FiltroDataMovimentoDe != null)
                cm.Parameters[":pFILTRO_DATA_MOVIMENTO_DE"].Value = request.FiltroDataMovimentoDe;

            if (request.FiltroNumeroPagamento != null)
                cm.Parameters[":pFILTRO_NUMERO_PAGAMENTO"].Value = request.FiltroNumeroPagamento;
            if (request.FiltroContaCliente != null)
                cm.Parameters[":pFILTRO_CONTA_CLIENTE"].Value = request.FiltroContaCliente; //1438
            if (request.FiltroTipoTransacao != null)
                cm.Parameters[":pFILTRO_TIPO_TRANSACAO"].Value = request.FiltroTipoTransacao;
            if (request.FiltroTipoRegistro != null)
                cm.Parameters[":pFILTRO_TIPO_REGISTRO"].Value = request.FiltroTipoRegistro;
            if (request.FiltroMeioCaptura != null)
                cm.Parameters[":pFILTRO_MEIO_CAPTURA"].Value = request.FiltroMeioCaptura;
            if (request.FiltroPlano != null)
                cm.Parameters[":pFILTRO_PLANO"].Value = request.FiltroPlano;
            if (request.FiltroStatusValidacao != null)
                cm.Parameters[":pFILTRO_STATUS_VALIDACAO"].Value = request.FiltroStatusValidacao;
            if (request.FiltroStatusConciliacao != null)
                cm.Parameters[":pFILTRO_STATUS_CONCILIACAO"].Value = request.FiltroStatusConciliacao;
            if (request.FiltroStatusPagamento != null)
                cm.Parameters[":pFILTRO_STATUS_PAGAMENTO"].Value = request.FiltroStatusPagamento;
            if (request.FiltroStatusRetorno != null)
                cm.Parameters[":pFILTRO_STATUS_RETORNO"].Value = request.FiltroStatusRetorno;
            if (request.FiltroDataLiquidacaoAte != null)
                cm.Parameters[":pFILTRO_DATA_LIQUIDACAO_ATE"].Value = request.FiltroDataLiquidacaoAte;
            if (request.FiltroDataLiquidacaoDe != null)
                cm.Parameters[":pFILTRO_DATA_LIQUIDACAO_DE"].Value = request.FiltroDataLiquidacaoDe;
            if (request.FiltroDataEnvioMateraAte != null)
                cm.Parameters[":pFILTRO_DATA_ENVIO_MATERA_ATE"].Value = request.FiltroDataEnvioMateraAte;
            if (request.FiltroDataEnvioMateraDe != null)
                cm.Parameters[":pFILTRO_DATA_ENVIO_MATERA_DE"].Value = request.FiltroDataEnvioMateraDe;
            if (request.FiltroDataAgendamentoAte != null)
                cm.Parameters[":pFILTRO_DATA_AGENDAMENTO_ATE"].Value = request.FiltroDataAgendamentoAte;
            if (request.FiltroDataAgendamentoDe != null)
                cm.Parameters[":pFILTRO_DATA_AGENDAMENTO_DE"].Value = request.FiltroDataAgendamentoDe;
            if (request.FiltroDataVencimentoAte != null)
                cm.Parameters[":pFILTRO_DATA_VENCIMENTO_ATE"].Value = request.FiltroDataVencimentoAte;
            if (request.FiltroDataVencimentoDe != null)
                cm.Parameters[":pFILTRO_DATA_VENCIMENTO_DE"].Value = request.FiltroDataVencimentoDe;
            if (request.FiltroDataTransacaoAte != null)
                cm.Parameters[":pFILTRO_DATA_TRANSACAO_ATE"].Value = request.FiltroDataTransacaoAte;
            if (request.FiltroDataTransacaoDe != null)
                cm.Parameters[":pFILTRO_DATA_TRANSACAO_DE"].Value = request.FiltroDataTransacaoDe;

            // Filtro por status de cancelamento on line
            // NR-033
            if (request.FiltroStatusCancelamentoOnline != null)
                cm.Parameters[":pFILTRO_STATUS_CANC_ON_LINE"].Value = request.FiltroStatusCancelamentoOnline;

            if (request.FiltroRetornoEnvioCCIDe != null)
                cm.Parameters[":pFILTRO_DATA_RET_CESSAO_DE"].Value = request.FiltroRetornoEnvioCCIDe;
            if (request.FiltroRetornoEnvioCCIAte != null)
                cm.Parameters[":pFILTRO_DATA_RET_CESSAO_ATE"].Value = request.FiltroRetornoEnvioCCIAte;

            // ECOMMRECE - Fernando Bove - 20160104: Inicio
            // Filtro por tipo financeiro contábil
            if (request.FiltroFinanceiroContabil != null)
                cm.Parameters[":pFILTRO_TIPO_FINANC_CTBIL"].Value = request.FiltroFinanceiroContabil;

            // Filtro por NSU Host
            if (request.FiltroNSUHost != null)
                cm.Parameters[":pFILTRO_NSU_HOST"].Value = request.FiltroNSUHost;
            // ECOMMRECE - Fernando Bove - 20160104: Fim

            // Filtro por Referencia - ClockWork - Marcos Matsuoka
            if (request.FiltroReferencia != null)
                cm.Parameters[":pFILTRO_REFERENCIA"].Value = request.FiltroReferencia;

            // Executa
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }

        #endregion

        #region PR_REL_TRANS_INCONSISTENTES_L

        public static OracleRefCursor PR_REL_TRANS_INCONSISTENTE_L(OracleConnection cn, int codigoArquivo)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_REL_TRANS_INCONSISTENTE_L", cn);
            // Atribui os valores                            
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = codigoArquivo;

            //Executa      
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }

        public static OracleRefCursor PR_REL_TRANS_INCONSISTENTE_B(OracleConnection cn, string cnpj)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_REL_TRANS_INCONSISTENTE_B", cn);
            // Atribui os valores                            
            cm.Parameters[":pCNPJ"].Value = SCFUtils.CompletarCNPJ(cnpj.Trim());

            //Executa      
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }

        #endregion

        #region PR_NUMERO_SEQUENCIAL_L

        /// <summary>
        /// Lista os itens de arquivo
        /// </summary>
        public static int PR_NUMERO_SEQUENCIAL_L(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_NUMERO_SEQUENCIAL_L", cn);

            int retorno = 0;

            try
            {
                retorno = Convert.ToInt32(cm.ExecuteScalar());
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                cm.Dispose();
            }

            // Retorna
            return retorno;
        }

        #endregion

        #region PR_TRANSACAO_AJUSTE_S

        /// <summary>
        /// Faz a preparacao do command para a procedure PR_TRANSACAO_AJUSTE
        /// </summary>
        public static OracleCommand PR_TRANSACAO_AJUSTE_S_preparar(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_AJUSTE_S", cn);

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Salva linha de arquivo
        /// </summary>
        public static DataTable PR_TRANSACAO_AJUSTE_S(OracleCommand cm, PR_TRANSACAO_AJUSTE_S_Request request)
        {
            // Atribui os valores
            cm.Parameters[":pCODIGO_TRANSACAO"].Value = request.CodigoTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_1"].Value = request.CodigoArquivoItem1;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_2"].Value = request.CodigoArquivoItem2;
            cm.Parameters[":pCODIGO_ESTABELECIMENTO"].Value = request.CodigoEstabelecimento;
            cm.Parameters[":pNSU_HOST"].Value = request.NsuHost;
            cm.Parameters[":pDATA_TRANSACAO"].Value = request.DataTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;
            cm.Parameters[":pCODIGO_ARQUIVO2"].Value = request.CodigoArquivo2;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso;
            cm.Parameters[":pCHAVE"].Value = request.Chave;
            cm.Parameters[":pCHAVE2"].Value = request.Chave2;
            cm.Parameters[":pCHAVE3"].Value = request.Chave3;
            cm.Parameters[":pCODIGO_PAGAMENTO"].Value = request.CodigoPagamento;
            cm.Parameters[":pDATA_TRANSACAO_ORIGINAL"].Value = request.DataTransacaoOriginal;
            cm.Parameters[":pTIPO_LANCAMENTO"].Value = request.TipoLancamento;
            cm.Parameters[":pDATA_REPASSE"].Value = request.DataRepasse;
            cm.Parameters[":pTIPO_AJUSTE"].Value = request.TipoAjuste;
            cm.Parameters[":pCODIGO_AJUSTE"].Value = request.CodigoAjuste;
            cm.Parameters[":pMOTIVO_AJUSTE"].Value = request.MotivoAjuste;
            cm.Parameters[":pVALOR_AJUSTE"].Value = request.ValorAjuste;
            cm.Parameters[":pVALOR_DESCONTO"].Value = request.ValorDesconto;
            cm.Parameters[":pVALOR_LIQUIDO"].Value = request.ValorLiquido;
            cm.Parameters[":pBANCO"].Value = request.Banco;
            cm.Parameters[":pAGENCIA"].Value = request.Agencia;
            cm.Parameters[":pCONTA"].Value = request.Conta;
            cm.Parameters[":pNSU_HOST_ORIGINAL"].Value = request.NsuHostOriginal;
            cm.Parameters[":pCODIGO_FAVORECIDO_ORIGINAL"].Value = request.CodigoFavorecidoOriginal;
            cm.Parameters[":pCODIGO_FAVORECIDO"].Value = request.CodigoFavorecido;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO_ORIG"].Value = request.CodigoContaFavorecidoOriginal;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO"].Value = request.CodigoContaFavorecido;
            cm.Parameters[":pCODIGO_PRODUTO"].Value = request.CodigoProduto;
            cm.Parameters[":retornarRegistro"].Value = request.RetornarRegistro ? "S" : "N";
            cm.Parameters[":pVALOR_REPASSE"].Value = request.ValorRepasse;
            cm.Parameters[":pSTATUS_RET_CESSAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusRetornoCessaoEnum>(request.StatusRetornoCessao);
            cm.Parameters[":pSTATUS_TRANSACAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusEnum>(request.StatusTransacao);
            cm.Parameters[":pDATA_MOVIMENTO"].Value = request.DataMovimento;
            cm.Parameters[":pNUMERO_LINHA_ARQUIVO_TSYS"].Value = request.NumeroLinhaArquivoTsys;

            if (request.DataRepasseCalculada != null)
                cm.Parameters[":pDATA_REPASSE_CALCULADA"].Value = request.DataRepasseCalculada;

            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Inicio
            if (request.AtualizaTransacao)
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "S";
            else
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "N";
            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Fim

            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Início
            if (!string.IsNullOrEmpty(request.NumeroContaCliente))
                cm.Parameters[":pNUM_CONTA_CLIENTE"].Value = request.NumeroContaCliente;
            else
                cm.Parameters[":pNUM_CONTA_CLIENTE"].Value = DBNull.Value;
            // Fernando Bove - envia o numero da conta do cliente caso exista - Ecommerce - Fim

            // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Início
            if (request.TipoFinanceiroContabil != null)
                cm.Parameters[":pTIPO_FINANCEIRO_CONTABIL"].Value = request.TipoFinanceiroContabil;
            // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Fim

            //SCF1148 - inicio
            if (!string.IsNullOrEmpty(request.NumeroCartao))
                cm.Parameters[":pNUMERO_CARTAO"].Value = request.NumeroCartao;
            else
                cm.Parameters[":pNUMERO_CARTAO"].Value = DBNull.Value;
            //SCF1148 - fim

            //BinReferencia - inicio
            if (!string.IsNullOrEmpty(request.BINRefTransacao))
                cm.Parameters[":pREFERENCIA"].Value = request.BINRefTransacao;
            else
                cm.Parameters[":pREFERENCIA"].Value = DBNull.Value;
            //BinReferencia - fim

            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataTable tb = null;

            // Se pediu para retornar o registro...
            if (request.RetornarRegistro)
            {
            }

            // Retorna
            return tb;
        }

        #endregion

        #region PR_TRANSACAO_STATUS_S
        // SCF 1702 - ID 29
        /// <summary>
        /// Faz a preparacao do command para a procedure PR_TRANSACAO_STATUS_S
        /// </summary>
        public static OracleCommand PR_TRANSACAO_STATUS_S_preparar(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_STATUS_S", cn);

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Atualiza Status da Transacao com base no Arquivo Item
        /// </summary>
        public static DataTable PR_TRANSACAO_STATUS_S(OracleCommand cm, string CodigoArquivoItem1, int StatusTransacao)
        {
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM"].Value = CodigoArquivoItem1;
            cm.Parameters[":pSTATUS_TRANSACAO"].Value = StatusTransacao;

            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataTable tb = null;

            // Retorna
            return tb;
        }

        #endregion
        #region PR_TRANSACAO_VENDA_S

        /// <summary>
        /// Faz a preparacao do command para a procedure PR_TRANSACAO_VENDA
        /// </summary>
        public static OracleCommand PR_TRANSACAO_VENDA_S_preparar(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_VENDA_S", cn);

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Salva linha de arquivo
        /// </summary>
        public static DataTable PR_TRANSACAO_VENDA_S(OracleCommand cm, PR_TRANSACAO_VENDA_S_Request request)
        {
            // Atribui os valores
            cm.Parameters[":pCODIGO_TRANSACAO"].Value = request.CodigoTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_1"].Value = request.CodigoArquivoItem1;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_2"].Value = request.CodigoArquivoItem2;
            cm.Parameters[":pCODIGO_ESTABELECIMENTO"].Value = request.CodigoEstabelecimento;
            cm.Parameters[":pNSU_HOST"].Value = request.NsuHost;
            cm.Parameters[":pDATA_TRANSACAO"].Value = request.DataTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;
            cm.Parameters[":pCODIGO_ARQUIVO2"].Value = request.CodigoArquivo2;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso;
            cm.Parameters[":pCHAVE"].Value = request.Chave;
            cm.Parameters[":pCHAVE2"].Value = request.Chave2;
            cm.Parameters[":pCHAVE3"].Value = request.Chave3;
            cm.Parameters[":pCODIGO_PAGAMENTO"].Value = request.CodigoPagamento;
            cm.Parameters[":pNSU_TEF"].Value = request.NsuTEF;
            cm.Parameters[":pCODIGO_AUTORIZACAO"].Value = request.CodigoAutorizacao;
            cm.Parameters[":pDATA_REPASSE"].Value = request.DataRepasse;
            cm.Parameters[":pTIPO_PRODUTO"].Value = request.TipoProduto;
            cm.Parameters[":pMEIO_CAPTURA"].Value = request.MeioCaptura;
            cm.Parameters[":pVALOR_VENDA"].Value = request.ValorVenda;
            cm.Parameters[":pVALOR_DESCONTO"].Value = request.ValorDesconto;
            cm.Parameters[":pVALOR_LIQUIDO"].Value = request.ValorLiquido;
            cm.Parameters[":pNUMERO_CARTAO"].Value = request.NumeroCartao;
            cm.Parameters[":pNUMERO_PARCELA"].Value = request.NumeroParcela;
            cm.Parameters[":pNUMERO_PARCELA_TOTAL"].Value = request.NumeroParcelaTotal;
            cm.Parameters[":pVALOR_PARCELA"].Value = request.ValorParcela;
            cm.Parameters[":pVALOR_PARCELA_DESCONTO"].Value = request.ValorParcelaDesconto;
            cm.Parameters[":pVALOR_PARCELA_LIQUIDO"].Value = request.ValorParcelaLiquido;
            cm.Parameters[":pBANCO"].Value = request.Banco;
            cm.Parameters[":pAGENCIA"].Value = request.Agencia;
            cm.Parameters[":pCONTA"].Value = request.Conta;
            cm.Parameters[":pNUMERO_CONTA_CLIENTE"].Value = request.NumeroContaCliente;
            cm.Parameters[":pCODIGO_PRODUTO"].Value = request.CodigoProduto;
            cm.Parameters[":pCODIGO_PLANO"].Value = request.CodigoPlano;
            cm.Parameters[":pCUPOM_FISCAL"].Value = request.CupomFiscal;
            cm.Parameters[":pMODALIDADE"].Value = request.Modalidade;
            cm.Parameters[":pTIPO_LANCAMENTO"].Value = request.TipoLancamento;
            cm.Parameters[":pSTATUS_RET_CESSAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusRetornoCessaoEnum>(request.StatusRetornoCessao);
            cm.Parameters[":pSTATUS_TRANSACAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusEnum>(request.StatusTransacao);
            cm.Parameters[":pCODIGO_PROCESSO_RET_CESSAO"].Value = request.CodigoProcessoRetornoCessao;
            cm.Parameters[":retornarRegistro"].Value = request.RetornarRegistro ? "S" : "N";
            cm.Parameters[":pBANCO_ORIGINAL"].Value = request.BancoOriginal;
            cm.Parameters[":pAGENCIA_ORIGINAL"].Value = request.AgenciaOriginal;
            cm.Parameters[":pCONTA_ORIGINAL"].Value = request.ContaOriginal;
            cm.Parameters[":pCODIGO_ARQUIVO_RET_CESSAO"].Value = request.CodigoArquivoRetornoCessao;
            cm.Parameters[":pCODIGO_FAVORECIDO_ORIGINAL"].Value = request.CodigoFavorecidoOriginal;
            cm.Parameters[":pCODIGO_FAVORECIDO"].Value = request.CodigoFavorecido;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO_ORIG"].Value = request.CodigoContaFavorecidoOriginal;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO"].Value = request.CodigoContaFavorecido;
            cm.Parameters[":pVALOR_COMISSAO"].Value = request.ValorComissao;
            cm.Parameters[":pVALOR_REPASSE"].Value = request.ValorRepasse;
            cm.Parameters[":pDATA_MOVIMENTO"].Value = request.DataMovimento;
            cm.Parameters[":pNUMERO_LINHA_ARQUIVO_TSYS"].Value = request.NumeroLinhaArquivoTsys;

            if (request.DataRepasseCalculada != null)
                cm.Parameters[":pDATA_REPASSE_CALCULADA"].Value = request.DataRepasseCalculada;

            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Inicio
            if (request.AtualizaTransacao)
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "S";
            else
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "N";
            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Fim

            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Início
            if (request.DataPreAutorizacao != null)
                cm.Parameters[":pDATA_PRE_AUTORIZACAO"].Value = request.DataPreAutorizacao;
            // Fernando Bove - envia a data de pre autorizacao caso exista - Ecommerce - Fim

            // Fernando Bove - envia o indicador de financeiro/contabil, NSU Host Original e Reversao - Ecommerce - Início
            if (request.TipoFinanceiroContabil != null)
                cm.Parameters[":pTIPO_FINANCEIRO_CONTABIL"].Value = request.TipoFinanceiroContabil;
            if (request.NsuHostOriginal != null)
                cm.Parameters[":pNSU_HOST_ORIGINAL"].Value = request.NsuHostOriginal;
            if (request.NsuHostReversao != null)
                cm.Parameters[":pNSU_HOST_REVERSAO"].Value = request.NsuHostReversao;
            // Fernando Bove - envia o indicador de financeiro/contabil, NSU Host Original e Reversao - Ecommerce - Fim

            //BinReferencia - inicio
            if (!string.IsNullOrEmpty(request.BINRefTransacao))
                cm.Parameters[":pREFERENCIA"].Value = request.BINRefTransacao;
            else
                cm.Parameters[":pREFERENCIA"].Value = DBNull.Value;
            //BinReferencia - fim

            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataTable tb = null;

            // Se pediu para retornar o registro...
            if (request.RetornarRegistro)
            {
            }

            // Retorna
            return tb;
        }

        #endregion

        #region PR_TRANSACAO_PAGAMENTO_S

        /// <summary>
        /// Faz a preparacao do command para a procedure PR_TRANSACAO_PAGAMENTO
        /// </summary>
        public static OracleCommand PR_TRANSACAO_PAGAMENTO_S_preparar(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_PAGAMENTO_S", cn);

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Salva linha de arquivo
        /// </summary>
        public static DataTable PR_TRANSACAO_PAGAMENTO_S(OracleCommand cm, PR_TRANSACAO_PAGAMENTO_S_Request request)
        {
            // Atribui os valores
            cm.Parameters[":pCODIGO_TRANSACAO"].Value = request.CodigoTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_1"].Value = request.CodigoArquivoItem1;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_2"].Value = request.CodigoArquivoItem2;
            cm.Parameters[":pCODIGO_ESTABELECIMENTO"].Value = request.CodigoEstabelecimento;
            cm.Parameters[":pNSU_HOST"].Value = request.NsuHost;
            cm.Parameters[":pDATA_TRANSACAO"].Value = request.DataTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;
            cm.Parameters[":pCODIGO_ARQUIVO2"].Value = request.CodigoArquivo2;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso;
            cm.Parameters[":pCHAVE"].Value = request.Chave;
            cm.Parameters[":pCHAVE2"].Value = request.Chave2;
            cm.Parameters[":pCHAVE3"].Value = request.Chave3;
            cm.Parameters[":pCODIGO_PAGAMENTO"].Value = request.CodigoPagamento;
            cm.Parameters[":pNSU_TEF"].Value = request.NsuTEF;
            cm.Parameters[":pCODIGO_AUTORIZACAO"].Value = request.CodigoAutorizacao;
            cm.Parameters[":pTIPO_LANCAMENTO"].Value = request.TipoLancamento;
            cm.Parameters[":pDATA_REPASSE"].Value = request.DataRepasse;
            cm.Parameters[":pMEIO_CAPTURA"].Value = request.MeioCaptura;
            cm.Parameters[":pVALOR_PAGAMENTO"].Value = request.ValorPagamento;
            cm.Parameters[":pVALOR_DESCONTO"].Value = request.ValorDesconto;
            cm.Parameters[":pVALOR_LIQUIDO"].Value = request.ValorLiquido;
            cm.Parameters[":pNUMERO_CARTAO"].Value = request.NumeroCartao;
            cm.Parameters[":pQUANTIDADE_MEIO_PAGAMENTO"].Value = request.QuantidadeMeioPagamento;
            cm.Parameters[":pMEIO_PAGAMENTO"].Value = request.MeioPagamento;
            cm.Parameters[":pMEIO_PAGAMENTO_SEQ"].Value = request.MeioPagamentoSeq;
            cm.Parameters[":pMEIO_PAGAMENTO_VALOR"].Value = request.MeioPagamentoValor;
            cm.Parameters[":pBANCO"].Value = request.Banco;
            cm.Parameters[":pAGENCIA"].Value = request.Agencia;
            cm.Parameters[":pCONTA"].Value = request.Conta;
            cm.Parameters[":pNUMERO_CONTA_CLIENTE"].Value = request.NumeroContaCliente;
            cm.Parameters[":pCODIGO_FAVORECIDO_ORIGINAL"].Value = request.CodigoFavorecidoOriginal;
            cm.Parameters[":pCODIGO_FAVORECIDO"].Value = request.CodigoFavorecido;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO_ORIG"].Value = request.CodigoContaFavorecidoOriginal;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO"].Value = request.CodigoContaFavorecido;
            cm.Parameters[":pCODIGO_PRODUTO"].Value = request.CodigoProduto;
            cm.Parameters[":retornarRegistro"].Value = request.RetornarRegistro ? "S" : "N";
            cm.Parameters[":pVALOR_REPASSE"].Value = request.ValorRepasse;
            cm.Parameters[":pSTATUS_RET_CESSAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusRetornoCessaoEnum>(request.StatusRetornoCessao);
            cm.Parameters[":pSTATUS_TRANSACAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusEnum>(request.StatusTransacao);
            cm.Parameters[":pDATA_MOVIMENTO"].Value = request.DataMovimento;
            cm.Parameters[":pNUMERO_LINHA_ARQUIVO_TSYS"].Value = request.NumeroLinhaArquivoTsys;

            if (request.DataRepasseCalculada != null)
                cm.Parameters[":pDATA_REPASSE_CALCULADA"].Value = request.DataRepasseCalculada;

            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Inicio
            if (request.AtualizaTransacao)
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "S";
            else
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "N";
            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Fim

            // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Início
            if (request.TipoFinanceiroContabil != null)
                cm.Parameters[":pTIPO_FINANCEIRO_CONTABIL"].Value = request.TipoFinanceiroContabil;
            // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Fim

            //BinReferencia - inicio
            if (!string.IsNullOrEmpty(request.BINRefTransacao))
                cm.Parameters[":pREFERENCIA"].Value = request.BINRefTransacao;
            else
                cm.Parameters[":pREFERENCIA"].Value = DBNull.Value;
            //BinReferencia - fim

            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataTable tb = null;

            // Se pediu para retornar o registro...
            if (request.RetornarRegistro)
            {
            }

            // Retorna
            return tb;
        }

        #endregion

        #region PR_TRANSACAO_ANULACAO_PAGTO_S

        /// <summary>
        /// Faz a preparacao do command para a procedure PR_TRANSACAO_ANULACAO_PAGTO
        /// </summary>
        public static OracleCommand PR_TRANSACAO_ANULACAO_PAGTO_S_preparar(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_ANULACAO_PAGTO_S", cn);

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Salva linha de arquivo
        /// </summary>
        public static DataTable PR_TRANSACAO_ANULACAO_PAGTO_S(OracleCommand cm, PR_TRANSACAO_ANULACAO_PAGTO_S_Request request)
        {
            // Atribui os valores
            cm.Parameters[":pCODIGO_TRANSACAO"].Value = request.CodigoTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_1"].Value = request.CodigoArquivoItem1;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_2"].Value = request.CodigoArquivoItem2;
            cm.Parameters[":pCODIGO_ESTABELECIMENTO"].Value = request.CodigoEstabelecimento;
            cm.Parameters[":pNSU_HOST"].Value = request.NsuHost;
            cm.Parameters[":pDATA_TRANSACAO"].Value = request.DataTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;
            cm.Parameters[":pCODIGO_ARQUIVO2"].Value = request.CodigoArquivo2;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso;
            cm.Parameters[":pCHAVE"].Value = request.Chave;
            cm.Parameters[":pCHAVE2"].Value = request.Chave2;
            cm.Parameters[":pCHAVE3"].Value = request.Chave3;
            cm.Parameters[":pCODIGO_PAGAMENTO"].Value = request.CodigoPagamento;
            cm.Parameters[":pNSU_HOST_ORIGINAL"].Value = request.NsuHostOriginal;
            cm.Parameters[":pNSU_TEF_ORIGINAL"].Value = request.NsuTEFOriginal;
            cm.Parameters[":pDATA_TRANSACAO_ORIGINAL"].Value = request.DataTransacaoOriginal;
            cm.Parameters[":pCODIGO_AUTORIZACAO_ORIGINAL"].Value = request.CodigoAutorizacaoOriginal;
            cm.Parameters[":pTIPO_LANCAMENTO"].Value = request.TipoLancamento;
            cm.Parameters[":pDATA_REEMBOLSO"].Value = request.DataReembolso;
            cm.Parameters[":pFORMA_MEIO_PAGAMENTO"].Value = request.FormaMeioPagamento;
            cm.Parameters[":pCODIGO_ANULACAO"].Value = request.CodigoAnulacao;
            cm.Parameters[":pMOTIVO_ANULACAO"].Value = request.MotivoAnulacao;
            cm.Parameters[":pVALOR_PAGAMENTO"].Value = request.ValorPagamento;
            cm.Parameters[":pVALOR_DESCONTO"].Value = request.ValorDesconto;
            cm.Parameters[":pVALOR_LIQUIDO"].Value = request.ValorLiquido;
            cm.Parameters[":pNUMERO_CARTAO"].Value = request.NumeroCartao;
            cm.Parameters[":pNUMERO_CONTA_CLIENTE"].Value = request.NumeroContaCliente;
            cm.Parameters[":pCODIGO_FAVORECIDO_ORIGINAL"].Value = request.CodigoFavorecidoOriginal;
            cm.Parameters[":pCODIGO_FAVORECIDO"].Value = request.CodigoFavorecido;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO_ORIG"].Value = request.CodigoContaFavorecidoOriginal;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO"].Value = request.CodigoContaFavorecido;
            cm.Parameters[":pCODIGO_PRODUTO"].Value = request.CodigoProduto;
            cm.Parameters[":retornarRegistro"].Value = request.RetornarRegistro ? "S" : "N";
            cm.Parameters[":pVALOR_REPASSE"].Value = request.ValorRepasse;
            cm.Parameters[":pSTATUS_RET_CESSAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusRetornoCessaoEnum>(request.StatusRetornoCessao);
            cm.Parameters[":pSTATUS_TRANSACAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusEnum>(request.StatusTransacao);
            cm.Parameters[":pDATA_MOVIMENTO"].Value = request.DataMovimento;
            cm.Parameters[":pNUMERO_LINHA_ARQUIVO_TSYS"].Value = request.NumeroLinhaArquivoTsys;

            cm.Parameters[":pDATA_REPASSE"].Value = request.DataRepasse;
            if (request.DataRepasseCalculada != null)
                cm.Parameters[":pDATA_REPASSE_CALCULADA"].Value = request.DataRepasseCalculada;

            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Inicio
            if (request.AtualizaTransacao)
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "S";
            else
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "N";
            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Fim
            
            // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Início
            if (request.TipoFinanceiroContabil != null)
                cm.Parameters[":pTIPO_FINANCEIRO_CONTABIL"].Value = request.TipoFinanceiroContabil;
            // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Fim
            
            //BinReferencia - inicio
            if (!string.IsNullOrEmpty(request.BINRefTransacao))
                cm.Parameters[":pREFERENCIA"].Value = request.BINRefTransacao;
            else
                cm.Parameters[":pREFERENCIA"].Value = DBNull.Value;
            //BinReferencia - fim
            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataTable tb = null;

            // Se pediu para retornar o registro...
            if (request.RetornarRegistro)
            {
            }

            // Retorna
            return tb;
        }

        #endregion

        #region PR_TRANSACAO_ANULACAO_VENDA_S

        /// <summary>
        /// Faz a preparacao do command para a procedure PR_TRANSACAO_ANULACAO_VENDA
        /// </summary>
        public static OracleCommand PR_TRANSACAO_ANULACAO_VENDA_S_preparar(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_ANULACAO_VENDA_S", cn);

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Salva linha de arquivo
        /// </summary>
        public static DataTable PR_TRANSACAO_ANULACAO_VENDA_S(OracleCommand cm, PR_TRANSACAO_ANULACAO_VENDA_S_Request request)
        {
            // Atribui os valores
            cm.Parameters[":pCODIGO_TRANSACAO"].Value = request.CodigoTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_1"].Value = request.CodigoArquivoItem1;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM_2"].Value = request.CodigoArquivoItem2;
            cm.Parameters[":pCODIGO_ESTABELECIMENTO"].Value = request.CodigoEstabelecimento;
            cm.Parameters[":pNSU_HOST"].Value = request.NsuHost;
            cm.Parameters[":pDATA_TRANSACAO"].Value = request.DataTransacao;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;
            cm.Parameters[":pCODIGO_ARQUIVO2"].Value = request.CodigoArquivo2;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso;
            cm.Parameters[":pCHAVE"].Value = request.Chave;
            cm.Parameters[":pCHAVE2"].Value = request.Chave2;
            cm.Parameters[":pCHAVE3"].Value = request.Chave3;
            cm.Parameters[":pCODIGO_PAGAMENTO"].Value = request.CodigoPagamento;
            cm.Parameters[":pNSU_HOST_ORIGINAL"].Value = request.NsuHostOriginal;
            cm.Parameters[":pNSU_TEF_ORIGINAL"].Value = request.NsuTEFOriginal;
            cm.Parameters[":pDATA_TRANSACAO_ORIGINAL"].Value = request.DataTransacaoOriginal;
            cm.Parameters[":pCODIGO_AUTORIZACAO_ORIGINAL"].Value = request.CodigoAutorizacaoOriginal;
            cm.Parameters[":pTIPO_LANCAMENTO"].Value = request.TipoLancamento;
            cm.Parameters[":pDATA_REEMBOLSO"].Value = request.DataReembolso;
            cm.Parameters[":pCODIGO_ANULACAO"].Value = request.CodigoAnulacao;
            cm.Parameters[":pMOTIVO_ANULACAO"].Value = request.MotivoAnulacao;
            cm.Parameters[":pVALOR_ANULACAO"].Value = request.ValorAnulacao;
            cm.Parameters[":pVALOR_DESCONTO"].Value = request.ValorDesconto;
            cm.Parameters[":pVALOR_LIQUIDO"].Value = request.ValorLiquido;
            cm.Parameters[":pNUMERO_CARTAO"].Value = request.NumeroCartao;
            cm.Parameters[":pNUMERO_PARCELA"].Value = request.NumeroParcela;
            cm.Parameters[":pNUMERO_PARCELA_TOTAL"].Value = request.NumeroParcelaTotal;
            cm.Parameters[":pVALOR_PARCELA"].Value = request.ValorParcela;
            cm.Parameters[":pVALOR_PARCELA_DESCONTO"].Value = request.ValorParcelaDesconto;
            cm.Parameters[":pVALOR_PARCELA_LIQUIDO"].Value = request.ValorParcelaLiquido;
            cm.Parameters[":pBANCO"].Value = request.Banco;
            cm.Parameters[":pAGENCIA"].Value = request.Agencia;
            cm.Parameters[":pCONTA"].Value = request.Conta;
            cm.Parameters[":pNUMERO_CONTA_CLIENTE"].Value = request.NumeroContaCliente;
            cm.Parameters[":pCODIGO_FAVORECIDO_ORIGINAL"].Value = request.CodigoFavorecidoOriginal;
            cm.Parameters[":pCODIGO_FAVORECIDO"].Value = request.CodigoFavorecido;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO_ORIG"].Value = request.CodigoContaFavorecidoOriginal;
            cm.Parameters[":pCODIGO_CONTA_FAVORECIDO"].Value = request.CodigoContaFavorecido;
            cm.Parameters[":pCODIGO_PRODUTO"].Value = request.CodigoProduto;
            cm.Parameters[":retornarRegistro"].Value = request.RetornarRegistro ? "S" : "N";
            cm.Parameters[":pVALOR_REPASSE"].Value = request.ValorRepasse;
            cm.Parameters[":pDATA_REPASSE"].Value = request.DataRepasse;
            cm.Parameters[":pSTATUS_RET_CESSAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusRetornoCessaoEnum>(request.StatusRetornoCessao);
            cm.Parameters[":pSTATUS_TRANSACAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusEnum>(request.StatusTransacao);
            cm.Parameters[":pDATA_MOVIMENTO"].Value = request.DataMovimento;
            cm.Parameters[":pNUMERO_LINHA_ARQUIVO_TSYS"].Value = request.NumeroLinhaArquivoTsys;

            if (request.DataRepasseCalculada != null)
                cm.Parameters[":pDATA_REPASSE_CALCULADA"].Value = request.DataRepasseCalculada;

            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Inicio
            if (request.AtualizaTransacao)
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "S";
            else
                cm.Parameters[":pFL_ATUALIZA_TRANSACAO"].Value = "N";
            // Fernando Orbite - envia se deve ajustar a trasacao - SCF-967 - Fim

            // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Início
            //if (request.TipoFinanceiroContabil != null) //SCF1162
            cm.Parameters[":pTIPO_FINANCEIRO_CONTABIL"].Value = request.TipoFinanceiroContabil;
            // Fernando Bove - envia o indicador de financeiro/contabil - Ecommerce - Fim

            //BinReferencia - inicio
            if (!string.IsNullOrEmpty(request.BINRefTransacao))
                cm.Parameters[":pREFERENCIA"].Value = request.BINRefTransacao;
            else
                cm.Parameters[":pREFERENCIA"].Value = DBNull.Value;
            //BinReferencia - fim

            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataTable tb = null;

            // Se pediu para retornar o registro...
            if (request.RetornarRegistro)
            {
            }

            // Retorna
            return tb;
        }

        #endregion

        #region PR_TRANSACAO_VENDA_L

        /// <summary>
        /// Lista transacoes de venda
        /// </summary>
        public static OracleRefCursor PR_TRANSACAO_VENDA_L(OracleConnection cn, int maxLinhas, PR_TRANSACAO_VENDA_L_Request request)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_VENDA_L", cn);

            // Atribui os valores
            if (request.CodigoArquivo != null)
                cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;

            cm.Parameters[":maxLinhas"].Value = maxLinhas;

            if (request.DataInclusao != null)
                cm.Parameters[":pDATA_INCLUSAO"].Value = request.DataInclusao;

            if (request.CodigoProcesso != null)
                cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso;

            if (request.StatusConciliacao != null)
                cm.Parameters[":pSTATUS_CONCILIACAO"].Value = OracleDbLib2.EnumToDb<TransacaoStatusConciliacaoEnum>(request.StatusConciliacao);

            if (request.Chave != null)
                cm.Parameters[":pCHAVE"].Value = request.Chave;

            if (request.Chave2 != null)
                cm.Parameters[":pCHAVE2"].Value = request.Chave;

            if (request.CodigoPlano != null)
                cm.Parameters[":pCODIGO_PLANO"].Value = request.CodigoPlano;

            //if (request.InformarOrigem != null) //warning 25/02/2019
                if (request.InformarOrigem == true)
                    cm.Parameters[":pINFORMAR_ORIGEM"].Value = "S";

            // Executa
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }


        /// <summary>
        /// Lista transacoes de venda
        /// </summary>
        public static OracleRefCursor PR_TRANSACAO_NAO_CONCILIADA_L(OracleConnection cn, PR_TRANSACAO_NAO_CONCILIADA_L_Request request)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_NAO_CONCILIADA_L", cn);

            if (request.TipoProcesso != null)
                cm.Parameters[":pTIPO_PROCESSO"].Value = request.TipoProcesso;

            // Executa
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }


        public static OracleRefCursor PR_TRANSACAO_CHAVE_L(OracleConnection cn, string CHAVE_LIKE, out int count)
        {
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_CHAVE_L", cn);

            cm.Parameters[":pCHAVE_LIKE"].Value = string.IsNullOrEmpty(CHAVE_LIKE) ? null : CHAVE_LIKE;
            // Executa
            OracleDataReader r = cm.ExecuteReader();

            r.NextResult();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;
            OracleRefCursor orcCount = (OracleRefCursor)cm.Parameters[":RETCURCOUNT"].Value;
            OracleDataReader dr = orcCount.GetDataReader();
            if (dr.Read())
                count = int.Parse(dr[0].ToString());
            else
                count = 0;
            orcCount.Dispose();

            // Retorna
            return orc;
        }

        /// <summary>
        /// Lista transacoes de venda
        /// </summary>
        public static OracleRefCursor PR_TRANSACAO_L(OracleConnection cn, int maxLinhas, PR_TRANSACAO_L_Request request)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_L", cn);

            // Atribui os valores
            if (request.CodigoArquivo != "")
                cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;

            cm.Parameters[":maxLinhas"].Value = maxLinhas;

            if (request.DataInclusao != null)
                cm.Parameters[":pDATA_INCLUSAO"].Value = request.DataInclusao;

            cm.Parameters[":pCHAVE"].Value = request.Chave;
            cm.Parameters[":pCHAVE_LIKE"].Value = request.ChaveLike;
            cm.Parameters[":pCHAVE2"].Value = request.Chave2;

            // filtra pelo NSU
            if (request.NSU_HOST != "")
                cm.Parameters[":pNSU_HOST"].Value = request.NSU_HOST;

            cm.Parameters[":pCODIGO_ARQUIVO_RET_CESSAO"].Value = request.CodigoArquivoRetornoCessao;
            cm.Parameters[":pSTATUS_RET_CESSAO"].Value = request.StatusRetornoCessao;

            // filtro do código do estabelecimento
            if (request.CodigoEstabelecimento != null)
                cm.Parameters[":pCODIGO_ESTABELECIMENTO"].Value = request.CodigoEstabelecimento;

            // Executa
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }

        #endregion

        #region PR_TRANSACAO_VENDA_AGENDAR

        /// <summary>
        /// Realiza agendamento das transações de venda
        /// </summary>
        public static void PR_TRANSACAO_VENDA_AGENDAR(int codigoProcesso, DateTime dataPagamento, string gerarApenasOrigem2)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_TRANSACAO_VENDA_AGENDAR");

            // Atribui os valores
            cm.Parameters[":pCODIGO_PROCESSO"].Value = codigoProcesso;
            cm.Parameters[":pDATA_PAGAMENTO"].Value = dataPagamento;
            cm.Parameters[":pGERAR_APENAS_ORIGEM2"].Value = gerarApenasOrigem2;

            // Executa
            cm.ExecuteNonQuery();

            // Finaliza
            cm.Connection.Dispose();
            cm.Dispose();
        }

        #endregion

        #region PR_PAGAMENTO_A

        /// <summary>
        /// Realiza agendamento das transações de venda
        /// </summary>
        public static void PR_PAGAMENTO_A(int? filtroCodigoPagamento, int? filtroCodigoProcesso)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_PAGAMENTO_A");

            // Atribui os valores
            cm.Parameters[":pFILTRO_CODIGO_PAGAMENTO"].Value = filtroCodigoPagamento;
            cm.Parameters[":pFILTRO_CODIGO_PROCESSO"].Value = filtroCodigoProcesso;

            // Executa
            cm.ExecuteNonQuery();

            // Finaliza
            cm.Connection.Dispose();
            cm.Dispose();
        }

        #endregion

        #region PR_ARQUIVO_ITEM_STATUS_A

        /// <summary>
        /// Ajusta status de arquivo item
        /// </summary>
        public static void PR_ARQUIVO_ITEM_STATUS_A(string codigoArquivo)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_ITEM_STATUS_A");

            // Atribui os valores
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = codigoArquivo;

            // Executa
            cm.ExecuteNonQuery();

            // Finaliza
            cm.Connection.Dispose();
            cm.Dispose();
        }

        #endregion

        #region PR_CRITICA_S

        /// <summary>
        /// Faz a preparacao do command para a procedure PR_CRITICA_S
        /// </summary>
        public static OracleCommand PR_CRITICA_S_preparar(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_CRITICA_S", cn);

            // Prepared
            cm.Prepare();

            // Retorna
            return cm;
        }

        /// <summary>
        /// Salva critica
        /// </summary>
        public static DataTable PR_CRITICA_S(OracleCommand cm, CriticaInfo criticaInfo)
        {
            // Verifica se é novo ou alteração
            string pk = criticaInfo.CodigoCritica;
            if (pk != null && pk.Contains("-"))
                pk = null;

            // Pega o tipo da critica
            Type tipoCritica = criticaInfo.GetType();

            // Serializa
            MemoryStream ms = new MemoryStream();
            new XmlSerializer(tipoCritica).Serialize(ms, criticaInfo);
            ms.Position = 0;
            StreamReader reader = new StreamReader(ms);
            string serializacaoObjeto = reader.ReadToEnd();
            reader.Close();

            // Propriedades que dependem do tipo do objeto
            string codigoProcesso = null;
            string codigoArquivo = null;
            string codigoArquivoItem = null;
            string tipoLinha = null;
            string nomeCampo = null;

            // Verifica se tem informacoes de arquivo, arquivoItem e processo
            if (tipoCritica.IsSubclassOf(typeof(CriticaSCFArquivoInfo)))
            {
                CriticaSCFArquivoInfo criticaSCFArquivo = (CriticaSCFArquivoInfo)criticaInfo;
                codigoProcesso = criticaSCFArquivo.CodigoProcesso;
                codigoArquivo = criticaSCFArquivo.CodigoArquivo;
            }
            if (tipoCritica.IsSubclassOf(typeof(CriticaSCFArquivoItemInfo)))
            {
                CriticaSCFArquivoItemInfo criticaSCFArquivoItem = (CriticaSCFArquivoItemInfo)criticaInfo;
                codigoArquivoItem = criticaSCFArquivoItem.CodigoArquivoItem;
                nomeCampo = criticaSCFArquivoItem.NomeCampo;
                tipoLinha = criticaSCFArquivoItem.TipoLinha;
            }

            // Repassa a chamada
            return
                Procedures.PR_CRITICA_S(
                    cm,
                    new PR_CRITICA_S_Request()
                    {
                        CodigoArquivo = codigoArquivo,
                        CodigoArquivoItem = codigoArquivoItem,
                        CodigoCritica = pk,
                        CodigoProcesso = codigoProcesso,
                        ConteudoCritica = serializacaoObjeto,
                        DataCritica = criticaInfo.DataCritica,
                        DescricaoCritica = criticaInfo.Descricao != null ? criticaInfo.Descricao : criticaInfo.ToString(),
                        TipoCritica = tipoCritica.Name,
                        NomeCampo = nomeCampo,
                        TipoLinha = tipoLinha,
                        RetornarRegistro = false
                    });
        }

        /// <summary>
        /// Salva critica
        /// </summary>
        public static DataTable PR_CRITICA_S(OracleCommand cm, PR_CRITICA_S_Request request)
        {
            // Atribui os valores
            cm.Parameters[":pCODIGO_CRITICA"].Value = request.CodigoCritica;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso;
            cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;
            cm.Parameters[":pCODIGO_ARQUIVO_ITEM"].Value = request.CodigoArquivoItem;
            cm.Parameters[":pDATA_CRITICA"].Value = request.DataCritica;
            cm.Parameters[":pTIPO_CRITICA"].Value = request.TipoCritica;
            cm.Parameters[":pDESCRICAO_CRITICA"].Value = request.DescricaoCritica;
            cm.Parameters[":pCONTEUDO_CRITICA"].Value = request.ConteudoCritica;
            cm.Parameters[":pNOME_CAMPO"].Value = request.NomeCampo;
            cm.Parameters[":pTIPO_LINHA"].Value = request.TipoLinha;
            cm.Parameters[":retornarRegistro"].Value = request.RetornarRegistro ? "S" : "N";

            // Executa
            cm.ExecuteNonQuery();

            // Prepara o retorno
            DataTable tb = null;

            // Se pediu para retornar o registro...
            if (request.RetornarRegistro)
            {
            }

            // Retorna
            return tb;
        }

        #endregion


        //1349 ESTE PROCESSO DELETA TRANSACAO COM STATUS RETORNO CESSAO 2
        //public static OracleRefCursor PR_ESTORNO_CESSAO_S(OracleConnection cn, string codigoArquivoRetornoCessao)
        //{
        //    // Pega o comando
        //    OracleCommand cm =
        //        (OracleCommand)
        //            OracleDbLib2.Default.GetProcedureCommand("PR_ESTORNO_CESSAO_S");

        //    // Atribui os valores
        //    cm.Parameters[":pCODIGO_ARQUIVO_RET_CESSAO"].Value = codigoArquivoRetornoCessao;

        //    // Executa
        //    cm.ExecuteNonQuery();

        //    // Verifica se tem retorno
        //    OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

        //    // Retorna
        //    return orc;
        //}


        /// <summary>
        /// Realiza agendamento das transações de venda
        /// </summary>
        public static void PR_EXTRATO_PREPARAR_AGENDA(string codigoProcesso, bool regraAtacadao)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_EXTRATO_PREPARAR_AGENDA");

            // Atribui os valores
            cm.Parameters[":pCODIGO_PROCESSO"].Value = codigoProcesso;

            if (regraAtacadao == true)
                cm.Parameters[":pREGRA_ATACADAO"].Value = "S";
            else
                cm.Parameters[":pREGRA_ATACADAO"].Value = "N";


            // Executa
            cm.ExecuteNonQuery();

            // Finaliza
            cm.Connection.Dispose();
            cm.Dispose();
        }

        /// <summary>
        /// Objeto de sinalizacao para prevenir execução simultânea
        /// </summary>
        private static object sinalizador_PR_EXTRATO_AGENDAR = new object();

        /// <summary>
        /// Realiza agendamento das transações de venda
        /// </summary>
        public static void PR_EXTRATO_AGENDAR(string codigoProcesso, bool regraAtacadao)
        {
            // Sinaliza se um processo estiver esperando para execução da procedure

            // Serializa as chamadas para esta procedure
            lock (sinalizador_PR_EXTRATO_AGENDAR)
            {
                // Sinaliza que está executando a procedure
                LogArquivo log = new LogArquivo("rotina", "nome", "PR_EXTRATO_AGENDAR");

                // Bloco de controle
                try
                {
                    // Pega o comando
                    OracleCommand cm =
                        (OracleCommand)
                            OracleDbLib2.Default.GetProcedureCommand("PR_EXTRATO_AGENDAR");

                    // Atribui os valores
                    cm.Parameters[":pCODIGO_PROCESSO"].Value = codigoProcesso;

                    if (regraAtacadao == true)
                        cm.Parameters[":pREGRA_ATACADAO"].Value = "S";
                    else
                        cm.Parameters[":pREGRA_ATACADAO"].Value = "N";

                    // Executa
                    cm.ExecuteNonQuery();

                    // Finaliza
                    cm.Connection.Dispose();
                    cm.Dispose();
                }
                finally
                {
                    // Finaliza o log
                    log.Dispose();
                }
            }
        }

        #region PR_ARQUIVO_EXPORTACAO_GERAR

        /// <summary>
        /// Lista os itens de arquivo
        /// </summary>
        public static int PR_ARQUIVO_EXPORTACAO_GERAR(OracleConnection cn, string siglaRede, DateTime dataMovimento)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_ARQUIVO_EXPORTACAO_GERAR", cn);

            // Atribui os valores
            cm.Parameters[":pSIGLA_REDE"].Value = siglaRede;
            cm.Parameters[":pDATA_MOVIMENTO"].Value = dataMovimento;

            int retorno = 0;

            try
            {
                retorno = Convert.ToInt32(cm.ExecuteScalar());
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                cm.Dispose();
            }

            // Retorna
            return retorno;
        }

        #endregion


        #region PR_PRODUTO_PLANO_L

        /// <summary>
        /// Lista a relação entre Produto e Plano
        /// </summary>
        public static OracleRefCursor PR_PRODUTO_PLANO_L(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_PRODUTO_PLANO_L", cn);

            // Executa
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }

        #endregion

        #region PR_CAMPOS_ENVIO_CCI_L
        public static OracleRefCursor PR_CAMPOS_ENVIO_CCI_L(OracleConnection cn, PR_CAMPOS_ENVIO_CCI_L_Request request)
        {
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_CAMPOS_ENVIO_CCI_L", cn);

            // Atribui os valores
            if (request.CodigoCampoEnvioCCI > 0)
                cm.Parameters[":pCODIGO_CAMPO_ENVIO_CCI"].Value = request.CodigoCampoEnvioCCI;

            if (request.NomeCampo != null)
                cm.Parameters[":pNOME_CAMPO"].Value = request.NomeCampo;

            if (request.StatusEnvioCCI != null)
                cm.Parameters[":pSTATUS_ENVIO"].Value = request.StatusEnvioCCI;

            // Executa
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }
        #endregion

        /// <summary>
        /// 1328 Processa a atualizacao da cessao na transacao
        /// </summary>

        public static void PR_CESSIONA_S(int CodigoTransacao, int CodigoArqRetCessao, int CodigoProcRetCessao, int CodigoFavorecido, int CodigoContaFavorecido)
        {
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_CESSIONA_S", cn))
                {
                    cm.Parameters[":pCODIGO_TRANSACAO"].Value = CodigoTransacao;
                    cm.Parameters[":pCODIGO_ARQUIVO_RET_CESSAO"].Value = CodigoArqRetCessao;
                    cm.Parameters[":pCODIGO_PROCESSO_RET_CESSAO"].Value = CodigoProcRetCessao;
                    cm.Parameters[":pCODIGO_FAVORECIDO"].Value = CodigoFavorecido;
                    cm.Parameters[":pCODIGO_CONTA_FAVORECIDO"].Value = CodigoContaFavorecido;
                    cm.ExecuteNonQuery();
                    cm.Connection.Dispose();
                }
            }
        }
        /// <summary>
        /// 1328 Processa a atualizacao da cessao na transacao
        /// </summary>

        public static void PR_CESSIONA_LOG_S(int CodigoProcRetCessao)
        {
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_CESSIONA_LOG_S", cn))
                {
                    cm.Parameters[":pCODIGO_PROCESSO_RET_CESSAO"].Value = CodigoProcRetCessao;
                    cm.ExecuteNonQuery();
                    cm.Connection.Dispose();
                }
            }
        }
        #region PR_PAGAMENTO_A2

        /// <summary>
        /// Realiza agendamento das transações do extrato
        /// </summary>
        public static void PR_PAGAMENTO_A2()
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_PAGAMENTO_A2");

            // Atribui os valores
            cm.Parameters[":pDATA_PAGAMENTO_MAIOR"].Value = DateTime.Now;

            // Executa
            cm.ExecuteNonQuery();

            // Finaliza
            cm.Connection.Dispose();
            cm.Dispose();
        }

        #endregion

        #region Estorno de Extrato

        public static void PR_ESTORNO_EXTRATO_S(string CodigoProcesso)
        {
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_ESTORNO_EXTRATO_S", cn))
                {
                    cm.Parameters[":pCodigoProcesso"].Value = CodigoProcesso;
                    cm.ExecuteNonQuery();
                    cm.Connection.Dispose();
                }
            }
        }

        #endregion

        
        public static void PR_ESTORNO_RETORNO_CESSAO_S(string CodigoProcesso)
        {
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_ESTORNO_RETORNO_CESSAO_S", cn))
                {
                    cm.Parameters[":pCodigoProcessoCessao"].Value = CodigoProcesso;
                    cm.ExecuteNonQuery();
                    cm.Connection.Dispose();
                }
            }
        }

        #region Tratamento de desbloqueio

        /// <summary>
        /// Lista os itens bloqueados e aguardando processamento
        /// </summary>
        public static OracleRefCursor PR_ITEM_DESBLOQUEIO_L(OracleConnection cn)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_ITEM_DESBLOQUEIO_L", cn);


            // Executa
            cm.ExecuteNonQuery();

            // Verifica se tem retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            // Retorna
            return orc;
        }






        /// <summary>
        /// Alterar
        /// </summary>
        /// <returns>Quantidade de itens alterados</returns>
        public static int PR_PROCESSAR_DESBLOQUEADOS(OracleConnection cn, string CodigoArquivo, string CodigoProcesso)
        {
            // Pega o comando
            OracleCommand cm =
                (OracleCommand)
                    OracleDbLib2.Default.GetProcedureCommand("PR_PROCESSAR_DESBLOQUEADOS", cn);

            // Atribui os parametros
            cm.Parameters[":pCODIGO_ARQUIVO_DESTINO"].Value = CodigoArquivo;
            cm.Parameters[":pCODIGO_PROCESSO"].Value = CodigoProcesso;

            // Executa
            cm.ExecuteNonQuery();

            // Pega o retorno
            OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

            OracleDataReader reader = orc.GetDataReader();
            int quantidade = 0;
            if (reader.Read())
                quantidade = Convert.ToInt32(reader.GetDecimal(0));

            // Retorna a quantidade de registros alterada
            return quantidade;
        }

        #endregion


        public static bool PR_PROCESSO_ENVIADO_MATERA(string CodigoProcesso)
        {

            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_PROCESSO_ENVIADO_MATERA", cn))
                {
                    cm.Parameters[":pcodigo_processo"].Value = CodigoProcesso;

                    cm.ExecuteNonQuery();

                    OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

                    OracleDataReader dr = orc.GetDataReader();

                    return dr.Read();

                }
            }
        }

        public static void PR_SALDO_CONSOLIDADO_S(PR_SALDO_CONSOLIDADO_S_Request request)
        {
            // Atribui os valores
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_SALDO_CONSOLIDADO_S", cn))
                {
                    cm.Parameters[":PDATACALCULOSALDO"].Value = request.DataSaldo;
                    cm.ExecuteNonQuery();
                }
            }
        }

        //================================================================================================================================================
        public static void PR_TRATAR_FAVORECIDO(out string nomFav, out string CNPJFav, out int codContaFavorecido, out int codFavorecido, out int retorno, out string codBanco, out string codAgencia, out string codConta, int codProduto, int codEstabelecimento, string vReferencia) //SCF1339 e SCF1341
        {
            //================================================================================================================================================
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                //================================================================================================================================================
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_TRATAR_FAVORECIDO", cn))
                {
                    codContaFavorecido = 0;
                    codFavorecido = 0;
                    retorno = 0;
                    codBanco = "";//1341
                    codAgencia = "";//1341
                    codConta = ""; //1341
                    nomFav = ""; //1341
                    CNPJFav = ""; //1341

                    cm.Parameters[":pCODIGO_PRODUTO"].Value = codProduto;
                    cm.Parameters[":pCODIGO_ESTABELECIMENTO"].Value = codEstabelecimento;
                    cm.Parameters[":pREFERENCIA"].Value = vReferencia; //1339
                    cm.ExecuteNonQuery();

                    OracleDecimal orcContaFav = (OracleDecimal)cm.Parameters[":pCODIGO_CONTA_FAVORECIDO"].Value;
                    OracleDecimal orcCodFav = (OracleDecimal)cm.Parameters[":pCODIGO_FAVORECIDO"].Value;
                    OracleDecimal orcRetorno = (OracleDecimal)cm.Parameters[":pRETORNO"].Value;
                    codFavorecido = orcCodFav.ToInt32();
                    codContaFavorecido = orcContaFav.ToInt32();
                    retorno = orcRetorno.ToInt32();

                    OracleRefCursor retcur = (OracleRefCursor)cm.Parameters[":retcur"].Value;
                    OracleDataReader dr = retcur.GetDataReader();
                    if (dr.Read())
                    {
                        codBanco = dr["BANCO"].ToString();
                        codAgencia = dr["AGENCIA"].ToString();
                        codConta = dr["CONTA"].ToString();
                        nomFav = dr["NOME_FAVORECIDO"].ToString();
                        CNPJFav = dr["CNPJ_FAVORECIDO"].ToString(); 
                    }
                }
                //================================================================================================================================================
            }
            //================================================================================================================================================
        }
        //================================================================================================================================================

        public static void PR_VALIDAR_CESSAO_B(int CodProc, int CodArq, string Banco, string Agencia, string Conta, string CnpjFavorecido, string NsuHost, DateTime DataTransacao, string CnpjEstabelecimento, string CodAutorizacao, string NumeroParcela, bool FlagAtualizarCessionada, out int CodProcRetCessao, out string RetValidacao, out string ChaveItem, out string CodigoProduto, out string NomeProduto, out string Referencia, out string StsTransacao )
        {
            //===============================================================================================
            // Efetua a conexao
            //===============================================================================================
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_VALIDAR_CESSAO_B", cn))
                {
                    //===============================================================================================
                    // Inicializa variaveis de saida
                    //===============================================================================================
                    CodProcRetCessao = 0; 
                    RetValidacao = "";
                    ChaveItem = "";
                    CodigoProduto = ""; //1343
                    NomeProduto = ""; //1343
                    Referencia = ""; //1343
                    StsTransacao = ""; //SCF1701 ID 29
                    //===============================================================================================
                    // Envia parametros
                    //===============================================================================================
                    cm.Parameters[":PCODIGO_ARQUIVO"].Value = CodArq;
                    cm.Parameters[":PCODIGO_PROCESSO"].Value = CodProc;
                    cm.Parameters[":PBANCO"].Value = Banco;
                    cm.Parameters[":PAGENCIA"].Value = Agencia;
                    cm.Parameters[":PCONTA"].Value = Conta;
                    cm.Parameters[":PCNPJ_FAVORECIDO"].Value = CnpjFavorecido;
                    cm.Parameters[":PNSU_HOST"].Value = NsuHost;
                    cm.Parameters[":PDATA_TRANSACAO_FORMATADA"].Value = DataTransacao;
                    cm.Parameters[":PCNPJ_ESTABELECIMENTO"].Value = CnpjEstabelecimento;
                    cm.Parameters[":PCOD_AUTORIZACAO"].Value = CodAutorizacao;
                    cm.Parameters[":PNUMERO_PARCELA"].Value = NumeroParcela;
                    cm.Parameters[":PREPROCESSAR_NSU_CESSIONADO"].Value = FlagAtualizarCessionada == true ? 'S' : 'N';
                    //===============================================================================================
                    // Executa a procedure
                    //===============================================================================================
                    cm.ExecuteNonQuery();
                    //===============================================================================================
                    // Recebe retornos
                    //===============================================================================================
                    OracleDecimal PRET_CESSAO_ANT = (OracleDecimal)cm.Parameters[":PRET_CESSAO_ANT"].Value;
                    CodProcRetCessao = PRET_CESSAO_ANT.ToInt32();
                    
                    OracleRefCursor retcur = (OracleRefCursor)cm.Parameters[":retcur"].Value;
                    OracleDataReader dr = retcur.GetDataReader();
                    if (dr.Read())
                    {
                        RetValidacao = dr["RetValidacao"].ToString();
                        ChaveItem = dr["ChaveItem"].ToString();
                        CodigoProduto = dr["CodigoProduto"].ToString(); //1343
                        NomeProduto = dr["NomeProduto"].ToString(); //1343
                        Referencia = dr["Referencia"].ToString(); //1343
                        StsTransacao = dr["Status"].ToString(); //SCF1701 ID 29
                    }
                    //===============================================================================================
                    // Fechando leitura de retorno de cessao
                    //===============================================================================================
                    dr.Close();
                    dr.Dispose();
                    retcur.Dispose();
                    //===============================================================================================
                    // Fechando conexao da procedure
                    //===============================================================================================
                    cm.Connection.Dispose();
                    cm.Dispose();
                }
            }
        }

        /*1328
         
//1311        public static void PR_ATUALIZAR_TRANS_RET_CESSAO(out bool haInconsistencias, out int codProcessoRetornoCessao, out int validaTransacao, int atualizar, int codigoProcesso, int codigoArquivo, string banco, string agencia, string conta, string cnpjFavorecido, string nsuHost, DateTime dataTransacao, string cnpjEstabelecimento, string codAutorizacao, string numeroParcela, bool atualizaCessionadas) // SCF975 - Marcos Matsuoka - Marcos Matsuoka - DE: out bool haInconsistencias / Para: out int haInconsistencias / Inclusao: out int codProcessoRetornoCessao, out int validaTransacao, bool atualizar
        public static void PR_ATUALIZAR_TRANS_RET_CESSAO(out int codProcessoRetornoCessao, out string validaTransacao, int atualizar, int codigoProcesso, int codigoArquivo, string banco, string agencia, string conta, string cnpjFavorecido, string nsuHost, DateTime dataTransacao, string cnpjEstabelecimento, string codAutorizacao, string numeroParcela, bool atualizaCessionadas) //1311
        {
            // Atribui os valores
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_ATUALIZAR_TRANS_RET_CESSAO", cn))
                {
                    //1311 haInconsistencias = false;
                    codProcessoRetornoCessao = 0;
                    //13111 validaTransacao = 0;
                    validaTransacao = ""; //1311

                    cm.Parameters[":PCODIGO_ARQUIVO"].Value = codigoArquivo;
                    cm.Parameters[":PCODIGO_PROCESSO"].Value = codigoProcesso;
                    cm.Parameters[":PBANCO"].Value = banco;
                    cm.Parameters[":PAGENCIA"].Value = agencia;
                    cm.Parameters[":PCONTA"].Value = conta;
                    cm.Parameters[":PCNPJ_FAVORECIDO"].Value = cnpjFavorecido;
                    cm.Parameters[":PNSU_HOST"].Value = nsuHost;
                    cm.Parameters[":PDATA_TRANSACAO_FORMATADA"].Value = dataTransacao;
                    cm.Parameters[":PCNPJ_ESTABELECIMENTO"].Value = cnpjEstabelecimento;
                    cm.Parameters[":PCOD_AUTORIZACAO"].Value = codAutorizacao;
                    cm.Parameters[":PNUMERO_PARCELA"].Value = numeroParcela;
                    cm.Parameters[":PREPROCESSAR_NSU_CESSIONADO"].Value = atualizaCessionadas == true ? 'S' : 'N'; // SCF975 - Marcos Matsuoka - Marcos Matsuoka
                    cm.Parameters[":PATUALIZAR_CESSAO"].Value = atualizar; //SCF975 - Marcos Matsuoka

                    cm.ExecuteNonQuery();

                    //1311 OracleDecimal orcValCes = (OracleDecimal)cm.Parameters[":PVALIDAR_CESSAO"].Value; //SCF975 - Marcos Matsuoka 
                    //1311 validaTransacao = orcValCes.ToInt32(); //SCF975 - Marcos Matsuoka
                    
                    OracleDecimal orcRetCes = (OracleDecimal)cm.Parameters[":PRET_CESSAO_ANT"].Value; //SCF975 - Marcos Matsuoka
                    codProcessoRetornoCessao = orcRetCes.ToInt32(); //SCF975 - Marcos Matsuoka

                    OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;
                    OracleDataReader dr = orc.GetDataReader();
                    
                    if (dr.Read())
                        validaTransacao = dr["RETORNO"].ToString(); //1311
                        //1311 haInconsistencias = dr["RETORNO"].ToString().Equals("1");
                    dr.Close();
                    dr.Dispose();
                    orc.Dispose();
                }
            }
        }
        1328 */

        public static void PR_CANCELAMENTO_CP_AP(PR_CANCELAMENTO_CP_AP_L_Request request)
        {
            // Atribui os valores
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_CANCELAMENTO_CP_AP", cn))
                {
                    // Atribui os valores
                    if (request.CodigoArquivo != 0l) //if (request.CodigoArquivo != null) //warning 25/02/2019
                        cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;

                    if (!String.IsNullOrEmpty(request.NSU))
                        cm.Parameters[":pNSU"].Value = request.NSU;

                    if (!String.IsNullOrEmpty(request.Data))
                        cm.Parameters[":pDATA"].Value = request.Data;

                    // Executa
                    cm.ExecuteNonQuery();
                }
            }
        }

        public static void PR_CANCELAMENTO_CV_AV(PR_CANCELAMENTO_CV_AV_L_Request request)
        {
            // Atribui os valores
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_CANCELAMENTO_CV_AV", cn))
                {
                    // Atribui os valores
                    if (request.CodigoArquivo != 0)  //if (request.CodigoArquivo != null) //warning 25/02/2019
                        cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;

                    if (!String.IsNullOrEmpty(request.NSU))
                        cm.Parameters[":pNSU"].Value = request.NSU;

                    if (!String.IsNullOrEmpty(request.Data))
                        cm.Parameters[":pDATA"].Value = request.Data;

                    // Executa
                    cm.ExecuteNonQuery();
                }
            }
        }

        public static void PR_CANCELAMENTO_ONLINE(PR_CANCELAMENTO_ONLINE_Request request)
        {
            // Atribui os valores
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_CANCELAMENTO_ONLINE", cn))
                {
                    // Atribui os valores
                    if (request.CodigoArquivo != null)
                    {
                        cm.Parameters[":pCODIGO_PROCESSO"].Value = request.CodigoProcesso; //1391
                        cm.Parameters[":pCODIGO_ARQUIVO"].Value = request.CodigoArquivo;
                    }

                    // Executa
                    cm.ExecuteNonQuery();
                }
            }
        }

        //=============================================================================================================================================
        public static bool PR_VERIFICA_FAVORECIDO_BANCO(PR_VERIFICA_FAVORECIDO_BANCO_Request request)
        {
            //=============================================================================================================================================
            bool retorno = false;
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                //=============================================================================================================================================
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_VERIFICA_FAVORECIDO_BANCO", cn))
                {
                    cm.Parameters[":PCNPJ_FAVORECIDO"].Value = request.CNPJFavorecido;
                    cm.Parameters[":PBANCO"].Value = request.Banco;
                    cm.Parameters[":PAGENCIA"].Value = request.Agencia;
                    cm.Parameters[":PCONTA"].Value = request.Conta;
                    cm.Parameters[":PPRODUTO"].Value = request.Produto;
                    cm.Parameters[":PREFERENCIA"].Value = request.Referencia; //1339
                    cm.ExecuteNonQuery();
                    OracleRefCursor orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;
                    OracleDataReader dr = orc.GetDataReader();
                    while (dr.Read())
                        retorno = int.Parse(dr["contagem"].ToString()) > 0;
                }
                //=============================================================================================================================================
            }
            return retorno;
            //=============================================================================================================================================
        }
        //=============================================================================================================================================

        public static int PR_EXPURGO_ARQUIVO_ITEM(PR_EXPURGO_ARQUIVO_ITEM_Request request)
        {
            int cont = 0;
            OracleRefCursor orc = null;
            using (OracleConnection cn = Procedures.ReceberConexao())
            {
                using (OracleCommand cm = (OracleCommand)OracleDbLib2.Default.GetProcedureCommand("PR_EXPURGO_ARQUIVO_ITEM", cn))
                {
                    //cm.Parameters[":PCNPJ_FAVORECIDO"].Value = request.CNPJFavorecido;
                    //cm.Parameters[":PBANCO"].Value = request.Banco;
                    //cm.Parameters[":PAGENCIA"].Value = request.Agencia;
                    //cm.Parameters[":PCONTA"].Value = request.Conta;
                    //cm.Parameters[":PPRODUTO"].Value = request.Produto;

                    cm.ExecuteNonQuery();

                    orc = (OracleRefCursor)cm.Parameters[":retcur"].Value;

                    OracleDataReader dr = orc.GetDataReader();

                    while (dr.Read())
                        cont = int.Parse(dr["QTD"].ToString());
                }
            }

            return cont;
        }
    }
}