﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Request para executar procedure PR_TRANSACAO_NAO_CONCILIADA_L
    /// </summary>
    public class PR_TRANSACAO_NAO_CONCILIADA_L_Request
    {
        public string TipoProcesso { get; set; }
    }
}
