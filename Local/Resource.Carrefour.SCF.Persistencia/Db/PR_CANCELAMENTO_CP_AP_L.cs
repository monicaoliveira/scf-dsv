﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    public class PR_CANCELAMENTO_CP_AP_L_Request
    {
        /// <summary>
        /// NSU para pesquisa de CP
        /// </summary>
        public string NSU { get; set; }

        /// <summary>
        /// Data para pesquisa de CP
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Codigo do arquivo para pesquisa do CP
        /// </summary>
        public int CodigoArquivo { get; set; }
    }
}
