﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    public class PR_VERIFICA_FAVORECIDO_BANCO_Request
    {
        public string CNPJFavorecido { get; set; }
        public string Banco {get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string Produto { get; set; }
        public string Referencia { get; set; } //1338
    }
}
