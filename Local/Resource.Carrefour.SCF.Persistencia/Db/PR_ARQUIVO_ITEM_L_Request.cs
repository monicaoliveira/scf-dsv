﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Solicitação de lista de arquivo_item
    /// </summary>
    public class PR_ARQUIVO_ITEM_L_Request
    {
        public string CodigoArquivo { get; set; }
        public string CodigoArquivoItem { get; set; }
        public string TipoArquivoItem { get; set; }
        public string TipoCritica { get; set; }
        public string NomeCampo { get; set; }
        public string CodigoProcesso { get; set; }
        public bool FiltrarSemCritica { get; set; }
        public bool FiltrarComCritica { get; set; }
        public bool FiltrarSobAnalise { get; set; }
        public bool FiltrarNaoBloqueados { get; set; }
        public bool FiltrarApenasBloqueados { get; set; }
        public ArquivoItemStatusEnum? StatusArquivoItem1 { get; set; }
        public ArquivoItemStatusEnum? StatusArquivoItem2 { get; set; }
        public string Chave { get; set; }
        public string ChaveInicio { get; set; }
        public int MaxLinhas { get; set; }
        public int LimiteMinimo { get; set; }
        public int LimiteMaximo { get; set; }
        public bool FiltrarStatusCancelamentoOnline { get; set; }
        public bool ReceberPendenteDesbloqueio { get; set; }
        public DateTime? DataInclusaoInicial { get; set; } //1308
        public DateTime? DataInclusaoFinal { get; set; } //1308
    }
}
