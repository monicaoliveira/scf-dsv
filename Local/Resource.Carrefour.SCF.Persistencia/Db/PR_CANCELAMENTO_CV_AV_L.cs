﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    public class PR_CANCELAMENTO_CV_AV_L_Request
    {
        /// <summary>
        /// NSU para pesquisa de CV
        /// </summary>
        public string NSU { get; set; }

        /// <summary>
        /// Data para pesquisa de CV
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Codigo do arquivo para pesquisa do CV
        /// </summary>
        public int CodigoArquivo { get; set; }
    }
}
