﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Persistencia.Db
{
    public class PR_REL_SALDO_CONCILIACAO_L_Request
    {
        public DateTime FiltroDataConciliacaoInicio { get; set; }

        public DateTime FiltroDataConciliacaoFim { get; set; }
    }
}
