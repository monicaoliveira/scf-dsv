﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
//==============================================================================
// 1328 Arrumando o nome do .CS e os campos a serem enviados para a procedure
// Estava PR_TRATA_RETORNO_L
//==============================================================================
namespace Resource.Carrefour.SCF.Persistencia.Db
{
    /// <summary>
    /// Request para executar procedure PR_TRANSACAO_L
    /// </summary>
    public class PR_TRANSACAO_VENDA_L_Request
    {
        public string CodigoArquivo { get; set; }
        public string CodigoProcesso { get; set; }
        public string TipoTransacao { get; set; }
        public DateTime? DataInclusao { get; set; }
        public string CodigoArquivoRetornoCessao { get; set; }
        public TransacaoStatusConciliacaoEnum? StatusConciliacao { get; set; }
        public string Chave { get; set; }
        public string Chave2 { get; set; }
        public bool InformarOrigem { get; set; }
        public string CodigoPlano { get; set; }
        
    }
}
