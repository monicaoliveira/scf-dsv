﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
//==============================================================================
// 1328 Arrumando o nome do .CS e os campos a serem enviados para a procedure
// Estava PR_TRANSACAO_VENDA_L
//==============================================================================
namespace Resource.Carrefour.SCF.Persistencia.Db
{
    public class PR_CESSIONA_S_Request
    {
        public string CodigoTransacao { get; set; }
        public string CodigoArquivoRetornoCessao { get; set; }
        public string CodigoProcessoRetornoCessao { get; set; }
        public string CodigoFavorecido { get; set; }
        public string CodigoContaFavorecido { get; set; }
    }
}
