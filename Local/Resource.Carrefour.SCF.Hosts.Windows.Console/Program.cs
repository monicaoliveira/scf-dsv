﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Framework.Sistemas.Comum;
using Resource.Framework.Library.Db.Oracle;
using System.Reflection;
using Resource.Carrefour.SCF.Sistemas.Principal;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum;

namespace Resource.Carrefour.SCF.Hosts.Server.Console
{
    class Program
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            System.Console.WriteLine("Inicializando...");
            log.Info("Inicializando console...");
            inicializarFramework();
            System.Console.WriteLine("Servidor iniciado");
            System.Console.WriteLine("Pressione qualquer tecla para finalizar");
            log.Info("Sistema inicializado");
            System.Console.ReadKey();

            log.Info("Finalizando sistema");
            System.Console.WriteLine("Finalizando...");
            finalizarFramework();
            System.Console.WriteLine("Servidor finalizado");
            System.Console.WriteLine("Pressione qualquer tecla para sair");
            log.Info("Sistema finalizado");
            System.Console.ReadKey();
        }

        private static void finalizarFramework()
        {
            ContainerServicoHost.GetInstance().Finalizar();
        }

        private static void inicializarFramework()
        {
            // Localizador de tipos
            GerenciadorConfig.SetarConfig(
                new LocalizadorTiposConfig()
                {
                    ListaFixa =
                        new List<LocalizadorTipoInfo>()
                        {
                            new LocalizadorTipoInfo()
                            {
                                IncluirNamespace =
                                    "Resource.Framework.Contratos.Comum.Dados, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Framework.Contratos.Comum.Mensagens, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Dados.Log, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador, Resource.Carrefour.SCF.Contratos.Principal;" +                                    
                                    "Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador, Resource.Carrefour.SCF.Contratos.Principal;" +                                    
                                    "Resource.Carrefour.SCF.Contratos.Principal.Mensagens, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Integracao.Dados, Resource.Carrefour.SCF.Contratos.Integracao;" +
                                    "Resource.Carrefour.SCF.Contratos.Integracao.Mensagens, Resource.Carrefour.SCF.Contratos.Integracao;" + 
                                    "Resource.Carrefour.SCF.Contratos.Principal.Permissoes, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Framework.Contratos.Comum.Permissoes, Resource.Framework.Contratos.Comum"
                            }
                        }
                    });

            // Configuracoes de persistencia
            GerenciadorConfig.SetarConfig(
                new ServicoPersistenciaConfig()
                {
                    Persistencias =
                        new List<PersistenciaInfo>()
                        {
                            new PersistenciaInfo() 
                            {
                                Default = true,
                                TipoPersistencia = typeof(PersistenciaDb)
                            }
                        }
                });

            // Configuracoes de seguranca
            GerenciadorConfig.SetarConfig(
                new ServicoSegurancaConfig()
                {
                    NomeUsuarioAdministrador = "Admin",
                    NamespacesPermissoes =
                        new List<string>() 
                        {
                            "Resource.Framework.Contratos.Comum.Permissoes, Resource.Framework.Contratos.Comum",
                            "Resource.Carrefour.SCF.Contratos.Principal.Permissoes, Resource.Carrefour.SCF.Contratos.Principal"
                        },
                    ValidarSenhas = false
                });

            // Configuracoes do resolutor de tipos
            GerenciadorConfig.SetarConfig(
                new ResolutorTiposConfig()
                {
                    AprofundarNamespaces = true,
                    IncluirNamespaces =
                        new List<string>() 
                        { 
                            "Resource.Carrefour.SCF.Sistemas.Principal.Processos, Resource.Carrefour.SCF.Sistemas.Principal",
                            "Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos, Resource.Carrefour.SCF.Sistemas.Integracao",
                            "Resource.Carrefour.SCF.Contratos.Principal.Dados, Resource.Carrefour.SCF.Contratos.Principal"
                        }
                });

            // Inicia os servicos
            ContainerServicoHost containerServicoHost = ContainerServicoHost.GetInstance();
            containerServicoHost.Iniciar(
                new ContainerServicoHostConfig()
                {
                    ServicoHostConfig =
                        new ServicoHostConfig()
                        {
                            ExporWCF = true,
                            TipoVarreduraTipos = ServicoHostTipoVarreduraEnum.DefaultIncluir,
                            IncluirTipos =
                                new List<Type>() 
                                { 
                                    typeof(ServicoSeguranca),
                                    typeof(ServicoPersistencia),
                                    typeof(ServicoMetadadoComum)
                                },
                            IncluirAssemblies =
                                new List<Assembly>()
                                {
                                    typeof(ServicoPrincipal).Assembly,
                                    typeof(ServicoIntegracao).Assembly
                                }
                        }
                });

            // Pede inicializacao da seguranca
            Mensageria.Processar(new InicializarSegurancaRequest());
        }
    }
}
