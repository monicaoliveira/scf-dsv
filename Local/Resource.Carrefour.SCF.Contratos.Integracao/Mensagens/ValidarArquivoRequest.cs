﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Xml.Serialization;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de validação de arquivo
    /// </summary>
    public class ValidarArquivoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a validar
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Permite passar informações para validação
        /// </summary>
        [XmlIgnore]
        public Dictionary<string, object> Contexto { get; set; }

        ///// <summary>
        ///// Grupo do arquivo
        ///// </summary>
        //public string Grupo { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ValidarArquivoRequest()
        {
            this.Contexto = new Dictionary<string, object>();
        }
    }
}
