﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using System.IO;
using Resource.Carrefour.SCF.Contratos.Principal;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    [Serializable]
    public class MontarArquivoMateraRequest : MensagemRequestBase
    {
        
        public PagamentoDetalheView PagamentoInfo { get; set; }


        public ProcessoExtratoValidacaoConfig Config { get; set; }


        public Object ArquivoMatera { get; set; }


        public StreamWriter FileStream { get; set; }


        public int QtdeRegistrosGerados { get; set; } //1447 estava inibido


        public int QtdeRegistros { get; set; }//1447 estava inibido


        public double ValorTotal { get; set; }//1447 estava inibido
    }
}
