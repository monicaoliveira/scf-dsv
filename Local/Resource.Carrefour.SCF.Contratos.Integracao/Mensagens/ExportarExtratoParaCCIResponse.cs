﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem resposta a uma solicitação de exportacao do extrato para a CCI
    /// </summary>
//    public class ExportarExtratoParaCCIResponse : MensagemResponseBase
    public class ExportarExtratoParaGrupoResponse : MensagemResponseBase
    {
    }
}
