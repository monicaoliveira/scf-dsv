﻿//SCF963 - Marcos Matsuoka
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de salvar detalhe de arquivo item
    /// </summary>
    public class SalvarProtocoloChequeResponse : MensagemResponseBase
    {
        /// <summary>
        /// ArquivoItem salvo
        /// </summary>
        public ProcessoProtocoloChequeInfo ProcessoProtocoloChequeInfo { get; set; }
    }
}
