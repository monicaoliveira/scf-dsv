﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de alteracao de status em lote de arquivoItens
    /// </summary>
    public class AlterarStatusArquivoItemResponse : MensagemResponseBase
    {
        /// <summary>
        /// Indica a quantidade de registros alterados
        /// </summary>
        public int QuantidadeRegistrosAlterados { get; set; }
    }
}
