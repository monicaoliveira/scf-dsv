﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de listagem do relatório de transações
    /// </summary>
    [Serializable]
    public class ListarRelatorioTransacaoRequest : MensagemRequestBase
    {      
        
        /// <summary>
        /// Filtro por origem
        /// </summary>
        public string FiltroOrigem { get; set; }
        
        /// <summary>
        /// Filtro por grupo
        /// </summary>
        public string FiltroEmpresaGrupo { get; set; }

        /// <summary>
        /// Filtro por subgrupo
        /// </summary>
        public string FiltroEmpresaSubgrupo { get; set; }

        /// <summary>
        /// Filtro por estabelecimento
        /// </summary>
        public string FiltroEstabelecimento { get; set; }

        /// <summary>
        /// Filtro por favorecido
        /// </summary>
        public string FiltroFavorecido { get; set; }

        /// <summary>
        /// Filtro por produto
        /// </summary>
        public string FiltroProduto { get; set; }

        /// <summary>
        /// Filtro por plano
        /// </summary>
        public string FiltroPlano { get; set; }

        /// <summary>
        /// Filtro por meio de captura
        /// </summary>
        public string FiltroMeioCaptura { get; set; }

        /// <summary>
        /// Filtro por tipo de registro
        /// </summary>
        public string FiltroTipoRegistro { get; set; }

        /// <summary>
        /// Filtro por tipo de transação
        /// </summary>
        public string FiltroTipoTransacao { get; set; }

        /// <summary>
        /// Filtro por numero de pagamento
        /// </summary>
        public string FiltroNumeroPagamento { get; set; }
        public string FiltroContaCliente { get; set; } //1438

        /// <summary>
        /// Filtro por data de transação (inicio)
        /// </summary>
        public DateTime? FiltroDataTransacaoDe { get; set; }

        /// <summary>
        /// Filtro por data de transação (fim)
        /// </summary>
        public DateTime? FiltroDataTransacaoAte { get; set; }

        /// <summary>
        /// Filtro por data de transação (inicio)
        /// </summary>
        public DateTime? FiltroDataTransacao { get; set; }

        /// <summary>
        /// Filtro por data de processamento (inicio)
        /// </summary>
        public DateTime? FiltroDataProcessamentoDe { get; set; }

        /// <summary>
        /// Filtro por data de processamento (inicio)
        /// </summary>
        public DateTime? FiltroDataProcessamento { get; set; }

        /// <summary>
        /// Filtro por data de processamento (fim)
        /// </summary>
        public DateTime? FiltroDataProcessamentoAte { get; set; }

        /// <summary>
        /// Filtro por data de vencimento (inicio)
        /// </summary>
        public DateTime? FiltroDataVencimentoDe { get; set; }

        /// <summary>
        /// Filtro por data de vencimento (inicio)
        /// </summary>
        public DateTime? FiltroDataVencimento { get; set; }

        /// <summary>
        /// Filtro por data de vencimento (fim)
        /// </summary>
        public DateTime? FiltroDataVencimentoAte { get; set; }

        /// <summary>
        /// Filtro por data de processamento (inicio)
        /// </summary>
        public DateTime? FiltroDataMovimentoDe { get; set; }

        /// <summary>
        /// Filtro por data de processamento
        /// </summary>
        public DateTime? FiltroDataMovimento { get; set; }

        /// <summary>
        /// Filtro por data de processamento (fim)
        /// </summary>
        public DateTime? FiltroDataMovimentoAte { get; set; }
        
        /// <summary>
        /// Filtro por data de agendamento (inicio)
        /// </summary>
        public DateTime? FiltroDataAgendamentoDe { get; set; }

        /// <summary>
        /// Filtro por data de agendamento (inicio)
        /// </summary>
        public DateTime? FiltroDataAgendamento { get; set; }

        /// <summary>
        /// Filtro por data de agendamento (fim)
        /// </summary>
        public DateTime? FiltroDataAgendamentoAte { get; set; }

        /// <summary>
        /// Filtro por data de envio para matera (inicio)
        /// </summary>
        public DateTime? FiltroDataEnvioMateraDe { get; set; }

        /// <summary>
        /// Filtro por data de envio para matera (inicio)
        /// </summary>
        public DateTime? FiltroDataEnvioMatera { get; set; }

        /// <summary>
        /// Filtro por data de envio para matera (fim)
        /// </summary>
        public DateTime? FiltroDataEnvioMateraAte { get; set; }

        /// <summary>
        /// Filtro por data de liquidação (inicio)
        /// </summary>
        public DateTime? FiltroDataLiquidacaoDe { get; set; }

        /// <summary>
        /// Filtro por data de liquidação (inicio)
        /// </summary>
        public DateTime? FiltroDataLiquidacao { get; set; }

        /// <summary>
        /// Filtro por data de liquidação (fim)
        /// </summary>
        public DateTime? FiltroDataLiquidacaoAte { get; set; }

        /// <summary>
        /// Filtro por status do retorno
        /// </summary>
        public string FiltroStatusRetorno { get; set; }

        /// <summary>
        /// Filtro por status do pagamento
        /// </summary>
        public string FiltroStatusPagamento { get; set; }

        /// <summary>
        /// Filtro por status da conciliação
        /// </summary>
        public string FiltroStatusConciliacao { get; set; }

        /// <summary>
        /// Filtro por status da validação
        /// </summary>
        public string FiltroStatusValidacao { get; set; }

        /// <summary>
        /// Filtro por status de cancelamento On Line
        /// NR-033
        /// </summary>
        public string FiltroStatusCancelamentoOnLine { get; set; }

        /// <summary>
        /// Data de retorno de cessao (inicio)
        /// NR-033
        /// </summary>
        public DateTime? FiltroDataRetornoCessaoDe { get; set; }

        /// <summary>
        /// Data de retorno de cessao (fim)
        /// NR-033
        /// </summary>
        public DateTime? FiltroDataRetornoCessaoAte { get; set; }

        /// <summary>
        /// Filtro por status da validação
        /// </summary>
        public int? MaxLinhas { get; set; }

        /// <summary>
        /// Filtro por status da validação
        /// </summary>
        public bool VerificarQuantidadeLinhas { get; set; }

        /// <summary>
        /// Filtro por tipo financeiro ou contábil
        /// ECOMMERCE - Fernando Bove - 20160105
        /// </summary>
        public string FiltroFinanceiroContabil { get; set; }

        /// <summary>
        /// Filtro por NSU Host
        /// ECOMMERCE - Fernando Bove - 20160106
        /// </summary>
        public string FiltroNSUHost { get; set; }

        /// <summary>
        /// Filtro por Referencia
        /// ClockWork - Marcos Matsuoka
        /// </summary>
        public string FiltroReferencia { get; set; }
    }
}
