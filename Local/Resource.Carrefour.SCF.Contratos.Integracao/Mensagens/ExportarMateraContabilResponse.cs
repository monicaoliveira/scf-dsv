﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de exportação do arquivo matera contábil
    /// </summary>
    public class ExportarMateraContabilResponse : MensagemResponseBase
    {
    }
}
