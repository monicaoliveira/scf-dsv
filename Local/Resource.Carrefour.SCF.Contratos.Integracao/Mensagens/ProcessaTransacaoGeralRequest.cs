﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de request do metodo de processar transacao de qualquer tipo
    /// </summary>
    public class ProcessaTransacaoGeralRequest : MensagemRequestBase
    {
        /// <summary>
        /// Linha completa
        /// </summary>
        public string Linha { get; set; }

        /// <summary>
        /// Codigo do item de arquivo
        /// </summary>
        public int CodigoArquivoItem { get; set; }

        /// <summary>
        /// Status do arquivo item
        /// </summary>
        public int StatusArquivoItem { get; set; }

        /// <summary>
        /// Tipo da transacao
        /// </summary>
        public string TipoTransacao { get; set; }

        /// <summary>
        /// Codigo do arquivo
        /// </summary>
        public int CodigoArquivo { get; set; }

        /// <summary>
        /// Codigo do processo
        /// </summary>
        public int CodigoProcesso { get; set; }

        /// <summary>
        /// Status da transacao
        /// </summary>
        public TransacaoStatusEnum StatusTransacao { get; set; }

        /// <summary>
        /// Status do retorno de cessao da transacao
        /// </summary>
        public TransacaoStatusRetornoCessaoEnum StatusRetornoCessaoTransacao { get; set; }

        /// <summary>
        /// Dicionario de planos
        /// </summary>
        public Dictionary<string, int> DicionarioPlano { get; set; }

        /// <summary>
        /// Dicionario de referencia de produtos AV
        /// </summary>
        public Dictionary<string, string> DicionarioReferenciaProdutoAV { get; set; }

        /// <summary>
        /// Dicionario de referencias de produtos AJ
        /// </summary>
        public Dictionary<string, string> DicionarioReferenciaProdutoAJ { get; set; }

        /// <summary>
        /// Dicionario de estabelecimentos
        /// </summary>
        public Dictionary<string, int> DicionarioEstabelecimento { get; set; }

        /// <summary>
        /// Dicionario de produtos
        /// </summary>
        public Dictionary<string, ProdutoInfo> DicionarioProduto { get; set; }

        /// <summary>
        /// Indica se a importacao deve alterar as informacoes de banco, agencia e conta
        /// Utilizado no processo do atacadao que deve
        /// </summary>
        public bool AlterarBancoAgenciaConta { get; set; }

        /// <summary>
        /// codigo do produto no ajuste
        /// </summary>
        public int? CodigoProdutoAjuste { get; set; }

        /// <summary>
        /// Codigo do produto na anulacao de venda
        /// </summary>
        public int? CodigoProdutoAnulacaoVenda { get; set; }

        /// <summary>
        /// codigo do produto na anulacao do pagamento
        /// </summary>
        public int? CodigoProdutoAnulacaoPagamento { get; set; }

        /// <summary>
        /// codigo do produto no pagamento
        /// </summary>
        public int? CodigoProdutoPagamento { get; set; }

        /// <summary>
        /// codigo do produto na venda
        /// </summary>
        public int? CodigoProdutoVenda { get; set; }

        /// <summary>
        /// Indica se deve traduzir o plano
        /// </summary>
        public bool TraduzirPlano { get; set; }

        /// <summary>
        /// Indica o tipo do processo
        /// </summary>
        public string TipoProcesso { get; set; }
    }
}
