﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de listagem do relatório de transações
    /// </summary>
    [Serializable]
    public class ListarRelatorioTransacaoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<RelatorioTransacaoInfo> Resultado { get; set; }

        /// <summary>
        /// Quantidade de linhas
        /// </summary>
        public int QuantidadeLinhas { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarRelatorioTransacaoResponse()
        {
            this.Resultado = new List<RelatorioTransacaoInfo>();
        }
    }
}
