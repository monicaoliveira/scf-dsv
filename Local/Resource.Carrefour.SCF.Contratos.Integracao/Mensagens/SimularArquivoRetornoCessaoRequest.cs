﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// 
    /// </summary>
    public class SimularArquivoRetornoCessaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a ser exportado
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Caminho do arquivo a ser gerado
        /// </summary>
        public string CaminhoArquivo { get; set; }
    }
}
