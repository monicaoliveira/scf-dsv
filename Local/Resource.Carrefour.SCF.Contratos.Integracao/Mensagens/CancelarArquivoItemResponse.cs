﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta do cancelamento de arquivo itens
    /// </summary>
    public class CancelarArquivoItemResponse : MensagemResponseBase
    {
        /// <summary>
        /// Codigo do item de arquivo que foi cancelado
        /// </summary>
        public string CodigoArquivoItemCancelado { get; set; }

        /// <summary>
        /// Código do arquivo que teve os itens cancelados
        /// </summary>
        public string CodigoArquivoCancelado { get; set; }
    }
}
