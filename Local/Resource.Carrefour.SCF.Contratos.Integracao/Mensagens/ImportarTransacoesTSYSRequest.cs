﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de importação de transacoes TSYS através de um arquivo
    /// </summary>
    public class ImportarTransacoesTSYSRequest : MensagemRequestBase
    {
        /// <summary>s
        /// Código do arquivo a ser lido
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do processo que está solicitando a importação das transacoes
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Indica se a importacao deve alterar as informacoes de banco, agencia e conta
        /// Utilizado no processo do atacadao que deve
        /// </summary>
        public bool AlterarBancoAgenciaConta { get; set; }

        /// <summary>
        /// Caso seja diferente de nulo, serve como um filtro de lojas a serem
        /// incluidas, separadas por ;
        /// </summary>
        public string IncluirLojas { get; set; }

        /// <summary>
        /// Flag para identificar se é para efetuar a traducoa do codigo do produto
        /// </summary>
        public bool TraduzirProduto { get; set; }

        /// <summary>
        /// Flag para identificar se é para efetuar a traducoa do codigo do plano
        /// </summary>
        public bool TraduzirPlano { get; set; }

        /// <summary>
        ///  Informa o tipo de processo. Dentro da rotina, precisamos saber se é atacadao ou extrato
        ///  Atacadao ou Extrato
        /// </summary>
        public string TipoProcesso { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ImportarTransacoesTSYSRequest()
        {
            this.AlterarBancoAgenciaConta = true;
        }
    }
}
