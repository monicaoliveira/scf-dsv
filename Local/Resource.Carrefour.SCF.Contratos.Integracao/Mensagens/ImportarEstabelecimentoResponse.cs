﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de importação de estabelecimento
    /// </summary>
    [Serializable]
    public class ImportarEstabelecimentoResponse : MensagemResponseBase
    {
        public bool ErroLayout { get; set; }
    }
}
