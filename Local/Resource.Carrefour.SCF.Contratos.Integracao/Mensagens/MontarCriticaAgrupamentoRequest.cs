﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de montagem do agrupamento de críticas
    /// </summary>
    public class MontarCriticaAgrupamentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro pelo código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Filtro pelo código do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Indica se o resultado deve ser em forma de árvore
        /// </summary>
        public Boolean RetornarArvore { get; set; }
    }
}
