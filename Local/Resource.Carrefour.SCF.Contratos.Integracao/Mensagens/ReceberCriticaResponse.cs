﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library.Servicos.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de detalhe de crítica
    /// </summary>
    public class ReceberCriticaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Crítica solicitada
        /// </summary>
        public CriticaInfo CriticaInfo { get; set; }
    }
}
