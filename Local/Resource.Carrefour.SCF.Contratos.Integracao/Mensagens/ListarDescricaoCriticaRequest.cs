﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de descricao de criticas
    /// </summary>
    public class ListarDescricaoCriticaRequest : MensagemRequestBase
    {
    }
}
