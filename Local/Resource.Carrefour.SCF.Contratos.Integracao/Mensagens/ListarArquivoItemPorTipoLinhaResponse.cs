﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta à solicitação de lista de arquivo item
    /// </summary>
    [Serializable]
    public class ListarArquivoItemPorTipoLinhaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado encontrado
        /// </summary>
        public List<ArquivoItemResumoInfo> Resultado { get; set; }

        /// <summary>
        /// Cabecalho dos valores dos itens
        /// </summary>
        public string[] Cabecalho { get; set; }
    }
}
