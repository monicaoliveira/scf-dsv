﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de importar transacoes sitef
    /// </summary>
    public class ImportarTransacoesSitefResponse : MensagemResponseBase
    {
    }
}
