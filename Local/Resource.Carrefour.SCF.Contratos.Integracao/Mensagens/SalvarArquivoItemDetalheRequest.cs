﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação para salvar detalhe de arquivo item
    /// </summary>
    public class SalvarArquivoItemDetalheRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo item a ser alterado
        /// </summary>
        public string CodigoArquivoItem { get; set; }

        /// <summary>
        /// Nomes dos campos que terão seus valores alterados
        /// </summary>
        public string[] NomeCampos { get; set; }

        /// <summary>
        /// Valores a serem alterados
        /// </summary>
        public string[] Valores { get; set; }

        /// <summary>
        /// Status do arquivoItem
        /// </summary>
        public ArquivoItemStatusEnum? StatusArquivoItem { get; set; }

        /// <summary>
        /// Referencia BIN 
        /// </summary>
        public string BinReferenciaArquivo { get; set; }
    }
}
