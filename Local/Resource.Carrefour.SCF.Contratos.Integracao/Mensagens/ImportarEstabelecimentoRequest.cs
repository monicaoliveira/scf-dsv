﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de importação de estabelecimentos
    /// </summary>
    [Serializable]
    public class ImportarEstabelecimentoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo de estabelecimentos
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Caminho do diretorio
        /// </summary>
        public string CaminhoArquivo { get; set; }  
    }
}
