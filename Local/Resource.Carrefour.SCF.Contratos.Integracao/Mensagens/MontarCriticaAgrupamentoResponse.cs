﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de montagem do agrupamento de críticas
    /// </summary>
    public class MontarCriticaAgrupamentoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista dos agrupamentos
        /// </summary>
        public List<CriticaAgrupamentoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public MontarCriticaAgrupamentoResponse()
        {
            this.Resultado = new List<CriticaAgrupamentoInfo>();
        }
    }
}
