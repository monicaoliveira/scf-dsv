﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de críticas
    /// </summary>
    public class ListarCriticaRequest : MensagemRequestBase
    {
        public string FiltroCodigoProcesso { get; set; }
        public string FiltroCodigoArquivo { get; set; }
        public string FiltroCodigoArquivoItem { get; set; }
        public string FiltroTipoCritica { get; set; }
        public string FiltroTipoLinha { get; set; }
        public string FiltroNomeCampo { get; set; }
        public string FiltroDescricaoCritica { get; set; } // 1282
        public DateTime? FiltroDataCriticaInicial { get; set; }
        public DateTime? FiltroDataCriticaFinal { get; set; }
        public bool RetornarAgrupado { get; set; }

        public int MaxLinhas { get; set; }

        public ListarCriticaRequest()
        {
            this.MaxLinhas = 200;
        }
    }
}
