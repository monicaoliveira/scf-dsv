﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de importação de arquivo
    /// </summary>
    public class IniciarProcessamentoDesbloqueiosRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do processo ao qual os desbloqueios serao incorporados
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Código do arquivo ao qual os desbloqueios serao incorporados
        /// </summary>
        public string CodigoArquivo { get; set; }
    }
}
