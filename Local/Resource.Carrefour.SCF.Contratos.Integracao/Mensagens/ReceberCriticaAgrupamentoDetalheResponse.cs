﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitacao de detalhe de agrupamento de critica
    /// </summary>
    public class ReceberCriticaAgrupamentoDetalheResponse : MensagemResponseBase
    {
        /// <summary>
        /// Detalhe de agrupamento de crítica
        /// </summary>
        public CriticaAgrupamentoDetalheInfo CriticaAgrupamentoDetalheInfo { get; set; }
    }
}
