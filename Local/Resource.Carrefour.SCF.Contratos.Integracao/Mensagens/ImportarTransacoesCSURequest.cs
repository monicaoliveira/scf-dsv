﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de importar transacoes csu
    /// </summary>
    public class ImportarTransacoesCSURequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a ser importado
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do processo que está solicitando a importação das transacoes
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Caso seja diferente de nulo, serve como um filtro de lojas a serem
        /// incluidas, separadas por ;
        /// </summary>
        public string IncluirLojas { get; set; }
    }
}
