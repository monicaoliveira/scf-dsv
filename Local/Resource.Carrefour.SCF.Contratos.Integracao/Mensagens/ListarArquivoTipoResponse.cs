﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de lista de tipos de arquivos
    /// </summary>
    public class ListarArquivoTipoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de tipos de arquivos
        /// </summary>
        public List<ArquivoTipoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarArquivoTipoResponse()
        {
            this.Resultado = new List<ArquivoTipoInfo>();
        }
    }
}
