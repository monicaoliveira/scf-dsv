﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta de processar transacao de qualquer tipo
    /// </summary>
    public class ProcessaTransacaoGeralResponse : MensagemResponseBase
    {
        /// <summary>
        /// Indica se há inconsistências no processo de retorno de cessão
        /// </summary>
        public bool HaInconsistencias { get; set; }

        /// <summary>
        /// Indica se deve atualizar o numero da transacao
        /// </summary>
        public bool ContaTransacao { get; set; }
    }
}
