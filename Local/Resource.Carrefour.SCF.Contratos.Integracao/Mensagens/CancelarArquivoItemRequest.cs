﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de request para o cancelamento de itens de arquivos
    /// </summary>
    public class CancelarArquivoItemRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do arquivo item a ser cancelado
        /// </summary>
        public string CodigoArquivoItem { get; set; }

        /// <summary>
        /// Código do arquivo que deverá ter os itens cancelados
        /// </summary>
        public string CodigoArquivo { get; set; }
    }
}
