﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    public class AtualizarEmMassaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Codigo do processo
        /// </summary>
        public string CodigoProcesso { get; set; }
        /// <summary>
        /// Tipo da linha
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Filtro por tipo de crítica
        /// </summary>
        public string TipoCritica { get; set; }

        /// <summary>
        /// Filtro por nome de campo de crítica
        /// </summary>
        public string NomeCampo { get; set; }

        public string[] NovosValores { get; set; }

        public string[] NomesCampos { get; set; }

        public string[] TiposLinhas { get; set; }

        public string[] DescricoesCriticas { get; set; } //1282

        public string[] TiposCriticas { get; set; } //1282
    }
}

