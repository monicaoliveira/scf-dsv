﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de atualizar o retorno de cessao nas transacoes
    /// </summary>
    public class AtualizarTransacoesRetornoCessaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a ser importado
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do processo que está solicitando a importação das transacoes
        /// </summary>
        public string CodigoProcesso { get; set; }

    }
}
