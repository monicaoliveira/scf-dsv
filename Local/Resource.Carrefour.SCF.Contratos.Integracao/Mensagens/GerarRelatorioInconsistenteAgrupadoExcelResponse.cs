﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a solicitação de relatório de transação inconsistente em excel
    /// </summary>
    [Serializable]
    public class GerarRelatorioInconsistenteAgrupadoExcelResponse : MensagemResponseBase
    {
        /// <summary>
        /// Indica o caminho do arquivo gerado
        /// </summary>
        public string CaminhoArquivoGerado { get; set; }

        /// <summary>
        /// Quantidade de linhas geradas no relatorio
        /// </summary>
        public int QuantidadeLinhasGeradas { get; set; }

        /// <summary>
        /// Indica o código do processo gerado
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
