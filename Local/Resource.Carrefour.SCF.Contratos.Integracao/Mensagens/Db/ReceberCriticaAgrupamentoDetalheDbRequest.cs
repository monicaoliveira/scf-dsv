﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db
{
    /// <summary>
    /// Mensagem de solicitação de persistencia de detalhe de agrupamento de crítica
    /// </summary>
    public class ReceberCriticaAgrupamentoDetalheDbRequest : ReceberObjetoRequest<CriticaAgrupamentoDetalheInfo>
    {
        /// <summary>
        /// Código do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Tipo da Linha
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Tipo da Crítica
        /// </summary>
        public string TipoCritica { get; set; }

        /// <summary>
        /// Nome do campo
        /// </summary>
        public string NomeCampo { get; set; }
    }
}
