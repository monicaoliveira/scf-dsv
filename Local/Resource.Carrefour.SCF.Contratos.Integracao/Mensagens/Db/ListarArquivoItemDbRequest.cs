﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db
{
    /// <summary>
    /// Mensagem de persistencia para lista de ArquivoItem
    /// </summary>
    public class ListarArquivoItemDbRequest : ConsultarObjetosRequest<ArquivoItemResumoInfo>
    {
        public bool IncluirConteudo { get; set; }
        public bool FiltrarApenasBloqueados { get; set; }
        public bool FiltrarComCritica { get; set; }
        public bool FiltrarSemCritica { get; set; }
        public bool FiltrarApenasSobAnalise { get; set; }
        public DateTime? FiltroDataInclusaoInicial { get; set; } //1308
        public DateTime? FiltroDataInclusaoFinal { get; set; } //1308
    }
}
