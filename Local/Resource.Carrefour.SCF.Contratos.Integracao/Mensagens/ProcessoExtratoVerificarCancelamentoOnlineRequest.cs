﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    public class ProcessoExtratoVerificarCancelamentoOnlineRequest : MensagemRequestBase
    {
        public string CodigoArquivo { get; set; }
        public string CodigoProcesso { get; set; }
        //public Dictionary<string, object> Contexto { get; set; }
    }
}
