﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    [Serializable]
    public class MontarArquivoMateraResponse : MensagemResponseBase
    {
        /// <summary>
        /// Quantidade de registros gerados
        /// </summary>
        public int QtdeRegistrosGerados { get; set; }

        /// <summary>
        /// Quantidade total de registros
        /// </summary>
        public int QtdeRegistros { get; set; }

        /// <summary>
        /// Valor total de pagamentos
        /// </summary>
        public double ValorTotal { get; set; }
        
        //ExportarMateraFinanceiroRequest request
    }
}
