﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de lista de descricao de criticas
    /// </summary>
    public class ListarDescricaoCriticaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Lista de descricao dos tipos de log
        /// </summary>
        public List<TipoDescricaoInfo> Resultado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarDescricaoCriticaResponse()
        {
            this.Resultado = new List<TipoDescricaoInfo>();
        }
    }
}
