﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de alteracao de status em lote de arquivoItens
    /// </summary>
    public class AlterarStatusArquivoItemRequest : MensagemRequestBase
    {
        /// <summary>
        /// Filtro por código de arquivo
        /// </summary>
        public string FiltroCodigoArquivo { get; set; }

        /// <summary>
        /// Filtro por código de arquivo item
        /// </summary>
        public string FiltroCodigoArquivoItem { get; set; }

        /// <summary>
        /// Filtro por tipo de linha
        /// </summary>
        public string FiltroTipoLinha { get; set; }

        /// <summary>
        /// Filtro por tipo de crítica
        /// </summary>
        public string FiltroTipoCritica { get; set; }

        /// <summary>
        /// Filtro por nome de campo
        /// </summary>
        public string FiltroNomeCampo { get; set; }

        /// <summary>
        /// Filtro por status de arquivoItem
        /// </summary>
        public ArquivoItemStatusEnum? FiltroStatusArquivoItem { get; set; }

        /// <summary>
        /// Valor do status que as linhas deverão conter
        /// </summary>
        public ArquivoItemStatusEnum StatusArquivoItem { get; set; }
    }
}
