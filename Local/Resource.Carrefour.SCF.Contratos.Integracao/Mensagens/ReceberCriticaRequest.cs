﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de detalhe de crítica
    /// </summary>
    public class ReceberCriticaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código da crítica
        /// </summary>
        public string CodigoCritica { get; set; }
    }
}
