﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de listagem do relatório de transações Inconsistentes
    /// </summary>
    [Serializable]
    public class ListarRelatorioTransacaoInconsistenteRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a no qual se deseja o relatório
        /// Esta informação é obrigatória
        /// </summary>
        public string CodigoArquivo { get; set; }
        public string CodigoProcesso { get; set; } //1463
        public string CodigoProcessoRelatorio { get; set; } //1463

        /// <summary>
        /// Filtro por data de transação (inicio)
        /// </summary>
        public DateTime? FiltroDataTransacaoDe { get; set; }

        /// <summary>
        /// Filtro por data de transação (fim)
        /// </summary>
        public DateTime? FiltroDataTransacaoAte { get; set; }

        /// <summary>
        /// Filtro por data de vencimento (inicio)
        /// </summary>
        public DateTime? FiltroDataVencimentoDe { get; set; }

        /// <summary>
        /// Filtro por data de vencimento (fim)
        /// </summary>
        public DateTime? FiltroDataVencimentoAte { get; set; }

        /// <summary>
        /// Filtro por status da validação
        /// </summary>
        public string FiltroStatusValidacao { get; set; }

        /// <summary>
        /// Informar qtd de registros do resultado 
        /// </summary>
        public bool VerificarQuantidadeLinhas { get; set; }

    }
}
