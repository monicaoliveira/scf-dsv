﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de exportacao de arquivo matera financeiro
    /// </summary>
    public class ExportarMateraFinanceiroRequest : MensagemRequestBase
    {
        /// <summary>
        /// Caminho do arquivo que será gerado
        /// </summary>
        public string CaminhoArquivoDestino { get; set; }

        /// <summary>
        /// Data no qual deverão ser gerados os pagamentos
        /// </summary>
        public DateTime DataPagamento { get; set; }

        /// <summary>
        /// Código do Processo
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
