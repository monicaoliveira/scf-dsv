﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de relatório de transação inconsistente em excel
    /// </summary>
    [Serializable]
    public class GerarRelatorioTransacaoInconsistenteExcelRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a no qual se deseja o relatório
        /// Esta informação é obrigatória
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Filtro por status da validação
        /// </summary>
        public string FiltroStatusValidacao { get; set; }

        /// <summary>
        /// Opcionalmente pode-se passar o código do processo relacionado
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
