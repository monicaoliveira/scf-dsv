﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de resposta a uma solicitação de lista de críticas
    /// </summary>
    public class ListarCriticaResponse : MensagemResponseBase
    {
        /// <summary>
        /// Resultado
        /// </summary>
        public List<CriticaSCFResumoInfo> Resultado { get; set; }

        public List<CriticaSCFResumoInfoAgrupado> ResultadoAgrupado { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ListarCriticaResponse()
        {
            this.ResultadoAgrupado = new List<CriticaSCFResumoInfoAgrupado>();
            this.Resultado = new List<CriticaSCFResumoInfo>();
        }
    }
}
