﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitacao de detalhe de agrupamento de critica
    /// </summary>
    public class ReceberCriticaAgrupamentoDetalheRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Tipo de Linha
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Tipo de crítica
        /// </summary>
        public string TipoCritica { get; set; }

        /// <summary>
        /// Nome do campo
        /// </summary>
        public string NomeCampo { get; set; }
    }
}
