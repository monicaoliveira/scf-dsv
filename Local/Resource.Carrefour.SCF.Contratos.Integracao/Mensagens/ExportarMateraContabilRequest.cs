﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de exportação do arquivo matera contábil
    /// </summary>
    public class ExportarMateraContabilRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }
        
        /// <summary>
        /// Caminho de destino do arquivo a ser gerado
        /// </summary>
        public string CaminhoArquivoDestino { get; set; }
        public string CodigoProcesso { get; set; } //1447


    }
}
