﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de retorno da validacao de arquivo de retorno de cessao
    /// </summary>
    public class ValidarArquivoCessaoResponse : MensagemResponseBase
    {
        /// <summary>
        /// Quantidade de críticas encontradas
        /// </summary>
        public int QuantidadeCriticas { get; set; }
    }
}
