﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de requisicao da validacao do arquivo do retorno de cessao
    /// </summary>
    public class ValidarArquivoCessaoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a validar
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }

    }
}
