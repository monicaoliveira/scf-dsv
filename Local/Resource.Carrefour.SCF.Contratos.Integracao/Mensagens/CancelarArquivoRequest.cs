﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de cancelamento de arquivo
    /// </summary>
    public class CancelarArquivoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a ser cancelado
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Informa se deve excluir os itens de arquivo
        /// </summary>
        public bool ExcluiArquivoItem { get; set; }
    }
}
