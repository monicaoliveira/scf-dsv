﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de exportacao do arquivo de Extrato para a CCI
    /// </summary>
//1333    public class ExportarExtratoParaCCIRequest : MensagemRequestBase
    public class ExportarExtratoParaGrupoRequest : MensagemRequestBase
    {
        /// <summary>
        /// Código do arquivo a ser exportado
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Caminho do arquivo a ser gerado
        /// </summary>
        public string CaminhoArquivo { get; set; }

        /// <summary>
        /// Codigo do processo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Identifica se Processo Extrato é Manual
        /// </summary>
        public bool ProcessoManual {get; set;}

        public ExportarExtratoParaGrupoRequest()
        {
            this.ProcessoManual = false;
        }
    }
}
