﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Mensagens
{
    /// <summary>
    /// Mensagem de solicitação de lista de arquivo item
    /// </summary>
    [Serializable]
    public class ListarArquivoItemPorTipoLinhaRequest : MensagemRequestBase
    {
        /// <summary>
        /// Codigo do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Tipo da linha
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Tipo da linha
        /// </summary>
        public string CodigoArquivoItem { get; set; }

        /// <summary>
        /// Filtro por tipo de crítica
        /// </summary>
        public string TipoCritica { get; set; }

        /// <summary>
        /// Filtro por nome de campo de crítica
        /// </summary>
        public string NomeCampo { get; set; }

        /// <summary>
        /// Indica se deve retornar os valores de cada linhas
        /// </summary>
        public bool IncluirValores { get; set; }

        /// <summary>
        /// Indica se deve incluir conteudo da linha nos itens
        /// </summary>
        public bool IncluirConteudo { get; set; }

        /// <summary>
        /// Indica se deve realizar filtro de apenas itens com crítica
        /// </summary>
        public bool FiltrarComCritica { get; set; }

        /// <summary>
        /// Indica se deve realizar filtro de apenas itens com crítica
        /// </summary>
        public bool FiltrarSemCritica { get; set; }
    }
}
