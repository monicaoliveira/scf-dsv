﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;

namespace Resource.Carrefour.SCF.Contratos.Integracao
{
    /// <summary>
    /// Interface do serviço de integração
    /// </summary>
    public interface IServicoIntegracao
    {
        /// <summary>
        /// Importacao de arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ImportarArquivoResponse ImportarArquivo(ImportarArquivoRequest request);

        /// <summary>
        /// Valida o arquivo solicitado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ValidarArquivoCessaoResponse ValidarArquivoCessao(ValidarArquivoCessaoRequest request);

        /// <summary>
        /// Valida o arquivo solicitado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ValidarArquivoResponse ValidarArquivo(ValidarArquivoRequest request);

        /// <summary>
        /// Cancelamento de arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CancelarArquivoResponse CancelarArquivo(CancelarArquivoRequest request);

        /// <summary>
        /// Cancelamento de arquivo item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CancelarArquivoItemResponse CancelarArquivoItem(CancelarArquivoItemRequest request);

        /// <summary>
        /// Cancelamento de Processo Extrato Transação
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ProcessoExtratoVerificarCancelamentoOnlineResponse ProcessoExtratoVerificarCancelamentoOnline(ProcessoExtratoVerificarCancelamentoOnlineRequest request);

        /// <summary>
        /// Inicia um processo para executar a exportacao do arquivo de pagamentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ProcessarExportacaoPagamentosResponse ProcessarExportacaoPagamentos(ProcessarExportacaoPagamentosRequest request);

        /// <summary>
        /// Faz a geracao de transacoes através de um arquivo TSYS
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ImportarTransacoesTSYSResponse ImportarTransacoesTSYS(ImportarTransacoesTSYSRequest request);

        /// <summary>
        /// Faz a geracao de transacoes através de um arquivo CSU
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ImportarTransacoesCSUResponse ImportarTransacoesCSU(ImportarTransacoesCSURequest request);

        /// <summary>
        /// Faz a geracao de transacoes através de um arquivo Sitef
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ImportarTransacoesSitefResponse ImportarTransacoesSitef(ImportarTransacoesSitefRequest request);

        /// <summary>
        /// Faz exportação do arquivo financeiro do matera
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExportarMateraFinanceiroResponse ExportarMateraFinanceiro(ExportarMateraFinanceiroRequest request);

        /// <summary>
        /// Faz exportação do resultado da conciliacao do Atacadao
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExportarResultadoConciliacaoResponse ExportarResultadoConciliacaoAtacadao(ExportarResultadoConciliacaoRequest request);

        /// <summary>
        /// Faz exportação do arquivo contabil do matera
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ExportarMateraContabilResponse ExportarMateraContabil(ExportarMateraContabilRequest request);

        /// <summary>
        /// Faz a exportação de um arquivo CSU sem as transações do Atacadão
        /// </summary>
        /// <param name="reuqest"></param>
        /// <returns></returns>
        ExportarCSUSemAtacadaoResponse ExportarCSUSemAtacadao(ExportarCSUSemAtacadaoRequest request);

        /// <summary>
        /// Faz a importação de estabelecimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ImportarEstabelecimentoResponse ImportarEstabelecimento(ImportarEstabelecimentoRequest request);
    
        /// <summary>
        /// Faz a atuailizacao do retorno de cessao 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        AtualizarTransacoesRetornoCessaoResponse AtualizarTransacoesRetornoCessao(AtualizarTransacoesRetornoCessaoRequest request);

        /// <summary>
        /// Estornar lote de cessao
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //1349

        //EstornarLoteRetornoCessaoResponse EstornarLoteRetornoCessao(EstornarLoteRetornoCessaoRequest request);
    
        /// <summary>
        /// Gerar arquivo de extrato para a CCI
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
//1333        ExportarExtratoParaCCIResponse ExportarExtratoParaCCI(ExportarExtratoParaCCIRequest request);
        ExportarExtratoParaGrupoResponse ExportarExtratoParaGrupo(ExportarExtratoParaGrupoRequest request);

        /// <summary>
        /// Lista de críticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarCriticaResponse ListarCritica(ListarCriticaRequest request);

        /// <summary>
        /// Detalhe de crítica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberCriticaResponse ReceberCritica(ReceberCriticaRequest request);

        /// <summary>
        /// Lista de descricoes de criticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarDescricaoCriticaResponse ListarDescricaoCritica(ListarDescricaoCriticaRequest request);

        /// <summary>
        /// Monta lista de agrupamento de críticas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        MontarCriticaAgrupamentoResponse MontarCriticaAgrupamento(MontarCriticaAgrupamentoRequest request);

        /// <summary>
        /// Faz a importação de produtos/planos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ImportarProdutoPlanoResponse ImportarProdutoPlano(ImportarProdutoPlanoRequest request);

        /// <summary>
        /// Retorna a lista de arquivo item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarArquivoItemPorTipoLinhaResponse ListarArquivoItemPorTipoLinha(ListarArquivoItemPorTipoLinhaRequest request);

        /// <summary>
        /// Solicita detalhe de agrupamento de crítica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ReceberCriticaAgrupamentoDetalheResponse ReceberCriticaAgrupamentoDetalhe(ReceberCriticaAgrupamentoDetalheRequest request);

        /// <summary>
        /// Salva detalhe de arquivo item
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SalvarArquivoItemDetalheResponse SalvarArquivoItemDetalhe(SalvarArquivoItemDetalheRequest request);

        /// <summary>
        /// Altera status de arquivoItem em lote
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        AlterarStatusArquivoItemResponse AlterarStatusArquivoItem(AlterarStatusArquivoItemRequest request);

        ///// <summary>
        ///// Monta o arquivo Matera
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //MontarArquivoMateraResponse montarArquivoMatera(MontarArquivoMateraRequest request);

        /// <summary>
        /// Gera o arquivo que simula o retorno da cessao
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SimularArquivoRetornoCessaoResponse SimularArquivoRetornoCessao(SimularArquivoRetornoCessaoRequest request);

        /// <summary>
        /// Lista relatório de transações inconsistentes 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarRelatorioTransacaoInconsistenteResponse ListarRelatorioTransacaoInconsistente(ListarRelatorioTransacaoInconsistenteRequest request);

        /// <summary>
        /// Lista relatório de transações
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ListarRelatorioTransacaoResponse ListarRelatorioTransacao(ListarRelatorioTransacaoRequest request);

        /// <summary>
        /// Gera relatório de transação inconsistente em formato excel
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GerarRelatorioTransacaoInconsistenteExcelResponse GerarRelatorioTransacaoInconsistenteExcel(GerarRelatorioTransacaoInconsistenteExcelRequest request);

        GerarRelatorioInconsistenteAgrupadoExcelResponse GerarRelatorioInconsistenteAgrupadoExcel(GerarRelatorioInconsistenteAgrupadoExcelRequest request); //1463
        
        /// <summary>
        /// Iniciar o processamento dos desbloqueados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        IniciarProcessamentoDesbloqueiosResponse IniciarProcessamentoDesbloqueios(IniciarProcessamentoDesbloqueiosRequest request);

        AtualizarEmMassaResponse AtualizarEmMassa(AtualizarEmMassaRequest request);
    }
}
