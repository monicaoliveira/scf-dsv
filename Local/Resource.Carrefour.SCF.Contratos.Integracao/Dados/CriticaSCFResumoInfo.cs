﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Contém informações resumidas de crítica SCF
    /// </summary>
    [Serializable]
    [Description("Resumo")]
    public class CriticaSCFResumoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código da crítica
        /// </summary>
        public string CodigoCritica { get; set; }

        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Código do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do arquivo item
        /// </summary>
        public string CodigoArquivoItem { get; set; }

        /// <summary>
        /// Data da crítica
        /// </summary>
        public DateTime? DataCritica { get; set; }

        /// <summary>
        /// Tipo da classe da crítica
        /// </summary>
        public string TipoCritica { get; set; }

        /// <summary>
        /// Nome do campo
        /// </summary>
        public string NomeCampo { get; set; }

        /// <summary>
        /// Descrição da crítica
        /// </summary>
        public string DescricaoCritica { get; set; }

        public string RetornaAgrupado { get; set; } //1282

        /// <summary>
        /// Tipo da linha
        /// </summary>
        public string TipoLinha { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return this.CodigoCritica;
        }

        #endregion
    }
}
