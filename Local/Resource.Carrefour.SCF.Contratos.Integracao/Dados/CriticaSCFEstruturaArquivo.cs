﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Crítica para a regra de validação de domínio
    /// </summary>
    [Serializable]
    [Description("Estrutura de Arquivo")]
    public class CriticaSCFEstruturaArquivo : CriticaSCFArquivoItemInfo
    {
        /// <summary>
        /// Informa em qual tipo de registro
        /// </summary>
        public string TipoRegistro { get; set; }

        /// <summary>
        /// Representação string da crítica
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("EstruturaArquivo: TipoRegistro={0}", this.TipoRegistro);
        }
    }
}
