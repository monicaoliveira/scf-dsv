﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Informações do relatório de transações
    /// </summary>
    [Serializable]
    public class RelatorioTransacaoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do Arquivo Item relacionado
        /// </summary>
        public string CodigoArquivoItem { get; set; }

        /// <summary>
        /// Forma do meio de pagamento
        /// </summary>
        public string FormaMeioPagamento { get; set; }

        /// <summary>
        /// Cnpj do Estabelecimento
        /// </summary>
        public string CnpjEstabelecimento { get; set; }

        public string CnpjFavorecido{ get; set; } //1463

        /// <summary>
        /// Data do relatório
        /// </summary>
        public DateTime? DataHoraRelatorio { get; set; }

        /// <summary>
        /// Página do relatório
        /// </summary>
        public string PaginaRelatorio { get; set; }
        
        /// <summary>
        /// Origem da transação
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Codigo do grupo
        /// </summary>
        public string CodigoEmpresaGrupo { get; set; }

        /// <summary>
        /// Nome do grupo
        /// </summary>
        public string NomeEmpresaGrupo { get; set; }

        /// <summary>
        /// Codigo do subgrupo
        /// </summary>
        public string CodigoEmpresaSubgrupo { get; set; }

        /// <summary>
        /// Nome subgrupo
        /// </summary>
        public string NomeEmpresaSubgrupo { get; set; }

        /// <summary>
        /// Codigo estabelecimento
        /// </summary>
        public string CodigoEstabelecimento { get; set; }

        /// <summary>
        /// Nome estabelecimento (razão social)
        /// </summary>
        public string NomeEstabelecimento { get; set; }

        /// <summary>
        /// Codigo do produto
        /// </summary>
        public string CodigoProduto { get; set; }

        /// <summary>
        /// Nome do produto
        /// </summary>
        public string NomeProduto { get; set; }

        /// <summary>
        /// Codigo do plano
        /// </summary>
        public string CodigoPlano { get; set; }

        /// <summary>
        /// Nome do plano
        /// </summary>
        public string NomePlano { get; set; }

        /// <summary>
        /// Codigo do favorecido
        /// </summary>
        public string CodigoFavorecido { get; set; }

        /// <summary>
        /// Nome do favorecido
        /// </summary>
        public string NomeFavorecido { get; set; }

        /// <summary>
        /// Status da importação
        /// </summary>
        public Nullable<ProcessoStatusEnum> StatusImportacao { get; set; }

        /// <summary>
        /// Status do retorno
        /// </summary>
        public Nullable<TransacaoStatusRetornoCessaoEnum> StatusRetorno { get; set; }

        /// <summary>
        /// Status do pagamento
        /// </summary>
        public Nullable<PagamentoStatusEnum> StatusPagamento { get; set; }

        /// <summary>
        /// Status da transacao
        /// </summary>
        public string StatusTransacao { get; set; }

        /// <summary>
        /// Status da conciliação
        /// </summary>
        public Nullable<TransacaoStatusConciliacaoEnum> StatusConciliacao { get; set; }

        /// <summary>
        /// Status da importação
        /// </summary>
        public string StatusValidacao { get; set; }

        /// <summary>
        /// Data do venciemento
        /// </summary>
        public DateTime? DataVencimento { get; set; }

        /// <summary>
        /// Data do agendamento
        /// </summary>
        public DateTime? DataAgendamento { get; set; }

        /// <summary>
        /// Data do processamento
        /// </summary>
        public DateTime? DataProcessamento { get; set; }

        /// <summary>
        /// Data de envio para matera
        /// </summary>
        public DateTime? DataEnvioMatera { get; set; }

        /// <summary>
        /// Data de liquidação
        /// </summary>
        public DateTime? DataLiquidacao { get; set; }

        /// <summary>
        /// Data da transação
        /// </summary>
        public DateTime? DataTransacao { get; set; }

        /// <summary>
        /// Hora da transacao
        /// </summary>
        public DateTime? HoraTransacao { get; set; }
        
        /// <summary>
        /// Tipo da transação
        /// </summary>
        public string TipoTransacao { get; set; }

        /// <summary>
        /// Meio de captura
        /// </summary>
        public string MeioCaptura { get; set; }

        /// <summary>
        /// Numero do pagamento
        /// </summary>
        public string NumeroPagamento { get; set; }
        public string ContaCliente { get; set; } //1438
 
        /// <summary>
        /// NSU Host
        /// </summary>
        public string NsuHost { get; set; }

        /// <summary>
        /// Banco
        /// </summary>
        public string Banco { get; set; }

        /// <summary>
        /// Agencia
        /// </summary>
        public string Agencia { get; set; }

        /// <summary>
        /// Conta
        /// </summary>
        public string Conta { get; set; }

        /// <summary>
        /// Código do ajuste
        /// </summary>
        public string CodigoAjuste { get; set; }

        /// <summary>
        /// Data de repasse
        /// </summary>
        public DateTime? DataRepasse { get; set; }

        /// <summary>
        /// Data da transação original
        /// </summary>
        public DateTime? DataTransacaoOriginal { get; set; }

        /// <summary>
        /// Motivo do ajuste
        /// </summary>
        public string MotivoAjuste { get; set; }

        /// <summary>
        /// NSU Host original
        /// </summary>
        public string NsuHostOriginal { get; set; }

        /// <summary>
        /// Tipo do ajuste
        /// </summary>
        public string TipoAjuste { get; set; }

        /// <summary>
        /// Tipo lançamento
        /// </summary>
        public string TipoLancamento { get; set; }

        /// <summary>
        /// Valor bruto
        /// </summary>
        public decimal ValorBruto { get; set; }

        /// <summary>
        /// Valor do desconto
        /// </summary>
        public decimal ValorDesconto { get; set; }

        /// <summary>
        /// Valor líquido
        /// </summary>
        public decimal ValorLiquido { get; set; }

        /// <summary>
        /// Número do cartão
        /// </summary>
        public string NumeroCartao { get; set; }

        /// <summary>
        /// Número da conta do cliente
        /// </summary>
        public string NumeroContaCliente { get; set; }

        /// <summary>
        /// Código da anulação
        /// </summary>
        public string CodigoAnulacao { get; set; }

        /// <summary>
        /// Código da autorização original
        /// </summary>
        public string CodigoAutorizacaoOriginal { get; set; }

        /// <summary>
        /// Motivo da anulação
        /// </summary>
        public string MotivoAnulacao { get; set; }

        /// <summary>
        /// Número da parcela
        /// </summary>
        public string NumeroParcela { get; set; }

        /// <summary>
        /// Numero total de parcelas
        /// </summary>
        public string NumerParcelaTotal { get; set; }

        /// <summary>
        /// NSU TEF original
        /// </summary>
        public string NsuTefOriginal { get; set; }

        /// <summary>
        /// Valor da percela
        /// </summary>
        public decimal ValorParcela { get; set; }

        /// <summary>
        /// Valor do desconto da parcela
        /// </summary>
        public decimal ValorParcelaDesconto { get; set; }

        /// <summary>
        /// Valor líquido da parcela
        /// </summary>
        public decimal ValorParcelaLiquido { get; set; }

        /// <summary>
        /// Código da autorização
        /// </summary>
        public string CodigoAutorizacao { get; set; }

        /// <summary>
        /// Meio de pagamento
        /// </summary>
        public string MeioPagamento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MeioPagamentoSequencia { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal MeioPagamentoValor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string QuantidadeMeioPagamento { get; set; }

        /// <summary>
        /// NSU TEF
        /// </summary>
        public string NsuTef { get; set; }

        /// <summary>
        /// Cupom fiscal
        /// </summary>
        public string CupomFiscal { get; set; }

        /// <summary>
        /// Modalidade
        /// </summary>
        public string Modalidade { get; set; }

        /// <summary>
        /// Tipo do produto
        /// </summary>
        public string TipoProduto { get; set; }

        /// <summary>
        /// Data de envio para CCI
        /// </summary>
        public DateTime? DataEnvioCCI { get; set; }

        /// <summary>
        /// Data de geração da agenda de pagamentos
        /// </summary>
        public DateTime? DataGeracaoAgenda { get; set; }

        /// <summary>
        /// Data de retorno da CCI
        /// </summary>
        public DateTime? DataRetornoCCI { get; set; }

        /// <summary>
        /// Tipo de registro
        /// </summary>
        public string TipoRegistro { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UsuarioEnvioCCI { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UsuarioEnvioMatera { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UsuarioRetornoCCI { get; set; }

        /// <summary>
        /// Data de reembolso do Estabelecimento (Arq TSYS, tipo AP)
        /// </summary>
        public DateTime? DataReembolsoEstabelecimento { get; set; }

        /// <summary>
        /// Atributo do arquivo TSYS, tipo AP
        /// </summary>
        public string NumeroCartaoTransacaoOriginal { get; set; }

        /// <summary>
        /// Lista de críticas
        /// </summary>
        public List<CriticaSCFResumoInfo> Criticas { get; set; }

        /// <summary>
        /// Código do Processo
        /// </summary>
        public string CodigoProcesso { get; set; }

        /// <summary>
        /// Data do Movimento
        /// </summary>
        public DateTime? DataMovimento { get; set; }

        /// <summary>
        /// Data de inclusão da transação no SCF
        /// </summary>
        public DateTime? DataInclusaoSCF { get; set; }

        /// <summary>
        /// Tipo Financeiro Contabil
        /// ECOMMERCE - Fernando Bove - 20160104
        /// </summary>
        //public string TipoFinanceiroContabil { get; set; }

        /// <summary>
        /// NSU Host Reversao
        /// ECOMMERCE - Fernando Bove - 20160106
        /// </summary>
        //public string NSUHostReversao { get; set; }

        /// <summary>
        /// Filtro por Referencia
        /// ClockWork - Marcos Matsuoka
        /// </summary>
        public ListaItemInfo Referencia { get; set; }
        
        /// <summary>
        /// Construtor default
        /// </summary>
        public RelatorioTransacaoInfo()
        {
            this.DataHoraRelatorio = DateTime.Now;
            this.Criticas = new List<CriticaSCFResumoInfo>();
        }

        #region
        /// <summary>
        /// Informa o código da entidade
        /// </summary>
        /// <returns></returns>
        public string ReceberCodigo()
        {
            return this.CodigoArquivoItem;
        }
        #endregion
    }
}
