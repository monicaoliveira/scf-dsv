﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{   [Serializable]
    [Description("Constante")]
    public class CriticaSCFConstante : CriticaSCFArquivoItemInfo
    {
        public TipoValidacaoConstanteEnum TipoValidacaoConstante { get; set; }
        public string Valor { get; set; }
        public override string ToString()
        {
            return string.Format("Constante: NOME CAMPO [{0}] {1} CONTEUDO [{2}] TIPO LINHA [{3}]", this.NomeCampo, this.TipoValidacaoConstante, this.Valor, this.TipoLinha); //1353 
        }
    }
}
