﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Representa uma crítica de arquivo
    /// </summary>
    [Serializable]
    [Description("Arquivo")]
    public class CriticaSCFArquivoInfo : CriticaInfo
    {
        /// <summary>
        /// Código do arquivo a que a crítica se refere
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do processo associado
        /// </summary>
        public string CodigoProcesso { get; set; }
    }
}
