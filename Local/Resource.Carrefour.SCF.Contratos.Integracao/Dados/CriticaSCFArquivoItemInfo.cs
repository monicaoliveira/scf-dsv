﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    [Serializable]
    [Description("Item de Arquivo")]
    public class CriticaSCFArquivoItemInfo : CriticaSCFArquivoInfo
    {
        public string CodigoArquivoItem { get; set; }
        public string NomeCampo { get; set; }
        public string TipoLinha{ get; set; }
        public string NSEQ { get; set; } //1353

    }
}
