﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Detalhe de agrupamento de crítica
    /// </summary>
    [Serializable]
    public class CriticaAgrupamentoDetalheInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Tipo da linha
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Tipo da crítica
        /// </summary>
        public string TipoCritica { get; set; }

        /// <summary>
        /// Nome do campo
        /// </summary>
        public string NomeCampo { get; set; }

        /// <summary>
        /// Quantidade de linhas
        /// </summary>
        public int QuantidadeLinhas { get; set; }

        /// <summary>
        /// Quantidade de críticas
        /// </summary>
        public int QuantidadeCriticas { get; set; }

        /// <summary>
        /// Quantidade de linhas bloqueadas
        /// </summary>
        public int QuantidadeLinhasBloqueadas { get; set; }

        /// <summary>
        /// Quantidade de linhas pendentes de desbloqueio
        /// </summary>
        public int QuantidadeLinhasPendentesDesbloqueio { get; set; }

        /// <summary>
        /// Quantidade de linhas alteradas
        /// </summary>
        public int QuantidadeLinhasPendentesValidacao { get; set; }

        /// <summary>
        /// Quantidade de linhas validas
        /// </summary>
        public int QuantidadeLinhasValidas { get; set; }

        /// <summary>
        /// Quantidade de linhas invalidas
        /// </summary>
        public int QuantidadeLinhasInvalidas { get; set; }

        /// <summary>
        /// Quantidade de linhas excluidas
        /// </summary>
        public int QuantidadeLinhasExcluidas { get; set; }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
