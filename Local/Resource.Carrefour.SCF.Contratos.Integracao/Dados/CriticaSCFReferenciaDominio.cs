﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using System.ComponentModel;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Crítica para regra de validação de referências de domínio
    /// </summary>
    [Serializable]
    [Description("Domínio")]
    public class CriticaSCFReferenciaDominio : CriticaSCFArquivoItemInfo
    {
        /// <summary>
        /// Valor não encontrado
        /// </summary>
        public string ValorNaoEncontrado { get; set; }

        /// <summary>
        /// Nome da referencia
        /// </summary>
        public ListaItemReferenciaNomeEnum NomeReferencia { get; set; }

        /// <summary>
        /// Situacao da referencia
        /// </summary>
        public ListaItemReferenciaSituacaoEnum SituacaoReferencia { get; set; }


        public override string ToString()
        {
            return string.Format("Sem referência {0} para {1} = {2}", NomeReferencia.ToString(), SituacaoReferencia.ToString(), ValorNaoEncontrado);   
        }
    }
}
