﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Tipo de verificacao para validar se a data não foi preenchida
    /// </summary>
    public enum TipoValidacaoConstanteEnum
    {
        Igual = 0,
        Diferente = 1
    }
}
