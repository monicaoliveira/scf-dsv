﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    public class DescricaoCriticaDistinct : IEqualityComparer<CriticaSCFResumoInfo>
    {
        #region IEqualityComparer<CriticaSCFResumoInfo> Members

        public bool Equals(CriticaSCFResumoInfo x, CriticaSCFResumoInfo y)
        {
            return x.DescricaoCritica.Trim().Equals(y.DescricaoCritica.Trim());
        }

        public int GetHashCode(CriticaSCFResumoInfo obj)
        {
            return obj.DescricaoCritica.Trim().GetHashCode();
        }

        #endregion
    }
}
