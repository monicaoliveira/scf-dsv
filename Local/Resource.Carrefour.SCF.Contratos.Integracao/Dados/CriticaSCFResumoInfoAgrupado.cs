﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    [Serializable]
    public class CriticaSCFResumoInfoAgrupado
    {
        public List<CriticaSCFResumoInfo> Criticas { get; set; }
        public string Descricao { get; set; }
        public string NomeCampo { get; set; }
        public int QtdRegistros { get; set; } //1282
        public string TipoCritica { get; set; } //1282
        public string TipoLinha { get; set; } //1282

        public CriticaSCFResumoInfoAgrupado()
        {
            this.Criticas = new List<CriticaSCFResumoInfo>();
        }
    }
}
