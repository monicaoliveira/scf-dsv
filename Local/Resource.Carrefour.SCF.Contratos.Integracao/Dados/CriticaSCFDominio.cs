﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Crítica para a regra de validação de domínio
    /// </summary>
    [Serializable]
    [Description("Domínio")]
    public class CriticaSCFDominio : CriticaSCFArquivoItemInfo
    {
        /// <summary>
        /// Indica o valor que não foi encontrado
        /// </summary>
        public string ValorNaoEncontrado { get; set; }

        /// <summary>
        /// Representação string da crítica
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Informacao [{0}] com valor [{1}] nao cadastrado no sistema", this.NomeCampo, this.ValorNaoEncontrado);
        }
    }
}
