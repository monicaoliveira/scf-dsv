﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Crítica para a regra de validação especifica, do campo NSU HOST
    /// </summary>
    [Serializable]
    [Description("Meio de Captura")]
    public class CriticaSCFMeioCaptura : CriticaSCFArquivoItemInfo
    {
        /// <summary>
        /// Mostra o valor inválido
        /// </summary>
        public string Valor { get; set; }

        /// <summary>
        /// Representação string da crítica
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Meio de Captura: {0}={1}", this.NomeCampo, this.Valor);
        }
    }
}
