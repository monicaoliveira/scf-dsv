﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    [Serializable]
    [Description("Obrigatoriedade")]
    public class CriticaSCFObrigatoriedade : CriticaSCFArquivoItemInfo
    {
        public bool CampoNaoPreenchido { get; set; }
        public string Valor { get; set; }
        public override string ToString()
        {
            return string.Format("Obrigatoriedade: NOME CAMPO [{0}] CONTEUDO [{1}]; CampoNaoPreenchido={2}", this.NomeCampo, this.Valor, this.CampoNaoPreenchido); //1353
        }
    }
}
