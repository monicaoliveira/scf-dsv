﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Crítica para a regra de validação de domínio
    /// </summary>
    [Serializable]
    [Description("Regra Específica")]
    public class CriticaSCFEspecifica : CriticaSCFArquivoItemInfo
    {
    }
}
