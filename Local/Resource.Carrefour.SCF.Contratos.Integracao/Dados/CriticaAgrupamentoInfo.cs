﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Info para agrupamento de críticas. É uma informação derivada do banco de dados, não é salvo diretamente
    /// </summary>
    [Serializable]
    public class CriticaAgrupamentoInfo : ICodigoEntidade
    {
        /// <summary>
        /// Código do arquivo
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do arquivo de origem quando for proveniente de desbloqueio
        /// </summary>
        public string CodigoArquivoOrigem { get; set; }

        /// <summary>
        /// Tipo da linha
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Tipo da crítica
        /// </summary>
        public string TipoCritica { get; set; }

        /// <summary>
        /// Nome do campo
        /// </summary>
        public string NomeCampo { get; set; }

        /// <summary>
        /// Quantidade de críticas neste nível
        /// </summary>
        public int Quantidade { get; set; }

        /// <summary>
        /// Indica o nivel deste elemento,
        /// Pode ser CodigoArquivo, TipoLinha, TipoCritica ou NomeCampo
        /// </summary>
        public CriticaAgrupamentoNivelEnum Nivel { get; set; }

        /// <summary>
        /// Caso os itens sejam representados em árvore, indica os itens filhos deste
        /// </summary>
        public List<CriticaAgrupamentoInfo> Filhos { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Descricao
        {
            get
            {
                // Prepara o retorno
                string retorno = null;
                
                // Acha a string de acordo com o nivel
                switch (this.Nivel)
                {
                    case CriticaAgrupamentoNivelEnum.AgrupamentoPorArquivo:
                        retorno = "Arquivo: " + this.CodigoArquivo;
                        break;
                    case CriticaAgrupamentoNivelEnum.AgrupamentoPorCritica:
                        retorno = this.TipoCritica;
                        break;
                    case CriticaAgrupamentoNivelEnum.AgrupamentoPorLinha:
                        retorno = this.TipoLinha;
                        break;
                    case CriticaAgrupamentoNivelEnum.Detalhe:
                        retorno = this.NomeCampo;
                        break;
                    case CriticaAgrupamentoNivelEnum.Total:
                        retorno = "Críticas";
                        break;
                }

                // Adiciona o total de linhas
                retorno += " (" + this.Quantidade + ")";

                // Retorna
                return retorno;
            }
        }

        /// <summary>
        /// Retorna a chave
        /// </summary>
        public string Chave
        {
            get 
            { 
                return string.Format(
                    "{0};{1};{2};{3};{4}",
                    (int)this.Nivel,
                    this.CodigoArquivoOrigem, 
                    this.TipoLinha, 
                    this.TipoCritica, 
                    this.NomeCampo); 
            }
        }

        /// <summary>
        /// Retorna a chave do elemento pai
        /// </summary>
        public string ChavePai 
        { 
            get 
            {
                // Prepara o retorno
                string chave = null;

                // Infere o nivel
                int nivelPai = (int)this.Nivel + 1;
                
                // Verifica se tem pai
                if (this.Nivel == CriticaAgrupamentoNivelEnum.Detalhe)
                    chave = 
                        string.Format(
                            "{0};{1};{2};{3};",
                            nivelPai,
                            this.CodigoArquivoOrigem,
                            this.TipoLinha,
                            this.TipoCritica);
                else if (this.Nivel == CriticaAgrupamentoNivelEnum.AgrupamentoPorCritica)
                    chave =
                        string.Format(
                            "{0};{1};{2};;",
                            nivelPai,
                            this.CodigoArquivoOrigem,
                            this.TipoLinha);
                else if (this.Nivel == CriticaAgrupamentoNivelEnum.AgrupamentoPorLinha)
                    chave =
                        string.Format(
                            "{0};{1};;;",
                            nivelPai,
                            this.CodigoArquivoOrigem);
                else if (this.Nivel == CriticaAgrupamentoNivelEnum.AgrupamentoPorArquivo)
                    chave =
                        string.Format(
                            "{0};;;;",
                            nivelPai);
                else if (this.Nivel == CriticaAgrupamentoNivelEnum.Total)
                    chave = null;

                // Retorna
                return chave;
            } 
        }

        /// <summary>
        /// Número do nível
        /// </summary>
        public int NivelNumero { get { return (int)this.Nivel; } }

        /// <summary>
        /// Construtor default
        /// </summary>
        public CriticaAgrupamentoInfo()
        {
            this.Filhos = new List<CriticaAgrupamentoInfo>();
        }

        #region ICodigoEntidade Members

        public string ReceberCodigo()
        {
            return null;
        }

        #endregion
    }
}
