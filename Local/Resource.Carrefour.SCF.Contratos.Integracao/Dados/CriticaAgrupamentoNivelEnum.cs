﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Integracao.Dados
{
    /// <summary>
    /// Niveis de agrupamentos de crítica
    /// </summary>
    [Serializable]
    public enum CriticaAgrupamentoNivelEnum
    {
        Detalhe = 0,
        AgrupamentoPorCritica = 1,
        AgrupamentoPorLinha = 2,
        AgrupamentoPorArquivo = 3,
        Total = 4
    }
}
