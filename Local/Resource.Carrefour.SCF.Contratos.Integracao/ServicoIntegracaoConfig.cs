﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Integracao
{
    /// <summary>
    /// Configurações do serviço de intregração
    /// </summary>
    public class ServicoIntegracaoConfig
    {
        /// <summary>
        /// Código do atacadão no arquivo da TSYS
        /// </summary>
        public string CodigoLojaAtacadaoTSYS { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoIntegracaoConfig()
        {
            this.CodigoLojaAtacadaoTSYS = "075315333003981";
        }
    }
}
