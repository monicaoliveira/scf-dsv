﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Contratos.Integracao
{
    /// <summary>
    /// Configurações do arquivo matera contábil
    /// </summary>
    public class ServicoIntegracaoArquivoMateraContabilConfig
    {
        /// <summary>
        /// Código da empresa
        /// </summary>
        public string COD_EMPRESA { get; set; }

        /// <summary>
        /// Código tipo contábil
        /// </summary>
        public string COD_TIPO_CONTABIL { get; set; }

        /// <summary>
        /// Numéro da guia
        /// </summary>
        public string NUM_GUIA { get; set; }

        /// <summary>
        /// Código do sistema
        /// </summary>
        public string COD_SIS { get; set; }

        /// <summary>
        /// Código da filial
        /// </summary>
        public string COD_FILIAL { get; set; }

        /// <summary>
        /// Código da conta contábil 1
        /// </summary>
        public string COD_CONTA_CONTABIL_1 { get; set; }

        /// <summary>
        /// Código da conta contábil 2
        /// </summary>
        public string COD_CONTA_CONTABIL_2 { get; set; }

        /// <summary>
        /// Identificação do centro de custo
        /// </summary>
        public int ID_CENTRO_CUSTO { get; set; }

        /// <summary>
        /// Código do projeto do valor bruto
        /// </summary>
        public string COD_PROJETO { get; set; }

        /// <summary>
        /// Tipo de lançamento do valor bruto
        /// </summary>
        public string TIPO_LANC_DEBITO { get; set; }

        /// <summary>
        /// Tipo de lançamento da comissão
        /// </summary>
        public string TIPO_LANC_CREDITO { get; set; }

        /// <summary>
        /// Código do histórico padrão
        /// </summary>
        public string COD_HIST_PADRAO { get; set; }

        /// <summary>
        /// Referência
        /// </summary>
        public string REFERENCIA { get; set; }

        /// <summary>
        /// Data de conversão
        /// </summary>
        public string DT_CONVERSAO { get; set; }

        /// <summary>
        /// Complemento
        /// </summary>
        public string COMPLEMENTO { get; set; }

        /// <summary>
        /// Histórico 1 (do valor bruto)
        /// </summary>
        public string HIST_1 { get; set; }

        /// <summary>
        /// Histórico 2 (da comissao)
        /// </summary>
        public string HIST_2 { get; set; }

        /// <summary>
        /// Ind rateio
        /// </summary>
        public string IND_RATEIO { get; set; }

        /// <summary>
        /// Código da conta contra
        /// </summary>
        public string COD_CONTA_CONTRA { get; set; }

        /// <summary>
        /// Identificação do custo contra
        /// </summary>
        public int ID_CCUSTO_CONTRA { get; set; }

        /// <summary>
        /// Código do centro de custo
        /// </summary>
        public string COD_CENTRO_CUSTO { get; set; }

        /// <summary>
        /// Código do centro de custo contra
        /// </summary>
        public string COD_CENTRO_CUSTO_CONTRA { get; set; }

        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoIntegracaoArquivoMateraContabilConfig()
        {
            this.COD_EMPRESA = "0004667";
            this.COD_TIPO_CONTABIL = "O";
            this.NUM_GUIA = "00000000";
            this.COD_SIS = "CSU";
            this.COD_FILIAL = "0004668";
            this.COD_CONTA_CONTABIL_1 = "49985002001";
            this.COD_CONTA_CONTABIL_2 = "49992002311";
            this.ID_CENTRO_CUSTO = 0;
            this.TIPO_LANC_DEBITO = "D";
            this.TIPO_LANC_CREDITO = "C";
            this.COD_HIST_PADRAO = "";
            this.REFERENCIA = "";
            this.DT_CONVERSAO = "";
            this.COMPLEMENTO = "";
            this.IND_RATEIO = "N";
            this.HIST_1 = "Transf de venda efetuada no Atacadao";
            this.HIST_2= "Transf comissao s/ venda efetuada no Atacadao";
            this.ID_CCUSTO_CONTRA = 000000;
            this.COD_CENTRO_CUSTO = "";
            this.COD_CENTRO_CUSTO_CONTRA = "";
            this.COD_PROJETO = "";
        }
    }
}
