﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Sistemas.Comum;
using Resource.Framework.Sistemas.Interface;
using Resource.Carrefour.SCF.Sistemas.Principal;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Framework.Contratos.Comum.Mensagens;
using System.Reflection;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Hosts.Server.WindowsService
{
    public partial class ServicoHostSCF : ServiceBase
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ServicoConsolidadorDeSaldo servicoDeSaldo;

        public ServicoHostSCF()
        {
            InitializeComponent();

            inicializar();

            servicoDeSaldo = new ServicoConsolidadorDeSaldo();

        }


        protected override void OnStart(string[] args)
        {
            //Debugger.Break();

            // Log
            log.Info("Iniciando o Serviço");

            // Bloco de controle
            try
            {
                // Inicia os servicos
                ContainerServicoHost containerServicoHost = ContainerServicoHost.GetInstance();
                containerServicoHost.Iniciar(
                    new ContainerServicoHostConfig()
                    {
                        ServicoHostConfig =
                            new ServicoHostConfig()
                            {
                                ExporWCF = true,
                                TipoVarreduraTipos = ServicoHostTipoVarreduraEnum.DefaultIncluir,
                                IncluirTipos =
                                    new List<Type>() 
                                { 
                                    typeof(ServicoSeguranca),
                                    typeof(ServicoPersistencia),
                                    typeof(ServicoMetadadoComum)
                                },
                                IncluirAssemblies =
                                    new List<Assembly>()
                                {
                                    typeof(ServicoPrincipal).Assembly,
                                    typeof(ServicoIntegracao).Assembly
                                }
                            }
                    });
                // Pede inicializacao do servico de seguranca
                Mensageria.Processar(new InicializarSegurancaRequest());
                //1430 servicoDeSaldo.IniciarServico();
                
                ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });// lucas.rodrigues - Verificar o status que ficou o serviço antes da aplicação parar
                // ==========================================================================================
                // 1430 - inclusao do parametro OrigemChamada - inicio
                // ==========================================================================================
                if (resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador == ServicoAgendadorStatusEnum.Iniciado)
                {
                    IniciarServicoAgendadorRequest venvio = new IniciarServicoAgendadorRequest();
                    venvio.OrigemChamada = "ServicoHostSCF";
                    Mensageria.Processar(venvio); //1430 Mensageria.Processar(new IniciarServicoAgendadorRequest());
                }
                // ==========================================================================================
                // 1430 - inclusao do parametro OrigemChamada - fim
                // ==========================================================================================
                if (resConfig.ConfiguracaoGeralInfo.StatusServicoExpurgo == ServicoExpurgoStatusEnum.Iniciado)
                    Mensageria.Processar(new IniciarServicoExpurgoRequest());
                // ==========================================================================================
                // 1070 - Confere processos que foram parados           
                // ==========================================================================================
                ServicoExecutarRegrasInicializacao executarRegrasInicializacao = new ServicoExecutarRegrasInicializacao();
                if (resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador == ServicoAgendadorStatusEnum.Iniciado)
                {
                    executarRegrasInicializacao.LiberarTransacoesEmAndamento();
                }
                else
                {
                    executarRegrasInicializacao.LogarTransacoesEmAndamento();
                }
            }
            catch (Exception ex)
            {
                // Log
                log.Error("Erro na inicialização do serviço", ex);

                // Repassa o erro
                throw ex;
            }
        }

        protected override void OnStop()
        {
            // Log
            log.Info("Parando o Serviço");

            // Bloco de controle
            try
            {
                // Para os servicos
                ContainerServicoHost containerServicoHost = ContainerServicoHost.GetInstance();
                containerServicoHost.Finalizar();
                //1430servicoDeSaldo.PararServico();
            }
            catch (Exception ex)
            {
                // Log
                log.Error("Erro ao parar o serviço", ex);

                // Repassa o erro
                throw ex;
            }
        }

        private void inicializar()
        {
            // Localizador de tipos
            GerenciadorConfig.SetarConfig(
                new LocalizadorTiposConfig()
                {
                    ListaFixa =
                        new List<LocalizadorTipoInfo>()
                        {
                            new LocalizadorTipoInfo()
                            {
                                IncluirNamespace = 
                                    "Resource.Framework.Contratos.Comum.Dados, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Framework.Contratos.Comum.Mensagens, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Dados.Log, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador, Resource.Carrefour.SCF.Contratos.Principal;" +                                    
                                    "Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Mensagens, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Carrefour.SCF.Contratos.Integracao.Dados, Resource.Carrefour.SCF.Contratos.Integracao;" +
                                    "Resource.Carrefour.SCF.Contratos.Integracao.Mensagens, Resource.Carrefour.SCF.Contratos.Integracao;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Permissoes, Resource.Carrefour.SCF.Contratos.Principal;" +
                                    "Resource.Framework.Contratos.Comum.Permissoes, Resource.Framework.Contratos.Comum;" +
                                    "Resource.Carrefour.SCF.Contratos.Principal.Dados.Log, Resource.Carrefour.SCF.Contratos.Principal",
                            },
                            new LocalizadorTipoInfo()
                            {
                                IncluirTipo =
                                        "Resource.Framework.Contratos.Comum.ServicoSegurancaConfig, Resource.Framework.Contratos.Comum;" +
                                        "Resource.Framework.Contratos.Comum.ConfigHelperConfig, Resource.Framework.Contratos.Comum"
                            }
                        }
                });

            // Configuracoes de persistencia
            GerenciadorConfig.SetarConfig(
                new ServicoPersistenciaConfig()
                {
                    Persistencias =
                        new List<PersistenciaInfo>()
                        {
                            new PersistenciaInfo() 
                            {
                                Default = true,
                                TipoPersistencia = typeof(PersistenciaDb)
                            }
                        }
                });

            //// Configuracoes de seguranca
            //GerenciadorConfig.SetarConfig(
            //    new ServicoSegurancaConfig()
            //    {
            //        NomeUsuarioAdministrador = "Admin",
            //        NamespacesPermissoes =
            //            new List<string>() 
            //            {
            //                "Resource.Framework.Contratos.Comum.Permissoes, Resource.Framework.Contratos.Comum",
            //                "Resource.Carrefour.SCF.Contratos.Principal.Permissoes, Resource.Carrefour.SCF.Contratos.Principal"
            //            },
            //        ValidarSenhas = false
            //    });

            // Configuracoes do resolutor de tipos
            GerenciadorConfig.SetarConfig(
                new ResolutorTiposConfig()
                {
                    AprofundarNamespaces = true,
                    IncluirNamespaces =
                        new List<string>() 
                        { 
                            "Resource.Framework.Contratos.Comum, Resource.Framework.Contratos.Comum",
                            "Resource.Framework.Contratos.Comum.Dados, Resource.Framework.Contratos.Comum",
                            "Resource.Framework.Contratos.Comum.Mensagens, Resource.Framework.Contratos.Comum",
                            "Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador, Resource.Carrefour.SCF.Contratos.Principal",
                            "Resource.Carrefour.SCF.Contratos.Principal.Mensagens, Resource.Carrefour.SCF.Contratos.Principal",
                            "Resource.Carrefour.SCF.Contratos.Principal.Dados, Resource.Carrefour.SCF.Contratos.Principal",
                            "Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador, Resource.Carrefour.SCF.Contratos.Principal",
                            "Resource.Carrefour.SCF.Contratos.Integracao.Mensagens, Resource.Carrefour.SCF.Contratos.Integracao",
                            "Resource.Carrefour.SCF.Contratos.Integracao.Dados, Resource.Carrefour.SCF.Contratos.Integracao",
                            "Resource.Carrefour.SCF.Sistemas.Principal.Processos, Resource.Carrefour.SCF.Sistemas.Principal",
                            "Resource.Carrefour.SCF.Sistemas.Integracao.Arquivos, Resource.Carrefour.SCF.Sistemas.Integracao"
                        },
                    IncluirTipos = new List<string>()
                    {
                                        "Resource.Framework.Contratos.Comum.ServicoSegurancaConfig, Resource.Framework.Contratos.Comum",
                                        "Resource.Framework.Contratos.Comum.ConfigHelperConfig, Resource.Framework.Contratos.Comum"
                    }

                });


        }
    }
}
