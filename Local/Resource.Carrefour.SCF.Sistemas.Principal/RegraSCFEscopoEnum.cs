﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    /// <summary>
    /// Escopo da regra
    /// </summary>
    public enum RegraSCFEscopoEnum
    {
        /// <summary>
        /// Indica validação a nível do arquivo,
        /// ou de várias linhas
        /// </summary>
        Arquivo,

        /// <summary>
        /// Indica validação a nível de linha
        /// </summary>
        Linha
    }
}
