﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using System.IO;

using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;

using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Library.Servicos.Mensagens;

using Resource.Carrefour.SCF.Contratos.Principal.Dados.Agendador;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Agendador;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
//1408
using System.Data;
using System.Text.RegularExpressions;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Integracao.Dados;
using Resource.Framework.Library.Db;
// using Resource.Framework.Contratos.Comum; // warning 25/02/2019
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
//1408

//================================================================================================================================= NAMESPACE
namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    //================================================================================================================================= ServicoAgendador
    public class ServicoAgendador : IServicoAgendador
    {
        string CodigoSessao = "";
        private ManualResetEvent _manualResetEvent = null;
        private Thread _threadServicoAgendador = null;
        private ServicoAgendadorInfo _statusServicoAgendadorInfo = new ServicoAgendadorInfo();
        private ConfiguracaoGeralInfo configGeral = new ConfiguracaoGeralInfo(); //1439
        private void processarArquivo(ConfiguracaoGeralInfo configGeral) { }
        private void inicializar()
        {
            _manualResetEvent = new ManualResetEvent(false);    //1430 para não ocorrer erro ao PARAR servico de captura
            configGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(CodigoSessao, null); //1439
            _statusServicoAgendadorInfo.StatusServico = configGeral.StatusServicoAgendador; //1439
            int codigoLog = 0;
            OracleConnection cn = Procedures.ReceberConexao();
            Procedures.PR_LOG_R(codigoLog);   // 1408 LIMPA LOGS ANTERIORS
            cn.Close();
        }

        //=================================================================================================================================
        public ServicoAgendador() { inicializar(); }

        //=================================================================================================================================
        public IniciarServicoAgendadorResponse IniciarServicoAgendador(IniciarServicoAgendadorRequest request)
        {
            //=================================================================================================================================
            // 1430 
            //=================================================================================================================================
            string vOrigemChamada = request.OrigemChamada; // informa a origem que chamou 
            ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { }); // MOVENDO DE BAIXO - Carrega o status do servico
            _statusServicoAgendadorInfo.StatusServico = resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador;
            //1439 ConfiguracaoGeralInfo configGeral = new ConfiguracaoGeralInfo();
            //1439 configGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(request.CodigoSessao, null);
            //=================================================================================================================================
            // Prepara a resposta
            //=================================================================================================================================
            IniciarServicoAgendadorResponse resposta = new IniciarServicoAgendadorResponse();
            resposta.PreparaResposta(request);
            string codigoProcessoAtacadao = "";
            string codigoProcessoExtrato = "";
            string codigoProcessoRetCessao = "";
            string vValidacoesNOK = "";
            int intervaloExecucao = 60;//scf1350 para iniciar imediatamente quando clicamos em INICIAR
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            _statusServicoAgendadorInfo.Validacoes = true; //scf1350
            CodigoSessao = request.CodigoSessao;
            try
            {
                //=================================================================================================================================
                // 1430 - não é utilizado
                //=================================================================================================================================
                //ServicoAgendadorConfig _configServico = GerenciadorConfig.ReceberConfig<ServicoAgendadorConfig>(); // carrega USUARIOROBO e SENHA
                //if (_configServico != null)
                //    _configServico = new ServicoAgendadorConfig();
                //=================================================================================================================================
                if (vOrigemChamada != null) //1430
                {
                    lLogSCFProcessoEstagioEventoInfo.setDescricao("Reiniciando Captura Automatica porque antes estava [" + _statusServicoAgendadorInfo.StatusServico.ToString() + "] reestart [" + vOrigemChamada + "]");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
                //=================================================================================================================================
                // CAPTURA ARQUIVO - INICIO
                //=================================================================================================================================                
                if ((_statusServicoAgendadorInfo.StatusServico == ServicoAgendadorStatusEnum.Parado) || (vOrigemChamada != null)) //1430 inclusao VorigemChamada
                {
                    if (vOrigemChamada == null) //1430
                    {
                        lLogSCFProcessoEstagioEventoInfo.setDescricao("Status da Captura esta [" + _statusServicoAgendadorInfo.StatusServico.ToString() + "] e será iniciado"); //1430
                        LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    }
                    //=================================================================================================================================
                    // TRATANDO O STATUS_AGENDADOR
                    //=================================================================================================================================
                    // altera o status do servico de captura
                    _statusServicoAgendadorInfo.StatusServico = ServicoAgendadorStatusEnum.Iniciado;

                    // salvar o novo status_agendador = 1
                    resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador = _statusServicoAgendadorInfo.StatusServico;
                    Mensageria.Processar<SalvarConfiguracaoGeralResponse>(new SalvarConfiguracaoGeralRequest() { ConfiguracaoGeralInfo = resConfig.ConfiguracaoGeralInfo });

                    // efetuar log
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(DateTime.Now.ToString() + " Iniciando a captura automatica. Status da captura arquivo [" + _statusServicoAgendadorInfo.StatusServico.ToString() + "]");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    _statusServicoAgendadorInfo.ObservacaoGeral = lLogSCFProcessoEstagioEventoInfo.Descricao;
                    resposta.StatusServicoAgendadorInfo = _statusServicoAgendadorInfo.StatusServico;

                    //=================================================================================================================================
                    ///1441 - movendo para o WHILE do UM MINUTO
                    //=================================================================================================================================
                    /*
                    //=================================================================================================================================
                    // TRATANDO OS DIRETORIOS
                    //=================================================================================================================================
                    //=================================================================================================================================
                    // PROCESSO ATACADAO
                    //=================================================================================================================================
                    _statusServicoAgendadorInfo.ValidacoesMsg = "ATACADAO - Dir Imp ";
                    lLogSCFProcessoEstagioEventoInfo.setDescricao("Processo Atacadao diretorios importacao from [" + vOrigemChamada + "]");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    //=================================================================================================================================
                    if (configGeral.DiretorioImportacaoAtacadaoMA.Trim() != "" &&
                        configGeral.DiretorioImportacaoAtacadaoMA.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoAtacadaoMA, true, true);
                    }
                    if (configGeral.DiretorioImportacaoAtacadaoSITEF.Trim() != "" &&
                        configGeral.DiretorioImportacaoAtacadaoSITEF.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoAtacadaoSITEF, true, true);
                    }
                    if (configGeral.DiretorioImportacaoAtacadaoTSYS.Trim() != "" &&
                        configGeral.DiretorioImportacaoAtacadaoTSYS.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoAtacadaoTSYS, true, true);
                    }
                    //=================================================================================================================================
                    if (!_statusServicoAgendadorInfo.Validacoes)
                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                    _statusServicoAgendadorInfo.ValidacoesMsg = "ATACADAO - Dir Exp ";
                    _statusServicoAgendadorInfo.Validacoes = true;
                    lLogSCFProcessoEstagioEventoInfo.setDescricao("Processo Atacadao diretorios exportacao from [" + vOrigemChamada + "]");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    //=================================================================================================================================
                    if (configGeral.DiretorioExportacaoAtacadaoConciliacao.Trim() != "" &&
                        configGeral.DiretorioExportacaoAtacadaoConciliacao.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoAtacadaoConciliacao, false, false);
                    }
                    if (configGeral.DiretorioExportacaoAtacadaoCSUSemAtacadao.Trim() != "" &&
                        configGeral.DiretorioExportacaoAtacadaoCSUSemAtacadao.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoAtacadaoCSUSemAtacadao, false, false);
                    }
                    if (configGeral.DiretorioExportacaoMateraContabil.Trim() != "" &&
                        configGeral.DiretorioExportacaoMateraContabil.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoMateraContabil, false, false);
                    }
                    if (configGeral.DiretorioExportacaoMateraFinanceiro.Trim() != "" &&
                        configGeral.DiretorioExportacaoMateraFinanceiro.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoMateraFinanceiro, false, false);
                    }
                    //=================================================================================================================================
                    if (!_statusServicoAgendadorInfo.Validacoes)
                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                    _statusServicoAgendadorInfo.ValidacoesMsg = "EXTRATO - Dir Imp ";
                    _statusServicoAgendadorInfo.Validacoes = true;
                    lLogSCFProcessoEstagioEventoInfo.setDescricao("Processo Extrato diretorios importacao from [" + vOrigemChamada + "]");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    //=================================================================================================================================
                    // PROCESSO EXTRATO
                    //=================================================================================================================================
                    if (configGeral.DiretorioImportacaoExtratoTSYSCarrefour.Trim() != "" &&
                        configGeral.DiretorioImportacaoExtratoTSYSCarrefour.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoExtratoTSYSCarrefour, false, false);
                    }
                    if (configGeral.DiretorioImportacaoExtratoTSYSAtacadao.Trim() != "" &&
                        configGeral.DiretorioImportacaoExtratoTSYSAtacadao.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoExtratoTSYSAtacadao, false, false);
                    }
                    if (configGeral.DiretorioImportacaoExtratoTSYSGaleria.Trim() != "" &&
                        configGeral.DiretorioImportacaoExtratoTSYSGaleria.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoExtratoTSYSGaleria, false, false);
                    }
                    //=================================================================================================================================
                    if (!_statusServicoAgendadorInfo.Validacoes)
                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                    _statusServicoAgendadorInfo.ValidacoesMsg = "EXTRATO - Dir Exp ";
                    _statusServicoAgendadorInfo.Validacoes = true;
                    lLogSCFProcessoEstagioEventoInfo.setDescricao("Processo Extrato diretorios exportacao from [" + vOrigemChamada + "]");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    //=================================================================================================================================
                    if (configGeral.DiretorioExportacaoCCI.Trim() != "" &&
                        configGeral.DiretorioExportacaoCCI.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoCCI, false, false);
                    }
                    if (configGeral.DiretorioExportacaoATA.Trim() != "" &&
                        configGeral.DiretorioExportacaoATA.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoATA, false, false);
                    }
                    if (configGeral.DiretorioExportacaoGAL.Trim() != "" &&
                        configGeral.DiretorioExportacaoGAL.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoGAL, false, false);
                    }
                    //=================================================================================================================================
                    if (!_statusServicoAgendadorInfo.Validacoes)
                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                    _statusServicoAgendadorInfo.ValidacoesMsg = "CESSAO - Dir Imp ";
                    _statusServicoAgendadorInfo.Validacoes = true;
                    lLogSCFProcessoEstagioEventoInfo.setDescricao("Processo Cessao diretorio importacao from [" + vOrigemChamada + "]");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                    //=================================================================================================================================
                    if (configGeral.DiretorioImportacaoCessao.Trim() != "" &&
                        configGeral.DiretorioImportacaoCessao.Trim() != null
                        )
                    {
                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoCessao, false, false);
                    }
                    //=================================================================================================================================
                    if (!_statusServicoAgendadorInfo.Validacoes)
                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                    _statusServicoAgendadorInfo.ValidacoesMsg = "";
                    _statusServicoAgendadorInfo.Validacoes = true;
                    //=================================================================================================================================
                    */
                    //=================================================================================================================================
                    ///1441 - FIM
                    //=================================================================================================================================

                    //=================================================================================================================================
                    // CRIANDO A THREAD DE CAPTURA AUTOMATICA DOS ARQUIVOS
                    //=================================================================================================================================

                    //=================================================================================================================================
                    // INICIO - THREAD DE CAPTURA AUTOMATICA
                    //=================================================================================================================================
                    _manualResetEvent = new ManualResetEvent(false);    //1430 para conseguir INICIAR o servico de captura
                    string vManualResentEventSet = _manualResetEvent.ToString();
                    _threadServicoAgendador = new Thread(new ParameterizedThreadStart(delegate(object o)
                    {
                        _statusServicoAgendadorInfo.DataProximaExecucao = DateTime.Now.Add(new TimeSpan(0, 0, intervaloExecucao));
                        //=================================================================================================================================
                        // INICIO - LOOPING DE CAPTURA DE ARQUIVO CONFORME O TEMPO EM SEGUNDOS INFORMADO NO INTERVALO
                        //=================================================================================================================================
                        while (!_manualResetEvent.WaitOne(new TimeSpan(0, 0, intervaloExecucao)))
                        {
                            try
                            {
                                //=================================================================================================================================
                                // 1439 CONFERE O STATUS_AGENDADOR
                                //=================================================================================================================================
                                resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { }); // MOVENDO DE BAIXO - Carrega o status do servico
                                _statusServicoAgendadorInfo.StatusServico = resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador;
                                if (_statusServicoAgendadorInfo.StatusServico == ServicoAgendadorStatusEnum.Parado)
                                {
                                    _manualResetEvent = new ManualResetEvent(true);
                                    _manualResetEvent.Set();
                                    _manualResetEvent.Close();
                                    break;
                                }
                                else
                                {
                                    //=================================================================================================================================
                                    ///1441 - INICIO
                                    //=================================================================================================================================
                                    configGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(CodigoSessao, null);

                                    //=================================================================================================================================
                                    // TRATANDO OS DIRETORIOS
                                    //=================================================================================================================================
                                    //=================================================================================================================================
                                    // PROCESSO ATACADAO
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "ATACADAO - Dir Imp ";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    if (configGeral.DiretorioImportacaoAtacadaoMA.Trim() != "" &&
                                        configGeral.DiretorioImportacaoAtacadaoMA.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoAtacadaoMA, true, true);
                                    }
                                    if (configGeral.DiretorioImportacaoAtacadaoSITEF.Trim() != "" &&
                                        configGeral.DiretorioImportacaoAtacadaoSITEF.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoAtacadaoSITEF, true, true);
                                    }
                                    if (configGeral.DiretorioImportacaoAtacadaoTSYS.Trim() != "" &&
                                        configGeral.DiretorioImportacaoAtacadaoTSYS.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoAtacadaoTSYS, true, true);
                                    }
                                    if (!_statusServicoAgendadorInfo.Validacoes)
                                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "ATACADAO - Dir Exp ";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    if (configGeral.DiretorioExportacaoAtacadaoConciliacao.Trim() != "" &&
                                        configGeral.DiretorioExportacaoAtacadaoConciliacao.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoAtacadaoConciliacao, false, false);
                                    }
                                    if (configGeral.DiretorioExportacaoAtacadaoCSUSemAtacadao.Trim() != "" &&
                                        configGeral.DiretorioExportacaoAtacadaoCSUSemAtacadao.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoAtacadaoCSUSemAtacadao, false, false);
                                    }
                                    if (configGeral.DiretorioExportacaoMateraContabil.Trim() != "" &&
                                        configGeral.DiretorioExportacaoMateraContabil.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoMateraContabil, false, false);
                                    }
                                    if (configGeral.DiretorioExportacaoMateraFinanceiro.Trim() != "" &&
                                        configGeral.DiretorioExportacaoMateraFinanceiro.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoMateraFinanceiro, false, false);
                                    }
                                    if (!_statusServicoAgendadorInfo.Validacoes)
                                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                                    //=================================================================================================================================
                                    // PROCESSO EXTRATO
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "EXTRATO - Dir Imp ";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    if (configGeral.DiretorioImportacaoExtratoTSYSCarrefour.Trim() != "" &&
                                        configGeral.DiretorioImportacaoExtratoTSYSCarrefour.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoExtratoTSYSCarrefour, false, false);
                                    }
                                    if (configGeral.DiretorioImportacaoExtratoTSYSAtacadao.Trim() != "" &&
                                        configGeral.DiretorioImportacaoExtratoTSYSAtacadao.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoExtratoTSYSAtacadao, false, false);
                                    }
                                    if (configGeral.DiretorioImportacaoExtratoTSYSGaleria.Trim() != "" &&
                                        configGeral.DiretorioImportacaoExtratoTSYSGaleria.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoExtratoTSYSGaleria, false, false);
                                    }
                                    if (!_statusServicoAgendadorInfo.Validacoes)
                                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "EXTRATO - Dir Exp ";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    if (configGeral.DiretorioExportacaoCCI.Trim() != "" &&
                                        configGeral.DiretorioExportacaoCCI.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoCCI, false, false);
                                    }
                                    if (configGeral.DiretorioExportacaoATA.Trim() != "" &&
                                        configGeral.DiretorioExportacaoATA.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoATA, false, false);
                                    }
                                    if (configGeral.DiretorioExportacaoGAL.Trim() != "" &&
                                        configGeral.DiretorioExportacaoGAL.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioExportacaoGAL, false, false);
                                    }
                                    if (!_statusServicoAgendadorInfo.Validacoes)
                                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                                    //=================================================================================================================================
                                    // PROCESSO CESSAO
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "CESSAO - Dir Imp ";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    if (configGeral.DiretorioImportacaoCessao.Trim() != "" &&
                                        configGeral.DiretorioImportacaoCessao.Trim() != null
                                        )
                                    {
                                        criaDiretoriosProcessamento(configGeral.DiretorioImportacaoCessao, false, false);
                                    }
                                    if (!_statusServicoAgendadorInfo.Validacoes)
                                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    //=================================================================================================================================
                                    // 1441 - FIM
                                    //=================================================================================================================================

                                    //=================================================================================================================================
                                    // PROCESSO ATACADAO - Captura automatica
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "Processo Atacadao ";
                                    codigoProcessoAtacadao = "";
                                    if ((configGeral.DiretorioImportacaoAtacadaoTSYS.Trim() != "" &&
                                             configGeral.DiretorioImportacaoAtacadaoTSYS.Trim() != null) ||
                                            (configGeral.DiretorioImportacaoAtacadaoSITEF.Trim() != "" &&
                                             configGeral.DiretorioImportacaoAtacadaoSITEF.Trim() != null) ||
                                            (configGeral.DiretorioImportacaoAtacadaoMA.Trim() != "" &&
                                             configGeral.DiretorioImportacaoAtacadaoMA.Trim() != null)
                                        )
                                    {
                                        codigoProcessoAtacadao = processarAtacadao(configGeral);
                                    }
                                    //=================================================================================================================================                            
                                    if (!_statusServicoAgendadorInfo.Validacoes)
                                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    //=================================================================================================================================                            
                                    if (codigoProcessoAtacadao == "")
                                        _statusServicoAgendadorInfo.ObservacaoAtacadao = DateTime.Now.ToString() + " [Nao iniciou processo Atacadao]";
                                    else
                                    {
                                        _statusServicoAgendadorInfo.ObservacaoAtacadao = DateTime.Now.ToString() + " [Processo Atacadao: " + codigoProcessoAtacadao + "]";
                                        _statusServicoAgendadorInfo.CodigoProcessoAtacadao = codigoProcessoAtacadao;
                                    }
                                    //=================================================================================================================================
                                    // PROCESSO EXTRATO  - Captura automatica
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "Processo Extrato ";
                                    codigoProcessoExtrato = "";
                                    if ((configGeral.DiretorioImportacaoExtratoTSYSCarrefour.Trim() != null &&
                                            configGeral.DiretorioImportacaoExtratoTSYSCarrefour.Trim() != "") ||
                                        (configGeral.DiretorioImportacaoExtratoTSYSAtacadao.Trim() != null &&
                                            configGeral.DiretorioImportacaoExtratoTSYSAtacadao.Trim() != "") ||
                                        (configGeral.DiretorioImportacaoExtratoTSYSGaleria.Trim() != null &&
                                            configGeral.DiretorioImportacaoExtratoTSYSGaleria.Trim() != "")
                                       )
                                    {
                                        codigoProcessoExtrato = processarExtrato(configGeral);
                                    }
                                    //=================================================================================================================================                            
                                    if (!_statusServicoAgendadorInfo.Validacoes)
                                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    //=================================================================================================================================                            
                                    if (codigoProcessoExtrato == "")
                                        _statusServicoAgendadorInfo.ObservacaoExtrato = DateTime.Now.ToString() + " [Nao iniciou processo Extrato]";
                                    else
                                    {
                                        _statusServicoAgendadorInfo.ObservacaoExtrato = DateTime.Now.ToString() + " [Processo Extrato: " + codigoProcessoExtrato + "]";
                                        _statusServicoAgendadorInfo.CodigoProcessoExtrato = codigoProcessoExtrato;
                                    }
                                    //=================================================================================================================================
                                    // PROCESSO CESSAO  - Captura automatica
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "Processo Cessao ";
                                    codigoProcessoRetCessao = "";
                                    if (configGeral.DiretorioImportacaoCessao.Trim() != null &&
                                            configGeral.DiretorioImportacaoCessao.Trim() != ""
                                        )
                                    {
                                        codigoProcessoRetCessao = processarRetornoCessao(configGeral);
                                    }
                                    //=================================================================================================================================                            
                                    if (!_statusServicoAgendadorInfo.Validacoes)
                                        vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                                    _statusServicoAgendadorInfo.ValidacoesMsg = "";
                                    _statusServicoAgendadorInfo.Validacoes = true;
                                    //=================================================================================================================================                            
                                    if (codigoProcessoRetCessao == "")
                                        _statusServicoAgendadorInfo.ObservacaoRetornoCessao = DateTime.Now.ToString() + " [Nao iniciou processo Cessao].";
                                    else
                                    {
                                        _statusServicoAgendadorInfo.ObservacaoRetornoCessao = DateTime.Now.ToString() + " [Processo Cessao: " + codigoProcessoRetCessao + "]";
                                        _statusServicoAgendadorInfo.CodigoProcessoRetonoCessao = codigoProcessoRetCessao;
                                    }
                                    //=================================================================================================================================
                                    // Informa proxima execucao
                                    //=================================================================================================================================
                                    _statusServicoAgendadorInfo.DataProximaExecucao = DateTime.Now.Add(new TimeSpan(0, 0, intervaloExecucao));
                                }
                            }
                            catch (Exception ex)
                            {
                                _statusServicoAgendadorInfo.Validacoes = false;
                                _statusServicoAgendadorInfo.ValidacoesMsg = DateTime.Now.ToString() + " [" + ex.Message + "]";
                                _statusServicoAgendadorInfo.DataProximaExecucao = DateTime.Now.Add(new TimeSpan(0, 0, intervaloExecucao));
                            }
                            //=================================================================================================================================
                            if (!_statusServicoAgendadorInfo.Validacoes)
                                vValidacoesNOK = vValidacoesNOK + "[" + _statusServicoAgendadorInfo.ValidacoesMsg + "]";
                            _statusServicoAgendadorInfo.ValidacoesMsg = "";
                            _statusServicoAgendadorInfo.Validacoes = true;
                            //=================================================================================================================================
                            if (vValidacoesNOK != "")
                            {
                                _statusServicoAgendadorInfo.ObservacaoGeral = DateTime.Now.ToString() + " NOK [" + vValidacoesNOK + "] from [" + vOrigemChamada + "]";
                                lLogSCFProcessoEstagioEventoInfo.setDescricao(_statusServicoAgendadorInfo.ObservacaoGeral);
                                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                            }
                        }
                        //=================================================================================================================================
                        // FIM - LOOPING DE CAPTURA DE ARQUIVO CONFORME O TEMPO EM SEGUNDOS INFORMADO NO INTERVALO
                        //=================================================================================================================================
                    }));
                    //=================================================================================================================================
                    // FIM - THREAD DE CAPTURA AUTOMATICA
                    //=================================================================================================================================

                    //=================================================================================================================================
                    // EXECUTANDO A THREAD
                    //=================================================================================================================================
                    _threadServicoAgendador.Start();
                }
                //=================================================================================================================================
                // CAPTURA ARQUIVO - FIM
                //=================================================================================================================================                
            }
            catch (Exception ex)
            {
                lLogSCFProcessoEstagioEventoInfo.setDescricao(DateTime.Now.ToString() + " [" + ex.Message + "] from [" + vOrigemChamada + "]");
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }

        //=================================================================================================================================
        public PararServicoAgendadorResponse PararServicoAgendador(PararServicoAgendadorRequest request)
        {
            PararServicoAgendadorResponse resposta = new PararServicoAgendadorResponse();
            resposta.PreparaResposta(request);
            try
            {   // parar a captura automatica
                _manualResetEvent = new ManualResetEvent(true);
                _manualResetEvent.Set();
                _manualResetEvent.Close();

                // receber o status_agendador
                _statusServicoAgendadorInfo.StatusServico = ServicoAgendadorStatusEnum.Parado;
                ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                // salvar o novo status_agendador = 0
                resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador = _statusServicoAgendadorInfo.StatusServico;
                Mensageria.Processar<SalvarConfiguracaoGeralResponse>(new SalvarConfiguracaoGeralRequest() { ConfiguracaoGeralInfo = resConfig.ConfiguracaoGeralInfo });

                // efetuar log
                var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEstagioEventoInfo.setDescricao(DateTime.Now.ToString() + " Parando a captura automatica. Status da captura arquivo [" + _statusServicoAgendadorInfo.StatusServico.ToString() + "]");
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                _statusServicoAgendadorInfo.ObservacaoGeral = lLogSCFProcessoEstagioEventoInfo.Descricao;
                resposta.ServicoAgendadorInfo = _statusServicoAgendadorInfo;
            }
            catch (Exception ex)
            {
                _statusServicoAgendadorInfo.Validacoes = false; //scf1350
                _statusServicoAgendadorInfo.ValidacoesMsg = _statusServicoAgendadorInfo.ValidacoesMsg + " Botao PARAR erro: " + " [" + ex.Message + "]";
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }

        //=================================================================================================================================
        public ReceberStatusServicoAgendadorResponse ReceberStatusServicoAgendador(ReceberStatusServicoAgendadorRequest request)
        {
            ReceberStatusServicoAgendadorResponse resposta = new ReceberStatusServicoAgendadorResponse();
            resposta.PreparaResposta(request);
            try
            {   // receber o status_agendador
                ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });
                _statusServicoAgendadorInfo.StatusServico = resConfig.ConfiguracaoGeralInfo.StatusServicoAgendador;

                // efetuar log
                var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEstagioEventoInfo.setDescricao(DateTime.Now.ToString() + " Status da captura arquivo [" + _statusServicoAgendadorInfo.StatusServico.ToString() + "]");
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                _statusServicoAgendadorInfo.ObservacaoGeral = lLogSCFProcessoEstagioEventoInfo.Descricao;
                resposta.ServicoAgendadorInfo = _statusServicoAgendadorInfo;
            }
            catch (Exception ex)
            {
                _statusServicoAgendadorInfo.Validacoes = false; //scf1350
                _statusServicoAgendadorInfo.ValidacoesMsg = _statusServicoAgendadorInfo.ValidacoesMsg + " Receber status erro: [" + ex.Message + "]";
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }

        //=================================================================================================================================
        public string processarAtacadao(ConfiguracaoGeralInfo configGeral)
        {
            ExecutarProcessoAtacadaoRequest processoAtacadaoRequest = new ExecutarProcessoAtacadaoRequest();
            processoAtacadaoRequest.CodigoSessao = this.CodigoSessao;
            ProcessoAtacadaoAutomaticoConfig configAtacadao = new ProcessoAtacadaoAutomaticoConfig();
            string codigoProcesso = "";
            //=================================================================================================================================
            // TRATA DIRETORIOS DE IMPORTACAO
            //=================================================================================================================================
            bool arquivoCompleto = false;
            //=================================================================================================================================
            // Arquivo da TSYS
            //=================================================================================================================================
            if (configGeral.DiretorioImportacaoAtacadaoTSYS != "" &&
                configGeral.DiretorioImportacaoAtacadaoTSYS != null)
            {
                string[] filesTSYS = System.IO.Directory.GetFiles(configGeral.DiretorioImportacaoAtacadaoTSYS);
                //=================================================================================================================================
                // Move o arquivo para pasta "EM PROCESSAMENTO"
                //=================================================================================================================================
                string arquivoTSYSEmProcessamento = "";
                configAtacadao.CaminhoArquivoTSYS = null;
                if (filesTSYS.Length > 0)
                {   //=================================================================================================================================
                    // Preenche o request
                    //=================================================================================================================================
                    processoAtacadaoRequest.ExecutarAssincrono = true;
                    //=================================================================================================================================
                    //Pegar o nome do arquivo
                    //=================================================================================================================================
                    arquivoTSYSEmProcessamento = configGeral.DiretorioImportacaoAtacadaoTSYS + "\\EmProcessamento\\" + Path.GetFileName(filesTSYS[0]);
                    _statusServicoAgendadorInfo.ValidacoesMsg = "ATACADAO TSYS - ";
                    arquivoCompleto = MoverArquivo(arquivoTSYSEmProcessamento, filesTSYS[0]);
                    configAtacadao.CaminhoArquivoTSYS = arquivoTSYSEmProcessamento;
                }
            };
            //=================================================================================================================================
            // Arquivo da SITEF
            //=================================================================================================================================
            if (configGeral.DiretorioImportacaoAtacadaoSITEF != "" &&
                configGeral.DiretorioImportacaoAtacadaoSITEF != null)
            {
                string[] filesSITEF = System.IO.Directory.GetFiles(configGeral.DiretorioImportacaoAtacadaoSITEF);
                //=================================================================================================================================
                // Move o arquivo para pasta "EM PROCESSAMENTO"
                //=================================================================================================================================
                string arquivoSITEFEmProcessamento = "";
                configAtacadao.CaminhoArquivoSitef = null;
                if (filesSITEF.Length > 0)
                {   //=================================================================================================================================
                    // Preenche o request
                    //=================================================================================================================================
                    processoAtacadaoRequest.ExecutarAssincrono = true;
                    //=================================================================================================================================
                    //Pegar o nome do arquivo
                    //=================================================================================================================================
                    arquivoSITEFEmProcessamento = configGeral.DiretorioImportacaoAtacadaoSITEF + "\\EmProcessamento\\" + Path.GetFileName(filesSITEF[0]);
                    _statusServicoAgendadorInfo.ValidacoesMsg = "ATACADAO SITEF - ";
                    arquivoCompleto = MoverArquivo(arquivoSITEFEmProcessamento, filesSITEF[0]);
                    configAtacadao.CaminhoArquivoSitef = arquivoSITEFEmProcessamento;
                }
            };
            //=================================================================================================================================
            // Arquivo da MA
            //=================================================================================================================================
            if (configGeral.DiretorioImportacaoAtacadaoMA != "" &&
                configGeral.DiretorioImportacaoAtacadaoMA != null)
            {
                string[] filesMA = System.IO.Directory.GetFiles(configGeral.DiretorioImportacaoAtacadaoMA);
                if (filesMA.Length > 0)
                {   //=================================================================================================================================
                    // Move o arquivo para pasta "EM PROCESSAMENTO"
                    //=================================================================================================================================
                    string arquivoMAEmProcessamento = "";
                    configAtacadao.CaminhoArquivoCSU = null;
                    if (filesMA.Length > 0)
                    {
                        //=================================================================================================================================
                        // Preenche o request
                        //=================================================================================================================================
                        processoAtacadaoRequest.ExecutarAssincrono = true;
                        //=================================================================================================================================
                        //Pegar o nome do arquivo
                        //=================================================================================================================================
                        arquivoMAEmProcessamento = configGeral.DiretorioImportacaoAtacadaoMA + "\\EmProcessamento\\" + Path.GetFileName(filesMA[0]);
                        _statusServicoAgendadorInfo.ValidacoesMsg = "ATACADAO CSU - ";
                        arquivoCompleto = MoverArquivo(arquivoMAEmProcessamento, filesMA[0]);
                        configAtacadao.CaminhoArquivoCSU = arquivoMAEmProcessamento;
                    }
                }
            };
            //=================================================================================================================================
            // TRATA DIRETORIOS DE EXPORTACAO
            //=================================================================================================================================
            configAtacadao.CaminhoArquivoCSUSemAtacadao = configGeral.DiretorioExportacaoAtacadaoCSUSemAtacadao;
            configAtacadao.CaminhoArquivoMateraContabil = configGeral.DiretorioExportacaoMateraContabil;
            configAtacadao.CaminhoArquivoMateraFinanceiro = configGeral.DiretorioExportacaoMateraFinanceiro;
            if (configGeral.DiretorioExportacaoAtacadaoConciliacao.Trim() != "" &&
                configGeral.DiretorioExportacaoAtacadaoConciliacao.Trim() != null
                )
            {
                configAtacadao.CaminhoArquivoResultadoConciliacao = configGeral.DiretorioExportacaoAtacadaoConciliacao;
            }
            else
            {
                configAtacadao.CaminhoArquivoResultadoConciliacao = null;
            }
            configAtacadao.MoverArquivosOrigem = true;
            processoAtacadaoRequest.Config = configAtacadao;
            processoAtacadaoRequest.CodigoSessao = this.CodigoSessao;
            //=================================================================================================================================
            // CHAMA PROCESSO ATACADAO
            //=================================================================================================================================
            if (arquivoCompleto)
            {
                codigoProcesso = ((ExecutarProcessoAtacadaoResponse)Mensageria.Processar(processoAtacadaoRequest)).CodigoProcesso;
                //===========================================================================================================
                // GERANDO LOG
                //===========================================================================================================
                var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(
                        "[CAPTURA AUTOMATICA: " + codigoProcesso + "]; " +
                        "[TSYS: " + processoAtacadaoRequest.Config.CaminhoArquivoTSYS + "]; " +
                        "[SITEF: " + processoAtacadaoRequest.Config.CaminhoArquivoSitef + "]; " +
                        "[MA: " + processoAtacadaoRequest.Config.CaminhoArquivoCSU + "]; " +
                        "[Grupo: ATA]" +
                        "[Exp CSU sem Atacadao: " + processoAtacadaoRequest.Config.CaminhoArquivoCSUSemAtacadao + "]; " +
                        "[Exp MATERA Contabil: " + processoAtacadaoRequest.Config.CaminhoArquivoMateraContabil + "]; " +
                        "[Exp MATERA Financeiro: " + processoAtacadaoRequest.Config.CaminhoArquivoMateraFinanceiro + "]; " +
                        "[Exp Relatorio Conciliacao: " + processoAtacadaoRequest.Config.CaminhoArquivoResultadoConciliacao + "]");
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            }
            return codigoProcesso;
        }

        //=================================================================================================================================
        public string processarExtrato(ConfiguracaoGeralInfo configGeral)
        {   //===========================================================================================================
            // CRIANDO A CHAMADA processoExtratoRequest
            //===========================================================================================================
            ExecutarProcessoExtratoRequest processoExtratoRequest = new ExecutarProcessoExtratoRequest();
            processoExtratoRequest.CodigoSessao = CodigoSessao;
            processoExtratoRequest.Config = new ProcessoExtratoAutomaticoConfig();
            //===========================================================================================================
            //1318 - REFAZENDO ESTE PROGRAMA A PARTIR DAQUI
            //===========================================================================================================
            bool arquivoCompleto = false;
            bool havefilesTSYS = false;

            string[] filesTSYS = null;
            string filesTSYSIMP = "";
            string filesTSYSEXP = "";
            string filesTSYSARQ = "";
            string filesTSYSGRP = "";
            string codigoProcesso = "";
            string arquivoTSYSEmProcessamento = "";

            //===========================================================================================================
            // TEM DIRETORIO INFORMADO PARA GRUPO CARREFOUR ?
            //===========================================================================================================
            if (
                configGeral.DiretorioImportacaoExtratoTSYSCarrefour != null &&
                configGeral.DiretorioImportacaoExtratoTSYSCarrefour != "" &&
                !(havefilesTSYS)
                )
            {
                filesTSYS = System.IO.Directory.GetFiles(configGeral.DiretorioImportacaoExtratoTSYSCarrefour);
                if (filesTSYS.Length > 0)
                {
                    havefilesTSYS = true;
                    filesTSYSEXP = configGeral.DiretorioExportacaoCCI;
                    filesTSYSIMP = configGeral.DiretorioImportacaoExtratoTSYSCarrefour;
                    filesTSYSARQ = Path.GetFileName(filesTSYS[0]);
                    filesTSYSGRP = "CCI";

                    arquivoTSYSEmProcessamento = filesTSYSIMP + "\\EmProcessamento\\" + filesTSYSARQ;// Move para o diretorio em processamento
                    _statusServicoAgendadorInfo.ValidacoesMsg = "EXTRATO TSYS Carrefour - ";
                    arquivoCompleto = MoverArquivo(arquivoTSYSEmProcessamento, filesTSYS[0]);

                    //===========================================================================================================
                    // MONTANDO A CHAMADA processoExtratoRequest
                    //===========================================================================================================
                    processoExtratoRequest.Config.CaminhoArquivoCCI = configGeral.DiretorioExportacaoCCI;
                    processoExtratoRequest.Config.CaminhoArquivoTSYS = arquivoTSYSEmProcessamento;
                }
            }
            //===========================================================================================================
            // TEM DIRETORIO INFORMADO PARA GRUPO ATACADAO ?
            //===========================================================================================================            
            if (
                configGeral.DiretorioImportacaoExtratoTSYSAtacadao != null &&
                configGeral.DiretorioImportacaoExtratoTSYSAtacadao != "" &&
                !(havefilesTSYS)
                )
            {
                filesTSYS = System.IO.Directory.GetFiles(configGeral.DiretorioImportacaoExtratoTSYSAtacadao);
                if (filesTSYS.Length > 0)
                {
                    havefilesTSYS = true;
                    filesTSYSEXP = configGeral.DiretorioExportacaoATA;
                    filesTSYSIMP = configGeral.DiretorioImportacaoExtratoTSYSAtacadao;
                    filesTSYSARQ = Path.GetFileName(filesTSYS[0]);
                    filesTSYSGRP = "ATA";

                    arquivoTSYSEmProcessamento = filesTSYSIMP + "\\EmProcessamento\\" + filesTSYSARQ;// Move para o diretorio em processamento
                    _statusServicoAgendadorInfo.ValidacoesMsg = "EXTRATO TSYS Atacadao - ";
                    arquivoCompleto = MoverArquivo(arquivoTSYSEmProcessamento, filesTSYS[0]);

                    //===========================================================================================================
                    // MONTANDO A CHAMADA processoExtratoRequest
                    //===========================================================================================================
                    processoExtratoRequest.Config.CaminhoArquivoATA = configGeral.DiretorioExportacaoATA;
                    processoExtratoRequest.Config.CaminhoArquivoTSYSAtacadao = arquivoTSYSEmProcessamento;
                }
            }
            //===========================================================================================================
            // TEM DIRETORIO INFORMADO PARA GRUPO GALERIA ?
            //===========================================================================================================            
            if (
                configGeral.DiretorioImportacaoExtratoTSYSGaleria != null &&
                configGeral.DiretorioImportacaoExtratoTSYSGaleria != "" &&
                !(havefilesTSYS)
                )
            {
                filesTSYS = System.IO.Directory.GetFiles(configGeral.DiretorioImportacaoExtratoTSYSGaleria);
                if (filesTSYS.Length > 0)
                {
                    havefilesTSYS = true;
                    filesTSYSEXP = configGeral.DiretorioExportacaoGAL;
                    filesTSYSIMP = configGeral.DiretorioImportacaoExtratoTSYSGaleria;
                    filesTSYSARQ = Path.GetFileName(filesTSYS[0]);
                    filesTSYSGRP = "GAL";

                    arquivoTSYSEmProcessamento = filesTSYSIMP + "\\EmProcessamento\\" + filesTSYSARQ;// Move para o diretorio em processamento
                    _statusServicoAgendadorInfo.ValidacoesMsg = "EXTRATO TSYS Galeria - ";
                    arquivoCompleto = MoverArquivo(arquivoTSYSEmProcessamento, filesTSYS[0]);

                    //===========================================================================================================
                    // MONTANDO A CHAMADA processoExtratoRequest
                    //===========================================================================================================
                    processoExtratoRequest.Config.CaminhoArquivoGAL = configGeral.DiretorioExportacaoGAL;
                    processoExtratoRequest.Config.CaminhoArquivoTSYSGaleria = arquivoTSYSEmProcessamento;
                }
            }
            //===========================================================================================================
            // SE TEM ARQUIVO e DIRETORIO ESTA COMPLETO
            //===========================================================================================================            
            if (havefilesTSYS)
            {
                if (arquivoCompleto)
                {
                    //===========================================================================================================
                    // EFETUANDO A CHAMADA processoExtratoRequest >> ProcessoExtratoEstagioImportacaoTSYS >> OnProcessar
                    //===========================================================================================================
                    processoExtratoRequest.ExecutarAssincrono = true;
                    processoExtratoRequest.MoverArquivoOrigem = true;
                    codigoProcesso = ((ExecutarProcessoExtratoResponse)Mensageria.Processar(processoExtratoRequest)).CodigoProcesso;	// Starta o processamento

                    //===========================================================================================================
                    // GERANDO LOG
                    //===========================================================================================================
                    var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(
                            "[CAPTURA AUTOMATICA: " + codigoProcesso + "]; " +
                            "[Arquivo: " + filesTSYSARQ + "]; " +
                            "[Grupo: " + filesTSYSGRP + "]; " +
                            "[Diretorio Importacao: " + filesTSYSIMP + "]; " +
                            "[Diretorio Exportacao: " + filesTSYSEXP + "]; ");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
            }
            //===========================================================================================================
            // DEVOLVE CODIGO DO PROCESSO GERADO OU NAO
            //===========================================================================================================            
            return codigoProcesso;
        }

        //=================================================================================================================================
        public string processarRetornoCessao(ConfiguracaoGeralInfo configGeral)
        {
            //===========================================================================================================
            // chama o processo
            //===========================================================================================================
            ExecutarProcessoCessaoRequest processoCessaoRequest = new ExecutarProcessoCessaoRequest();
            processoCessaoRequest.CodigoSessao = this.CodigoSessao;
            processoCessaoRequest.Config = new ProcessoCessaoAutomaticoConfig();

            // verificacao se terminou de copiar o arquivo
            bool arquivoCompleto = true;

            string codigoProcesso = "";

            // Ler o 1.o arquivo de Extrato do diretorio do cessao 

            // Pegar o 1.o arquivo da TSYS - Processo Retorno de cessao
            string[] fileRetCessao = System.IO.Directory.GetFiles(configGeral.DiretorioImportacaoCessao);

            // Se nao houver nenhum arquivo de entrada, nao executa
            if (fileRetCessao.Length > 0)
            {
                // Preenche o request
                processoCessaoRequest.ExecutarAssincrono = true;

                // Açoes para mover o arquivo para pasta "EM PROCESSAMENTO"
                string arquivoRetCessaoEmProcessamento = "";

                if (fileRetCessao.Length > 0)
                {
                    // Move para o diretorio em processamento
                    arquivoRetCessaoEmProcessamento = configGeral.DiretorioImportacaoCessao + "\\EmProcessamento\\" + Path.GetFileName(fileRetCessao[0]);
                    // System.IO.File.Move(fileRetCessao[0], arquivoRetCessaoEmProcessamento);
                    _statusServicoAgendadorInfo.ValidacoesMsg = "CESSAO - ";
                    arquivoCompleto = MoverArquivo(arquivoRetCessaoEmProcessamento, fileRetCessao[0]);
                }

                // Passar as informações para processamento
                processoCessaoRequest.Config.CaminhoArquivoRetornoCessao = arquivoRetCessaoEmProcessamento;
                processoCessaoRequest.MoverArquivoOrigem = true;

                // Starta o processamento
                if (arquivoCompleto)
                {
                    codigoProcesso = ((ExecutarProcessoCessaoResponse)Mensageria.Processar(processoCessaoRequest)).CodigoProcesso;
                    //===========================================================================================================
                    // GERANDO LOG
                    //===========================================================================================================
                    var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(
                            "[CAPTURA AUTOMATICA: " + codigoProcesso + "]; " +
                            "[CESSAO: " + processoCessaoRequest.Config.CaminhoArquivoRetornoCessao + "]; ");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
            }

            return codigoProcesso;
        }
        //04 - processarRetornoCessao ================================================================================================ 

        //07 - criaDiretoriosProcessamento ================================================================================================ 
        private void criaDiretoriosProcessamento(string diretorio, bool criarEmProcessamento, bool criarProcessado)
        {       //===========================================================================================================
            // CONFERE O TAMANO DOS DIRETORIOS
            //===========================================================================================================
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            string vMensagem = "Criar diretorios - ";
            string vdir_emproc = diretorio + "\\EmProcessamento\\";
            string vdir_proc = diretorio + "\\Processados\\";
            bool vdir_ok = true;
            if (diretorio.Length > 259)
            {
                vMensagem = vMensagem + " Diretorio [" + diretorio + "]";
                vdir_ok = false;
            }
            if (vdir_emproc.Length > 259)
            {
                vMensagem = vMensagem + " Diretorio [" + vdir_emproc.Length + "]";
                vdir_ok = false;
            }
            if (vdir_proc.Length > 259)
            {
                vMensagem = vMensagem + " Diretorio [" + vdir_proc.Length + "]";
                vdir_ok = false;
            }
            if (!vdir_ok)
            {
                vMensagem = vMensagem + " [ Diretorios tem mais de 260 caracteres, favor diminuir ]";
                _statusServicoAgendadorInfo.Validacoes = false; //scf1350
                _statusServicoAgendadorInfo.ValidacoesMsg = _statusServicoAgendadorInfo.ValidacoesMsg + " [" + vMensagem + "]";
                throw new Exception(vMensagem);
            }
            //===========================================================================================================
            // CRIA SE NÃO EXISTIR
            //===========================================================================================================
            else
            {
                try
                {
                    if (!System.IO.Directory.Exists(diretorio))
                    {
                        System.IO.Directory.CreateDirectory(diretorio);
                        System.IO.Directory.CreateDirectory(vdir_emproc);
                        System.IO.Directory.CreateDirectory(vdir_proc);
                    }
                    else
                    {
                        if (criarEmProcessamento)
                        {
                            if (!System.IO.Directory.Exists(vdir_emproc))
                                System.IO.Directory.CreateDirectory(vdir_emproc);
                        }
                        if (criarProcessado)
                        {
                            if (!System.IO.Directory.Exists(vdir_proc))
                                System.IO.Directory.CreateDirectory(vdir_proc);
                        }
                    }
                }
                catch (Exception ex) //1350
                {
                    vMensagem = vMensagem + " [" + diretorio + "] " + ex.Message;
                    _statusServicoAgendadorInfo.Validacoes = false; //scf1350
                    _statusServicoAgendadorInfo.ValidacoesMsg = _statusServicoAgendadorInfo.ValidacoesMsg + " [" + vMensagem + "]";
                }
            }
        }
        //07 - criaDiretoriosProcessamento ================================================================================================ 

        //08 - MoverArquivo ================================================================================================ 
        private bool MoverArquivo(string fileNameAndPathDestination, string fileNameAndPathSource)
        {
            if (fileNameAndPathDestination.Length > 259)
            {
                string vMensagem = "Em mover diretorio [" + fileNameAndPathDestination + "] erro: [Mais de 260 caracteres]";
                _statusServicoAgendadorInfo.Validacoes = false; //scf1350
                _statusServicoAgendadorInfo.ValidacoesMsg = _statusServicoAgendadorInfo.ValidacoesMsg + " [" + vMensagem + "]";
                throw new Exception(vMensagem);
            }
            try
            {   //===========================================================================================================
                // Confere se o arquivo existe na pasta destino, se sim, deleta
                //===========================================================================================================
                FileStream arquivoImportacao = File.Open((fileNameAndPathSource), FileMode.Open, FileAccess.Read, FileShare.None);
                arquivoImportacao.Close();
                arquivoImportacao.Dispose();
                GC.Collect();
                if (System.IO.File.Exists(fileNameAndPathDestination))
                {
                    System.IO.File.Delete(fileNameAndPathDestination);
                    var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                    lLogSCFProcessoEstagioEventoInfo.setDescricao("CAPTURA AUTOMATICA - Deletando arquivo existente [" + fileNameAndPathDestination + "]");
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }
                //===========================================================================================================
                // Movendo o arquivo
                //===========================================================================================================
                System.IO.File.Move(fileNameAndPathSource, fileNameAndPathDestination);
                return true;
            }
            catch (Exception ex) //1350
            {
                string vMensagem = "Em mover arquivo [" + fileNameAndPathDestination + "] erro: [" + ex.Message + "]";
                _statusServicoAgendadorInfo.Validacoes = false; //scf1350
                _statusServicoAgendadorInfo.ValidacoesMsg = _statusServicoAgendadorInfo.ValidacoesMsg + " [" + vMensagem + "]";
                return false;
            }
        }
    }
}