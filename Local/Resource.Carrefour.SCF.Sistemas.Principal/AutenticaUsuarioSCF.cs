﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    public class AutenticaUsuarioSCF : IComplementoAutenticacao
    {
        #region IComplementoAutenticacao Members

        public AutenticarUsuarioResponse ComplementarAutenticacao(AutenticarUsuarioRequest parametros, SessaoInfo sessaoInfo)
        {
            AutenticarUsuarioResponse res = new AutenticarUsuarioResponse();
            res.CodigoMensagem = parametros.CodigoMensagem;
            res.StatusResposta = MensagemResponseStatusEnum.OK;
            ReceberUsuarioRequest reqUsuario = new ReceberUsuarioRequest()
            {
                CodigoUsuario = sessaoInfo.CodigoUsuario,
                CodigoSessao = sessaoInfo.CodigoSessao
            };
            ReceberUsuarioResponse resUsuario = Mensageria.Processar<ReceberUsuarioResponse>(reqUsuario);

            if (resUsuario.StatusResposta != MensagemResponseStatusEnum.OK)
            {
                res.DescricaoResposta = "Usuário Inválido.";
                res.Erro = res.DescricaoResposta;
            }
            else
            {
                UsuarioSCFInfo usuarioSCF = (UsuarioSCFInfo)resUsuario.Usuario;

                // 1433 if (usuarioSCF.Bloqueado || (usuarioSCF.DataBloqueadoInicio <= DateTime.Now && DateTime.Now < usuarioSCF.DataBloqueadoFim))
                if (usuarioSCF.Bloqueado)
                {
                    res.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    res.DescricaoResposta = "Usuário Bloqueado por tempo indeterminado ( por Flag )";
                }
                if (usuarioSCF.DataBloqueadoInicio <= DateTime.Now && DateTime.Now < usuarioSCF.DataBloqueadoFim)
                {
                    res.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    res.DescricaoResposta = "Usuário Bloqueado por Periodo De [" + usuarioSCF.DataBloqueadoInicio + "] Ate [" + usuarioSCF.DataBloqueadoFim + "]";
                }
            }
            return res;
        }

        public void ComplementarUsuario(UsuarioInfo usuarioInfo)
        {
        }

        public Resource.Framework.Contratos.Comum.Dados.UsuarioInfo LocalizarUsuario(AutenticarUsuarioRequest parametros)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
