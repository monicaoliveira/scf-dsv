﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Xml.Serialization;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db;
using System.Globalization;
using Resource.Framework.Library.Db;
using Resource.Framework.Sistemas.Comum;
using System.Text.RegularExpressions;
using System.Net;
using System.Web;
using System.Windows.Forms;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    //Classe criada para atender o Jira SCF1070 - Marcos Matsuoka
    public class ServicoExecutarRegrasInicializacao
    {
        /// <summary>
        /// Faz o log nas transações que estavam em andamento quando houve o interrupção do serviço
        /// </summary>
        public void LogarTransacoesEmAndamento()
        {
            try
            {                
                foreach (ProcessoResumoInfo processo in this.ObterProcessosEmAndamento())
                {
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        "Reestart da aplicação, porém este processo continua parado!", null, processo.CodigoProcesso);//1430 mudando o descritivo
                }
            }
            catch (Exception ex)
            {
                //==========================================================================================
                // Indica que pulou este estágio
                //==========================================================================================
                var lLog = new LogSCFProcessoEventoInfo();
                lLog.setDescricao(ex.ToString()); //1333
                LogHelper.Salvar(null, lLog);


            }                                                                       
        }

        /// <summary>
        /// Caso o Servico Esteja iniciado, liberar os processos que estavam em andamento quando houve a interrupcao do serviço
        /// </summary>
        public void LiberarTransacoesEmAndamento()
        {            
            foreach (ProcessoResumoInfo processo in this.ObterProcessosEmAndamento())
            {
                if (processo.StatusEstagio != EstagioStatusEnum.AguardandoValidacao && processo.StatusEstagio != EstagioStatusEnum.Erro) //SCF-1110 - Marcos Matsuoka // codigo 5 e 9
                {
                    ExecutarProcessoResponse respostaExecutar =
                            Mensageria.Processar<ExecutarProcessoResponse>(
                                new ExecutarProcessoRequest()
                                {
                                    CodigoProcesso = processo.CodigoProcesso,
                                    Descricao = "Reestart da aplicação, processo Liberado Automaticamente", //1430 mudando o descritivo
                                    ValidaProcessoEmAndamento = false,
                                    ExecutarAssincrono = true,
                                    CodigoSessao = null
                                });
                }
            }
        }


        public List<ProcessoResumoInfo> ObterProcessosEmAndamento()
        {
            DateTime yesterday = DateTime.Now.AddDays(-1); //SCF1110 - Marcos Matsuoka            
            try
            {
                //Obtem os processos no status em andamento
                return
                Mensageria.Processar<ListarProcessoResponse>(
                          new ListarProcessoRequest()
                          {
                              FiltroStatusProcesso = ProcessoStatusEnum.EmAndamento,
                              FiltroDataInclusaoInicial = yesterday, //SCF1110 - Marcos Matsuoka
                          }).Resultado;
            }
            catch (Exception ex)
            {
                //==========================================================================================
                // Indica que pulou este estágio
                //==========================================================================================
                var lLog = new LogSCFProcessoEventoInfo();
                lLog.setDescricao(ex.ToString()); //1333
                LogHelper.Salvar(null, lLog);

                return null;
            }
        }
    }
}
