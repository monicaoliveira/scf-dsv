using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Persistencia.Db.Entidades;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Xml.Serialization;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens.Db;
using System.Globalization;
using Resource.Framework.Library.Db;
using Resource.Framework.Sistemas.Comum;
using System.Text.RegularExpressions;
using System.Net;
using System.Web;
using System.Windows.Forms;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    public class ServicoPrincipal : IServicoPrincipal
    {
        /// <summary>
        /// Construtor default
        /// </summary>
        public ServicoPrincipal()
        {
        }

        /// <summary>
        /// Referencia para a classe de configurações
        /// </summary>
        private ServicoSegurancaConfig _config =
            GerenciadorConfig.ReceberConfig<ServicoSegurancaConfig>() != null ?
            GerenciadorConfig.ReceberConfig<ServicoSegurancaConfig>() :
            new ServicoSegurancaConfig();

        /// <summary>
        /// Referencia para o serviço de persistencia da segurança
        /// </summary>
        private IServicoSegurancaPersistencia _servicoPersistencia = new ServicoSegurancaPersistencia();

        #region Listas

        /// <summary>
        /// Faz a inicialização das listas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public InicializarListasResponse InicializarListas(InicializarListasRequest request)
        {
            // Prepara resposta
            InicializarListasResponse resposta = new InicializarListasResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Garante que as permissoes estao salvas no banco
                Mensageria.Processar(new SalvarPermissoesRequest());

                // Pede inicializacao do servico de seguranca
                Mensageria.Processar(new InicializarSegurancaRequest());

                // Cria lista das listas que devem estar criadas
                List<string> nomeListas =
                    new List<string>() 
                    { 
                        "VersaoLayout", "IdentificacaoAutorizada", "IdentificacaoMoeda", "IdentificacaoTEF",
                        "TipoProduto", "MeioCaptura", "CupomFiscal", "ModalidadeVenda", "MeioPagamento", 
                        "CodigoAjuste", "CodigoAnulacao", "FormaMeioPagamento"
                    };

                // Pede a lista das listas que já estão criadas
                List<ListaInfo> listas =
                    PersistenciaHelper.Listar<ListaInfo>(
                        request.CodigoSessao);

                // Cria as listas necessarias
                foreach (string nomeLista in nomeListas)
                    if (listas.Find(l => l.Mnemonico == nomeLista) == null)
                        PersistenciaHelper.Salvar<ListaInfo>(
                            request.CodigoSessao, new ListaInfo()
                            {
                                Mnemonico = nomeLista,
                                Descricao = nomeLista
                            });
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Lista as listas fixas 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarListasFixasResponse ListarListasFixas(ListarListasFixasRequest request)
        {
            // Prepara resposta
            ListarListasFixasResponse resposta = new ListarListasFixasResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                resposta.Resultado.Add("FaixaEtaria");
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Ativa/Inativa um item das listas genérica
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverListaItemResponse RemoverListaItem(RemoverListaItemRequest request)
        {
            RemoverListaItemResponse res = new RemoverListaItemResponse();
            res.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove o item solicitado
                Mensageria.Processar<RemoverObjetoResponse<ListaItemInfo>>(
                    new RemoverListaItemDbRequest()
                    {
                        Ativar = request.Ativar,
                        CodigoObjeto = request.CodigoListaItem,
                        CodigoSessao = request.CodigoSessao
                    });
            }
            catch (Exception ex)
            {
                // Trata o erro
                res.ProcessarExcessao(ex);
            }

            // Retorna
            return res;
        }

        /// <summary>
        /// Salva item lista
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarListaItemResponse SalvarListaItem(SalvarListaItemRequest request)
        {
            // Prepara resposta
            SalvarListaItemResponse resposta = new SalvarListaItemResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                /// Verifica se nao existe o valor

                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo status do cedivel
                condicoes.Add(
                        new CondicaoInfo(
                            "CodigoLista", CondicaoTipoEnum.Igual, request.ListaItemInfo.CodigoLista));


                //Permite ao usuário buscar pelo status do cedivel
                condicoes.Add(
                        new CondicaoInfo(
                            "Valor", CondicaoTipoEnum.Igual, request.ListaItemInfo.Valor));

                // Solicita a lista
                List<ListaItemInfo> itens =
                    PersistenciaHelper.Listar<ListaItemInfo>(
                        request.CodigoSessao, condicoes);

                if (itens.Count <= 0)
                {
                    // Salva
                    resposta.ListaItemInfo =
                        PersistenciaHelper.Salvar<ListaItemInfo>(
                            request.CodigoSessao, request.ListaItemInfo);
                }
                else
                {
                    // Cria a critica
                    CriticaInfo critica = new CriticaInfo();
                    critica.Descricao = "Valor já existe no cadastro.";
                    resposta.Criticas.Add(critica);

                }

            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        public ListarListaItemResponse ListarListaItem(ListarListaItemRequest request)
        {
            ListarListaItemResponse resposta = new ListarListaItemResponse();
            resposta.PreparaResposta(request);

            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                if (request.CodigoLista > 0)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoLista", CondicaoTipoEnum.Igual, request.CodigoLista));

                if (request.NomeLista != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "NomeLista", CondicaoTipoEnum.Igual, request.NomeLista));

                if (request.Valor != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Valor", CondicaoTipoEnum.Igual, request.Valor));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ListaItemInfo>(
                        request.CodigoSessao, condicoes);

            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // retorna a resposta
            return resposta;
        }

        #endregion

        #region Produto

        /// <summary>
        /// Lista os Produtos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarProdutoResponse ListarProduto(ListarProdutoRequest request)
        {
            // Prepara resposta
            ListarProdutoResponse resposta = new ListarProdutoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo status do cedivel
                if (request.FiltroCedivel != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Cedivel", CondicaoTipoEnum.Igual, request.FiltroCedivel));

                //Recebe o maxlinhas
                if (request.FiltroMaxLinhas != null && request.FiltroMaxLinhas > 0)
                    condicoes.Add(
                        new CondicaoInfo(
                            "MaxLinhas", CondicaoTipoEnum.Igual, request.FiltroMaxLinhas));


                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ProdutoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Recebe um Produto
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberProdutoResponse ReceberProduto(ReceberProdutoRequest request)
        {
            // Prepara resposta
            ReceberProdutoResponse resposta = new ReceberProdutoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.ProdutoInfo =
                    PersistenciaHelper.Receber<ProdutoInfo>(
                        request.CodigoSessao, request.CodigoProduto);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove um Produto
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverProdutoResponse RemoverProduto(RemoverProdutoRequest request)
        {
            // Prepara resposta
            RemoverProdutoResponse resposta = new RemoverProdutoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove
                RemoverObjetoResponse<ProdutoInfo> respostaRemover =
                    Mensageria.Processar<RemoverObjetoResponse<ProdutoInfo>>(
                        new RemoverObjetoRequest<ProdutoInfo>()
                        {
                            CodigoObjeto = request.CodigoProduto
                        });

                // Informa na resposta
                if (respostaRemover.Criticas.Count > 0)
                    resposta.Criticas = respostaRemover.Criticas;
                else
                    resposta.CodigoProdutoRemovido = request.CodigoProduto;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Salva produto
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarProdutoResponse SalvarProduto(SalvarProdutoRequest request)
        {
            // Prepara resposta
            SalvarProdutoResponse resposta = new SalvarProdutoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.ProdutoInfo =
                    Mensageria.Processar<SalvarObjetoResponse<ProdutoInfo>>(
                        new SalvarObjetoRequest<ProdutoInfo>()
                        {
                            CodigoSessao = request.CodigoSessao,
                            Objeto = request.ProdutoInfo
                        }).Objeto;

            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Favorecido

        /// <summary>
        /// Lista os Favorecidos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarFavorecidoResponse ListarFavorecido(ListarFavorecidoRequest request)
        {
            // Prepara resposta
            ListarFavorecidoResponse resposta = new ListarFavorecidoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo codigo do favorecido
                if (request.FiltroCodigoFavorecido != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoFavorecido", CondicaoTipoEnum.Igual, request.FiltroCodigoFavorecido));

                //Permite ao usuário buscar pelo cnpj do favorecido
                if (request.FiltroCNPJFavorecido != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CnpjFavorecido", CondicaoTipoEnum.Igual, SCFUtils.CompletarCNPJ(request.FiltroCNPJFavorecido)));

                //Recebe o maxlinhas
                if (request.FiltroMaxLinhas != null && request.FiltroMaxLinhas > 0)
                    condicoes.Add(
                        new CondicaoInfo(
                            "MaxLinhas", CondicaoTipoEnum.Igual, request.FiltroMaxLinhas));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<FavorecidoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        //<summary>
        //Recebe um Favorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public ReceberFavorecidoResponse ReceberFavorecido(ReceberFavorecidoRequest request)
        {
            // Prepara resposta
            ReceberFavorecidoResponse resposta = new ReceberFavorecidoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.FavorecidoInfo =
                    PersistenciaHelper.Receber<FavorecidoInfo>(
                        request.CodigoSessao, request.CodigoFavorecido);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        //<summary>
        //Remove um Favorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public RemoverFavorecidoResponse RemoverFavorecido(RemoverFavorecidoRequest request)
        {
            // Prepara resposta
            RemoverFavorecidoResponse resposta = new RemoverFavorecidoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                if (request.ForcarRemocao == false)
                {
                    // cria a lista de condicoes com o codigo do favorecido
                    List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                    // insere o filtro
                    if (request.CodigoFavorecido != null)
                        condicoes.Add(new CondicaoInfo(
                            "FiltroCodigoFavorecido", CondicaoTipoEnum.Igual, request.CodigoFavorecido));

                    // preenche a lista de pagamentos
                    List<PagamentoInfo> pagamentos =
                        PersistenciaHelper.Listar<PagamentoInfo>(
                            request.CodigoSessao, condicoes);

                    // preenche a lista de transacoes
                    List<TransacaoResumoInfo> transacoes =
                        PersistenciaHelper.Listar<TransacaoResumoInfo>(
                            request.CodigoSessao, condicoes);

                    // preenche a lista de estabelecimentos
                    List<EstabelecimentoInfo> estabelecimentos =
                        PersistenciaHelper.Listar<EstabelecimentoInfo>(
                            request.CodigoSessao, condicoes);

                    if (pagamentos.Count > 0)
                    {
                        // existem pagamentos associados
                        resposta.RepassarExcessao = true;
                        resposta.StatusResposta = MensagemResponseStatusEnum.OK;
                        resposta.Criticas.Add(
                            new CriticaInfo()
                            {
                                Descricao = "O favorecido selecionado possui pagamentos vinculados.",
                                DataCritica = DateTime.Now
                            });

                        return resposta;
                    }
                    else if (transacoes.Count > 0)
                    {
                        // existem transacoes / cessoes associadas
                        resposta.RepassarExcessao = true;
                        resposta.StatusResposta = MensagemResponseStatusEnum.OK;
                        resposta.Criticas.Add(
                            new CriticaInfo()
                            {
                                Descricao = "O favorecido selecionado possui transacões / cessões vinculadas.",
                                DataCritica = DateTime.Now
                            });

                        return resposta;
                    }
                    else if (estabelecimentos.Count > 0)
                    {
                        // existem estabelecimentos associados
                        resposta.RepassarExcessao = true;
                        resposta.StatusResposta = MensagemResponseStatusEnum.OK;
                        resposta.Criticas.Add(
                            new CriticaInfo()
                            {
                                Descricao = "O favorecido selecionado possui estabelecimentos vinculados.",
                                DataCritica = DateTime.Now
                            });

                        return resposta;
                    }
                    else
                    {

                        // Remove
                        PersistenciaHelper.Remover<FavorecidoInfo>(
                            request.CodigoSessao, request.CodigoFavorecido);

                        // Informa na resposta
                        resposta.CodigoFavorecidoRemovido = request.CodigoFavorecido;
                    }
                }
                else
                {
                    // Remove
                    PersistenciaHelper.Remover<FavorecidoInfo>(
                        request.CodigoSessao, request.CodigoFavorecido);

                    // Informa na resposta
                    resposta.CodigoFavorecidoRemovido = request.CodigoFavorecido;
                }
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        //<summary>
        //Salva Favorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public SalvarFavorecidoResponse SalvarFavorecido(SalvarFavorecidoRequest request)
        {
            // Prepara resposta
            SalvarFavorecidoResponse resposta = new SalvarFavorecidoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.FavorecidoInfo =
                    PersistenciaHelper.Salvar<FavorecidoInfo>(
                        request.CodigoSessao, request.FavorecidoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region ContaFavorecido

        /// <summary>
        /// Lista os Favorecidos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarContaFavorecidoResponse ListarContaFavorecido(ListarContaFavorecidoRequest request)
        {
            // Prepara resposta
            ListarContaFavorecidoResponse resposta = new ListarContaFavorecidoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // Permite ao usuário buscar pelo codigo do ContaFavorecido
                if (request.FiltroCodigoFavorecido != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoFavorecido", CondicaoTipoEnum.Igual, request.FiltroCodigoFavorecido));

                // Permite ao usuário buscar pelo banco
                if (request.FiltroBanco != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Banco", CondicaoTipoEnum.Igual, request.FiltroBanco));

                // Permite ao usuário buscar pela agencia
                if (request.FiltroAgencia != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Agencia", CondicaoTipoEnum.Igual, request.FiltroAgencia));

                // Permite ao usuário buscar pela conta
                if (request.FiltroConta != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Conta", CondicaoTipoEnum.Igual, request.FiltroConta));

                // Permite ao usuário buscar pela agencia CCI
                if (request.FiltroAgencia_CCI != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Agencia_CCI", CondicaoTipoEnum.Igual, request.FiltroAgencia_CCI));

                // Permite ao usuário buscar pela conta CCI
                if (request.FiltroConta_CCI != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Conta_CCI", CondicaoTipoEnum.Igual, request.FiltroConta_CCI));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ContaFavorecidoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        //<summary>
        //Recebe um ContaFavorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public ReceberContaFavorecidoResponse ReceberContaFavorecido(ReceberContaFavorecidoRequest request)
        {
            // Prepara resposta
            ReceberContaFavorecidoResponse resposta = new ReceberContaFavorecidoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.ContaFavorecidoInfo =
                    PersistenciaHelper.Receber<ContaFavorecidoInfo>(
                        request.CodigoSessao, request.CodigoContaFavorecido);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        //<summary>
        //Remove um ContaFavorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public RemoverContaFavorecidoResponse RemoverContaFavorecido(RemoverContaFavorecidoRequest request)
        {
            // Prepara resposta
            RemoverContaFavorecidoResponse resposta = new RemoverContaFavorecidoResponse();
            resposta.PreparaResposta(request);

            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo nome do grupo
                if (request.CodigoContaFavorecido != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroCodigoContaFavorecido", CondicaoTipoEnum.Igual, request.CodigoContaFavorecido));

                // Solicita a lista pelo códigoContaFavorecido
                List<ContaFavorecidoProdutoInfo> lista = PersistenciaHelper.Listar<ContaFavorecidoProdutoInfo>(request.CodigoSessao, condicoes);

                // Verifica se a relação já existe
                if (lista.Count > 0)
                {
                    resposta.RepassarExcessao = false;
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    resposta.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = "A conta selecionada está sendo usada.",
                            DataCritica = DateTime.Now
                        });

                    //resposta.Erro = "A conta selecionada está sendo usada.";
                    //resposta.DescricaoResposta = "A conta selecionada está sendo usada.";
                    return resposta;
                }
                else
                {
                    // Bloco de controle
                    try
                    {
                        // Remove
                        PersistenciaHelper.Remover<ContaFavorecidoInfo>(
                            request.CodigoSessao, request.CodigoContaFavorecido);
                    }
                    catch (Exception ex)
                    {
                        // Informa erro na resposta
                        resposta.ProcessarExcessao(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }


        //<summary>
        //Salva ContaFavorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public SalvarContaFavorecidoResponse SalvarContaFavorecido(SalvarContaFavorecidoRequest request)
        {
            // Prepara resposta
            SalvarContaFavorecidoResponse resposta = new SalvarContaFavorecidoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // verifica se ja existe banco/agencia/conta para o favorecido
                ListarContaFavorecidoResponse contaFavorecido = new ListarContaFavorecidoResponse();

                // monta a lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                condicoes.Add(new CondicaoInfo("CodigoFavorecido", CondicaoTipoEnum.Igual, request.ContaFavorecidoInfo.CodigoFavorecido));

                contaFavorecido.Resultado =
                    PersistenciaHelper.Listar<ContaFavorecidoInfo>(
                        request.CodigoSessao, condicoes);

                // SCF-978 e SCF-979 (vai poder ser salvo mais de uma agencia e conta real)
                //foreach(ContaFavorecidoInfo contaFavorecidoInfo in contaFavorecido.Resultado)
                //{
                //    if (request.ContaFavorecidoInfo.Banco == contaFavorecidoInfo.Banco
                //        && request.ContaFavorecidoInfo.Agencia == contaFavorecidoInfo.Agencia
                //        && request.ContaFavorecidoInfo.Conta == contaFavorecidoInfo.Conta)
                //    {
                //        resposta.DescricaoResposta = "Banco agência e conta já cadastrados para o favorecido";
                //        return resposta;
                //    }
                //}

                // Salva
                resposta.ContaFavorecidoInfo =
                    PersistenciaHelper.Salvar<ContaFavorecidoInfo>(
                        request.CodigoSessao, request.ContaFavorecidoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        // SCF1701 - ID 27
        public SubstituirContaFavorecidoResponse SubstituirContaFavorecido(SubstituirContaFavorecidoRequest request)
        {
            ServicoPrincipal _executar = new ServicoPrincipal();
            SalvarFavorecidoRequest _param =
                new SalvarFavorecidoRequest()
                {
                    FavorecidoInfo = new FavorecidoInfo
                    {
                        //Bloqueando o Favorecido para Edição
                        CodigoFavorecido = request.CodigoFavorecido,
                        FavorecidoBloqueado = true
                    }
                };
            _executar.SalvarFavorecido(_param);

            // Prepara resposta
            SubstituirContaFavorecidoResponse resposta = new SubstituirContaFavorecidoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                ProcessoFavorecidoSubstituirContaInfo processoInfo =
                    new ProcessoFavorecidoSubstituirContaInfo()
                    {
                        CodigoFavorecido = request.CodigoFavorecido,
                        CodigoContaFavorecido = request.CodigoContaFavorecido,
                        CodigoContaFavorecidoNovo = request.CodigoContaFavorecidoNovo
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        #endregion

        #region ContaFavorecidoProduto

        //<summary>
        //Salva Favorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public SalvarContaFavorecidoProdutoResponse SalvarContaFavorecidoProduto(SalvarContaFavorecidoProdutoRequest request)
        {
            // Prepara resposta
            SalvarContaFavorecidoProdutoResponse resposta = new SalvarContaFavorecidoProdutoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.ContaFavorecidoProdutoInfo = PersistenciaHelper.Salvar<ContaFavorecidoProdutoInfo>(request.CodigoSessao, request.ContaFavorecidoProdutoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        //==========================================================================================A
        public ListarContaFavorecidoProdutoResponse ListarContaFavorecidoProduto(ListarContaFavorecidoProdutoRequest request)
        {
            //==========================================================================================
            // Prepara resposta
            //==========================================================================================
            ListarContaFavorecidoProdutoResponse resposta = new ListarContaFavorecidoProdutoResponse();
            resposta.PreparaResposta(request);
            //==========================================================================================
            // Bloco de controle
            //==========================================================================================
            try
            {
                //==========================================================================================
                // Monta a lista de condições
                //==========================================================================================
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                //==========================================================================================
                //PRODUTO
                //==========================================================================================
                if (request.FiltroCodigoProduto != null)
                    condicoes.Add(new CondicaoInfo("FiltroCodigoProduto", CondicaoTipoEnum.Igual, request.FiltroCodigoProduto));
                //==========================================================================================
                //FAVORECIDO
                //==========================================================================================
                if (request.FiltroCodigoFavorecido != null)
                    condicoes.Add(new CondicaoInfo("FiltroCodigoFavorecido", CondicaoTipoEnum.Igual, request.FiltroCodigoFavorecido));
                //==========================================================================================
                //REFERENCIA
                //==========================================================================================
                if (request.FiltroReferencia != null) //1339
                    condicoes.Add(new CondicaoInfo("FiltroReferencia", CondicaoTipoEnum.Igual, request.FiltroReferencia));
                //==========================================================================================
                // Solicita a lista
                //==========================================================================================
                resposta.Resultado = PersistenciaHelper.Listar<ContaFavorecidoProdutoInfo>(request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                //==========================================================================================
                // Trata erro
                //==========================================================================================
                resposta.ProcessarExcessao(ex);
            }
            //==========================================================================================
            // Retorna
            //==========================================================================================
            return resposta;
        }
        //==========================================================================================A

        //<summary>
        //Remove um ContaFavorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public RemoverContaFavorecidoProdutoResponse RemoverContaFavorecidoProduto(RemoverContaFavorecidoProdutoRequest request)
        {
            // Prepara resposta
            RemoverContaFavorecidoProdutoResponse resposta = new RemoverContaFavorecidoProdutoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove o item solicitado
                Mensageria.Processar<RemoverObjetoResponse<ContaFavorecidoProdutoInfo>>(
                    new RemoverContaFavorecidoDbRequest()
                    {
                        CodigoContaFavorecido = request.CodigoContaFavorecido,
                        CodigoProduto = request.CodigoProduto,
                        Referencia = request.Referencia, //1353
                        CodigoSessao = request.CodigoSessao
                    });

            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        // SCF1701 - ID 27
        //<summary>
        //Substitui uma ContaFavorecido
        //</summary>
        //<param name="request"></param>
        //<returns></returns>
        public SubstituirContaFavorecidoProdutoResponse SubstituirContaFavorecidoProduto(SubstituirContaFavorecidoProdutoRequest request)
        {
            // Prepara resposta
            SubstituirContaFavorecidoProdutoResponse resposta = new SubstituirContaFavorecidoProdutoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                ProcessoFavorecidoProdutoSubstituirContaInfo processoInfo = new ProcessoFavorecidoProdutoSubstituirContaInfo() { };
                processoInfo.CodigoFavorecido = request.CodigoFavorecido;

                for (var i = 0; i < request.ListaFavorecidoProduto.Count; i++)
                {
                    //if (request.ListaFavorecidoProduto[i].Transacoes > 0)
                    //{
                    ProcessoFavorecidoProdutoLista dados = new ProcessoFavorecidoProdutoLista();

                    dados.CodigoContaFavorecido = request.ListaFavorecidoProduto[i].CodigoContaFavorecido;
                    dados.CodigoProduto = request.ListaFavorecidoProduto[i].CodigoProduto;
                    dados.Referencia = request.ListaFavorecidoProduto[i].Referencia;
                    dados.Transacoes = request.ListaFavorecidoProduto[i].Transacoes;

                    processoInfo.ListaFavorecidoProduto.Add(dados);
                    //}
                }


                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }
        #endregion

        #region EmpresaGrupo

        /// <summary>
        /// Lista os Grupos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarEmpresaGrupoResponse ListarEmpresaGrupo(ListarEmpresaGrupoRequest request)
        {
            // Prepara resposta
            ListarEmpresaGrupoResponse resposta = new ListarEmpresaGrupoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo nome do grupo
                if (request.FiltroNomeEmpresaGrupo != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "NomeEmpresaGrupo", CondicaoTipoEnum.Igual, request.FiltroNomeEmpresaGrupo));

                //Recebe o maxlinhas
                if (request.FiltroMaxLinhas != null && request.FiltroMaxLinhas > 0)
                    condicoes.Add(
                        new CondicaoInfo(
                            "MaxLinhas", CondicaoTipoEnum.Igual, request.FiltroMaxLinhas));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<EmpresaGrupoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Receber um Grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberEmpresaGrupoResponse ReceberEmpresaGrupo(ReceberEmpresaGrupoRequest request)
        {
            // Prepara resposta
            ReceberEmpresaGrupoResponse resposta = new ReceberEmpresaGrupoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.EmpresaGrupoInfo =
                    PersistenciaHelper.Receber<EmpresaGrupoInfo>(
                        request.CodigoSessao, request.CodigoEmpresaGrupo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove um Grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverEmpresaGrupoResponse RemoverEmpresaGrupo(RemoverEmpresaGrupoRequest request)
        {
            // Prepara resposta
            RemoverEmpresaGrupoResponse resposta = new RemoverEmpresaGrupoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove
                PersistenciaHelper.Remover<EmpresaGrupoInfo>(
                    request.CodigoSessao, request.CodigoEmpresaGrupo);

                // Informa na resposta
                resposta.CodigoEmpresaGrupoRemovido = request.CodigoEmpresaGrupo;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Salva Grupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarEmpresaGrupoResponse SalvarEmpresaGrupo(SalvarEmpresaGrupoRequest request)
        {
            // Prepara resposta
            SalvarEmpresaGrupoResponse resposta = new SalvarEmpresaGrupoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.EmpresaGrupoInfo =
                    PersistenciaHelper.Salvar<EmpresaGrupoInfo>(
                        request.CodigoSessao, request.EmpresaGrupoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region EmpresaSubGrupo

        /// <summary>
        /// Lista os Subgrupos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarEmpresaSubGrupoResponse ListarEmpresaSubGrupo(ListarEmpresaSubGrupoRequest request)
        {
            // Prepara resposta
            ListarEmpresaSubGrupoResponse resposta = new ListarEmpresaSubGrupoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo codigo do subgrupo
                if (request.FiltroCodigoEmpresaSubGrupo != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoEmpresaSubGrupo", CondicaoTipoEnum.Igual, request.FiltroCodigoEmpresaSubGrupo));

                //Permite ao usuário buscar pelo codigo do grupo
                if (request.FiltroCodigoEmpresaGrupo != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoEmpresaGrupo", CondicaoTipoEnum.Igual, request.FiltroCodigoEmpresaGrupo));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<EmpresaSubGrupoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Receber um subgrupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberEmpresaSubGrupoResponse ReceberEmpresaSubGrupo(ReceberEmpresaSubGrupoRequest request)
        {
            // Prepara resposta
            ReceberEmpresaSubGrupoResponse resposta = new ReceberEmpresaSubGrupoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.EmpresaSubGrupoInfo =
                    PersistenciaHelper.Receber<EmpresaSubGrupoInfo>(
                        request.CodigoSessao, request.CodigoEmpresaSubGrupo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove um subgrupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverEmpresaSubGrupoResponse RemoverEmpresaSubGrupo(RemoverEmpresaSubGrupoRequest request)
        {
            // Prepara resposta
            RemoverEmpresaSubGrupoResponse resposta = new RemoverEmpresaSubGrupoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove
                PersistenciaHelper.Remover<EmpresaSubGrupoInfo>(
                    request.CodigoSessao, request.CodigoEmpresaSubGrupo);

                // Informa na resposta
                resposta.CodigoEmpresaSubGrupo = request.CodigoEmpresaSubGrupo;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Salva subgrupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarEmpresaSubGrupoResponse SalvarEmpresaSubGrupo(SalvarEmpresaSubGrupoRequest request)
        {
            // Prepara resposta
            SalvarEmpresaSubGrupoResponse resposta = new SalvarEmpresaSubGrupoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.EmpresaSubGrupoInfo =
                    PersistenciaHelper.Salvar<EmpresaSubGrupoInfo>(
                        request.CodigoSessao, request.EmpresaSubGrupoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Tipo de Estorno
        //==================================================================================================================================
        // 963 INICIO - CARREGAR COMBO - site_configuracao_cadastro_protocolo_cheque_devolvido >> carregarTipoEstorno >> cmbFiltroTipoEstorno
        //==================================================================================================================================
        public ListarTipoEstornoResponse ListarTipoEstorno(ListarTipoEstornoRequest request)
        {
            ListarTipoEstornoResponse resposta = new ListarTipoEstornoResponse();
            resposta.PreparaResposta(request);
            try
            {
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                condicoes.Add(new CondicaoInfo("NomeLista", CondicaoTipoEnum.Igual, "TipoEstorno"));
                if (request.FiltroTipoEstorno != null) //1465
                    condicoes.Add(new CondicaoInfo("Valor", CondicaoTipoEnum.Igual, request.FiltroTipoEstorno));
                resposta.Resultado = PersistenciaHelper.Listar<ListaItemInfo>(null, condicoes); // ListaItemDbLib >> ConsultarObjetosResponse >> PR_LISTA_ITEM_L
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }
        //==================================================================================================================================
        // 963 FIM - CARREGAR COMBO - site_configuracao_cadastro_protocolo_cheque_devolvido >> carregarTipoEstorno >> cmbFiltroTipoEstorno
        //==================================================================================================================================

        //==================================================================================================================================
        // 963 INICIO - SALVAR LINHA DO GRID - site_configuracao_cadastro_protocolo_cheque_devolvido >> salvarLinha
        //==================================================================================================================================
        public SalvarProtocoloChequeResponse SalvarProtocoloCheque(SalvarProtocoloChequeRequest request)
        {
            SalvarProtocoloChequeResponse resposta = new SalvarProtocoloChequeResponse();
            resposta.PreparaResposta(request);
            var VarDataEstorno = request.DataEstorno;
            var VarDataRegProtocolo = request.DataRegProtocolo;
            var VarNumeroProtocolo = request.NumeroProtocolo;
            var VarCodigoTransacao = request.CodigoTransacao;
            var VarTipoEstorno = request.TipoEstorno; //963 
            try
            {
                ProcessoProtocoloChequeInfo protocoloInfo = PersistenciaHelper.Receber<ProcessoProtocoloChequeInfo>(request.CodigoSessao, request.CodigoTransacao); // ProtocoloChequeDbLib >> ReceberObjetoResponse >> PR_CHEQUE_DEVOLVIDO_B
                protocoloInfo.DataEstorno = VarDataEstorno;
                protocoloInfo.DataRegProtocolo = VarDataRegProtocolo;
                protocoloInfo.NumeroProtocolo = VarNumeroProtocolo;
                protocoloInfo.CodigoTransacao = VarCodigoTransacao;
                protocoloInfo.TipoEstorno = VarTipoEstorno;
                resposta.ProcessoProtocoloChequeInfo = PersistenciaHelper.Salvar<ProcessoProtocoloChequeInfo>(request.CodigoSessao, protocoloInfo); // ProtocoloChequeDbLib >> SalvarObjetoResponse >> PR_CHEQUE_DEVOLVIDO_S
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }

        //==================================================================================================================================
        // 1465 INICIO - relatorios excel e pdf
        //==================================================================================================================================
        public GerarRelatorioChequesExcelResponse GerarRelatorioChequesExcel(GerarRelatorioChequesExcelRequest request) //1465
        {
            // prepara a resposta
            GerarRelatorioChequesExcelResponse resposta = new GerarRelatorioChequesExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioChequesExcelInfo processoInfo =
                    new ProcessoRelatorioChequesExcelInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioChequesExcel(request);
            }

            // retorna a resposta
            return resposta;
        }
        private GerarRelatorioChequesExcelResponse gerarRelatorioChequesExcel(GerarRelatorioChequesExcelRequest request) //1465
        {
            // Prepara resposta
            GerarRelatorioChequesExcelResponse respostaRelatorioExcel = new GerarRelatorioChequesExcelResponse();
            respostaRelatorioExcel.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            var filtros = request.Request;
            StringBuilder sb = new StringBuilder();
            //=========================================================================================
            // GERANDO LINHA DOS FILTROS
            //=========================================================================================
            sb.Append("Filtros:  Tipo de Registro = AP");
            sb.Append(";Meio de Pagamento = 4 - Devolução cheques");
            //=========================================================================================
            sb.Append(";Numero Protocolo = ");
            if (filtros.FiltroNumeroProtocolo == "" || filtros.FiltroNumeroProtocolo == null)
                sb.Append("Todos");
            else
                sb.Append(filtros.FiltroNumeroProtocolo);
            //=========================================================================================
            sb.Append(";Conta Cliente = ");
            if (filtros.FiltroContaCliente == "" || filtros.FiltroContaCliente == null)
                sb.Append("Todas");
            else
                sb.Append(filtros.FiltroContaCliente);
            //=========================================================================================
            // Tratando estabelecimento
            //=========================================================================================
            sb.Append(";Estabelcimento = ");
            string codigoEstabelecimento = filtros.FiltroEstabelecimento;
            if (codigoEstabelecimento == "" || codigoEstabelecimento == null)
                sb.Append("Todos");
            else
            {
                ReceberEstabelecimentoResponse reg = Mensageria.Processar<ReceberEstabelecimentoResponse>
                (new ReceberEstabelecimentoRequest()
                {
                    CodigoEstabelecimento = codigoEstabelecimento
                }
                );
                if (reg.StatusResposta == Resource.Framework.Library.Servicos.Mensagens.MensagemResponseStatusEnum.OK)
                    sb.Append(reg.EstabelecimentoInfo.CNPJ + "-" + reg.EstabelecimentoInfo.RazaoSocial);
                else
                    sb.Append(codigoEstabelecimento + " não cadastrado");
            }
            //=========================================================================================
            // Tratando tipo estorno
            //=========================================================================================
            sb.Append(";Tipo Estorno = ");
            string codigotipoEstorno = filtros.FiltroTipoEstorno;
            if (codigotipoEstorno == "" || codigotipoEstorno == null)
                sb.Append("Todos");
            else
            {
                ListarTipoEstornoResponse ListaTipoEstorno = Mensageria.Processar<ListarTipoEstornoResponse>(new ListarTipoEstornoRequest() { FiltroTipoEstorno = codigotipoEstorno });
                foreach (ListaItemInfo itemtipoEstorno in ListaTipoEstorno.Resultado)
                {
                    if (codigotipoEstorno == itemtipoEstorno.Valor)
                    {
                        sb.Append(itemtipoEstorno.Valor + "-" + itemtipoEstorno.Descricao);
                        break;
                    }
                    else
                        sb.Append(codigotipoEstorno + " não cadastrado");
                }
            }
            //=========================================================================================
            sb.Append(";Sts Envio CCI = ");
            string codigoEnvioCci = filtros.FiltroEnvioCci;
            if (codigoEnvioCci == "" || codigoEnvioCci == null)
                sb.Append("Todos");
            else
            {
                if (codigoEnvioCci == "1")
                    sb.Append("Enviadas");
                else
                    if (codigoEnvioCci == "9")
                        sb.Append("Não Enviadas");
                    else
                        sb.Append("Todos");
            }
            //=========================================================================================
            // Tratando Status Protocolo
            //=========================================================================================
            sb.Append(";Sts Protocolo = ");
            string codigostatusProtocolo = filtros.FiltroStatusProtocolo;
            if (codigostatusProtocolo == "" || codigostatusProtocolo == null)
                sb.Append("Todos");
            else
            {
                if (codigostatusProtocolo == "S")
                    sb.Append("Concluídos");
                else
                    if (codigostatusProtocolo == "N")
                        sb.Append("Pendentes");
                    else
                        sb.Append("Status Invalido: " + codigostatusProtocolo);
            }
            //=========================================================================================
            sb.Append(";Período Estorno de ");
            if (filtros.FiltroDataEstornoDe != null && filtros.FiltroDataEstornoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(filtros.FiltroDataEstornoDe).ToString("dd/MM/yyyy"));
            else
                sb.Append("Qualquer");
            sb.Append(" ate ");
            if (filtros.FiltroDataEstornoAte != null && filtros.FiltroDataEstornoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(filtros.FiltroDataEstornoAte).ToString("dd/MM/yyyy"));
            else
                sb.Append("Qualquer");
            //=========================================================================================
            sb.Append(";Período Registro Protocolo de ");
            if (filtros.FiltroDataRegProtocoloDe != null && filtros.FiltroDataRegProtocoloDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(filtros.FiltroDataRegProtocoloDe).ToString("dd/MM/yyyy"));
            else
                sb.Append("Qualquer");
            sb.Append(" até ");
            if (filtros.FiltroDataRegProtocoloAte != null && filtros.FiltroDataRegProtocoloAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(filtros.FiltroDataRegProtocoloAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer");
            //=========================================================================================
            sb.Append(";Período Processamento de ");
            if (filtros.FiltroDataProcessamentoDe != null && filtros.FiltroDataProcessamentoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(filtros.FiltroDataProcessamentoDe).ToString("dd/MM/yyyy"));
            else
                sb.Append("Qualquer");
            sb.Append(" ate ");
            if (filtros.FiltroDataProcessamentoAte != null && filtros.FiltroDataProcessamentoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(filtros.FiltroDataProcessamentoAte).ToString("dd/MM/yyyy"));
            else
                sb.Append("Qualquer");
            //=========================================================================================
            StreamWriter file = null;
            try
            {
                ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Iniciando validações", request.CodigoSessao, request.CodigoProcesso);
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques == "")
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioExcel.Erro,
                            DataCritica = DateTime.Now
                        });
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);
                    return respostaRelatorioExcel;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques.Length - 1) == @"\")
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques;
                    else
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques + "\\";
                }
                UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                DateTime dataAtual = DateTime.Now;

                string nome = "";
                if (usuarioInfo.Nome.Contains(" "))
                    nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                else
                    nome = usuarioInfo.Nome;

                string caminho = respostaRelatorioExcel.CaminhoArquivoGerado
                    + nome + @"_RelChequesDevolvidos_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + ".csv";

                if (!System.IO.Directory.Exists(respostaRelatorioExcel.CaminhoArquivoGerado))
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioExcel.CaminhoArquivoGerado + "]";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioExcel.Erro,
                        DataCritica = DateTime.Now
                    });

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);
                    return respostaRelatorioExcel;
                }
                else
                {
                    file = new StreamWriter(caminho, true, Encoding.Default);

                    try
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Arquivo criado:" + caminho, request.CodigoSessao, request.CodigoProcesso);
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Realizando consulta dos Cheques Devolvidos no banco de dados", request.CodigoSessao, request.CodigoProcesso);
                    }
                    catch (Exception e)
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Excecao:" + e.InnerException, request.CodigoSessao, request.CodigoProcesso);
                    }
                    ListarProtocoloChequeResponse resposta =
                        Mensageria.Processar<ListarProtocoloChequeResponse>(
                            new ListarProtocoloChequeRequest()
                            {
                                CodigoSessao = request.CodigoSessao
                            ,
                                FiltroStatusProtocolo = filtros.FiltroStatusProtocolo
                            ,
                                FiltroEnvioCci = filtros.FiltroEnvioCci
                            ,
                                FiltroTipoEstorno = filtros.FiltroTipoEstorno
                            ,
                                FiltroEstabelecimento = filtros.FiltroEstabelecimento
                            ,
                                FiltroContaCliente = filtros.FiltroContaCliente
                            ,
                                FiltroNumeroProtocolo = filtros.FiltroNumeroProtocolo
                            ,
                                FiltroDataProcessamentoDe = filtros.FiltroDataProcessamentoDe
                            ,
                                FiltroDataProcessamentoAte = filtros.FiltroDataProcessamentoAte + ts
                            ,
                                FiltroDataRegProtocoloDe = filtros.FiltroDataRegProtocoloDe
                            ,
                                FiltroDataRegProtocoloAte = filtros.FiltroDataRegProtocoloAte + ts
                            ,
                                FiltroDataEstornoAte = filtros.FiltroDataEstornoAte + ts
                            ,
                                FiltroDataEstornoDe = filtros.FiltroDataEstornoDe
                            ,
                                TipoRelatorio = "Excel"
                            });

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Consulta dos Cheques Devolvidos finalizada", request.CodigoSessao, request.CodigoProcesso);
                    file.WriteLine(sb.ToString());
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(sb.ToString().Replace(";", ","), request.CodigoSessao, request.CodigoProcesso);
                    //=========================================================================================
                    // GERANDO LINHA DAS COLUNAS - 11 colunas
                    //=========================================================================================
                    String dados;
                    dados = "Data Processamento;Conta Cliente;NSUHOST;NSUHOST Original;Estabelecimento;Valor Repasse;Data Protocolo;Data Estorno;Numero Protocolo;Tipo Estorno;PSL;";
                    file.WriteLine(dados);
                    //=========================================================================================
                    // GERANDO LINHA DO DETALHE - 11 colunas
                    // ==========================================================================================
                    // Adicionado "=\" nos campos que contem números mas precisam ser expressos como textos
                    // ==========================================================================================
                    int quantidadeLog = 0;
                    codigostatusProtocolo = "";
                    string descricaostatusProtocolo = "";
                    codigotipoEstorno = "";
                    string descricaotipoEstorno = "";
                    foreach (ProcessoProtocoloChequeInfo relatorioCheques in resposta.Resultado)
                    {
                        // ==========================================================================================
                        // Tratando Tipo Estorno
                        // ==========================================================================================
                        codigotipoEstorno = relatorioCheques.TipoEstorno;
                        descricaotipoEstorno = "";
                        ListarTipoEstornoResponse ListaTipoEstorno = Mensageria.Processar<ListarTipoEstornoResponse>(new ListarTipoEstornoRequest() { FiltroTipoEstorno = codigotipoEstorno });
                        foreach (ListaItemInfo itemtipoEstorno in ListaTipoEstorno.Resultado)
                        {
                            if (codigotipoEstorno == itemtipoEstorno.Valor)
                            {
                                descricaotipoEstorno = itemtipoEstorno.Valor + "-" + itemtipoEstorno.Descricao;
                                break;
                            }
                        }
                        // ==========================================================================================
                        // Tratando status protocolo
                        // ==========================================================================================
                        codigostatusProtocolo = relatorioCheques.StatusProtocolo;
                        descricaostatusProtocolo = "";
                        if (codigostatusProtocolo == "S")
                            descricaostatusProtocolo = "Concluídos";
                        else
                        {
                            if (codigostatusProtocolo == "N")
                                descricaostatusProtocolo = "Pendentes";
                        }
                        // ==========================================================================================
                        dados =
                              String.Format("{0:dd/MM/yyyy}", relatorioCheques.DataProcessamento) + ";"
                            + "=\"" + relatorioCheques.ContaCliente + "\";"
                            + "=\"" + relatorioCheques.NsuHost + "\";"
                            + "=\"" + relatorioCheques.NsuHostOriginal + "\";"
                            + "=\"" + relatorioCheques.Estabelecimento + "\";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioCheques.Valor) + ";"
                            + String.Format("{0:dd/MM/yyyy}", relatorioCheques.DataRegProtocolo) + ";"
                            + String.Format("{0:dd/MM/yyyy}", relatorioCheques.DataEstorno) + ";"
                            + "=\"" + relatorioCheques.NumeroProtocolo + "\";"
                            + "=\"" + descricaotipoEstorno + "\";"
                            + "=\"" + descricaostatusProtocolo + "\";"
                            ;
                        file.WriteLine(dados);
                        respostaRelatorioExcel.QuantidadeLinhasGeradas++;
                        quantidadeLog++;
                        if (quantidadeLog >= 100)
                        {
                            LogSCFProcessoEstagioEventoInfo.EfetuarLog(respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);
                            quantidadeLog = 0;
                        }
                    }
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Total de linhas geradas: " + respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);
                }
            }
            catch (Exception ex)
            {
                respostaRelatorioExcel.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }
            return respostaRelatorioExcel;
        }
        public GerarRelatorioChequesPDFResponse GerarRelatorioChequesPDF(GerarRelatorioChequesPDFRequest request) //1465
        {
            // prepara a resposta
            GerarRelatorioChequesPDFResponse resposta = new GerarRelatorioChequesPDFResponse();
            resposta.PreparaResposta(request);

            ProcessoRelatorioChequesPDFInfo processoInfo = null;

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                processoInfo =
                    new ProcessoRelatorioChequesPDFInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                // Executa o processo assíncrono              
                resposta = gerarRelatorioChequesPDF(request);
            }

            // retorna a resposta
            return resposta;
        }
        private GerarRelatorioChequesPDFResponse gerarRelatorioChequesPDF(GerarRelatorioChequesPDFRequest request) //1465
        {
            // Prepara resposta
            GerarRelatorioChequesPDFResponse respostaRelatorioPdf = new GerarRelatorioChequesPDFResponse();
            respostaRelatorioPdf.PreparaResposta(request);

            StreamWriter file = null;
            try
            {
                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques == "")
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioPdf.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioPdf;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques.Length - 1) == @"\")
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques;
                    else
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioCheques + "\\";
                }

                UsuarioInfo usuarioInfo = new UsuarioInfo();

                DateTime dataAtual = DateTime.Now;
                string nome = string.Empty;

                //Caso o codigo de sessao seja nulo, atribui um nome do usuario "Automatico", pois se trata da execução do processo
                //LiberarTransacoesEmAndamento executado quando o servidor de aplicação é reiniciado.
                if (request.CodigoSessao != null)
                {
                    // pega o usuario para recuperar o nome e colocar no arquivo gerado
                    usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                    if (usuarioInfo.Nome.Contains(" "))
                        nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                    else
                        nome = usuarioInfo.Nome;
                }
                else
                {
                    //Atribui o nome Automatico pois ainda não existe Sessão válida
                    nome = "Automatico";
                }
                //Fim

                string caminho = respostaRelatorioPdf.CaminhoArquivoGerado
                    + nome + @"_RelChequesDevolvidos_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + (request.Request.TipoArquivo == null ? ".csv" : ".pdf");

                if (!System.IO.Directory.Exists(respostaRelatorioPdf.CaminhoArquivoGerado))
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioPdf.CaminhoArquivoGerado + "]";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioPdf.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioPdf;
                }

                request.Request.Link = request.Request.Link + "&codLog=" + request.CodigoProcesso;
                string urlAddress = request.Request.Link;
                string strCSS = null;

                ServicePointManager.ServerCertificateValidationCallback =
                    new System.Net.Security.RemoteCertificateValidationCallback(
                        (object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors) =>
                    {
                        return true;
                    });

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                webRequest.Timeout = 1000 * 60 * 60 * 5; // 5 horas
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0";
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.AllowAutoRedirect = true;
                webRequest.ContentType = "application/x-www-form-urlencoded";

                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioChequesPDF - Efetuando requisição WEB: " + request.Request.Link, request.CodigoSessao, request.CodigoProcesso);

                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

                string data = "";
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                       "Iniciando conversão do HTML para PDF. ", request.CodigoSessao, request.CodigoProcesso);

                    Stream receiveStream = webResponse.GetResponseStream();
                    StreamReader readStream = null;

                    if (webResponse.CharacterSet == null)
                    {
                        readStream = new StreamReader(receiveStream);
                    }
                    else
                    {
                        readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    }

                    data = readStream.ReadToEnd();
                    if (data != "")
                    {
                        // Recupera Estilos do CSS para aplicar no relatório PDF
                        urlAddress = urlAddress.Replace(".aspx", ".css");
                        int posIni = urlAddress.IndexOf("?");
                        urlAddress = urlAddress.Substring(0, posIni);

                        posIni = data.IndexOf("link href");
                        if (posIni > 0)
                        {
                            int posFim = data.IndexOf(".css", posIni) + 3;
                            string urlAddressCss = data.Substring(posIni + 11, posFim - posIni - 10);

                            posFim = urlAddress.LastIndexOf(@"/");
                            urlAddress = urlAddress.Substring(0, posFim + 1) + urlAddressCss;
                        }

                        try
                        {
                            webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                            webResponse = (HttpWebResponse)webRequest.GetResponse();

                            receiveStream = null;
                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                receiveStream = webResponse.GetResponseStream();
                                strCSS = new StreamReader(receiveStream, Encoding.UTF8).ReadToEnd();
                            }
                        }
                        catch (Exception ) {/* ignorar erro caso não tenha CSS */} // catch (Exception ex) {/* ignorar erro caso não tenha CSS */} // warning 25/02/2019
                    }
                    webResponse.Close();
                    readStream.Close();
                }

                if (data != "")
                {
                    //Create a byte array that will eventually hold our final PDF
                    Byte[] bytes;
                    ITextEvents ev = new ITextEvents(null, null);

                    //Gerar o PDF a partir do HTML
                    bytes = ev.ImprimirPDF(data, strCSS, "A4,Rotate");

                    //Now we just need to do something with those bytes.
                    //Here I'm writing them to disk but if you were in ASP.Net you might Response.BinaryWrite() them.
                    //You could also write the bytes to a database in a varbinary() column (but please don't) or you
                    //could pass them to another function for further PDF processing.
                    System.IO.File.WriteAllBytes(caminho, bytes);

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        "Conversão concluída. Iniciando a criação do PDF em arquivo.\n" + caminho, request.CodigoSessao, request.CodigoProcesso);
                }
            }
            catch (Exception ex)
            {
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioChequesPDF -  - Erro na requisição:" + ex.Message, request.CodigoSessao, request.CodigoProcesso);

                // Trata erro
                respostaRelatorioPdf.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioPdf;
        }
        public ListarProtocoloChequeResponse ListarRelatorioCheques(ListarProtocoloChequeRequest request) //1465
        {
            // Prepara resposta
            ListarProtocoloChequeResponse resposta = new ListarProtocoloChequeResponse();
            resposta.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            try
            {
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                if (request.FiltroDataEstornoDe.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataEstorno", CondicaoTipoEnum.MaiorIgual, request.FiltroDataEstornoDe.Value));
                if (request.FiltroDataEstornoAte.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataEstorno", CondicaoTipoEnum.MenorIgual, request.FiltroDataEstornoAte.Value + ts));
                //1465 if (request.FiltroDataEstorno.HasValue)
                //    condicoes.Add(new CondicaoInfo("FiltroDataEstorno",CondicaoTipoEnum.Igual,request.FiltroDataEstorno.Value));
                if (request.FiltroEnvioCci != null)
                    condicoes.Add(new CondicaoInfo("FiltroEnvioCci", CondicaoTipoEnum.Igual, request.FiltroEnvioCci));
                if (request.FiltroNumeroProtocolo != null && request.FiltroNumeroProtocolo != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroNumeroProtocolo", CondicaoTipoEnum.Igual, request.FiltroNumeroProtocolo));
                if (request.FiltroEstabelecimento != null && request.FiltroEstabelecimento != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroEstabelecimento", CondicaoTipoEnum.Igual, request.FiltroEstabelecimento));
                if (request.FiltroDataRegProtocoloDe.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataRegProtocolo", CondicaoTipoEnum.MaiorIgual, request.FiltroDataRegProtocoloDe.Value));
                if (request.FiltroDataRegProtocoloAte.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataRegProtocolo", CondicaoTipoEnum.MenorIgual, request.FiltroDataRegProtocoloAte.Value + ts));
                //1465if (request.FiltroDataRegProtocolo.HasValue)
                //1465condicoes.Add(new CondicaoInfo("FiltroDataRegProtocolo",CondicaoTipoEnum.Igual,request.FiltroDataRegProtocolo.Value));
                if (request.FiltroStatusProtocolo != null && request.FiltroStatusProtocolo != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroStatusProtocolo", CondicaoTipoEnum.Igual, request.FiltroStatusProtocolo));
                if (request.FiltroContaCliente != null && request.FiltroContaCliente != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroContaCliente", CondicaoTipoEnum.Igual, request.FiltroContaCliente));
                if (request.FiltroTipoEstorno != null && request.FiltroTipoEstorno != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroTipoEstorno", CondicaoTipoEnum.Igual, request.FiltroTipoEstorno));
                if (request.FiltroDataProcessamentoDe.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataProcessamento", CondicaoTipoEnum.MaiorIgual, request.FiltroDataProcessamentoDe.Value));
                if (request.FiltroDataProcessamentoAte.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataProcessamento", CondicaoTipoEnum.MenorIgual, request.FiltroDataProcessamentoAte.Value + ts));
                //1465 if (request.FiltroDataProcessamento.HasValue)
                //1465condicoes.Add(new CondicaoInfo("FiltroDataProcessamento",CondicaoTipoEnum.Igual,request.FiltroDataProcessamento.Value));

                resposta.Resultado = PersistenciaHelper.Listar<ProcessoProtocoloChequeInfo>(request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }
        //==================================================================================================================================
        // 1465 FIM - relatorios excel e pdf
        //==================================================================================================================================
        #endregion

        #region Estabelecimento
        //==================================================================================================================================
        // 963 INICIO - CARREGAR COMBO - site_configuracao_cadastro_protocolo_cheque_devolvido >> carregarEstabelecimentos >> cmbEstabelecimento
        //==================================================================================================================================
        public ListarEstabelecimentoResponse ListarEstabelecimento(ListarEstabelecimentoRequest request)
        {
            ListarEstabelecimentoResponse resposta = new ListarEstabelecimentoResponse();
            resposta.PreparaResposta(request);

            try
            {
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                if (request.FiltroAtivo != null && request.FiltroAtivo != "")
                    condicoes.Add(new CondicaoInfo("Ativo", CondicaoTipoEnum.Igual, request.FiltroAtivo));
                if (request.FiltroCodigoCsu != null && request.FiltroCodigoCsu != "")
                    condicoes.Add(new CondicaoInfo("CodigoCsu", CondicaoTipoEnum.Igual, request.FiltroCodigoCsu));
                if (request.FiltroCodigoSitef != null && request.FiltroCodigoSitef != "")
                    condicoes.Add(new CondicaoInfo("CodigoSitef", CondicaoTipoEnum.Igual, request.FiltroCodigoSitef));
                if (request.FiltroCodigoCnpj != null && request.FiltroCodigoCnpj != "")
                    condicoes.Add(new CondicaoInfo("CodigoCnpj", CondicaoTipoEnum.Igual, SCFUtils.CompletarCNPJ(request.FiltroCodigoCnpj)));
                if (request.FiltroCnpjFavorecido != null && request.FiltroCnpjFavorecido != "")
                    condicoes.Add(new CondicaoInfo("CnpjFavorecido", CondicaoTipoEnum.Igual, SCFUtils.CompletarCNPJ(request.FiltroCnpjFavorecido)));
                if (request.FiltroRazaoSocial != null && request.FiltroRazaoSocial != "")
                    condicoes.Add(new CondicaoInfo("RazaoSocial", CondicaoTipoEnum.Igual, request.FiltroRazaoSocial));
                if (request.FiltroMaxLinhas != null && request.FiltroMaxLinhas > 0)
                    condicoes.Add(new CondicaoInfo("MaxLinhas", CondicaoTipoEnum.Igual, request.FiltroMaxLinhas));
                resposta.Resultado = PersistenciaHelper.Listar<EstabelecimentoInfo>(request.CodigoSessao, condicoes); //1351 EstabelecimentoDbLib >> ConsultarObjetosResponse >> PR_ESTABELECIMENTO_L
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }
        //==================================================================================================================================
        // 963 INICIO - CARREGAR COMBO - site_configuracao_cadastro_protocolo_cheque_devolvido >> carregarEstabelecimentos >> cmbEstabelecimento
        //==================================================================================================================================

        public ReceberEstabelecimentoResponse ReceberEstabelecimento(ReceberEstabelecimentoRequest request)
        {
            // Prepara resposta
            ReceberEstabelecimentoResponse resposta = new ReceberEstabelecimentoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.EstabelecimentoInfo =
                    PersistenciaHelper.Receber<EstabelecimentoInfo>(
                        request.CodigoSessao, request.CodigoEstabelecimento);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove um Estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverEstabelecimentoResponse RemoverEstabelecimento(RemoverEstabelecimentoRequest request)
        {
            // Prepara resposta
            RemoverEstabelecimentoResponse resposta = new RemoverEstabelecimentoResponse();
            resposta.PreparaResposta(request);

            //// recebe o estabelecimento para verificar se está vinculado a um favorecido
            //EstabelecimentoInfo estabelecimentoInfo = 
            //    PersistenciaHelper.Receber<EstabelecimentoInfo>(request.CodigoSessao, request.CodigoEstabelecimento);

            //if (!String.IsNullOrEmpty(estabelecimentoInfo.CodigoFavorecido))
            //{
            //    resposta.DescricaoResposta = "O estabelecimento não pode ser removido pois está associado a um favorecido.";
            //    return resposta;
            //}

            // declaracoes
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;

            // Inicializa procedure
            PR_TRANSACAO_L_Request transacaoLrequest = new PR_TRANSACAO_L_Request()
            {
                CodigoEstabelecimento = request.CodigoEstabelecimento
            };
            cursor = Procedures.PR_TRANSACAO_L(cn, 0, transacaoLrequest);
            OracleDataReader reader = cursor.GetDataReader();

            // varre o cursor
            while (reader.Read())
            {
                //throw new Exception("O plano está associado a uma transação de venda.");
                resposta.DescricaoResposta = "O estabelecimento não pode ser removido pois tem transação vinculada.";
                return resposta;
            }

            // Bloco de controle
            try
            {
                // Remove
                PersistenciaHelper.Remover<EstabelecimentoInfo>(
                    request.CodigoSessao, request.CodigoEstabelecimento);

                // Informa na resposta
                resposta.CodigoEstabelecimentoRemovido = request.CodigoEstabelecimento;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Salva Estabelecimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarEstabelecimentoResponse SalvarEstabelecimento(SalvarEstabelecimentoRequest request)
        {
            // Prepara resposta
            SalvarEstabelecimentoResponse resposta = new SalvarEstabelecimentoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.EstabelecimentoInfo =
                    PersistenciaHelper.Salvar<EstabelecimentoInfo>(
                        request.CodigoSessao, request.EstabelecimentoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region DiaNaoUtil

        /// <summary>
        /// Lista os dias não úteis
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarDiaNaoUtilResponse ListarDiaNaoUtil(ListarDiaNaoUtilRequest request)
        {
            // Prepara resposta
            ListarDiaNaoUtilResponse resposta = new ListarDiaNaoUtilResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // Permite ao usuário buscar pelo status
                if (request.FiltroAtivo.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Ativo", CondicaoTipoEnum.Igual, request.FiltroAtivo.Value));

                // Filtro por data maior
                if (request.FiltroDataMaior.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataDiaNaoUtil", CondicaoTipoEnum.Maior, request.FiltroDataMaior.Value));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<DiaNaoUtilInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }


        /// <summary>
        /// Recebe um dia não útil
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberDiaNaoUtilResponse ReceberDiaNaoUtil(ReceberDiaNaoUtilRequest request)
        {
            // Prepara resposta
            ReceberDiaNaoUtilResponse resposta = new ReceberDiaNaoUtilResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.DiaNaoUtilInfo =
                    PersistenciaHelper.Receber<DiaNaoUtilInfo>(
                        request.CodigoSessao, request.CodigoDiaNaoUtil);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove um dia não útil
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverDiaNaoUtilResponse RemoverDiaNaoUtil(RemoverDiaNaoUtilRequest request)
        {
            RemoverDiaNaoUtilResponse res = new RemoverDiaNaoUtilResponse();
            res.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove o item solicitado
                Mensageria.Processar<RemoverObjetoResponse<DiaNaoUtilInfo>>(
                    new RemoverDiaNaoUtilDbRequest()
                    {
                        Ativar = request.Ativar,
                        CodigoObjeto = request.CodigoDiaNaoUtil,
                        CodigoSessao = request.CodigoSessao
                    });

                // Informa na resposta
                res.CodigoDiaNaoUtilRemovido = request.CodigoDiaNaoUtil;
            }
            catch (Exception ex)
            {
                // Trata o erro
                res.ProcessarExcessao(ex);
            }

            // Retorna
            return res;
        }

        /// <summary>
        /// Salva dia não útil
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarDiaNaoUtilResponse SalvarDiaNaoUtil(SalvarDiaNaoUtilRequest request)
        {
            // Prepara resposta
            SalvarDiaNaoUtilResponse resposta = new SalvarDiaNaoUtilResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.DiaNaoUtilInfo =
                    PersistenciaHelper.Salvar<DiaNaoUtilInfo>(
                        request.CodigoSessao, request.DiaNaoUtilInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Plano

        /// <summary>
        /// Lista os planos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarPlanoResponse ListarPlano(ListarPlanoRequest request)
        {
            // Prepara resposta
            ListarPlanoResponse resposta = new ListarPlanoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo status
                if (request.FiltroAtivo)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Ativo", CondicaoTipoEnum.Igual, request.FiltroAtivo));

                //Permite ao usuário buscar por produto
                if (request.FiltroCodigoProduto != null && request.FiltroCodigoProduto != "")
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoProduto", CondicaoTipoEnum.Igual, request.FiltroCodigoProduto));

                // Permite ao usuário buscar por Código TSYS
                if (request.FiltroCodigoPlanoTSYS != null && request.FiltroCodigoPlanoTSYS != "")
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoPlanoTSYS", CondicaoTipoEnum.Igual, request.FiltroCodigoPlanoTSYS));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<PlanoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Recebe um plano
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberPlanoResponse ReceberPlano(ReceberPlanoRequest request)
        {
            // Prepara resposta
            ReceberPlanoResponse resposta = new ReceberPlanoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.PlanoInfo =
                    PersistenciaHelper.Receber<PlanoInfo>(
                        request.CodigoSessao, request.CodigoPlano);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove um plano
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverPlanoResponse RemoverPlano(RemoverPlanoRequest request)
        {
            // prepara a resposta
            RemoverPlanoResponse res = new RemoverPlanoResponse();
            res.PreparaResposta(request);

            // declaracoes
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;

            // prepara as condicoes
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

            // adiciona a condicao
            condicoes.Add(new CondicaoInfo("CodigoPlano", CondicaoTipoEnum.Igual, request.CodigoPlano));

            // recupera os produtos associados ao plano
            // Retorna o item solicitado
            List<ProdutoPlanoInfo> listarProdutoPlanoResponse =
                PersistenciaHelper.Listar<ProdutoPlanoInfo>(
                    request.CodigoSessao, condicoes);

            // verifica se retornou plano. Se sim, já está associado a um produto.
            if (listarProdutoPlanoResponse.Count > 0)
            {
                //throw new Exception("O plano está associado a um produto.");
                res.DescricaoResposta = "O plano não pode ser removido pois está associado a um produto.";
                return res;
            }

            // Inicializa procedure
            PR_TRANSACAO_VENDA_L_Request transacaoVendaLrequest = new PR_TRANSACAO_VENDA_L_Request()
            {
                CodigoPlano = request.CodigoPlano
            };
            cursor = Procedures.PR_TRANSACAO_VENDA_L(cn, 0, transacaoVendaLrequest);
            OracleDataReader reader = cursor.GetDataReader();

            // varre o cursor
            while (reader.Read())
            {
                //throw new Exception("O plano está associado a uma transação de venda.");
                res.DescricaoResposta = "O plano não pode ser removido pois está associado a uma transação de venda.";
                return res;
            }

            // Bloco de controle
            try
            {
                // Remove o item solicitado
                Mensageria.Processar<RemoverObjetoResponse<PlanoInfo>>(
                    new RemoverPlanoDbRequest()
                    {
                        Ativar = request.Ativar,
                        RemoverAssociacoesPlanos = true,
                        CodigoObjeto = request.CodigoPlano,
                        CodigoSessao = request.CodigoSessao
                    });

                // Informa na resposta
                res.CodigoPlanoRemovido = request.CodigoPlano;
            }
            catch (Exception ex)
            {
                // Trata o erro
                res.ProcessarExcessao(ex);
            }

            // Retorna
            return res;
        }

        /// <summary>
        /// Salva plano
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarPlanoResponse SalvarPlano(SalvarPlanoRequest request)
        {
            // Prepara resposta
            SalvarPlanoResponse resposta = new SalvarPlanoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.PlanoInfo =
                    PersistenciaHelper.Salvar<PlanoInfo>(
                        request.CodigoSessao, request.PlanoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }


        #endregion

        /// <summary>
        /// Salva plano
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarLogResponse SalvarLog(SalvarLogRequest request)
        {
            // Prepara resposta
            SalvarLogResponse resposta = new SalvarLogResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.retorno =
                    PersistenciaHelper.Salvar<LogSCFRelatorioInfo>(
                        request.CodigoSessao, new LogSCFRelatorioInfo()
                        {
                            CodigoLog = request.CodigoLog,
                            CodigoUsuario = request.CodigoUsuario,
                            Descricao = request.Descricao,
                            TipoOrigem = request.TipoOrigem,
                            CodigoOrigem = request.CodigoOrigem
                        });
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #region ConfiguracaoGeral

        /// <summary>
        /// Recebe configuração atual do sistemas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberConfiguracaoGeralResponse ReceberConfiguracaoGeral(ReceberConfiguracaoGeralRequest request)
        {
            // Prepara resposta
            ReceberConfiguracaoGeralResponse resposta = new ReceberConfiguracaoGeralResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.ConfiguracaoGeralInfo =
                    PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                        request.CodigoSessao, null);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Salva nova configuração
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarConfiguracaoGeralResponse SalvarConfiguracaoGeral(SalvarConfiguracaoGeralRequest request)
        {
            // Prepara resposta
            SalvarConfiguracaoGeralResponse resposta = new SalvarConfiguracaoGeralResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.ConfiguracaoGeralInfo =
                    PersistenciaHelper.Salvar<ConfiguracaoGeralInfo>(
                        request.CodigoSessao, request.ConfiguracaoGeralInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Testes

        public TesteResponse Teste(TesteRequest request)
        {
            return
                new TesteResponse()
                {
                    ValorResponse = request.ValorRequest
                };
        }

        /// <summary>
        /// Função para remover acentos de strings
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public string retiraAcentos(string strcomAcentos) //SCF845
        {
            string strsemAcentos = strcomAcentos;
            strsemAcentos = Regex.Replace(strsemAcentos, "[áàâãª]", "a");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÁÀÂÃ]", "A");
            strsemAcentos = Regex.Replace(strsemAcentos, "[éèê]", "e");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÉÈÊ]", "e");
            strsemAcentos = Regex.Replace(strsemAcentos, "[íìî]", "i");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÍÌÎ]", "I");
            strsemAcentos = Regex.Replace(strsemAcentos, "[óòôõº]", "o");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÓÒÔÕ]", "O");
            strsemAcentos = Regex.Replace(strsemAcentos, "[úùû]", "u");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ÚÙÛ]", "U");
            strsemAcentos = Regex.Replace(strsemAcentos, "[ç]", "c");
            strsemAcentos = Regex.Replace(strsemAcentos, "[Ç]", "C");
            strsemAcentos = Regex.Replace(strsemAcentos, "[Ñ]", "N"); //SCF948
            strsemAcentos = Regex.Replace(strsemAcentos, "[ñ]", "n"); //SCF948
            strsemAcentos = Regex.Replace(strsemAcentos, "[£]", ""); //SCF1200
            strsemAcentos = Regex.Replace(strsemAcentos, "[§]", ""); //SCF1200


            return strsemAcentos;
        }
        #endregion

        #region Pagamento

        /// <summary>
        /// Agendamento de Pagamentos seguindo regras do atacadão
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AgendarPagamentoAtacadaoResponse AgendarPagamentoAtacadao(AgendarPagamentoAtacadaoRequest request)
        {
            // Prepara resposta
            AgendarPagamentoAtacadaoResponse resposta = new AgendarPagamentoAtacadaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Pega informações do processo
                ProcessoInfo processo =
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        request.CodigoSessao, request.CodigoProcesso);

                // Calcula a data de pagamento
                DateTime dataPagamento = processo.DataInclusao.Value;

                //// Executa o agendamento
                //Procedures.PR_TRANSACAO_VENDA_AGENDAR(int.Parse(request.CodigoProcesso), dataPagamento, "S");

                //// Calcula os somatorios dos pagamentos
                //Procedures.PR_PAGAMENTO_A(null, int.Parse(request.CodigoProcesso));


                // Faz a preparacao para agendamento
                //1328 Procedures.PR_EXTRATO_PREPARAR_AGENDA(request.CodigoProcesso, true);

                // Executa o agendamento
                //1328 Procedures.PR_EXTRATO_AGENDAR(request.CodigoProcesso, true);
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Agendamento de Pagamentos seguindo regras do extrato
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AgendarPagamentoExtratoResponse AgendarPagamentoExtrato(AgendarPagamentoExtratoRequest request)
        {
            // Prepara resposta
            AgendarPagamentoExtratoResponse resposta = new AgendarPagamentoExtratoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Pega informações do processo
                ProcessoInfo processo =
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        request.CodigoSessao, request.CodigoProcesso);

                //bool isHomologacaoTSYS =
                //    PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                //        null, null).HomologacaoTSYS;

                // Selecionar as transacoes do processo
                // Pegar apenas as que estiverem configuradas para gerar agenda
                // Pegar a configuraca dos registros que permitem enviar pagto negativo
                // calcular a data da agenda
                // Se data de repasse original < hoje (vencida)
                // data base = hoje + parametro de dias vencidos
                // Se nao estiver vencida
                // data base = data do repasse
                // Verificar se a data base é dia útil (calendario)

                /// Criar uma procedure que irá selecionar todas as transacoes
                /// Apenas os tipo de registro que estiverem configurados para gerar agenda
                /// Abrir um cursor para acertar a data de repasse calculada

                // Chamar a proc que vai calcular as novas datas

                // Faz a preparacao para agendamento
                //1328 if (!isHomologacaoTSYS)
                //1328 Procedures.PR_EXTRATO_PREPARAR_AGENDA(request.CodigoProcesso, false);

                // Executa o agendamento
                //1328 Procedures.PR_EXTRATO_AGENDAR(request.CodigoProcesso, false);

            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Agendamento das transacoes do extrato (versão 2)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AgendarPagamentoExtrato2Response AgendarPagamentoExtrato2(AgendarPagamentoExtrato2Request request)
        {
            // Prepara resposta
            AgendarPagamentoExtrato2Response resposta = new AgendarPagamentoExtrato2Response();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Faz o cálculo das datas efetivas de repasse
                // Faz o distinct das datas das transacoes do processo informado
                // Verifica tabela de feriados
                // Altera a data das transacoes

                // Marca eventuais pagamentos utilizados por essas transacoes para reprocessamento
                // Isto manterá integro os valores totais dos pagamentos em questão caso tenha remanejamento de pagamentos

                // Pede o distinct dos valores de pagamento (data, favorecido, conta, etc...)
                // Verificando se o pagamento está criado, senão cria
                // Altera as transações referentes a este pagamento
                // Adiciona o pagamento na lista de reprocessamento

                // Pede o reprocessamento dos pagamentos pendentes de reprocessamento
            }
            catch (Exception ex)
            {
                // Trata excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Processo

        /// <summary>
        /// Salva processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarProcessoResponse SalvarProcesso(SalvarProcessoRequest request)
        {
            // Prepara resposta
            SalvarProcessoResponse resposta = new SalvarProcessoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.ProcessoInfo =
                    Mensageria.Processar<SalvarObjetoResponse<ProcessoInfo>>(
                        new SalvarObjetoRequest<ProcessoInfo>()
                        {
                            CodigoSessao = request.CodigoSessao,
                            Objeto = request.ProcessoInfo
                        }).Objeto;

            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Faz a execução do processo atacadão usando informações de configurações e 
        /// criando diretório com resultado do processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExecutarProcessoAtacadaoResponse ExecutarProcessoAtacadao(ExecutarProcessoAtacadaoRequest request)
        {
            // Prepara resposta
            ExecutarProcessoAtacadaoResponse resposta = new ExecutarProcessoAtacadaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {

                // Lê configurações
                ProcessoAtacadaoAutomaticoConfig config =
                    request.Config != null ? request.Config : GerenciadorConfig.ReceberConfig<ProcessoAtacadaoAutomaticoConfig>();

                // Cria o diretório de resultado do processo

                // Se deve mover, move os arquivos para dentro do diretório do processo

                // Cria o processo do atacadao
                ProcessoAtacadaoInfo processoAtacadaoInfo =
                    new ProcessoAtacadaoInfo()
                    {
                        CaminhoArquivoCSU = config.CaminhoArquivoCSU,
                        CaminhoArquivoSitef = config.CaminhoArquivoSitef,
                        CaminhoArquivoTSYS = config.CaminhoArquivoTSYS,
                        CaminhoArquivoMateraFinanceiro = config.CaminhoArquivoMateraFinanceiro,
                        CaminhoArquivoResultadoConciliacao = config.CaminhoArquivoResultadoConciliacao,
                        CaminhoArquivoCSUSemAtacadao = config.CaminhoArquivoCSUSemAtacadao,
                        CaminhoArquivoMateraContabil = config.CaminhoArquivoMateraContabil,
                        MoverArquivoProcessado = config.MoverArquivosOrigem
                    };

                // Executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoAtacadaoInfo,
                            ExecutarAssincrono = request.ExecutarAssincrono
                        });

                // Informa código do processo na resposta
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
                resposta.ProcessoFinalizado = respostaExecutar.ProcessoFinalizado;
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Faz a execução do processo cessao usando informações de configurações e 
        /// criando diretório com resultado do processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExecutarProcessoCessaoResponse ExecutarProcessoCessao(ExecutarProcessoCessaoRequest request)
        {
            // Prepara resposta
            ExecutarProcessoCessaoResponse resposta = new ExecutarProcessoCessaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Lê configurações
                ProcessoCessaoAutomaticoConfig config =
                    request.Config != null ? request.Config : GerenciadorConfig.ReceberConfig<ProcessoCessaoAutomaticoConfig>();

                // Cria o diretório de resultado do processo

                // Se deve mover, move os arquivos para dentro do diretório do processo

                // Cria o processo do cessao
                ProcessoCessaoInfo processoCessaoInfo =
                    new ProcessoCessaoInfo()
                    {
                        CaminhoArquivoRetornoCessao = config.CaminhoArquivoRetornoCessao,
                        MoverArquivoProcessado = request.MoverArquivoOrigem
                    };

                // Executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoCessaoInfo,
                            ExecutarAssincrono = request.ExecutarAssincrono
                        });

                // Informa código do processo na resposta
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
                resposta.ProcessoFinalizado = respostaExecutar.ProcessoFinalizado;
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Faz a execução do processo expurgo usando informações de configurações 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //public ExecutarProcessoExpurgoResponse ExecutarProcessoExpurgo(ExecutarProcessoExpurgoRequest request)
        //{
        //    // Prepara resposta
        //    ExecutarProcessoExpurgoResponse resposta = new ExecutarProcessoExpurgoResponse();
        //    resposta.PreparaResposta(request);

        //    // Bloco de controle
        //    try
        //    {

        //        // Cria o processo do extrato
        //        ProcessoExpurgoArquivoItemInfo processoExpurgoInfo =
        //            new ProcessoExpurgoArquivoItemInfo()
        //            {

        //            };


        //        // Executa o processo
        //        ExecutarProcessoResponse respostaExecutar =
        //            Mensageria.Processar<ExecutarProcessoResponse>(
        //                new ExecutarProcessoRequest()
        //                {
        //                    CodigoSessao = request.CodigoSessao,
        //                    ProcessoInfo = processoExpurgoInfo,
        //                    ExecutarAssincrono = request.ExecutarAssincrono
        //                });

        //        // Informa código do processo na resposta
        //        resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
        //        resposta.ProcessoFinalizado = respostaExecutar.ProcessoFinalizado;
        //    }
        //    catch (Exception ex)
        //    {
        //        // Processa excessao
        //        resposta.ProcessarExcessao(ex);
        //    }

        //    // Retorna
        //    return resposta;
        //}


        /// <summary>
        /// Faz a execução do processo extrato usando informações de configurações 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExecutarProcessoExtratoResponse ExecutarProcessoExtrato(ExecutarProcessoExtratoRequest request)
        {
            // Prepara resposta
            ExecutarProcessoExtratoResponse resposta = new ExecutarProcessoExtratoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Lê configurações
                ProcessoExtratoAutomaticoConfig config =
                    request.Config != null ? request.Config : GerenciadorConfig.ReceberConfig<ProcessoExtratoAutomaticoConfig>();

                // Cria o processo do extrato
                ProcessoExtratoInfo processoExtratoInfo =
                    new ProcessoExtratoInfo()
                    {
                        CaminhoArquivoTSYS = config.CaminhoArquivoTSYS,
                        CaminhoArquivoTSYSAtacadao = config.CaminhoArquivoTSYSAtacadao,
                        CaminhoArquivoTSYSGaleria = config.CaminhoArquivoTSYSGaleria,
                        CaminhoArquivoCCI = config.CaminhoArquivoCCI,
                        CaminhoArquivoATA = config.CaminhoArquivoATA,
                        CaminhoArquivoGAL = config.CaminhoArquivoGAL,
                        MoverArquivoProcessado = request.MoverArquivoOrigem
                    };

                // Lê configurações de sistema
                // Buscar configuracao
                //ConfiguracaoGeralInfo configGeral = new ConfiguracaoGeralInfo();

                // Verificar se já o arquivo já foi importado alguma vez

                // Executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoExtratoInfo,
                            ExecutarAssincrono = request.ExecutarAssincrono
                        });

                // Informa código do processo na resposta
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
                resposta.ProcessoFinalizado = respostaExecutar.ProcessoFinalizado;
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Faz a execução do processo CCI Manual
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExecutarProcessoCCIManualResponse ExecutarProcessoCCIManual(ExecutarProcessoCCIManualRequest request)
        {
            // Prepara resposta
            ExecutarProcessoCCIManualResponse resposta = new ExecutarProcessoCCIManualResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {

                // Cria o processo do extrato
                ProcessoCCIManualInfo processoCCIManualInfo =
                    new ProcessoCCIManualInfo()
                    {
                        CodigoArquivoTSYS = request.CodigoArquivoTSYS,
                        CaminhoArquivoCCI = request.CaminhoGeracaoArquivo,
                        MoverArquivoProcessado = request.MoverArquivoOrigem
                    };

                // Executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoCCIManualInfo,
                            ExecutarAssincrono = request.ExecutarAssincrono
                        });

                // Informa código do processo na resposta
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
                resposta.ProcessoFinalizado = respostaExecutar.ProcessoFinalizado;
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        // MELHORIAS-2017 - ESSE PROCESSO FOI SUBSTITUIDO POR OUTRO
        ///// <summary>
        ///// Faz a execução do processo extrato usando informações de configurações 
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //public ExecutarProcessoPagamentoResponse ExecutarProcessoPagamento(ExecutarProcessoPagamentoRequest request)
        //{
        //    // Prepara resposta
        //    ExecutarProcessoPagamentoResponse resposta = new ExecutarProcessoPagamentoResponse();
        //    resposta.PreparaResposta(request);

        //    // Bloco de controle
        //    try
        //    {
        //        // Lê configurações
        //        ProcessoPagamentoAutomaticoConfig config =
        //            request.Config != null ? request.Config : GerenciadorConfig.ReceberConfig<ProcessoPagamentoAutomaticoConfig>();

        //        // Cria o processo do pagamento
        //        ProcessoPagamentoInfo processoPagamentoInfo =
        //            new ProcessoPagamentoInfo()
        //            {
        //                CaminhoArquivoMateraFinanceiro = config.CaminhoArquivoMateraFinanceiro,
        //                ProcessarAtacadao = config.ProcessarAtacadao,
        //                ProcessarExtrato = config.ProcessarExtrato
        //            };

        //        // Executa o processo
        //        ExecutarProcessoResponse respostaExecutar =
        //            Mensageria.Processar<ExecutarProcessoResponse>(
        //                new ExecutarProcessoRequest()
        //                {
        //                    CodigoSessao = request.CodigoSessao,
        //                    ProcessoInfo = processoPagamentoInfo,
        //                    ExecutarAssincrono = request.ExecutarAssincrono
        //                });

        //        // Informa código do processo na resposta
        //        resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
        //        resposta.ProcessoFinalizado = respostaExecutar.ProcessoFinalizado;
        //    }
        //    catch (Exception ex)
        //    {
        //        // Processa excessao
        //        resposta.ProcessarExcessao(ex);
        //    }

        //    // Retorna
        //    return resposta;
        //}

        /// <summary>
        /// Executa um processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExecutarProcessoResponse ExecutarProcesso(ExecutarProcessoRequest request)
        {
            ExecutarProcessoResponse resposta = new ExecutarProcessoResponse();
            resposta.PreparaResposta(request);

            bool isNew = false;

            // Bloco de controle
            try
            {
                // Inicializa
                ProcessoInfo processoInfo = null;

                // Novo ou existente?
                if (request.CodigoProcesso != null)
                {
                    // Carrega processo informado
                    processoInfo =
                        PersistenciaHelper.Receber<ProcessoInfo>(
                            request.CodigoProcesso, request.CodigoProcesso);

                }
                else
                {
                    // Pega info informado
                    processoInfo = request.ProcessoInfo;

                    // Inicia o processo
                    GerenciadorProcesso.IniciarProcesso(
                        request.CodigoSessao, processoInfo);

                    isNew = true;
                }

                // Indica código do processo gerado na resposta
                resposta.CodigoProcesso = processoInfo.CodigoProcesso;
                resposta.StatusProcesso = processoInfo.StatusProcesso;
                resposta.StatusEstagio = processoInfo.StatusEstagio;

                if (!request.ValidaProcessoEmAndamento
                    || isNew
                    || ((resposta.StatusEstagio == EstagioStatusEnum.AguardandoValidacao || resposta.StatusEstagio == EstagioStatusEnum.Erro) && resposta.StatusProcesso != ProcessoStatusEnum.Finalizado))
                {
                    if (!request.ValidaProcessoEmAndamento
                        || (resposta.StatusEstagio != EstagioStatusEnum.EmExecucao && resposta.StatusEstagio != EstagioStatusEnum.EmValidacao))
                    {
                        // Síncrono ou assíncrono?
                        if (request.ExecutarAssincrono)
                        {
                            // Executa o processo assíncrono
                            new Thread(     //captura automatica de processo manual
                                new ParameterizedThreadStart(
                                    delegate(object param)
                                    {
                                        try
                                        {
                                            //Jira 968 - Marcos Matsuoka
                                            LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                                                 request.Descricao, request.CodigoSessao, request.CodigoProcesso);

                                            // Cria o timer para atualizar a data de atualizacao do processo
                                            // todo: Amanda
                                            //Timer stateTimer =
                                            //    new Timer(new TimerCallback(
                                            //        delegate(object timerParam)
                                            //        {
                                            //            // pega os dados do processo
                                            //            ProcessoInfo processoInfoAtualizacao =
                                            //                PersistenciaHelper.Receber<ProcessoInfo>(
                                            //                    null, processoInfo.CodigoProcesso);

                                            //            // atualiza a data de atualizacao
                                            //            processoInfoAtualizacao.DataAtualizacao = DateTime.Now;

                                            //            // salva os dados do processo
                                            //            processoInfo.Atualizar(
                                            //                PersistenciaHelper.Salvar<ProcessoInfo>(
                                            //                null, processoInfoAtualizacao));

                                            //        }),
                                            //        null, 1000, 1000);

                                            // Pede a execução do processo
                                            GerenciadorProcesso.Processar(
                                                (string)((object[])param)[0],
                                                (ProcessoInfo)((object[])param)[1]);

                                            // Remove o timer
                                            // todo: Amanda
                                            // stateTimer.Dispose();
                                        }
                                        catch (Exception ex)
                                        {
                                            //==========================================================================================
                                            // Indica que pulou este estágio
                                            //==========================================================================================
                                            var lLog = new LogSCFProcessoEventoInfo();
                                            lLog.setCodigoProcesso(((ProcessoInfo)((object[])param)[1]).CodigoProcesso);
                                            lLog.setDescricao(ex.ToString()); //1333
                                            LogHelper.Salvar(null, lLog);

                                        }
                                        finally
                                        {

                                        }
                                    })).Start(new object[] { request.CodigoSessao, processoInfo });

                        }
                        else
                        {
                            // Executa o processo síncrono
                            resposta.ProcessoFinalizado =
                                GerenciadorProcesso.Processar(
                                    request.CodigoSessao, processoInfo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Trata excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Solicita detalhe de processo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberProcessoResponse ReceberProcesso(ReceberProcessoRequest request)
        {
            // Prepara resposta
            ReceberProcessoResponse resposta = new ReceberProcessoResponse();

            // Bloco de controle
            try
            {
                // Pede a lista para a persistencia
                resposta.ProcessoInfo =
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        request.CodigoSessao,
                        request.CodigoProcesso);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Lista de processos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarProcessoResponse ListarProcesso(ListarProcessoRequest request)
        {
            // Prepara a resposta
            ListarProcessoResponse resposta = new ListarProcessoResponse();
            resposta.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 29);

            // Bloco de controle
            try
            {
                // Monta lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // DataInclusao inicial
                if (request.FiltroDataInclusaoInicial.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataInclusao",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataInclusaoInicial.Value));

                // DataInclusao final
                if (request.FiltroDataInclusaoFinal.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataInclusao",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataInclusaoFinal.Value + ts));

                // DataInclusao inicial
                if (request.FiltroDataInclusao.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataInclusao",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataInclusaoInicial.Value));

                // StatusProcesso igual
                if (request.FiltroStatusProcesso.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "StatusProcesso",
                            CondicaoTipoEnum.Igual,
                            request.FiltroStatusProcesso.Value));

                // StatusProcesso diferente
                if (request.FiltroStatusProcessoDiferente.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "StatusProcessoDiferente",
                            CondicaoTipoEnum.Diferente,
                            request.FiltroStatusProcessoDiferente.Value));

                // StatusBloqueio
                if (request.FiltroStatusBloqueio != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "StatusBloqueio",
                            CondicaoTipoEnum.Igual,
                            request.FiltroStatusBloqueio));

                if (request.FiltroTipoProcesso != null && request.FiltroTipoProcesso != string.Empty)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "TipoProcesso",
                            CondicaoTipoEnum.Contem, //1463 .Igual,
                            request.FiltroTipoProcesso));
                }

                // Pede a lista de processos
                resposta.Resultado =
                    PersistenciaHelper.Listar<ProcessoResumoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        //==================================================================================================================================
        // 963 INICIO - CARREGAR GRID - site_configuracao_cadastro_protocolo_cheque_devolvido >> tbProtocolo
        //==================================================================================================================================
        public ListarProtocoloChequeResponse ListarProtocoloCheque(ListarProtocoloChequeRequest request)
        {
            ListarProtocoloChequeResponse resposta = new ListarProtocoloChequeResponse();
            resposta.PreparaResposta(request);
            TimeSpan ts = new TimeSpan(23, 59, 59);
            try
            {
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                if (request.FiltroDataEstornoDe.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataEstorno", CondicaoTipoEnum.MaiorIgual, request.FiltroDataEstornoDe.Value));
                if (request.FiltroDataEstornoAte.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataEstorno", CondicaoTipoEnum.MenorIgual, request.FiltroDataEstornoAte.Value + ts));
                if (request.FiltroDataEstorno.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataEstorno", CondicaoTipoEnum.Igual, request.FiltroDataEstorno.Value));
                if (request.FiltroEnvioCci != null)
                    condicoes.Add(new CondicaoInfo("FiltroEnvioCci", CondicaoTipoEnum.Igual, request.FiltroEnvioCci));
                if (request.FiltroNumeroProtocolo != null && request.FiltroNumeroProtocolo != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroNumeroProtocolo", CondicaoTipoEnum.Igual, request.FiltroNumeroProtocolo));
                if (request.FiltroEstabelecimento != null && request.FiltroEstabelecimento != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroEstabelecimento", CondicaoTipoEnum.Igual, request.FiltroEstabelecimento));
                if (request.FiltroDataRegProtocoloDe.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataRegProtocolo", CondicaoTipoEnum.MaiorIgual, request.FiltroDataRegProtocoloDe.Value));
                if (request.FiltroDataRegProtocoloAte.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataRegProtocolo", CondicaoTipoEnum.MenorIgual, request.FiltroDataRegProtocoloAte.Value + ts));
                if (request.FiltroDataRegProtocolo.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataRegProtocolo", CondicaoTipoEnum.Igual, request.FiltroDataRegProtocolo.Value));
                if (request.FiltroStatusProtocolo != null && request.FiltroStatusProtocolo != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroStatusProtocolo", CondicaoTipoEnum.Igual, request.FiltroStatusProtocolo));
                if (request.FiltroContaCliente != null && request.FiltroContaCliente != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroContaCliente", CondicaoTipoEnum.Igual, request.FiltroContaCliente));
                if (request.FiltroTipoEstorno != null && request.FiltroTipoEstorno != string.Empty)
                    condicoes.Add(new CondicaoInfo("FiltroTipoEstorno", CondicaoTipoEnum.Igual, request.FiltroTipoEstorno));
                if (request.FiltroDataProcessamentoDe.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataProcessamento", CondicaoTipoEnum.MaiorIgual, request.FiltroDataProcessamentoDe.Value));
                if (request.FiltroDataProcessamentoAte.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataProcessamento", CondicaoTipoEnum.MenorIgual, request.FiltroDataProcessamentoAte.Value + ts));
                if (request.FiltroDataProcessamento.HasValue)
                    condicoes.Add(new CondicaoInfo("FiltroDataProcessamento", CondicaoTipoEnum.Igual, request.FiltroDataProcessamento.Value));

                resposta.Resultado = PersistenciaHelper.Listar<ProcessoProtocoloChequeInfo>(request.CodigoSessao, condicoes); // ProtocoloChequeDbLib >> ConsultarObjetos >> PR_CHEQUE_DEVOLVIDO_L
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            return resposta;
        }
        //==================================================================================================================================
        // 963 FIM - CARREGAR GRID - site_configuracao_cadastro_protocolo_cheque_devolvido >> tbProtocolo
        //==================================================================================================================================

        /// <summary>
        /// Retorna configuracao do processo atacadao
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberProcessoAtacadaoAutomaticoConfigResponse ReceberProcessoAtacadaoAutomaticoConfig(ReceberProcessoAtacadaoAutomaticoConfigRequest request)
        {
            // Prepara resposta
            ReceberProcessoAtacadaoAutomaticoConfigResponse resposta = new ReceberProcessoAtacadaoAutomaticoConfigResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o config
                resposta.Config =
                    GerenciadorConfig.ReceberConfig<ProcessoAtacadaoAutomaticoConfig>();
                if (resposta.Config == null)
                    resposta.Config = new ProcessoAtacadaoAutomaticoConfig();
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Retorna configuracao do processo extrato
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberProcessoExtratoAutomaticoConfigResponse ReceberProcessoExtratoAutomaticoConfig(ReceberProcessoExtratoAutomaticoConfigRequest request)
        {
            // Prepara resposta
            ReceberProcessoExtratoAutomaticoConfigResponse resposta = new ReceberProcessoExtratoAutomaticoConfigResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o config
                resposta.Config =
                    GerenciadorConfig.ReceberConfig<ProcessoExtratoAutomaticoConfig>();
                if (resposta.Config == null)
                    resposta.Config = new ProcessoExtratoAutomaticoConfig();
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Retorna configuracao do processo retorno cessao
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberProcessoCessaoAutomaticoConfigResponse ReceberProcessoCessaoAutomaticoConfig(ReceberProcessoCessaoAutomaticoConfigRequest request)
        {
            // Prepara resposta
            ReceberProcessoCessaoAutomaticoConfigResponse resposta = new ReceberProcessoCessaoAutomaticoConfigResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o config
                resposta.Config =
                    GerenciadorConfig.ReceberConfig<ProcessoCessaoAutomaticoConfig>();
                if (resposta.Config == null)
                    resposta.Config = new ProcessoCessaoAutomaticoConfig();
            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Cancela o processo solicitado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public CancelarProcessoResponse CancelarProcesso(CancelarProcessoRequest request)
        {
            // Prepara resposta
            CancelarProcessoResponse resposta = new CancelarProcessoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Inicializa
                ProcessoInfo processoInfo = null;

                // Carrega processo informado
                processoInfo =
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        request.CodigoProcesso, request.CodigoProcesso);

                // Síncrono ou assíncrono?
                if (request.ExecutarAssincrono)
                {
                    // Executa o processo assíncrono
                    new Thread( //captura automatica de processo manual
                        new ParameterizedThreadStart(
                            delegate(object param)
                            {
                                // Bloco de controle
                                try
                                {
                                    // Pede a execução
                                    GerenciadorProcesso.Cancelar(
                                        (string)((object[])param)[0],
                                        (ProcessoInfo)((object[])param)[1]);
                                }
                                catch (Exception ex)
                                {
                                    // Faz o log do erro
                                    LogHelper.Salvar(
                                        (string)((object[])param)[0],
                                        new LogSCFProcessoEventoInfo() { Descricao = ex.ToString() });
                                }
                            })).Start(new object[] { request.CodigoSessao, processoInfo });
                }
                else
                {
                    // Executa o processo síncrono
                    GerenciadorProcesso.Cancelar(
                        request.CodigoSessao, processoInfo);
                }

                // Indica o código do processo cancelado
                resposta.CodigoProcesso = request.CodigoProcesso;
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Arquivo

        /// <summary>
        /// Lista de arquivos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarCancelamentoPagamentoOnlineResponse ListarPagamentoCancelamentoOnline(ListarCancelamentoPagamentoOnlineRequest request)
        {
            // Prepara a resposta
            ListarCancelamentoPagamentoOnlineResponse resposta = new ListarCancelamentoPagamentoOnlineResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // Filtra por codigo do processo
                if (request.FiltroStatusCancelamentoOnline != null)
                    condicoes.Add(new CondicaoInfo(
                        "CodigoProcesso", CondicaoTipoEnum.Igual, request.FiltroStatusCancelamentoOnline));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<PagamentoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }



        /// <summary>
        /// Retorna dados do arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberArquivoResponse ReceberArquivo(ReceberArquivoRequest request)
        {
            // Prepara resposta
            ReceberArquivoResponse resposta = new ReceberArquivoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o arquivo solicitado
                resposta.ArquivoInfo =
                    Mensageria.Processar<ReceberObjetoResponse<ArquivoInfo>>(
                        new ReceberArquivoDbRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            CodigoObjeto = request.CodigoArquivo,
                            RetornarDetalhe = request.RetornarDetalhe
                        }).Objeto;

            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Exporta arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExportarArquivoResponse ExportarArquivo(ExportarArquivoRequest request)
        {
            // Prepara resposta
            ExportarArquivoResponse resposta = new ExportarArquivoResponse();
            resposta.PreparaResposta(request);

            // Declara o cursor para que ele possa ser corretamente finalizado
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor cursor = null;

            // Bloco de controle
            try
            {
                // Cria fileStream no caminho
                FileStream fileStream = File.Create(request.CaminhoArquivo);
                StreamWriter writer = new StreamWriter(fileStream);

                // Inicializa procedure
                cursor = Procedures.PR_ARQUIVO_ITEM_L(cn, request.CodigoArquivo, null, 0, false);
                OracleDataReader reader = cursor.GetDataReader();

                // Varre as linhas
                while (reader.Read())
                {
                    // Pega informacoes da linha
                    string linha =
                        reader.GetString(reader.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));

                    // Salva o conteudo da linha
                    writer.WriteLine(linha);
                }

                // Fecha stream
                writer.Close();
            }

            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            finally
            {
                // Finaliza o cursor
                if (cursor != null)
                {
                    cursor.Connection.Dispose();
                    cursor.Dispose();
                }
            }

            // Resposta
            return resposta;
        }

        /// <summary>
        /// Lista de itens de arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarArquivoItemResponse ListarArquivoItem(ListarArquivoItemRequest request)
        {
            // Prepara a resposta
            ListarArquivoItemResponse resposta = new ListarArquivoItemResponse();
            resposta.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 29); //1308

            // Bloco de controle
            try
            {
                // Monta lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // 1308 inicio
                // DataInclusao inicial
                if (request.FiltroDataInclusaoInicial.HasValue)
                    condicoes.Add(new CondicaoInfo("DataInclusaoInicial", CondicaoTipoEnum.MaiorIgual, request.FiltroDataInclusaoInicial.Value));
                // DataInclusao final
                if (request.FiltroDataInclusaoFinal.HasValue)
                    condicoes.Add(new CondicaoInfo("DataInclusaoFinal", CondicaoTipoEnum.MenorIgual, request.FiltroDataInclusaoFinal.Value + ts));
                // 1308 fim

                // Verifica se o status foi informado
                if (request.FiltroStatusArquivoItem != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "StatusArquivoItem", CondicaoTipoEnum.Igual, request.FiltroStatusArquivoItem));

                // Verifica se o codigo de processo foi informado
                if (request.FiltroCodigoProcesso != null && request.FiltroCodigoProcesso != "")
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoProcesso", CondicaoTipoEnum.Igual, request.FiltroCodigoProcesso));

                //Recebe o maxlinhas
                if (request.FiltroMaxLinhas != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "MaxLinhas", CondicaoTipoEnum.Igual, request.FiltroMaxLinhas));


                //// Verifica se deve ignorar o conteudo dos arquivos
                //if (request.FiltroIgnorarConteudo)
                //    condicoes.Add(
                //        new CondicaoInfo(
                //            "IgnorarConteudo", CondicaoTipoEnum.Igual, true));

                //// Verifica se deve trazer somente bloqueados e pendendentes de bloqueio
                //if (request.FiltroSomenteBloqueados && request.FiltroStatusArquivoItem == null)
                //    condicoes.Add(
                //        new CondicaoInfo(
                //            "SomenteBloqueados", CondicaoTipoEnum.Igual, true));

                //// Pede a lista de arquivos
                //resposta.Resultado = 
                //    PersistenciaHelper.Listar<ArquivoItemResumoInfo>(
                //        request.CodigoSessao, condicoes);

                resposta.Resultado =
                    Mensageria.Processar<ConsultarObjetosResponse<ArquivoItemResumoInfo>>(
                        new ListarArquivoItemDbRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            Condicoes = condicoes,
                            IncluirConteudo = true,
                            FiltrarApenasBloqueados = true
                        }).Resultado;


            }
            catch (Exception ex)
            {
                // Trata a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }



        /// <summary>
        /// Salva itens de arquivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarArquivoItemResponse SalvarArquivoItem(SalvarArquivoItemRequest request)
        {
            // Prepara a resposta
            SalvarArquivoItemResponse resposta = new SalvarArquivoItemResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta mensagem de request
                SalvarBloqueioArquivoItemDbRequest dbRequest = new SalvarBloqueioArquivoItemDbRequest();
                dbRequest.CodigosArquivoItemBloqueados = request.CodigosArquivoItemBloqueados;
                dbRequest.CodigosArquivoItemDesbloqueados = request.CodigosArquivoItemDesbloqueados;

                // Salva
                Mensageria.Processar<SalvarObjetoResponse<ArquivoItemResumoInfo>>(dbRequest);
            }
            catch (Exception ex)
            {
                // Trata a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Log

        /// <summary>
        /// Lista de logs
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarLogResponse ListarLog(ListarLogRequest request)
        {
            // Prepara resposta
            ListarLogResponse resposta = new ListarLogResponse();

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 29);

            // Bloco de controle
            try
            {
                // Monta as condicoes de filtro
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
                if (request.FiltroCodigoOrigem != null && request.FiltroCodigoOrigem != "")
                    condicoes.Add(new CondicaoInfo("CodigoOrigem", CondicaoTipoEnum.Igual, request.FiltroCodigoOrigem));
                if (request.FiltroCodigoUsuario != null)
                    condicoes.Add(new CondicaoInfo("CodigoUsuario", CondicaoTipoEnum.Igual, request.FiltroCodigoUsuario));

                // DataInclusao fim
                if (request.FiltroDataFim.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMaior",
                            CondicaoTipoEnum.MenorIgual,
                            new DateTime(request.FiltroDataFim.Value.Year, request.FiltroDataFim.Value.Month, request.FiltroDataFim.Value.Day, 23, 59, 59)));

                // DataInclusao ini
                if (request.FiltroDataInicio.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMenor",
                            CondicaoTipoEnum.MaiorIgual,
                            new DateTime(request.FiltroDataInicio.Value.Year, request.FiltroDataInicio.Value.Month, request.FiltroDataInicio.Value.Day, 00, 00, 00)));

                // DataInclusao inicial
                if (request.FiltroData.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroData",
                            CondicaoTipoEnum.Igual,
                            request.FiltroData.Value));

                //1469 - INICIO
                //if (request.FiltroTipoLog != null)
                //    condicoes.Add(new CondicaoInfo("TipoLog", CondicaoTipoEnum.Igual, request.FiltroTipoLog));
                //if (request.FiltroTipoOrigem != null)
                //    condicoes.Add(new CondicaoInfo("TipoOrigem", CondicaoTipoEnum.Igual, request.FiltroTipoOrigem));
                if (request.FiltroTipoLog != null && request.FiltroTipoLog != "")
                    condicoes.Add(new CondicaoInfo("TipoLog", CondicaoTipoEnum.Contem, request.FiltroTipoLog));
                if (request.FiltroTipoOrigem != null && request.FiltroTipoOrigem != "")
                    condicoes.Add(new CondicaoInfo("TipoOrigem", CondicaoTipoEnum.Contem, request.FiltroTipoOrigem));
                //1469 - FIM

                // Pede a lista para a persistencia
                resposta.Resultado =
                    PersistenciaHelper.Listar<LogResumoInfo>(
                        request.CodigoSessao, condicoes, request.QuantidadeRegistros);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Solicita detalhe de log
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberLogResponse ReceberLog(ReceberLogRequest request)
        {
            // Prepara resposta
            ReceberLogResponse resposta = new ReceberLogResponse();

            // Bloco de controle
            try
            {
                // Pede a lista para a persistencia
                resposta.LogInfo =
                    PersistenciaHelper.Receber<LogInfo>(
                        request.CodigoSessao,
                        request.CodigoLog);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Lista as descricoes de log
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarDescricaoLogResponse ListarDescricaoLog(ListarDescricaoLogRequest request)
        {
            // Prepara resposta
            ListarDescricaoLogResponse resposta = new ListarDescricaoLogResponse();

            // Bloco de controle
            try
            {
                // Pede a lista
                resposta.Resultado =
                    Mensageria.Processar<ListarDescricaoTipoResponse>(
                        new ListarDescricaoTipoRequest()
                        {
                            IncluirNamespaces =
                                new List<string>() 
                                { 
                                    typeof(LogSCFInfo).Namespace + "," + typeof(LogSCFInfo).Assembly.FullName
                                }
                        }).Resultado;
            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Auxilio

        /// <summary>
        /// Solicitacao de execucao de query
        /// </summary>
        public ExecutarAuxilioQueryResponse ExecutarAuxilioQuery(ExecutarAuxilioQueryRequest request)
        {
            // Prepara resposta
            ExecutarAuxilioQueryResponse resposta = new ExecutarAuxilioQueryResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Pede a execucao da query
                DataTable tb = Procedures.ExecutarQuery(request.Query);

                // Coloca o cabecalho
                resposta.Cabecalho = new string[tb.Columns.Count];
                for (int i = 0; i < tb.Columns.Count; i++)
                    resposta.Cabecalho[i] = tb.Columns[i].ColumnName;

                // Insere as linhas, limita em 100
                int limite = 100;
                resposta.Resultado = new string[tb.Rows.Count > limite ? limite : tb.Rows.Count];
                for (int j = 0; j < tb.Rows.Count && j < limite; j++)
                {
                    string[] coluna = new string[tb.Columns.Count];
                    for (int i = 0; i < tb.Columns.Count; i++)
                        coluna[i] = tb.Rows[j][i].ToString();
                    resposta.Resultado[j] = string.Join(";", coluna);
                }

            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Solicitacao de config
        /// </summary>
        public ExecutarAuxilioConfigResponse ExecutarAuxilioConfig(ExecutarAuxilioConfigRequest request)
        {
            // Prepara resposta
            ExecutarAuxilioConfigResponse resposta = new ExecutarAuxilioConfigResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta o tipo do config
                Type tipoConfig = ResolutorTipos.Resolver(request.TipoConfig);
                if (tipoConfig == null)
                    tipoConfig = Type.GetType(request.TipoConfig);

                // Verifica se encontrou o tipo
                if (tipoConfig == null)
                    throw new Exception("Tipo não encontrado");

                // Pede o config
                object config = ConfigHelper.Receber(tipoConfig, request.CodigoSessao);

                // Transforma em xml
                XmlSerializer serializer = new XmlSerializer(config.GetType());
                MemoryStream ms = new MemoryStream();
                serializer.Serialize(ms, config);

                // Insere na resposta
                ms.Position = 0;
                StreamReader reader = new StreamReader(ms);
                resposta.ConfigXML = reader.ReadToEnd();

            }
            catch (Exception ex)
            {
                // Processa a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region ConfiguracaoTipoRegistro
        /// <summary>
        /// Lista configuracao por tipo de retistro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarConfiguracaoTipoRegistroResponse ListarConfiguracaoTipoRegistro(ListarConfiguracaoTipoRegistroRequest request)
        {
            // Prepara resposta
            ListarConfiguracaoTipoRegistroResponse resposta = new ListarConfiguracaoTipoRegistroResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //1361 Permite ao usuário buscar pelo tipo arquivo 
                if (request.FiltroTipoArquivo != null)
                    condicoes.Add(new CondicaoInfo("TipoArquivo", CondicaoTipoEnum.Igual, request.FiltroTipoArquivo));

                //1361 Não existe este filtro na procedure PR_CONFIG_TIPO_REGISTRO_L
                ////Permite ao usuário buscar pelo status
                //if (request.FiltroAtivo != false)
                //    condicoes.Add(new CondicaoInfo("Ativo", CondicaoTipoEnum.Igual, request.FiltroAtivo));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ConfiguracaoTipoRegistroInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Recebe um configuracao do tipo de registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberConfiguracaoTipoRegistroResponse ReceberConfiguracaoTipoRegistro(ReceberConfiguracaoTipoRegistroRequest request)
        {
            // Prepara resposta
            ReceberConfiguracaoTipoRegistroResponse resposta = new ReceberConfiguracaoTipoRegistroResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.ConfiguracaoTipoRegistroInfo =
                    PersistenciaHelper.Receber<ConfiguracaoTipoRegistroInfo>(
                        request.CodigoSessao, request.CodigoConfiguracaoTipoRegistro);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove um configuracao do tipo de registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverConfiguracaoTipoRegistroResponse RemoverDiaNaoUtil(RemoverConfiguracaoTipoRegistroRequest request)
        {
            RemoverConfiguracaoTipoRegistroResponse res = new RemoverConfiguracaoTipoRegistroResponse();
            res.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove o item solicitado
                PersistenciaHelper.Remover<ConfiguracaoTipoRegistroInfo>(
                        request.CodigoSessao, request.CodigoConfiguracaoTipoRegistro);

                // Informa na resposta
                res.CodigoConfiguracaoTipoRegistro = request.CodigoConfiguracaoTipoRegistro;
            }
            catch (Exception ex)
            {
                // Trata o erro
                res.ProcessarExcessao(ex);
            }

            // Retorna
            return res;
        }

        /// <summary>
        /// Salva configuracao tipo de registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarConfiguracaoTipoRegistroResponse SalvarConfiguracaoTipoRegistro(SalvarConfiguracaoTipoRegistroRequest request)
        {
            // Prepara resposta
            SalvarConfiguracaoTipoRegistroResponse resposta = new SalvarConfiguracaoTipoRegistroResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.ConfiguracaoTipoRegistroInfo =
                    PersistenciaHelper.Salvar<ConfiguracaoTipoRegistroInfo>(
                        request.CodigoSessao, request.ConfiguracaoConfiguracaoTipoRegistroInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        #endregion

        #region CamposEnvioCCI
        /// <summary>
        /// Lista campos para envio CCI
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarConfiguracaoEnvioCamposCCIResponse ListarConfiguracaoEnvioCamposCCI(ListarConfiguracaoEnvioCamposCCIRequest request)
        {
            // Prepara resposta
            ListarConfiguracaoEnvioCamposCCIResponse resposta = new ListarConfiguracaoEnvioCamposCCIResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo status
                if (request.FiltroEnviarCCI != false)
                    condicoes.Add(
                        new CondicaoInfo(
                            "Ativo", CondicaoTipoEnum.Igual, request.FiltroEnviarCCI));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ConfiguracaoEnvioCamposCCIInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Recebe um campo para envio CCI
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //public ReceberConfiguracaoEnvioCamposCCIResponse ReceberConfiguracaoEnvioCamposCCI(ReceberConfiguracaoEnvioCamposCCIRequest request)
        //{
        //    // Prepara resposta
        //    ReceberConfiguracaoEnvioCamposCCIResponse resposta = new ReceberConfiguracaoEnvioCamposCCIResponse();
        //    resposta.PreparaResposta(request);

        //    // Bloco de controle
        //    try
        //    {
        //        // Retorna o item solicitado
        //        resposta.ConfiguracaoEnvioCamposCCIInfo =
        //            PersistenciaHelper.Receber<ConfiguracaoEnvioCamposCCIInfo>(
        //                request.CodigoSessao, request.CodigoCampoEnvioCCI);
        //    }
        //    catch (Exception ex)
        //    {
        //        // Informa erro na resposta
        //        resposta.ProcessarExcessao(ex);
        //    }

        //    // Retorna
        //    return resposta;
        //}

        /// <summary>
        /// Remove um configuracao do tipo de registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //public RemoverConfiguracaoEnvioCamposCCIResponse RemoverConfiguracaoEnvioCamposCCI(RemoverConfiguracaoEnvioCamposCCIRequest request)
        //{
        //    RemoverConfiguracaoEnvioCamposCCIResponse res = new RemoverConfiguracaoEnvioCamposCCIResponse();
        //    res.PreparaResposta(request);

        //    // Bloco de controle
        //    try
        //    {
        //        // Remove o item solicitado
        //        PersistenciaHelper.Remover<ConfiguracaoEnvioCamposCCIInfo>(
        //                request.CodigoSessao, request.CodigoCampoEnvioCCI);

        //        // Informa na resposta
        //        res.CodigoCampoEnvioCCI = request.CodigoCampoEnvioCCI;
        //    }
        //    catch (Exception ex)
        //    {
        //        // Trata o erro
        //        res.ProcessarExcessao(ex);
        //    }

        //    // Retorna
        //    return res;
        //}

        /// <summary>
        /// Salva configuracao tipo de registro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarConfiguracaoEnvioCamposCCIResponse SalvarConfiguracaoEnvioCamposCCI(SalvarConfiguracaoEnvioCamposCCIRequest request)
        {
            // Prepara resposta
            SalvarConfiguracaoEnvioCamposCCIResponse resposta = new SalvarConfiguracaoEnvioCamposCCIResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.ConfiguracaoEnvioCamposCCIInfo =
                    PersistenciaHelper.Salvar<ConfiguracaoEnvioCamposCCIInfo>(
                        request.CodigoSessao, request.ConfiguracaoEnvioCamposCCIInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        #endregion

        #region Relatórios

        /// <summary>
        /// Gera o processo para os relatório de vencimentos para excel
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GerarRelatorioVencimentoExcelResponse GerarRelatorioVencimentoExcel(GerarRelatorioVencimentoExcelRequest request)
        {
            // prepara a resposta
            GerarRelatorioVencimentoExcelResponse resposta = new GerarRelatorioVencimentoExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioVencimentosExcelInfo processoInfo =
                    new ProcessoRelatorioVencimentosExcelInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioVencimentoExcel(request);
            }

            // retorna a resposta
            return resposta;
        }

        public ExecutarProcessoExpurgoResponse ExecutarProcessoExpurgo(ExecutarProcessoExpurgoRequest request)
        {
            ExecutarProcessoExpurgoResponse resposta = new ExecutarProcessoExpurgoResponse();
            resposta.PreparaResposta(request);
            /*
                        if (request.CodigoProcesso == null)
                        {
                            // Carrega as configurações do sistema
                            ReceberConfiguracaoGeralResponse resConfig =
                                Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                            if (resConfig.ConfiguracaoGeralInfo.DiasAguardarExpurgo == null)
                                resConfig.ConfiguracaoGeralInfo.DiasAguardarExpurgo = 1;                

                            // Se a data da última atualização foi menor do que a de hoje menos os Dias a Aguardar, executa
                            DateTime hoje = DateTime.Now;
                            if (resConfig.ConfiguracaoGeralInfo.DataUltimoExpurgo == null || resConfig.ConfiguracaoGeralInfo.DataUltimoExpurgo < DateTime.Now.AddDays(resConfig.ConfiguracaoGeralInfo.DiasAguardarExpurgo * -1) || request.ExecutarAgora == true)
                            {
                                //resConfig.ConfiguracaoGeralInfo.DiasAguardarExpurgo 

                                // cria o processo de geracao do relatorio
                                ProcessoExpurgoArquivoItemInfo processoInfo =
                                    new ProcessoExpurgoArquivoItemInfo() { };

                                // executa o processo
                                ExecutarProcessoResponse respostaExecutar =
                                    Mensageria.Processar<ExecutarProcessoResponse>(
                                        new ExecutarProcessoRequest()
                                        {
                                            CodigoSessao = request.CodigoSessao,
                                            ProcessoInfo = processoInfo,
                                            ExecutarAssincrono = true
                                        });

                                // informa o codigo do processo
                                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
                            }
                        }
                        else
                        {
                            resposta = executarProcessoExpurgo(request);
                        }
             */
            // retorna a resposta
            return resposta;
        }

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private ExecutarProcessoExpurgoResponse executarProcessoExpurgo(ExecutarProcessoExpurgoRequest request)
        {
            // Prepara resposta
            ExecutarProcessoExpurgoResponse respostaRelatorioExpurgo = new ExecutarProcessoExpurgoResponse();
            respostaRelatorioExpurgo.PreparaResposta(request);

            try
            {
                // Logando log do processo
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Iniciando processo de expurgo ", request.CodigoSessao, request.CodigoProcesso);

                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Considerado o parâmetro de dias a ficar na base igual a " + resConfig.ConfiguracaoGeralInfo.DiasIgnorarExpurgo + ".", request.CodigoSessao, request.CodigoProcesso);

                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Considerado a data base " +
                    DateTime.Now.AddDays(-1 * resConfig.ConfiguracaoGeralInfo.DiasIgnorarExpurgo) + ", excluindo os registros com data de inclusão menor que esta", request.CodigoSessao, request.CodigoProcesso);

                int qtd = 0;

                // efetua o expurgo e traz a quantidade excluída
                qtd =
                    Procedures.PR_EXPURGO_ARQUIVO_ITEM(
                        new PR_EXPURGO_ARQUIVO_ITEM_Request() { });

                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Processo de expurgo da tabela arquivo_item finalizado", request.CodigoSessao, request.CodigoProcesso);

                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Quantidade de linhas removidas: " + qtd, request.CodigoSessao, request.CodigoProcesso);

            }
            catch (Exception ex)
            {
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Erro no processo de expurgo de arquivo_item: " + ex.InnerException, request.CodigoSessao, request.CodigoProcesso);
                // Trata erro
                respostaRelatorioExpurgo.ProcessarExcessao(ex);

            }

            // Retorna
            return respostaRelatorioExpurgo;
        }

        /// <summary>
        /// Gera o processo para geracao de pagamentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GerarPagamentobyRelatorioVenctoResponse GerarPagamentobyRelatorioVencto(GerarPagamentobyRelatorioVenctoRequest request)
        {
            // prepara a resposta
            GerarPagamentobyRelatorioVenctoResponse resposta = new GerarPagamentobyRelatorioVenctoResponse();
            resposta.PreparaResposta(request);

            ProcessoGerarPagamentoInfo processoInfo = null;

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                processoInfo =
                    new ProcessoGerarPagamentoInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                // Executa o processo assíncrono              
                resposta = gerarPagamentobyRelatorioVencto(request);
            }

            // retorna a resposta
            return resposta;
        }

        /// <summary>
        /// Executa a chamada para o relatorio de vencimentos, com o intuito de gerar pagamentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private GerarPagamentobyRelatorioVenctoResponse gerarPagamentobyRelatorioVencto(GerarPagamentobyRelatorioVenctoRequest request)
        {
            // Prepara resposta
            GerarPagamentobyRelatorioVenctoResponse respostaGeracaoPagamentos = new GerarPagamentobyRelatorioVenctoResponse();
            respostaGeracaoPagamentos.PreparaResposta(request);

            // Gerar log de inicio de processo
            Mensageria.Processar<SalvarLogResponse>(
                new SalvarLogRequest()
                {
                    CodigoUsuario = "",
                    Descricao = "Iniciando o processo de geração de pagamento.",
                    TipoOrigem = "ProcessoInfo",
                    CodigoOrigem = request.CodigoProcesso,
                    CodigoSessao = request.CodigoSessao
                });

            // Gerar log dos filtros
            // Informa os filtros da consulta
            LogSCFProcessoEstagioEventoInfo.EfetuarLog(retornaFiltroRelatorioVencimentoTOLog(request.Request).ToString().Replace(";", ","), request.CodigoSessao, request.CodigoProcesso);

            StreamWriter file = null;
            try
            {

                // Recupera o usuario que solicitou a geracao do pagamento
                UsuarioInfo usuarioInfo = new UsuarioInfo();
                usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);

                // Começa o preparacao para a geracao do relatorio
                request.Request.Link = request.Request.Link + "&codProcesso=" + request.CodigoProcesso;
                string urlAddress = request.Request.Link;

                // prepara o web request
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                webRequest.Timeout = 1000 * 60 * 60 * 5; // 5 horas
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0";
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.AllowAutoRedirect = true;
                webRequest.ContentType = "application/x-www-form-urlencoded";

                // Edgar Oliveira - 19/02/2019 - Adicionado gravação de log do link
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarPagamentobyRelatorioVencto - Efetuando requisição WEB: " + request.Request.Link, request.CodigoSessao, request.CodigoProcesso);

                // Efetua a chamada
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                string data = "";
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream receiveStream = webResponse.GetResponseStream();
                    StreamReader readStream = null;

                    if (webResponse.CharacterSet == null)
                    {
                        readStream = new StreamReader(receiveStream);
                    }
                    else
                    {
                        readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    }

                    data = readStream.ReadToEnd();

                    webResponse.Close();
                    readStream.Close();
                }
            }
            catch (Exception ex)
            {
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarPagamentobyRelatorioVencto - Erro na requisição:" + ex.Message, request.CodigoSessao, request.CodigoProcesso);

                // Trata erro
                respostaGeracaoPagamentos.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaGeracaoPagamentos;
        }

        private string retornaFiltroRelatorioVencimentoTOLog(ListarRelatorioVencimentoRequest request)
        {
            // busca informacoes
            // Recupera o grupo
            EmpresaGrupoInfo empresaGrupo = PersistenciaHelper.Receber<EmpresaGrupoInfo>(
                request.CodigoSessao, request.FiltroEmpresaGrupo);

            // recupera o subGrupo
            EmpresaSubGrupoInfo empresaSubGrupo = PersistenciaHelper.Receber<EmpresaSubGrupoInfo>(
                request.CodigoSessao, request.FiltroEmpresaSubGrupo);

            // recupera o favorecido
            FavorecidoInfo favorecido = PersistenciaHelper.Receber<FavorecidoInfo>(
                request.CodigoSessao, request.FiltroFavorecido);

            // monta a string para informar os filtros da consulta no LOG

            StringBuilder sb = new StringBuilder();

            string consolidados = null;

            // ---- Data de Movimento
            sb.Append("Data de Movimento = ");
            if (request.FiltroDataInicioMovimento != null && request.FiltroDataInicioMovimento != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltroDataInicioMovimento).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.FiltroDataFimMovimento != null && request.FiltroDataFimMovimento != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltroDataFimMovimento).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            // ---- Data de Transacao
            sb.Append("Data de Transacao = ");
            if (request.FiltroDataInicioTransacao != null && request.FiltroDataInicioTransacao != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltroDataInicioTransacao).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.FiltroDataFimTransacao != null && request.FiltroDataFimTransacao != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltroDataFimTransacao).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            // ---- Data de Vencimento
            sb.Append("Data de Vencimento = ");
            if (request.FiltroDataInicioVencimento != null && request.FiltroDataInicioVencimento != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltroDataInicioVencimento).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.FiltroDataFimVencimento != null && request.FiltroDataFimVencimento != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltroDataFimVencimento).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            // ---- Data de Agendamento
            sb.Append("Data de Agendamento = ");
            if (request.FiltroDataAgendamentoDe != null && request.FiltroDataAgendamentoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltroDataAgendamentoDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.FiltroDataAgendamentoAte != null && request.FiltroDataAgendamentoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltroDataAgendamentoAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            // ---- Data de Retorno CCI
            sb.Append("Data de Retorno CCI = ");
            if (request.FiltrotxtDataRetornoCCIInicio != null && request.FiltrotxtDataRetornoCCIInicio != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltrotxtDataRetornoCCIInicio).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.FiltrotxtDataRetornoCCIFim != null && request.FiltrotxtDataRetornoCCIFim != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.FiltrotxtDataRetornoCCIFim).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            // ---- Grupo
            sb.Append("Grupo = ");
            if (empresaGrupo != null)
                sb.Append(empresaGrupo.NomeEmpresaGrupo + "; ");
            else
                sb.Append("Todos; ");
            sb.Append("SubGrupo = ");
            if (empresaSubGrupo != null)
                sb.Append(empresaSubGrupo.NomeEmpresaSubGrupo + "; ");
            else
                sb.Append("Todos; ");

            // ---- Favorecido
            sb.Append("Favorecido = ");
            if (favorecido != null)
                sb.Append(favorecido.NomeFavorecido + "; ");
            else
                sb.Append("Todos; ");

            // ---- Consolidacao
            if (request.FiltroConsolidaReferencia == true) consolidados = "Referencia";
            if (request.FiltroConsolidaAgendamentoVencimento == true)
                consolidados = "Dt Agend + Dt. Venc" + (consolidados != null ? " + " + consolidados : "");
            else if (request.FiltroConsolidaMaiorData == true)
                consolidados = "Maior Dt. Agend" + (consolidados != null ? " + " + consolidados : "");
            sb.Append("Consolidacao = " + consolidados + ";");

            // ---- Produtos
            sb.Append("Produtos = ");
            if (String.IsNullOrEmpty(request.FiltroProduto))
                sb.Append("Todos; ");
            else
            {
                StringBuilder sbProduto = new StringBuilder();
                string[] produtos = request.FiltroProduto.Split(',');
                foreach (string produto in produtos)
                {
                    // recupera o produto
                    ProdutoInfo produtoInfo = PersistenciaHelper.Receber<ProdutoInfo>(
                        request.CodigoSessao, produto);

                    if (produtoInfo != null)
                        sbProduto.Append(produtoInfo.NomeProduto + ", ");
                }

                string produtoRecuperados = sbProduto.ToString().Substring(0, sbProduto.ToString().LastIndexOf(","));
                sb.Append(produtoRecuperados + "; ");
            }

            // ---- Referencias
            sb.Append("Referencias = ");
            if (String.IsNullOrEmpty(request.FiltroReferencia))
                sb.Append("Todos; ");
            else
            {

                StringBuilder sbReferencia = new StringBuilder();
                string[] referencias = request.FiltroReferencia.Split(',');
                foreach (string referencia in referencias)
                {
                    List<CondicaoInfo> condicoesRef = new List<CondicaoInfo>();

                    condicoesRef.Add(
                    new CondicaoInfo(
                        "Valor", CondicaoTipoEnum.Igual, referencia));

                    // recupera a referencia
                    List<ListaItemInfo> referenciaInfo =
                        PersistenciaHelper.Listar<ListaItemInfo>(
                            null, condicoesRef);

                    foreach (ListaItemInfo listaItem in referenciaInfo)
                    {
                        sbReferencia.Append(listaItem.Descricao + ", ");
                    }
                }

                string referenciaRecuperados = sbReferencia.ToString().Substring(0, sbReferencia.ToString().LastIndexOf(","));
                sb.Append(referenciaRecuperados + "; ");
            }

            // ---- Tipo de Registro
            sb.Append("Tipo de Registro = " + request.FiltroTipoRegistro + "; ");
            if (request.FiltroStatusRetornoCessao != null)
            {
                sb.Append("Status Cessao = ");
                sb.Append(request.FiltroStatusRetornoCessao == "S" ? "Cessionadas" : "Nao cessionadas");
                sb.Append("; ");
            }
            else
                sb.Append("Status Cessao = Todos; ");


            // ---- Posicao em carteira
            // filtro de posicao em carteira
            sb.Append("Posicao em Carteira = " + String.Format("{0:dd/MM/yyyy}", request.FiltroDataPosicao) + "; ");

            // ---- Tipo Financeiro ou Contabil
            if (request.FiltroFinanceiroContabil != null)
            {
                if (request.FiltroFinanceiroContabil == "C")
                    sb.Append("Contabil; ");
                else
                    sb.Append("Financeiro; ");
            }

            // ---- Retornar Pagas
            sb.Append("Retornar Pagas = ");
            if (request.FiltroRetornarTransacaoPaga)
                sb.Append("Sim; ");
            else
                sb.Append("Não; ");

            return sb.ToString();
        }




        /// <summary>
        /// Gera o processo para os relatório de vencimentos para excel
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GerarRelatorioVencimentoPDFResponse GerarRelatorioVencimentoPDF(GerarRelatorioVencimentoPDFRequest request)
        {
            // prepara a resposta
            GerarRelatorioVencimentoPDFResponse resposta = new GerarRelatorioVencimentoPDFResponse();
            resposta.PreparaResposta(request);

            ProcessoRelatorioVencimentosPDFInfo processoInfo = null;

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                processoInfo =
                    new ProcessoRelatorioVencimentosPDFInfo()
                    {
                        Param = request.Request
                    };
                //1353//Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Iniciando criação do processo.",
                //        TipoOrigem = "RelatorioVencimento"
                //    });

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                //1353 //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Processo criado com sucesso.",
                //        TipoOrigem = "RelatorioVencimento",
                //        CodigoOrigem = respostaExecutar.CodigoProcesso
                //    });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Iniciando o processo.",
                //        TipoOrigem = "RelatorioVencimento",
                //        CodigoOrigem = request.CodigoProcesso
                //    });

                // Executa o processo assíncrono              
                resposta = gerarRelatorioVencimentoPDF(request);
            }

            // retorna a resposta
            return resposta;
        }

        /// <summary>
        /// Gera o processo para o relatório de consolidado para PDF
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GerarRelatorioConsolidadoPDFResponse GerarRelatorioConsolidadoPDF(GerarRelatorioConsolidadoPDFRequest request)
        {
            // prepara a resposta
            GerarRelatorioConsolidadoPDFResponse resposta = new GerarRelatorioConsolidadoPDFResponse();
            resposta.PreparaResposta(request);

            ProcessoRelatorioConsolidadoPDFInfo processoInfo = null;

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                processoInfo =
                    new ProcessoRelatorioConsolidadoPDFInfo()
                    {
                        Param = request.Request
                    };

                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Iniciando criação do processo.",
                //        TipoOrigem = "RelatorioConsolidado"
                //    });

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Processo criado com sucesso.",
                //        TipoOrigem = "RelatorioConsolidado",
                //        CodigoOrigem = respostaExecutar.CodigoProcesso
                //    });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Iniciando o processo.",
                //        TipoOrigem = "RelatorioConsolidado",
                //        CodigoOrigem = request.CodigoProcesso
                //    });

                // Executa o processo assíncrono              
                resposta = gerarRelatorioConsolidadoPDF(request);
            }

            // retorna a resposta
            return resposta;
        }

        /// <summary>
        /// Gera o processo para os relatório de vendas para PDF
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GerarRelatorioVendasPDFResponse GerarRelatorioVendasPDF(GerarRelatorioVendasPDFRequest request)
        {
            // prepara a resposta
            GerarRelatorioVendasPDFResponse resposta = new GerarRelatorioVendasPDFResponse();
            resposta.PreparaResposta(request);
            ProcessoRelatorioVendasPDFInfo processoInfo = null;
            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                processoInfo =
                    new ProcessoRelatorioVendasPDFInfo()
                    {
                        Param = request.Request
                    };

                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Iniciando criação do processo.",
                //        TipoOrigem = "RelatorioVencimento"
                //    });

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Processo criado com sucesso.",
                //        TipoOrigem = "RelatorioVencimento",
                //        CodigoOrigem = respostaExecutar.CodigoProcesso
                //    });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "APP - Processo - Iniciando o processo.",
                //        TipoOrigem = "RelatorioVencimento",
                //        CodigoOrigem = request.CodigoProcesso
                //    });

                resposta = gerarRelatorioVendasPDF(request);

            }

            // retorna a resposta
            return resposta;
        }

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private GerarRelatorioVencimentoExcelResponse gerarRelatorioVencimentoExcel(GerarRelatorioVencimentoExcelRequest request)
        {
            // Prepara resposta
            GerarRelatorioVencimentoExcelResponse respostaRelatorioExcel = new GerarRelatorioVencimentoExcelResponse();
            respostaRelatorioExcel.PreparaResposta(request);

            //// Recupera o grupo
            //EmpresaGrupoInfo empresaGrupo = PersistenciaHelper.Receber<EmpresaGrupoInfo>(
            //    request.CodigoSessao, request.Request.FiltroEmpresaGrupo);

            //// recupera o subGrupo
            //EmpresaSubGrupoInfo empresaSubGrupo = PersistenciaHelper.Receber<EmpresaSubGrupoInfo>(
            //    request.CodigoSessao, request.Request.FiltroEmpresaSubGrupo);

            //// recupera o favorecido
            //FavorecidoInfo favorecido = PersistenciaHelper.Receber<FavorecidoInfo>(
            //    request.CodigoSessao, request.Request.FiltroFavorecido);

            //// monta a string para informar os filtros da consulta
            //StringBuilder sb = new StringBuilder();
            //string consolidados = null;
            //sb.Append("Data de Movimento = ");
            //if (request.Request.FiltroDataInicioMovimento != null && request.Request.FiltroDataInicioMovimento != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltroDataInicioMovimento).ToString("dd/MM/yyyy") + " - ");
            //else
            //    sb.Append("Qualquer - ");
            //if (request.Request.FiltroDataFimMovimento != null && request.Request.FiltroDataFimMovimento != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltroDataFimMovimento).ToString("dd/MM/yyyy") + "; ");
            //else
            //    sb.Append("Qualquer; ");
            //sb.Append("Data de Transacao = ");
            //if (request.Request.FiltroDataInicioTransacao != null && request.Request.FiltroDataInicioTransacao != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltroDataInicioTransacao).ToString("dd/MM/yyyy") + " - ");
            //else
            //    sb.Append("Qualquer - ");
            //if (request.Request.FiltroDataFimTransacao != null && request.Request.FiltroDataFimTransacao != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltroDataFimTransacao).ToString("dd/MM/yyyy") + "; ");
            //else
            //    sb.Append("Qualquer; ");
            //sb.Append("Data de Vencimento = ");
            //if (request.Request.FiltroDataInicioVencimento != null && request.Request.FiltroDataInicioVencimento != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltroDataInicioVencimento).ToString("dd/MM/yyyy") + " - ");
            //else
            //    sb.Append("Qualquer - ");
            //if (request.Request.FiltroDataFimVencimento != null && request.Request.FiltroDataFimVencimento != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltroDataFimVencimento).ToString("dd/MM/yyyy") + "; ");
            //else
            //    sb.Append("Qualquer; ");
            //sb.Append("Data de Agendamento = ");
            //if (request.Request.FiltroDataAgendamentoDe != null && request.Request.FiltroDataAgendamentoDe != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltroDataAgendamentoDe).ToString("dd/MM/yyyy") + " - ");
            //else
            //    sb.Append("Qualquer - ");
            //if (request.Request.FiltroDataAgendamentoAte != null && request.Request.FiltroDataAgendamentoAte != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltroDataAgendamentoAte).ToString("dd/MM/yyyy") + "; ");
            //else
            //    sb.Append("Qualquer; ");
            ////SCF1170
            //sb.Append("Data de Retorno CCI = ");
            //if (request.Request.FiltrotxtDataRetornoCCIInicio != null && request.Request.FiltrotxtDataRetornoCCIInicio != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltrotxtDataRetornoCCIInicio).ToString("dd/MM/yyyy") + " - ");
            //else
            //    sb.Append("Qualquer - ");
            //if (request.Request.FiltrotxtDataRetornoCCIFim != null && request.Request.FiltrotxtDataRetornoCCIFim != DateTime.MinValue)
            //    sb.Append(Convert.ToDateTime(request.Request.FiltrotxtDataRetornoCCIFim).ToString("dd/MM/yyyy") + "; ");
            //else
            //    sb.Append("Qualquer; ");
            ////SCF1170

            ////sb.Append("Data de Posicao de Carteira = ");
            ////if (request.Request.FiltroDataPosicao != null && request.Request.FiltroDataPosicao != DateTime.MinValue)
            ////    sb.Append(Convert.ToDateTime(request.Request.FiltroDataPosicao).ToString("dd/MM/yyyy") + "; ");
            ////else
            ////    sb.Append("Qualquer;");

            //sb.Append("Grupo = ");
            //if (empresaGrupo != null)
            //    sb.Append(empresaGrupo.NomeEmpresaGrupo + "; ");
            //else
            //    sb.Append("Todos; ");
            //sb.Append("SubGrupo = ");
            //if (empresaSubGrupo != null)
            //    sb.Append(empresaSubGrupo.NomeEmpresaSubGrupo + "; ");
            //else
            //    sb.Append("Todos; ");
            //sb.Append("Favorecido = ");
            //if (favorecido != null)
            //    sb.Append(favorecido.NomeFavorecido + "; ");
            //else
            //    sb.Append("Todos; ");

            ////ClockWork - Rogério Alves 
            //if (request.Request.FiltroConsolidaReferencia == true) consolidados = "Referencia";
            //if (request.Request.FiltroConsolidaAgendamentoVencimento == true)
            //    consolidados = "Dt Agend + Dt. Venc" + (consolidados != null ? " + " + consolidados : "");
            //else if (request.Request.FiltroConsolidaMaiorData == true)
            //    consolidados = "Maior Dt. Agend" + (consolidados != null ? " + " + consolidados : "");
            //sb.Append("Consolidacao = " + consolidados + ";");

            //sb.Append("Produtos = ");
            //if (String.IsNullOrEmpty(request.Request.FiltroProduto))
            //    sb.Append("Todos; ");
            //else
            //{
            //    StringBuilder sbProduto = new StringBuilder();
            //    string[] produtos = request.Request.FiltroProduto.Split(',');
            //    foreach (string produto in produtos)
            //    {
            //        // recupera o produto
            //        ProdutoInfo produtoInfo = PersistenciaHelper.Receber<ProdutoInfo>(
            //            request.CodigoSessao, produto);

            //        if (produtoInfo != null)
            //            sbProduto.Append(produtoInfo.NomeProduto + ", ");
            //    }

            //    string produtoRecuperados = sbProduto.ToString().Substring(0, sbProduto.ToString().LastIndexOf(","));
            //    sb.Append(produtoRecuperados + "; ");
            //}

            ////ClockWork Rogério : Inicio
            //sb.Append("Referencias = ");
            //if (String.IsNullOrEmpty(request.Request.FiltroReferencia))
            //    sb.Append("Todos; ");
            //else
            //{

            //    StringBuilder sbReferencia = new StringBuilder();
            //    string[] referencias = request.Request.FiltroReferencia.Split(',');
            //    foreach (string referencia in referencias)
            //    {
            //        List<CondicaoInfo> condicoesRef = new List<CondicaoInfo>();

            //        condicoesRef.Add(
            //        new CondicaoInfo(
            //            "Valor", CondicaoTipoEnum.Igual, referencia));

            //        // recupera a referencia
            //        List<ListaItemInfo> referenciaInfo =
            //            PersistenciaHelper.Listar<ListaItemInfo>(
            //                null, condicoesRef);

            //        foreach (ListaItemInfo listaItem in referenciaInfo)
            //        {
            //            sbReferencia.Append(listaItem.Descricao + ", ");
            //        }
            //    }

            //    string referenciaRecuperados = sbReferencia.ToString().Substring(0, sbReferencia.ToString().LastIndexOf(","));
            //    sb.Append(referenciaRecuperados + "; ");
            //}
            ////ClockWork Rogério : Fim

            //sb.Append("Tipo de Registro = " + request.Request.FiltroTipoRegistro + "; ");
            //if (request.Request.FiltroStatusRetornoCessao != null)
            //{
            //    sb.Append("Status Cessao = ");
            //    sb.Append(request.Request.FiltroStatusRetornoCessao == "S" ? "Cessionadas" : "Nao cessionadas");
            //    sb.Append("; ");
            //}
            //else
            //    sb.Append("Status Cessao = Todos; ");

            //// filtro de posicao em carteira
            //sb.Append("Posicao em Carteira = " + String.Format("{0:dd/MM/yyyy}", request.Request.FiltroDataPosicao) + "; ");

            //// ECOMMERCE - Fernando Bove - 20160111: Inicio
            //if (request.Request.FiltroFinanceiroContabil != null)
            //{
            //    if (request.Request.FiltroFinanceiroContabil == "C")
            //        sb.Append("Contabil; ");
            //    else
            //        sb.Append("Financeiro; ");
            //}
            //// ECOMMERCE - Fernando Bove - 20160111: Fim

            // gera a string 
            StringBuilder sb = new StringBuilder();
            sb.Append(retornaFiltroRelatorioVencimentoTOLog(request.Request));


            StreamWriter file = null;
            try
            {
                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });
                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //    new SalvarLogRequest()
                //    {
                //        CodigoUsuario = "",
                //        Descricao = "Iniciando validações",
                //        TipoOrigem = "APP",
                //        CodigoOrigem = request.CodigoProcesso,
                //        CodigoSessao = request.CodigoSessao
                //    });
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Iniciando validações", request.CodigoSessao, request.CodigoProcesso);
                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento == "")
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioExcel.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);
                    return respostaRelatorioExcel;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento.Length - 1) == @"\")
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento;
                    else
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento + "\\";
                }
                // pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                DateTime dataAtual = DateTime.Now;
                //string caminho = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento
                //    + @"\RelatorioVencimento_" + dataAtual.Year.ToString("0000")
                //    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                //    + dataAtual.Hour.ToString("00") + dataAtual.Minute.ToString("00")
                //    + dataAtual.Second.ToString("00") + ".csv";

                string nome = "";
                if (usuarioInfo.Nome.Contains(" "))
                    nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                else
                    nome = usuarioInfo.Nome;

                string caminho = respostaRelatorioExcel.CaminhoArquivoGerado
                    //scf1248+ @"\" + nome + @"_RelVencimento_" + dataAtual.Year.ToString("0000")
                    + nome + @"_RelVencimento_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + ".csv";

                if (!System.IO.Directory.Exists(respostaRelatorioExcel.CaminhoArquivoGerado))
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioExcel.CaminhoArquivoGerado + "]";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioExcel.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioExcel;
                }
                else
                {
                    file = new StreamWriter(caminho, true, Encoding.Default);

                    try
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Arquivo criado:" + caminho, request.CodigoSessao, request.CodigoProcesso);
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Realizando consulta dos vencimentos no banco de dados", request.CodigoSessao, request.CodigoProcesso);
                    }
                    catch (Exception e)
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Excecao:" + e.InnerException, request.CodigoSessao, request.CodigoProcesso);
                    }
                    // Pede o relatório
                    ListarRelatorioVencimentoResponse resposta =
                        Mensageria.Processar<ListarRelatorioVencimentoResponse>(
                            new ListarRelatorioVencimentoRequest()
                            {
                                CodigoSessao = request.CodigoSessao,
                                FiltroBanco = request.Request.FiltroBanco,
                                FiltroDataFimMovimento = request.Request.FiltroDataFimMovimento,
                                FiltroDataFimTransacao = request.Request.FiltroDataFimTransacao,
                                FiltroDataFimVencimento = request.Request.FiltroDataFimVencimento,
                                FiltroDataInicioMovimento = request.Request.FiltroDataInicioMovimento,
                                FiltroDataInicioTransacao = request.Request.FiltroDataInicioTransacao,
                                FiltroDataInicioVencimento = request.Request.FiltroDataInicioVencimento,
                                FiltroDataAgendamentoDe = request.Request.FiltroDataAgendamentoDe,
                                FiltroDataAgendamentoAte = request.Request.FiltroDataAgendamentoAte,
                                FiltroDataPosicao = request.Request.FiltroDataPosicao,
                                FiltroDataMovimento = request.Request.FiltroDataMovimento,
                                FiltroDataTransacao = request.Request.FiltroDataTransacao,
                                FiltroDataVencimento = request.Request.FiltroDataVencimento,
                                FiltroEmpresaGrupo = request.Request.FiltroEmpresaGrupo,
                                FiltroEmpresaSubGrupo = request.Request.FiltroEmpresaSubGrupo,
                                FiltroFavorecido = request.Request.FiltroFavorecido,
                                FiltroConsolidaAgendamentoVencimento = request.Request.FiltroConsolidaAgendamentoVencimento,
                                FiltroConsolidaMaiorData = request.Request.FiltroConsolidaMaiorData,
                                FiltroConsolidaReferencia = request.Request.FiltroConsolidaReferencia,
                                FiltroProduto = request.Request.FiltroProduto,
                                FiltroReferencia = request.Request.FiltroReferencia,
                                FiltroTipoRegistro = request.Request.FiltroTipoRegistro,
                                VerificarQuantidadeLinhas = request.Request.VerificarQuantidadeLinhas,
                                FiltroStatusRetornoCessao = request.Request.FiltroStatusRetornoCessao,
                                // ECOMMERCE - Fernando Bove - 20160111: Filtro Tipo Financeiro Contabil
                                FiltroFinanceiroContabil = request.Request.FiltroFinanceiroContabil,
                                //SCF1170
                                FiltrotxtDataRetornoCCIInicio = request.Request.FiltrotxtDataRetornoCCIInicio,
                                FiltrotxtDataRetornoCCIFim = request.Request.FiltrotxtDataRetornoCCIFim,
                                //SCF1170
                                CodigoProcesso = request.CodigoProcesso,
                                //Edgar Oliveira
                                FiltroRetornarTransacaoPaga = request.Request.FiltroRetornarTransacaoPaga,
                                TipoRelatorio = "Excel"
                            });

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Consulta dos vencimentos finalizada", request.CodigoSessao, request.CodigoProcesso);

                    // adiciona os filtros
                    file.WriteLine(sb.ToString());
                    // Informa os filtros da consulta
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(sb.ToString().Replace(";", ","), request.CodigoSessao, request.CodigoProcesso);
                    String dados;
                    dados = "Grupo;Banco;Agência;Conta;CNPJ;Tipo Registro;Produto;Data de Vencimento;Data de Agendamento;Valor Bruto;Comissão;Valor Liquido";
                    if (request.Request.FiltroConsolidaReferencia)
                        dados += ";Referência";

                    file.WriteLine(dados);

                    // Quantidade de linhas para log
                    int quantidadeLog = 0;

                    foreach (RelatorioVencimentoInfo relatorioVencimento in resposta.Resultado)
                    {
                        // ==========================================================================================
                        // 1443 - INICIO
                        // Ocorreu de no campo CONTA existir o valor 1-9 e no excel ficou 01/09/2017
                        // Tratando os campos AGENCIA e CONTA que podem conter HIFEN para serem consideradas string
                        // ==========================================================================================

                        dados = relatorioVencimento.DescricaoEmpresaGrupo + ";"
                            + relatorioVencimento.Estabelecimento.Banco + ";"
                            //1443 + relatorioVencimento.Estabelecimento.Agencia + ";"
                            + "=\"" + relatorioVencimento.Estabelecimento.Agencia + "\";" //1443
                            //1443 + relatorioVencimento.Estabelecimento.Conta + ";" 
                            + "=\"" + relatorioVencimento.Estabelecimento.Conta + "\";" //1443 
                            + "=\"" + relatorioVencimento.Estabelecimento.CNPJ + "\";"
                            + request.Request.FiltroTipoRegistro + ";"
                            + relatorioVencimento.Produto.DescricaoProduto + ";"
                            //+ relatorioVencimento.CodigoPagamento + ";"
                            + String.Format("{0:dd/MM/yyyy}", relatorioVencimento.DataVencimento) + ";"
                            + String.Format("{0:dd/MM/yyyy}", relatorioVencimento.DataAgendamento) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVencimento.ValorBruto) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVencimento.ValorComissao) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVencimento.ValorLiquido) + ";"
                            + "=\"" + relatorioVencimento.Referencia.Valor + "\";";
                        // + relatorioVencimento.ValorLiquido.ToString("N2") + ";";

                        file.WriteLine(dados);

                        // Atualiza a quantidade de linhas geradas
                        respostaRelatorioExcel.QuantidadeLinhasGeradas++;
                        quantidadeLog++;

                        // Sinaliza
                        if (quantidadeLog >= 15000)
                        {
                            // Sinaliza
                            LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                                respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                            // Zera linhas do log
                            quantidadeLog = 0;
                        }

                    }
                    //1353
                    //Mensageria.Processar<SalvarLogResponse>(
                    //new SalvarLogRequest()
                    //{
                    //    CodigoUsuario = "",
                    //    Descricao = "APP - FIM - ListarRelatorioConsolidado - Fim da geração do Excel.",
                    //    TipoOrigem = "APP",
                    //    CodigoOrigem = resposta.CodLog
                    //});
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Total de linhas geradas: " + respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);
                }

            }
            catch (Exception ex)
            {
                // Trata erro
                respostaRelatorioExcel.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioExcel;
        }

        //private HttpWebRequest webRequest;
        //private string sessao;
        //private string processo;
        //private string caminho;

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private GerarRelatorioVencimentoPDFResponse gerarRelatorioVencimentoPDF(GerarRelatorioVencimentoPDFRequest request)
        {
            // Prepara resposta
            GerarRelatorioVencimentoPDFResponse respostaRelatorioPdf = new GerarRelatorioVencimentoPDFResponse();
            respostaRelatorioPdf.PreparaResposta(request);

            StreamWriter file = null;
            try
            {
                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento == "")
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioPdf.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    //respostaRelatorioExcel.Erro = "O diretório de exportação do relatório não está configurado.";
                    return respostaRelatorioPdf;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento.Length - 1) == @"\")
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento;
                    else
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVencimento + "\\";
                }

                //Jira SCF1070 - Marcos Matsuoka
                // pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = new UsuarioInfo();

                DateTime dataAtual = DateTime.Now;
                string nome = string.Empty;

                //Caso o codigo de sessao seja nulo, atribui um nome do usuario "Automatico", pois se trata da execução do processo
                //LiberarTransacoesEmAndamento executado quando o servidor de aplicação é reiniciado.
                if (request.CodigoSessao != null)
                {
                    // pega o usuario para recuperar o nome e colocar no arquivo gerado
                    usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                    if (usuarioInfo.Nome.Contains(" "))
                        nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                    else
                        nome = usuarioInfo.Nome;
                }
                else
                {
                    //Atribui o nome Automatico pois ainda não existe Sessão válida
                    nome = "Automatico";
                }
                //Fim

                string caminho = respostaRelatorioPdf.CaminhoArquivoGerado
                    //scf1248+ @"\" + nome + @"_RelVencimento_" + dataAtual.Year.ToString("0000")
                    + nome + @"_RelVencimento_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + (request.Request.TipoArquivo == null ? ".csv" : ".pdf");

                if (!System.IO.Directory.Exists(respostaRelatorioPdf.CaminhoArquivoGerado))
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioPdf.CaminhoArquivoGerado + "]";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioPdf.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioPdf;
                }

                request.Request.Link = request.Request.Link + "&codLog=" + request.CodigoProcesso;
                string urlAddress = request.Request.Link;
                string strCSS = null;

                ServicePointManager.ServerCertificateValidationCallback =
                    new System.Net.Security.RemoteCertificateValidationCallback(
                        (object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors) =>
                        {
                            return true;
                        });

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                webRequest.Timeout = 1000 * 60 * 60 * 5; // 5 horas
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0";
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.AllowAutoRedirect = true;
                webRequest.ContentType = "application/x-www-form-urlencoded";

                //webRequest.Proxy = proxy;
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioVencimentoPDF - Efetuando requisição WEB: " + request.Request.Link, request.CodigoSessao, request.CodigoProcesso);

                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                //webRequest.BeginGetResponse(new AsyncCallback(gerarPDF), webRequest);
                string data = "";
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    //PdfDocument doc = converter.ConvertUrl(request.Request.Link);
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                       "Iniciando conversão do HTML para PDF. ", request.CodigoSessao, request.CodigoProcesso);

                    Stream receiveStream = webResponse.GetResponseStream();
                    StreamReader readStream = null;

                    if (webResponse.CharacterSet == null)
                    {
                        readStream = new StreamReader(receiveStream);
                    }
                    else
                    {
                        readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    }

                    data = readStream.ReadToEnd();
                    if (data != "")
                    {
                        // Recupera Estilos do CSS para aplicar no relatório PDF
                        urlAddress = urlAddress.Replace(".aspx", ".css");
                        int posIni = urlAddress.IndexOf("?");
                        urlAddress = urlAddress.Substring(0, posIni);

                        posIni = data.IndexOf("link href");
                        if (posIni > 0)
                        {
                            int posFim = data.IndexOf(".css", posIni) + 3;
                            string urlAddressCss = data.Substring(posIni + 11, posFim - posIni - 10);

                            posFim = urlAddress.LastIndexOf(@"/");
                            urlAddress = urlAddress.Substring(0, posFim + 1) + urlAddressCss;
                        }

                        try
                        {
                            webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                            webResponse = (HttpWebResponse)webRequest.GetResponse();

                            receiveStream = null;
                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                receiveStream = webResponse.GetResponseStream();
                                strCSS = new StreamReader(receiveStream, Encoding.UTF8).ReadToEnd();
                            }
                        }
                        catch (Exception) {} // catch (Exception ex) {/* ignorar erro caso não tenha CSS */} // warning 25/02/219
                    }

                    webResponse.Close();
                    readStream.Close();
                }

                if (data != "")
                {
                    //Create a byte array that will eventually hold our final PDF
                    Byte[] bytes;
                    ITextEvents ev = new ITextEvents(null, null);

                    //Gerar o PDF a partir do HTML
                    bytes = ev.ImprimirPDF(data, strCSS, "A4,Rotate");

                    //Now we just need to do something with those bytes.
                    //Here I'm writing them to disk but if you were in ASP.Net you might Response.BinaryWrite() them.
                    //You could also write the bytes to a database in a varbinary() column (but please don't) or you
                    //could pass them to another function for further PDF processing.
                    System.IO.File.WriteAllBytes(caminho, bytes);

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        "Conversão concluída. Iniciando a criação do PDF em arquivo.\n" + caminho, request.CodigoSessao, request.CodigoProcesso);
                }
            }
            catch (Exception ex)
            {
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioVencimentoPDF - Erro na requisição:" + ex.Message, request.CodigoSessao, request.CodigoProcesso);

                // Trata erro
                respostaRelatorioPdf.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioPdf;
        }

        private GerarRelatorioConsolidadoPDFResponse gerarRelatorioConsolidadoPDF(GerarRelatorioConsolidadoPDFRequest request)
        {
            // Prepara resposta
            GerarRelatorioConsolidadoPDFResponse respostaRelatorioPdf = new GerarRelatorioConsolidadoPDFResponse();
            respostaRelatorioPdf.PreparaResposta(request);

            StreamWriter file = null;
            try
            {
                // Carrega as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado == "")
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioPdf.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    //respostaRelatorioExcel.Erro = "O diretório de exportação do relatório não está configurado.";
                    return respostaRelatorioPdf;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado.Length - 1) == @"\")
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado;
                    else
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado + "\\";
                }

                //Jira SCF1070 - Marcos Matsuoka
                // pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = new UsuarioInfo();

                DateTime dataAtual = DateTime.Now;
                string nome = string.Empty;

                //Caso o codigo de sessao seja nulo, atribui um nome do usuario "Automatico", pois se trata da execução do processo
                //LiberarTransacoesEmAndamento executado quando o servidor de aplicação é reiniciado.
                if (request.CodigoSessao != null)
                {
                    // pega o usuario para recuperar o nome e colocar no arquivo gerado
                    usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                    if (usuarioInfo.Nome.Contains(" "))
                        nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                    else
                        nome = usuarioInfo.Nome;
                }
                else
                {
                    //Atribui o nome Automatico pois ainda não existe Sessão válida
                    nome = "Automatico";
                }
                //Fim

                string caminho = respostaRelatorioPdf.CaminhoArquivoGerado
                    //scf1248+ @"\" + nome + @"_RelConsolidado_" + dataAtual.Year.ToString("0000")
                    + nome + @"_RelConsolidado_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + (request.Request.TipoArquivo == null ? ".csv" : ".pdf");

                if (!System.IO.Directory.Exists(respostaRelatorioPdf.CaminhoArquivoGerado))
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioPdf.CaminhoArquivoGerado + "]";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioPdf.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioPdf;
                }

                request.Request.Link = request.Request.Link + "&codLog=" + request.CodigoProcesso;
                string urlAddress = request.Request.Link;
                string strCSS = null;

                ServicePointManager.ServerCertificateValidationCallback =
                    new System.Net.Security.RemoteCertificateValidationCallback(
                        (object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors) =>
                        {
                            return true;
                        });

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                webRequest.Timeout = 1000 * 60 * 60 * 5; // 5 horas
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0";
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.AllowAutoRedirect = true;
                webRequest.ContentType = "application/x-www-form-urlencoded";


                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioConsolidadoPDF - Efetuando requisição WEB: " + request.Request.Link, request.CodigoSessao, request.CodigoProcesso);

                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                //webRequest.BeginGetResponse(new AsyncCallback(gerarPDF), webRequest);
                string data = "";
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                       "Iniciando conversão do HTML para PDF. ", request.CodigoSessao, request.CodigoProcesso);

                    Stream receiveStream = webResponse.GetResponseStream();
                    StreamReader readStream = null;

                    if (webResponse.CharacterSet == null)
                    {
                        readStream = new StreamReader(receiveStream);
                    }
                    else
                    {
                        readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    }

                    data = readStream.ReadToEnd(); //SCF1097
                    if (data != "")
                    {
                        // Recupera Estilos do CSS para aplicar no relatório PDF
                        urlAddress = urlAddress.Replace(".aspx", ".css");
                        int posIni = urlAddress.IndexOf("?");
                        urlAddress = urlAddress.Substring(0, posIni);

                        posIni = data.IndexOf("link href");
                        if (posIni > 0)
                        {
                            int posFim = data.IndexOf(".css", posIni) + 3;
                            string urlAddressCss = data.Substring(posIni + 11, posFim - posIni - 10);

                            posFim = urlAddress.LastIndexOf(@"/");
                            urlAddress = urlAddress.Substring(0, posFim + 1) + urlAddressCss;
                        }

                        try
                        {
                            webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                            webResponse = (HttpWebResponse)webRequest.GetResponse();

                            receiveStream = null;
                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                receiveStream = webResponse.GetResponseStream();
                                strCSS = new StreamReader(receiveStream, Encoding.UTF8).ReadToEnd();
                            }
                        }
                        catch (Exception) { } // catch (Exception ex) {/* ignorar erro caso não tenha CSS */} // warning 25/02/219
                    }

                    webResponse.Close();
                    readStream.Close();
                }

                if (data != "")
                {
                    //Create a byte array that will eventually hold our final PDF
                    Byte[] bytes;
                    ITextEvents ev = new ITextEvents(null, null);

                    //Gerar o PDF baseado no HTML
                    bytes = ev.ImprimirPDF(data, strCSS, "842, 595");

                    //Now we just need to do something with those bytes.
                    //Here I'm writing them to disk but if you were in ASP.Net you might Response.BinaryWrite() them.
                    //You could also write the bytes to a database in a varbinary() column (but please don't) or you
                    //could pass them to another function for further PDF processing.
                    System.IO.File.WriteAllBytes(caminho, bytes);

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        "Conversão concluída. Iniciando a criação do PDF em arquivo.\n" + caminho, request.CodigoSessao, request.CodigoProcesso);

                }

            }
            catch (Exception ex)
            {
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioConsolidadoPDF - Erro na requisição:" + ex.Message, request.CodigoSessao, request.CodigoProcesso);

                // Trata erro
                respostaRelatorioPdf.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioPdf;
        }


        private GerarRelatorioVendasPDFResponse gerarRelatorioVendasPDF(GerarRelatorioVendasPDFRequest request)
        {
            // Prepara resposta
            GerarRelatorioVendasPDFResponse respostaRelatorioPdf = new GerarRelatorioVendasPDFResponse();
            respostaRelatorioPdf.PreparaResposta(request);

            StreamWriter file = null;
            try
            {
                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda == "")
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioPdf.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    //respostaRelatorioExcel.Erro = "O diretório de exportação do relatório não está configurado.";
                    return respostaRelatorioPdf;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda.Length - 1) == @"\")
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda;
                    else
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda + "\\";
                }

                UsuarioInfo usuarioInfo = new UsuarioInfo();

                DateTime dataAtual = DateTime.Now;
                string nome = string.Empty;

                //Caso o codigo de sessao seja nulo, atribui um nome do usuario "Automatico", pois se trata da execução do processo
                //LiberarTransacoesEmAndamento executado quando o servidor de aplicação é reiniciado.
                if (request.CodigoSessao != null)
                {
                    // pega o usuario para recuperar o nome e colocar no arquivo gerado
                    usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                    if (usuarioInfo.Nome.Contains(" "))
                        nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                    else
                        nome = usuarioInfo.Nome;
                }
                else
                {
                    //Atribui o nome Automatico pois ainda não existe Sessão válida
                    nome = "Automatico";
                }


                string caminho = respostaRelatorioPdf.CaminhoArquivoGerado
                    //1248+ @"\" + nome + @"_RelVendas_" + dataAtual.Year.ToString("0000")
                    + nome + @"_RelVendas_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + (request.Request.TipoArquivo == null ? ".csv" : ".pdf");

                if (!System.IO.Directory.Exists(respostaRelatorioPdf.CaminhoArquivoGerado))
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioPdf.CaminhoArquivoGerado + "]";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioPdf.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioPdf;
                }

                string urlAddress = request.Request.Link;

                ServicePointManager.ServerCertificateValidationCallback =
                    new System.Net.Security.RemoteCertificateValidationCallback(
                        (object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors) =>
                        {
                            return true;
                        });

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                webRequest.Timeout = 1000 * 60 * 60 * 5; // 5 horas
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0";
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.AllowAutoRedirect = true;
                webRequest.ContentType = "application/x-www-form-urlencoded";

                //webRequest.Proxy = proxy;
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioVendasPDF - Efetuando requisição WEB: " + request.Request.Link, request.CodigoSessao, request.CodigoProcesso);

                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                //webRequest.BeginGetResponse(new AsyncCallback(gerarPDF), webRequest);
                string data = "";
                string strCSS = null;
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    //PdfDocument doc = converter.ConvertUrl(request.Request.Link);
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                       "Iniciando conversão do HTML para PDF. ", request.CodigoSessao, request.CodigoProcesso);

                    Stream receiveStream = webResponse.GetResponseStream();
                    StreamReader readStream = null;

                    if (webResponse.CharacterSet == null)
                    {
                        readStream = new StreamReader(receiveStream);
                    }
                    else
                    {
                        readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    }

                    data = readStream.ReadToEnd();
                    if (data != "")
                    {
                        // Recupera Estilos do CSS para aplicar no relatório PDF
                        urlAddress = urlAddress.Replace(".aspx", ".css");
                        int posIni = urlAddress.IndexOf("?");
                        urlAddress = urlAddress.Substring(0, posIni);

                        posIni = data.IndexOf("link href");
                        if (posIni > 0)
                        {
                            int posFim = data.IndexOf(".css", posIni) + 3;
                            string urlAddressCss = data.Substring(posIni + 11, posFim - posIni - 10);

                            posFim = urlAddress.LastIndexOf(@"/");
                            urlAddress = urlAddress.Substring(0, posFim + 1) + urlAddressCss;
                        }

                        try
                        {
                            webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                            webResponse = (HttpWebResponse)webRequest.GetResponse();

                            receiveStream = null;
                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                receiveStream = webResponse.GetResponseStream();
                                strCSS = new StreamReader(receiveStream, Encoding.UTF8).ReadToEnd();
                            }
                        }
                        catch (Exception) { } // catch (Exception ex) {/* ignorar erro caso não tenha CSS */} // warning 25/02/219
                    }

                    webResponse.Close();
                    readStream.Close();
                }

                if (data != "")
                {
                    //Create a byte array that will eventually hold our final PDF
                    Byte[] bytes;
                    ITextEvents ev = new ITextEvents(null, null);

                    //Gerar o PDF a partir do HTML
                    bytes = ev.ImprimirPDF(data, strCSS, "842, 595");

                    //Now we just need to do something with those bytes.
                    //Here I'm writing them to disk but if you were in ASP.Net you might Response.BinaryWrite() them.
                    //You could also write the bytes to a database in a varbinary() column (but please don't) or you
                    //could pass them to another function for further PDF processing.
                    System.IO.File.WriteAllBytes(caminho, bytes);

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        "Conversão concluída. Iniciando a criação do PDF em arquivo.\n" + caminho, request.CodigoSessao, request.CodigoProcesso);


                }
            }
            catch (Exception ex)
            {
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioVendasPDF - Erro na requisição:" + ex.Message, request.CodigoSessao, request.CodigoProcesso);

                // Trata erro
                respostaRelatorioPdf.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioPdf;

        }


        public GerarRelatorioConsolidadoExcelResponse GerarRelatorioConsolidadoExcel(GerarRelatorioConsolidadoExcelRequest request)
        {
            // prepara a resposta
            GerarRelatorioConsolidadoExcelResponse resposta = new GerarRelatorioConsolidadoExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioConsolidadoExcelInfo processoInfo =
                    new ProcessoRelatorioConsolidadoExcelInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioConsolidadoExcel(request);
            }

            // retorna a resposta
            return resposta;
        }
        /// <summary>
        /// Lista relatório consolidado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private GerarRelatorioConsolidadoExcelResponse gerarRelatorioConsolidadoExcel(GerarRelatorioConsolidadoExcelRequest request)
        {
            // Prepara resposta
            GerarRelatorioConsolidadoExcelResponse respostaRelatorioExcel = new GerarRelatorioConsolidadoExcelResponse();
            respostaRelatorioExcel.PreparaResposta(request);

            // monta a string para informar os filtros da consulta
            StringBuilder sb = new StringBuilder();
            if (request.Request.FiltroTipoConsolidado != null)
            {
                if (request.Request.FiltroTipoConsolidado == "V")
                    sb.Append("Vendas; ");
                else
                    sb.Append("Recebimento; ");
            }

            // ECOMMERCE - Fernando Bove - 20160106: Inicio
            if (request.Request.FiltroFinanceiroContabil != null)
            {
                if (request.Request.FiltroFinanceiroContabil == "C")
                    sb.Append("Contabil; ");
                else
                    sb.Append("Financeiro; ");
            }
            // ECOMMERCE - Fernando Bove - 20160106: Fim

            sb.Append("Quebra Favorecido = ");
            if (request.Request.FiltroQuebraFavorecido)
                sb.Append("Sim; ");
            else
                sb.Append("Nao; ");

            sb.Append("Quebra Referencia = ");
            if (request.Request.FiltroQuebraReferencia)
            {
                sb.Append("Sim; ");
            }
            else
                sb.Append("Nao; ");

            sb.Append("Data de Agendamento = ");
            if (request.Request.FiltroDataAgendamentoDe != null && request.Request.FiltroDataAgendamentoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltroDataAgendamentoDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Request.FiltroDataAgendamentoAte != null && request.Request.FiltroDataAgendamentoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltroDataAgendamentoAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Data de Processamento = ");
            if (request.Request.FiltroDataInicioMovimento != null && request.Request.FiltroDataInicioMovimento != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltroDataInicioMovimento).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Request.FiltroDataFimMovimento != null && request.Request.FiltroDataFimMovimento != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltroDataFimMovimento).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Favorecido = ");
            if (String.IsNullOrEmpty(request.Request.FiltroFavorecidoOriginal))
                sb.Append("Todos; ");
            else
            {
                StringBuilder sbFavorecido = new StringBuilder();
                string[] favorecidos = request.Request.FiltroFavorecidoOriginal.Split(',');
                foreach (string favorecido in favorecidos)
                {
                    // recupera o produto
                    FavorecidoInfo favorecidoInfo = PersistenciaHelper.Receber<FavorecidoInfo>(
                        request.CodigoSessao, favorecido);

                    if (favorecidoInfo != null)
                        sbFavorecido.Append(favorecidoInfo.NomeFavorecido + ", ");
                }
                string favorecidoRecuperados = sbFavorecido.ToString().Substring(0, sbFavorecido.ToString().LastIndexOf(","));
                sb.Append(favorecidoRecuperados + "; ");
            }

            sb.Append("Estabelecimento = ");
            if (request.Request.FiltroEstabelecimento != null)
                sb.Append(request.Request.FiltroDescricaoEstabelecimento + "; ");
            else
                sb.Append("Todos; ");

            if (request.Request.FiltroTipoConsolidado == "V")
            {
                sb.Append("Produtos = ");
                if (String.IsNullOrEmpty(request.Request.FiltroProduto))
                    sb.Append("Todos; ");
                else
                {
                    StringBuilder sbProduto = new StringBuilder();
                    string[] produtos = request.Request.FiltroProduto.Split(',');
                    foreach (string produto in produtos)
                    {
                        // recupera o produto
                        ProdutoInfo produtoInfo = PersistenciaHelper.Receber<ProdutoInfo>(
                            request.CodigoSessao, produto);

                        if (produtoInfo != null)
                            sbProduto.Append(produtoInfo.NomeProduto + ", ");
                    }

                    string produtoRecuperados = sbProduto.ToString().Substring(0, sbProduto.ToString().LastIndexOf(","));
                    sb.Append(produtoRecuperados + "; ");
                }
            }
            else if (request.Request.FiltroTipoConsolidado == "R")
            {
                sb.Append("Meio Pagamento = ");
                if (String.IsNullOrEmpty(request.Request.FiltroMeioPagamento))
                    sb.Append("Todos; ");
                else
                {
                    StringBuilder sbMeioPagamento = new StringBuilder();
                    string[] meioPagamentos = request.Request.MeioPagamento.Split(',');
                    foreach (string meioPagamento in meioPagamentos)
                    {
                        // recupera o produto
                        ListaItemInfo meioPagamentoInfo = PersistenciaHelper.Receber<ListaItemInfo>(
                            request.CodigoSessao, meioPagamento);

                        if (meioPagamentoInfo != null)
                            sbMeioPagamento.Append(meioPagamentoInfo.Descricao + ", ");
                    }

                    string meioPagamentoRecuperados = sbMeioPagamento.ToString().Substring(0, sbMeioPagamento.ToString().LastIndexOf(","));
                    sb.Append(meioPagamentoRecuperados + "; ");
                }
            }

            sb.Append("Tipo de Registro = ");
            if (String.IsNullOrEmpty(request.Request.FiltroCodigoTipoRegistro))
                sb.Append("Todos; ");
            else
            {
                StringBuilder sbRegsitro = new StringBuilder();
                string[] registros = request.Request.FiltroCodigoTipoRegistro.Split(',');
                foreach (string registro in registros)
                {
                    // recupera o produto
                    ConfiguracaoTipoRegistroInfo registroInfo = PersistenciaHelper.Receber<ConfiguracaoTipoRegistroInfo>(
                        request.CodigoSessao, registro);

                    if (registroInfo != null)
                        sbRegsitro.Append(registroInfo.TipoRegistro + ", ");
                }

                string registroRecuperados = sbRegsitro.ToString().Substring(0, sbRegsitro.ToString().LastIndexOf(","));
                sb.Append(registroRecuperados + "; ");
            }

            if (request.Request.FiltroReferencia != null)
            {
                sb.Append("Referencia = ");
                sb.Append(request.Request.FiltroReferencia);
                sb.Append(";");
            }
            else
                sb.Append("Referencia = Todos; ");

            sb.Append("layout Relatorio = ");
            if (request.Request.FiltroTipoConsolidado == "V")
            {
                sb.Append(request.Request.FiltroLayoutVenda == "T" ? "Totalizado por Produto; " : request.Request.FiltroLayoutVenda == "A" ? "Analitico por Data; " : "Sintetico por Data; ");
                sb.Append("Visualizar Qtde = ");
                if (request.Request.FiltroQtdeVenda)
                    sb.Append("Sim; ");
                else
                    sb.Append("Nao; ");
            }
            else
            {
                sb.Append(request.Request.FiltroLayoutRecebimento == "T" ? "Totalizado por Meio Pagamento; " : request.Request.FiltroLayoutRecebimento == "A" ? "Analitico por Data; " : request.Request.FiltroLayoutRecebimento == "S" ? "Sintetico por Data; " : "Totalizado envio CCI; ");
                sb.Append("Visualizar Qtde = ");
                if (request.Request.FiltroQtdeRecebimento)
                    sb.Append("Sim; ");
                else
                    sb.Append("Nao; ");
            }

            if (request.Request.FiltroCodigoStatusCCI != null)
            {
                sb.Append("Status Envio CCI = ");
                sb.Append(request.Request.FiltroCodigoStatusCCI == "S" ? "Enviadas; " : "Nao enviadas; ");
            }
            else
                sb.Append("Status Envio CCI = Todos; ");

            StreamWriter file = null;
            try
            {
                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado == "")
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioExcel.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);

                    //respostaRelatorioExcel.Erro = "O diretório de exportação do relatório não está configurado.";
                    return respostaRelatorioExcel;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado.Length - 1) == @"\")
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado;
                    else
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado + "\\";
                }

                // pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                DateTime dataAtual = DateTime.Now;

                string nome = "";
                if (usuarioInfo.Nome.Contains(" "))
                    nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                else
                    nome = usuarioInfo.Nome;

                string caminho = respostaRelatorioExcel.CaminhoArquivoGerado
                    //scf1248+ @"\" + nome + @"_RelConsolidado_" + dataAtual.Year.ToString("0000")
                    + nome + @"_RelConsolidado_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + ".csv";

                if (!System.IO.Directory.Exists(respostaRelatorioExcel.CaminhoArquivoGerado))
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioExcel.CaminhoArquivoGerado + "]";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioExcel.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioExcel;
                }
                else
                    file = new StreamWriter(caminho, true, Encoding.Default);

                // adiciona os filtros
                file.WriteLine(sb.ToString());


                // Pede o relatório passando o request com os filtros do relatório
                ListarRelatorioConsolidadoResponse resposta =
                    Mensageria.Processar<ListarRelatorioConsolidadoResponse>(
                        new ListarRelatorioConsolidadoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            FiltroTipoConsolidado = request.Request.FiltroTipoConsolidado,
                            FiltroQuebraFavorecido = request.Request.FiltroQuebraFavorecido,
                            FiltroProduto = request.Request.FiltroProduto,
                            FiltroFavorecidoOriginal = request.Request.FiltroFavorecidoOriginal,
                            FiltroCodigoTipoRegistro = request.Request.FiltroCodigoTipoRegistro,
                            FiltroDescricaoTipoRegistro = request.Request.FiltroDescricaoTipoRegistro,
                            FiltroEstabelecimento = request.Request.FiltroEstabelecimento,
                            FiltroMeioPagamento = request.Request.FiltroMeioPagamento,
                            FiltroLayoutVenda = request.Request.FiltroLayoutVenda,
                            FiltroLayoutRecebimento = request.Request.FiltroLayoutRecebimento,
                            FiltroQtdeVenda = request.Request.FiltroQtdeVenda,
                            FiltroQtdeRecebimento = request.Request.FiltroQtdeRecebimento,
                            FiltroDataAgendamentoDe = request.Request.FiltroDataAgendamentoDe,
                            FiltroDataAgendamentoAte = request.Request.FiltroDataAgendamentoAte,
                            FiltroDataInicioMovimento = request.Request.FiltroDataInicioMovimento,
                            FiltroDataFimMovimento = request.Request.FiltroDataFimMovimento,
                            FiltroCodigoStatusCCI = request.Request.FiltroCodigoStatusCCI,
                            // ECOMMERCE - Fernando Bove - 20160105
                            FiltroFinanceiroContabil = request.Request.FiltroFinanceiroContabil,
                            VerificarQuantidadeLinhas = request.Request.VerificarQuantidadeLinhas,
                            FiltroQuebraReferencia = request.Request.FiltroQuebraReferencia,
                            FiltroReferencia = request.Request.FiltroReferencia,
                            TipoRelatorio = "Excel"
                        });

                String mensagemAtacadao = "Filtro Período Dt Agendamento informado, valores nao apresentados para Atacadão";
                if (request.Request.FiltroDataAgendamentoDe != null || request.Request.FiltroDataAgendamentoAte != null)
                {
                    if (resposta.ListaConfigTipoRegistro.Count > 0)
                    {

                        String reg = "";
                        foreach (ConfiguracaoTipoRegistroInfo config in resposta.ListaConfigTipoRegistro)
                            reg = reg + config.TipoRegistro + ", ";

                        reg = reg.Trim();
                        reg = reg.Substring(0, reg.Length - 1);

                        file.WriteLine(mensagemAtacadao + " " + "e Tipo Registro [" + reg + "].");

                    }
                    else
                    {
                        file.WriteLine(mensagemAtacadao + ".");
                    }
                }

                // Informa os filtros da consulta
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    sb.ToString().Replace(";", ","), request.CodigoSessao, request.CodigoProcesso);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Arquivo criado: " + caminho, request.CodigoSessao, request.CodigoProcesso);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Realizando consulta para o relatório consolidado", request.CodigoSessao, request.CodigoProcesso);


                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //new SalvarLogRequest()
                //{
                //    CodigoUsuario = "",
                //    Descricao = "APP - INI - ListarRelatorioConsolidado - Iniciando geração do Excel.",
                //    TipoOrigem = "APP",
                //    CodigoOrigem = resposta.CodLog
                //});

                String dados = "Favorecido;Data Proc.;";
                if (request.Request.FiltroTipoConsolidado.Equals("V"))
                {
                    dados = dados + "Produto;Vlr.Bruto CV;Vlr.Bruto AV;Vlr.Bruto AJ;Vlr.Comissão CV;Vlr.Comissão AV;Vlr.Comissão AJ;Total Liquido;";
                    if (request.Request.FiltroQtdeVenda)
                        dados = dados + "Qtde CV;Qtde AV;Qtde AJ;";
                }
                else
                {
                    if (request.Request.FiltroLayoutRecebimento != "C")
                        dados = dados + "Meio Pagamento;";
                    dados = dados + "Valor CP;Valor AP;Valor AJ;Valor Total;";
                    if (request.Request.FiltroQtdeRecebimento)
                        dados = dados + "Qtde MP(CP);Qtde MP(AP);Qtde MP(AJ);";
                }
                if (request.Request.FiltroQuebraReferencia)
                {
                    dados = dados + "Referência";
                }
                file.WriteLine(dados);

                // Quantidade de linhas para log
                int quantidadeLog = 0;

                foreach (RelatorioConsolidadoInfo relatorioConsolidado in resposta.Resultado)
                {
                    NumberFormatInfo nfi = new NumberFormatInfo();
                    nfi.NumberDecimalDigits = 2;
                    nfi.NumberDecimalSeparator = ",";
                    nfi.NumberGroupSeparator = ".";

                    if (respostaRelatorioExcel.QuantidadeLinhasGeradas == 0)
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                            "Consulta finalizada", request.CodigoSessao, request.CodigoProcesso);

                    if (request.Request.FiltroQuebraFavorecido)
                        dados = relatorioConsolidado.NomeFavorecido + ";";
                    else
                        dados = " ;";

                    if (request.Request.FiltroLayoutVenda.Equals("A") || request.Request.FiltroLayoutVenda.Equals("S")
                        || request.Request.FiltroLayoutRecebimento.Equals("A") || request.Request.FiltroLayoutRecebimento.Equals("S"))
                        dados = dados + String.Format("{0:dd/MM/yyyy}", relatorioConsolidado.DataProcessamento) + ";";
                    else
                        dados = dados + " ;";

                    if (request.Request.FiltroTipoConsolidado.Equals("V"))
                    {
                        dados = dados + relatorioConsolidado.NomeProduto + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorBrutoCV)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorBrutoAV)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorBrutoAJ)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorComissaoCV)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorComissaoAV)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorComissaoAJ)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.TotalLiquido)) + ";";
                        if (request.Request.FiltroQuebraReferencia)
                        {
                            dados = dados + "=\"" + String.Format(new CultureInfo("PT-BR"), "{0:D3}", relatorioConsolidado.CodigoReferencia) + "\";";
                        }
                        if (request.Request.FiltroQtdeVenda)
                        {
                            dados = dados + relatorioConsolidado.QtdeCV + ";"
                                + relatorioConsolidado.QtdeAV + ";"
                                + relatorioConsolidado.QtdeAJ + ";";
                        }
                    }
                    else
                    {
                        if (request.Request.FiltroLayoutRecebimento != "C")
                            dados = dados + relatorioConsolidado.MeioPagamento + ";";
                        dados = dados
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorCP) * (-1)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorAP) * (-1)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorAJ) * (-1)) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(relatorioConsolidado.ValorTotal) * (-1)) + ";";

                        if (request.Request.FiltroQtdeRecebimento)
                        {
                            dados = dados + relatorioConsolidado.QtdeCP + ";"
                                + relatorioConsolidado.QtdeAP + ";"
                                + relatorioConsolidado.QtdeAJ + ";";
                        }
                        if (request.Request.FiltroQuebraReferencia)
                        {
                            dados = dados + "=\"" + String.Format(new CultureInfo("PT-BR"), "{0:D3}", relatorioConsolidado.CodigoReferencia) + "\";";
                        }
                    }
                    file.WriteLine(dados);

                    // Atualiza a quantidade de linhas geradas
                    respostaRelatorioExcel.QuantidadeLinhasGeradas++;
                    quantidadeLog++;

                    // Sinaliza
                    if (quantidadeLog >= 15000)
                    {
                        // Sinaliza
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                            respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                        // Zera linhas do log
                        quantidadeLog = 0;
                    }
                }


                //1353
                //Mensageria.Processar<SalvarLogResponse>(
                //new SalvarLogRequest()
                //{
                //    CodigoUsuario = "",
                //    Descricao = "APP - FIM - ListarRelatorioConsolidado - Fim da geração do Excel.",
                //    TipoOrigem = "APP",
                //    CodigoOrigem = resposta.CodLog
                //});
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Total de linhas geradas: " + respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);
            }
            catch (Exception ex)
            {
                // Trata erro
                respostaRelatorioExcel.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioExcel;
        }

        public GerarRelatorioVendaExcelResponse GerarRelatorioVendaExcel(GerarRelatorioVendaExcelRequest request)
        {
            // Prepara resposta
            GerarRelatorioVendaExcelResponse resposta = new GerarRelatorioVendaExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioVendasExcelInfo processoInfo =
                    new ProcessoRelatorioVendasExcelInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioVendasExcel(request);
            }

            // retorna a resposta
            return resposta;
        }

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private GerarRelatorioVendaExcelResponse gerarRelatorioVendasExcel(GerarRelatorioVendaExcelRequest request)
        {
            // Prepara resposta
            GerarRelatorioVendaExcelResponse respostaRelatorioExcel = new GerarRelatorioVendaExcelResponse();
            respostaRelatorioExcel.PreparaResposta(request);

            // Recupera o grupo
            EmpresaGrupoInfo empresaGrupo = PersistenciaHelper.Receber<EmpresaGrupoInfo>(
                request.CodigoSessao, request.Request.FiltroEmpresaGrupo);

            // recupera o subGrupo
            EmpresaSubGrupoInfo empresaSubGrupo = PersistenciaHelper.Receber<EmpresaSubGrupoInfo>(
                request.CodigoSessao, request.Request.FiltroEmpresaSubgrupo);

            // recupera o favorecido
            FavorecidoInfo favorecido = PersistenciaHelper.Receber<FavorecidoInfo>(
                request.CodigoSessao, request.Request.FiltroFavorecido);

            // recupera o estabelecimento
            EstabelecimentoInfo estabelecimento = PersistenciaHelper.Receber<EstabelecimentoInfo>(
                request.CodigoSessao, request.Request.FiltroEstabelecimento);



            // monta a string para informar os filtros da consulta
            StringBuilder sb = new StringBuilder();
            sb.Append("Data da Venda = ");
            if (request.Request.FiltroDataInicioVendas != null && request.Request.FiltroDataInicioVendas != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltroDataInicioVendas).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Request.FiltroDataFimVendas != null && request.Request.FiltroDataFimVendas != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltroDataFimVendas).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");
            sb.Append("Data de Processamento = ");
            if (request.Request.FiltroDataMovimentoInicio != null && request.Request.FiltroDataMovimentoInicio != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltroDataMovimentoInicio).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Request.FiltroDataMovimentoFim != null && request.Request.FiltroDataMovimentoFim != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltroDataMovimentoFim).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");
            sb.Append("Data de Retorno CCI = ");
            if (request.Request.FiltrotxtDataRetornoCCIInicio != null && request.Request.FiltrotxtDataRetornoCCIInicio != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltrotxtDataRetornoCCIInicio).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Request.FiltrotxtDataRetornoCCIFim != null && request.Request.FiltrotxtDataRetornoCCIFim != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Request.FiltrotxtDataRetornoCCIFim).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");
            sb.Append("Grupo = ");
            if (empresaGrupo != null)
                sb.Append(empresaGrupo.NomeEmpresaGrupo + "; ");
            else
                sb.Append("Todos; ");
            sb.Append("SubGrupo = ");
            if (empresaSubGrupo != null)
                sb.Append(empresaSubGrupo.NomeEmpresaSubGrupo + "; ");
            else
                sb.Append("Todos; ");

            sb.Append("Estabelecimento = ");
            if (estabelecimento != null)
                sb.Append(estabelecimento.RazaoSocial + "; ");
            else
                sb.Append("Todos; ");

            sb.Append("Favorecido = ");
            if (favorecido != null)
                sb.Append(favorecido.NomeFavorecido + "; ");
            else
                sb.Append("Todos; ");

            //ClockWork Rogerio : Inicio
            sb.Append("Referencia = ");
            if (request.Request.FiltroReferencia != null)
            {
                StringBuilder sbReferencia = new StringBuilder();
                string[] referencias = request.Request.FiltroReferencia.Split(',');
                foreach (string referencia in referencias)
                {
                    List<CondicaoInfo> condicoesRef = new List<CondicaoInfo>();

                    condicoesRef.Add(
                    new CondicaoInfo(
                        "Valor", CondicaoTipoEnum.Igual, referencia));

                    // recupera a referencia
                    List<ListaItemInfo> referenciaInfo =
                        PersistenciaHelper.Listar<ListaItemInfo>(
                            null, condicoesRef);

                    foreach (ListaItemInfo listaItem in referenciaInfo)
                    {
                        sbReferencia.Append(listaItem.Descricao + ", ");
                    }
                }

                string referenciaRecuperados = sbReferencia.ToString().Substring(0, sbReferencia.ToString().LastIndexOf(","));
                sb.Append(referenciaRecuperados + "; ");
            }
            else
                sb.Append("Todos; ");

            // consolidacao
            sb.Append("Consolidacao = ");
            if (request.Request.FiltroConsolidaGrupoSubGrupoProduto == true)
                sb.Append("Grupo + SubGrupo + Produto");
            if (request.Request.FiltroConsolidaDataVenda == true)
                sb.Append(" + Data da venda");
            if (request.Request.FiltroConsolidaDataProcessamento == true)
                sb.Append(" + Data de processamento");
            if (request.Request.FiltroConsolidaBancoAgenciaConta == true)
                sb.Append(" + Banco + Agencia + C/C");
            if (request.Request.FiltroConsolidaEstabelecimento == true)
                sb.Append(" + Estabelecimento");
            if (request.Request.FiltroConsolidaReferencia == true) //ClockWork 
                sb.Append(" + Referencia");
            sb.Append("; ");

            // ECOMMERCE - Fernando Bove - 20160111: Inicio
            if (request.Request.FiltroFinanceiroContabil != null)
            {
                if (request.Request.FiltroFinanceiroContabil == "C")
                    sb.Append("Contabil; ");
                else
                    sb.Append("Financeiro; ");
            }
            // ECOMMERCE - Fernando Bove - 20160111: Fim

            sb.Append("Tipo de Registro = " + request.Request.FiltroTipoRegistro);

            //ClockWork Rogerio: Inicio



            //ClockWork Rogerio: Fim

            // Informa os filtros da consulta
            LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                sb.ToString(), request.CodigoSessao, request.CodigoProcesso);

            StreamWriter file = null;
            try
            {
                // Pede as configurações do sistema para saber em qual diretório salvar
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(
                        new ReceberConfiguracaoGeralRequest()
                        {
                        });

                // Monto e Crio o arquivo com o caminho da configuração concatenado com a data atual
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda == "")
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioExcel.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);

                    //respostaRelatorioExcel.Erro = "O diretório de exportação do relatório não está configurado.";
                    return respostaRelatorioExcel;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda.Length - 1) == @"\")
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda;
                    else
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda + "\\";
                }

                // pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);

                DateTime dataAtual = DateTime.Now;
                //string caminho = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioVenda
                //    + @"\RelatorioVendas_" + dataAtual.Year.ToString("0000")
                //    + "_" + dataAtual.Month.ToString("00") + "_" + dataAtual.Day.ToString("00")
                //    + "_" + dataAtual.Hour.ToString("00") + "_" + dataAtual.Minute.ToString("00")
                //    + "_" + dataAtual.Second.ToString("00") + ".csv";

                string nome = "";
                if (usuarioInfo.Nome.Contains(" "))
                    nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                else
                    nome = usuarioInfo.Nome;

                string caminho = respostaRelatorioExcel.CaminhoArquivoGerado
                    //scf1248+ @"\" + nome + @"_RelVendas_" + dataAtual.Year.ToString("0000")
                    + nome + @"_RelVendas_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + ".csv";

                //respostaRelatorioExcel.CaminhoArquivoGerado = caminho;

                if (!System.IO.Directory.Exists(respostaRelatorioExcel.CaminhoArquivoGerado))
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioExcel.CaminhoArquivoGerado + "]";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioExcel.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioExcel;
                }
                else
                    file = new StreamWriter(caminho, true, Encoding.Default);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Arquivo criado: " + caminho, request.CodigoSessao, request.CodigoProcesso);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Realizando consulta das vendas no banco de dados", request.CodigoSessao, request.CodigoProcesso);

                // pede o relatório
                ListarRelatorioConferenciaVendasResponse resposta =
                    Mensageria.Processar<ListarRelatorioConferenciaVendasResponse>(
                        new ListarRelatorioConferenciaVendasRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            FiltroDataFimVendas = request.Request.FiltroDataFimVendas,
                            FiltroDataInicioVendas = request.Request.FiltroDataInicioVendas,
                            FiltroDataMovimento = request.Request.FiltroDataMovimento,
                            FiltroDataMovimentoFim = request.Request.FiltroDataMovimentoFim,
                            FiltroDataMovimentoInicio = request.Request.FiltroDataMovimentoInicio,
                            FiltroDataVendas = request.Request.FiltroDataVendas,
                            FiltroEmpresaGrupo = request.Request.FiltroEmpresaGrupo,
                            FiltroEmpresaSubgrupo = request.Request.FiltroEmpresaSubgrupo,
                            FiltroEstabelecimento = request.Request.FiltroEstabelecimento,
                            FiltroFavorecido = request.Request.FiltroFavorecido,
                            FiltroReferencia = request.Request.FiltroReferencia, //ClockWork
                            VerificarQuantidadeLinhas = request.Request.VerificarQuantidadeLinhas,
                            FiltroTipoRegistro = request.Request.FiltroTipoRegistro,
                            FiltrotxtDataRetornoCCIInicio = request.Request.FiltrotxtDataRetornoCCIInicio,
                            FiltrotxtDataRetornoCCIFim = request.Request.FiltrotxtDataRetornoCCIFim,
                            FiltroConsolidaGrupoSubGrupoProduto = request.Request.FiltroConsolidaGrupoSubGrupoProduto,
                            FiltroConsolidaBancoAgenciaConta = request.Request.FiltroConsolidaBancoAgenciaConta,
                            FiltroConsolidaDataVenda = request.Request.FiltroConsolidaDataVenda,
                            FiltroConsolidaDataProcessamento = request.Request.FiltroConsolidaDataProcessamento,
                            FiltroConsolidaEstabelecimento = request.Request.FiltroConsolidaEstabelecimento,
                            FiltroConsolidaReferencia = request.Request.FiltroConsolidaReferencia, //ClockWork
                            // ECOMMERCE - Fernando Bove - 20160106
                            FiltroFinanceiroContabil = request.Request.FiltroFinanceiroContabil
                        }
                    );

                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Consulta das vendas finalizada", request.CodigoSessao, request.CodigoProcesso);

                // insere as condicoes
                file.WriteLine(sb.ToString());

                // cria o header e insere no arquivo
                String s = "Grupo;Subgrupo;Estabelecimento;Tipo Registro;Produto;Banco;Agência;Conta;Tot.Qtd Vendas;Tot.Bruto Venda;Tot. Comissão;Total Vendas;Total Cedido;Total Cedivel não Negociado;Total não Cediveis;Pendente;";

                if (request.Request.FiltroConsolidaDataProcessamento)
                    s = "Data Processamento;" + s;
                else
                    s = "Data Venda;" + s;

                if (request.Request.FiltroConsolidaReferencia)
                    s = s + "Referência";


                file.WriteLine(s);

                // Quantidade de linhas para log
                int quantidadeLog = 0;

                foreach (RelatorioConferenciaVendasInfo relatorioVendas in resposta.Resultado)
                {
                    // ==========================================================================================
                    // 1443 - INICIO
                    // Ocorreu de no campo CONTA existir o valor 1-9 e no excel ficou 01/09/2017
                    // Tratando os campos AGENCIA e CONTA que podem conter HIFEN para serem consideradas string
                    // ==========================================================================================
                    s = String.Format("{0:dd/MM/yyyy}", relatorioVendas.DataVenda) + ";"
                        + relatorioVendas.NomeEmpresaGrupo + ";"
                        + relatorioVendas.NomeEmpresaSubgrupo + ";"
                        + "=\"" + relatorioVendas.Estabelecimento.RazaoSocial + "\";"
                        + request.Request.FiltroTipoRegistro + ";"
                        + relatorioVendas.Produto.DescricaoProduto + ";"
                        + relatorioVendas.Estabelecimento.Banco + ";"
                        //1443 + relatorioVendas.Estabelecimento.Agencia + ";"
                        + "=\"" + relatorioVendas.Estabelecimento.Agencia + "\";" //1443 
                        //1443 + relatorioVendas.Estabelecimento.Conta + ";"
                        + "=\"" + relatorioVendas.Estabelecimento.Conta + "\";" //1443                        
                        + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVendas.Quantidade) + ";"
                        + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVendas.TotalBruto) + ";"
                        + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVendas.TotalComissao) + ";"
                        + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVendas.ValorLiquido) + ";"
                        + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVendas.TotalCedido) + ";"
                        + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVendas.TotalCedivelNaoNegociado) + ";"
                        + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVendas.TotalNaoCedido) + ";"
                        + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioVendas.TotalPendente) + ";"
                        + "=\"" + relatorioVendas.Referencia.Valor + "\";";
                    file.WriteLine(s);

                    // Atualiza a quantidade de linhas geradas
                    respostaRelatorioExcel.QuantidadeLinhasGeradas++;
                    quantidadeLog++;

                    // Sinaliza
                    if (quantidadeLog >= 15000)
                    {
                        // Sinaliza
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                            respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                        // Zera linhas do log
                        quantidadeLog = 0;
                    }
                }

                // Informa total de linhas
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Total de linhas geradas: " + respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);

            }
            catch (Exception ex)
            {
                // Trata erro
                respostaRelatorioExcel.ProcessarExcessao(ex);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioExcel;
        }

        public GerarRelatorioConciliacaoContabilExcelResponse GerarRelatorioConciliacaoContabilExcel(GerarRelatorioConciliacaoContabilExcelRequest request)
        {
            // prepara a resposta
            GerarRelatorioConciliacaoContabilExcelResponse resposta = new GerarRelatorioConciliacaoContabilExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioConciliacaoContabilExcelInfo processoInfo =
                    new ProcessoRelatorioConciliacaoContabilExcelInfo()
                    {
                        FiltroDataFimConciliacao = request.FiltroDataFimConciliacao,
                        FiltroDataInicioConciliacao = request.FiltroDataInicioConciliacao
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioConciliacaoContabilExcel(request);
            }

            // retorna a resposta
            return resposta;
        }

        private GerarRelatorioConciliacaoContabilExcelResponse gerarRelatorioConciliacaoContabilExcel(GerarRelatorioConciliacaoContabilExcelRequest request)
        {
            GerarRelatorioConciliacaoContabilExcelResponse response = new GerarRelatorioConciliacaoContabilExcelResponse();
            response.PreparaResposta(request);

            // Inicializa
            StreamWriter writer = null;
            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            // Bloco de controle
            try
            {
                // Pega informações de configuração
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(
                        new ReceberConfiguracaoGeralRequest());

                // Declare o reader para que possa ser corretamente finalizado
                OracleConnection cn = Procedures.ReceberConexao();
                OracleRefCursor cursorSaldo = null;
                OracleRefCursor cursorLista = null;

                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioSaldoConciliacao == "")
                {
                    response.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    response.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    response.DescricaoResposta = response.Erro;

                    response.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = response.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        response.Erro, request.CodigoSessao, request.CodigoProcesso);

                    //respostaRelatorioExcel.Erro = "O diretório de exportação do relatório não está configurado.";
                    return response;
                }
                else
                {
                    // Verificar se o ultimo caracter é uma barra, se não for, coloco uma
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioSaldoConciliacao.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioSaldoConciliacao.Length - 1) == @"\")
                        response.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioSaldoConciliacao;
                    else
                        response.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioSaldoConciliacao + "\\";
                }

                // Monta o nome do arquivo
                DateTime dataAtual = DateTime.Now;
                string caminho = response.CaminhoArquivoGerado
                    + @"RelatorioConciliacaoContabil_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + dataAtual.Hour.ToString("00") + dataAtual.Minute.ToString("00")
                    + dataAtual.Second.ToString("00") + ".csv";

                // Cria o arquivo
                if (!System.IO.Directory.Exists(response.CaminhoArquivoGerado))
                {
                    response.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    response.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + response.CaminhoArquivoGerado + "]";
                    response.DescricaoResposta = response.Erro;

                    response.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = response.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        response.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return response;
                }
                else
                    writer = new StreamWriter(caminho, true, Encoding.Default);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Arquivo criado: " + caminho, request.CodigoSessao, request.CodigoProcesso);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Realizando consulta dos dados no banco de dados", request.CodigoSessao, request.CodigoProcesso);

                CultureInfo culture = new CultureInfo("pt-BR");

                PR_REL_SALDO_CONCILIACAO_L_Request reqRel = new PR_REL_SALDO_CONCILIACAO_L_Request();
                reqRel.FiltroDataConciliacaoInicio = request.FiltroDataInicioConciliacao;
                reqRel.FiltroDataConciliacaoFim = request.FiltroDataFimConciliacao + ts;
                Procedures.PR_REL_SALDO_CONCILIACAO_L(cn, reqRel, out cursorLista, out cursorSaldo);

                OracleDataReader drSaldo = cursorSaldo.GetDataReader();
                OracleDataReader dr = cursorLista.GetDataReader();

                string separador = ";";
                StringBuilder LinhaHeader = new StringBuilder();
                LinhaHeader.Append("CONCILIACAO Status;");
                LinhaHeader.Append("CONCILIACAO Saldo Acumulado;");
                LinhaHeader.Append("CONCILIACAO Valor;");
                LinhaHeader.Append("CONCILIACAO Data;");
                LinhaHeader.Append("TRANSACAO Data;");
                LinhaHeader.Append("TRANSACAO NSU HOST;");
                LinhaHeader.Append("TRANSACAO Codigo Autorizacao;");
                LinhaHeader.Append("TRANSACAO Valor;");
                LinhaHeader.Append("SITEF/REDE;");
                LinhaHeader.Append("Processo");

                writer.WriteLine(LinhaHeader.ToString());
                decimal saldoAux = 0;
                decimal saldoAnterior = 0;
                if (drSaldo.Read()) //1175
                {
                    saldoAnterior = decimal.Parse(drSaldo[0].ToString());
                }
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(("Período " + reqRel.FiltroDataConciliacaoInicio + " - " + reqRel.FiltroDataConciliacaoFim), request.CodigoSessao, request.CodigoProcesso);
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(("Calculando saldo anterior " + saldoAnterior.ToString()), request.CodigoSessao, request.CodigoProcesso);

                string txtSaldoAnterior = (saldoAnterior < 0) ? "(" + saldoAnterior.ToString("n") + ")" : saldoAnterior.ToString("n");
                //if (drSaldo.Read())
                //{
                //    saldoAnterior = decimal.Parse(drSaldo[0].ToString());
                writer.WriteLine("Saldo Anterior;" + txtSaldoAnterior + ";;;;;;;;");
                //}

                saldoAux = saldoAnterior;

                // Quantidade de linhas para log
                int quantidadeLog = 0;

                // Log
                bool primeiroLog = false;
                LogArquivo.LogTag("log", false, "GerarRelatorioConciliacaoContabilExcel:Antes Primeira Linha");

                while (dr.Read())
                {
                    // Log
                    if (!primeiroLog)
                    {
                        LogArquivo.LogTag("log", false, "GerarRelatorioConciliacaoContabilExcel:Depois Primeira Linha");
                        primeiroLog = true;
                    }

                    decimal valorConciliacao = (decimal)dr["valor_conciliacao"];

                    StringBuilder sbItens = new StringBuilder();
                    sbItens.Append(((TransacaoStatusConciliacaoEnum)int.Parse(dr["status_conciliacao"].ToString())).ToString());
                    sbItens.Append(separador);
                    sbItens.Append(saldoAux < 0 ? "(" + (saldoAux * -1).ToString("n") + ")" : saldoAux.ToString("n"));
                    sbItens.Append(separador);
                    sbItens.Append(valorConciliacao < 0 ? "(" + (valorConciliacao * -1).ToString("n") + ")" : valorConciliacao.ToString("n"));
                    sbItens.Append(separador);
                    sbItens.Append(Convert.IsDBNull(dr["data_conciliacao"]) ? "" : Convert.ToDateTime(dr["data_conciliacao"].ToString()).ToString("dd/MM/yyyy"));
                    sbItens.Append(separador);
                    sbItens.Append(Convert.ToDateTime(dr["data_transacao"].ToString()).ToString("dd/MM/yyyy"));
                    sbItens.Append(separador);
                    sbItens.Append(dr["nsu_host"].ToString());
                    sbItens.Append(separador);
                    sbItens.Append(dr["codigo_autorizacao"].ToString());
                    sbItens.Append(separador);
                    sbItens.Append(decimal.Parse(dr["valor_venda"].ToString()).ToString("n"));
                    sbItens.Append(separador);
                    sbItens.Append(dr["SITEF_REDE"].ToString());
                    sbItens.Append(separador);
                    sbItens.Append(dr["codigo_processo"].ToString());
                    saldoAux += valorConciliacao;
                    writer.WriteLine(sbItens.ToString());

                    // atualiza quantida de linhas
                    response.QuantidadeLinhas++;
                    quantidadeLog++;

                    // Sinaliza
                    if (quantidadeLog >= 15000)
                    {
                        // Sinaliza
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                            response.QuantidadeLinhas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                        // Zera linhas do log
                        quantidadeLog = 0;
                    }
                }

                string txtSaldoFinal = (saldoAux < 0) ? "(" + (saldoAux * -1).ToString("n") + ")" : saldoAux.ToString("n");

                writer.WriteLine("Saldo Atual;" + txtSaldoFinal + ";;;;;;;;");

                // Informa total de linhas
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Total de linhas geradas: " + response.QuantidadeLinhas.ToString(), request.CodigoSessao, request.CodigoProcesso);
            }
            catch (Exception ex)
            {
                // Trata erro
                response.ProcessarExcessao(ex);
            }
            finally
            {
                // Finaliza o arquivo
                if (writer != null)
                    writer.Close();
            }

            return response;
        }

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GerarRelatorioTransacaoExcelResponse GerarRelatorioTransacaoExcel(GerarRelatorioTransacaoExcelRequest request)
        {
            // Prepara resposta
            GerarRelatorioTransacaoExcelResponse resposta = new GerarRelatorioTransacaoExcelResponse();
            resposta.PreparaResposta(request);

            // Se não passou código de processo, gera processo e pede execucao
            if (request.CodigoProcesso == null)
            {
                // Cria o processo do cessao
                ProcessoRelatorioTransacaoExcelInfo processoInfo =
                    new ProcessoRelatorioTransacaoExcelInfo()
                    {
                        Param = request.Param
                    };

                // Executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // Informa código do processo na resposta
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                // Executa
                resposta = gerarRelatorioTransacaoExcel(request);
            }

            // Retorna
            return resposta;
        }

        public GerarRelatorioLogExcelResponse GerarRelatorioLogExcel(GerarRelatorioLogExcelRequest request)
        {
            // prepara a resposta
            GerarRelatorioLogExcelResponse resposta = new GerarRelatorioLogExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioLogExcelInfo processoInfo =
                    new ProcessoRelatorioLogExcelInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioLogExcel(request);
            }

            // retorna a resposta
            return resposta;
        }
        /// <summary>
        /// Lista relatório consolidado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private GerarRelatorioLogExcelResponse gerarRelatorioLogExcel(GerarRelatorioLogExcelRequest request)
        {
            // Prepara resposta
            GerarRelatorioLogExcelResponse respostaRelatorioExcel = new GerarRelatorioLogExcelResponse();
            respostaRelatorioExcel.PreparaResposta(request);

            StreamWriter file = null;
            try
            {
                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado == "")
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioExcel.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);

                    //respostaRelatorioExcel.Erro = "O diretório de exportação do relatório não está configurado.";
                    return respostaRelatorioExcel;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado.Length - 1) == @"\")
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado;
                    else
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioConsolidado + "\\";
                }

                // pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                DateTime dataAtual = DateTime.Now;

                string nome = "";
                if (usuarioInfo.Nome.Contains(" "))
                    nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                else
                    nome = usuarioInfo.Nome;

                string caminho = respostaRelatorioExcel.CaminhoArquivoGerado
                    //scf1248+ @"\" + nome + @"_RelLog_" + dataAtual.Year.ToString("0000")
                    + nome + @"_RelLog_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + ".csv";

                if (!System.IO.Directory.Exists(respostaRelatorioExcel.CaminhoArquivoGerado))
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioExcel.CaminhoArquivoGerado + "]";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioExcel.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioExcel;
                }
                else
                    file = new StreamWriter(caminho, true, Encoding.Default);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Arquivo criado: " + caminho, request.CodigoSessao, request.CodigoProcesso);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Realizando consulta para o relatório de log", request.CodigoSessao, request.CodigoProcesso);

                // Pede o relatório passando o request com os filtros do relatório
                ListarLogResponse resposta =
                    Mensageria.Processar<ListarLogResponse>(
                        new ListarLogRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            FiltroCodigoOrigem = request.Request.FiltroCodigoOrigem,
                            FiltroCodigoUsuario = request.Request.FiltroCodigoUsuario,
                            FiltroDataFim = request.Request.FiltroDataFim,
                            FiltroDataInicio = request.Request.FiltroDataInicio,
                            FiltroTipoLog = request.Request.FiltroTipoLog,
                            FiltroTipoOrigem = request.Request.FiltroTipoOrigem,
                        });

                String dados = "Codigo Log;Data Log;Data BD Log;Codigo Usuario;Descricao Log;Tipo Log;Tipo Origem; Codigo origem;";
                file.WriteLine(dados);

                // Quantidade de linhas para log
                int quantidadeLog = 0;

                foreach (LogResumoInfo relatorioLog in resposta.Resultado)
                {
                    if (respostaRelatorioExcel.QuantidadeLinhasGeradas == 0)
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                            "Consulta finalizada", request.CodigoSessao, request.CodigoProcesso);

                    dados = relatorioLog.CodigoLog + ";";
                    dados = dados + relatorioLog.DataLog + ";";
                    dados = dados + relatorioLog.DataBDLog + ";";
                    dados = dados + relatorioLog.CodigoUsuario + ";";
                    dados = dados + retiraAcentos(relatorioLog.Descricao) + ";";
                    dados = dados + relatorioLog.TipoLog + ";";
                    dados = dados + relatorioLog.TipoOrigem + ";";
                    dados = dados + relatorioLog.CodigoOrigem + ";";

                    file.WriteLine(dados);

                    // Atualiza a quantidade de linhas geradas
                    respostaRelatorioExcel.QuantidadeLinhasGeradas++;
                    quantidadeLog++;

                    // Sinaliza
                    if (quantidadeLog >= 15000)
                    {
                        // Sinaliza
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                            respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                        // Zera linhas do log
                        quantidadeLog = 0;
                    }
                }

                //Informa total de linhas
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Total de linhas geradas: " + respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);
            }
            catch (Exception ex)
            {
                // Trata erro
                respostaRelatorioExcel.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioExcel;
        }

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private GerarRelatorioTransacaoExcelResponse gerarRelatorioTransacaoExcel(GerarRelatorioTransacaoExcelRequest request)
        {
            // Prepara resposta
            GerarRelatorioTransacaoExcelResponse resposta = new GerarRelatorioTransacaoExcelResponse();
            resposta.PreparaResposta(request);

            StringBuilder sb = new StringBuilder();

            sb.Append("Origem = ");
            if (!String.IsNullOrEmpty(request.Param.TipoArquivo))
                sb.Append(request.Param.TipoArquivo + "; ");
            else
                sb.Append("Todos; ");
            sb.Append("Grupo = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoEmpresaGrupo))
            {
                EmpresaGrupoInfo grupo = PersistenciaHelper.Receber<EmpresaGrupoInfo>(
                    request.CodigoSessao, request.Param.CodigoEmpresaGrupo);
                if (grupo != null)
                    sb.Append(grupo.NomeEmpresaGrupo + "; ");
            }
            else
                sb.Append("Todos; ");

            sb.Append("SubGrupo = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoEmpresaSubGrupo))
            {
                EmpresaSubGrupoInfo subGrupo = PersistenciaHelper.Receber<EmpresaSubGrupoInfo>(
                    request.CodigoSessao, request.Param.CodigoEmpresaSubGrupo);
                if (subGrupo != null)
                    sb.Append(subGrupo.NomeEmpresaSubGrupo + "; ");
            }
            else
                sb.Append("Todos; ");

            sb.Append("Estabelecimento = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoEstabelecimento))
            {
                EstabelecimentoInfo estabelecimento = PersistenciaHelper.Receber<EstabelecimentoInfo>(
                    request.CodigoSessao, request.Param.CodigoEstabelecimento);
                if (estabelecimento != null)
                    sb.Append(estabelecimento.RazaoSocial + "; ");
            }
            else
                sb.Append("Todos; ");


            //30/11/2018 - Adição dos Filtros Restantes da tela
            sb.Append("Número do Pagamento = ");
            if (!String.IsNullOrEmpty(request.Param.NumeroPagamento))
            {
                sb.Append(request.Param.NumeroPagamento + "; ");
            }
            else
            {
                sb.Append("Qualquer ;");
            }

            sb.Append("Meio de Captura = ");
            if (!String.IsNullOrEmpty(request.Param.MeioCaptura))
            {
                sb.Append(request.Param.MeioCaptura + " ;");
            }
            else
                sb.Append("Todos;");

            sb.Append("Conta Cliente = ");
            if (!String.IsNullOrEmpty(request.Param.ContaCliente)) //1438
                sb.Append(request.Param.ContaCliente + "; ");
            else
            {
                sb.Append("Qualquer;");
            }

            sb.Append("Status Validação = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoStatusValidacao))
            {
                switch (request.Param.CodigoStatusRetorno)
                {
                    case "0":
                        sb.Append("Pendente Validacao; ");
                        break;

                    case "1":
                        sb.Append("Valido; ");
                        break;

                    case "2":
                        sb.Append("Invalido; ");
                        break;
                }
            }
            else
                sb.Append("Todos; ");


            

            sb.Append("Favorecido = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoFavorecido))
            {
                FavorecidoInfo favorecido = PersistenciaHelper.Receber<FavorecidoInfo>(
                    request.CodigoSessao, request.Param.CodigoFavorecido);
                if (favorecido != null)
                    sb.Append(favorecido.NomeFavorecido + "; ");
            }
            else
                sb.Append("Todos; ");

            sb.Append("Produto = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoProduto))
            {
                ProdutoInfo produto = PersistenciaHelper.Receber<ProdutoInfo>(
                    request.CodigoSessao, request.Param.CodigoProduto);
                if (produto != null)
                    sb.Append(produto.NomeProduto + "; ");
            }
            else
                sb.Append("Todos; ");

            sb.Append("Plano = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoPlano))
            {
                PlanoInfo plano = PersistenciaHelper.Receber<PlanoInfo>(
                    request.CodigoSessao, request.Param.CodigoPlano);
                if (plano != null)
                    sb.Append(plano.CodigoPlanoTSYS + "; ");
            }
            else
                sb.Append("Todos; ");


            sb.Append("Tipo de Registro = ");
            if (!String.IsNullOrEmpty(request.Param.TipoRegistro))
                sb.Append(request.Param.TipoRegistro + "; ");
            else
                sb.Append("Todos; ");



            sb.Append("Data de Transação = ");
            if (request.Param.DataTransacaoDe != null && request.Param.DataTransacaoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataTransacaoDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Param.DataTransacaoAte != null && request.Param.DataTransacaoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataTransacaoAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Data de Vencimento = ");
            if (request.Param.DataVencimentoDe != null && request.Param.DataVencimentoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataVencimentoDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Param.DataVencimentoAte != null && request.Param.DataVencimentoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataVencimentoAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Data de Agendamento = ");
            if (request.Param.DataAgendamentoDe != null && request.Param.DataAgendamentoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataAgendamentoDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Param.DataAgendamentoAte != null && request.Param.DataAgendamentoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataAgendamentoAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Data do Envio para Matera = ");
            if (request.Param.DataEnvioMateraDe != null)
                sb.Append(Convert.ToDateTime(request.Param.DataEnvioMateraDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Param.DataEnvioMateraAte != null)
                sb.Append(Convert.ToDateTime(request.Param.DataEnvioMateraAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Data de Liquidação = ");
            if (request.Param.DataLiquidacaoDe != null && request.Param.DataLiquidacaoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataLiquidacaoDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Param.DataLiquidacaoAte != null && request.Param.DataLiquidacaoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataLiquidacaoAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Data de Processamento = ");
            if (request.Param.DataMovimentoDe != null && request.Param.DataMovimentoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataMovimentoDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Param.DataMovimentoAte != null && request.Param.DataMovimentoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataMovimentoAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Data de Retorno CCI = ");
            if (request.Param.DataRetornoCessaoDe != null && request.Param.DataRetornoCessaoDe != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataRetornoCessaoDe).ToString("dd/MM/yyyy") + " - ");
            else
                sb.Append("Qualquer - ");
            if (request.Param.DataRetornoCessaoAte != null && request.Param.DataRetornoCessaoAte != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(request.Param.DataRetornoCessaoAte).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            sb.Append("Status Retorno = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoStatusRetorno))
            {
                switch (request.Param.CodigoStatusRetorno)
                {
                    case "1":
                        sb.Append("Retornado OK; ");
                        break;

                    case "0":
                        sb.Append("Transação não Retornada; ");
                        break;

                    case "3":
                        sb.Append("Produto Não Cedível; ");
                        break;

                    case "4":
                        sb.Append("Transação já Liquidada; ");
                        break;

                    case "2":
                        sb.Append("Retorno Não Identificado; ");
                        break;
                }
            }
            else
                sb.Append("Todos; ");

            sb.Append("Status Pagamento = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoStatusPagamento))
            {
                switch (request.Param.CodigoStatusPagamento)
                {
                    case "0":
                        sb.Append("Pendente; ");
                        break;

                    case "1":
                        sb.Append("Efetuado; ");
                        break;

                    case "9":
                        sb.Append("Cancelado; ");
                        break;
                }
            }
            else
                sb.Append("Todos; ");

            sb.Append("Status Conciliação = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoStatusConciliacao))
            {
                switch (request.Param.CodigoStatusConciliacao)
                {
                    case "1":
                        sb.Append("Conciliado; ");
                        break;

                    case "2":
                        sb.Append("Não Conciliado; ");
                        break;
                }
            }
            else
                sb.Append("Todos; ");

            // ECOMMERCE - Fernando Bove - 20160106: Inicio
            sb.Append("NSU Host = ");
            if (request.Param.FiltroNSUHost != null)
                sb.Append(request.Param.FiltroNSUHost + "; ");
            else
                sb.Append("Todos; ");

            sb.Append("Financ./Contabil = ");
            if (request.Param.FiltroFinanceiroContabil != null)
            {
                if (request.Param.FiltroFinanceiroContabil == "C")
                    sb.Append("Contabil; ");
                else
                    sb.Append("Financeiro; ");
            }
            else
                sb.Append("Todos; ");
            // ECOMMERCE - Fernando Bove - 20160106: Fim

            // ClockWork - Marcos Matsuoka - inicio
            //1312 inibido
            //sb.Append("Referencias = ");
            //if (String.IsNullOrEmpty(request.Param.FiltroReferencia))
            //    sb.Append("Todos; ");
            //else
            //1312
            sb.Append("Referencias = ");
            if (String.IsNullOrEmpty(request.Param.FiltroReferenciaDescricao))
                sb.Append("Todos; ");
            else
            {

                StringBuilder sbReferencia = new StringBuilder();
                //1312 string[] referencias = request.Param.FiltroReferencia.Split(',');
                string[] referencias = request.Param.FiltroReferenciaCodigo.Split(','); //1312
                foreach (string referencia in referencias)
                {
                    List<CondicaoInfo> condicoesRef = new List<CondicaoInfo>();

                    condicoesRef.Add(
                    new CondicaoInfo(
                        "Valor", CondicaoTipoEnum.Igual, referencia));

                    // recupera a referencia
                    List<ListaItemInfo> referenciaInfo =
                        PersistenciaHelper.Listar<ListaItemInfo>(
                            null, condicoesRef);

                    foreach (ListaItemInfo listaItem in referenciaInfo)
                    {
                        sbReferencia.Append(listaItem.Descricao + ", ");
                    }
                }

                string referenciaRecuperados = sbReferencia.ToString().Substring(0, sbReferencia.ToString().LastIndexOf(","));
                sb.Append(referenciaRecuperados + "; ");
            }

            // 30/11/2018 - Adicionando Filtro Faltante
            sb.Append("Status Transação = ");
            if (!String.IsNullOrEmpty(request.Param.CodigoStatusCancelamentoOnLine))
            {
                StringBuilder sbTransacao = new StringBuilder();
                string[] descTransacoes = request.Param.DescricaoStatusTransacaoCancelamentoOnline.Split(',');
                List<string> listaStatusTransacao = new List<string>();
                foreach (string transacao in descTransacoes)
                {
                    listaStatusTransacao.Add(transacao);
                }
                foreach (string listaItem in listaStatusTransacao)
                {
                    sbTransacao.Append(listaItem + ", ");
                }

                string transacaoRecuperados = sbTransacao.ToString().Substring(0, sbTransacao.ToString().LastIndexOf(","));
                sb.Append(transacaoRecuperados + "; ");

            }
            else
            {
                sb.Append("Todos;");
            }

            // ClockWork - Marcos Matsuoka - fim

            // Informa os filtros da consulta
            LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                sb.ToString(), request.CodigoSessao, request.CodigoProcesso);


            // Prepara o dicionário de processos
            Dictionary<string, ProcessoInfo> processos =
                new Dictionary<string, ProcessoInfo>();

            // Inicializa
            StreamWriter writer = null;

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            // Bloco de controle
            try
            {
                // Pega informações de configuração
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(
                        new ReceberConfiguracaoGeralRequest());

                // Declare o reader para que possa ser corretamente finalizado
                OracleConnection cn = Procedures.ReceberConexao();
                OracleRefCursor cursor = null;


                // Verificar se o ultimo caracter é uma barra, se não for, coloco uma
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao == "")
                {
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    resposta.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    resposta.DescricaoResposta = resposta.Erro;

                    resposta.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = resposta.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        resposta.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return resposta;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao.Length - 1) == @"\")
                        resposta.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao;
                    else
                        resposta.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao + "\\";
                }

                // pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);

                // Monta o nome do arquivo
                DateTime dataAtual = DateTime.Now;
                //resposta.CaminhoArquivoGerado = resposta.CaminhoArquivoGerado 
                //    + @"RelatorioTransacao_" + dataAtual.Year.ToString("0000")
                //    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                //    + dataAtual.Hour.ToString("00") + dataAtual.Minute.ToString("00")
                //    + dataAtual.Second.ToString("00") + ".csv";

                string nome = "";
                if (usuarioInfo.Nome.Contains(" "))
                    nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                else
                    nome = usuarioInfo.Nome;

                // Cria o arquivo
                if (!System.IO.Directory.Exists(resposta.CaminhoArquivoGerado))
                {
                    resposta.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    resposta.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + resposta.CaminhoArquivoGerado + "]";
                    resposta.DescricaoResposta = resposta.Erro;

                    resposta.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = resposta.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        resposta.Erro, request.CodigoSessao, request.CodigoProcesso);


                    return resposta;
                }
                else
                {
                    resposta.CaminhoArquivoGerado = resposta.CaminhoArquivoGerado
                        //1248 + @"\" + nome + @"_RelTransacao_" + dataAtual.Year.ToString("0000")
                        + nome + @"_RelTransacao_" + dataAtual.Year.ToString("0000")
                        + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                        + "_" + request.CodigoProcesso.ToString() + ".csv";

                    writer = new StreamWriter(resposta.CaminhoArquivoGerado, true, Encoding.Default);
                }
                CultureInfo culture = new CultureInfo("pt-BR");

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Arquivo criado: " + resposta.CaminhoArquivoGerado, request.CodigoSessao, request.CodigoProcesso);

                // Sinaliza
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Realizando consulta das transações no banco de dados", request.CodigoSessao, request.CodigoProcesso);

                // Pega a lista de transações do relatório
                PR_RELATORIO_TRANSACAO_L_Request prRequest =
                    new PR_RELATORIO_TRANSACAO_L_Request()
                    {
                        FiltroOrigem = request.Param.TipoArquivo,
                        FiltroEmpresaGrupo = request.Param.CodigoEmpresaGrupo,
                        FiltroEmpresaSubgrupo = request.Param.CodigoEmpresaSubGrupo,
                        FiltroEstabelecimento = request.Param.CodigoEstabelecimento,
                        FiltroFavorecido = request.Param.CodigoFavorecido,
                        FiltroProduto = request.Param.CodigoProduto,
                        FiltroPlano = request.Param.CodigoPlano,
                        FiltroMeioCaptura = request.Param.CodigoMeioCaptura,
                        FiltroTipoTransacao = request.Param.TipoRegistro,
                        FiltroNumeroPagamento = request.Param.NumeroPagamento,
                        FiltroContaCliente = request.Param.ContaCliente,//1438
                        FiltroDataTransacaoDe = request.Param.DataTransacaoDe != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataTransacaoDe, culture) : null,
                        FiltroDataTransacaoAte = request.Param.DataTransacaoAte != null ? ((Nullable<DateTime>)Convert.ToDateTime(request.Param.DataTransacaoAte, culture) + ts) : null,
                        FiltroDataVencimentoDe = request.Param.DataVencimentoDe != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataVencimentoDe, culture) : null,
                        FiltroDataVencimentoAte = request.Param.DataVencimentoAte != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataVencimentoAte, culture) + ts : null,
                        FiltroDataAgendamentoDe = request.Param.DataAgendamentoDe != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataAgendamentoDe, culture) : null,
                        FiltroDataAgendamentoAte = request.Param.DataAgendamentoAte != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataAgendamentoAte, culture) + ts : null,
                        FiltroDataEnvioMateraDe = request.Param.DataEnvioMateraDe != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataEnvioMateraDe, culture) : null,
                        FiltroDataEnvioMateraAte = request.Param.DataEnvioMateraAte != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataEnvioMateraAte, culture) + ts : null,
                        FiltroDataLiquidacaoDe = request.Param.DataLiquidacaoDe != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataLiquidacaoDe, culture) : null,
                        FiltroDataLiquidacaoAte = request.Param.DataLiquidacaoAte != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataLiquidacaoAte, culture) + ts : null,
                        //FiltroDataProcessamentoDe = request.Param.DataProcessamentoDe != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataProcessamentoDe, culture) : null,
                        //FiltroDataProcessamentoAte = request.Param.DataProcessamentoAte != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataProcessamentoAte, culture) + ts : null,
                        FiltroDataMovimentoDe = request.Param.DataMovimentoDe != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataMovimentoDe, culture) : null,
                        FiltroDataMovimentoAte = request.Param.DataMovimentoAte != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataMovimentoAte, culture) + ts : null,
                        FiltroStatusRetorno = request.Param.CodigoStatusRetorno,
                        FiltroStatusPagamento = request.Param.CodigoStatusPagamento,
                        FiltroStatusConciliacao = request.Param.CodigoStatusConciliacao,
                        FiltroStatusValidacao = request.Param.CodigoStatusValidacao,
                        FiltroStatusCancelamentoOnline = request.Param.CodigoStatusCancelamentoOnLine,
                        FiltroRetornoEnvioCCIDe = request.Param.DataRetornoCessaoDe != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataRetornoCessaoDe, culture) : null,
                        FiltroRetornoEnvioCCIAte = request.Param.DataRetornoCessaoAte != null ? (Nullable<DateTime>)Convert.ToDateTime(request.Param.DataRetornoCessaoAte, culture) + ts : null,
                        //ECOMMERCE - Fernando Bove - 20160111: Inicio
                        FiltroFinanceiroContabil = request.Param.FiltroFinanceiroContabil,
                        FiltroNSUHost = request.Param.FiltroNSUHost,
                        //ECOMMERCE - Fernando Bove - 20160111: Fim
                        //1312FiltroReferencia = request.Param.FiltroReferencia //ClockWork - Marcos Matsuoka
                        FiltroReferencia = request.Param.FiltroReferenciaCodigo //1312
                    };

                // Consulta as transacoes do arquivo
                cursor = Procedures.PR_RELATORIO_TRANSACAO_L(cn, prRequest);
                OracleDataReader reader = cursor.GetDataReader();

                // Monta o csv
                writer.WriteLine("Origem;Data Processamento;Codigo do Processo SCF;Status Importacao;Grupo;Subgrupo;Estabelecimento;Identificacao da Loja;NSHU_HOST;Data Transacao;Hora;Status Transacao;Codigo Registro;NSU_TEF;Codigo Autorizacao;Tipo Lancamento;Data Vencimento;Tipo Produto;Meio_Captura;Valor_Bruto;Valor_Desconto;Valor_Liquido;Nro_Cartao;Nro_Parcela;Nro_Parcela_Total;Valor_Parcela;Valor_Parcela_Desconto;Valor_Parcela_Liquido;Banco;Agencia;Conta;Numero conta cliente;Produto;Plano;Cupom_Fiscal;Modalidade;NSU_HOST_Original;NSU_TEF_Original;Data Transacao Original;Codigo Autorizacao Original;Codigo Anulacao;Motivo Anulacao;Quantidade Total Meio Pagamento;Meio Pagamento;Forma Meio Pagamento;Meio Pagamento Sequencia;Meio Pagamento Valor;Tipo Ajuste;Codigo Ajuste;Motivo Ajuste;Data Geracao Agenda;Numero Pagamento;Data Envio CCI;Usuario Envio CCI;Data Retorno CCI;Usuario Retorno CCI;Status Retorno;Data Envio Matera;Usuario Envio Matera;Data Pre Autorizacao;Financeiro/Contabil;NSU Host Reversao;Referencia"); //SCF1123 - Marcos Matsuoka // ClockWork - Marcos Matsuoka (campo Referencia)

                // Inicializa
                string separador = ";";

                // Quantidade de linhas para log
                int quantidadeLog = 0;

                // Sinaliza final da consulta
                if (resposta.QuantidadeLinhasGeradas == 0)
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        "Consulta das transações finalizada", request.CodigoSessao, request.CodigoProcesso);

                // Varre as linhas
                while (reader.Read())
                {
                    // Ajusta as informacoes da exportacao CCI e da exportacao Matera caso o processo esteja no dicionário
                    DateTime? dataExportacaoCCI = null;
                    string usuarioExportacaoCCI = null;
                    DateTime? dataExportacaoMatera = null;
                    string usuarioExportacaoMatera = null;

                    string usuarioRetornoCCI =
                        SegurancaHelper.ReceberUsuario(request.CodigoSessao).CodigoUsuario;

                    // Pega o codigo do processo
                    string codigoProcesso = reader["CODIGO_PROCESSO"] != DBNull.Value ? reader["CODIGO_PROCESSO"].ToString() : null;

                    // Apenas se tem código de processo
                    if (codigoProcesso != null)
                    {
                        // Se o processo não estiver no dicionário, carrega
                        if (!processos.ContainsKey(codigoProcesso))
                            processos.Add(
                                codigoProcesso,
                                PersistenciaHelper.Receber<ProcessoInfo>(
                                    request.CodigoSessao, codigoProcesso));

                        // Pega o processo
                        ProcessoInfo processoInfo = processos[codigoProcesso];

                        // Pega as informações de acordo com o tipo de processo
                        if (processoInfo is ProcessoExtratoInfo)
                        {
                            // Informações do processo de extrato
                            dataExportacaoCCI = ((ProcessoExtratoInfo)processoInfo).DataEnvioCCI;
                            usuarioExportacaoCCI = ((ProcessoExtratoInfo)processoInfo).CodigoUsuarioEnvioCCI;

                            // Informações do processo matera automático
                            if (reader["STATUS_TRANSACAO"].ToString() == "2")
                            {
                                dataExportacaoMatera = ((ProcessoExtratoInfo)processoInfo).DataEnvioMatera;
                                usuarioExportacaoMatera = ((ProcessoExtratoInfo)processoInfo).CodigoUsuarioEnvioMatera;
                            }
                        }
                        else if (processoInfo is ProcessoAtacadaoInfo)
                        {
                            // Informações do processo atacadão
                            dataExportacaoMatera = ((ProcessoAtacadaoInfo)processoInfo).DataEnvioMatera;
                            usuarioExportacaoMatera = ((ProcessoAtacadaoInfo)processoInfo).CodigoUsuarioEnvioMatera;
                        }
                        //else if (processoInfo is ProcessoPagamentoInfo)
                        //{
                        //    // Informações do processo pagamento
                        //    dataExportacaoMatera = ((ProcessoPagamentoInfo)processoInfo).DataEnvioMatera;
                        //    usuarioExportacaoMatera = ((ProcessoPagamentoInfo)processoInfo).CodigoUsuarioEnvioMatera;
                        //}
                    }

                    string refString = (reader["REFERENCIA"] != DBNull.Value ? reader["REFERENCIA"].ToString() : "");

                    // Insere a linha no arquivo
                    writer.WriteLine(
                        // ((reader["STATUS_TRANSACAO"] != DBNull.Value && reader["STATUS_TRANSACAO"].ToString() == "6") ? "*" : "") + separador +
                        ((reader["ORIGEM"] == DBNull.Value ? (reader["ORIGEM2"] != DBNull.Value ? reader["ORIGEM2"].ToString() : "") : reader["ORIGEM"].ToString()) + separador +

                        //(reader["DATA_PROCESSAMENTO"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_PROCESSAMENTO"]).ToString("dd/MM/yyyy") : "") + separador +

                        (reader["DATA_MOVIMENTO"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_MOVIMENTO"]).ToString("dd/MM/yyyy") : "") + separador +
                        codigoProcesso + separador +
                        (reader["STATUS_IMPORTACAO"] != DBNull.Value ? SqlDbLib.EnumToObject<ProcessoStatusEnum>(reader["STATUS_IMPORTACAO"]) : ProcessoStatusEnum.Finalizado) + separador +
                        (reader["NOME_GRUPO"] != DBNull.Value ? reader["NOME_GRUPO"].ToString() : "") + separador +
                        (reader["NOME_SUBGRUPO"] != DBNull.Value ? reader["NOME_SUBGRUPO"].ToString() : "") + separador +
                        (reader["NOME_ESTABELECIMENTO"] != DBNull.Value ? reader["NOME_ESTABELECIMENTO"].ToString() : "") + separador +
                        (reader["CNPJ"] != DBNull.Value ? "=\"" + reader["CNPJ"].ToString() + "\"" : "") + separador +
                        (reader["NSU_HOST"] != DBNull.Value ? reader["NSU_HOST"].ToString() : "") + separador +
                        (reader["DATA_TRANSACAO"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_TRANSACAO"]).ToString("dd/MM/yyyy") : "") + separador +
                        (reader["DATA_TRANSACAO"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_TRANSACAO"]).ToString("hh:mm") : "") + separador +
                        (reader["STATUS_TRANSACAO"] != DBNull.Value ? SqlDbLib.EnumToObject<TransacaoStatusEnum>(reader["STATUS_TRANSACAO"]) : TransacaoStatusEnum.Importado) + separador +
                        (reader["TIPO_TRANSACAO"] != DBNull.Value ? reader["TIPO_TRANSACAO"].ToString() : "") + separador +
                        (reader["NSU_TEF"] != DBNull.Value ? reader["NSU_TEF"].ToString() : "") + separador +
                        (reader["CODIGO_AUTORIZACAO"] != DBNull.Value ? reader["CODIGO_AUTORIZACAO"].ToString() : "") + separador +
                        (reader["TIPO_LANCAMENTO"] != DBNull.Value ? reader["TIPO_LANCAMENTO"].ToString() : "") + separador +
                        (reader["DATA_REPASSE"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_REPASSE"]).ToString("dd/MM/yyyy") : "") + separador +
                        (reader["TIPO_PRODUTO"] != DBNull.Value ? reader["TIPO_PRODUTO"].ToString() : "") + separador +
                        (reader["MEIO_CAPTURA"] != DBNull.Value ? reader["MEIO_CAPTURA"].ToString() : "") + separador +
                        (reader["VALOR_BRUTO"] != DBNull.Value ? String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(reader["VALOR_BRUTO"])) : "") + separador +
                        (reader["VALOR_DESCONTO"] != DBNull.Value ? String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(reader["VALOR_DESCONTO"])) : "") + separador +
                        (reader["VALOR_LIQUIDO"] != DBNull.Value ? String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(reader["VALOR_LIQUIDO"])) : "") + separador +
                        (reader["NUMERO_CARTAO"] != DBNull.Value ? "=\"" + reader["NUMERO_CARTAO"].ToString() + "\"" : "") + separador +
                        (reader["NUMERO_PARCELA"] != DBNull.Value ? reader["NUMERO_PARCELA"].ToString() : "") + separador +
                        (reader["NUMERO_PARCELA_TOTAL"] != DBNull.Value ? reader["NUMERO_PARCELA_TOTAL"].ToString() : "") + separador +
                        (reader["VALOR_PARCELA"] != DBNull.Value ? String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(reader["VALOR_PARCELA"])) : "") + separador +
                        (reader["VALOR_PARCELA_DESCONTO"] != DBNull.Value ? String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(reader["VALOR_PARCELA_DESCONTO"])) : "") + separador +
                        (reader["VALOR_PARCELA_LIQUIDO"] != DBNull.Value ? String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(reader["VALOR_PARCELA_LIQUIDO"])) : "") + separador +
                        (reader["BANCO"] != DBNull.Value ? reader["BANCO"].ToString() : "") + separador +
                        (reader["AGENCIA"] != DBNull.Value ? reader["AGENCIA"].ToString() : "") + separador +
                        (reader["CONTA"] != DBNull.Value ? reader["CONTA"].ToString() : "") + separador +
                        (reader["NUMERO_CONTA_CLIENTE"] != DBNull.Value ? reader["NUMERO_CONTA_CLIENTE"].ToString() : "") + separador +
                        (reader["NOME_PRODUTO"] != DBNull.Value ? reader["NOME_PRODUTO"].ToString() : "") + separador +
                        (reader["CODIGO_PLANO"] != DBNull.Value ? reader["CODIGO_PLANO"].ToString() : "") + separador +
                        (reader["CUPOM_FISCAL"] != DBNull.Value ? reader["CUPOM_FISCAL"].ToString() : "") + separador +
                        (reader["MODALIDADE"] != DBNull.Value ? reader["MODALIDADE"].ToString() : "") + separador +
                        (reader["NSU_HOST_ORIGINAL"] != DBNull.Value ? reader["NSU_HOST_ORIGINAL"].ToString() : "") + separador +
                        (reader["NSU_TEF_ORIGINAL"] != DBNull.Value ? reader["NSU_TEF_ORIGINAL"].ToString() : "") + separador +
                        // ECOMMERCE - Fernando Bove - 20160111
                        //(reader["NSU_HOST_REVERSAO"] != DBNull.Value ? reader["NSU_HOST_REVERSAO"].ToString() : "") + separador + // SCF1123 - Marcos Matsuoka (Movido para o final do relatorio)
                        (reader["DATA_TRANSACAO_ORIGINAL"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_TRANSACAO_ORIGINAL"]).ToString("dd/MM/yyyy") : "") + separador +
                        (reader["CODIGO_AUTORIZACAO_ORIGINAL"] != DBNull.Value ? reader["CODIGO_AUTORIZACAO_ORIGINAL"].ToString() : "") + separador +
                        (reader["CODIGO_ANULACAO"] != DBNull.Value ? reader["CODIGO_ANULACAO"].ToString() : "") + separador +
                        (reader["MOTIVO_ANULACAO"] != DBNull.Value ? reader["MOTIVO_ANULACAO"].ToString() : "") + separador +
                        (reader["QUANTIDADE_MEIO_PAGAMENTO"] != DBNull.Value ? reader["QUANTIDADE_MEIO_PAGAMENTO"].ToString() : "") + separador +
                        (reader["MEIO_PAGAMENTO"] != DBNull.Value ? reader["MEIO_PAGAMENTO"].ToString() : "") + separador +
                        (reader["FORMA_MEIO_PAGAMENTO"] != DBNull.Value ? reader["FORMA_MEIO_PAGAMENTO"].ToString() : "") + separador +
                        (reader["MEIO_PAGAMENTO_SEQ"] != DBNull.Value ? reader["MEIO_PAGAMENTO_SEQ"].ToString() : "") + separador +
                        (reader["MEIO_PAGAMENTO_VALOR"] != DBNull.Value ? String.Format(new CultureInfo("PT-BR"), "{0:N2}", Convert.ToDouble(reader["MEIO_PAGAMENTO_VALOR"])) : "") + separador +
                        (reader["TIPO_AJUSTE"] != DBNull.Value ? reader["TIPO_AJUSTE"].ToString() : "") + separador +
                        (reader["CODIGO_AJUSTE"] != DBNull.Value ? reader["CODIGO_AJUSTE"].ToString() : "") + separador +
                        (reader["MOTIVO_AJUSTE"] != DBNull.Value ? reader["MOTIVO_AJUSTE"].ToString() : "") + separador +
                        (reader["DATA_GERACAO_AGENDA"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_GERACAO_AGENDA"]).ToString("dd/MM/yyyy") : "") + separador +
                        (reader["NUMERO_PAGAMENTO"] != DBNull.Value ? reader["NUMERO_PAGAMENTO"].ToString() : "") + separador +
                        (dataExportacaoCCI.HasValue ? Convert.ToDateTime(dataExportacaoCCI).ToString("dd/MM/yyyy") : "") + separador +
                        (usuarioExportacaoCCI != null ? usuarioExportacaoCCI.ToString() : "") + separador +
                        (reader["DATA_RETORNO_CCI"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_RETORNO_CCI"]).ToString("dd/MM/yyyy") : "") + separador +
                        (reader["DATA_RETORNO_CCI"] != DBNull.Value ? usuarioRetornoCCI.ToString() : "") + separador +
                        (reader["STATUS_RETORNO"] != DBNull.Value ? SqlDbLib.EnumToObject<TransacaoStatusRetornoCessaoEnum>(reader["STATUS_RETORNO"]) : TransacaoStatusRetornoCessaoEnum.NaoRetornado) + separador +
                        (dataExportacaoMatera.HasValue ? Convert.ToDateTime(dataExportacaoMatera).ToString("dd/MM/yyyy") : "") + separador +
                        (usuarioExportacaoMatera != null ? usuarioExportacaoMatera.ToString() : "") + separador +
                        // ECOMMERCE - Fernando Bove - 20160111: Inicio
                        (reader["DATA_PRE_AUTORIZACAO"] != DBNull.Value ? Convert.ToDateTime(reader["DATA_PRE_AUTORIZACAO"]).ToString("dd/MM/yyyy") : "") + separador +
                        (reader["TIPO_FINANCEIRO_CONTABIL"] != DBNull.Value ? reader["TIPO_FINANCEIRO_CONTABIL"].ToString() : "") + separador +
                        (reader["NSU_HOST_REVERSAO"] != DBNull.Value ? reader["NSU_HOST_REVERSAO"].ToString() : "") + separador + // SCF1123 - Marcos Matsuoka
                        // ECOMMERCE - Fernando Bove - 20160111: Fim
                        // CLOCKWORK - Marcos Matsuoka - inicio
                        "=\"" +
                        (reader["REFERENCIA"] != DBNull.Value ? reader["REFERENCIA"].ToString() : "")
                        + "\";"));
                    // CLOCKWORK - Marcos Matsuoka - fim


                    // Atualiza a quantidade de linhas geradas
                    resposta.QuantidadeLinhasGeradas++;
                    quantidadeLog++;

                    // Sinaliza
                    if (quantidadeLog >= 15000)
                    {
                        // Sinaliza
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                            resposta.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                        // Zera linhas do log
                        quantidadeLog = 0;
                    }
                }

                // Informa total de linhas
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "Total de linhas geradas: " + resposta.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);

            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                // Finaliza o arquivo
                if (writer != null)
                    writer.Close();
            }

            // Retorna
            return resposta;
        }

        public ListarRelatorioConciliacaoContabilResponse ListarRelatorioConciliacaoContabil(ListarRelatorioConciliacaoContabilRequest request)
        {
            ListarRelatorioConciliacaoContabilResponse response = new ListarRelatorioConciliacaoContabilResponse();
            response.PreparaResposta(request);
            TimeSpan ts = new TimeSpan(23, 59, 59);
            PR_REL_SALDO_CONCILIACAO_L_Request req = new PR_REL_SALDO_CONCILIACAO_L_Request()
            {
                FiltroDataConciliacaoFim = request.FiltroDataFimConciliacao + ts,
                FiltroDataConciliacaoInicio = request.FiltroDataInicioConciliacao
            };

            // Declare o reader para que possa ser corretamente finalizado
            OracleConnection cn = Procedures.ReceberConexao();
            OracleRefCursor retcur = null;

            OracleRefCursor orcSaldo = null;

            Procedures.PR_REL_SALDO_CONCILIACAO_L(cn, req, out retcur, out orcSaldo);

            OracleDataReader drSaldo = orcSaldo.GetDataReader();

            decimal saldoDiaAnterior = 0;
            decimal saldoAux = 0;

            if (drSaldo.Read())
            {
                saldoDiaAnterior = decimal.Parse(drSaldo[0].ToString());
            }

            response.SaldoAnterior = saldoDiaAnterior;

            saldoAux = saldoDiaAnterior;

            OracleDataReader dr = retcur.GetDataReader();

            List<RelatorioConciliacaoContabilInfo> ListRel = new List<RelatorioConciliacaoContabilInfo>();

            while (dr.Read())
            {
                RelatorioConciliacaoContabilInfo rel = new RelatorioConciliacaoContabilInfo();
                rel.CodigoAutorizacao = dr["codigo_autorizacao"].ToString();
                rel.CodigoProcesso = int.Parse(dr["codigo_processo"].ToString());
                rel.DataConciliacao = Convert.IsDBNull(dr["data_conciliacao"]) ? null : (Nullable<DateTime>)Convert.ToDateTime(dr["data_conciliacao"].ToString());
                rel.DataTransacao = Convert.ToDateTime(dr["data_transacao"].ToString());
                rel.NSUHost = dr["nsu_host"].ToString();
                rel.SitefRede = dr["SITEF_REDE"].ToString();
                rel.StatusConciliacao = (TransacaoStatusConciliacaoEnum)int.Parse(dr["status_conciliacao"].ToString());
                rel.ValorConciliacao = decimal.Parse(dr["valor_conciliacao"].ToString());
                rel.ValorTransacao = decimal.Parse(dr["valor_venda"].ToString());

                rel.SaldoAcumulado = saldoAux;
                saldoAux += rel.ValorConciliacao;

                ListRel.Add(rel);
            }

            response.SaldoFinal = saldoAux;

            response.QuantidadeLinhas = ListRel.Count;

            response.Resultado = ListRel;

            return response;
        }

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarRelatorioConferenciaVendasResponse ListarRelatorioConferenciaVendas(ListarRelatorioConferenciaVendasRequest request)
        {
            // Prepara resposta 12/12/2018
            ListarRelatorioConferenciaVendasResponse resposta = new ListarRelatorioConferenciaVendasResponse();
            resposta.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 29);

            // Bloco de controle
            try
            {
                //===============================================================================================================
                // Monta a lista de condições
                //===============================================================================================================
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                if (request.FiltroEmpresaGrupo != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroEmpresaGrupo", CondicaoTipoEnum.Igual, request.FiltroEmpresaGrupo));                    
                }

                if (request.FiltroEmpresaSubgrupo != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroEmpresaSubgrupo", CondicaoTipoEnum.Igual, request.FiltroEmpresaSubgrupo));
                }

                if (request.FiltroDataFimVendas.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataVendas",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataFimVendas.Value + ts));                    
                }

                if (request.FiltroDataInicioVendas.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataVendas",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataInicioVendas.Value));
                }
                
                if (request.FiltroDataVendas.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataVendas",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataVendas.Value));
                }

                if (request.FiltroDataMovimentoFim.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMovimento",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataMovimentoFim.Value + ts));
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("FiltroDataMovimento fim [" + request.FiltroDataMovimentoFim + "]", request.CodigoSessao, "0");
                }

                if (request.FiltroDataMovimentoInicio.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMovimento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataMovimentoInicio.Value));
                }
                // DataMovimento igual
                if (request.FiltroDataMovimento.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMovimento",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataMovimento.Value));
                }
                if (request.FiltroEstabelecimento != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroEstabelecimento", CondicaoTipoEnum.Igual, request.FiltroEstabelecimento));
                }
                if (request.FiltroFavorecido != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroFavorecido", CondicaoTipoEnum.Igual, request.FiltroFavorecido));
                }

                //===============================================================================================================
                // consolidações
                //===============================================================================================================
                condicoes.Add(
                    new CondicaoInfo(
                        "FiltroConsolidaGrupoSubGrupoProduto", CondicaoTipoEnum.Igual, request.FiltroConsolidaGrupoSubGrupoProduto));
                
                condicoes.Add(
                    new CondicaoInfo(
                        "FiltroConsolidaDataVenda", CondicaoTipoEnum.Igual, request.FiltroConsolidaDataVenda));
                
                condicoes.Add(
                    new CondicaoInfo(
                        "FiltroConsolidaDataProcessamento", CondicaoTipoEnum.Igual, request.FiltroConsolidaDataProcessamento));
                
                condicoes.Add(
                    new CondicaoInfo(
                        "FiltroConsolidaBancoAgenciaConta", CondicaoTipoEnum.Igual, request.FiltroConsolidaBancoAgenciaConta));
                
                condicoes.Add(
                    new CondicaoInfo(
                        "FiltroConsolidaEstabelecimento", CondicaoTipoEnum.Igual, request.FiltroConsolidaEstabelecimento));
                
                //===============================================================================================================
                // data de retorno da CCI
                //===============================================================================================================
                if (request.FiltrotxtDataRetornoCCIInicio.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltrotxtDataRetornoCCI",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltrotxtDataRetornoCCIInicio.Value));
                 }
                if (request.FiltrotxtDataRetornoCCIFim.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltrotxtDataRetornoCCI",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltrotxtDataRetornoCCIFim.Value));
                }
                // tupo de registro
                if (request.FiltroTipoRegistro != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroTipoRegistro", CondicaoTipoEnum.Igual, request.FiltroTipoRegistro));                    
                }
                if (request.FiltroFinanceiroContabil != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroFinanceiroContabil", CondicaoTipoEnum.Igual, request.FiltroFinanceiroContabil));                  
                }
                if (request.FiltroReferencia != null)
                {
                    condicoes.Add(
                        new CondicaoInfo("FiltroReferencia", CondicaoTipoEnum.Igual, request.FiltroReferencia));                    
                }
                if (request.FiltroConsolidaReferencia == true)
                {
                    condicoes.Add(
                            new CondicaoInfo("FiltroConsolidaReferencia", CondicaoTipoEnum.Igual, request.FiltroConsolidaReferencia));                    
                }
                if (request.VerificarQuantidadeLinhas == true)
                {
                    ListarRelatorioVendasDbResponse res = new ListarRelatorioVendasDbResponse();
                    res.PreparaResposta(request);

                    try
                    {
                        res = (ListarRelatorioVendasDbResponse)Mensageria.Processar<ConsultarObjetosResponse<RelatorioConferenciaVendasInfo>>(
                                new ListarRelatorioVendasDbRequest()
                                {
                                    CodigoSessao = request.CodigoSessao,
                                    VerificarQuantidadeLinhas = true,
                                    Condicoes = condicoes
                                });                        

                        resposta.QuantidadeLinhas = res.QuantidadeLinhas;
                        return resposta;
                    }
                    catch (Exception ex)
                    {
                        // Trata erro
                        resposta.ProcessarExcessao(ex);
                    }
                }

                resposta.Resultado =
                    PersistenciaHelper.Listar<RelatorioConferenciaVendasInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarRelatorioVencimentoResponse ListarRelatorioVencimento(ListarRelatorioVencimentoRequest request)
        {
            // Prepara resposta
            ListarRelatorioVencimentoResponse resposta = new ListarRelatorioVencimentoResponse();
            resposta.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 29);

            // Bloco de controle
            try
            {
                //// isso nao está mais sendo utilizado
                //// Faz o log
                //string codLog = request.CodLog;

                //if (request.CodLog == null || request.CodLog == string.Empty)
                //{
                //    codLog = Mensageria.Processar<SalvarLogResponse>(
                //        new SalvarLogRequest()
                //        {
                //            CodigoUsuario = "",
                //            Descricao = "Iniciando relatório - " + request.TipoRelatorio,
                //            TipoOrigem = "RelatorioVencimento",
                //            CodigoOrigem = codLog
                //        }).retorno.CodigoLog;
                //}

                // monta a string para informar os filtros da consulta
                StringBuilder sb = new StringBuilder();

                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // DataPosicao
                sb.Append("Filtros: Data Posição = ");
                if (request.FiltroDataPosicao.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataPosicao",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataPosicao.Value));
                    sb.Append(Convert.ToDateTime(request.FiltroDataPosicao.Value).ToString("dd/MM/yyyy") + " - ");
                }
                else
                    sb.Append("Qualquer - ");

                // DataTransacao inicial
                sb.Append("Filtros: Data de Transacao = ");
                if (request.FiltroDataInicioTransacao.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataTransacao",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataInicioTransacao.Value));
                    sb.Append(Convert.ToDateTime(request.FiltroDataInicioTransacao.Value).ToString("dd/MM/yyyy") + " - ");
                }
                else
                    sb.Append("Qualquer - ");

                // DataTransacao final
                if (request.FiltroDataFimTransacao.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataTransacao",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataFimTransacao.Value + ts));
                    sb.Append(Convert.ToDateTime(request.FiltroDataFimTransacao.Value).ToString("dd/MM/yyyy") + "; ");
                }
                else
                    sb.Append("Qualquer; ");

                // DataTransacao
                if (request.FiltroDataTransacao.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataTransacao",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataTransacao.Value));

                // DataVencimento inicial
                sb.Append("Data de Vencimento = ");
                if (request.FiltroDataInicioVencimento.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataVencimento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataInicioVencimento.Value));
                    sb.Append(Convert.ToDateTime(request.FiltroDataInicioVencimento.Value).ToString("dd/MM/yyyy") + " - ");
                }
                else
                    sb.Append("Qualquer - ");

                // DataVencimento final
                if (request.FiltroDataFimVencimento.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataVencimento",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataFimVencimento.Value + ts));
                    sb.Append(Convert.ToDateTime(request.FiltroDataFimVencimento.Value).ToString("dd/MM/yyyy") + "; ");
                }
                else
                    sb.Append("Qualquer; ");

                // DataVencimento
                if (request.FiltroDataVencimento.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataVencimento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataVencimento.Value));


                // DataProcessamento inicial
                sb.Append("Data de Movimento = ");
                if (request.FiltroDataInicioMovimento.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMovimento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataInicioMovimento.Value));
                    sb.Append(Convert.ToDateTime(request.FiltroDataInicioMovimento.Value).ToString("dd/MM/yyyy") + " - ");
                }
                else
                    sb.Append("Qualquer - ");

                // DataProcessamento final
                if (request.FiltroDataFimMovimento.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMovimento",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataFimMovimento.Value + ts));
                    sb.Append(Convert.ToDateTime(request.FiltroDataFimMovimento.Value).ToString("dd/MM/yyyy") + "; ");
                }
                else
                    sb.Append("Qualquer; ");

                // DataProcessamento
                if (request.FiltroDataMovimento.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMovimento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataMovimento.Value));

                // DataAgendamento inicial
                sb.Append("Data de Agendamento = ");
                if (request.FiltroDataAgendamentoDe.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataAgendamento",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltroDataAgendamentoDe.Value));
                    sb.Append(Convert.ToDateTime(request.FiltroDataAgendamentoDe.Value).ToString("dd/MM/yyyy") + " - ");
                }
                else
                    sb.Append("Qualquer - ");

                // DataAgendamento final
                if (request.FiltroDataAgendamentoAte.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataAgendamento",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltroDataAgendamentoAte.Value + ts));
                    sb.Append(Convert.ToDateTime(request.FiltroDataAgendamentoAte.Value).ToString("dd/MM/yyyy") + "; ");
                }
                else
                    sb.Append("Qualquer; ");

                // SCF1170 data de retorno da CCI - inicio
                if (request.FiltrotxtDataRetornoCCIInicio.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltrotxtDataRetornoCCI",
                            CondicaoTipoEnum.MaiorIgual,
                            request.FiltrotxtDataRetornoCCIInicio.Value));
                }
                else
                    sb.Append("Qualquer; ");

                if (request.FiltrotxtDataRetornoCCIFim.HasValue)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltrotxtDataRetornoCCI",
                            CondicaoTipoEnum.MenorIgual,
                            request.FiltrotxtDataRetornoCCIFim.Value));
                }
                else
                    sb.Append("Qualquer; ");
                // SCF1170 data de retorno da CCI - fim

                /*******************************/
                sb.Append("Grupo = ");
                if (request.FiltroEmpresaGrupo != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroEmpresaGrupo", CondicaoTipoEnum.Igual, request.FiltroEmpresaGrupo));
                    sb.Append(request.FiltroEmpresaGrupo + "; ");
                }
                else
                    sb.Append("Todos; ");

                sb.Append("SubGrupo = ");
                if (request.FiltroEmpresaSubGrupo != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroEmpresaSubGrupo", CondicaoTipoEnum.Igual, request.FiltroEmpresaSubGrupo));
                    sb.Append(request.FiltroEmpresaSubGrupo + "; ");
                }
                else
                    sb.Append("Todos; ");

                sb.Append("Produtos = ");
                if (request.FiltroProduto != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroProduto", CondicaoTipoEnum.Igual, request.FiltroProduto));

                    StringBuilder sbProduto = new StringBuilder();
                    string[] produtos = request.FiltroProduto.Split(',');
                    foreach (string produto in produtos)
                    {
                        // recupera o produto
                        ProdutoInfo produtoInfo = PersistenciaHelper.Receber<ProdutoInfo>(
                            request.CodigoSessao, produto);

                        if (produtoInfo != null)
                            sbProduto.Append(produtoInfo.NomeProduto + ", ");
                    }

                    string produtoRecuperados = sbProduto.ToString().Substring(0, sbProduto.ToString().LastIndexOf(","));
                    sb.Append(produtoRecuperados + "; ");
                }
                else
                    sb.Append("Todos; ");

                sb.Append("Favorecido = ");
                if (request.FiltroFavorecido != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroFavorecido", CondicaoTipoEnum.Igual, request.FiltroFavorecido));
                    sb.Append(request.FiltroFavorecido + "; ");
                }
                else
                    sb.Append("Todos; ");

                sb.Append("Tipo de Registro = " + request.FiltroTipoRegistro + "; ");
                if (request.FiltroTipoRegistro != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroTipoRegistro", CondicaoTipoEnum.Igual, request.FiltroTipoRegistro));

                if (request.FiltroStatusRetornoCessao != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroStatusRetornoCessao", CondicaoTipoEnum.Igual, request.FiltroStatusRetornoCessao));
                    sb.Append("Status Cessao = ");
                    sb.Append(request.FiltroStatusRetornoCessao == "S" ? "Cessionadas" : "Nao cessionadas" + "; ");
                }
                else
                    sb.Append("Retorno de cessao = Todos; ");

                // ECOMMERCE - Fernando Bove - 20160106: Inicio
                sb.Append("Financeiro/Contabil = ");
                if (request.FiltroFinanceiroContabil != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroFinanceiroContabil", CondicaoTipoEnum.Igual, request.FiltroFinanceiroContabil));
                    sb.Append(request.FiltroFinanceiroContabil + "; ");
                }
                else
                    sb.Append("Todos; ");

                //ClockWork Rogerio : Inicio
                sb.Append("Referencias = ");
                if (request.FiltroReferencia != null)
                {

                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroReferencia", CondicaoTipoEnum.Igual, request.FiltroReferencia));

                    StringBuilder sbReferencia = new StringBuilder();
                    string[] referencias = request.FiltroReferencia.Split(',');
                    foreach (string referencia in referencias)
                    {
                        List<CondicaoInfo> condicoesRef = new List<CondicaoInfo>();

                        condicoesRef.Add(
                        new CondicaoInfo(
                            "Valor", CondicaoTipoEnum.Igual, referencia));

                        // recupera a referencia
                        List<ListaItemInfo> referenciaInfo =
                            PersistenciaHelper.Listar<ListaItemInfo>(
                                null, condicoesRef);

                        foreach (ListaItemInfo listaItem in referenciaInfo)
                        {
                            sbReferencia.Append(listaItem.Descricao + ", ");
                        }
                    }

                    string referenciaRecuperados = sbReferencia.ToString().Substring(0, sbReferencia.ToString().LastIndexOf(","));
                    sb.Append(referenciaRecuperados + "; ");
                }
                else
                    sb.Append("Todos; ");
                //ClockWork Rogerio : Fim

                // ECOMMERCE - Fernando Bove - 20160106: Fim

                // Consolidacao por agendamento e vencimento
                if (request.FiltroConsolidaAgendamentoVencimento == true)
                    sb.Append("Consolidacao = Dt. Agend + Dt. Venc; ");
                condicoes.Add(
                    new CondicaoInfo("FiltroConsolidacaoAgendamentoVencimento", CondicaoTipoEnum.Igual, request.FiltroConsolidaAgendamentoVencimento));

                if (request.FiltroConsolidaMaiorData == true)
                    sb.Append("Consolidacao = Maior Dt. Agend; ");
                // Consolidacao por maior data de agendamento
                condicoes.Add(
                    new CondicaoInfo("FiltroConsolidacaoMaiorData", CondicaoTipoEnum.Igual, request.FiltroConsolidaMaiorData));

                // Consolidacao por referência
                if (request.FiltroConsolidaReferencia == true)
                    sb.Append("Consolidacao = Referencia; ");
                condicoes.Add(
                    new CondicaoInfo("FiltroConsolidacaoReferencia", CondicaoTipoEnum.Igual, request.FiltroConsolidaReferencia));

                // Retornar Transações Pagas
                if (request.FiltroRetornarTransacaoPaga == true)
                    sb.Append("Retornar Transações Pagas; ");
                condicoes.Add(
                    new CondicaoInfo("FiltroRetornarTransacaoPaga", CondicaoTipoEnum.Igual, request.FiltroRetornarTransacaoPaga));

                // Informa se é para gerar os pagamentos
                if (request.GerarPagamentos == true)
                    sb.Append("Gerar Pagamentos; ");
                condicoes.Add(
                    new CondicaoInfo("GerarPagamentos", CondicaoTipoEnum.Igual, request.GerarPagamentos));

                // nao é mais utilizado
                //if (null != codLog || string.Empty != codLog)                
                //    condicoes.Add(
                //        new CondicaoInfo("CodLog", CondicaoTipoEnum.Igual, codLog));

                if (request.CodigoProcesso != null)
                    condicoes.Add(
                        new CondicaoInfo("CodigoProcesso", CondicaoTipoEnum.Igual, request.CodigoProcesso));

                if (!request.RetornarRelatorio)
                {
                    //1353
                    //Mensageria.Processar<SalvarLogResponse>(
                    //    new SalvarLogRequest()
                    //    {
                    //        CodigoUsuario = "",
                    //        Descricao = sb.ToString().Replace(";", ","),
                    //        TipoOrigem = "APP",
                    //        CodigoOrigem = codLog
                    //    });
                }

                if (request.VerificarQuantidadeLinhas == true)
                {
                    ListarRelatorioVencimentoDbResponse res = new ListarRelatorioVencimentoDbResponse();
                    res.PreparaResposta(request);

                    try
                    {
                        //1353
                        //Mensageria.Processar<SalvarLogResponse>(
                        //    new SalvarLogRequest()
                        //    {
                        //        CodigoUsuario = "",
                        //        Descricao = "Quantidade de linhas.",
                        //        TipoOrigem = "APP",
                        //        CodigoOrigem = codLog
                        //    });

                        res = (ListarRelatorioVencimentoDbResponse)Mensageria.Processar<ConsultarObjetosResponse<RelatorioVencimentoInfo>>(
                                new ListarRelatorioVencimentoDbRequest()
                                {
                                    CodigoSessao = request.CodigoSessao,
                                    VerificarQuantidadeLinhas = true,
                                    Condicoes = condicoes
                                });

                        resposta.QuantidadeLinhas = res.QuantidadeLinhas;
                        // nao é mais utilizado
                        //resposta.CodLog = codLog;

                        //1353
                        //Mensageria.Processar<SalvarLogResponse>(
                        //    new SalvarLogRequest()
                        //    {
                        //        CodigoUsuario = "",
                        //        Descricao = "Retornando a Quantidade de linhas: " + resposta.QuantidadeLinhas + " linhas.",
                        //        TipoOrigem = "APP",
                        //        CodigoOrigem = codLog
                        //    });

                        return resposta;
                    }
                    catch (Exception ex)
                    {
                        // Trata erro
                        resposta.ProcessarExcessao(ex);
                    }
                }
                else
                {

                    //1353
                    //Mensageria.Processar<SalvarLogResponse>(
                    //    new SalvarLogRequest()
                    //    {
                    //        CodigoUsuario = "",
                    //        Descricao = "Fazendo a chamada do banco.",
                    //        TipoOrigem = "APP",
                    //        CodigoOrigem = codLog
                    //    });

                    // Solicita a lista
                    resposta.Resultado =
                        PersistenciaHelper.Listar<RelatorioVencimentoInfo>(
                            request.CodigoSessao, condicoes);

                    // nao é mais utilizado
                    // resposta.CodLog = codLog;

                    //1353
                    //Mensageria.Processar<SalvarLogResponse>(
                    //    new SalvarLogRequest()
                    //    {
                    //        CodigoUsuario = "",
                    //        Descricao = "ListarRelatorioVencimento - Retornando a resposta do banco.",
                    //        TipoOrigem = "APP",
                    //        CodigoOrigem = codLog
                    //    });
                }
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Lista relatório vencimentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarRelatorioConsolidadoResponse ListarRelatorioConsolidado(ListarRelatorioConsolidadoRequest request)
        {
            // Prepara resposta
            ListarRelatorioConsolidadoResponse resposta = new ListarRelatorioConsolidadoResponse();
            resposta.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 29);

            // Bloco de controle
            try
            {
                // Faz o log
                string codLog = request.CodLog;

                if (!request.RetornarRelatorio)
                {
                    codLog = Mensageria.Processar<SalvarLogResponse>(
                        new SalvarLogRequest()
                        {
                            CodigoUsuario = "",
                            Descricao = "Iniciando relatório - " + request.TipoRelatorio,
                            TipoOrigem = "RelatorioConsolidado"
                        }).retorno.CodigoLog;
                }

                // monta a string para informar os filtros da consulta
                StringBuilder sb = new StringBuilder();

                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                sb.Append("Filtros: Relatório de ");
                if (request.FiltroTipoConsolidado != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroTipoConsolidado", CondicaoTipoEnum.Igual, request.FiltroTipoConsolidado));

                    if (request.FiltroTipoConsolidado == "V")
                        sb.Append("Vendas; ");
                    else
                        sb.Append("Recebimento; ");
                }

                if (request.FiltroFinanceiroContabil != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroFinanceiroContabil", CondicaoTipoEnum.Igual, request.FiltroFinanceiroContabil));

                    if (request.FiltroFinanceiroContabil == "C")
                        sb.Append("Contábil; ");
                    else
                        sb.Append("Financeiro; ");
                }

                sb.Append("Quebra Favorecido = ");
                if (request.FiltroQuebraFavorecido)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroQuebraFavorecido", CondicaoTipoEnum.Igual, request.FiltroQuebraFavorecido));
                    sb.Append("Sim; ");
                }
                else
                    sb.Append("Nao; ");

                sb.Append("Produtos = ");
                if (request.FiltroProduto != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroProduto", CondicaoTipoEnum.Igual, request.FiltroProduto));

                    StringBuilder sbProduto = new StringBuilder();
                    string[] produtos = request.FiltroProduto.Split(',');
                    foreach (string produto in produtos)
                    {
                        // recupera o produto
                        ProdutoInfo produtoInfo = PersistenciaHelper.Receber<ProdutoInfo>(
                            request.CodigoSessao, produto);

                        if (produtoInfo != null)
                            sbProduto.Append(produtoInfo.NomeProduto + ", ");
                    }

                    string produtoRecuperados = sbProduto.ToString().Substring(0, sbProduto.ToString().LastIndexOf(","));
                    sb.Append(produtoRecuperados + "; ");
                }
                else
                    sb.Append("Todos; ");

                sb.Append("Favorecido = ");
                if (request.FiltroFavorecidoOriginal != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroFavorecidoOriginal", CondicaoTipoEnum.Igual, request.FiltroFavorecidoOriginal));

                    StringBuilder sbFavorecido = new StringBuilder();
                    string[] favorecidos = request.FiltroFavorecidoOriginal.Split(',');
                    foreach (string favorecido in favorecidos)
                    {
                        // recupera o produto
                        FavorecidoInfo favorecidoInfo = PersistenciaHelper.Receber<FavorecidoInfo>(
                            request.CodigoSessao, favorecido);

                        if (favorecidoInfo != null)
                            sbFavorecido.Append(favorecidoInfo.NomeFavorecido + ", ");
                    }
                    string favorecidoRecuperados = sbFavorecido.ToString().Substring(0, sbFavorecido.ToString().LastIndexOf(","));
                    sb.Append(favorecidoRecuperados + "; ");
                }
                else
                    sb.Append("Todos; ");

                sb.Append("Tipo de Registro = ");
                resposta.ListaConfigTipoRegistro = new List<ConfiguracaoTipoRegistroInfo>();


                if (request.FiltroDescricaoTipoRegistro != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroTipoRegistro", CondicaoTipoEnum.Igual, request.FiltroDescricaoTipoRegistro));

                    StringBuilder sbRegsitro = new StringBuilder();
                    string[] registros = request.FiltroCodigoTipoRegistro.Split(',');
                    foreach (string registro in registros)
                    {
                        // recupera o registro
                        ConfiguracaoTipoRegistroInfo registroInfo = PersistenciaHelper.Receber<ConfiguracaoTipoRegistroInfo>(
                            request.CodigoSessao, registro);

                        if (registroInfo != null)
                        {
                            if (registroInfo.GerarAgenda == false)
                                resposta.ListaConfigTipoRegistro.Add(registroInfo);
                            sbRegsitro.Append(registroInfo.TipoRegistro + ", ");
                        }
                    }
                    string registroRecuperados = sbRegsitro.ToString().Substring(0, sbRegsitro.ToString().LastIndexOf(","));
                    sb.Append(registroRecuperados + "; ");
                }
                else
                {
                    sb.Append("Todos; ");

                    List<ConfiguracaoTipoRegistroInfo> listaCompleta = new List<ConfiguracaoTipoRegistroInfo>();

                    //Permite ao usuário buscar pelo status
                    condicoes.Add(
                        new CondicaoInfo(
                             "Ativo", CondicaoTipoEnum.Igual, true));

                    // Solicita a lista
                    listaCompleta =
                        PersistenciaHelper.Listar<ConfiguracaoTipoRegistroInfo>(
                            request.CodigoSessao, condicoes);

                    if (request.FiltroTipoConsolidado == "V")
                    {
                        if (((ConfiguracaoTipoRegistroInfo)(listaCompleta.Where(s => s.TipoRegistro.Equals("AJ")).ToList()[0])).GerarAgenda == false)
                            resposta.ListaConfigTipoRegistro.Add((listaCompleta.Where(s => s.TipoRegistro.Equals("AJ")).ToList()[0]));
                        if (((ConfiguracaoTipoRegistroInfo)(listaCompleta.Where(s => s.TipoRegistro.Equals("AV")).ToList()[0])).GerarAgenda == false)
                            resposta.ListaConfigTipoRegistro.Add((listaCompleta.Where(s => s.TipoRegistro.Equals("AV")).ToList()[0]));
                        if (((ConfiguracaoTipoRegistroInfo)(listaCompleta.Where(s => s.TipoRegistro.Equals("CV")).ToList()[0])).GerarAgenda == false)
                            resposta.ListaConfigTipoRegistro.Add((listaCompleta.Where(s => s.TipoRegistro.Equals("CV")).ToList()[0]));
                    }
                    else
                    {
                        if (((ConfiguracaoTipoRegistroInfo)(listaCompleta.Where(s => s.TipoRegistro.Equals("AP")).ToList()[0])).GerarAgenda == false)
                            resposta.ListaConfigTipoRegistro.Add((listaCompleta.Where(s => s.TipoRegistro.Equals("AP")).ToList()[0]));
                        if (((ConfiguracaoTipoRegistroInfo)(listaCompleta.Where(s => s.TipoRegistro.Equals("CP")).ToList()[0])).GerarAgenda == false)
                            resposta.ListaConfigTipoRegistro.Add((listaCompleta.Where(s => s.TipoRegistro.Equals("CP")).ToList()[0]));
                    }
                }

                String reg = "";
                foreach (ConfiguracaoTipoRegistroInfo config in resposta.ListaConfigTipoRegistro)
                    reg = reg + config.TipoRegistro + " ";

                reg = reg.Trim();
                reg = reg + ".";

                if (resposta.ListaConfigTipoRegistro.Count > 0)
                    sb.Append(
                        "Filtro Período Dt Agendamento informado, valores não apresentados para Atacadão " +
                        "e Tipo Registro [" + reg + "]");

                sb.Append("Estabelecimento = ");
                if (request.FiltroEstabelecimento != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroEstabelecimento", CondicaoTipoEnum.Igual, request.FiltroEstabelecimento));
                    sb.Append(request.FiltroDescricaoEstabelecimento + "; ");
                }
                else
                    sb.Append("Todos; ");

                sb.Append("Meio Pagamento = ");
                if (request.FiltroMeioPagamento != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroMeioPagamento", CondicaoTipoEnum.Igual, request.FiltroMeioPagamento));

                    StringBuilder sbMeioPagamento = new StringBuilder();
                    string[] meioPagamentos = request.FiltroMeioPagamento.Split(',');
                    foreach (string meioPagamento in meioPagamentos)
                    {
                        // recupera o produto
                        ListaItemInfo meioPagamentoInfo = PersistenciaHelper.Receber<ListaItemInfo>(
                            request.CodigoSessao, meioPagamento);

                        if (meioPagamentoInfo != null)
                            sbMeioPagamento.Append(meioPagamentoInfo.Descricao + ", ");
                    }
                    string meioPagamentoRecuperados = sbMeioPagamento.ToString().Substring(0, sbMeioPagamento.ToString().LastIndexOf(","));
                    sb.Append(meioPagamentoRecuperados + "; ");
                }
                else
                    sb.Append("Todos; ");

                sb.Append("layout Relatorio = ");
                if (request.FiltroLayoutVenda != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroLayoutVenda", CondicaoTipoEnum.Igual, request.FiltroLayoutVenda));
                    sb.Append(request.FiltroLayoutVenda == "T" ? "Totalizado por Produto; " : request.FiltroLayoutVenda == "A" ? "Analitico por Data; " : "Sintetico por Data; ");
                }

                if (request.FiltroLayoutRecebimento != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroLayoutRecebimento", CondicaoTipoEnum.Igual, request.FiltroLayoutRecebimento));
                    sb.Append(request.FiltroLayoutRecebimento == "T" ? "Totalizado por Meio Pagamento; " : request.FiltroLayoutRecebimento == "A" ? "Analitico por Data; " : request.FiltroLayoutRecebimento == "S" ? "Sintetico por Data; " : "Totalizado envio CCI; ");
                }

                condicoes.Add(new CondicaoInfo(
                    "FiltroQtdeVenda", CondicaoTipoEnum.Igual, request.FiltroQtdeVenda));

                condicoes.Add(new CondicaoInfo(
                    "FiltroQtdeRecebimento", CondicaoTipoEnum.Igual, request.FiltroQtdeRecebimento));

                sb.Append("Data de Agendamento = ");
                if (request.FiltroDataAgendamentoDe != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroDataAgendamentoDe", CondicaoTipoEnum.MaiorIgual, request.FiltroDataAgendamentoDe));
                    sb.Append(Convert.ToDateTime(request.FiltroDataAgendamentoDe).ToString("dd/MM/yyyy") + " - ");
                }
                else
                    sb.Append("Qualquer - ");

                if (request.FiltroDataAgendamentoAte != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroDataAgendamentoAte", CondicaoTipoEnum.MenorIgual, request.FiltroDataAgendamentoAte));
                    sb.Append(Convert.ToDateTime(request.FiltroDataAgendamentoAte).ToString("dd/MM/yyyy") + "; ");
                }
                else
                    sb.Append("Qualquer; ");

                sb.Append("Data de Processamento = ");
                if (request.FiltroDataInicioMovimento != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroDataInicioMovimento", CondicaoTipoEnum.MaiorIgual, request.FiltroDataInicioMovimento));
                    sb.Append(Convert.ToDateTime(request.FiltroDataInicioMovimento).ToString("dd/MM/yyyy") + " - ");
                }
                else
                    sb.Append("Qualquer - ");

                if (request.FiltroDataFimMovimento != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroDataFimMovimento", CondicaoTipoEnum.MenorIgual, request.FiltroDataFimMovimento + ts)); //SCF1169 (+ ts)
                    sb.Append(Convert.ToDateTime(request.FiltroDataFimMovimento).ToString("dd/MM/yyyy") + "; ");
                }
                else
                    sb.Append("Qualquer; ");


                if (request.FiltroCodigoStatusCCI != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroCodigoStatusCCI", CondicaoTipoEnum.Igual, request.FiltroCodigoStatusCCI));
                    sb.Append("Status Transação = ");
                    //SCF1701 - ID 29 - Correção do Status Envio CCI
                    //sb.Append(request.FiltroCodigoStatusCCI == "S" ? "Enviadas; " : "Nao enviadas; ");
                    sb.Append(request.FiltroCodigoStatusCCI);
                }
                else
                    sb.Append("Status Transação = Todos; ");

                if (request.FiltroReferencia != null)
                {
                    condicoes.Add(new CondicaoInfo(
                        "FiltroReferencia", CondicaoTipoEnum.Igual, request.FiltroReferencia));
                    sb.Append("Codigo Referencia = ");
                    sb.Append(request.FiltroReferencia);
                }
                else
                    sb.Append("Codigo Referencia = Todos; ");

                sb.Append("Quebra Referencia = ");
                if (request.FiltroQuebraReferencia)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroQuebraReferencia", CondicaoTipoEnum.Igual, request.FiltroQuebraReferencia));
                    sb.Append("Sim; ");
                }
                else
                    sb.Append("Nao; ");

                if (!request.RetornarRelatorio)
                {
                    //1353
                    //Mensageria.Processar<SalvarLogResponse>(
                    //    new SalvarLogRequest()
                    //    {
                    //        CodigoUsuario = "",
                    //        Descricao = sb.ToString().Replace(";",","),
                    //        TipoOrigem = "APP",
                    //        CodigoOrigem = codLog
                    //    });
                }

                // CodLog
                condicoes.Add(new CondicaoInfo("CodLog", CondicaoTipoEnum.Igual, codLog));

                if (request.VerificarQuantidadeLinhas == true)
                {
                    ListarRelatorioConsolidadoDbResponse res = new ListarRelatorioConsolidadoDbResponse();
                    res.PreparaResposta(request);

                    try
                    {
                        //1353
                        //Mensageria.Processar<SalvarLogResponse>(
                        //    new SalvarLogRequest()
                        //    {
                        //        CodigoUsuario = "",
                        //        Descricao = "Quantidade de linhas.",
                        //        TipoOrigem = "APP",
                        //        CodigoOrigem = codLog
                        //    });

                        res = (ListarRelatorioConsolidadoDbResponse)Mensageria.Processar<ConsultarObjetosResponse<RelatorioConsolidadoInfo>>(
                                new ListarRelatorioConsolidadoDbRequest()
                                {
                                    CodigoSessao = request.CodigoSessao,
                                    VerificarQuantidadeLinhas = true,
                                    Condicoes = condicoes
                                });

                        resposta.QuantidadeLinhas = res.QuantidadeLinhas;
                        resposta.CodLog = codLog;

                        //1353
                        //Mensageria.Processar<SalvarLogResponse>(
                        //    new SalvarLogRequest()
                        //    {
                        //        CodigoUsuario = "",
                        //        Descricao = "Retornando a Quantidade de linhas: " + resposta.QuantidadeLinhas + " linhas.",
                        //        TipoOrigem = "APP",
                        //        CodigoOrigem = codLog
                        //    });

                        return resposta;
                    }
                    catch (Exception ex)
                    {
                        // Trata erro
                        resposta.ProcessarExcessao(ex);
                    }
                }
                else
                {
                    //1353
                    //Mensageria.Processar<SalvarLogResponse>(
                    //    new SalvarLogRequest()
                    //    {
                    //        CodigoUsuario = "",
                    //        Descricao = "Fazendo a chamada do banco.",
                    //        TipoOrigem = "APP",
                    //        CodigoOrigem = codLog
                    //    });

                    // Solicita a lista
                    resposta.Resultado = PersistenciaHelper.Listar<RelatorioConsolidadoInfo>(request.CodigoSessao, condicoes);
                    resposta.CodLog = codLog;

                    //1353
                    //Mensageria.Processar<SalvarLogResponse>(
                    //    new SalvarLogRequest()
                    //    {
                    //        CodigoUsuario = "",
                    //        Descricao = "Retornando a resposta do banco.",
                    //        TipoOrigem = "APP",
                    //        CodigoOrigem = codLog
                    //    });
                }
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        #endregion

        #region Pagamentos





        /// <summary>
        /// Lista os pagamentos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarPagamentoResponse ListarPagamento(ListarPagamentoRequest request)
        {
            // Prepara resposta
            ListarPagamentoResponse resposta = new ListarPagamentoResponse();

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //if (request.ListarPagamentoNegativo != null)
                //    condicoes.Add(
                //        new CondicaoInfo(
                //            "ListarPagamentoNegativo", CondicaoTipoEnum.Igual, request.ListarPagamentoNegativo));

                if (request.FiltroDataPagamentoInicio != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataPagamento", CondicaoTipoEnum.MaiorIgual, request.FiltroDataPagamentoInicio.Value.ToString("yyyy-MM-dd")));

                if (request.FiltroDataPagamentoFim != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataPagamento", CondicaoTipoEnum.MenorIgual, request.FiltroDataPagamentoFim.Value.ToString("yyyy-MM-dd")));

                // DataInclusao inicial
                if (request.FiltroDataPagamento.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataPagamento",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataPagamento.Value));

                if (request.FiltroDataLiquidacao != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataLiquidacao", CondicaoTipoEnum.Igual, request.FiltroDataLiquidacao));


                if (request.FiltroStatusPagamento != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroStatusPagamento", CondicaoTipoEnum.Igual, request.FiltroStatusPagamento));


                if (request.FiltroCodigoFavorecido != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroCodigoFavorecido", CondicaoTipoEnum.Igual, request.FiltroCodigoFavorecido));

                if (request.FiltroDataGeracaoDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataGeracaoDe", CondicaoTipoEnum.MaiorIgual, request.FiltroDataGeracaoDe.Value.ToString("yyyy-MM-dd")));

                if (request.FiltroDataGeracaoAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataGeracaoAte", CondicaoTipoEnum.MenorIgual, request.FiltroDataGeracaoAte.Value.ToString("yyyy-MM-dd")));

                if (request.FiltroDataEnvioDe.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataEnvioDe", CondicaoTipoEnum.MaiorIgual, request.FiltroDataEnvioDe.Value.ToString("yyyy-MM-dd")));

                if (request.FiltroDataEnvioAte.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataEnvioAte", CondicaoTipoEnum.MenorIgual, request.FiltroDataEnvioAte.Value.ToString("yyyy-MM-dd")));

                if (request.FiltroBanco != null && request.FiltroBanco != "")
                    condicoes.Add(
                        new CondicaoInfo(
                            "Banco", CondicaoTipoEnum.Igual, request.FiltroBanco));

                if (request.FiltroAgencia != null && request.FiltroAgencia != "")
                    condicoes.Add(
                        new CondicaoInfo(
                            "Agencia", CondicaoTipoEnum.Igual, request.FiltroAgencia));

                if (request.FiltroConta != null && request.FiltroConta != "")
                    condicoes.Add(
                        new CondicaoInfo(
                            "Conta", CondicaoTipoEnum.Igual, request.FiltroConta));


                if (request.RetornaApenasQuantidade)  // if (request.RetornaApenasQuantidade != null) //warning 25/02/2019
                    condicoes.Add(
                        new CondicaoInfo(
                            "RetornaApenasQuantidade", CondicaoTipoEnum.Igual, request.RetornaApenasQuantidade));





                if (request.RetornaApenasQuantidade)
                {

                    // Solicita a lista retornando apenas a qtd
                    ListarPagamentoQtdeDbResponse res = new ListarPagamentoQtdeDbResponse();
                    res = (ListarPagamentoQtdeDbResponse)Mensageria.Processar<ConsultarObjetosResponse<PagamentoInfo>>(
                            new ListarPagamentoQtdeDbRequest()
                            {
                                CodigoSessao = request.CodigoSessao,
                                Condicoes = condicoes
                            });
                    resposta.Quantidade = res.QuantidadeLinhas;
                }

                else
                    // Solicita a lista
                    resposta.Resultado =
                        PersistenciaHelper.Listar<PagamentoInfo>(
                            request.CodigoSessao, condicoes);

            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /* 2017-MELHORIAS - RETIRANDO ESSE SERVICO, POIS O CONCEITO DE PAGAMENTOS MUDOU
        /// <summary>
        /// Liquidar pagamentos negativo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public LiquidarPagamentoNegativoResponse LiquidarPagamentoNegativo(LiquidarPagamentoNegativoRequest request)
        {
            // Prepara resposta
            LiquidarPagamentoNegativoResponse resposta = new LiquidarPagamentoNegativoResponse();


            // Bloco de controle
            try
            {
                // criar a mensagem de DB request
                LiquidarPagamentoNegativoDbRequest liquidarDb = new LiquidarPagamentoNegativoDbRequest();
                liquidarDb.CodigoPagamento = request.codigoPagamento;
                liquidarDb.DataLiquidacao = request.dataLiquidacao;

                // Pede inicializacao do servico de seguranca
                Mensageria.Processar(liquidarDb);

            }
            catch (Exception ex)
            {
                // Trata o erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        */

        // DESABILITANDO A CHAMADA DO REMOVER PAGAMENTO DIRETAMENTE
        // ESSA CHAMADA PRECISA SER FEITA DE FORMA ASSINCRONA POIS IRA AFETAR A TABELA DE TRANSACOES
        //public RemoverPagamentoResponse RemoverPagamento(RemoverPagamentoRequest request)
        //{
        //    // Prepara resposta
        //    RemoverPagamentoResponse resposta = new RemoverPagamentoResponse();
        //    resposta.PreparaResposta(request);

        //    try
        //    {
        //        // Bloco de controle
        //        try
        //        {
        //            // Remove
        //            PersistenciaHelper.Remover<PagamentoInfo>(
        //                request.CodigoSessao, request.CodigoPagamento);
        //        }
        //        catch (Exception ex)
        //        {
        //            // Informa erro na resposta
        //            resposta.ProcessarExcessao(ex);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        // Informa erro na resposta
        //        resposta.ProcessarExcessao(ex);
        //    }

        //    // Retorna 
        //    return resposta;
        //}


        /// <summary>
        /// Gera o processo para a exclusao de pagamentos já gerados 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ProcessarExclusaoPagamentosResponse ProcessarExclusaoPagamentos(ProcessarExclusaoPagamentosRequest request)
        {

            //   ESTE PROCESSO CHAMA A PROCEDURE QUE IRÁ ATUALIZAR O STATUS DO PAGAMENTO PARA EXCLUIDO E
            //   RETIRAR  CODIGO DESSE PAGAMENTO DA TABELA DE TRANSACOES
            //   COMO É UM PROCESSO QUE IRÁ ATUALIZAR A TABELA DE TRANSACOES, PRECISA SE FEITO DE FORMA ASSINCRONA

            // prepara a resposta
            ProcessarExclusaoPagamentosResponse resposta = new ProcessarExclusaoPagamentosResponse();
            resposta.PreparaResposta(request);

            ProcessoExclusaoPagamentoInfo processoInfo = null;

            if (request.CodigoProcesso == null)
            {
                // cria o processo de exportação
                processoInfo = new ProcessoExclusaoPagamentoInfo() { };
                processoInfo.CodigoPagamento = request.CodigoPagamento;

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                // Executa o processo assíncrono 
                RemoverPagamentoResponse resp2 = removerPagamentoAssincrono(
                    new RemoverPagamentoRequest
                    {
                        CodigoPagamento = request.CodigoPagamento,
                        CodigoProcesso = request.CodigoProcesso
                    });


            }

            // retorna a resposta
            return resposta;
        }

        public RemoverPagamentoResponse removerPagamentoAssincrono(RemoverPagamentoRequest request)
        {

            //   ESTE PROCESSO CHAMA A PROCEDURE QUE IRÁ ATUALIZAR O STATUS DO PAGAMENTO PARA EXCLUIDO E
            //   RETIRAR  CODIGO DESSE PAGAMENTO DA TABELA DE TRANSACOES
            //   COMO É UM PROCESSO QUE IRÁ ATUALIZAR A TABELA DE TRANSACOES, PRECISA SE FEITO DE FORMA ASSINCRONA


            // Prepara resposta
            RemoverPagamentoResponse resposta = new RemoverPagamentoResponse();
            resposta.PreparaResposta(request);

            // Gerar log de inicio de processo
            Mensageria.Processar<SalvarLogResponse>(
                new SalvarLogRequest()
                {
                    CodigoUsuario = "",
                    Descricao = String.Format("Iniciando o processo de exclusão do Pagamento {0} .", request.CodigoPagamento),
                    TipoOrigem = "ProcessoInfo",
                    CodigoOrigem = request.CodigoProcesso,
                    CodigoSessao = request.CodigoSessao
                });

            try
            {
                // Bloco de controle
                try
                {
                    // Remove
                    PersistenciaHelper.Remover<PagamentoInfo>(
                        request.CodigoSessao, request.CodigoPagamento);

                    // Gerar log de inicio de processo
                    Mensageria.Processar<SalvarLogResponse>(
                        new SalvarLogRequest()
                        {
                            CodigoUsuario = "",
                            Descricao = "Processo de exclusão do Pagamento finalizado.",
                            TipoOrigem = "ProcessoInfo",
                            CodigoOrigem = request.CodigoProcesso,
                            CodigoSessao = request.CodigoSessao
                        });
                }
                catch (Exception ex)
                {
                    // Informa erro na resposta
                    resposta.ProcessarExcessao(ex);
                }

            }
            catch (Exception ex)
            {
                // Gerar log de inicio de processo
                Mensageria.Processar<SalvarLogResponse>(
                    new SalvarLogRequest()
                    {
                        CodigoUsuario = "",
                        Descricao = String.Format("Ocorreu o erro {0} = {1} .", ex.Message, ex.InnerException),
                        TipoOrigem = "ProcessoInfo",
                        CodigoOrigem = request.CodigoProcesso,
                        CodigoSessao = request.CodigoSessao
                    });

                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        #endregion

        #region Transacao

        /// <summary>
        /// Lista de transações
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarTransacaoResponse ListarTransacao(ListarTransacaoRequest request)
        {
            // Prepara reposta
            ListarTransacaoResponse resposta = new ListarTransacaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // Status Retorno Cessao
                if (request.FiltroStatusRetornoCessao.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "StatusRetornoCessao", CondicaoTipoEnum.Igual, request.FiltroStatusRetornoCessao.Value));

                // Status Retorno Cessao
                if (request.FiltroRetornoCessao)
                    condicoes.Add(
                        new CondicaoInfo(
                            "RetornoCessao", CondicaoTipoEnum.Igual, request.FiltroRetornoCessao));

                // Data inclusao maior
                if (request.FiltroDataInclusaoMaior.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataInclusao", CondicaoTipoEnum.Maior, request.FiltroDataInclusaoMaior.Value));

                // Data inclusao menor
                if (request.FiltroDataInclusaoMenor.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataInclusao", CondicaoTipoEnum.Menor, request.FiltroDataInclusaoMenor.Value));

                if (!String.IsNullOrEmpty(request.FiltroCodigoProcesso))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoProcesso", CondicaoTipoEnum.Igual, request.FiltroCodigoProcesso));

                if (!String.IsNullOrEmpty(request.Chave))
                    condicoes.Add(
                        new CondicaoInfo(
                            "Chave", CondicaoTipoEnum.Igual, request.Chave));

                if (!String.IsNullOrEmpty(request.Chave2))
                    condicoes.Add(
                        new CondicaoInfo(
                            "Chave2", CondicaoTipoEnum.Igual, request.Chave2));

                // Pede a lista para a persistencia
                resposta.Resultado =
                    PersistenciaHelper.Listar<TransacaoResumoInfo>(
                        request.CodigoSessao, condicoes, request.MaxLinhas);
            }
            catch (Exception ex)
            {
                // Processa excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        /// <summary>
        /// Lista de arquivos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarArquivoResponse ListarArquivo(ListarArquivoRequest request)
        {
            // Prepara a resposta
            ListarArquivoResponse resposta = new ListarArquivoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta lista de condicoes
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                // Filtra por data de inclusão
                if (request.FiltroDataInclusao.HasValue)
                    condicoes.Add(new CondicaoInfo(
                        "DataInclusao", CondicaoTipoEnum.Igual, request.FiltroDataInclusao.Value));

                // Filtra por data de inclusão
                if (request.FiltroStatusArquivo != null)
                    condicoes.Add(new CondicaoInfo(
                        "StatusArquivo", CondicaoTipoEnum.Igual, request.FiltroStatusArquivo));

                // Filtra por codigo do processo
                if (request.FiltroCodigoProcesso != null)
                    condicoes.Add(new CondicaoInfo(
                        "CodigoProcesso", CondicaoTipoEnum.Igual, request.FiltroCodigoProcesso));

                // Pede a lista de arquivos
                resposta.Resultado =
                    PersistenciaHelper.Listar<ArquivoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata a excessao
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #region UsuarioSCF


        //=================================================================================================================
        //1356 INICIO CENTRALIZANDO CONSISTENCIA DA SENHA
        //=================================================================================================================
        public SalvarUsuarioSCFResponse SalvarUsuarioSCF(SalvarUsuarioSCFRequest request)
        {
            SalvarUsuarioSCFResponse resposta = new SalvarUsuarioSCFResponse();
            SalvarUsuarioResponse respostaFramework = new SalvarUsuarioResponse();
            UsuarioInfo usuarioInfo = new UsuarioInfo();
            string vsenha = request.UsuarioSCF.Senha;
            request.UsuarioSCF.Senha = "";
            try
            {
                respostaFramework.usuarioInfo =
                Mensageria.Processar<SalvarUsuarioResponse>
                (new SalvarUsuarioRequest()
                    {
                        CodigoSessao = request.CodigoSessao
                    ,
                        Novo = request.Novo
                    ,
                        Usuario = request.UsuarioSCF
                    }
                ).usuarioInfo;
                AlterarSenhaResponse respostasenha = Mensageria.Processar<AlterarSenhaResponse>(new AlterarSenhaRequest() { CodigoSessao = request.CodigoSessao, CodigoUsuario = request.UsuarioSCF.CodigoUsuario, NovaSenha = vsenha });
                if (respostasenha.Criticas.Count > 0)
                    resposta.Criticas = respostasenha.Criticas;
            }
            catch (Exception ex)
            {
                resposta.ProcessarExcessao(ex);
            }
            finally
            {
                resposta.usuarioSCFInfo = (UsuarioSCFInfo)respostaFramework.usuarioInfo;
            }
            return resposta;
        }
        //=================================================================================================================
        //1356 FIM CENTRALIZANDO CONSISTENCIA DA SENHA
        //=================================================================================================================

        #endregion

        /// <summary>
        /// Faz a inicialização do sistema SCF
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public InicializarSCFResponse InicializarSCF(InicializarSCFRequest request)
        {
            // Prepara resposta
            InicializarSCFResponse resposta = new InicializarSCFResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        //=======================================================================================================================================
        public EstornarLoteExtratoResponse EstornarLoteExtrato(EstornarLoteExtratoRequest req)
        {
            EstornarLoteExtratoResponse res = new EstornarLoteExtratoResponse();
            try
            {
                //=======================================================================================================================================
                // Carrega dados do processo
                //=======================================================================================================================================
                ProcessoInfo processoInfo = this.ReceberProcesso(new ReceberProcessoRequest()
                {
                    CodigoSessao = req.CodigoSessao
                ,
                    CodigoProcesso = req.CodigoProcesso
                }).ProcessoInfo;
                //=======================================================================================================================================
                // Estorno EXTRATO
                //=======================================================================================================================================
                if (processoInfo.TipoProcesso.ToLower() == "processoextratoinfo" ||
                        processoInfo.TipoProcesso.ToLower() == "processoatacadaoinfo"
                    )
                {
                    Procedures.PR_ESTORNO_EXTRATO_S(req.CodigoProcesso);
                    res.DescricaoResposta = "Estorno Extrato " + req.CodigoProcesso + " executado com sucesso."; //1349
                }
                //=======================================================================================================================================
                // Estorno CESSAO
                //=======================================================================================================================================
                else if
                    (processoInfo.TipoProcesso.ToLower() == "processocessaoinfo"
                    )
                {
                    Procedures.PR_ESTORNO_RETORNO_CESSAO_S(req.CodigoProcesso);
                    res.DescricaoResposta = "Estorno Cessao " + req.CodigoProcesso + " executado com sucesso."; //1349
                }
                //1349 INIBINDO
                //=======================================================================================================================================
                // REFAENDO AGENDA
                //=======================================================================================================================================
                //var resultado = ListarProcesso(new ListarProcessoRequest()
                //{
                //    FiltroStatusProcesso = ProcessoStatusEnum.Finalizado
                //});
                //List<ProcessoResumoInfo> processos = new List<ProcessoResumoInfo>();
                //var r = from p in resultado.Resultado
                //        where p.CodigoProcesso != req.CodigoProcesso
                //        select p;

                //foreach (var item in r)
                //{
                //    AgendarPagamentoExtratoRequest reqAgenda = new AgendarPagamentoExtratoRequest();
                //    reqAgenda.CodigoProcesso = item.CodigoProcesso;
                //    reqAgenda.CodigoSessao = req.CodigoSessao;
                //    AgendarPagamentoExtratoResponse resAgenda = Mensageria.Processar<AgendarPagamentoExtratoResponse>(reqAgenda);
                //}
                //LogSCFProcessoInfo lLog = new LogSCFProcessoInfo();
                //lLog.setCodigoProcesso(req.CodigoProcesso);
                //lLog.Descricao = "Estorno executado com sucesso.";
                //LogHelper.Salvar(req.CodigoSessao, lLog);
                //1349 INIBINDO
            }
            catch (Exception ex)
            {
                LogSCFProcessoInfo lLog = new LogSCFProcessoInfo();
                lLog.setCodigoProcesso(req.CodigoProcesso);
                //lLog.Descricao = "Erro ao efetuar o estorno."; //1349
                lLog.Descricao = "Estorno com ERRO no PROCESSO  " + req.CodigoProcesso + " Exception [" + ex.Message + "]"; //1349
                LogHelper.Salvar(req.CodigoSessao, lLog);

                res.Erro = ex.Message;
                //res.DescricaoResposta = "Erro ao efetuar o estorno."; //1349
                res.DescricaoResposta = "Estorno com ERRO no PROCESSO  " + req.CodigoProcesso; //1349
                res.StatusResposta = MensagemResponseStatusEnum.ErroPrograma;
            }
            return res;
        }
        //=======================================================================================================================================

        public ProcessoEnviadoMateraResponse ProcessoEnviadoMatera(ProcessoEnviadoMateraRequest req)
        {
            ProcessoEnviadoMateraResponse res = new ProcessoEnviadoMateraResponse();
            res.PreparaResposta(req);

            try
            {
                res.EnviadoMatera = Procedures.PR_PROCESSO_ENVIADO_MATERA(req.CodigoProcesso);
            }
            catch (Exception ex)
            {
                res.ProcessarExcessao(ex);
            }
            return res;
        }

        public ListarProdutoPlanoResponse ListarProdutoPlano(ListarProdutoPlanoRequest request)
        {
            // monta e prepara a resposta
            ListarProdutoPlanoResponse resposta = new ListarProdutoPlanoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo status do cedivel
                if (request.FiltroCodigoPlano != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoPlano", CondicaoTipoEnum.Igual, request.FiltroCodigoPlano));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ProdutoPlanoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // retorna
            return resposta;
        }

        #region Referencia de dominio
        public ListarListaItemReferenciaResponse ListarListaItemReferencia(ListarListaItemReferenciaRequest request)
        {
            // Prepara resposta
            ListarListaItemReferenciaResponse resposta = new ListarListaItemReferenciaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo status
                if (!String.IsNullOrEmpty(request.FiltroCodigoListaItemReferencia))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoListaItemReferencia", CondicaoTipoEnum.Igual, request.FiltroCodigoListaItemReferencia));

                //Permite ao usuário buscar por situacao
                if (!String.IsNullOrEmpty(request.FiltroCodigoListaItemSituacao))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoListaItemSituacao", CondicaoTipoEnum.Igual, request.FiltroCodigoListaItemSituacao));

                //Permite ao usuário buscar por lista item (dominio)
                if (!String.IsNullOrEmpty(request.FiltroCodigoListaItem))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoListaItem", CondicaoTipoEnum.Igual, request.FiltroCodigoListaItem));

                // Permite ao usuário buscar por Código TSYS
                if (!String.IsNullOrEmpty(request.FiltroNomeReferencia))
                    condicoes.Add(
                        new CondicaoInfo(
                            "NomeReferencia", CondicaoTipoEnum.Igual, request.FiltroNomeReferencia));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ListaItemReferenciaInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Recebe dados de um item de referencia de dominios
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberListaItemReferenciaResponse ReceberListaItemReferencia(ReceberListaItemReferenciaRequest request)
        {
            // Prepara resposta
            ReceberListaItemReferenciaResponse resposta = new ReceberListaItemReferenciaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.ListaItemReferenciaInfo =
                    PersistenciaHelper.Receber<ListaItemReferenciaInfo>(
                        request.CodigoSessao, request.CodigoListaItemReferencia);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Receb dados de listaItem
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberListaItemResponse ReceberListaItem(ReceberListaItemRequest request)
        {
            // Prepara resposta
            ReceberListaItemResponse resposta = new ReceberListaItemResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.ListaItemInfo =
                    PersistenciaHelper.Receber<ListaItemInfo>(
                        request.CodigoSessao, request.FiltroCodigoListaItem);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        public SalvarListaItemReferenciaResponse SalvarListaItemReferencia(SalvarListaItemReferenciaRequest request)
        {
            // Prepara resposta
            SalvarListaItemReferenciaResponse resposta = new SalvarListaItemReferenciaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Verifica se já existe
                //ListarListaItemReferenciaResponse listaItemReferencia =
                //    Mensageria.Processar<ListarListaItemReferenciaResponse>(
                //    new ListarListaItemReferenciaRequest()
                //    {
                //        FiltroCodigoListaItem = request.ListaItemReferenciaInfo.CodigoListaItem,
                //        FiltroCodigoListaItemSituacao = request.ListaItemReferenciaInfo.CodigoListaItemSituacao
                //    });

                ListarListaItemReferenciaResponse listaItemReferencia =
                    Mensageria.Processar<ListarListaItemReferenciaResponse>(
                    new ListarListaItemReferenciaRequest()
                    {
                        FiltroCodigoListaItem = request.ListaItemReferenciaInfo.CodigoListaItem,
                        FiltroNomeReferencia = request.ListaItemReferenciaInfo.NomeReferencia
                    });

                if (listaItemReferencia.Resultado.Count > 0)
                {
                    resposta.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = "Situação e domínio já cadastrados",
                            DataCritica = DateTime.Now
                        });

                    return resposta;
                }
                else
                {
                    // Salva
                    resposta.ListaItemReferenciaInfo =
                        PersistenciaHelper.Salvar<ListaItemReferenciaInfo>(
                            request.CodigoSessao, request.ListaItemReferenciaInfo);
                }
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        public RemoverListaItemReferenciaResponse RemoverListaItemReferencia(RemoverListaItemReferenciaRequest request)
        {
            // Prepara resposta
            RemoverListaItemReferenciaResponse resposta = new RemoverListaItemReferenciaResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove
                PersistenciaHelper.Remover<ListaItemReferenciaInfo>(
                    request.CodigoSessao, request.CodigoListaItemReferencia);

                // Informa na resposta
                resposta.CodigoListaItemReferenciaRemovido = request.CodigoListaItemReferencia;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        #endregion

        public ListarListaConfiguracaoResponse ListarListaConfiguracao(ListarListaConfiguracaoRequest request)
        {
            // Prepara resposta
            ListarListaConfiguracaoResponse resposta = new ListarListaConfiguracaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                if (!String.IsNullOrEmpty(request.FiltroCodigoListaConfiguracao))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoListaConfiguracao", CondicaoTipoEnum.Igual, request.FiltroCodigoListaConfiguracao));

                if (!String.IsNullOrEmpty(request.FiltroCodigoListaItem))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoListaItem", CondicaoTipoEnum.Igual, request.FiltroCodigoListaItem));

                if (!String.IsNullOrEmpty(request.FiltroCodigoLista))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoLista", CondicaoTipoEnum.Igual, request.FiltroCodigoLista));

                if (!String.IsNullOrEmpty(request.FiltroCodigoEmpresaGrupo))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoEmpresaGrupo", CondicaoTipoEnum.Igual, request.FiltroCodigoEmpresaGrupo));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ListaConfiguracaoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;

        }

        public SalvarListaConfiguracaoResponse SalvarListaConfiguracao(SalvarListaConfiguracaoRequest request)
        {
            // Prepara resposta
            SalvarListaConfiguracaoResponse resposta = new SalvarListaConfiguracaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.ListaConfiguracaoInfo =
                    PersistenciaHelper.Salvar<ListaConfiguracaoInfo>(
                        request.CodigoSessao, request.Objeto);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;

        }

        public ReceberListaConfiguracaoResponse ReceberListaConfiguracao(ReceberListaConfiguracaoRequest request)
        {
            // Prepara resposta
            ReceberListaConfiguracaoResponse resposta = new ReceberListaConfiguracaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.ListaConfiguracaoInfo =
                    PersistenciaHelper.Receber<ListaConfiguracaoInfo>(
                        request.CodigoSessao, request.CodigoObjeto);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        public RemoverListaConfiguracaoResponse RemoverListaConfiguracao(RemoverListaConfiguracaoRequest request)
        {
            // Prepara resposta
            RemoverListaConfiguracaoResponse resposta = new RemoverListaConfiguracaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove
                PersistenciaHelper.Remover<ListaConfiguracaoInfo>(
                    request.CodigoSessao, request.CodigoConfiguracaoLista);

                // Informa na resposta
                resposta.CodigoListaConfiguracaoRemovido = request.CodigoConfiguracaoLista;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        public ListarListaItemConfiguracaoResponse ListarListaItemConfiguracao(ListarListaItemConfiguracaoRequest request)
        {
            // Prepara resposta
            ListarListaItemConfiguracaoResponse resposta = new ListarListaItemConfiguracaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                if (!String.IsNullOrEmpty(request.FiltroCodigoLista))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoLista", CondicaoTipoEnum.Igual, request.FiltroCodigoLista));

                if (!String.IsNullOrEmpty(request.FiltroCodigoEmpresaGrupo))
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoEmpresaGrupo", CondicaoTipoEnum.Igual, request.FiltroCodigoEmpresaGrupo));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<ListaConfiguracaoItemInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;

        }

        public SalvarListaItemConfiguracaoResponse SalvarListaItemConfiguracao(SalvarListaItemConfiguracaoRequest request)
        {
            // Prepara resposta
            SalvarListaItemConfiguracaoResponse resposta = new SalvarListaItemConfiguracaoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                /// Verifica se nao existe o valor

                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                foreach (ListaConfiguracaoItemInfo listaItemConfiguracao in request.Itens)
                {
                    // Salva
                    resposta.Retorno =
                        PersistenciaHelper.Salvar<ListaConfiguracaoItemInfo>(
                            request.CodigoSessao, listaItemConfiguracao);
                }
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;

        }

        #region Taxa de Recebimento
        /// <summary>
        /// Lista as Taxas de Recebimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarTaxaRecebimentoResponse ListarTaxaRecebimento(ListarTaxaRecebimentoRequest request)
        {
            // Prepara resposta
            ListarTaxaRecebimentoResponse resposta = new ListarTaxaRecebimentoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pelo codigo do Grupo
                if (request.FiltroCodigoGrupo != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoGrupo", CondicaoTipoEnum.Igual, request.FiltroCodigoGrupo));

                //Permite ao usuário buscar pelo codigo da Referencia
                if (request.FiltroCodigoReferencia != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoReferencia", CondicaoTipoEnum.Igual, request.FiltroCodigoReferencia));

                //Permite ao usuário buscar pelo codigo do Meio de Pagamento
                if (request.FiltroCodigoMeioPagamento != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoMeioPagamento", CondicaoTipoEnum.Igual, request.FiltroCodigoMeioPagamento));

                //Permite ao usuário buscar pela Data Inicio
                if (request.FiltroDataInicio != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataInicio", CondicaoTipoEnum.Igual, request.FiltroDataInicio));

                //Permite ao usuário buscar pela Data Fim
                if (request.FiltroDataFim != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "DataFim", CondicaoTipoEnum.Igual, request.FiltroDataFim));

                //Permite ao usuário buscar pelo codigo da Taxa de Recebimento
                if (request.FiltroCodigoTaxaRecebimento != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "CodigoTaxaRecebimento", CondicaoTipoEnum.Igual, request.FiltroCodigoTaxaRecebimento));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<TaxaRecebimentoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Receber uma Taxa de Recebimento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ReceberTaxaRecebimentoResponse ReceberTaxaRecebimento(ReceberTaxaRecebimentoRequest request)
        {
            // Prepara resposta
            ReceberTaxaRecebimentoResponse resposta = new ReceberTaxaRecebimentoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Retorna o item solicitado
                resposta.TaxaRecebimentoInfo =
                    PersistenciaHelper.Receber<TaxaRecebimentoInfo>(
                        request.CodigoSessao, request.CodigoTaxaRecebimento);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        /// <summary>
        /// Remove um subgrupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoverTaxaRecebimentoResponse RemoverTaxaRecebimento(RemoverTaxaRecebimentoRequest request)
        {
            // Prepara resposta
            RemoverTaxaRecebimentoResponse resposta = new RemoverTaxaRecebimentoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Remove
                PersistenciaHelper.Remover<TaxaRecebimentoInfo>(
                    request.CodigoSessao, request.CodigoTaxaRecebimento);

                // Informa na resposta
                resposta.CodigoTaxaRecebimento = request.CodigoTaxaRecebimento;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna 
            return resposta;
        }

        /// <summary>
        /// Salva subgrupo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SalvarTaxaRecebimentoResponse SalvarTaxaRecebimento(SalvarTaxaRecebimentoRequest request)
        {
            // Prepara resposta
            SalvarTaxaRecebimentoResponse resposta = new SalvarTaxaRecebimentoResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Salva
                resposta.TaxaRecebimentoInfo =
                    PersistenciaHelper.Salvar<TaxaRecebimentoInfo>(
                        request.CodigoSessao, request.TaxaRecebimentoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        public GerarRelatorioRecebimentoExcelResponse GerarRelatorioRecebimentoExcel(GerarRelatorioRecebimentoExcelRequest request)
        {
            // prepara a resposta
            GerarRelatorioRecebimentoExcelResponse resposta = new GerarRelatorioRecebimentoExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                ProcessoRelatorioRecebimentoExcelInfo processoInfo =
                    new ProcessoRelatorioRecebimentoExcelInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                resposta = gerarRelatorioRecebimentoExcel(request);
            }

            // retorna a resposta
            return resposta;
        }
        private GerarRelatorioRecebimentoExcelResponse gerarRelatorioRecebimentoExcel(GerarRelatorioRecebimentoExcelRequest request)
        {
            // Prepara resposta
            GerarRelatorioRecebimentoExcelResponse respostaRelatorioExcel = new GerarRelatorioRecebimentoExcelResponse();
            respostaRelatorioExcel.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            var filtros = request.Request;
            // gera a string 
            StringBuilder sb = new StringBuilder();
            sb.Append("Filtros:  Grupo = ");
            sb.Append(filtros.NomeGrupo + "; ");

            sb.Append("Referência = ");
            sb.Append(filtros.DescricaoReferencia + "; ");

            sb.Append("Meio de Pagamento = ");
            sb.Append(filtros.DescricaoMeioPagamento + "; ");

            sb.Append("Período de Agendamento de ");
            if (filtros.FiltroDataRepasseInicio != null && filtros.FiltroDataRepasseInicio != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(filtros.FiltroDataRepasseInicio).ToString("dd/MM/yyyy"));
            else
                sb.Append("Qualquer");

            sb.Append(" até ");

            if (filtros.FiltroDataRepasseFim != null && filtros.FiltroDataRepasseFim != DateTime.MinValue)
                sb.Append(Convert.ToDateTime(filtros.FiltroDataRepasseFim).ToString("dd/MM/yyyy") + "; ");
            else
                sb.Append("Qualquer; ");

            StreamWriter file = null;
            try
            {
                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Iniciando validações", request.CodigoSessao, request.CodigoProcesso);
                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento == "")
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioExcel.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);
                    return respostaRelatorioExcel;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento.Length - 1) == @"\")
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento;
                    else
                        respostaRelatorioExcel.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento + "\\";
                }
                // pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                DateTime dataAtual = DateTime.Now;

                string nome = "";
                if (usuarioInfo.Nome.Contains(" "))
                    nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                else
                    nome = usuarioInfo.Nome;

                string caminho = respostaRelatorioExcel.CaminhoArquivoGerado
                    + nome + @"_RelRecebimento_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + ".csv";

                if (!System.IO.Directory.Exists(respostaRelatorioExcel.CaminhoArquivoGerado))
                {
                    respostaRelatorioExcel.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioExcel.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioExcel.CaminhoArquivoGerado + "]";
                    respostaRelatorioExcel.DescricaoResposta = respostaRelatorioExcel.Erro;

                    respostaRelatorioExcel.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioExcel.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioExcel.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioExcel;
                }
                else
                {
                    file = new StreamWriter(caminho, true, Encoding.Default);

                    try
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Arquivo criado:" + caminho, request.CodigoSessao, request.CodigoProcesso);
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Realizando consulta dos Recebimentos no banco de dados", request.CodigoSessao, request.CodigoProcesso);
                    }
                    catch (Exception e)
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Excecao:" + e.InnerException, request.CodigoSessao, request.CodigoProcesso);
                    }
                    // Pede o relatório
                    ListarRelatorioRecebimentoResponse resposta =
                        Mensageria.Processar<ListarRelatorioRecebimentoResponse>(
                            new ListarRelatorioRecebimentoRequest()
                            {
                                CodigoSessao = request.CodigoSessao,
                                FiltroGrupo = filtros.FiltroGrupo,
                                FiltroReferencia = filtros.FiltroReferencia,
                                FiltroMeioPagamento = filtros.FiltroMeioPagamento,
                                FiltroDataRepasseInicio = request.Request.FiltroDataRepasseInicio,
                                FiltroDataRepasseFim = request.Request.FiltroDataRepasseFim + ts,
                                TipoRelatorio = "Excel"
                            });

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Consulta dos Recebimentos finalizada", request.CodigoSessao, request.CodigoProcesso);

                    // adiciona os filtros
                    file.WriteLine(sb.ToString());
                    // Informa os filtros da consulta
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(sb.ToString().Replace(";", ","), request.CodigoSessao, request.CodigoProcesso);
                    String dados;
                    dados = "Grupo;Referência;CNPJ;Estabelecimento;Data de Agendamento;Meio de Pagamento;Qtde de Recebimentos;Valor de Recebimentos;Valor Taxa Recebimento;Taxa Recebimento";

                    file.WriteLine(dados);

                    // Quantidade de linhas para log
                    int quantidadeLog = 0;

                    foreach (RelatorioRecebimentoInfo relatorioRecebimento in resposta.Resultado)
                    {
                        // ==========================================================================================
                        // Adicionado "=\" nos campos que contem números mas precisam ser expressos como textos
                        // ==========================================================================================
                        string taxaRecebimento;
                        if (relatorioRecebimento.TipoTaxaRecebimento == "%")
                            taxaRecebimento = String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioRecebimento.TaxaRecebimento) + relatorioRecebimento.TipoTaxaRecebimento;
                        else
                            taxaRecebimento = relatorioRecebimento.TipoTaxaRecebimento + " " + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioRecebimento.TaxaRecebimento);

                        dados = relatorioRecebimento.NomeGrupo + ";"
                            + "=\"" + relatorioRecebimento.DescricaoReferencia + "\";"
                            + "=\"" + relatorioRecebimento.Estabelecimento.CNPJ + "\";"
                            + relatorioRecebimento.Estabelecimento.RazaoSocial + ";"
                            + String.Format("{0:dd/MM/yyyy}", relatorioRecebimento.DataAgendamento) + ";"
                            + relatorioRecebimento.DescricaoMeioPagamento + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioRecebimento.QtdeRecebimento) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioRecebimento.ValorRecebimento) + ";"
                            + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioRecebimento.ValorTaxaRecebimento) + ";"
                            + taxaRecebimento + ";";

                        file.WriteLine(dados);

                        // Atualiza a quantidade de linhas geradas
                        respostaRelatorioExcel.QuantidadeLinhasGeradas++;
                        quantidadeLog++;

                        // Sinaliza
                        if (quantidadeLog >= 15000)
                        {
                            // Sinaliza
                            LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                                respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                            // Zera linhas do log
                            quantidadeLog = 0;
                        }

                    }
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Total de linhas geradas: " + respostaRelatorioExcel.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);
                }

            }
            catch (Exception ex)
            {
                // Trata erro
                respostaRelatorioExcel.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioExcel;
        }
        public GerarRelatorioRecebimentoPDFResponse GerarRelatorioRecebimentoPDF(GerarRelatorioRecebimentoPDFRequest request)
        {
            // prepara a resposta
            GerarRelatorioRecebimentoPDFResponse resposta = new GerarRelatorioRecebimentoPDFResponse();
            resposta.PreparaResposta(request);

            ProcessoRelatorioRecebimentoPDFInfo processoInfo = null;

            if (request.CodigoProcesso == null)
            {
                // cria o processo de geracao do relatorio
                processoInfo =
                    new ProcessoRelatorioRecebimentoPDFInfo()
                    {
                        Param = request.Request
                    };

                // executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // informa o codigo do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;
            }
            else
            {
                // Executa o processo assíncrono              
                resposta = gerarRelatorioRecebimentoPDF(request);
            }

            // retorna a resposta
            return resposta;
        }
        private GerarRelatorioRecebimentoPDFResponse gerarRelatorioRecebimentoPDF(GerarRelatorioRecebimentoPDFRequest request)
        {
            // Prepara resposta
            GerarRelatorioRecebimentoPDFResponse respostaRelatorioPdf = new GerarRelatorioRecebimentoPDFResponse();
            respostaRelatorioPdf.PreparaResposta(request);

            StreamWriter file = null;
            try
            {
                // Carrefa as configurações do sistema
                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                // Monto o caminho e o nome do arquivo que será gerado.
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento == "")
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não está configurado.";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = respostaRelatorioPdf.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    //respostaRelatorioExcel.Erro = "O diretório de exportação do relatório não está configurado.";
                    return respostaRelatorioPdf;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento.Length - 1) == @"\")
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento;
                    else
                        respostaRelatorioPdf.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioRecebimento + "\\";
                }

                UsuarioInfo usuarioInfo = new UsuarioInfo();

                DateTime dataAtual = DateTime.Now;
                string nome = string.Empty;

                //Caso o codigo de sessao seja nulo, atribui um nome do usuario "Automatico", pois se trata da execução do processo
                //LiberarTransacoesEmAndamento executado quando o servidor de aplicação é reiniciado.
                if (request.CodigoSessao != null)
                {
                    // pega o usuario para recuperar o nome e colocar no arquivo gerado
                    usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                    if (usuarioInfo.Nome.Contains(" "))
                        nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                    else
                        nome = usuarioInfo.Nome;
                }
                else
                {
                    //Atribui o nome Automatico pois ainda não existe Sessão válida
                    nome = "Automatico";
                }
                //Fim

                string caminho = respostaRelatorioPdf.CaminhoArquivoGerado
                    + nome + @"_RelRecebimento_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + (request.Request.TipoArquivo == null ? ".csv" : ".pdf");

                if (!System.IO.Directory.Exists(respostaRelatorioPdf.CaminhoArquivoGerado))
                {
                    respostaRelatorioPdf.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    respostaRelatorioPdf.Erro = "O relatório não foi gerado, pois o diretório de exportação do relatório não existe. [" + respostaRelatorioPdf.CaminhoArquivoGerado + "]";
                    respostaRelatorioPdf.DescricaoResposta = respostaRelatorioPdf.Erro;

                    respostaRelatorioPdf.Criticas.Add(
                    new CriticaInfo()
                    {
                        Descricao = respostaRelatorioPdf.Erro,
                        DataCritica = DateTime.Now
                    });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        respostaRelatorioPdf.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return respostaRelatorioPdf;
                }

                request.Request.Link = request.Request.Link + "&codLog=" + request.CodigoProcesso;
                string urlAddress = request.Request.Link;
                string strCSS = null;

                ServicePointManager.ServerCertificateValidationCallback =
                    new System.Net.Security.RemoteCertificateValidationCallback(
                        (object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors) =>
                        {
                            return true;
                        });

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                webRequest.Timeout = 1000 * 60 * 60 * 5; // 5 horas
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0";
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.AllowAutoRedirect = true;
                webRequest.ContentType = "application/x-www-form-urlencoded";

                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioRecebimentoPDF - Efetuando requisição WEB: " + request.Request.Link, request.CodigoSessao, request.CodigoProcesso);

                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

                string data = "";
                if (webResponse.StatusCode == HttpStatusCode.OK)
                {
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                       "Iniciando conversão do HTML para PDF. ", request.CodigoSessao, request.CodigoProcesso);

                    Stream receiveStream = webResponse.GetResponseStream();
                    StreamReader readStream = null;

                    if (webResponse.CharacterSet == null)
                    {
                        readStream = new StreamReader(receiveStream);
                    }
                    else
                    {
                        readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    }

                    data = readStream.ReadToEnd();
                    if (data != "")
                    {
                        // Recupera Estilos do CSS para aplicar no relatório PDF
                        urlAddress = urlAddress.Replace(".aspx", ".css");
                        int posIni = urlAddress.IndexOf("?");
                        urlAddress = urlAddress.Substring(0, posIni);

                        posIni = data.IndexOf("link href");
                        if (posIni > 0)
                        {
                            int posFim = data.IndexOf(".css", posIni) + 3;
                            string urlAddressCss = data.Substring(posIni + 11, posFim - posIni - 10);

                            posFim = urlAddress.LastIndexOf(@"/");
                            urlAddress = urlAddress.Substring(0, posFim + 1) + urlAddressCss;
                        }

                        try
                        {
                            webRequest = (HttpWebRequest)WebRequest.Create(urlAddress);
                            webResponse = (HttpWebResponse)webRequest.GetResponse();

                            receiveStream = null;
                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                receiveStream = webResponse.GetResponseStream();
                                strCSS = new StreamReader(receiveStream, Encoding.UTF8).ReadToEnd();
                            }
                        }
                        catch (Exception) { } // catch (Exception ex) {/* ignorar erro caso não tenha CSS */} // warning 25/02/219
                    }

                    webResponse.Close();
                    readStream.Close();
                }

                if (data != "")
                {
                    //Create a byte array that will eventually hold our final PDF
                    Byte[] bytes;
                    ITextEvents ev = new ITextEvents(null, null);

                    //Gerar o PDF a partir do HTML
                    bytes = ev.ImprimirPDF(data, strCSS, "A4,Rotate");

                    //Now we just need to do something with those bytes.
                    //Here I'm writing them to disk but if you were in ASP.Net you might Response.BinaryWrite() them.
                    //You could also write the bytes to a database in a varbinary() column (but please don't) or you
                    //could pass them to another function for further PDF processing.
                    System.IO.File.WriteAllBytes(caminho, bytes);

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        "Conversão concluída. Iniciando a criação do PDF em arquivo.\n" + caminho, request.CodigoSessao, request.CodigoProcesso);
                }
            }
            catch (Exception ex)
            {
                LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                    "GerarRelatorioRecebimentoPDF - Erro na requisição:" + ex.Message, request.CodigoSessao, request.CodigoProcesso);

                // Trata erro
                respostaRelatorioPdf.ProcessarExcessao(ex);

            }
            finally
            {
                if (file != null)
                    file.Close();
            }

            // Retorna
            return respostaRelatorioPdf;
        }
        public ListarRelatorioRecebimentoResponse ListarRelatorioRecebimento(ListarRelatorioRecebimentoRequest request)
        {
            // Prepara resposta
            ListarRelatorioRecebimentoResponse resposta = new ListarRelatorioRecebimentoResponse();
            resposta.PreparaResposta(request);

            // Time para somar á data final
            TimeSpan ts = new TimeSpan(23, 59, 59);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pela Empresa
                if (request.FiltroGrupo != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroEmpresaGrupo", CondicaoTipoEnum.Igual, request.FiltroGrupo));

                //Permite ao usuário buscar pela Referencia
                if (request.FiltroReferencia != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroReferencia", CondicaoTipoEnum.Igual, request.FiltroReferencia));

                //Permite ao usuário buscar pelo banco
                if (request.FiltroMeioPagamento != null)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroMeioPagamento", CondicaoTipoEnum.Igual, request.FiltroMeioPagamento));

                // Data Agendamento Inicio
                if (request.FiltroDataRepasseInicio.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataRepasseInicio",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataRepasseInicio.Value));

                if (request.FiltroDataRepasseFim.HasValue)
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataRepasseFim",
                            CondicaoTipoEnum.Igual,
                            request.FiltroDataRepasseFim.Value + ts));

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<RelatorioRecebimentoInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }
        #endregion

        #region Relatorio Interchange

        /// Jira: SCF - 1520
        public GerarRelatorioInterchangeExcelResponse GerarRelatorioInterchangeExcel(GerarRelatorioInterchangeExcelRequest request)
        {
            // Prepara a resposta
            GerarRelatorioInterchangeExcelResponse resposta = new GerarRelatorioInterchangeExcelResponse();
            resposta.PreparaResposta(request);

            if (request.CodigoProcesso == null)
            {
                ListarRelatorioInterchangeRequest req = new ListarRelatorioInterchangeRequest();
                req.FiltroDataMovimentoDe = request.FiltroDataMovimentoDe;
                req.FiltroDataMovimentoAte = request.FiltroDataMovimentoAte;

                // Cria o processo de geracao do relatorio
                ProcessoRelatorioInterchangeExcelInfo processoInfo =
                    new ProcessoRelatorioInterchangeExcelInfo()
                    {
                        Param = req
                    };

                // Executa o processo
                ExecutarProcessoResponse respostaExecutar =
                    Mensageria.Processar<ExecutarProcessoResponse>(
                        new ExecutarProcessoRequest()
                        {
                            CodigoSessao = request.CodigoSessao,
                            ProcessoInfo = processoInfo,
                            ExecutarAssincrono = true
                        });

                // Informa o código do processo
                resposta.CodigoProcesso = respostaExecutar.CodigoProcesso;

            }
            else
            {
                resposta = gerarRelatorioInterchangeExcel(request);
            }

            // Retorna a resposta
            return resposta;
        }

        /// Jira: SCF - 1519 e 1520
        private GerarRelatorioInterchangeExcelResponse gerarRelatorioInterchangeExcel(GerarRelatorioInterchangeExcelRequest request)
        {
            //Prepara a Resposta
            GerarRelatorioInterchangeExcelResponse response = new GerarRelatorioInterchangeExcelResponse();
            response.PreparaResposta(request);

            var filtros = request;

            // gera a string 
            StringBuilder sb = new StringBuilder();
            sb.Append("Filtros:;Data Processamento De ");
            sb.Append(filtros.FiltroDataMovimentoDe.ToString("dd/MM/yyyy") + ";");
            sb.Append("Até ");
            sb.Append(filtros.FiltroDataMovimentoAte.ToString("dd/MM/yyyy") + ";");
            StreamWriter file = null;

            try
            {
                // Carrega as configurações do Sistema
                ReceberConfiguracaoGeralResponse resConfig = Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });
                LogSCFProcessoEstagioEventoInfo.EfetuarLog("Iniciando validações", request.CodigoSessao, request.CodigoProcesso);

                // Monto o caminho e o nome do arquivo que sera gerado
                if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao == "")
                {
                    response.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    response.Erro = "O relatório não foi gerado, pois o diretório de transação não está configurado.";
                    response.DescricaoResposta = response.Erro;

                    response.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = response.Erro,
                            DataCritica = DateTime.Now
                        });

                    // Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(response.Erro, request.CodigoSessao, request.CodigoProcesso);
                    return response;
                }
                else
                {
                    if (resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao.Substring(resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao.Length - 1) == @"\")
                        response.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao;
                    else
                        response.CaminhoArquivoGerado = resConfig.ConfiguracaoGeralInfo.DiretorioExportacaoRelatorioTransacao + "\\";
                }
                // Pega o usuario para recuperar o nome e colocar no arquivo gerado
                UsuarioInfo usuarioInfo = SegurancaHelper.ReceberUsuario(request.CodigoSessao);
                DateTime dataAtual = DateTime.Now;

                string nome = "";
                if (usuarioInfo.Nome.Contains(" "))
                    nome = usuarioInfo.Nome.Substring(0, usuarioInfo.Nome.IndexOf(" "));
                else
                    nome = usuarioInfo.Nome;

                string caminho = response.CaminhoArquivoGerado
                    + nome + @"_RelInterchange_" + dataAtual.Year.ToString("0000")
                    + dataAtual.Month.ToString("00") + dataAtual.Day.ToString("00")
                    + "_" + request.CodigoProcesso.ToString() + ".csv";

                if (!System.IO.Directory.Exists(response.CaminhoArquivoGerado))
                {
                    response.StatusResposta = MensagemResponseStatusEnum.ErroNegocio;
                    response.Erro = "O relatório não foi gerado, pois o diretório de transação do relatório não existe. [" + response.CaminhoArquivoGerado + "]";
                    response.DescricaoResposta = response.Erro;

                    response.Criticas.Add(
                        new CriticaInfo()
                        {
                            Descricao = response.Erro,
                            DataCritica = DateTime.Now
                        });

                    //Sinaliza
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(
                        response.Erro, request.CodigoSessao, request.CodigoProcesso);

                    return response;
                }
                else
                {
                    file = new StreamWriter(caminho, true, Encoding.Default);

                    try
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Arquivo criado:" + caminho, request.CodigoSessao, request.CodigoProcesso);
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Realizando consulta do Interchange no banco de dados", request.CodigoSessao, request.CodigoProcesso);
                    }
                    catch (Exception e)
                    {
                        LogSCFProcessoEstagioEventoInfo.EfetuarLog("Exceção:" + e.InnerException, request.CodigoSessao, request.CodigoProcesso);
                    }

                    //Pede o relatório
                    ListarRelatorioInterchangeResponse resposta =
                        Mensageria.Processar<ListarRelatorioInterchangeResponse>(
                            new ListarRelatorioInterchangeRequest()
                            {
                                CodigoSessao = request.CodigoSessao,
                                FiltroDataMovimentoDe = request.FiltroDataMovimentoDe,
                                FiltroDataMovimentoAte = request.FiltroDataMovimentoAte
                            }
                        );

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Consulta do Interchange finalizada", request.CodigoSessao, request.CodigoProcesso);

                    // Adiciona os filtros
                    file.WriteLine(sb.ToString());

                    // Informa os filtros da consulta
                    LogSCFProcessoEstagioEventoInfo.EfetuarLog(sb.ToString().Replace(";", " "), request.CodigoSessao, request.CodigoProcesso);
                    String dados;
                    dados = "Grupo;Referência;Data Processamento;Tipo Transação;Código do Produto;Código do Plano;CNPJ da Loja;Número da autorização da transação;Quantidade de Parcelas;Data da transação;Hora da transação;Número do NSU_HOST;Número da Conta Cliente(TSYS);Valor Bruto da Venda;Valor comissão aplicado;Valor líquido;Valor comissão SCF;Taxa Interchange%;Vigência Inicial;Vigência Final;Diferença;Observação";
                    file.WriteLine(dados);

                    //Quantidade de linhas para o log
                    int quantidadeLog = 0;

                    if (resposta.Resultado.Count == 0)
                    {
                        dados = "NÃO HÁ DADOS DIVERGENTES";
                        file.WriteLine(dados);
                    }
                    else
                    {
                        foreach (RelatorioInterChangeInfo relatorioInterchange in resposta.Resultado)
                        {
                            // ==========================================================================================
                            // Adicionado "=\" nos campos que contem números mas precisam ser expressos como textos
                            // ==========================================================================================
                            string observacao = "";
                            string codPlano = "";
                            string taxaInterchange = "";

                            if (relatorioInterchange.CodigoPlano == null)
                            {
                                observacao = "PLANO NÃO ENCONTRADO";
                                codPlano = " ";

                            }
                            else
                            {
                                codPlano = relatorioInterchange.CodigoPlano;
                            }
                            if (relatorioInterchange.CodigoPlano == null && relatorioInterchange.TaxaInterchange == null)
                            {
                                observacao = "PLANO NÃO ENCONTRADO";
                                codPlano = " ";
                            }


                            if (relatorioInterchange.TaxaInterchange == null && relatorioInterchange.CodigoPlano != null)
                            {
                                observacao = "TAXA NÃO CADASTRADA";
                                taxaInterchange = " ";
                            }
                            else if (relatorioInterchange.TaxaInterchange != null)
                            {
                                taxaInterchange = String.Format(new CultureInfo("PT-BR"), "{0:N5}", relatorioInterchange.TaxaInterchange) + "%";
                            }

                            dados = relatorioInterchange.NomeGrupo + ";"
                                + "=\"" + relatorioInterchange.Referencia + "\";"
                                + String.Format("{0:dd/MM/yyyy}", relatorioInterchange.DataProcessamento) + ";"
                                + relatorioInterchange.TipoTransacao + ";"
                                + relatorioInterchange.CodigoProduto + ";"
                                + "=\"" + codPlano + "\";"
                                + "=\"" + relatorioInterchange.Estabelecimento.CNPJ + "\";"
                                + "=\"" + relatorioInterchange.NumeroAutorizacaoTransacao + "\";"
                                + "=\"" + relatorioInterchange.QuantidadeParcelas + "\";"
                                + String.Format("{0:dd/MM/yyyy}", relatorioInterchange.DataTransacao) + ";"
                                + String.Format("{0:HH:mm:ss}", relatorioInterchange.DataTransacao) + ";"
                                + "=\"" + relatorioInterchange.Numero_NSU_HOST + "\";"
                                + "=\"" + relatorioInterchange.NumeroContaClienteTSYS + "\";"
                                + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioInterchange.ValorBrutoVenda) + ";"
                                + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioInterchange.ValorComissaoAplicado) + ";"
                                + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioInterchange.ValorLiquido) + ";"
                                + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioInterchange.ValorComissaoSCF) + ";"
                                + "=\"" + taxaInterchange + "\";"
                                + "=\"" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", relatorioInterchange.VigenciaInical) + "\";"//";"
                                + "=\"" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", relatorioInterchange.VigenciaFinal) + "\";"//";"
                                + String.Format(new CultureInfo("PT-BR"), "{0:N}", relatorioInterchange.Diferenca) + ";"
                                + observacao + ";"
                                ;


                            file.WriteLine(dados);

                            // Atualiza a quantidade de linhas geradas
                            response.QuantidadeLinhasGeradas++;
                            quantidadeLog++;

                            // Sinaliza
                            if (quantidadeLog >= 15000)
                            {
                                // Sinaliza
                                LogSCFProcessoEstagioEventoInfo.EfetuarLog(response.QuantidadeLinhasGeradas.ToString() + " linhas geradas", request.CodigoSessao, request.CodigoProcesso);

                                //Zera linhas do log
                                quantidadeLog = 0;
                            }
                        }
                    }

                    LogSCFProcessoEstagioEventoInfo.EfetuarLog("Total de linhas geradas: " + response.QuantidadeLinhasGeradas.ToString(), request.CodigoSessao, request.CodigoProcesso);

                }

            }
            catch (Exception e)
            {
                // Trata erro
                response.ProcessarExcessao(e);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
            return response;
        }

        public ListarRelatorioInterchangeResponse ListarRelatorioInterchange(ListarRelatorioInterchangeRequest request)
        {
            ListarRelatorioInterchangeResponse resposta = new ListarRelatorioInterchangeResponse();
            // Prepara resposta
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                // Monta a lista de condições
                List<CondicaoInfo> condicoes = new List<CondicaoInfo>();

                //Permite ao usuário buscar pela Data de Movimento
                if (request.FiltroDataMovimentoDe != null)
                {
                    condicoes.Add(
                        new CondicaoInfo(
                            "FiltroDataMovimentoDe", CondicaoTipoEnum.Igual, request.FiltroDataMovimentoDe));
                }
                if (request.FiltroDataMovimentoAte != null)
                {
                    condicoes.Add(
                    new CondicaoInfo(
                        "FiltroDataMovimentoAte", CondicaoTipoEnum.Igual, request.FiltroDataMovimentoAte));
                }

                // Solicita a lista
                resposta.Resultado =
                    PersistenciaHelper.Listar<RelatorioInterChangeInfo>(
                        request.CodigoSessao, condicoes);
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion
        #region Layout

        /// <summary>
        /// Lista os Produtos
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ListarLayoutResponse ListarLayouts(ListarLayoutRequest request)
        {
            // Prepara resposta
            ListarLayoutResponse resposta = new ListarLayoutResponse();
            resposta.PreparaResposta(request);

            // Bloco de controle
            try
            {
                List<LayoutArquivoEnum> lista = EnumHelper.EnumToList<LayoutArquivoEnum>();
                foreach (LayoutArquivoEnum layoutArquivo in lista)
                {
                    string valor = (string)EnumHelper.GetEnumDescriptionAnnotation(layoutArquivo);
                    if (!String.IsNullOrEmpty(valor))
                    {
                        LayoutInfo info = new LayoutInfo();
                        info.CodigoLayout = (int)layoutArquivo;
                        info.NomeLayout = valor;
                        resposta.Resultado.Add(info);
                    }
                }
            }
            catch (Exception ex)
            {
                // Trata erro
                resposta.ProcessarExcessao(ex);
            }

            // Retorna
            return resposta;
        }

        #endregion

        #region Functions Validadoras Letras Numeros
        public bool contemLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
                return true;
            else
                return false;
        }

        public bool contemNumeros(string texto)
        {
            if (texto.Where(c => char.IsNumber(c)).Count() > 0)
                return true;
            else
                return false;
        }

        #endregion
    }
}
