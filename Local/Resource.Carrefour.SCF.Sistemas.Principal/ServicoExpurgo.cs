﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Library;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Framework.Library.Servicos.Mensagens;
using System.Xml.Serialization;
using System.IO;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Contratos.Comum.Mensagens;


namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    public class ServicoExpurgo : IServicoExpurgo
    {

        //string CodigoSessao = ""; //warning 25/02/2019
        //private ManualResetEvent _manualResetEvent = null; // warning 25/02/2019
        //private Thread _threadServicoExpurgo = null;// warning 25/02/2019
        private ServicoExpurgoInfo _statusServicoExpurgoInfo = new ServicoExpurgoInfo();
        public ServicoExpurgo()
        {
            inicializar();
        }
        private void inicializar()
        {
            
        }        

        public IniciarServicoExpurgoResponse IniciarServicoExpurgo(IniciarServicoExpurgoRequest request)
        {
            IniciarServicoExpurgoResponse resposta = new IniciarServicoExpurgoResponse();
            resposta.PreparaResposta(request);

            /*
            int intervaloExecucao = 60;

            // Bloco de controle
            try
            {
                // Usar mesmos usuários do Agendador para o Expurgo
                ServicoAgendadorConfig _configServico = GerenciadorConfig.ReceberConfig<ServicoAgendadorConfig>();
                if (_configServico != null)
                    _configServico = new ServicoAgendadorConfig();
                
                CodigoSessao = request.CodigoSessao;

                _statusServicoExpurgoInfo.ObservacaoGeral = "Iniciando o serviço.";

                // Se o servico já estiver iniciado não faz nada
                if (_statusServicoExpurgoInfo.StatusServico == ServicoExpurgoStatusEnum.Parado)
                {
                    // Cria o sinalizador
                    _manualResetEvent = new ManualResetEvent(false);

                    _statusServicoExpurgoInfo.ObservacaoGeral = "Busca as configuracoes.";
                    
                    // Buscar configuracao
                    ConfiguracaoGeralInfo configGeral = new ConfiguracaoGeralInfo();

                    // Retorna o item solicitado
                    configGeral =
                        PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                            request.CodigoSessao, null);

                    // Garante que os diretorios estao criados
                    _statusServicoExpurgoInfo.ObservacaoGeral = "Iniciando thread para expurgo do arquivo_item.";                                                            

                    // Cria a thread
                    _threadServicoExpurgo =
                        new Thread(
                            new ParameterizedThreadStart(
                                delegate(object o)
                                {
                                    // Atualiza informacoes do servico
                                    _statusServicoExpurgoInfo.StatusServico = ServicoExpurgoStatusEnum.Iniciado;
                                  
                                    // proxima execucao (tentativa de expurgo e não o expurgo em si)
                                    // o expurgo irá acontecer dependendo do intervalo de dias a aguardar
                                    _statusServicoExpurgoInfo.DataProximaExecucao =  DateTime.Now.Add(new TimeSpan(0, 0, intervaloExecucao));

                                    // Faz enquanto nao for sinalizado para parar
                                    while (!_manualResetEvent.WaitOne(new TimeSpan(0, 0, intervaloExecucao)))
                                    {
                                        // Bloco de controle
                                        try
                                        {

                                            //////////////// PROCESSO EXPURGO
                                            string codigoProcessoExpurgo = processarExpurgo(configGeral);

                                            if (codigoProcessoExpurgo == "")
                                            {
                                                _statusServicoExpurgoInfo.ObservacaoExpurgo = DateTime.Now.ToShortTimeString() + " Não executou o expurgo de arquivo_item.";
                                            }
                                            else
                                            {
                                                _statusServicoExpurgoInfo.ObservacaoExpurgo = DateTime.Now.ToShortTimeString() + " Uma tentativa para expurgo de tb_arquivo_item.";
                                                _statusServicoExpurgoInfo.CodigoProcessoExpurgo = codigoProcessoExpurgo;
                                            }

                                                  
                                            // proxima verificacao
                                            _statusServicoExpurgoInfo.DataProximaExecucao = DateTime.Now.Add(new TimeSpan(0, 0, intervaloExecucao));
                                            _statusServicoExpurgoInfo.ObservacaoGeral = "";
                                            
                                        }
                                        catch (Exception ex)
                                        {
                                            // Atualiza informacoes do servico
                                            _statusServicoExpurgoInfo.ObservacaoGeral = ex.Message ;
                                            _statusServicoExpurgoInfo.DataProximaExecucao = DateTime.Now.Add(new TimeSpan(0, 0, intervaloExecucao));
                                            
                                        }
                                    }

                                    // Atualiza informacoes do servico
                                    _statusServicoExpurgoInfo.StatusServico = ServicoExpurgoStatusEnum.Parado;
                                    _statusServicoExpurgoInfo.DataProximaExecucao = null;
                                    _statusServicoExpurgoInfo.ObservacaoGeral = DateTime.Now.ToShortTimeString() + " Em execuçao";
 
                                }));

                    // Inicia a thread
                    _threadServicoExpurgo.Start();

                    // lucas.rodrigues - Verificar se o status que ficou o serviço antes da aplicação parar
                    ReceberConfiguracaoGeralResponse resConfig =
                        Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                    resConfig.ConfiguracaoGeralInfo.StatusServicoExpurgo = ServicoExpurgoStatusEnum.Iniciado;

                    Mensageria.Processar<SalvarConfiguracaoGeralResponse>(
                        new SalvarConfiguracaoGeralRequest()
                        {
                            ConfiguracaoGeralInfo = resConfig.ConfiguracaoGeralInfo
                        });

                    // Retorna status
                    resposta.StatusServicoExpurgoInfo  = _statusServicoExpurgoInfo.StatusServico;
                }
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }
             */

            // Retorna
            return resposta;
        }

        public string processarExpurgo(ConfiguracaoGeralInfo configGeral)
        {
            /*
            // chama o processo
            ExecutarProcessoExpurgoRequest processoExtratoRequest = new ExecutarProcessoExpurgoRequest();
            processoExtratoRequest.CodigoSessao = CodigoSessao;
            
            // Preenche o request
            processoExtratoRequest.ExecutarAssincrono = true;

            // Starta o processamento
            string codigoProcesso = ((ExecutarProcessoExpurgoResponse)Mensageria.Processar(processoExtratoRequest)).CodigoProcesso;
            */
            string codigoProcesso = "";
            return codigoProcesso;
        }

        public PararServicoExpurgoResponse PararServicoExpurgo(PararServicoExpurgoRequest request)
        {
            // Prepara resposta
            PararServicoExpurgoResponse resposta = new PararServicoExpurgoResponse();
            resposta.PreparaResposta(request);
            /*
            // Bloco de controle
            try
            {
                // Se o servico estiver em execucao, pede para parar
                if (_statusServicoExpurgoInfo.StatusServico == ServicoExpurgoStatusEnum.Iniciado)
                {
                    _manualResetEvent.Set();

                    // garante que o serviço esteja realmente parado
                    // OBS: havia um problema onde, em algumas situações, o usuário precisava clicar duas vezes
                    // no botão "Parar" para que o serviço realmente parasse de ser executado.
                    if (_statusServicoExpurgoInfo.StatusServico != ServicoExpurgoStatusEnum.Parado)
                    {
                        _manualResetEvent.Set();
                        _statusServicoExpurgoInfo.StatusServico = ServicoExpurgoStatusEnum.Parado;
                        PararServicoExpurgo(request);
                    }
                }

                ReceberConfiguracaoGeralResponse resConfig =
                    Mensageria.Processar<ReceberConfiguracaoGeralResponse>(new ReceberConfiguracaoGeralRequest() { });

                resConfig.ConfiguracaoGeralInfo.StatusServicoExpurgo = ServicoExpurgoStatusEnum.Parado;

                Mensageria.Processar<SalvarConfiguracaoGeralResponse>(
                    new SalvarConfiguracaoGeralRequest()
                    {
                        ConfiguracaoGeralInfo = resConfig.ConfiguracaoGeralInfo
                    });

                _statusServicoExpurgoInfo.ObservacaoGeral = DateTime.Now.ToShortTimeString() + " Parado";
                // Retorna status
                resposta.ServicoExpurgoInfo  = _statusServicoExpurgoInfo;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                _statusServicoExpurgoInfo.ObservacaoGeral = ex.Message;
                resposta.ProcessarExcessao(ex);
            }
            */
            // Retorna
            return resposta;
        }

        public ReceberStatusServicoExpurgoResponse ReceberStatusServicoExpurgo(ReceberStatusServicoExpurgoRequest request)
        {
            // Prepara resposta
            ReceberStatusServicoExpurgoResponse resposta = new ReceberStatusServicoExpurgoResponse();
            resposta.PreparaResposta(request);
            /*
            // Bloco de controle
            try
            {
                // Informa status na resposta
                resposta.ServicoExpurgoInfo = _statusServicoExpurgoInfo;

                // Buscar configuracao
                ConfiguracaoGeralInfo configGeral = new ConfiguracaoGeralInfo();

                // Retorna o item solicitado
                configGeral =
                    PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                        request.CodigoSessao, null);

                // Seta a próxima execução do expurgo
                resposta.ServicoExpurgoInfo.DataProximaExecucao = 
                    configGeral.DataUltimoExpurgo != null ? 
                    ((DateTime)configGeral.DataUltimoExpurgo).AddDays(configGeral.DiasAguardarExpurgo) : (DateTime?)null;

                resposta.ServicoExpurgoInfo.DataUltimaExecucao = configGeral.DataUltimoExpurgo;
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                resposta.ProcessarExcessao(ex);
            }
            */
            // Retorna
            return resposta;
        }
    }
}
