﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    /// <summary>
    /// Classe base para uma regra
    /// </summary>
    public class RegraSCFBase
    {
        /// <summary>
        /// Construtor default
        /// </summary>
        public RegraSCFBase()
        {
            this.Escopo = RegraSCFEscopoEnum.Linha;
        }

        /// <summary>
        /// Escopo da regra
        /// </summary>
        public RegraSCFEscopoEnum Escopo { get; set; }

        /// <summary>
        /// Inicio da validação
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public void IniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            // Repassa a chamada
            this.OnIniciarValidacaoArquivo(request);
        }

        /// <summary>
        /// Método virtual para início de validação
        /// </summary>
        /// <param name="criticas"></param>
        /// <param name="arquivo"></param>
        protected virtual void OnIniciarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }

        /// <summary>
        /// Inicia validação da linha
        /// </summary>
        public void IniciarValidacaoLinha(RegraSCFValidarLinhaRequest request)
        {
            // Repassa a chamada
            this.OnIniciarValidacaoLinha(request);
        }

        /// <summary>
        /// Método virtual para inicio de validação da linha
        /// </summary>
        protected virtual void OnIniciarValidacaoLinha(RegraSCFValidarLinhaRequest request)
        {
        }

        /// <summary>
        /// Realiza a validação
        /// </summary>
        public void ValidarLinha(RegraSCFValidarLinhaRequest request)
        {
            // Repassa a chamada
            this.OnValidarLinha(request);
        }

        /// <summary>
        /// Método virtual para a validação
        /// </summary>
        protected virtual void OnValidarLinha(RegraSCFValidarLinhaRequest request)
        {
        }

        /// <summary>
        /// Finaliza validação da linha
        /// </summary>
        public void FinalizarValidacaoLinha(RegraSCFValidarLinhaRequest request)
        {
            // Repassa a chamada
            this.OnFinalizarValidacaoLinha(request);
        }

        /// <summary>
        /// Método virtual para finalização de validação da linha
        /// </summary>
        protected virtual void OnFinalizarValidacaoLinha(RegraSCFValidarLinhaRequest request)
        {
        }

        /// <summary>
        /// Finaliza a validação do arquivo
        /// </summary>
        public void FinalizarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
            // Repassa a chamada
            this.OnIniciarValidacaoArquivo(request);
        }

        /// <summary>
        /// Método virtual para finalizar validação do arquivo
        /// </summary>
        protected virtual void OnFinalizarValidacaoArquivo(RegraSCFValidarArquivoRequest request)
        {
        }
    }
}
