﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    /// <summary>
    /// Classe que contem as lógicas de geração das chaves por tipo de registro
    /// </summary>
    public static class ChaveTransacaoHelper
    {
        /// <summary>
        /// Chave de comprovante de venda
        /// </summary>
        /// <returns></returns>
        public static string GerarChaveComprovanteVenda(DateTime dataTransacao, string nsuTef)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + "CV" + nsuTef.PadLeft(12, '0');
        }

        /// <summary>
        /// Chave de comprovante de pagamento
        /// </summary>
        /// <returns></returns>
        public static string GerarChaveComprovantePagamento(DateTime dataTransacao, string nsuTef)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + "CP" + nsuTef.PadLeft(12, '0');
        }

        /// <summary>
        /// Chave de comprovante de anulacao de venda
        /// </summary>
        /// <returns></returns>
        public static string GerarChaveAnulacaoVenda(DateTime dataTransacao, string nsuTef)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + "AV" + nsuTef.PadLeft(12, '0');
        }

        /// <summary>
        /// Chave de comprovante de anulacao de pagamento
        /// </summary>
        /// <returns></returns>
        public static string GerarChaveAnulacaoPagamento(DateTime dataTransacao, string nsuTef)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + "AV" + nsuTef.PadLeft(12, '0');
        }

        /// <summary>
        /// Chave de ajuste
        /// </summary>
        /// <returns></returns>
        public static string GerarChaveAjuste(DateTime dataTransacao, double valor)
        {
            return dataTransacao.ToString("yyyyMMddHHmmss") + "AJ" + valor.ToString();
        }
    }
}
