﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    /// <summary>
    /// Mensagem local para validação de linha
    /// </summary>
    public class RegraSCFValidarLinhaRequest
    {
        /// <summary>
        /// Críticas da linha
        /// </summary>
        public List<CriticaSCFArquivoItemInfo> Criticas { get; set; } 

        /// <summary>
        /// Interpretador do arquivo
        /// </summary>
        public ArquivoBase Arquivo { get; set; }

        /// <summary>
        /// Código do processo
        /// </summary>
        public string CodigoProcesso { get; set; }
        
        /// <summary>
        /// Código do arquivo que está sendo validado
        /// </summary>
        public string CodigoArquivo { get; set; }

        /// <summary>
        /// Código do arquivoItem que está sendo validado
        /// </summary>
        public string CodigoArquivoItem { get; set; }

        /// <summary>
        /// Tipo da linha
        /// </summary>
        public string TipoLinha { get; set; }

        /// <summary>
        /// Conteúdo da linha
        /// </summary>
        public string Linha { get; set; }
    }
}
