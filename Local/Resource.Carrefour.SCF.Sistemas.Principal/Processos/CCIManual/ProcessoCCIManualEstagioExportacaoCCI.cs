﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.ProcessoCCIManual
{

    /// <summary>
    /// Estagio de exportacao Manual para CCI
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoCCIManualInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]
     public class ProcessoCCIManualEstagioExportacaoCCI : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoCCIManualEstagioExportacaoCCI(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoCCIManualInfo ProcessoCCIManualInfo
        {
            get { return (ProcessoCCIManualInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento exportacao do arquivo para CCI
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede para processar apenas se o arquivo Carrefour foi informado
            if (this.ProcessoCCIManualInfo.CaminhoArquivoCCI != null && !String.IsNullOrEmpty(this.ProcessoCCIManualInfo.CodigoArquivoTSYS))
            {
                // Retorna o item solicitado
                ConfiguracaoGeralInfo configGeral =
                    PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                        codigoSessao, null);

                // Pede a exportacao do arquivo da CCI do Carrefour
                //ExportarExtratoParaCCIResponse respostaExportar =
                ExportarExtratoParaGrupoResponse respostaExportar =

                    Mensageria.Processar<ExportarExtratoParaGrupoResponse>( //1333 extava ExportarExtratoParaCCIResponse
                        new ExportarExtratoParaGrupoRequest() //1333 estava ExportarExtratoParaCCIRequest
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoCCIManualInfo.CaminhoArquivoCCI,
                            CodigoArquivo = this.ProcessoCCIManualInfo.CodigoArquivoTSYS,
                            CodigoProcesso = this.ProcessoCCIManualInfo.CodigoProcesso,
                            ProcessoManual = true
                        });
  
                
                // Atualiza o objeto
                this.ProcessoCCIManualInfo.Atualizar(
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        codigoSessao, this.ProcessoCCIManualInfo.CodigoProcesso));
            }
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEventoInfo()
                    {
                        Descricao = "A geração Manual da CCI não foi executada."
                    };
                    lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoCCIManualInfo.CodigoProcesso);
                // Indica que não gerou o arquivo
                LogHelper.Salvar(
                    codigoSessao, lLogSCFProcessoEventoInfo);

            }

        }

        /// <summary>
        /// Processa o cancelamento da exportacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
        }
    }
}
