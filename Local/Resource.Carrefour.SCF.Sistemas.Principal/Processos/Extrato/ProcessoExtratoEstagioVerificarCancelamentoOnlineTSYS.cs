﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;
using System.IO;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log; //1391

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Extrato
{
    [Estagio(
        TipoProcesso = typeof(ProcessoExtratoInfo),
        TipoEstagioAnterior = typeof(ProcessoExtratoEstagioValidacaoTSYS),
//1333  TipoProximoEstagio = typeof(ProcessoExtratoEstagioExportacaoCCI))]
        TipoProximoEstagio = typeof(ProcessoExtratoEstagioExportacaoGrupo))]
    public class ProcessoExtratoEstagioVerificarCancelamentoOnlineTSYS : EstagioBase
    {
        public ProcessoExtratoEstagioVerificarCancelamentoOnlineTSYS(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }
        public ProcessoExtratoInfo ProcessoExtratoInfo
        {
            get { return (ProcessoExtratoInfo)this.ProcessoInfo; }
        }

        protected override void OnProcessar(string codigoSessao)
        {
            //========================================================================================================================
            //1391 - inibindo inicio
            //========================================================================================================================
            // Continua essa etapa apenas se diretorio do arquivo TSYS foi informado
            //========================================================================================================================
            //if      (this.ProcessoExtratoInfo.CaminhoArquivoTSYS != null)
            //{
            //    Dictionary<string, object> contexto = new Dictionary<string, object>();
            //    contexto.Add("CodigoGrupo", null);
            //    ProcessoExtratoVerificarCancelamentoOnlineResponse respostaProcessoCancelamentoOnline =
            //        Mensageria.Processar<ProcessoExtratoVerificarCancelamentoOnlineResponse>(
            //            new ProcessoExtratoVerificarCancelamentoOnlineRequest()
            //            {
            //                CodigoSessao = codigoSessao,
            //                CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYS,
            //                CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
            //                Contexto = contexto
            //            });
            //}   
            //========================================================================================================================
            //1391 - inibindo fim
            //========================================================================================================================

            //========================================================================================================================
            // CANCELAMENTO ON LINE - Confere o codigo do arquivo
            //========================================================================================================================
            //1391 - novo inicio
            //========================================================================================================================
            if (
                    (this.ProcessoExtratoInfo.CodigoArquivoTSYS != null)                    ||
                    (this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao != null)            ||
                    (this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria != null)          
                )
            {
                //===================================================================================================================		
                // CONFERE QUAL É O CODIGO DO ARQUIVO A SER UTILIZADO
                //===================================================================================================================		            
                string codigoarquivo = null;
                string codigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso;

                if (this.ProcessoExtratoInfo.CodigoArquivoTSYS != null)
	            {
            	    codigoarquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYS;	 
	            }
                if (this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao != null)
	            {
            	    codigoarquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao;	 
	            }
                if (this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria != null)
	            {
            	    codigoarquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria;
	            }
                //===================================================================================================================		
                // LOG DA FASE - CANCELAMENTO ONLINE
                //===================================================================================================================		            
                var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CANCELAMENTO ONLINE - Inicio - Codigo arquivo [{0}] ",codigoarquivo));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                //===================================================================================================================		            
                // CARREGA O CONTEXTO
                //===================================================================================================================		            
                //Dictionary<string, object> contexto = new Dictionary<string, object>();
                //contexto.Add("CodigoGrupo", null);
                //===================================================================================================================		            
                // EXECUTA A PROCEDURE PR_CANCELAMENTO_ONLINE << ProcessoExtratoVerificarCancelamentoOnlineResponse << ServicoIntegracao
                //===================================================================================================================		            
                ProcessoExtratoVerificarCancelamentoOnlineResponse respostaProcessoCancelamentoOnline =
                    Mensageria.Processar<ProcessoExtratoVerificarCancelamentoOnlineResponse>
                    (
                        new ProcessoExtratoVerificarCancelamentoOnlineRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = codigoarquivo, //1391
                            CodigoProcesso = codigoProcesso, //1391
                            //Contexto = contexto
                        }
                    );
                //===================================================================================================================		
                // LOG DA FASE - CANCELAMENTO ONLINE
                //===================================================================================================================		            
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(codigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("CANCELAMENTO ONLINE - Fim"));

                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            }       
        //===================================================================================================================		            
        //1391 - novo fim
        //===================================================================================================================	                      
        }
    }
}
