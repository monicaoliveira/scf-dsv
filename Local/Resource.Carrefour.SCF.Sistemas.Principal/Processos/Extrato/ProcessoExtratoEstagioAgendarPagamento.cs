﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Extrato
{
    /// <summary>
    /// Estagio de Geração de agenda
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoExtratoInfo),
        TipoEstagioAnterior = typeof(ProcessoExtratoEstagioTransacaoTSYS),
        TipoProximoEstagio = typeof(ProcessoExtratoEstagioEnviarMATERAExtrato))
    ]
    public class ProcessoExtratoEstagioAgendarPagamento : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoExtratoEstagioAgendarPagamento(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoExtratoInfo ProcessoExtratoInfo
        {
            get { return (ProcessoExtratoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo TSYS
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // TODO: FALTA VERIFICAR COMO VER SE A VALIDACAO ESTA COM FALHA
            if (this.ProcessoExtratoInfo.CaminhoArquivoTSYS != null)
            {
                // Gera transacoes do arquivo importado
                AgendarPagamentoExtratoResponse respostaTransacaoes =
                    Mensageria.Processar<AgendarPagamentoExtratoResponse>(
                        new AgendarPagamentoExtratoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso
                        });
            }
        }

        
    }
}
