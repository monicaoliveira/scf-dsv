﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Extrato
{
    /// <summary>
    /// Estagio de Geração de agenda
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoExtratoInfo),
        TipoEstagioAnterior = typeof(ProcessoExtratoEstagioAgendarPagamento),
        TipoProximoEstagio = null)
    ]
    public class ProcessoExtratoEstagioEnviarMATERAExtrato : EstagioBase
    {

        public ProcessoExtratoEstagioEnviarMATERAExtrato(ProcessoInfo processoInfo) : base(processoInfo) { }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoExtratoInfo ProcessoExtratoInfo
        {
            get { return (ProcessoExtratoInfo)this.ProcessoInfo; }
        }

        protected override void OnProcessar(string codigoSessao)
        {
            //==========================================================================================================
            // Retorna o item solicitado
            //==========================================================================================================
            ConfiguracaoGeralInfo configGeral = PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(codigoSessao, null);
            //==========================================================================================================
            //1328 - inicio
            //==========================================================================================================
            var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            string descricao = "";
            //==========================================================================================================
            //1328 - fim
            //==========================================================================================================
            if (configGeral != null && configGeral.EnviarMateraFinanceiroAutomatico)
            {
                if (!String.IsNullOrEmpty(configGeral.DiretorioExportacaoMateraExtrato))
                {
                    //==========================================================================================================
                    // Acertar o nome do arquivo YYYYMMDD_CodigoProcesso_ReasultadoMateraContabil.txt
                    //==========================================================================================================
                    string caminhoArquivo;
                    ExportarMateraFinanceiroRequest reqMatera = new ExportarMateraFinanceiroRequest();
                    caminhoArquivo = configGeral.DiretorioExportacaoMateraExtrato;
                    reqMatera.CaminhoArquivoDestino = caminhoArquivo;
                    reqMatera.DataPagamento = DateTime.Now.AddDays(Convert.ToDouble(configGeral.PrazoPagamento));
                    reqMatera.CodigoProcesso = ProcessoInfo.CodigoProcesso;
                    reqMatera.CodigoSessao = codigoSessao;
                    //==========================================================================================================
                    //1328 - inicio
                    //==========================================================================================================
                    descricao = "EXPORTACAO MATERA - Arquivo destino" + caminhoArquivo;
                    lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                    lLogSCFProcessoEventoInfo.setDescricao(descricao);
                    //1353 lLogSCFProcessoEventoInfo.setTipoEstagio("ProcessoExtratoEstagioEnviarMATERAExtrato");
                    LogHelper.Salvar(codigoSessao, lLogSCFProcessoEventoInfo);
                    //==========================================================================================================
                    //1328 - inicio
                    //==========================================================================================================
                    var resMatera = Mensageria.Processar<ExportarMateraFinanceiroResponse>(reqMatera);
                    string codigoProcesso = ProcessoInfo.CodigoProcesso;

                    if (resMatera.StatusResposta != MensagemResponseStatusEnum.OK)
                    {
                        //1328 var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                        descricao = "EXPORTACAO MATERA - " + resMatera.DescricaoResposta; //1328
                        lLogSCFProcessoEventoInfo.setCodigoProcesso(codigoProcesso);
                        //1328 lLogSCFProcessoEventoInfo.setDescricao(resMatera.DescricaoResposta);
                        lLogSCFProcessoEventoInfo.setDescricao(descricao); //1328
                        //1353 lLogSCFProcessoEventoInfo.setTipoEstagio("ProcessoExtratoEstagioEnviarMATERAExtrato");
                        LogHelper.Salvar(codigoSessao, lLogSCFProcessoEventoInfo);
                    }

                    //==========================================================================================================
                    // Atualiza o objeto
                    //==========================================================================================================
                    this.ProcessoExtratoInfo.Atualizar(PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, this.ProcessoExtratoInfo.CodigoProcesso));
                }
                else
                {
                    //1328 string descricao = "EXPORTACAO MATERA - Não foi exportado o arquivo Matera Extrato. Motivo: O diretório do arquivo não foi configurado no sistema.";
                    //1328 var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                    descricao = "EXPORTACAO MATERA - Não foi exportado o arquivo Matera Extrato. Motivo: O diretório do arquivo não foi configurado no sistema.";                    
                    lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                    lLogSCFProcessoEventoInfo.setDescricao(descricao);
                    //1353lLogSCFProcessoEventoInfo.setTipoEstagio("ProcessoExtratoEstagioEnviarMATERAExtrato");
                    LogHelper.Salvar(codigoSessao, lLogSCFProcessoEventoInfo);
                }
            }
            else
            {
                // 1328 string descricao = "EXPORTACAO MATERA - Não foi gerado o Matera Extrato. Motivo: O sistema está configurado para não gerar o arquivo da matera automaticamente.";
                // 1328 var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                descricao = "EXPORTACAO MATERA - Não foi gerado o Matera Extrato. Motivo: O sistema está configurado para não gerar o arquivo da matera automaticamente.";
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                lLogSCFProcessoEventoInfo.setDescricao(descricao);
                //1353lLogSCFProcessoEventoInfo.setTipoEstagio("ProcessoExtratoEstagioEnviarMATERAExtrato");
                LogHelper.Salvar(codigoSessao, lLogSCFProcessoEventoInfo);
            }
        }
    }
}
