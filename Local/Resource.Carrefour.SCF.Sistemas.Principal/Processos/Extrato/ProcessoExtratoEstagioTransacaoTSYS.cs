﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Extrato
{
    /// <summary>
    /// Estagio de Geração de Transação TSYS
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoExtratoInfo),
        //TipoEstagioAnterior = typeof(ProcessoExtratoEstagioValidacaoTSYS),
        //TipoProximoEstagio = typeof(ProcessoExtratoEstagioAgendarPagamento))]
//1333        TipoEstagioAnterior = typeof(ProcessoExtratoEstagioExportacaoCCI),
        TipoEstagioAnterior = typeof(ProcessoExtratoEstagioExportacaoGrupo),
       // TipoProximoEstagio = typeof(ProcessoExtratoEstagioAgendarPagamento))] // nao haverá mais o processo de agenda e exportacao de pagamentos
       TipoProximoEstagio = null)]
    
    public class ProcessoExtratoEstagioTransacaoTSYS : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoExtratoEstagioTransacaoTSYS(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoExtratoInfo ProcessoExtratoInfo
        {
            get { return (ProcessoExtratoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo TSYS
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo Carrefour foi informado
            if (this.ProcessoExtratoInfo.CaminhoArquivoTSYS != null)
            {
                // Gera transacoes do arquivo importado
                ImportarTransacoesTSYSResponse respostaTransacaoes =
                    Mensageria.Processar<ImportarTransacoesTSYSResponse>(
                        new ImportarTransacoesTSYSRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYS,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            AlterarBancoAgenciaConta = false,
                            TraduzirProduto = true,
                            TraduzirPlano = true
                        });
            }

            // Continua apenas se o arquivo Atacadao foi informado
            if (this.ProcessoExtratoInfo.CaminhoArquivoTSYSAtacadao != null)
            {
                // Gera transacoes do arquivo importado
                ImportarTransacoesTSYSResponse respostaTransacaoesAtacadao =
                    Mensageria.Processar<ImportarTransacoesTSYSResponse>(
                        new ImportarTransacoesTSYSRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            AlterarBancoAgenciaConta = false,
                            TraduzirProduto = true,
                            TraduzirPlano = true
                        });
            }

            // Continua apenas se o arquivo Galeria foi informado
            if (this.ProcessoExtratoInfo.CaminhoArquivoTSYSGaleria != null)
            {
                // Gera transacoes do arquivo importado
                ImportarTransacoesTSYSResponse respostaTransacaoesGaleria =
                    Mensageria.Processar<ImportarTransacoesTSYSResponse>(
                        new ImportarTransacoesTSYSRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            AlterarBancoAgenciaConta = false,
                            TraduzirProduto = true,
                            TraduzirPlano = true
                        });
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoExtratoInfo.CodigoArquivoTSYS = null;
        }
    }
}
