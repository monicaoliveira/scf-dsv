﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Extrato
{
    /// <summary>
    /// Estagio de Validação de Arquivo TSYS
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoExtratoInfo),
        TipoEstagioAnterior = typeof(ProcessoExtratoEstagioImportacaoTSYS),
        TipoProximoEstagio = typeof(ProcessoExtratoEstagioVerificarCancelamentoOnlineTSYS),
        EhValidacao = true )]
        //TipoProximoEstagio = typeof(ProcessoExtratoEstagioTransacaoTSYS))]
    public class ProcessoExtratoEstagioValidacaoTSYS : EstagioBase
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoExtratoEstagioValidacaoTSYS(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoExtratoInfo ProcessoExtratoInfo
        {
            get { return (ProcessoExtratoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Faz a validação do arquivo TSYS
        /// </summary>
        /// <param name="codigoSessao"></param>
        /// <returns></returns>
        protected override bool OnValidar(string codigoSessao)
        {
            ProcessoExtratoValidacaoConfig config = GerenciadorConfig.ReceberConfig<ProcessoExtratoValidacaoConfig>();
            if (config == null)
                config = new ProcessoExtratoValidacaoConfig();
            Dictionary<string, object> contexto = new Dictionary<string, object>();
            
            // inicializa as variaveis e objetos
            contexto.Add("CodigoGrupo", null);
            int quantidadeCriticas = 0;

            // Faz o log
            log.Debug(this.ProcessoExtratoInfo);

            // executa a verificação do grupo Carrefour
            if (this.ProcessoExtratoInfo.CodigoArquivoTSYS != null)
            {
                // ajusta o codigo do grupo
                contexto["CodigoGrupo"] = config.CodigoGrupoCarrefour;

                // Pede a validação do arquivo
                ValidarArquivoResponse respostaCarrefour =
                    Mensageria.Processar<ValidarArquivoResponse>(
                        new ValidarArquivoRequest()
                        {                                                           
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYS,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            Contexto = contexto,
                            RepassarExcessao = true
                        });



                quantidadeCriticas += respostaCarrefour.QuantidadeCriticas;
            }

            // executa a verificação do grupo Atacadão
            if (this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao != null)
            {
                // ajusta o codigo do grupo
                contexto["CodigoGrupo"] = config.CodigoGrupoAtacadao;

                // Pede a validação do arquivo
                ValidarArquivoResponse respostaAtacadao =
                    Mensageria.Processar<ValidarArquivoResponse>(
                        new ValidarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            Contexto = contexto,
                            RepassarExcessao = true
                        });

                quantidadeCriticas += respostaAtacadao.QuantidadeCriticas;
            }

            // executa a verificação do grupo Galeria
            if (this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria != null)
            {
                // ajusta o codigo do grupo
                contexto["CodigoGrupo"] = config.CodigoGrupoGaleria;

                // Pede a validação do arquivo
                ValidarArquivoResponse respostaGaleria =
                    Mensageria.Processar<ValidarArquivoResponse>(
                        new ValidarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            Contexto = contexto,
                            RepassarExcessao = true
                        });

                quantidadeCriticas += respostaGaleria.QuantidadeCriticas;
            }

            // validacao de linhas desbloqueadas de arquivos de outros processos
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
            condicoes.Add(
                new CondicaoInfo(
                    "StatusArquivoItem", CondicaoTipoEnum.Igual, ArquivoItemStatusEnum.PendenteDesbloqueio));

            List<ArquivoItemResumoInfo> arquivoItem =
                PersistenciaHelper.Listar<ArquivoItemResumoInfo>(null, condicoes);

            foreach (ArquivoItemResumoInfo arquivo in arquivoItem)
            {
                ValidarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ValidarArquivoResponse>(
                    new ValidarArquivoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = arquivo.CodigoArquivo,
                    });
            }

             // Retorna
            return quantidadeCriticas == 0;
        }
    }
}
