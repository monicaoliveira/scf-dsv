﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using System.IO;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Persistencia.Db;
using Oracle.DataAccess.Client;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Framework.Library;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Extrato
{
    /// <summary>
    /// Estagio de Importacao de Arquivo TSYS
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoExtratoInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = typeof(ProcessoExtratoEstagioValidacaoTSYS))]
    public class ProcessoExtratoEstagioImportacaoTSYS : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoExtratoEstagioImportacaoTSYS(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoExtratoInfo ProcessoExtratoInfo
        {
            get { return (ProcessoExtratoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo TSYS
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            ProcessoExtratoValidacaoConfig config = GerenciadorConfig.ReceberConfig<ProcessoExtratoValidacaoConfig>();
            string codigoEmpresaArquivo = null;

            // Continua essa etapa apenas se o arquivo "Carrefour" foi informado
            if (this.ProcessoExtratoInfo.CaminhoArquivoTSYS != null)
            {
                codigoEmpresaArquivo = config.CodigoGrupoCarrefour;

                // Faz a importacao do arquivo
                ImportarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ImportarArquivoResponse>(
                        new ImportarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoExtratoInfo.CaminhoArquivoTSYS,
                            //1333 TipoArquivo = "ArquivoTSYS",
                            TipoArquivo = "ArquivoTSYSCCI",
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            CodigoGrupoEmpresaArquivo = codigoEmpresaArquivo

                        });

                // Informa codigo do arquivo no processo
                this.ProcessoExtratoInfo.CodigoArquivoTSYS = respostaArquivo.CodigoArquivo;

                if (this.ProcessoExtratoInfo.MoverArquivoProcessado)
                {
                    // Retirar o arquivo da pasta de em processamento
                    string diretorioAcima = Path.GetDirectoryName(this.ProcessoExtratoInfo.CaminhoArquivoTSYS).Remove(Path.GetDirectoryName(this.ProcessoExtratoInfo.CaminhoArquivoTSYS).LastIndexOf("\\"));
                    string arquivoProcessado = diretorioAcima + "\\Processados\\" + Path.GetFileName(this.ProcessoExtratoInfo.CaminhoArquivoTSYS);
                    // System.IO.File.Move(this.ProcessoExtratoInfo.CaminhoArquivoTSYS, arquivoProcessado);
                    GerenciadorProcesso.MoverArquivo(arquivoProcessado, this.ProcessoExtratoInfo.CaminhoArquivoTSYS);
                }

                // Faz o desbloqueio dos itens solicitados
                IniciarProcessamentoDesbloqueiosResponse respostaDesbloqueio =
                    Mensageria.Processar<IniciarProcessamentoDesbloqueiosResponse>(
                        new IniciarProcessamentoDesbloqueiosRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYS,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso
                        });
            }

            // Continua essa etapa apenas se o arquivo "Atacadao" foi informado
            if (this.ProcessoExtratoInfo.CaminhoArquivoTSYSAtacadao != null)
            {
                codigoEmpresaArquivo = config.CodigoGrupoAtacadao;
                // Faz a importacao do arquivo
                ImportarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ImportarArquivoResponse>(
                        new ImportarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoExtratoInfo.CaminhoArquivoTSYSAtacadao,
                            //1333 TipoArquivo = "ArquivoTSYS",
                            TipoArquivo = "ArquivoTSYSATA",
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            CodigoGrupoEmpresaArquivo = codigoEmpresaArquivo
                        });

                // Informa codigo do arquivo no processo
                this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao = respostaArquivo.CodigoArquivo;

                if (this.ProcessoExtratoInfo.MoverArquivoProcessado)
                {
                    // Retirar o arquivo da pasta de em processamento
                    string diretorioAcima = Path.GetDirectoryName(this.ProcessoExtratoInfo.CaminhoArquivoTSYSAtacadao).Remove(Path.GetDirectoryName(this.ProcessoExtratoInfo.CaminhoArquivoTSYSAtacadao).LastIndexOf("\\"));
                    string arquivoProcessado = diretorioAcima + "\\Processados\\" + Path.GetFileName(this.ProcessoExtratoInfo.CaminhoArquivoTSYSAtacadao);
                    // System.IO.File.Move(this.ProcessoExtratoInfo.CaminhoArquivoTSYS, arquivoProcessado);
                    GerenciadorProcesso.MoverArquivo(arquivoProcessado, this.ProcessoExtratoInfo.CaminhoArquivoTSYSAtacadao);
                }

                // Faz o desbloqueio dos itens solicitados
                IniciarProcessamentoDesbloqueiosResponse respostaDesbloqueio =
                    Mensageria.Processar<IniciarProcessamentoDesbloqueiosResponse>(
                        new IniciarProcessamentoDesbloqueiosRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso
                        });
            }

            // Continua essa etapa apenas se o arquivo "Galeria" foi informado
            if (this.ProcessoExtratoInfo.CaminhoArquivoTSYSGaleria != null)
            {
                codigoEmpresaArquivo = config.CodigoGrupoGaleria;
                // Faz a importacao do arquivo
                ImportarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ImportarArquivoResponse>(
                        new ImportarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoExtratoInfo.CaminhoArquivoTSYSGaleria,
                            //1333 TipoArquivo = "ArquivoTSYS",
                            TipoArquivo = "ArquivoTSYSGAL",
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso,
                            CodigoGrupoEmpresaArquivo = codigoEmpresaArquivo
                        });

                // Informa codigo do arquivo no processo
                this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria = respostaArquivo.CodigoArquivo;

                if (this.ProcessoExtratoInfo.MoverArquivoProcessado)
                {
                    // Retirar o arquivo da pasta de em processamento
                    string diretorioAcima = Path.GetDirectoryName(this.ProcessoExtratoInfo.CaminhoArquivoTSYSGaleria).Remove(Path.GetDirectoryName(this.ProcessoExtratoInfo.CaminhoArquivoTSYSGaleria).LastIndexOf("\\"));
                    string arquivoProcessado = diretorioAcima + "\\Processados\\" + Path.GetFileName(this.ProcessoExtratoInfo.CaminhoArquivoTSYSGaleria);
                    // System.IO.File.Move(this.ProcessoExtratoInfo.CaminhoArquivoTSYS, arquivoProcessado);
                    GerenciadorProcesso.MoverArquivo(arquivoProcessado, this.ProcessoExtratoInfo.CaminhoArquivoTSYSGaleria);
                }

                // Faz o desbloqueio dos itens solicitados
                IniciarProcessamentoDesbloqueiosResponse respostaDesbloqueio =
                    Mensageria.Processar<IniciarProcessamentoDesbloqueiosResponse>(
                        new IniciarProcessamentoDesbloqueiosRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria,
                            CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso
                        });
            }


        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Caso exista (Carrefour)...
            if (this.ProcessoExtratoInfo.CodigoArquivoTSYS != null)
            {
                // ... pede o cancelamento do arquivo
                Mensageria.Processar(
                    new CancelarArquivoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYS
                    });

                // Remove indicacao do arquivo do processo
                this.ProcessoExtratoInfo.CodigoArquivoTSYS = null;
            }

            // Caso exista (Atacadão)...
            if (this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao != null)
            {
                // ... pede o cancelamento do arquivo
                Mensageria.Processar(
                    new CancelarArquivoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao
                    });

                // Remove indicacao do arquivo do processo
                this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao = null;
            }

            // Caso exista (Galeria)...
            if (this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria != null)
            {
                // ... pede o cancelamento do arquivo
                Mensageria.Processar(
                    new CancelarArquivoRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria
                    });

                // Remove indicacao do arquivo do processo
                this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria = null;
            }

        }
    }
}

