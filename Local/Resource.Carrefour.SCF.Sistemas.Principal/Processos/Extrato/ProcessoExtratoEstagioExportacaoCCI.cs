﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Extrato
{

    /// <summary>
    /// Estagio de exportacao para CCI
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoExtratoInfo),
        TipoEstagioAnterior = typeof(ProcessoExtratoEstagioVerificarCancelamentoOnlineTSYS),
        TipoProximoEstagio = typeof(ProcessoExtratoEstagioTransacaoTSYS))]
        //TipoEstagioAnterior = typeof(ProcessoExtratoEstagioAgendarPagamento),
        //TipoProximoEstagio = null)]
    //1333public class ProcessoExtratoEstagioExportacaoCCI : EstagioBase
    public class ProcessoExtratoEstagioExportacaoGrupo : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
//1333        public ProcessoExtratoEstagioExportacaoCCI(ProcessoInfo processoInfo)
        public ProcessoExtratoEstagioExportacaoGrupo(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoExtratoInfo ProcessoExtratoInfo
        {
            get { return (ProcessoExtratoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento exportacao do arquivo para CCI
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            bool atualizarProcesso = false;
            // Pede para processar apenas se o arquivo Carrefour foi informado
            if (this.ProcessoExtratoInfo.CaminhoArquivoCCI != null)
            {
                // Retorna o item solicitado
                ConfiguracaoGeralInfo configGeral =  PersistenciaHelper.Receber<ConfiguracaoGeralInfo>( codigoSessao, null);

                if (configGeral.EnviarCessaoAutomatico == true)
                {
                    if (!String.IsNullOrEmpty(this.ProcessoExtratoInfo.CodigoArquivoTSYS))
                    {
                        // Pede a exportacao do arquivo da CCI do Carrefour
                        ExportarExtratoParaGrupoResponse respostaExportar = //1333 estava ExportarExtratoParaCCIResponse
                            Mensageria.Processar<ExportarExtratoParaGrupoResponse>( //1333 estava ExportarExtratoParaCCIResponse
                                new ExportarExtratoParaGrupoRequest() // 1333 estava ExportarExtratoParaCCIRequest
                                {
                                    CodigoSessao = codigoSessao,
                                    CaminhoArquivo = this.ProcessoExtratoInfo.CaminhoArquivoCCI,
                                    CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYS,
                                    CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso
                                });
                    }
                    atualizarProcesso = true;
                }
                else
                {
                    //======================================================================================================
                    // Indica que não gerou o arquivo
                    //======================================================================================================
                    var lLog = new LogSCFProcessoEventoInfo();
                    lLog.setCodigoProcesso(this.ProcessoExtratoInfo.CodigoProcesso);
                    lLog.setDescricao("Não foi gerado arquivo para a CCI. Motivo: O sistema está configurado para não gerar automaticamente."); //1333
                    LogHelper.Salvar(codigoSessao, lLog);
                }
            }
            //1333 else
            //{
            //    //======================================================================================================
            //    // Indica que não gerou o arquivo
            //    //======================================================================================================
            //    var lLog = new LogSCFProcessoEventoInfo();
            //    lLog.setCodigoProcesso(this.ProcessoExtratoInfo.CodigoProcesso);
            //    lLog.setDescricao("Não foi exportado o arquivo para a CCI"); //1333
            //    LogHelper.Salvar(codigoSessao, lLog);
            //}
            if (this.ProcessoExtratoInfo.CaminhoArquivoATA != null)
            {
                // Retorna o item solicitado
                ConfiguracaoGeralInfo configGeral =
                    PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                        codigoSessao, null);

                if (configGeral.EnviarCessaoAutomaticoAtacadao == true)
                {
                    if (!String.IsNullOrEmpty(this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao))
                    {
                        // Pede a exportacao do arquivo da CCI do Atacadao
                        ExportarExtratoParaGrupoResponse respostaExportarAtacadao = // 1333 estava ExportarExtratoParaCCI Response
                            Mensageria.Processar<ExportarExtratoParaGrupoResponse>( //1333 estava ExportarExtratoParaCCIResponse
                                new ExportarExtratoParaGrupoRequest() //1333 estava ExportarExtratoParaCCIRequest
                                {
                                    CodigoSessao = codigoSessao,
                                    CaminhoArquivo = this.ProcessoExtratoInfo.CaminhoArquivoATA,
                                    CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSAtacadao,
                                    CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso
                                });
                    }
                    atualizarProcesso = true;
                }
                else
                {
                //==========================================================================================
                // Indica que pulou este estágio
                //==========================================================================================
                var lLog = new LogSCFProcessoEventoInfo();
                lLog.setCodigoProcesso(this.ProcessoExtratoInfo.CodigoProcesso);
                lLog.setDescricao("Não foi gerado arquivo para a ATA. Motivo: O sistema está configurado para não gerar automaticamente."); //1333
                LogHelper.Salvar(codigoSessao, lLog);
                }
            }
            //1333 inibido else
            //{
            //    //==========================================================================================
            //    // Indica que pulou este estágio
            //    //==========================================================================================
            //    var lLog = new LogSCFProcessoEventoInfo();
            //    lLog.setCodigoProcesso(this.ProcessoExtratoInfo.CodigoProcesso);
            //    lLog.setDescricao("Não foi exportado o arquivo para a ATA"); //1333
            //    LogHelper.Salvar(codigoSessao, lLog);
            //}
            if (this.ProcessoExtratoInfo.CaminhoArquivoGAL != null)
            {
                // Retorna o item solicitado
                ConfiguracaoGeralInfo configGeral =
                    PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                        codigoSessao, null);

                if (configGeral.EnviarCessaoAutomaticoGaleria == true)
                {
                    if (!String.IsNullOrEmpty(this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria))
                    {
                        // Pede a exportacao do arquivo da CCI do Atacadao
                        ExportarExtratoParaGrupoResponse respostaExportarAtacadao = // 1333 estava ExportarExtratoParaCCIResponse
                            Mensageria.Processar<ExportarExtratoParaGrupoResponse>( //1333 estava ExportarExtratoParaCCIResponse
                                new ExportarExtratoParaGrupoRequest() //1333 estava ExportarExtratoParaCCIRequest
                                {
                                    CodigoSessao = codigoSessao,
                                    CaminhoArquivo = this.ProcessoExtratoInfo.CaminhoArquivoGAL,
                                    CodigoArquivo = this.ProcessoExtratoInfo.CodigoArquivoTSYSGaleria,
                                    CodigoProcesso = this.ProcessoExtratoInfo.CodigoProcesso
                                });
                    }
                    atualizarProcesso = true;
                }
                else
                {
                    //==========================================================================================
                    // Indica que pulou este estágio
                    //==========================================================================================
                    var lLog = new LogSCFProcessoEventoInfo();
                    lLog.setCodigoProcesso(this.ProcessoExtratoInfo.CodigoProcesso);
                    lLog.setDescricao("Não foi gerado arquivo para a GAL. Motivo: O sistema está configurado para não gerar automaticamente."); //1333
                    LogHelper.Salvar(codigoSessao, lLog);
                }
            }
            //1333 inibindo
            // else
            //{
            //        //==========================================================================================
            //        // Indica que pulou este estágio
            //        //==========================================================================================
            //        var lLog = new LogSCFProcessoEventoInfo();
            //        lLog.setCodigoProcesso(this.ProcessoExtratoInfo.CodigoProcesso);
            //        lLog.setDescricao("Não foi exportado o arquivo para a GAL"); //1333
            //        LogHelper.Salvar(codigoSessao, lLog);
            //}
            if(atualizarProcesso)
            {
                // Atualiza o objeto
                this.ProcessoExtratoInfo.Atualizar(
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        codigoSessao, this.ProcessoExtratoInfo.CodigoProcesso));
            }
        }

        /// <summary>
        /// Processa o cancelamento da exportacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
        }
    }
}
