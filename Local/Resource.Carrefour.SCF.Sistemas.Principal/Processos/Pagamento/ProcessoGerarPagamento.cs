﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Pagamento
{
    [Estagio(
    TipoProcesso = typeof(ProcessoGerarPagamentoInfo),
    TipoEstagioAnterior = null,
    TipoProximoEstagio = null)]
    public class ProcessoGerarPagamento : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoGerarPagamento(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoGerarPagamentoInfo ProcessoGerarPagamentoInfo
        {
            get { return (ProcessoGerarPagamentoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geracao dos pagamentos
            Mensageria.Processar(
                new GerarPagamentobyRelatorioVenctoRequest()
                {
                    CodigoSessao = codigoSessao,
                    Request = this.ProcessoGerarPagamentoInfo.Param,
                    CodigoProcesso = this.ProcessoGerarPagamentoInfo.CodigoProcesso
                });
        }

    }
}
