﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Pagamento
{
    /// <summary>
    /// Estagio de Exportacao de Matera Financeiro
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoPagamentoInfo), 
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]
    public class ProcessoPagamentoEstagioExportacaoMateraFinanceiro : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoPagamentoEstagioExportacaoMateraFinanceiro(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoPagamentoInfo ProcessoPagamentoInfo
        {
            get { return (ProcessoPagamentoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento exportação do arquivo matera financeiro
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede para processar apenas se o arquivo foi informado
            if (this.ProcessoPagamentoInfo.CaminhoArquivoMateraFinanceiro != null)
            {

                // Buscar configuracao
                ConfiguracaoGeralInfo configGeral = new ConfiguracaoGeralInfo();

                // Retorna o item solicitado
                configGeral =
                    PersistenciaHelper.Receber<ConfiguracaoGeralInfo>(
                        codigoSessao, null);


                // Time para somar á data final                
                DateTime dataPagamento = DateTime.Now.AddDays(Int16.Parse(configGeral.PrazoPagamento));
                dataPagamento = new DateTime(dataPagamento.Year, dataPagamento.Month, dataPagamento.Day, 23, 58, 59);                               
                
                // Pede a exportacao do arquivo da matera
                ExportarMateraFinanceiroResponse respostaExportar =
                    Mensageria.Processar<ExportarMateraFinanceiroResponse>(
                        new ExportarMateraFinanceiroRequest()
                        {
                            CaminhoArquivoDestino = this.ProcessoPagamentoInfo.CaminhoArquivoMateraFinanceiro,
                            CodigoSessao = codigoSessao,
                            DataPagamento = dataPagamento,
                            CodigoProcesso = this.ProcessoPagamentoInfo.CodigoProcesso
                        });

                if (respostaExportar.StatusResposta != MensagemResponseStatusEnum.OK)
                {
                    var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();

                    lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                    lLogSCFProcessoEventoInfo.setDescricao(respostaExportar.DescricaoResposta);
                    lLogSCFProcessoEventoInfo.setTipoEstagio("ProcessoPagamentoEstagioExportacaoMateraFinanceiro");

                    // Indica que pulou este estágio
                    LogHelper.Salvar(codigoSessao, lLogSCFProcessoEventoInfo);
                }

                // Atualiza o objeto
                this.ProcessoPagamentoInfo.Atualizar(
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        codigoSessao, this.ProcessoPagamentoInfo.CodigoProcesso));
            }
        }

        /// <summary>
        /// Processa o cancelamento da exportacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoPagamentoInfo.CodigoArquivoMateraFinanceiro = null;
        }
    }
}
