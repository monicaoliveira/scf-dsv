﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Pagamento
{
    /// <summary>
    /// Estagio de Exportacao do arquivo de Pagamento
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoExportacaoPagamentoInfo), 
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]
    public class ProcessoExportacaoPagamento : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoExportacaoPagamento(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoExportacaoPagamentoInfo ProcessoExportacaoPagamentoInfo
        {
            get { return (ProcessoExportacaoPagamentoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento da exportacao do pagamento
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório de vendas
            //ProcessarExportacaoPagamentosResponse response =  Mensageria.Processar(
            //    new ProcessarExportacaoPagamentosRequest()
            //    {
            //        CodigoSessao = codigoSessao,
                    
            //        CodigoProcesso = this.ProcessoExportacaoPagamentoInfo.CodigoProcesso
            //    });

            // Gera transacoes do arquivo importado
            ProcessarExportacaoPagamentosResponse response =
                Mensageria.Processar<ProcessarExportacaoPagamentosResponse>(
                    new ProcessarExportacaoPagamentosRequest()
                    {
                        CodigoSessao = codigoSessao,

                        CodigoProcesso = this.ProcessoExportacaoPagamentoInfo.CodigoProcesso
                    });



            if (response.StatusResposta != MensagemResponseStatusEnum.OK)
                this.ProcessoInfo.EstagioValido = false;

        }

        
    }
}
