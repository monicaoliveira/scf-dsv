﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Pagamento
{
    /// <summary>
    /// Estagio de Exclusao de linha de pagamento
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoExclusaoPagamentoInfo), 
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]
    public class ProcessoExclusaoPagamento : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoExclusaoPagamento(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoExclusaoPagamentoInfo ProcessoExclusaoPagamentoInfo
        {
            get { return (ProcessoExclusaoPagamentoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento da exportacao do pagamento
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório de vendas
            Mensageria.Processar(
                new ProcessarExclusaoPagamentosRequest()
                {
                    CodigoSessao = codigoSessao,
                    CodigoPagamento = this.ProcessoExclusaoPagamentoInfo.CodigoPagamento,
                    CodigoProcesso = this.ProcessoExclusaoPagamentoInfo.CodigoProcesso
                });
        }

        
    }
}
