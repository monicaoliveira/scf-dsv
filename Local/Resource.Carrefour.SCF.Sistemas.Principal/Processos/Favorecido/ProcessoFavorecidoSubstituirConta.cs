﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Persistencia.Db.Entidades;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Favorecido
{
    /// <summary>
    /// Substituição de conta do Favorecido
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoFavorecidoSubstituirContaInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]

    public class ProcessoFavorecidoSubstituirConta : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="favorecidoInfo"></param>
        public ProcessoFavorecidoSubstituirConta(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o Favorecido
        /// </summary>
        /// <returns></returns>
        public ProcessoFavorecidoSubstituirContaInfo ProcessoFavorecidoSubstituirContaInfo
        {
            get { return (ProcessoFavorecidoSubstituirContaInfo)this.ProcessoInfo; }
        }

        protected override void OnProcessar(string codigoSessao)
        {
            //===================================================================================================================		
            // LOG DA FASE - SUBSTITUIR CONTA DE FAVORECIDO
            //===================================================================================================================		            
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(this.ProcessoFavorecidoSubstituirContaInfo.CodigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("SUBSTITUIR CONTA FAVORECIDO - Inicio - Codigo Conta de [{0}] para conta [{1}]", this.ProcessoFavorecidoSubstituirContaInfo.CodigoContaFavorecido, this.ProcessoFavorecidoSubstituirContaInfo.CodigoContaFavorecidoNovo));
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);

            // Bloco de controle
            try
            {
                SubstituirContaFavorecidoDbRequest _param =
                    new SubstituirContaFavorecidoDbRequest()
                    {
                        CodigoContaFavorecido = this.ProcessoFavorecidoSubstituirContaInfo.CodigoContaFavorecido,
                        CodigoContaFavorecidoNovo = this.ProcessoFavorecidoSubstituirContaInfo.CodigoContaFavorecidoNovo,
                        CodigoProcesso = this.ProcessoFavorecidoSubstituirContaInfo.CodigoProcesso,
                        CodigoSessao = codigoSessao
                    };

                // Substitui item solicitado
                ContaFavorecidoDbLib _executar = new ContaFavorecidoDbLib();
                _executar.SubstituirObjeto(_param);

                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(this.ProcessoFavorecidoSubstituirContaInfo.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao("SUBSTITUIR CONTA FAVORECIDO - Fim");
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(this.ProcessoFavorecidoSubstituirContaInfo.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("SUBSTITUIR CONTA FAVORECIDO - Erro - Mensagem: [{0}]", ex.Message));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            }
        }
    }
}
