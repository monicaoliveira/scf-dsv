﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Principal.Processos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Framework.Contratos.Comum.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens.Db;
using Resource.Carrefour.SCF.Persistencia.Db.Entidades;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Favorecido
{
    /// <summary>
    /// Substituição de conta do Favorecido
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoFavorecidoProdutoSubstituirContaInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]

    public class ProcessoFavorecidoProdutoSubstituirConta : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="favorecidoInfo"></param>
        public ProcessoFavorecidoProdutoSubstituirConta(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o Favorecido
        /// </summary>
        /// <returns></returns>
        public ProcessoFavorecidoProdutoSubstituirContaInfo ProcessoFavorecidoProdutoSubstituirContaInfo
        {
            get { return (ProcessoFavorecidoProdutoSubstituirContaInfo)this.ProcessoInfo; }
        }

        protected override void OnProcessar(string codigoSessao)
        {
            // Prepara resposta
            SubstituirObjetoResponse<ContaFavorecidoProdutoInfo> resposta = new SubstituirObjetoResponse<ContaFavorecidoProdutoInfo>();

            //===================================================================================================================		
            // LOG DA FASE - SUBSTITUIR CONTA DE FAVORECIDO
            //===================================================================================================================		            
            var lLogSCFProcessoEstagioEventoInfo = new LogSCFProcessoEstagioEventoInfo();
            lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(this.ProcessoFavorecidoProdutoSubstituirContaInfo.CodigoProcesso);
            lLogSCFProcessoEstagioEventoInfo.setDescricao("SUBSTITUIR CONTA FAVORECIDO PRODUTO - Inicio");
            LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);

            // Bloco de controle
            try
            {
                foreach (ProcessoFavorecidoProdutoLista dados in this.ProcessoFavorecidoProdutoSubstituirContaInfo.ListaFavorecidoProduto)
                {
                    SubstituirContaFavorecidoProdutoDbRequest _param =
                        new SubstituirContaFavorecidoProdutoDbRequest()
                        {
                            CodigoContaFavorecido = dados.CodigoContaFavorecido,
                            CodigoProduto = dados.CodigoProduto,
                            Referencia = dados.Referencia,
                            CodigoSessao = codigoSessao
                        };

                    // Substitui item solicitado
                    ContaFavorecidoProdutoDbLib _executar = new ContaFavorecidoProdutoDbLib();
                    resposta = 
                        _executar.SubstituirObjeto(_param);

                    lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(this.ProcessoFavorecidoProdutoSubstituirContaInfo.CodigoProcesso);
                    lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("SUBSTITUIR CONTA FAVORECIDO PRODUTO - Codigo Conta [{0}], Codigo Produto [{1}] e Codigo Referencia [{2}] do Favorecido [{3}]. Total substituições [{4}].", dados.CodigoContaFavorecido, dados.CodigoProduto, dados.Referencia, this.ProcessoFavorecidoProdutoSubstituirContaInfo.CodigoFavorecido, resposta.QuantidadeAlterada));
                    LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
                }

                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(this.ProcessoFavorecidoProdutoSubstituirContaInfo.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao("SUBSTITUIR CONTA FAVORECIDO PRODUTO - Fim");
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            }
            catch (Exception ex)
            {
                // Informa erro na resposta
                lLogSCFProcessoEstagioEventoInfo.setCodigoProcesso(this.ProcessoFavorecidoProdutoSubstituirContaInfo.CodigoProcesso);
                lLogSCFProcessoEstagioEventoInfo.setDescricao(string.Format("SUBSTITUIR CONTA FAVORECIDO PRODUTO - Erro - Mensagem: [{0}]", ex.Message));
                LogHelper.Salvar(null, lLogSCFProcessoEstagioEventoInfo);
            }
            finally { 
                ServicoPrincipal _executar = new ServicoPrincipal();
                SalvarFavorecidoRequest request =
                    new SalvarFavorecidoRequest()
                    {
                        FavorecidoInfo = new FavorecidoInfo
                            {
                                //Liberando o Favorecido para Edição
                                CodigoFavorecido = this.ProcessoFavorecidoProdutoSubstituirContaInfo.CodigoFavorecido,
                                FavorecidoBloqueado = false
                            }
                    };
                _executar.SalvarFavorecido(request);
            }
        }
    }
}
