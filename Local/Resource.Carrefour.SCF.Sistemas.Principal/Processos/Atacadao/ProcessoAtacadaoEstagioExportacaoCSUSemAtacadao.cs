﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Exportacao de Matera Financeiro
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioResultadoConciliacao),
        TipoProximoEstagio = null)]
    public class ProcessoAtacadaoEstagioExportacaoCSUSemAtacadao : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioExportacaoCSUSemAtacadao(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento exportação do arquivo csu sem atacadao
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede para processar apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CaminhoArquivoCSUSemAtacadao != null && this.ProcessoAtacadaoInfo.CodigoArquivoCSU != null)
            {

                // Acertar o nome do arquivo YYYYMMDD_MASemAtacadao.txt
                string nomeArquivo = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + this.ProcessoInfo.CodigoProcesso.Trim() + "_MASemAtacadao.txt";
                string caminhoArquivo;

                if (this.ProcessoAtacadaoInfo.CaminhoArquivoCSUSemAtacadao.EndsWith("/"))
                    caminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoCSUSemAtacadao + nomeArquivo;
                else
                    caminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoCSUSemAtacadao + "/" + nomeArquivo;



                // Pede a exportacao do arquivo csu sem atacadao
                ExportarCSUSemAtacadaoResponse respostaExportar =
                    Mensageria.Processar<ExportarCSUSemAtacadaoResponse>(
                        new ExportarCSUSemAtacadaoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = caminhoArquivo,
                            CodigoArquivo = this.ProcessoAtacadaoInfo.CodigoArquivoCSU
                        });
            }
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                lLogSCFProcessoEventoInfo.setDescricao("Não foi exportado o arquivo CSU sem Atacadão. Motivo: Não foi configurado o caminho ou arquivo da CSU não foi importado");
                //scf1189 lLogSCFProcessoEventoInfo.setTipoEstagio(typeof(ProcessoAtacadaoEstagioExportacaoMateraContabil).Name);
                lLogSCFProcessoEventoInfo.setTipoEstagio(typeof(ProcessoAtacadaoEstagioExportacaoCSUSemAtacadao).Name);

                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao, lLogSCFProcessoEventoInfo);
            }
        }

        /// <summary>
        /// Processa o cancelamento da exportacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
        }
    }
}
