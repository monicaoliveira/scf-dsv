﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioTransacaoSitef),
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioExportacaoMateraFinanceiro))]
    public class ProcessoAtacadaoEstagioAgendarPagamento : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioAgendarPagamento(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

            /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processa criação da agenda
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Solicita a criação da agenda
            Mensageria.Processar(
                new AgendarPagamentoAtacadaoRequest() 
                {
                    CodigoSessao = codigoSessao,
                    CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso,
                    DataReferencia = this.ProcessoInfo.DataInclusao.Value.AddDays(1).Date
                });
        }
    }
}
