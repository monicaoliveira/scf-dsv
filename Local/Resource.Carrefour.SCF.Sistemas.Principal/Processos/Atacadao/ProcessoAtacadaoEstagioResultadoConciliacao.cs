﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
//scf1189 - inicio
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
//scf1189 - fim

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Exportacao de Matera Financeiro
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioTransacaoSitef),
        //TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioExportacaoMateraFinanceiro),
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioExportacaoMateraContabil))]
    public class ProcessoAtacadaoEstagioResultadoConciliacao : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioResultadoConciliacao(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento exportação do arquivo matera financeiro
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede para processar apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CaminhoArquivoResultadoConciliacao != null)
            {
                // Acertar o nome do arquivo YYYYMMDD_CodigoProcesso_ResultadoConciliacao.csv
                string nomeArquivo = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + this.ProcessoInfo.CodigoProcesso.Trim() + "_ResultadoConciliacao.csv";
                string caminhoArquivo;

                if (this.ProcessoAtacadaoInfo.CaminhoArquivoResultadoConciliacao.EndsWith("/"))
                    caminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoResultadoConciliacao + nomeArquivo;
                else
                    caminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoResultadoConciliacao + "/" + nomeArquivo;
                
                // Pede a exportacao do arquivo da matera
                ExportarResultadoConciliacaoResponse respostaExportar =
                    Mensageria.Processar<ExportarResultadoConciliacaoResponse>(
                        new ExportarResultadoConciliacaoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivoDestino = caminhoArquivo,
                            CodigoProcesso = this.ProcessoInfo.CodigoProcesso
                        });

            }
            //scf1189 - inicio
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                lLogSCFProcessoEventoInfo.setDescricao("Não foi gerado Relatorio Conciliação. Motivo: Não foi configurado o caminho");
                lLogSCFProcessoEventoInfo.setTipoEstagio(typeof(ProcessoAtacadaoEstagioResultadoConciliacao).Name);

                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao, lLogSCFProcessoEventoInfo);
            }
            //scf1189 - fim
        }

        /// <summary>
        /// Processa o cancelamento da exportacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
        }
    }
}
