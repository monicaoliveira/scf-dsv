﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Library.Servicos.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Exportacao de Matera Financeiro
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioResultadoConciliacao),
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioExportacaoCSUSemAtacadao))]
    public class ProcessoAtacadaoEstagioExportacaoMateraContabil : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioExportacaoMateraContabil(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento exportação do arquivo matera contabil
        /// </summary>

        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede para processar apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CaminhoArquivoMateraContabil != null && this.ProcessoAtacadaoInfo.CodigoArquivoCSU != null)
            {

                // Acertar o nome do arquivo YYYYMMDD_CodigoProcesso_ReasultadoMateraContabil.txt
                string nomeArquivo = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" + this.ProcessoInfo.CodigoProcesso.Trim() + "_ResultadoMateraContabil.txt";
                string caminhoArquivo;

                if (this.ProcessoAtacadaoInfo.CaminhoArquivoMateraContabil.EndsWith("/"))
                    caminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoMateraContabil + nomeArquivo;
                else
                    caminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoMateraContabil + "/" + nomeArquivo;



                // Pede a exportacao do arquivo da matera contabil
                ExportarMateraContabilResponse respostaExportar =
                    Mensageria.Processar<ExportarMateraContabilResponse>(
                        new ExportarMateraContabilRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivoDestino = caminhoArquivo,
                            CodigoArquivo = this.ProcessoAtacadaoInfo.CodigoArquivoCSU,
                            CodigoProcesso = this.ProcessoInfo.CodigoProcesso
                        });

                if (respostaExportar.StatusResposta != MensagemResponseStatusEnum.OK)
                {
                    var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                    lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                    lLogSCFProcessoEventoInfo.setDescricao(respostaExportar.DescricaoResposta);
                    lLogSCFProcessoEventoInfo.setTipoEstagio(typeof(ProcessoAtacadaoEstagioExportacaoMateraContabil).Name);

                    // Indica que pulou este estágio
                    LogHelper.Salvar(
                        codigoSessao, lLogSCFProcessoEventoInfo);

                }
            }
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                lLogSCFProcessoEventoInfo.setDescricao("Não foi exportado o arquivo Matera Contábil. Motivo: Não foi configurado o caminho ou arquivo da CSU não foi importado");
                lLogSCFProcessoEventoInfo.setTipoEstagio(typeof(ProcessoAtacadaoEstagioExportacaoMateraContabil).Name);

                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao, lLogSCFProcessoEventoInfo);
            }
        }

        /// <summary>
        /// Processa o cancelamento da exportacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
        }
    }
}
