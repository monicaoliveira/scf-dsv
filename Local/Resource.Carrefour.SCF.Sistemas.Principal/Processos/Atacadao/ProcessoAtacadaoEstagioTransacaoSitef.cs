﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Importacao Sitef
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioImportacaoCSU),
        //TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioAgendarPagamento))] // nao vai mais processar agenda e exportacao de pagamentos por aqui
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioResultadoConciliacao))]

    
    public class ProcessoAtacadaoEstagioTransacaoSitef : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioTransacaoSitef(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo Sitef
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CodigoArquivoSitef != null)
            {
                // Gera transacoes do arquivo importado
                ImportarTransacoesSitefResponse respostaTransacaoes =
                    Mensageria.Processar<ImportarTransacoesSitefResponse>(
                        new ImportarTransacoesSitefRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoAtacadaoInfo.CodigoArquivoSitef,
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso
                        });
            }
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEventoInfo();
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoAtacadaoInfo.CodigoProcesso);
                lLogSCFProcessoEventoInfo.Descricao = "Não foi importado transações Sitef. Motivo: O arquivo não foi importado";

                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao,
                    lLogSCFProcessoEventoInfo
                    );
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoSitef = null;
        }
    }
}
