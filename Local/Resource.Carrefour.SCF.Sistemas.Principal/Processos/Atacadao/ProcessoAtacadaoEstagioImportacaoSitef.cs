﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using System.IO;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Importacao Sitef
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioTransacaoCSU),
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioTransacaoSitef))]
    public class ProcessoAtacadaoEstagioImportacaoSitef : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioImportacaoSitef(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo Sitef
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CaminhoArquivoSitef != null)
            {
                // Faz a importacao do arquivo
                ImportarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ImportarArquivoResponse>(
                        new ImportarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoSitef,
                            TipoArquivo = "ArquivoSitef",
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso
                        });

                // Informa codigo do arquivo no processo
                this.ProcessoAtacadaoInfo.CodigoArquivoSitef = respostaArquivo.CodigoArquivo;

                if (this.ProcessoAtacadaoInfo.MoverArquivoProcessado)
                {
                    // Retirar o arquivo da pasta de em processamento
                    string diretorioAcima = Path.GetDirectoryName(this.ProcessoAtacadaoInfo.CaminhoArquivoSitef).Remove(Path.GetDirectoryName(this.ProcessoAtacadaoInfo.CaminhoArquivoSitef).LastIndexOf("\\"));
                    string arquivoSITEFProcessado = diretorioAcima + "\\Processados\\" + Path.GetFileName(this.ProcessoAtacadaoInfo.CaminhoArquivoSitef);
                    // System.IO.File.Move(this.ProcessoAtacadaoInfo.CaminhoArquivoSitef, arquivoSITEFProcessado);
                    GerenciadorProcesso.MoverArquivo(arquivoSITEFProcessado, this.ProcessoAtacadaoInfo.CaminhoArquivoSitef);
                }
            }
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEventoInfo()
                    {
                        Descricao = "Não foi importado o arquivo Sitef"
                    };

                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoAtacadaoInfo.CodigoProcesso);

                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao, lLogSCFProcessoEventoInfo);
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoSitef = null;
        }
    }
}
