﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Contratos.Principal;
using Resource.Framework.Library;
using System.IO;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Importacao de Arquivo TSYS
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioTransacaoTSYS))]
    public class ProcessoAtacadaoEstagioImportacaoTSYS : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioImportacaoTSYS(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo TSYS
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS != null)
            {
                // Faz a importacao do arquivo
                ImportarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ImportarArquivoResponse>(
                        new ImportarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS,
                            //1333 TipoArquivo = "ArquivoTSYS",
                            TipoArquivo = "ArquivoTSYSATA",
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso
                        });

                // Informa codigo do arquivo no processo
                this.ProcessoAtacadaoInfo.CodigoArquivoTSYS = respostaArquivo.CodigoArquivo;

                if (this.ProcessoAtacadaoInfo.MoverArquivoProcessado)
                {
                    // Retirar o arquivo da pasta de em processamento
                    string diretorioAcima = Path.GetDirectoryName(this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS).Remove(Path.GetDirectoryName(this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS).LastIndexOf("\\"));
                    string arquivoTSYSProcessado = diretorioAcima + "\\Processados\\" + Path.GetFileName(this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS);
                    //System.IO.File.Move(this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS, arquivoTSYSProcessado);
                    GerenciadorProcesso.MoverArquivo(arquivoTSYSProcessado, this.ProcessoAtacadaoInfo.CaminhoArquivoTSYS);
                }
            }
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEventoInfo()
                {
                    Descricao = "Não foi importado o arquivo TSYS"
                };
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoAtacadaoInfo.CodigoProcesso);
                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao, lLogSCFProcessoEventoInfo);
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoTSYS = null;
        }
    }
}

