﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Importacao de Transacoes CSU
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioImportacaoCSU),
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioImportacaoSitef))]
    public class ProcessoAtacadaoEstagioTransacaoCSU : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioTransacaoCSU(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo CSU
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CodigoArquivoCSU != null)
            {
                // Pega config
                ServicoIntegracaoConfig configIntegracao = 
                    GerenciadorConfig.ReceberConfig<ServicoIntegracaoConfig>();
                if (configIntegracao == null)
                    configIntegracao = new ServicoIntegracaoConfig();

                // Acha o estabelecimento atacadão
                List<EstabelecimentoInfo> estabelecimentos =
                    PersistenciaHelper.Listar<EstabelecimentoInfo>(
                        codigoSessao, new List<CondicaoInfo>() 
                        { 
                            
                            //new CondicaoInfo("CNPJ", CondicaoTipoEnum.Igual, configIntegracao.CodigoLojaAtacadaoTSYS) 
                            new CondicaoInfo("CodigoSubGrupo", CondicaoTipoEnum.Igual, "2") 
                        });

                //// Se não veio apenas 1 estabelecimento é erro
                //if (estabelecimentos.Count != 1)
                //    throw new Exception("Foi encontrado 0 ou mais de 1 estabelecimento com o CNPJ do atacadão (" + configIntegracao.CodigoLojaAtacadaoTSYS + ") e o processo não consegue continuar.");

                
                // Se não veio apenas nenhum estabelecimento
                if (estabelecimentos.Count == 0)
                    throw new Exception("Não há estabelecimentos do Atacadão cadastradados e o processo não consegue continuar.");
                
                // Pega o código CSU
                //string codigoEstabelecimentoCSU = estabelecimentos[0].EstabelecimentoCSU;
                string codigoEstabelecimentoCSU = "";  

                foreach(EstabelecimentoInfo  estab in estabelecimentos)
                {
                    codigoEstabelecimentoCSU += estab.EstabelecimentoCSU + ";";
                }

                
                // Gera transacoes do arquivo importado
                ImportarTransacoesCSUResponse respostaTransacaoes =
                    Mensageria.Processar<ImportarTransacoesCSUResponse>(
                        new ImportarTransacoesCSURequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoAtacadaoInfo.CodigoArquivoCSU,
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso,
                            IncluirLojas = codigoEstabelecimentoCSU
 
                        });
            }
            else
            {
                    var log = new LogSCFProcessoEventoInfo()
                    {
                        Descricao = "Não foi importado transações CSU. Motivo: O arquivo não foi importado"
                    };
                log.setCodigoProcesso(this.ProcessoAtacadaoInfo.CodigoProcesso);
                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao, log);
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoCSU = null;
        }
    }
}
