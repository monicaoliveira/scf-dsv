﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos.Mensagens;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Contratos.Comum;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Exportacao de Matera Financeiro
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo), 
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioAgendarPagamento),
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioResultadoConciliacao))]
    public class ProcessoAtacadaoEstagioExportacaoMateraFinanceiro : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioExportacaoMateraFinanceiro(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento exportação do arquivo matera financeiro
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede para processar apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CaminhoArquivoMateraFinanceiro != null)
            {
                string caminhoArquivo = this.ProcessoAtacadaoInfo.CaminhoArquivoMateraFinanceiro;

                // Pede a exportacao do arquivo da matera
                ExportarMateraFinanceiroResponse respostaExportar =
                    Mensageria.Processar<ExportarMateraFinanceiroResponse>(
                        new ExportarMateraFinanceiroRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivoDestino = caminhoArquivo,
                            DataPagamento = this.ProcessoInfo.DataInclusao.Value,
                            CodigoProcesso = this.ProcessoInfo.CodigoProcesso
                        });

                if (respostaExportar.StatusResposta != MensagemResponseStatusEnum.OK)
                {
                    var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();

                    lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                    lLogSCFProcessoEventoInfo.setDescricao(respostaExportar.DescricaoResposta);
                    lLogSCFProcessoEventoInfo.setTipoEstagio("ProcessoAtacadaoEstagioExportacaoMateraFinanceiro");

                    // Indica que pulou este estágio
                    LogHelper.Salvar(codigoSessao, lLogSCFProcessoEventoInfo);
                }


                // Atualiza o objeto
                this.ProcessoAtacadaoInfo.Atualizar(
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        codigoSessao, this.ProcessoAtacadaoInfo.CodigoProcesso));
            }
            else
            {
                string descricao = "Não foi gerado o Matera Atacadão. Motivo: O diretório do arquivo não foi configurado no sistema.";

                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEstagioEventoInfo();
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoInfo.CodigoProcesso);
                lLogSCFProcessoEventoInfo.setDescricao(descricao);
                lLogSCFProcessoEventoInfo.setTipoEstagio("ProcessoAtacadaoEstagioExportacaoMateraFinanceiro");
                
                // Indica que pulou este estágio
                LogHelper.Salvar(codigoSessao, lLogSCFProcessoEventoInfo);
            }
        }

        /// <summary>
        /// Processa o cancelamento da exportacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoMateraFinanceiro = null;
        }
    }
}
