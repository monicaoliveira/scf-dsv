﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Atacadao
{
    /// <summary>
    /// Estagio de Importacao de Transacoes TSYS
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoAtacadaoInfo),
        TipoEstagioAnterior = typeof(ProcessoAtacadaoEstagioImportacaoTSYS),
        TipoProximoEstagio = typeof(ProcessoAtacadaoEstagioImportacaoCSU))]
    public class ProcessoAtacadaoEstagioTransacaoTSYS : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoAtacadaoEstagioTransacaoTSYS(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoAtacadaoInfo ProcessoAtacadaoInfo
        {
            get { return (ProcessoAtacadaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo TSYS
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoAtacadaoInfo.CodigoArquivoTSYS != null)
            {
                // Pega config
                ServicoIntegracaoConfig configIntegracao =
                    GerenciadorConfig.ReceberConfig<ServicoIntegracaoConfig>();
                if (configIntegracao == null)
                    configIntegracao = new ServicoIntegracaoConfig();

                // Acha o estabelecimento atacadão
                List<EstabelecimentoInfo> estabelecimentos =
                    PersistenciaHelper.Listar<EstabelecimentoInfo>(
                        codigoSessao, new List<CondicaoInfo>() 
                        { 
                            
                            //new CondicaoInfo("CNPJ", CondicaoTipoEnum.Igual, configIntegracao.CodigoLojaAtacadaoTSYS) 
                            new CondicaoInfo("CodigoSubGrupo", CondicaoTipoEnum.Igual, "2") 
                        });

                string codigoEstabelecimentoTSYS = "";

                foreach (EstabelecimentoInfo estab in estabelecimentos)
                {
                    codigoEstabelecimentoTSYS += estab.CNPJ + ";";
                }

                // Gera transacoes do arquivo importado
                ImportarTransacoesTSYSResponse respostaTransacaoes =
                    Mensageria.Processar<ImportarTransacoesTSYSResponse>(
                        new ImportarTransacoesTSYSRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoAtacadaoInfo.CodigoArquivoTSYS,
                            CodigoProcesso = this.ProcessoAtacadaoInfo.CodigoProcesso,
                            AlterarBancoAgenciaConta = false,
                            // IncluirLojas = configIntegracao.CodigoLojaAtacadaoTSYS
                            IncluirLojas = codigoEstabelecimentoTSYS
                        });
            }
            else
            {
                //==========================================================================================
                // Indica que pulou este estágio
                //==========================================================================================
                var lLog = new LogSCFProcessoEventoInfo();
                lLog.setCodigoProcesso(this.ProcessoAtacadaoInfo.CodigoProcesso);
                lLog.setDescricao("Não foi importado transações TSYS. Motivo: O arquivo não foi importado."); //1333
                LogHelper.Salvar(codigoSessao, lLog);
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoAtacadaoInfo.CodigoArquivoTSYS = null;
        }
    }
}

