﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    [Estagio(
    TipoProcesso = typeof(ProcessoRelatorioVencimentosExcelInfo),
    TipoEstagioAnterior = null,
    TipoProximoEstagio = null)]
    public class ProcessoRelatorioVencimentoInfo : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoRelatorioVencimentoInfo(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoRelatorioVencimentosExcelInfo ProcessoRelatorioVencimentosInfo
        {
            get { return (ProcessoRelatorioVencimentosExcelInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório de vendas
            Mensageria.Processar(
                new GerarRelatorioVencimentoExcelRequest()
                {
                    CodigoSessao = codigoSessao,
                    Request = this.ProcessoRelatorioVencimentosInfo.Param,
                    CodigoProcesso = this.ProcessoRelatorioVencimentosInfo.CodigoProcesso
                });
        }

    }
}
