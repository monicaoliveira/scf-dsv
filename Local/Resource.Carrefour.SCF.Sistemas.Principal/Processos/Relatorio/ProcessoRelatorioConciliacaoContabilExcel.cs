﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    [Estagio(
        TipoProcesso = typeof(ProcessoRelatorioConciliacaoContabilExcelInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]
    public class ProcessoRelatorioConciliacaoContabilExcel : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoRelatorioConciliacaoContabilExcel(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoRelatorioConciliacaoContabilExcelInfo ProcessoRelatorioConciliacaoContabilInfo
        {
            get { return (ProcessoRelatorioConciliacaoContabilExcelInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório de transacoes
            Mensageria.Processar(
                new GerarRelatorioConciliacaoContabilExcelRequest() 
                { 
                    CodigoSessao = codigoSessao,
                    FiltroDataFimConciliacao = this.ProcessoRelatorioConciliacaoContabilInfo.FiltroDataFimConciliacao,
                    FiltroDataInicioConciliacao = this.ProcessoRelatorioConciliacaoContabilInfo.FiltroDataInicioConciliacao,
                    CodigoProcesso = this.ProcessoRelatorioConciliacaoContabilInfo.CodigoProcesso
                });
        }

    }
}
