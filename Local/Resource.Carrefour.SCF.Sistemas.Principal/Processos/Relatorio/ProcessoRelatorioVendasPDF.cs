﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    [Estagio(
    TipoProcesso = typeof(ProcessoRelatorioVendasPDFInfo),
    TipoEstagioAnterior = null,
    TipoProximoEstagio = null)]
    public class ProcessoRelatorioVendasPDF : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoRelatorioVendasPDF(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoRelatorioVendasPDFInfo ProcessoRelatorioVendasInfo
        {
            get { return (ProcessoRelatorioVendasPDFInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório de vendas
            Mensageria.Processar(
                new GerarRelatorioVendasPDFRequest()
                {
                    CodigoSessao = codigoSessao,
                    Request = this.ProcessoRelatorioVendasInfo.Param,
                    CodigoProcesso = this.ProcessoRelatorioVendasInfo.CodigoProcesso
                });
        }

    }
}
