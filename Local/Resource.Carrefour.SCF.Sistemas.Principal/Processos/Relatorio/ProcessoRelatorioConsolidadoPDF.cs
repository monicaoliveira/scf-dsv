﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    [Estagio(
    TipoProcesso = typeof(ProcessoRelatorioConsolidadoPDFInfo),
    TipoEstagioAnterior = null,
    TipoProximoEstagio = null)]
    public class ProcessoRelatorioConsolidadoPDF : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoRelatorioConsolidadoPDF(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoRelatorioConsolidadoPDFInfo ProcessoRelatorioConsolidadoInfo
        {
            get { return (ProcessoRelatorioConsolidadoPDFInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório consolidado
            Mensageria.Processar(
                new GerarRelatorioConsolidadoPDFRequest()
                {
                    CodigoSessao = codigoSessao,
                    Request = this.ProcessoRelatorioConsolidadoInfo.Param,
                    CodigoProcesso = this.ProcessoRelatorioConsolidadoInfo.CodigoProcesso
                });
        }

    }
}
