﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio //1463
{
    [Estagio(
        TipoProcesso = typeof(ProcessoRelatorioInconsistenteAgrupadoExcelInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]
    public class ProcessoRelatorioInconsistenteAgrupadoExcel : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoRelatorioInconsistenteAgrupadoExcel(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoRelatorioInconsistenteAgrupadoExcelInfo ProcessoRelatorioInconsistenteAgrupadoExcelInfo
        {
            get { return (ProcessoRelatorioInconsistenteAgrupadoExcelInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório de vendas
            Mensageria.Processar(
                new GerarRelatorioInconsistenteAgrupadoExcelRequest()
                {
                    CodigoSessao = codigoSessao,
                    CodigoArquivo = this.ProcessoRelatorioInconsistenteAgrupadoExcelInfo.CodigoArquivo,
                    CodigoProcesso = this.ProcessoRelatorioInconsistenteAgrupadoExcelInfo.CodigoProcesso
                });
        }
    }
}
