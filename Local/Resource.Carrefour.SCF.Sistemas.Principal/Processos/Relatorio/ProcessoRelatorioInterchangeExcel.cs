﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    /// <summary>
    /// Estagio Relatório INterchange
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoRelatorioInterchangeExcelInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]
    public class ProcessoRelatorioInterchangeExcel : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>

        public ProcessoRelatorioInterchangeExcel(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoRelatorioInterchangeExcelInfo ProcessoRelatorioInterchangeInfo
        {
            get { return (ProcessoRelatorioInterchangeExcelInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            //Pede a geração do relatório de transações
            Mensageria.Processar(
                new GerarRelatorioInterchangeExcelRequest() { 
                    CodigoSessao = codigoSessao,
                    CodigoProcesso = this.ProcessoRelatorioInterchangeInfo.CodigoProcesso,
                    FiltroDataMovimentoDe = this.ProcessoRelatorioInterchangeInfo.Param.FiltroDataMovimentoDe,
                    FiltroDataMovimentoAte = this.ProcessoRelatorioInterchangeInfo.Param.FiltroDataMovimentoAte
                });
        }
    }
}
