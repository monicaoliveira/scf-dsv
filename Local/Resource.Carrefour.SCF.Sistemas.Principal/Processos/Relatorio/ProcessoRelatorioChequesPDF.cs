﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    [Estagio(
    TipoProcesso = typeof(ProcessoRelatorioChequesPDFInfo),
    TipoEstagioAnterior = null,
    TipoProximoEstagio = null)]
    public class ProcessoRelatorioChequesPDF : EstagioBase //1465
    {
        public ProcessoRelatorioChequesPDF(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        public ProcessoRelatorioChequesPDFInfo ProcessoRelatorioChequesInfo
        {
            get { return (ProcessoRelatorioChequesPDFInfo)this.ProcessoInfo; }
        }

        protected override void OnProcessar(string codigoSessao)
        {
            Mensageria.Processar(
                new GerarRelatorioChequesPDFRequest()
                {
                    CodigoSessao = codigoSessao,
                    Request = this.ProcessoRelatorioChequesInfo.Param,
                    CodigoProcesso = this.ProcessoRelatorioChequesInfo.CodigoProcesso
                });
        }
    }
}
