﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    [Estagio(
    TipoProcesso = typeof(ProcessoRelatorioRecebimentoPDFInfo),
    TipoEstagioAnterior = null,
    TipoProximoEstagio = null)]
    public class ProcessoRelatorioRecebimentoPDF : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoRelatorioRecebimentoPDF(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoRelatorioRecebimentoPDFInfo ProcessoRelatorioRecebimentoInfo
        {
            get { return (ProcessoRelatorioRecebimentoPDFInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório de vendas
            Mensageria.Processar(
                new GerarRelatorioRecebimentoPDFRequest()
                {
                    CodigoSessao = codigoSessao,
                    Request = this.ProcessoRelatorioRecebimentoInfo.Param,
                    CodigoProcesso = this.ProcessoRelatorioRecebimentoInfo.CodigoProcesso
                });
        }

    }
}
