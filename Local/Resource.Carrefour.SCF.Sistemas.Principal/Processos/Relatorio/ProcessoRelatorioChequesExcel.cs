﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    [Estagio(
    TipoProcesso = typeof(ProcessoRelatorioChequesExcelInfo), // 1465
    TipoEstagioAnterior = null,
    TipoProximoEstagio = null)]
    public class ProcessoRelatorioChequesExcel : EstagioBase
    {
        public ProcessoRelatorioChequesExcel(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        public ProcessoRelatorioChequesExcelInfo ProcessoRelatorioChequesInfo
        {
            get { return (ProcessoRelatorioChequesExcelInfo)this.ProcessoInfo; }
        }

        protected override void OnProcessar(string codigoSessao)
        {
            Mensageria.Processar(
                new GerarRelatorioChequesExcelRequest()
                {
                    CodigoSessao = codigoSessao,
                    Request = this.ProcessoRelatorioChequesInfo.Param,
                    CodigoProcesso = this.ProcessoRelatorioChequesInfo.CodigoProcesso
                });
        }
    }
}
