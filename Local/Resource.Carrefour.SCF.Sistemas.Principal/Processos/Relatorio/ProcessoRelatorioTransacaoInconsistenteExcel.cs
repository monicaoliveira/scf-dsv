﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Relatorio
{
    [Estagio(
        TipoProcesso = typeof(ProcessoRelatorioTransacaoInconsistenteExcelInfo),
        TipoEstagioAnterior = null,
        TipoProximoEstagio = null)]
    public class ProcessoRelatorioTransacaoInconsistenteExcel : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoRelatorioTransacaoInconsistenteExcel(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoRelatorioTransacaoInconsistenteExcelInfo ProcessoRelatorioTransacaoInconsistenteExcelInfo
        {
            get { return (ProcessoRelatorioTransacaoInconsistenteExcelInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Pede a geração do relatório de vendas
            Mensageria.Processar(
                new GerarRelatorioTransacaoInconsistenteExcelRequest()
                {
                    CodigoSessao = codigoSessao,
                    FiltroStatusValidacao = this.ProcessoRelatorioTransacaoInconsistenteExcelInfo.FiltroStatusValidacao,
                    CodigoArquivo = this.ProcessoRelatorioTransacaoInconsistenteExcelInfo.CodigoArquivo,
                    CodigoProcesso = this.ProcessoRelatorioTransacaoInconsistenteExcelInfo.CodigoProcesso
                });
        }
    }
}
