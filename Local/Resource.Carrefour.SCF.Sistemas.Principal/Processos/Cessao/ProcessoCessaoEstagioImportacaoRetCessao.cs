﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using System.IO;
using Resource.Framework.Contratos.Comum.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Cessao
{
    /// <summary>
    /// Estagio de Importacao do arquivo de Retorno de Cessao (CCI)
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoCessaoInfo),
        TipoEstagioAnterior =  null,
        TipoProximoEstagio = typeof(ProcessoCessaoEstagioValidacaoRetCessao))]
    public class ProcessoCessaoEstagioImportacaoRetCessao : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoCessaoEstagioImportacaoRetCessao(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoCessaoInfo ProcessoCessaoInfo
        {
            get { return (ProcessoCessaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento importação do arquivo CCI (Retorno de Cessao)
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoCessaoInfo.CaminhoArquivoRetornoCessao != null)
            {
                // Faz a importacao do arquivo
                ImportarArquivoResponse respostaArquivo =
                    Mensageria.Processar<ImportarArquivoResponse>(
                        new ImportarArquivoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CaminhoArquivo = this.ProcessoCessaoInfo.CaminhoArquivoRetornoCessao,
                            TipoArquivo = "ArquivoRetornoCessao",
                            CodigoProcesso = this.ProcessoCessaoInfo.CodigoProcesso
                        });

                // Informa codigo do arquivo no processo
                this.ProcessoCessaoInfo.CodigoArquivoRetCessao = respostaArquivo.CodigoArquivo;

                if (this.ProcessoCessaoInfo.MoverArquivoProcessado)
                {
                    // Retirar o arquivo da pasta de em processamento
                    string diretorioAcima = Path.GetDirectoryName(this.ProcessoCessaoInfo.CaminhoArquivoRetornoCessao).Remove(Path.GetDirectoryName(this.ProcessoCessaoInfo.CaminhoArquivoRetornoCessao).LastIndexOf("\\"));
                    string arquivoProcessado = diretorioAcima + "\\Processados\\" + Path.GetFileName(this.ProcessoCessaoInfo.CaminhoArquivoRetornoCessao);
                    // System.IO.File.Move(this.ProcessoCessaoInfo.CaminhoArquivoRetornoCessao, arquivoProcessado);
                    GerenciadorProcesso.MoverArquivo(arquivoProcessado, this.ProcessoCessaoInfo.CaminhoArquivoRetornoCessao);
                }

            }
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEventoInfo()
                {
                    Descricao = "Não foi importado o arquivo Retorno de Cessão"
                };
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoCessaoInfo.CodigoProcesso);
                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao, lLogSCFProcessoEventoInfo);
            }

            // validacao de linhas desbloqueadas de arquivos de outros processos
            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();
            condicoes.Add(
                new CondicaoInfo(
                    "StatusArquivoItem", CondicaoTipoEnum.Igual, ArquivoItemStatusEnum.PendenteDesbloqueio));

            List<ArquivoItemResumoInfo> arquivoItem =
                PersistenciaHelper.Listar<ArquivoItemResumoInfo>(null, condicoes);

            // Faz o processamento do desbloqueio
            IniciarProcessamentoDesbloqueiosResponse respostaDesbloqueio =
                Mensageria.Processar<IniciarProcessamentoDesbloqueiosResponse>(
                    new IniciarProcessamentoDesbloqueiosRequest()
                    {
                        CodigoSessao = codigoSessao,
                        CodigoArquivo = this.ProcessoCessaoInfo.CodigoArquivoRetCessao,
                        CodigoProcesso = this.ProcessoCessaoInfo.CodigoProcesso
                    });

        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoCessaoInfo.CodigoArquivoRetCessao = null;
        }
    }
}
