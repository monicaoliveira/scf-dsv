﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Carrefour.SCF.Contratos.Integracao;
using Resource.Framework.Library;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Cessao
{
    /// <summary>
    /// Estagio de Importacao de Transacoes CSU
    /// </summary>
    [Estagio(
        TipoProcesso = typeof(ProcessoCessaoInfo),
        TipoEstagioAnterior = typeof(ProcessoCessaoEstagioValidacaoRetCessao),
        TipoProximoEstagio = null)]
    public class ProcessoCessaoEstagioAtualizarTransacao : EstagioBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoCessaoEstagioAtualizarTransacao(ProcessoInfo processoInfo)
            : base(processoInfo)
        {
        }

        /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoCessaoInfo ProcessoCessaoInfo
        {
            get { return (ProcessoCessaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnProcessar(string codigoSessao)
        {
            // Continua apenas se o arquivo foi informado
            if (this.ProcessoCessaoInfo.CaminhoArquivoRetornoCessao != null)
            {
                // Pega config
                ServicoIntegracaoConfig configIntegracao = 
                    GerenciadorConfig.ReceberConfig<ServicoIntegracaoConfig>();
                if (configIntegracao == null)
                    configIntegracao = new ServicoIntegracaoConfig();

                // Atualiza as transacoes
                AtualizarTransacoesRetornoCessaoResponse respostaTransacaoes =
                    Mensageria.Processar<AtualizarTransacoesRetornoCessaoResponse>(
                        new AtualizarTransacoesRetornoCessaoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoCessaoInfo.CodigoArquivoRetCessao,
                            CodigoProcesso = this.ProcessoCessaoInfo.CodigoProcesso
                        });

                if (respostaTransacaoes.StatusResposta != Resource.Framework.Library.Servicos.Mensagens.MensagemResponseStatusEnum.OK)
                {
                    var lLogSCFProcessoEventoInfo = 
                        new LogSCFProcessoEventoInfo()
                        {
                            Descricao = "Não foram atualizadas as transações. Erro: " + respostaTransacaoes.Erro
                        };

                    lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoCessaoInfo.CodigoProcesso);

                    // Indica que pulou este estágio
                    LogHelper.Salvar(
                        codigoSessao, lLogSCFProcessoEventoInfo);
                }

                // Atualiza o objeto
                this.ProcessoCessaoInfo.Atualizar(
                    PersistenciaHelper.Receber<ProcessoInfo>(
                        codigoSessao, this.ProcessoCessaoInfo.CodigoProcesso));
            }
            else
            {
                var lLogSCFProcessoEventoInfo = new LogSCFProcessoEventoInfo()
                    {
                        Descricao = "Não foram atualizadas as transações. Motivo: O arquivo não foi importado"
                    };
                lLogSCFProcessoEventoInfo.setCodigoProcesso(this.ProcessoCessaoInfo.CodigoProcesso);

                // Indica que pulou este estágio
                LogHelper.Salvar(
                    codigoSessao,
                    lLogSCFProcessoEventoInfo
                    );
            }
        }

        /// <summary>
        /// Processa o cancelamento da importacao do arquivo
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override void OnCancelar(string codigoSessao)
        {
            // Sinaliza o arquivo como cancelado

            // Remove indicacao do arquivo do processo
            this.ProcessoCessaoInfo.CodigoArquivoRetCessao = null;
        }
    }
}
