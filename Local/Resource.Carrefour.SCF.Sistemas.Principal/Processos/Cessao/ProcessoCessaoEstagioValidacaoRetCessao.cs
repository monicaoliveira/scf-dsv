﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Framework.Contratos.Comum;

namespace Resource.Carrefour.SCF.Sistemas.Principal.Processos.Cessao
{
    [Estagio(
        TipoProcesso = typeof(ProcessoCessaoInfo),
        TipoEstagioAnterior = typeof(ProcessoCessaoEstagioImportacaoRetCessao),
        TipoProximoEstagio = typeof(ProcessoCessaoEstagioAtualizarTransacao),
        EhValidacao = true )]

    public class ProcessoCessaoEstagioValidacaoRetCessao : EstagioBase
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Construtor default
        /// </summary>
        /// <param name="processoInfo"></param>
        public ProcessoCessaoEstagioValidacaoRetCessao(ProcessoInfo processoInfo)
            : base(processoInfo)
        {

        }

                /// <summary>
        /// Retorna o processo com o tipo correto
        /// </summary>
        /// <returns></returns>
        public ProcessoCessaoInfo ProcessoCessaoInfo
        {
            get { return (ProcessoCessaoInfo)this.ProcessoInfo; }
        }

        /// <summary>
        /// Processamento atualizacao das transacoes
        /// </summary>
        /// <param name="codigoSessao"></param>
        protected override bool OnValidar(string codigoSessao)
        {
            // Faz o log
            log.Debug(this.ProcessoCessaoInfo);
            int quantidadeCriticas = 0;

            // executa a verificação do grupo Carrefour
            if (this.ProcessoCessaoInfo.CodigoArquivoRetCessao != null)
            {
                // Pede a validação do arquivo
                ValidarArquivoCessaoResponse respostaCarrefour =
                    Mensageria.Processar<ValidarArquivoCessaoResponse>(
                        new ValidarArquivoCessaoRequest()
                        {
                            CodigoSessao = codigoSessao,
                            CodigoArquivo = this.ProcessoCessaoInfo.CodigoArquivoRetCessao,
                            CodigoProcesso = this.ProcessoCessaoInfo.CodigoProcesso
                        });

                quantidadeCriticas += respostaCarrefour.QuantidadeCriticas;
            }

            // Retorna
            return quantidadeCriticas == 0;
        }
    }
}
