﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Resource.Carrefour.SCF.Sistemas.Integracao
{
    public enum LayoutArquivoEnum
    {
        [Description("001.6c")]        Layout1 = 1,
        [Description("001.6d")]        Layout2 = 2,
        [Description("001.6d e-commerce CV, AJ não")]        Layout3 = 3,
        [Description("001.6d e-commerce AJ, CV não")]        Layout4 = 4,
        [Description("001.6e Referencia")]        Layout5 = 5
    }
}
