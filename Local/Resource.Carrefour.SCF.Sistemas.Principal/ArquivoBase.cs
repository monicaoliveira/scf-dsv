﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Resource.Carrefour.SCF.Persistencia.Db;
using System.Data;
using Oracle.DataAccess.Client;
using Resource.Framework.Library.Servicos.Dados;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    /// <summary>
    /// Classe base 
    /// </summary>
    public abstract class ArquivoBase
    {
        #region Manipulação de Linhas

        /// <summary>
        /// Infere o tipo da linha
        /// </summary>
        /// <param name="linha"></param>
        public string InferirTipoLinha(string linha)
        {
            return this.OnInferirTipoLinha(linha);
        }

        /// <summary>
        /// Infere o tipo da linha
        /// </summary>
        /// <param name="linha"></param>
        public string InferirTipoLinha(object linha)
        {
            return this.OnInferirTipoLinha(linha);
        }

        /// <summary>
        /// Método virtual para inferir o tipo da linha
        /// </summary>
        protected virtual string OnInferirTipoLinha(object linha)
        {
            return null;
        }

        /// <summary>
        /// Cria uma nova linha recebendo os parametros por array
        /// </summary>
        public string CriarLinha(string tipoLinha, params object[] valores)
        {
            // Refaz a coleção de valores
            Dictionary<string, object> valores2 = new Dictionary<string, object>();
            for (int i = 0; i < valores.Length; i += 2)
                valores2.Add((string)valores[i], valores[i + 1]);

            // Repassa a chamada
            return this.CriarLinha(tipoLinha, valores2);
        }

        /// <summary>
        /// Cria uma nova linha recebendo os parametros por dicionario
        /// </summary>
        public string CriarLinha(string tipoLinha, Dictionary<string, object> valores)
        {
            // Chama o método virtual
            return this.OnCriarLinha(tipoLinha, valores);
        }

        /// <summary>
        /// Método virtual para a criação da linha
        /// </summary>
        protected virtual string OnCriarLinha(string tipoLinha, Dictionary<string, object> valores)
        {
            return null;
        }

        /// <summary>
        /// Faz a leitura do campo
        /// Infere o tipo da linha através do método InferirTipoLinha
        /// Passa interpretar campo como false
        /// </summary>
        public object LerCampo(object linha, string nomeCampo)
        {
            // Repassa a chamada
            return this.LerCampo(this.InferirTipoLinha((string)linha), (string)linha, nomeCampo, false);
        }

        /// <summary>
        /// Faz a leitura do campo
        /// Infere o tipo da linha através do método InferirTipoLinha
        /// </summary>
        public object LerCampo(object linha, string nomeCampo, bool interpretar)
        {
            // Repassa a chamada
            return this.LerCampo(this.InferirTipoLinha((string)linha), (string)linha, nomeCampo, interpretar);
        }

        /// <summary>
        /// Faz a leitura do campo
        /// Infere o tipo da linha através do método InferirTipoLinha
        /// </summary>
        public object LerCampo(object linha, int indiceCampo, bool interpretar)
        {
            // Repassa a chamada
            return this.LerCampo(this.InferirTipoLinha(linha), linha, indiceCampo, interpretar);
        }

        /// <summary>
        /// Faz a leitura do campo
        /// Passa interpretar campo como false
        /// </summary>
        public object LerCampo(string tipoLinha, object linha, string nomeCampo)
        {
            // Repassa a chamada
            return this.LerCampo(tipoLinha, linha, nomeCampo, false);
        }

        /// <summary>
        /// Faz a leitura de uma Campo
        /// </summary>
        public object LerCampo(string tipoLinha, object linha, string nomeCampo, bool interpretar)
        {
            // Chama o método virtual
            return this.OnLerCampo(tipoLinha, linha, nomeCampo, interpretar);
        }

        /// <summary>
        /// Faz a leitura de uma Campo
        /// </summary>
        public object LerCampo(string tipoLinha, object linha, int indiceCampo, bool interpretar)
        {
            // Chama o método virtual
            return this.OnLerCampo(tipoLinha, linha, indiceCampo, interpretar);
        }

        /// <summary>
        /// Método virtual de leitura de Campo
        /// </summary>
        protected virtual object OnLerCampo(string tipoLinha, object linha, object campo, bool interpretar)
        {
            return null;
        }

        /// <summary>
        /// Escreve um valor na Campo
        /// </summary>
        /// <returns>Retorna a linha com o novo valor da Campo</returns>
        public string EscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor)
        {
            // Chama o método virtual
            return this.OnEscreverCampo(tipoLinha, linha, nomeCampo, valor);
        }

        /// <summary>
        /// Método virtual para escrever valor em uma Campo
        /// </summary>
        /// <returns>Retorna a linha com o novo valor da Campo</returns>
        protected virtual string OnEscreverCampo(string tipoLinha, object linha, string nomeCampo, object valor)
        {
            return null;
        }

        #endregion

        #region Importacao

        /// <summary>
        /// Faz a importacao do arquivo
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns>Código do arquivo gerado</returns>
        public long ImportarArquivo(string arquivo, string codigoProcesso)
        {
            return this.OnImportarArquivo(arquivo, codigoProcesso);
        }

        /// <summary>
        /// Método virtual para importar arquivo
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        protected virtual long OnImportarArquivo(string arquivo, string codigoProcesso)
        {
            // Prepara o retorno
            long codigoArquivo = 0;

            // Abre conexao
            OracleConnection cn = Procedures.ReceberConexao();
            
            // Bloco de controle
            try
            {
                // Abre o arquivo solicitado
                FileInfo fileInfo = new FileInfo(arquivo);
                StreamReader reader = File.OpenText(arquivo);

                // Cria o registro do arquivo
                DataRow drArquivo = Procedures.PR_ARQUIVO_S(cn, null, this.GetType().Name, fileInfo.Name, "0", true, codigoProcesso);
                codigoArquivo = (long)drArquivo["CODIGO_ARQUIVO"];

                // Inicializa command de item de arquivo
                OracleCommand cmArquivoItemS = Procedures.PR_ARQUIVO_ITEM_S_preparar(cn);

                // Mantem numero de linha
                int numeroLinha = 0;

                // Varre as linhas adicionando no banco
                while (!reader.EndOfStream)
                {
                    // Recebe a linha
                    string linha = reader.ReadLine().Trim();

                    // Atualiza numero da linha
                    numeroLinha++;

                    // Pede a importacao da linha
                    this.ImportarLinha(cmArquivoItemS, codigoArquivo, numeroLinha, linha);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Finaliza conexao
                if (cn != null)
                    cn.Dispose();
            }

            // Retorna
            return codigoArquivo;
        }

        /// <summary>
        /// Faz a importacao da linha
        /// </summary>
        /// <param name="codigoArquivo"></param>
        /// <param name="linha"></param>
        public void ImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha)
        {
            this.OnImportarLinha(cm, codigoArquivo, numeroLinha, linha);
        }

        /// <summary>
        /// Método virtual para importação da linha
        /// </summary>
        /// <param name="codigoArquivo"></param>
        /// <param name="linha"></param>
        protected virtual void OnImportarLinha(OracleCommand cm, long codigoArquivo, int numeroLinha, string linha)
        {
        }

        #endregion

        #region Validação

        /// <summary>
        /// Retorna a lista de regras para o arquivo
        /// </summary>
        /// <returns></returns>
        public List<RegraSCFBase> ReceberRegras()
        {
            return this.OnReceberRegras();
        }

        /// <summary>
        /// Método virtual para retornar a lista de regras
        /// </summary>
        /// <returns></returns>
        protected virtual List<RegraSCFBase> OnReceberRegras()
        {
            return null;
        }

        /// <summary>
        /// Faz a validação do arquivo com o conjunto de regras informadas
        /// Pede a lista de regras para o método ReceberRegras
        /// </summary>
        /// <returns>Retorna a quantidade de críticas geradas</returns>
        public int ValidarArquivo(string codigoProcesso, string codigoArquivo)
        {
            return this.OnValidarArquivo(codigoProcesso, codigoArquivo, this.ReceberRegras());
        }

        /// <summary>
        /// Faz a validação do arquivo com o conjunto de regras informadas
        /// </summary>
        /// <returns>Retorna a quantidade de críticas geradas</returns>
        public int ValidarArquivo(string codigoProcesso, string codigoArquivo, List<RegraSCFBase> regras)
        {
            // Chama o método virtual
            return this.OnValidarArquivo(codigoProcesso, codigoArquivo, regras);
        }

        /// <summary>
        /// Faz a validação do arquivo com o conjunto de regras informadas
        /// </summary>
        /// <returns>Retorna a quantidade de críticas encontrada</returns>
        protected int OnValidarArquivo(string codigoProcesso, string codigoArquivo, List<RegraSCFBase> regras)
        {
            // Inicializa
            int quantidadeCriticas = 0;
            List<CriticaSCFArquivoInfo> criticasArquivo = new List<CriticaSCFArquivoInfo>();

            // Pega conexao com o banco
            OracleConnection cn = Procedures.ReceberConexao();

            // Pega o command para salvar criticas
            OracleCommand cmSalvarCritica = Procedures.PR_CRITICA_S_preparar(cn);

            // Pede as linhas do arquivo
            OracleDataReader reader = 
                Procedures.PR_ARQUIVO_ITEM_L(
                    cn, codigoArquivo, null, 0).GetDataReader();

            // Monta a mensagem de validacao do arquivo
            RegraSCFValidarArquivoRequest requestValidarArquivo =
                new RegraSCFValidarArquivoRequest()
                {
                    Arquivo = this,
                    CodigoArquivo = codigoArquivo
                };

            // Para as regras com escopo de arquivo, dispara Inicializacao
            requestValidarArquivo.Criticas = new List<CriticaSCFArquivoInfo>();
            foreach (RegraSCFBase regra in regras)
                regra.IniciarValidacaoArquivo(requestValidarArquivo);

            // Persiste as críticas de inicio de arquivo
            foreach (CriticaSCFArquivoInfo criticaArquivoInfo in requestValidarArquivo.Criticas)
            {
                criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
            }

            // Varre as linhas executando as regras
            while (reader.Read())
            {
                // Pega a linha
                string linha = 
                    reader.GetString(
                        reader.GetOrdinal("CONTEUDO_ARQUIVO_ITEM"));

                // Acha o tipo da linha
                string tipoLinha =
                    reader.GetString(
                        reader.GetOrdinal("TIPO_ARQUIVO_ITEM"));

                // Monta a mensagem
                RegraSCFValidarLinhaRequest requestValidarLinha =
                    new RegraSCFValidarLinhaRequest()
                    {
                        Criticas = new List<CriticaSCFArquivoItemInfo>(),
                        Arquivo = this,
                        CodigoProcesso = codigoProcesso,
                        CodigoArquivo = codigoArquivo,
                        CodigoArquivoItem = reader.GetDouble(reader.GetOrdinal("CODIGO_ARQUIVO_ITEM")).ToString(),
                        Linha = linha,
                        TipoLinha = tipoLinha 
                    };

                // Inicializa as regras
                foreach (RegraSCFBase regra in regras)
                    regra.IniciarValidacaoLinha(requestValidarLinha);

                // Pede a validação
                foreach (RegraSCFBase regra in regras)
                    regra.ValidarLinha(requestValidarLinha);

                // Finaliza as regras
                foreach (RegraSCFBase regra in regras)
                    regra.FinalizarValidacaoLinha(requestValidarLinha);

                // Atualiza a quantidade de críticas
                quantidadeCriticas += requestValidarLinha.Criticas.Count;

                // Persiste as críticas da linha
                foreach (CriticaSCFArquivoInfo criticaArquivoInfo in requestValidarLinha.Criticas)
                {
                    criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                    criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                    ((CriticaSCFArquivoItemInfo)criticaArquivoInfo).CodigoArquivoItem = requestValidarLinha.CodigoArquivoItem;
                    Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
                }
            }

            // Para as regras com escopo de arquivo, dispara Finalizacao
            requestValidarArquivo.Criticas = new List<CriticaSCFArquivoInfo>();
            foreach (RegraSCFBase regra in regras.Where(r => r.Escopo == RegraSCFEscopoEnum.Arquivo))
                regra.FinalizarValidacaoArquivo(requestValidarArquivo);

            // Persiste as críticas de fim de arquivo
            foreach (CriticaSCFArquivoInfo criticaArquivoInfo in requestValidarArquivo.Criticas)
            {
                criticaArquivoInfo.CodigoProcesso = codigoProcesso;
                criticaArquivoInfo.CodigoArquivo = codigoArquivo;
                Procedures.PR_CRITICA_S(cmSalvarCritica, criticaArquivoInfo);
            }

            // Retorna
            return quantidadeCriticas;
        }

        #endregion

    }
}
