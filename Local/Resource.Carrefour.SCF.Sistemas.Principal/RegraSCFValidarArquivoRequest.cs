﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    /// <summary>
    /// Mensagem local para validação de arquivo
    /// </summary>
    public class RegraSCFValidarArquivoRequest
    {
        /// <summary>
        /// Lista de críticas
        /// </summary>
        public List<CriticaSCFArquivoInfo> Criticas { get; set; }

        /// <summary>
        /// Interpretador do arquivo
        /// </summary>
        public ArquivoBase Arquivo { get; set; }

        /// <summary>
        /// Código do arquivo que está sendo validado
        /// </summary>
        public string CodigoArquivo { get; set; }
    }
}
