﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Resource.Carrefour.SCF.Contratos.Principal.Dados;
using Resource.Carrefour.SCF.Sistemas.Integracao;
using Resource.Framework.Contratos.Comum;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;
using Resource.Framework.Contratos.Comum.Dados;
using Resource.Carrefour.SCF.Contratos.Integracao.Mensagens;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal.Mensagens;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{
    public static class GerenciadorProcesso
    {

        public static bool Processar(string codigoSessao, ProcessoInfo processoInfo)
        {   while (processoInfo.StatusProcesso == ProcessoStatusEnum.EmAndamento && GerenciadorProcesso.IrProximoEstagio(codigoSessao, processoInfo))// Vai pedindo o processamento
            {
            }
            return processoInfo.StatusProcesso == ProcessoStatusEnum.Finalizado;// Indica se chegou no final
        }

        public static bool Cancelar(string codigoSessao, ProcessoInfo processoInfo)
        {

            if (processoInfo.StatusProcesso == ProcessoStatusEnum.Finalizado)   // Se o processo estiver finalizado, navega para o ultimo estagio
                GerenciadorProcesso.IrEstagioAnterior(codigoSessao, processoInfo);
            
            while (processoInfo.TipoEstagioAtual != null)
                GerenciadorProcesso.IrEstagioAnterior(codigoSessao, processoInfo);  // Vai pedindo o processamento

            processoInfo.StatusProcesso = ProcessoStatusEnum.Cancelado; // Indica se chegou no final
            processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));// Salva

            // PROCESSO CANCELADO
            var lLogSCFProcessoCanceladoInfoSalvar = new LogSCFProcessoCanceladoInfo();
            lLogSCFProcessoCanceladoInfoSalvar.setCodigoProcesso(processoInfo.CodigoProcesso);
            LogHelper.Salvar(codigoSessao, lLogSCFProcessoCanceladoInfoSalvar);

            List<CondicaoInfo> condicoes = new List<CondicaoInfo>();    // cancela os arquivos referentes ao processo
            condicoes.Add(new CondicaoInfo("CodigoProcesso", CondicaoTipoEnum.Igual, processoInfo.CodigoProcesso)); // cria as condicoes
            List<ArquivoInfo> listaArquivos = PersistenciaHelper.Listar<ArquivoInfo>(codigoSessao, condicoes);// pede a lista de arquivos

            foreach (ArquivoInfo arquivo in listaArquivos)  // cancela os arquivos referentes ao processo
            {
                Mensageria.Processar<CancelarArquivoResponse>(
                    new CancelarArquivoRequest()
                    {
                        CodigoArquivo = arquivo.CodigoArquivo,
                        ExcluiArquivoItem = false
                    });
            }

            // Retorna
            return true;
        }

        public static bool IrProximoEstagio(string codigoSessao, ProcessoInfo processoInfo)
        {
            bool processado = false;    // Flag para indica se foi processado

            if (processoInfo.StatusProcesso != ProcessoStatusEnum.EmAndamento)  // Se o status do processo for diferente de EmAndamento, é erro
                throw new ProcessoException("Status do processo inválido para ir para próximo estágio");

            EstagioBase estagio = GerenciadorProcesso.criarEstagio(codigoSessao, processoInfo.TipoEstagioAtual, processoInfo);// Cria instancia do estagio atual

            if (processoInfo.EstagioValido == null || processoInfo.EstagioValido == false)  // Verifica se deve pedir a validação do estágio
            {
                processoInfo.StatusEstagio = EstagioStatusEnum.EmValidacao;// Informa status
                processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));// Salva

                try
                {
                    processoInfo.EstagioValido = estagio.Validar(codigoSessao); // Pede a validação
                    var processoInfo2 = PersistenciaHelper.Receber<ProcessoInfo>(codigoSessao, processoInfo.CodigoProcesso); //ClockWork - Verificar se houve bloqueio automatico de regra AJ.

                    if (processoInfo2.StatusBloqueio == ProcessoStatusBloqueioEnum.HaBloqueios)
                        processoInfo.StatusBloqueio = ProcessoStatusBloqueioEnum.HaBloqueios;
                }
                catch (Exception ex)
                {
                    var lLogSCFProcessoEstagioErroInfo = new LogSCFProcessoEstagioErroInfo();
                    lLogSCFProcessoEstagioErroInfo.Erro = ex.Message.ToString();
                    lLogSCFProcessoEstagioErroInfo.setCodigoProcesso(processoInfo.CodigoProcesso);
                    lLogSCFProcessoEstagioErroInfo.setTipoEstagio(processoInfo.TipoEstagioAtualString);                    
                    LogHelper.Salvar(codigoSessao,lLogSCFProcessoEstagioErroInfo);

                    processoInfo.StatusEstagio = EstagioStatusEnum.Erro;// Indica erro no estagio atual
                    PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo);// Salva o processo
                    
                    return false;
                }
            }

            if (processoInfo.EstagioValido == true) // Verifica se pode prosseguir
            {
                processoInfo.StatusEstagio = EstagioStatusEnum.EmExecucao;  // Informa status
                processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));    // Salva

                try
                {
                    estagio.Processar(codigoSessao);
                }
                catch (Exception ex)
                {
                    var lLogSCFProcessoEstagioErroInfo = new LogSCFProcessoEstagioErroInfo();
                    lLogSCFProcessoEstagioErroInfo.Erro = ex.Message.ToString();
                    lLogSCFProcessoEstagioErroInfo.setCodigoProcesso(processoInfo.CodigoProcesso);
                    lLogSCFProcessoEstagioErroInfo.setTipoEstagio(processoInfo.TipoEstagioAtualString);
                    LogHelper.Salvar(codigoSessao, lLogSCFProcessoEstagioErroInfo);

                    processoInfo.StatusEstagio = EstagioStatusEnum.Erro;
                    PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo);
                    return false;
                }

                processado = true;
                processoInfo.StatusEstagio = EstagioStatusEnum.Finalizado;
                processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));

                //1353
                //// Log de fim do estagio
                //if (processoInfo.TipoEstagioAtual != null)
                //{
                //    var lLogSCFProcessoEstagioFimInfo = new LogSCFProcessoEstagioFimInfo();
                //    lLogSCFProcessoEstagioFimInfo.setCodigoProcesso(processoInfo.CodigoProcesso);
                //    lLogSCFProcessoEstagioFimInfo.setTipoEstagio(processoInfo.TipoEstagioAtualString);
                //    LogHelper.Salvar(
                //        codigoSessao,
                //        lLogSCFProcessoEstagioFimInfo);
                //}

                // Vai para o proximo estagio
                GerenciadorProcesso.navegarEstagio("proximoEstagio", codigoSessao, processoInfo);

                //1353
                //// Log de inicio do estagio
                //if (processoInfo.TipoEstagioAtual != null)
                //{
                //    var lLogSCFProcessoEstagioInicioInfo = new LogSCFProcessoEstagioInicioInfo();
                //    lLogSCFProcessoEstagioInicioInfo.setCodigoProcesso(processoInfo.CodigoProcesso);
                //    lLogSCFProcessoEstagioInicioInfo.setTipoEstagio(processoInfo.TipoEstagioAtualString);
                //    LogHelper.Salvar(
                //        codigoSessao, lLogSCFProcessoEstagioInicioInfo);
                //}

                processoInfo.StatusEstagio = EstagioStatusEnum.Parado;
                processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));
            }
            else
            {
                processoInfo.StatusEstagio = EstagioStatusEnum.AguardandoValidacao;
                processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));

                if (processoInfo.TipoEstagioAtual != null)
                {
                    var lLogSCFProcessoEstagioAguardandoValidacaoInfo = new LogSCFProcessoEstagioAguardandoValidacaoInfo();
                    lLogSCFProcessoEstagioAguardandoValidacaoInfo.setCodigoProcesso(processoInfo.CodigoProcesso);
                    lLogSCFProcessoEstagioAguardandoValidacaoInfo.setTipoEstagio(processoInfo.TipoEstagioAtualString);
                    LogHelper.Salvar(codigoSessao, lLogSCFProcessoEstagioAguardandoValidacaoInfo);
                }
            }

            return processado;
        }

        public static void IrEstagioAnterior(string codigoSessao, ProcessoInfo processoInfo)
        {
            if (processoInfo.StatusProcesso == ProcessoStatusEnum.Cancelado)
                throw new ProcessoException("Status do processo inválido para ir para estágio anterior");

            if (processoInfo.TipoEstagioAtual != null)
            {
                EstagioBase estagio = GerenciadorProcesso.criarEstagio(codigoSessao, processoInfo.TipoEstagioAtual, processoInfo);
                processoInfo.StatusEstagio = EstagioStatusEnum.EmCancelamento;
                processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));
                estagio.Cancelar(codigoSessao);
                var lLogSCFProcessoEstagioCanceladoInfo = new LogSCFProcessoEstagioCanceladoInfo();
                lLogSCFProcessoEstagioCanceladoInfo.setCodigoProcesso(processoInfo.CodigoProcesso);
                lLogSCFProcessoEstagioCanceladoInfo.setTipoEstagio(processoInfo.TipoEstagioAtualString);
                LogHelper.Salvar(codigoSessao, lLogSCFProcessoEstagioCanceladoInfo);
            }

            if (processoInfo.StatusProcesso == ProcessoStatusEnum.Finalizado)
                GerenciadorProcesso.navegarEstagio("ultimoEstagio", codigoSessao, processoInfo);
            else
                GerenciadorProcesso.navegarEstagio("estagioAnterior", codigoSessao, processoInfo);

            processoInfo.StatusEstagio = EstagioStatusEnum.Parado;
            processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));
        }

        public static bool ValidarEstagio(string codigoSessao, ProcessoInfo processoInfo)
        {
            EstagioBase estagio = GerenciadorProcesso.criarEstagio(codigoSessao, processoInfo.TipoEstagioAtual, processoInfo);
            processoInfo.EstagioValido = estagio.Validar(codigoSessao);
            processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));
            return processoInfo.EstagioValido.Value;
        }

        public static void IniciarProcesso(string codigoSessao, ProcessoInfo processoInfo)
        {
            //================================================================================================
            // Se o estagio atual não for nulo, tem algo errado
            //================================================================================================
            if (processoInfo.TipoEstagioAtual != null)
                throw new ProcessoException("Não é possível iniciar um processo que já está em andamento");

            //================================================================================================
            // Cria o estagio inicial
            //================================================================================================
            GerenciadorProcesso.navegarEstagio("estagioInicial", codigoSessao, processoInfo);

            //================================================================================================
            // Seta o status
            //================================================================================================
            processoInfo.StatusProcesso = ProcessoStatusEnum.EmAndamento;
            //================================================================================================
            // Salva
            //================================================================================================
            processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));
            //================================================================================================
            // Log
            //================================================================================================
            //1333  nao tem descricao gera log vazio
            // var lLog = new LogSCFProcessoInicioInfo();
            //lLog.setCodigoProcesso(processoInfo.CodigoProcesso);
            //LogHelper.Salvar(codigoSessao, lLog);
        }

        private static EstagioBase criarEstagio(string codigoSessao, Type tipoEstagio, ProcessoInfo processoInfo)
        {
            EstagioBase estagio = (EstagioBase)Activator.CreateInstance(tipoEstagio, processoInfo);
            processoInfo.TipoEstagioAtual = tipoEstagio;
            return estagio;
        }

        private static void navegarEstagio(string tipoProcura, string codigoSessao, ProcessoInfo processoInfo)
        {   processoInfo.EstagioValido = null;
            Type tipoEstagio = null;
            foreach (Type tipo in Assembly.GetExecutingAssembly().GetTypes())
            {
                EstagioAttribute estagioAttribute =(EstagioAttribute)tipo.GetCustomAttributes(typeof(EstagioAttribute), false).FirstOrDefault();
                if (estagioAttribute != null && estagioAttribute.TipoProcesso == processoInfo.GetType())
                {   switch(tipoProcura)
                    {
                        case "estagioInicial":
                            if (estagioAttribute.TipoEstagioAnterior == null)
                                tipoEstagio = tipo;
                            break;
                        case "ultimoEstagio":
                            if (estagioAttribute.TipoProximoEstagio == null)
                            {   tipoEstagio = tipo;
                                processoInfo.StatusProcesso = ProcessoStatusEnum.EmAndamento;
                            }
                            break;
                        case "proximoEstagio":
                            if (tipo == processoInfo.TipoEstagioAtual)
                            {   tipoEstagio = estagioAttribute.TipoProximoEstagio;
                                if (tipoEstagio == null)
                                    processoInfo.StatusProcesso = ProcessoStatusEnum.Finalizado;
                            }
                            break;
                        case "estagioAnterior":
                            if (tipo == processoInfo.TipoEstagioAtual)
                            {   tipoEstagio = estagioAttribute.TipoEstagioAnterior;
                                if (tipoEstagio == null)
                                    processoInfo.StatusProcesso = ProcessoStatusEnum.Cancelado;
                            }
                            break;
                        case "estagioValidacao":
                            if (estagioAttribute.EhValidacao == true)
                                tipoEstagio = tipo;
                            break;
                    }
                }

                if (tipoEstagio != null)
                    break;
            }

            processoInfo.TipoEstagioAtual = tipoEstagio;
            if (tipoEstagio != null)
            {
                EstagioBase estagio = GerenciadorProcesso.criarEstagio(codigoSessao, tipoEstagio, processoInfo);
                switch (tipoProcura)
                {
                    case "estagioInicial":
                    case "proximoEstagio":
                        estagio.Inicializar(codigoSessao);
                        break;
                    case "ultimoEstagio":
                    case "estagioAnterior":
                        processoInfo.StatusEstagio = EstagioStatusEnum.EmCancelamento;
                        processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));
                        estagio.Cancelar(codigoSessao);
                        processoInfo.StatusEstagio = EstagioStatusEnum.EmCancelamento;
                        processoInfo.Atualizar(PersistenciaHelper.Salvar<ProcessoInfo>(codigoSessao, processoInfo));
                        break;
                }
            }
        }

        public static void MoverArquivo(string fileNameAndPathDestination, string fileNameAndPathSource)
        {
            if (System.IO.File.Exists(fileNameAndPathDestination))
            {
                System.IO.File.Delete(fileNameAndPathDestination);
            }
            System.IO.File.Move(fileNameAndPathSource, fileNameAndPathDestination);

        }
    }
}
