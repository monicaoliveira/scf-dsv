﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Resource.Framework.Library.Servicos;
using Resource.Carrefour.SCF.Contratos.Principal;
using System.Timers;
using System.Threading;
using Resource.Carrefour.SCF.Persistencia.Db;
using Resource.Carrefour.SCF.Contratos.Principal.Dados.Log;

namespace Resource.Carrefour.SCF.Sistemas.Principal
{

    public class ServicoConsolidadorDeSaldo : IServicoConsolidadorDeSaldo
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        #region Atributos
        private System.Timers.Timer _timer;
        private ServicoStatus _status;
         
        #endregion

        #region Construtores
        public ServicoConsolidadorDeSaldo()
        {
            _timer = new System.Timers.Timer(86400000);
            _timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
        }
        #endregion

        void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.ConsolidarSaldo();
        }

        #region IServicoControlavel Members

        public void IniciarServico()
        {
            _status = ServicoStatus.EmExecucao;
            
            this.ConsolidarSaldo();
            _timer.Start();

            log.Info("Serviço iniciado com sucesso.");
        }

        public void PararServico()
        {
            _status = ServicoStatus.Parado;
            _timer.Stop();
            log.Info("serviço parado com sucesso.");
        }

        public ServicoStatus ReceberStatusServico()
        {
            return _status;
        }

        #endregion

        private void ConsolidarSaldo()
        {
            try
            {
                Procedures.PR_SALDO_CONSOLIDADO_S(new PR_SALDO_CONSOLIDADO_S_Request()
                {
                    DataSaldo = DateTime.Now
                });

                LogSCFInfo scf = new LogSCFInfo();
                scf.DataLog = DateTime.Now.AddDays(-1);
                scf.Descricao = "Saldo do dia " + DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy") + " calculado com sucesso.";
                scf.TipoLog = "ServicoConsolidadorDeSaldo";
                scf.TipoOrigem = "ServicoConsolidadorDeSaldo._timer_Elapsed";

                log.Info(scf.Descricao);

            }
            catch (Exception ex)
            {
                _status = ServicoStatus.Erro;
                LogSCFInfo scf = new LogSCFInfo();
                scf.DataLog = DateTime.Now.AddDays(-1);
                scf.Descricao = ex.ToString();
                scf.TipoLog = "ServicoConsolidadorDeSaldo";
                scf.TipoOrigem = "ServicoConsolidadorDeSaldo._timer_Elapsed";

                log.Error(ex.Message, ex);

            }
        }
    }
}
